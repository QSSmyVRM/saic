//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
//FB 2027 - starts
namespace myVRM.BusinessLayer
{
    #region References
    using System;
    using System.Xml;
    using System.Diagnostics;
    using System.IO;
    using cryptography;
    #endregion

    public class Config
    {
        internal String errMsg = null;
        internal string globalConfigPath, databaseLogin, databaseServer, databasePwd, databaseName, client, trustedConnection;//FB 2700
        internal string mailLogoPath; //FB 2136
        internal int databaseConTimeout;
        internal enum eDatabaseType { SQLSERVER, ORACLE, MYSQL, DB2 };
        internal eDatabaseType databaseType;
        internal bool ldapEnabled;
        internal int ConfReccurance, CosignEnable, MaxConfDurationInHours;


        #region Config 
        internal Config(String path)
        {
            if (path != null)
                globalConfigPath = path;
            else
            {
                this.errMsg = "Error reading global config file.";
            }
        }
        #endregion

        #region Initialize
        internal bool Initialize()
        {

            bool ret = Read_Global_Config_File();
            if (!ret)
            {
                this.errMsg = "Error reading global config file.";
                return (false);
            }
			//FB 2136
            ret = false;
            ret = Read_Local_Config_File();
            if (!ret)
            {
                this.errMsg = "Error reading local config file.";
                return (false);
            }

            return (true);
        }
        #endregion

        #region Read_Global_Config_File
        private bool Read_Global_Config_File()
        {
            try
            {
                XmlDocument xmlConfig = new XmlDocument();
                xmlConfig.Load(globalConfigPath + "VRMConfig.xml");

                // Db Server Address
                databaseServer = xmlConfig.SelectSingleNode("//VRMConfig/Database/Server").InnerXml.Trim();

                // Db Name
                databaseName = xmlConfig.SelectSingleNode("//VRMConfig/Database/Name").InnerXml.Trim();

                // Db Login
                databaseLogin = xmlConfig.SelectSingleNode("//VRMConfig/Database/Login").InnerXml.Trim();

                // Db Connection Timeout
                try
                {
                    databaseConTimeout = Int32.Parse(xmlConfig.SelectSingleNode("//VRMConfig/Database/ConnectionTimout").InnerXml.Trim());
                }
                catch (Exception)
                {
                    databaseConTimeout = 10;
                }
                // Db Trunsted Connection FB 2700
                try
                {
                    trustedConnection = xmlConfig.SelectSingleNode("//VRMConfig/Database/trustedConnection").InnerXml.Trim();
                }
                catch (Exception)
                {
                    trustedConnection = "";
                }


                // Db Password
                string encryptedPwd = xmlConfig.SelectSingleNode("//VRMConfig/Database/Password").InnerXml.Trim();
                // decrypt password
                if (encryptedPwd.Trim() != "")// FB 2700
                {
                    cryptography.Crypto crypto = new Crypto();
                    databasePwd = crypto.decrypt(encryptedPwd);
                }

                // Db type
                string databaseTypes = xmlConfig.SelectSingleNode("//VRMConfig/Database/Type").InnerXml.Trim();
                if (databaseTypes.CompareTo("SA_SQLServer_Client") != 0)
                {
                    // Only MS-SQLSERVER-2000 is supported currently. 
                    // Return error if any other string.
                    this.errMsg = "This database type is not supported. Only MS SqlServer 2000/2005 are supported.";
                    return (false);
                }
                else
                {
                    // It is SQL Server .
                    databaseType = eDatabaseType.SQLSERVER;
                }

                // Client switch
                client = xmlConfig.SelectSingleNode("//VRMConfig/Client").InnerXml.ToUpper().Trim();

                // Ldap enabled or disabled
                try
                {
                    string ldap = xmlConfig.SelectSingleNode("//VRMConfig/SSOMode").InnerXml.ToUpper().Trim();
                    if (ldap.Equals("YES"))
                    {
                        ldapEnabled = true;
                    }
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading global config file. SSOMode is invalid.";
                    return false;
                }

				//Conf_Reccurance
                try 
                {
                    Int32.TryParse(xmlConfig.SelectSingleNode("//VRMConfig/Conf_Reccurance").InnerXml.Trim(), out ConfReccurance);
                    if (ConfReccurance <= 0)
                        ConfReccurance = 50;
                }
                catch
                {
                    ConfReccurance = 50;
                }

                //MaxConferenceDurationInHours
                try
                {
                    Int32.TryParse(xmlConfig.SelectSingleNode("//VRMConfig/MaxConferenceDurationInHours").InnerXml.Trim(), out MaxConfDurationInHours);
                    if (MaxConfDurationInHours <= 0)
                        MaxConfDurationInHours = 24;
                }
                catch
                {
                    MaxConfDurationInHours = 24;
                }

                //CosignEnable
                try
                {
                    Int32.TryParse(xmlConfig.SelectSingleNode("//VRMConfig/CosignEnable").InnerXml.Trim(), out CosignEnable);
                    if (CosignEnable < 0)
                        CosignEnable = 0;
                }
                catch
                {
                    CosignEnable = 0;
                }
                
                return (true);
            }
            catch (Exception e)
            {
                this.errMsg = "Error reading global config file. Exception Message : " + e.Message;
                return (false);
            }
        }
        #endregion

        //FB 2136
        #region Read_Local_Config_File
        private bool Read_Local_Config_File()
        {
            // Read from XML config file 
            try
            {
                // Open an XML file 
                XmlDocument xmlConfig = new XmlDocument();
                xmlConfig.Load(globalConfigPath + "VRMRTCConfig.xml");

                //Mail Logo Path
                try
                {
                    mailLogoPath = xmlConfig.SelectSingleNode("//RTCConfig/MailLogoPath").InnerXml.Trim();
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading local config file. MailLogoPath param is invalid.";
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Error reading local config file. Exception Message : " + e.Message;
                return false;
            }
        }
        #endregion
    }
}
//FB 2027 - End
