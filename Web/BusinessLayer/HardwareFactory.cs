//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.Data;
using NHibernate;
using NHibernate.Criterion;

using log4net;

using myVRM.DataLayer;

namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Business Layer Logic for all hardware related functions. 
    /// </summary>
    /// 
    public class HardwareFactory
    {
        #region Data Members
        private hardwareDAO m_hardwareDAO;

        private IEptDao m_IeptDao;
        private IMCUDao m_ImcuDao;
        private static log4net.ILog m_log;
        private string m_configPath;

        private conferenceDAO m_confDAO;
        private IConferenceDAO m_vrmConfDAO;
        private IConfRoomDAO m_IconfRoom;
        private IConfUserDAO m_IconfUser;
        private IRoomDAO m_IRoomDAO; //FB 1886,1552
        private LocationDAO m_locDAO; //FB 1886,1552
        private IMCUIPServicesDao m_IipServices;

        private IMCUE164ServicesDao m_IE164Services; // FB 2636

        private IMCUISDNServicesDao m_IisdnServices;
        private IMCUMPIServicesDao m_ImpiService;
        private IMCUCardListDao m_IcardList;
        private IMCUApproverDao m_Iapprover;
        private ILanguageDAO m_ILanguageDAO; //FB 2486
        private GeneralDAO m_generalDAO; //FB 2486

        private userDAO m_usrDAO;
        private IUserDao m_vrmUserDAO;
        private IInactiveUserDao m_vrmIUserDAO; //FB 1462
        private IUserRolesDao m_IUserRolesDao;//ZD 100263
        private vrmSystemFactory m_systemFactory;

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private OrgData orgInfo;
        private const int defaultOrgId = 11;  //Default organization
        private int organizationID = 0;

        private IConfCascadeDAO m_IconfCascade;//FB 1937
		private IMessageDao m_msgDAO; //FB 2486
        private myVRMSearch m_SearchFactory; //FB 2539

        private IMCUProfilesDao m_IMcuProfilesDAO;//FB 2591
        private ns_SqlHelper.SqlHelper m_bridgelayer = null; //FB 2441
        private IExtMCUServiceDao m_IExtMCUService;//FB 2556
        private IExtMCUSiloDao m_IExtMCUSilo;//FB 2556


        #endregion

        #region HardwareFactory
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        public HardwareFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                m_OrgDAO = new orgDAO(m_configPath, m_log); //Organization Module Fixes
                m_generalDAO = new GeneralDAO(m_configPath, m_log); //FB 2486

                m_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);
                m_IeptDao = m_hardwareDAO.GetEptDao();
                m_ImcuDao = m_hardwareDAO.GetMCUDao();

                m_usrDAO = new userDAO(obj.ConfigPath, obj.log);
                m_vrmUserDAO = m_usrDAO.GetUserDao();
                m_IUserRolesDao = m_usrDAO.GetUserRolesDao();//ZD 100263

                m_vrmIUserDAO = m_usrDAO.GetInactiveUserDao(); //FB 1462

                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log);

                m_vrmConfDAO = m_confDAO.GetConferenceDao();
                m_IconfRoom = m_confDAO.GetConfRoomDao();
                m_IconfUser = m_confDAO.GetConfUserDao();

                m_IconfCascade = m_confDAO.GetConfCascadeDao();//FB 1937

                m_IE164Services = m_hardwareDAO.GetE164ServicesDao(); // FB 2636

                m_IipServices = m_hardwareDAO.GetMCUIPServicesDao();
                m_IisdnServices = m_hardwareDAO.GetMCUISDNServicesDao();
                m_ImpiService = m_hardwareDAO.GetMCUMPIServicesDao();
                m_IcardList = m_hardwareDAO.GetMCUCardListsDao();
                m_Iapprover = m_hardwareDAO.GetMCUApproverDao();
                m_systemFactory = new vrmSystemFactory(ref obj);
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();   //Organization Module Fixes
                m_locDAO = new LocationDAO(m_configPath, obj.log); //FB 1886,1552
                m_IRoomDAO = m_locDAO.GetRoomDAO(); //FB 1886,1552
                m_msgDAO = m_hardwareDAO.GetMessageDao(); //FB 2486
                m_ILanguageDAO = m_generalDAO.GetLanguageDAO(); //FB 2486
                m_SearchFactory = new myVRMSearch(obj); //FB 2539
                m_IMcuProfilesDAO = m_hardwareDAO.GetMCUProfilesDao();//FB 2591 
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region DeleteEndpoint
        public bool DeleteEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder searchOutXml = new StringBuilder(); //String concatenation changed to StringBuilder for Performance - FB 1820
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//DeleteEndpoint/UserID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//DeleteEndpoint/EndPointID");
                string endpointID = node.InnerXml.Trim();

                // this will delete ALL profiles in the endpoint table with the same endpoint ID 
                // (effectivly deleting the endpoint)
                DateTime dt = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref dt);
                dt.AddMinutes(-5);

                List<vrmEndPoint> eptList = new List<vrmEndPoint>();

                //// first check if ANY are in use 
                //string rmQuery = "SELECT C.confid FROM myVRM.DataLayer.vrmConference C, vrmConfRoom R ";
                //rmQuery += " WHERE C.confid = R.confid AND C.instanceid = R.instanceid ";
                //rmQuery += " AND R.endpointId = " + endpointID;
                //rmQuery += " AND (C.deleted = 0 AND C.confdate >= '";
                //rmQuery += dt.ToString("d") + " " + dt.ToString("t") + "' OR C.status = 5)";

                // first check if ANY are in use      //Endpoint functionality  Fix
                string rmQuery = "SELECT R.Name FROM myVRM.DataLayer.vrmRoom R ";
                rmQuery += " WHERE R.Disabled=0 and R.endpointid = " + endpointID;//FB 2494

                IList results = m_IeptDao.execQuery(rmQuery);

                // cant delete this one, it is in use....
                if (results.Count > 0)
                {
                    String name = "";
                    name = results[0].ToString();

                    for (int i = 0; i < results.Count; i++)
                    {
                        if (name != results[i].ToString())
                            name += "," + results[i].ToString();
                    }
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("Cannot delete the endpoint: endpoint is associated with the room(s)  " + name);
                    myVRMException myVRMEx = new myVRMException(507);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;      //Endpoint Search Fix
                }

                criterionList.Add(Expression.Eq("endpointid", Int32.Parse(endpointID)));
                criterionList.Add(Expression.Eq("deleted", 0));
                eptList = m_IeptDao.GetByCriteria(criterionList);

                // use soft delete
                foreach (vrmEndPoint ve in eptList)
                    ve.deleted = 1;

                m_IeptDao.SaveOrUpdateList(eptList);
                //FB 1820 - Start
                searchOutXml.Append("<DeleteEndpoint>");
                searchOutXml.Append("<UserID>" + userID + "</UserID>");
                searchOutXml.Append("<ID>" + endpointID + "</ID>");
                searchOutXml.Append("</DeleteEndpoint>");
                obj.outXml = searchOutXml.ToString();
                //FB 1820 - Ends
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetAddressType
        public bool GetAddressType(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmAddressType> addressType = vrmGen.getAddressType();

                obj.outXml += "<GetAddressType>";
                obj.outXml += "<AddressType>";
                foreach (vrmAddressType at in addressType)
                {
                    obj.outXml += "<Type>";
                    obj.outXml += "<ID>" + at.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + at.name + "</Name>";
                    obj.outXml += "</Type>";
                }
                obj.outXml += "</AddressType>";
                obj.outXml += "</GetAddressType>";

                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetBridges
        public bool GetBridges(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetBridges/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//GetBridges/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes
                //FB 2593
                node = xd.SelectSingleNode("//GetBridges/SearchType"); 
                string searchType = "";
                if (node != null)
                    searchType = node.InnerXml.Trim();

                List<ICriterion> criterionLst = new List<ICriterion>();
                if(searchType == "") //FB 2593
                    criterionLst.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1))); // FB 1920
                criterionLst.Add(Expression.Eq("deleted", 0));


                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionLst);//Organization Module Fixes
                obj.outXml += "<Bridges>";
                foreach (vrmMCU mcu in MCUList)
                {
                    obj.outXml += "<Bridge>";
                    obj.outXml += "<BridgeID>" + mcu.BridgeID.ToString() + "</BridgeID> ";
                    //FB 1920 starts
                    if (organizationID != 11)
                    {
                        if (mcu.isPublic.ToString() == "1")
                            mcu.BridgeName = mcu.BridgeName + "(*)";
                    }
                    // FB 1920 Ends
                    obj.outXml += "<BridgeName>" + mcu.BridgeName + "</BridgeName>";
                    obj.outXml += "<BridgeType>" + mcu.MCUType.id.ToString() + "</BridgeType>";//FB 2839
                    obj.outXml += "<ConferenceServiceID>" + mcu.ConfServiceID + "</ConferenceServiceID>";//FB 2839
					obj.outXml += "<OrgId>" + mcu.orgId + "</OrgId>"; //FB 2593
                    obj.outXml += "<Public>" + mcu.isPublic + "</Public>"; //FB 2593
                    obj.outXml += "</Bridge>";
                }
                obj.outXml += "</Bridges>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetEndpointDetails
        public bool GetEndpointDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            int confType = 0;//FB 2602
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder searchOutXml = new StringBuilder(); //String concatenation changed to StringBuilder for Performance - FB 1820
            vrmMCU MCU = null;//FB 2839
            int userid= 0;
            vrmUser Vuser = null;//1000263_Nov11
            vrmUserRoles VuserRole = null;//1000263_Nov11
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//EndpointDetails/UserID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//EndpointDetails/EndpointID");
                string EndpointID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//EndpointDetails/confType");//FB 2602
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out confType);

                m_IeptDao.addOrderBy(Order.Desc("isDefault"));
                m_IeptDao.addOrderBy(Order.Asc("profileId"));

                criterionList.Add(Expression.Eq("endpointid", Int32.Parse(EndpointID)));
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);

                //ZD 100263_Nov11 Start
                int.TryParse(userID, out userid);
                Vuser = m_vrmUserDAO.GetByUserId(userid);
                VuserRole = m_IUserRolesDao.GetById(Vuser.roleID);
                if (VuserRole.crossaccess == 0 && Vuser.companyId != epList[0].orgId)
                {
                    obj.outXml = "<error>-1</error>";
                    return false;
                }
                //ZD 100263_Nov11 End

                m_IeptDao.clearOrderBy();

                List<ICriterion> ProfilecriterionList = new List<ICriterion>();
               
                //FB 1820 - Starts
                searchOutXml.Append("<EndpointDetails>");
                int idx = 0;

                foreach (vrmEndPoint ep in epList)
                {
                    if (ep.isTelePresence == 1 && confType == 4)//FB 2602
                        continue;
                    if (idx == 0)
                    {
                        searchOutXml.Append("<Endpoint>");
                        searchOutXml.Append("<ID>" + ep.endpointid.ToString() + "</ID>");
                        searchOutXml.Append("<Name>" + ep.name + "</Name>");
                        //Code Commented for FB 1361 - Start
                        //FB 1886,1552 - Start
                        //criterionList = new List<ICriterion>();
                        //criterionList.Add(Expression.Eq("endpointid", ep.endpointid));
                        //criterionList.Add(Expression.Eq("Disabled", 0));
                        //List<vrmRoom> roomList = m_IRoomDAO.GetByCriteria(criterionList);
                        String roomnames = " ";
                        int j = 1;
                        //if (roomList.Count >= 1)
                        //{
                        //    for (int i = 0; i < roomList.Count; i++)
                        //    {
                        //        roomnames += j + ". ";
                        //        roomnames += roomList[i].Name.ToString();
                        //        if (i != (roomList.Count - 1))
                        //            roomnames += " % ";
                        //        j++;
                        //    }
                        //}
                        //Code Commented for FB 1361 - End

                        searchOutXml.Append("<RoomName>" + roomnames + "</RoomName>");
                        //FB 1886,1552 - End
                        searchOutXml.Append("<DefaultProfileID>" +
                                                 ep.profileId.ToString() + "</DefaultProfileID>");
                        searchOutXml.Append("<Profiles>");
                        idx++;
                    }
                    searchOutXml.Append("<Profile>");
                    searchOutXml.Append("<ProfileID>" + ep.profileId.ToString() + "</ProfileID>");
                    searchOutXml.Append("<ProfileName>" + ep.profileName + "</ProfileName>");
                    searchOutXml.Append("<EncryptionPreferred>" +
                                                    ep.encrypted.ToString() + "</EncryptionPreferred>");
                    searchOutXml.Append("<AddressType>" + ep.addresstype.ToString() + "</AddressType>");
                    if (ep.password != null)
                        searchOutXml.Append("<Password>" + ep.password + "</Password> ");
                    else
                        searchOutXml.Append("<Password></Password> ");
                    searchOutXml.Append("<Address>" + ep.address + "</Address>");
                    searchOutXml.Append("<URL>" + ep.endptURL + "</URL>");
                    searchOutXml.Append("<IsOutside>" + ep.outsidenetwork.ToString() + "</IsOutside>");
                    if (ep.GateKeeeperAddress != null)//ZD 100132
                        searchOutXml.Append("<GateKeeeperAddress>" + ep.GateKeeeperAddress.ToString() + "</GateKeeeperAddress>");//ZEN 100132
                    else
                        searchOutXml.Append("<GateKeeeperAddress></GateKeeeperAddress>");
                    
                    searchOutXml.Append("<VideoEquipment>" +
                                                    ep.videoequipmentid.ToString() + "</VideoEquipment> ");
                    searchOutXml.Append("<LineRate>" + ep.linerateid.ToString() + "</LineRate>");
                    searchOutXml.Append("<Bridge>" + ep.bridgeid.ToString() + "</Bridge>");
                    if (ep.bridgeid > 0) //FB 2839 Start
                    {
                        MCU = m_ImcuDao.GetById(ep.bridgeid);

                        ProfilecriterionList.Add(Expression.Eq("MCUId", ep.bridgeid));
                        ProfilecriterionList.Add(Expression.Eq("ProfileId", MCU.ConfServiceID));
                        List<vrmMCUProfiles> MCUProf = m_IMcuProfilesDAO.GetByCriteria(ProfilecriterionList);

                        searchOutXml.Append("<BridgeProfileID>" + MCU.ConfServiceID + "</BridgeProfileID>"); //FB 2839 Ends
                        if (MCUProf.Count > 0)
                            searchOutXml.Append("<BridgeProfileName>" + MCUProf[0].ProfileName + "</BridgeProfileName>");
                        else
                            searchOutXml.Append("<BridgeProfileName></BridgeProfileName>"); //FB 2839 Ends
                    }
                    else
                    {
                        searchOutXml.Append("<BridgeProfileID>0</BridgeProfileID>");
                        searchOutXml.Append("<BridgeProfileName></BridgeProfileName>"); //FB 2839 Ends
                    }//FB 2839 Ends
                    searchOutXml.Append("<ConnectionType>" + ep.connectiontype.ToString() + "</ConnectionType>");
                    searchOutXml.Append("<DefaultProtocol>" + ep.protocol.ToString() + "</DefaultProtocol>");
                    searchOutXml.Append("<MCUAddress>" + ep.MCUAddress + "</MCUAddress>");
                    searchOutXml.Append("<MCUAddressType>" + ep.MCUAddressType.ToString() + "</MCUAddressType>");
                    searchOutXml.Append("<ExchangeID>" + ep.ExchangeID + "</ExchangeID>"); //Cisco Telepresence fix
                    //Code Added For FB1422 -Start
                    searchOutXml.Append("<TelnetAPI>" + ep.TelnetAPI.ToString() + "</TelnetAPI>");
                    //Code Added For FB1422 -End   
                    searchOutXml.Append("<IsCalendarInvite>" + ep.CalendarInvite.ToString() + "</IsCalendarInvite>");
                    searchOutXml.Append("<ApiPortno>" + ep.ApiPortNo.ToString() + "</ApiPortno>");//API Port...
                    searchOutXml.Append("<conferenceCode>" + ep.ConferenceCode + "</conferenceCode>");//FB 1642-Audio add on
                    searchOutXml.Append("<leaderPin>" + ep.LeaderPin + "</leaderPin>");//FB 1642-Audio add on
                    searchOutXml.Append("<MultiCodec>");//FB 2400 start
                    if (ep.MultiCodecAddress != null)
                    {
                        String[] multiCodec = ep.MultiCodecAddress.Split('�');
                        for (int i = 0; i < multiCodec.Length; i++)
                        {
                            if (multiCodec[i].Trim() != "")
                                searchOutXml.Append("<Address>" + multiCodec[i].Trim() + "</Address>");

                        }
                    }
                    searchOutXml.Append("</MultiCodec>");
                    searchOutXml.Append("<RearSecCameraAddress>" + ep.RearSecCameraAddress + "</RearSecCameraAddress>");
                    searchOutXml.Append("<isTelePresence>" + ep.isTelePresence + "</isTelePresence>");//FB 2400 end
                    searchOutXml.Append("<Secured>" + ep.Secured + "</Secured>");//FB 2595
                    searchOutXml.Append("<NetworkURL>" + ep.NetworkURL + "</NetworkURL>");//FB 2595
                    searchOutXml.Append("<Securedport>" + ep.Secureport + "</Securedport>");//FB 2595
                    searchOutXml.Append("</Profile>");
                }
                if (idx > 0)
                {
                    searchOutXml.Append("</Profiles>");
                    searchOutXml.Append("</Endpoint>");
                }
                else // empty tags
                {
                    searchOutXml.Append("<Profiles/>");
                    searchOutXml.Append("<Endpoint/>");
                }

                searchOutXml.Append("</EndpointDetails>");
                obj.outXml = searchOutXml.ToString();
                //FB 1820 - Ends
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetLineRate
        public bool GetLineRate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmLineRate> LineRate = vrmGen.getLineRate();

                obj.outXml += "<LineRate>";
                foreach (vrmLineRate lr in LineRate)
                {
                    obj.outXml += "<Rate>";
                    obj.outXml += "<LineRateID>" + lr.Id.ToString() + "</LineRateID>";
                    obj.outXml += "<LineRateName>" + lr.LineRateType.ToString() + "</LineRateName>";
                    obj.outXml += "</Rate>";
                }
                obj.outXml += "</LineRate>";

                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoEquipment
        public bool GetVideoEquipment(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmVideoEquipment> videoList = vrmGen.getVideoEquipment();

                obj.outXml += "<VideoEquipment>";
                foreach (vrmVideoEquipment ve in videoList)
                {
                    obj.outXml += "<Equipment>";
                    obj.outXml += "<VideoEquipmentID>" + ve.Id.ToString() + "</VideoEquipmentID>";
                    obj.outXml += "<VideoEquipmentName>" + ve.VEName + "</VideoEquipmentName>";
                    obj.outXml += "</Equipment>";
                }
                obj.outXml += "</VideoEquipment>";


            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region SetEndpoint
        public bool SetEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            int maxProfileId = 0;
            int isPublicEP = 0;//FB 2594


            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SetEndpoint/EndpointID");
                string EndpointID = node.InnerXml.Trim();
                if (EndpointID.ToLower() == "new")
                {
                    EndpointID = "0";
                }

                node = xd.SelectSingleNode("//SetEndpoint/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetEndpoint/EndpointName");
                string EndpointName = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetEndpoint/EntityType");
                string EntityType = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetEndpoint/UserID");
                string UserID = node.InnerXml.Trim();

                if (EntityType.Trim().ToUpper() != "U" && EndpointID == "0")
                {

                    List<ICriterion> selcnt = new List<ICriterion>();
                    //selcnt.Add(Expression.Eq("deleted", 0));
                    selcnt.Add(Expression.Eq("deleted", 0));
                    selcnt.Add(Expression.Eq("isDefault", 1));
                    selcnt.Add(Expression.Eq("orgId", organizationID));//code added for organization
                    selcnt.Add(Expression.Eq("Extendpoint", 0)); //FB 2549
                    selcnt.Add(Expression.Eq("PublicEndPoint", 0)); //FB 2594

                    List<vrmEndPoint> checkEptCount = m_IeptDao.GetByCriteria(selcnt);

                    OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (checkEptCount.Count >= orgdt.MaxEndpoints)
                    {
                        //FB 1881 start
                        myVRMException ex = new myVRMException(458);
                        m_log.Error("Error EndPoint Limit exceeeded: ", ex);
                        //obj.outXml = myVRMException.toXml(ex.Message);
                        obj.outXml = ex.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }
                }

                XmlNodeList itemList, multiCodelist; //FB 2400

                List<vrmEndPoint> endPoints = new List<vrmEndPoint>();

                itemList = xd.GetElementsByTagName("Profile");
                int pId = 0, eId = 0;

                foreach (XmlNode innerNode in itemList)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    vrmEndPoint ve = new vrmEndPoint();
                    XmlElement itemElement = (XmlElement)innerNode;

                    if (EndpointID == "0")
                    {
                        if (pId == 0)
                        {
                            // FB 114 (AG 04/14/08)
                            // check if EP name exists (but not in case of user end point)
                            if (EntityType != "U")
                            {
                                List<ICriterion> checkName = new List<ICriterion>();
                                checkName.Add(Expression.Eq("name", EndpointName));
                                checkName.Add(Expression.Not(Expression.Eq("profileId", 0)));
                                checkName.Add(Expression.Eq("deleted", 0));
                                checkName.Add(Expression.Eq("orgId", organizationID));
                                List<vrmEndPoint> ep = m_IeptDao.GetByCriteria(checkName);

                                if (ep.Count > 0)
                                {
                                    //FB 1881 start
                                    //myVRMException e = new myVRMException("Error endpoint name: " + 
                                    //                        EndpointName + " already exists");
                                    //obj.outXml = myVRMException.toXml(e.Message);
                                    myVRMException myVRMEx = new myVRMException(434);
                                    obj.outXml = myVRMEx.FetchErrorMsg();
                                    //FB 1881 end
                                    return false;
                                }
                            }
                            m_IeptDao.addProjection(Projections.Max("endpointid"));
                            IList maxId = m_IeptDao.GetObjectByCriteria(new List<ICriterion>());
                            if (maxId[0] != null)
                                eId = ((int)maxId[0]) + 1;
                            else
                                eId = 1;
                            m_IeptDao.clearProjection();
                        }
                        pId++;
                        ve.endpointid = eId;
                        ve.profileId = pId;
                        ve.PublicEndPoint = 0; //FB 2594
                    }
                    else
                    {
                        ve.endpointid = Int32.Parse(EndpointID);

                        //FB 2594 Starts
                        List<ICriterion> criterionPublicEp = new List<ICriterion>();
                        criterionPublicEp.Add(Expression.Eq("endpointid", ve.endpointid));
                        List<vrmEndPoint> eplist = m_IeptDao.GetByCriteria(criterionPublicEp);
                        isPublicEP = eplist[0].PublicEndPoint;
                        //FB 2594 Ends
                        // 114 do not allow dupllicate endpoint in update
                        if (EntityType.ToUpper() != "U")
                        {
                            List<ICriterion> checkName = new List<ICriterion>();
                            checkName.Add(Expression.Eq("name", EndpointName));
                            checkName.Add(Expression.Eq("orgId", organizationID));
                            checkName.Add(Expression.Eq("deleted", 0));
                            checkName.Add(Expression.Not(Expression.Eq("profileId", 0)));
                            checkName.Add(Expression.Not(Expression.Eq("endpointid", ve.endpointid)));
                            List<vrmEndPoint> ep = m_IeptDao.GetByCriteria(checkName);

                            if (ep.Count > 0)
                            {
                                //FB 1881 start
                                //myVRMException e = new myVRMException("Error endpoint name: " + EndpointName + " already exists");
                                //obj.outXml = myVRMException.toXml(e.Message);
                                myVRMException myVRMEx = new myVRMException(434);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                //FB 1881 end
                                return false;
                            }
                        }

                        string sProfileId = itemElement.GetElementsByTagName("ProfileID")[0].InnerText;
                        if (sProfileId.ToLower() == "new")
                        {
                            ve.uId = 0;
                            if (EntityType.ToUpper() != "U")
                            {
                                List<ICriterion> maxProfile = new List<ICriterion>();
                                maxProfile.Add(Expression.Eq("endpointid", ve.endpointid));
                                m_IeptDao.addProjection(Projections.Max("profileId"));
                                IList maxId = m_IeptDao.GetObjectByCriteria(maxProfile);
                                if (maxId[0] != null)
                                    maxProfileId = ((int)maxId[0]) + 1;
                                else
                                    maxProfileId = 1;
                                m_IeptDao.clearProjection();

                                ve.profileId = maxProfileId;
                            }
                            // user profile
                            else
                            {
                                ve.profileId = 0;
                            }
                        }
                        else
                        {
                            // get uid
                            ve.profileId = Int32.Parse(sProfileId);

                            criterionList.Add(Expression.And(
                                              Expression.Eq("endpointid", ve.endpointid),
                                              Expression.Eq("profileId", ve.profileId)));

                            List<vrmEndPoint> epl = m_IeptDao.GetByCriteria(criterionList);
                            if (epl.Count > 0)
                            {
                                ve.uId = epl[0].uId;
                                ve.password = epl[0].password; //FB 3054
                            }

                            else
                            {
                                //FB 1881 start
                                //myVRMException e = new myVRMException("Cannot update endpoint: " + EndpointID + "/" + ve.profileId.ToString() +
                                // "  Endpoint not found");
                                myVRMException e = new myVRMException(508);
                                m_log.Error(e.Message);
                                obj.outXml = e.FetchErrorMsg();
                                //FB 1881 end
                                bRet = false;
                            }


                        }
                    }
                    ve.name = EndpointName;
                    ve.profileName = itemElement.GetElementsByTagName("ProfileName")[0].InnerText;
                    ve.deleted = Int32.Parse(itemElement.GetElementsByTagName("Deleted")[0].InnerText);
                    ve.isDefault = Int32.Parse(itemElement.GetElementsByTagName("Default")[0].InnerText);
                    ve.encrypted = Int32.Parse(itemElement.GetElementsByTagName("EncryptionPreferred")[0].InnerText);
                    ve.addresstype = Int32.Parse(itemElement.GetElementsByTagName("AddressType")[0].InnerText);
                    if (itemElement.GetElementsByTagName("Password")[0] != null) //FB 3054
                        ve.password = itemElement.GetElementsByTagName("Password")[0].InnerText;
                    ve.address = itemElement.GetElementsByTagName("Address")[0].InnerText;
                    ve.endptURL = itemElement.GetElementsByTagName("URL")[0].InnerText;
                    ve.outsidenetwork =
                                     Int32.Parse(itemElement.GetElementsByTagName("IsOutside")[0].InnerText);
                    
                    ve.GateKeeeperAddress = "";//ZD 100132
                    if (itemElement.GetElementsByTagName("GateKeeeperAddress")[0] != null)
                        ve.GateKeeeperAddress = itemElement.GetElementsByTagName("GateKeeeperAddress")[0].InnerText;
                    
                    ve.connectiontype =
                                    Int32.Parse(itemElement.GetElementsByTagName("ConnectionType")[0].InnerText);
                    ve.videoequipmentid =
                                    Int32.Parse(itemElement.GetElementsByTagName("VideoEquipment")[0].InnerText);
                    ve.linerateid = Int32.Parse(itemElement.GetElementsByTagName("LineRate")[0].InnerText);
                    ve.bridgeid = Int32.Parse(itemElement.GetElementsByTagName("Bridge")[0].InnerText);
                    ve.MCUAddress = itemElement.GetElementsByTagName("MCUAddress")[0].InnerText;
                    ve.MCUAddressType =
                                     Int32.Parse(itemElement.GetElementsByTagName("MCUAddressType")[0].InnerText);

                    ve.protocol = Int32.Parse(itemElement.GetElementsByTagName("DefaultProtocol")[0].InnerText);
                    //Code Added For FB1422- Start
                    ve.TelnetAPI = Int32.Parse(itemElement.GetElementsByTagName("TelnetAPI")[0].InnerText);
                    //Code Added For FB1422- End
                    ve.ExchangeID = itemElement.GetElementsByTagName("ExchangeID")[0].InnerText; //Cisco Telepresence fix
                    if (itemElement.GetElementsByTagName("IsCalendarInvite").Count != 0)  //Cisco ICAL FB 1602
                        ve.CalendarInvite = Int32.Parse(itemElement.GetElementsByTagName("IsCalendarInvite")[0].InnerText);  //Cisco ICAL FB 1602
                    //Code added for organization module
                    //FB 1642 Audio Add On Starts..
                    String conferenceCode = "";
                    String leaderPin = "";
                    if (itemElement.GetElementsByTagName("conferenceCode").Count > 0)
                        conferenceCode = itemElement.GetElementsByTagName("conferenceCode")[0].InnerText;
                    ve.ConferenceCode = conferenceCode;
                    if (itemElement.GetElementsByTagName("leaderPin").Count > 0)
                        leaderPin = itemElement.GetElementsByTagName("leaderPin")[0].InnerText;
                    ve.LeaderPin = leaderPin;
                    //FB 1642 Audio Add On Ends..
                    ve.PublicEndPoint = isPublicEP;//FB 2594

                    //API Port Starts...
                    ve.orgId = organizationID;
                    int apiPort = 23;

                    if (itemElement.GetElementsByTagName("ApiPortno").Count > 0)
                        int.TryParse(itemElement.GetElementsByTagName("ApiPortno")[0].InnerText, out apiPort);
                    if (apiPort <= 0)
                        apiPort = 23;
                    ve.ApiPortNo = apiPort;
                    //API Port Ends...

                    //FB 2400 start

                    int isTelePresence = 0;
                    if (itemElement.GetElementsByTagName("isTelePresence")[0] != null)
                        if (itemElement.GetElementsByTagName("isTelePresence")[0].InnerText.Trim() != "")
                            Int32.TryParse(itemElement.GetElementsByTagName("isTelePresence")[0].InnerText, out isTelePresence);

                    ve.isTelePresence = isTelePresence;
                    ve.MultiCodecAddress = "";


                    multiCodelist = innerNode.SelectNodes("MultiCodec/Address");//FB 2602
                    for (int i = 0; i < multiCodelist.Count; i++)
                    {
                        if (multiCodelist[i] != null)
                        {
                            if (multiCodelist[i].InnerText.Trim() != "")
                            {
                                if (i > 0)
                                    ve.MultiCodecAddress += "�";

                                ve.MultiCodecAddress += multiCodelist[i].InnerText.Trim();
                            }
                        }

                    }

                    if (isTelePresence == 1 && ve.MultiCodecAddress.Trim() == "")
                        ve.MultiCodecAddress = ve.address;

                    ve.RearSecCameraAddress = "";
                    if (itemElement.GetElementsByTagName("RearSecCameraAddress")[0] != null)
                        ve.RearSecCameraAddress = itemElement.GetElementsByTagName("RearSecCameraAddress")[0].InnerText;
                    
                    //FB 2400 end

                    //FB 2501 EM7 Starts
                    int EptOnlinestatus = 1;
                    if (xd.SelectSingleNode("//SetEndpoint/EndpointStatus") != null)
                        Int32.TryParse(xd.SelectSingleNode("//SetEndpoint/EndpointStatus").InnerText, out EptOnlinestatus);
                    ve.EptOnlineStatus = EptOnlinestatus;
                    //FB 2501 EM7 Ends
                    //FB 2616 EM7 Starts
                    int Eptcurrentstatus = 0;
                    if (xd.SelectSingleNode("//SetEndpoint/Eptcurrentstatus") != null)
                        Int32.TryParse(xd.SelectSingleNode("//SetEndpoint/Eptcurrentstatus").InnerText, out Eptcurrentstatus);
                    ve.EptCurrentStatus = Eptcurrentstatus;
                    //FB 2616 EM7 Ends
                    //FB 2595 Start
                    int Secured = 0, securedport = 0;
                    if (itemElement.GetElementsByTagName("Secured")[0] != null)
                        int.TryParse(itemElement.GetElementsByTagName("Secured")[0].InnerText, out Secured);
                    ve.Secured = Secured;

                    if (itemElement.GetElementsByTagName("Securedport")[0] != null)
                        int.TryParse(itemElement.GetElementsByTagName("Securedport")[0].InnerText, out securedport);
                    ve.Secureport = securedport;

                    ve.NetworkURL = "";
                    if (itemElement.GetElementsByTagName("NetworkURL")[0] != null)
                        ve.NetworkURL = itemElement.GetElementsByTagName("NetworkURL")[0].InnerText;

                    //FB 2595 End
                    endPoints.Add(ve);
                }
                if (EntityType.ToUpper() == "U")
                {
                    endPoints[0].profileId = 0; // 0 for user ...
                    m_IeptDao.SaveOrUpdate(endPoints[0]);
                    vrmUser user = m_vrmUserDAO.GetByUserId(Int32.Parse(UserID));
                    //FB 1462 - to handle error if the user is deleted - Start
                    if (user != null)
                    {
                        if (eId == 0)
                            user.endpointId = endPoints[0].endpointid;
                        else
                            user.endpointId = eId;

                        m_vrmUserDAO.SaveOrUpdate(user);
                    }
                    //FB 1462 - to handle error if the user is deleted - End
                }
                else
                {
                    m_IeptDao.SaveOrUpdateList(endPoints);
                }
                if (endPoints.Count < 1)
                    return false;

                obj.outXml = "<SetEndpoint><EndpointID>" + ((vrmEndPoint)endPoints[0]).endpointid.ToString() +
                   "</EndpointID></SetEndpoint>";
                // FB 1699
                if (EntityType.ToUpper() != "U")
                {
                    List<ICriterion> uptcriterion = null;
                    List<vrmConfRoom> rooms = null;
                    vrmConfRoom room = null;
                    DateTime today = DateTime.Now.AddHours(1);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref today);

                    for (Int32 uptcnts = 0; uptcnts < endPoints.Count; uptcnts++)
                    {
                        uptcriterion = new List<ICriterion>();
                        uptcriterion.Add(Expression.Eq("endpointId", endPoints[uptcnts].endpointid));
                        uptcriterion.Add(Expression.Eq("profileId", endPoints[uptcnts].profileId));
                        uptcriterion.Add(Expression.Eq("addressType", endPoints[uptcnts].addresstype));
                        uptcriterion.Add(Expression.Not(Expression.Eq("ipisdnaddress", endPoints[uptcnts].address)));
                        uptcriterion.Add(Expression.Gt("StartDate", today));

                        rooms = m_IconfRoom.GetByCriteria(uptcriterion);

                        if (rooms.Count > 0)
                        {
                            for (Int32 uptrmcnts = 0; uptrmcnts < rooms.Count; uptrmcnts++)
                            {
                                room = rooms[uptrmcnts];
                                room.ipisdnaddress = endPoints[uptcnts].address;
                                m_IconfRoom.Update(room);

                            }

                        }

                    }
                }
                // FB 1699

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region SearchEndpoint
        public bool SearchEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder searchOutXml = new StringBuilder(); // String concatenation changed to StringBuilder for Performance - FB 1820
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int IsPublicEP=0; //FB 2594
                node = xd.SelectSingleNode("//SearchEndpoint/EndpointName");
                string EndpointName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SearchEndpoint/EndpointType");
                string addressType = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SearchEndpoint/PageNo");
                string PageNo = node.InnerXml.Trim();
                
                node = xd.SelectSingleNode("//SearchEndpoint/PublicEndpoint"); //FB 2594
                if(node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out IsPublicEP);

                node = xd.SelectSingleNode("//SearchEndpoint/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                if (EndpointName.Length > 0)
                    criterionList.Add(Expression.Like("name", "%%" + EndpointName.ToLower() + "%%").IgnoreCase());
                if (addressType.Length > 0)
                    criterionList.Add(Expression.Eq("addresstype", Int32.Parse(addressType)));
                criterionList.Add(Expression.Eq("isDefault", 1));
                criterionList.Add(Expression.Eq("deleted", 0));
                if(IsPublicEP == 0) //FB 2594
                    criterionList.Add(Expression.Eq("PublicEndPoint", IsPublicEP));
                criterionList.Add(Expression.Eq("orgId", organizationID));//Organization Module Fixes

                m_IeptDao.addProjection(Projections.RowCount());
                IList list = m_IeptDao.GetObjectByCriteria(criterionList);
                int MaxRecords = 0;
                if (list.Count > 0)
                    MaxRecords = Int32.Parse(list[0].ToString());
                m_IeptDao.clearProjection();

                if (PageNo.Length > 0)
                {
                    m_IeptDao.pageNo(Int32.Parse(PageNo));
                }


                m_IeptDao.addOrderBy(Order.Asc("endpointid"));

                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);
                m_IeptDao.clearOrderBy();
                //FB 1820 - Start
                searchOutXml.Append("<SearchEndpoint>");
                searchOutXml.Append("<Endpoints>");


                foreach (vrmEndPoint ep in epList)
                {
                    searchOutXml.Append("<Endpoint>");
                    searchOutXml.Append("<ID>" + ep.endpointid.ToString() + "</ID>");
                    searchOutXml.Append("<Name>" + ep.name + "</Name>");
                    searchOutXml.Append("</Endpoint>");
                }

                searchOutXml.Append("<Paging>");
                int totalPages = MaxRecords / m_IeptDao.getPageSize();
                if ((MaxRecords % m_IeptDao.getPageSize()) > 0)
                    totalPages++;

                searchOutXml.Append("<TotalPages>" + totalPages.ToString() + "</TotalPages>");
                searchOutXml.Append("<PageNo>" + PageNo + "</PageNo>");
                searchOutXml.Append("<TotalRecords>" + MaxRecords.ToString() + "</TotalRecords>");
                searchOutXml.Append("</Paging>");

                searchOutXml.Append("</Endpoints>");
                searchOutXml.Append("</SearchEndpoint>");

                obj.outXml = searchOutXml.ToString();
                //FB 1820 - End
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetAudioCodecs
        public bool GetAudioCodecs(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetAudioCodecs/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmAudioAlg> al_List = vrmGen.getAudioAlg();

                obj.outXml += "<GetAudioCodecs>";
                foreach (vrmAudioAlg al in al_List)
                {
                    obj.outXml += "<AudioCodec>";
                    obj.outXml += "<ID>" + al.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + al.AudioType + "</Name>";
                    obj.outXml += "</AudioCodec>";
                }

                obj.outXml += "</GetAudioCodecs>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoCodecs
        public bool GetVideoCodecs(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetVideoCodecs/UserID");
                string UserID = node.InnerXml.Trim();

                List<vrmVideoSession> ve_List = vrmGen.getVideoSession();
                obj.outXml += "<GetVideoCodecs>";
                foreach (vrmVideoSession ve in ve_List)
                {
                    obj.outXml += "<VideoCodec>";
                    obj.outXml += "<ID>" + ve.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + ve.VideoSessionType + "</Name> ";
                    obj.outXml += "</VideoCodec>";
                }
                obj.outXml += "</GetVideoCodecs>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoModes
        public bool GetVideoModes(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetVideoModes/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmVideoMode> vm_List = vrmGen.getVideoMode();
                obj.outXml += "<GetVideoModes>";
                foreach (vrmVideoMode vm in vm_List)
                {
                    obj.outXml += "<VideoMode>";
                    obj.outXml += "<ID>" + vm.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + vm.modeName + "</Name> ";
                    obj.outXml += "</VideoMode>";
                }
                obj.outXml += "</GetVideoModes>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMediaTypes
        public bool GetMediaTypes(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetMediaTypes/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmMediaType> mt_List = vrmGen.getMediaTypes();
                obj.outXml += "<GetMediaTypes>";
                foreach (vrmMediaType mt in mt_List)
                {
                    obj.outXml += "<Type>";
                    obj.outXml += "<ID>" + mt.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + mt.mediaType + "</Name> ";
                    obj.outXml += "</Type>";
                }
                obj.outXml += "</GetMediaTypes>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoProtocols
        public bool GetVideoProtocols(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetVideoProtocols/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmVideoProtocol> vp_List = vrmGen.getVideoProtocols();
                obj.outXml += "<GetVideoProtocols>";
                foreach (vrmVideoProtocol vp in vp_List)
                {
                    obj.outXml += "<Protocol>";
                    obj.outXml += "<ID>" + vp.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + vp.VideoProtocolType + "</Name> ";
                    obj.outXml += "</Protocol>";
                }
                obj.outXml += "</GetVideoProtocols>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMCUCards
        public bool GetMCUCards(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                obj.outXml = "<GetMCUCards>";
                foreach (vrmMCUCardDetail cd in vrmGen.getMCUCardDetail())
                {
                    obj.outXml += "<MCUCard>";
                    obj.outXml += "<ID>" + cd.id.ToString() + "</ID> ";
                    obj.outXml += "<Name>" + cd.CardName + "</Name>";
                    obj.outXml += "</MCUCard>";
                }
                obj.outXml += "</GetMCUCards>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMCUAvailableResources
        public bool GetMCUAvailableResources(ref vrmDataObject obj)
        {
            bool bRet = true;
            int audioPorts = 0;//Fb 1937
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetMCUAvailableResources/UserID");
                string UserID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetMCUAvailableResources/BridgeID");
                string BridgeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetMCUAvailableResources/ConfID");
                string ConfID = node.InnerXml.Trim();
                DateTime confDate;
                int iDuration;
                node = xd.SelectSingleNode("//GetMCUAvailableResources/StartDate");
                string StartDate = node.InnerXml.Trim();
                CConfID Conf = new CConfID(ConfID);

                //this bypasses the date time for now (confid is NEVER 0. 
                if (ConfID.Length == 0)
                {
                    node = xd.SelectSingleNode("//GetMCUAvailableResources/StartTime");
                    string StartTime = node.InnerXml.Trim();
                    confDate = DateTime.Parse(StartDate + " " + StartTime);
                    node = xd.SelectSingleNode("//GetMCUAvailableResources/Duration");
                    string Duration = node.InnerXml.Trim();
                    iDuration = Int32.Parse(Duration);

                    vrmUser myVrmUser = m_vrmUserDAO.GetByUserId(Int32.Parse(UserID));

                    if (myVrmUser != null)   //FB 1462 - related fix
                        timeZone.changeToGMTTime(myVrmUser.TimeZone, ref confDate);
                }
                else
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", Conf.ID));
                    if (Conf.instance == 0)
                        Conf.instance = 1;
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                    List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList);

                    vrmConference conf = confList[0];

                    confDate = conf.confdate;
                    iDuration = conf.duration;
                }

                vrmMCUResources vMCU = new vrmMCUResources(Int32.Parse(BridgeID));
                vrmMCU MCU = m_ImcuDao.GetById(Int32.Parse(BridgeID));

                vMCU.bridgeType = MCU.MCUType.bridgeInterfaceId;

                List<vrmMCUResources> vMCUList = new List<vrmMCUResources>();
                loadVRMResourceData(MCU, ref vMCU);
                vMCUList.Add(vMCU);
                if (GetCompleteMCUusage(confDate, iDuration, Conf.ID, Conf.instance, ref vMCUList))// FB 1937
                {
                    vrmMCU MCUData = MCU;
                    string CardsAvailable, Description;
                    Description = string.Empty;
                    CardsAvailable = string.Empty;
                    //Conditions Changed for FB 2472 Start
                    if (vMCU.bridgeType == MCUType.MSE8000) //FB 2008
                    {
                        MCUData.MaxConcurrentAudioCalls = vMCU.AUDIO.calls;
                        MCUData.MaxConcurrentVideoCalls = vMCU.VIDEO.calls;
                        CardsAvailable = "Yes";

                        if (vMCU.AUDIO.available < 0) //FB 1837 - changed <= to <
                        {
                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Audio";

                        }

                        if (vMCU.VIDEO.available < 0) //FB 1837 - changed <= to <
                        {
                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Video";
                        }


                        if (Description.Length > 0)
                            Description = "Insufficient " + Description + " ports.";

                        audioPorts = vMCU.AUDIO.available;

                    }
                    else if (vMCU.bridgeType == MCUType.CODIAN) 
                    {
                        MCUData.MaxConcurrentAudioCalls = vMCU.AUDIO.calls;
                        MCUData.MaxConcurrentVideoCalls = vMCU.VIDEO.calls;
                        CardsAvailable = "Yes";

                        if (vMCU.VIDEO.available < 0) 
                        {
                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Audio/Video";
                        }


                        if (Description.Length > 0)
                            Description = "Insufficient " + Description + " ports.";

                        audioPorts = vMCU.VIDEO.available;

                    }
                    else // all other bridges(polycom/Tanberg/radvision)
                    {
                        //FB 1937
                        MCUData.MaxConcurrentAudioCalls = vMCU.AUDIO.calls;
                        MCUData.MaxConcurrentVideoCalls = vMCU.VIDEO.calls;
                        CardsAvailable = "Yes";
                        if (vMCU.VIDEO.available < 0)//FB 1837 - changed <= to <
                        {

                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Audio/Video"; //FB 1937
                        }

                        if (Description.Length > 0)
                            Description = "Insufficient " + Description + " ports.";

                        audioPorts = -1000;//FB 1937
                        // 1134 AG 01/27/09

                    }
                    //FB 2472 End

                    obj.outXml = "<MCUResources>";
                    obj.outXml += "<AudioVideo>";
                    obj.outXml += "<Available>" + vMCU.VIDEO.available.ToString() + "</Available>";
                    obj.outXml += "<Total>" + MCUData.MaxConcurrentVideoCalls.ToString() + "</Total>";
                    obj.outXml += "</AudioVideo>";
                    obj.outXml += "<AudioOnly>";
                    obj.outXml += "<Available>" + audioPorts.ToString() + "</Available>";//FB 1937
                    obj.outXml += "<Total>" + MCUData.MaxConcurrentAudioCalls.ToString() + "</Total>";
                    obj.outXml += "</AudioOnly>";
                    obj.outXml += "<CardsAvailable>" + CardsAvailable + "</CardsAvailable>";
                    obj.outXml += "<Description>" + Description + "</Description>";
                    obj.outXml += "</MCUResources>";
                }
                else
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("ERROR cannot calculate MCU Usage");
                    myVRMException myVRMEx = new myVRMException(435);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881 // myVRMException.toXml(e.Message);
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMCUusage
        public bool GetMCUusage(DateTime confDate, int Duration, int confId, int instanceId,
          ref List<vrmMCUResources> vMCUList)
        {
            return GetMCUusage(confDate, Duration, confId, instanceId, ref vMCUList); //FB 1937
        }
        #endregion

        #region GetMCUusage
        public bool GetMCUusage(DateTime confDate, int Duration, int confId, int instanceId,
            ref List<vrmMCUResources> vMCUList, bool bSkipCheck)
        {
            // find this nubmer anywhere (user/room) that this conference is scheduled and it is not to be used.
            try
            {
                string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

                string hql;

                // select all converences that fall into this date time range (with correct status and are not rooms or templates
                // 
                hql = "SELECT c.confid, c.instanceid FROM myVRM.DataLayer.vrmConference c ";
                hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= c.confdate ";
                hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                hql += " <= (dateadd(minute, c.duration, c.confdate ))";
                hql += "AND c.deleted = 0 AND c.status IN( " + status + ")";
                hql += "AND c.conftype != " + vrmConfType.RooomOnly;
                hql += "AND c.conftype != " + vrmConfType.Template;


                IList conferences = m_vrmConfDAO.execQuery(hql);
                // step thru each conference and count the ports used. 
                List<ICriterion> criterionList;
                foreach (object[] obj in conferences)
                {
                    int sConfid = Int32.Parse(obj[0].ToString());
                    int sInstcanceid = Int32.Parse(obj[1].ToString());

                    vrmConfAdvAvParams advAVParams = new vrmConfAdvAvParams();
                    for (int i = 0; i < vMCUList.Count; i++)
                    {
                        vrmMCUResources vMCU = vMCUList[i];
                        vrmConference baseConf = m_vrmConfDAO.GetByConfId(sConfid, sInstcanceid);
                        vrmConfAdvAvParams AvParams = baseConf.ConfAdvAvParams;
                        int isSwitched = 0;
                        int isEncryption = 0;

                        if (AvParams.videoMode == 1 && isSwitched != 1)
                            isSwitched = 1;
                        if (AvParams.encryption == 1 && isEncryption != 1)
                            isEncryption = 1;

                        // first lookup confrooms
                        // now the users (only external)
                        // FB 4460 use the vMCU (virtual bridge alorithm for Polycomm mcu's
                        criterionList = new List<ICriterion>();
                        if (confId == 0)
                        {
                            criterionList.Add(Expression.Eq("confid", sConfid));
                            criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                            criterionList.Add(Expression.Eq("bridgeid", vMCU.bridgeId));
                        }
                        else
                        {

                            criterionList.Add(Expression.Eq("confid", sConfid));
                            criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                            if (confId != sConfid || (confId == sConfid && instanceId != sInstcanceid))
                                criterionList.Add(Expression.Eq("bridgeid", vMCU.bridgeId));
                        }
                        List<vrmConfRoom> room = m_IconfRoom.GetByCriteria(criterionList);

                        foreach (vrmConfRoom rm in room)
                        {
                            bool notBypass = true;
                            if ((bSkipCheck && (sConfid == confId && sInstcanceid == instanceId)) ||
                                 (!bSkipCheck && (sConfid == confId &&
                                        sInstcanceid == instanceId && rm.bridgeid == vMCU.bridgeId)))

                                // exclude current conference
                                if (notBypass)
                                {

                                    switch (vMCU.bridgeType)
                                    {
                                        case MCUType.POLYCOM:
                                            vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                                   isEncryption, vMCU);
                                            break;
                                        default:
                                            if (rm.audioorvideo == 1) //FB 1744
                                                vMCU.AUDIO.used++;
                                            else
                                                vMCU.VIDEO.used++;
                                            break;
                                    }
                                }
                        }

                        // only external users (as they are using port)
                        criterionList.Add(Expression.Eq("invitee", vrmConfUserType.External));
                        criterionList.Add(Expression.Not(Expression.Eq("status", vrmConfUserStatus.Rejectetd)));
                        List<vrmConfUser> user = m_IconfUser.GetByCriteria(criterionList);
                        foreach (vrmConfUser us in user)
                        {
                            // if we are checking mcu resource back out the resources from the conference bridge.
                            // if we are settup up a conference back out the requested resources as they 
                            //     (irespecitve of bridge) will be added in later. 
                            bool notBypass = true;
                            if ((bSkipCheck && (sConfid == confId && sInstcanceid == instanceId)) ||
                                (!bSkipCheck && (sConfid == confId &&
                                       sInstcanceid == instanceId && us.bridgeid == vMCU.bridgeId)))
                                notBypass = false;

                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    case MCUType.POLYCOM:
                                        vMCUResourceLoad(us.addressType, us.audioOrVideo, isSwitched,
                                               isEncryption, vMCU);
                                        break;
                                    default:
                                        if (us.audioOrVideo == 1) //FB 1744
                                            vMCU.AUDIO.used++;
                                        else
                                            vMCU.VIDEO.used++;
                                        break;

                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        // FB 1937
        #region GetCompleteMCUusage
        public bool GetCompleteMCUusage(DateTime confDate, int Duration, int confId, int instanceId,
            ref List<vrmMCUResources> vMCUList)
        {
            // find this nubmer anywhere (user/room) that this conference is scheduled and it is not to be used.
            try
            {
                string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

                string hql;

                // select all conferences that fall into this date time range (with correct status and are not rooms or templates
                // 
                hql = "SELECT c.confid, c.instanceid FROM myVRM.DataLayer.vrmConference c ";
                hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= c.confdate ";
                hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                hql += " <= (dateadd(minute, c.duration, c.confdate ))";
                hql += "AND c.deleted = 0 AND c.status IN( " + status + ")";
                hql += "AND c.conftype != " + vrmConfType.RooomOnly;
                hql += "AND c.conftype != " + vrmConfType.Template;


                IList conferences = m_vrmConfDAO.execQuery(hql);
                // step thru each conference and count the ports used. 
                List<ICriterion> criterionList;
                foreach (object[] obj in conferences)
                {
                    int sConfid = Int32.Parse(obj[0].ToString());
                    int sInstcanceid = Int32.Parse(obj[1].ToString());

                    vrmConfAdvAvParams advAVParams = new vrmConfAdvAvParams();
                    for (int i = 0; i < vMCUList.Count; i++)
                    {
                        vrmMCUResources vMCU = vMCUList[i];
                        vrmConference baseConf = m_vrmConfDAO.GetByConfId(sConfid, sInstcanceid);
                        vrmConfAdvAvParams AvParams = baseConf.ConfAdvAvParams;
                        int isSwitched = 0;
                        int isEncryption = 0;

                        if (AvParams.videoMode == 1)
                            isSwitched = 1;
                        if (AvParams.encryption == 1)
                            isEncryption = 1;

                        // first lookup confrooms
                        // now the users (only external)
                        // FB 4460 use the vMCU (virtual bridge alorithm for Polycomm mcu's
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", sConfid));
                        criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                        criterionList.Add(Expression.Eq("bridgeid", vMCU.bridgeId));
                        List<vrmConfRoom> room = m_IconfRoom.GetByCriteria(criterionList);

                        foreach (vrmConfRoom rm in room)
                        {
                            bool notBypass = true;


                            // exclude current conference
                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    //FB 1937 & 2008
                                    //case MCUType.POLYCOM:
                                    //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                    //           isEncryption, vMCU);
                                    //    break;
                                    case MCUType.MSE8000://FB 1937 & 2008
                                    case MCUType.CODIAN:
                                        //Commented for FB 2472
                                        //if (rm.audioorvideo == 1 && vMCU.AUDIO.available > 0) //FB 1744  
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        //{
                                            vMCU.VIDEO.used++;
                                            //vMCU.AUDIO.used++; //FB 1937
                                        //}
                                        break;
                                    default:
                                        //if (vMCU.AUDIO.available > 0) //FB 1744 //FB 1937
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        vMCU.VIDEO.used++;
                                        vMCU.AUDIO.used = -1000;
                                        break;
                                }
                            }
                        }

                        // only external users (as they are using port)
                        criterionList.Add(Expression.Eq("invitee", vrmConfUserType.External));
                        criterionList.Add(Expression.Not(Expression.Eq("status", vrmConfUserStatus.Rejectetd)));
                        List<vrmConfUser> user = m_IconfUser.GetByCriteria(criterionList);
                        foreach (vrmConfUser us in user)
                        {
                            // if we are checking mcu resource back out the resources from the conference bridge.
                            // if we are settup up a conference back out the requested resources as they 
                            //     (irespecitve of bridge) will be added in later. 
                            bool notBypass = true;

                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    //FB 1937 & 2008
                                    //case MCUType.POLYCOM:
                                    //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                    //           isEncryption, vMCU);
                                    //    break;
                                    case MCUType.MSE8000://FB 1937 & 2008
                                    case MCUType.CODIAN:
                                        //Commented for FB 2472
                                        //if (us.audioOrVideo == 1 && vMCU.AUDIO.available >= 0) //FB 1744  FB 2472
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        //{
                                            vMCU.VIDEO.used++;
                                            //vMCU.AUDIO.used++; //FB 1937
                                        //}
                                        break;
                                    default:
                                        //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        vMCU.VIDEO.used++;
                                        vMCU.AUDIO.used = -1000;
                                        break;
                                }
                            }
                        }

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", sConfid));
                        criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                        criterionList.Add(Expression.Eq("bridgeId", vMCU.bridgeId));
                        List<vrmConfCascade> cascade = m_IconfCascade.GetByCriteria(criterionList);

                        foreach (vrmConfCascade csde in cascade)
                        {
                            bool notBypass = true;


                            // exclude current conference
                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    //FB 1937 & 2008
                                    //case MCUType.POLYCOM:
                                    //    vMCUResourceLoad(csde.addresstype, csde.audioOrVideo, isSwitched,
                                    //           isEncryption, vMCU);
                                    //    break;
                                    case MCUType.MSE8000://FB 1937 & 2008
                                    case MCUType.CODIAN:
                                        //Commented for FB 2472
                                        //if (csde.audioOrVideo == 1 && vMCU.AUDIO.available >= 0) //FB 2472
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        //{
                                            vMCU.VIDEO.used++;
                                            //vMCU.AUDIO.used++; //FB 1937
                                        //}
                                        break;
                                    default:
                                        //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        vMCU.VIDEO.used++;
                                        vMCU.AUDIO.used = -1000;
                                        break;
                                }
                            }
                        }

                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region vMCUResourceLoad
        public bool vMCUResourceLoad(int addressType, int isVideo, int isSwitched, int isEncryted, vrmMCUResources vMCU)
        {
            return vMCUResourceLoad(addressType, isVideo, isSwitched, isEncryted, vMCU, false);

        }
        #endregion

        #region vMCUResourceLoad
        public bool vMCUResourceLoad(int addressType, int isVideo, int isSwitched, int isEncryted,
            vrmMCUResources vMCU, bool check)
        {
            try
            {
                int port = 1;
                if (isEncryted == 1)
                    port = 2;
                switch (addressType)
                {
                    case eptDetails.typeISDN:
                        // ISDN/Audio
                        if (isVideo == 0)
                        {
                            if (check)
                                vMCU.needAUDIO = true;
                            vMCU.AUDIO.used += port;
                            if (check)
                                vMCU.needISDN = true;
                            vMCU.ISDN.used += port;
                        }
                        else
                        {
                            // ISDN/Video switched
                            if (isSwitched == 1)
                            {
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needMUX = true;
                                vMCU.MUX.used += port;
                                if (check)
                                    vMCU.needISDN = true;
                                vMCU.ISDN.used += port;
                            }
                            // ISDN/Video non-switched
                            else
                            {
                                if (check)
                                    vMCU.needMUX = true;
                                vMCU.MUX.used += port;
                                if (check)
                                    vMCU.needVIDEO = true;
                                vMCU.VIDEO.used += port;
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needISDN = true;
                                vMCU.ISDN.used += port;
                            }
                        }
                        break;
                    case eptDetails.typeMPI:
                        if (check)
                            vMCU.needAUDIO = true;
                        vMCU.AUDIO.used += port;
                        if (check)
                            vMCU.needMUX = true;
                        vMCU.MUX.used += port;
                        if (check)
                            vMCU.needMPI = true;
                        vMCU.MPI.used += port;
                        break;

                    default:
                        // IP is default address type
                        // IP/Audio
                        if (isVideo == 0)
                        {
                            if (check)
                                vMCU.needAUDIO = true;
                            vMCU.AUDIO.used += port;
                            if (check)
                                vMCU.needVIDEO = true;
                            vMCU.VIDEO.used += port;
                        }
                        else
                        {
                            // IP/Video switched
                            if (isSwitched == 1)
                            {
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needIP = true;
                                vMCU.IP.used += port;
                            }
                            // IP/Video non-switched
                            else
                            {
                                if (check)
                                    vMCU.needVIDEO = true;
                                vMCU.VIDEO.used += port;
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needIP = true;
                                vMCU.IP.used += port;
                            }
                        }
                        break;


                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region loadVRMResourceData
        public bool loadVRMResourceData(vrmMCU MCU, ref vrmMCUResources vMCU)
        {
            try
            {
                //1036 auto detect MCU add support for Codian and Tanberg
                vMCU.bridgeType = MCU.MCUType.bridgeInterfaceId;
                vMCU.bridgeId = MCU.BridgeID;
                vMCU.bridgeName = MCU.BridgeName;
                vMCU.adminId = MCU.Admin;

                switch (MCU.MCUType.bridgeInterfaceId)
                {
                    case MCUType.RADVISION://Added for radvision elite - 1998
                    case MCUType.POLYCOM:
                        //Code Commented for FB 1937
                        //foreach (vrmMCUCardList cl in MCU.MCUCardList)
                        //{
                        //    if (cl.MCUCardDetail.CardName == vMCU.HDLC.tag)
                        //        vMCU.HDLC.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.ISDN.tag)
                        //        vMCU.ISDN.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.ATM.tag)
                        //        vMCU.ATM.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.IP.tag)
                        //        vMCU.IP.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName.Contains(vMCU.MUX.tag))
                        //        vMCU.MUX.calls += cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName.Contains(vMCU.AUDIO.tag))
                        //        vMCU.AUDIO.calls += cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName.Contains(vMCU.VIDEO.tag))
                        //        vMCU.VIDEO.calls += cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.DATA.tag)
                        //        vMCU.DATA.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.ISDN_8.tag)
                        //        vMCU.ISDN_8.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.MPI.tag)
                        //        vMCU.MPI.calls = cl.maxCalls;
                        //}
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    // (FB 1036) use max concurrent for Tanberg/Codian 
                    case MCUType.TANDBERG:
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.CODIAN:
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.MSE8000://Code added for MSE 8000
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.LIFESIZE://Fb 2261
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.CISCO: //FB 2501 Call Monitoring
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.iVIEW://Fb 2556
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    // default error
                    default:
                        Exception e = new Exception("Invalid MCU Type. Cannot allocate resources");
                        throw e;

                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetBridgeInformation
        //
        // GetBridgeList and GetBridge commands have been consolidated. 
        // true == GetBridgeList command
        //      
        public bool GetBridgeInformation(ref vrmDataObject obj, bool extendInfo)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                obj.outXml = "<bridgeInfo>";
                obj.outXml += FetchBridgeTypeList();
                obj.outXml += FetchBridgeStatusList();

                int bridgeCount = 0;
                obj.outXml += FetchBridgeList(ref bridgeCount);

                if (extendInfo)
                {
                    OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    int Remaining = orgdt.MCULimit - bridgeCount;
                    obj.outXml += "<totalNumber>" + bridgeCount.ToString() + "</totalNumber>";
                    obj.outXml += "<licensesRemain>" + Remaining.ToString() + "</licensesRemain>";
                }
                obj.outXml += "</bridgeInfo>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetNewBridge
        public bool GetNewBridge(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string UserID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes

                //FB 2486 - Start
                List <ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("EnhancedMCU", 1)); //FB 2636
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmMCU> chkMcu = m_ImcuDao.GetByCriteria(criterionList);
                //FB 2486 - End
               
                obj.outXml = "<bridge>";
                obj.outXml += "<timeZone>" + sysSettings.TimeZone.ToString() + "</timeZone>";
                StringBuilder outXml = new StringBuilder();

                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);    //Organization Module
                m_OrgFactory.organizationID = organizationID;
                obj.outXml += "<EnhancedMCUCnt>" + chkMcu.Count + "</EnhancedMCUCnt>";//FB 2486 //FB 2636
                m_OrgFactory.timeZonesToXML(ref outXml);
                obj.outXml += outXml;
                obj.outXml += FetchBridgeTypeList();
                obj.outXml += FetchBridgeStatusList();
                obj.outXml += FetchAddressTypeList();
                obj.outXml += "</bridge>";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetOldBridge
        public bool GetOldBridge(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string UserID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                //FB 2486 - Start
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("EnhancedMCU", 1)); // FB 2636
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmMCU> chkMcu = m_ImcuDao.GetByCriteria(criterionList);
                //FB 2486 - End

                node = xd.SelectSingleNode("//login/bridgeID");
                string bridgeID = node.InnerXml.Trim();

                obj.outXml = "<bridge>";

                vrmMCU mcu = m_ImcuDao.GetById(Int32.Parse(bridgeID));

                cryptography.Crypto crypto = new cryptography.Crypto();

                obj.outXml += "<bridgeID>" + mcu.BridgeID.ToString() + "</bridgeID>";

                if (organizationID != 11 && mcu.isPublic != 0) // FB 1920
                    mcu.BridgeName = mcu.BridgeName.ToString() + "(*)";

                obj.outXml += "<name>" + mcu.BridgeName + "</name>";
                obj.outXml += "<login>" + mcu.BridgeLogin + "</login>";
                //obj.outXml += "<password>" + crypto.decrypt(mcu.BridgePassword) + "</password>"; //FB 3054
                obj.outXml += "<password>" + mcu.BridgePassword + "</password>";
                obj.outXml += "<bridgeType>" + mcu.MCUType.id.ToString() + "</bridgeType>";
                obj.outXml += "<bridgeStatus>" + mcu.Status.ToString() + "</bridgeStatus>";
                obj.outXml += "<URLAccess>" + mcu.URLAccess.ToString() + "</URLAccess>"; //ZD 100113
                obj.outXml += "<virtualBridge>" + mcu.VirtualBridge.ToString() + "</virtualBridge>";
                obj.outXml += "<malfunctionAlert>" + mcu.malfunctionalert.ToString() + "</malfunctionAlert>";
                obj.outXml += "<timeZone>" + mcu.timezone.ToString() + "</timeZone>";
                obj.outXml += "<firmwareVersion>" + mcu.SoftwareVer + "</firmwareVersion>";
                obj.outXml += "<percentReservedPort>" + mcu.reservedportperc + "</percentReservedPort>";
                obj.outXml += "<ApiPortNo>" + mcu.ApiPortNo.ToString() + "</ApiPortNo>";//API Port...
                obj.outXml += "<isPublic>" + mcu.isPublic.ToString() + "</isPublic>"; // FB 1920
                obj.outXml += "<setFavourite>" + mcu.setFavourite.ToString() + "</setFavourite>"; // FB 2501 CallMonitor Favourite
                obj.outXml += "<enableCDR>" + mcu.EnableCDR.ToString() + "</enableCDR>"; // FB 2660
                obj.outXml += "<deleteCDRDays>" + mcu.DeleteCDRDays.ToString() + "</deleteCDRDays>"; // FB 2660
                obj.outXml += "<EnableMessage>" + mcu.EnableMessage + "</EnableMessage>";//FB 2486
                obj.outXml += "<EnhancedMCUCnt>" + chkMcu.Count + "</EnhancedMCUCnt>"; //FB 2486 //FB 2636
                ////FB 1642 - DTMF Start Commented for FB 2655
                //obj.outXml += " <DTMF>";
                //obj.outXml += "     <PreConfCode>" + mcu.PreConfCode + "</PreConfCode>";
                //obj.outXml += "     <PreLeaderPin>" + mcu.PreLeaderPin + "</PreLeaderPin>";
                //obj.outXml += "     <PostLeaderPin>" + mcu.PostLeaderPin + "</PostLeaderPin>";
                //obj.outXml += "</DTMF>";
                ////FB 1642 - DTMF End

                obj.outXml += "<bridgeAdmin>";
                vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);

                obj.outXml += "<ID>" + mcu.Admin + "</ID>";
                /**** Code Modified For FB 1462 - Start ****/
                if (user != null)
                {
                    obj.outXml += "<firstName>" + user.FirstName + "</firstName>";
                    obj.outXml += "<lastName>" + user.LastName + "</lastName>";
                    obj.outXml += "<userstate>A</userstate>";    /*A- User Active */
                }
                else
                {
                    vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                    if (iuser != null)
                    {
                        obj.outXml += "<firstName>" + iuser.FirstName + "</firstName>";
                        obj.outXml += "<lastName>" + iuser.LastName + "</lastName>";
                        obj.outXml += "<userstate>I</userstate>";    /* I- User in-Active */
                    }
                    else
                    {
                        obj.outXml += "<firstName></firstName>";
                        obj.outXml += "<lastName></lastName>";
                        obj.outXml += "<userstate>D</userstate>";    /*D- User Deleted */
                    }
                }
                /**** Code Modified For FB 1462 - End ****/

                obj.outXml += "<maxAudioCalls>" + mcu.MaxConcurrentAudioCalls.ToString() + "</maxAudioCalls>";
                obj.outXml += "<maxVideoCalls>" + mcu.MaxConcurrentVideoCalls.ToString() + "</maxVideoCalls>";
                obj.outXml += "</bridgeAdmin>";

                StringBuilder tzOutXml = new StringBuilder();
                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);    //Organization Module
                m_OrgFactory.organizationID = organizationID;
                m_OrgFactory.timeZonesToXML(ref tzOutXml);
                obj.outXml += tzOutXml;

                // All lists
                obj.outXml += "<approvers>";
                obj.outXml += "<responseMsg>";
                obj.outXml += mcu.responsemessage;
                obj.outXml += "</responseMsg>";
                obj.outXml += "<responseTime>";
                obj.outXml += mcu.responsetime.ToString("g");
                obj.outXml += "</responseTime>";
                foreach (vrmMCUApprover approver in mcu.MCUApprover)
                {
                    obj.outXml += "<approver>";
                    obj.outXml += "<ID>" + approver.approverid.ToString() + "</ID>";

                    vrmUser appUser = m_vrmUserDAO.GetByUserId(approver.approverid);
                    if (appUser != null)    //FB 1462 - Related issue fix
                    {
                        obj.outXml += "<firstName>" + appUser.FirstName + "</firstName>";
                        obj.outXml += "<lastName>" + appUser.LastName + "</lastName>";
                        obj.outXml += "<userstate>A</userstate>";
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            obj.outXml += "<firstName>" + appUser.FirstName + "</firstName>";
                            obj.outXml += "<lastName>" + appUser.LastName + "</lastName>";
                            obj.outXml += "<userstate>I</userstate>";
                        }
                        else
                        {
                            obj.outXml += "<firstName></firstName>";
                            obj.outXml += "<lastName></lastName>";
                            obj.outXml += "<userstate>D</userstate>";
                        }
                    }
                    obj.outXml += "</approver>";
                }

                obj.outXml += "</approvers>";

                obj.outXml += "<timezones>" + sysSettings.TimeZone.ToString() + "</timezones>";
                obj.outXml += FetchBridgeDetails(mcu);
                obj.outXml += FetchBridgeTypeList();
                obj.outXml += FetchBridgeStatusList();
                obj.outXml += FetchAddressTypeList();

                obj.outXml += "<ISDNThresholdAlert>" + mcu.isdnthresholdalert.ToString() + "</ISDNThresholdAlert>";
                if (mcu.isdnthresholdalert == 1)
                {
                    obj.outXml += "<ISDNPortCharge>" + mcu.isdnportrate + "</ISDNPortCharge>";
                    obj.outXml += "<ISDNLineCharge>" + mcu.isdnlinerate.ToString() + "</ISDNLineCharge>";
                    obj.outXml += "<ISDNMaxCost>" + mcu.isdnmaxcost.ToString() + "</ISDNMaxCost>";
                    obj.outXml += "<ISDNThreshold>" + mcu.isdnthreshold.ToString() + "</ISDNThreshold>";

                    if (organizationID < 11)    //Organization Module
                        organizationID = defaultOrgId;

                    if (orgInfo == null)
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    string isdntimeframe = "";
                    if (orgInfo != null)
                        isdntimeframe = orgInfo.ISDNTimeFrame.ToString();    //Organization Module

                    if (isdntimeframe == "1" || isdntimeframe == "3")
                        obj.outXml += "<ISDNThresholdTimeframe>" + isdntimeframe + "</ISDNThresholdTimeframe>";
                    else
                        obj.outXml += "<ISDNThresholdTimeframe></ISDNThresholdTimeframe>";
                }

                obj.outXml += "<MCUCards>";
                foreach (vrmMCUCardList cl in mcu.MCUCardList)
                {
                    obj.outXml += "<MCUCard>";
                    obj.outXml += "<ID>" + cl.MCUCardDetail.id.ToString() + "</ID> ";
                    obj.outXml += "<MaximumCalls>" + cl.maxCalls.ToString() + "</MaximumCalls>";
                    obj.outXml += "</MCUCard>";
                }
                obj.outXml += "</MCUCards>";

                obj.outXml += "</bridge>";
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;

        }
        #endregion

        #region FetchBridgeDetails

        private string FetchBridgeDetails(vrmMCU mcu)
        {
            cryptography.Crypto crypto = new cryptography.Crypto(); // FB 2441
            string outputXML = string.Empty;
            List<vrmMCUOrgSpecificDetails> orgSpecificdetails = null;//FB 2556
            IMCUOrgSpecificDetailsDao m_IMCUOrgSpecificDetailsDao = null;
            List<ICriterion> osCriterionList = null;
            int cntDtls = 0;
            List<vrmExtMCUSilo> MCUSilo = null;
            List<vrmExtMCUService> MCUService = null;
            int p = 0;
            try
            {
                List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                int i = -1;
                foreach (vrmMCUVendor vendor in vendorList)
                {
                    i++;
                    if (vendor.id == mcu.MCUType.id)
                        break;
                }
                if (i < 0)
                {
                    //FB 1881 start
                    myVRMException myVRMEx = new myVRMException(436);
                    throw myVRMEx;
                    //FB 1881 end
                }
                #region POLYCOM
                if (vendorList[i].BridgeInterfaceId == MCUType.POLYCOM) // Polycom Accord
                {
                    outputXML = "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ConferenceServiceID>" + mcu.ConfServiceID + "</ConferenceServiceID>"; //FB 2427
                    
                    //FB 2591 start
                    outputXML += "<Profiles>";
                    //outputXML += "<Id>" + MCUProf[p].ProfileId + "</Id>";
                    List<ICriterion> criterionListmp = new List<ICriterion>();
                    criterionListmp.Add(Expression.Eq("MCUId", mcu.BridgeID));
                    List<vrmMCUProfiles> MCUProf = m_IMcuProfilesDAO.GetByCriteria(criterionListmp);
                    for ( p = 0; p < MCUProf.Count; p++)
                    {
                        outputXML += "<Profile>";
                        outputXML += "<ID>" + MCUProf[p].ProfileId + "</ID>";
                        outputXML += "<name>" + MCUProf[p].ProfileName + "</name>";
                        outputXML += "</Profile>";
                    }
                    
                    outputXML += "</Profiles>";

                    //FB 2591 end

                    outputXML += "<EnableIVR>" + mcu.EnableIVR + "</EnableIVR>";//FB 1766
                    outputXML += "<IVRServiceName>" + mcu.IVRServiceName + "</IVRServiceName>";//FB 1766
                    outputXML += "<enableRecord>" + mcu.EnableRecording + "</enableRecord>";//FB 1907
                    outputXML += "<enableLPR>" + mcu.LPR + "</enableLPR>";//FB 1907
                    outputXML += "<EnableMessage>" + mcu.EnableMessage + "</EnableMessage>";//FB 2486

                    //FB 2441 RPRM Starts
                    outputXML += "<PolycomRPRM>";
                    outputXML += "<RPRMDomain>" + mcu.RPRMDomain + "</RPRMDomain>"; // FB 2441 II
                    outputXML += "<Synchronous>" + mcu.Synchronous + "</Synchronous>";
                    outputXML += "<Sendmail>" + mcu.DMASendMail + "</Sendmail>";
                    outputXML += "<MonitorMCU>" + mcu.DMAMonitorMCU + "</MonitorMCU>";
                    outputXML +=  "<DMAName>" + mcu.DMAName + "</DMAName>";
                    outputXML +=  "<DMALogin>" + mcu.DMALogin + "</DMALogin>";
                    if (mcu.DMAPassword != null && mcu.DMAPassword != "")
                        //outputXML += "<DMAPassword>" + crypto.decrypt(mcu.DMAPassword) + "</DMAPassword>"; //FB 3054
                        outputXML += "<DMAPassword>" + mcu.DMAPassword + "</DMAPassword>";
                    else
                        outputXML += "<DMAPassword></DMAPassword>";

                    outputXML +=  "<DMAURL>" + mcu.DMAURL + "</DMAURL>";
                    outputXML +=  "<DMAPort>" + mcu.DMAPort + "</DMAPort>";
					outputXML+=   "<DMADomain>"+mcu.DMADomain+"</DMADomain>"; // FB 2441 II
                    outputXML +=  "<DMADialinPrefix>"+mcu.DialinPrefix+"</DMADialinPrefix>"; //FB 2689
                    //FB 2709
                    outputXML += "<LoginAccount>" + mcu.LoginName + "</LoginAccount>";
                    outputXML += "<LoginCount>" + mcu.loginCount + "</LoginCount>";
                    outputXML += "<RPRMEmail>" + mcu.RPRMEmailaddress + "</RPRMEmail>";
                    //FB 2709
                    outputXML +=  "</PolycomRPRM>";
                    //FB 2441 RPRM Ends
                    vrmMCUIPServices ip = null; //FB 2636
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipServices = m_IipServices.GetByCriteria(criterionList);
                    outputXML += "<IPServices>";
                    for (int k = 0; k < ipServices.Count; k++)
                    {
                        ip = ipServices[k];
                        outputXML += "<IPService>";
                        outputXML += "<SortID>" + ip.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + ip.ServiceName + "</name>";
                        outputXML += "<addressType>" + ip.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + ip.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + ip.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + ip.usage.ToString() + "</usage>";
                        outputXML += "</IPService>";
                    }
                    outputXML += "</IPServices>";
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    vrmMCUMPIServices mpi = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUMPIServices> mpiServices = m_ImpiService.GetByCriteria(criterionList);
                    outputXML += "<MPIServices>";
                    for (int k = 0; k < mpiServices.Count; k++)
                    {
                        mpi = mpiServices[k];
                        outputXML += "<MPIService>";
                        outputXML += "<SortID>" + mpi.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + mpi.ServiceName + "</name>";
                        outputXML += "<addressType>" + mpi.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + mpi.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + mpi.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + mpi.usage.ToString() + "</usage>";
                        outputXML += "</MPIService>";
                    }
                    outputXML += "</MPIServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region CODIAN
                else if (vendorList[i].BridgeInterfaceId == MCUType.CODIAN) // Codian bridge
                {
                    // BridgeIPAddress table contains the two ports
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipService = m_IipServices.GetByCriteria(criterionList);

                    outputXML = "<bridgeDetails>";
                    string portA = string.Empty, portB = string.Empty;
                    if (ipService.Count > 0)
                    {
                        portA = ipService[0].ipAddress;
                        if (ipService.Count > 0)
                            portB = ipService[1].ipAddress;
                    }
                    outputXML += "<portA>" + portA + "</portA>";
                    outputXML += "<portB>" + portB + "</portB>";
                    outputXML += "<ISDNAudioPref>" + mcu.ISDNAudioPrefix + "</ISDNAudioPref>";//FB 2003
                    outputXML += "<ISDNVideoPref>" + mcu.ISDNVideoPrefix + "</ISDNVideoPref>";
                    //NG fixes
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    // FB 2636 Starts
                    outputXML += "<E164Dialing>" + mcu.E164Dialing + "</E164Dialing>";
                    outputXML += "<H323Dialing>" + mcu.H323Dialing + "</H323Dialing>";
                    vrmMCUE164Services E164 = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeID", mcu.BridgeID));
                    List<vrmMCUE164Services> E164Services = m_IE164Services.GetByCriteria(criterionList);
                    outputXML += "<E164Services>";
                    for (int j = 0; j < E164Services.Count; j++)
                    {
                        E164 = E164Services[j];
                        outputXML += "<E164Service>";
                        outputXML += "<SortID>" + (j + 1).ToString() + "</SortID>";
                        outputXML += "<StartRange>" + E164.StartRange + "</StartRange>";
                        outputXML += "<EndRange>" + E164.EndRange + "</EndRange>";
                        outputXML += "</E164Service>";
                    }
                    outputXML += "</E164Services>";
                    // FB 2636 Ends
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region TANDBERG
                else if (vendorList[i].BridgeInterfaceId == MCUType.TANDBERG) // Tanberg bridge
                {
                    // Get the control port
                    outputXML += "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    vrmMCUISDNServices isdn = null; //FB 2636
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region RADVISION
                //Code Modified For FB 1222 - Start
                else if (vendorList[i].BridgeInterfaceId == MCUType.RADVISION) // Radvision 
                {
                    // Get the control port
                    outputXML = "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ConferenceServiceID>" + mcu.ConfServiceID + "</ConferenceServiceID>"; //FB 2016

                    vrmMCUIPServices ip = null;//FB 2636
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipServices = m_IipServices.GetByCriteria(criterionList);
                    outputXML += "<IPServices>";
                    for (int k = 0; k < ipServices.Count; k++)
                    {
                        ip = ipServices[k];
                        outputXML += "<IPService>";
                        outputXML += "<SortID>" + ip.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + ip.ServiceName + "</name>";
                        outputXML += "<addressType>" + ip.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + ip.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + ip.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + ip.usage.ToString() + "</usage>";
                        outputXML += "</IPService>";
                    }
                    outputXML += "</IPServices>";
                    
                    vrmMCUISDNServices isdn = null;//FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    vrmMCUMPIServices mpi = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUMPIServices> mpiServices = m_ImpiService.GetByCriteria(criterionList);
                    outputXML += "<MPIServices>";
                    for (int k = 0; k < mpiServices.Count; k++)
                    {
                        mpi = mpiServices[k];
                        outputXML += "<MPIService>";
                        outputXML += "<SortID>" + mpi.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + mpi.ServiceName + "</name>";
                        outputXML += "<addressType>" + mpi.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + mpi.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + mpi.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + mpi.usage.ToString() + "</usage>";
                        outputXML += "</MPIService>";
                    }
                    outputXML += "</MPIServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region Codian  MSE8000
                else if (vendorList[i].BridgeInterfaceId == MCUType.MSE8000) //FB 2008 start
                {
                    // BridgeIPAddress table contains the two ports
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipService = m_IipServices.GetByCriteria(criterionList);

                    outputXML = "<bridgeDetails>";
                    string portA = string.Empty, portB = string.Empty;
                    if (ipService.Count > 0)
                    {
                        portA = ipService[0].ipAddress;
                        if (ipService.Count > 0)
                            portB = ipService[1].ipAddress;
                    }
                    outputXML += "<portA>" + portA + "</portA>";
                    outputXML += "<portB>" + portB + "</portB>";
                    outputXML += "<ISDNGateway>" + mcu.ISDNGateway + "</ISDNGateway>";//FB 2003
                    outputXML += "<ISDNAudioPref>" + mcu.ISDNAudioPrefix + "</ISDNAudioPref>";//FB 2003
                    outputXML += "<ISDNVideoPref>" + mcu.ISDNVideoPrefix + "</ISDNVideoPref>";
                    //NG fixes
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    //FB 2636 Starts
                    outputXML += "<E164Dialing>" + mcu.E164Dialing + "</E164Dialing>";
                    outputXML += "<H323Dialing>" + mcu.H323Dialing + "</H323Dialing>";
                    vrmMCUE164Services E164 = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeID", mcu.BridgeID));
                    List<vrmMCUE164Services> E164Services = m_IE164Services.GetByCriteria(criterionList);
                    outputXML += "<E164Services>";
                    for (int j = 0; j < E164Services.Count; j++)
                    {
                        E164 = E164Services[j];
                        outputXML += "<E164Service>";
                        outputXML += "<SortID>" + (j + 1).ToString() + "</SortID>";
                        outputXML += "<StartRange>" + E164.StartRange + "</StartRange>";
                        outputXML += "<EndRange>" + E164.EndRange + "</EndRange>";
                        outputXML += "</E164Service>";
                    }
                    outputXML += "</E164Services>";
                    //FB 2636 Ends
                    outputXML += "</bridgeDetails>";
                }
                //FB 2008 end
                #endregion
                # region LIFESIZE
                //FB 2261 start
                else if (vendorList[i].BridgeInterfaceId == MCUType.LIFESIZE) // lifesize bridge
                {
                    // Get the control port
                    outputXML += "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ISDNServices></ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "</bridgeDetails>";
                }
                //FB 2261 end
                #endregion
                #region CISCO
                //Code Modified For FB 1222 - End
                //FB 2501 Call Monitoring Start
                else if (vendorList[i].BridgeInterfaceId == MCUType.CISCO) // CISCO bridge
                {
                    // BridgeIPAddress table contains the two ports
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipService = m_IipServices.GetByCriteria(criterionList);

                    outputXML = "<bridgeDetails>";
                    string portA = string.Empty, portB = string.Empty;
                    if (ipService.Count > 0)
                    {
                        portA = ipService[0].ipAddress;
                        if (ipService.Count > 0)
                            portB = ipService[1].ipAddress;
                    }
                    outputXML += "<portA>" + portA + "</portA>";
                    outputXML += "<portB>" + portB + "</portB>";
                    outputXML += "<ISDNAudioPref>" + mcu.ISDNAudioPrefix + "</ISDNAudioPref>";
                    outputXML += "<ISDNVideoPref>" + mcu.ISDNVideoPrefix + "</ISDNVideoPref>";
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    //FB 2636 Starts
                    outputXML += "<E164Dialing>" + mcu.E164Dialing + "</E164Dialing>";
                    outputXML += "<H323Dialing>" + mcu.H323Dialing + "</H323Dialing>";
                    vrmMCUE164Services E164;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeID", mcu.BridgeID));
                    List<vrmMCUE164Services> E164Services = m_IE164Services.GetByCriteria(criterionList);
                    outputXML += "<E164Services>";
                    for (int j = 0; j < E164Services.Count; j++)
                    {
                        E164 = E164Services[j];
                        outputXML += "<E164Service>";
                        outputXML += "<SortID>" + (j + 1).ToString() + "</SortID>";
                        outputXML += "<StartRange>" + E164.StartRange + "</StartRange>";
                        outputXML += "<EndRange>" + E164.EndRange + "</EndRange>";
                        outputXML += "</E164Service>";
                    }
                    outputXML += "</E164Services>";
                    //FB 2636 Ends
                    outputXML += "<Synchronous>" + mcu.Synchronous + "</Synchronous>"; //FB 2718
                    outputXML += "<Domain>" + mcu.RPRMDomain + "</Domain>"; //FB 2718
                    outputXML += "</bridgeDetails>";
                }
                //FB 2501 Call Monitoring Ends
                #endregion 
                //FB 2556 - Starts
                else if (vendorList[i].BridgeInterfaceId == MCUType.iVIEW)
                {
                    outputXML += "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ISDNServices></ISDNServices>";
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    outputXML += "<Multitenant>" + mcu.IsMultitenant + "</Multitenant>";
                    outputXML += "<Synchronous>" + mcu.Synchronous + "</Synchronous>";
                    m_IMCUOrgSpecificDetailsDao = m_hardwareDAO.GetMCUOrgSpecificDetailsDao();//FB 2556
                    {
                        outputXML += "<orgSpecificDetails>";
                        osCriterionList = new List<ICriterion>();
                        osCriterionList.Add(Expression.Eq("bridgeID", mcu.BridgeID));
                        osCriterionList.Add(Expression.Eq("orgID", organizationID));

                        orgSpecificdetails = m_IMCUOrgSpecificDetailsDao.GetByCriteria(osCriterionList,true);

                        if (orgSpecificdetails.Count > 0)
                        {
                            for (cntDtls = 0; cntDtls < orgSpecificdetails.Count; cntDtls++)
                                outputXML += "<" + orgSpecificdetails[cntDtls].propertyName.Trim() + ">" + orgSpecificdetails[cntDtls].propertyValue.Trim() + "</" + orgSpecificdetails[cntDtls].propertyName.Trim() + ">";
 
                        }
                        outputXML += "</orgSpecificDetails>";
                    }
                    outputXML += "<ExtSiloProfiles>";
                    osCriterionList = new List<ICriterion>();
                    osCriterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    m_IExtMCUSilo = m_hardwareDAO.GetExtMCUSiloDao();
                    MCUSilo = m_IExtMCUSilo.GetByCriteria(osCriterionList);
                    for (p = 0; p < MCUSilo.Count; p++)
                    {
                        outputXML += "<Profile>";
                        outputXML += "<ID>" + MCUSilo[p].MemberId + "</ID>";
                        outputXML += "<name>" + MCUSilo[p].Name + "</name>";
                        outputXML += "</Profile>";
                    }
                    outputXML += "</ExtSiloProfiles>";

                    outputXML += "<ExtServiceProfiles>";
                    m_IExtMCUService = m_hardwareDAO.GetExtMCUServiceDao(); //FB 2556                    
                    MCUService = m_IExtMCUService.GetByCriteria(osCriterionList);
                    for (p = 0; p < MCUService.Count; p++)
                    {
                        outputXML += "<Profile>";
                        outputXML += "<ID>" + MCUService[p].ServiceId+ "</ID>";
                        outputXML += "<name>" + MCUService[p].ServiceName + "</name>";
                        outputXML += "</Profile>";
                    }
                    outputXML += "</ExtServiceProfiles>";

                    outputXML += "</bridgeDetails>";
                }
                //FB 2556 - End
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return outputXML;
        }
        #endregion

        #region FetchBridgeTypeList
        // fetch bridge list
        private string FetchBridgeTypeList()
        {
            try
            {
                string outputXML = "<bridgeTypes>";

                List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                vrmMCUVendor vendor = null;
                for (int i = 0; i < vendorList.Count; i++)
                {
                    vendor = vendorList[i];
                    outputXML += "<type>";
                    outputXML += "<ID>" + vendor.id.ToString() + "</ID>";
                    outputXML += "<name>" + vendor.name + "</name>";
                    outputXML += "<interfaceType>" + vendor.BridgeInterfaceId + "</interfaceType>";
                    outputXML += "</type>";
                }

                outputXML += "</bridgeTypes>";

                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region FetchBridgeStatusList
        private string FetchBridgeStatusList()
        {
            try
            {
                string outputXML = "<bridgeStatuses>";
                List<vrmMCUStatus> statusList = vrmGen.getMCUStatus();

                foreach (vrmMCUStatus MCUStatus in statusList)
                {
                    outputXML += "<status>";
                    outputXML += "<ID>" + MCUStatus.id.ToString() + "</ID>";
                    outputXML += "<name>" + MCUStatus.status + "</name>";
                    outputXML += "</status>";
                }
                outputXML += "</bridgeStatuses>";

                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region FetchAddressTypeList
        // please look at GetAddressType as they are identicle accept for spelling
        public string FetchAddressTypeList()
        {
            string outXml = string.Empty;

            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmAddressType> addressType = vrmGen.getAddressType();

                outXml = "<addressType>";
                foreach (vrmAddressType at in addressType)
                {
                    outXml += "<type>";
                    outXml += "<ID>" + at.Id.ToString() + "</ID>";
                    outXml += "<name>" + at.name + "</name>";
                    outXml += "</type>";
                }
                outXml += "</addressType>";

                return outXml;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }

        }
        #endregion

        #region FetchBridgeList

        // FetchBridgeList is a little different from FetchBridges 
        //  FetchBridges(only bridges with stats > 0, also returns a virtual tag and an administratot tag

        private string FetchBridgeList(ref int count)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Gt("Status", 0));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1))); // FB 1920
                //criterionList.Add(Expression.Eq("orgId", organizationID));//organization fixes
                m_ImcuDao.addOrderBy(Order.Asc("ChainPosition"));

                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionList);
                count = MCUList.Count;
                m_ImcuDao.sledgeHammerReset();

                string outputXML = "<bridges>";

                foreach (vrmMCU mcu in MCUList)
                {
                    outputXML += "<bridge>";
                    outputXML += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    // FB 1920 Starts
                    outputXML += "<Orgid>" + mcu.orgId.ToString() + "</Orgid>";
                    outputXML += "<isPublic>" + mcu.isPublic.ToString() + "</isPublic>";
                    if (mcu.isPublic == 1)
                    {
                        if (organizationID != 11) //Default org 11
                        {
                            mcu.BridgeName = mcu.BridgeName + "(*)";
                            count = count - 1;
                        }
                    }
                    // FB 1920 Ends
                    outputXML += "<name>" + mcu.BridgeName + "</name>";
                    outputXML += "<interfaceType>" + mcu.MCUType.id.ToString() + "</interfaceType>";

                    vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);
                    //* *** FB 1462 - start *** */
                    if (user != null)
                    {
                        outputXML += "<administrator>" + user.FirstName + ", " + user.LastName + "</administrator>";
                        outputXML += "<userstate>A</userstate>";
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            outputXML += "<administrator>" + iuser.FirstName + ", " + iuser.LastName + "</administrator>";
                            outputXML += "<userstate>I</userstate>";
                        }
                        else
                        {
                            outputXML += "<administrator>User does not exist</administrator>";
                            outputXML += "<userstate>D</userstate>";
                        }
                    }
                    /* *** FB 1462 - end *** */

                    string exist = "0";
                    if (mcu.VirtualBridge == 0)
                        exist = "1";
                    outputXML += "<exist>" + exist + "</exist>";
                    outputXML += "<status>" + mcu.Status.ToString() + "</status>";
                    outputXML += "<order>" + mcu.ChainPosition.ToString() + "</order>";
                    outputXML += "<address>" + mcu.BridgeAddress + "</address>"; //FB 2362
                    outputXML += "</bridge>";
                }
                outputXML += "</bridges>";
                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region FetchBridges

        private string FetchBridges()
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("deleted", 0));

                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionList);

                string outputXML = "<bridges>";

                foreach (vrmMCU mcu in MCUList)
                {
                    outputXML += "<bridge>";
                    outputXML += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    outputXML += "<name>" + mcu.BridgeName + "</name>";
                    outputXML += "<interfaceType>" + mcu.BridgeTypeId.ToString() + "</interfaceType>";

                    vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);

                    /* *** FB 1462 - Fixes start *** */
                    if (user != null)
                    {
                        outputXML += "<administrator>" + user.FirstName + ", " + user.LastName + "</administrator>";
                        outputXML += "<userstate>A</userstate>";
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            outputXML += "<administrator>" + iuser.FirstName + ", " + iuser.LastName + "</administrator>";
                            outputXML += "<userstate>I</userstate>";
                        }
                        else
                        {
                            outputXML += "<administrator>User does not exist</administrator>";
                            outputXML += "<userstate>D</userstate>";
                        }
                    }
                    /* *** FB 1462 - Fixes end *** */

                    outputXML += "<exist>" + mcu.VirtualBridge.ToString() + "</exist>";
                    outputXML += "<status>" + mcu.Status.ToString() + "</status>";
                    outputXML += "<order>" + mcu.ChainPosition.ToString() + "</order>";
                    outputXML += "</bridge>";
                }
                outputXML += "</bridges>";
                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region SetBridge

        public bool SetBridge(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            XmlNode node;
            IConfApprovalDAO IApprover; //To Check Conf_approval Table//FB 1171

            string[] orgSpecificProps = null;//FB 2556-TDB
            List<vrmMCUOrgSpecificDetails> orgSpecificdetails = null;//FB TDB
            vrmMCUOrgSpecificDetails orgSpecificdetail = null;//FB TDB
            IMCUOrgSpecificDetailsDao m_IMCUOrgSpecificDetailsDao = null;
            List<ICriterion> osCriterionList = null;
            int cntDtls = 0;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                int ConfServiceID = 0; //FB 2016 //FB 2427
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                cryptography.Crypto crypto = new cryptography.Crypto();

                node = xd.SelectSingleNode("//setBridge/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//setBridge/bridge/bridgeID");
                string bridgeid = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/name");
                string bridgename = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/login");
                string login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/password");
                string password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/bridgeType");
                string bridgetype = node.InnerXml.Trim();
                if (bridgetype.Length == 0) bridgetype = "1"; // Default to Accord
                node = xd.SelectSingleNode("//setBridge/bridge/bridgeStatus");
                string bridgeStatus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/URLAccess");//ZD 100113
                string URLAccess = node.InnerXml.Trim();
                if (bridgeStatus.Length < 1) bridgeStatus = "1"; // Active
                // FB 1920 Starts
                node = xd.SelectSingleNode("//setBridge/bridge/isPublic");
                string chkisPublic = node.InnerXml.Trim();
                if (chkisPublic.Length < 1)
                    chkisPublic = "0"; // Private
                // FB 1920 Ends

                // FB 2501 CallMonitor Favourite Starts
                int chkSetFav = 0;
                node = xd.SelectSingleNode("//setBridge/bridge/setFavourite");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out chkSetFav);
                // FB 2501 Ends
                
                // 2660 Starts
                int EnableCDR = 0, DeleteCDR = 0;//FB 2714
                node = xd.SelectSingleNode("//setBridge/bridge/enableCDR");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableCDR);
                node = xd.SelectSingleNode("//setBridge/bridge/deleteCDRDays");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out DeleteCDR);
                //FB 2714 Starts
                //if (EnableCDR > 0 && (DeleteCDR < 1 || DeleteCDR > 100) )
                //{
                //    myVRMException myVRMEx = new myVRMException(670);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}
                // FB 2660 Ends//FB 2714 Ends
                node = xd.SelectSingleNode("//setBridge/bridge/virtualBridge");
                string virtbridge = node.InnerXml.Trim();
                if (virtbridge.Length < 1) virtbridge = "0"; // false
                node = xd.SelectSingleNode("//setBridge/bridge/bridgeAdmin/ID");
                string bridgeadmin = node.InnerXml.Trim();
                if (bridgeadmin.Length < 1) bridgeadmin = "11"; // default
                node = xd.SelectSingleNode("//setBridge/bridge/malfunctionAlert");
                string malfunctionalert = node.InnerXml.Trim();
                if (malfunctionalert.Length < 1) malfunctionalert = "0"; // no alert
                node = xd.SelectSingleNode("//setBridge/bridge/timeZone");
                string timezone = node.InnerXml.Trim();
                if (timezone.Length < 1) timezone = "26"; // default EST hard-coded. Change to default system
                node = xd.SelectSingleNode("//setBridge/bridge/firmwareVersion");
                string firmwarever = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/percentReservedPort");
                string resportperc = node.InnerXml.Trim();
                if (resportperc.Length < 1) resportperc = "0"; // Reserve 0% ports by default

                node = xd.SelectSingleNode("//setBridge/bridge/maxAudioCalls");
                string maxAudioCalls = node.InnerXml.Trim();
                if (maxAudioCalls.Length < 1) maxAudioCalls = "1";
                node = xd.SelectSingleNode("//setBridge/bridge/maxVideoCalls");
                string maxVideoCalls = node.InnerXml.Trim();
                //FB 2660 Starts
                int EnhancedMCU =0;
                node = xd.SelectSingleNode("//setBridge/bridge/EnhancedMCU");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnhancedMCU);
                //FB 2660 Ends
                //API Port starts..
                int apiPort = 23;
                if (xd.SelectSingleNode("//setBridge/bridge/ApiPortNo") != null)
                {
                    int.TryParse(xd.SelectSingleNode("//setBridge/bridge/ApiPortNo").InnerText.Trim(), out apiPort);
                }
                if (apiPort <= 0)
                    apiPort = 23;
                //API Port ends..

                node = xd.SelectSingleNode("//setBridge/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (maxVideoCalls.Length < 1) maxVideoCalls = "1";

                int BridgeID = 0;
                bridgeid = bridgeid.ToLower();
                if (bridgeid != "new")
                    BridgeID = Int32.Parse(bridgeid);

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeName", bridgename));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));//code added for organization

                List<vrmMCU> checkMCU = m_ImcuDao.GetByCriteria(criterionList);

                if (checkMCU.Count > 0)
                {
                    if (checkMCU.Count > 1 || checkMCU[0].BridgeID != BridgeID)
                    {
                        myVRMException ex = new myVRMException(260);
                        m_log.Error("Error in SetBridge: ", ex);
                        obj.outXml = ex.FetchErrorMsg();//FB 1881//myVRMException.toXml(ex.Message);
                        return false;
                    }
                }

                if (BridgeID == 0) // New bridge //FB 1286 Starts
                {
                    List<ICriterion> selcnt = new List<ICriterion>();
                    selcnt.Add(Expression.Eq("deleted", 0));
                    selcnt.Add(Expression.Eq("orgId", organizationID));//code added for organization


                    List<vrmMCU> checkMCUCount = m_ImcuDao.GetByCriteria(selcnt);

                    if (checkMCUCount.Count >= orgdt.MCULimit)
                    {
                        myVRMException ex = new myVRMException(252);
                        m_log.Error("Error MCU Limit exceeeded: ", ex);
                        obj.outXml = ex.FetchErrorMsg();//FB 1881//myVRMException.toXml(ex.Message);
                        return false;
                    }
                }
                //FB 1286 Ends

                //FB 2660 Starts
                if (EnhancedMCU > 0)
                {
                    List<ICriterion> EnhancedMCUList = new List<ICriterion>();
                    EnhancedMCUList.Add(Expression.Eq("EnhancedMCU", 1));
                    EnhancedMCUList.Add(Expression.Eq("deleted", 0));
                    EnhancedMCUList.Add(Expression.Eq("orgId", organizationID));
                    if (BridgeID > 0)
                        EnhancedMCUList.Add(Expression.Not(Expression.Eq("BridgeID", BridgeID)));
                    List<vrmMCU> chkMcu = m_ImcuDao.GetByCriteria(EnhancedMCUList);
                    if (chkMcu.Count >= orgdt.MCUEnchancedLimit)
                    {
                        myVRMException ex = new myVRMException(252);
                        m_log.Error("Error EnhancedMCU Limit exceeeded: ", ex);
                        obj.outXml = ex.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2660 Ends
                vrmMCU MCU = null;

                if (BridgeID == 0)
                {
                    MCU = new vrmMCU();
                    MCU.BridgeID = 0;
                    MCU.orgId = organizationID;
                }
                else
                {
                    MCU = m_ImcuDao.GetById(BridgeID, true);
                }


                // FB 2653 Starts                
                string McuIpAddress = "";
                XmlNode McuIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                McuIpAddress = (McuIp != null && McuIp.InnerXml.Trim() != "") ? McuIp.InnerXml.Trim()
                               : xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA").InnerXml.Trim();

                string ispublic = xd.SelectSingleNode("//setBridge/bridge/isPublic").InnerXml.Trim();
                List<ICriterion> selMCucnt = new List<ICriterion>();
                selMCucnt.Add(Expression.Eq("deleted", 0));
                selMCucnt.Add(Expression.Eq("BridgeAddress", McuIpAddress));

                if (MCU.BridgeID != 0)
                    selMCucnt.Add(Expression.Not(Expression.Eq("BridgeID", MCU.BridgeID)));

                List<vrmMCU> chkMCUDuplicateRecCount = m_ImcuDao.GetByCriteria(selMCucnt);
                if (chkMCUDuplicateRecCount.Count > 0)
                {
                    myVRMException ex = new myVRMException(668);
                    m_log.Error("Mcu ip address has been duplicated", ex);
                    obj.outXml = ex.FetchErrorMsg();
                    return false;
                }
                // FB 2653 Ends

                //FB 1642 - DTMF - Start Commented for FB 2655
                //node = xd.SelectSingleNode("//setBridge/bridge/DTMF/PreConfCode");
                //MCU.PreConfCode = node.InnerText.Trim();

                //node = xd.SelectSingleNode("//setBridge/bridge/DTMF/PreLeaderPin");
                //MCU.PreLeaderPin = node.InnerText.Trim();

                //node = xd.SelectSingleNode("//setBridge/bridge/DTMF/PostLeaderPin");
                //MCU.PostLeaderPin = node.InnerText.Trim();

                //FB 1642 - DTMF - End

                MCU.BridgeID = BridgeID;
                MCU.BridgeName = bridgename;
                MCU.BridgeLogin = login;
                //MCU.BridgePassword = crypto.encrypt(password);
                MCU.BridgePassword = password; //FB 3054
                MCU.BridgeTypeId = Int32.Parse(bridgetype);
                MCU.Status = Int32.Parse(bridgeStatus);
                MCU.URLAccess = Int32.Parse(URLAccess);//ZD 100113
                MCU.VirtualBridge = Int32.Parse(virtbridge);

                //FB 2539 Start
                int adminId = Int32.Parse(bridgeadmin);
                MCU.Admin = adminId; //Commented - FB 2539
                //vrmUser user = m_vrmUserDAO.GetByUserId(adminId);
                //if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                //else
                //{
                //    myVRMException myVRMEx = new myVRMException(629);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}
                //FB 2539 End
                
                MCU.malfunctionalert = Int32.Parse(malfunctionalert);
                MCU.timezone = Int32.Parse(timezone);
                MCU.SoftwareVer = firmwarever;
                MCU.reservedportperc = Int32.Parse(resportperc);
                MCU.MaxConcurrentAudioCalls = Int32.Parse(maxAudioCalls);
                MCU.MaxConcurrentVideoCalls = Int32.Parse(maxVideoCalls);
                MCU.CurrentDateTime = DateTime.Now;//FB 2448 Start
                MCU.LastModified = DateTime.Now;
                MCU.EnableCDR = EnableCDR; //FB 2660
                MCU.DeleteCDRDays = DeleteCDR; //FB 2660
                MCU.ApiPortNo = apiPort;//API Ports..
                string xmlField;
                node = xd.SelectSingleNode("//setBridge/bridge/ISDNThresholdAlert");
                xmlField = node.InnerXml.Trim();
                int isdnthresholdalert = 0;
                if (xmlField.Length > 0)
                    isdnthresholdalert = Int32.Parse(xmlField);

                MCU.isdnthresholdalert = isdnthresholdalert;
                if (isdnthresholdalert == 1)
                {
                    node = xd.SelectSingleNode("//setBridge/bridge/ISDNPortCharge");
                    xmlField = node.InnerXml.Trim();
                    if (xmlField.Length == 0)
                        MCU.isdnportrate = 0;
                    else
                        MCU.isdnportrate = Int32.Parse(xmlField);

                    node = xd.SelectSingleNode("//setBridge/bridge/ISDNLineCharge");
                    xmlField = node.InnerXml.Trim();
                    if (xmlField.Length == 0)
                        MCU.isdnlinerate = 0;
                    else
                        MCU.isdnlinerate = Int32.Parse(xmlField);

                    node = xd.SelectSingleNode("//setBridge/bridge/ISDNThreshold");
                    xmlField = node.InnerXml.Trim();
                    if (xmlField.Length == 0)
                        MCU.isdnthreshold = 0;
                    else
                        MCU.isdnthreshold = Int32.Parse(xmlField);

                    node = xd.SelectSingleNode("//setBridge/bridge/ISDNMaxCost");
                    xmlField = node.InnerXml.Trim();
                    if (xmlField.Length == 0)
                        MCU.isdnmaxcost = 0;
                    else
                        MCU.isdnmaxcost = Int32.Parse(xmlField);



                }

                List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                int i = -1;
                int bridgeInterfaceId = 0;
                foreach (vrmMCUVendor vendor in vendorList)
                {
                    i++;
                    if (vendor.id == MCU.BridgeTypeId)
                    {
                        bridgeInterfaceId = vendor.BridgeInterfaceId;
                        if (MCU.BridgeID == 0)
                            MCU.MCUType = new vrmMCUType();
                        MCU.MCUType.bridgeInterfaceId = vendor.BridgeInterfaceId;
                        MCU.MCUType.audioParticipants = vendor.audioparticipants;
                        MCU.MCUType.videoParticipants = vendor.videoparticipants;
                        MCU.MCUType.name = vendor.name;
                        MCU.MCUType.id = vendor.id;

                        break;
                    }
                }

                //FB 1920 start
                List<vrmConfRoom> rList = new List<vrmConfRoom>();
                List<vrmConfUser> uList = new List<vrmConfUser>();
                List<vrmConference> confList = new List<vrmConference>();
                criterionList = new List<ICriterion>();
                if (chkisPublic == "0" && MCU.isPublic == 1)
                {
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Not(Expression.Eq("status", (int)vrmConfStatus.Completed)));
                    criterionList.Add(Expression.Ge("confdate", DateTime.Now));
                    criterionList.Add(Expression.Not(Expression.Eq("orgId", 11)));
                    confList = m_vrmConfDAO.GetByCriteria(criterionList);
                    for (int c = 0; c < confList.Count; c++)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", confList[c].confid));
                        criterionList.Add(Expression.Eq("instanceid", confList[c].instanceid));
                        criterionList.Add(Expression.Eq("bridgeid", MCU.BridgeID));
                        rList = m_IconfRoom.GetByCriteria(criterionList);
                        uList = m_IconfUser.GetByCriteria(criterionList);
                        if (rList.Count > 0 || uList.Count > 0)
                        {
                            //Not allow to change as private
                            MCU.isPublic = 1;
                            myVRMException ex = new myVRMException(516);
                            obj.outXml = ex.FetchErrorMsg();
                            m_ImcuDao.SaveOrUpdate(MCU);
                            return false;
                        }
                    }
                }

                MCU.isPublic = Int32.Parse(chkisPublic);
                MCU.setFavourite = chkSetFav; // FB 2501 CallMonitor Favourite
                //FB 1920 end
                string BridgeExtNo = " ";//FB 2610
                MCU.BridgeExtNo = ""; //FB 2610
                
                //FB 2539 Start
                XmlNodeList appList1 = xd.GetElementsByTagName("approver");
                foreach (XmlNode innerNode in appList1)
                {
                    XmlElement innerElement = (XmlElement)innerNode;
                    string sAppId = innerElement.GetElementsByTagName("ID")[0].InnerText;
                    if (sAppId != "" && sAppId != null)
                    {
                        if (!m_SearchFactory.CheckApproverRights(sAppId))
                        {
                            myVRMException myVRMEx = new myVRMException(632);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }

                //FB 2539 End
                
                m_ImcuDao.SaveOrUpdate(MCU);

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("mcuid", MCU.BridgeID));
                List<vrmMCUApprover> approverList = m_Iapprover.GetByCriteria(criterionList);
                //Code Added for FB Issu 1171 --Start
                foreach (vrmMCUApprover app in approverList)
                {
                    criterionList = new List<ICriterion>();
                    confList = new List<vrmConference>(); //FB 1920
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("status", (int)vrmConfStatus.Pending));
                    criterionList.Add(Expression.Ge("confdate", DateTime.Now));
                    confList = m_vrmConfDAO.GetByCriteria(criterionList);
                    foreach (vrmConference conf in confList)
                    {
                        criterionList = new List<ICriterion>();
                        rList = new List<vrmConfRoom>(); //FB 1920
                        uList = new List<vrmConfUser>(); //FB 1920
                        List<vrmMCU> mcuAppList = new List<vrmMCU>();
                        criterionList.Add(Expression.Eq("confid", conf.confid));
                        criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                        rList = m_IconfRoom.GetByCriteria(criterionList);
                        uList = m_IconfUser.GetByCriteria(criterionList);
                        loadMCUInfo(ref mcuAppList, rList, uList);

                        foreach (vrmMCU Appmcu in mcuAppList)
                        {
                            if (MCU.BridgeID == Appmcu.BridgeID)
                            {
                                IApprover = m_confDAO.GetConfApprovalDao();
                                criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.Eq("approverid", app.approverid));
                                criterionList.Add(Expression.Eq("entityid", Appmcu.BridgeID));
                                criterionList.Add(Expression.Eq("entitytype", (int)LevelEntity.MCU));
                                criterionList.Add(Expression.Eq("confid", conf.confid));
                                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                                criterionList.Add(Expression.Eq("decision", 0)); //Pending
                                List<vrmConfApproval> ConfApprList = IApprover.GetByCriteria(criterionList);
                                if (ConfApprList.Count > 0)
                                {
                                    //FB 1881 start
                                    //obj.outXml = "<error><message>There are conferences pending for approval from this user.&lt;br&gt;Please approve conferences in List>View Approval Pending prior to deleting this user.</message></error>";  
                                    myVRMException myVRMEx = new myVRMException(437);
                                    obj.outXml = myVRMEx.FetchErrorMsg();
                                    //FB 1881 end
                                    return false;
                                }
                            }
                        }
                    }

                }

                //Code Added for FB Issu 1171 --Start
                foreach (vrmMCUApprover app in approverList)
                    m_Iapprover.Delete(app);

                MCU.MCUApprover.Clear();
                XmlNodeList appList = xd.GetElementsByTagName("approver");
                int a = 0;
                foreach (XmlNode innerNode in appList)
                {
                    a++;
                    XmlElement innerElement = (XmlElement)innerNode;
                    string sAppId = innerElement.GetElementsByTagName("ID")[0].InnerText;
                    if (sAppId.Length > 0)
                    {
                        int iApproverId = Int32.Parse(sAppId);
                        vrmMCUApprover approver = new vrmMCUApprover();
                        approver.approverid = iApproverId;
                        approver.mcuid = MCU.BridgeID;
                        MCU.MCUApprover.Add(approver);
                    }
                }
                if (a > 0)
                    m_ImcuDao.SaveOrUpdate(MCU);

                MCU.EnableMessage = 0; //FB 2486
                MCU.BridgeExtNo = ""; //FB 2610
                switch (bridgeInterfaceId)
                {
                    case MCUType.POLYCOM:
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        MCU.BridgeAddress = node.InnerXml.Trim();

                        //FB 1766 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/EnableIVR");
                        int EnableIVR = 0;
                        Int32.TryParse(node.InnerXml.Trim(), out EnableIVR);
                        MCU.EnableIVR = EnableIVR;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/IVRServiceName");
                        MCU.IVRServiceName = node.InnerXml.Trim();
                        //FB 1766 - End
                        //FB 1907 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/enableRecord");
                        int EnableRecord = 0;
                        Int32.TryParse(node.InnerXml.Trim(), out EnableRecord);
                        MCU.EnableRecording = EnableRecord;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/enableLPR");
                        int enableLPR = 0;
                        Int32.TryParse(node.InnerXml.Trim(), out enableLPR);
                        MCU.LPR = enableLPR;

                        //FB 2486 - Start
                        int EnableMessage = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/EnableMessage");
                        int.TryParse(node.InnerXml.Trim(), out EnableMessage);
                        MCU.EnableMessage = EnableMessage;
                        //FB 2486 - End
                        //FB 2636 Starts
                        int tmpE164Dialing = 0, tmpH323Dialing = 0;
                        MCU.EnhancedMCU = 0;
                        if (EnableMessage > 0) 
                            MCU.EnhancedMCU = 1;
                        //FB 2636 Ends
						//FB 2441 II Starts

                        string RPRMDomain = "";
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMDomain");
                        if (node != null)
                            RPRMDomain = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMDomain").InnerText.Trim();
                        MCU.RPRMDomain = RPRMDomain;

                        int isSynchronous = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/Synchronous");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/Synchronous").InnerText.Trim(), out isSynchronous);

                        if (MCU.Synchronous == 1 && isSynchronous == 0)
                        {
                            string hql;
                            DataSet ds = null;

                            if (m_bridgelayer == null)
                                m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                            //hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            //hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ((getutcdate() between b.confdate and ";
                            //hql += " dateAdd(minute,b.duration,b.confdate)) or b.confdate > getutcdate() )";
                            //Doubt
                            hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ";
                            hql += " dateAdd(minute,b.duration,b.confdate) >= getutcdate() ";

                            if (hql != "")
                                ds = m_bridgelayer.ExecuteDataSet(hql);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                myVRMException myVRMEx = new myVRMException(665);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        MCU.Synchronous = isSynchronous;

						//FB 2441 - Starts
                        int DMASendMail = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/Sendmail");
                        if(node != null)
                           int.TryParse(node.InnerXml.Trim(), out DMASendMail);
                        MCU.DMASendMail = DMASendMail;

                        
                        int DMAMonitorMCU = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/MonitorMCU");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out DMAMonitorMCU);
                        MCU.DMAMonitorMCU = DMAMonitorMCU;

                        string Name = "", sDMALogin = "", sDMAPassword = "", sDMAURL = "", sDMADialinprefix = "", loginname = "", email = "",sDMADomain=""; //FB 2689 //FB 2441 II //FB 2709
                        int sDMAPort = 0;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/EnhancedMCU");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/EnhancedMCU").InnerText.Trim(), out EnhancedMCU);
                        if(EnhancedMCU>0)
                           MCU.EnhancedMCU = 1;
                        else
                            MCU.EnhancedMCU = 0; 
                        
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAName");
                        if (node != null)
                            Name = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAName").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMALogin");
                        if (node != null)
                            sDMALogin = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMALogin").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPassword");
                        if (node != null)
                            sDMAPassword = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAURL");
                        if (node != null)
                            sDMAURL = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAURL").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPort");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPort").InnerText.Trim(), out sDMAPort);

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADomain");
                        if (node != null)
                            sDMADomain=xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADomain").InnerText.Trim();
                        //FB 2689 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix");
                        if (node != null)
                        {
                             sDMADialinprefix = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix").InnerText.Trim();
                        }
                        //FB 2689 Ends

						//FB 2709
                        int logincount = 0; //FB 2709
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginAccount");
                        if (node != null)
                            loginname = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginAccount").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginCount");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginCount").InnerText.Trim(), out logincount);

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMEmail");
                        if (node != null)
                            email = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMEmail").InnerText.Trim();

                        //FB 2709
   
                        MCU.DMAName = Name;
                        MCU.DMALogin = sDMALogin;
                        //MCU.DMAPassword = crypto.encrypt(sDMAPassword);
                        MCU.DMAPassword = sDMAPassword; //FB 3054
                        MCU.DMAURL = sDMAURL;
                        MCU.DMAPort = sDMAPort;
                        MCU.DMADomain = sDMADomain;
						MCU.DialinPrefix = sDMADialinprefix; //FB 2689
                        //FB 2441 II Ends
						//FB 2709
                        MCU.LoginName = loginname;
                        MCU.loginCount = logincount;
                        MCU.RPRMEmailaddress = email;
                        //FB 2709
                        
                        if (xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferenceServiceID") != null) //FB 2427
                        {
                            Int32.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferenceServiceID").InnerXml.Trim(), out ConfServiceID);
                            MCU.ConfServiceID = ConfServiceID; 
                        }
                        

                        //FB 1907 - End
                        #region IP Services
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        List<vrmMCUIPServices> IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        // Now get the services and insert the info
                        XmlNodeList xmlIPList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/IPServices/IPService");

                        vrmMCUIPServices ipService = null;
                        foreach (XmlNode innerNode in xmlIPList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string atype = innerElement.GetElementsByTagName("addressType")[0].InnerText;
                            string address = innerElement.GetElementsByTagName("address")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;
                            ipService = new vrmMCUIPServices();

                            ipService.SortID = Int32.Parse(id);
                            ipService.ServiceName = name;
                            ipService.addressType = Int32.Parse(atype);
                            ipService.ipAddress = address;
                            ipService.networkAccess = Int32.Parse(netaccess);
                            ipService.usage = Int32.Parse(usage);
                            ipService.bridgeId = MCU.BridgeID;
                            IPList.Add(ipService);
                        }
                        m_IipServices.SaveOrUpdateList(IPList);
                        #endregion

                        #region ISDN Services
                        // ISDNServices updation in BridgeISDNServices table
                        // First delete all ISDN services for this bridge

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        List<vrmMCUISDNServices> ISDNList = m_IisdnServices.GetByCriteria(criterionList);
                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();

                        // Now get the services and insert the info
                        XmlNodeList xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        vrmMCUISDNServices isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion

                        #region MPI Services
                        // now get MPI services information                         
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        List<vrmMCUMPIServices> MPIList = m_ImpiService.GetByCriteria(criterionList);
                        foreach (vrmMCUMPIServices mpi in MPIList)
                            m_ImpiService.Delete(mpi);
                        MPIList.Clear();

                        // Now get the services and insert the info
                        XmlNodeList xmlMPINList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/MPIServices/MPIService");

                        vrmMCUMPIServices mpiService = null;
                        foreach (XmlNode innerNode in xmlMPINList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string atype = innerElement.GetElementsByTagName("addressType")[0].InnerText;
                            string address = innerElement.GetElementsByTagName("address")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            if (netaccess.Length == 0)
                                netaccess = "0";
                            if (usage.Length == 0)
                                usage = "0";

                            mpiService = new vrmMCUMPIServices();
                            mpiService.SortID = Int32.Parse(id);
                            mpiService.ServiceName = name;
                            mpiService.addressType = Int32.Parse(atype);
                            mpiService.ipAddress = address;
                            mpiService.networkAccess = Int32.Parse(netaccess);
                            mpiService.usage = Int32.Parse(usage);
                            mpiService.bridgeId = MCU.BridgeID;
                            MPIList.Add(mpiService);
                        }
                        m_ImpiService.SaveOrUpdateList(MPIList);
                        #endregion

                        #region MCU Cards
                        // Now process MCU cards
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        List<vrmMCUCardList> CardList = m_IcardList.GetByCriteria(criterionList);
                        foreach (vrmMCUCardList cl in CardList)
                            m_IcardList.Delete(cl);

                        MCU.MCUCardList.Clear();
                        XmlNodeList xmlCardList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/MCUCards/MCUCard");

                        vrmMCUCardList card = null;
                        i = 0;
                        foreach (XmlNode innerNode in xmlCardList)
                        {
                            i++;
                            XmlElement innerElement = (XmlElement)innerNode;

                            string cardId = innerElement.GetElementsByTagName("ID")[0].InnerText;
                            string MaximumCalls = innerElement.GetElementsByTagName("MaximumCalls")[0].InnerText;
                            if (MaximumCalls.Length == 0)
                                MaximumCalls = "0";

                            card = new vrmMCUCardList();
                            card.MCUCardDetail = new vrmMCUCardDetail();

                            card.id = 0;
                            card.MCUCardDetail.id = Int32.Parse(cardId);
                            card.maxCalls = Int32.Parse(MaximumCalls);
                            card.bridgeId = MCU.BridgeID;

                            MCU.MCUCardList.Add(card);
                        }
                        #endregion

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends
                        //FB 2636 Starts
                        #region E.164 Services
                        vrmMCUE164Services E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        List<vrmMCUE164Services> E164List = m_IE164Services.GetByCriteria(criterionList);
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();

                        XmlNodeList xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");

                        if (tmpE164Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.EnhancedMCU = 1;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        XmlNode E164innerNode;
                        int SortID = 0;
                        string StartRange = "", EndRange = "";
                        vrmMCUE164Services E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion
                        //FB 2636 Ends
                        m_ImcuDao.SaveOrUpdate(MCU);
                        break;

                    case MCUType.CODIAN:
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA");
                        string portA = node.InnerXml.Trim();
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portB");
                        string portB = node.InnerXml.Trim();
                        //FB 2003 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNAudioPref");//FB 2003                        
                        MCU.ISDNAudioPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNVideoPref");//FB 2003                                               
                        MCU.ISDNVideoPrefix = node.InnerXml.Trim();
                        //FB 2003 - End

                        MCU.BridgeAddress = portA;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();
                        
                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends
                        //FB 2636 Starts
                        tmpE164Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/E164Dialing");
                        if(node!= null)
                            int.TryParse(node.InnerXml.Trim(), out tmpE164Dialing);
                        tmpH323Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/H323Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpH323Dialing);

                        #region E.164 Services
                        E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        E164List = m_IE164Services.GetByCriteria(criterionList);
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();

                        xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");

                        MCU.EnhancedMCU = 0;
                        MCU.E164Dialing = 0;
                        MCU.H323Dialing = 0;
                        if (tmpE164Dialing > 0 || tmpH323Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.H323Dialing = tmpH323Dialing;
                                MCU.EnhancedMCU = 1;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        E164innerNode = null;
                        SortID = 0;
                        StartRange = ""; EndRange = "";
                        E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion

                        //FB 2636 Ends
                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portA;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = portB;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        #endregion

                        break;

                    case MCUType.TANDBERG: // Tandberg bridge
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        string bridgeAddress = node.InnerXml.Trim();
                        MCU.BridgeAddress = bridgeAddress;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region ISDN Service
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        ISDNList = m_IisdnServices.GetByCriteria(criterionList);
                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();

                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion

                        

                        break;

                    case MCUType.RADVISION: // Radvision bridge
                        MCU.BridgeAddress = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                        Int32.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferenceServiceID").InnerXml.Trim(), out ConfServiceID); // FB 2016
                        MCU.ConfServiceID = ConfServiceID; //FB 2016

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        // Now get the services and insert the info
                        xmlIPList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/IPServices/IPService");

                        ipService = null;
                        foreach (XmlNode innerNode in xmlIPList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string atype = innerElement.GetElementsByTagName("addressType")[0].InnerText;
                            string address = innerElement.GetElementsByTagName("address")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;
                            ipService = new vrmMCUIPServices();

                            ipService.SortID = Int32.Parse(id);
                            ipService.ServiceName = name;
                            ipService.addressType = Int32.Parse(atype);
                            ipService.ipAddress = address;
                            ipService.networkAccess = Int32.Parse(netaccess);
                            ipService.usage = Int32.Parse(usage);
                            ipService.bridgeId = MCU.BridgeID;
                            IPList.Add(ipService);
                        }
                        m_IipServices.SaveOrUpdateList(IPList);

                        #endregion

                        #region ISDN Services
                        // ISDN Services
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        ISDNList = m_IisdnServices.GetByCriteria(criterionList);
                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();

                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion

                        break;
                    case MCUType.MSE8000: //NGC fixes
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA");
                        string portMSEA = node.InnerXml.Trim();
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portB");
                        string portMSEB = node.InnerXml.Trim();
                        //FB 2003 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNAudioPref");
                        MCU.ISDNAudioPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNVideoPref");
                        MCU.ISDNVideoPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNGateway");
                        MCU.ISDNGateway = node.InnerXml.Trim();
                        //FB 2003 - End

                        //FB 2636 Starts
                        tmpE164Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/E164Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpE164Dialing);
                        tmpH323Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/H323Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpH323Dialing);

                        #region E.164 Services
                        E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        E164List = m_IE164Services.GetByCriteria(criterionList);
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();

                        xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");

                        MCU.EnhancedMCU = 0;
                        MCU.E164Dialing = 0;
                        MCU.H323Dialing = 0;
                        if (tmpE164Dialing > 0 || tmpH323Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.H323Dialing = tmpH323Dialing;
                                MCU.EnhancedMCU = 1;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        E164innerNode = null;
                        SortID = 0;
                        StartRange = ""; EndRange = "";
                        E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion

                        //FB 2636 Ends

                        MCU.BridgeAddress = portMSEA;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portMSEA;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = portMSEB;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        #endregion
                        break;

                    case MCUType.LIFESIZE: // lifesize bridge FB 2261
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        string portbridgeAddress = node.InnerXml.Trim();
                        MCU.BridgeAddress = portbridgeAddress;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        m_ImcuDao.SaveOrUpdate(MCU);
                      
                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portbridgeAddress;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = "";
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        #endregion

                        break;
                    //FB 2501 Call Monitoring Start
                    case MCUType.CISCO:
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA");
                        portA = node.InnerXml.Trim();
                        portB = ""; //FB 2718
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portB");
                        if (node != null) //FB 2718
                             portB = node.InnerXml.Trim();
                        //FB 2003 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNAudioPref");
                        if (node != null) //FB 2718
                            MCU.ISDNAudioPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNVideoPref");
                        if (node != null)             
                          MCU.ISDNVideoPrefix = node.InnerXml.Trim();
                    

                        MCU.BridgeAddress = portA;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends
							
                        //FB 2636 Starts
                        tmpE164Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/E164Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpE164Dialing);

                        tmpH323Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/H323Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpH323Dialing);

                        #region E.164 Services
                        E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        E164List = m_IE164Services.GetByCriteria(criterionList);
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();

                        xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");

                        MCU.EnhancedMCU = 0;
                        MCU.E164Dialing = 0;
                        MCU.H323Dialing = 0;
                        if (tmpE164Dialing > 0 || tmpH323Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.EnhancedMCU = 1;
                                MCU.H323Dialing = tmpH323Dialing;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        E164innerNode = null;
                        SortID = 0;
                        StartRange = ""; EndRange = "";
                        E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion

                        //FB 2718 Starts
                        isSynchronous = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous").InnerText.Trim(), out isSynchronous);

                        if (MCU.Synchronous == 1 && isSynchronous == 0)
                        {
                            string hql;
                            DataSet ds = null;

                            if (m_bridgelayer == null)
                                m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                            //hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            //hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ((getutcdate() between b.confdate and ";
                            //hql += " dateAdd(minute,b.duration,b.confdate)) or b.confdate > getutcdate() )";
                            //Doubt
                            hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ";
                            hql += " dateAdd(minute,b.duration,b.confdate) >= getutcdate() ";

                            if (hql != "")
                                ds = m_bridgelayer.ExecuteDataSet(hql);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                myVRMException myVRMEx = new myVRMException(665);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        MCU.Synchronous = isSynchronous;
                        
                        string MCUDomain = "";
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Domain");
                        if (node != null)
                            MCUDomain = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Domain").InnerText.Trim();
                        MCU.RPRMDomain = MCUDomain;
                        //FB 2718 Starts

                        //FB 2636 Ends

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portA;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = portB;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        #endregion

                        break;
                        //FB 2501 Call Monitoring End
                    //FB 2556 - Starts
                    case MCUType.iVIEW:
                        orgSpecificProps = new string[] { "ScopiaDesktopURL", "ScopiaOrgID", "ScopiaOrgLogin", "ScopiaServiceID" };//FB 2556-TDB
                        string portbridgeIp = "";
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        if (node != null)
                            portbridgeIp = node.InnerText.Trim();
                        MCU.BridgeAddress = portbridgeIp;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();
                        MCU.BridgeExtNo = BridgeExtNo;

                        int multiTenant = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Multitenant");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Multitenant").InnerText.Trim(), out multiTenant);
                        MCU.IsMultitenant = multiTenant;

                        isSynchronous = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous").InnerText.Trim(), out isSynchronous);
                        if (sysSettings.EnableCloudInstallation == 1)//FB 2659
                            isSynchronous = 1;
                        MCU.Synchronous = isSynchronous;

                        m_ImcuDao.SaveOrUpdate(MCU);
						//FB 2556-TDB
                        m_IMCUOrgSpecificDetailsDao = m_hardwareDAO.GetMCUOrgSpecificDetailsDao();
                        {
                            
                            osCriterionList = new List<ICriterion>();
                            osCriterionList.Add(Expression.Eq("bridgeID", MCU.BridgeID));
                            osCriterionList.Add(Expression.Eq("orgID", organizationID));

                            orgSpecificdetails = m_IMCUOrgSpecificDetailsDao.GetByCriteria(osCriterionList,true);

                            if (orgSpecificdetails.Count > 0)
                            {
                                for (cntDtls = 0; cntDtls < orgSpecificdetails.Count; cntDtls++)
                                    m_IMCUOrgSpecificDetailsDao.Delete(orgSpecificdetails[cntDtls]);

                            }
                            orgSpecificdetails = new List<vrmMCUOrgSpecificDetails>();
                            for (cntDtls = 0; cntDtls < orgSpecificProps.Length; cntDtls++)
                            {
                                node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/orgSpecificDetails/" + orgSpecificProps[cntDtls].Trim());
                                if (node != null)
                                {
                                    orgSpecificdetail = new vrmMCUOrgSpecificDetails();
                                    orgSpecificdetail.bridgeID = MCU.BridgeID;
                                    orgSpecificdetail.orgID = organizationID;
                                    orgSpecificdetail.propertyName = orgSpecificProps[cntDtls].Trim();
                                    orgSpecificdetail.propertyValue = node.InnerText.Trim();
                                    orgSpecificdetails.Add(orgSpecificdetail);
                                }

                                
                            }
                            if (orgSpecificdetails.Count > 0)
                                 m_IMCUOrgSpecificDetailsDao.SaveOrUpdateList(orgSpecificdetails);

                            

                        }

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portbridgeIp;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = "";
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        #endregion

                        #region ISDN Service
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        ISDNList = m_IisdnServices.GetByCriteria(criterionList);
                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();

                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion
                        break;
                    //FB 2556 - End
                }
                if (obj.outXml.Trim() == "") //FB 1920
                    obj.outXml = "<SetBridge><success>Operation successful</success><MCUID>" + MCU.BridgeID + "</MCUID></SetBridge>";//FB 2709

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }

        #endregion

        #region loadMCUInfo
        //Code added FO FB Issue 1171

        public bool loadMCUInfo(ref List<vrmMCU> mcuList, List<vrmConfRoom> confRoom, List<vrmConfUser> confUser)
        {
            try
            {
                Hashtable bId = new Hashtable();
                foreach (vrmConfRoom cr in confRoom)
                {
                    if (cr.bridgeid > 0)
                    {
                        if (!bId.ContainsKey(cr.bridgeid))
                            bId.Add(cr.bridgeid, cr.bridgeid);
                    }
                }
                foreach (vrmConfUser cu in confUser)
                {
                    if (cu.bridgeid > 0 && cu.invitee == 1) // external only
                    {
                        if (!bId.ContainsKey(cu.bridgeid))
                            bId.Add(cu.bridgeid, cu.bridgeid);
                    }
                }
                IDictionaryEnumerator iEnum = bId.GetEnumerator();
                while (iEnum.MoveNext())
                {
                    int bridgeId = (int)iEnum.Value;
                    vrmMCU MCU = m_ImcuDao.GetById(bridgeId);
                    mcuList.Add(MCU);
                }
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        

        //FB 2486 End

        //Code added FO FB Issue 1171
        #endregion

        /* *** New Method - FB 1462 start *** */

        #region Fetches the selected MCUs details
        /// <summary>
        /// Fetches the selected MCUs details
        /// </summary>
        /// <param name="bridgeIDs"></param>
        /// <returns></returns>
        public bool FetchBridgeInfo(ref vrmDataObject obj)
        {
            List<int> bridgeIDs = new List<int>();
            int bridgeId = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNodeList nodes = null;
                nodes = xd.SelectNodes("Bridges/ID");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        if (node.InnerText.Trim() != "")
                        {
                            bridgeId = 0;
                            Int32.TryParse(node.InnerText.Trim(), out bridgeId);
                            if (!bridgeIDs.Contains(bridgeId))
                            {
                                bridgeIDs.Add(bridgeId);
                            }
                        }
                    }
                }

                if (bridgeIDs.Count <= 0)
                {
                    obj.outXml = "<bridges></bridges>";
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("BridgeID", bridgeIDs));
                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionList);

                if (MCUList == null)
                {
                    obj.outXml = "<bridges></bridges>";
                    return false;
                }

                obj.outXml = "<bridges>";

                foreach (vrmMCU mcu in MCUList)
                {
                    obj.outXml += "<bridge>";
                    obj.outXml += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    obj.outXml += "<name>" + mcu.BridgeName + "</name>";
                    obj.outXml += "<interfaceType>" + mcu.BridgeTypeId.ToString() + "</interfaceType>";

                    vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);

                    if (user != null)
                    {
                        obj.outXml += "<administrator>" + user.FirstName + ", " + user.LastName + "</administrator>";
                        obj.outXml += "<userstate>A</userstate>";
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            obj.outXml += "<administrator>" + iuser.FirstName + ", " + iuser.LastName + "</administrator>";
                            obj.outXml += "<userstate>I</userstate>";
                        }
                        else
                        {
                            obj.outXml += "<administrator>User does not exist</administrator>";
                            obj.outXml += "<userstate>D</userstate>";
                        }
                    }
                    obj.outXml += "<exist>" + mcu.VirtualBridge.ToString() + "</exist>";
                    obj.outXml += "<status>" + mcu.Status.ToString() + "</status>";
                    obj.outXml += "<order>" + mcu.ChainPosition.ToString() + "</order>";
                    obj.outXml += "</bridge>";
                }
                obj.outXml += "</bridges>";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //FB 1881 start
                //obj.outXml = "<error>Error in fetching the bridge details</error>";
                m_log.Error("Error in fetching the bridge details");
                obj.outXml = "";
                //FB 1881 end
                return false;
            }
            return true;
        }
        #endregion

        /* *** New Method - FB 1462 end *** */

        // Method added for Endpoint Search    --- Start

        #region GetAllEndpoints
        /// <summary>
        /// Clone of GetEndpointDetails Command,Difference without endpoint id it will reaturn all endpoints.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetAllEndpoints(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            IList EPCnt = null; //FB 2594
            IList PublicEPCnt = null; //FB 2594
            StringBuilder searchOutXml = new StringBuilder(); // String concatenation changed to StringBuilder for Performance - FB 1820
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//EndpointDetails/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//EndpointDetails/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
               
                //FB 2274 Starts
                node = xd.SelectSingleNode("//EndpointDetails/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                //FB 2594 Starts
                int PublicEP = 0;
                node = xd.SelectSingleNode("//EndpointDetails/PublicEndpoint"); 
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out PublicEP);
                //FB 2594 Ends
                criterionList.Add(Expression.Eq("isDefault", 1));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));//Organization Module Fixes
                criterionList.Add(Expression.Eq("Extendpoint", 0));//FB 2426
                if(PublicEP ==0)
                    criterionList.Add(Expression.Eq("PublicEndPoint", 0));//FB 2594
                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);
                //FB 2594 Starts
                int endpointcnt = 0,PubEPCNT =0;
                string totalEP = "SELECT count(*) FROM myVRM.DataLayer.vrmEndPoint vo WHERE vo.deleted=0 and vo.isDefault=1 and vo.Extendpoint=0 and vo.PublicEndPoint = 0 and vo.orgId='" + organizationID.ToString() + "'";
                string totalPubEP = "SELECT count(*) FROM myVRM.DataLayer.vrmEndPoint vo WHERE vo.deleted=0 and vo.Extendpoint=0 and vo.PublicEndPoint = 1 and vo.isDefault=1  and vo.orgId='" + organizationID.ToString() + "'";
                EPCnt = m_IeptDao.execQuery(totalEP);
                PublicEPCnt = m_IeptDao.execQuery(totalPubEP);


                if (EPCnt != null)
                {
                    if (EPCnt.Count > 0)
                    {
                        if (EPCnt[0] != null)
                        {
                            if (EPCnt[0].ToString() != "")
                                Int32.TryParse(EPCnt[0].ToString(), out endpointcnt);
                        }
                    }
                }
                if (PublicEPCnt != null)
                {
                    if (PublicEPCnt.Count > 0)
                    {
                        if (PublicEPCnt[0] != null)
                        {
                            if (PublicEPCnt[0].ToString() != "")
                                Int32.TryParse(PublicEPCnt[0].ToString(), out PubEPCNT);
                        }
                    }
                }
                //FB 2594 Ends
                int MaxRecords = 0;
                //FB 1820- Starts
                searchOutXml.Append("<EndpointDetails>");
                int idx = 0;

                foreach (vrmEndPoint ep in epList)
                {
                    //if (idx == 0)
                    //{                   

                    searchOutXml.Append("<Endpoint>");
                    searchOutXml.Append("<EndpointID>" + ep.endpointid.ToString() + "</EndpointID>");

                    searchOutXml.Append("<EndpointName>" + ep.name + "</EndpointName>");

                    String roomnames = " ";                  
                    searchOutXml.Append("<RoomName>" + roomnames + "</RoomName>");

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("endpointid", ep.endpointid));//Organization Module Fixes

                    //FB 2361
                    List<vrmEndPoint> epList1 = m_IeptDao.GetByCriteria(criterionList);
                    searchOutXml.Append("<ProfileCount>" + epList1.Count + "</ProfileCount>"); 
                    
                    searchOutXml.Append("<DefaultProfileID>" +
                                             ep.profileId.ToString() + "</DefaultProfileID>");
                    searchOutXml.Append("<TotalRecords>" + endpointcnt + "</TotalRecords>");
                    searchOutXml.Append("<TotalPublicEPRecords>" + PubEPCNT + "</TotalPublicEPRecords>"); //FB 2594
                    //obj.outXml += "<Profiles>";
                    //    idx++;
                    //}
                    //obj.outXml += "<Profile>";                     
                    searchOutXml.Append("<ProfileID>" + ep.profileId.ToString() + "</ProfileID>");
                    searchOutXml.Append("<ProfileName>" + ep.profileName + "</ProfileName>");
                    searchOutXml.Append("<EncryptionPreferred>" +
                                                    ep.encrypted.ToString() + "</EncryptionPreferred>");
                    searchOutXml.Append("<AddressType>" + ep.addresstype.ToString() + "</AddressType>");
                    if (ep.password != null)
                        searchOutXml.Append("<Password>" + ep.password + "</Password> ");
                    else
                        searchOutXml.Append("<Password></Password> ");

                    searchOutXml.Append("<Address>" + ep.address + "</Address>");
                    searchOutXml.Append("<URL>" + ep.endptURL + "</URL>");
                    searchOutXml.Append("<IsOutside>" + ep.outsidenetwork.ToString() + "</IsOutside>");

                    if (ep.GateKeeeperAddress != null)//ZD 100132
                        searchOutXml.Append("<GateKeeeperAddress>" + ep.GateKeeeperAddress + "</GateKeeeperAddress>");
                    else
                        searchOutXml.Append("<GateKeeeperAddress></GateKeeeperAddress>");

                    
                    searchOutXml.Append("<VideoEquipment>" +
                                                    ep.videoequipmentid.ToString() + "</VideoEquipment> ");
                    
                    searchOutXml.Append("<LineRate>" + ep.linerateid.ToString() + "</LineRate>");
                    searchOutXml.Append("<Bridge>" + ep.bridgeid.ToString() + "</Bridge>");
                    searchOutXml.Append("<BridgeName></BridgeName>");
                    searchOutXml.Append("<ConnectionType>" + ep.connectiontype.ToString() + "</ConnectionType>");
                    searchOutXml.Append("<DefaultProtocol>" + ep.protocol.ToString() + "</DefaultProtocol>");
                    searchOutXml.Append("<MCUAddress>" + ep.MCUAddress + "</MCUAddress>");
                    searchOutXml.Append("<MCUAddressType>" + ep.MCUAddressType.ToString() + "</MCUAddressType>");
                    //Code Added For FB1422 -Start
                    searchOutXml.Append("<TelnetAPI>" + ep.TelnetAPI.ToString() + "</TelnetAPI>");
                    searchOutXml.Append("<RearSecCameraAddress>" + ep.RearSecCameraAddress + "</RearSecCameraAddress>");//FB 2400
                    //FB 2400 start
                    searchOutXml.Append("<MultiCodec>");
                    if (ep.MultiCodecAddress != null)
                    {
                        String[] multiCodec = ep.MultiCodecAddress.Split('�');
                        for (int i = 0; i < multiCodec.Length; i++)
                        {
                            if (multiCodec[i].Trim() != "")
                                searchOutXml.Append("<Address>" + multiCodec[i].Trim() + "</Address>");

                        }
                    }
                    searchOutXml.Append("</MultiCodec>");
                    searchOutXml.Append("<isTelePresence>" + ep.isTelePresence + "</isTelePresence>");
                    //FB 2400 end
                    //Code Added For FB1422 -End   
                    //obj.outXml += "</Profile>";
                    searchOutXml.Append("<Extendpoint>" + ep.Extendpoint.ToString() + "</Extendpoint>");//FB 2426
                    searchOutXml.Append("<Secured>" + ep.Secured.ToString() + "</Secured>");//FB 2595
                    searchOutXml.Append("<NetworkURL>" + ep.NetworkURL + "</NetworkURL>");//FB 2595
                    searchOutXml.Append("<Securedport>" + ep.Secureport + "</Securedport>");//FB 2595
                    searchOutXml.Append("<OrgId>" + ep.orgId + "</OrgId>"); //FB 2593
                    searchOutXml.Append("</Endpoint>");
                }

                searchOutXml.Append("</EndpointDetails>");
                obj.outXml = searchOutXml.ToString();
                //FB 1820 - End
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        //Method added for Endpoint Search    -- End

        //FB 1938
        #region GetCompleteMCUusageReport

        public bool GetCompleteMCUusageReport(ref vrmDataObject obj)
        {
            bool bRet = true;
            String audioOnly = "N/A";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetCompleteMCUusageReport/BridgeID");
                string BridgeID = node.InnerXml.Trim();
                DateTime startDate;
                node = xd.SelectSingleNode("//GetCompleteMCUusageReport/StartDate");
                startDate = DateTime.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//GetCompleteMCUusageReport/timezone");
                Int32 tzone = Int32.Parse(node.InnerXml.Trim());

                vrmMCUResources vMCU = new vrmMCUResources(Int32.Parse(BridgeID));
                vrmMCU MCU = m_ImcuDao.GetById(Int32.Parse(BridgeID));

                vMCU.bridgeType = MCU.MCUType.bridgeInterfaceId;

                List<vrmMCUResources> vMCUList = new List<vrmMCUResources>();
                loadVRMResourceData(MCU, ref vMCU);
                vMCUList.Add(vMCU);

                string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU,
                    vrmConfStatus.Completed);

                string hql;
                int Duration = 0;
                // select all conferences that fall into this date time range (with correct status and are not rooms or templates                
                hql = "SELECT c.confid, c.instanceid, c.externalname, c.confdate, c.duration, c.confnumname ";
                hql += " FROM myVRM.DataLayer.vrmConference c ";
                hql += " where (dateadd(second, 45,'" + startDate.ToString("d") + " " + startDate.ToString("t") + "')) between dbo.changeTime(" + tzone + ",confdate) ";
                hql += " and (dateadd(minute, duration,dbo.changeTime(" + tzone + ",confdate))) ";
                hql += "AND c.deleted = 0 AND c.status IN( " + status + ")";
                hql += "AND c.conftype != " + vrmConfType.RooomOnly;
                hql += "AND c.conftype != " + vrmConfType.Template;

                IList conferences = m_vrmConfDAO.execQuery(hql);
                // step thru each conference and count the ports used. 
                List<ICriterion> criterionList;
                obj.outXml = "<UsageReport>";

                foreach (object[] objConf in conferences)
                {
                    int sConfid = Int32.Parse(objConf[0].ToString());
                    int sInstcanceid = Int32.Parse(objConf[1].ToString());
                    String sExternalName = objConf[2].ToString();
                    DateTime sStartDate = DateTime.Parse(objConf[3].ToString());

                    sStartDate = sStartDate.AddSeconds(-45);
                    //timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                    timeZone.userPreferedTime(tzone, ref sStartDate);

                    DateTime sEndDate = startDate.AddMinutes(Int32.Parse(objConf[4].ToString()));
                    String sconfnumname = objConf[5].ToString();

                    vrmMCUResources vMCUs = new vrmMCUResources(Int32.Parse(BridgeID));
                    vMCUs.bridgeType = MCU.MCUType.bridgeInterfaceId;
                    loadVRMResourceData(MCU, ref vMCUs);

                    vrmConfAdvAvParams advAVParams = new vrmConfAdvAvParams();
                    vrmConference baseConf = m_vrmConfDAO.GetByConfId(sConfid, sInstcanceid);
                    vrmConfAdvAvParams AvParams = baseConf.ConfAdvAvParams;
                    int isSwitched = 0;
                    int isEncryption = 0;

                    if (AvParams.videoMode == 1)
                        isSwitched = 1;
                    if (AvParams.encryption == 1)
                        isEncryption = 1;

                    // first lookup confrooms
                    // now the users (only external)
                    // FB 4460 use the vMCU (virtual bridge alorithm for Polycomm mcu's
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", sConfid));
                    criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                    criterionList.Add(Expression.Eq("bridgeid", vMCUs.bridgeId));
                    List<vrmConfRoom> room = m_IconfRoom.GetByCriteria(criterionList);

                    foreach (vrmConfRoom rm in room)
                    {
                        bool notBypass = true;

                        // exclude current conference
                        if (notBypass)
                        {
                            switch (vMCU.bridgeType)
                            {
                                //FB 1937 & 2008
                                //case MCUType.POLYCOM:
                                //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                //           isEncryption, vMCU);
                                //    break;
                                case MCUType.MSE8000://FB 1937 & 2008
                                case MCUType.CODIAN:
                                    if (rm.audioorvideo == 1)
                                    {//FB 1744
                                        vMCU.AUDIO.used++;
                                        vMCUs.AUDIO.used++;
                                    }
                                    else
                                    {
                                        vMCU.VIDEO.used++;
                                        vMCUs.VIDEO.used++;
                                        //vMCU.AUDIO.used++; //FB 1937
                                    }
                                    break;
                                default:
                                    //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                    //    vMCU.AUDIO.used++;
                                    //else
                                    vMCU.VIDEO.used++;
                                    vMCUs.VIDEO.used++;
                                    vMCU.AUDIO.used = -1000;
                                    vMCUs.AUDIO.used = -1000;
                                    break;
                            }
                        }
                    }

                    // only external users (as they are using port)
                    criterionList.Add(Expression.Eq("invitee", vrmConfUserType.External));
                    criterionList.Add(Expression.Not(Expression.Eq("status", vrmConfUserStatus.Rejectetd)));
                    List<vrmConfUser> user = m_IconfUser.GetByCriteria(criterionList);
                    foreach (vrmConfUser us in user)
                    {
                        // if we are checking mcu resource back out the resources from the conference bridge.
                        // if we are settup up a conference back out the requested resources as they 
                        //     (irespecitve of bridge) will be added in later. 
                        bool notBypass = true;

                        if (notBypass)
                        {
                            switch (vMCU.bridgeType)
                            {
                                //FB 1937 & 2008
                                //case MCUType.POLYCOM:
                                //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                //           isEncryption, vMCU);
                                //    break;
                                case MCUType.MSE8000://FB 1937 & FB 2008
                                case MCUType.CODIAN:
                                    if (us.audioOrVideo == 1)
                                    {//FB 1744
                                        vMCU.AUDIO.used++;
                                        vMCUs.AUDIO.used++;
                                    }
                                    else
                                    {
                                        vMCU.VIDEO.used++;
                                        vMCUs.VIDEO.used++;
                                        //vMCU.AUDIO.used++; //FB 1937
                                    }
                                    break;
                                default:
                                    //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                    //    vMCU.AUDIO.used++;
                                    //else
                                    vMCU.VIDEO.used++;
                                    vMCUs.VIDEO.used++;
                                    vMCU.AUDIO.used = -1000;
                                    vMCUs.AUDIO.used = -1000;
                                    break;
                            }
                        }
                    }

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", sConfid));
                    criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                    criterionList.Add(Expression.Eq("bridgeId", vMCUs.bridgeId));
                    List<vrmConfCascade> cascade = m_IconfCascade.GetByCriteria(criterionList);

                    foreach (vrmConfCascade csde in cascade)
                    {
                        bool notBypass = true;

                        // exclude current conference
                        if (notBypass)
                        {
                            switch (vMCU.bridgeType)
                            {
                                //FB 1937 & 2008
                                //case MCUType.POLYCOM:
                                //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                //           isEncryption, vMCU);
                                //    break;
                                case MCUType.MSE8000://FB 1937 & 2008
                                case MCUType.CODIAN:
                                    if (csde.audioOrVideo == 1)
                                    {//FB 1744
                                        vMCU.AUDIO.used++;
                                        vMCUs.AUDIO.used++;
                                    }
                                    else
                                    {
                                        vMCU.VIDEO.used++;
                                        vMCUs.VIDEO.used++;
                                        //vMCU.AUDIO.used++; //FB 1937
                                    }
                                    break;
                                default:
                                    //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                    //    vMCU.AUDIO.used++;
                                    //else
                                    vMCU.VIDEO.used++;
                                    vMCUs.VIDEO.used++;
                                    vMCU.AUDIO.used = -1000;
                                    vMCUs.AUDIO.used = -1000;
                                    break;
                            }
                        }
                    }

                    if (vMCUs.VIDEO.used > 0 || vMCUs.AUDIO.used > 0)
                    {
                        obj.outXml += "<UsageDetails>";
                        obj.outXml += "<ConfName>" + sExternalName + "</ConfName>";
                        obj.outXml += "<ConfID>" + sconfnumname + "</ConfID>";
                        obj.outXml += "<StartDate>" + sStartDate.ToString() + "</StartDate>";
                        obj.outXml += "<EndDate>" + sEndDate.ToString() + "</EndDate>";
                        obj.outXml += "<AudioVideo>" + vMCUs.VIDEO.used + "</AudioVideo>";
                        if (vMCUs.AUDIO.used <= -1000)
                            obj.outXml += "<AudioOnly>N/A</AudioOnly>";
                        else
                            obj.outXml += "<AudioOnly>" + vMCUs.AUDIO.used + "</AudioOnly>";
                        obj.outXml += "</UsageDetails>";
                    }
                }
                obj.outXml += "<TotalVideoUsed>" + vMCUList[0].VIDEO.used + "</TotalVideoUsed>";
                obj.outXml += "<TotalVideoAvailable>" + vMCUList[0].VIDEO.available + "</TotalVideoAvailable>";
                if (vMCUList[0].bridgeType == MCUType.MSE8000 || vMCUList[0].bridgeType == MCUType.CODIAN)
                {
                    obj.outXml += "<TotalAudioUsed>" + vMCUList[0].AUDIO.used + "</TotalAudioUsed>";
                    obj.outXml += "<TotalAudioAvailable>" + vMCUList[0].AUDIO.available + "</TotalAudioAvailable>";
                }
                else
                {
                    obj.outXml += "<TotalAudioUsed>N/A</TotalAudioUsed>";
                    obj.outXml += "<TotalAudioAvailable>N/A</TotalAudioAvailable>";
                }




                obj.outXml += "</UsageReport>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //FB 2027 - Starts
        #region DeleteBridge
        /// <summary>
        /// DeleteBridge (COM to .Net conversion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool DeleteBridge(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionlist = new List<ICriterion>();
            List<ICriterion> criterionlist2 = new List<ICriterion>();
            StringBuilder OutXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userID = 0;
            int bridgeID = 0;
            List<ICriterion> criterionList = null; //ZD 100298
            List<vrmMCUProfiles> MCUProf = null; //ZD 100298
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/bridge/bridgeID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out bridgeID);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if ((userID <= 0) || (bridgeID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                criterionlist.Add(Expression.Eq("BridgeID", bridgeID));
                List<vrmUser> iUsers = m_vrmUserDAO.GetByCriteria(criterionlist);
               
                criterionlist2.Add(Expression.Eq("bridgeid", bridgeID));
                criterionlist2.Add(Expression.Eq("deleted", 0));
                List<vrmEndPoint> iEndPt = m_IeptDao.GetByCriteria(criterionlist2);
                
                if ((iUsers.Count == 0) && (iEndPt.Count == 0))
                {
                    vrmMCU mcu = null;
                    mcu = m_ImcuDao.GetById(bridgeID, true);
                    mcu.deleted = 1;
                    m_ImcuDao.SaveOrUpdate(mcu);
                    //ZD 100298 Starts
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("MCUId", bridgeID));
                    MCUProf = m_IMcuProfilesDAO.GetByCriteria(criterionList);
                    for (int i = 0; i < MCUProf.Count; i++)
                        m_IMcuProfilesDAO.Delete(MCUProf[i]);
                    //ZD 100298 Ends
                }
                else
                {
                    String message = "";
                    if (iUsers.Count > 0)
                    {
                        myVRMEx = new myVRMException(495);
                        message = myVRMEx.Message.Replace("{0}", iUsers.Count.ToString());
                    }

                    if (iEndPt.Count > 0)
                    {
                        myVRMEx = new myVRMException(496);
                        message += myVRMEx.Message.Replace("{0}", iEndPt.Count.ToString());
                    }

                    OutXml.Append("<error>");
                    OutXml.Append("<errorCode>495</errorCode>");
                    OutXml.Append("<message>" + message + "</message>");
                    OutXml.Append("<Description>" + message + "</Description>");
                    OutXml.Append("<level>E</level>");
                    OutXml.Append("</error>");
                    obj.outXml = OutXml.ToString();
                    return false;
                }
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetEndpoint
        /// <summary>
        /// GetEndpoint (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder OutXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userid = 0, epid = 0, bridgeCount = 0;
            String orgid = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out epid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if ((userid <= 0) || (epid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                OutXml.Append("<getEndpoint>");

                OutXml.Append(FetchAddressTypeList());
                GetVideoEquipment(ref obj);
                GetLineRate(ref obj);
                OutXml.Append(obj.outXml);
                OutXml.Append(FetchBridgeList(ref bridgeCount));

                criterionList.Add(Expression.Eq("endpointid", epid));
                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);
                vrmEndPoint ep = new vrmEndPoint();
                if (epList.Count > 0)
                    ep = epList[0];

                OutXml.Append("<endpoint>");
                OutXml.Append("<ID>" + ep.endpointid + "</ID>");
                OutXml.Append("<name>" + ep.name + "</name>");
                OutXml.Append("<password>" + ep.password + "</password>");
                OutXml.Append("<addressType>" + ep.addresstype + "</addressType>");
                OutXml.Append("<address>" + ep.address + "</address>");
                OutXml.Append("<isOutside>" + ep.outsidenetwork + "</isOutside>");
                OutXml.Append("<GateKeeeperAddress>" + ep.GateKeeeperAddress + "</GateKeeeperAddress>");//ZD 100132
                OutXml.Append("<connectionType>" + ep.connectiontype + "</connectionType>");
                OutXml.Append("<videoEquipment>" + ep.videoequipmentid + "</videoEquipment>");
                OutXml.Append("<lineRate>" + ep.linerateid + "</lineRate>");
                OutXml.Append("<bridge>" + ep.bridgeid + "</bridge>");
                OutXml.Append("<URL>" + ep.endptURL + "</URL>");
                OutXml.Append("<TelnetAPI>" + ep.TelnetAPI + "</TelnetAPI>");
                OutXml.Append("<ExchangeID>" + ep.ExchangeID + "</ExchangeID>");
                //FB 2400 start
                OutXml.Append("<MultiCodec>");
                if (ep.MultiCodecAddress != null)
                {
                    String[] multiCodec = ep.MultiCodecAddress.Split('�');
                    for (int i = 0; i < multiCodec.Length; i++)
                    {
                        if (multiCodec[i].Trim() != "")
                            OutXml.Append("<Address>" + multiCodec[i].Trim() + "</Address>");

                    }
                }
                OutXml.Append("</MultiCodec>");
                OutXml.Append("<isTelePresence>" + ep.isTelePresence + "</isTelePresence>");
                //FB 2400 end
                OutXml.Append("</endpoint>");
                OutXml.Append("</getEndpoint>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBridgeList
        /// <summary>
        /// SetBridgeList (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetBridgeList(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList blist = root.SelectNodes(@"/login/bridgeOrder/bridge");
                for (int i = 0; i < blist.Count; i++)
                {
                    int order = 0, bridgeID = 0;
                    node = blist[i].SelectSingleNode("order");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out order);
                    node = blist[i].SelectSingleNode("bridgeID");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out bridgeID);
                    vrmMCU mcu = null;
                    mcu = m_ImcuDao.GetById(bridgeID, true);
                    mcu.ChainPosition = order;
                    m_ImcuDao.SaveOrUpdate(mcu);
                }
                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2027(GetOldRoom)
        #region OldFetchBridgeList
        /// <summary>
        /// OldFetchBridgeList (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchBridgeList(int orgid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> critermcu = new List<ICriterion>();
            try
            {
                outXml.Append("<bridges>");
                critermcu.Add(Expression.Eq("VirtualBridge", 0));
                critermcu.Add(Expression.Eq("deleted", 0));
                critermcu.Add(Expression.Eq("orgId", orgid));
                List<vrmMCU> mculist = m_ImcuDao.GetByCriteria(critermcu);
                for (int i = 0; i < mculist.Count; i++)
                {
                    outXml.Append("<bridge>");
                    outXml.Append("<ID>" + mculist[i].BridgeID.ToString() + "</ID>");
                    outXml.Append("<name>" + mculist[i].BridgeName + "</name>");
                    outXml.Append("</bridge>");
                }
                outXml.Append("</bridges>");
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("OldFetchBridgeList :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        //FB 2027(GetOldRoom)
        #region FetchEndpointList
        /// <summary>
        /// FetchEndpointList (COM to .Net conversion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchEndpointList(int orgid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> criterept = new List<ICriterion>();
            try
            {
                outXml.Append("<endpoints>");
                m_IeptDao.addOrderBy(Order.Asc("name"));
                criterept.Add(Expression.Eq("deleted", 0));
                criterept.Add(Expression.Eq("isDefault", 1));
                criterept.Add(Expression.Eq("orgId", orgid));
                List<vrmEndPoint> EndPointList = m_IeptDao.GetByCriteria(criterept);
                m_IeptDao.clearOrderBy();
                for (int a = 0; a < EndPointList.Count; a++)
                {
                    outXml.Append("<endpoint>");
                    outXml.Append("<ID>" + EndPointList[a].endpointid.ToString() + "</ID>");
                    outXml.Append("<name>" + EndPointList[a].name + "</name>");
                    outXml.Append("<protocol>" + EndPointList[a].protocol.ToString() + "</protocol>");
                    outXml.Append("<connectionType>" + EndPointList[a].connectiontype.ToString() + "</connectionType>");
                    outXml.Append("<addressTypeID>" + EndPointList[a].addresstype.ToString() + "</addressTypeID>");
                    outXml.Append("<address>" + EndPointList[a].address + "</address>");
                    outXml.Append("</endpoint>");
                }
                outXml.Append("</endpoints>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchEndpointList :" + ex.Message);
                throw ex;
            }
            return true;
        }
        #endregion
        //FB 2027 - End
        
        //FB 2426 Start

        #region SetGuesttoNormalEndpoint
        /// <summary>
        /// SetGuesttoNormalEndpoint
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetGuesttoNormalEndpoint(int endpointid, ref vrmDataObject obj)
        {
            vrmEndPoint ept = null;
            List<ICriterion> Licencechk = new List<ICriterion>();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionLst = new List<ICriterion>();
            try
            {
                ept =  m_IeptDao.GetByEptId(endpointid);
                Licencechk.Add(Expression.Eq("deleted", 0));
                Licencechk.Add(Expression.Eq("isDefault", 1));
                Licencechk.Add(Expression.Eq("orgId", ept.orgId));
                Licencechk.Add(Expression.Eq("Extendpoint", 0));
                List<vrmEndPoint> checkEptCount = m_IeptDao.GetByCriteria(Licencechk);

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(ept.orgId);

                if (checkEptCount.Count >= orgdt.MaxEndpoints)
                {
                    myVRMException ex = new myVRMException(458);
                    m_log.Error("Error EndPoint Limit exceeeded: ", ex);
                    obj.outXml = ex.FetchErrorMsg();
                    return false;
                }

                criterionList.Add(Expression.Eq("endpointid", endpointid));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("Extendpoint", 1));
                criterionList.Add(Expression.Eq("orgId", ept.orgId));
                List<vrmEndPoint> checkendpoint = m_IeptDao.GetByCriteria(criterionList, true);
                if (checkendpoint.Count > 0)
                {
                    criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("name", checkendpoint[0].name));
                    criterionLst.Add(Expression.Not(Expression.Eq("endpointid", endpointid)));
                    criterionLst.Add(Expression.Eq("Extendpoint", 0));
                    List<vrmEndPoint> EptChkList = m_IeptDao.GetByCriteria(criterionLst);
                    if (EptChkList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(434);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }

                ept.Extendpoint = 0;
                m_IeptDao.Update(ept);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region DeleteGuestEndpoint
        /// <summary>
        /// DeleteGuestEndpoint
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteGuestEndpoint(int endpointid)
        {
            vrmEndPoint ept = null;
            try
            {
                ept = m_IeptDao.GetByEptId(endpointid);
                ept.deleted = 1;
                m_IeptDao.Update(ept);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        //FB 2426 End

        //FB 2486 - Start
        #region GetAllMessage

        public bool GetAllMessage(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                StringBuilder outputXML =new StringBuilder();

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                m_msgDAO.addOrderBy(Order.Asc("msgId"));
                m_msgDAO.addOrderBy(Order.Asc("ID"));
                m_msgDAO.addOrderBy(Order.Asc("Languageid"));
                List<vrmMessage> msgList = m_msgDAO.GetByCriteria(criterionList);

                outputXML.Append("<GetAllMessage><MessageList>");

                List<vrmLanguage> lang = m_ILanguageDAO.GetAll();
                Hashtable langTable = new Hashtable();
                for (int l = 0; l < lang.Count; l++)
                    langTable.Add(lang[l].Id, lang[l].Name);

                for (int i = 0; i < msgList.Count; i++)
                {
                    outputXML.Append("<Message>");
                    outputXML.Append("<ID>" + msgList[i].ID.ToString() + "</ID>");
                    outputXML.Append("<TxtMsg>" + msgList[i].Message.ToString() + "</TxtMsg>");
                    outputXML.Append("<LangID>" + msgList[i].Languageid + "</LangID>");
                    outputXML.Append("<LangName>" + langTable[msgList[i].Languageid].ToString() + "</LangName>");

                    //vrmLanguage language = m_ILanguageDAO.GetLanguageById(msgList[i].LangId);
                    //if(language != null)
                    //    outputXML.Append("<LangName>" + language.Name + "</LangName>");

                    outputXML.Append("<Type>" + msgList[i].Type.ToString() + "</Type>");
                    outputXML.Append("<MsgID>" + msgList[i].msgId.ToString() + "</MsgID>");
                    outputXML.Append("</Message>");
                }
                outputXML.Append("</MessageList></GetAllMessage>");
                obj.outXml = outputXML.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("GetAllMessage", e);
                return false;
            }
        }

        #endregion

        #region SetMessage

        public bool SetMessage(ref vrmDataObject obj)
        {
            vrmMessage vrmMsg = new vrmMessage();
            List<vrmMessage> MsgList = new List<vrmMessage>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                XmlNodeList nodelist;
                int MaxId = 0;

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                Int32 msgID = 0;
                Int32 uID = 0;

                String msg = "";
                int Langid =1;
                nodelist = xd.SelectNodes("//login/MessageList/Message");
                if (nodelist != null)
                {
                    MsgList = new List<vrmMessage>();
                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        vrmMsg = null;
                        vrmMsg = new vrmMessage();

                        if (nodelist[i].SelectSingleNode("ID") != null)
                            Int32.TryParse(nodelist[i].SelectSingleNode("ID").InnerText.Trim(), out uID);

                        if (nodelist[i].SelectSingleNode("LangID") != null)
                            Int32.TryParse(nodelist[i].SelectSingleNode("LangID").InnerText.Trim(), out Langid);

                        if (nodelist[i].SelectSingleNode("TxtMsg") != null)
                            msg = nodelist[i].SelectSingleNode("TxtMsg").InnerText.Trim();

                        if (uID == 0)
                        {
                            vrmMsg.Languageid = Langid;
                            vrmMsg.Message = msg;
                            vrmMsg.orgId = organizationID;
                            vrmMsg.Type = 0;
                            if (msgID == 0)
                            {
                                string MsgID = "SELECT max(vm.msgId) FROM myVRM.DataLayer.vrmMessage vm";
                                IList result = m_vrmConfDAO.execQuery(MsgID);

                                if (result[0] != null)
                                    int.TryParse(result[0].ToString(), out MaxId);

                                MaxId = MaxId + 1;
                                vrmMsg.msgId = MaxId;
                            }
                            else
                                vrmMsg.msgId = msgID;

                            MsgList.Add(vrmMsg);
                        }
                        else
                        {
                            vrmMsg = m_msgDAO.GetById(uID);
                            vrmMsg.Message = msg;
                            m_msgDAO.SaveOrUpdate(vrmMsg);
                        }
                    }

                    if(uID == 0)                    
                        m_msgDAO.SaveOrUpdateList(MsgList);
                }
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region DeleteMessage

        public bool DeleteMessage(ref vrmDataObject obj)
        {
            vrmMessage vrmMsg = new vrmMessage();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                Int32 msgID = 0;
                node = xd.SelectSingleNode("//login/MsgID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out msgID);

                if (msgID > 0)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("msgId", msgID));
                    List<vrmMessage> msgList = m_msgDAO.GetByCriteria(criterionList);

                    foreach (vrmMessage msg in msgList)
                        m_msgDAO.Delete(msg);
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        //FB 2486 - End

        //FB 2501 - Call Monitoring
        #region SetFavouriteMCU
        /// <summary>
        /// SetFavouriteMCU
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetFavouriteMCU(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlNode node = null;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                int setFavourite = 0, bridgeId = 0, userid = 0;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/bridgeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out bridgeId);
                else
                {
                    myVRMEx = new myVRMException(531);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                
                node = xd.SelectSingleNode("//login/favouriteMCU");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out setFavourite);

                criterionList.Add(Expression.Eq("BridgeID", bridgeId));
                //criterionList.Add(Expression.Eq("orgId", organizationID)); //FB 2646

                List<vrmMCU> McuList = m_ImcuDao.GetByCriteria(criterionList,true);
                for (int i = 0; i < McuList.Count; i++)
                {
                    McuList[i].setFavourite = setFavourite;
                    m_ImcuDao.SaveOrUpdate(McuList[i]);
                }
                obj.outXml = "<success>1</success>";

            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                bRet = false;
            }
            return bRet;
        }

        #endregion
        //FB 2501 - Call Monitoring 

        //FB 2591 Start 
        #region
        public bool GetMCUProfiles(ref vrmDataObject obj)
        {
            StringBuilder OutXml = new StringBuilder();
            List<vrmMCUProfiles> MCUProfiles = new List<vrmMCUProfiles>();

            List<ICriterion> criterionList = null;
            List<vrmMCUProfiles> lstProfileName = null;//FB 2947
            int MCUId = 0;
            vrmMCU MCU = null; //FB 2947
            try
            {

                
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetMCUProfiles/MCUId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out MCUId);

                              
                OutXml.Append("<GetMCUProfiles>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("MCUId", MCUId));
                if (MCUId > 0) //FB 2947
                {
                    lstProfileName = m_IMcuProfilesDAO.GetByCriteria(criterionList);
                    m_IMcuProfilesDAO.clearOrderBy();
                    MCU = m_ImcuDao.GetById(MCUId); //FB 2947
                    OutXml.Append("<DefaultProfileID>" + MCU.ConfServiceID.ToString() + "</DefaultProfileID>"); //FB 2947
                    if (lstProfileName.Count > 0) 
                    {
                        for (int i = 0; i < lstProfileName.Count; i++)
                        {
                            OutXml.Append("<Profile>");
                            OutXml.Append("<Id>" + lstProfileName[i].ProfileId.ToString() + "</Id>");
                            OutXml.Append("<Name>" + lstProfileName[i].ProfileName + "</Name>");
                            OutXml.Append("</Profile>");
                        }
                    }
                }
                OutXml.Append("</GetMCUProfiles>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;

        }
        #endregion
        //FB 2591 End 


        //FB 2556 - Starts
        #region GetExtMCUServices
        /// <summary>
        /// GetExtMCUServices
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetExtMCUServices(ref vrmDataObject obj)
        {
            StringBuilder OutXml = new StringBuilder();
            List<vrmExtMCUService> MCUServiceList = new List<vrmExtMCUService>();
            List<ICriterion> criterionList = null;
            int bridgeTypeId = 0;

            try
            {
               
                m_IExtMCUService = m_hardwareDAO.GetExtMCUServiceDao(); 
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetExtMCUService/BridgeTypeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out bridgeTypeId);

                int BridgeId = 0;
                node = xd.SelectSingleNode("//GetExtMCUService/BridgeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out BridgeId);

                OutXml.Append("<GetExtMCUServices>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeId", BridgeId));

                MCUServiceList = m_IExtMCUService.GetByCriteria(criterionList);
                m_IMcuProfilesDAO.clearOrderBy();

                if (MCUServiceList.Count > 0)
                {
                    for (int i = 0; i < MCUServiceList.Count; i++)
                    {

                        OutXml.Append("<GetExtMCUService>");
                         OutXml.Append("<ID>" + MCUServiceList[i].MemberId + "</ID>");
                        OutXml.Append("<name>" + MCUServiceList[i].ServiceName + "</name>");
                        OutXml.Append("</GetExtMCUService>");
                    }
                }

                OutXml.Append("</GetExtMCUServices>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;

        }
        #endregion

        #region GetExtMCUSilo
        /// <summary>
        /// GetExtMCUSilo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetExtMCUSilo(ref vrmDataObject obj)
        {
            StringBuilder OutXml = new StringBuilder();
            List<vrmExtMCUSilo> MCUSiloList = new List<vrmExtMCUSilo>();
            List<ICriterion> criterionList = null;
            int bridgeTypeId = 0;

            try
            {
                m_IExtMCUSilo = m_hardwareDAO.GetExtMCUSiloDao();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetExtMCUSilo/BridgeTypeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out bridgeTypeId);

                int BridgeId = 0;
                node = xd.SelectSingleNode("//GetExtMCUSilo/BridgeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out BridgeId);

                
                OutXml.Append("<GetExtMCUSilos>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeId", BridgeId));

                MCUSiloList = m_IExtMCUSilo.GetByCriteria(criterionList);
                m_IMcuProfilesDAO.clearOrderBy();

                if (MCUSiloList.Count > 0)
                {
                    for (int i = 0; i < MCUSiloList.Count; i++)
                    {
                        OutXml.Append("<GetExtMCUSilo>");
                        OutXml.Append("<ID>" + MCUSiloList[i].MemberId + "</ID>");
                        OutXml.Append("<name>" + MCUSiloList[i].Name + "</name>");
                        OutXml.Append("</GetExtMCUSilo>");
                    }
                }

                OutXml.Append("</GetExtMCUSilos>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;

        }
        #endregion
        //FB 2556 - End

    }
}
