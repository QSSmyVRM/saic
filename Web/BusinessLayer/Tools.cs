//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;

namespace Frameworx
{

    /// <summary>
    /// Generic helper functions
    /// </summary>
    public class Tools
    {
        /// <summary>
        /// Converts Integers to enum types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">Enum int value</param>
        /// <returns></returns>
        /// <example>
        /// Enums.ConvertToEnum<enum.type>([EnumAsInt]);
        /// </example>
        public static T ConvertToEnum<T>(int value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }


        /// <summary>
        /// Converts String to enum types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">Enum string value</param>
        /// <returns></returns>
        /// <example>
        /// Enums.ConvertToEnum<enum.type>([EnumAsString]);
        /// </example>
        public static T ConvertToEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}