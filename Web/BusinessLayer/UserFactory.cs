//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using NHibernate;
using NHibernate.Criterion;

using log4net;

using myVRM.DataLayer;

using System.Data; 

namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Data Layer Logic for loading/saving Reports(Templates/Schedule)
    /// </summary>
    /// 

    #region UserFactory

    public class UserFactory
    {
        #region Private Members

        private static int m_PAGE_SIZE = 20;

        private static int m_isUser = 1;
        private static int m_isLDAP = 2;
        private static int m_isGustUser = 3;//FB 2027
        private static int m_isInActiveUser = 4;//FB 2027
        private static int CREATENEW = 0;  // create a new user FB 2027S
        private static int MODIFYOLD = 1;  // modify an existing user FB 2027S
        private static int TOTALTIMEFORNEWUSER = 100000; // FB 2027S

        private userDAO m_userDAO;
        private deptDAO m_deptDAO;
		private GeneralDAO m_generalDAO; //NewLobby
        private conferenceDAO m_confDAO; //FB 2027
        //FB 2027 Start
        private ILanguageDAO m_langDAO;
        private IDeptDao m_IdeptDAO;
        private hardwareDAO m_hardwareDAO;
        private IUserLotusDao m_userlotusDAO;
        private IMCUDao m_ImcuDao;
        private SystemDAO m_systemDAO;//FB 2027
        private ISystemDAO m_IsystemDAO;//FB 2027
        private IEptDao m_IeptDao;
        //FB 2027 End
        private IUserDao m_IuserDao;
        private IGuestUserDao m_IGuestUserDao;
        private IInactiveUserDao m_IinactiveUserDao;
        private IVRMLdapUserDao m_IldapDao;
        private IUserRolesDao m_IUserRolesDao;
        private IUserDeptDao m_IuserDeptDAO;
        private IUserTemplateDao m_ItempDao;
        private IGrpDetailDao m_IGrpDetailDao;
        private IGrpParticipantsDao m_IGrpParticipantsDao;
        private IUserLobbyDAO m_IUserLobbyDao; //NewLobby
        private IIconsRefDAO m_IIconsRefDAO;//NewLobby
        private IConfUserDAO m_IConfUserDAO; //FB 2027
        private IConferenceDAO m_IConferenceDAO; //FB 2027
        
        private myVRMSearch m_Search;
        private static log4net.ILog m_log;
        private string m_configPath;

        private int m_iMaxRecords;
        private int m_userType;
        private int isAudioAddOn = 2; //FB 2023

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private OrgData orgInfo;
        private const int defaultOrgId = 11;  //Default organization
        private int organizationID = 0;
        private int multiDepts = 1;
        private emailFactory m_emailFactory;// FB 1860
        private myVRMException myvrmEx; //FB 1881
        private ns_SqlHelper.SqlHelper m_rptLayer = null; //FB 1497

		//FB 1959 - Start
        private IRoomDAO m_IRoomDAO; 
        private LocationDAO m_locDAO;        
        //FB 1959 - End
        private ILanguageTextDao m_ILanguagetext;//FB 1830 - Translation
        //FB 2027 - Starts
        private ILocApprovDAO m_ILocApprovDAO;
        private ISysApproverDAO m_ISysApproverDAO;
        private IAccountGroupListDAO m_IGroupAccountDAO;
        private ITempUserDAO m_TempUserDAO;
        private IMCUApproverDao m_IMCUapprover;
        private IUserAccountDao m_IuserAccountDAO;
        private IOrgDAO m_IOrgDAO;
        private ISysTechDAO m_ISysTechDAO;
		private IEmailLanguageDao m_IEmailLanguageDAO;
        //FB 2027 - End
        private IUserPCDAO m_IUserPCDAO; //FB 2693 
        private UtilFactory m_UtilFactory; //FB 2721

        // ZD 100157 Start  
        XmlWriter xWriter = null;
        XmlWriterSettings xSettings = null;
        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        StringBuilder OUTXML = null;
        // ZD 100157 End

        private string macID = "";//ZD 100263
        cryptography.Crypto crypto = null; //ZD 100263
        #endregion

        #region User Factory
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        /// 
        public enum clientFrom {None =0, Exchange = 1, Lotus, AppleDevices, AndroidDevices}; //FB 1899, 1900

        public UserFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;

                m_userDAO = new userDAO(obj.ConfigPath, obj.log);
                m_deptDAO = new deptDAO(obj.ConfigPath, obj.log);
                m_Search = new myVRMSearch(obj.ConfigPath, obj.log);
                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log); //FB 2027
				m_generalDAO = new GeneralDAO(m_configPath, m_log); //NewLobby

                m_IuserDao = m_userDAO.GetUserDao();
                m_IGuestUserDao = m_userDAO.GetGuestUserDao();
                m_IinactiveUserDao = m_userDAO.GetInactiveUserDao();
                m_IldapDao = m_userDAO.GetLDAPUserDao();
                m_IUserRolesDao = m_userDAO.GetUserRolesDao();
                m_ItempDao = m_userDAO.GetUserTemplateDao();
                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao();
                m_IGrpDetailDao = m_userDAO.GetGrpDetailDao();
                m_IGrpParticipantsDao = m_userDAO.GetGrpParticipantDAO();
                m_IConferenceDAO = m_confDAO.GetConferenceDao(); //FB 2027
                m_IConfUserDAO = m_confDAO.GetConfUserDao(); //FB 2027

                m_OrgDAO = new orgDAO(obj.ConfigPath, obj.log); //Organization Module Fixes
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();

                m_userType = vrmUserConstant.TYPE_USER;
                m_iMaxRecords = 10;                

                //FB 1959 - Start
                m_locDAO = new LocationDAO(m_configPath, obj.log);
                m_IRoomDAO = m_locDAO.GetRoomDAO();
                m_IuserDao = m_userDAO.GetUserDao();
                //FB 1959 - End
				m_ILanguagetext = m_userDAO.GetLanguageTextDao();//FB 1830 - Translation

                m_IUserLobbyDao = m_userDAO.GetUserLobbyIconsDAO(); //NewLobby
                m_IIconsRefDAO = m_generalDAO.GetIconRefDAO(); //NewLobby
                //FB 2027 Start
                m_generalDAO = new GeneralDAO(m_configPath, m_log);
                m_langDAO = m_generalDAO.GetLanguageDAO();
                m_IdeptDAO = m_deptDAO.GetDeptDao();
                m_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);
                m_ImcuDao = m_hardwareDAO.GetMCUDao();
                m_userlotusDAO = m_userDAO.GetUserLotusDao();
                m_ILocApprovDAO = m_locDAO.GetLocApprovDAO(); 
                m_ISysApproverDAO = m_OrgDAO.GetSysApproverDao();
                m_IGroupAccountDAO = m_userDAO.GetGroupAccountDao();
                m_IMCUapprover = m_hardwareDAO.GetMCUApproverDao();
                m_IuserAccountDAO = m_userDAO.GetUserAccountDao();
                m_TempUserDAO = m_confDAO.GetTempUserDAO();
                m_systemDAO = new SystemDAO(m_configPath, m_log);//FB 2027
                m_IsystemDAO = m_systemDAO.GetSystemDao();//FB 2027
				m_IOrgDAO = m_OrgDAO.GetOrgDao();
                m_ISysTechDAO = m_OrgDAO.GetSysTechDao();
				m_IeptDao = m_hardwareDAO.GetEptDao();
				m_confDAO = new conferenceDAO(m_configPath, m_log);
                m_IEmailLanguageDAO = m_confDAO.GetEmailLanguageDao();
                //FB 2027 End
                m_IUserPCDAO = m_userDAO.GetUserPCDAO(); //FB 2693
                m_UtilFactory = new UtilFactory(ref obj); //FB 2721
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        public UserFactory()
        {
            //Default Constructor
        }

        #endregion

        #region GetMultipleUsers
        public bool GetMultipleUsers(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/alphabet");
                string alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                string pageno = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/sortBy");
                string field = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/searchFor");
                string searchFor = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                if(organizationID < 11)
                    organizationID = defaultOrgId;

                if (field == "1")
                    field = "FirstName";
                else if (field == "3")
                    field = "Login";
                else if (field == "4")
                    field = "Email";
                else
                    field = "LastName"; // Also default

                obj.outXml = "<getMultipleUsers>";

                List<vrmUserTemplate> templateList = new List<vrmUserTemplate>();
                List<vrmLDAPUser> userList = new List<vrmLDAPUser>();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("companyId", organizationID));//Organization Module Fixes
                criterionList.Add(Expression.Eq("deleted", 0)); //Fixed during Organization Module

                templateList = m_ItempDao.GetByCriteria(criterionList);

                obj.outXml += "<templates>";
                if (templateList.Count > 0)
                {
                    foreach (vrmUserTemplate temp in templateList)
                    {
                        obj.outXml += "<template>";
                        obj.outXml += "<ID>" + temp.id.ToString() + "</ID>";
                        obj.outXml += "<name>" + temp.name + "</name>";
                        obj.outXml += "<role>" + temp.roleId.ToString() + "</role>";
                        obj.outXml += "<language>" + temp.languageId.ToString() + "</language>";
                        obj.outXml += "</template>";
                    }
                } 
                obj.outXml += "</templates>";
                m_userDAO.pageSize = 20;
                m_userDAO.pageNo = Int32.Parse(pageno);
                m_userDAO.pageEnabled = true;

                int MaxRecords = m_userDAO.pageSize;
                int MaxSearchRecords = 0;

                obj.outXml += "<users>";
                if (!GetLDAPUserList(userID, alphabet, field, searchFor, ref MaxRecords, ref MaxSearchRecords,  ref userList))
                {
                    m_log.Error("Error in get user list");
                    obj.outXml = "";
                    return false;
                }

                foreach (vrmLDAPUser lUser in userList)
                {
                    obj.outXml += "<user>";
                    obj.outXml += "<userID>" + lUser.userid.ToString() + "</userID>";
                    obj.outXml += "<firstName>" + lUser.FirstName + "</firstName>";
                    obj.outXml += "<lastName>" + lUser.LastName + "</lastName>";
                    obj.outXml += "<login>" + lUser.Login + "</login>";
                    obj.outXml += "<email>" + lUser.Email + "</email>";
                    obj.outXml += "<telephone>" + lUser.Telephone + "</telephone>";
                    obj.outXml += "</user>";
                }

                obj.outXml += "</users>";

                int iMaxPage = 0;

                iMaxPage = MaxSearchRecords / m_userDAO.pageSize;

                // and modulus remainder...
                if (MaxSearchRecords % m_userDAO.pageSize > 0)
                    iMaxPage++;
                if (iMaxPage == 0)
                    iMaxPage = 1;

                obj.outXml += "<pageNo>" + pageno + "</pageNo>";
                obj.outXml += "<totalPages>" + string.Format("{0:d}", iMaxPage) + "</totalPages>";
                obj.outXml += "<totalNumber>" + string.Format("{0:d}", MaxRecords) + "</totalNumber>";
                
                obj.outXml += "</getMultipleUsers>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region DeleteLDAPUser
        public bool DeleteLDAPUser(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/LDAPUserID");
                string LDAPID = node.InnerXml.Trim();      

                vrmLDAPUser user = new vrmLDAPUser();
                user.userid = Int32.Parse(LDAPID);
                m_IldapDao.Delete(user);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region SetMultipleUsers
        public bool SetMultipleUsers(ref vrmDataObject obj)
        {
            bool bRet = true;
            cryptography.Crypto cPwd = null; //ZD 100263
            ArrayList arrDuplicateUsr = new ArrayList(); //Added for LDAP 
            try
            {

                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/userTemplateID");
                string userTemplateID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID");
                organizationID = Int32.Parse(node.InnerXml.Trim());

                XmlElement root = xd.DocumentElement;
                XmlNodeList elemList = root.SelectNodes(@"/login/users/userID");
                ArrayList ldapID = new ArrayList();

                //Edited for LDAP Browser START
                XmlNodeList usrList = root.SelectNodes(@"/login/users/user");

                foreach (XmlNode xlNde in usrList)
                {
                    node = xlNde.SelectSingleNode("FirstName");
                    string firstName = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("LastName");
                    string lastName = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("Login");
                    string loginName = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("Email");
                    string emailId = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("Telephone");
                    string telephone = node.InnerXml.Trim();

                    //New LDAP Design
                    string sPwd = "";
                    if (xlNde.SelectSingleNode("Password") != null)
                    {
                        node = xlNde.SelectSingleNode("Password");
                        cPwd = new cryptography.Crypto();

                        sPwd = cPwd.encrypt(node.InnerXml.Trim());
                    }
                    //Edited for LDAP Browser END

                    //START

                    //New LDAP Design Start
                    vrmUserTemplate template = new vrmUserTemplate();
                    vrmUserRoles roles = new vrmUserRoles();

                    template = m_ItempDao.GetById(Int32.Parse(userTemplateID));
                    organizationID = template.companyId;
                    //New LDAP Design End
                    List<ICriterion> criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", organizationID));//Organization Module Fixes
                    criUsrList.Add(Expression.Eq("Deleted", 0)); //Fixed during Organization Module

                    List<vrmUser> results = m_IuserDao.GetByCriteria(criUsrList);

                    int userCount = 0;

                    userCount = results.Count;
                    //m_userDAO.GetUserCount(ref userCount, organizationID);
                    //END      
                    DateTime accountExpiry = sysSettings.ExpiryDate;

                    //New LDAP Design Start
                    //vrmUserTemplate template = new vrmUserTemplate();
                    //vrmUserRoles roles = new vrmUserRoles();

                    //template = m_ItempDao.GetById(Int32.Parse(userTemplateID));
                    //New LDAP Design End
                    if (template.id == 0)
                    {
                        myvrmEx = new myVRMException(476);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    roles = m_IUserRolesDao.GetById(template.roleId);
                    if (roles.roleID == 0)
                    {
                        myvrmEx = new myVRMException(477);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    if (orgdt.UserLimit >= usrList.Count + userCount)
                    //if (sysSettings.UserLimit >= elemList.Count + userCount)
                    {

                        for (int i = 0; i < elemList.Count; i++)
                        {
                            node = elemList[i];
                            string sID = node.InnerXml;
                            ldapID.Add(Int32.Parse(sID));
                        }
                        List<vrmLDAPUser> LDAPRecords = new List<vrmLDAPUser>();
                        List<vrmUser> UserRecords = new List<vrmUser>();

                        List<ICriterion> criterionList = new List<ICriterion>();

                        int epid = 0;
                        hardwareDAO m_hardwareDAO = new hardwareDAO(m_configPath, m_log);
                        IEptDao eptDao = m_hardwareDAO.GetEptDao();

                        //Edited for LDAP Browser START
                        vrmLDAPUser vrmLDAP = new vrmLDAPUser();
                        vrmLDAP.FirstName = firstName;
                        vrmLDAP.LastName = lastName;
                        vrmLDAP.Email = emailId;
                        vrmLDAP.Login = loginName;
                        vrmLDAP.Telephone = telephone;
                        LDAPRecords.Add(vrmLDAP);
                        //Edited for LDAP Browser END

                        //if (m_userDAO.GetMultipleLDAP(ldapID, LDAPRecords))//Edited for LDAP Browser
                        //{

                        int id = 0;
                        foreach (vrmLDAPUser ldap in LDAPRecords)
                        {
                            vrmUser user = new vrmUser();

                            user.LastLogin = DateTime.Now;

                            user.userid = 0;
                            user.PreferedRoom = template.location;
                            user.DefLineRate = template.lineRateId;
                            user.DefVideoProtocol = template.videoProtocol;
                            user.connectionType = template.connectiontype;
                            user.IPISDNAddress = template.ipisdnaddress;
                            user.initialTime = template.initialtime;
                            user.Language = template.languageId;
                            user.BridgeID = template.bridgeId;
                            user.roleID = template.roleId;
                            user.emailmask = template.emailNotification;// FB 1592
                            //ZD 100318
                            user.PerCalStartTime = DateTime.Today;
                            user.PerCalEndTime = DateTime.Today;
                            user.RoomCalStartTime = DateTime.Today;
                            user.RoomCalEndime = DateTime.Today;
                            //ZD 100318
                            if (template.expirationDate <= DateTime.Today)//New LDAP Design
                            {
                                myvrmEx = new myVRMException(429);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                            else
                                user.accountexpiry = template.expirationDate;
                            user.MenuMask = roles.roleMenuMask;
                            user.Admin = roles.level;
                            user.searchId = -1; // default value (FB 381)

                            user.EndPoint.linerateid = template.lineRateId;
                            user.EndPoint.protocol = template.videoProtocol;
                            user.EndPoint.connectiontype = template.connectiontype;
                            user.EndPoint.address = template.ipisdnaddress;
                            user.EndPoint.bridgeid = template.bridgeId;
                            user.EndPoint.profileId = 0; // no profile
                            user.EndPoint.isDefault = 0; // no default
                        
                            if (template.timezone == 0)
                                user.TimeZone = sysSettings.TimeZone;
                            else
                                user.TimeZone = template.timezone;

                            user.roleID = roles.roleID;
                            user.MenuMask = roles.roleMenuMask;
                            user.Admin = roles.level;
                            //user.EndPoint.endpointid = epid++;
                            // we do it this way for now. (endpoint added as a seperate transaction)
                            user.EndPoint.endpointid = 0;
                            user.FirstName = ldap.FirstName;
                            user.LastName = ldap.LastName;
                            user.Email = ldap.Email;
                            user.Telephone = ldap.Telephone;
                            user.Login = ldap.Login;
                            user.EndPoint.name = user.LastName;                           

                            //Edited for LDAP Browser START
                            user.DateFormat = "MM/dd/yyyy"; //Default Format
                            user.TimeFormat = "1";
                            user.Timezonedisplay = "1";
                            user.TimeZone = template.timezone;
                            user.initialTime = template.initialtime;
                            if (sPwd != "")
                            {
                                user.Password = sPwd;
                                sPwd = "";
                            }
                            user.companyId = template.companyId;
                            user.PreferedRoom = "0";//FB 2027 - Starts
                            user.TickerStatus = 1;
                            user.TickerStatus1 = 1;
                            user.TickerSpeed = 6;
                            user.TickerSpeed1 = 6;//FB 2027 - End
                            //Edited for LDAP Browser END

                            //FB 2320 - Starts
                            user.Company = "";
                            user.AlternativeEmail = ""; 
                            user.TickerBackground = "#3399ff";
                            user.TickerBackground1 = "#ffff99";
                            user.Audioaddon = "0";
                            user.RSSFeedLink = "";
                            user.RSSFeedLink1 = "";
                            user.WorkPhone = "";
                            user.CellPhone = "";
                            user.ConferenceCode = "";
                            user.LeaderPin = "";
                            user.InternalVideoNumber = "";
                            user.ExternalVideoNumber = "";
                            user.HelpReqEmailID = "";
                            user.HelpReqPhone = "";
                            //FB 2320 - End

                            string testLogin = user.Login;
                            string testEmail = user.Email;
                            Int32 testOrgId = user.companyId;

                            // first check user and guest table for this login name OR email
                            // if found DO NOT ADD, log error and continue

                            //FB 2119 Start
                            m_userType = m_isLDAP;
                            crypto = new cryptography.Crypto();
                            user.Password = crypto.encrypt("pwd");
                            //FB 2119 End

                            // cant delete this one, it is in use....
                            if (!checkUser(0, testLogin, testEmail, testOrgId))
                            {
                                myVRMException e = new myVRMException("Error user name " + user.Login + " or email " + user.Email + " already exist as either user or guest");
                                m_log.Error(e.Message);
                                string uName = ""; //FB 1623, 1624
                                uName = user.FirstName + " " + user.LastName;
                                if (uName.Trim() == "")
                                    uName = user.Email;

                                arrDuplicateUsr.Add(uName); //Added for LDAP
                            }
                            else
                            {
                                if (id == 0)
                                    id = m_userDAO.GetID();
                                else
                                    id++;
                                // set  up endpoiont, find max endpointid....
                                user.EndPoint.uId = 0;
                                if (epid == 0)
                                {
                                    if (epid == 0)
                                    {
                                        eptDao.addProjection(Projections.Max("endpointid"));
                                        IList maxId = eptDao.GetObjectByCriteria(new List<ICriterion>());
                                        if (maxId[0] != null)
                                            epid = ((int)maxId[0]) + 1;
                                        else
                                            epid = 1;
                                        eptDao.clearProjection();
                                    }
                                }
                                else
                                    epid++;
                                user.userid = id;
                                user.newUser = 1;
                                user.endpointId = epid;
                                user.EndPoint.endpointid = epid;
                                UserRecords.Add(user);
                                ldap.newUser = 0;
                            }
                        }
                        //Edited for LDAP Browser START                        
                        //if (!m_userDAO.SetMutipleUsers(UserRecords, LDAPRecords))
                        if (!m_userDAO.SetMutipleLDAPUsers(UserRecords))
                        {
                            myvrmEx = new myVRMException(509);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //else // for now we have to set the endpoints manually when we shit to .net we
                        //// will implement this through or mapping
                        //{
                        //Edited for LDAP Browser END
                        foreach (vrmUser ur in UserRecords)
                        {
                            ur.endpointId = ur.EndPoint.endpointid;
                            eptDao.Save(ur.EndPoint);
                            if (template.deptId > 0)
                            {
                                vrmUserDepartment ud = new vrmUserDepartment();
                                ud.userId = ur.userid;
                                ud.departmentId = template.deptId;
                                m_IuserDeptDAO.Save(ud);
                            }
                        }
                        if (arrDuplicateUsr.Count > 0) //Added for LDAP
                        {
                            String strDup = "";
                            obj.outXml = "<error><message>";
                            foreach (String str in arrDuplicateUsr)
                            {
                                if (strDup == "")
                                {
                                    obj.outXml += "User's email address or login ("+ str; //FB 1921
                                    strDup = "1";
                                }
                                else
                                    obj.outXml += "," + str;

                            }

                            obj.outXml += ") already exists either as Active/Inactive/Guest user in same/other organization</message></error>"; //Added for LDAP
                            bRet = false;

                        }
                    }
                    else
                    {
                        throw new myVRMException(252);
                    }
                } //Added for LDAP Browser

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();                
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region UserSearch
        private bool UserSearch(string alphabet, string field, string search,
            ref List<ICriterion> criterionList)
        {
            try
            {
                if (search.Length > 0)
                {
                    search = search.ToLower();
                    ICriterion criterium;
                    criterium = Expression.Or(
                         Expression.Like("FirstName", search + "%%"),
                         Expression.Like("LastName", search + "%%"));
                    criterium = Expression.Or(criterium, Expression.Like("Email", search + "%%"));
                    criterium = Expression.Or(criterium, Expression.Like("Login", search + "%%"));
                    criterionList.Add(criterium);
                }
                else
                {
                    if (alphabet.Length > 0)
                        if (alphabet == "a")
                            criterionList.Add(Expression.Le(field, alphabet));
                        else
                            if (alphabet.ToLower() != "all")
                                criterionList.Add(Expression.Like(field, alphabet.ToLower() + "%%"));
                }
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        #endregion

        #region GetUserList
        private bool GetUserList(string userID, string alphabet, string field, string search,
           ref int MaxRecords, ref List<vrmUser> userList, string BulkUser)
        {
            List<ICriterion> criterionList = new List<ICriterion>();

            try
            {
                IList Result = new ArrayList();

                criterionList.Add(Expression.Eq("userType", m_userType));
                UserSearch(alphabet, field, search, ref criterionList);
                m_IuserDao.addProjection(Projections.RowCount());

                IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                MaxRecords = Int32.Parse(list[0].ToString());
                m_IuserDao.clearProjection();

                if (m_userDAO.maxResultsReturned > 0)
                {
                    m_IuserDao.pageNo(1);
                    m_IuserDao.pageSize(m_userDAO.maxResultsReturned);
                }

                userList = m_IuserDao.GetByCriteria(criterionList);
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        #endregion

        #region GetLDAPUserList
        private bool GetLDAPUserList(string userID, string alphabet, string field, string search,
                            ref int MaxRecords, ref int MaxSearchRecords,  ref List<vrmLDAPUser> userList)
        {
            List<ICriterion> criterionList = new List<ICriterion>();

            try
            {
                IList Result = new ArrayList();

                criterionList.Add(Expression.Eq("newUser", 1));
                m_IldapDao.addProjection(Projections.RowCount());
               
                IList list = m_IldapDao.GetObjectByCriteria(criterionList);
                MaxRecords = Int32.Parse(list[0].ToString());
                UserSearch(alphabet, field, search, ref criterionList);
             
                IList list_2 = m_IldapDao.GetObjectByCriteria(criterionList);
                MaxSearchRecords = Int32.Parse(list_2[0].ToString());
                m_IldapDao.clearProjection();
                // FB 330 use user pageing informatioin (same change as in v18 branch AG 5/12/2008)
                m_IldapDao.pageNo(m_userDAO.pageNo);
                m_IldapDao.pageSize(m_userDAO.pageSize);
                //FB 509 sort order

                if (field == "FirstName")
                {
                    m_IldapDao.addOrderBy(Order.Asc("FirstName"));
                    m_IldapDao.addOrderBy(Order.Asc("LastName"));
                }
                else if (field == "Login")
                {
                    m_IldapDao.addOrderBy(Order.Asc("Login"));
                }
                else if (field == "Email")
                {
                    m_IldapDao.addOrderBy(Order.Asc("Email"));
                }
                else
                {
                    m_IldapDao.addOrderBy(Order.Asc("LastName"));
                    m_IldapDao.addOrderBy(Order.Asc("FirstName"));
                }

                userList = m_IldapDao.GetByCriteria(criterionList);
                m_IldapDao.clearOrderBy();
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        #endregion

        #region GetUserTemplateList
        public bool GetUserTemplateList(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes


                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("companyId", organizationID));

                List<vrmUserTemplate> vTlist = m_ItempDao.GetByCriteria(criterionList);
                obj.outXml += "<UserTemplates>";
                if (vTlist.Count > 0)
                {
                    foreach (vrmUserTemplate Temp in vTlist)
                    {
                        if (Temp.deleted == 0)
                        {
                            obj.outXml += "<Template>";
                            obj.outXml += "<ID>" + Temp.id.ToString() + "</ID>";
                            obj.outXml += "<Name>" + Temp.name + "</Name>";
                            obj.outXml += "<RoleID>" + Temp.roleId.ToString() + "</RoleID>";

                            vrmUserRoles rl = (m_IUserRolesDao.GetById(Temp.roleId));
                            obj.outXml += "<RoleName>" + rl.roleName + "</RoleName>";

                            obj.outXml += "</Template>";
                        }
                    }
                }
                obj.outXml += "</UserTemplates>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetUserTemplateDetails
        public bool GetUserTemplateDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                obj.outXml = string.Empty;
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();


                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes
                if(organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //<!-- new means new template, if there is 
                //     any value then you need to return that information-->
                node = xd.SelectSingleNode("//login/userTemplateID");
                string userTemplateID = node.InnerXml.Trim();

                obj.outXml += "<UserTemplate>";

                vrmUserTemplate vT = new vrmUserTemplate();
                if (userTemplateID.ToLower().CompareTo("new") != 0)
                    if ((vT = m_ItempDao.GetById(Int32.Parse(userTemplateID))) == null)
                        return false;
                obj.outXml += "<templateID>" + vT.id.ToString() + "</templateID>";
                obj.outXml += "<templateName>" + vT.name + "</templateName>";
                obj.outXml += "<initialTime>" + vT.initialtime + "</initialTime>";
                obj.outXml += "<videoProtocol>" + vT.videoProtocol + "</videoProtocol>";
                obj.outXml += "<IPISDNAddress>" + vT.ipisdnaddress + "</IPISDNAddress>";
                obj.outXml += "<connectionType>" + vT.connectiontype + "</connectionType>";
                obj.outXml += "<roleID>" + vT.roleId.ToString() + "</roleID>";
                obj.outXml += "<location>" + vT.location + "</location>";
                obj.outXml += "<languageID>" + vT.languageId.ToString() + "</languageID>";
                obj.outXml += "<lineRateID>" + vT.lineRateId.ToString() + "</lineRateID>";
                obj.outXml += "<bridgeID>" + vT.bridgeId.ToString() + "</bridgeID>";
                obj.outXml += "<timezone>" + vT.timezone.ToString() + "</timezone>";
                obj.outXml += "<departmentID>" + vT.deptId.ToString() + "</departmentID>";
                obj.outXml += "<accountExpiry>" + vT.expirationDate.ToString("MM/dd/yyyy") + "</accountExpiry>";
                obj.outXml += "<licenseExpiry>" + sysSettings.ExpiryDate.ToString("MM/dd/yyyy") + "</licenseExpiry>";
                obj.outXml += "<EquipmentID>" + vT.equipmentId.ToString() + "</EquipmentID>";
                obj.outXml += "<OutsideNetwork>" + vT.outsideNetwork.ToString() + "</OutsideNetwork>";
                obj.outXml += "<EmailNotification>" + vT.emailNotification.ToString() + "</EmailNotification>";

                obj.outXml += "<locationList>";

                if (vT.location != "" || vT.location != null)//Code changed for room search
                    obj.outXml += "<selected>" + vT.location + "</selected>";
                else
                    obj.outXml += "<selected></selected>";

                obj.outXml += "<mode>0</mode>";

                obj.outXml += "</locationList>";
                //FB 2891 - Start
                //List<vrmUserRoles> roles = m_IUserRolesDao.GetAll();                
                obj.outXml += "<roles>";
                
                List<ICriterion> criterionList1 = new List<ICriterion>();
                List<ICriterion> criterionList2= new List<ICriterion>();                
                                              
                criterionList1.Add(Expression.Gt("DisplayOrder", 0));
                m_IUserRolesDao.addOrderBy(Order.Asc("DisplayOrder"));
                List<vrmUserRoles> usrRoles = m_IUserRolesDao.GetByCriteria(criterionList1);
                
                criterionList2.Add(Expression.Lt("DisplayOrder", 1));
                List<vrmUserRoles> usrRoles1 = m_IUserRolesDao.GetByCriteria(criterionList2);
                usrRoles.AddRange(usrRoles1);                

                if (usrRoles.Count <= 0)
                {
                    m_log.Error("Error in getting role list");
                    obj.outXml = "";
                    return false;
                }

                foreach (vrmUserRoles uR in usrRoles)//FB 2891 - End
                {
                    obj.outXml += "<role>";
                    obj.outXml += "<ID>" + uR.roleID.ToString() + "</ID>";
                    obj.outXml += "<name>" + uR.roleName + "</name>";
                    obj.outXml += "<menuMask>" + uR.roleMenuMask + "</menuMask>";
                    obj.outXml += "<locked>" + uR.locked.ToString() + "</locked>";
                    obj.outXml += "<level>" + uR.level.ToString() + "</level>";
                    obj.outXml += "<active>1</active>";
                    obj.outXml += "</role>";
                }
                obj.outXml += "</roles>";

                IList languages = vrmGen.getLanguage();

                obj.outXml += "<languages>";
                foreach (vrmLanguage ln in languages)
                {
                    obj.outXml += "<language>";
                    obj.outXml += "<ID>" + ln.Id.ToString() + "</ID>";
                    obj.outXml += "<name>" + ln.Name + "</name>";
                    obj.outXml += "</language>";
                }
                obj.outXml += "</languages>";

                List<vrmLineRate> lineRate = vrmGen.getLineRate();

                obj.outXml += "<lineRate>";
                foreach (vrmLineRate lr in lineRate)
                {
                    obj.outXml += "<rate>";
                    obj.outXml += "<lineRateID>" + lr.Id.ToString() + "</lineRateID>";
                    obj.outXml += "<lineRateName>" + lr.LineRateType + "</lineRateName>";
                    obj.outXml += "</rate>";
                }
                obj.outXml += "</lineRate>";
                IMCUDao IMCUDao = new hardwareDAO(m_configPath, m_log).GetMCUDao();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1))); // FB 1920
                //criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmMCU> bridges = IMCUDao.GetByCriteria(criterionList);

                obj.outXml += "<bridges>";
                foreach (vrmMCU mcu in bridges)
                {
                    obj.outXml += "<bridge>";
                    obj.outXml += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    //FB 1920 starts
                    if (organizationID != 11)
                    {
                        if (mcu.isPublic.ToString() == "1")
                            mcu.BridgeName = mcu.BridgeName + "(*)";
                    }
                    // FB 1920 Ends
                    obj.outXml += "<name>" + mcu.BridgeName + "</name>";
                    obj.outXml += "</bridge>";
                }
                obj.outXml += "</bridges>";
                Hashtable timeZones = new Hashtable();

                if (!timeZone.GetAllTimeZone(ref timeZones))
                    return false;
                if (timeZones.Count <= 0)
                    return false;

                obj.outXml += "<timezones>";
                List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                foreach (timeZoneData time in timeZoneList)
                {
                    obj.outXml += "<timezone>";
                    obj.outXml += "<timezoneID>" + time.TimeZoneID.ToString() + "</timezoneID>";
                    obj.outXml += "<timezoneName>" + time.TimeZoneDiff.ToString() + " ";
                    obj.outXml += time.TimeZone + "</timezoneName>";
                    obj.outXml += "</timezone>";
                }
                obj.outXml += "</timezones>";

                obj.outXml += "</UserTemplate>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region SetUserTemplate
        public bool SetUserTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                vrmUserTemplate template = new vrmUserTemplate();


                node = xd.SelectSingleNode("//SetUserTemplate/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                template.companyId = organizationID;


                node = xd.SelectSingleNode("//SetUserTemplate/login");
                string userID = node.InnerXml.Trim();

                

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/UserTemplateID");
                string userTemplateID = node.InnerXml.Trim();
                if (userTemplateID.ToLower().Trim().CompareTo("new") == 0)
                    template.id = 0;
                else
                    template.id = Int32.Parse(userTemplateID);

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/TemplateName");
                template.name = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/InitialTime");
                if (node.InnerXml.Trim().Length > 0)
                    template.initialtime = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/VideoProtocol");
                if (node.InnerXml.Trim().Length > 0)
                    template.videoProtocol = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/IPISDNAddress");
                if (node.InnerXml.Trim().Length > 0)
                    template.ipisdnaddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/ConnectionType");
                if (node.InnerXml.Trim().Length > 0)
                    template.connectiontype = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/Location");
                if (node.InnerXml.Trim().Length > 0)
                    template.location = node.InnerXml.Trim();//Int32.Parse(node.InnerXml.Trim());code chaged for room search
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/LanguageID");
                if (node.InnerXml.Trim().Length > 0)
                    template.languageId = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/LineRateID");
                if (node.InnerXml.Trim().Length > 0)
                    template.lineRateId = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/BridgeID");
                if (node.InnerXml.Trim().Length > 0)
                    template.bridgeId = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/DepartmentID");
                if (node.InnerXml.Trim().Length > 0)
                    template.deptId = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/GroupID");
                if (node.InnerXml.Trim().Length > 0)
                    template.groupId = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/ExpirationDate");
                if (node.InnerXml.Trim().Length > 0)
                    template.expirationDate = DateTime.Parse(node.InnerXml.Trim());
                else
                    template.expirationDate = DateTime.Parse(sysSettings.ExpiryDate.ToString("MM/dd/yyyy"));//FB 1493

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/RoleID");
                if (node.InnerXml.Trim().Length > 0)
                    template.roleId = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/TimeZoneID");
                if (node.InnerXml.Trim().Length > 0)
                    template.timezone = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/EmailNotification");
                if (node.InnerXml.Trim().Length > 0)
                    template.emailNotification = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/OutsideNetwork");
                if (node.InnerXml.Trim().Length > 0)
                    template.outsideNetwork = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/EquipmentID");
                if (node.InnerXml.Trim().Length > 0)
                    template.equipmentId = Int32.Parse(node.InnerXml.Trim());

                m_ItempDao.SaveOrUpdate(template);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region DeleteUserTemplate
        public bool DeleteUserTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/userTemplateID");
                string userTemplateID = node.InnerXml.Trim();

                vrmUserTemplate vT = m_ItempDao.GetById(Int32.Parse(userTemplateID));

                if (vT == null)
                    return false;
                vT.deleted = 1;

                m_ItempDao.SaveOrUpdate(vT);
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetUserDetails
        public bool GetUserDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/EntityType");
                string EntityType = node.InnerXml.Trim();

                vrmBaseUser user = new vrmBaseUser();
             
                switch (Int32.Parse(EntityType))
                {
                    case vrmUserConstant.TYPE_GUEST:
                        user = (vrmBaseUser)m_IGuestUserDao.GetByUserId(Int32.Parse(userID));
                        break;
                    case vrmUserConstant.TYPE_INACTIVE:
                        user = (vrmBaseUser)m_IinactiveUserDao.GetByUserId(Int32.Parse(userID));
                        break;
                    default:
                        user = (vrmBaseUser)m_IuserDao.GetByUserId(Int32.Parse(userID));
                        break;
                }

                obj.outXml = "<user>";
                obj.outXml += "<userID>" + user.userid.ToString() + "</userID>";
                obj.outXml += "<firstName>" + user.FirstName + "</firstName>";
                obj.outXml += "<lastName>" + user.LastName + "</lastName>";
                obj.outXml += "<userEmail>" + user.Email + "</userEmail>";
                obj.outXml += "<userPhone>" + user.Telephone + "</userPhone>";
                obj.outXml += "<alternativeEmail>" + user.AlternativeEmail + "</alternativeEmail>";
                obj.outXml += "<sendBoth>" + user.DoubleEmail.ToString() + "</sendBoth>";
                obj.outXml += "<timeZone>" + user.TimeZone.ToString() + "</timeZone>";
                obj.outXml += "<languageID>" + user.Language.ToString() + "</languageID>";
                obj.outXml += "<menuMask>" + user.MenuMask + "</menuMask>";
                obj.outXml += "<initialTime>" + user.initialTime.ToString() + "</initialTime>";
                obj.outXml += "<status>";
                obj.outXml += "<level>" + user.newUser.ToString() + "</level>";
                obj.outXml += "<deleted>" + user.Deleted.ToString() + "</deleted>";
                obj.outXml += "<locked>" + user.LockStatus.ToString() + "</locked>";
                obj.outXml += "</status>";
                // end point information
                //obj.outXml += "<lineRateID>" + user.EndPoint.linerateid.ToString() + "</lineRateID>";
                //obj.outXml += "<videoProtocol>" + user.EndPoint.protocol.ToString() + "</videoProtocol>";
                //obj.outXml += "<IPISDNAddress>" + user.EndPoint.address + "</IPISDNAddress>";
                //obj.outXml += "<connectionType>" + user.EndPoint.connectiontype.ToString() + "</connectionType>";
                //obj.outXml += "<videoEquipmentID>" + user.EndPoint.videoequipmentid.ToString() + "</videoEquipmentID>";
                // address book ????            
                obj.outXml += "<addressBook>";
                obj.outXml += "<type></type>";
                obj.outXml += "<loginName></loginName>";
                obj.outXml += "<loginPassword></loginPassword>";
                obj.outXml += "<loginPath></loginPath>";
                obj.outXml += "</addressBook>";

                obj.outXml += "</user>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region SetUserDetails
        public bool SetUserDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/EntityType");
                string EntityType = node.InnerXml.Trim();


                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes


	            int userid = 0;;
                int iEntityType = Int32.Parse(EntityType);

	            if(userID.ToLower() == "new")
                {
                    if(iEntityType != vrmUserConstant.TYPE_USER)
                    {
                        myvrmEx = new myVRMException(479);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                    }
                }
                else
                {
                    userid = Int32.Parse(userID);
                }
 
                node = xd.SelectSingleNode("//login/firstName");
                string firstName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/lastName");
                string lastName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/login");
                string login = node.InnerXml.Trim();
	            node = xd.SelectSingleNode("//login/password");
                string password = node.InnerXml.Trim();
	            node = xd.SelectSingleNode("//login/userEmail");
                string userEmail = node.InnerXml.Trim();
	
	            node = xd.SelectSingleNode("//login/timeZone");
                string timeZone = node.InnerXml.Trim();
	            node = xd.SelectSingleNode("//login/location");
                string location = node.InnerXml.Trim();
            	if (location.Length == 0)	location = "0";
 	            node = xd.SelectSingleNode("//login/group");
                string group = node.InnerXml.Trim();
                if (group.Length == 0) group = "0";
 	            node = xd.SelectSingleNode("//login/ccGroup");
                string ccGroup = node.InnerXml.Trim();
                if (ccGroup.Length == 0) ccGroup = "0";
 	            node = xd.SelectSingleNode("//login/altemail");
                string altemail = node.InnerXml.Trim();
	            node = xd.SelectSingleNode("//login/sendBoth");
                string sendBoth = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/emailClient");
                string emailClient = node.InnerXml.Trim();
                if (emailClient.Length == 0) emailClient = "0";
	            node = xd.SelectSingleNode("//login/roleID");
                string roleID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/languageID");
                string languageID = node.InnerXml.Trim();
                if (languageID.Length == 0) languageID = "1"; // default US English
	            node = xd.SelectSingleNode("//login/lineRateID");
                string defaultLineRate = node.InnerXml.Trim();
                if (defaultLineRate.Length == 0) defaultLineRate = "0";
                node = xd.SelectSingleNode("//login/videoProtocol");
                string defaultVideoProtocol = node.InnerXml.Trim();
                if (defaultVideoProtocol == "IP")
		            defaultVideoProtocol = "1";
                else if (defaultVideoProtocol == "ISDN")
		            defaultVideoProtocol = "2";
	            node = xd.SelectSingleNode("//login/IPISDNAddress");
                string IPISDNAddress = node.InnerXml.Trim();	            
   		        node = xd.SelectSingleNode("//login/<connectionType");
                string connectionType = node.InnerXml.Trim();
                if (connectionType.Length == 0) connectionType = "-1"; // [None]
   			    node = xd.SelectSingleNode("//login/<videoEquipmentID");
                string defaultEquipment = node.InnerXml.Trim();
                if (defaultEquipment.Length == 0) defaultEquipment = "-1"; // [None]
                node = xd.SelectSingleNode("//login/<isOutside");
                string outsidenetwork = node.InnerXml.Trim();
                if (outsidenetwork.Length == 0) outsidenetwork = "0"; // inside by default
                node = xd.SelectSingleNode("//login/emailMask");
                string emailMask = node.InnerXml.Trim();	            
              	// Email mask
                if (emailMask.Length == 0) emailMask = "0xFFFF"; // Allow all emails    
	            // Address type
	            node = xd.SelectSingleNode("//login/addressTypeID");
                string addressTypeID = node.InnerXml.Trim();
                if (addressTypeID.Length == 0) addressTypeID = "1"; // default 1 (IP Address)
	
    //// We removed <level> attribute because it is controlled by the <roleID>
    //// field now. We have to determine the value of superadmin from that
    //CString superadmin, admin;
    //stmt = "SELECT level FROM Usr_Roles_D WHERE roleid = " + roleID;
    //SEx.GenericCommand((CString) __FILE__, (int) __LINE__, cmd, (CString) stmt);
    //if ( cmd.FetchNext() )
    //    admin = cmd.Field("level").asString();
    //else
    //    admin = "0";
    //if ( admin.Compare(_T("2")) == 0 ) superadmin = "1";
	
    //CString deleted		=	strOps.extractData ("<deleted>","</deleted>",inputXML);
    //if(deleted.Compare(_T("1"))==0)
    //{
    //    if ( !canDeleteUser(userid) )
    //    {
    //        outputXML = globalErrHandle.FetchErrorMessage(310);
    //        globalErrHandle.WriteMessage(_ttoi(userid), 310, "Cannot delete user. ");
    //        return outputXML;
    //    }
    //    else
    //    {
    //        userDeleted = true;
    //    }
    //}
	            node = xd.SelectSingleNode("//login/locked");
                string locked = node.InnerXml.Trim();	            
   	            
	            node = xd.SelectSingleNode("//login/accountexpiry");
                string accountexpiry = node.InnerXml.Trim();	            
	            
                DateTime accountExpiry = new DateTime();
    //if ( accountexpiry.IsEmpty() )
    //{
    //    if(m_pDBConn)
    //        accountexpiry = m_pDBConn->GetExpiryDate(); // Expiry date of VRM license
    //}
    //else
    //{
    //    if(common.CheckExpiryDate(accountexpiry)>0){
    //        outputXML = globalErrHandle.FetchErrorMessage(307);
    //        globalErrHandle.WriteMessage(_ttoi(userid), 307, "Cannot save user");
    //        return outputXML;
    //    }
    //}
                node = xd.SelectSingleNode("//login/initialTime");
                string initialTime = node.InnerXml.Trim();	            
	            node = xd.SelectSingleNode("//login/creditCard");
                string creditCard = node.InnerXml.Trim();
                if (creditCard.Length == 0) creditCard = "0";
	            node = xd.SelectSingleNode("//login/bridgeID");
                string bridgeID = node.InnerXml.Trim();	            
	            if ( bridgeID.Length == 0) bridgeID = "1"; // [None]
	
    //try
    //{
    //    if(mode == CREATENEW)  // now the email and alternativeEmail are same. We need to change it later.
    //    {
			
			
    //        // KM 09/17/04
    //        // Check if current active user count has exceeded the max permissible.
    //        VRMContainer	vrmContainer(m_pDBConn);
    //        bool maxLimitReached = vrmContainer.IsMaxEntityLimitReached(1);
    //        if (maxLimitReached)
    //        {
    //            outputXML = globalErrHandle.FetchErrorMessage(252);
    //            goto quit;
    //        }		
			
						
    //        CString errMsg;	
    //        long maxusrid = common.FetchMaxUserID(err, errMsg);
    //        if(err < 0) return errMsg;
    //        userid = strCon.longToCString(maxusrid+1);
			
			
    //        outputXML = CheckUser(userid,firstname,lastname,email,login,password,0 ,err);
    //        if(err<0)
    //            return outputXML;

                
                vrmBaseUser user = new vrmBaseUser();
                if (userid == 0)
                {
                    user.id = 0;
                    user.userid = m_userDAO.GetID();
                }
                else
                {
                    if(iEntityType == vrmUserConstant.TYPE_GUEST)
                    {
                        user = (vrmBaseUser)m_IGuestUserDao.GetByUserId(userid);
                        user.LastLogin = DateTime.Today;
                    }
                    else
                    {
                        user = (vrmBaseUser)m_IuserDao.GetByUserId(userid);
                    }
                }
                if(!checkUser(user.userid, login, userEmail,user.companyId))
                {
                    myvrmEx = new myVRMException(478);
                    obj.outXml = myvrmEx.FetchErrorMsg();

                }
                user.companyId = organizationID;
                user.FirstName = firstName;
                user.LastName = lastName;
                user.Login = login;
                // MUST BE ENCRYPTED !!!
                user.Password = password;
                user.Email = userEmail;
                user.TimeZone = Int32.Parse(timeZone);
                user.PreferedRoom = location; //Int32.Parse(location); Code changed for Room Search
                user.PreferedGroup = Int32.Parse(group);   
                user.CCGroup = Int32.Parse(ccGroup);
                user.AlternativeEmail = altemail;
                user.DoubleEmail = Int32.Parse(sendBoth);
                user.EmailClient = Int32.Parse(emailClient);
// have to lookup user roles 
                //node = xd.SelectSingleNode("//login/roleID");
                //string roleID = node.InnerXml.Trim();
                //node = xd.SelectSingleNode("//login/languageID");
                
                // load endpoint information
                user.Language = Int32.Parse(languageID);
                user.EndPoint.linerateid = Int32.Parse(defaultLineRate);
                user.EndPoint.protocol = Int32.Parse(defaultVideoProtocol);
                user.EndPoint.address = IPISDNAddress;
                user.EndPoint.addresstype = Int32.Parse(addressTypeID);
                user.EndPoint.videoequipmentid = Int32.Parse(defaultEquipment);
                user.EndPoint.outsidenetwork = Int32.Parse(outsidenetwork);
                user.EndPoint.connectiontype = Int32.Parse(connectionType);
                user.EndPoint.bridgeid = Int32.Parse(bridgeID);
                user.emailmask = Int32.Parse(emailMask);
                
                user.LockStatus = Int32.Parse(locked);
                user.accountexpiry = accountExpiry;
                user.initialTime = Int32.Parse(initialTime);

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetUserList
        public bool GetUserList(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userId = 0; //FB 3041
            try
            {
                
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                if (userID != "") //FB 3041
                    int.TryParse(userID, out userId);

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                string alphabet = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                string pageno = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/sortBy");
                string sortBy = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/EntityType");
                string EntityType = node.InnerXml.Trim();

                List<ICriterion> criterionList = new List<ICriterion>();

                criterionList.Add(Expression.Eq("companyId", organizationID));//Organization Module Fixes

                ICriterion criterium = null;

                string field = "LastName";
                switch (Int32.Parse(sortBy))
                {
                    case 1:
                        field = "FirstName";
                        break;
                    case 3:
                        field = "Login";
                        break;
                    case 4:
                        field = "Email";
                        break;
                }
                if (alphabet != "all")
                {
                    if (alphabet == "a")
                    {
                        criterionList.Add(Expression.Le(field, "aZ"));
                    }
                    else
                    {
                        criterionList.Add(Expression.Like(field, alphabet + "%%"));
                    }
                }
                if (multiDepts == 1)    //Organization Module Fixes
                {
                    vrmUser user = m_IuserDao.GetByUserId(userId);//FB 3041
                    if (getDeptLevelCriterion(user, ref criterium))
                    {
                        criterionList.Add(criterium);
                    }
                }
                int iPageNo = Int32.Parse(pageno);
                if (iPageNo == 0)
                    iPageNo = 1;
                long ttlRecords = 0;

                IList userList = new List<vrmBaseUser>();
                switch (Int32.Parse(EntityType))
                {
                    case vrmUserConstant.TYPE_GUEST:
                        m_IGuestUserDao.pageSize(m_iMaxRecords);
                        m_IGuestUserDao.pageNo(iPageNo);
                        if (userId > 0) //FB 3041
                            criterionList.Add(Expression.Eq("userid", userId));

                        ttlRecords =
                            m_IGuestUserDao.CountByCriteria(criterionList);
                        m_IGuestUserDao.addOrderBy(Order.Asc(field));
                        userList = m_IGuestUserDao.GetByCriteria(criterionList);
                        break;
                    case vrmUserConstant.TYPE_INACTIVE:
                        m_IinactiveUserDao.pageSize(m_iMaxRecords);
                        m_IinactiveUserDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IinactiveUserDao.CountByCriteria(criterionList);
                        m_IinactiveUserDao.addOrderBy(Order.Asc(field));
                        userList = m_IinactiveUserDao.GetByCriteria(criterionList);
                        break;
                    default:
                        m_IuserDao.pageSize(m_iMaxRecords);
                        m_IuserDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IuserDao.CountByCriteria(criterionList);
                        m_IuserDao.addOrderBy(Order.Asc(field));
                        userList = m_IuserDao.GetByCriteria(criterionList);
                        break;
                }
                int ttlPages = (int)(ttlRecords / m_iMaxRecords);

                // and modulo remainder...
                if (ttlRecords % m_iMaxRecords > 0)
                    ttlPages++;

                obj.outXml = "<users>";
                foreach (vrmBaseUser user in userList)
                {
                    obj.outXml += "<user>";
                    obj.outXml += "<userID>" + user.userid + "</userID>";
                    obj.outXml += "<firstName>" + user.FirstName + "</firstName>";
                    obj.outXml += "<lastName>" + user.LastName + "</lastName>";
                    obj.outXml += "<email>" + user.Email + "</email>";
                    //FB 3041 Start
                    vrmEndPoint ept = m_IeptDao.GetByEptId(user.endpointId);
                    if (ept != null)
                    {
                        obj.outXml += "<address>" + ept.address + "</address>";
                        obj.outXml += "<addresstype>" + ept.addresstype + "</addresstype>";
                        obj.outXml += "<outsidenetwork>" + ept.outsidenetwork + "</outsidenetwork>";
                        obj.outXml += "<protocol>1</protocol>";
                        obj.outXml += "<connectiontype>" + ept.connectiontype + "</connectiontype>";
                    }
                    else
                    {
                        obj.outXml += "<address></address>";
                        obj.outXml += "<addresstype>-1</addresstype>";
                        obj.outXml += "<outsidenetwork>0</outsidenetwork>";
                        obj.outXml += "<protocol>1</protocol>";
                        obj.outXml += "<connectiontype>1</connectiontype>";
                    }
                    //FB 3041 End
                    obj.outXml += "<status>";
                    obj.outXml += "<level>" + user.Admin.ToString() + "</level>";
                    obj.outXml += "<deleted>" + user.Deleted.ToString() + "</deleted>";
                    obj.outXml += "<locked>" + user.LockStatus.ToString() + "</locked>";
                    obj.outXml += "</status>";
                    obj.outXml += "</user>";
                }

                obj.outXml += "<pageNo>" + pageno.ToString() + "</pageNo>";
                obj.outXml += "<totalPages>" + ttlPages.ToString() + "</totalPages>";          
                obj.outXml += "<sortBy>" + sortBy + "</sortBy>";
                obj.outXml += "<alphabet>" + alphabet + "</alphabet>";
                obj.outXml += "<totalNumber>" + ttlRecords.ToString() + "</totalNumber>";

                obj.outXml += "</users>";
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region ConvertToGMT
        public bool ConvertToGMT(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//ConvertToGMT/DateTime");
                string dateTime = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//ConvertToGMT/TimeZone");
                string timeZoneID = node.InnerXml.Trim();

                DateTime dt = DateTime.Parse(dateTime);
                if (timeZone.changeToGMTTime(Int32.Parse(timeZoneID), ref dt))
                {
                    obj.outXml += "<ConvertToGMT>";
                    obj.outXml += "<DateTime>" + dt.ToString("d") + " " + dt.ToString("t") + "</DateTime>";
                    obj.outXml += "</ConvertToGMT>";
                    return true;
                }
                myvrmEx = new myVRMException(480);
                obj.outXml = myvrmEx.FetchErrorMsg();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region ConvertFromGMT
        public bool ConvertFromGMT(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//ConvertFromGMT/DateTime");
                string dateTime = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//ConvertFromGMT/TimeZone");
                string timeZoneID = node.InnerXml.Trim();

                DateTime dt = DateTime.Parse(dateTime);
                if (timeZone.userPreferedTime(Int32.Parse(timeZoneID), ref dt))
                {
                    obj.outXml += "<ConvertFromGMT>";
                    obj.outXml += "<DateTime>" + dt.ToString("d") + " " + dt.ToString("t") + "</DateTime>";
                    obj.outXml += "</ConvertFromGMT>";
                    return true;
                }
                myvrmEx = new myVRMException(480);
                obj.outXml = myvrmEx.FetchErrorMsg();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region getDeptLevelCriterion
        private bool getDeptLevelCriterion(vrmUser user, ref ICriterion criterium)
        {
            try
            {
                // superadmin, nothing to do!
                if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                    return false;

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> In = new List<int>();

                criterionList.Add(Expression.Eq("userId", user.userid));
                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                foreach (vrmUserDepartment dept in deptList)
                {
                    In.Add(dept.departmentId);
                }
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("departmentId", In));

                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                In = new List<int>();
                foreach (vrmUserDepartment dept in deptList)
                {
                   In.Add(dept.userId);
                }

                criterium = Expression.In("userid", In);
                criterium = Expression.Or(criterium, Expression.Eq("userid", 11));
                

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region ChangeUserStatus
        public bool ChangeUserStatus(ref vrmDataObject obj)
        {
	
    	    try
	        {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                int userID = Int32.Parse(node.InnerXml.Trim());             
                node = xd.SelectSingleNode("//login/user/userID");
                int actionID = Int32.Parse(node.InnerXml.Trim());                     
                node = xd.SelectSingleNode("//login/user/action");
                int action = Int32.Parse(node.InnerXml.Trim());
                
                vrmUser user = new vrmUser();
                vrmInactiveUser userIn = new vrmInactiveUser();
                vrmGuestUser guest = new vrmGuestUser();
                switch(action)
                {
                    case vrmUserAction.USER_STATUS_ACTIVE:                  
                        userIn = m_IinactiveUserDao.GetByUserId(actionID);
                        user = new vrmUser(userIn);
                        user.id = 0;
                        user.LockStatus = vrmUserStatus.USER_ACTIVE;
                        m_IinactiveUserDao.Delete(userIn);
                        m_IuserDao.Save(user);
	                    break;
                    case vrmUserAction.USER_STATUS_DELETE:	
          	            user = m_IuserDao.GetByUserId(actionID);
                        int errid = 310;//FB 1497

                        //ZD 100263
                        vrmUser LoginUser = m_IuserDao.GetByUserId(userID);
                        if (!CheckUserRights(LoginUser, user) || (LoginUser.userid == user.userid))
                        {
                            myvrmEx = new myVRMException(229);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        if (!canDeleteUser(user, ref errid))//FB 1497
			            {
                            myVRMException e = new myVRMException(errid);//FB 1497
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();//FB 1881
		                    return false;
			            }
                        user.Email = user.Email + "_D"; //FB 2346
                        userIn = new vrmInactiveUser(user);
                        userIn.id = user.id;
                        userIn.Deleted = vrmUserStatus.USER_INACTIVE;
                        m_IuserDao.Delete(user);
                        userIn.searchId = -1; //FB 2617
                        m_IinactiveUserDao.Save(userIn);                    
	                    break;
                    case vrmUserAction.USER_STATUS_LOCK:
                        user = m_IuserDao.GetByUserId(actionID);
                        //user.lockCntTrns = vrmUserStatus.MAN_LOCKED; // Code Commented for FB 1401
                        //user.LockStatus = 1; // Code Commented for FB 1401
                        user.lockCntTrns = 1;   //FB 1401
                        user.LockStatus = vrmUserStatus.MAN_LOCKED; //FB 1401
                        m_IuserDao.Update(user);
		                break;
                    case vrmUserAction.USER_STATUS_UNLOCK:
		                user = m_IuserDao.GetByUserId(actionID);
                        //user.lockCntTrns = vrmUserStatus.USER_ACTIVE; // Code Commented for FB 1401
                        //user.LockStatus = 0;  // Code Commented for FB 1401
                        user.lockCntTrns = 0;   //FB 1401
                        user.LockStatus = vrmUserStatus.USER_ACTIVE;    //FB 1401
                        m_IuserDao.Update(user);
		                break;
                    case vrmUserAction.USER_GUEST_DELETE:
		                guest = m_IGuestUserDao.GetByUserId(actionID);
                        guest.Deleted = 1;
                        guest.accountexpiry = DateTime.Parse("01/01/1970");
                        m_IGuestUserDao.Update(guest);
                        break;
                    
                    case vrmUserAction.USER_GUEST_UNDELETE:
		                guest = m_IGuestUserDao.GetByUserId(actionID);
                        guest.Deleted = 0;
                        guest.accountexpiry = DateTime.Parse("01/01/1970");
                        m_IGuestUserDao.Update(guest);
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetManageUser
        public bool GetManageUser(ref vrmDataObject obj)
        {
	
    	    try
	        {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                //FB 2027 Starts
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 1;//FB 2269

                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes
                //FB 2027 Ends
                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out sortBy);
                //FB 2027 Ends

                node = xd.SelectSingleNode("//login/audioaddon"); //FB 2023
                if (node != null)
                    if (node.InnerText.Trim() != "")
                        Int32.TryParse(node.InnerText.Trim(), out isAudioAddOn);

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml, organizationID, 0, deptUsers, BulkUser))//FB 2027 //FB 2269
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetUserList
        public bool GetUserList(int userID, int type, int pageno, ref int totpages, string alphabet, int sortBy, int mode, ref string outXml, string BulkUser)
        {
	
    	    try
	        {
                int numrows = 0;
	            int thresholdmax = 200; // Not more than 200 users at a time
	            bool thresholdpassed = false;
            	
                // this routing must work for LDAP also (not implemented yet)
                // when getting LDAP count newuser = 1

	            int admin = 0;
                
                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;    //Organization Module

	            vrmUser user = m_IuserDao.GetByUserId(userID);
              
	            outXml = "<users>";
	            
            	// Spoof admin if this is an LDAP lookup (this is only visible to admmins) [AG 5-19-08]
                if(type == m_isUser)
                {
                    if (multiDepts == 1)    //Organization Module
                        admin = user.Admin;
                    else
                        admin = vrmUserConstant.SUPER_ADMIN;
                }
                else
                {
                    admin = vrmUserConstant.SUPER_ADMIN;
                }
                string checkAlpha = alphabet.ToLower();
                	
                List<ICriterion> criteriumList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
			    List<int> userIn = new List<int>();
			    //
		        // if multi departments is enabled apply roles based security. 
		        // if not then anyone can see all depts 
		        //
                if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN) //Organization Module
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", user.userid));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        deptIn.Add(dept.departmentId);
                    }
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.In("departmentId", deptIn));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        userIn.Add(dept.userId);
                    }
                    
                    criteriumList.Add(Expression.In("userid", userIn));                 
                }
              
                // LDAP_BlankUser_D
                // here : AND newuser = 1
	              
                // first get the number of records WITHOUT criteria applied
                m_IuserDao.addProjection(Projections.RowCount());

                IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                numrows = Int32.Parse(list[0].ToString());
                totpages = numrows / m_PAGE_SIZE;
                if ((numrows % m_PAGE_SIZE) > 0)
                    totpages++;

                list = m_IuserDao.GetObjectByCriteria(criteriumList);
                int numrecs = Int32.Parse(list[0].ToString());
                m_IuserDao.clearProjection();

                string a_Order = null; 
           
                switch(sortBy)
                {
                    case 1:
                       a_Order = "FirstName";
                       if ( checkAlpha != "all" )
                       {		        
                           if(checkAlpha == "a")
                               criteriumList.Add(Expression.Like("FirstName", "A%%"));
                            else
                               criteriumList.Add(Expression.Like("FirstName", alphabet + "%%"));
                       }
                       break;
                    case 3:                       		        
                       a_Order = "Login";
                       if ( checkAlpha != "all" )
                       {
                        if(checkAlpha == "a")
                           criteriumList.Add(Expression.Like("Login", "A%%"));
                        else
                           criteriumList.Add(Expression.Like("Login", alphabet + "%%"));
                       }
                       break;
                    case 4:
                        a_Order = "Email";
                        if ( checkAlpha != "all" )
                        {
                           if(checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Email", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Email", alphabet + "%%"));
                        }
                        break;
                    default:
                       a_Order = "LastName";
                       if (checkAlpha != "all")
                       {
                           if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("LastName", "A%%"));
                             else
                                criteriumList.Add(Expression.Like("LastName", alphabet + "%%"));
                       }
                       break;
                    
                }              
	            if ( numrows > thresholdmax )
	            {
		            thresholdpassed = true;
	            }

                // add order by condition
                m_IuserDao.addOrderBy(Order.Asc(a_Order));
        
                m_IuserDao.pageSize(m_PAGE_SIZE);
                if (pageno == 0)
                    pageno = 1;

                m_IuserDao.pageNo(pageno);

                // always true criteria 
                if (criteriumList.Count == 0)
                    criteriumList.Add(Expression.Ge("Deleted", 0));
                
                List<vrmUser>userList = m_IuserDao.GetByCriteria(criteriumList);
               
                foreach(vrmUser vUser in userList)                
		        {			    	
			        outXml += "<user>";
			        outXml += "<userID>" + vUser.userid.ToString() + "</userID>";
			        outXml += "<firstName>" + vUser.FirstName + "</firstName>";
			        outXml += "<lastName>" + vUser.LastName + "</lastName>";
			        outXml += "<login>" + vUser.Login + "</login>";
			        outXml += "<email>" + vUser.Email + "</email>";
			        outXml += "<telephone>" + vUser.Telephone + "</telephone>";
			
                    if ( type != m_isLDAP)  //FB 2027
			        {
                        if (vUser.Password != null)
                            outXml += "<password>" + vUser.Password + "</password>";
                        else
                            outXml += "<password></password>";
			        }
			
			        if ( mode == 1 )
			        {                    
                        IUserAccountDao IuserAccountDAO = m_userDAO.GetUserAccountDao();
                        vrmAccount uAccount = IuserAccountDAO.GetByUserId(vUser.userid); //FB 2027

                        outXml += "<report>";
                        outXml += "<total>" + uAccount.TotalTime.ToString() + "</total>";
				        long timeUsed = uAccount.TotalTime - uAccount.TimeRemaining;
        				
				        outXml +="<used>" + timeUsed.ToString() + "</used>";
				        // used = totalAmount- left
				        outXml +="<left>" + uAccount.TimeRemaining.ToString() + "</left>";
				        outXml +="</report>";
			        }
			        else if ( mode == 2 )
			        {
                        string lockstatus = "0";
                        string deleted = "0";

                        if(vUser.LockStatus != 0)
                            lockstatus = "1";                        

				        if ( vUser.Deleted != 0 ) // Deleted
					        deleted = "1";
				     
                        outXml += "<status>";
				        outXml += "<level>" + vUser.Admin.ToString() + "</level>";
				        outXml += "<deleted>" + deleted + "</deleted>";
				        outXml += "<locked>"+ lockstatus + "</locked>";
				        outXml += "</status>";
			        }			
			        outXml += "</user>";
		        }
		        outXml += "<pageNo>" + pageno.ToString() + "</pageNo>";
	            outXml += "<totalPages>" + totpages.ToString() + "</totalPages>";
	            outXml += "<sortBy>" + sortBy.ToString() + "</sortBy>";
	            outXml += "<alphabet>" + alphabet + "</alphabet>";
	            if ( thresholdpassed )
		            outXml += "<canAll>0</canAll>";
	            else
		            outXml += "<canAll>1</canAll>";

	            outXml += "<totalNumber>" + numrecs.ToString() + "</totalNumber>";
        	    if ( type != m_isLDAP )
	            {
                    int usersRemain = sysSettings.UserLimit - numrows;
                    outXml += "<licensesRemain>" + usersRemain.ToString() + "</licensesRemain>";
	            }   
	            outXml += "</users>";	                    
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region canDeleteUser
        //
        // check if deleting this user valid
        //
        bool canDeleteUser(vrmUser user, ref int errid)//FB 1497
        {

    	    try
	        {
                IConferenceDAO a_IconfDAO;
                conferenceDAO a_confDAO = new conferenceDAO(m_configPath, m_log);
                a_IconfDAO = a_confDAO.GetConferenceDao();

                //
                // check if this user is room assistant, MCU Approver, Dept approver, Room approver, 
                // work oreder admin or inventory admin. If not cannot delete. 
                //if (m_userDAO.checkUser(user))
                if (checkUser(user, ref errid)) //FB 1497
                {
                    //
                    // now check if user has any active conferences
                    //
                    DateTime checkDate = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref checkDate);
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Ge("confEnd", checkDate));
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("userid", user.userid));
                    List<vrmConference> confList = a_IconfDAO.GetByCriteria(criterionList);

                    // now check for ongoing conferences
                    if (confList.Count > 0)
                    {
                        return false;
                    }
                    return true;
                }
                return false;
	        }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region checkUser
        public bool checkUser(int userid, string login, string email, Int32 testOrgId)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium;

                organizationID = testOrgId;
                //FB 1623, 1624
                //criterium = Expression.Eq("companyId", organizationID); //Organization fixes
                //criterionList.Add(criterium);

                //criterium = Expression.Eq("Login", login.ToUpper()).IgnoreCase(); //FB 1623, 1624
                //if (email.Trim().Length > 0)
                //{
                //    criterium = Expression.Or(criterium, Expression.Like("Email", email.Trim().ToUpper()).IgnoreCase());
                //}
                //FB 1921 Start
                if (login != "")
                {
                    criterium = Expression.Eq("Login", login.ToUpper()).IgnoreCase();
                    criterium = Expression.Or(criterium, Expression.Eq("Email", email.Trim().ToUpper()).IgnoreCase());
                }
                else
                {
                    criterium = Expression.Eq("Email", email.Trim().ToUpper()).IgnoreCase(); //FB 1623, 1624
                }
                //FB 1921 End
                criterionList.Add(criterium);
                if(userid > 0)
                {
                    criterium = Expression.And(criterium, Expression.Not(Expression.Eq("userid", userid)));
                }    
                List<vrmUser> results = m_IuserDao.GetByCriteria(criterionList);
                int errCount = results.Count;
                if( errCount > 0)
                    return false;
                IGuestUserDao guest = m_userDAO.GetGuestUserDao();
                
                //FB 2119 Start
                List<vrmGuestUser> guestResults = guest.GetByCriteria(criterionList,true);
                if (m_userType == m_isLDAP)
                {
                    for (int i = 0; i < guestResults.Count; i++)
                        guest.Delete(guestResults[i]);
                }
                else
                {
                    errCount = guestResults.Count;
                    if( errCount > 0)
                        return false;
                }
                //FB 2119 End

                IInactiveUserDao inactive = m_userDAO.GetInactiveUserDao();
                List<vrmInactiveUser> inactiveResults = inactive.GetByCriteria(criterionList);
                errCount = inactiveResults.Count;
                if( errCount > 0)
                    return false;
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        //FB 1497
        private bool checkUser(vrmUser user, ref int errid) 
        {
            try
            {
                string hql;
                DataSet ds = null;

                if (m_rptLayer == null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);
                
                hql = " SELECT  R.RoomID, R.Disabled FROM Loc_Room_D R WHERE Disabled = 0 and assistant = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 520;
                    return false;
                }
                hql = " SELECT M.approverid FROM MCU_Approver_D M WHERE approverid = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " SELECT D.approverid FROM Dept_Approver_D D WHERE approverid = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " SELECT L.approverid FROM Loc_Approver_D L WHERE approverid = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " SELECT W.AdminID FROM Inv_WorkOrder_D W WHERE AdminID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " SELECT C.AdminID FROM Inv_Category_D C WHERE AdminID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 520;
                    return false;
                }
                
                hql = " SELECT M.Admin FROM MCU_List_D M WHERE Admin = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 520;
                    return false;
                }
               
                hql = " SELECT A.approverid FROM  Sys_Approver_D A WHERE approverid = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
               
                hql = " SELECT A.UserID FROM  Grp_Participant_D A WHERE UserID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }

                //FB 2027
                hql = " SELECT A.Owner FROM  Grp_Detail_D A WHERE Owner = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }

                hql = " SELECT A.TmpOwner FROM  Tmp_List_D A WHERE TmpOwner = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }

                hql = " SELECT A.UserID FROM  Tmp_Participant_D A WHERE UserID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }
                //FB 2027

                //FB 3004 Start

                DateTime confEnd = DateTime.Now;
                DateTime confFrom = DateTime.Now;

                if (m_Search.getSearchDateRange(1, ref confEnd, ref confFrom, 0))
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                }

                //Check whether participant is scheduler
                hql = "Select c.userid from Conf_Conference_D c WHERE c.deleted=0 and c.userid='" + user.userid.ToString() + "'";
                hql += " and c.status in (0,1,5,6) and ((c.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') ";
                hql += " or c.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 518;
                    return false;
                }
                //Check whether participant is host
                hql = "Select c.confid from Conf_Conference_D c " +
                             " WHERE c.deleted=0 and c.owner='" + user.userid.ToString() + "'" +
                             " and c.status in (0,1,5,6) and ((c.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') " +
                             " or c.confdate >= '" + confFrom.ToString() + "')";

                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 518;
                    return false;
                }

                hql = "SELECT DISTINCT cu.confid FROM Conf_Conference_D vc, conf_user_D cu " +
                              " WHERE vc.deleted=0 and vc.status in (0,1,5,6)" +
                              " and vc.confid=cu.confid and cu.userid ='" + user.userid.ToString() + "' and ((vc.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') " +
                              " or vc.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 518;
                    return false;
                }
                //FB 1497 end
                // FB 2670 Starts
                hql = "SELECT DISTINCT cv.confid FROM Conf_Conference_D vc, Conf_VNOCOperator_D cv " +
                            " WHERE vc.deleted=0 and vc.status in (0,1,5,6)" +
                            " and vc.confid=cv.confid and cv.vnocId ='" + user.userid.ToString() + "' and ((vc.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') " +
                            " or vc.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 697;
                    return false;
                }

                hql = "SELECT DISTINCT cv.confid FROM Conf_Conference_D vc, Conf_VNOCOperator_D cv " +
                         " WHERE vc.deleted=0 and vc.status in (0,1,5,6)" +
                         " and vc.confid=cv.confid and cv.vnocAssignAdminId ='" + user.userid.ToString() + "' and ((vc.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') ";
                hql += " or vc.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 697;
                    return false;
                }
                //FB 2670 Ends
                //FB 3004 End
                return true;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SetBulkUserUpdate

        public bool SetBulkUserUpdate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                List<ICriterion> criUsrList = new List<ICriterion>(); //FB 1599
                int typeLimit = 0, errorID = 200,usrType=0; //FB 1599
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                int userID = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/type");
                int type = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/newValue");
                string newValue = node.InnerXml.Trim();
                //FB 1599 start
                if (xd.SelectSingleNode("//login/organizationID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/organizationID").InnerText.Trim(), out organizationID);
                if (organizationID < 11)
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                //FB 1599 end

                vrmUser user = null;
                XmlElement root = xd.DocumentElement;
                XmlNodeList elemList = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < elemList.Count; i++)
                {
                    node = elemList[i];
                    user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    m_IuserDao.clearFetch();
                    switch (type)
                    {
                        case 9:
                            user.DateFormat = newValue.Trim();
                            break;
                        case 10:
                            user.TimeFormat = newValue.Trim();
                            break;
                        case 11:
                            user.Timezonedisplay = newValue.Trim();
                            break;
                        //case 12:
                        //    user.EnableAV = newValue.Trim();
                        //    break;
                        case 13: //FB 1599 start
                            usrType = user.enableDomino;
                            user.enableDomino = Int32.Parse(newValue.Trim());
                            criUsrList.Add(Expression.Eq("enableDomino", 1));
                            typeLimit = orgdt.DominoUserLimit;
                            errorID = 461;
                            break;
                        case 14:
                            usrType = user.enableExchange;
                            user.enableExchange = Int32.Parse(newValue.Trim());
                            criUsrList.Add(Expression.Eq("enableExchange", 1));
                            typeLimit = orgdt.ExchangeUserLimit;
                            errorID = 460;
                            break;
                        case 15: //FB 1979 
                            usrType = user.enableMobile;
                            user.enableMobile = Int32.Parse(newValue.Trim());
                            criUsrList.Add(Expression.Eq("enableMobile", 1));
                            typeLimit = orgdt.MobileUserLimit;
                            errorID = 526;
                            break;

                    }

                    if (newValue.IndexOf("yyyy") < 0) //FB 2018
                    {
                        if (newValue.Trim() != "0" && usrType != Int32.Parse(newValue.Trim()) && (type == 13 || type == 14 || type == 15)) //FB 1979
                        {
                            criUsrList.Add(Expression.Eq("companyId", organizationID));
                            criUsrList.Add(Expression.Eq("Deleted", 0));
                            List<vrmUser> results = m_IuserDao.GetByCriteria(criUsrList);
                            if (results.Count + 1 > typeLimit || elemList.Count > typeLimit)
                            {
                                myvrmEx = new myVRMException(errorID);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    //FB 1599 end
                    m_IuserDao.SaveOrUpdate(user);
                }
                bRet = true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                bRet = false;
                throw ex;
            }
            return bRet;
        }
#endregion

        //Code Added for FB Issue 826 -- Start
        #region GetAllManageUser
        public bool GetAllManageUser(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                int userID = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                string alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                int pageNo = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/sortBy");
                int sortBy = Int32.Parse(node.InnerXml.Trim());

                int totPages = 0;
                string outXml = string.Empty;
                if (GetAllUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml))
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion
                
        #region GetAllUserList
        public bool GetAllUserList(int userID, int type, int pageno, ref int totpages, string alphabet, int sortBy, int mode, ref string outXml)
        {

            try
            {
                int numrows = 0;
                int thresholdmax = 200; // Not more than 200 users at a time
                bool thresholdpassed = false;

                // this routing must work for LDAP also (not implemented yet)
                // when getting LDAP count newuser = 1
                int admin = 0;
                
                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;    //Organization Module

                vrmUser user = m_IuserDao.GetByUserId(userID);

                outXml = "<users>";

                // Spoof admin if this is an LDAP lookup (this is only visible to admmins) [AG 5-19-08]
                if (type == m_isUser)
                {
                    if (multiDepts == 1)
                        admin = user.Admin;
                    else
                        admin = vrmUserConstant.SUPER_ADMIN;
                }
                else
                {
                    admin = vrmUserConstant.SUPER_ADMIN;
                }
                string checkAlpha = alphabet.ToLower();

                List<ICriterion> criteriumList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
                List<int> userIn = new List<int>();
                //
                // if multi departments is enabled apply roles based security. 
                // if not then anyone can see all depts 
                //
                if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN)
                {

                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", user.userid));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        deptIn.Add(dept.departmentId);
                    }
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.In("departmentId", deptIn));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        userIn.Add(dept.userId);
                    }

                    criteriumList.Add(Expression.In("userid", userIn));
                }

                // LDAP_BlankUser_D
                // here : AND newuser = 1

                // first get the number of records WITHOUT criteria applied
                m_IuserDao.addProjection(Projections.RowCount());

                IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                numrows = Int32.Parse(list[0].ToString());
                totpages = numrows / m_PAGE_SIZE;
                if ((numrows % m_PAGE_SIZE) > 0)
                    totpages++;

                list = m_IuserDao.GetObjectByCriteria(criteriumList);
                int numrecs = Int32.Parse(list[0].ToString());
                m_IuserDao.clearProjection();

                string a_Order = null;

                switch (sortBy)
                {
                    case 1:
                        a_Order = "FirstName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("FirstName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("FirstName", alphabet + "%%"));
                        }
                        break;
                    case 3:
                        a_Order = "Login";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Login", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Login", alphabet + "%%"));
                        }
                        break;
                    case 4:
                        a_Order = "Email";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Email", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Email", alphabet + "%%"));
                        }
                        break;
                    default:
                        a_Order = "LastName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("LastName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("LastName", alphabet + "%%"));
                        }
                        break;

                }
                if (numrows > thresholdmax)
                {
                    thresholdpassed = true;
                }

                // add order by condition
                m_IuserDao.addOrderBy(Order.Asc(a_Order));

                m_IuserDao.pageSize(numrows);
                if (pageno == 0)
                    pageno = 1;

                m_IuserDao.pageNo(pageno);

                // always true criteria 
                if (criteriumList.Count == 0)
                    criteriumList.Add(Expression.Ge("Deleted", 0));

                List<vrmUser> userList = m_IuserDao.GetByCriteria(criteriumList);

                foreach (vrmUser vUser in userList)
                {
                    outXml += "<user>";
                    outXml += "<userID>" + vUser.userid.ToString() + "</userID>";
                    outXml += "<firstName>" + vUser.FirstName + "</firstName>";
                    outXml += "<lastName>" + vUser.LastName + "</lastName>";
                    outXml += "<login>" + vUser.Login + "</login>";
                    outXml += "<email>" + vUser.Email + "</email>";
                    outXml += "<telephone>" + vUser.Telephone + "</telephone>";

                    if (type != m_isLDAP)
                    {
                        if (vUser.Password != null)
                            outXml += "<password>" + vUser.Password + "</password>";
                        else
                            outXml += "<password></password>";
                    }

                    if (mode == 1)
                    {
                        IUserAccountDao IuserAccountDAO = m_userDAO.GetUserAccountDao();
                        vrmAccount uAccount = IuserAccountDAO.GetByUserId(vUser.userid);//FB 2027

                        outXml += "<report>";
                        outXml += "<total>" + uAccount.TotalTime.ToString() + "</total>";
                        long timeUsed = uAccount.TotalTime - uAccount.TimeRemaining;

                        outXml += "<used>" + timeUsed.ToString() + "</used>";
                        // used = totalAmount- left
                        outXml += "<left>" + uAccount.TimeRemaining.ToString() + "</left>";
                        outXml += "</report>";
                    }
                    else if (mode == 2)
                    {
                        string lockstatus = "0";
                        string deleted = "0";

                        if (vUser.LockStatus != 0)
                            lockstatus = "1";

                        if (vUser.Deleted != 0) // Deleted
                            deleted = "1";

                        outXml += "<status>";
                        outXml += "<level>" + vUser.Admin.ToString() + "</level>";
                        outXml += "<deleted>" + deleted + "</deleted>";
                        outXml += "<locked>" + lockstatus + "</locked>";
                        outXml += "</status>";
                    }
                    outXml += "</user>";
                }
                outXml += "<pageNo>" + pageno.ToString() + "</pageNo>";
                outXml += "<totalPages>" + totpages.ToString() + "</totalPages>";
                outXml += "<sortBy>" + sortBy.ToString() + "</sortBy>";
                outXml += "<alphabet>" + alphabet + "</alphabet>";
                if (thresholdpassed)
                    outXml += "<canAll>0</canAll>";
                else
                    outXml += "<canAll>1</canAll>";

                outXml += "<totalNumber>" + numrecs.ToString() + "</totalNumber>";
                if (type != m_isLDAP)
                {
                    int usersRemain = sysSettings.UserLimit - numrows;
                    outXml += "<licensesRemain>" + usersRemain.ToString() + "</licensesRemain>";
                }
                outXml += "</users>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
        //Code added for FB Issue 826 -- End

        //Code Added for Webservice START
        #region ChkUserValidation
        public bool ChkUserValidation(ref vrmDataObject obj)
        {
            string sLogin = "";
            string sPwd = "";
            string sEnPwd = "";
            string sAuthenticated = "NO";//SSo Mode
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//myVRM/login");
                if (node.InnerText != "")
                   sLogin = node.InnerXml.Trim();
                else
                {
                    myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//myVRM/IsAuthenticated");//SSo Mode
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();
                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {

                    node = xd.SelectSingleNode("//myVRM/password");
                    if (node.InnerText != "")
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(530); //FB 2054
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Login", sLogin));
                //criterionList.Add(Expression.Eq("Password", sEnPwd)); //FB 2054
                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList);
                
                //FB 2054 Starts
                if((vrmUsr.Count <= 0))
                {
                   myvrmEx = new myVRMException(207); //FB 2054 //FB 3055
                   obj.outXml = myvrmEx.FetchErrorMsg();
                   return false;

                }

                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {
                    if (vrmUsr[0].Password != sEnPwd)
                    {
                        myvrmEx = new myVRMException(207);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                if (vrmUsr[0].accountexpiry <= DateTime.Now)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(263);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (vrmUsr[0].LockStatus == 2 || vrmUsr[0].LockStatus == 3)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(254);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                else if (vrmUsr[0].lockCntTrns == 10)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(425);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2054 End
                if (vrmUsr[0] == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                if (usrOrg == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (usrOrg.EnableAPI == 1)
                {
                    obj.outXml = "<success><organizationID>" + vrmUsr[0].companyId + "</organizationID><PIMServiceType>"+ usrOrg.EnablePIMServiceType +"</PIMServiceType></success>";//FB 2038
                    return true;
                }
                myvrmEx = new myVRMException(252);
                obj.outXml = myvrmEx.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("systemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region CheckUserCredentials
        /// <summary>
        /// CheckUserCredentials
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CheckUserCredentials(ref vrmDataObject obj)
        {
            string sLogin = "";
            string sPwd = "";
            string sEnPwd = "";
            int sClient = 0;//FB 1899,1900

            string sAuthenticated = "NO";

            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//myVRM/login");
                if (node.InnerText != "")
                    sLogin = node.InnerXml.Trim();
                else
                {
                    myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//myVRM/IsAuthenticated");
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();

                List<ICriterion> criterionList = new List<ICriterion>();

                if (sAuthenticated.ToUpper() == "NO")
                {

                    node = xd.SelectSingleNode("//myVRM/password");
                    if (node.InnerText != "")
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(530); //FB 2054
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    /*//FB 1899,1900 Start
                    node = xd.SelectSingleNode("//myVRM/client");
                    if (node.InnerText != "")
                        int.TryParse(node.InnerXml.Trim(), out sClient);

                    clientFrom  cFrom;
                    cFrom = GetClientName(sClient);
                    //FB 1899,1900 End*/
                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);

                    criterionList.Add(Expression.Eq("Email", sLogin));
                }
                else
                    criterionList.Add(Expression.Eq("Login", sLogin));



                //criterionList.Add(Expression.Eq("Password", sEnPwd));//FB 2054

                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList);



                if (vrmUsr.Count <= 0)
                {
                    myvrmEx = new myVRMException(207); //FB 2054 //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2054 Start
                if (sAuthenticated.ToUpper() == "NO")
                {
                    if (vrmUsr[0].Password != sEnPwd)
                    {
                        myvrmEx = new myVRMException(207);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                if (vrmUsr[0].accountexpiry <= DateTime.Now)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(263);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (vrmUsr[0].LockStatus == 2 || vrmUsr[0].LockStatus == 3)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(254);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                else if (vrmUsr[0].lockCntTrns == 10)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(425);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2054 End
                if (vrmUsr[0] == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                if (usrOrg == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (usrOrg.EnableAPI == 1)
                {
                    obj.outXml = "<success><organizationID>" + vrmUsr[0].companyId + "</organizationID><PIMServiceType>" + usrOrg.EnablePIMServiceType + "</PIMServiceType><LoginUserId>" + vrmUsr[0].userid + "</LoginUserId></success>";//FB 2038 //FB 2724
                    return true;
                }

                /*if (usrOrg.EnableAPI == 1)
                {   
                    //FB 1899,1900 Start
                    String strUser = "";
                    Int32 errID;
                    if ((cFrom == clientFrom.Exchange && vrmUsr[0].enableExchange == 1) || (cFrom == clientFrom.Lotus && vrmUsr[0].enableDomino == 1))                        
                        // Need to handle Apple Devices & Android Devices
                    {
                        obj.outXml = "<success><organizationID>" + vrmUsr[0].companyId + "</organizationID></success>";
                        return true;
                    }
                    else
                    {
                        if (cFrom == clientFrom.Exchange && vrmUsr[0].enableExchange == 0)
                            errID = 523;
                        else if (cFrom == clientFrom.Lotus && vrmUsr[0].enableDomino == 0)
                            errID = 524;
                        else if ((cFrom == clientFrom.AppleDevices && vrmUsr[0].enableMobile == 0) || (cFrom == clientFrom.AndroidDevices && vrmUsr[0].enableMobile == 0))
                            errID = 525;
                        else
                            errID = 522;

                        myvrmEx = new myVRMException(errID);
                        obj.outXml = myvrmEx.FetchErrorMsg();

                        return false;
                    }
                    //FB 1899,1900 End
                }*/

                myvrmEx = new myVRMException(252);
                obj.outXml = myvrmEx.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("systemException", e);
                obj.outXml = "";//FB 1881
                return false;
            }
        }
        #endregion
        //Code Added for Webservice END

        /* Organization Module - start */
        #region ManageApproverCounters
        /// <summary>
        /// Every user has a field 'approver' in the USER table which is actually 
        /// a counter. For every instance that a user is made an approver, this counter	
        /// is incremented. And if a user is removed from the approver list of any 
        /// entity, her approver counter is decremented. The flag signifies increment
        /// or decrement
        /// </summary>
        /// <param name="approverLevel">Entity Level (Room/MCU/Dept/System)</param>
        /// <param name="operatingFlag">Specifies whether increment(1) or decrement (2) by 1</param>
        /// <param name="approversList">List of Entity Approvers ID</param>
        /// <returns></returns>
        internal bool ManageApproverCounters(int approverLevel, int operatingFlag, List<int> approversList)
        {
            int COUNTER_INCREMENT = 1;
            int COUNTER_DECREMENT = 2;
            try
            {
                if (approverLevel == (int)LevelEntity.DEPT)
                    return true;

                if (approversList != null)
                {
                    vrmUser approver = null;
                    foreach (int approverID in approversList)
                    {
                        approver = m_IuserDao.GetByUserId(approverID);
                        if (approver != null)
                        {
                            int appCount = approver.approverCount;

                            if (operatingFlag == COUNTER_INCREMENT)
                                approver.approverCount = appCount + 1;
                            else if (operatingFlag == COUNTER_DECREMENT)
                                approver.approverCount = appCount - 1;

                            // In case the approver count goes below zero, set it back to zero
                            // This is just a fault-tolerance mechanism
                            if (approver.approverCount < 0)
                                approver.approverCount = 0;

                            m_IuserDao.SaveOrUpdate(approver);
                        }
                        approver = null;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("systemException", e);
                return false;
            }
        }
        #endregion
        /* Organization Module - end */

        #region BlockAllUsers
        public bool BlockAllUsers(ref vrmDataObject obj)
        {
            Int32 strExec = -1;
            ns_SqlHelper.SqlHelper sqlCon = null;
            String updtqry = "";
            String succs = "0";
            try
            {
                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);

                sqlCon.OpenConnection();
                updtqry = "Update USR_LIST_D set LockStatus = 3, LockcntTrns = 10 where UserID <> 11 and LockStatus <> 3"; //FB 1606
                strExec = sqlCon.ExecuteNonQuery(updtqry);

                if (strExec > 0)
                    succs = "1";
                obj.outXml = "<success>" + succs + "</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            finally  //FB 1606
            {
                if (sqlCon != null)
                {
                    sqlCon.CloseConnection();
                    sqlCon = null;
                }
            }
        }
        #endregion

        #region UNBlockAllUsers
        public bool UnBlockAllUsers(ref vrmDataObject obj)
        {
            Int32 strExec = -1;
            ns_SqlHelper.SqlHelper sqlCon = null;
            String updtqry = "";
            String succs = "0";
            try
            {
                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);

                sqlCon.OpenConnection();
                
                updtqry = "Update USR_LIST_D set LockStatus=0, LockcntTrns=0 where UserID <> 11 and LockcntTrns=10 and LockStatus=3";
                
                strExec = sqlCon.ExecuteNonQuery(updtqry);

                if (strExec > 0)
                    succs = "1";
                obj.outXml = "<success>" + succs + "</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            finally  //FB 1606
            {
                if (sqlCon != null)
                {
                    sqlCon.CloseConnection();
                    sqlCon = null;
                }
            }
        }
        #endregion

        #region GetAudioUserList
        public bool GetAudioUserList(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                List<ICriterion> criterionList = new List<ICriterion>();

                criterionList.Add(Expression.Eq("companyId", organizationID));//Organization Module Fixes

                ICriterion criterium = null;

                
                if (multiDepts == 1)    //Organization Module Fixes
                {
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(userID));
                    if (getDeptLevelCriterionforAudiousers(user, ref criterium))
                    {
                        criterionList.Add(criterium);
                    }
                }
                

                IList userList = new List<vrmBaseUser>();
               
                userList = m_IuserDao.GetByCriteria(criterionList);
                obj.outXml = "<users>";
                foreach (vrmBaseUser user in userList)
                {
                    if (user.Audioaddon == "1" && user.LockStatus == vrmUserStatus.USER_ACTIVE)
                    {
                        obj.outXml += "<user>";
                        obj.outXml += "<userID>" + user.userid + "</userID>";
                        obj.outXml += "<firstName>" + user.FirstName + "</firstName>";
                        obj.outXml += "<lastName>" + user.LastName + "</lastName>";
                        obj.outXml += "<userEmail>" + user.Email + "</userEmail>";
                        obj.outXml += "<login>" + user.Login + "</login>";
                        obj.outXml += "<audioaddon>" + user.Audioaddon + "</audioaddon>";
                        
                        //FB 2341 - Start
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        criterionList1.Add(Expression.Eq("endpointid", user.endpointId));
                        criterionList1.Add(Expression.Eq("deleted", 0));
                        List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList1);

                        obj.outXml += "<audioDialIn>" + epList[0].address + "</audioDialIn>";
                        obj.outXml += "<confCode>" + epList[0].ConferenceCode + "</confCode>";
                        obj.outXml += "<leaderPin>" + epList[0].LeaderPin + "</leaderPin>";
                        //obj.outXml += "<audioDialIn>" +  user .IPISDNAddress + "</audioDialIn>";
                        //obj.outXml += "<confCode>" + user.ConferenceCode + "</confCode>";
                        //obj.outXml += "<leaderPin>" + user.LeaderPin + "</leaderPin>";
                        //FB 2341 - End
                        
                        //FB 2227 Starts
                        obj.outXml += "<IntVideoNum>" + user.InternalVideoNumber + "</IntVideoNum>";
                        obj.outXml += "<ExtVideoNum>" + user.ExternalVideoNumber + "</ExtVideoNum>"; 
                        //FB 2227 Ends
                        obj.outXml += "</user>";
                    }
                }

                
                obj.outXml += "</users>";
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region getDeptLevelCriterion
        private bool getDeptLevelCriterionforAudiousers(vrmUser user, ref ICriterion criterium)
        {
            String deptdID = "";
            try
            {
                // superadmin, nothing to do!
                if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                    return false;

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> In = new List<int>();

                criterionList.Add(Expression.Eq("userId", user.userid));
                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                foreach (vrmUserDepartment dept in deptList)
                {
                    if (deptdID == "")
                        deptdID = dept.departmentId.ToString();
                    else
                        deptdID += "," + dept.departmentId.ToString();
                }
                string stmt = "SELECT DISTINCT c.userId FROM myVRM.DataLayer.vrmUserDepartment c WHERE c.departmentId in ( " + deptdID + ")";
                IList result = m_IuserDeptDAO.execQuery(stmt);
                List<int> userList = new List<int>(); 
                if (result.Count > 0)
                {
                    foreach (object obj in result)
                    {
                        if (!userList.Contains((int)obj))
                            userList.Add((int)obj);    
                    }
                }

                criterium = Expression.In("userid", userList);
                criterium = Expression.Or(criterium, Expression.Eq("userid", 11));


                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //Method changes for ZD 100263 FB 1830 - start
        #region RequestPassword
        public bool RequestPassword(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = null;
            List<vrmUser> requestor = null; 
            emailFactory m_Email = null;
            string RequestID = "";//ZD 100263
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                String userEmail = "Its not there.";

                node = xd.SelectSingleNode("//login/emailLoginInfo/email");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        userEmail = node.InnerXml.Trim();
                }

                obj.outXml = "<error>-1</error>";

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Email",userEmail));

                requestor = m_IuserDao.GetByCriteria(criterionList);

                if (requestor != null)
                {
                    if (requestor.Count > 0)
                    {
                        m_Email = new emailFactory(ref obj);
						//ZD 100263
                        Guid messageId = System.Guid.NewGuid();
                        RequestID = messageId + requestor[0].userid.ToString();
                        requestor[0].RequestID = RequestID;
                        m_IuserDao.SaveOrUpdate(requestor[0]);
                        m_Email.PasswordRequest(requestor[0],ref obj);
                    }
                }
                    
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region RequestVRMAccount
        /// <summary>
        /// RequestVRMAccount
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RequestVRMAccount(ref vrmDataObject obj)
        {
            emailFactory m_Email = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                String fname = "", lname = "", email = "", loginname = "", addinfo = "", orgid="11";
               
                node = xd.SelectSingleNode("//requestVRMAccount/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
              
                node = xd.SelectSingleNode("//requestVRMAccount/firstName");
                if (node != null)
                    fname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/lastName");
                if (node != null)
                    lname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/email");
                if (node != null)
                    email = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/login");
                if (node != null)
                    loginname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/additionalInfo");
                if (node != null)
                    addinfo = node.InnerText.Trim();

                if (fname == "" || email == "" || lname == "")
                {
                    obj.outXml = "Input xml error";
                    return false;
                }
                StringDictionary reqVal = new StringDictionary();
                reqVal.Add("fname", fname);
                reqVal.Add("lname", lname);
                reqVal.Add("email", email);
                reqVal.Add("login", loginname);
                reqVal.Add("orgid", orgid.ToString());
                reqVal.Add("addinfo", addinfo);

                m_Email = new emailFactory(ref obj);
                m_Email.RequestVRMAccount(ref reqVal, ref obj);
            }
            catch (Exception e)
            {
                m_log.Error("RequestVRMAccount", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion
        //Method addeded for FB 1830 - end

        //ZD 100263 Start
        #region ChangePasswordRequest
        public bool ChangePasswordRequest(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmUser requestor = null;
            string encryppassword = "", userEmail = "", password = "", RequestID = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/emailLoginInfo/email");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        userEmail = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//login/emailLoginInfo/password");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        password = node.InnerXml.Trim();
                }

                if (userEmail == "" || password == "")
                    return false;

                requestor = m_IuserDao.GetByUserEmail(userEmail);

                if (requestor != null)
                {
                    cryptography.Crypto crypto = new cryptography.Crypto();
                    encryppassword = crypto.encrypt(password);
                    requestor.Password = encryppassword;
                    requestor.RequestID = "";
                    m_IuserDao.SaveOrUpdate(requestor);
                }
                else
                {
                    obj.outXml = "<error>-1</error>";
                    return false;
                }
             
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetRequestIDUser
        public bool GetRequestIDUser(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmUser requestor = null;
            string RequestID = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/emailLoginInfo/RequestID");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        RequestID = node.InnerXml.Trim();
                }

                if (RequestID == "")
                    return false;

                requestor = m_IuserDao.GetByUserRequestID(RequestID);

                if (requestor != null)
                {
                    obj.outXml = "<emailLoginInfo>";
                    obj.outXml += "<email>" + requestor.Email + "</email>";
                    obj.outXml += "</emailLoginInfo>";
                    requestor.RequestID = "";
                    m_IuserDao.SaveOrUpdate(requestor);
                }
                else
                {
                    obj.outXml = "<error>-1</error>";
                    return false;
                }
                
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //ZD 100263 End

        //Method addeded for FB 1860

        #region Get User Emails
        public bool GetUserEmails(ref vrmDataObject obj)
        {
            bool bRet = true;
            Int32 usrID = 0;
            string userID = "";
            try
            {
                

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if(node != null)
                    userID = node.InnerXml.Trim();

                Int32.TryParse(userID, out usrID);

                 if (usrID <= 0)
                 {
                     throw new Exception("Invalid User");
                 }

                vrmUser Usr = m_IuserDao.GetByUserId(usrID);


                m_emailFactory = new emailFactory(ref obj);

                m_emailFactory.GetBlockedEmails(ref Usr, ref obj);
                

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region Set User Emails BLock Status
        public bool SetUserEmailsBlockStatus(ref vrmDataObject obj)
        {
            bool bRet = true;
            Int32 usrID = 0;
            Int32 blockStatus = 0;
            string userID = "";
            String blckStatus = "0";
            DateTime blockDate = DateTime.Now;
            try
            {


                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    userID = node.InnerXml.Trim();

                Int32.TryParse(userID, out usrID);

                node = xd.SelectSingleNode("//login/MailBlocked");
                if (node != null)
                    blckStatus = node.InnerXml.Trim();

                Int32.TryParse(blckStatus, out blockStatus);

                if (usrID <= 0)
                {
                    throw new Exception("Invalid User");
                }

                vrmUser Usr = m_IuserDao.GetByUserId(usrID);

                if (Usr.MailBlocked != blockStatus)
                {
                    Usr.MailBlocked = blockStatus;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref blockDate);
                    Usr.MailBlockedDate = blockDate;
                    m_IuserDao.SaveOrUpdate(Usr);
                }

                obj.outXml = "<success>1</success>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //Method addeded for FB 1860

        // FB 1899 & 1900
        #region GetClientName
        private clientFrom GetClientName(Int32 sClient)
        {
            clientFrom cFrom = clientFrom.None;
            try
            {
                //01 - Outlook York
                //02 - Outlook Generic
                //03 - Lotus York 
                //04 - Lotus Generic
                //05 - Ipad
                //06 - Other mobile devices

                if (sClient == 0)
                    cFrom = clientFrom.None;
                else if (sClient == 01 || sClient == 02)
                    cFrom = clientFrom.Exchange;
                else if (sClient == 03 || sClient == 04)
                    cFrom = clientFrom.Lotus;
                else if (sClient == 05)
                    cFrom = clientFrom.AppleDevices;
                else if (sClient == 06)
                    cFrom = clientFrom.AndroidDevices;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }

            return cFrom;
        }
        #endregion

        //FB 1959 - Start
        #region SetPreferedRoom
        public bool SetPreferedRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                
                int userid = 0;
                userid = Int32.Parse(userID);

                node = xd.SelectSingleNode("//login/location");
                string location = node.InnerXml.Trim();

                vrmUser user = new vrmUser();
                user = (vrmUser)m_IuserDao.GetByUserId(userid);
                user.PreferedRoom = location;
                user.LastLogin = DateTime.Today;

                m_IuserDao.SaveOrUpdate(user);

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }

        #endregion

        #region GetPreferedRoom

        public bool GetPreferedRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                StringBuilder strOutXML = new StringBuilder();
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                int userid = 0;
                userid = Int32.Parse(userID);

                strOutXML.Append("<GetPreferedRoom>");
                strOutXML.Append("<userID>" + userID.ToString() + "</userID>");

                vrmUser user = new vrmUser();
                user = (vrmUser)m_IuserDao.GetByUserId(userid);
                string location = user.PreferedRoom ;

                if (location.Trim() != "" && location.Trim() != "0") //FB 2124
                {
                    String[] strAry = null;
                    if (location.IndexOf(',') > 0)
                        strAry = location.Split(',');
                    else if (location != "0")
                    {
                        strAry = new String[1];//WhygoDoubt-2594
                        strAry[0] = location;
                    }

                    List<vrmRoom> locRooms = null;
                    vrmUser userInfo = null;

                    strOutXML.Append("<Rooms>");
                    for (Int32 r = 0; r < strAry.Length; r++)
                    {
                        if (Convert.ToInt32(strAry[r].ToString()) == 0)
                            continue;

                        List<ICriterion> criterionListorg = new List<ICriterion>();
                        if (Convert.ToInt32(strAry[r].ToString()) > 0)
                            criterionListorg.Add(Expression.Eq("RoomID", Convert.ToInt32(strAry[r].ToString())));

                        locRooms = m_IRoomDAO.GetByCriteria(criterionListorg);

                        foreach (vrmRoom locRoom in locRooms)
                        {
                            if (locRoom.Name != "Phantom Room")
                            {
                                strOutXML.Append("<Room>");
                                strOutXML.Append("<RoomID>" + locRoom.roomId.ToString() + "</RoomID>");
                                strOutXML.Append("<RoomName>" + locRoom.Name + "</RoomName>");
                                strOutXML.Append("<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>");
                                strOutXML.Append("<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>");
                                strOutXML.Append("<MaximumConcurrentPhoneCalls>" + locRoom.MaxPhoneCall.ToString() + "</MaximumConcurrentPhoneCalls>");
                                strOutXML.Append("<SetupTime>" + locRoom.SetupTime.ToString() + "</SetupTime>");
                                strOutXML.Append("<TeardownTime>" + locRoom.TeardownTime.ToString() + "</TeardownTime>");
                                strOutXML.Append("<AssistantInchargeID>" + locRoom.assistant.ToString() + "</AssistantInchargeID>");
                                userInfo = m_IuserDao.GetByUserId(locRoom.assistant);
                                if (userInfo != null)
                                    strOutXML.Append("<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>");
                                else
                                    strOutXML.Append("<AssistantInchargeName></AssistantInchargeName>");

                                strOutXML.Append("<MultipleAssistantEmails>" + locRoom.notifyemails + "</MultipleAssistantEmails>");
                                if (locRoom.tier2 != null)
                                {
                                    if (locRoom.tier2.tier3 != null)
                                    {
                                        strOutXML.Append("<Tier1ID>" + locRoom.tier2.tier3.ID.ToString() + "</Tier1ID>");
                                        strOutXML.Append("<Tier1Name>" + locRoom.tier2.tier3.Name + "</Tier1Name>");
                                    }
                                    else
                                    {
                                        strOutXML.Append("<Tier1ID></Tier1ID>");
                                        strOutXML.Append("<Tier1Name></Tier1Name>");
                                    }
                                    strOutXML.Append("<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>");
                                    strOutXML.Append("<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>");
                                }
                                else
                                {
                                    strOutXML.Append("<Tier1ID></Tier1ID>");
                                    strOutXML.Append("<Tier1Name></Tier1Name>");
                                    strOutXML.Append("<Tier2ID></Tier2ID>");
                                    strOutXML.Append("<Tier2Name></Tier2Name>");
                                }
                                strOutXML.Append("<CatererFacility>" + locRoom.Caterer.ToString() + "</CatererFacility>");
                                strOutXML.Append("<DynamicRoomLayout>" + locRoom.DynamicRoomLayout.ToString() + "</DynamicRoomLayout>");
                                strOutXML.Append("<ProjectorDefault>" + locRoom.ProjectorAvailable.ToString() + "</ProjectorDefault>");
                                strOutXML.Append("<Video>" + locRoom.VideoAvailable.ToString() + "</Video>");
                                strOutXML.Append("<Floor>" + locRoom.RoomFloor + "</Floor>");
                                strOutXML.Append("<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>");
                                strOutXML.Append("<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>");
                                strOutXML.Append("<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>");
                                strOutXML.Append("<City>" + locRoom.City + "</City>");
                                strOutXML.Append("<State>" + locRoom.State.ToString() + "</State>");
                                strOutXML.Append("<Disabled>" + locRoom.disabled.ToString() + "</Disabled>");
                                strOutXML.Append("<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>");
                                strOutXML.Append("<ZipCode>" + locRoom.Zipcode + "</ZipCode>");
                                strOutXML.Append("<Country>" + locRoom.Country.ToString() + "</Country>");
                                strOutXML.Append("<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>");
                                strOutXML.Append("<Longitude>" + locRoom.Longitude + "</Longitude>");
                                strOutXML.Append("<Latitude>" + locRoom.Latitude + "</Latitude>");
                                strOutXML.Append("<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>");

                                int appCnt = 0, j = 1;
                                if (locRoom.locationApprover.Count > 0)
                                    appCnt = 1;

                                strOutXML.Append("<Approval>" + appCnt + "</Approval>");
                                strOutXML.Append("<Approvers>");
                                for (j = 1; j < locRoom.locationApprover.Count; j++)
                                {
                                    vrmLocApprover locRoomApprov = locRoom.locationApprover[j];
                                    userInfo = m_IuserDao.GetByUserId(locRoomApprov.approverid);
                                    strOutXML.Append("<Approver" + j.ToString() + "ID>" + locRoomApprov.approverid.ToString() + "</Approver" + j.ToString() + "ID>");
                                    strOutXML.Append("<Approver" + j.ToString() + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "  </Approver" + j.ToString() + "Name>");
                                }
                                if (j == 1)
                                {
                                    strOutXML.Append("<Approver1ID></Approver1ID>");
                                    strOutXML.Append("<Approver1Name></Approver1Name>");
                                    strOutXML.Append("<Approver2ID></Approver2ID>");
                                    strOutXML.Append("<Approver2Name></Approver2Name>");
                                    strOutXML.Append("<Approver3ID></Approver3ID>");
                                    strOutXML.Append("<Approver3Name></Approver3Name>");
                                    strOutXML.Append("<ApprovalReq>No</ApprovalReq>");
                                }
                                else if (j == 2)
                                {
                                    strOutXML.Append("<Approver2ID></Approver2ID>");
                                    strOutXML.Append("<Approver2Name></Approver2Name>");
                                    strOutXML.Append("<Approver3ID></Approver3ID>");
                                    strOutXML.Append("<Approver3Name></Approver3Name>");
                                    strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                                }
                                else if (j == 3)
                                {
                                    strOutXML.Append("<Approver3ID></Approver3ID>");
                                    strOutXML.Append("<Approver3Name></Approver3Name>");
                                    strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                                }
                                strOutXML.Append("</Approvers>");
                                strOutXML.Append("</Room>");
                            }
                        }
                    }

                    strOutXML.Append("</Rooms>");
                }
                strOutXML.Append("</GetPreferedRoom>");

                obj.outXml = strOutXML.ToString();

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }

        #endregion
        //FB 1959 - End

        //FB 2027 - Start
        #region SetUserRoles
        /// <summary>
        /// SetUserRoles (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUserRoles(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            ArrayList activeRoles = new ArrayList();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//userRole/roles/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                XmlElement root = xd.DocumentElement;
                XmlNodeList rlist = root.SelectNodes(@"/userRole/roles/role");
                for (int i = 0; i < rlist.Count; i++)
                {
                    String id, name, menumask;
                    node = rlist[i].SelectSingleNode("ID");
                    id = node.InnerXml.Trim();
                    node = rlist[i].SelectSingleNode("name");
                    name = node.InnerXml.Trim();
                    node = rlist[i].SelectSingleNode("menuMask");
                    menumask = node.InnerXml.Trim();
                    if (id.ToLower() == "new")
                    {
                        vrmUserRoles role = new vrmUserRoles();
                        role.roleID = getMaxRoleID();
                        role.roleName = name;
                        role.roleMenuMask = menumask;
                        role.level = 0;
                        role.crossaccess = 0;
                        role.createType = 2;
                        role.DisplayOrder = 0;//FB 2891
                        m_IUserRolesDao.Save(role);
                        activeRoles.Add(role.roleID);
                    }
                    else
                    {
                        int rid;
                        Int32.TryParse(id, out rid);
                        vrmUserRoles rle = m_IUserRolesDao.GetById(rid);
                        if (rle != null)
                        {
                            rle.roleName = name;
                            rle.roleMenuMask = menumask;
                            m_IUserRolesDao.SaveOrUpdate(rle);
                            activeRoles.Add(rle.roleID);

                            criterionList1 = new List<ICriterion>();
                            criterionList1.Add(Expression.Eq("roleID", rid));
                            List<vrmUser> usrRole = m_IuserDao.GetByCriteria(criterionList1);
                            for (int l = 0; l < usrRole.Count; l++)
                            {
                                usrRole[l].MenuMask = menumask;
                                m_IuserDao.SaveOrUpdate(usrRole[l]);
                            }
                            List<vrmInactiveUser> inactiveURole = m_IinactiveUserDao.GetByCriteria(criterionList1);
                            for (int k = 0; k < inactiveURole.Count; k++)
                            {
                                inactiveURole[k].MenuMask = menumask;
                                m_IinactiveUserDao.SaveOrUpdate(inactiveURole[k]);
                            }
                        }
                    }
                }
                List<vrmUserRoles> roles = m_IUserRolesDao.GetAll();
                for (int i = 0; i < roles.Count; i++)
                {
                    if (!activeRoles.Contains(roles[i].roleID))
                    {
                        if (roles[i].locked == 0)
                        {
                            m_IUserRolesDao.Delete(roles[i]);
                            roles.Remove(roles[i]);
                        }
                    }
                }
                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region getMaxRoleID
        /// <summary>
        /// getMaxRoleID (COM to .Net conversion)
        /// </summary>
        /// <returns></returns>
        private int getMaxRoleID()
        {
            try
            {
                string qString = "select max(rle.roleID) from myVRM.DataLayer.vrmUserRoles rle";

                IList list = m_IUserRolesDao.execQuery(qString);

                string sMax;
                if (list[0] != null)
                    sMax = list[0].ToString();
                else
                    sMax = "0";

                int id = Int32.Parse(sMax);
                id += 1;
                return id;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion
                
        #region SearchUserOrGuest
        /// <summary>
        /// SearchUserOrGuest (COM to .Net conversion) 
        /// </summary>
        /// <param name="obj"></param>
        public bool SearchUserOrGuest(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, superadmin = 0, type = 0;
            String orgid = "", firstName = "", lastName = "", email = "";
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            try
            {
                
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/user/firstName");
                if (node != null)
                    firstName = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/user/lastName");
                if (node != null)
                    lastName = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/user/email");
                if (node != null)
                    email = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/user/type");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out type);
                
                vrmUser user = m_IuserDao.GetByUserId(userid);
                superadmin = user.Admin;

                if (superadmin == 2)
                    superadmin = 1;
                else
                    superadmin = 0;
                outXML.Append("<users>");
                outXML.Append("<superAdmin>" + superadmin + "</superAdmin>");
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("companyId", organizationID));
                if (firstName.Length > 0)
                    criterionList.Add(Expression.Like("FirstName", "%%" + firstName.ToLower() + "%%").IgnoreCase());
                if (lastName.Length > 0)
                    criterionList.Add(Expression.Like("LastName", "%%" + lastName.ToLower() + "%%").IgnoreCase());
                if (email.Length > 0)
                    criterionList.Add(Expression.Like("Email", "%%" + email.ToLower() + "%%").IgnoreCase());                
                
                IList usrFilt = new List<vrmBaseUser>();
                if (type == 2)
                    usrFilt = m_IGuestUserDao.GetByCriteria(criterionList);
                else
                {   //FB 2704
                    criterionList.Add(Expression.Eq("Audioaddon", "0"));//FB 2673
                    usrFilt = m_IuserDao.GetByCriteria(criterionList);
                }
                crypto = new cryptography.Crypto();
                foreach (vrmBaseUser uf in usrFilt)
                {
                    outXML.Append("<user>");
                    outXML.Append("<userID>" + uf.userid + "</userID>");
                    outXML.Append("<firstName>" + uf.FirstName + "</firstName>");
                    outXML.Append("<lastName>" + uf.LastName + "</lastName>");
                    outXML.Append("<login>" + uf.Login + "</login>");
                    if (uf.Password != null && uf.Password != "") //FB 2318
                        outXML.Append("<password>" + crypto.decrypt(uf.Password) + "</password>");
                    else
                        outXML.Append("<password></password>");
                    outXML.Append("<email>" + uf.Email + "</email>");
                    outXML.Append("<status>");
                    outXML.Append("<level>" + uf.Admin + "</level>");
                    outXML.Append("<deleted>" + uf.Deleted + "</deleted>");
                    outXML.Append("<locked>" + uf.LockStatus + "</locked>");
                    if (type == 2)
                        outXML.Append("<crossaccess>0</crossaccess>");
                    else
                    {
                        vrmUserRoles role = m_IUserRolesDao.GetById(uf.roleID);
                        outXML.Append("<crossaccess>" + role.crossaccess + "</crossaccess>");
                    }
                    outXML.Append("</status>");
                    outXML.Append("</user>");
                }
                outXML.Append("</users>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
		
		
        // mode = 0: Normal mode returns only basic user information
        // mode = 1: In addition to basic information, returns account
        //			usage report. The table should be "Usr_List_D" (active VRM
        //			user) for this mode
        // mode = 2: In addition to basic information, returns status
        //			information. The table should be "Usr_List_D" (active VRM
        //			user) or Guest for this mode
        //
        // dept = 0 Return user list based on roles (ie super admin sess all
        //          admin sees at dept(s) level and user sees only their entries
        // dept = 1 For GetManageUser (conf create) user can see all in dept
        //

        #region GetUserList
        private bool GetUserList(int userID, int type, int pageno, ref int totpages, string alphabet, int sortBy, int mode, ref string outXml, int orgId, int alphabetmode, int deptUsers, string BulkUser) //FB 2269
        {
            bool bRet = true;
            int adminaccess = 0; //ZD 100263
            try
            {

                int numrows = 0, numrecs = 0, audiobridgerec= 0; //FB 2023
                int isDeptUser = 1; //FB 2269
                bool thresholdpassed = false;
                StringBuilder outUser = new StringBuilder();//FB 2027

                // this routing must work for LDAP also (not implemented yet)
                // when getting LDAP count newuser = 1

                int admin = 0;

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                //FB 2269
                if (deptUsers == 0)
                    isDeptUser = orgInfo.isDeptUser;

                vrmUser user = m_IuserDao.GetByUserId(userID);

                //ZD 100263
                if (user.roleID != 0)
                {
                    vrmUserRoles roles = new vrmUserRoles();
                    roles = m_IUserRolesDao.GetById((int)user.roleID);
                    adminaccess = roles.crossaccess;

                }
                //ZD 100263

                // Spoof admin if this is an LDAP lookup (this is only visible to admmins) [AG 5-19-08]
                if (type == m_isUser)
                {
                    if (multiDepts == 1)
                        admin = user.Admin;
                    else
                        admin = vrmUserConstant.SUPER_ADMIN;
                }
                else
                {
                    admin = vrmUserConstant.SUPER_ADMIN;
                }

                List<ICriterion> criteriumList = new List<ICriterion>();
                List<ICriterion> citeriaOrg = new List<ICriterion>();
                List<ICriterion> AudiobridgescritList = new List<ICriterion>(); //FB 2023
                List<vrmConfUser> confuserList = new List<vrmConfUser>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
                List<int> userIn = new List<int>();

                criteriumList.Add(Expression.Eq("companyId", organizationID));
                citeriaOrg.Add(Expression.Eq("companyId", organizationID));
                AudiobridgescritList.Add(Expression.Eq("companyId", organizationID)); //FB 2023

                string a_Order = null;
                string checkAlpha = alphabet.ToLower();
                switch (sortBy)
                {
                    case 1:
                        a_Order = "FirstName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("FirstName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("FirstName", alphabet + "%%"));
                        }
                        break;
                    case 3:
                        a_Order = "Login";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Login", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Login", alphabet + "%%"));
                        }
                        break;
                    case 4:
                        a_Order = "Email";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Email", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Email", alphabet + "%%"));
                        }
                        break;
                    default:
                        a_Order = "LastName";
                        if (checkAlpha != "all")
                        {
                            if (alphabetmode == 1 && checkAlpha == "a")
                                criteriumList.Add(Expression.Sql("(SUBSTRING(LastName, 1, 1) <= 'a')"));
                            else if (checkAlpha == "a" && alphabet == "a" && alphabetmode == 0)
                                criteriumList.Add(Expression.Sql("(SUBSTRING(LastName, 1, 1) <= 'a')"));
                            else if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("LastName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("LastName", alphabet + "%%"));
                        }
                        break;

                }

                if (alphabetmode == 1)
                {
                    criteriumList.Add(Expression.Eq("Deleted", 0));
                    citeriaOrg.Add(Expression.Eq("Deleted", 0));
                    AudiobridgescritList.Add(Expression.Eq("Deleted", 0)); //FB 2023
                }

                if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                {
                    criteriumList.Add(Expression.Eq("Audioaddon", isAudioAddOn.ToString()));
                    AudiobridgescritList.Add(Expression.Eq("Audioaddon", isAudioAddOn.ToString()));
                }
                if (isDeptUser == 1)
                {
                    if (type == m_isUser)
                    {
                        if (admin == 1)
                        {
                            List<ICriterion> criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("userId", user.userid));
                            deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                            foreach (vrmUserDepartment dept in deptList)
                            {
                                deptIn.Add(dept.departmentId);
                            }
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.In("departmentId", deptIn));
                            deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                            foreach (vrmUserDepartment dept in deptList)
                            {
                                userIn.Add(dept.userId);
                            }
                        }
                        if (admin == 0)
                        {
                            List<ICriterion> CriterionNew = new List<ICriterion>();
                            List<ICriterion> criti = new List<ICriterion>();
                            List<vrmConference> confList = new List<vrmConference>();
                            List<int> confIn = new List<int>();
                            CriterionNew.Add(Expression.Eq("owner", userID));
                            CriterionNew.Add(Expression.Eq("orgId", organizationID));
                            confList = m_IConferenceDAO.GetByCriteria(CriterionNew);
                            vrmConference ConfUserID = null;
                            for (int i = 0; i < confList.Count; i++)
                            {
                                ConfUserID = confList[i];
                                confIn.Add(ConfUserID.confid);
                            }
                            criti.Add(Expression.In("confid", confIn));
                            confuserList = m_IConfUserDAO.GetByCriteria(criti);
                            vrmConfUser Conf = null;
                            for (int i = 0; i < confuserList.Count; i++)
                            {
                                Conf = confuserList[i];
                                userIn.Add(Conf.userid);
                            }
                            userIn.Add(11);
                            userIn.Add(userID);

                            if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN)
                            {
                                List<ICriterion> criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.Eq("userId", user.userid));
                                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                                foreach (vrmUserDepartment dept in deptList)
                                {
                                    deptIn.Add(dept.departmentId);
                                }
                                criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.In("departmentId", deptIn));
                                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                                foreach (vrmUserDepartment dept in deptList)
                                {
                                    userIn.Add(dept.userId);
                                }
                            }
                            if (BulkUser != "" && BulkUser == "0")
                            {
                                criteriumList.Add(Expression.In("userid", userIn));//ZD 100263
                                criteriumList.Add(Expression.Not(Expression.Eq("Admin", 2)));
                            }
                            else
                            {
                                criteriumList.Add(Expression.Or(Expression.Eq("Admin", 2), Expression.In("userid", userIn)));
                            }
                            citeriaOrg.Add(Expression.Or(Expression.Eq("Admin", 2), Expression.In("userid", userIn)));
                            AudiobridgescritList.Add(Expression.Or(Expression.Eq("Admin", 2), Expression.In("userid", userIn))); //FB 2023
                        }
                    }
                }

                int iPageNo = pageno;
                if (iPageNo == 0)
                    iPageNo = 1;
                long ttlRecords = 0;

                IList userList = new List<vrmBaseUser>();
                switch (type)
                {
                    case 1:
                        m_IuserDao.addProjection(Projections.RowCount());
                        IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                        Int32.TryParse((list[0].ToString()), out numrows);
                        list = m_IuserDao.GetObjectByCriteria(citeriaOrg);
                        Int32.TryParse((list[0].ToString()), out numrecs);

                        if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                        {
                            list = m_IuserDao.GetObjectByCriteria(AudiobridgescritList);
                            Int32.TryParse((list[0].ToString()), out audiobridgerec);
                        }

                        m_IuserDao.clearProjection();

                        m_IuserDao.pageSize(m_PAGE_SIZE);
                        m_IuserDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IuserDao.CountByCriteria(criteriumList);
                        m_IuserDao.addOrderBy(Order.Asc(a_Order));

                        userList = m_IuserDao.GetByCriteria(criteriumList);
                        break;
                    case 2:
                        m_IldapDao.addProjection(Projections.RowCount());
                        IList listL = m_IldapDao.GetObjectByCriteria(new List<ICriterion>());
                        Int32.TryParse((listL[0].ToString()), out numrows);
                        listL = m_IldapDao.GetObjectByCriteria(citeriaOrg);
                        Int32.TryParse((listL[0].ToString()), out numrecs);
                        m_IldapDao.clearProjection();

                        m_IldapDao.pageSize(m_PAGE_SIZE);
                        m_IldapDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IldapDao.CountByCriteria(criteriumList);
                        m_IldapDao.addOrderBy(Order.Asc(a_Order));
                        userList = m_IldapDao.GetByCriteria(criteriumList);
                        break;
                    case 3:
                        m_IGuestUserDao.addProjection(Projections.RowCount());
                        IList listG = m_IGuestUserDao.GetObjectByCriteria(new List<ICriterion>());
                        Int32.TryParse((listG[0].ToString()), out numrows);
                        listG = m_IGuestUserDao.GetObjectByCriteria(citeriaOrg);
                        Int32.TryParse((listG[0].ToString()), out numrecs);
                        m_IGuestUserDao.clearProjection();

                        m_IGuestUserDao.pageSize(m_PAGE_SIZE);
                        m_IGuestUserDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IGuestUserDao.CountByCriteria(criteriumList);
                        m_IGuestUserDao.addOrderBy(Order.Asc(a_Order));
                        userList = m_IGuestUserDao.GetByCriteria(criteriumList);
                        break;
                    case 4:
                        m_IinactiveUserDao.addProjection(Projections.RowCount());
                        IList listI = m_IinactiveUserDao.GetObjectByCriteria(new List<ICriterion>());
                        Int32.TryParse((listI[0].ToString()), out numrows);

                        listI = m_IinactiveUserDao.GetObjectByCriteria(citeriaOrg);
                        Int32.TryParse((listI[0].ToString()), out numrecs);

                        if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                        {
                            listI = m_IuserDao.GetObjectByCriteria(AudiobridgescritList);
                            Int32.TryParse((listI[0].ToString()), out audiobridgerec);
                        }

                        m_IinactiveUserDao.clearProjection();

                        m_IinactiveUserDao.pageSize(m_PAGE_SIZE);
                        m_IinactiveUserDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IinactiveUserDao.CountByCriteria(criteriumList);
                        m_IinactiveUserDao.addOrderBy(Order.Asc(a_Order));
                        userList = m_IinactiveUserDao.GetByCriteria(criteriumList);
                        break;
                }

                totpages = numrows / m_PAGE_SIZE;
                if ((numrows % m_PAGE_SIZE) > 0)
                    totpages++;

                int ttlPages = (int)(ttlRecords / m_PAGE_SIZE);

                // and modulo remainder...
                if (ttlRecords % m_PAGE_SIZE > 0)
                    ttlPages++;

                outUser.Append("<users>");
                if (userList != null || userList.Count < 0)
                {
                    vrmBaseUser userL = null;
                    for(int i=0;i < userList.Count;i++)
                    {
                        userL = (vrmBaseUser)userList[i];

                        //ZD 100263 Start
                        string crossaccess = "0";
                        int level = 0;
                        if (userL.roleID != 0)
                        {
                            vrmUserRoles roles = new vrmUserRoles();
                            roles = m_IUserRolesDao.GetById((int)userL.roleID);
                            crossaccess = roles.crossaccess.ToString();
                            level = roles.level;
                        }

                        if (adminaccess != 1 && BulkUser != "" && BulkUser == "0")
                            if (crossaccess == "1" && level == 2)
                                continue;

                        //ZD 100263 End

                        outUser.Append("<user>");
                        outUser.Append("<userID>" + userL.userid + "</userID>");
                        outUser.Append("<firstName>" + userL.FirstName + "</firstName>");
                        outUser.Append("<lastName>" + userL.LastName + "</lastName>");
                        outUser.Append("<login>" + userL.Login + "</login>");
                        
                        if(type == m_isInActiveUser && userL.Audioaddon.Trim() == "1") //FB 2023
                            outUser.Append("<email></email>");
                        else
                            outUser.Append("<email>" + userL.Email + "</email>");
                           
                        outUser.Append("<telephone>" + userL.Telephone + "</telephone>");
                        if (userL.Password != null)
                            outUser.Append("<password>" + userL.Password + "</password>");
                        else
                            outUser.Append("<password></password>");
                        outUser.Append("<externalBridgeNumber>" + userL.ExternalVideoNumber + "</externalBridgeNumber>"); //FB 2376
                        outUser.Append("<internalBridgeNumber>" + userL.InternalVideoNumber + "</internalBridgeNumber>"); //FB 2376

                        if (mode == 1)
                        {
                            IUserAccountDao IuserAccountDAO = m_userDAO.GetUserAccountDao();
                            vrmAccount uAccount = IuserAccountDAO.GetByUserId(userL.userid); //FB 2027

                            outUser.Append("<report>");
                            outUser.Append("<total>" + uAccount.TotalTime.ToString() + "</total>");
                            long timeUsed = uAccount.TotalTime - uAccount.TimeRemaining;

                            outUser.Append("<used>" + timeUsed.ToString() + "</used>");
                            // used = totalAmount- left
                            outUser.Append("<left>" + uAccount.TimeRemaining.ToString() + "</left>");
                            outUser.Append("</report>");
                        }
                        else if (mode == 2)
                        {
                            string lockstatus = "0";
                            string deleted = "0";
                            int Roleid = 0;
                            

                            if (userL.LockStatus != 0)
                                lockstatus = "1";

                            if (userL.Deleted != 0) // Deleted
                                deleted = "1";

                            if (userL.roleID.ToString() != null)
                                Roleid = userL.roleID;

                            outUser.Append("<Survey>" + userL.SendSurveyEmail.ToString() + "</Survey>");//FB 2348
                            outUser.Append("<status>");
                            outUser.Append("<level>" + userL.Admin.ToString() + "</level>");
                            outUser.Append("<deleted>" + deleted + "</deleted>");
                            outUser.Append("<locked>" + lockstatus + "</locked>");
                            outUser.Append("<crossaccess>" + crossaccess + "</crossaccess>");
                            outUser.Append("</status>");
                        }
                        outUser.Append("</user>");
                    }
                    outUser.Append("<pageNo>" + pageno.ToString() + "</pageNo>");
                    outUser.Append("<totalPages>" + ttlPages.ToString() + "</totalPages>");
                    outUser.Append("<sortBy>" + sortBy + "</sortBy>");
                    outUser.Append("<alphabet>" + alphabet + "</alphabet>");
                    
                    if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                        outUser.Append("<totalNumber>" + audiobridgerec + "</totalNumber>");
                    else
                        outUser.Append("<totalNumber>" + numrecs + "</totalNumber>");

                    if (thresholdpassed)
                        outUser.Append("<canAll>0</canAll>");
                    else
                        outUser.Append("<canAll>1</canAll>");

                    if (type != m_isLDAP)
                    {
                        OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                        int usersRemain = orgdt.UserLimit - numrecs;
                        outUser.Append("<licensesRemain>" + usersRemain.ToString() + "</licensesRemain>");
                    }
                }
                outUser.Append("</users>");
                outXml = outUser.ToString();

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region RetrieveUsers
        /// <summary>
        /// GetGuestList - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RetrieveUsers(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                string firstname = "";
                string lastname = "";
                string login = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/name/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/name/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("/login/name/userName");
                if (node != null)
                    login = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);

                if (lastname != "")
                {
                    sortBy = 2;
                    alphabet = lastname;
                }
                else if (firstname != "")
                {
                    sortBy = 1;
                    alphabet = firstname;
                }
                else
                {
                    sortBy = 3;
                    alphabet = login;
                }

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml, organizationID, 1, deptUsers, BulkUser)) //FB 2269
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region RetrieveGuest
        /// <summary>
        /// RetrieveGuest - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RetrieveGuest(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                string firstname = "";
                string lastname = "";
                string login = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/name/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/name/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("/login/name/userName");
                if (node != null)
                    login = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);

                if (lastname != "")
                {
                    sortBy = 2;
                    alphabet = lastname;
                }
                else if (firstname != "")
                {
                    sortBy = 1;
                    alphabet = firstname;
                }
                else
                {
                    sortBy = 3;
                    alphabet = login;
                }

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isGustUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml, organizationID, 1, deptUsers, BulkUser)) //FB 2269
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

		 #region GetManageGuest
        /// <summary>
        /// GetManageGuest - COM to .NET Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetManageGuest(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out sortBy);

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isGustUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml, organizationID, 0, deptUsers, BulkUser))//FB 2269
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetUsers
        /// <summary>
        /// GetUsers - COM to .NET Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetUsers(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out sortBy);

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isInActiveUser, pageNo, ref totPages, alphabet, sortBy, 0, ref outXml, organizationID, 0, deptUsers, BulkUser)) //FB 2269
                {
                    string Out = "<userInfo>" + outXml + "</userInfo>";
                    obj.outXml = Out;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SetOldUser
        /// <summary>
        /// SetOldUser (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetOldUser(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userid");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                vrmUser user = m_IuserDao.GetByUserId(userid);
                user.newUser = 0;
                m_IuserDao.Update(user);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetUserPreference
        /// <summary>
        /// GetUserPreference (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetUserPreference(ref vrmDataObject obj)
        {
            string lang = "";
            StringBuilder addr = new StringBuilder();
            string video = "";
            string group = "";
            string lot = "";
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            List<ICriterion> criterionList3 = new List<ICriterion>();
            StringBuilder outXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                outXml.Append("<userPreference>");
                criterionList.Add(Expression.Eq("userid", userid));
                List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList);
                outXml.Append("<userName>");
                outXml.Append("<firstName>" + userList[0].FirstName.ToString() + "</firstName>");
                outXml.Append("<lastName>" + userList[0].LastName.ToString() + "</lastName>");
                outXml.Append("</userName>");
                outXml.Append("<login>" + userList[0].Login.ToString() + "</login>");
                crypto = new cryptography.Crypto();
                if (userList[0].Password != null)
                    outXml.Append("<password>" + crypto.decrypt(userList[0].Password.ToString()) + "</password>");
                else
                    outXml.Append("<password></password>");
                outXml.Append("<userEmail>" + userList[0].Email.ToString() + "</userEmail>");
                outXml.Append("<alternativeEmail>" + userList[0].AlternativeEmail.ToString() + "</alternativeEmail>");
                outXml.Append("<sendBoth>" + userList[0].DoubleEmail.ToString() + "</sendBoth>");
                outXml.Append("<emailClient>" + userList[0].EmailClient.ToString() + "</emailClient>"); //0/1/2; 0: none, 1: Outlook, 2: Lotus
                outXml.Append("<timeZone>" + userList[0].TimeZone.ToString() + "</timeZone>");
                outXml.Append("<languageID>" + userList[0].Language.ToString() + "</languageID>");
                if (OldFetchLanguageList(obj, ref lang))
                    outXml.Append(lang);
                outXml.Append("<timezones>");
                int tzsid;
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);
                tzsid = orgInfo.tzsystemid;
                if (tzsid == 0)
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        outXml.Append("<timezone>");
                        outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                        outXml.Append("<timezoneName>" + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                        outXml.Append("</timezone>");
                    }
                }
                else
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        if (orgInfo.tzsystemid == timeZoneList[t].systemID)
                        {
                            outXml.Append("<timezone>");
                            outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                            outXml.Append("<timezoneName>" + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                            outXml.Append("</timezone>");
                        }
                    }
                }
                outXml.Append("</timezones>");
                outXml.Append("<locationList><selected><level1ID>" + userList[0].PreferedRoom.ToString() + "</level1ID></selected></locationList>");
                outXml.Append("<lineRateID>" + userList[0].DefLineRate.ToString() + "</lineRateID>");
                outXml.Append("<lineRate>");
                List<vrmLineRate> lineRate = vrmGen.getLineRate();
                for (int l = 0; l < lineRate.Count; l++)
                {
                    if (userList[0].userid == userid)
                    {
                        outXml.Append("<rate>");
                        outXml.Append("<lineRateID>" + lineRate[l].Id.ToString() + "</lineRateID>");
                        outXml.Append("<lineRateName>" + lineRate[l].LineRateType.ToString() + "</lineRateName>");
                        outXml.Append("</rate>");
                    }
                }
                outXml.Append("</lineRate>");
                outXml.Append("<videoProtocol>" + GetVideoPro(userList[0].DefVideoProtocol, obj) + "</videoProtocol>");
                if (FetchAddressTypeList(obj, ref addr))
                    outXml.Append(addr);
                outXml.Append("<IPISDNAddress>" + userList[0].IPISDNAddress.ToString() + "</IPISDNAddress>");
                outXml.Append("<connectionType>" + userList[0].connectionType.ToString() + "</connectionType>");  //0: Dial_Out, 1: Dial_in
                outXml.Append("<isOutside>" + userList[0].outsidenetwork.ToString() + "</isOutside>");
                outXml.Append("<emailMask>" + userList[0].emailmask.ToString() + "</emailMask>");
                outXml.Append("<addressTypeID>" + userList[0].addresstype.ToString() + "</addressTypeID>");
                outXml.Append("<videoEquipmentID>" + userList[0].DefaultEquipmentId.ToString() + "</videoEquipmentID>");
                if (FetchVideoEquipment2(userList[0].DefaultEquipmentId.ToString(), userid, 1, obj, ref video))
                    outXml.Append(video);
                criterionList1.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userid)));
                criterionList1.Add(Expression.Eq("GroupID", userList[0].PreferedGroup));
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList1);
                for (int g = 0; g < Groups.Count; g++)
                {
                    outXml.Append("<defaultAGroups>");
                    outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                    outXml.Append("</defaultAGroups>");
                }
                criterionList2.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userid)));
                if (FetchGroups(criterionList2, 0, obj, ref group))
                    outXml.Append(group);
                outXml.Append("<multiDepartment>" + orgInfo.MultipleDepartments + "</multiDepartment>");
                outXml.Append("<departments>");
                criterionList3.Add(Expression.Eq("deleted", 0));
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                for (int d = 0; d < deptList.Count; d++)
                    criterionList3.Add(Expression.Eq("departmentId", deptList[d].departmentId));
                List<vrmDept> dept = m_IdeptDAO.GetByCriteria(criterionList3);
                for (int j = 0; j < dept.Count; j++)
                {
                    outXml.Append("<department>");
                    outXml.Append("<id>" + dept[j].departmentId.ToString() + "</id>");
                    outXml.Append("<name>" + dept[j].departmentName + "</name>");
                    outXml.Append("</department>");
                }
                outXml.Append("</departments>");
                outXml.Append("<expiryDate>" + userList[0].accountexpiry.ToString("MM/dd/yyyy") + "</expiryDate>");
                List<ICriterion> criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("BridgeID", userList[0].BridgeID));
                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionLst);
                for (int m = 0; m < MCUList.Count; m++)
                {
                    if (MCUList[m].BridgeName == "")
                    {
                        outXml.Append("<bridgeName> Unknown </bridgeName>");
                    }
                    else
                    {
                        outXml.Append("<bridgeName>" + MCUList[m].BridgeName + "</bridgeName>");
                    }
                }
                if (FetchLotusInfo(userid, obj, ref lot))
                    outXml.Append(lot);
                outXml.Append("</userPreference>");
                obj.outXml = outXml.ToString();
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region OldFetchLanguageList
        /// <summary>
        /// OldFetchLanguageList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchLanguageList(vrmDataObject obj, ref string langout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<languages>");
                List<vrmLanguage> lang = m_langDAO.GetAll();
                for (int l = 0; l < lang.Count; l++)
                {
                    outXml.Append("<language>");
                    outXml.Append("<ID>" + lang[l].Id.ToString() + "</ID>");
                    outXml.Append("<name>" + lang[l].Name.ToString() + "</name>");
                    outXml.Append("</language>");
                }
                outXml.Append("</languages>");
                obj.outXml = outXml.ToString();
                langout = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region GetVideoPro
        /// <summary>
        /// GetVideoPro (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public string GetVideoPro(int DefVideoProtocol, vrmDataObject obj)
        {
            String videoPro = "0";
            try
            {
                if (DefVideoProtocol == 0)
                    return videoPro;
                List<vrmVideoProtocol> vp_List = vrmGen.getVideoProtocols();
                for (int v = 0; v < vp_List.Count; v++)
                {
                    if (vp_List[v].Id == DefVideoProtocol)
                    {
                        videoPro = vp_List[v].VideoProtocolType;
                        return videoPro;
                    }
                    else
                    {
                        videoPro = "";
                    }
                }
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return obj.outXml;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return obj.outXml;
            }
            return videoPro;
        }
        #endregion

        #region FetchAddressTypeList
        /// <summary>
        /// FetchAddressTypeList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchAddressTypeList(vrmDataObject obj, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            try
            {
                outXml.Append("<addressType>");
                List<vrmAddressType> AddressTypeList = vrmGen.getAddressType();
                for (int a = 0; a < AddressTypeList.Count; a++)
                {
                    outXml.Append("<type>");
                    outXml.Append("<ID>" + AddressTypeList[a].Id.ToString() + "</ID>");
                    outXml.Append("<name>" + AddressTypeList[a].name.ToString() + "</name>");
                    outXml.Append("</type>");
                }
                outXml.Append("</addressType>");
                obj.outXml = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region FetchVideoEquipment2
        /// <summary>
        /// FetchVideoEquipment2 (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchVideoEquipment2(string selected, int userid, int mode, vrmDataObject obj, ref string videoout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<videoEquipment>");
                if (mode == 1)
                    outXml.Append("<selectedEquipment>" + selected + "</selectedEquipment>");
                List<vrmVideoEquipment> videoEqList = vrmGen.getVideoEquipment();
                for (int v = 0; v < videoEqList.Count; v++)
                {
                    outXml.Append("<equipment>");
                    outXml.Append("<videoEquipmentID>" + videoEqList[v].Id.ToString() + "</videoEquipmentID>");
                    outXml.Append("<videoEquipmentName>" + videoEqList[v].VEName + "</videoEquipmentName>");
                    outXml.Append("</equipment>");
                }
                outXml.Append("</videoEquipment>");
                obj.outXml = outXml.ToString();
                videoout = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region FetchGroups
        /// <summary>
        /// FetchGroups (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchGroups(List<ICriterion> criterionList2, int mode, vrmDataObject obj, ref string grpout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<groups>");
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList2);
                for (int g = 0; g < Groups.Count; g++)
                {
                    outXml.Append("<group>");
                    if (mode == 1)
                    {
                        outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                        outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                    }
                    else if (mode == 2)
                    {
                        outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                        outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                    }
                    else if (mode == 0)
                    {
                        outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                        outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                        List<ICriterion> criterionListG = new List<ICriterion>();
                        criterionListG.Add(Expression.Eq("GroupID", Groups[g].GroupID));
                        List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionListG);
                        //Count no of users in group				
                        outXml.Append("<numParticipants>" + partList.Count.ToString() + "</numParticipants>");
                        for (int p = 0; p < partList.Count; p++)
                        {
                            vrmUser partUser = m_IuserDao.GetByUserId(partList[p].UserID);
                            outXml.Append("<user>");
                            outXml.Append("<userID>" + partList[p].UserID.ToString() + "</userID>");
                            outXml.Append("<userFirstName>" + partUser.FirstName + "</userFirstName>");
                            outXml.Append("<userLastName>" + partUser.LastName + "</userLastName>");
                            outXml.Append("<userEmail>" + partUser.Email + "</userEmail>");
                            outXml.Append("<CC>" + partList[p].CC.ToString() + "</CC>");
                            outXml.Append("</user>");
                        }
                    }
                    outXml.Append("</group>");
                }
                outXml.Append("</groups>");
                obj.outXml = outXml.ToString();
                grpout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region FetchLotusInfo
        /// <summary>
        /// FetchLotusInfo (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchLotusInfo(int userid, vrmDataObject obj, ref string lotout)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionLot = new List<ICriterion>();
            try
            {
                outXml.Append("<lotus>");
                criterionLot.Add(Expression.Eq("userid", userid));
                List<vrmUserLotusNotesPrefs> usrlot = m_userlotusDAO.GetByCriteria(criterionLot);
                for (int l = 0; l < usrlot.Count; l++)
                {
                    outXml.Append("<lnLoginName>" + usrlot[l].noteslogin + "</lnLoginName>");
                    outXml.Append("<lnLoginPwd>" + usrlot[l].notespwd + "</lnLoginPwd>");
                    outXml.Append("<lnDBPath>" + usrlot[l].notespath + "</lnDBPath>");
                }
                if (usrlot.Count == 0)
                {
                    outXml.Append("<lnLoginName></lnLoginName>");
                    outXml.Append("<lnLoginPwd></lnLoginPwd>");
                    outXml.Append("<lnDBPath></lnDBPath>");
                }
                outXml.Append("</lotus>");
                obj.outXml = outXml.ToString();
                lotout = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
		
		#region DeleteAllGuests
        /// <summary>
        /// DeleteAllGuests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteAllGuests(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = null;
            
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                else
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("companyId", organizationID));
                    criterionList.Add(Expression.Eq("Deleted", 0));
                    List<vrmGuestUser> guestlN = m_IGuestUserDao.GetByCriteria(criterionList);

                    for (int i = 0; i < guestlN.Count; i++)
                    {
                        guestlN[i].Deleted = 1;
                    }
                    m_IGuestUserDao.SaveOrUpdateList(guestlN);
                    if(guestlN.Count < 0)
                    {
                        obj.outXml = "<error>-1</error>";
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in Deleting Guest User", ex);
                return false;
            }

        }
        #endregion

		#region GetAllocation
        /// <summary>
        /// GetAllocation - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetAllocation(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                myVRMException myVRMEx = null;
                string alphabet="";
                int pageNo = 0;
                int sortBy = 0;
                int deptUsers = 1; //FB 2269
                StringBuilder Out = new StringBuilder();

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID"); 
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; 

                
                node = xd.SelectSingleNode("//login/alphabet");
                if(node != null)
                    alphabet = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                if(node != null)
                    Int32.TryParse(node.InnerXml.Trim(),out pageNo);

                node = xd.SelectSingleNode("//login/sortBy");
                if(node != null)
                    Int32.TryParse(node.InnerXml.Trim(),out sortBy);

                int totPages = 0;
                string BulkUser = "0";//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, 0, ref outXml, organizationID, 0, deptUsers, BulkUser)) //FB 2269
                {
                    Out.Append("<getAllocation>" + outXml.ToString() + "</getAllocation>");
                    obj.outXml = Out.ToString();
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SetGroup
        /// <summary>
        /// SetGroup (COM to .Net conversion) 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmGroupDetails grpdetails = new vrmGroupDetails();
            vrmGroupParticipant grppartys = new vrmGroupParticipant();
            myVRMException myVRMEx = new myVRMException();
            String firstname = "", lastname = "", email = "", orgid = "";
            String partyid = "", groupid = "";
            int loginid = 0, ownerid = 0, CC = 0, gustid = 0, grpid = 0, GroupID = 0, PartyID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userInfo/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out loginid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groupInfo/group/ownerID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out ownerid);
                grpdetails.Owner = ownerid;

                if (loginid <= 0 || ownerid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                grpdetails.orgId = organizationID;

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groupInfo/group/groupID");
                if (node != null)
                    groupid = node.InnerText.Trim();
                if (groupid != "new")
                    Int32.TryParse(groupid, out GroupID);

                String groupname = "";
                node = xd.SelectSingleNode("//login/groupInfo/group/groupName");
                if (node != null)
                    groupname = node.InnerText.Trim();
                if (groupname.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                grpdetails.Name = groupname;

                String description = "";
                node = xd.SelectSingleNode("//login/groupInfo/group/description");
                if (node != null)
                    description = node.InnerText.Trim();
                grpdetails.Description = description;

                int privategrp = 0;
                node = xd.SelectSingleNode("//login/groupInfo/group/public");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out privategrp);
                if (privategrp == 1)
                    grpdetails.Private = 0;
                else
                    grpdetails.Private = 1;

                if (groupid.ToLower() == "new")
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("orgId", organizationID));
                    criterionList.Add(Expression.Eq("Name", groupname.ToUpper()));

                    if (groupid.ToLower() != "new")
                    {
                        criterionList.Add(Expression.Not(Expression.Eq("GroupID", GroupID)));
                    }
                    IList<vrmGroupDetails> GroupIds = m_IGrpDetailDao.GetByCriteria(criterionList);
                    if (GroupIds.Count > 0)
                    {
                        myVRMEx = new myVRMException(415); //Duplicate Group Name.
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    m_IGrpDetailDao.addProjection(Projections.Max("GroupID"));
                    IList maxId = m_IGrpDetailDao.GetObjectByCriteria(new List<ICriterion>());
                    if (maxId[0] != null)
                        grpid = ((int)maxId[0]) + 1;
                    else
                        grpid = 1;
                    grpdetails.GroupID = grpid;
                    grpdetails.CC = CC;
                    m_IGrpDetailDao.Save(grpdetails);
                }
                else
                {
                    grpdetails = m_IGrpDetailDao.GetById(GroupID);
                    grpdetails.Name = groupname;
                    if (privategrp == 1)
                        grpdetails.Private = 0;
                    else
                        grpdetails.Private = 1;
                    grpdetails.Description = description;
                    grpdetails.CC = CC;
                    m_IGrpDetailDao.Update(grpdetails);
                }

                if (groupid != "new")
                {
                    List<ICriterion> criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("GroupID", GroupID));
                    List<vrmGroupParticipant> delgrps = m_IGrpParticipantsDao.GetByCriteria(criterionList1);
                    for (int i = 0; i < delgrps.Count; i++)
                    {
                        m_IGrpParticipantsDao.Delete(delgrps[i]);
                    }
                    delgrps.Clear();
                }

                vrmGuestUser gustusr = new vrmGuestUser();
                XmlNodeList nodes = xd.SelectNodes("//login//groupInfo/group/user");

                if (nodes.Count > 0)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        node = nodes[i].SelectSingleNode("userID");
                        if (node != null)
                            partyid = node.InnerText.Trim();
                        if (partyid != "new")
                            Int32.TryParse(partyid, out PartyID);

                        if (partyid.ToLower() == "new")
                        {
                            m_IGuestUserDao.addProjection(Projections.Max("userid"));
                            IList maxId1 = m_IGuestUserDao.GetObjectByCriteria(new List<ICriterion>());
                            if (maxId1[0] != null)
                                gustid = ((int)maxId1[0]) + 1;
                            else
                                gustid = 1;
                            gustusr.userid = gustid;

                            node = nodes[i].SelectSingleNode("userFirstName");
                            if (node != null)
                                firstname = node.InnerText.Trim();
                            gustusr.FirstName = firstname;

                            node = nodes[i].SelectSingleNode("userLastName");
                            if (nodes[i] != null)
                                lastname = node.InnerText.Trim();
                            gustusr.LastName = lastname;

                            node = nodes[i].SelectSingleNode("userEmail");
                            if (node != null)
                                email = node.InnerText.Trim();
                            gustusr.Email = email;
                            gustusr.companyId = organizationID;

                            vrmUser loginusr = m_IuserDao.GetByUserId(loginid);
                            gustusr.DefLineRate = loginusr.DefLineRate;
                            gustusr.DefVideoProtocol = loginusr.DefVideoProtocol;
                            gustusr.DefVideoSession = loginusr.DefVideoSession;
                            gustusr.DefAudio = loginusr.DefAudio;
                            gustusr.TimeZone = loginusr.TimeZone;

                            m_IGuestUserDao.Save(gustusr);
                        }

                        if (groupid == "new")
                            grppartys.GroupID = grpid;
                        else
                            grppartys.GroupID = GroupID;
                        if (partyid == "new")
                            grppartys.UserID = gustid;
                        else
                            grppartys.UserID = PartyID;

                        grppartys.CC = CC;
                        m_IGrpParticipantsDao.Save(grppartys);
                    }
                }
                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetGroup
        /// <summary>
        /// GetGroup (COM to .Net conversion)  
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool singleDeptMode = true;
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            int userid = 0, sortby = 0, mode = 1;
            String orgid = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0 || sortby < 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out sortby);

                // Check if the user is a superadmin, show all groups.
                // Check if the user is an Admin, show Private Groups of Superadmin and all other groups.
                // Check if the user is a General, show their Groups and public Groups.

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    user = m_IuserDao.GetByUserId(userid);
                }

                user = m_IuserDao.GetByUserId(userid);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(user.roleID); //crossaccess

                if (mode == 0) //If User is General User
                {
                    criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", user.userid)));
                }
                criterionList.Add(Expression.Eq("orgId", organizationID));  //If User is Site Admin

                if (singleDeptMode)
                {
                    if (!user.isSuperAdmin())
                    {
                        criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid)));
                    }
                }
                else
                {
                    if (!(user.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (user.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_IuserDao.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                userId.Add(userList[i].userid);
                            }
                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("Owner", userId)), Expression.Eq("Private", 0)));
                        }
                        else
                        {
                            criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid))); //If its Gereral Users
                        }
                    }
                }
                outXML.Append("<groups>");
                outXML.Append("<sortBy>" + sortby + "</sortBy>");

                if (sortby == 1)
                    m_IGrpDetailDao.addOrderBy(Order.Asc("Name"));
                else if (sortby == 2)
                    m_IGrpDetailDao.addOrderBy(Order.Asc("Owner"));
                else if (sortby == 3)
                    m_IGrpDetailDao.addOrderBy(Order.Asc("Private"));

                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);

                for (int i = 0; i < Groups.Count; i++)
                {
                    if (Groups[i].Private == 1)
                        Groups[i].Private = 0;
                    else
                        Groups[i].Private = 1;

                    vrmUser owner = m_IuserDao.GetByUserId(Groups[i].Owner);
                    outXML.Append("<group>");
                    outXML.Append("<groupID>" + Groups[i].GroupID.ToString() + "</groupID>");
                    outXML.Append("<groupName>" + Groups[i].Name + "</groupName>");
                    if (owner != null)
                    {
                        outXML.Append("<ownerName>" + owner.FirstName + " " + owner.LastName + "</ownerName>");
                        outXML.Append("<ownerID>" + Groups[i].Owner + "</ownerID>");
                    }
                    else//FB 2318
                    {
                        outXML.Append("<ownerName></ownerName>");
                        outXML.Append("<ownerID></ownerID>"); 
                    }
                    outXML.Append("<description>" + Groups[i].Description + "</description>");
                    outXML.Append("<public>" + Groups[i].Private + "</public>");

                    List<ICriterion> criterionList2 = new List<ICriterion>();
                    criterionList2.Add(Expression.Eq("GroupID", Groups[i].GroupID));

                    List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList2);
                    for (int j = 0; j < partList.Count; j++)
                    {
                        vrmUser partUser = m_IuserDao.GetByUserId(partList[j].UserID);
                        if (partUser != null)
                        {
                            outXML.Append("<user>");
                            outXML.Append("<userID>" + partList[j].UserID.ToString() + "</userID>");
                            outXML.Append("<userFirstName>" + partUser.FirstName + "</userFirstName>");
                            outXML.Append("<userLastName>" + partUser.LastName + "</userLastName>");
                            outXML.Append("<userEmail>" + partUser.Email + "</userEmail>");
                            outXML.Append("</user>");
                        }
                        else
                        {
                            vrmGuestUser guestUser = m_IGuestUserDao.GetByUserId(partList[j].UserID);

                            if (guestUser != null) //FB 2318
                            {
                                outXML.Append("<user>");
                                outXML.Append("<userID>" + partList[j].UserID.ToString() + "</userID>");
                                outXML.Append("<userFirstName>" + guestUser.FirstName + "</userFirstName>");
                                outXML.Append("<userLastName>" + guestUser.LastName + "</userLastName>");
                                outXML.Append("<userEmail>" + guestUser.Email + "</userEmail>");
                                outXML.Append("</user>");
                            }
                        }
                    }
                    outXML.Append("</group>");
                }
                outXML.Append("</groups>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SearchGroup
        /// <summary>
        /// SearchGroup (COM to .Net conversion) 
        /// </summary>
        /// <param name="obj"></param>
        public bool SearchGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool singleDeptMode = true;
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            int userid = 0, mode = 1;
            String orgid = "", groupName = "", party = "", description = "";
            List<int> UserIds = null;
            List<int> GrpIds = null;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groupName");
                if (node != null)
                    groupName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/includedParticipant");
                if (node != null)
                    party = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/descriptionIncludes");
                if (node != null)
                    description = node.InnerText.Trim();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    user = m_IuserDao.GetByUserId(userid);
                }

                user = m_IuserDao.GetByUserId(userid);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(user.roleID); //crossaccess

                if (mode == 0) //If User is General User
                {
                    criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", user.userid)));
                }
                criterionList.Add(Expression.Eq("orgId", organizationID));  //If User is Site Admin
                if (groupName.Length > 0)
                    criterionList.Add(Expression.Like("Name", "%%" + groupName.ToLower() + "%%").IgnoreCase());
                if (description.Length > 0)
                    criterionList.Add(Expression.Like("Description", "%%" + description.ToLower() + "%%").IgnoreCase());

                if (singleDeptMode)
                {
                    if (!user.isSuperAdmin())
                    {
                        criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid)));
                    }
                }
                else
                {
                    if (!(user.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (user.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_IuserDao.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                userId.Add(userList[i].userid);
                            }
                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("Owner", userId)), Expression.Eq("Private", 0)));
                        }
                        else
                        {
                            criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid))); //If its Gereral Users
                        }
                    }
                }
                outXML.Append("<groups>");

                List<vrmGroupDetails> Groups1 = m_IGrpDetailDao.GetByCriteria(criterionList);
                if (party.Length > 0)
                {
                    List<ICriterion> criterionList4 = new List<ICriterion>();
                    criterionList4.Add(Expression.Or(Expression.Like("FirstName", "%%" + party.ToLower() + "%%").IgnoreCase(), Expression.Like("LastName", "%%" + party.ToLower() + "%%").IgnoreCase()));
                    List<vrmUser> partyNames = m_IuserDao.GetByCriteria(criterionList4);
                    List<vrmGuestUser> guestNames = m_IGuestUserDao.GetByCriteria(criterionList4);
                    if (guestNames.Count > 0)
                    {
                        for (int a = 0; a < guestNames.Count; a++)
                        {
                            if (UserIds == null)
                                UserIds = new List<int>();

                            UserIds.Add(guestNames[a].userid);
                        }
                    }
                    for (int j = 0; j < partyNames.Count; j++)
                    {
                        if (UserIds == null)
                            UserIds = new List<int>();

                        UserIds.Add(partyNames[j].userid);
                    }
                    if (UserIds != null)
                    {
                        if (UserIds.Count > 0)
                        {
                            List<ICriterion> criterionList3 = new List<ICriterion>();
                            criterionList3.Add(Expression.In("UserID", UserIds));
                            List<vrmGroupParticipant> groupid = m_IGrpParticipantsDao.GetByCriteria(criterionList3);

                            for (int i = 0; i < groupid.Count; i++)
                            {
                                if (GrpIds == null)
                                    GrpIds = new List<int>();

                                GrpIds.Add(groupid[i].GroupID);
                            }
                        }
                    }
                }
                if (!(party != "" && GrpIds == null))
                {
                    for (int i = 0; i < Groups1.Count; i++)
                    {
                        if (Groups1[i].Private == 1)
                            Groups1[i].Private = 0;
                        else
                            Groups1[i].Private = 1;

                        if (GrpIds != null)
                            if (GrpIds.Count > 0)
                                if (!GrpIds.Contains(Groups1[i].GroupID))
                                    continue;

                        vrmUser owner1 = m_IuserDao.GetByUserId(Groups1[i].Owner);
                        outXML.Append("<group>");
                        outXML.Append("<groupID>" + Groups1[i].GroupID + "</groupID>");
                        outXML.Append("<groupName>" + Groups1[i].Name + "</groupName>");
                        outXML.Append("<ownerID>" + Groups1[i].Owner + "</ownerID>");
                        outXML.Append("<ownerName>" + owner1.FirstName + " " + owner1.LastName + "</ownerName>");
                        outXML.Append("<description>" + Groups1[i].Description + "</description>");
                        outXML.Append("<public>" + Groups1[i].Private + "</public>");
                        outXML.Append("<users>");

                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("GroupID", Groups1[i].GroupID));
                        List<vrmGroupParticipant> partList1 = m_IGrpParticipantsDao.GetByCriteria(criterionList3);
                        for (int s = 0; s < partList1.Count; s++)
                        {
                            vrmUser partUser1 = m_IuserDao.GetByUserId(partList1[s].UserID);
                            if (partUser1 != null)
                            {
                                outXML.Append("<user>");
                                outXML.Append("<userID>" + partList1[s].UserID.ToString() + "</userID>");
                                outXML.Append("<userFirstName>" + partUser1.FirstName + "</userFirstName>");
                                outXML.Append("<userLastName>" + partUser1.LastName + "</userLastName>");
                                outXML.Append("<userEmail>" + partUser1.Email + "</userEmail>");
                                outXML.Append("</user>");
                            }
                            else
                            {
                                vrmGuestUser guestUser1 = m_IGuestUserDao.GetByUserId(partList1[s].UserID);
                                outXML.Append("<user>");
                                outXML.Append("<userID>" + partList1[s].UserID.ToString() + "</userID>");
                                outXML.Append("<userFirstName>" + guestUser1.FirstName + "</userFirstName>");
                                outXML.Append("<userLastName>" + guestUser1.LastName + "</userLastName>");
                                outXML.Append("<userEmail>" + guestUser1.Email + "</userEmail>");
                                outXML.Append("</user>");
                            }
                        }
                        outXML.Append("</users>");
                        outXML.Append("</group>");
                    }
                }
                outXML.Append("<sortBy>1</sortBy>");
                outXML.Append("</groups>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetUserStatus
        /// <summary>
        /// SetUserStatus 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUserStatus(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmUser userIDs = new vrmUser();
            vrmInactiveUser deluser = new vrmInactiveUser();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                int loginid = 0, userid = 0, mode = 0;
                String orgid = "";
                long newUser = 0, currUsrCount = 0;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out loginid);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/users/user/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/users/user/mode");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mode);

                if (loginid <= 0 || userid <= 0 || mode <= 0)
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                userIDs = m_IuserDao.GetByUserId(userid);
                deluser = m_IinactiveUserDao.GetByUserId(userid);

                if (mode == 1) //Permanently Delete One User
                {
                    Config config = new Config(m_configPath);
                    bool ret = config.Initialize();
                    if (config.ldapEnabled)
                    {
                        myvrmEx = new myVRMException(229);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    else
                    {
                        //FB 2164 Start
                        int oroleID = deluser.roleID;
                        List<ICriterion> del = new List<ICriterion>();
                        del.Add(Expression.Eq("roleID", oroleID));
                        List<vrmUser> Ausrlist = m_IuserDao.GetByCriteria(del);
                        List<vrmInactiveUser> usrlist = m_IinactiveUserDao.GetByCriteria(del);
                        int numrows = usrlist.Count;
                        int Act = Ausrlist.Count;
                        if (Act == 0)
                        {
                            if (numrows == 1)
                            {
                                List<ICriterion> critrole = new List<ICriterion>();
                                critrole.Add(Expression.Eq("roleID", oroleID));
                                critrole.Add(Expression.Eq("createType", 2));
                                List<vrmUserRoles> usrrle = m_IUserRolesDao.GetByCriteria(critrole);
                                for (int j = 0; j < usrrle.Count; j++)
                                {
                                    usrrle[j].locked = 0;
                                    m_IUserRolesDao.Update(usrrle[j]);
                                }
                            }
                        }
                        //FB 2164 End
                        if (userIDs != null)
                            m_IuserDao.Delete(userIDs);
                        m_IinactiveUserDao.Delete(deluser);

                        List<ICriterion> criterionListA = new List<ICriterion>();
                        criterionListA.Add(Expression.Eq("userid", userid));
                        List<vrmAccountGroupList> AccountGroup = m_IGroupAccountDAO.GetByCriteria(criterionListA);
                        for (int i = 0; i < AccountGroup.Count; i++)
                            m_IGroupAccountDAO.Delete(AccountGroup[i]);

                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("approverid", userid));
                        List<vrmLocApprover> LocApprov = m_ILocApprovDAO.GetByCriteria(criterionList);
                        for (int a = 0; a < LocApprov.Count; a++)
                            m_ILocApprovDAO.Delete(LocApprov[a]);

                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        criterionList1.Add(Expression.Eq("approverid", userid));
                        List<vrmMCUApprover> MCUapprov = m_IMCUapprover.GetByCriteria(criterionList1);
                        for (int b = 0; b < MCUapprov.Count; b++)
                            m_IMCUapprover.Delete(MCUapprov[b]);

                        List<ICriterion> criterionList2 = new List<ICriterion>();
                        criterionList2.Add(Expression.Eq("approverid", userid));
                        List<sysApprover> sysApprover = m_ISysApproverDAO.GetByCriteria(criterionList2);
                        for (int c = 0; c < sysApprover.Count; c++)
                            m_ISysApproverDAO.Delete(sysApprover[c]);

                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("userid", userid));
                        List<vrmConfUser> ConfUser = m_IConfUserDAO.GetByCriteria(criterionList3);
                        for (int d = 0; d < ConfUser.Count; d++)
                            m_IConfUserDAO.Delete(ConfUser[d]);

                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("userId", userid));
                        List<vrmAccount> usrAccount = m_IuserAccountDAO.GetByCriteria(criterionList4);
                        for (int e = 0; e < usrAccount.Count; e++)
                            m_IuserAccountDAO.Delete(usrAccount[e]);

                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("UserID", userid));
                        List<vrmGroupParticipant> grpusr = m_IGrpParticipantsDao.GetByCriteria(criterionList5);
                        for (int f = 0; f < grpusr.Count; f++)
                            m_IGrpParticipantsDao.Delete(grpusr[f]);

                        List<ICriterion> criterionList6 = new List<ICriterion>();
                        criterionList6.Add(Expression.Eq("userid", userid));
                        List<vrmTempUser> tempusr = m_TempUserDAO.GetByCriteria(criterionList6);
                        for (int g = 0; g < tempusr.Count; g++)
                            m_TempUserDAO.Delete(tempusr[g]);

                        List<ICriterion> criterionList7 = new List<ICriterion>();
                        criterionList7.Add(Expression.Eq("userId", userid));
                        List<vrmUserDepartment> deptusr = m_IuserDeptDAO.GetByCriteria(criterionList7);
                        for (int h = 0; h < deptusr.Count; h++)
                            m_IuserDeptDAO.Delete(deptusr[h]);
                    }
                }
                else if (mode == 2) //Restore One User
                {
                    List<ICriterion> criterionList8 = new List<ICriterion>();
                    criterionList8.Add(Expression.Eq("companyId", deluser.companyId));
                    newUser = m_IuserDao.CountByCriteria(criterionList8);
                    newUser = newUser + 1;
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(deluser.companyId);
                    if (newUser > orgInfo.UserLimit)
                    {
                        myvrmEx = new myVRMException(252);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //FB 2346 Starts
                    string userMail = deluser.Email.Substring(deluser.Email.IndexOf('@'));
                    string tempemail = userMail;
                    if (userMail.Contains("_D"))
                        userMail = userMail.Replace("_D", "");
                    deluser.Email = deluser.Email.Replace(tempemail, userMail);
                    
                    List<ICriterion> criterionListUser = new List<ICriterion>();
                    criterionListUser.Add(Expression.Eq("Email", deluser.Email));
                    newUser = m_IuserDao.CountByCriteria(criterionListUser);
                    if(newUser > 0)
                    {
                        myvrmEx = new myVRMException(261);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //FB 2346 Ends
                    if (deluser.enableExchange == 1)
                    {
                        List<ICriterion> criterionList9 = new List<ICriterion>();
                        criterionList9.Add(Expression.Eq("enableExchange", 1));
                        criterionList9.Add(Expression.Eq("Deleted", 0));
                        criterionList9.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList9);

                        if (currUsrCount >= orgInfo.ExchangeUserLimit)
                        {
                            myvrmEx = new myVRMException(460);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (deluser.enableDomino == 1)
                    {
                        List<ICriterion> criterionList10 = new List<ICriterion>();
                        criterionList10.Add(Expression.Eq("enableDomino", 1));
                        criterionList10.Add(Expression.Eq("Deleted", 0));
                        criterionList10.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList10);

                        if (currUsrCount >= orgInfo.DominoUserLimit)
                        {
                            myvrmEx = new myVRMException(461);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (deluser.enableMobile == 1)
                    {
                        List<ICriterion> criterionList11 = new List<ICriterion>();
                        criterionList11.Add(Expression.Eq("enableMobile", 1));
                        criterionList11.Add(Expression.Eq("Deleted", 0));
                        criterionList11.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList11);

                        if (currUsrCount >= orgInfo.MobileUserLimit)
                        {
                            myvrmEx = new myVRMException(526);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (deluser.enablePCUser == 1)
                    {
                        List<ICriterion> criterionList11 = new List<ICriterion>();
                        criterionList11.Add(Expression.Eq("enablePCUser", 1));
                        criterionList11.Add(Expression.Eq("Deleted", 0));
                        criterionList11.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList11);

                        if (currUsrCount >= orgInfo.PCUserLimit)
                        {
                            myvrmEx = new myVRMException(682);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    vrmUser user = new vrmUser();
                    user = new vrmUser(deluser);
                    user.LockStatus = vrmUserStatus.USER_ACTIVE;
                    user.Deleted = 0;
                    user.newUser = 1;
                    user.TickerSpeed = 6;//FB 2027 - Starts
                    user.TickerSpeed1 = 6;
                    user.TickerStatus = 1;
                    user.TickerStatus1 = 1;//FB 2027 - End
                    m_IinactiveUserDao.Delete(deluser);
                    m_IuserDao.Save(user);
                }
                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GuestRegister
        /// <summary>
        /// GuestRegister (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GuestRegister(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            string fname = "";
            string lname = "";
            string email = "";
            string pwd = "";
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/userFirstName");
                if (node != null)
                    fname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/userLastName");
                if (node != null)
                    lname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/userPassword");
                if (node != null)
                    pwd = node.InnerXml.Trim();

                criterionList.Add(Expression.Eq("Email", email));
                List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                List<vrmGuestUser> Gusrlist = m_IGuestUserDao.GetByCriteria(criterionList);
                for (int u = 0; u < usrlist.Count; u++)
                {
                    obj.outXml = "<GuestRegister><userID>" + usrlist[u].userid.ToString() + "</userID></GuestRegister>";
                    return false;
                }
                for (int i = 0; i < Gusrlist.Count; i++)
                {
                    obj.outXml = "<GuestRegister><userID>" + Gusrlist[i].userid.ToString() + "</userID></GuestRegister>";
                    return false;
                }
                if (pwd.Length > 0)
                {
                    crypto = new cryptography.Crypto();
                    pwd = crypto.encrypt(pwd);
                }
                string uString = "SELECT MAX(userid + 1) as userid FROM myVRM.DataLayer.vrmUser";
                IList list = m_IuserDao.execQuery(uString);
				//Empty DB Issue FB 2164
                int uid = 0;
                if (list[0] != null)
                    uid = Int32.Parse(list[0].ToString());
               
                string qString = "SELECT MAX(userid + 1) as userid FROM myVRM.DataLayer.vrmGuestUser";
                IList glist = m_IGuestUserDao.execQuery(qString);
				//Empty DB Issue FB 2164
                int gid = 0;
                if (glist[0] != null)
                    gid = Int32.Parse(glist[0].ToString());
                
                if (uid > gid)
                {
                    userid = uid;
                }
                else
                {
                    userid = gid;
                }
                vrmGuestUser Gusr = new vrmGuestUser();
                Gusr.companyId = orgid;
                Gusr.userid = userid;
                Gusr.FirstName = fname;
                Gusr.LastName = lname;
                Gusr.Login = email;
                Gusr.Email = email;
                Gusr.Password = pwd;
                Gusr.emailmask = 1; //FB 2550
                Gusr.TimeZone = 26;//FB 2550
                m_IGuestUserDao.Save(Gusr);
                obj.outXml = "<GuestRegister><userID>" + userid.ToString() + "</userID></GuestRegister>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetUserRoles
        /// <summary>
        /// GetUserRoles (COM TO .NetConversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetUserRoles(ref vrmDataObject obj)
        {
            try
            {
                List<int> ModuleList = new List<int>();
                StringBuilder outXML = new StringBuilder();
                List<ICriterion> criterionList = new List<ICriterion>();
                List<ICriterion> criterionList1 = new List<ICriterion>();//FB 2891
                int userID = 11, isFoodEnabled = 1, isRoomEnabled = 1, isHkEnabled = 1;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                ModuleList.Add(0);
                node = xd.SelectSingleNode("//login/foodModuleEnable");
                if (node != null)
                {
                    Int32.TryParse(node.InnerText.Trim(), out isFoodEnabled);
                    if (isFoodEnabled == 0)
                        ModuleList.Add(4);
                }
                node = xd.SelectSingleNode("//login/roomModuleEnable");
                if (node != null)
                {
                    Int32.TryParse(node.InnerText.Trim(), out isRoomEnabled);
                    if (isRoomEnabled == 0)
                        ModuleList.Add(5);
                }
                node = xd.SelectSingleNode("//login/hkModuleEnable");
                if (node != null)
                {
                    Int32.TryParse(node.InnerText.Trim(), out isHkEnabled);
                    if (isHkEnabled == 0)
                        ModuleList.Add(6);
                }
                //FB 2891 - Start
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Not(Expression.In("roleID", ModuleList)));
                outXML.Append("<userRole><roles>");
                criterionList.Add(Expression.Gt("DisplayOrder", 0));
                m_IUserRolesDao.addOrderBy(Order.Asc("DisplayOrder"));
                List<vrmUserRoles> usrRoles = m_IUserRolesDao.GetByCriteria(criterionList);
                
                criterionList1.Add(Expression.Not(Expression.In("roleID", ModuleList)));
                criterionList1.Add(Expression.Lt("DisplayOrder", 1));
                List<vrmUserRoles> usrRoles1 = m_IUserRolesDao.GetByCriteria(criterionList1);
                usrRoles.AddRange(usrRoles1);
                //FB 2891 - End

                for (int i = 0; i < usrRoles.Count; i++)
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("roleId", usrRoles[i].roleID));
                    List<vrmUserTemplate> usrTemplate = m_ItempDao.GetByCriteria(criterionList);

                    outXML.Append("<role>");
                    outXML.Append("<ID>" + usrRoles[i].roleID.ToString() + "</ID>");
                    outXML.Append("<name>" + usrRoles[i].roleName + "</name>");
                    outXML.Append("<menuMask>" + usrRoles[i].roleMenuMask + "</menuMask>");
                    outXML.Append("<locked>" + usrRoles[i].locked.ToString() + "</locked>");
                    outXML.Append("<level>" + usrRoles[i].level.ToString() + "</level>");


                    if (usrTemplate.Count > 0)
                        outXML.Append("<active>1</active>");
                    else
                        outXML.Append("<active>0</active>");

                    outXML.Append("<crossaccess>" + usrRoles[i].crossaccess.ToString() + "</crossaccess>");
                    outXML.Append("<createType>" + usrRoles[i].createType.ToString() + "</createType>");
                    outXML.Append("</role>");
                }
                outXML.Append("</roles></userRole>");
                obj.outXml = outXML.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetUserRoles" + ex.Message);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region DeleteGroup
        /// <summary>
        /// DeleteGroup
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteGroup(ref vrmDataObject obj)
        {
            try
            {
                myVRMException myVRMEx = null;
                List<ICriterion> criterionList = new List<ICriterion>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 11, groupID = 0, i = 0;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groups/group/groupID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out groupID);

                criterionList.Add(Expression.Eq("GroupID", groupID));

                List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList);
                for (i = 0; i < partList.Count; i++)
                    m_IGrpParticipantsDao.Delete(partList[i]);

                List<vrmGroupDetails> GrpDetails = m_IGrpDetailDao.GetByCriteria(criterionList);
                for (i = 0; i < GrpDetails.Count; i++)
                    m_IGrpDetailDao.Delete(GrpDetails[i]);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("DeleteGroup" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetEmailList
        /// <summary>
        /// GetEmailList - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetEmailList(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 0; //FB 2269 
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out sortBy);

                node = xd.SelectSingleNode("//login/audioaddon"); //FB 2023
                if (node != null)
                    if (node.InnerText.Trim() != "")
                        Int32.TryParse(node.InnerText.Trim(), out isAudioAddOn);

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml, organizationID, 1, deptUsers, BulkUser)) //FB 2269
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetGuestList
        /// <summary>
        /// GetGuestList - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetGuestList(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out sortBy);

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isGustUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml, organizationID, 1, deptUsers, BulkUser)) //FB 2269
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        //FB 2670 START
        #region GetVNOCUserList
        /// <summary>
        /// GetVNOCUserList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetVNOCUserList(ref vrmDataObject obj)
        {
            bool bRet = true;
            int i = 0, isCrossSilo = 0; //FB 2766
            vrmBaseUser user = null;
            vrmOrganization vrmOrg = null;
            StringBuilder OutXML = new StringBuilder();
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;


                node = xd.SelectSingleNode("//login/isCrossSilo"); //FB 2766
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out isCrossSilo);

                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                List<ICriterion> VNOCcriterionList = new List<ICriterion>();
                VNOCcriterionList.Add(Expression.Eq("level", 3));
                VNOCcriterionList.Add(Expression.Eq("crossaccess", 1));
                List<vrmUserRoles> usrrole = m_IUserRolesDao.GetByCriteria(VNOCcriterionList);

                List<ICriterion> criterionList = new List<ICriterion>();

                if (isCrossSilo == 0) //FB 2766
                {
                    if (sysSettings.ConciergeSupport == 1)
                        criterionList.Add(Expression.Or(Expression.Eq("companyId", organizationID), Expression.Eq("companyId", 11)));
                    else if (sysSettings.ConciergeSupport == 2)
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                }

                criterionList.Add(Expression.Eq("roleID", usrrole[0].roleID));

                IList userList = new List<vrmBaseUser>();

                userList = m_IuserDao.GetByCriteria(criterionList);
                OutXML.Append("<users>");

                for (i = 0; i < userList.Count; i++)
                {
                    user = (vrmBaseUser)userList[i];
                    vrmOrg = m_IOrgDAO.GetById(organizationID);
                    if (user.LockStatus == vrmUserStatus.USER_ACTIVE)
                    {
                        vrmOrg = m_IOrgDAO.GetById(user.companyId); //FB 2766
                        OutXML.Append("<user>");
                        OutXML.Append("<userID>" + user.userid + "</userID>");
                        if (sysSettings.ConciergeSupport == 1)
                        {
                            if ((organizationID != 11 && user.companyId == 11) || (organizationID == 11 && user.companyId == 11 && isCrossSilo == 1)) //FB 2820
                            {
                                OutXML.Append("<firstName>" + user.FirstName + " *</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");
                            }
                            else
                            {
                                OutXML.Append("<firstName>" + user.FirstName + "</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");
                            }
                        }
                        else
                        {
                            if (user.companyId == 11 && isCrossSilo == 1) //FB 2820
                            {
                                OutXML.Append("<firstName>" + user.FirstName + " *</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");
                            }
                            else
                            {
                                OutXML.Append("<firstName>" + user.FirstName + "</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");

                            }
                        }
                        OutXML.Append("<userEmail>" + user.Email + "</userEmail>");
                        OutXML.Append("<orgName>" + vrmOrg.orgname + "</orgName>"); //FB 2766
                        OutXML.Append("<login>" + user.Login + "</login>");
                        OutXML.Append("</user>");
                    }
                }

                OutXML.Append("</users>");
                obj.outXml = OutXML.ToString();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
        //FB 2670 END

        #region FetchGroups
        /// <summary>
        /// FetchGroups (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public void FetchGroups(List<ICriterion> criterionList, ref StringBuilder outXml, int mode, String ParentTag)
        {
            try
            {
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);
                vrmGuestUser gustUsr = null;
                vrmUser partUser = null;
                string partyFName = "", partyLName = "", partyEmail = "";

                for (int g = 0; g < Groups.Count; g++)
                {
                    if (ParentTag.Trim() != "")
                        outXml.Append("<" + ParentTag + ">");

                    outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                    outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                    if (mode == 0)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("GroupID", Groups[g].GroupID));
                        List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList);
                        outXml.Append("<numParticipants>" + partList.Count + "</numParticipants>"); //FB 2027 goc
                        for (int p = 0; p < partList.Count; p++)
                        {
                            partUser = m_IuserDao.GetByUserId(partList[p].UserID);
                            if (partUser == null)
                            {
                                gustUsr = m_IGuestUserDao.GetByUserId(partList[p].UserID);
                                if (gustUsr != null) //FB 2318
                                {
                                    partyFName = gustUsr.FirstName;
                                    partyLName = gustUsr.LastName;
                                    partyEmail = gustUsr.Email;
                                }
                            }
                            else
                            {
                                partyFName = partUser.FirstName;
                                partyLName = partUser.LastName;
                                partyEmail = partUser.Email;
                            }
                            outXml.Append("<user>");
                            outXml.Append("<userID>" + partList[p].UserID.ToString() + "</userID>");
                            outXml.Append("<userFirstName>" + partyFName + "</userFirstName>");
                            outXml.Append("<userLastName>" + partyLName + "</userLastName>");
                            outXml.Append("<userEmail>" + partyEmail + "</userEmail>");
                            outXml.Append("<CC>" + partList[p].CC.ToString() + "</CC>");
                            outXml.Append("</user>");
                        }
                    }

                    if (ParentTag.Trim() != "")
                        outXml.Append("</" + ParentTag + ">");
                }
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("vrmException", myVRMEx);
                throw myVRMEx;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                throw ex;
            }
        }

        //FB 2639_GetOldConf
        public void FetchGroups(List<ICriterion> criterionList, ref XmlWriter outXml, int mode, string ParentTag)
        {
            try
            {
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);
                vrmGuestUser gustUsr = null;
                vrmUser partUser = null;
                string partyFName = "", partyLName = "", partyEmail = "";

                for (int g = 0; g < Groups.Count; g++)
                {
                    outXml.WriteStartElement(ParentTag);
                    outXml.WriteElementString("groupID", Groups[g].GroupID.ToString());
                    outXml.WriteElementString("groupName", Groups[g].Name);
                    if (mode == 0)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("GroupID", Groups[g].GroupID));
                        List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList);
                        outXml.WriteElementString("numParticipants", partList.Count.ToString()); //FB 2027 goc
                        for (int p = 0; p < partList.Count; p++)
                        {
                            partUser = m_IuserDao.GetByUserId(partList[p].UserID);
                            if (partUser == null)
                            {
                                gustUsr = m_IGuestUserDao.GetByUserId(partList[p].UserID);
                                if (gustUsr != null) //FB 2318
                                {
                                    partyFName = gustUsr.FirstName;
                                    partyLName = gustUsr.LastName;
                                    partyEmail = gustUsr.Email;
                                }
                            }
                            else
                            {
                                partyFName = partUser.FirstName;
                                partyLName = partUser.LastName;
                                partyEmail = partUser.Email;
                            }
                            outXml.WriteStartElement("user");
                            outXml.WriteElementString("userID", partList[p].UserID.ToString());
                            outXml.WriteElementString("userFirstName", partyFName);
                            outXml.WriteElementString("userLastName", partyLName);
                            outXml.WriteElementString("userEmail", partyEmail);
                            outXml.WriteElementString("CC", partList[p].CC.ToString());
                            outXml.WriteFullEndElement(); //ZD 100116
                        }
                    }
                    outXml.WriteFullEndElement(); //ZD 100116
                }
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("vrmException", myVRMEx);
                throw myVRMEx;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                throw ex;
            }
        }
        //FB 2639_GetOldConf End
        #endregion

        #region GetManageBulkUsers
        /// <summary>
        /// GetManageBulkUsers (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetManageBulkUsers(ref vrmDataObject obj)
        {
            string lang = "";
            string time = "";
            string role = "";
            string bridge = "";
            string deptout = "";
            int err = 0;
            bool bRet = true;

            List<ICriterion> criterionList3 = new List<ICriterion>();
            StringBuilder outXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                outXml.Append("<getManageBulkUsers>");
                List<sysData> sysDatas = new List<sysData>();
                sysDatas = m_IsystemDAO.GetAll();
                int tzid = sysDatas[0].TimeZone;
                outXml.Append("<timezoneID>" + tzid.ToString() + "</timezoneID>");
                outXml.Append("<timezones>");
                if (FetchAllTimeZones(obj, err, orgid, ref time))
                    outXml.Append(time);
                outXml.Append("</timezones>");
                if (OldFetchUserRoleList(obj, ref role))
                    outXml.Append(role);
                if (OldFetchLanguageList(obj, ref lang))
                    outXml.Append(lang);
                if (FetchBridges(obj, err, orgid, ref bridge))
                    outXml.Append(bridge);
                if (GetManageDepartment(obj, userid, orgid, ref deptout))
                    outXml.Append(deptout);
                outXml.Append("</getManageBulkUsers>");
                obj.outXml = outXml.ToString();
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchAllTimeZones
        /// <summary>
        /// FetchAllTimeZones (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchAllTimeZones(vrmDataObject obj, int err, int orgid, ref string time)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                int tzsid;
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);
                tzsid = orgInfo.tzsystemid;
                if (tzsid == 0)
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        outXml.Append("<timezone>");
                        outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                        outXml.Append("<timezoneName>" + timeZoneList[t].TimeZoneDiff.ToString() + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                        outXml.Append("</timezone>");
                    }
                }
                else
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        if (orgInfo.tzsystemid == timeZoneList[t].systemID)
                        {
                            outXml.Append("<timezone>");
                            outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                            outXml.Append("<timezoneName>" + timeZoneList[t].TimeZoneDiff.ToString() + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                            outXml.Append("</timezone>");
                        }
                    }
                }
                obj.outXml = outXml.ToString();
                time = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region OldFetchUserRoleList
        /// <summary>
        /// OldFetchUserRoleList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchUserRoleList(vrmDataObject obj, ref string role)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<roles>");
                //FB 2891 - Start
                //List<vrmUserRoles> roles = m_IUserRolesDao.GetAll();
                List<ICriterion> criterionList1 = new List<ICriterion>();
                List<ICriterion> criterionList2 = new List<ICriterion>();               
                
                criterionList1.Add(Expression.Gt("DisplayOrder", 0));
                m_IUserRolesDao.addOrderBy(Order.Asc("DisplayOrder"));
                List<vrmUserRoles> usrRoles = m_IUserRolesDao.GetByCriteria(criterionList1);
                
                criterionList2.Add(Expression.Lt("DisplayOrder", 1));
                List<vrmUserRoles> usrRoles1 = m_IUserRolesDao.GetByCriteria(criterionList2);

                usrRoles.AddRange(usrRoles1);

                if (usrRoles.Count <= 0) 
                {
                    m_log.Error("Error in getting role list");
                    outXml.Append("");
                    return false;
                }
                foreach (vrmUserRoles uR in usrRoles)//FB 2891 - End
                {
                    outXml.Append("<role>");
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("roleId", uR.roleID));
                    List<vrmUserTemplate> tmplist = m_ItempDao.GetByCriteria(criterionList);
                    outXml.Append("<ID>" + uR.roleID.ToString() + "</ID>");
                    outXml.Append("<name>" + uR.roleName + "</name>");
                    outXml.Append("<menuMask>" + uR.roleMenuMask + "</menuMask>");
                    outXml.Append("<locked>" + uR.locked.ToString() + "</locked>");
                    outXml.Append("<level>" + uR.level.ToString() + "</level>");
                    if (tmplist.Count > 0)
                    {
                        outXml.Append("<active>1</active>");
                    }
                    else
                    {
                        outXml.Append("<active>0</active>");
                    }
                    outXml.Append("<createType>" + uR.createType.ToString() + "</createType>");
                    outXml.Append("</role>");
                }
                outXml.Append("</roles>");
                obj.outXml = outXml.ToString();
                role = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region FetchBridges
        /// <summary>
        /// FetchBridges (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchBridges(vrmDataObject obj, int err, int orgid, ref string bridge)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<bridges>");
                List<ICriterion> criterionList2 = new List<ICriterion>();
                criterionList2.Add(Expression.Eq("deleted", 0));
                criterionList2.Add(Expression.Or(Expression.Eq("orgId", orgid), Expression.Eq("isPublic", 1)));
                List<vrmMCU> mculist = m_ImcuDao.GetByCriteria(criterionList2);
                foreach (vrmMCU mcu in mculist)
                {
                    outXml.Append("<bridge>");
                    outXml.Append("<ID>" + mcu.BridgeID.ToString() + "</ID>");
                    string PublicMCU = mcu.isPublic.ToString();
                    if (orgid != 11 && PublicMCU != "0")
                    {
                        outXml.Append("<name>" + mcu.BridgeName + "(*)</name>");
                    }
                    else
                    {
                        outXml.Append("<name>" + mcu.BridgeName + "</name>");
                    }
                    outXml.Append("<interfaceType>" + mcu.MCUType.id.ToString().ToString() + "</interfaceType>");
                    List<ICriterion> criterionList3 = new List<ICriterion>();
                    criterionList3.Add(Expression.Eq("userid", mcu.Admin));
                    List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList3);
                    if (usrlist.Count > 0)
                    {
                        outXml.Append("<administrator>" + usrlist[0].FirstName + usrlist[0].LastName + "</administrator>");
                    }
                    else
                    {
                        outXml.Append("<administrator> Unknown </administrator>");
                    }
                    string exit, virtualBridge;
                    virtualBridge = mcu.VirtualBridge.ToString();
                    if (virtualBridge == "0")
                    {
                        exit = "1";
                    }
                    else
                    {
                        exit = "0";
                    }
                    outXml.Append("<administrator>" + exit + "</administrator>");
                    outXml.Append("<status>" + mcu.Status.ToString() + "</status>");
                    outXml.Append("</bridge>");
                }
                outXml.Append("</bridges>");
                obj.outXml = outXml.ToString();
                bridge = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region GetManageDepartment
        /// <summary>
        /// GetManageDepartment (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetManageDepartment(vrmDataObject obj, int userid, int orgid, ref string deptout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                if (orgid < 11)
                    orgid = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = m_IuserDao.GetByUserId(userid);

                List<ICriterion> deptCriterionList = new List<ICriterion>();
                deptCriterionList.Add(Expression.Eq("deleted", 0));
                string org = Convert.ToString(orgid);
                deptCriterionList.Add(Expression.Eq("orgId", org));

                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
                if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", user.userid));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        deptIn.Add(dept.departmentId);
                    }
                    deptCriterionList.Add(Expression.In("departmentId", deptIn));
                }
                List<vrmDept> selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);

                outXml.Append("<multiDepartment>" + multiDepts.ToString() + "</multiDepartment>");
                outXml.Append("<departments>");

                foreach (vrmDept dept in selectedDept)
                {
                    outXml.Append("<department>");
                    outXml.Append("<id>" + dept.departmentId.ToString() + "</id>");
                    outXml.Append("<name>" + dept.departmentName + "</name>");
                    outXml.Append("<securityKey>" + dept.securityKey + "</securityKey>");
                    outXml.Append("</department>");
                }
                outXml.Append("</departments>");
                obj.outXml = outXml.ToString();
                deptout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

		//FB 2027
        #region FetchGlobalInfo
        /// <summary>
        /// FetchGlobalInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public void FetchGlobalInfo(Int32 organizationID, Int32 userID, ref StringBuilder outXml, ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            crypto = new cryptography.Crypto(); //ZD 100263
            try
            {
                outXml.Append("<globalInfo>");
                vrmOrganization vrmOrg = m_IOrgDAO.GetById(organizationID);
                outXml.Append("<organizationName>" + vrmOrg.orgname + "</organizationName>");

                vrmUser usr = new vrmUser();
                usr = m_IuserDao.GetByUserId(userID);

                outXml.Append("<userID>"+ userID.ToString()+"</userID>");
                outXml.Append("<userName>");
                    outXml.Append("<firstName>"+ usr.FirstName +"</firstName>");
                    outXml.Append("<lastName>"+ usr.LastName +"</lastName>");
                outXml.Append("</userName>");
                outXml.Append("<userEmail>"+usr.Email +"</userEmail>");
                outXml.Append("<menuMask>"+usr.MenuMask+"</menuMask>");

                //FB 2659 - Starts
                string[] mary = usr.MenuMask.ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int usrmenu = Convert.ToInt32(ccary[1]);
                bool isExpuser = Convert.ToBoolean((((usrmenu & 1) == 1) && ((usrmenu & 2) == 2)) || ((((usrmenu & 2) == 2) && ((usrmenu & 64) == 64))));

                if (isExpuser)
                    outXml.Append("<poweruser>0</poweruser>");
                else
                    outXml.Append("<poweruser>1</poweruser>");
                //FB 2659 - End


                if (usr.Admin == 2 || usr.approverCount > 0)
                    outXml.Append("<approver>" + 1 + "</approver>");
                else
                    outXml.Append("<approver>" + 0 + "</approver>");

                outXml.Append("<admin>" + usr.Admin + "</admin>");
                outXml.Append("<level>3</level>");// hardcode level as 3 (that's what it used to be)
                outXml.Append("<enableAV>" + usr.enableAV + "</enableAV>");
                outXml.Append("<enableParticipants>" + usr.enableParticipants + "</enableParticipants>");

                vrmLanguage hostLang = m_langDAO.GetLanguageById(usr.Language);
                outXml.Append("<language>" + hostLang.LanguageFolder + "</language>");
                outXml.Append("<languageid>" + hostLang.Id + "</languageid>");
                
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(usr.roleID);
                outXml.Append("<crossaccess>" + usrRole.crossaccess.ToString() + "</crossaccess>");
                outXml.Append("<level>" + usrRole.level.ToString() + "</level>");
                outXml.Append("<rolename>" + usrRole.roleName + "</rolename>");

                outXml.Append("<timeFormat>" + usr.TimeFormat + "</timeFormat>");
                outXml.Append("<timeZoneDisplay>" + usr.Timezonedisplay + "</timeZoneDisplay>");
                outXml.Append("<organizationID>" + organizationID.ToString() + "</organizationID>");
                outXml.Append("<defaultConfTemplate>" + usr.DefaultTemplate.ToString() + "</defaultConfTemplate>");
                outXml.Append("<userWorkNo>"+ usr.WorkPhone +"</userWorkNo>");
                outXml.Append("<userCellNo>" + usr.CellPhone + "</userCellNo>");
                outXml.Append("<tickerStatus>" + usr.TickerStatus.ToString() + "</tickerStatus>");
                outXml.Append("<tickerSpeed>" + usr.TickerSpeed.ToString() + "</tickerSpeed>");
                outXml.Append("<tickerPosition>" + usr.TickerPosition.ToString() + "</tickerPosition>");
                outXml.Append("<tickerDisplay>" + usr.TickerDisplay.ToString() + "</tickerDisplay>");
                outXml.Append("<tickerBackground>" + usr.TickerBackground + "</tickerBackground>");
                outXml.Append("<rssFeedLink>" + usr.RSSFeedLink + "</rssFeedLink>");
                outXml.Append("<tickerStatus1>" + usr.TickerStatus1.ToString() + "</tickerStatus1>");
                outXml.Append("<tickerSpeed1>" + usr.TickerSpeed1.ToString() + "</tickerSpeed1>");
                outXml.Append("<tickerPosition1>" + usr.TickerPosition1.ToString() + "</tickerPosition1>");
                outXml.Append("<tickerDisplay1>" + usr.TickerDisplay1.ToString() + "</tickerDisplay1>");
                outXml.Append("<tickerBackground1>" + usr.TickerBackground1 + "</tickerBackground1>");
                outXml.Append("<rssFeedLink1>" + usr.RSSFeedLink1 + "</rssFeedLink1>");

                OrganizationFactory orgFactory = new OrganizationFactory(ref obj); 
                orgFactory.AppendOrgDetails(ref outXml, organizationID);

                vrmSystemFactory m_systemFactory = new vrmSystemFactory(ref obj);

                m_systemFactory.AppendEmailDetails(ref outXml);
                
                String outStrXML = "";
                FetchLotusInfo(userID, obj, ref outStrXML);
                outXml.Append(outStrXML);

                sysTechData sysTechDta = m_ISysTechDAO.GetTechByOrgId(organizationID);
                if (sysTechDta != null)
                {
                    outXml.Append("<contactDetails>");
                    outXml.Append( "<name>" + sysTechDta.name + "</name>");
                    outXml.Append( "<email>" + sysTechDta.email + "</email>");
                    outXml.Append( "<phone>" + sysTechDta.phone + "</phone>");
                    outXml.Append( "<additionInfo>" + sysTechDta.info + "</additionInfo>");
                    outXml.Append( "<feedback>" + sysTechDta.feedback + "</feedback>");
                    outXml.Append( "</contactDetails>");
                }

                if (usr.BridgeID > 0)
                {
                    vrmMCU mcu = new vrmMCU();
                    mcu = m_ImcuDao.GetById(usr.BridgeID);

                    List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                    foreach (vrmMCUVendor vendor in vendorList)
                    {
                        if (vendor.id == mcu.BridgeTypeId)
                        {
                            outXml.Append("<primaryBridge>" + vendor.BridgeInterfaceId + "</primaryBridge>");
                            break;
                        }
                    }
                }
                else
                    outXml.Append("<primaryBridge/>");    
                //ZD 100263 Starts 
                outXml.Append("<enEmailID>"+ crypto.encrypt(usr.Email) +"</enEmailID>");
                outXml.Append("<enUserPWD>" + usr.Password + "</enUserPWD>");
                if (macID != "")
                    outXml.Append("<enMacID>" + crypto.encrypt(macID) + "</enMacID>");
                else
                    outXml.Append("<enMacID></enMacID>");
                //ZD 100263 Ends
                outXml.Append("</globalInfo>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchGlobalInfo" + ex.Message);
            }
        }

        #endregion

        #region GetSettingsSelect
        /// <summary>
        /// GetSettingsSelect
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSettingsSelect(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                outXml.Append("<user>");

                Conference confFact = new Conference(ref obj);
                confFact.FetchMostConfs(organizationID, userID, ref outXml);

                UserFactory usrFact = new UserFactory(ref obj);
                usrFact.FetchGlobalInfo(organizationID, userID, ref outXml, ref obj);

                outXml.Append("</user>");

                obj.outXml = outXml.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
                return false;
            }
        }

        #endregion
        //FB 2027 - End

        //FB 2027(Bulk Tools) Start

        #region SetBulkUserAddMinutes
        /// <summary>
        /// SetBulkUserAddMinutes (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserAddMinutes(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int minutes = 0;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                vrmUser user = new vrmUser();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/minutes");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out minutes);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    criterionList.Add(Expression.Eq("userId", uid));
                    node = uid[i];
                    vrmAccount acc = m_IuserAccountDAO.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    acc.TotalTime = acc.TotalTime + minutes;
                    acc.TimeRemaining = acc.TimeRemaining + minutes;
                    m_IuserAccountDAO.Update(acc);
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserBridge
        /// <summary>
        /// SetBulkUserBridge (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserBridge(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int bridgeID = 0;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/bridgeID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out bridgeID);
                else
                {
                    myVRMEx = new myVRMException(531);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    criterionList.Add(Expression.Eq("userId", uid));
                    node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    user.BridgeID = bridgeID;
                    m_IuserDao.Update(user);
                    int eptid = user.endpointId;
                    List<ICriterion> criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("endpointid", eptid));
                    List<vrmEndPoint> ept = m_IeptDao.GetByCriteria(criterionList1);
                    for (int e = 0; e < ept.Count; e++)
                    {
                        ept[e].bridgeid = bridgeID;
                        m_IeptDao.Update(ept[e]);
                    }
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserDelete
        /// <summary>
        /// SetBulkUserDelete (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserDelete(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int errid = 0;
            try
            {
                vrmUser user = new vrmUser();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    node = uid[i];
                    user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    m_IuserDao.clearFetch();
                    if (!canDeleteUser(user, ref errid))
                    {
                        myVRMException e = new myVRMException(errid);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                        return false;
                    }
                }
                for (int i = 0; i < uid.Count; i++)
                {
                    node = uid[i];
                    user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    m_IuserDao.clearFetch();
                    vrmInactiveUser Inusr = new vrmInactiveUser(user);
                    Inusr.id = user.id;
                    Inusr.Deleted = vrmUserStatus.USER_INACTIVE;
                    m_IuserDao.Delete(user);
                    m_IinactiveUserDao.Save(Inusr);
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserDepartment
        /// <summary>
        /// SetBulkUserDepartment (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserDepartment(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            List<ICriterion> criterionList = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                XmlNodeList deptid = root.SelectNodes(@"/login/departments/departmentID");
                for (int i = 0; i < uid.Count; i++)
                {
                    node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    if (deptid.Count != 0)
                    {
                        for (int j = 0; j < deptid.Count; j++)
                        {
                            node = deptid[j];
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("departmentId", Convert.ToInt32(deptid[j].InnerText)));
                            criterionList.Add(Expression.Eq("userId", Convert.ToInt32(uid[i].InnerText)));
                            List<vrmUserDepartment> usrdpt = m_IuserDeptDAO.GetByCriteria(criterionList);
                            if (usrdpt.Count == 0)
                            {
                                vrmUserDepartment usrDpt = new vrmUserDepartment();
                                usrDpt.userId = Convert.ToInt32(uid[i].InnerText);
                                usrDpt.departmentId = Convert.ToInt32(deptid[j].InnerText);
                                m_IuserDeptDAO.Save(usrDpt);
                            }
                        }
                    }
                    else
                    {
                        myVRMEx = new myVRMException(533);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }
                obj.outXml = "<success>Operation successful</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserExpiryDate
        /// <summary>
        /// SetBulkUserExpiryDate (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserExpiryDate(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            string date;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                date = xd.SelectSingleNode("//login/expiryDate").InnerText;
                DateTime expiryDate = Convert.ToDateTime(date);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    criterionList.Add(Expression.Eq("userId", uid));
                    node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    user.accountexpiry = expiryDate;
                    m_IuserDao.Update(user);
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserLanguage
        /// <summary>
        /// SetBulkUserLanguage (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserLanguage(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int langid = 0;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/languageID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out langid);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    criterionList.Add(Expression.Eq("userId", uid));
                    node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    user.Language = langid;
                    m_IuserDao.Update(user);
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserLock
        /// <summary>
        /// SetBulkUserLock (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserLock(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    criterionList.Add(Expression.Eq("userId", uid));
                    node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    user.lockCntTrns = 1;
                    user.LockStatus = vrmUserStatus.MAN_LOCKED;
                    m_IuserDao.Update(user);
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserRole
        /// <summary>
        /// SetBulkUserRole (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserRole(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int roleid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/roleID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out roleid);
                else
                {
                    myVRMEx = new myVRMException(477);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    vrmUserRoles roles = new vrmUserRoles();
                    roles = m_IUserRolesDao.GetById(roleid);
                    string menu = roles.roleMenuMask;
                    int level = roles.level;
                    user.roleID = roleid;
                    user.MenuMask = menu;
                    user.Admin = level;
                    m_IuserDao.Update(user);
                    roles.locked = 1;
                    m_IUserRolesDao.Update(roles);
                    List<ICriterion> criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("createType", 2));
                    List<vrmUserRoles> rolelist = m_IUserRolesDao.GetByCriteria(criterionList1);
                    for (int r = 0; r < rolelist.Count; r++)
                    {
                        List<ICriterion> criterionList2 = new List<ICriterion>();
                        criterionList2.Add(Expression.Eq("roleID", rolelist[r].roleID));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList2);
                        if (usr.Count == 0)
                        {
                            rolelist[r].locked = 0;
                            m_IUserRolesDao.Update(rolelist[r]);
                        }
                    }
                }
                obj.outXml = "<success> Operation succesful </success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserTimeZone
        /// <summary>
        /// SetBulkUserTimeZone (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserTimeZone(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int time = 0;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/timeZoneID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out time);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList uid = root.SelectNodes(@"/login/users/userID");
                for (int i = 0; i < uid.Count; i++)
                {
                    criterionList.Add(Expression.Eq("userId", uid));
                    node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(Int32.Parse(node.InnerXml.Trim()));
                    user.TimeZone = time;
                    m_IuserDao.Update(user);
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2027(Bulk Tools) Ends

        #region GetOldUser

        public bool GetOldUser(ref vrmDataObject obj)
        {
            try
            {
                string lot = "";
                StringBuilder addr = new StringBuilder();
                string video = "";
                string lang = "";
                string bridge = "";
                string dept = "";
                String blckStatus = "0";
                DateTime blockDate = DateTime.Now;
                int loginID = 0;
                int userID = 0;
                int orgid = 0;
                int err = 0;
                StringBuilder outXml = new StringBuilder();
                StringBuilder PCoutXml = new StringBuilder(); //FB 2693
                XmlDocument xd = new XmlDocument();
                myVRMException myVRMEx = null;
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out loginID);

                node = xd.SelectSingleNode("//login/user/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = mutliOrgID;
                //FB 2274 Ends

                List<ICriterion> criterionList = new List<ICriterion>();
                List<ICriterion> criterionList2 = new List<ICriterion>();
                List<ICriterion> criterionList3 = new List<ICriterion>();
                outXml.Append("<oldUser>");

                vrmUser user = m_IuserDao.GetByUserId(userID);
                if (orgid == 0)
                {
                    orgid = user.companyId;
                }

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                hardwareDAO a_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);
                a_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);

                outXml.Append("<userID>" + user.userid.ToString() + "</userID>");
                outXml.Append("<userName>");
                outXml.Append("<firstName>" + user.FirstName + "</firstName>");
                outXml.Append("<lastName>" + user.LastName + "</lastName>");
                outXml.Append("</userName>");
                outXml.Append("<userOrgID>" + user.companyId + "</userOrgID>"); //FB 2865
                outXml.Append("<login>" + user.Login + "</login>");
                if (user.Password != null)
                {
                    //string password = string.Empty; //FB 3054 Starts
                    //if (user.Password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    password = crypto.decrypt(user.Password);
                    //}
                    outXml.Append("<password>" + user.Password + "</password>"); //FB 3054 Ends
                }
                else
                    outXml.Append("<password></password>");
                outXml.Append("<userEmail>" + user.Email + "</userEmail>");
                outXml.Append("<workPhone>" + user.WorkPhone + "</workPhone>");
                outXml.Append("<cellPhone>" + user.CellPhone + "</cellPhone>");
                outXml.Append("<emailClient>" + user.EmailClient + "</emailClient>");
                outXml.Append("<SavedSearch>" + user.searchId + "</SavedSearch>");
                outXml.Append("<timeZone>" + user.TimeZone.ToString() + "</timeZone>");
                outXml.Append("<dateFormat>" + user.DateFormat + "</dateFormat>");
                outXml.Append("<enableAV>" + user.enableAV.ToString() + "</enableAV>");
                outXml.Append("<enableParticipants>" + user.enableParticipants.ToString() + "</enableParticipants>");
                outXml.Append("<timeFormat>" + user.TimeFormat + "</timeFormat>");
                outXml.Append("<timezoneDisplay>" + user.Timezonedisplay + "</timezoneDisplay>");
                outXml.Append("<exchangeUser>" + user.enableExchange.ToString() + "</exchangeUser>");
                outXml.Append("<dominoUser>" + user.enableDomino.ToString() + "</dominoUser>");
                outXml.Append("<mobileUser>" + user.enableMobile.ToString() + "</mobileUser>");
                outXml.Append("<PIMNotification>" + user.PluginConfirmations.ToString() + "</PIMNotification>");//FB 2141
                if (user.TickerSpeed == 0)
                {
                    outXml.Append("<tickerStatus></tickerStatus>");
                    outXml.Append("<tickerSpeed></tickerSpeed>");
                }
                else
                {
                    outXml.Append("<tickerStatus>" + user.TickerStatus + "</tickerStatus>");
                    outXml.Append("<tickerSpeed>" + user.TickerSpeed + "</tickerSpeed>");
                }
                outXml.Append("<tickerPosition>" + user.TickerPosition + "</tickerPosition>");
                outXml.Append("<tickerDisplay>" + user.TickerDisplay + "</tickerDisplay>");
                outXml.Append("<tickerBackground>" + user.TickerBackground + "</tickerBackground>");
                outXml.Append("<rssFeedLink>" + user.RSSFeedLink + "</rssFeedLink>");
                if (user.TickerSpeed1 == 0)
                {
                    outXml.Append("<tickerStatus1></tickerStatus1>");
                    outXml.Append("<tickerSpeed1></tickerSpeed1>");
                }
                else
                {
                    outXml.Append("<tickerStatus1>" + user.TickerStatus1 + "</tickerStatus1>");
                    outXml.Append("<tickerSpeed1>" + user.TickerSpeed1 + "</tickerSpeed1>");
                }
                outXml.Append("<tickerPosition1>" + user.TickerPosition1 + "</tickerPosition1>");
                outXml.Append("<tickerDisplay1>" + user.TickerDisplay1 + "</tickerDisplay1>");
                outXml.Append("<tickerBackground1>" + user.TickerBackground1 + "</tickerBackground1>");
                outXml.Append("<rssFeedLink1>" + user.RSSFeedLink1 + "</rssFeedLink1>");
                if (user.Audioaddon == null)//FB 2027
                    user.Audioaddon = "0";
                outXml.Append("<Audioaddon>" + user.Audioaddon + "</Audioaddon>");

                List<ICriterion> criterionEmail = new List<ICriterion>();
                criterionEmail.Add(Expression.Eq("EmailLangId", user.EmailLangId));
                List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterionEmail);
                if (emailLangs.Count != 0)
                {
                    for (int e = 0; e < emailLangs.Count; e++)
                    {
                        outXml.Append("<EmailLang>" + emailLangs[e].EmailLangId.ToString() + "</EmailLang>");
                        outXml.Append("<EmailLangName>" + emailLangs[e].EmailLanguage + "</EmailLangName>");
                    }
                }
                else
                {
                    outXml.Append("<EmailLang></EmailLang>");
                    outXml.Append("<EmailLangName></EmailLangName>");
                }

                outXml.Append("<systemExpiryDate>" + sysSettings.ExpiryDate.ToString("d") + "</systemExpiryDate>");
                StringBuilder tzOutXml = new StringBuilder();
                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);
                m_OrgFactory.organizationID = orgid;
                m_OrgFactory.timeZonesToXML(ref tzOutXml);
                outXml.Append(tzOutXml);

                outXml.Append("<locationList>");
                outXml.Append("<selected>");
                outXml.Append("<level1ID>" + user.PreferedRoom.ToString() + "</level1ID>");
                outXml.Append("</selected>");
                outXml.Append("</locationList>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                    Expression.Eq("Owner", user.userid)));
                criterionList.Add(Expression.Eq("GroupID", user.PreferedGroup));
                criterionList.Add(Expression.Eq("orgId", orgid));
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);
                foreach (vrmGroupDetails gd in Groups)
                {
                    outXml.Append("<defaultAGroups>");
                    outXml.Append("<groupID>" + gd.GroupID.ToString() + "</groupID>");
                    outXml.Append("</defaultAGroups>");
                }
                criterionList.Add(Expression.Eq("GroupID", user.CCGroup));
                List<vrmGroupDetails> ccGroups = m_IGrpDetailDao.GetByCriteria(criterionList);

                foreach (vrmGroupDetails gc in ccGroups)
                {
                    outXml.Append("<defaultCGroups>");
                    outXml.Append("<groupID>" + gc.GroupID.ToString() + "</groupID>");
                    outXml.Append("</defaultCGroups>");
                }

                criterionList2.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userID)));
                criterionList2.Add(Expression.Eq("orgId", orgid));
                outXml.Append("<groups>");
                FetchGroups(criterionList2, ref outXml, 0, "group");
                outXml.Append("</groups>");

                outXml.Append("<alternativeEmail>" + user.AlternativeEmail + "</alternativeEmail>");
                outXml.Append("<sendBoth>" + user.DoubleEmail.ToString() + "</sendBoth>");

                outXml.Append("<lineRateID>" + user.DefLineRate + "</lineRateID>");

                List<vrmLineRate> LineRate = vrmGen.getLineRate();
                outXml.Append("<lineRate>");

                foreach (vrmLineRate lr in LineRate)
                {
                    outXml.Append("<rate>");
                    outXml.Append("<lineRateID>" + lr.Id.ToString() + "</lineRateID>");
                    outXml.Append("<lineRateName>" + lr.LineRateType + "</lineRateName>");
                    outXml.Append("</rate>");
                }
                outXml.Append("</lineRate>");

                IEptDao a_IeptDao;
                a_IeptDao = a_hardwareDAO.GetEptDao();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("endpointid", user.endpointId));
                List<vrmEndPoint> epList = a_IeptDao.GetByCriteria(criterionList);

                vrmEndPoint ep = new vrmEndPoint();
                if (epList.Count > 0)
                    ep = epList[0];

                outXml.Append("<videoProtocol>" + ep.protocol + "</videoProtocol>");
                outXml.Append("<conferenceCode>" + ep.ConferenceCode + "</conferenceCode>");
                outXml.Append("<leaderPin>" + ep.LeaderPin + "</leaderPin>");
                outXml.Append("<IPISDNAddress>" + ep.address + "</IPISDNAddress>");
                outXml.Append("<connectionType>" + ep.connectiontype + "</connectionType>");
                outXml.Append("<isOutside>" + ep.outsidenetwork + "</isOutside>");
                outXml.Append("<emailMask>" + user.emailmask + "</emailMask>");
                outXml.Append("<addressTypeID>" + ep.addresstype + "</addressTypeID>");
                outXml.Append("<ExchangeID>" + ep.ExchangeID + "</ExchangeID>");
                outXml.Append("<APIPortNo>" + ep.ApiPortNo + "</APIPortNo>");
                outXml.Append("<URL>" + ep.endptURL + "</URL>");
                outXml.Append("<videoEquipmentID>" + ep.videoequipmentid + "</videoEquipmentID>");

                if (FetchVideoEquipment2(user.DefaultEquipmentId.ToString(), userID, 0, obj, ref video))
                    outXml.Append(video);

                outXml.Append("<multiDepartment>" + multiDepts.ToString() + "</multiDepartment>");

                if (FetchDepartmentList(userID, obj, 1, err, orgid, ref dept))
                    outXml.Append(dept);

                criterionList3.Add(Expression.Eq("userId", user.userid));
                List<vrmAccount> uAccount = m_IuserAccountDAO.GetByCriteria(criterionList3);
                if (uAccount.Count != 0)
                {
                    for (int i = 0; i < uAccount.Count; i++)
                        outXml.Append("<initialTime>" + uAccount[i].TimeRemaining.ToString() + "</initialTime>");
                }
                else
                {
                    outXml.Append("<initialTime></initialTime>");
                }
                outXml.Append("<expiryDate>" + user.accountexpiry.ToString("d") + "</expiryDate>");
                outXml.Append("<bridgeID>" + user.BridgeID.ToString() + "</bridgeID>");
                outXml.Append("<EndpointID>" + user.endpointId.ToString() + "</EndpointID>");
                outXml.Append("<status>");
                outXml.Append("<level>" + user.Admin + "</level>");
                outXml.Append("<deleted>0</deleted>");

                string locked = "0";
                if (user.lockCntTrns > 0)
                    locked = "1";

                outXml.Append("<locked>" + locked + "</locked>");
                outXml.Append("</status>");
                outXml.Append("<creditCard></creditCard>");

                outXml.Append("<languageID>" + user.Language + "</languageID>");

                if (OldFetchLanguageList(obj, ref lang))
                    outXml.Append(lang);

                outXml.Append("<roleID>" + user.roleID + "</roleID>");
                IUserRolesDao a_IUserRolesDao = m_userDAO.GetUserRolesDao();
                List<vrmUserRoles> uRoleList = a_IUserRolesDao.GetAll();

                outXml.Append("<roles>");
                foreach (vrmUserRoles uRole in uRoleList)
                {
                    outXml.Append("<role>");
                    outXml.Append("<ID>" + uRole.roleID.ToString() + "</ID>");
                    outXml.Append("<name>" + uRole.roleName + "</name>");
                    outXml.Append("<menuMask>" + uRole.roleMenuMask + "</menuMask>");
                    outXml.Append("<locked>" + uRole.locked.ToString() + "</locked>");
                    outXml.Append("<level>" + uRole.level + "</level>");
                    IUserTemplateDao a_userTemplateDAO = m_userDAO.GetUserTemplateDao();
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("roleId", user.roleID));
                    List<vrmUserTemplate> uTempList = a_userTemplateDAO.GetByCriteria(criterionList);
                    if (uTempList.Count > 0)
                        outXml.Append("<active>1</active>");
                    else
                        outXml.Append("<active>0</active>");
                    outXml.Append("<createType>" + uRole.createType + "</createType>");
                    outXml.Append("</role>");
                }
                outXml.Append("</roles>");

                if (FetchLotusInfo(userID, obj, ref lot))
                    outXml.Append(lot);

                if (OldFetchBridgeList(orgid, obj, ref bridge))
                    outXml.Append(bridge);

                if (FetchAddressTypeList(obj, ref addr))
                    outXml.Append(addr);

                if (user.MailBlocked == 1)
                {
                    blckStatus = "1";
                    blockDate = user.MailBlockedDate;
                    timeZone.GMTToUserPreferedTime(user.TimeZone, ref blockDate);
                }
                //FB 2227 Start
                outXml.Append("<IntVideoNum>" + user.InternalVideoNumber + "</IntVideoNum>");
                outXml.Append("<ExtVideoNum>" + user.ExternalVideoNumber + "</ExtVideoNum>");
                //FB 2227 End
                outXml.Append("<HelpRequsetorEmail>" + user.HelpReqEmailID + "</HelpRequsetorEmail>"); //FB 2268
                outXml.Append("<HelpRequsetorPhone>" + user.HelpReqPhone + "</HelpRequsetorPhone>"); //FB 2268
                outXml.Append("<RFIDValue>" + user.RFIDValue + "</RFIDValue>"); //FB 2724
                outXml.Append("<SendSurveyEmail>" + user.SendSurveyEmail + "</SendSurveyEmail>"); //FB 2348
                outXml.Append("<EnableVNOCselection>" + user.EnableVNOCselection + "</EnableVNOCselection>"); //FB 2608
                outXml.Append("<PrivateVMR>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(user.PrivateVMR) + "</PrivateVMR>"); //FB 2481  //FB 2721
                //FB 2262 //FB 2599 code added starts
                outXml.Append("<VidyoURL>" + user.VidyoURL + "</VidyoURL>");
                outXml.Append("<Extension>" + user.Extension + "</Extension>");
                outXml.Append("<Pin>" + user.Pin + "</Pin>");
                //FB 2262 //FB 2599 code added ends
                //FB 2392-Whygo Starts
                outXml.Append("<ParentReseller>" + user.ParentReseller + "</ParentReseller>");
                outXml.Append("<CorpUser>" + user.CorpUser + "</CorpUser>");
                outXml.Append("<Region>" + user.Region + "</Region>");
                outXml.Append("<ESUserID>" + user.ESUserID + "</ESUserID>");
                //FB 2392-Whygo End
                outXml.Append("<BrokerRoomNum>" + user.BrokerRoomNum + "</BrokerRoomNum>"); //FB 3001
                //outXml.Append("<Secured>" + user.Secured + "</Secured>"); //FB 2595 
                outXml.Append("<login><emailBlockStatus>" + blckStatus + "</emailBlockStatus><emailBlockDate>" + blockDate.ToString() + "</emailBlockDate></login>");
                //FB 2655 - DTMF Start 
                outXml.Append("<DTMF>");
                outXml.Append("<PreConfCode>" + user.PreConfCode + "</PreConfCode>");
                outXml.Append("<PreLeaderPin>" + user.PreLeaderPin + "</PreLeaderPin>");
                outXml.Append("<PostLeaderPin>" + user.PostLeaderPin + "</PostLeaderPin>");
                outXml.Append("<AudioDialInPrefix>" + user.AudioDialInPrefix + "</AudioDialInPrefix>");
                outXml.Append("</DTMF>");
                //FB 2655 - DTMF End
                //FB 2693 Starts
                outXml.Append("<EnablePCUser>" + user.enablePCUser + "</EnablePCUser>");
                if (user.enablePCUser > 0)
                {
                    GetUserPCDetails(user.userid,0, ref PCoutXml);
                    outXml.Append(PCoutXml);
                }
                //FB 2693 Ends
                outXml.Append("</oldUser>");
                obj.outXml = outXml.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region OldFetchBridgeList
        /// <summary>
        /// OldFetchBridgeList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchBridgeList(int orgid, vrmDataObject obj, ref string bridgeout)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterion = new List<ICriterion>();
            try
            {
                outXml.Append("<bridges>");
                criterion.Add(Expression.Eq("VirtualBridge", 0));
                criterion.Add(Expression.Eq("deleted", 0));
                criterion.Add(Expression.Eq("orgId", orgid));
                List<vrmMCU> bridges = m_ImcuDao.GetByCriteria(criterion);

                for (int i = 0; i < bridges.Count; i++)
                {
                    outXml.Append("<bridge>");
                    outXml.Append("<ID>" + bridges[i].BridgeID.ToString() + "</ID>");
                    outXml.Append("<name>" + bridges[i].BridgeName + "</name>");
                    outXml.Append("</bridge>");
                }
                outXml.Append("</bridges>");
                obj.outXml = outXml.ToString();
                bridgeout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region FetchDepartmentList
        /// <summary>
        /// FetchDepartmentList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchDepartmentList(int userid, vrmDataObject obj, int mode, int err, int orgid, ref string deptout)
        {
            string admin = "";
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterion = new List<ICriterion>();
            List<ICriterion> criterion2 = new List<ICriterion>();
            List<ICriterion> criterion3 = new List<ICriterion>();
            try
            {
                outXml.Append("<departments>");
                if (userid != 0)
                {
                    vrmUser user = m_IuserDao.GetByUserId(userid);
                    admin = Convert.ToString(user.Admin);
                    if (mode == 1)
                    {
                        criterion.Add(Expression.Eq("userId", userid));
                        List<vrmUserDepartment> deptlist = m_IuserDeptDAO.GetByCriteria(criterion);
                        for (int i = 0; i < deptlist.Count; i++)
                        {
                            outXml.Append("<selected>" + deptlist[i].departmentId.ToString() + "</selected>");
                        }
                        criterion2.Add(Expression.Eq("deleted", 0));
                        criterion2.Add(Expression.Eq("orgId", Convert.ToString(orgid)));
                        List<vrmDept> depart = m_IdeptDAO.GetByCriteria(criterion2);
                        for (int j = 0; j < depart.Count; j++)
                        {
                            outXml.Append("<department>");
                            outXml.Append("<id>" + depart[j].departmentId.ToString() + "</id>");
                            outXml.Append("<name>" + depart[j].departmentName + "</name>");
                            outXml.Append("<securityKey>" + depart[j].securityKey + "</securityKey>");
                            outXml.Append("</department>");
                        }
                    }
                    else if (mode == 0)
                    {
                        if (admin == "2")
                        {
                            List<vrmDept> depart = m_IdeptDAO.GetByCriteria(criterion2);
                            for (int j = 0; j < depart.Count; j++)
                            {
                                outXml.Append("<department>");
                                outXml.Append("<id>" + depart[j].departmentId.ToString() + "</id>");
                                outXml.Append("<name>" + depart[j].departmentName + "</name>");
                                outXml.Append("<securityKey>" + depart[j].securityKey + "</securityKey>");
                                outXml.Append("</department>");
                            }
                        }
                        else
                        {
                            vrmUserDepartment dept = m_IuserDeptDAO.GetById(userid); //FB 2027
                            criterion3.Add(Expression.Eq("departmentId", dept.departmentId));
                            List<vrmDept> dept1 = m_IdeptDAO.GetByCriteria(criterion3);
                            for (int k = 0; k < dept1.Count; k++)
                            {
                                outXml.Append("<department>");
                                outXml.Append("<id>" + dept1[k].departmentId.ToString() + "</id>");
                                outXml.Append("<name>" + dept1[k].departmentName + "</name>");
                                outXml.Append("<securityKey>" + dept1[k].securityKey + "</securityKey>");
                                outXml.Append("</department>");
                            }
                        }
                    }
                }
                outXml.Append("</departments>");
                obj.outXml = outXml.ToString();
                deptout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //FB 2027 SetUser Start

        #region SetUser
        /// <summary>
        /// SetUser
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUser(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false;
                int loginID = 0, userid = 0, deleted = 0, roleID = 0, initialTime = 0, err = 0;
                string userID = "", lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", outputXML = "";
                string firstname = "", lastname = "", password = "", email = "", altemail = "", login = "";
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        Int32.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/audioaddon"); //FB 2023
                if (node != null)
                    if (node.InnerText.Trim() != "")
                        Int32.TryParse(node.InnerText.Trim(), out isAudioAddOn);


                if (userID == "new")
                {
                    mode = CREATENEW;
                }
                else
                {
                    mode = MODIFYOLD;
                    Int32.TryParse(userID, out userid);
                }

                if (mode == CREATENEW)
                {
                    try
                    {
                        if (userDeleted == false)
                        {
                            vrmUser user = new vrmUser();
                            if (!ActiveUser(ref obj, ref user, err))
                            {
                                outputXML = obj.outXml;
                                return false;
                            }
                            else
                            {
                                userid = user.userid;
                            }
                        }
                        else
                        {
                            vrmInactiveUser Inuser = new vrmInactiveUser();
                            if (!InActiveUser(ref obj, ref Inuser, err))
                            {
                                outputXML = obj.outXml;
                                return false;
                            }
                            else
                            {
                                userid = Inuser.userid;
                            }
                        }

                        List<ICriterion> criterionLot = new List<ICriterion>();
                        criterionLot.Add(Expression.Eq("userid", userid));
                        List<vrmUserLotusNotesPrefs> usrlot = m_userlotusDAO.GetByCriteria(criterionLot);
                        foreach (vrmUserLotusNotesPrefs usrlt in usrlot)
                            if (usrlt != null)
                                m_userlotusDAO.Delete(usrlt);
                        if (lotus != "")
                        {
                            vrmUserLotusNotesPrefs usrlotus = new vrmUserLotusNotesPrefs();
                            usrlotus.userid = userid;
                            usrlotus.noteslogin = lnLoginName;
                            usrlotus.notespwd = lnLoginPwd;
                            usrlotus.notespath = lnDBPath;
                            m_userlotusDAO.Save(usrlotus);
                        }
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    try
                    {
                        List<ICriterion> criterionList6 = new List<ICriterion>();
                        int insertTime = 0;
                        if (initialTime == 0)
                        {
                            insertTime = TOTALTIMEFORNEWUSER;
                        }
                        else
                        {
                            insertTime = initialTime;
                        }
                        vrmAccount usracc = new vrmAccount();
                        usracc.userId = userid;
                        usracc.TotalTime = insertTime;
                        usracc.TimeRemaining = insertTime;
                        usracc.InitTime = DateTime.Now;
                        usracc.ExpirationTime = DateTime.Today.AddYears(10);
                        m_IuserAccountDAO.Save(usracc);
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    List<ICriterion> criterionList7 = new List<ICriterion>();
                    criterionList7.Add(Expression.Eq("roleID", roleID));
                    List<vrmUserRoles> usrrole = m_IUserRolesDao.GetByCriteria(criterionList7);
                    for (int i = 0; i < usrrole.Count; i++)
                    {
                        usrrole[i].locked = 1;
                        m_IUserRolesDao.Update(usrrole[i]);
                    }
                }
                else
                {
                    Config config = new Config(m_configPath);
                    bool ret = config.Initialize();
                    if (!config.ldapEnabled)
                    {
                        string output = "";
                        output = CheckUser(userid, firstname, lastname, email, altemail, login, password, 1, ref err, loginID, obj);
                        if (err < 0)
                        {
                            obj.outXml = output;
                            return false;
                        }
                    }
                    try
                    {
                        vrmInactiveUser LdInusr = m_IinactiveUserDao.GetByUserId(userid);
                        int numrows = 0, oroleID = 0;
                        vrmUser Ldusr = m_IuserDao.GetByUserId(userid);
                        oroleID = Ldusr.roleID;
                        List<ICriterion> criterionList9 = new List<ICriterion>();
                        criterionList9.Add(Expression.Eq("roleID", oroleID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList9);
                        numrows = usrlist.Count;
                        if (numrows == 1)
                        {
                            //FB 2164 Start
                            criterionList9.Add(Expression.Eq("roleID", oroleID));
                            List<vrmInactiveUser> Inusrlist = m_IinactiveUserDao.GetByCriteria(criterionList9);
                            if (Inusrlist.Count == 0)
                            {
                                List<ICriterion> critrole = new List<ICriterion>();
                                critrole.Add(Expression.Eq("roleID", oroleID)); 
                                critrole.Add(Expression.Eq("createType", 2));
                                List<vrmUserRoles> usrrle = m_IUserRolesDao.GetByCriteria(critrole);
                                for (int j = 0; j < usrrle.Count; j++)
                                {
                                    usrrle[j].locked = 0;
                                    m_IUserRolesDao.Update(usrrle[j]);
                                }
                            }
                            //FB 2164 End
                        }
                        if (userDeleted == false)
                        {
                            if (!LDAPActiveUser(ref obj, userid))
                                return false;
                        }
                        else
                        {
                            if (!LDAPInActiveUser(ref obj, userid))
                                return false;
                        }
                        if (userDeleted == false)
                        {
                            List<ICriterion> critrol = new List<ICriterion>();
                            critrol.Add(Expression.Eq("roleID", roleID));
                            List<vrmUserRoles> usrle = m_IUserRolesDao.GetByCriteria(critrol);
                            for (int k = 0; k < usrle.Count; k++)
                            {
                                usrle[k].locked = 1;
                                m_IUserRolesDao.Update(usrle[k]);
                            }
                        }
                        List<ICriterion> criterionLot = new List<ICriterion>();
                        criterionLot.Add(Expression.Eq("userid", userid));
                        List<vrmUserLotusNotesPrefs> usrlot = m_userlotusDAO.GetByCriteria(criterionLot);
                        foreach (vrmUserLotusNotesPrefs usrlt in usrlot)
                            if (usrlt != null)
                                m_userlotusDAO.Delete(usrlt);
                        if (lotus != "")
                        {
                            vrmUserLotusNotesPrefs usrlotus = new vrmUserLotusNotesPrefs();
                            usrlotus.userid = userid;
                            usrlotus.noteslogin = lnLoginName;
                            usrlotus.notespwd = lnLoginPwd;
                            usrlotus.notespath = lnDBPath;
                            m_userlotusDAO.Save(usrlotus);
                        }
                        if (userDeleted == false)
                        {
                            int totalTime = 0;
                            List<ICriterion> criterionList6 = new List<ICriterion>();
                            criterionList6.Add(Expression.Eq("userId", userid));
                            List<vrmAccount> uAccount = m_IuserAccountDAO.GetByCriteria(criterionList6);
                            for (int c = 0; c < uAccount.Count; c++)
                            {
                                //ZD 100263 Starts //NATOFIX
                                vrmUser LoginUser = m_IuserDao.GetByUserId(loginID);
                                vrmUserRoles LoginUserRole = m_IUserRolesDao.GetById(LoginUser.roleID);//ZD 100263
                                if (!CheckUserRights(LoginUser, Ldusr))
                                {
                                    if (LoginUser.userid != Ldusr.userid)
                                    {
                                        myvrmEx = new myVRMException(229);
                                        obj.outXml = myvrmEx.FetchErrorMsg();
                                        return false;
                                    }
                                }
                                else if (LoginUser.userid != Ldusr.userid)
                                {
                                    totalTime = uAccount[c].TotalTime;
                                    if (initialTime > totalTime)
                                        totalTime = initialTime;
                                    uAccount[c].TimeRemaining = initialTime;
                                    uAccount[c].TotalTime = totalTime;
                                    m_IuserAccountDAO.Update(uAccount[c]);
                                }
                                //ZD 100263 Ends

                            }

                        }
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    bool userExists = false;
                    try
                    {

                        List<ICriterion> criterionL = new List<ICriterion>();
                        criterionL.Add(Expression.Eq("userId", userid));
                        List<vrmAccount> usracc1 = m_IuserAccountDAO.GetByCriteria(criterionL);
                        if (usracc1.Count != 0)
                            userExists = true;
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    if (!userExists)
                    {
                        vrmAccount usracc1 = m_IuserAccountDAO.GetByUserId(userid);
                        usracc1.userId = userid;
                        usracc1.TotalTime = TOTALTIMEFORNEWUSER;
                        usracc1.TimeRemaining = TOTALTIMEFORNEWUSER;
                        usracc1.InitTime = DateTime.Now;
                        usracc1.ExpirationTime = DateTime.Today.AddYears(10);
                        m_IuserAccountDAO.Save(usracc1);
                    }
                }
                List<ICriterion> criterionD = new List<ICriterion>();
                criterionD.Add(Expression.Eq("userId", userid));
                List<vrmUserDepartment> depart = m_IuserDeptDAO.GetByCriteria(criterionD);
                for (int d = 0; d < depart.Count; d++)
                {
                    m_IuserDeptDAO.Delete(depart[d]);
                }
                XmlNodeList deplist = xd.SelectNodes("//saveUser/user/departments/id");

                if (deplist.Count > 0)
                {
                    for (int i = 0; i < deplist.Count; i++)
                    {
                        int id = 0;
                        Int32.TryParse(deplist[i].InnerText.Trim(), out id);
                        vrmUserDepartment usrdept = new vrmUserDepartment();
                        usrdept.userId = userid;
                        usrdept.departmentId = id;
                        m_IuserDeptDAO.Save(usrdept);
                    }
                }
                outputXML = "<SetUser><userID>" + userid + "</userID></SetUser>";
                //FB 2158 start
                if (mode == CREATENEW)
                    WelcomeNewUser(ref obj);
                //FB 2158 end
                obj.outXml = outputXML;
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }


        #endregion

        #region FetchMaxUserID
        public bool FetchMaxUserID(int err, string errmsg, vrmDataObject obj, ref int maxuserid)
        {
            int maxID = 0, maxIDInactive = 0, maxIDUser = 0, maxIDGuest = 0;
            try
            {
                vrmUser user = new vrmUser();
                vrmInactiveUser userIn = new vrmInactiveUser();
                vrmGuestUser guest = new vrmGuestUser();
                m_IuserDao.addProjection(Projections.Max("userid"));
                IList maxId1 = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                if (maxId1[0] != null)
                    maxIDUser = ((int)maxId1[0]);

                m_IinactiveUserDao.addProjection(Projections.Max("userid"));
                IList maxId2 = m_IinactiveUserDao.GetObjectByCriteria(new List<ICriterion>());
                if (maxId2[0] != null)
                    maxIDInactive = ((int)maxId2[0]);

                m_IGuestUserDao.addProjection(Projections.Max("userid"));
                IList maxId3 = m_IGuestUserDao.GetObjectByCriteria(new List<ICriterion>());
                if (maxId3[0] != null)
                    maxIDGuest = ((int)maxId3[0]);
                if (err < 0)
                    return false;
                if (maxIDUser > maxIDInactive)
                {
                    if (maxIDUser > maxIDGuest)
                        maxID = maxIDUser;
                    else
                        maxID = maxIDGuest;
                }
                else
                {
                    if (maxIDInactive > maxIDGuest)
                        maxID = maxIDInactive;
                    else
                        maxID = maxIDGuest;
                }
                maxuserid = maxID;
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region CheckUser
        /// <summary>
        /// CheckUser
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="emailAddress"></param>
        /// <param name="altemail"></param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="mode"></param>
        /// <param name="err"></param>
        /// <param name="loginID"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string CheckUser(int userid, string firstname, string lastname, string emailAddress, string altemail, string login, string password, int mode, ref int err, int loginID, vrmDataObject obj)
        {
            string email = ""; //ZD 100263_SQL
            try
            {
                //ZD 100263_SQL Starts
                //firstname - used only in nHibernate
                //lastname  - Not used anywhere in this command
                email = emailAddress.Replace("'", "''"); // Used in both SQL and nHibernate. So changed variable name
                altemail = altemail.Replace("'", "''"); // Used only in SQL 
                login = login.Replace("'", "''"); //Used in SQL and nHibernate( but no use in nHibernate).
                //ZD 100263_SQL End

                string stmt;
                DataSet ds = null;
                if (m_rptLayer == null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);

                vrmUser actuser = new vrmUser();
                if (isAudioAddOn == 1) //FB 2023
                {
                    if (firstname == "" || email == "")
                    {
                        err = -1;
                        obj.outXml = "Blank Fields";
                    }
                }
                else
                {
                    if (firstname == "" || lastname == "" || email == "")
                    {
                        err = -1;
                        obj.outXml = "Blank Fields";
                    }
                }

                if (isAudioAddOn == 1) //FB 2023
                {
                    stmt = "Select * from Usr_List_D where Audioaddon = 1 and Deleted = 0 and email = '"
                          + email.Trim()+"' and userid != " + userid;
                    if (stmt != "")
                        ds = m_rptLayer.ExecuteDataSet(stmt);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        err = -2;
                        myVRMException e = new myVRMException(335);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                        return obj.outXml;
                    }
                }
                if (mode != 2)
                {
                    if (mode == 0)
                    {
                        stmt = "SELECT firstname, lastname from Usr_List_D WHERE ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " userid=" + userid + " or email='" + email + "'";
                        if (login.Length > 0)
                        {
                            stmt += "  or login = '" + login + "'";
                        }
                    }
                    else if (mode == 3)
                    {
                        stmt = "SELECT firstname, lastname FROM Usr_List_D WHERE ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += "email = '" + email + "' AND userid != " + userid;
                    }
                    else
                    {
                        stmt = " select firstname, lastname from Usr_List_D WHERE (";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " email='" + email + "'";
                        if (login.Length > 0)
                        {
                            stmt += "  or login = '" + login + "'";
                        }
                        stmt += " ) and userid!=" + userid;
                    }
                    if (stmt != "")
                        ds = m_rptLayer.ExecuteDataSet(stmt);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        err = -2;
                        myVRMException e = new myVRMException(261);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }

                    if (mode == 0)
                    {
                        stmt = "select firstname, lastname from Usr_Inactive_D where ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " userid=" + userid + " or email='" + email + "'";
                        if (login.Length > 0)
                        {
                            stmt += "  or login = '" + login + "'";
                        }
                    }
                    else if (mode == 3)
                    {
                        stmt = "SELECT firstname, lastname FROM Usr_Inactive_D WHERE ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += "email = '" + email + "' AND userid != " + userid;
                    }
                    else
                    {
                        stmt = " select firstname, lastname from Usr_Inactive_D where ( ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " email='" + email + "'";
                        if (login.Length > 0)
                        {
                            stmt += "  or login = '" + login + "'";
                        }
                        stmt += " ) and userid!=" + userid;
                    }
                    if (stmt != "")
                        ds = m_rptLayer.ExecuteDataSet(stmt);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        err = -2;
                        myVRMException e = new myVRMException(426);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                }
                else
                {
                    List<ICriterion> Email = new List<ICriterion>();
                    Email.Add(Expression.Eq("Email", emailAddress)); //ZD 100263_SQL
                    //Email.Add(Expression.Eq("Email", email));
                    List<vrmUser> mail = m_IuserDao.GetByCriteria(Email);
                    if (mail.Count > 0)
                    {
                        for (int i = 0; i < mail.Count; i++)
                        {
                            if (mail[i].FirstName == "" || mail[i].LastName == "" || mail[i].Login == "")
                            {
                                err = 1;
                                userid = mail[i].userid;
                                obj.outXml = "";
                            }
                            else
                            {
                                err = -3;
                                myVRMException e = new myVRMException(261);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                            }
                        }
                    }
                    else
                    {
                        List<ICriterion> fname = new List<ICriterion>();
                        fname.Add(Expression.Eq("FirstName", firstname));
                        List<vrmUser> first = m_IuserDao.GetByCriteria(fname);
                        if (first.Count > 0)
                        {
                            login = firstname + userid;
                            password = login;
                        }
                        else
                        {
                            login = firstname;
                            password = login;
                        }
                    }
                }
                return obj.outXml;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region ActiveUser
        private bool ActiveUser(ref vrmDataObject obj, ref vrmUser user, int err)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false;
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, superadmin;
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0, endpointId = 0, userid = 0, timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0, tickerStatus1 = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0, tickerStatus = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, enablePIMnotification = 0, SendSurveyEmail = 0, EnableVNOCselection=0;//FB 2141 FB 2348 FB 2608
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0; //FB 2392-Whygo 
                string BrokerRoomNum = ""; //FB 3001
				//int Secured = 0;//FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "";
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", timezonedisplay = "", tickerBackground = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail="", HelpReqPhone = "";//FB 2227//FB 2268
                string PrivateVMR = "";//FB 2481
                string VidyoURL = "", Extension = "", Pin = ""; //FB 2262 //FB 2599
                String PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 FB 2724
                int enablePCUser = 0; //FB 2693
                DateTime accountexpiry = new DateTime();

                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    if (location.Length == 0)
                        location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out prefergroup);
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                Int32.TryParse(EMailLanguage, out EmailLanguage);
                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        Int32.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    if (accountexpiry == DateTime.MinValue)
                        accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                }
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableParticipants);
                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableMob);
                node = xd.SelectSingleNode("//saveUser/PIMNotification");//FB 2141
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enablePIMnotification);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail");//FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone");//FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail");//FB 2348
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SendSurveyEmail); //FB 2633

                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection); //FB 2633
                
                node = xd.SelectSingleNode("//saveUser/PrivateVMR");//FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End

                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out Region);

                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End

                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();

                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    Int32.TryParse(node.InnerText.Trim(), out Secured);

                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End

                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");
               
                //FB 2693 Ends
                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcUsrLimit = 0; //FB 2693
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1; //FB 2693
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcUsrLimit = orgInfo.PCUserLimit; //FB 2693
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        if (newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        Int32.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > exchangeUsrLimit)
                        {
                            myVRMException e = new myVRMException(460);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > dominoUsrLimit)
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > mobileUsrLimit)
                        {
                            myVRMException e = new myVRMException(526);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcUsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    int maxusrid = 0; string errMsg = "", Output = "";
                    if (FetchMaxUserID(err, errMsg, obj, ref maxusrid))
                        if (err < 0)
                            obj.outXml = errMsg;
                    userid = maxusrid + 1;
                    Output = CheckUser(userid, firstname, lastname, email, altemail, login, password, 0, ref err, loginID, obj);
                    if (err < 0)
                    {
                        Output = obj.outXml;
                        return false;
                    }
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string menu = usr1rle.roleMenuMask;
                    user = new vrmUser();
                    user.userid = userid;
                    user.FirstName = firstname;
                    user.LastName = lastname;
                    string pass = string.Empty;
                    //if (password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    pass = crypto.encrypt(password);
                    //}
                    user.Password = password; //FB 3054
                    user.TimeZone = timezoneid;
                    user.PreferedRoom = location;
                    user.PreferedGroup = prefergroup;
                    user.CCGroup = preferCCgroup;
                    user.Email = email;
                    user.WorkPhone = workPhone;
                    user.CellPhone = cellPhone;
                    user.AlternativeEmail = altemail;
                    user.DoubleEmail = doubleemail;
                    user.DefLineRate = defaultLineRate;
                    user.DefVideoProtocol = defaultVideoProtocol;
                    user.ConferenceCode = conferenceCode;
                    user.LeaderPin = leaderPin;
                    user.Admin = admin;
                    user.Deleted = deleted;
                    user.lockCntTrns = locked;
                    user.Login = login;
                    user.roleID = roleID;
                    user.EmailClient = emailClient;
                    user.newUser = 1;
                    user.IPISDNAddress = IPISDNAddress;
                    user.BridgeID = bridgeID;
                    user.DefaultEquipmentId = defaultEquipment;
                    user.connectionType = connectionType;
                    user.Language = languageID;
                    user.EmailLangId = EmailLanguage;
                    user.outsidenetwork = outsidenetwork;
                    user.emailmask = emailmask;
                    user.addresstype = addresstype;
                    user.accountexpiry = accountexpiry;
                    user.searchId = SavedSearch;
                    user.endpointId = endpointId;
                    user.DateFormat = dateformat;
                    user.enableAV = enableAV;
                    user.enableParticipants = enableParticipants;
                    user.TimeFormat = timeformat;
                    user.Timezonedisplay = timezonedisplay;
                    user.enableExchange = enableExc;
                    user.enableDomino = enableDom;
                    user.enableMobile = enableMob;
                    user.TickerStatus = tickerStatus;
                    user.TickerPosition = tickerPosition;
                    user.TickerSpeed = tickerSpeed;
                    user.TickerBackground = tickerBackground;
                    user.TickerDisplay = tickerDisplay;
                    user.RSSFeedLink = rssFeedLink;
                    user.TickerStatus1 = tickerStatus1;
                    user.TickerPosition1 = tickerPosition1;
                    user.TickerSpeed1 = tickerSpeed1;
                    user.TickerBackground1 = tickerBackground1;
                    user.TickerDisplay1 = tickerDisplay1;
                    user.RSSFeedLink1 = rssFeedLink1;
                    user.Audioaddon = Audioaddon;
                    user.LockStatus = locked;
                    user.companyId = organizationID;
                    user.MenuMask = menu;
                    user.PluginConfirmations = enablePIMnotification;//FB 2141
                    //FB 2227 - Start
                    user.InternalVideoNumber = InternalVideoNumber;
                    user.ExternalVideoNumber = ExternalVideoNumber;
                    //FB 2227 - End
                    user.HelpReqEmailID = HelpReqEmail; //FB 2268
                    user.HelpReqPhone = HelpReqPhone;//FB 2268
                    user.RFIDValue = RFIDValue;//FB 2724
                    user.SendSurveyEmail = SendSurveyEmail;//FB 2348
                    user.EnableVNOCselection = EnableVNOCselection;//FB 2608
                    user.PrivateVMR = PrivateVMR;//FB 2481
					//FB 2599 Start
                    //FB 2262
                    user.VidyoURL = VidyoURL;
                    user.Extension = Extension;
                    user.Pin = Pin;
                    //FB 2599 end

					//FB 2392-Whygo Starts
                    user.ParentReseller = ParentReseller;
                    user.CorpUser = CorpUser;
                    user.Region = Region;
                    user.ESUserID = ESUserID;
                    //FB 2392-Whygo End
                    user.BrokerRoomNum = BrokerRoomNum; //FB 3001
                    //user.Secured = Secured; //FB 2595
                    //FB 2655 Start
                    user.PreConfCode = PreConfCode;
                    user.PreLeaderPin = PreLeaderPin;
                    user.PostLeaderPin = PostLeaderPin;
                    user.AudioDialInPrefix = AudioDialInPrefix;
                    //FB 2655 End
                    //FB 2693 Starts
                    user.enablePCUser = enablePCUser;
                    //ZD 100157 Starts
                    if (user.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        user.PerCalStartTime = DateTime.Now;
                    if (user.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM")
                        user.PerCalEndTime = DateTime.Now;
                    if (user.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        user.RoomCalStartTime = DateTime.Now;
                    if (user.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM")
                        user.RoomCalEndime = DateTime.Now;
                    //ZD 100157 Ends
                    if (!InsertUserPCDetails(PCNodes, userid))
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    m_IuserDao.Save(user);
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region InActiveUser
        private bool InActiveUser(ref vrmDataObject obj, ref vrmInactiveUser Inuser, int err)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false;
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, superadmin;
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0, endpointId = 0, userid = 0, timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, tickerStatus = 0, tickerStatus1 = 0, SendSurveyEmail = 0, EnableVNOCselection=0;//FB 2348 FB 2608
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0; //FB 2392-Whygo
                string BrokerRoomNum = ""; //FB 3001
				//int Secured = 0; //FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "";
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", tickerBackground = "", timezonedisplay = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail = "", HelpReqPhone=""; //FB 2227//FB 2268
                string PrivateVMR = "";//FB 2481
                string VidyoURL = "", Extension = "", Pin = ""; //FB 2262 //FB 2599
                String PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 //FB 2724
                DateTime accountexpiry = new DateTime();
                int enablePCUser = 0; //FB 2693
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    if (location.Length == 0)
                        location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out prefergroup);
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                Int32.TryParse(EMailLanguage, out EmailLanguage);

                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                //if (EmailLanguage == 0)
                //    EmailLanguage = languageID;
                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        Int32.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    if (accountexpiry == DateTime.MinValue)
                        accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                }
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableParticipants);
                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableMob);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail"); //FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone"); //FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                //FB 2348
                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail"); 
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SendSurveyEmail); //FB 2633

                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection);//FB 2633

                node = xd.SelectSingleNode("//saveUser/PrivateVMR"); //FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End
                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out Region);
                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End
                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();
                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    Int32.TryParse(node.InnerText.Trim(), out Secured);

                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End
                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");

                //FB 2693 Ends
                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcLsrLimit = 0; //FB 2693
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1; //FB 2693
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcLsrLimit = orgInfo.PCUserLimit; //FB 2693
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        if (newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        Int32.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > exchangeUsrLimit)
                        {
                            myVRMException e = new myVRMException(460);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > dominoUsrLimit)
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > mobileUsrLimit)
                        {
                            myVRMException e = new myVRMException(526);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0; 
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcLsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    int maxusrid = 0; string errMsg = "", Output = "";
                    if (FetchMaxUserID(err, errMsg, obj, ref maxusrid))
                        if (err < 0)
                            obj.outXml = errMsg;
                    userid = maxusrid + 1;
                    Output = CheckUser(userid, firstname, lastname, email, altemail, login, password, 0, ref err, loginID, obj);
                    if (err < 0)
                    {
                        Output = obj.outXml;
                        return false;
                    }
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string menu = usr1rle.roleMenuMask;
                    Inuser = new vrmInactiveUser();
                    Inuser.userid = userid;
                    Inuser.FirstName = firstname;
                    Inuser.LastName = lastname;
                    //string pass = string.Empty;
                    //if (password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    pass = crypto.encrypt(password);
                    //}
                    Inuser.Password = password; //FB 3054
                    Inuser.TimeZone = timezoneid;
                    Inuser.PreferedRoom = location;
                    Inuser.PreferedGroup = prefergroup;
                    Inuser.CCGroup = preferCCgroup;
                    Inuser.Email = email;
                    Inuser.WorkPhone = workPhone;
                    Inuser.CellPhone = cellPhone;
                    Inuser.AlternativeEmail = altemail;
                    Inuser.DoubleEmail = doubleemail;
                    Inuser.DefLineRate = defaultLineRate;
                    Inuser.DefVideoProtocol = defaultVideoProtocol;
                    Inuser.ConferenceCode = conferenceCode;
                    Inuser.LeaderPin = leaderPin;
                    Inuser.Admin = admin;
                    Inuser.Deleted = deleted;
                    Inuser.lockCntTrns = locked;
                    Inuser.Login = login;
                    Inuser.roleID = roleID;
                    Inuser.EmailClient = emailClient;
                    Inuser.newUser = 1;
                    Inuser.IPISDNAddress = IPISDNAddress;
                    Inuser.BridgeID = bridgeID;
                    Inuser.DefaultEquipmentId = defaultEquipment;
                    Inuser.connectionType = connectionType;
                    Inuser.Language = languageID;
                    Inuser.EmailLangId = EmailLanguage;
                    Inuser.outsidenetwork = outsidenetwork;
                    Inuser.emailmask = emailmask;
                    Inuser.addresstype = addresstype;
                    Inuser.accountexpiry = accountexpiry;
                    Inuser.searchId = SavedSearch;
                    Inuser.endpointId = endpointId;
                    Inuser.DateFormat = dateformat;
                    Inuser.enableAV = enableAV;
                    Inuser.enableParticipants = enableParticipants;
                    Inuser.TimeFormat = timeformat;
                    Inuser.Timezonedisplay = timezonedisplay;
                    Inuser.enableExchange = enableExc;
                    Inuser.enableDomino = enableDom;
                    Inuser.enableMobile = enableMob;
                    Inuser.TickerStatus = tickerStatus;
                    Inuser.TickerPosition = tickerPosition;
                    Inuser.TickerSpeed = tickerSpeed;
                    Inuser.TickerBackground = tickerBackground;
                    Inuser.TickerDisplay = tickerDisplay;
                    Inuser.RSSFeedLink = rssFeedLink;
                    Inuser.TickerStatus1 = tickerStatus1;
                    Inuser.TickerPosition1 = tickerPosition1;
                    Inuser.TickerSpeed1 = tickerSpeed1;
                    Inuser.TickerBackground1 = tickerBackground1;
                    Inuser.TickerDisplay1 = tickerDisplay1;
                    Inuser.RSSFeedLink1 = rssFeedLink1;
                    Inuser.Audioaddon = Audioaddon;
                    Inuser.LockStatus = locked;
                    Inuser.Deleted = deleted;
                    Inuser.companyId = organizationID;
                    Inuser.MenuMask = menu;
                    //FB 2227 - start
                    Inuser.InternalVideoNumber = InternalVideoNumber;
                    Inuser.ExternalVideoNumber = ExternalVideoNumber;
                    //FB 2227 end
                    Inuser.HelpReqEmailID = HelpReqEmail; //FB 2268
                    Inuser.HelpReqPhone = HelpReqPhone;//FB 2268
                    Inuser.RFIDValue = RFIDValue;//FB 2724
                    Inuser.SendSurveyEmail = SendSurveyEmail;//FB 2348
                    Inuser.EnableVNOCselection = EnableVNOCselection;//FB 2608
                    Inuser.PrivateVMR = PrivateVMR;//FB 2481
                    //FB 2599 Start
                    //FB 2262
                    Inuser.VidyoURL = VidyoURL;//FB 2481
                    Inuser.Extension = Extension;
                    Inuser.Pin = Pin;
                    //FB 2599 End
                    //FB 2392-Whygo Starts
                    Inuser.ParentReseller = ParentReseller;
                    Inuser.CorpUser = CorpUser;
                    Inuser.Region = Region;
                    Inuser.ESUserID = ESUserID;
                    //FB 2392-Whygo End
                    Inuser.BrokerRoomNum = BrokerRoomNum; //FB 3001
                    //Inuser.Secured = Secured; //FB 2595
                    //FB 2655 Start
                    Inuser.PreConfCode = PreConfCode;
                    Inuser.PreLeaderPin = PreLeaderPin;
                    Inuser.PostLeaderPin = PostLeaderPin;
                    Inuser.AudioDialInPrefix = AudioDialInPrefix;
                    //FB 2655 End
                    //FB 2693 Starts
                    Inuser.enablePCUser = enablePCUser;
                    //ZD 100157 Starts
                    if (Inuser.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.PerCalStartTime = DateTime.Now;
                    if (Inuser.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.PerCalEndTime = DateTime.Now;
                    if (Inuser.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.RoomCalStartTime = DateTime.Now;
                    if (Inuser.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.RoomCalEndime = DateTime.Now;
                    //ZD 100157 Ends
                    if (!InsertUserPCDetails(PCNodes, userid))
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    m_IinactiveUserDao.Save(Inuser);
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region LDAPActiveUser
        private bool LDAPActiveUser(ref vrmDataObject obj, int userid)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false;
                string outputXML = "";
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, sLocked = 1, superadmin;
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0,  timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, tickerStatus = 0, tickerStatus1 = 0, enablePIMnotification = 0, SendSurveyEmail = 0, EnableVNOCselection = 0;//FB 2141 //FB 2348 FB 2608
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0; //FB 2392-Whygo
                string BrokerRoomNum = "";//FB 3001
				//int Secured = 0; //FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "";
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", timezonedisplay = "", tickerBackground = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail = "", HelpReqPhone="";//FB 2227 Fix//FB 2268
                DateTime accountexpiry = new DateTime();
                string PrivateVMR = "";//FB 2481
                string VidyoURL = "", Extension = "", Pin = ""; //FB 2262 //FB 2599 
                String PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 FB 2724
                int enablePCUser = 0; //FB 2693
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    if (location.Length == 0)
                        location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out prefergroup);
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                Int32.TryParse(EMailLanguage, out EmailLanguage);
                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        Int32.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    if (accountexpiry == DateTime.MinValue)
                        accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                }
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableParticipants);
                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableMob);

                node = xd.SelectSingleNode("//saveUser/PIMNotification");//FB 2141
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enablePIMnotification);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail");//FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone"); //FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail"); //FB 2348
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SendSurveyEmail);//FB 2633

                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection);//FB 2633

                node = xd.SelectSingleNode("//saveUser/PrivateVMR");//FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End

                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out Region);

                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End
                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();
                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    Int32.TryParse(node.InnerText.Trim(), out Secured);
                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End

                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");

                //FB 2693 Ends
                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcLsrLimit = 0; //FB 2693 
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1; //FB 2693
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcLsrLimit = orgInfo.PCUserLimit; //FB 2693
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        if (newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        Int32.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > exchangeUsrLimit)
                        {
                            myVRMException e = new myVRMException(460);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > dominoUsrLimit)
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > mobileUsrLimit)
                        {
                            myVRMException e = new myVRMException(526);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcLsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string mask = usr1rle.roleMenuMask;
                    vrmUser Ldusr = m_IuserDao.GetByUserId(userid);
                    if (locked == vrmUserStatus.USER_ACTIVE)
                        sLocked = 0;
                    Config config = new Config(m_configPath);
                    bool ret = config.Initialize();
                    if (!config.ldapEnabled)
                    {
                        //string pass = string.Empty;
                        //if (password.Length > 0)
                        //{
                        //    cryptography.Crypto crypto = new cryptography.Crypto();
                        //    pass = crypto.encrypt(password);
                        //}
                        Ldusr.FirstName = firstname;
                        Ldusr.LastName = lastname;
                        Ldusr.Password = password;
                        Ldusr.Login = login;
                    }

                    //ZD 100263-Starts
                    vrmUser LoginUser = m_IuserDao.GetByUserId(loginID);
                    vrmUserRoles LoginUserRole = m_IUserRolesDao.GetById(LoginUser.roleID);//ZD 100263
                    if (!CheckUserRights(LoginUser, Ldusr))
                    {
                        if (LoginUser.userid != Ldusr.userid)
                        {
                            myvrmEx = new myVRMException(229);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else if (LoginUser.userid != Ldusr.userid)
                    {
                        Ldusr.Admin = admin;
                        Ldusr.roleID = roleID;
                        Ldusr.MenuMask = mask;
                        Ldusr.accountexpiry = accountexpiry;
                        //ZD 100263 Phase 2 Starts
                        Ldusr.enableExchange = enableExc;
                        Ldusr.enableDomino = enableDom;
                        Ldusr.enableMobile = enableMob;
                        Ldusr.PluginConfirmations = enablePIMnotification;//FB 2141
                        //ZD 100263 Phase 2 Ends
                     }
                    else if (LoginUserRole.level == 2 && LoginUserRole.crossaccess == 0)
                    {
                        Ldusr.enableExchange = enableExc;
                        Ldusr.enableDomino = enableDom;
                        Ldusr.enableMobile = enableMob;
                        Ldusr.PluginConfirmations = enablePIMnotification;//FB 2141
                        //ZD 100263 Phase 2 Ends
                    }
                    else if (LoginUserRole.crossaccess == 1) //ZD 100263
                    {
                        Ldusr.accountexpiry = accountexpiry;
                        //ZD 100263 Phase 2 Starts
                        Ldusr.enableExchange = enableExc;
                        Ldusr.enableDomino = enableDom;
                        Ldusr.enableMobile = enableMob;
                        Ldusr.PluginConfirmations = enablePIMnotification;//FB 2141
                        //ZD 100263 Phase 2 Ends
                    }
                    //ZD 100263-End

                    Ldusr.Email = email;
                    Ldusr.WorkPhone = workPhone;
                    Ldusr.CellPhone = cellPhone;
                    Ldusr.TimeZone = timezoneid;
                    Ldusr.PreferedRoom = location;
                    Ldusr.PreferedGroup = prefergroup;
                    Ldusr.CCGroup = preferCCgroup;
                    Ldusr.AlternativeEmail = altemail;
                    Ldusr.DoubleEmail = doubleemail;
                    Ldusr.EmailClient = emailClient;
                    Ldusr.DefLineRate = defaultLineRate;
                    Ldusr.DefVideoProtocol = defaultVideoProtocol;
                    Ldusr.ConferenceCode = conferenceCode;
                    Ldusr.LeaderPin = leaderPin;
                    //Ldusr.Admin = admin; //ZD 100263
                    Ldusr.Deleted = deleted;
                    Ldusr.lockCntTrns = locked;
                    Ldusr.IPISDNAddress = IPISDNAddress;
                    Ldusr.connectionType = connectionType;
                    //Ldusr.roleID = roleID;//ZD 100263
                    Ldusr.Language = languageID;
                    Ldusr.EmailLangId = EmailLanguage;
                    Ldusr.DefaultEquipmentId = defaultEquipment;
                    Ldusr.outsidenetwork = outsidenetwork;
                    Ldusr.emailmask = emailmask;
                    Ldusr.addresstype = addresstype;
                    //Ldusr.accountexpiry = accountexpiry; //ZD 100263
                    Ldusr.LockStatus = sLocked;
                    Ldusr.BridgeID = bridgeID;
                    Ldusr.searchId = SavedSearch;
                    Ldusr.DateFormat = dateformat;
                    Ldusr.enableAV = enableAV;
                    Ldusr.enableParticipants = enableParticipants;
                    Ldusr.TimeFormat = timeformat;
                    Ldusr.Timezonedisplay = timezonedisplay;
                   
                    Ldusr.TickerStatus = tickerStatus;
                    Ldusr.TickerPosition = tickerPosition;
                    Ldusr.TickerSpeed = tickerSpeed;
                    Ldusr.TickerBackground = tickerBackground;
                    Ldusr.TickerDisplay = tickerDisplay;
                    Ldusr.RSSFeedLink = rssFeedLink;
                    Ldusr.TickerStatus1 = tickerStatus1;
                    Ldusr.TickerPosition1 = tickerPosition1;
                    Ldusr.TickerSpeed1 = tickerSpeed1;
                    Ldusr.TickerBackground1 = tickerBackground1;
                    Ldusr.TickerDisplay1 = tickerDisplay1;
                    Ldusr.RSSFeedLink1 = rssFeedLink1;
                    Ldusr.Audioaddon = Audioaddon;
                    //Ldusr.MenuMask = mask;//ZD 100263
                    
                    //FB 2227 Start
                    Ldusr.InternalVideoNumber = InternalVideoNumber;
                    Ldusr.ExternalVideoNumber = ExternalVideoNumber;
                    //FB 2227 End
                    Ldusr.HelpReqEmailID = HelpReqEmail; //FB 2268
                    Ldusr.HelpReqPhone = HelpReqPhone;//FB 2268
                    Ldusr.RFIDValue = RFIDValue;//FB 2724
                    Ldusr.SendSurveyEmail = SendSurveyEmail;//FB 2348
                    Ldusr.EnableVNOCselection = EnableVNOCselection;//FB 2608
                    Ldusr.PrivateVMR = PrivateVMR;//FB 2481
                    //FB 2599 Start
                    //FB 2262
                    Ldusr.VidyoURL = VidyoURL;
                    Ldusr.Extension = Extension;
                    Ldusr.Pin = Pin;
                    //FB 2599 End
                    //FB 2392-Whygo Starts
                    Ldusr.ParentReseller = ParentReseller;
                    Ldusr.CorpUser = CorpUser;
                    Ldusr.Region = Region;
                    Ldusr.ESUserID = ESUserID;
                    //FB 2392-Whygo End
                    Ldusr.BrokerRoomNum = BrokerRoomNum;//FB 3001
                    //Ldusr.Secured = Secured;//FB 2595
                    //FB 2655 Start
                    Ldusr.PreConfCode = PreConfCode;
                    Ldusr.PreLeaderPin = PreLeaderPin;
                    Ldusr.PostLeaderPin = PostLeaderPin;
                    Ldusr.AudioDialInPrefix = AudioDialInPrefix;
                    //FB 2655 End
                    
                    //FB 2693 Starts
                    Ldusr.enablePCUser = enablePCUser;
                    //ZD 100157 Starts
                    if (Ldusr.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldusr.PerCalStartTime = DateTime.Now;
                    if (Ldusr.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldusr.PerCalEndTime = DateTime.Now;
                    if (Ldusr.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldusr.RoomCalStartTime = DateTime.Now;
                    if (Ldusr.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldusr.RoomCalEndime = DateTime.Now;
                    //ZD 100157 Ends
                    if (!InsertUserPCDetails(PCNodes, userid))
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    m_IuserDao.Update(Ldusr);
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region LDAPInActiveUser
        private bool LDAPInActiveUser(ref vrmDataObject obj, int userid)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false;
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, sLocked = 1, superadmin;
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0, timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, tickerStatus = 0, tickerStatus1 = 0, SendSurveyEmail = 0, EnableVNOCselection = 0;//FB 2348
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0;//FB 2392-Whygo
                string BrokerRoomNum = "";//FB 3001
				//int Secured = 0; //FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "";
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", timezonedisplay = "", tickerBackground = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail = "", HelpReqPhone="";//FB 2227//FB 2268
                string PrivateVMR = "";//FB 2481
                string VidyoURL = "", Extension = "", Pin = ""; //FB 2262 //FB 2599
                String PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 FB 2724
                DateTime accountexpiry = new DateTime();
                int enablePCUser = 0; //FB 2693
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    if (location.Length == 0)
                        location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out prefergroup);
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                Int32.TryParse(EMailLanguage, out EmailLanguage);
                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                //if (EmailLanguage == 0)
                //    EmailLanguage = languageID;

                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        Int32.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    if (accountexpiry == DateTime.MinValue)
                        accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                }
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableParticipants);
                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out enableMob);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail");//FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone"); //FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail"); //FB 2348
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out SendSurveyEmail);
                
                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection); //FB 2633


                node = xd.SelectSingleNode("//saveUser/PrivateVMR");//FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim());//FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End
                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out Region);

                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End
                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();
                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    Int32.TryParse(node.InnerText.Trim(), out Secured);

                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End

                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");

               
                //FB 2693 Ends
                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcLsrLimit = 0; //FB 2693
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1; //FB 2693
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcLsrLimit = orgInfo.PCUserLimit; //FB 2693
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        if (newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        Int32.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > exchangeUsrLimit)
                        {
                            myVRMException e = new myVRMException(460);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > dominoUsrLimit)
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > mobileUsrLimit)
                        {
                            myVRMException e = new myVRMException(526);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcLsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string mask = usr1rle.roleMenuMask;
                    int id = 0;
                    vrmUser Lusr = m_IuserDao.GetByUserId(userid);
                    id = Lusr.id;
                    vrmInactiveUser Ldinusr = m_IinactiveUserDao.GetById(id);
                    Ldinusr.id = id;
                    Ldinusr.userid = userid;
                    Ldinusr.FirstName = firstname;
                    Ldinusr.LastName = lastname;
                    string pass = string.Empty;
                    //if (password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    pass = crypto.encrypt(password);
                    //}
                    Ldinusr.Password = password; //FB 3054
                    Ldinusr.Email = email;
                    Ldinusr.Email = email;
                    Ldinusr.WorkPhone = workPhone;
                    Ldinusr.CellPhone = cellPhone;
                    Ldinusr.TimeZone = timezoneid;
                    Ldinusr.PreferedRoom = location;
                    Ldinusr.PreferedGroup = prefergroup;
                    Ldinusr.CCGroup = preferCCgroup;
                    Ldinusr.AlternativeEmail = altemail;
                    Ldinusr.DoubleEmail = doubleemail;
                    Ldinusr.EmailClient = emailClient;
                    Ldinusr.DefLineRate = defaultLineRate;
                    Ldinusr.DefVideoProtocol = defaultVideoProtocol;
                    Ldinusr.ConferenceCode = conferenceCode;
                    Ldinusr.LeaderPin = leaderPin;
                    Ldinusr.Admin = admin;
                    Ldinusr.Deleted = deleted;
                    Ldinusr.lockCntTrns = locked;
                    Ldinusr.Login = login;
                    //Ldinusr.newUser = 0;
                    Ldinusr.IPISDNAddress = IPISDNAddress;
                    Ldinusr.connectionType = connectionType;
                    Ldinusr.roleID = roleID;
                    Ldinusr.Language = languageID;
                    Ldinusr.EmailLangId = EmailLanguage;
                    Ldinusr.DefaultEquipmentId = defaultEquipment;
                    Ldinusr.outsidenetwork = outsidenetwork;
                    Ldinusr.emailmask = emailmask;
                    Ldinusr.addresstype = addresstype;
                    Ldinusr.accountexpiry = accountexpiry;
                    Ldinusr.LockStatus = sLocked;
                    Ldinusr.BridgeID = bridgeID;
                    Ldinusr.searchId = SavedSearch;
                    Ldinusr.DateFormat = dateformat;
                    Ldinusr.enableAV = enableAV;
                    Ldinusr.enableParticipants = enableParticipants;
                    Ldinusr.TimeFormat = timeformat;
                    Ldinusr.Timezonedisplay = timezonedisplay;
                    Ldinusr.enableExchange = enableExc;
                    Ldinusr.enableDomino = enableDom;
                    Ldinusr.enableMobile = enableMob;
                    Ldinusr.TickerStatus = tickerStatus;
                    Ldinusr.TickerPosition = tickerPosition;
                    Ldinusr.TickerSpeed = Ldinusr.TickerSpeed;
                    Ldinusr.TickerBackground = tickerBackground;
                    Ldinusr.TickerDisplay = tickerDisplay;
                    Ldinusr.RSSFeedLink = rssFeedLink;
                    Ldinusr.TickerStatus1 = tickerStatus1;
                    Ldinusr.TickerPosition1 = tickerPosition1;
                    Ldinusr.TickerSpeed1 = Ldinusr.TickerSpeed1;
                    Ldinusr.TickerBackground1 = tickerBackground1;
                    Ldinusr.TickerDisplay1 = tickerDisplay1;
                    Ldinusr.RSSFeedLink1 = rssFeedLink1;
                    Ldinusr.Audioaddon = Audioaddon;
                    Ldinusr.MenuMask = mask;
                    //FB 2227 Start
                    Ldinusr.InternalVideoNumber = InternalVideoNumber;
                    Ldinusr.ExternalVideoNumber = ExternalVideoNumber;
                    //FB 2227 End
                    Ldinusr.HelpReqEmailID = HelpReqEmail;//FB 2268
                    Ldinusr.HelpReqPhone = HelpReqPhone;//FB 2268
                    Ldinusr.RFIDValue = RFIDValue;//FB 2724
                    Ldinusr.SendSurveyEmail = SendSurveyEmail;//FB 2348
                    Ldinusr.EnableVNOCselection = EnableVNOCselection;//FB 2608
                    Ldinusr.PrivateVMR = PrivateVMR;//FB 2481
                    //FB 2599 Start
                    //FB 2262
                    Ldinusr.VidyoURL = VidyoURL;
                    Ldinusr.Extension = Extension;
                    Ldinusr.Pin = Pin;
                    //FB 2599 End
                    //FB 2392-Whygo Starts
                    Ldinusr.ParentReseller = ParentReseller;
                    Ldinusr.CorpUser = CorpUser;
                    Ldinusr.Region = Region;
                    Ldinusr.ESUserID = ESUserID;
                    //FB 2392-Whygo End
                    Ldinusr.BrokerRoomNum = BrokerRoomNum; //FB 3001
                    //FB 2655 Start
                    Ldinusr.PreConfCode = PreConfCode;
                    Ldinusr.PreLeaderPin = PreLeaderPin;
                    Ldinusr.PostLeaderPin = PostLeaderPin;
                    Ldinusr.AudioDialInPrefix = AudioDialInPrefix;
                    //FB 2655 End
                    //FB 2693 Starts
                    Ldinusr.enablePCUser = enablePCUser;
                    //ZD 100157 Starts
                    if (Ldinusr.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.PerCalStartTime = DateTime.Now;
                    if (Ldinusr.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.PerCalEndTime = DateTime.Now;
                    if (Ldinusr.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.RoomCalStartTime = DateTime.Now;
                    if (Ldinusr.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.RoomCalEndime = DateTime.Now;
                    //ZD 100157 Ends
                    if (!InsertUserPCDetails(PCNodes, userid))
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    m_IinactiveUserDao.Save(Ldinusr);
                    m_IuserDao.Delete(Lusr);
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        //FB 2027 SetUser End

        #region GetHome
        /// <summary>
        /// GetHome
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetHome(ref vrmDataObject obj)
        {
            string sLogin = "";
            string sEmail = "";
            string sPwd = "";
            string sEnPwd = "";
            string sHomeurl = "";
            string stmt = "";
            string sAuthenticated = "NO";//SSo Mode
            bool bRet = true;
            DateTime currentLoginDate = DateTime.Now;
            DateTime currentdate = new DateTime();
            int lockcnt = 0;
            int lockcntrns = 0;
            int errno = 0;
            StringBuilder outXMLSB = null;
            Conference fetchconf = null;
            ns_SqlHelper.SqlHelper sqlCon = null;
            vrmSystemFactory validateLicense = null;
            string errMsg = "", datetime = "";//FB 2678
            string enEmailID = "", enUserPWD = "", enMacID = ""; //ZD 100263
            try
            {

                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentLoginDate);
                currentdate = new DateTime(currentLoginDate.Year, currentLoginDate.Month, currentLoginDate.Day, 0, 0, 0);
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node, node1; //ZD 100263

                node = xd.SelectSingleNode("//login/homeURL");
                if (node != null)
                {
                    if (node.InnerText != "")
                        sHomeurl = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//login/userName"); //SSO Fix
                if (node != null)
                {
                    if (node.InnerText != "")
                        sLogin = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                else
                {
                    node = xd.SelectSingleNode("//login/emailID");
                    node1 = xd.SelectSingleNode("//login/enEmailID"); //ZD 100263
                    if (node != null || node1!= null)
                    {
                        if (node.InnerText != "" || (node1.InnerText != ""))//ZD 100263
                            sEmail = node.InnerXml.Trim();
                        else
                        {
                            myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;

                        }
                    }

                }

                node = xd.SelectSingleNode("//login/userAuthenticated");//SSo Mode
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();
                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {

                    node = xd.SelectSingleNode("//login/userPassword");
                    node1 = xd.SelectSingleNode("//login/enUserPWD"); //ZD 100263
                    if (node != null || node1 != null) 
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(530); //FB 2054
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);
                }

                //ZD 100263 Starts
                node = xd.SelectSingleNode("//login/macID");
                if (node != null)
                    if (node.InnerText != "")
                        macID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/enEmailID");
                if (node != null)
                {
                    crypto = new cryptography.Crypto();
                    if (node.InnerText != "")
                    {
                        enEmailID = node.InnerXml.Trim();
                        sEmail = crypto.decrypt(enEmailID);


                        node = xd.SelectSingleNode("//login/enUserPWD");
                        if (node != null)
                            if (node.InnerText != "")
                                sEnPwd = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//login/enMacID");
                        if (node != null)
                            if (node.InnerText != "")
                            {
                                enMacID = node.InnerXml.Trim();
                                enMacID = crypto.decrypt(enMacID);
                            }
                        if (enMacID != macID)
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        myvrmEx = new myVRMException(207);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                //ZD 100263 Ends

                List<ICriterion> criterionList = new List<ICriterion>();

                if (sLogin.Trim() != "")
                    criterionList.Add(Expression.Eq("Login", sLogin));
                else if (sEmail.Trim() != "")
                    criterionList.Add(Expression.Eq("Email", sEmail));

                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList, true);

                if ((vrmUsr.Count <= 0))
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (vrmUsr[0] == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                lockcntrns = vrmUsr[0].lockCntTrns;

                if (vrmUsr[0].userid != 11)
                {
                    if (validateLicense == null)
                        validateLicense = new vrmSystemFactory(ref obj);

                    if (!validateLicense.ValidateLicense(sysSettings.license, ref errno, ref datetime)) //FB 2678
                    {
                        if (errno == 515)
                        {
                            vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                            m_IuserDao.Update(vrmUsr[0]);
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                errno = 207;
                            else
                                errno = 263;
                        }
                        myvrmEx = new myVRMException(errno);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (!(currentLoginDate <= sysSettings.ExpiryDate && vrmUsr[0].accountexpiry >= currentLoginDate))
                    {
                        vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                        m_IuserDao.Update(vrmUsr[0]);
                        if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                            myvrmEx = new myVRMException(207);
                        else
                            myvrmEx = new myVRMException(263);

                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (vrmUsr[0].LockStatus == vrmUserStatus.MAN_LOCKED)
                    {
                        if (vrmUsr[0].lockCntTrns == 10)
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(425);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        else
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(254);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else if (vrmUsr[0].LockStatus == vrmUserStatus.AUTO_LOCKED)
                    {
                        if (currentLoginDate.Subtract(vrmUsr[0].LastLogin).Minutes < 1)
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(208);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }

                if (sAuthenticated.ToUpper() == "NO")
                {
                    if (vrmUsr[0].Password != sEnPwd)
                    {
                        lockcntrns++;
                        if (lockcntrns >= 3 && vrmUsr[0].userid != 11)//FB 2027T
                        {
                            lockcnt = vrmUserStatus.AUTO_LOCKED;
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(208);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        else
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        bRet = false;
                    }
                }

                if (bRet)
                {
                    if ((vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days) + 1 < 15)
                    {
                        if (vrmUsr[0].userid != 11)
                        {
                            myvrmEx = new myVRMException(264);
                            errMsg = myvrmEx.FetchErrorMsg().Replace("%s", vrmUsr[0].accountexpiry.ToString(vrmUsr[0].DateFormat)).Replace("{0}", (vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days + 1).ToString()); 
                        }
                    }

                    outXMLSB = new StringBuilder();

                    outXMLSB.Append("<user>");

                    if (fetchconf == null)
                        fetchconf = new Conference(ref obj);

                    fetchconf.FetchMostConfs(vrmUsr[0].companyId, vrmUsr[0].userid, ref outXMLSB);
                    FetchGlobalInfo(vrmUsr[0].companyId, vrmUsr[0].userid, ref outXMLSB, ref obj);
                    outXMLSB.Append(errMsg);
                    outXMLSB.Append("</user>");

                    obj.outXml = outXMLSB.ToString();

                    lockcnt = vrmUserStatus.USER_ACTIVE;
                    lockcntrns = 0;

                    OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                    if (usrOrg == null)
                    {
                        myvrmEx = new myVRMException(207); //FB 3055
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                vrmUsr[0].LastLogin = currentLoginDate;
                vrmUsr[0].LockStatus = lockcnt;
                vrmUsr[0].lockCntTrns = lockcntrns;
                vrmUsr[0].RequestID = "";//ZD 100263
                m_IuserDao.SaveOrUpdateList(vrmUsr);

                stmt = "INSERT INTO Sys_LoginAudit_D (UserID,LoginDate) Values(" + vrmUsr[0].userid.ToString();
                stmt += ",'" + currentLoginDate.ToString() + "')";

                if (sqlCon == null)
                    sqlCon = new ns_SqlHelper.SqlHelper(obj.ConfigPath);

                sqlCon.OpenConnection();

                sqlCon.ExecuteNonQuery(stmt);

                sqlCon.CloseConnection();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //FB 2558 WhyGo - Starts
        #region UserAuthentication
        /// <summary>
        /// UserAuthentication
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UserAuthentication(ref vrmDataObject obj)
        {
            String sLogin = "", sEmail = "", sPwd = "", sEnPwd = "", sHomeurl = "";
            String errMsg = "", sAuthenticated = "NO";//SSo Mode
            int lockcnt = 0, lockcntrns = 0, errno = 0;
            bool bRet = true;
            DateTime currentLoginDate = DateTime.Now;
            DateTime currentdate = new DateTime();
            StringBuilder outXMLSB = null;
            vrmSystemFactory validateLicense = null;
            string datetime = ""; //FB 2678
            try
            {

                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentLoginDate);
                currentdate = new DateTime(currentLoginDate.Year, currentLoginDate.Month, currentLoginDate.Day, 0, 0, 0);
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/homeURL");
                if (node != null)
                {
                    if (node.InnerText != "")
                        sHomeurl = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//login/userName"); //SSO Fix
                if (node != null)
                {
                    if (node.InnerText != "")
                        sLogin = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(207); //FB 3055 1st Pint
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                else
                {
                    node = xd.SelectSingleNode("//login/emailID");
                    if (node != null)
                    {
                        if (node.InnerText != "")
                            sEmail = node.InnerXml.Trim();
                        else
                        {
                            myvrmEx = new myVRMException(207); //FB 3055 1st Pint
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }

                node = xd.SelectSingleNode("//login/userAuthenticated");//SSo Mode
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();
                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {
                    node = xd.SelectSingleNode("//login/userPassword");
                    if (node.InnerText != "")
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(530);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);
                }

                List<ICriterion> criterionList = new List<ICriterion>();

                if (sLogin.Trim() != "")
                    criterionList.Add(Expression.Eq("Login", sLogin));
                else if (sEmail.Trim() != "")
                    criterionList.Add(Expression.Eq("Email", sEmail));

                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList, true);

                if (vrmUsr.Count <= 0)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (vrmUsr[0] == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                lockcntrns = vrmUsr[0].lockCntTrns;

                if (vrmUsr[0].userid != 11)
                {
                    if (validateLicense == null)
                        validateLicense = new vrmSystemFactory(ref obj);

                    if (!validateLicense.ValidateLicense(sysSettings.license, ref errno, ref datetime)) //FB 2678
                    {
                        if (errno == 515)
                        {
                            vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                            m_IuserDao.Update(vrmUsr[0]);
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                errno = 207;
                            else
                                errno = 263;
                        }
                        myvrmEx = new myVRMException(errno);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (!(currentLoginDate <= sysSettings.ExpiryDate && vrmUsr[0].accountexpiry >= currentLoginDate))
                    {
                        vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                        m_IuserDao.Update(vrmUsr[0]);
                        if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                            myvrmEx = new myVRMException(207);
                        else
                            myvrmEx = new myVRMException(263);

                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (vrmUsr[0].LockStatus == vrmUserStatus.MAN_LOCKED)
                    {
                        if (vrmUsr[0].lockCntTrns == 10)
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(425);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        else
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(254);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else if (vrmUsr[0].LockStatus == vrmUserStatus.AUTO_LOCKED)
                    {
                        if (currentLoginDate.Subtract(vrmUsr[0].LastLogin).Minutes < 1)
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(208);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }

                if (sAuthenticated.ToUpper() == "NO")
                {
                    if (vrmUsr[0].Password != sEnPwd)
                    {
                        lockcntrns++;
                        if (lockcntrns >= 3 && vrmUsr[0].userid != 11)
                        {
                            lockcnt = vrmUserStatus.AUTO_LOCKED;
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(208);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        else
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        bRet = false;
                    }
                }

                if (bRet)
                {
                    if ((vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days) + 1 < 15)
                    {
                        if (vrmUsr[0].userid != 11)
                        {
                            myvrmEx = new myVRMException(264);
                            errMsg = myvrmEx.FetchErrorMsg().Replace("%s", vrmUsr[0].accountexpiry.ToString(vrmUsr[0].DateFormat)).Replace("{0}", (vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days + 1).ToString());
                        }
                    }

                    //FB 2659 - Starts
                    string[] mary = vrmUsr[0].MenuMask.ToString().Split('-');
                    string[] mmary = mary[1].Split('+');
                    string[] ccary = mary[0].Split('*');
                    int usrmenu = Convert.ToInt32(ccary[1]);
                    bool isExpuser = Convert.ToBoolean((((usrmenu & 1) == 1) && ((usrmenu & 2) == 2)) || ((((usrmenu & 2) == 2) && ((usrmenu & 64) == 64))));

                    outXMLSB = new StringBuilder();

                    outXMLSB.Append("<user>");

                    outXMLSB.Append("<globalInfo>");
                    outXMLSB.Append("<userID>" + vrmUsr[0].userid.ToString() + "</userID>");
                    outXMLSB.Append("<enableAV>" + vrmUsr[0].enableAV + "</enableAV>");
                    if (isExpuser) //FB 2659
                        outXMLSB.Append("<poweruser>0</poweruser>");
                    else
                        outXMLSB.Append("<poweruser>1</poweruser>");

                    outXMLSB.Append("<organizationID>" + vrmUsr[0].companyId + "</organizationID>");
                    outXMLSB.Append("<EnableNetworkFeatures>" + sysSettings.EnableNetworkFeatures + "</EnableNetworkFeatures>");
                    outXMLSB.Append("<roomCascadingControl>" + sysSettings.roomCascadingControl + "</roomCascadingControl>"); //ZD 100263
                    outXMLSB.Append("</globalInfo>");

                    outXMLSB.Append("</user>");
                    obj.outXml = outXMLSB.ToString();

                    lockcnt = vrmUserStatus.USER_ACTIVE;
                    lockcntrns = 0;

                    OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                    if (usrOrg == null)
                    {
                        myvrmEx = new myVRMException(207); //FB 3055
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                vrmUsr[0].LastLogin = currentLoginDate;
                vrmUsr[0].LockStatus = lockcnt;
                vrmUsr[0].lockCntTrns = lockcntrns;
                m_IuserDao.SaveOrUpdateList(vrmUsr);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion
        //FB 2558 WhyGo - End
        //FB 2027 Ends

        //FB 1830 - Tranlsation
        #region Get Language Texts
        public bool GetLanguageTexts(ref vrmDataObject obj)
        {
            bool bRet = true;
            Int32 usrID = 0;
            string userID = "";
            StringBuilder m_Translatetext = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    userID = node.InnerXml.Trim();

                Int32.TryParse(userID, out usrID);

                if (usrID <= 0)
                {
                    throw new Exception("Invalid User");
                }

                vrmUser Usr = m_IuserDao.GetByUserId(usrID);
                List<vrmLanguageText> languagetexts = m_ILanguagetext.GetByLanguageId(Usr.Language);

                if (languagetexts != null)
                {
                    if (languagetexts.Count > 0)
                    {
                        m_Translatetext.Append("<Translations>");
                        for (int languagecnt = 0; languagecnt < languagetexts.Count; languagecnt++)
                        {
                            m_Translatetext.Append("<Translation>");
                            m_Translatetext.Append("<uid>" + languagetexts[languagecnt].uId.ToString() + "</uid>");
                            m_Translatetext.Append("<LanguageID>" + languagetexts[languagecnt].LanguageID.ToString() + "</LanguageID>");
                            m_Translatetext.Append("<Text>" + languagetexts[languagecnt].Text.Replace("\r", "").Replace("\n", "").ToString() + "</Text>");//FB 2272
                            m_Translatetext.Append("<TranslatedText>" + languagetexts[languagecnt].TranslatedText.Replace("\r", "").Replace("\n", "").ToString() + "</TranslatedText>");
                            m_Translatetext.Append("</Translation>");
                        }
                        m_Translatetext.Append("</Translations>");

                    }
                }
                obj.outXml = m_Translatetext.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = myVRMException.toXml(e.Message);
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //NewLobby Start
        #region GetUserLobbyIcons
        /// <summary>
        /// GetUserLobbyIcons
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <GetUserLobbyIcons><OrganisationID></OrganisationID><userid></userid></GetUserLobbyIcons>
        public bool GetUserLobbyIcons(ref vrmDataObject obj)
        {
            int userID = 0;
            StringBuilder outXML = new StringBuilder();
            try
            {
                vrmIconsRef iconRef = null;
                List<ICriterion> critList = new List<ICriterion>();

                critList.Add(Expression.Eq("UserID", userID));
                List<vrmUserLobby> usrLobbyIcons = m_IUserLobbyDao.GetByCriteria(critList);

                outXML.Append("<GetUserLobbyIcons>");
                if (usrLobbyIcons != null)
                {
                    outXML.Append("<LobbyIcons>");
                    for (int i = 0; i < usrLobbyIcons.Count; i++)
                    {
                        iconRef = m_IIconsRefDAO.GetById(usrLobbyIcons[i].IconID);

                        outXML.Append("<Icon>");
                        outXML.Append("<IconID>" + usrLobbyIcons[i].IconID + "</IconID>");
                        outXML.Append("<Label>" + usrLobbyIcons[i].Label + "</Label>");
                        outXML.Append("<GridPosition>" + usrLobbyIcons[i].GridPosition + "</GridPosition>");

                        if (iconRef != null)
                        {
                            outXML.Append("<IconUri>" + iconRef.IconUri + "</IconUri>");
                            outXML.Append("<IconTarget>" + iconRef.IconTarget + "</IconTarget>");
                        }
                        else
                        {
                            outXML.Append("<IconUri></IconUri>");
                            outXML.Append("<IconTarget></IconTarget>");
                        }
                        outXML.Append("</Icon>");
                    }
                    outXML.Append("</LobbyIcons>");
                }
                outXML.Append("</GetUserLobbyIcons>");

                obj.outXml = outXML.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("GetUserLobbyIcons: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region SetUserLobbyIcons
        /// <summary>
        /// SetUserLobbyIcons
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <SetUserLobbyIcons>
        //<OrganisationID></OrganisationID>
        //<userid></userid>
        //<LobbyIcons>
        //<Icon>  (Node for each icon - start)
        //<GridPosition></GridPosition>
        //<Label></Label>
        //<IconID></IconID>
        //</Icon>  (Node for each icon - start)
        //</LobbyIcons>
        //</SetUserLobbyIcons>

        public bool SetUserLobbyIcons(ref vrmDataObject obj)
        {
            try
            {
                int gridPos = 0, IconId = 0, UserID = 11;
                List<vrmUserLobby> usrLobbyIcons = new List<vrmUserLobby>();
                vrmUserLobby userlobby = null;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNodeList nodes;

                nodes = xd.SelectNodes("//SetIconReference/LobbyIcons/Icon/userID");
                Int32.TryParse(nodes[0].SelectSingleNode("//SetIconReference/LobbyIcons/Icon/userID").InnerText, out UserID);
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("UserID", UserID));
                usrLobbyIcons = m_IUserLobbyDao.GetByCriteria(criterionList, true);
                for (int i = 0; i < usrLobbyIcons.Count; i++)
                    m_IUserLobbyDao.Delete(usrLobbyIcons[i]);


                nodes = xd.SelectNodes("//SetIconReference/LobbyIcons/Icon");

                if (nodes != null)
                {
                    usrLobbyIcons = new List<vrmUserLobby>();
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        userlobby = new vrmUserLobby();

                        if (nodes[i].SelectSingleNode("userID") != null)
                            Int32.TryParse(nodes[i].SelectSingleNode("userID").InnerText, out UserID);
                        userlobby.UserID = UserID;

                        if (nodes[i].SelectSingleNode("iconUri") != null)
                        {
                            Int32.TryParse(nodes[i].SelectSingleNode("iconUri").InnerText, out gridPos);
                            userlobby.GridPosition = gridPos;
                        }

                        if (nodes[i].SelectSingleNode("defaultLabel") != null)
                            userlobby.Label = nodes[i].SelectSingleNode("defaultLabel").InnerText;

                        if (nodes[i].SelectSingleNode("iconID") != null)
                        {
                            Int32.TryParse(nodes[i].SelectSingleNode("iconID").InnerText, out IconId);
                            userlobby.IconID = IconId;
                        }

                        usrLobbyIcons.Add(userlobby);
                    }

                    if (usrLobbyIcons.Count > 0)
                        m_IUserLobbyDao.SaveOrUpdateList(usrLobbyIcons);
                }
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SetUserLobbyIcons: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //NewLobby End

        //FB 2158 start
        #region WelcomeNewUser
        /// <summary>
        /// Account email
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool WelcomeNewUser(ref vrmDataObject obj)
        {
            emailFactory m_Email = null;
            try
            {
                String fname = "", lname = "", email = "", password = "";
                StringDictionary newUsr = new StringDictionary();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    fname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                {
                    password = node.InnerXml.Trim();
                    crypto = new cryptography.Crypto();
                    if (password != "")
                        password = crypto.decrypt(password);
                }
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();

                if (fname == "" || lname == "" || email == "" || password == "")
                {
                    obj.outXml = "User Detail Missed";
                    return false;
                }
                newUsr.Add("fname", fname);
                newUsr.Add("lname", lname);
                newUsr.Add("email", email);
                newUsr.Add("password", password);
                newUsr.Add("orgid", organizationID.ToString());

                m_Email = new emailFactory(ref obj);
                m_Email.SendMailtoNewUser(ref newUsr, ref obj);
            }
            catch (Exception e)
            {
                m_log.Error("RequestVRMAccount", e);
                obj.outXml = ""; ;
                return false;
            }
            return true;
        }
        #endregion
        //FB 2158 end

        //FB 2227
        #region SetBridgenumbers
        /// <summary>
        /// Set the dial in number ie video bridge numbers from notes
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool SetBridgenumbers(ref vrmDataObject obj)
        {
            XmlDocument xd = null;
            XmlNode node = null;
            int usrID = 0;
            string externalVideoNumber = "";
            string internalVideoNumber = "";
            vrmUser usr = null;
            try
            {
                
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                node = xd.SelectSingleNode("//SetBridgenumbers/userID");

                if(node != null)
                    Int32.TryParse(node.InnerText, out usrID);

                try
                {
                    usr = m_IuserDao.GetByUserId(usrID);
                }
                catch (Exception ex)
                {
                    
                     obj.outXml = "<error><message>User does not exist</message></error>";
                     return false;
                }

                node = xd.SelectSingleNode("//SetBridgenumbers/Bridgenumber/External");
                if (node != null)
                    externalVideoNumber = node.InnerText;

                node = xd.SelectSingleNode("//SetBridgenumbers/Bridgenumber/Internal");
                if (node != null)
                    internalVideoNumber = node.InnerText;

                usr.InternalVideoNumber = internalVideoNumber;
                usr.ExternalVideoNumber = externalVideoNumber;

                m_IuserDao.SaveOrUpdate(usr);

               
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SetUserLobbyIcons: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //FB 2268 Request iPhone Starts
        #region SetPhoneNumber
        /// <summary>
        /// SetPhoneNumber
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetPhoneNumber(ref vrmDataObject obj)
        {
            try
            {
                int loginID =0;
                vrmUser vuser = new vrmUser();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SetPhoneNumber/login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetPhoneNumber/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                string phonenumber = "";
                node = xd.SelectSingleNode("//SetPhoneNumber/login/phonenumber");
                if (node != null)
                    phonenumber = node.InnerXml.Trim().ToString();
                else
                {
                    myvrmEx = new myVRMException(605);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (phonenumber.Trim() == "")
                {
                    myvrmEx = new myVRMException(605);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = new vrmUser();
                user = m_IuserDao.GetByUserId(loginID);

                user.companyId = organizationID;
                user.HelpReqPhone = phonenumber;
                m_IuserDao.Update(user);
                
                obj.outXml = "<success>1</success>";

            }
            catch (Exception e)
            {
                m_log.Error("HelpRequest", e);
                obj.outXml = ""; ;
                return false;
            }
            return true;
        }
        #endregion

        //Exchange
        #region SetDynamicHost
        /// <summary>
        /// SetDynamicHost
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDynamicHost(string emailid,ref string orgid, ref int userid)
        {
            try
            {
                string errMsg = "";
                int err = 0, endpointid = 0;
                string Lastname = "";
                string password = "pwd";
                int maxusrid = 0;
                vrmUser vuser = new vrmUser();
                if (FetchMaxUserID(err, errMsg, null, ref maxusrid))
                    if (err < 0)
                        return false;
                userid = maxusrid + 1;
                vuser.userid = userid;
                vuser.FirstName = "Host";
                Lastname = emailid.Split('@')[0];
                vuser.LastName = Lastname;
                vuser.Email = emailid;
                vuser.accountexpiry = sysSettings.ExpiryDate;
                vuser.companyId = Int32.Parse(orgid);
                string pass = string.Empty;
                if (password.Length > 0)
                {
                    crypto = new cryptography.Crypto();
                    pass = crypto.encrypt(password);
                }
                vuser.Password = pass;
                vuser.Login = "";
                vuser.TimeZone = 26;
                vuser.Language = 1;
                vuser.roleID = 1;
                vrmUserRoles userrole = m_IUserRolesDao.GetById(vuser.roleID);
                vuser.MenuMask = userrole.roleMenuMask;
                vuser.Financial = 0;
                vuser.Admin = 0;
                vuser.PreferedRoom = "0";
                vuser.DoubleEmail = 0;
                vuser.PreferedGroup = -1;
                vuser.AlternativeEmail = "";
                vuser.WorkPhone = vuser.CellPhone = vuser.ConferenceCode = vuser.LeaderPin = "";
                vuser.InternalVideoNumber = vuser.ExternalVideoNumber = vuser.HelpReqEmailID = vuser.HelpReqPhone = "";
                vuser.CCGroup = 0;
                vuser.RoomID = 0;
                vuser.PreferedOperator = 0;
                vuser.lockCntTrns = 0;
                vuser.DefLineRate = -1;
                vuser.DefVideoProtocol = 0;
                vuser.Deleted = 0;
                vuser.DefAudio = 0;
                vuser.DefVideoSession = 0;
                vuser.initialTime = 0;
                vuser.EmailClient = -1;
                vuser.newUser = 1;
                vuser.PrefTZSID = 0;
                vuser.IPISDNAddress = "";
                vuser.DefaultEquipmentId = -1;
                vuser.connectionType = -1;
                vuser.LockStatus = 0;
                vuser.LastLogin = DateTime.Now;
                vuser.outsidenetwork = 1;
                vuser.emailmask = 1;
                vuser.addresstype = -1;
                vuser.approverCount = 0;
                vuser.BridgeID = -1;
                vuser.LevelID = 0;
                vuser.preferredISDN = 0;
                vuser.portletId = 0;
                vuser.searchId = -1;
                vuser.DateFormat = "MM/dd/yyyy";
                vuser.enableAV = 1;
                vuser.Timezonedisplay = "1";
                vuser.TimeFormat = "1";
                vuser.enableParticipants = 1;
                vuser.TickerStatus1 = vuser.TickerStatus = 1;
                vuser.TickerSpeed1 = vuser.TickerSpeed = 3;
                vuser.TickerPosition1 = vuser.TickerPosition = 0;
                vuser.TickerBackground1 = vuser.TickerBackground = "#660066";
                vuser.TickerDisplay1 = vuser.TickerDisplay = 0;
                vuser.RSSFeedLink1 = vuser.RSSFeedLink = "";
                vuser.enableExchange = vuser.enableDomino = vuser.enableMobile = 0;
                vuser.Audioaddon = "0";
                vuser.DefaultTemplate = vuser.MailBlocked = 0;
                vuser.MailBlockedDate = DateTime.Now;
                vuser.EmailLangId = vuser.PluginConfirmations = vuser.SendSurveyEmail = 0;
                vuser.endpointId = 0;
                m_IuserDao.Save(vuser);
                SetDynamicHostEndpoint(ref vuser,ref orgid, ref endpointid);
                vrmAccount usracc = new vrmAccount();
                usracc.userId = vuser.userid;
                usracc.TotalTime = TOTALTIMEFORNEWUSER;
                usracc.TimeRemaining = TOTALTIMEFORNEWUSER;
                usracc.InitTime = DateTime.Now;
                usracc.ExpirationTime = DateTime.Today.AddYears(10);
                m_IuserAccountDAO.Save(usracc);

            }
            catch (Exception e)
            {
                m_log.Error("HelpRequest", e);
                return false;
            }
            return true;
        }
        #endregion

        #region SetDynamicHostEndpoint
        /// <summary>
        /// SetDynamicHost
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDynamicHostEndpoint(ref vrmUser user,ref string organizationID, ref int endpointid)
        {
            try
            {
            
                int pId = 0, eId = 0;

                vrmEndPoint ve = new vrmEndPoint();
                if (endpointid == 0)
                {
                    if (pId == 0)
                    {      
                        m_IeptDao.addProjection(Projections.Max("endpointid"));
                        IList maxId = m_IeptDao.GetObjectByCriteria(new List<ICriterion>());
                        if (maxId[0] != null)
                            eId = ((int)maxId[0]) + 1;
                        else
                            eId = 1;
                            m_IeptDao.clearProjection();
                     }
                     ve.endpointid = eId;
                     ve.profileId = pId;
                 }
                 ve.name = ve.profileName = user.FirstName+user.LastName;
                 ve.password = "";
                 ve.protocol = -1;
                 ve.connectiontype = -1;
                 ve.addresstype = -1;
                 ve.address = "";
                 ve.deleted = 0;
                 ve.outsidenetwork = 1;
                 ve.videoequipmentid = -1;
                 ve.linerateid =384;
                 ve.bridgeid = -1;
                 ve.endptURL = "";
                 ve.isDefault = 0;
                ve.encrypted = 1;
                ve.MCUAddress = "";
                ve.MCUAddressType = -1;
                ve.TelnetAPI = 0;
                ve.orgId = Int32.Parse(organizationID);
                ve.ExchangeID = "";
                ve.CalendarInvite = 0;
                ve.ApiPortNo = 23;
                ve.ConferenceCode = ve.LeaderPin = "";
                m_IeptDao.Save(ve);
                if (user != null)
                {
                    if (eId == 0)
                        user.endpointId = ve.endpointid;
                    else
                        user.endpointId = eId;

                    m_IuserDao.SaveOrUpdate(user);
                 }
            }
            catch (Exception e)
            {
                m_log.Error("HelpRequest", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2693 Starts
        private bool InsertUserPCDetails(XmlNodeList userPCDetails, int userID)
        {
            try 
            {
                List<vrmUserPC> UserExtVMRList = null;
                List<vrmUserPC> UserPCList = null;
                vrmUserPC ExtVMRUser = null;
                List<ICriterion> criterionList = null;

                int PCId=0;
                string Description = "", SkypeURL = "", VCDialinIP = "", VCMeetingID = "";
                string VCDialinSIP = "", VCDialinH323 = "", VCPin = "", PCDialinNum = "", PCDialinFreeNum = "";
                string PCMeetingID = "", PCPin = "", Intructions = "";

                if (userPCDetails != null && userPCDetails.Count > 0)
                {
                    UserExtVMRList = new List<vrmUserPC>();
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", userID));
                    UserPCList = m_IUserPCDAO.GetByCriteria(criterionList, true);

                    for (int i = 0; i < UserPCList.Count; i++)
                    {
                        m_IUserPCDAO.Delete(UserPCList[i]);
                    }

                    for (int i = 0; i < userPCDetails.Count; i++)
                    {
                        Description = ""; SkypeURL = ""; VCDialinIP = ""; VCMeetingID = "";
                        VCDialinSIP = ""; VCDialinH323 = ""; VCPin = ""; PCDialinNum = ""; PCDialinFreeNum = "";
                        PCMeetingID = ""; PCPin = ""; Intructions = ""; PCId = 0;

                        ExtVMRUser = new vrmUserPC();

                        if (userPCDetails[i].SelectSingleNode("PCId") != null)
                            int.TryParse(userPCDetails[i].SelectSingleNode("PCId").InnerText.Trim(), out PCId);

                        if (userPCDetails[i].SelectSingleNode("Description") != null)
                            Description = userPCDetails[i].SelectSingleNode("Description").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("SkypeURL") != null)
                            SkypeURL = userPCDetails[i].SelectSingleNode("SkypeURL").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCDialinIP") != null)
                            VCDialinIP = userPCDetails[i].SelectSingleNode("VCDialinIP").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCMeetingID") != null)
                            VCMeetingID = userPCDetails[i].SelectSingleNode("VCMeetingID").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCDialinSIP") != null)
                            VCDialinSIP = userPCDetails[i].SelectSingleNode("VCDialinSIP").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCDialinH323") != null)
                            VCDialinH323 = userPCDetails[i].SelectSingleNode("VCDialinH323").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCPin") != null)
                            VCPin = userPCDetails[i].SelectSingleNode("VCPin").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCDialinNum") != null)
                            PCDialinNum = userPCDetails[i].SelectSingleNode("PCDialinNum").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCDialinFreeNum") != null)
                            PCDialinFreeNum = userPCDetails[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCMeetingID") != null)
                            PCMeetingID = userPCDetails[i].SelectSingleNode("PCMeetingID").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCPin") != null)
                            PCPin = userPCDetails[i].SelectSingleNode("PCPin").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("Intructions") != null)
                            Intructions = userPCDetails[i].SelectSingleNode("Intructions").InnerText.Trim();

                        ExtVMRUser.userid = userID;
                        ExtVMRUser.PCId = PCId;
                        ExtVMRUser.Description = Description;
                        ExtVMRUser.SkypeURL = SkypeURL;
                        ExtVMRUser.VCDialinIP = VCDialinIP;
                        ExtVMRUser.VCDialinSIP = VCDialinSIP;
                        ExtVMRUser.VCDialinH323 = VCDialinH323;
                        ExtVMRUser.VCMeetingID = VCMeetingID;
                        ExtVMRUser.VCPin = VCPin;
                        ExtVMRUser.PCDialinNum = PCDialinNum;
                        ExtVMRUser.PCDialinFreeNum = PCDialinFreeNum;
                        ExtVMRUser.PCMeetingID = PCMeetingID;
                        ExtVMRUser.PCPin = PCPin;
                        ExtVMRUser.Intructions = Intructions;

                        UserExtVMRList.Add(ExtVMRUser);
                    }
                    if (UserExtVMRList.Count > 0)
                        m_IUserPCDAO.SaveOrUpdateList(UserExtVMRList);
                }
                else
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", userID));
                    UserPCList = m_IUserPCDAO.GetByCriteria(criterionList, true);

                    for (int i = 0; i < UserPCList.Count; i++)
                    {
                        m_IUserPCDAO.Delete(UserPCList[i]);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("InsertUserPCDetails", e);
                return false;
            }
        }

        private void GetUserPCDetails(int userID ,int PCType, ref StringBuilder UserPCOutxml)
        {
            try
            {
                vrmUserPC UserVMR = null;
                List<ICriterion> critList = new List<ICriterion>();
                UserPCOutxml = new StringBuilder();
                
                if (userID > 0)
                {
                    UserPCOutxml.Append("<PCDetails>");
                    critList.Add(Expression.Eq("userid", userID));
                    if(PCType > 0)
                        critList.Add(Expression.Eq("PCId", PCType));

                    List<vrmUserPC> usrExtVMRs = m_IUserPCDAO.GetByCriteria(critList);

                    for (int i = 0; i < usrExtVMRs.Count; i++)
                    {
                        UserVMR = usrExtVMRs[i];
                        UserPCOutxml.Append("<PCDetail>");
                        UserPCOutxml.Append("<PCId>" + UserVMR.PCId + "</PCId>");
                        UserPCOutxml.Append("<Description>" + UserVMR.Description + "</Description>");
                        UserPCOutxml.Append("<SkypeURL>" + UserVMR.SkypeURL + "</SkypeURL>");
                        UserPCOutxml.Append("<VCDialinIP>" + UserVMR.VCDialinIP + "</VCDialinIP>");
                        UserPCOutxml.Append("<VCMeetingID>" + UserVMR.VCMeetingID + "</VCMeetingID>");
                        UserPCOutxml.Append("<VCDialinSIP>" + UserVMR.VCDialinSIP + "</VCDialinSIP>");
                        UserPCOutxml.Append("<VCDialinH323>" + UserVMR.VCDialinH323 + "</VCDialinH323>");
                        UserPCOutxml.Append("<VCPin>" + UserVMR.VCPin + "</VCPin>");
                        UserPCOutxml.Append("<PCDialinNum>" + UserVMR.PCDialinNum + "</PCDialinNum>");
                        UserPCOutxml.Append("<PCDialinFreeNum>" + UserVMR.PCDialinFreeNum + "</PCDialinFreeNum>");
                        UserPCOutxml.Append("<PCMeetingID>" + UserVMR.PCMeetingID + "</PCMeetingID>");
                        UserPCOutxml.Append("<PCPin>" + UserVMR.PCPin + "</PCPin>");
                        UserPCOutxml.Append("<Intructions>" + UserVMR.Intructions + "</Intructions>");
                        UserPCOutxml.Append("</PCDetail>");
                    }
                    UserPCOutxml.Append("</PCDetails>");
                }
                else
                    UserPCOutxml.Append("<PCDetails></PCDetails>");
            }
            catch (Exception e)
            {
                m_log.Error("GetUserPCDetaisl", e);
            }
        }

        #region FetchPCDetails

        public bool FetchSelectedPCDetails(ref vrmDataObject obj)
        {
            try
            {
                int PCType = 0;
                int userID = 0;
                StringBuilder outXml = new StringBuilder();
                XmlDocument xd = new XmlDocument();
                myVRMException myVRMEx = null;
                xd.LoadXml(obj.inXml);
                XmlNode node;


                node = xd.SelectSingleNode("//FetchPCDetails/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);

                node = xd.SelectSingleNode("//FetchPCDetails/PCType");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out PCType);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                GetUserPCDetails(userID, PCType, ref outXml);

                obj.outXml = outXml.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        
        //FB 2693 Ends

        //ZD 100157 Starts

        #region SetCalendarTimes
        /// <summary>
        /// Sets the Personal and Room Calendar Start/End Time
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetCalendarTimes(ref vrmDataObject obj)
        {
            XmlDocument xd = null;
            XmlNode node = null;
            int usrID = 0;
            vrmUser usr = null;
            string PerStTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string PerEndTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string RoomStTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string RoomEndTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            DateTime sysSttime = DateTime.Now;
            DateTime sysEndtime = DateTime.Now;
            int PerShowHrs = 0, RoomShwHrs = 0;

            try
            {

                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                node = xd.SelectSingleNode("//SetCalendarTimes/userID");

                if (node != null)
                    Int32.TryParse(node.InnerText, out usrID);

                try
                {
                    usr = m_IuserDao.GetByUserId(usrID);
                }
                catch (Exception ex)
                {

                    obj.outXml = "<error><message>User does not exist</message></error>"; //205
                    return false;
                }

                node = xd.SelectSingleNode("//SetCalendarTimes/PerosnalCalendar/ShowPerHrs");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out PerShowHrs);
                    usr.PerCalShowHours= PerShowHrs;
                }
                node = xd.SelectSingleNode("//SetCalendarTimes/PerosnalCalendar/StartTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysSttime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysSttime);
                    usr.PerCalStartTime = sysSttime;
                }

                node = xd.SelectSingleNode("//SetCalendarTimes/PerosnalCalendar/EndTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysEndtime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysEndtime);
                    usr.PerCalEndTime = sysEndtime;
                }
                node = xd.SelectSingleNode("//SetCalendarTimes/RoomCalendar/ShowRoomHrs");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out RoomShwHrs);
                    usr.RoomCalShowHours = RoomShwHrs;
                }
                node = xd.SelectSingleNode("//SetCalendarTimes/RoomCalendar/StartTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysSttime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysSttime);
                    usr.RoomCalStartTime = sysSttime;
                }

                node = xd.SelectSingleNode("//SetCalendarTimes/RoomCalendar/EndTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysEndtime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysEndtime);
                    usr.RoomCalEndime = sysEndtime;
                }
                
                m_IuserDao.SaveOrUpdate(usr);


                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SetCalendarTimes: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetCalendarTimes
        /// <summary>
        /// Sets the Personal and Room Calendar Start/End Time
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetCalendarTimes(ref vrmDataObject obj)
        {
            int usrID = 0;
            vrmUser usr = null;

            DateTime PerStTime = DateTime.Now,PerEndTime = DateTime.Now;
            DateTime RoomStrtTime = DateTime.Now, RoomEndTime = DateTime.Now;
            try
            {
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetCalendarTimes/userID");

                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out usrID);

                        if (usrID < 0)
                        {
                            obj.outXml = "<error><message>User does not exist</message></error>"; //205
                            return false;
                        }
                    }
                }

                m_IuserDao.clearFetch();
                usr = m_IuserDao.GetByUserId(usrID);

                OUTXML = new StringBuilder();
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetCalendarTimes");
                    xWriter.WriteElementString("userID", usr.userid.ToString());
                    xWriter.WriteStartElement("PerosnalCalendar");
                    xWriter.WriteElementString("ShowPerHrs", usr.PerCalShowHours.ToString());

                    DateTime.TryParse(usr.PerCalStartTime.ToString(), out PerStTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref PerStTime);
                    xWriter.WriteElementString("StartDateTime", PerStTime.ToString());
                    xWriter.WriteElementString("StartTime", PerStTime.ToString("hh:mm tt"));

                    DateTime.TryParse(usr.PerCalEndTime.ToString(), out PerEndTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref PerEndTime);
                    xWriter.WriteElementString("EndDateTime", PerEndTime.ToString());
                    xWriter.WriteElementString("EndTime", PerEndTime.ToString("hh:mm tt"));

                    xWriter.WriteEndElement();
                    xWriter.WriteStartElement("RoomCalendar");
                    xWriter.WriteElementString("ShowRoomHrs", usr.RoomCalShowHours.ToString());

                    DateTime.TryParse(usr.RoomCalStartTime.ToString(), out RoomStrtTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref RoomStrtTime);
                    xWriter.WriteElementString("StartDateTime", RoomStrtTime.ToString());
                    xWriter.WriteElementString("StartTime", RoomStrtTime.ToString("hh:mm tt"));

                    DateTime.TryParse(usr.RoomCalEndime.ToString(), out RoomEndTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref RoomEndTime);
                    xWriter.WriteElementString("EndDateTime", RoomEndTime.ToString());
                    xWriter.WriteElementString("EndTime", RoomEndTime.ToString("hh:mm tt"));
                    xWriter.WriteEndElement();
                    xWriter.WriteEndElement();
                    xWriter.Flush();
                    obj.outXml = OUTXML.ToString();
                    return true;
                }
            }
            catch (Exception e)
            {
                m_log.Error("GetCalendarTimes: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //ZD 100157 Ends

        //ZD 100263 Starts
        #region CheckUserRights
        /// <summary>
        /// CheckUserRights
        /// </summary>
        /// <param name="LoginUser"></param>
        /// <param name="ModifiedUser"></param>
        /// <returns></returns>
        public bool CheckUserRights(vrmUser LoginUser, vrmUser ModifiedUser)
        {
            vrmUserRoles ModifiedUserRole = null;
            vrmUserRoles LoginUserRole = null;

            try
            {
                if (LoginUser.userid != ModifiedUser.userid && ModifiedUser.userid == 11) //ZD 100263//Only VRM Admin can change thier settings not other users  
                    return false;

                if (LoginUser.Admin == 2)
                {
                    ModifiedUserRole = m_IUserRolesDao.GetById(ModifiedUser.roleID);
                    LoginUserRole = m_IUserRolesDao.GetById(LoginUser.roleID);
                
                    if (ModifiedUserRole == null || LoginUserRole == null)
                        return false;

                    if ((LoginUserRole.crossaccess == 1))
                        return true;
                    if (ModifiedUserRole.crossaccess == 1 && LoginUserRole.crossaccess == 0)
                        return false;

                    if ((LoginUserRole.crossaccess == 0 && ModifiedUserRole.crossaccess == 0))
                    {
                        if ((LoginUser.companyId == ModifiedUser.companyId))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("CheckUserRights failed" + ex.Message);
                return false;
            }
        }
        #endregion

        //ZD 100263 End

    }
    #endregion


    
}