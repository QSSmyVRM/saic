﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using myVRM.DataLayer;

namespace myVRM.BusinessLayer
{
    public class vrmSpecialChar
    {
        #region Const Special characters
        internal const String LessThan = "õ"; //Alt 0245
        internal const String GreaterThan = "æ"; //Alt 145
        internal const String ampersand = "σ"; //Alt 229
        #endregion
    }

    public class UtilFactory
    {
        #region Private Members
        private ILog m_log;
        #endregion

        #region Constructor
        public UtilFactory(ref vrmDataObject obj)
        {
            m_log = obj.log;
        }
        #endregion

        #region ReplaceInXMLSpecialCharacters
        internal string ReplaceInXMLSpecialCharacters(string strXml)
        {
            try
            {
                strXml = strXml.Replace(vrmSpecialChar.LessThan, "<")
                               .Replace(vrmSpecialChar.GreaterThan, ">")
                               .Replace(vrmSpecialChar.ampersand, "&");
            }
            catch (Exception ex)
            {
                m_log.Error("Util_InXML :" + ex.StackTrace);
            }
            return strXml;
        }
        #endregion

        #region ReplaceOutXMLSpecialCharacters
        internal string ReplaceOutXMLSpecialCharacters(string strXml)
        {
            try
            {
                strXml = strXml.Replace("<", vrmSpecialChar.LessThan)
                               .Replace(">", vrmSpecialChar.GreaterThan)
                               .Replace("&", vrmSpecialChar.ampersand);
            }
            catch (Exception ex)
            {
                m_log.Error("Util_OutXML :" + ex.StackTrace);
            }
            return strXml;
        }
        #endregion

       
    }
}
