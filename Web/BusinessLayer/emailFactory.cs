//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using DDay.iCal;
using DDay.iCal.Components;
using DDay.iCal.DataTypes;
using DDay.iCal.Serialization;
using System.Net;
using System.Net.Mime;
using System.Net.Mail;
using System.Linq; //FB 2153
using Frameworx;
using Frameworx.GMap; //FB 2268
using System.Text.RegularExpressions;
using NHibernate;
using NHibernate.Criterion;

using log4net;

using myVRM.DataLayer;
using System.IO; //buffer zone

namespace myVRM.BusinessLayer
{
    //FB 1830 start
    public enum eEmailCategory { GENERAL=1, PRE_CONF_SCHEDULE, APPROVAL_DENIAL,CONFIRMATION_MAILS,POST_CONF_SCHEDULE }; //FB 1830
    public enum eEmailMode { CONF_NEW = 1, CONF_EDIT, CONF_DELETE, CONF_DENY, CONF_TERMINATE, CONF_REMINDER }; //FB 1830 //FB 2027 SetConference
    public enum eEmailType { 
        Feedback = 1,
        MailSeverConnection,
        TestMailConnection,
        RequestAccount,
        ForgotPassword,
        JoinPublicConf,
        RoomAppRequest,
        MCUAppRequest,
        SystemAppRequest,
        MCUAlert_MCUAdmin,
        Scheduler,
        Host,
        Participant,
        MCUAdmin,
        RoomAdmin,
        InventoryAdmin,
        CateringAdmin,
        HKAdmin,
        PartyRequestChange,
        PartyAcceptanceEmail,
        PartyRemainder,
        InventoryRemainder,
        CateringRemainder,
        HKRemainder,
        MCUAlert_Host,
        Ical_Emails,
        AppDenial_Host,
        AppDenial_Scheduler,
        MCU_Threshold_Email,//FB 2027 SetTerminalCtrl
        WelcomeToMyVRM, //FB 2158
        HelpRequest, //FB 2268
        SurveyEmail, //FB 2348
        UserReportMail, //FB 2363
        AVOnsitesupportMail, //FB 2415
        ErrorEventReport, //FB 2363
        VNOC_Operator, //FB 2501
		Public_Participant, //FB 2550
        Network_Switching,
        Conference_Cancellation,
        Conference_Creation_Failed,
        BatchReport, //FB 2410
        PartyMultiRequestChange //FB 2020
    };
    public enum eEmailMCUAlert { None = 0, Host, Mcu_Admin, Both }; //FB 2472
    public enum eEmailDateFormat { UserPreference =0,EuropeanDateFormat }; //FB 2555
    public enum eEmailPCType { BlueJeans = 1, Jabber, Lync, Vidtel }; //FB 2693

    #region Struct - sConferenceEmail
    /// <summary>
	/// Struct to contain the PlaceHolder details
	/// </summary>
    public struct sConferenceEmail
	{
        private string s_MailLogo;
        private string s_Recipient;
        private string s_MCUNames;
        private string s_ConferenceName;
        private string s_UniqueId;
        private string s_StartDatetime;
        private string s_Setup;
        private string s_Teardown;
        private string s_Duration;
        private string s_Host;
        private string s_Description;
        private string s_RecurrencePattern;
        private string s_LocationInfo;
        private string s_MCUInfo;
        private string s_AudioInfo;
        private string s_CustomOptions;
        private string s_ConfDatetime;
        private string s_PendingStatus;
        private string s_Workordername;
        private string s_RoomsAssigned;
        private string s_InventorySetused;
        private string s_Person_in_charge;
        private string s_Start_byDatetime;
        private string s_End_byDatetime;
        private string s_Deliverytype;
        private string s_Deliverycost;
        private string s_Servicecharge;
        private string s_Currentstatus;
        private string s_Requestitem;
        private string s_Totalcost;
        private string s_Tobecompleted;
        private string s_WorkorderComment;
        private string s_ServiceType;
        private string s_Cateringmenus;
        private string s_DeliveryDatetime;
        private string s_WebsiteURL;
        private string s_TechContact;
        private string s_TechEmail;
        private string s_TechPhone;
        private string s_Attachments;
        private string s_ConnectionDetails;
        private string s_Name;
        private string s_Email;
        private string s_CurrencyFormat; // FB 1933
        private string s_Password; // FB 2051
        private string s_InternalBridge; //FB 2376
        private string s_ExternalBridge; //FB 2376
        private string s_SecImage; // FB 2136
        private string s_SurveyURL; // FB 2348
        private string s_Requestor;
		private string s_PersonalAudioInfo; //FB 2439
        private string s_GuestLocations; //FB 2426
        private string s_VirtualMeetingRooms; //FB 2448
        private string s_CallLaunch; //FB 2522
        private string s_ConciergeSupport; //FB 2632
        private string s_ConferenceDialinNumber; //FB 2675
        private string s_PCDetails; //FB 2693
		private string s_DesktopLink;//FB 2659
		private string s_RoomVMRLink; //FB 2727
        
		#region Public Property Definitions
		/// <summary>
		/// Public Property Definitions
		/// </summary>
        public string MailLogo { get { return s_MailLogo; } set { s_MailLogo = value; } }
        public string Recipient { get { return s_Recipient; } set { s_Recipient = value; } }
        public string MCUNames { get { return s_MCUNames; } set { s_MCUNames = value; } }
        public string ConferenceName { get { return s_ConferenceName; } set { s_ConferenceName = value; } }
        public string UniqueId { get { return s_UniqueId; } set { s_UniqueId = value; } }
        public string StartDatetime { get { return s_StartDatetime; } set { s_StartDatetime = value; } }
        public string Setup { get { return s_Setup; } set { s_Setup = value; } }
        public string Teardown { get { return s_Teardown; } set { s_Teardown = value; } }
        public string Duration { get { return s_Duration; } set { s_Duration = value; } }
        public string Host { get { return s_Host; } set { s_Host = value; } }
        public string Description { get { return s_Description; } set { s_Description = value; } }
        public string RecurrencePattern { get { return s_RecurrencePattern; } set { s_RecurrencePattern = value; } }
        public string LocationInfo { get { return s_LocationInfo; } set { s_LocationInfo = value; } }
        public string MCUInfo { get { return s_MCUInfo; } set { s_MCUInfo = value; } }
        public string AudioInfo { get { return s_AudioInfo; } set { s_AudioInfo = value; } }
        public string CustomOptions { get { return s_CustomOptions; } set { s_CustomOptions = value; } }
        public string ConfDatetime { get { return s_ConfDatetime; } set { s_ConfDatetime = value; } }
        public string PendingStatus { get { return s_PendingStatus; } set { s_PendingStatus = value; } }
        public string Workordername { get { return s_Workordername; } set { s_Workordername = value; } }
        public string RoomsAssigned { get { return s_RoomsAssigned; } set { s_RoomsAssigned = value; } }
        public string InventorySetused { get { return s_InventorySetused; } set { s_InventorySetused = value; } }
        public string Person_in_charge { get { return s_Person_in_charge; } set { s_Person_in_charge = value; } }
        public string Start_byDatetime { get { return s_Start_byDatetime; } set { s_Start_byDatetime = value; } }
        public string End_byDatetime { get { return s_End_byDatetime; } set { s_End_byDatetime = value; } }
        public string Deliverytype { get { return s_Deliverytype; } set { s_Deliverytype = value; } }
        public string Deliverycost { get { return s_Deliverycost; } set { s_Deliverycost = value; } }
        public string Servicecharge { get { return s_Servicecharge; } set { s_Servicecharge = value; } }
        public string Currentstatus { get { return s_Currentstatus; } set { s_Currentstatus = value; } }
        public string Requestitem { get { return s_Requestitem; } set { s_Requestitem = value; } }
        public string Totalcost { get { return s_Totalcost; } set { s_Totalcost = value; } }
        public string Tobecompleted { get { return s_Tobecompleted; } set { s_Tobecompleted = value; } }
        public string WorkorderComment { get { return s_WorkorderComment; } set { s_WorkorderComment = value; } }
        public string ServiceType { get { return s_ServiceType; } set { s_ServiceType = value; } }
        public string Cateringmenus { get { return s_Cateringmenus; } set { s_Cateringmenus = value; } }
        public string DeliveryDatetime { get { return s_DeliveryDatetime; } set { s_DeliveryDatetime = value; } }
        public string WebsiteURL { get { return s_WebsiteURL; } set { s_WebsiteURL = value; } }
        public string TechContact { get { return s_TechContact; } set { s_TechContact = value; } }
        public string TechEmail { get { return s_TechEmail; } set { s_TechEmail = value; } }
        public string TechPhone { get { return s_TechPhone; } set { s_TechPhone = value; } }
        public string Attachments { get { return s_Attachments; } set { s_Attachments = value; } }
        public string ConnectionDetails { get { return s_ConnectionDetails; } set { s_ConnectionDetails = value; } }
        public string Name { get { return s_Name; } set { s_Name = value; } }
        public string Email { get { return s_Email; } set { s_Email = value; } }
        public string CurrencyFormat { get { return s_CurrencyFormat; } set { s_CurrencyFormat = value; } } // FB 1933
        public string Password { get { return s_Password; } set { s_Password = value; } } // FB 2051
        public string InternalBridge { get { return s_InternalBridge; } set { s_InternalBridge = value; } } // FB 2376
        public string ExternalBridge { get { return s_ExternalBridge; } set { s_ExternalBridge = value; } } // FB 2376
        public string SecImage { get { return s_SecImage; } set { s_SecImage = value; } }
        public string SurveyURL { get { return s_SurveyURL; } set { s_SurveyURL = value; } }//FB 2348
        public string Requestor { get { return s_Requestor; } set { s_Requestor = value; } }
		public string PersonalAudioInfo { get { return s_PersonalAudioInfo; } set { s_PersonalAudioInfo = value; } } //FB 2439
        public string GuestLocations { get { return s_GuestLocations; } set { s_GuestLocations = value; } } //FB 2426
        public string VirtualMeetingRooms { get { return s_VirtualMeetingRooms; } set { s_VirtualMeetingRooms = value; } } //FB 2448
        public string CallLaunch { get { return s_CallLaunch; } set { s_CallLaunch = value; } } //FB 2522
        public string ConciergeSupport { get { return s_ConciergeSupport; } set { s_ConciergeSupport = value; } }//FB 2632
        public string ConferenceDialinNumber { get { return s_ConferenceDialinNumber; } set { s_ConferenceDialinNumber = value; } }//FB 2675
        public string PCDetails { get { return s_PCDetails; } set { s_PCDetails = value; } }//FB 2693
		public string DesktopLink { get { return s_DesktopLink; } set { s_DesktopLink = value; } }//FB 2659
		public string RoomVMRLink { get { return s_RoomVMRLink; } set { s_RoomVMRLink = value; } }//FB 2727
		#endregion
	}
	#endregion

    //FB 1830 end

    /// <summary>
    /// Business Layer Logic for emails 
    /// </summary>
    /// 
    public class emailFactory
    {
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        /// 
        #region Private Data Members
        private static log4net.ILog m_log;
        private string m_configPath;
        
        private conferenceDAO m_confDAO;
        private IConferenceDAO m_vrmConfDAO;
        private IConfRoomDAO m_IconfRoom;

        private IConfRoomDAO m_vrmConfRoomDAO;
        private IConfRecurDAO m_vrmConfRecurDAO;
        private IConfUserDAO m_vrmConfUserDAO;

        private LocationDAO m_locationDAO;
        private IRoomDAO m_roomDAO;

        private userDAO m_userDAO;
        private IUserDao m_IuserDao;
        private IGuestUserDao m_IguestUserDao;

        private IEmailDao m_IemailDao;
        private SystemDAO m_systemDAO;  //buffer zone
        private ISystemDAO m_IsystemDAO;  //buffer zone

        private deptDAO m_deptDAO;  //Custom Attribute Fixes
        private IDeptCustomAttrDao m_IDeptCustDAO;  //Custom Attribute Fixes

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private ISysTechDAO m_ISysTechDAO;
        private const int defaultOrgId = 11;  //Default organization
        private sysTechData sysTech;
        internal OrgData orgInfo; 
        private IEptDao m_vrmEpt; //ICAL Cisco FB 1602
        private hardwareDAO m_Hardware;//ICAL Cisco FB 1602
        internal Boolean conf_isEdit; //FB 1749
        private IEmailContentDao m_IEmailContentDao; //FB 1830 start
        private GeneralDAO m_generalDAO;
        private ILanguageDAO m_ILanguageDAO;
        private sysMailData sysMail;
        private sysSettings sys;
        private string emailContent = ""; 
        private string emailSubject = "";
        private List<int> placeHolders;
        private string websiteURL = "";
        private string Language = "en";
        private string LanguageUpload = "upload";
        private int emailLanguage = 1; //default
        private sConferenceEmail emailObject;
        private vrmConference conf;
        private vrmUser confHost;
        string userDateFormat = "MM/dd/yyyy"; //FB 2555
        string userTimeFormat = "";
        string tzDisplay = "";
        int usrTimezone = 26;
        string ical = "";
        private WorkOrderDAO m_woDAO;
        private InvCategoryDAO m_InvCategoryDAO;
        private IAVItemDAO m_IAVItemDAO;
        private ICAItemDAO m_ICAItemDAO;
        private IHKItemDAO m_IHKItemDAO;
        private InvListDAO m_InvListDAO;
        private InvRoomDAO m_InvRoomDAO;
        private InvWorkOrderDAO m_InvWorkOrderDAO;
        private InvWorkItemDAO m_InvWorkItemDAO;
        private InvWorkChargeDAO m_InvWorkChargeDAO;
        internal List<vrmConfRoom> confRoomList;
        internal string instanceType = "A"; //Default A for approving both normal and whole recurring conferene
        internal List<vrmConfUser> confUserLst; //FB 1830 end
        private IMCUDao m_ImcuDao; // FB 1920
        internal int organizationID;
        private IEmailDomainDAO m_IEmailDomainDAO; //FB 2154
        private IEmailLanguageDao m_IEmailLanguageDAO;//FB 1830
        private ICountryDAO m_ICountryDAO; //FB 2189
        private IStateDAO m_IStateDAO; //FB 2189
        string Password = "";//FB 2051   
        string VMRRDetails = "";//FB 2453  
        private UtilFactory m_UtilFactory; //FB 2236
        private IConfAdvAvParamsDAO m_confAdvAvParamsDAO;//FB 2376
        private IConfAttrDAO m_IconfAttrDAO; //FB 2415
        private bool IsParty = false; //FB 2419
        private vrmUser confRequstor;
        //string hostAudioBridgeDt = ""; //FB 2439
        //string mcuAudioBridgeDt = ""; //FB 2439
        string partyAudioBridgeDt = ""; //FB 2439
        string callLaunch = "No"; //FB 2522 Manual Launch
        private IConfBridgeDAO m_IconfBridge; //FB 2636 
        List<vrmConfAdvAvParams> confAVParam = null;//FB 2636
        private IConfVNOCOperatorDAO m_ConfVNOCOperatorDAO; //FB 2670
        List<vrmConfVNOCOperator> ConfVNOCList = null; //FB 2670
        private string RoomInternalNum = "", RoomExternalNum = ""; //FB 2690
        private IUserPCDAO m_IUserPCDAO; //FB 2693 
        internal List<vrmUserPC> confPCLst; //FB 2693
		internal string sDektopURL = "";//FB 2659
        internal string sE164DialNumber = "";//FB 2659
        #endregion 

        #region  emailFactory
        public emailFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                
                sys = new sysSettings(obj.ConfigPath);  //FB 1830
                sysMail = new sysMailData();    //FB 1830

                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log);
                m_vrmConfDAO = m_confDAO.GetConferenceDao();
                m_vrmConfRoomDAO = m_confDAO.GetConfRoomDao();
                m_vrmConfRecurDAO = m_confDAO.GetConfRecurDao();
                m_vrmConfUserDAO = m_confDAO.GetConfUserDao();

                m_locationDAO = new LocationDAO(obj.ConfigPath, obj.log);
                m_roomDAO = m_locationDAO.GetRoomDAO();

                m_userDAO = new userDAO(obj.ConfigPath, obj.log);
                m_IuserDao = m_userDAO.GetUserDao();
                m_IguestUserDao = m_userDAO.GetGuestUserDao();
                m_IemailDao = m_userDAO.GetUserEmailDao();

                m_IconfRoom = m_confDAO.GetConfRoomDao();
                m_systemDAO = new SystemDAO(m_configPath, m_log);
                m_IsystemDAO = m_systemDAO.GetSystemDao();//buffer zone
                
                m_deptDAO = new deptDAO(m_configPath, m_log);   //Custom Attribute Fixes
                m_IDeptCustDAO = m_deptDAO.GetDeptCustomAttrDao();  //Custom Attribute Fixes

                m_OrgDAO = new orgDAO(m_configPath, m_log); //Organization Module Fixes
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();
                m_ISysTechDAO = m_OrgDAO.GetSysTechDao();
                m_Hardware = new hardwareDAO(obj.ConfigPath, obj.log); //ICAL Cisco FB 1602
                m_vrmEpt = m_Hardware.GetEptDao(); //ICAL Cisco FB 1602
                m_IEmailContentDao = m_confDAO.GetEmailContentDao(); //FB 1830 start

                m_generalDAO = new GeneralDAO(m_configPath, m_log);
                m_ILanguageDAO = m_generalDAO.GetLanguageDAO();  //FB 1830 end
                
                //FB 1920
                m_vrmEpt = m_Hardware.GetEptDao();
                m_ImcuDao = m_Hardware .GetMCUDao();
                
                //FB 2189
                m_ICountryDAO = m_generalDAO.GetCountryDAO();
                m_IStateDAO = m_generalDAO.GetStateDAO();

                m_UtilFactory = new UtilFactory(ref obj); //FB 2236
                m_confAdvAvParamsDAO = m_confDAO.GetConfAdvAvParamsDAO();//FB 2376
                m_IconfAttrDAO = m_confDAO.GetConfAttrDao(); //FB 2415
                m_IconfBridge = new confBridgeDAO(obj.ConfigPath); //FB 2636
                m_ConfVNOCOperatorDAO = m_confDAO.GetConfVNOCOperatorDao(); //FB 2670
				m_IUserPCDAO = m_userDAO.GetUserPCDAO(); //FB 2693
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region Commented Area

        //#region sendWorkOrderDeleteEmail
        //public bool sendWorkOrderDeleteEmail(int userID, List<WorkOrder> wkList)
        //{
        //    string emailMessage;

        //    try
        //    {

        //        userDAO userDAO = new userDAO(m_configPath, m_log);
        //        IUserDao uDAO = userDAO.GetUserDao();


        //        vrmUser sysAdmin = uDAO.GetByUserId(userID);
        //        // right now the confernece is updated in another transaction. Later
        //        // the conf info will have to come from another oject as it will be 
        //        // transient at this point...
        //        conferenceDAO confDAO = new conferenceDAO(m_configPath, m_log);
        //        IConferenceDAO cDAO = confDAO.GetConferenceDao();

        //        List<vrmEmail> emailList = new List<vrmEmail>();

        //        foreach (WorkOrder work in wkList)
        //        {
        //            vrmConference conf = cDAO.GetByConfId(work.ConfID, work.InstanceID);
        //            vrmUser user = uDAO.GetByUserId(work.AdminID);

        //            vrmEmail email = new vrmEmail();
        //            // FB 278, 279 email from, to was not being set (AG 04/29/08)
        //            email.emailFrom = sysAdmin.Email;
        //            email.emailTo = user.Email;
        //            email.Subject = "Conference Work Order Delete. ";
        //            DateTime dt = work.CompletedBy;
        //            if (!timeZone.userPreferedTime(conf.timezone, ref dt))
        //                return false;
        //            emailMessage = "This is a notification that a pending work order assigned to you";
        //            emailMessage += " has been deleted<br><br>";
        //            emailMessage += "<br>Conference Details:<br><br>";

        //            string emailBody = string.Empty;
        //            string dummy = string.Empty;

        //            getConferenceDetailsForEmail(user, work.ConfID, work.InstanceID, "", ref emailBody, ref dummy); //buffer zone
        //            emailBody = emailBody.Replace("{A}", ""); //FB 1642
        //            emailBody = emailBody.Replace("{C}", ""); //FB 1718
        //            emailMessage += emailBody;

        //            email.Message = AttachEmailLogo(conf.orgId) + emailMessage;
        //            email.UUID = getEmailUUID();

        //            email.orgID = conf.orgId; //Added for FB 1710

        //            m_IemailDao.Save(email);

        //        }
        //        // m_woEmail.s              
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("sytemException", e);
        //        return false;
        //    }

        //}
        //#endregion

        //#region  sendAcceptanceEmail
        //public bool sendAcceptanceEmail(int confId, int instanceId, int userId)
        //{
        //    try
        //    {
        //        //FB: 1014(external user email error) 
        //        List<ICriterion> criterionList = new List<ICriterion>();
        //        criterionList.Add(Expression.Eq("userid", userId));

        //        vrmUser user = new vrmUser();
        //        IList<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList);
        //        if (userList.Count == 0)
        //        {
        //            m_log.Debug("User not found looking up guest (id  = " + userId.ToString() + ")");

        //            vrmGuestUser guest = m_IguestUserDao.GetByUserId(userId);
        //            user.FirstName = guest.FirstName;
        //            user.LastName = guest.LastName;
        //            user.Email = guest.Email;
        //            user.TimeZone = guest.TimeZone;
        //        }
        //        else
        //        {
        //            user = userList[0];
        //        }

        //        if (instanceId == 0)
        //            instanceId = 1;

        //        string subject = "[Invitation Acceptance Confirmation]";
        //        string body, greeting;

        //        criterionList = new List<ICriterion>();
        //        criterionList.Add(Expression.Eq("confid", confId));
        //        criterionList.Add(Expression.Eq("instanceid", instanceId));

        //        List<vrmConference> conferences = m_vrmConfDAO.GetByCriteria(criterionList);

        //        if (conferences.Count < 1)
        //        {
        //            return false;
        //        }
        //        vrmConference conf = conferences[0];
        //        int confType; //FB 1642
        //        confType = conf.conftype; //FB 1642
        //        string audioInfo = ""; //FB 1642

        //        greeting = "<span style=\"font-size: 10pt; font-family: Verdana\">";
        //        greeting += "Hello " + user.FirstName + " " + user.LastName + ", <BR><BR>";
        //        greeting += "You have chosen to attend the following conference: </span><BR><BR>";

        //        body = string.Empty;
        //        /* *** Code Added for Buffer Zone *** --Start */
        //        string bufferzone = string.Empty;
        //        DateTime setupDate = conf.SetupTime;
        //        if (setupDate != DateTime.MinValue)
        //            bufferzone = "Participants";
        //        else
        //            bufferzone = "";
        //        /* *** Code Added for Buffer Zone *** --End */
        //        getConferenceDetailsForEmail(user, confId, instanceId, "", ref body, ref subject, bufferzone);   //buffer zone

        //        body += "<br>";
        //        body += " Connection details:";
        //        body += "<br>";

        //        criterionList = new List<ICriterion>();
        //        criterionList.Add(Expression.Eq("confid", confId));
        //        criterionList.Add(Expression.Eq("instanceid", instanceId));
        //        criterionList.Add(Expression.Eq("userid", userId));
        //        List<vrmConfUser> confUser = new List<vrmConfUser>();

        //        confUser = m_vrmConfUserDAO.GetByCriteria(criterionList);

        //        if (confUser.Count > 0)
        //        {
        //            vrmConfUser cu = confUser[0];

        //            string ipisdn, bridgeipisdn, rname;
        //            int invitee, vidpro, roomid, conn;
        //            int mediaType; //FB 1642

        //            invitee = cu.invitee;
        //            roomid = cu.roomId;
        //            vidpro = cu.defVideoProtocol;
        //            ipisdn = cu.ipisdnaddress;
        //            bridgeipisdn = cu.bridgeIPISDNAddress;
        //            conn = cu.connectionType;
        //            mediaType = cu.audioOrVideo; //FB 1642

        //            if (invitee == 2) // invitee. Coming into room              
        //            {
        //                if (cu.roomId > 0 && cu.roomId != vrmConfType.Phantom)
        //                {

        //                    vrmRoom Loc = m_roomDAO.GetById(cu.roomId);
        //                    rname = Loc.Name;
        //                    body += "<span style=\"font-size: 10pt; font-family: Verdana\">";
        //                    body += "You have chosen to attend the conference ";
        //                    body += "from the conference room : " + rname;
        //                    body += "</span><BR>";
        //                }

        //            }
        //            else if (invitee == 1) //invited
        //            {
        //                // FB 1642 Audio Add On starts
        //                if (confType == 2 && mediaType == 1) //Code Changed For FB 1744
        //                {
        //                    audioInfo = FetchAudioDialString(conf.confid, conf.instanceid, "P", userId);
        //                }
        //                // FB 1642 Audio Add On ends

        //                body += "<table width=\"400\" style=\"font-size: 10pt; font-family: Verdana; background-color: #000080\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">";

        //                string pro, ctype;
        //                if (vidpro == 1)
        //                    pro = "IP";
        //                else
        //                {
        //                    pro = "ISDN";
        //                }
        //                if (conn == dialingOption.dialOut)
        //                    ctype = "Dial-out";
        //                else if (conn == dialingOption.dialIn)
        //                    ctype = "Dial-in";
        //                else if (conn == dialingOption.direct)
        //                    ctype = "Direct";
        //                else
        //                    ctype = "N/A";

        //                body += "<tr>";
        //                body += "<td align=\"right\">";
        //                body += "Video protocol: </td>";
        //                body += "<td style=\"background-color: silver\">";
        //                body += " <strong>" + pro + "</strong></td>";
        //                body += "</tr>";

        //                body += "<tr>";
        //                body += "<td align=\"right\">";
        //                body += "Your IP/ISDN address: </td>";
        //                body += "<td style=\"background-color: silver\">";
        //                body += " <strong>" + ipisdn + "</strong></td>";
        //                body += "</tr>";

        //                body += "<tr>";
        //                body += "<td align=\"right\">";
        //                body += "Connection type: </td>";
        //                body += "<td style=\"background-color: silver\">";
        //                body += " <strong>" + ctype + "</strong></td>";
        //                body += "</tr>";

        //                if (conn == 1) // Dial in
        //                {
        //                    body += "<tr>";
        //                    body += "<td align=\"right\">";
        //                    body += "Bridge IP/ISDN address: </td>";
        //                    body += "<td style=\"background-color: silver\">";
        //                    body += " <strong>" + bridgeipisdn + "</strong></td>";
        //                    body += "</tr>";
        //                    List<vrmAddressType> addressType = vrmGen.getAddressType();
        //                    foreach (vrmAddressType at in addressType)
        //                    {
        //                        if (at.Id == cu.bridgeAddressType)
        //                        {
        //                            body += "<tr>";
        //                            body += "<td align=\"right\">";
        //                            body += "Address Type: </td>";
        //                            body += "<td style=\"background-color: silver\">";
        //                            body += " <strong>" + at.name + "</strong></td>";
        //                            body += "</tr>";
        //                            break;
        //                        }
        //                    }
        //                }
        //                body += "</table>";
        //            }
        //        }

        //        body = body.Replace("{A}", audioInfo); //FB 1642

        //        //Custom Attribute Fixes - starts
        //        //FB 1718 - starts corrected ui during 1718
        //        //FB 1767 start
        //        String custAttrBody = "";
        //        //custAttrBody = addConfCustomAttr(conf, "Party");
        //        custAttrBody = addConfCustomAttr("Party");
        //        body = body.Replace("{C}", custAttrBody);
        //        //FB 1767 end
        //        //FB 1718 - end

        //        //string customAttValues = "";
        //        //customAttValues = AddConfAttrToEmailString(conf);
        //        body = greeting + body;
        //        //Custom Attribute Fixes - end

        //        vrmEmail theEmail = new vrmEmail();
        //        theEmail.emailTo = user.Email;
        //        theEmail.emailFrom = user.Email;
        //        theEmail.Message = AttachEmailLogo(conf.orgId) + body;//FB 1602
        //        theEmail.Subject = subject;
        //        theEmail.orgID = conf.orgId; //Added for FB 1710

        //        theEmail.UUID = getEmailUUID();
        //        m_IemailDao.Save(theEmail);

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("sytemException", e);
        //        throw e;
        //    }

        //}
        //#endregion

        //#region  sendBridgeBookedEmail
        //public bool sendBridgeBookedEmail(int userID, int confid, int instanceid, vrmMCUResources vMCU)
        //{
        //    string emailMessage;

        //    try
        //    {
        //        // FB 1013 Send email to MCU assist
        //        userDAO userDAO = new userDAO(m_configPath, m_log);
        //        IUserDao uDAO = userDAO.GetUserDao();

        //        // first email conference host
        //        vrmUser sysAdmin = uDAO.GetByUserId(11);
        //        conferenceDAO confDAO = new conferenceDAO(m_configPath, m_log);
        //        IConferenceDAO cDAO = confDAO.GetConferenceDao();

        //        List<vrmEmail> emailList = new List<vrmEmail>();
        //        vrmConference conf = cDAO.GetByConfId(confid, instanceid);

        //        vrmUser user = uDAO.GetByUserId(userID);
        //        vrmUser admin = uDAO.GetByUserId(vMCU.adminId);

        //        vrmEmail email = new vrmEmail();

        //        email.emailFrom = sysAdmin.Email;
        //        email.emailTo = user.Email;
        //        emailMessage = "This is a notification that a conference was automatically booked on the MCU";
        //        emailMessage += "<B> " + vMCU.bridgeName + " </B> which does NOT have enough available resources! <BR><B>";
        //        emailMessage += "Please take appropriate action and notify the bridge administrator: ";
        //        emailMessage += admin.FirstName + " " + admin.LastName + " " + admin.Email;
        //        emailMessage += "</B><br>Conference Details:<br><br>";

        //        string emailBody = string.Empty;
        //        string subject = string.Empty;

        //        getConferenceDetailsForEmail(user, confid, instanceid, "", ref emailBody, ref subject); //buffer zone
        //        emailBody = emailBody.Replace("{A}", ""); //FB 1642
        //        emailBody = emailBody.Replace("{C}", ""); //FB 1718

        //        email.Subject = "[VRM MCU ALERT] " + subject;
        //        emailMessage = emailMessage + emailBody;

        //        email.Message = AttachEmailLogo(conf.orgId) +  emailMessage;//FB 1602
        //        email.UUID = getEmailUUID();

        //        email.orgID = conf.orgId; //Added for FB 1710

        //        m_IemailDao.Save(email);

        //        email = new vrmEmail();

        //        email.emailFrom = sysAdmin.Email;
        //        email.emailTo = admin.Email;
        //        //Coded Added for 1073 Start
        //        subject = String.Empty;
        //        getConferenceDetailsForEmail(admin, confid, instanceid, "", ref emailBody, ref subject);
        //        //Coded Added for 1073 End
        //        emailBody = emailBody.Replace("{A}", ""); //FB 1642
        //        emailBody = emailBody.Replace("{C}", ""); //FB 1718
        //        email.Subject = "[VRM MCU ALERT] " + subject;

        //        emailMessage = "This is a notification that a conference was automatically booked on the MCU";
        //        emailMessage += "<B> " + vMCU.bridgeName + " </B> which does NOT have enough available resources! ";
        //        emailMessage += "Please take appropriate action and notify the conference host.";
        //        emailMessage += "<br>Conference Details:<br><br>";

        //        email.Message = AttachEmailLogo(conf.orgId)+ emailMessage + emailBody;//Fb 1602
        //        email.UUID = getEmailUUID();

        //        email.orgID = conf.orgId; //Added for FB 1710

        //        m_IemailDao.Save(email);

        //        // now email bridge administrator

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("sytemException", e);
        //        return false;
        //    }

        //}
        //#endregion

       // #region  getConferenceDetailsForEmail
       // public bool getConferenceDetailsForEmail(vrmUser user, int confId, int instanceId,
       //     string additionalMessage, ref string emailbody, ref string subject)
       // {
       //     try
       //     {
       //         List<ICriterion> criterionList = new List<ICriterion>();
       //         criterionList.Add(Expression.Eq("confid", confId));
       //         if (instanceId > 0)
       //             criterionList.Add(Expression.Eq("instanceid", instanceId));

       //         List<vrmConference> conferences = m_vrmConfDAO.GetByCriteria(criterionList);

       //         List<string> Locations = new List<string>();

       //         if (conferences.Count < 1)
       //         {
       //             return false;
       //         }
       //         vrmConference conf = conferences[0];

       //         criterionList = new List<ICriterion>();
       //         criterionList.Add(Expression.Eq("confid", confId));
       //         if (instanceId > 0)
       //             criterionList.Add(Expression.Eq("instanceid", instanceId));
       //         else
       //             criterionList.Add(Expression.Eq("instanceid", 1));
       //         List<vrmConfRoom> confRoomList = new List<vrmConfRoom>();
       //         confRoomList = m_vrmConfRoomDAO.GetByCriteria(criterionList);

       //         //Changes to the Room list: 10-24-07 (AG)
       //         //		Tiers for each room will be specified. 
       //         //		Conference time in room�s time zone will be specified for each room. This will be calculated from  
       //         //		the assistant-in-charge time zone.
       //         //		For each room the assistant-in-charge name, email, phone number will be displayed.
       //         //		For each room�s endpoint, the connection information would be displayed. This will only be shown 
       //         //			(audio-only & audio/video conference.)

       //         foreach (vrmConfRoom room in confRoomList)
       //         {
       //             vrmRoom Loc = m_roomDAO.GetById(room.roomId);
       //             int assistant = Loc.assistant;

       //             // assistant info....
       //             if (assistant == 0)
       //                 assistant = 11;

       //             string assistantInfo = string.Empty;
       //             string endpointInfo = string.Empty;
       //             string roomDateInfo = "";

       //             vrmUser asst = m_IuserDao.GetByUserId(assistant);
       //             if (asst != null)
       //             {
       //                 string assFirstName = asst.FirstName;
       //                 string assLastName = asst.LastName;
       //                 string assEmail = asst.Email;
       //                 int assTimeZone = asst.TimeZone;

       //                 DateTime roomDate = conf.confdate;

       //                 // FB 140 time zones in user time (AG 04/14/08)
       //                 timeZone.GMTToUserPreferedTime(assTimeZone, ref roomDate);

       //                 assEmail.Trim();
       //                 assFirstName.Trim();
       //                 assLastName.Trim();

       //                 timeZoneData aTz = new timeZoneData();
       //                 timeZone.GetTimeZone(assTimeZone, ref aTz);

       //               //  roomDateInfo = " (" + roomDate.ToString("g") + " " + aTz.TimeZone + ")<BR>"; //Email Text Changes
       //                 assistantInfo = assFirstName + " " + assLastName + "(";

       //                 assistantInfo += "Email - " + assEmail;
       //                 assistantInfo += ")<BR>";

       //             }

       //             // if audio only or audio video
       //             if (conf.conftype == 6 || conf.conftype == 2)
       //             {

       //                 string ipisdnaddress = room.ipisdnaddress;
       //                 string bridgeIP = room.bridgeIPISDNAddress;

       //                 //criterionList = new List<ICriterion>();//FB 833
       //                 //criterionList.Add(Expression.Eq("confid", confId));//FB 833
       //                 //if (instanceId > 0)//FB 833
       //                 //    criterionList.Add(Expression.Eq("instanceid", instanceId));//FB 833
       //                 //else//FB 833
       //                 //    criterionList.Add(Expression.Eq("instanceid", 1));//FB 833

       //                 //List<vrmConfUser> confUser = new List<vrmConfUser>();//FB 833
       //                 //confUser = m_vrmConfUserDAO.GetByCriteria(criterionList, true);//FB 833

       //                 //if (confUser.Count > 0)//FB 833
       //                 //{

       //                 //vrmConfUser cu = confUser[0];//FB 833
       //                 if (room.connectionType == 1)
       //                 {
       //                     endpointInfo = "Endpoint (Endpoint IP - " + ipisdnaddress + "; Connect Type - Dial In to MCU";
       //                     endpointInfo += "; Bridge IP - " + bridgeIP + ")<BR>";
       //                 }
       //                 else
       //                 {
       //                     endpointInfo = "Endpoint (Endpoint IP - " + ipisdnaddress + "; Connect Type - Dial Out from MCU)<BR>";
       //                 }
       //                 //}
       //             }
       //             //connection info...
       //             string confLocs;
       //             confLocs = "<strong>" +
       //                         Loc.tier2.tier3.Name + " > " +
       //                         Loc.tier2.Name + " > " +
       //                         Loc.Name + "</strong>";
       //             confLocs += roomDateInfo;
       //             if (endpointInfo.Length > 0)
       //                 confLocs += endpointInfo;
       //             confLocs += assistantInfo;
       //             Locations.Add(confLocs);
       //         }

       //         timeZoneData tz = new timeZoneData();

       //         string recurring = "";

       //         vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);

       //         List<ICriterion> recurInfo = new List<ICriterion>();

       //         recurInfo.Add(Expression.Eq("confid", confId));
       //         List<vrmRecurInfo> recurPattern = m_vrmConfRecurDAO.GetByCriteria(recurInfo, true);
       //         if (recurPattern.Count > 0)
       //         {
       //             if (recurPattern[0].RecurringPattern != null)//FB 1044
       //                 recurring = recurPattern[0].RecurringPattern;
       //         }
       //         //// ONLY one conference (no recuring);

       //         DateTime conferenceDate = conf.confdate;
                            
       //         timeZone.userPreferedTime(conf.timezone, ref conferenceDate);
       //         timeZone.GetTimeZone(conf.timezone, ref tz);

       //         //Code changed by Offshore for Issue 1073 -- start  // WO Bug Fix
       //         subject += " " + conf.externalname + "(" + conferenceDate.ToString(user.DateFormat.Trim()) + " " + conferenceDate.ToString((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
       //         subject += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + ")";
       //         //Code changed by Offshore for Issue 1073 -- end

       //         emailbody = "<table width=\"800\" style=\"font-size: 10pt; font-family: Verdana; background-color: silver\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" style=\"width: 20%\">";
       //         emailbody += " Conference Name:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "  <strong>" + conf.externalname + "</strong></td>";
       //         emailbody += "</tr>";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Unique ID:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += " <strong>" + conf.confnumname.ToString() + "</strong></td>";
       //         emailbody += "</tr>";
               
       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" style=\"height: 18px\">";
       //         emailbody += "Date and Time:</td>";
       //         emailbody += "<td style=\"height: 18px; background-color: silver\">";
       //         //Code changed  by offshore for FB Issue 1073 -- start  // WO Bug Fix
       //         emailbody += "<strong>" + conferenceDate.ToString(user.DateFormat.Trim()) + " " + conferenceDate.ToString((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
       //         emailbody += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
       //         //Code changed  by offshore for FB Issue 1073 -- end
       //         emailbody += "</tr>";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Duration: </td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         //Email Text Changes Start
       //         //emailbody += "<strong>" + conf.duration.ToString() + " (Minutes) </strong></td>";
       //         int hr = conf.duration / 60;
       //         int min = conf.duration % 60;
       //         String durationString = "";
       //         if (hr > 0 && min > 0)
       //             durationString = hr.ToString() + " hrs " + min.ToString() + " mins";
       //         else if (hr > 0 && min == 0)
       //             durationString = hr.ToString() + " hrs ";
       //         else
       //             durationString = min.ToString() + " mins";
       //         emailbody += "<strong>" + durationString + "</strong></td>"; //Email Text Changes
       //         //Email Text Changes End

       //         emailbody += "</tr>";
                

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Host:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "<strong>" + confHost.FirstName.Trim() + " " + confHost.LastName.Trim() + " </strong>";//Email Text Changes
       //         //(Email - admin@myvrm.com)
       //         emailbody += "</td>";
       //         emailbody += "</tr>";


       //         if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
       //             conf.description = "N/A"; //Email Text Changes

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">Description";
       //         emailbody += "</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "<strong>" + conf.description + " </strong>";
       //         emailbody += "</td>";
       //         emailbody += "</tr>";

       //         if (recurring.Length > 0)
       //         {
       //             emailbody += "<tr>";
       //             emailbody += "<td align=\"right\">";
       //             emailbody += "Conference Recurring: </td>";
       //             emailbody += "<td style=\"background-color: silver\">";
       //             emailbody += "<strong>" + recurring + "</strong></td>";
       //             emailbody += "</tr>";
       //         }

       //         string rowspan;
       //         rowspan = Locations.Count.ToString();
       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" >";
       //         emailbody += "Location(s):";
       //         emailbody += "<br />";
       //         emailbody += "</td>";

       //         int i = 0;
       //         emailbody += "<td style=\"background-color: silver\">";
       //         foreach (string loc in Locations)
       //         {
       //             if (i > 0)
       //                 emailbody += "<br>";
       //             emailbody += "<strong>" + loc + "</strong>";
       //             if (i > 0)
       //                 emailbody += "<br />";
       //             i++;
       //         }
       //         emailbody += "</td>";

       //         emailbody += "</tr>";

       //         /**************     Code added for FB# 1017 - start   ***************/
       //         /* This is to display the MCU details in the Email Body         */
       //         if (conf.mcuList != null)
       //         {
       //             if (conf.mcuList.Count > 0)
       //             {
       //                 string mcurowspan;
       //                 mcurowspan = conf.mcuList.Count.ToString();
       //                 emailbody += "<tr>";
       //                 emailbody += "<td align=\"right\" rowspan=\"" + mcurowspan + "\">";
       //                 emailbody += "MCU Details:";
       //                 emailbody += "<br />";
       //                 emailbody += "</td>";

       //                 int lp = 0;
       //                 emailbody += "<td style=\"background-color: silver\">";
       //                 foreach (vrmMCU mcu in conf.mcuList)
       //                 {
       //                     if (lp > 0)
       //                         emailbody += "<br>";
       //                     emailbody += "<strong>" + mcu.BridgeName + "</strong>";//
       //                     if (lp > 0)
       //                         emailbody += "<br />";
       //                     lp++;
       //                 }
       //                 emailbody += "</td>";

       //                 emailbody += "</tr>";
       //             }
       //         }
       //         /**************     Code added for FB# 1017 - End   ***************/
       //         emailbody += "{A}"; //FB 1642
       //         emailbody += "{C}"; //FB 1718
                
       //         emailbody += "</table>";
       //         emailbody += "<br />";
       //         emailbody += "<br>";
       //     }
       //     catch (Exception e)
       //     {
       //         m_log.Error("Cannot save email", e);
       //         m_log.Error("sytemException", e);
       //         throw e;
       //     }
       //     return true;
       // }

       // #endregion

       // #region   getConferenceDetailsForEmail(overloaded method which accepts vrmConference object instead confid and instanceid)
       // /* ******************       FogBugz Case No:1017 - start       ********************* */
       // /* Created one overloaded method which accepts vrmConference object instead confid and instanceid  */
       // /* This is retain the mcu information which got lost in the existing method          */
       // /* Also set all the references to the existing method to point this overloaded method */

       // public bool getConferenceDetailsForEmail(vrmUser user, vrmConference conf, string additionalMessage, ref string emailbody, ref string subject,string bufferzone)
       // {
       //     int confId;
       //     int instanceId;
       //     try
       //     {
       //         List<ICriterion> criterionList = new List<ICriterion>();

       //         List<string> Locations = new List<string>();

       //         /* *** Commented the following block - since the conference information is on hand  *** */

       //         //criterionList.Add(Expression.Eq("confid", confId));
       //         //if (instanceId > 0)
       //         //    criterionList.Add(Expression.Eq("instanceid", instanceId));

       //         //List<vrmConference> conferences = m_vrmConfDAO.GetByCriteria(criterionList);

       //         //if (conferences.Count < 1)
       //         //{
       //         //    return false;
       //         //}
       //         //vrmConference conf = conferences[0];

       //         /* ************************** End ***************************************/

       //         confId = conf.confid;
       //         instanceId = conf.instanceid;

       //         criterionList = new List<ICriterion>();
       //         criterionList.Add(Expression.Eq("confid", confId));
       //         if (instanceId > 0)
       //             criterionList.Add(Expression.Eq("instanceid", instanceId));
       //         else
       //             criterionList.Add(Expression.Eq("instanceid", 1));

       //         List<vrmConfRoom> confRoomList = new List<vrmConfRoom>();
       //         confRoomList = m_vrmConfRoomDAO.GetByCriteria(criterionList);

       //         //Changes to the Room list: 10-24-07 (AG)
       //         //		Tiers for each room will be specified. 
       //         //		Conference time in room�s time zone will be specified for each room. This will be calculated from  
       //         //		the assistant-in-charge time zone.
       //         //		For each room the assistant-in-charge name, email, phone number will be displayed.
       //         //		For each room�s endpoint, the connection information would be displayed. This will only be shown 
       //         //			(audio-only & audio/video conference.)

       //         foreach (vrmConfRoom room in confRoomList)
       //         {
       //             vrmRoom Loc = m_roomDAO.GetById(room.roomId);
       //             int assistant = Loc.assistant;

       //             // assistant info....
       //             if (assistant == 0)
       //                 assistant = 11;

       //             string assistantInfo = string.Empty;
       //             string endpointInfo = string.Empty;
       //             string roomDateInfo = "";

       //             vrmUser asst = m_IuserDao.GetByUserId(assistant);
       //             if (asst != null)
       //             {
       //                 string assFirstName = asst.FirstName;
       //                 string assLastName = asst.LastName;
       //                 string assEmail = asst.Email;
       //                 int assTimeZone = asst.TimeZone;

       //                 DateTime roomDate = conf.confdate;

       //                 // FB 140 time zones in user time (AG 04/14/08)
       //                 timeZone.GMTToUserPreferedTime(assTimeZone, ref roomDate);

       //                 assEmail.Trim();
       //                 assFirstName.Trim();
       //                 assLastName.Trim();

       //                 timeZoneData aTz = new timeZoneData();
       //                 timeZone.GetTimeZone(assTimeZone, ref aTz);

       //                 //roomDateInfo = " (" + roomDate.ToString("g") + " " + aTz.TimeZone + ")<BR>"; //Email Text Changes
       //                 assistantInfo = assFirstName + " " + assLastName + "(";

       //                 assistantInfo += "Email - " + assEmail;
       //                 assistantInfo += ")<BR>";

       //             }

       //             // if audio only or audio video
       //             if (conf.conftype == 6 || conf.conftype == 2)
       //             {

       //                 string ipisdnaddress = room.ipisdnaddress;
       //                 string bridgeIP = room.bridgeIPISDNAddress;
       //                 //FB 833 Starts
       //                 //criterionList = new List<ICriterion>();
       //                 //criterionList.Add(Expression.Eq("confid", confId));
       //                 //if (instanceId > 0)
       //                 //    criterionList.Add(Expression.Eq("instanceid", instanceId));
       //                 //else
       //                 //    criterionList.Add(Expression.Eq("instanceid", 1));

       //                 //List<vrmConfUser> confUser = new List<vrmConfUser>();
       //                 //confUser = m_vrmConfUserDAO.GetByCriteria(criterionList, true);

       //                 //if (confUser.Count > 0)
       //                 //{

       //                 //    vrmConfUser cu = confUser[0];
       //                 //FB 833 Ends
       //                     if (room.connectionType == 1)
       //                     {
       //                         endpointInfo = "Endpoint (Endpoint IP - " + ipisdnaddress + "; Connect Type - Dial In to MCU";
       //                         endpointInfo += "; Bridge IP - " + bridgeIP + ")<BR>";
       //                     }
       //                     else
       //                     {
       //                         endpointInfo = "Endpoint (Endpoint IP - " + ipisdnaddress + "; Connect Type - Dial Out from MCU)<BR>";
       //                     }
       //                 //}//FB 833 
       //             }
       //             //connection info...
       //             string confLocs;
       //             confLocs = "<strong>" +
       //                         Loc.tier2.tier3.Name + " > " +
       //                         Loc.tier2.Name + " > " +
       //                         Loc.Name + "</strong>";
       //             confLocs += roomDateInfo;
       //             if (endpointInfo.Length > 0)
       //                 confLocs += endpointInfo;
       //             confLocs += assistantInfo;
       //             Locations.Add(confLocs);
       //         }

       //         timeZoneData tz = new timeZoneData();

       //         string recurring = "";

       //         vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);

       //         List<ICriterion> recurInfo = new List<ICriterion>();

       //         recurInfo.Add(Expression.Eq("confid", confId));
       //         List<vrmRecurInfo> recurPattern = m_vrmConfRecurDAO.GetByCriteria(recurInfo, true);
       //         if (recurPattern.Count > 0)
       //         {
       //             recurring = recurPattern[0].RecurringPattern;
       //         }
       //         //// ONLY one conference (no recuring);

       //         DateTime conferenceDate = conf.confdate;

       //         /* *** Code Added for Buffer Zone *** -- Start */
       //         DateTime setupDate = conf.SetupTime;      
       //         DateTime teardownDate = conf.TearDownTime;
       //         timeZone.userPreferedTime(conf.timezone, ref setupDate);
       //         timeZone.userPreferedTime(conf.timezone, ref teardownDate);
       //         Double dur = Convert.ToDouble(conf.duration);
       //         DateTime endDate = conferenceDate.AddMinutes(dur);
       //         /* *** Code Added for Buffer Zone *** -- End */

       //         timeZone.userPreferedTime(conf.timezone, ref conferenceDate);
       //         timeZone.GetTimeZone(conf.timezone, ref tz);

       //         //Code changed by offshore for Issue 1073 -- start
       //         subject += " " + conf.externalname + "(" + conferenceDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //         subject += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + ")";
       //         //Code changed by offshore for Issue 1073 -- end

       //         emailbody = "<table width=\"800\" style=\"font-size: 10pt; font-family: Verdana; background-color: silver\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" style=\"width: 20%\">";
       //         emailbody += " Conference Name:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "  <strong>" + conf.externalname + "</strong></td>";
       //         emailbody += "</tr>";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Unique ID:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += " <strong>" + conf.confnumname.ToString() + "</strong></td>";
       //         emailbody += "</tr>";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" style=\"height: 18px\">";
       //         emailbody += "Start Date and Time:</td>";
       //         emailbody += "<td style=\"height: 18px; background-color: silver\">";
       //         emailbody += "<strong>" + conferenceDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //         emailbody += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";

       //         /* *** Added for Buffer Zone *** -- Start */

       //         if (organizationID < 11)    //Organization Module
       //             organizationID = defaultOrgId;
                
       //         if (orgInfo == null) 
       //             orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                
       //         if (orgInfo != null)
       //             enableBZ = orgInfo.EnableBufferZone;
                
       //         if (enableBZ == "1")    //Organization Module
       //         {
       //             if (bufferzone.Contains("Approvers"))
       //             {
       //                 emailbody += "<tr>";
       //                 emailbody += "<td align=\"right\" style=\"height: 18px\">";
       //                 emailbody += "Attendee show-up/Setup completed :</td>";
       //                 emailbody += "<td style=\"height: 18px; background-color: silver\">";
       //                 emailbody += "<strong>" + setupDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //                 emailbody += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";

       //                 emailbody += "<tr>";
       //                 emailbody += "<td align=\"right\" style=\"height: 18px\">";
       //                 emailbody += "Attendee leave/ Teardown started :</td>";
       //                 emailbody += "<td style=\"height: 18px; background-color: silver\">";
       //                 emailbody += "<strong>" + teardownDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //                 emailbody += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
       //                 emailbody += "</tr>";

       //                 /* emailbody += "<tr>";
       //                  emailbody += "<td align=\"right\" style=\"height: 18px\">";
       //                  emailbody += "EndDate and Time :</td>";
       //                  emailbody += "<td style=\"height: 18px; background-color: silver\">";
       //                  emailbody += "<strong>" + endDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //                  emailbody += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
       //                  emailbody += "</tr>";    */
       //             }
       //         }
       //         /* *** Added for Buffer Zone *** -- End */
       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Duration: </td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         //Email Text Changes Start
       //         //emailbody += "<strong>" + conf.duration.ToString() + " (Minutes) </strong></td>";
       //         int hr = conf.duration / 60;
       //         int min = conf.duration % 60;
       //         String durationString = "";
       //         if (hr > 0 && min
       //             > 0)
       //             durationString = hr.ToString() + " hrs " + min.ToString() + " mins";
       //         else if (hr > 0 && min == 0)
       //             durationString = hr.ToString() + " hrs ";
       //         else
       //             durationString = min.ToString() + " mins";
       //         emailbody += "<strong>" + durationString + "</strong></td>"; //Email Text Changes
       //         //Email Text Changes End
       //         emailbody += "</tr>";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Host:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "<strong>" + confHost.FirstName.Trim() + " " + confHost.LastName.Trim() + " </strong>";
       //         //(Email - admin@myvrm.com)
       //         emailbody += "</td>";
       //         emailbody += "</tr>";


       //         if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
       //             conf.description = "N/A"; //Email Text Changes

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">Description";
       //         emailbody += "</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "<strong>" + conf.description + " </strong>";
       //         emailbody += "</td>";
       //         emailbody += "</tr>";

       //         if (recurring.Length > 0)
       //         {
       //             emailbody += "<tr>";
       //             emailbody += "<td align=\"right\">";
       //             emailbody += "Conference Recurring: </td>";
       //             emailbody += "<td style=\"background-color: silver\">";
       //             emailbody += "<strong>" + recurring + "</strong></td>";
       //             emailbody += "</tr>";
       //         }

       //         string rowspan;
       //         rowspan = Locations.Count.ToString();
       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" >";
       //         emailbody += "Location(s):";
       //         emailbody += "<br />";
       //         emailbody += "</td>";

       //         int i = 0;
       //         emailbody += "<td style=\"background-color: silver\">";
       //         foreach (string loc in Locations)
       //         {
       //             if (i > 0)
       //                 emailbody += "<br>";
       //             emailbody += "<strong>" + loc + "</strong>";
       //             if (i > 0)
       //                 emailbody += "<br />";
       //             i++;
       //         }
       //         emailbody += "</td>";

       //         emailbody += "<tr>";

       //         /**************     Code added for FB# 1017 - start   ***************/
       //         /* This is to display the MCU details in the Email Body         */
       //         if (conf.mcuList != null)
       //         {
       //             if (conf.mcuList.Count > 0)
       //             {
       //                 //string mcurowspan;
       //                 //mcurowspan = conf.mcuList.Count.ToString();
       //                 //emailbody += "<tr>";
       //                 //emailbody += "<td align=\"right\" rowspan=\"" + mcurowspan + "\">";
       //                 emailbody += "<td align=\"right\">";
       //                 emailbody += "MCU Details:";
       //                 emailbody += "<br />";
       //                 emailbody += "</td>";

       //                 int lp = 0;
       //                 emailbody += "<td style=\"background-color: silver\">";
       //                 foreach (vrmMCU mcu in conf.mcuList)
       //                 {
       //                     if (lp > 0)
       //                         emailbody += "<br>";
       //                     emailbody += "<strong>" + mcu.BridgeName + "</strong>";
       //                     if (lp > 0)
       //                         emailbody += "<br />";
       //                     lp++;
       //                 }
       //                 emailbody += "</td>";
       //             }
       //         }
       //         emailbody += "</tr>";
       //         /**************     Code added for FB# 1017 - End   ***************/
       //         emailbody += "{A}"; //FB 1642
       //         emailbody += "{C}"; //FB 1718
                
       //         emailbody += "</table>";
       //         emailbody += "<br />";
       //         emailbody += "<br>";
       //     }
       //     catch (Exception e)
       //     {
       //         m_log.Error("Cannot save email", e);
       //         m_log.Error("sytemException", e);
       //         throw e;
       //     }
       //     return true;
       // }
       // /* ******************       FogBugz Case No:1013,1017 - end       ********************* */

       // #endregion

       ///* *** Added for Buffer Zone *** -- Start */
       // #region  getConferenceDetailsForEmail(overloaded method which has extra parameter for buffer zone)
       
       // public bool getConferenceDetailsForEmail(vrmUser user, int confId, int instanceId,
       //     string additionalMessage, ref string emailbody, ref string subject,string bufferzone)
       // {
       //     try
       //     {
       //         List<ICriterion> criterionList = new List<ICriterion>();
       //         criterionList.Add(Expression.Eq("confid", confId));
       //         if (instanceId > 0)
       //             criterionList.Add(Expression.Eq("instanceid", instanceId));

       //         List<vrmConference> conferences = m_vrmConfDAO.GetByCriteria(criterionList);

       //         List<string> Locations = new List<string>();

       //         if (conferences.Count < 1)
       //         {
       //             return false;
       //         }
       //         vrmConference conf = conferences[0];

       //         criterionList = new List<ICriterion>();
       //         criterionList.Add(Expression.Eq("confid", confId));
       //         if (instanceId > 0)
       //             criterionList.Add(Expression.Eq("instanceid", instanceId));
       //         else
       //             criterionList.Add(Expression.Eq("instanceid", 1));
       //         List<vrmConfRoom> confRoomList = new List<vrmConfRoom>();
       //         confRoomList = m_vrmConfRoomDAO.GetByCriteria(criterionList);

       //         //Changes to the Room list: 10-24-07 (AG)
       //         //		Tiers for each room will be specified. 
       //         //		Conference time in room�s time zone will be specified for each room. This will be calculated from  
       //         //		the assistant-in-charge time zone.
       //         //		For each room the assistant-in-charge name, email, phone number will be displayed.
       //         //		For each room�s endpoint, the connection information would be displayed. This will only be shown 
       //         //			(audio-only & audio/video conference.)

       //         foreach (vrmConfRoom room in confRoomList)
       //         {
       //             vrmRoom Loc = m_roomDAO.GetById(room.roomId);
       //             int assistant = Loc.assistant;

       //             // assistant info....
       //             if (assistant == 0)
       //                 assistant = 11;

       //             string assistantInfo = string.Empty;
       //             string endpointInfo = string.Empty;
       //             string roomDateInfo = "";

       //             vrmUser asst = m_IuserDao.GetByUserId(assistant);
       //             if (asst != null)
       //             {
       //                 string assFirstName = asst.FirstName;
       //                 string assLastName = asst.LastName;
       //                 string assEmail = asst.Email;
       //                 int assTimeZone = asst.TimeZone;

       //                 DateTime roomDate = conf.confdate;

       //                 // FB 140 time zones in user time (AG 04/14/08)
       //                 timeZone.GMTToUserPreferedTime(assTimeZone, ref roomDate);

       //                 assEmail.Trim();
       //                 assFirstName.Trim();
       //                 assLastName.Trim();

       //                 timeZoneData aTz = new timeZoneData();
       //                 timeZone.GetTimeZone(assTimeZone, ref aTz);

       //                 //  roomDateInfo = " (" + roomDate.ToString("g") + " " + aTz.TimeZone + ")<BR>"; //Email Text Changes
       //                 assistantInfo = assFirstName + " " + assLastName + "(";

       //                 assistantInfo += "Email - " + assEmail;
       //                 assistantInfo += ")<BR>";

       //             }

       //             // if audio only or audio video
       //             if (conf.conftype == 6 || conf.conftype == 2)
       //             {

       //                 string ipisdnaddress = room.ipisdnaddress;
       //                 string bridgeIP = room.bridgeIPISDNAddress;
                        
       //                 if (room.connectionType == 1)
       //                 {
       //                     endpointInfo = "Endpoint (Endpoint IP - " + ipisdnaddress + "; Connect Type - Dial In to MCU";
       //                     endpointInfo += "; Bridge IP - " + bridgeIP + ")<BR>";
       //                 }
       //                 else
       //                 {
       //                     endpointInfo = "Endpoint (Endpoint IP - " + ipisdnaddress + "; Connect Type - Dial Out from MCU)<BR>";
       //                 }
       //                 //}
       //             }
       //             //connection info...
       //             string confLocs;
       //             confLocs = "<strong>" +
       //                         Loc.tier2.tier3.Name + " > " +
       //                         Loc.tier2.Name + " > " +
       //                         Loc.Name + "</strong>";
       //             confLocs += roomDateInfo;
       //             if (endpointInfo.Length > 0)
       //                 confLocs += endpointInfo;
       //             confLocs += assistantInfo;
       //             Locations.Add(confLocs);
       //         }

       //         timeZoneData tz = new timeZoneData();

       //         string recurring = "";

       //         vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);

       //         List<ICriterion> recurInfo = new List<ICriterion>();

       //         recurInfo.Add(Expression.Eq("confid", confId));
       //         List<vrmRecurInfo> recurPattern = m_vrmConfRecurDAO.GetByCriteria(recurInfo, true);
       //         if (recurPattern.Count > 0)
       //         {
       //             if (recurPattern[0].RecurringPattern != null)//FB 1044
       //                 recurring = recurPattern[0].RecurringPattern;
       //         }
       //         //// ONLY one conference (no recuring);

       //         DateTime conferenceDate = conf.confdate;

       //         /* *** Added for Buffer Zone *** -- Start */
       //         DateTime setupDate = conf.SetupTime;
       //         DateTime teardownDate = conf.TearDownTime;
       //         timeZone.userPreferedTime(conf.timezone, ref setupDate); //Onsite Issues fix
       //         timeZone.userPreferedTime(conf.timezone, ref teardownDate); //Onsite Issues fix
       //         TimeSpan bufferDuration = teardownDate - setupDate;
       //         /* *** Added for Buffer Zone *** -- End */

       //         timeZone.userPreferedTime(conf.timezone, ref conferenceDate);
       //         timeZone.GetTimeZone(conf.timezone, ref tz);

       //         //Code changed by Offshore for Issue 1073 -- start
       //         subject += " " + conf.externalname + "(" + conferenceDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //         subject += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + ")";
       //         //Code changed by Offshore for Issue 1073 -- end

       //         emailbody = "<table width=\"800\" style=\"font-size: 10pt; font-family: Verdana; background-color: silver\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" style=\"width: 20%\">";
       //         emailbody += " Conference Name:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "  <strong>" + conf.externalname + "</strong></td>";
       //         emailbody += "</tr>";

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Unique ID:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += " <strong>" + conf.confnumname.ToString() + "</strong></td>";
       //         emailbody += "</tr>";
       //         /* *** Added for Buffer Zone *** -- Start */
               
       //         if (bufferzone.Contains("Participants"))
       //         {
       //             emailbody += "<tr>";
       //             emailbody += "<td align=\"right\" style=\"height: 18px\">";
       //             emailbody += "Date and Time :</td>";
       //             emailbody += "<td style=\"height: 18px; background-color: silver\">";
       //             emailbody += "<strong>" + setupDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //             emailbody += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
       //             emailbody += "</tr>";

       //             emailbody += "<tr>";
       //             emailbody += "<td align=\"right\">";
       //             emailbody += "Duration: </td>";
       //             emailbody += "<td style=\"background-color: silver\">";
       //             String durationString = "";
       //             int hr = bufferDuration.Hours;
       //             int min = bufferDuration.Minutes;

       //             if (hr > 0 && min > 0)
       //                 durationString = hr.ToString() + " hrs " + min.ToString() + " mins";
       //             else if (hr > 0 && min == 0)
       //                 durationString = hr.ToString() + " hrs ";
       //             else
       //                 durationString = min.ToString() + " mins";

       //             emailbody += "<strong>" + durationString + "</strong></td>"; //Email Text Changes
       //             //Email Text Changes End

       //             emailbody += "</tr>";
       //         }
            
       //         else   /* *** Added for Buffer Zone *** -- End */
       //         {
       //             emailbody += "<tr>";
       //             emailbody += "<td align=\"right\" style=\"height: 18px\">";
       //             emailbody += "Date and Time:</td>";
       //             emailbody += "<td style=\"height: 18px; background-color: silver\">";
       //             //Code changed  by offshore for FB Issue 1073 -- start
       //             emailbody += "<strong>" + conferenceDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
       //             emailbody += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
       //             //Code changed  by offshore for FB Issue 1073 -- end
       //             emailbody += "</tr>";

       //             emailbody += "<tr>";
       //             emailbody += "<td align=\"right\">";
       //             emailbody += "Duration: </td>";
       //             emailbody += "<td style=\"background-color: silver\">";
       //             //Email Text Changes Start
       //             //emailbody += "<strong>" + conf.duration.ToString() + " (Minutes) </strong></td>";
       //             int hr = conf.duration / 60;
       //             int min = conf.duration % 60;
       //             String durationString = "";
       //             if (hr > 0 && min > 0)
       //                 durationString = hr.ToString() + " hrs " + min.ToString() + " mins";
       //             else if (hr > 0 && min == 0)
       //                 durationString = hr.ToString() + " hrs ";
       //             else
       //                 durationString = min.ToString() + " mins";
       //             emailbody += "<strong>" + durationString + "</strong></td>"; //Email Text Changes
       //             //Email Text Changes End

       //             emailbody += "</tr>";
       //         }

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">";
       //         emailbody += "Host:</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "<strong>" + confHost.FirstName.Trim() + " " + confHost.LastName.Trim() + " </strong>";//Email Text Changes
       //         //(Email - admin@myvrm.com)
       //         emailbody += "</td>";
       //         emailbody += "</tr>";


       //         if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
       //             conf.description = "N/A"; //Email Text Changes

       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\">Description";
       //         emailbody += "</td>";
       //         emailbody += "<td style=\"background-color: silver\">";
       //         emailbody += "<strong>" + conf.description + " </strong>";
       //         emailbody += "</td>";
       //         emailbody += "</tr>";

       //         if (recurring.Length > 0)
       //         {
       //             emailbody += "<tr>";
       //             emailbody += "<td align=\"right\">";
       //             emailbody += "Conference Recurring: </td>";
       //             emailbody += "<td style=\"background-color: silver\">";
       //             emailbody += "<strong>" + recurring + "</strong></td>";
       //             emailbody += "</tr>";
       //         }

       //         string rowspan;
       //         rowspan = Locations.Count.ToString();
       //         emailbody += "<tr>";
       //         emailbody += "<td align=\"right\" >";
       //         emailbody += "Location(s):";
       //         emailbody += "<br />";
       //         emailbody += "</td>";

       //         int i = 0;
       //         emailbody += "<td style=\"background-color: silver\">";
       //         foreach (string loc in Locations)
       //         {
       //             if (i > 0)
       //                 emailbody += "<br>";
       //             emailbody += "<strong>" + loc + "</strong>";
       //             if (i > 0)
       //                 emailbody += "<br />";
       //             i++;
       //         }
       //         emailbody += "</td>";

       //         emailbody += "</tr>";

       //         /**************     Code added for FB# 1017 - start   ***************/
       //         /* This is to display the MCU details in the Email Body         */
       //         if (conf.mcuList != null)
       //         {
       //             if (conf.mcuList.Count > 0)
       //             {
       //                 string mcurowspan;
       //                 mcurowspan = conf.mcuList.Count.ToString();
       //                 emailbody += "<tr>";
       //                 emailbody += "<td align=\"right\" rowspan=\"" + mcurowspan + "\">";
       //                 emailbody += "MCU Details:";
       //                 emailbody += "<br />";
       //                 emailbody += "</td>";

       //                 int lp = 0;
       //                 emailbody += "<td style=\"background-color: silver\">";
       //                 foreach (vrmMCU mcu in conf.mcuList)
       //                 {
       //                     if (lp > 0)
       //                         emailbody += "<br>";
       //                     emailbody += "<strong>" + mcu.BridgeName + "</strong>";//
       //                     if (lp > 0)
       //                         emailbody += "<br />";
       //                     lp++;
       //                 }
       //                 emailbody += "</td>";

       //                 emailbody += "</tr>";
       //             }
       //         }
       //         /**************     Code added for FB# 1017 - End   ***************/
       //         emailbody += "{A}"; //FB 1642
       //         emailbody += "{C}"; //FB 1718
       //         emailbody += "</table>";
       //         emailbody += "<br />";
       //         emailbody += "<br>";
       //     }
       //     catch (Exception e)
       //     {
       //         m_log.Error("Cannot save email", e);
       //         m_log.Error("sytemException", e);
       //         throw e;
       //     }
       //     return true;
       // }

       // #endregion
              
        /* *** Added for Buffer Zone *** -- End */
        //#region  emailParticipants
        //public bool emailParticipants(List<vrmConference> confList)
        //{
        //    try
        //    {
        //        string participantBody = ""; //FB 1642
        //        String CustAttrBody = "";//FB 1767
        //        vrmConference conf = confList[0];
        //        vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);
        //        string emailbody = string.Empty;
        //        string dummy = string.Empty;
        //        string bufferzone = string.Empty;

        //        DateTime setupDate = conf.SetupTime;
        //        if (setupDate != DateTime.MinValue)
        //            bufferzone = "Participants";
        //        else
        //            bufferzone = "";

        //        getConferenceDetailsForEmail(confHost, conf.confid, conf.instanceid, "", ref emailbody, ref dummy, bufferzone); //Email to owner
        //        //emailbody = emailbody.Replace("{C}", ""); //FB 1718 //FB 1767

        //        List<ICriterion> criterionList;
        //        List<vrmConfUser> uList = new List<vrmConfUser>();

        //        criterionList = new List<ICriterion>();
        //        criterionList.Add(Expression.Eq("confid", conf.confid));
        //        criterionList.Add(Expression.Eq("instanceid", conf.instanceid));

        //        uList = m_vrmConfUserDAO.GetByCriteria(criterionList);
        //        bool hostEmail = false;
        //        bool SchedEmail = false;
        //        string confcodepin = ""; //FB 1642
        //        foreach (vrmConfUser us in uList)
        //        {
        //            if (us.userid == conf.userid)
        //                SchedEmail = true;
        //            if (us.partyNotify != 0)
        //            {
        //                if (us.userid == confHost.userid)//Changed if Participant is not checked 
        //                    hostEmail = true;
        //                //FB 1642 - Audio Add-on start

        //                confcodepin = "";
        //                participantBody = "";
        //                if (conf.conftype == 2 && us.audioOrVideo == 1 && us.invitee == 1) //Code Changed For FB 1744
        //                {
        //                    confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "P", us.userid);
        //                }
        //                participantBody = emailbody.Replace("{A}", confcodepin);

        //                //FB 1642 - Audio Add-on end
        //                participantBody = participantBody.Replace("{C}", ""); //FB 1767
        //                if (!sendParticipantEmail(us.userid, us.invitee, participantBody, conf)) //FB 1365, FB 1642
        //                    m_log.Error("Cannot send email [userid]" + us.userid.ToString());
        //            }

        //        }

        //        if (!hostEmail)//Email to Owner
        //        {
        //            //FB 1642 - Audio Add-on start
        //            string hostEmailBody = "";
        //            string hostAudInfo = "";
        //            if (conf.conftype == 2)
        //            {
        //                hostAudInfo = FetchAudioDialString(conf.confid, conf.instanceid, "H", 0);
        //            }
        //            hostEmailBody = emailbody.Replace("{A}", hostAudInfo);
        //            //FB 1642 - Audio Add-on end
        //            //FB 1767 start
        //            CustAttrBody = "";
        //            CustAttrBody = addConfCustomAttr(conf, "Host");
        //            hostEmailBody = hostEmailBody.Replace("{C}", CustAttrBody);
        //            //FB 1767 end
        //            if (!sendParticipantEmail(confHost.userid, 0, hostEmailBody, conf)) //FB 1642
        //                m_log.Error("Cannot send email [userid]" + confHost.userid.ToString());
        //        }
        //        //Email Changes TRV 
        //        //Scheduler is not a participant and not the owner - person who schedule conf on behalf of host
        //        if (!SchedEmail && conf.owner != conf.userid)
        //        {
        //            emailbody = emailbody.Replace("{A}", ""); //FB 1642
        //            //FB 1767 start
        //            CustAttrBody = "";
        //            CustAttrBody = addConfCustomAttr(conf, "Scheduler");
        //            emailbody = emailbody.Replace("{C}", CustAttrBody);
        //            //FB 1767 end
        //            if (!sendParticipantEmail(conf.userid, -1, emailbody, conf))
        //                m_log.Error("Cannot send email [userid]" + conf.userid.ToString());
        //        }
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("Cannot save email", e);
        //        m_log.Error("sytemException", e);
        //        throw e;
        //    }
        //}
        //#endregion

        //#region  sendParticipantEmail
        //private bool sendParticipantEmail_X(int userid, int cc, string emailbody, vrmConference conf)
        //{
        //    try
        //    {
        //        Boolean blnical = false;//Code added for Error 200 -- Start
        //        string ical = "";//Code added for Error 200 -- Start
        //        string Language = "/en";
        //        string LanguageUpload = "en/upload/";
        //        sysSettings sys = new sysSettings(m_configPath);
        //        sysMailData sysMail = new sysMailData();
        //        vrmUtility util = new vrmUtility();

        //        sys.getSysMail(ref sysMail);

        //        if (organizationID < 11)    //Organization Module
        //            organizationID = defaultOrgId;

        //        if (sysTech == null)
        //        {
        //            sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);
        //        }

        //        string dislaimer = sysSettings.emaildisclaimer;
        //        string websiteURL = sysMail.websiteURL;
        //        string tempID = conf.confid.ToString();

        //        string techname = sysTech.name; //Organization Module
        //        string techemail = sysTech.email;
        //        string techphone = sysTech.phone;

        //        vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);
        //        string from = confHost.Email;
        //        string host = "Conference host: " + confHost.FirstName + " " + confHost.LastName + "<BR />";

        //        List<ICriterion> criterionList = new List<ICriterion>();

        //        List<vrmUser> userList;
        //        criterionList.Add(Expression.Eq("userid", userid));

        //        string userDateFormat = string.Empty;
        //        string userTimeFormat = string.Empty;
        //        string tzDisplay = string.Empty;
        //        string userEmail = string.Empty;
        //        string lastName = string.Empty;
        //        string firstName = string.Empty;
        //        string alternate = string.Empty;
        //        int emailMask = 0;
        //        int doubleEmail = 0;
        //        string req = userid.ToString();

        //        userList = m_IuserDao.GetByCriteria(criterionList);
        //        if (userList.Count == 0)
        //        {
        //            List<vrmGuestUser> guestList = m_IguestUserDao.GetByCriteria(criterionList);
        //            if (guestList.Count > 0)
        //            {
        //                emailMask = guestList[0].emailmask;
        //                if (emailMask != 1)
        //                    return true;
        //                userDateFormat = ((guestList[0].DateFormat.Trim() == "") ? "MM/dd/yyyy" : guestList[0].DateFormat.Trim());
        //                userTimeFormat = ((guestList[0].TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
        //                tzDisplay = ((guestList[0].Timezonedisplay.Trim() == "") ? "1" : guestList[0].Timezonedisplay.Trim());
        //                userEmail = guestList[0].Email;
        //                lastName = guestList[0].LastName;
        //                firstName = guestList[0].FirstName;
        //                req = guestList[0].userid.ToString();
        //                if (guestList[0].DoubleEmail == 1)
        //                    alternate = guestList[0].AlternativeEmail;
        //            }
        //        }
        //        else
        //        {
        //            emailMask = userList[0].emailmask;
        //            if (emailMask != 1)
        //                return true;
        //            //if (userList[0].DateFormat.Trim() == "dd/MM/yyyy")
        //            //    userDateFormat = userList[0].DateFormat.Trim();
        //            //else
        //            //    userDateFormat = "g";
        //            userDateFormat = ((userList[0].DateFormat.Trim() == "") ? "MM/dd/yyyy" : userList[0].DateFormat.Trim());
        //            userTimeFormat = ((userList[0].TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
        //            tzDisplay = ((userList[0].Timezonedisplay.Trim() == "") ? "0" : userList[0].Timezonedisplay.Trim());

        //            userEmail = userList[0].Email;
        //            lastName = userList[0].LastName;
        //            firstName = userList[0].FirstName;
        //            req = userList[0].userid.ToString();
        //            if (userList[0].DoubleEmail == 1)
        //                alternate = userList[0].AlternativeEmail;

        //        }
        //        string ownerid = confHost.userid.ToString();
        //        util.simpleEncrypt(ref req);
        //        util.simpleEncrypt(ref tempID);
        //        //string url_params = websiteURL + Language + "/dispatcher/emaildispatcher.asp?cmd=ResponseInvite&id="; //FB 1628
        //        string url_params = websiteURL + Language + "/genlogin.aspx?id="; //FB 1628
        //        url_params += "0x" + tempID;
        //        url_params += "&req=0x" + req + "&tp=2";

        //        DateTime conferenceDate = conf.confdate;
        //        DateTime setupDate = conf.SetupTime; //Buffer Zone
        //        timeZoneData tz = new timeZoneData();
        //        timeZone.userPreferedTime(conf.timezone, ref conferenceDate);
        //        timeZone.userPreferedTime(conf.timezone, ref setupDate);
        //        timeZone.GetTimeZone(conf.timezone, ref tz);

        //        //Determine subject type (invitation only for now)
        //        string subject = "[Conference Invitation] ";  //Email Changes TRV
        //        subject += conf.externalname + " @ ";
        //        //Code changed by offshore for Issue 1073 -- start
        //        //subject += conferenceDate.ToString("g") + " " + tz.TimeZone;
        //        //subject += conferenceDate.ToString("f") + " " + tz.TimeZone;
        //        subject += setupDate.ToString(userDateFormat + " " + userTimeFormat);// +" " + tz.TimeZone;  //Buffer Zone Changes
        //        subject += " " + ((tzDisplay == "1") ? tz.TimeZone : "");
        //        //Code changed by offshore for Issue 1073 -- end

        //        string common_message = "<span style=\"font-size: 10pt; font-family: Verdana\">";
        //        if (cc != -1) // Email Text Changes for Checking the Scheduler
        //        {             // Email Text Changes for Checking the Scheduler  
        //            common_message += "<BR><U>Tech Support:</U><BR>";
        //            common_message += "Contact : " + techname + "<BR>";
        //            common_message += "Email : " + techemail + "<BR>";
        //            common_message += "Phone : " + techphone + "<BR>";
        //            common_message += "</span>";
        //        }             // Email Text Changes for Checking the Scheduler
        //        //Determine destination type

        //        //Determine user's first name for greeting
        //        string message = "<span style=\"font-size: 10pt; font-family: Verdana\">Hello ";
        //        message += firstName + " " + lastName + ", <BR><BR>";

        //        if (conf.owner == userid) //FB 1041
        //            message += "You have been scheduled to host the following meeting: <br/>";//FB 1041 Email Text Changes //FB 1767
        //        else//FB 1041
        //            message += "The following conference has been set up or modified <br/>";//Email Text Changes //FB 1767
        //        //Determine the type of conference access


        //        message += "<BR></span>";

        //        message += emailbody;
        //        IConfAttachmentDAO confAttach = m_confDAO.GetConfAttachmentDao();
        //        criterionList = new List<ICriterion>();
        //        criterionList.Add(Expression.Eq("confid", conf.confid));
        //        criterionList.Add(Expression.Eq("instanceid", conf.instanceid));

        //        List<vrmConfAttachments> attachments = confAttach.GetByCriteria(criterionList);
        //        int i = 0;
        //        string att = string.Empty;
        //        foreach (vrmConfAttachments attach in attachments)
        //        {
        //            if (attach.attachment.Length > 0)
        //            {
        //                string sAttachment = attach.attachment;
        //                if (i == 0)
        //                {
        //                    att = "<span style=\"font-family: Verdana;font-size: 10pt\">";//Email Text Changes
        //                    att += "<BR><U>Attachments: </U><BR />";
        //                }

        //                if (sAttachment.Length > 6)//Code added for Error 200 -- Start
        //                {
        //                    if (sAttachment.Substring(0, 6) == "[ICAL]")
        //                    {
        //                        blnical = true;
        //                        ical = sAttachment.Substring(6, sAttachment.Length - 6);
        //                    }
        //                }//Code added for Error 200 -- End

        //                string testString = "\\en\\upload\\";
        //                int j = sAttachment.IndexOf(testString);
        //                if (j > 0)
        //                {
        //                    sAttachment = sAttachment.Substring(j + testString.Length, sAttachment.Length - (j + testString.Length));
        //                    sAttachment.Replace("\\", "/");
        //                    string quote = "\"";
        //                    att += "<br /><a href= " + quote + websiteURL + LanguageUpload +
        //                    sAttachment + quote + ">" + sAttachment + "</a>";
        //                    i++;
        //                }

        //            }
        //        }
        //        if (i > 0)
        //        {
        //            att += "</span><BR>";//Email Text Changes
        //        }
        //        if ((cc != -1) && (cc != 0)) //Added for 1364 // Email Text Changes for Checking the Scheduler
        //        {// Email Text Changes for Checking the Scheduler
        //            if (conf.owner != userid || conf.owner != conf.userid) //FB 1007
        //            {
        //                message += "<span style=\"font-size: 10pt; font-family: Verdana\">Please <B>click</B> <a href= \"";
        //                message += url_params + "\">";
        //                message += "<B>here</B></a> to accept/decline the invitation.<br> ";
        //                message += "<br />If you have any problems accessing the link, please copy and paste the following URL into the address bar of your browser: ";
        //                message += url_params + "</span><br/>"; //FB 1767
        //            }
        //        }// Email Text Changes for Checking the Scheduler
        //        common_message += att + dislaimer;

        //        message += common_message;

        //        vrmEmail theEmail = new vrmEmail();
        //        theEmail.emailTo = userEmail;
        //        theEmail.emailFrom = confHost.Email;
        //        theEmail.Message = AttachEmailLogo(conf.orgId) + message;
        //        theEmail.Subject = subject;
        //        if (cc == 1)
        //            theEmail.cc = userEmail;
        //        if (blnical)//Code added for Error 200
        //            theEmail.Attachment = ical;//Code added for Error 200

        //        theEmail.UUID = getEmailUUID();

        //        theEmail.orgID = conf.orgId; //Added for FB 1710

        //        m_IemailDao.Save(theEmail);
        //        // DoubleEmail - email a copy to the alternateEmail for the user
        //        if (doubleEmail == 1)
        //        {
        //            if (alternate.Length > 0)
        //            {
        //                theEmail.emailTo = alternate;
        //                theEmail.UUID = getEmailUUID();
        //                m_IemailDao.Save(theEmail);
        //            }
        //        }

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("Cannot save email", e);
        //        m_log.Error("sytemException", e);
        //        throw e;
        //    }
        //}
        //#endregion

        //#region emailLocations
        //public bool emailLocations(List<vrmConference> confList)
        //{
        //    try
        //    {
        //        vrmConference conf = confList[0];

        //        vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);
        //        DateTime conferenceDate = conf.confdate;
               
        //        timeZoneData tz = new timeZoneData();
        //        timeZone.userPreferedTime(conf.timezone, ref conferenceDate);

        //        /* *** Code Added for Buffer Zone *** --Start */
        //        DateTime setupDate = conf.SetupTime;
        //        timeZone.userPreferedTime(conf.timezone, ref setupDate);
        //        DateTime teardownDate = conf.TearDownTime;
        //        timeZone.userPreferedTime(conf.timezone, ref teardownDate);
        //        Double dur = Convert.ToDouble(conf.duration);
        //        DateTime endDate = conferenceDate.AddMinutes(dur);
        //        /* *** Code Added for Buffer Zone *** --End */
        //        timeZone.GetTimeZone(conf.timezone, ref tz);

        //        List<ICriterion> criterionList;
        //        List<vrmConfRoom> rList = new List<vrmConfRoom>();
        //        String emailbody = "";

        //        emailbody += "<span style=\"font-size: 10pt; font-family: Verdana\">";
        //        emailbody += "Hello {0}, <BR/><br/>";//FB 1041
        //        emailbody += "One or more of your conference rooms have been reserved ";
        //        emailbody += "by the following conference(s).<BR/>";
        //        emailbody += "</span><BR/>";

        //        emailbody += "<table width=\"800\" style=\"font-size: 10pt; font-family: Verdana; background-color: silver\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">";

        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\" style=\"width: 20%\">";
        //        emailbody += " Conference Name:</td>";
        //        emailbody += "<td style=\"background-color: silver\">";
        //        emailbody += "  <strong>" + conf.externalname + "</strong></td>";
        //        emailbody += "</tr>";

        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\">";
        //        emailbody += "Unique ID:</td>";
        //        emailbody += "<td style=\"background-color: silver\">";
        //        emailbody += " <strong>" + conf.confnumname + "</strong></td>";
        //        emailbody += "</tr>";

        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\" style=\"height: 18px\">";
        //        emailbody += "Start Date and Time:</td>";    //Buffer Zone
        //        emailbody += "<td style=\"height: 18px; background-color: silver\">";
        //        //Code changed by offshore for FB Issue 1073 -- start
        //        emailbody += "<strong>" + conferenceDate.ToString(confHost.DateFormat.Trim() + " " + ((confHost.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
        //        emailbody += ((confHost.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
        //        //Code changed by offshore for FB Issue 1073 -- end
        //        emailbody += "</tr>";
        //        /* *** Code Added for Buffer Zone *** -- Start */

        //        if (organizationID < 11)    //Organization Module
        //            organizationID = defaultOrgId;

        //        if (orgInfo == null)
        //            orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

        //        if (orgInfo != null)
        //            enableBZ = orgInfo.EnableBufferZone;

        //        if (enableBZ == "1")    //Organization Module
        //        {
        //            emailbody += "<tr>";
        //            emailbody += "<td align=\"right\" style=\"height: 18px\">";
        //            emailbody += "Attendee show-up/Setup completed :</td>";
        //            emailbody += "<td style=\"height: 18px; background-color: silver\">";
        //            emailbody += "<strong>" + setupDate.ToString(confHost.DateFormat.Trim() + " " + ((confHost.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
        //            emailbody += ((confHost.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
        //            emailbody += "</tr>";

        //            emailbody += "<tr>";
        //            emailbody += "<td align=\"right\" style=\"height: 18px\">";
        //            emailbody += "Attendee leave/ Teardown started :</td>";
        //            emailbody += "<td style=\"height: 18px; background-color: silver\">";
        //            emailbody += "<strong>" + teardownDate.ToString(confHost.DateFormat.Trim() + " " + ((confHost.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
        //            emailbody += ((confHost.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
        //            emailbody += "</tr>";
        //        }
        //        /*
        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\" style=\"height: 18px\">";
        //        emailbody += "EndDate and Time :</td>";
        //        emailbody += "<td style=\"height: 18px; background-color: silver\">";
        //        emailbody += "<strong>" + endDate.ToString(confHost.DateFormat.Trim() + " " + ((confHost.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
        //        emailbody += ((confHost.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + " </strong></td>";
        //        emailbody += "</tr>"; */

        //        /* *** Code Added for Buffer Zone *** -- End */ 

        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\">";
        //        emailbody += "Duration: </td>";
        //        emailbody += "<td style=\"background-color: silver\">";
        //        //Email Text Changes Start
        //        int hr = conf.duration / 60;
        //        int min = conf.duration % 60;
        //        String durationString = "";
        //        if (hr > 0 && min
        //            > 0)
        //            durationString = hr.ToString() + " hrs " + min.ToString() + " mins";
        //        else if (hr > 0 && min == 0)
        //            durationString = hr.ToString() + " hrs ";
        //        else
        //            durationString = min.ToString() + " mins";
        //        emailbody += "<strong>" + durationString + "</strong></td>"; //Email Text Changes
        //        //Email Text Changes End
        //        emailbody += "</tr>";

        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\">";
        //        emailbody += "Host:</td>";
        //        emailbody += "<td><strong>";
        //        emailbody += confHost.FirstName + " " + confHost.LastName;  //Email Text Changes

        //        emailbody += "</strong></td>";
        //        emailbody += "</tr>";

        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\">Description";
        //        emailbody += "</td>";
        //        if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
        //            conf.description = "N/A"; //Email Text Changes
        //        emailbody += "<td><strong>" + conf.description;
        //        emailbody += "</strong></td>";
        //        emailbody += "</tr>";

        //        List<ICriterion> recurInfo = new List<ICriterion>();

        //        recurInfo.Add(Expression.Eq("confid", conf.confid));
        //        List<vrmRecurInfo> recurPattern = m_vrmConfRecurDAO.GetByCriteria(recurInfo, true);

        //        if (recurPattern.Count > 0)
        //        {
        //            emailbody += "<tr>";
        //            emailbody += "<td align=\"right\">";
        //            emailbody += "Conference Recurring: </td>";
        //            emailbody += "<td style=\"background-color: silver\">";
        //            emailbody += "<strong>" + recurPattern[0].RecurringPattern + "</strong></td>";
        //            emailbody += "</tr>";
        //        }
        //        emailbody += "<tr>";
        //        emailbody += "<td align=\"right\">";
        //        emailbody += "Location(s):";
        //        emailbody += "<br />";
        //        emailbody += "</td>";

        //        emailbody += "{1}";

        //        emailbody += "</tr>";
        //        //FB 1767 start
        //        String custAttrBody = "";
        //        custAttrBody = addConfCustomAttr(conf, "RoomAdmin");
        //        emailbody += custAttrBody;
        //        //FB 1767 end                    
        //        emailbody += "</table>"; //Email Text Changes
        //        emailbody += "<br />";
        //        emailbody += "<br>";

        //        vrmUser objUser = null;
        //        String locationString = "";
        //        String strAsstName = "";
        //        string AsstEmail = "";
        //        criterionList = new List<ICriterion>();
        //        criterionList.Add(Expression.Eq("confid", conf.confid));
        //        criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
        //        rList = m_IconfRoom.GetByCriteria(criterionList);
        //        foreach (vrmConfRoom cr in rList)
        //        {
        //            locationString = "";
        //            objUser = new vrmUser();
        //            objUser = m_IuserDao.GetByUserId(cr.Room.assistant);
        //            AsstEmail = objUser.Email;
        //            strAsstName = objUser.FirstName + " " + objUser.LastName;
        //            locationString += "<td style=\"background-color: silver\">";
        //            locationString += "<strong>" + cr.Room.tier2.tier3.Name + ">" +
        //                cr.Room.tier2.Name + ">" + cr.Room.Name + "</strong><br />";
        //            locationString += "</td>";

        //            vrmEmail theEmail = new vrmEmail();
        //            theEmail.emailTo = AsstEmail;
        //            theEmail.emailFrom = confHost.Email;
        //            //emailbody = emailbody.Replace("{1}", locationString);
        //            theEmail.Message = emailbody.Replace("{0}", strAsstName);
        //            theEmail.Message = theEmail.Message.Replace("{1}", locationString); //FB 1265
        //            //Determine subject type (invitation only for now)
        //            string subject = "[Conference Room Reservation] ";
        //            subject += conf.externalname + " @ ";
        //            //Code changed by Offshore for FB Issue 1073 -- start
        //            //subject += conferenceDate.ToString("g") + " " + tz.TimeZone;
        //            //subject += conferenceDate.ToString("f") + " " + tz.TimeZone;
        //            subject += "(" + conferenceDate.ToString(objUser.DateFormat.Trim() + " " + ((objUser.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
        //            subject += ((objUser.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + ")";
        //            //Code changed by Offshore for FB Issue 1073 -- end
        //            theEmail.Subject = subject;
        //            theEmail.UUID = getEmailUUID();
        //            theEmail.Message = AttachEmailLogo(organizationID) + theEmail.Message;//fb 1602

        //            theEmail.orgID = conf.orgId; //Added for FB 1710

        //            if (theEmail.emailTo.Trim().Length > 0)
        //                m_IemailDao.Save(theEmail);

        //            string strNotifyEmails = cr.Room.notifyemails;
        //            if (strNotifyEmails.Length > 1)
        //            {
        //                while (strNotifyEmails.Length > 0)
        //                {
        //                    int x = strNotifyEmails.IndexOf(";");
        //                    string value = string.Empty;
        //                    if (x > 0)
        //                    {
        //                        value = strNotifyEmails.Substring(0, x);
        //                        strNotifyEmails = strNotifyEmails.Substring(x + 1, strNotifyEmails.Length - (x + 1));
        //                    }
        //                    else
        //                    {
        //                        value = strNotifyEmails;
        //                        strNotifyEmails = string.Empty;
        //                    }
        //                    if (value.Length > 0 && !AsstEmail.ToLower().Equals(value.ToLower()))
        //                    {
        //                        theEmail = new vrmEmail();
        //                        theEmail.emailTo = value;
        //                        theEmail.emailFrom = confHost.Email;
        //                        theEmail.Message = emailbody.Replace("{0}", "");
        //                        theEmail.Message = theEmail.Message.Replace("{1}", locationString); //FB 1265
        //                        theEmail.Message = AttachEmailLogo(organizationID) + theEmail.Message;
        //                        theEmail.Subject = subject;
        //                        theEmail.UUID = getEmailUUID();

        //                        theEmail.orgID = conf.orgId; //Added for FB 1710

        //                        m_IemailDao.Save(theEmail);
        //                    }
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        m_log.Error("Cannot save email Locations", ex);
        //        m_log.Error("sytemException", ex);
        //        throw ex;
        //    }
        //    return true;
        //}
        //#endregion

        //#region sendMCUAdminEmail
        //public bool sendMCUAdminEmail(vrmConference conf)
        //{
        //    try
        //    {
        //        vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);
        //        string subject = string.Empty;
        //        string bufferzone = string.Empty;
        //        /* *** Code Added for Buffer Zone *** --Start */
        //        DateTime conferenceDate = conf.confdate;
        //        DateTime setupDate = conf.SetupTime;
        //        if (setupDate != DateTime.MinValue)
        //            bufferzone = "Approvers";
        //        else
        //            bufferzone = "";
        //        /* *** Code Added for Buffer Zone *** --End */
        //        foreach (vrmMCU mcu in conf.mcuList)
        //        {

        //            vrmUser user = m_IuserDao.GetByUserId(mcu.Admin);
        //            string message  = "<span style=\"font-size: 10pt; font-family: Verdana\">";
        //            message += "Hello " + user.FirstName + " " + user.LastName + ", <BR/><BR/>";
        //            timeZoneData tz = new timeZoneData();
        //            timeZone.userPreferedTime(conf.timezone, ref conferenceDate); //Email Changes TRV
        //            timeZone.GetTimeZone(conf.timezone, ref tz);

        //            //string confdate = conferenceDate.ToString("g") + " " + tz.TimeZone;
        //            string confdate = "";
        //            if(user.TimeFormat.Trim() == "1")
        //                confdate = conferenceDate.ToString(user.DateFormat.Trim() + " hh:mm tt");// +" " + tz.TimeZone;
        //            else
        //                confdate = conferenceDate.ToString(user.DateFormat.Trim() + " HH:mm");// +" " + tz.TimeZone;

        //            if (user.Timezonedisplay.Trim() == "1")
        //                confdate += " " + tz.TimeZone; 

        //            /**************     Code Modified For FB#:1017 - Start  *************/

        //            //subject = "[New Pending Conference Notification]" + conf.externalname + " @ " + confdate;
        //            //message += "<BR>A new conference has been requested.";

        //            //subject = "MCU RESERVATION";
        //            subject = "[Conference MCU Reservation] ";
        //            subject += conf.externalname + " @ ";
        //            //Cdoe change dfor FB Issue 1073
        //            //subject += conferenceDate.ToString("g") + " " + tz.TimeZone;
        //            //subject += conferenceDate.ToString("f") + " " + tz.TimeZone;
        //            subject += "(" + conferenceDate.ToString(user.DateFormat.Trim() + " " + ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt"));
        //            subject += ((user.Timezonedisplay.Trim() == "1") ? " " + tz.TimeZone : "") + ")";
        //            if (!conf_isEdit) //FB 1749
        //                message += "<BR>A New conference has been requested.<br/><br/>One or more of your MCUs have been reserved by the following conference(s).<br/>"; //FB 1749
        //            else
        //                message += "<BR>An Existing conference has been modified.<BR><BR>One or more of your MCUs have been reserved by the following conference(s).<br/>"; //FB 1749

        //            //message += "<BR>The details are as follows:<BR>";
        //            message += "</span><BR/>";

        //            /**************  Code Modified For FB#:1017 - End     *************/
        //            string additionalMessage = "";
        //            string emailbody = string.Empty;
        //            /* ******************       FogBugz Case No:1017 - start       ********************* */

        //            //getConferenceDetailsForEmail(user, conf.confid, conf.instanceid, "", ref emailbody, ref additionalMessage);

        //            getConferenceDetailsForEmail(user, conf, "", ref emailbody, ref additionalMessage,bufferzone); //buffer zone

        //            /* ******************       FogBugz Case No:1017 - end         ********************* */

        //            //FB 1642 Audio Add-On Strats

        //            string confcodepin = "";

        //            if (conf.conftype == 2)
        //                confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "M", 0);

        //            emailbody = emailbody.Replace("{A}", confcodepin);

        //            //FB 1642 Audio Add-On Ends
                    
        //            //FB 1718 - starts
        //            //FB 1767 start
        //            String custAttrBody = "";
        //            custAttrBody = addConfCustomAttr(conf, "McuAdmin"); 
        //            emailbody = emailbody.Replace("{C}", custAttrBody);
        //            //FB 1767 end
        //            //FB 1718 - end

        //            message += emailbody;


        //            /***    Code Commented to meet FB#: 1017 - Start     ***/

        //            //message += "<BR>Please log into the VRM website to view the above conference";

        //            //sysSettings sys = new sysSettings(m_configPath);
        //            //sysMailData sysMail = new sysMailData();

        //            //sys.getSysMail(ref sysMail);

        //            //string websiteURL = sysMail.websiteURL;

        //            //message += "<br>Please <B>click </B> <a href=";
        //            //message += websiteURL;
        //            //message += "><B>HERE</B></a> and log into the myVRM scheduling web site.<br><br>";
        //            //message += "If you have any problems accessing the link, please copy ";
        //            //message += " and paste the following URL into the address bar of your browser: ";
        //            //message += websiteURL + "<BR>";

        //            /***    Code Commented to meet FB#: 1017 - End  ***/

        //            /* Code Added For FB#:1017 - Start */
        //            message += "<BR>";
        //            /* Code Added For FB#:1017 - End */

        //            vrmEmail theEmail = new vrmEmail();
        //            theEmail.emailTo = user.Email;
        //            theEmail.emailFrom = confHost.Email;
        //            theEmail.Message = AttachEmailLogo(conf.orgId) + message;
        //            theEmail.Subject = subject;

        //            theEmail.orgID = conf.orgId; //Added for FB 1710

        //            theEmail.UUID = getEmailUUID();
        //            m_IemailDao.Save(theEmail);
        //        }
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("Cannot save email", e);
        //        m_log.Error("sytemException", e);
        //        throw e;
        //    }
        //}
        //#endregion
                
        //#region sendLevelEmail
        //public bool sendLevelEmail_X(vrmConference conf, int level, vrmUser user)
        //{
        //    try
        //    {
        //        string CustAttrBody = ""; //FB 1767
        //        vrmUser confHost = m_IuserDao.GetByUserId(conf.owner);
        //        string message = "<span style=\"font-size: 10pt; font-family: Verdana\">";
        //        message += "Hello " + user.FirstName + " " + user.LastName + ", <br/><br/>";
        //        string subject = string.Empty;
        //        DateTime conferenceDate = conf.confdate;

        //        timeZoneData tz = new timeZoneData();
        //        timeZone.userPreferedTime(user.TimeZone, ref conferenceDate);
        //        timeZone.GetTimeZone(user.TimeZone, ref tz);  //code changed for display the user time zone

        //        //code changed for FB Issue 1073 
        //        //string confdate = conferenceDate.ToString("g") + " " + tz.TimeZone;
        //        //string confdate = conferenceDate.ToString("f") + " " + tz.TimeZone;
        //        string confdate = "";
        //        if (user.TimeFormat.Trim() == "1")
        //            confdate = conferenceDate.ToString(user.DateFormat.Trim() + " hh:mm tt");// +" " + tz.TimeZone;
        //        else
        //            confdate = conferenceDate.ToString(user.DateFormat.Trim() + " HH:mm");// +" " + tz.TimeZone;

        //        if (user.Timezonedisplay.Trim() == "1")
        //            confdate += " " + tz.TimeZone; 

        //        switch (level)
        //        {
        //            case (int)LevelEntity.ROOM:
        //                //subject = "[Room Approval Request]" + conf.confnumname + " @ " + confdate;
        //                subject = "[Room Approval Request]" + conf.externalname + " @ " + confdate;
        //                message += "One of the rooms in your domain has been requested for a conference."; //Email Text Changes//FB 1767
        //                CustAttrBody = addConfCustomAttr(conf, "RoomApp"); //FB 1767
        //                break;
        //            case (int)LevelEntity.MCU:
        //                //subject = "[MCU Approval Request]" + conf.confnumname + " @ " + confdate;
        //                subject = "[MCU Approval Request]" + conf.externalname + " @ " + confdate;
        //                message += "One of the MCU(s) in your domain has been requested for a conference <BR/><br/>"; //Email Text Changes
        //                message += "MCU names: ";

        //                int x = 0;
        //                foreach (vrmMCU mcu in conf.mcuList)
        //                {
        //                    bool addit = false;
        //                    foreach (vrmMCUApprover mcuApp in mcu.MCUApprover)
        //                    {
        //                        if (mcuApp.approverid == user.userid)
        //                            addit = true;
        //                        break;
        //                    }
        //                    if (addit)
        //                    {
        //                        message += mcu.BridgeName; ;
        //                        if (x > 0)
        //                            message += " : ";
        //                        x++;
        //                    }
        //                }
        //                CustAttrBody = addConfCustomAttr(conf, "McuApp"); //FB 1767
        //                break;
        //            case (int)LevelEntity.DEPT:
        //                subject = "[Department Approval Request] " + conf.externalname + " @ " + confdate; //Email Changes TRV
        //                message += "One of the conferences in your domain has been requested for a conference "; //Email Text Changes
        //                break;
        //            case (int)LevelEntity.SYSTEM:
        //                subject = "[Conference Approval Request] " + conf.externalname + " @ " + confdate; //Email Changes TRV
        //                message += "A conference is set up or modified and awaiting your approval. "; //Email Text Changes
        //                CustAttrBody = addConfCustomAttr(conf, "SystemApp"); //FB 1767
        //                break;

        //        }
        //        message += "<br/><br/>This is an official request for approval of the same.<br/><br/>"; //Email Text Changes
        //        message += "The details are as follows:</span><BR /><br/>"; //Email Text Changes

        //        string additionalMessage = "";
        //        string emailbody = string.Empty;
        //        /* *** Code Added for Buffer Zone *** --Start */
        //        string bufferzone = string.Empty;
        //        DateTime setupDate = conf.SetupTime;
        //        if (setupDate != DateTime.MinValue)
        //            bufferzone = "Approvers";
        //        else
        //            bufferzone = "";
        //        /* *** Code Added for Buffer Zone *** --End */

        //        /* ******************       FogBugz Case No:1017 - start       ********************* */

        //        //getConferenceDetailsForEmail(user, conf.confid, conf.instanceid,"", ref emailbody, ref additionalMessage);

        //        getConferenceDetailsForEmail(user, conf, "", ref emailbody, ref additionalMessage, bufferzone);  //buffer zone

        //        /* ******************       FogBugz Case No:1017 - start       ********************* */
        //        emailbody = emailbody.Replace("{A}", ""); //FB 1642
        //        emailbody = emailbody.Replace("{C}", CustAttrBody);//FB 1718 //FB 1767 
        //        message += emailbody;
        //        message += "<span style=\"font-size: 10pt; font-family: Verdana\">";
        //        message += "<BR>Please log into the myVRM website to approve the above conference.<br/><br/>"; //Email Text Changes
        //        sysSettings sys = new sysSettings(m_configPath);
        //        sysMailData sysMail = new sysMailData();

        //        sys.getSysMail(ref sysMail);

        //        string websiteURL = sysMail.websiteURL;

        //        message += "<br>Please <B>click </B><a href=";
        //        message += websiteURL;
        //        message += "><B>here</B></a> and log into the myVRM scheduling web site to approve/deny this request.<br><br>";
        //        message += "If you have any problems accessing the link, please copy ";
        //        message += " and paste the following URL into the address bar of your browser: ";
        //        message += websiteURL + " </span><BR>"; //Email Text Changes

        //        vrmEmail theEmail = new vrmEmail();
        //        theEmail.emailTo = user.Email;
        //        theEmail.emailFrom = confHost.Email;
        //        theEmail.Message = AttachEmailLogo(conf.orgId) + message;
        //        theEmail.Subject = subject;
        //        theEmail.orgID = conf.orgId; //Added for FB 1710

        //        theEmail.UUID = getEmailUUID();
        //        m_IemailDao.Save(theEmail);
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("Cannot save email", e);
        //        m_log.Error("sytemException", e);
        //        throw e;
        //    }
        //}
        //#endregion
       

        //#region  FeedbackEmail
        //public bool FeedbackEmail(vrmEmailFeedback feedback, ref string outXml)
        //{
        //    bool bRet = true;
        //    try
        //    {
        //        vrmUser user = m_IuserDao.GetByUserId(feedback.userID);
        //        DateTime dtTime = DateTime.Now;
        //        timeZone.changeToGMTTime(sysSettings.TimeZone, ref dtTime);
        //        timeZone.GMTToUserPreferedTime(user.TimeZone, ref dtTime);

        //        string emailSubject = "[myVRM Feedback] " + feedback.subject;

        //        string mailbody = "<br><b>This is a feedback message from a user of the myVRM website. </b>";
        //        mailbody += "<br><br><b> The user details are as follows - </b><br>";
        //        mailbody += "<br><b>Name:</b> " + feedback.name + "<br><b>Email:</b> " + feedback.email + "<br>";

        //        mailbody += "<br><b>The PC details are as follows - </b><br>";
        //        //Code Chnged for FB Issue 1073 
        //        //mailbody += "<br><b>Users Date/Time:</b> " + dtTime.ToString("g");
        //        mailbody += "<br><b>Users Date/Time:</b> " + dtTime.ToString("f");
        //        mailbody += "<br><b>Web Page:</b> " + feedback.parentPage;
        //        mailbody += "<br><b>Browser Agent:</b>" + feedback.browserAgent;
        //        mailbody += "<br><b>Browser Language:</b>" + feedback.browserLanguage;
        //        mailbody += "<br><b>Users IPAddress:</b>" + feedback.ipAddress;
        //        mailbody += "<br><br><b>The feedback comments are as follows - </b>";
        //        mailbody += "<hr width=90% align=left><br><b></b><br>" + feedback.comment;

                
        //        if (organizationID < 11)    //Organization Module
        //            organizationID = defaultOrgId;

        //        if (sysTech == null)
        //        {
        //            sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);
        //        }

        //        if (sysTech != null)     //Organization Module
        //        {
        //            vrmEmail theEmail = new vrmEmail();
        //            theEmail.emailTo = sysTech.email;
        //            theEmail.emailFrom = user.Email;
        //            theEmail.Message = AttachEmailLogo(user.companyId) + mailbody;
        //            theEmail.Subject = feedback.subject;
        //            theEmail.UUID = getEmailUUID();

        //            theEmail.orgID = user.companyId; //Added for FB 1710

        //            m_IemailDao.Save(theEmail);
        //        }
        //        else // No support email entry found in the database so throw an error
        //        {
        //            myVRMException e = new myVRMException(425);
        //            m_log.Error("vrmException", e);
        //            outXml = myVRMException.toXml("Unable to find Support email address in the database.");
        //            return false;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("Cannot save email", e);
        //        m_log.Error("sytemException", e);
        //        throw e;
        //    }

        //    return bRet;

        //}
        //#endregion
        #endregion

        #region getEmailUUID
        private int getEmailUUID()
        {
            try
            {
                string qString = "select max(em.UUID) from myVRM.DataLayer.vrmEmail em";

                IList list = m_IemailDao.execQuery(qString);

                string sMax;
                if (list[0] != null)
                    sMax = list[0].ToString();
                else
                    sMax = "0";

                int id = Int32.Parse(sMax);
                return ++id;
            }
            catch (Exception e)
            {
                m_log.Error("Cannot save email", e);
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Get Custom Attribute Details
        /// <summary>
        /// Get Custom Attribute Details
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        private String AddConfAttrToEmailString(vrmConference conf,String mailTo) //FB 1767
        {
            string confAttString = "";
            Dictionary<string, string> custAtts = null; //FB 2419
            IList<vrmConfAttribute> custAttrList = null;
            List<vrmDeptCustomAttr> custAttributeList = null;
            List<ICriterion> criterionList = null;
            try
            {   
                if (conf != null)
                {
                    custAttrList = m_confDAO.GetConfCustAttributes(conf.confid, conf.instanceid);
                    if (custAttrList != null)
                    {
                        foreach (vrmConfAttribute confAtt in custAttrList)
                        {
                            if (confAtt.CustomAttributeId > 0)
                            {
                                if(custAtts == null)
                                    custAtts = new Dictionary<string, string>(); //FB 2419

                                criterionList = null;
                                criterionList = new List<ICriterion>();
                                custAttributeList = null;
                                custAttributeList = new List<vrmDeptCustomAttr>();
                                
                                criterionList.Add(Expression.Eq("CustomAttributeId", confAtt.CustomAttributeId));
                                criterionList.Add(Expression.Eq("IncludeInEmail", 1)); //FB 1767
                                criterionList.Add(Expression.Eq(mailTo, 1));  //FB 1767
                                custAttributeList = m_IDeptCustDAO.GetByCriteria(criterionList);
                                if (custAttributeList != null)
                                {
                                    if (custAttributeList.Count > 0)
                                    {
                                        if (custAttributeList[0].IncludeInEmail == 1)
                                        {
                                            if (!custAtts.ContainsKey(custAttributeList[0].DisplayTitle)) //FB 2419
                                            {
                                                if (custAttributeList[0].Type == 2 || custAttributeList[0].Type == 3)//FB 2377
                                                {
                                                    if (confAtt.SelectedValue == "1")
                                                    {
                                                        custAtts.Add(custAttributeList[0].DisplayTitle, "Yes");
                                                    }
                                                    else
                                                    {
                                                        custAtts.Add(custAttributeList[0].DisplayTitle, "No");
                                                    }
                                                }
                                                else if (custAttributeList[0].Type == 7)
                                                {
                                                    string urlText = "<a href=http://" + confAtt.SelectedValue + " target='_blank'>" + confAtt.SelectedValue + "</a>";
                                                    custAtts.Add(custAttributeList[0].DisplayTitle, urlText);
                                                }
                                                else if (custAttributeList[0].Type == 10) //FB 1718
                                                {
                                                    custAtts.Add(custAttributeList[0].DisplayTitle, confAtt.SelectedValue.Replace("\r\n", "<br/>").Replace("\n", "<br/>")); //FB 1718
                                                }
                                                else
                                                {
                                                    custAtts.Add(custAttributeList[0].DisplayTitle, confAtt.SelectedValue);
                                                }
                                            }
                                            else
                                            {
                                                if(custAttributeList[0].Type == 5)
                                                {
                                                    string custVal = custAtts[custAttributeList[0].DisplayTitle].ToString() +","+ confAtt.SelectedValue;
                                                    custAtts[custAttributeList[0].DisplayTitle] = custVal;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (custAtts != null)
                {
                    if (custAtts.Count > 0)
                    {
						//FB 2480 Starts
                        confAttString = "<td>"; 
                        foreach(String hKey in custAtts.Keys) 
                        {
                            confAttString += " <strong> " + hKey + ": </strong>";
                            confAttString += "   " + custAtts[hKey].ToString() + "<br/><br/>";
                        }
                        confAttString += "</td>";
						//FB 2480 Ends
                    }
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return confAttString;
        }

        //Method added for FB 1767
        private String addConfCustomAttr(String custMailTo)     //FB 1830
        {
            string customAttValues = "";
            try
            {
                string confAttrs = AddConfAttrToEmailString(conf, custMailTo);
				//FB 2480 Starts
                customAttValues = "<td align=\"right\"valign=\"top\" >"
                + " Custom Options:</td>";

                if (confAttrs != "")
                {
                    customAttValues +=  confAttrs ;
                }
                else
                {
                    customAttValues += "<td><b>N/A</b></td>";
                }
				//FB 2480 Ends
            }
            catch (Exception e)
            {
                return "";
            }
            return customAttValues;
        }
        #endregion

        #region Attach Email Logo

        public string AttachEmailLogo(int orgId)
        {
            String attchmnt = "";
            string logoId = "";
            try
            {
                sysSettings sys = new sysSettings(m_configPath);
                sysMailData sysMail = new sysMailData();
                sys.getSysMail(ref sysMail);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgId);

                if (orgInfo.MailLogo > 0)
                {
                    //logoId = "MailLogo�" + orgInfo.MailLogo + "�ImgId"; //FB 1658 Embedded Image Alt147
                    logoId = "LogoImg"; //FB 1658 Embedded Image Alt147
                    string logoName = "Org_" + orgId + "mail_logo.gif"; //FB 1860
                    attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\""+ logoId +"\" src=\""+ logoName +"\" alt /></td></tr></table>";
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return attchmnt;
        }

        #endregion

        //Create ICAL for Cisco Endpoints -- Start

        #region sendICALforCISCOEndpoints
        /// <summary>
        /// sendICALforCISCOEndpoints
        /// </summary>
        /// <param name="confList"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        // Modified for FB 1830
        public bool sendICALforCISCOEndpoints(List<vrmConference> confList, String action, bool isDeletedRooms, int eptID) //FB 1675
        {
            IList conferences = new ArrayList();
            ArrayList lstMailmsg = new ArrayList();
            String location = "";
            try
            {
                if (isDeletedRooms)//FB 1675
                    action = "D";

                if (orgInfo == null) //FB 1786
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(confList[0].orgId);
                List<vrmConfRoom> rList = new List<vrmConfRoom>();
                List<ICriterion> criterionList;

                confHost = m_IuserDao.GetByUserId(confList[0].owner);
                string from = confHost.Email;

                List<vrmUser> userList;

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", confList[0].confid));
                criterionList.Add(Expression.Eq("instanceid", confList[0].instanceid));

                List<vrmConfUser> uList = m_vrmConfUserDAO.GetByCriteria(criterionList);
                //FB 2453 Starts
                emailObject.ExternalBridge = "N/A";
                emailObject.InternalBridge = "N/A";

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionList);
                // if (confList[0].isVMR == 1)
                if (confList[0].isVMR > 0) // FB 2620 //FB 2957
                {
                    if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                    {
                        emailObject.ExternalBridge = confAVParam[0].externalBridge;
                    }

                    if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                    {
                        emailObject.InternalBridge = confAVParam[0].internalBridge;
                    }
                }
                //FB 2453 Ends
                String required = "";

                bool hostEmail = false;


                for (int u = 0; u < uList.Count; u++)
                {

                    if (uList[u].userid == confHost.userid)
                        hostEmail = true;

                    String reqemails = "";
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", uList[u].userid));
                    userList = m_IuserDao.GetByCriteria(criterionList);
                    if (userList.Count == 0)
                    {
                        List<vrmGuestUser> guestList = m_IguestUserDao.GetByCriteria(criterionList);
                        if (guestList.Count > 0)
                        {
                            if (guestList.Count > 0)
                                reqemails = guestList[0].Email;
                        }
                    }
                    else
                        reqemails = userList[0].Email;
                    if (reqemails != "")
                    {

                        if (required == "")
                            required = reqemails;
                        else
                            required += ";" + reqemails;
                    }

                }

                if (!hostEmail)
                {
                    if (required == "")
                        required = confHost.Email;
                    else
                        required += ";" + confHost.Email; ;
                }


                String[] recipients = required.Split(';');
                //FB 2075
                DateTime serverTime = DateTime.Now;
                serverTime = serverTime.AddMinutes(-5);
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                //FB 2075

                foreach (vrmConference cf in confList)
                {
                    //FB 2075
                    if (cf.confdate < serverTime)
                        continue;
                    //FB 2075


                    DateTime dt = cf.confdate;
                    DateTime conferenceDate = cf.confdate;

                    timeZoneData tz = new timeZoneData();
                    /* *** Code Added for Buffer Zone *** --Start */
                    DateTime setupDate = cf.SetupTime;
                    DateTime teardownDate = cf.TearDownTime;
                    Double dur = Convert.ToDouble(cf.duration);
                    DateTime endDate = conferenceDate.AddMinutes(dur);
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", cf.confid));
                    criterionList.Add(Expression.Eq("instanceid", cf.instanceid));

                    if (isDeletedRooms)// FB 1675
                        criterionList.Add(Expression.Eq("disabled",1));

                    if (eptID > 0)// FB 1675
                        criterionList.Add(Expression.Eq("roomId", eptID));


                    rList = m_IconfRoom.GetByCriteria(criterionList);



                    foreach (vrmConfRoom cr in rList)
                    {


                        List<ICriterion> criterionlist = new List<ICriterion>();
                        criterionlist.Add(Expression.Eq("endpointid", cr.endpointId));
                        criterionlist.Add(Expression.Eq("deleted", 0));
                        criterionlist.Add(Expression.Eq("profileId", cr.profileId));
                        List<vrmEndPoint> epList = m_vrmEpt.GetByCriteria(criterionlist);


                        if (epList.Count > 0)
                        {
                            if (epList[0].CalendarInvite.ToString().Equals("0"))
                                continue;
                        }
                        else //FB 2027 SetConference
                            continue;

                        MailMessage app = new MailMessage();

                        iCalendar iCal = new iCalendar();
                        if (action == "C")
                            iCal.Method = "REQUEST"; // CANCEL - send meeting cancelation
                        else
                            iCal.Method = "CANCEL";

                        Event evt = iCal.Create<Event>();
                        evt.Location = "";
                        vrmState objState = null;//FB 2189 Starts
                        vrmCountry objCountry = null;
                        string roomAddress = "";
                        for (int rl = 0; rl < rList.Count; rl++)
                        {
                            rList[rl].Room.Name = rList[rl].Room.Name.Replace(",", "?"); // FB 1888
                            if (rList[rl].Room.State > 0)
                                objState = m_IStateDAO.GetById(rList[rl].Room.State);
                            if (rList[rl].Room.Country > 0)
                                objCountry = m_ICountryDAO.GetById(rList[rl].Room.Country);
                            if (rList[rl].Room.RoomFloor != null&& rList[rl].Room.RoomFloor != "") //FB 2778
                                roomAddress = rList[rl].Room.RoomFloor;
                            if (rList[rl].Room.RoomNumber != null && rList[rl].Room.RoomNumber != "") //FB 2778
                                roomAddress += "  # " + rList[rl].Room.RoomNumber;
                            if (rList[rl].Room.Address1 != null && rList[rl].Room.Address1 != "") //FB 2778
                                roomAddress += " , " + rList[rl].Room.Address1;
                            if (rList[rl].Room.Address2 != null && rList[rl].Room.Address2 != "") //FB 2778
                                roomAddress += " , " + rList[rl].Room.Address2 + "\r\n \r\n";
                            if (rList[rl].Room.City != null && rList[rl].Room.City != "") //FB 2778
                                roomAddress += " , " + rList[rl].Room.City;
                            if (objState != null)
                            {
                                if (objState.State.ToString() != "")
                                    roomAddress += " , " + objState.State.ToString();
                            }
                            if (objCountry != null)
                            {
                                if (objCountry.CountryName.ToString() != "")
                                    roomAddress += " , " + objCountry.CountryName.ToString();
                            }
                            if (rList[rl].Room.Zipcode != null && rList[rl].Room.Zipcode != "") //FB 2778
                                roomAddress += " - " + rList[rl].Room.Zipcode + "\r\n \r\n";

                            if (rl == 0)
                                evt.Location = "\r\n" + rList[rl].Room.Name + "\r\n" + roomAddress;
                            else
                                evt.Location += "\r\n , " + rList[rl].Room.Name + "\r\n \r\n" + roomAddress; // FB 1888

                            rList[rl].Room.Name = rList[rl].Room.Name.Replace("!!", "'").Replace("||", "\"");//FB 1888
                            evt.AddResource(rList[rl].Room.Name);

                            if (rList[rl].Room.IsVMR == 1) //FB 2957
                            {
                                RoomInternalNum = rList[rl].Room.Name + " > " + rList[rl].Room.InternalNumber;
                                RoomExternalNum = rList[rl].Room.Name + " > " + rList[rl].Room.ExternalNumber;
                            }
                        }
                        evt.Location = evt.Location.ToString().Replace("!!", "'").Replace("||", "\"");//FB 1888
                        location = evt.Location.ToString();
                        //FB 2957 Starts
                        string strVMR = "";
                        string VMRhtml = confHost.PrivateVMR.Replace("<br />", "\n").Replace("<p>", "").Replace("</p>", "\n").Replace("&nbsp;", " ").Replace("<div>", "").Replace("</div>", "\n");
                        strVMR = StripTagsRegex(VMRhtml);
                        strVMR = StripTagsRegexCompiled(VMRhtml);
                        strVMR = StripTagsCharArray(VMRhtml);


                        string internalBridgeNo = "", externalBridgeNo = "";
                        if (conf.isVMR == 1)
                        {
                            if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                            {
                                externalBridgeNo = confAVParam[0].externalBridge;
                            }

                            if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                            {
                                internalBridgeNo = confAVParam[0].internalBridge;
                            }
                        }
                        else if (conf.isVMR == 2)
                        {
                            externalBridgeNo = RoomExternalNum;
                            internalBridgeNo = RoomInternalNum;
                        }
                        else if (conf.isVMR == 3)
                        {
                            if (conf.CloudConferencing == 1)
                                strVMR = confHost.VidyoURL;
                        }
                        //FB 2957 Ends
                        evt.Summary = cf.externalname;
                        iCalDateTime evticaldate = new iCalDateTime(setupDate.Year, setupDate.Month, setupDate.Day, setupDate.Hour, setupDate.Minute, 00);
                        evticaldate.IsUniversalTime = true;
                        evt.Start = evticaldate;
                        evt.Duration = TimeSpan.FromHours(dur);
                        evticaldate = new iCalDateTime(teardownDate.Year, teardownDate.Month, teardownDate.Day, teardownDate.Hour, teardownDate.Minute, 00);
                        evticaldate.IsUniversalTime = true;
                        evt.End = evticaldate;
                        evt.Sequence = 1;//FB 2218
                        evt.Created = new iCalDateTime(DateTime.Now.AddMinutes(10));
                        evt.DTStamp = new iCalDateTime(DateTime.Now.AddMinutes(10));
                        if (cf.IcalID.Trim() != "")
                        {
                            evt.UID = cf.IcalID;

                        }
                        else
                            evt.UID = cf.confnumname.ToString(); //FB 2306


                        // FB 1786 - Start
                        if (orgInfo.IcalReqEmailID == null)
                            orgInfo.IcalReqEmailID = "";

                        if (orgInfo.IcalReqEmailID.Trim() == "")
                            evt.Organizer = confHost.Email;
                        else
                            evt.Organizer = orgInfo.IcalReqEmailID;
                        // FB 1786 - End

                        if (recipients.Length > 0)
                        {
                            Converter<string, Cal_Address> converter = input => input;
                            evt.Attendee = Array.ConvertAll(recipients, converter);
                        }

                        DateTime descriptionDate = cf.SetupTime;
                        timeZone.userPreferedTime(cf.timezone, ref descriptionDate);
                        timeZone.GetTimeZone(cf.timezone, ref tz);
                        TimeSpan durationDt = endDate.Subtract(conferenceDate);
                        //FB 1675 start
                        cf.description = cf.description.Replace("||", "\"").Replace("!!", "'"); // FB 1888
                        if (cf.description.Trim() == "")
                            cf.description = "N/A";

                        Password = cf.password; //FB 2051
                        if (cf.password.Trim() == "") 
                            Password = "N/A";

                        //FB 2453 Starts
                        //if (conf.isVMR == 1)
                        if (conf.isVMR > 0) //FB 2620
                        {
                            if (conf.isVMR == 3) //FB 2957
                                VMRRDetails = strVMR;
                            else
                                VMRRDetails = "\r\n Internal Bridge : " + internalBridgeNo + "\r\n External Bridge : " + externalBridgeNo;
                            //VMRRDetails = "\r\n Internal Bridge : " + emailObject.InternalBridge + "\r\n External Bridge : " + emailObject.ExternalBridge;
                        }
                        else
                        {
                            VMRRDetails = " N/A";
                        }
                        //FB 2453 Ends

                        String descrptn = cf.description + " \r\n \r\n";
                        //FB 1675 end
                        emailContent = ""; //FB 2380
                        if (action == "C")
                        {
                            if (orgInfo == null)
                                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                            FetchEmailString(orgInfo.EmailLangId, 0, eEmailType.Ical_Emails);
                            //emailContent = String.Format(emailContent, cf.externalname, cf.confnumname.ToString(), descriptionDate.ToString("MM/dd/yyyy hh:mm tt") + " " + tz.TimeZone, DateTime.Today.Add(durationDt).ToString("HH:mm:ss"), confHost.FirstName + " " + confHost.LastName, location);
							//FB 1675 start
                            String dtfrmt = "MM/dd/yyyy";
                            String tmefrmt = "hh:mm tt";
                            if (confHost != null)
                            {
                                //dtfrmt = confHost.DateFormat.Trim();
                                SetEmailDateFormat(confHost.DateFormat.Trim(), ref dtfrmt); //FB 2555
                                if(confHost.TimeFormat.Trim() == "0") //FB 1675
                                      tmefrmt = "HH:mm";
 
                            }
                            emailContent = emailContent.Replace("{4}", cf.externalname);
                            emailContent = emailContent.Replace("{5}", cf.confnumname.ToString());
                            emailContent = emailContent.Replace("{6}", descriptionDate.ToString(dtfrmt +" "+ tmefrmt ) + " " + tz.TimeZone);
                            emailContent = emailContent.Replace("{9}", DateTime.Today.Add(durationDt).ToString("HH:mm:ss"));
                            emailContent = emailContent.Replace("{10}", confHost.FirstName.Replace("||", "\"").Replace("!!", "'") + " " + confHost.LastName.Replace("||", "\"").Replace("!!", "'")); // FB 1888
                            emailContent = emailContent.Replace("{13}", location); // FB 1888
                            emailContent = emailContent.Replace("{61}", Password); // FB 2051 //FB 2439
                            emailContent = emailContent.Replace("{69}", VMRRDetails); // FB 2453

                            //descrptn += "Resource has been added to a meeting with the following information. \r\n \r\n";
                            //descrptn += "Conference Name: " + cf.externalname + " \r\n";
                            //descrptn += "Unique ID: " + cf.confnumname.ToString() + " \r\n";
                            //descrptn += "Date and Time: " + descriptionDate.ToString("MM/dd/yyyy hh:mm tt") + " " + tz.TimeZone + "\r\n";
                            //descrptn += "Duration: " + DateTime.Today.Add(durationDt).ToString("HH:mm:ss") + "\r\n";
                            //descrptn += "Host: " + confHost.FirstName + " " + confHost.LastName + "(mailto:" + confHost.Email + ") \r\n";
                            //descrptn += "Location(s): " + location;
                            evt.Description = descrptn + emailContent;
							//FB 1675 end
                        }
                        iCalendarSerializer serializer = new iCalendarSerializer(iCal);
                        ContentType calType = new ContentType("text/calendar");

                        if (action == "C")
                            calType.Parameters.Add("method", "REQUEST");
                        else
                            calType.Parameters.Add("method", "CANCEL");

                        calType.Parameters.Add("name", "meeting.ics");


                        AlternateView calendarView = AlternateView.CreateAlternateViewFromString(serializer.SerializeToString(), calType);
                        calendarView.TransferEncoding = TransferEncoding.SevenBit;

                        app.AlternateViews.Add(calendarView);
                        app.Body = serializer.SerializeToString();
                        app.Subject = cf.externalname;

                        // FB 1786 - Start
                        if (orgInfo.IcalReqEmailID.Trim() == "")
                            app.From = new MailAddress(confHost.Email);
                        else
                            app.From = new MailAddress(orgInfo.IcalReqEmailID);
                        // FB 1786 - End

                        app.To.Add(epList[0].ExchangeID);

                        lstMailmsg.Add(app);

                    }

                }

                foreach (object obj in lstMailmsg)
                {
                    MailMessage msg = (MailMessage)obj;

                    vrmEmail theEmail = new vrmEmail();
                    theEmail.emailTo = msg.To[0].Address;
                    theEmail.emailFrom = msg.From.Address;
                    //msg.Body = msg.Body.Replace("?", ",");
                    theEmail.Message = msg.Body;
                    theEmail.Subject = msg.Subject;
                    theEmail.UUID = getEmailUUID();
                    theEmail.Iscalendar = 1;
                    theEmail.orgID = confList[0].orgId;
                    m_IemailDao.Save(theEmail);

                }
            }
            catch (Exception ex)
            {
                m_log.Error("Cannot send ICAL to Cisco Endpoints", ex);
                m_log.Error("sytemException", ex);
                throw ex;
            }
            return true;
        }
        #endregion

        #region GetDayName for Int
        private string GetDayName(String day)
        {
            String dayName = "";
            if (day == "1")
                dayName = "Sunday";
            else if (day == "2")
                dayName = "Monday";
            else if (day == "3")
                dayName = "TuesDay";
            else if (day == "4")
                dayName = "WednesDay";
            else if (day == "5")
                dayName = "ThursDay";
            else if (day == "6")
                dayName = "Friday";
            else if (day == "7")
                dayName = "Saturday";

            return dayName;
        }
        #endregion

        //Create ICAL for Cisco Endpoints -- End

        #region FB 1642 Audio Add On - starts

        #region MethhodToSpiltAddress
        /// <summary>
        /// MethodToSplitAddress
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private string[] SpiltAddress(String address)
        {

            String[] add = null;
            try
            {
                add = new String[3];
                if (address.Contains("D") && address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    if (address.IndexOf('+') > 0)
                    {
                        add[1] = address.Split('D')[1].Split('+')[0].Trim(); //Conference Code
                        add[2] = address.Split('D')[1].Split('+')[1].Trim(); // Leader Pin
                    }
                    else
                    {
                        add[1] = address.Split('D')[1].Trim(); //Conference Code
                        add[2] = ""; // Leader Pin
                    }
                }
                else if (address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address.Split('D')[0].Trim();//Address
                    add[1] = address.Split('D')[1].Trim(); //Conference Code
                    add[2] = ""; // Leader Pin
                }
                else if (!address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address;//Address
                    add[1] = ""; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                return add;

            }
            catch (Exception ex)
            {
                m_log.Error("Error in FetchAudioDialString:", ex);
                return null;
            }
        }

        #endregion

        #region FetchAudioDialString
        private string FetchAudioDialString(Int32 confID, Int32 instanceID, string mailType, Int32 partyid)
        {
            String[] dialString = null;
            string dialno = "", confcode = "", leaderpin = "";
            string audioString = "";
            string detailString = "";
            try
            {
                if (confUserLst == null) //FB 1830
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", confID));
                    criterionList.Add(Expression.Eq("instanceid", instanceID));
                    //criterionList.Add(Expression.Eq("invitee", 1)); //External Participants
                    confUserLst = m_vrmConfUserDAO.GetByCriteria(criterionList);
                }

                if (confUserLst != null)
                {
                    if (confUserLst.Count > 0)
                    {
                        if (partyid == 0)
                            dialString = new String[confUserLst.Count];
                        else
                            dialString = new String[1];

                        for (int count=0; count < confUserLst.Count; count++)
                        {
                            if (confUserLst[count].invitee == 1) //External Participant //FB 1830
                            {
                                if (partyid == 0 && confUserLst[count].audioOrVideo == 1 && confUserLst[count].invitee == 1) //Code Changed For FB 1744
                                    dialString[count] = confUserLst[count].ipisdnaddress;
                                else if (confUserLst[count].userid == partyid)
                                {
                                    dialString[0] = confUserLst[count].ipisdnaddress;
                                    break;
                                }
                            }
                        }

                        if (dialString != null)
                        {
                            foreach (string singledial in dialString)
                            {
                                String[] spiltAdd = SpiltAddress(singledial);

                                if (spiltAdd != null)
                                {
                                    if (spiltAdd.Length > 2)
                                    {
                                        dialno = spiltAdd[0];
                                        confcode = spiltAdd[1];
                                        leaderpin = spiltAdd[2];
                                    }


                                    if (dialno == "")
                                        dialno = "N/A";
                                    if (confcode == "")
                                        confcode = "N/A";
                                    if (leaderpin == "")
                                        leaderpin = "N/A";
                                    detailString += "<tr>";
                                    detailString += "<td><b>Audio Dial In #:</b> " + dialno.Trim() + "</td>";
                                    detailString += "<td><b>Conference Code:</b> " + confcode.Trim() + "</td>";

                                    if (mailType.ToUpper().Trim() == "M") //FB 2381
                                    {
                                        detailString += "<td><b>Leader Pin:</b> " + leaderpin.Trim() + "</td>";
                                    }
                                    detailString += "</tr>";
                                    dialno = "";
                                    confcode = "";
                                    leaderpin = "";

                                }

                            }
                        }
                        if (detailString == "")
                            detailString = "<tr><td><b>N/A</b></td></tr>";
                        audioString = "<td align=\"right\" style=\"width: 20%;\">";

                        //FB 2439 start
                        if(mailType == "P")
                            audioString += " Personal Audio Information:</td>";
                        else
                        audioString += " Audio Information:</td>";
                        //FB 2439 end

                        audioString += "<td style=\"padding:0px; \">";
                        audioString += "<table width=\"100%\" style=\"font-size: 10pt; font-family: Verdana;\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
                        audioString += detailString + "</table></td>";
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("Error in FetchAudioDialString:", e);
            }
            return audioString;
        }
        #endregion

        #endregion

        //FB 2439 Starts         

        #region FetchAudioBridgeDialString

        private void FetchAudioBridgeDialString(Int32 confID, Int32 instanceID)
        {
            List<string> dialString = new List<string>();
            string dialno = "", confcode = "", leaderpin = "";
            List<ICriterion> criterionAudiouserList = new List<ICriterion>();
            List<vrmUser> AudiouserList;
            List<int> parties = new List<int>();
            try
            {
                //hostAudioBridgeDt = mcuAudioBridgeDt = "";
                partyAudioBridgeDt = "";
                if (confUserLst == null) //FB 1830
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", confID));
                    criterionList.Add(Expression.Eq("instanceid", instanceID));
                    //criterionList.Add(Expression.Eq("invitee", 1)); //External Participants
                    confUserLst = m_vrmConfUserDAO.GetByCriteria(criterionList);
                }
                if (confUserLst != null)
                {
                    if (confUserLst.Count > 0)
                    {
                        for (int count = 0; count < confUserLst.Count; count++)
                        {
                            if(!parties.Contains(confUserLst[count].userid))
                                parties.Add(confUserLst[count].userid);
                        }

                        criterionAudiouserList = new List<ICriterion>();
                        criterionAudiouserList.Add(Expression.Eq("Audioaddon", "1"));
                        criterionAudiouserList.Add(Expression.In("userid", parties));
                        AudiouserList = m_IuserDao.GetByCriteria(criterionAudiouserList);

                        if (AudiouserList!= null)
                        {
                            if (AudiouserList.Count >0)
                            {
                                for (int usrCnt = 0; usrCnt < confUserLst.Count; usrCnt++)
                                {
                                    for (int bcount = 0; bcount < AudiouserList.Count; bcount++)
                                    {
                                        if (confUserLst[usrCnt].userid == AudiouserList[bcount].userid)
                                        {
                                            dialString.Add(confUserLst[usrCnt].ipisdnaddress);
                                            break;
                                        }
                                    }
                                    if (dialString.Count == AudiouserList.Count)
                                        break;
                                }
                            }
                        }

                        if (dialString != null)
                        {
                            for (int k = 0; k < dialString.Count; k++)
                            {
                                String[] spiltAdd = SpiltAddress(dialString[k]);

                                if (spiltAdd != null)
                                {
                                    if (spiltAdd.Length > 2)
                                    {
                                        dialno = spiltAdd[0];
                                        confcode = spiltAdd[1];
                                        leaderpin = spiltAdd[2];
                                    }

                                    if (dialno == "")
                                        dialno = "N/A";
                                    if (confcode == "")
                                        confcode = "N/A";
                                    if (leaderpin == "")
                                        leaderpin = "N/A";

                                    //hostAudioBridgeDt += "<tr>" +
                                    //"<td><b>Audio Dial In #:</b> " + dialno.Trim() + "</td>" +
                                    //"<td><b>Conference Code:</b> " + confcode.Trim() + "</td></tr>";

                                    partyAudioBridgeDt += "<tr>" +
                                    "<td><b>Audio Dial In #:</b> " + dialno.Trim() + "</td>" +
                                    "<td><b>Conference Code:</b> " + confcode.Trim() + "</td></tr>";

                                    //mcuAudioBridgeDt += "<tr>" +
                                    //"<td><b>Audio Dial In #:</b> " + dialno.Trim() + "</td>" +
                                    //"<td><b>Conference Code:</b> " + confcode.Trim() + "</td>"+
                                    //"<td><b>Leader Pin:</b> " + leaderpin.Trim() + "</td></tr>";

                                    dialno = "";
                                    confcode = "";
                                    leaderpin = "";
                                }
                            }
                        }
                        if (partyAudioBridgeDt == "")
                        {
                            //hostAudioBridgeDt = "<tr><td><b>N/A</b></td></tr>";
                            partyAudioBridgeDt = "<tr><td><b>N/A</b></td></tr>";
                            //mcuAudioBridgeDt = "<tr><td><b>N/A</b></td></tr>";
                        }
                        //hostAudioBridgeDt = "<tr>"
                        //+"<td align=\"right\" style=\"width: 20%;\">"
                        //+" <b>Audio Bridge Information:</b></td>"
                        //+"<td style=\"padding:0px; \">"
                        //+"<table width=\"100%\" style=\"font-size: 10pt; font-family: Verdana;\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">"
                        //+ hostAudioBridgeDt + "</table></td></tr>";
                                                
                        partyAudioBridgeDt = "<td align=\"right\" style=\"width: 20%;\">"
                        + " Audio Information:</td>"
                        + "<td style=\"padding:0px; \">"
                        + "<table width=\"100%\" style=\"font-size: 10pt; font-family: Verdana;\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">"
                        + partyAudioBridgeDt + "</table></td>";

                        //mcuAudioBridgeDt = "<tr>"
                        //+ "<td align=\"right\" style=\"width: 20%;\">"
                        //+ " <b>Audio Bridge Information:</b></td>"
                        //+ "<td style=\"padding:0px; \">"
                        //+ "<table width=\"100%\" style=\"font-size: 10pt; font-family: Verdana;\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">"
                        //+ mcuAudioBridgeDt + "</table></td></tr>";
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("Error in FetchAudioBridgeDialString:", e);
            }
        }
        #endregion

        //FB 2439 Ends
        
        //FB 1782 start
        #region send ICAL for Participants
        public bool sendICALforParticipants(List<vrmConference> confList, String action, bool ispending, bool sendIcal, bool isreccuring)
        {
            IList conferences = new ArrayList();
            try
            {
                List<ICriterion> ConfcriterionList = new List<ICriterion>();
                ConfcriterionList.Add(Expression.Eq("confid", confList[0].confid));
                ConfcriterionList.Add(Expression.Eq("instanceid", confList[0].instanceid));


                List<ICriterion> recurInfo = new List<ICriterion>();
                recurInfo.Add(Expression.Eq("confid", confList[0].confid));
                List<vrmRecurInfo> recurPattern = m_vrmConfRecurDAO.GetByCriteria(recurInfo, true);
                List<vrmRecurInfoDefunct> recurDefunctPattern = m_confDAO.GetConfRecurDefunctDAO().GetByCriteria(recurInfo, true);//FB 2218
                if (recurPattern.Count > 0)
                {
                    if (recurPattern[0].RecurringPattern != null || confList[0].IcalID != "")
                    {
                        if (recurPattern[0].recurType == 5 && isreccuring && recurDefunctPattern.Count <= 0)//FB 2218
                        {
                            ConfcriterionList = null;
                            ConfcriterionList = new List<ICriterion>();
                            ConfcriterionList.Add(Expression.Eq("confid", confList[0].confid));

                        }
                    }
                }
                conferences = m_vrmConfDAO.GetByCriteria(ConfcriterionList);

                //2075
                //foreach (vrmConference conf in conferences)
                //{

                //    if (!sendParticipantIcal(conf, action, ispending, isreccuring,sendIcal))
                //        m_log.Error("Cannot send email " + conf.userid.ToString());
                //}

                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                vrmConference confObj = null;
                for (int k = 0; k < conferences.Count; k++)
                {
                    confObj = (vrmConference)conferences[k];

                    if (confObj.confdate < serverTime)
                        continue;

                    conf = confObj;

                    organizationID = conf.orgId; //FB 2879
                    if (organizationID < 11)
                        organizationID = defaultOrgId;

                    if (!sendParticipantIcal(conf, action, ispending, isreccuring, sendIcal, recurPattern, recurDefunctPattern))//FB 2218
                        m_log.Error("Cannot send email " + conf.userid.ToString());
                }
                //conf = confList[0];     
                //2075

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("Cannot send ICAL to Cisco Endpoints", ex);
                m_log.Error("sytemException", ex);
                throw ex;
            }
        }
        #endregion

        //FB 2218

        #region  sendParticipantIcal
        /// <summary>
        /// This method has been overhauled for //FB 2218
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="action"></param>
        /// <param name="ispending"></param>
        /// <param name="isreccuring"></param>
        /// <param name="sendIcal"></param>
        /// <param name="recurPattern"></param>
        /// <param name="defunctRecur"></param>
        /// <returns></returns>
        private Boolean sendParticipantIcal(vrmConference conf, String action, bool ispending, bool isreccuring, bool sendIcal, List<vrmRecurInfo> recurPattern, List<vrmRecurInfoDefunct> defunctRecur)
        {
            String lctn = "";
            List<RecurrenceDates> deletedIns = null;
            List<vrmConference> changedIns = null;
            int errNo = 0;
            Event chngEvt = null;
            List<iCalendar> chngIcals = null;
            iCalendar chngIcal = null;
            DateTime chndSetup = DateTime.Now;
            DateTime chndtrDwn = DateTime.Now;
            iCalendar serIcal = null;
            int seqNumber = 2;

            
            try
            {
                if (action == "C")
                {
                    if (conf.deleted == 1)
                        return false;
                }
                List<vrmConfRoom> rList = new List<vrmConfRoom>();

                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);
                string from = confHost.Email;
                List<ICriterion> criterionList = new List<ICriterion>();

                List<vrmUser> userList;

                bool hostEmail = false;
                List<vrmConfUser> uList = new List<vrmConfUser>();

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));

                uList = m_vrmConfUserDAO.GetByCriteria(criterionList);

                //FB 2453 Starts
                emailObject.ExternalBridge = "N/A";
                emailObject.InternalBridge = "N/A";

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionList);//FB 2636

                //FB 2957
                //if (conf.isVMR == 1) 
                /*if (conf.isVMR > 0) //FB 2620 
                {
                    if (confAVParam[0].externalBridge != null)
                    {
                        if (confAVParam[0].externalBridge != "")
                            emailObject.ExternalBridge = confAVParam[0].externalBridge;
                    }

                    if (confAVParam[0].internalBridge != null)
                    {
                        if (confAVParam[0].internalBridge != "")
                            emailObject.InternalBridge = confAVParam[0].internalBridge;
                    }
                }*/
                //FB 2453 Ends
                String required = "";
                for (int u = 0; u < uList.Count; u++)
                {
                    if (conf.ConfMode == (int)eEmailMode.CONF_EDIT && uList[u].NotifyOnEdit == 0) //FB 2120 Case 3- if party notify 0 in Editmode
                        continue;

                    if (uList[u].userid == confHost.userid)
                        hostEmail = true;

                    String reqemails = "";
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", uList[u].userid));
                    userList = m_IuserDao.GetByCriteria(criterionList);
                    if (userList.Count == 0)
                    {
                        List<vrmGuestUser> guestList = m_IguestUserDao.GetByCriteria(criterionList);
                        if (guestList.Count > 0)
                        {

                            if (guestList[0].emailmask == 1 && (uList[u].partyNotify != 0 || sendIcal))
                            {
                                if (guestList.Count > 0)
                                    reqemails = guestList[0].Email;
                            }
                        }
                    }
                    else
                    {

                        if (userList[0].emailmask == 1 && (uList[u].partyNotify != 0 || sendIcal) && userList[0].Audioaddon.Trim() != "1") //FB 2023
                        {
                            reqemails = userList[0].Email;
                        }

                    }
                    if (reqemails != "")
                    {

                        if (required == "")
                            required = reqemails;
                        else
                            required += ";" + reqemails;
                    }

                }

                if (!hostEmail)
                {
                    if (required == "")
                        required = confHost.Email;
                    else
                        required += ";" + confHost.Email; ;
                }


                String[] recipients = required.Split(';');

                string ownerid = confHost.userid.ToString();

                DateTime conferenceDate = conf.confdate;
                DateTime setupDate = conf.SetupTime; 
                DateTime teardownDate = conf.TearDownTime;
                Double dur = Convert.ToDouble(conf.duration);
                DateTime endDate = conferenceDate.AddMinutes(dur);
                TimeSpan partyDur = teardownDate.Subtract(setupDate);//FB 2092


                MailMessage app = new MailMessage();

                iCalendar iCal = new iCalendar();
                if (action == "C")
                    iCal.Method = "REQUEST";
                else
                    iCal.Method = "CANCEL";

                Event evt = iCal.Create<Event>();
                evt.Summary = conf.externalname;

                if (ispending)
                    evt.Summary = " [Pending Approval] " + conf.externalname;
                iCalDateTime evticaldate = new iCalDateTime(setupDate.Year, setupDate.Month, setupDate.Day, setupDate.Hour, setupDate.Minute, 00);
                evticaldate.IsUniversalTime = true;
                evt.Start = evticaldate;
                evt.Duration = TimeSpan.FromHours(partyDur.Minutes);//FB 2092
                evticaldate = new iCalDateTime(teardownDate.Year, teardownDate.Month, teardownDate.Day, teardownDate.Hour, teardownDate.Minute, 00);
                evticaldate.IsUniversalTime = true;
                evt.End = evticaldate;
                evt.Organizer = confHost.Email;
                evt.Sequence = 1;
                evt.Created = new iCalDateTime(DateTime.Now.AddMinutes(10));
                evt.DTStamp = new iCalDateTime(DateTime.Now.AddMinutes(10));
                if (conf.IcalID.Trim() != "")
                {
                    evt.UID = conf.IcalID;

                }
                else
                    evt.UID = conf.confnumname.ToString(); //FB 2306                
                
                vrmState objState = null;//FB 2189
                vrmCountry objCountry = null;
                string roomAddress = "";
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                rList = m_IconfRoom.GetByCriteria(criterionList);
                evt.Location = "";
                for (int rl = 0; rl < rList.Count; rl++)
                {
                    objState = null; //FB 2448 start
                    objCountry = null;
                    roomAddress = ""; //FB 2448 end

                    rList[rl].Room.Name = rList[rl].Room.Name.Replace(",", "?"); // FB 1888
                    if (rList[rl].Room.State > 0)
                        objState = m_IStateDAO.GetById(rList[rl].Room.State);
                    if (rList[rl].Room.Country > 0)
                        objCountry = m_ICountryDAO.GetById(rList[rl].Room.Country);
                    if (rList[rl].Room.RoomFloor != null && rList[rl].Room.RoomFloor != "") //FB 2778
                        roomAddress = rList[rl].Room.RoomFloor;
                    if (rList[rl].Room.RoomNumber != null && rList[rl].Room.RoomNumber != "") //FB 2778
                        roomAddress += "  # " + rList[rl].Room.RoomNumber;
                    if (rList[rl].Room.Address1 != null && rList[rl].Room.Address1 != "") //FB 2778
                        roomAddress += " , " + rList[rl].Room.Address1;
                    if (rList[rl].Room.Address2 != null && rList[rl].Room.Address2 != "") //FB 2778
                        roomAddress += " , " + rList[rl].Room.Address2 + "\r\n \r\n";
                    if (rList[rl].Room.City != null && rList[rl].Room.City != "") //FB 2778
                        roomAddress += " , " + rList[rl].Room.City;
                    if (objState != null)
                    {
                        if (objState.State.ToString() != "")
                            roomAddress += " , " + objState.State.ToString();
                    }
                    if (objCountry != null)
                    {
                        if (objCountry.CountryName.ToString() != "")
                            roomAddress += " , " + objCountry.CountryName.ToString();
                    }
                    if (rList[rl].Room.Zipcode != null && rList[rl].Room.Zipcode != "") //FB 2778
                        roomAddress += " - " + rList[rl].Room.Zipcode + "\r\n \r\n";

                    if (rl == 0)
                    {
                        evt.Location = rList[rl].Room.Name; //FB 2333
                        lctn = rList[rl].Room.Name + "\r\n" + roomAddress;
                    }
                    else
                    {
                        evt.Location += " , " + rList[rl].Room.Name; //FB 2333 Room Dipaly Issue in ICAL
                        lctn += rList[rl].Room.Name + "\r\n" + roomAddress; // FB 1888
                    }
                  
                    rList[rl].Room.Name = rList[rl].Room.Name.Replace("||", "\"").Replace("!!", "'"); // FB 1888
                    evt.AddResource(rList[rl].Room.Name);

                    if (rList[rl].Room.IsVMR == 1) //FB 2690
                    {
                        RoomInternalNum = rList[rl].Room.Name + " > " + rList[rl].Room.InternalNumber;
                        RoomExternalNum = rList[rl].Room.Name + " > " + rList[rl].Room.ExternalNumber;
                    }
                }
                evt.Location = evt.Location.ToString().Replace("||", "\"").Replace("!!", "'"); // FB 1888
                lctn = lctn.Replace("||", "\"").Replace("!!", "'"); //FB 2333
                //FB 2957 Starts
                string strVMR = "";
                string VMRhtml = confHost.PrivateVMR.Replace("<br />", "\n").Replace("<p>", "").Replace("</p>", "\n").Replace("&nbsp;", " ").Replace("<div>", "").Replace("</div>", "\n");
                strVMR = StripTagsRegex(VMRhtml);
                strVMR = StripTagsRegexCompiled(VMRhtml);
                strVMR = StripTagsCharArray(VMRhtml);
                
                
                string internalBridgeNo = "", externalBridgeNo = "";
                if (conf.isVMR == 1)
                {
                    if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                    {
                        externalBridgeNo = confAVParam[0].externalBridge;
                    }

                    if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                    {
                        internalBridgeNo = confAVParam[0].internalBridge;
                    }
                }
                else if (conf.isVMR == 2)
                {
                    externalBridgeNo = RoomExternalNum;
                    internalBridgeNo = RoomInternalNum;
                }
                else if (conf.isVMR == 3)
                {
                    if (conf.CloudConferencing == 1)
                        strVMR = confHost.VidyoURL;
                }
                //FB 2957 Ends

                if (isreccuring)
                {
                    

                    if (recurPattern.Count > 0)
                    {
                        if (defunctRecur.Count > 0) 
                        {
                            recurPattern[0] = MapRecurProperties(defunctRecur[0]);


                            if (!CustomRecurringDates(ref conf, ref errNo, ref deletedIns, ref changedIns, true, ref defunctRecur,ref setupDate,ref teardownDate,ref partyDur))
                                return false;
                            else
                            {
                                evticaldate = new iCalDateTime(setupDate.Year, setupDate.Month, setupDate.Day, setupDate.Hour, setupDate.Minute, 00);
                                evticaldate.IsUniversalTime = true;
                                evt.Start = evticaldate;
                                evt.Duration = TimeSpan.FromHours(partyDur.Minutes);//FB 2092
                                evticaldate = new iCalDateTime(teardownDate.Year, teardownDate.Month, teardownDate.Day, teardownDate.Hour, teardownDate.Minute, 00);
                                evticaldate.IsUniversalTime = true;
                                evt.End = evticaldate;
                            }


                        }


                        if (recurPattern[0].RecurringPattern != "" || conf.IcalID != "")
                        {
                            //vrmRecurInfo Ri = conf.ConfRecurInfo;
                            // rp.Frequency = FrequencyType.Monthly;
                            // rp.ByDay.Add(new DaySpecifier(DayOfWeek.Monday, FrequencyOccurrence.First));
                            // rp.ByDay.Add(new DaySpecifier(DayOfWeek.Monday, FrequencyOccurrence.SecondToLast));
                            //// evt.AddRecurrencePattern(rp);
                            // 1: Daily, 2: Weekly, 3: Monthly, 4: Yearly
                            RecurrencePattern rp = new RecurrencePattern();
                            System.Globalization.DateTimeFormatInfo timeFormat = new System.Globalization.DateTimeFormatInfo();

                            switch (recurPattern[0].recurType)
                            {
                                case 1:
                                    rp.Frequency = FrequencyType.Daily;
                                    if (recurPattern[0].gap >= 0)
                                        rp.Interval = recurPattern[0].gap;

                                    if (recurPattern[0].subType == 1)
                                        rp.Count = recurPattern[0].occurrence;
                                    if (recurPattern[0].subType == 2)
                                    {
                                        if (recurPattern[0].endTime >= DateTime.Now)
                                        {

                                            evticaldate = new iCalDateTime(recurPattern[0].endTime.Year, recurPattern[0].endTime.Month, recurPattern[0].endTime.Day, recurPattern[0].endTime.Hour, recurPattern[0].endTime.Minute, recurPattern[0].endTime.Second);
                                            evticaldate.IsUniversalTime = true;
                                            rp.Until = evticaldate;
                                        }
                                        else
                                            rp.Count = recurPattern[0].occurrence;
                                    }
                                    break;
                                case 2:
                                    rp.Frequency = FrequencyType.Weekly;
                                    if (recurPattern[0].gap >= 0)
                                        rp.Interval = recurPattern[0].gap;

                                    if (recurPattern[0].endTime >= DateTime.Now)
                                    {
                                        evticaldate = new iCalDateTime(recurPattern[0].endTime.Year, recurPattern[0].endTime.Month, recurPattern[0].endTime.Day, recurPattern[0].endTime.Hour, recurPattern[0].endTime.Minute, recurPattern[0].endTime.Second);
                                        evticaldate.IsUniversalTime = true;
                                        rp.Until = evticaldate;
                                    }
                                    else
                                        rp.Count = recurPattern[0].occurrence;

                                    String[] day = recurPattern[0].days.Split(',');
                                    if (day.Length > 0)
                                    {
                                        for (int i = 0; i < day.Length; i++)
                                        {
                                            String dayD = GetDayName(day[i].ToString());
                                            rp.ByDay.Add(new DaySpecifier(dayD));

                                        }
                                    }
                                    else
                                    {
                                        String dayd = GetDayName(recurPattern[0].days.ToString());
                                        rp.ByDay.Add(new DaySpecifier(dayd));
                                    }

                                    break;

                                case 3:
                                    rp.Frequency = FrequencyType.Monthly;
                                    switch (recurPattern[0].subType)
                                    {
                                        case 1:
                                            rp.ByMonthDay.Add(recurPattern[0].dayno);
                                            if (recurPattern[0].gap >= 0)
                                                rp.Interval = recurPattern[0].gap;
                                            rp.Count = recurPattern[0].occurrence;
                                            break;
                                        case 2:
                                            // rp.ByDay.Add(new DaySpecifier(GetDayName(recurPattern[0].dayno.ToString()));
                                            day = recurPattern[0].days.Split(',');
                                            if (recurPattern[0].gap >= 0)
                                                rp.Interval = recurPattern[0].gap;
                                            if (recurPattern[0].endTime >= DateTime.Now)
                                            {
                                                evticaldate = new iCalDateTime(recurPattern[0].endTime.Year, recurPattern[0].endTime.Month, recurPattern[0].endTime.Day, recurPattern[0].endTime.Hour, recurPattern[0].endTime.Minute, recurPattern[0].endTime.Second);
                                                evticaldate.IsUniversalTime = true;
                                                rp.Until = evticaldate;

                                            }
                                            else
                                                rp.Count = recurPattern[0].occurrence;
                                            break;
                                    }
                                    break;
                                case 4:

                                    rp.Frequency = FrequencyType.Yearly;
                                    // 1: expressed in day #, 2: expressed in weekday#
                                    switch (recurPattern[0].subType)
                                    {
                                        case 1:
                                            rp.ByMonth.Add(recurPattern[0].yearMonth);
                                            rp.Count = recurPattern[0].occurrence;
                                            day = recurPattern[0].days.Split(',');
                                            if (day.Length > 0)
                                            {
                                                for (int i = 0; i < day.Length; i++)
                                                {
                                                    rp.ByMonthDay.Add(Convert.ToInt32(day[i]));
                                                }
                                            }
                                            else
                                            {
                                                rp.ByMonthDay.Add(Convert.ToInt32(recurPattern[0].days));
                                            }

                                            if (recurPattern[0].gap >= 0)
                                                rp.Interval = recurPattern[0].gap;
                                            break;
                                        case 2:

                                            rp.ByMonth.Add(recurPattern[0].yearMonth);
                                            //String dayD = GetDayName(recurPattern[0].dayno.ToString());
                                            rp.ByMonthDay.Add(recurPattern[0].dayno);
                                            if (recurPattern[0].gap >= 0)
                                                rp.Interval = recurPattern[0].gap;
                                            if (recurPattern[0].endTime >= DateTime.Now)
                                            {
                                                evticaldate = new iCalDateTime(recurPattern[0].endTime.Year, recurPattern[0].endTime.Month, recurPattern[0].endTime.Day, recurPattern[0].endTime.Hour, recurPattern[0].endTime.Minute, recurPattern[0].endTime.Second);
                                                evticaldate.IsUniversalTime = true;
                                                rp.Until = evticaldate;
                                            }
                                            else
                                                rp.Count = recurPattern[0].occurrence;

                                            break;
                                    }
                                    break;

                            }
                            if (recurPattern[0].recurType != 5)
                                evt.AddRecurrencePattern(rp);
                        }
                    }
                }
                else
                {

                    if (conf.recuring == 1)
                    {
                        evticaldate = new iCalDateTime(setupDate.Year, setupDate.Month, setupDate.Day, setupDate.Hour, setupDate.Minute, 00);
                        evticaldate.IsUniversalTime = true;
                        evt.Recurrence_ID = evticaldate;
                    }
                }

                Converter<string, Cal_Address> converter = input => input;
                evt.Attendee = Array.ConvertAll(recipients, converter);

                DateTime descriptionDate = conf.SetupTime;
                timeZone.userPreferedTime(conf.timezone, ref descriptionDate);
                timeZoneData tz = new timeZoneData();
                timeZone.GetTimeZone(conf.timezone, ref tz);                
                TimeSpan durationDt = endDate.Subtract(conferenceDate);
                conf.description = conf.description.Replace("||", "\"").Replace("!!","'"); // FB 1888
                if (conf.description.Trim() == "") //FB 1675
                    conf.description = "";//ZD 100193 

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "")
                    Password = "N/A";

                //FB 2453 Starts
                //if (conf.isVMR == 1)
                if (conf.isVMR > 0) //FB 2620
                {
                    if (conf.isVMR == 3) //FB 2957
                        VMRRDetails = strVMR;
                    else
                        VMRRDetails = "\r\n Internal Bridge : " + internalBridgeNo + "\r\n External Bridge : " + externalBridgeNo;
                    //VMRRDetails = "\r\n Internal Bridge : " + emailObject.InternalBridge + "\r\n External Bridge : " + emailObject.ExternalBridge;
                }
                else
                {
                    VMRRDetails = " N/A";
                }
                //FB 2453 Ends

                String descrptn = conf.description + "\r\n \r\n";
                emailContent = ""; //FB 2380
                if (action == "C")
                {
                     if (orgInfo == null)
                            orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                     //FB 2659 - Starts	
                     if (sysSettings.EnableCloudInstallation <= 0)
                         FetchEmailString(orgInfo.EmailLangId, 0, eEmailType.Ical_Emails);
                     else
                     {
                         emailContent = orgInfo.DefaultInvitation;
                         emailSubject = orgInfo.DefaultSubject;
                         emailObject = new sConferenceEmail();
                         FetchDesktopURLandDialNumber();
                         
                     }
					 //FB 2659 End
					 //FB 1675 start
                     String dtfrmt = "MM/dd/yyyy";
                     String tmefrmt = "hh:mm tt";
                     if (confHost != null)
                     {
                         //dtfrmt = confHost.DateFormat.Trim();
                         SetEmailDateFormat(confHost.DateFormat.Trim(), ref dtfrmt); //FB 2555
                         if (confHost.TimeFormat.Trim() == "0") //FB 1675
                             tmefrmt = "HH:mm";

                     }
                     
                     emailContent = emailContent.Replace("{4}", conf.externalname);
                     emailContent = emailContent.Replace("{5}", conf.confnumname.ToString());
                     emailContent = emailContent.Replace("{6}", descriptionDate.ToString(dtfrmt+" "+tmefrmt) + " " + tz.TimeZone);
                     emailContent = emailContent.Replace("{9}", DateTime.Today.Add(durationDt).ToString("HH:mm:ss"));
                     emailContent = emailContent.Replace("{10}", confHost.FirstName.Replace("||", "\"").Replace("!!", "'") + " " + confHost.LastName.Replace("||", "\"").Replace("!!", "'"));
                     emailContent = emailContent.Replace("{13}", lctn);
                     emailContent = emailContent.Replace("{61}", Password); //FB 2051 
                     emailContent = emailContent.Replace("{69}", VMRRDetails); //FB 2453
					 //FB 2659 - Starts
                     if (sysSettings.EnableCloudInstallation > 0)
                     {
                         emailContent = emailContent.Replace("{71}", emailObject.ConferenceDialinNumber);
                         emailContent = emailContent.Replace("{72}", emailObject.DesktopLink); //FB 2879
                     }
					 //FB 2659 End
                    //descrptn += "You have been invited to a meeting with the following information. \r\n \r\n";
                    //descrptn += "Conference Name: " + conf.externalname + " \r\n";
                    //descrptn += "Unique ID: " + conf.confnumname.ToString() + " \r\n";
                    //descrptn += "Date and Time: " + descriptionDate.ToString("MM/dd/yyyy hh:mm tt") + " " + tz.TimeZone + "\r\n";
                    //descrptn += "Duration: " + DateTime.Today.Add(durationDt).ToString("HH:mm:ss") + "\r\n";
                    //descrptn += "Host: " + confHost.FirstName + " " + confHost.LastName + "\r\n";
                    //descrptn += "Location(s): " + lctn;
                }
               
                evt.Description = descrptn + emailContent;
				//FB 1675 end

                if (deletedIns != null && deletedIns.Count > 0)
                    evt.ExDate = deletedIns.ToArray();

                

                if (changedIns != null && changedIns.Count > 0)
                {
                    for (int rcnt = 0; rcnt < changedIns.Count; rcnt++)
                    {

                        chngEvt = iCal.Create<Event>();
                        chngEvt.Summary = evt.Summary;
                        chngEvt.UID = evt.UID;
                        chngEvt.Sequence = seqNumber;
                        evticaldate = new iCalDateTime(changedIns[rcnt].settingtime.Year, changedIns[rcnt].settingtime.Month, changedIns[rcnt].settingtime.Day, changedIns[rcnt].settingtime.Hour, changedIns[rcnt].settingtime.Minute, 00);
                        evticaldate.IsUniversalTime = true;
                        chngEvt.Recurrence_ID = evticaldate;

                        evticaldate = new iCalDateTime(changedIns[rcnt].SetupTime.Year, changedIns[rcnt].SetupTime.Month, changedIns[rcnt].SetupTime.Day, changedIns[rcnt].SetupTime.Hour, changedIns[rcnt].SetupTime.Minute, 00);
                        evticaldate.IsUniversalTime = true;
                        chngEvt.Start = evticaldate;
                        chngEvt.Duration = TimeSpan.FromHours(Convert.ToDouble(changedIns[rcnt].duration));
                        evticaldate = new iCalDateTime(changedIns[rcnt].TearDownTime.Year, changedIns[rcnt].TearDownTime.Month, changedIns[rcnt].TearDownTime.Day, changedIns[rcnt].TearDownTime.Hour, changedIns[rcnt].TearDownTime.Minute, 00);
                        evticaldate.IsUniversalTime = true;
                        chngEvt.End = evticaldate;


                        chngEvt.Organizer = confHost.Email;
                        chngEvt.Created = new iCalDateTime(DateTime.Now.AddMinutes(10));
                        chngEvt.DTStamp = new iCalDateTime(DateTime.Now.AddMinutes(10));
                        chngEvt.Attendee = evt.Attendee;
                        chngEvt.Description = evt.Description;
                        seqNumber = seqNumber + 1;

                    }

                }

                if (chngIcals == null)
                    chngIcals = new List<iCalendar>();
                chngIcals.Add(iCal);

                for (int sercnt = 0; sercnt < chngIcals.Count; sercnt++)
                {
                    serIcal = chngIcals[sercnt];
                    iCalendarSerializer serializer = new iCalendarSerializer(serIcal);
                    ContentType calType = new ContentType("text/calendar");
                    calType.Parameters.Add("method", serIcal.Method);

                    calType.Parameters.Add("name", "meeting.ics");


                    AlternateView calendarView = AlternateView.CreateAlternateViewFromString(serializer.SerializeToString(), calType);
                    calendarView.TransferEncoding = TransferEncoding.SevenBit;

                    app.AlternateViews.Add(calendarView);
                    app.Body = serializer.SerializeToString();
                    string subject = "";
                    if (ispending)
                        subject += " [Pending Approval] ";
                    subject += conf.externalname;
                    
                    //FB 2879
                    if (sysSettings.EnableCloudInstallation > 0)
                        subject = emailSubject;

                    app.Subject = subject;

                    app.From = new MailAddress(confHost.Email);

                    for (int recp = 0; recp < recipients.Length; recp++)
                    {
                        if (recipients[recp] != "")
                            app.To.Add(recipients[recp]);

                    }

                    if (required != "")
                    {
                        vrmEmail theEmail = new vrmEmail();
                        theEmail.emailTo = required;
                        theEmail.emailFrom = confHost.Email;
                        //app.Body = app.Body.Replace("?", ","); // FB 1888
                        app.Body = app.Body.Replace("&amp;", "&").Replace(@"&amp\;", "&");//FB 2879
                        theEmail.Message = app.Body;
                        theEmail.Subject = app.Subject;
                        theEmail.UUID = getEmailUUID();
                        theEmail.orgID = conf.orgId;
                        theEmail.Iscalendar = 1;
                        m_IemailDao.Save(theEmail);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Cannot save email" + e.StackTrace, e);
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        //FB 2218

        //FB 1782 end

        //New methods added & existing methods modified for FB 1830 start

        #region FetchEmailString
        /// <summary>
        /// FetchEmailString
        /// </summary>
        /// <param name="languageid"></param>
        /// <param name="emailMode"></param>
        /// <param name="emailTypeid"></param>
        private void FetchEmailString(int languageid, int emailMode, eEmailType emailTypeid)
        {
            try
            {
                emailContent = "";
                emailSubject = "";
                placeHolders = new List<int>();

                if (languageid <= 0)
                    languageid = 1; //default language

                List<ICriterion> criterionList = new List<ICriterion>();

                criterionList.Add(Expression.Eq("EmailLangId", languageid));
                criterionList.Add(Expression.Eq("EmailMode", emailMode));
                criterionList.Add(Expression.Eq("Emailtypeid", (int)emailTypeid));

                List<vrmEmailContent> emailContLst = m_IEmailContentDao.GetByCriteria(criterionList);

                if (emailContLst == null)
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("EmailLangId", 1)); //default english
                    criterionList.Add(Expression.Eq("EmailMode", emailMode));
                    criterionList.Add(Expression.Eq("Emailtypeid", (int)emailTypeid));
                    emailContLst = m_IEmailContentDao.GetByCriteria(criterionList);
                }
                else
                {
                    if (emailContLst.Count <= 0)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("EmailLangId", 1)); //default english
                        criterionList.Add(Expression.Eq("EmailMode", emailMode));
                        criterionList.Add(Expression.Eq("Emailtypeid", (int)emailTypeid));
                        emailContLst = m_IEmailContentDao.GetByCriteria(criterionList);
                    }
                }

                if(emailContLst != null)
                    if (emailContLst.Count > 0)
                    {
                        emailContent = emailContLst[0].EmailBody;
                        emailSubject = emailContLst[0].EmailSubject;

                        //if (emailContLst[0].Placeholders.Trim() != "")
                        //{
                        //    string[] placeHolds = emailContLst[0].Placeholders.Split(',');
                        //    if (placeHolds != null)
                        //    {
                        //        int plholdId = 0;
                        //        for (int k = 0; k < placeHolds.Length; k++)
                        //        {
                        //            int.TryParse(placeHolds[k], out plholdId);
                                    
                        //            if(! placeHolders.Contains(plholdId))
                        //                placeHolders.Add(plholdId);
                        //        } 
                        //    }
                        //}
                    }
            }
            catch (Exception e)
            {
                m_log.Error("Error in FetchEmailString:", e);
                emailContent = "";
            }
        }
        #endregion

        #region  sendParticipantEmail
        /// <summary>
        /// sendParticipantEmail
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="cc"></param>
        /// <returns></returns>
        private bool sendParticipantEmail(int userid, int cc, eEmailType emailType)
        {
            
            try
            {
                GetAttachementsStr() ;//FB 2154

                vrmUtility util = new vrmUtility();
                string dislaimer = sysSettings.emaildisclaimer;
                string tempID = conf.confid.ToString();
                string userEmail = "";
                string usrName = "";
                string alternate = "";
                int emailMask = 0;
                int doubleEmail = 0;
                string req = userid.ToString();
                int userEmailLang = 1;  //default en
                int userLanguage = 1;   //default en



                // FB 2675 Starts 

                List<vrmConfBridge> vConfbridgeList = null;
                List<ICriterion> ConfBridgeList = null;
                string BridgeEXTNo = "";
                List<ICriterion> criterionList = new List<ICriterion>();
                if (confUserLst == null)
                {
                    //Fetch Participants list                    
                    criterionList.Add(Expression.Eq("confid", conf.confid));
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    confUserLst = m_vrmConfUserDAO.GetByCriteria(criterionList);
                }

                ConfBridgeList = new List<ICriterion>();
                if (confUserLst.Count > 0)
                {
                    ConfBridgeList.Add(Expression.Eq("ConfID", conf.confid));
                    if (conf.instanceid > 0)
                        ConfBridgeList.Add(Expression.Eq("InstanceID", conf.instanceid));
                    else
                        ConfBridgeList.Add(Expression.Eq("InstanceID", 1));
                    vConfbridgeList = m_IconfBridge.GetByCriteria(ConfBridgeList);
                }

                if (vConfbridgeList.Count > 0)
                {
                    BridgeEXTNo = "";
                    if (vConfbridgeList[0].E164Dialnumber != "")//FB 2659
                        BridgeEXTNo = vConfbridgeList[0].E164Dialnumber.ToString();
                    if (vConfbridgeList.Count > 1)
                    {
                        BridgeEXTNo = "";
                        for (int i = 0; i < vConfbridgeList.Count; i++)
                        {
                            if (vConfbridgeList[i].E164Dialnumber != "")//FB 2659
                                BridgeEXTNo += vConfbridgeList[i].BridgeName + " - " + vConfbridgeList[i].E164Dialnumber.ToString() + "; ";
                        }
                    }
                }
                emailObject.ConferenceDialinNumber = BridgeEXTNo.ToString();

                if (sysSettings.EnableCloudInstallation > 0) //FB 2659
                    FetchDesktopURLandDialNumber();
                vConfbridgeList = new List<vrmConfBridge>();
                ConfBridgeList = new List<ICriterion>();
                criterionList = new List<ICriterion>();
                // FB 2675 Ends


                criterionList.Add(Expression.Eq("userid", userid));
                List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList);

                if (userList.Count == 0)
                {
                    List<vrmGuestUser> guestList = m_IguestUserDao.GetByCriteria(criterionList);
                    if (guestList.Count > 0)
                    {
                        emailMask = guestList[0].emailmask;
                        if (emailMask != 1)
                            return true;
                        //userDateFormat = ((guestList[0].DateFormat.Trim() == "") ? "MM/dd/yyyy" : guestList[0].DateFormat.Trim());
                        SetEmailDateFormat(guestList[0].DateFormat.Trim(), ref userDateFormat); //FB 2555
                        //userTimeFormat = ((guestList[0].TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                        SetEmailTimeFormat(guestList[0].TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                        tzDisplay = ((guestList[0].Timezonedisplay.Trim() == "") ? "1" : guestList[0].Timezonedisplay.Trim());
                        usrTimezone = guestList[0].TimeZone; //FB 1702 - 1830
                        userEmail = guestList[0].Email;
                        usrName = guestList[0].FirstName.Trim() + " " + guestList[0].LastName.Trim();
                        req = guestList[0].userid.ToString();
                        if (guestList[0].DoubleEmail == 1)
                            alternate = guestList[0].AlternativeEmail;
                    }
                }
                else
                {
                    emailMask = userList[0].emailmask;
                    if (emailMask != 1)
                        return true;

                    if (userList[0].Audioaddon.Trim() == "1") //FB 2023 (Stop Mail to Audio bridge user)
                        return true;

                    //userDateFormat = ((userList[0].DateFormat.Trim() == "") ? "MM/dd/yyyy" : userList[0].DateFormat.Trim());
                    SetEmailDateFormat(userList[0].DateFormat.Trim(),ref userDateFormat);//FB 2555
                    //userTimeFormat = ((userList[0].TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                    SetEmailTimeFormat(userList[0].TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                    tzDisplay = ((userList[0].Timezonedisplay.Trim() == "") ? "0" : userList[0].Timezonedisplay.Trim());
                    usrTimezone = userList[0].TimeZone; //FB 1702 - 1830
                    userEmail = userList[0].Email;
                    usrName = userList[0].FirstName.Trim() + " " + userList[0].LastName.Trim();
                    req = userList[0].userid.ToString();
                    userLanguage = userList[0].Language;
                    userEmailLang = userList[0].EmailLangId;
                    if (userList[0].DoubleEmail == 1)
                        alternate = userList[0].AlternativeEmail;
                }
                
                if(instanceType == "I") //FB 1830-individual instance of recurrence
                    tempID = tempID + "," + conf.instanceid.ToString(); 
   
                //FB 2154
               if(orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

               if (orgInfo.SendAttachmentsExternal <= 0)
                   if (!CheckEmailDomain(userEmail))
                       emailObject.Attachments = "";

               //FB 2154         

               if (orgInfo.EnableCncigSupport > 0) //FB 2632
                   emailObject.ConciergeSupport = ConciergeSupport();
           
                util.simpleEncrypt(ref req);
                util.simpleEncrypt(ref tempID);
                //string url_params = websiteURL + Language + "/dispatcher/emaildispatcher.asp?cmd=ResponseInvite&id="; //FB 1628
                string url_params = websiteURL +"/"+ Language + "/genlogin.aspx?id="; //FB 1628
                url_params += "0x" + tempID;
                url_params += "&req=0x" + req + "&tp=2";

                emailObject.WebsiteURL = url_params; //complete website url
                emailObject.Recipient = usrName; //Determine user's first name for greeting

                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;
                
                //FB 2693 Starts
                if (conf.isPCconference > 0)
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", conf.owner));
                    criterionList.Add(Expression.Eq("PCId", conf.pcVendorId));

                    confPCLst = m_IUserPCDAO.GetByCriteria(criterionList);
                }
                if (confPCLst != null && confPCLst.Count > 0)
                    emailObject.PCDetails = BuildPCContent(confPCLst[0]);
                //FB 2693 Ends

                SetEmailLanguage(ref userLanguage, ref userEmailLang); //Decision for email language

                if(emailType == eEmailType.PartyRemainder)
                    FetchEmailString(emailLanguage, 0, eEmailType.PartyRemainder);
                else if (emailType == eEmailType.Public_Participant) //FB 2550 // FB 2675
                    FetchEmailString(emailLanguage, 0, eEmailType.Public_Participant);
                else
                    FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.Participant);
                                 
                SetConfDateTime(eEmailType.Participant); //Conf Duration & subject
                IsParty = true; //FB 2419
                BuildEmailBody();
                IsParty = false;
                emailContent = emailContent + dislaimer;

                vrmEmail theEmail = new vrmEmail();
                theEmail.emailTo = userEmail;
                theEmail.emailFrom = confHost.Email;
                theEmail.Message = emailContent;
                theEmail.Subject = emailSubject;

                if (cc == 1)
                    theEmail.cc = userEmail;

                if (ical.Trim().Length > 0)
                    theEmail.Attachment = ical;

                theEmail.UUID = getEmailUUID();
                theEmail.orgID = conf.orgId;

                m_IemailDao.Save(theEmail);

                // DoubleEmail - email a copy to the alternateEmail for the user
                if (doubleEmail == 1)
                {
                    if (alternate.Length > 0)
                    {
                        theEmail.emailTo = alternate;
                        theEmail.UUID = getEmailUUID();
                        m_IemailDao.Save(theEmail);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendParticipantEmail: ", e);
                throw e;
            }
        }
        #endregion

        #region sendSchedulerEmail
        //This method is used to send email for both host and scheduler
        /// <summary>
        /// sendSchedulerEmail
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="emailType"></param>
        /// <returns></returns>
        private bool sendSchedulerEmail(int userid, eEmailType emailType)
        {
            try
            {
                string dislaimer = sysSettings.emaildisclaimer;
                string userEmail = "";
                string usrName = "";
                string alternate = "";
                int emailMask = 0;
                int doubleEmail = 0;
                int userPrefLang = 1;
                int userEmailLang = 1;

                if (orgInfo == null) //FB 2632
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);


                List<vrmUser> userList = new List<vrmUser>();
                if (userid == confHost.userid)
                {
                    userList.Add(confHost);
                }
                else
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", userid));
                    userList = m_IuserDao.GetByCriteria(criterionList);
                }

                emailMask = userList[0].emailmask;
                if (emailMask != 1)
                    return true;

                //userDateFormat = ((userList[0].DateFormat.Trim() == "") ? "MM/dd/yyyy" : userList[0].DateFormat.Trim());
                SetEmailDateFormat(userList[0].DateFormat.Trim(),ref userDateFormat);//FB 2555
                //userTimeFormat = ((userList[0].TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                SetEmailTimeFormat(userList[0].TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                tzDisplay = ((userList[0].Timezonedisplay.Trim() == "") ? "0" : userList[0].Timezonedisplay.Trim());
                usrTimezone = userList[0].TimeZone; //FB 1702 - 1830
                userEmail = userList[0].Email;
                usrName = userList[0].FirstName.Trim() + " " + userList[0].LastName.Trim();
                userPrefLang = userList[0].Language;
                userEmailLang = userList[0].EmailLangId;

                if (userList[0].DoubleEmail == 1)
                    alternate = userList[0].AlternativeEmail;

                string customAttrInfo = "", hostAudInfo="";
                if (emailType == eEmailType.Scheduler)
                {
                    customAttrInfo = addConfCustomAttr("Scheduler");
                }
                else
                {
                    customAttrInfo = addConfCustomAttr("Host");
                }
                if (conf.conftype == 2) //FB 2381
                {
                    hostAudInfo = FetchAudioDialString(conf.confid, conf.instanceid, "H", 0); //FB 2381 //FB 2439
                }
                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";

                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                    emailObject.ConciergeSupport = ConciergeSupport();

                //FB 2693 Starts
                if (conf.isPCconference > 0)
                {
                    List<ICriterion> criterionPCList = new List<ICriterion>();
                    criterionPCList.Add(Expression.Eq("userid", conf.owner));
                    criterionPCList.Add(Expression.Eq("PCId", conf.pcVendorId));
                    confPCLst = m_IUserPCDAO.GetByCriteria(criterionPCList);
                }
                if (confPCLst != null && confPCLst.Count > 0)
                    emailObject.PCDetails = BuildPCContent(confPCLst[0]);
                //FB 2693 Ends

                emailObject.AudioInfo = hostAudInfo;
                emailObject.Recipient = usrName; 
                emailObject.CustomOptions = customAttrInfo;
                emailObject.CallLaunch = callLaunch;//FB 2522 
                SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                FetchEmailString(emailLanguage, conf.ConfMode, emailType);
                SetConfDateTime(emailType);

                if (sysSettings.EnableCloudInstallation > 0)//FB 2659
                    FetchDesktopURLandDialNumber();

                BuildEmailBody();

                vrmEmail theEmail = new vrmEmail();
                theEmail.emailTo = userEmail;
                theEmail.emailFrom = confHost.Email;
                theEmail.Message = emailContent;
                theEmail.Subject = emailSubject;
                
                if (ical.Trim().Length > 0)
                    theEmail.Attachment = ical;

                theEmail.UUID = getEmailUUID();
                theEmail.orgID = conf.orgId;
                m_IemailDao.Save(theEmail);
                
                // DoubleEmail - email a copy to the alternateEmail for the user
                if (doubleEmail == 1)
                {
                    if (alternate.Length > 0)
                    {
                        theEmail.emailTo = alternate;
                        theEmail.UUID = getEmailUUID();
                        m_IemailDao.Save(theEmail);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Cannot save email", e);
                throw e;
            }
        }
        #endregion

        #region SetHostLangFolder
        /// <summary>
        /// SetHostLangFolder
        /// </summary>
        /// <param name="languageId"></param>
        private void SetHostLangFolder(int languageId)
        {
            try
            {
                Language = "en"; //default language
                LanguageUpload = "upload";

                vrmLanguage hostLang = m_ILanguageDAO.GetLanguageById(languageId);
                Language = hostLang.LanguageFolder;

            }
            catch (Exception e)
            {
                m_log.Error("SetHostLangFolder", e);
            }
        }
        #endregion

        #region SetWebsiteURL
        /// <summary>
        /// SetWebsiteURL
        /// </summary>
        private void SetWebsiteURL()
        {
            try
            {
                sysSettings sys = new sysSettings(m_configPath);
                sysMailData sysMail = new sysMailData();
                sys.getSysMail(ref sysMail);

                websiteURL = sysMail.websiteURL;
            }
            catch (Exception e)
            {
                m_log.Error("SetWebsiteURL", e);
            }
        }
        #endregion

        #region GetAttachementsStr
        /// <summary>
        /// GetAttachementsStr
        /// </summary>
        private void GetAttachementsStr()
        {
            String newFileName = "";//FB 2154
            try
            {
                IConfAttachmentDAO confAttach = m_confDAO.GetConfAttachmentDao();
                List<ICriterion>  criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));

                List<vrmConfAttachments> attachments = confAttach.GetByCriteria(criterionList);
                int i = 0;
                string att = "";
                vrmConfAttachments attach;
                for (int lp = 0; lp < attachments.Count; lp++)
                {
                    attach = attachments[lp];
                    if (attach.attachment.Length > 0)
                    {
                        string sAttachment = attach.attachment;
                        if (i == 0)
                        {
                            att = "<span style=\"font-family: Verdana;font-size: 10pt\">";
                            att += "<BR><U>Attachments: </U><BR />";
                        }

                        if (sAttachment.Length > 6)
                        {
                            if (sAttachment.Substring(0, 6) == "[ICAL]")
                            {
                                ical = sAttachment.Substring(6, sAttachment.Length - 6);
                            }
                        }

                        //string testString = "\\"+ Language +"\\"+ LanguageUpload +"\\";
                        string testString = "\\en\\" + LanguageUpload + "\\";//FB 2154
                        int j = sAttachment.IndexOf(testString);

                        if (j > 0)
                        {
                            sAttachment = sAttachment.Substring(j + testString.Length, sAttachment.Length - (j + testString.Length));
                            sAttachment.Replace("\\", "/");
                            newFileName = sAttachment;//FB 2154
                            string quote = "\"";
                            string slashQuote = "/";
                            //att += "<br /><a href= " + quote + websiteURL + slashQuote + Language + slashQuote + LanguageUpload + slashQuote +
                            att += "<br /><a href= " + quote + websiteURL + slashQuote + "en" + slashQuote + LanguageUpload + slashQuote + //FB 2154
                            sAttachment + quote + ">" + sAttachment + "</a>";//FB 2154
                            i++;
                        }
                    }
                }
                if (i > 0)
                {
                    att += "</span><BR>";
                }

                emailObject.Attachments = att;
            }
            catch (Exception e)
            {
                m_log.Error("GetAttachementsStr - Email", e);
            }
        }
        

        #endregion

        #region SetEmailLanguage
        /// <summary>
        /// SetEmailLanguage //FB 1830-translation
        /// </summary>
        private void SetEmailLanguage(ref int userLanguage, ref int userEmailLang)
        {
            vrmUser confhostUser = null;
            int userid = 11;
            emailLanguage = 1; //default englisn
            vrmEmailLanguage emailLangs = null;
            try
            {
                //FB 2418 Starts
                if (confHost != null)
                    userid = confHost.userid;

                confhostUser = m_IuserDao.GetByUserId(userid);

                if (userEmailLang <= 0)
                    userEmailLang = userLanguage;

                emailLanguage = userLanguage; // No Host,Org Customization so base email content

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (confhostUser.EmailLangId > 0)
                {
                    if (confhostUser.Language == userLanguage)
                        emailLanguage = confhostUser.EmailLangId;
                }
                else if (orgInfo.EmailLangId > 100) //FB 2418 Ends
                {
                    if (m_IEmailLanguageDAO == null)
                        m_IEmailLanguageDAO = m_confDAO.GetEmailLanguageDao(); //FB 1830

                    emailLangs = m_IEmailLanguageDAO.GetById(orgInfo.EmailLangId);

                    if (emailLangs != null)
                    {
                        if (emailLangs.LanguageId == userLanguage)
                            emailLanguage = orgInfo.EmailLangId;
                    }
                }
            }


            catch (Exception e)
            {
                m_log.Error("GetEmailLanguage", e);
            }
        }

        #endregion

        #region SetConfDateTime
        /// <summary>
        /// SetConfDateTime
        /// </summary>
        /// <param name="emailtype"></param>
        private void SetConfDateTime(eEmailType emailtype)
        {
            string durationString = "";
            string confStart = "";
            string setup = "";
            string teardown = "";
            try
            {
                DateTime conferenceDate = conf.confdate;
                DateTime setupDate = conf.SetupTime; //Buffer Zone
                DateTime teardownDate = conf.TearDownTime;
                timeZoneData tz = new timeZoneData();
                timeZone.userPreferedTime(usrTimezone, ref conferenceDate);
                timeZone.userPreferedTime(usrTimezone, ref setupDate);
                timeZone.userPreferedTime(usrTimezone, ref teardownDate);
                timeZone.GetTimeZone(usrTimezone, ref tz);

                string subject = " " + conf.externalname + " @ "
                    + setupDate.ToString(userDateFormat + " " + userTimeFormat)
                    + " " + ((tzDisplay == "1") ? tz.TimeZone : "");

                emailSubject = emailSubject + subject;

                if (emailtype == eEmailType.HKAdmin || emailtype == eEmailType.CateringAdmin || emailtype == eEmailType.InventoryAdmin) //Disney (emailtype == eEmailType.Participant || )
                {
                    TimeSpan bufferDuration = teardownDate - setupDate;

                    confStart = setupDate.ToString(userDateFormat.Trim() + " " + userTimeFormat) // FB 1906
                        + ((tzDisplay == "1") ? " " + tz.TimeZone : "");

                    int hr = bufferDuration.Hours;
                    int min = bufferDuration.Minutes;

                    if (hr > 0 && min > 0)
                        durationString = hr.ToString() + " hrs " + min.ToString() + " mins";
                    else if (hr > 0 && min == 0)
                        durationString = hr.ToString() + " hrs ";
                    else
                        durationString = min.ToString() + " mins";
                }
                else
                {
                    confStart = conferenceDate.ToString(userDateFormat.Trim() + " " + userTimeFormat) // FB 1906
                        + ((tzDisplay == "1") ? " " + tz.TimeZone : "");

                    setup = setupDate.ToString(userDateFormat.Trim() + " " + userTimeFormat)   // FB 1906
                        + ((tzDisplay == "1") ? " " + tz.TimeZone : "");

                    teardown = teardownDate.ToString(userDateFormat.Trim() + " " + userTimeFormat) // FB 1906
                        + ((tzDisplay == "1") ? " " + tz.TimeZone : "");

                    durationString = "";

                    int hr = conf.duration / 60;
                    int min = conf.duration % 60;

                    if (hr > 0 && min > 0)
                        durationString = hr.ToString() + " hrs " + min.ToString() + " mins";
                    else if (hr > 0 && min == 0)
                        durationString = hr.ToString() + " hrs ";
                    else
                        durationString = min.ToString() + " mins";
                }
                
                emailObject.StartDatetime = confStart;
                emailObject.Duration = durationString;
                emailObject.Setup = setup;
                emailObject.Teardown = teardown;
            }
            catch(Exception e)
            {
                m_log.Error("Email- SetConfDateTime: ", e);
            }
        }
        #endregion

        #region  emailInvitation
        /// <summary>
        /// emailInvitation
        /// </summary>
        /// <returns></returns>
        public bool emailInvitation()
        {
            bool sendEmail = false;//FB 2722

            try
            {
                //GetAttachementsStr(); FB 2154

                if (confUserLst == null)
                {
                    //Fetch Participants list
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf.confid));
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    confUserLst = m_vrmConfUserDAO.GetByCriteria(criterionList);
                }

                if (conf.StartMode == 0) //FB 2522 
                    callLaunch = "Yes";
                bool hostEmail = false;
                bool SchedEmail = false;
                string confcodepin = ""; //FB 1642
                vrmConfUser us;
                //Sending mail to participants
                if (conf.ConfMode != (int)eEmailMode.CONF_DENY) //Denial mails will not be sent to participants
                {
                    FetchAudioBridgeDialString(conf.confid,conf.instanceid); //FB 2439
                    for (int i = 0; i < confUserLst.Count; i++)
                    {
                        sendEmail = false;//FB 2722
                        us = confUserLst[i]; //FB 1830

                        if (us.partyNotify == 1) //FB 1830
                        {
                            confcodepin = "";
                            if (conf.conftype == 2 && us.audioOrVideo == 1) //FB 1744 //FB 2381
                            {
                                confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "P", us.userid);
                            }
                            //FB 2439 start
                            emailObject.PersonalAudioInfo = confcodepin;
                            emailObject.AudioInfo = partyAudioBridgeDt;
                            //FB 2439 end
                            emailObject.CustomOptions = addConfCustomAttr("Party"); //FB 2381
                            emailObject.CallLaunch = callLaunch;//FB 2522 
                            
                            if (orgInfo.EnableCncigSupport > 0) //FB 2632
                                emailObject.ConciergeSupport = ConciergeSupport();

                            if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                            {
                                sendEmail = true;//FB 2722 Starts
                                //FB 1830 Email Edit - start
                                if (conf.ConfMode == (int)eEmailMode.CONF_EDIT && us.NotifyOnEdit != 1)
                                    sendEmail = false;
                                if (sendEmail)
                                    sendEmail = sendParticipantEmail(us.userid, us.invitee, eEmailType.Participant);

                                if (!sendEmail)
                                    m_log.Error("Cannot send participant email [userid]" + us.userid.ToString());
                                //FB 2722 Ends
                            }
                            //FB 1830 Email Edit - end
                        }

                        if (us.userid == conf.userid && sendEmail)
                            SchedEmail = true;

                        if (us.userid == confHost.userid && sendEmail)//if participant is host
                            hostEmail = true;
                    }
                }

                if (!hostEmail)//Email to Owner
                {
                   
                    if (conf.ConfMode == (int)eEmailMode.CONF_DENY )
                    {
                        if (!sendSchedulerEmail(confHost.userid, eEmailType.AppDenial_Host))
                            m_log.Error("Cannot send host email [userid]" + confHost.userid.ToString());
                    }
                    else if (!(conf.ConfMode == (int)eEmailMode.CONF_DELETE && conf.conftype == 7))//FB 2995
                        if (!sendSchedulerEmail(confHost.userid, eEmailType.Host))
                            m_log.Error("Cannot send host email [userid]" + confHost.userid.ToString());
                    
                }

                //Scheduler is not a participant and not the owner - person who schedule conf on behalf of host
                emailObject.AudioInfo = "";
                if (!SchedEmail && conf.owner != conf.userid && orgInfo.SendConfirmationEmail == 0)//FB 2470
                {
                    if (conf.ConfMode == (int)eEmailMode.CONF_DENY)
                    {
                        if (!sendSchedulerEmail(conf.userid, eEmailType.AppDenial_Scheduler))
                            m_log.Error("Cannot send scheduler email [userid]" + conf.userid.ToString());
                    }
                    else
                        if (!sendSchedulerEmail(conf.userid, eEmailType.Scheduler))
                            m_log.Error("Cannot send scheduler email [userid]" + conf.userid.ToString());
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("emailParticipants: Cannot save email", e);
                throw e;
            }
        }
        #endregion

        //Invitation  Email for Plugin FB 2141

        #region  SendConfirmationEmailsforPlugin
        /// <summary>
        /// SendConfirmationEmailsforPlugin Invitation  Email for Plugin
        /// </summary>
        /// <param name="confList"></param>
        /// <returns></returns>
        public bool SendConfirmationEmailsforPlugin(List<vrmConference> confList)
        {
            List<vrmUser> userList = null;
            List<ICriterion> criterionList = null;
            List<vrmGuestUser> guestList = null;
            bool hostEmail = false;
            bool SchedEmail = false;
            bool forceEmail = false;
            string confcodepin = ""; //FB 1642
            vrmConfUser us;
            try
            {
                emailObject = new sConferenceEmail();
                conf = confList[0];

                if (conf == null)
                    return false;

                SetWebsiteURL();

                confHost = m_IuserDao.GetByUserId(conf.owner);

                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                SetHostLangFolder(confHost.Language);

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;

                emailObject.MailLogo = AttachEmailLogo(organizationID);

                List<ICriterion> criterionListConf = new List<ICriterion>();
                criterionListConf.Add(Expression.Eq("confid", conf.confid));

                if (conf.instanceid > 0)
                    criterionListConf.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionListConf.Add(Expression.Eq("instanceid", 1));

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionListConf);//FB 2636


                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "") 
                    Password = "N/A";
                emailObject.Password = Password;

                //FB 2376  PIM Conf - VMR Type - end
                emailObject.ExternalBridge = "N/A";
                emailObject.InternalBridge = "N/A";

               
                //if (conf.isVMR == 1)
                if (conf.isVMR  > 0) //FB 2620 //FB 2957
                {
                    if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                    {
                        emailObject.ExternalBridge = confAVParam[0].externalBridge;
                    }

                    if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                    {
                        emailObject.InternalBridge = confAVParam[0].internalBridge;
                    }
                }
                //FB 2376  PIM Conf - VMR Type - end
                //GetAttachementsStr(); FB 2154

                if (confUserLst == null)
                {
                    //Fetch Participants list
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf.confid));
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    confUserLst = m_vrmConfUserDAO.GetByCriteria(criterionList);
                }




                //Sending mail to participants
                if (conf.ConfMode != (int)eEmailMode.CONF_DENY) //Denial mails will not be sent to participants
                {
                    for (int i = 0; i < confUserLst.Count; i++)
                    {
                        forceEmail = false;

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("userid", confUserLst[i].userid));
                        userList = m_IuserDao.GetByCriteria(criterionList);

                        if (userList.Count == 0)
                        {
                            
                            forceEmail = true;
                            /*guestList = m_IguestUserDao.GetByCriteria(criterionList);
                             * if (guestList.Count > 0)
                            {
                                if (confHost.Email.Split('@')[1].ToLower() != guestList[0].Email.Split('@')[1].ToLower())
                                    forceEmail = true;
                            }*/
                        }
                        else
                        {
                            /*if (confHost.Email.Split('@')[1].ToLower() != userList[0].Email.Split('@')[1].ToLower())
                                forceEmail = true;*/

                            if (userList[0].PluginConfirmations > 0)
                                forceEmail = true;

                        }

                        us = confUserLst[i]; //FB 1830

                        if (us.userid == conf.userid)
                            SchedEmail = true;

                        if (us.partyNotify == 1 || forceEmail) //FB 1830
                        {
                            if (us.userid == confHost.userid)//if participant is host
                                hostEmail = true;

                            confcodepin = "";
                            if (conf.conftype == 2 && us.audioOrVideo == 1) //FB 1744 //FB 2381
                            {
                                confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "P", us.userid);
                            }
                            emailObject.AudioInfo = confcodepin;

                            if (!sendParticipantEmail(us.userid, us.invitee, eEmailType.Participant))
                                m_log.Error("Cannot send participant email [userid]" + us.userid.ToString());

                        }
                    }
                }
                /* Just worrying abt participant mail for now.
                if (!hostEmail)//Email to Owner
                {
                    string hostAudInfo = "";
                    if (conf.conftype == 2)
                    {
                        hostAudInfo = FetchAudioDialString(conf.confid, conf.instanceid, "H", 0);
                    }
                    emailObject.AudioInfo = hostAudInfo;

                    if (conf.ConfMode == (int)eEmailMode.CONF_DENY)
                    {
                        if (!sendSchedulerEmail(confHost.userid, eEmailType.AppDenial_Host))
                            m_log.Error("Cannot send host email [userid]" + confHost.userid.ToString());
                    }
                    else
                        if (!sendSchedulerEmail(confHost.userid, eEmailType.Host))
                            m_log.Error("Cannot send host email [userid]" + confHost.userid.ToString());

                }

                //Scheduler is not a participant and not the owner - person who schedule conf on behalf of host
                if (conf.ConfMode != 2)
                {
                    emailObject.AudioInfo = "";
                    if (!SchedEmail && conf.owner != conf.userid)
                    {
                        if (conf.ConfMode == (int)eEmailMode.CONF_DENY)
                        {
                            if (!sendSchedulerEmail(conf.userid, eEmailType.AppDenial_Scheduler))
                                m_log.Error("Cannot send scheduler email [userid]" + conf.userid.ToString());
                        }
                        else
                            if (!sendSchedulerEmail(conf.userid, eEmailType.Scheduler))
                                m_log.Error("Cannot send scheduler email [userid]" + conf.userid.ToString());
                    }
                }*/
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SendConfirmationEmail", e);
                throw e;
            }
        }
        #endregion

        #region  SendConfirmationEmails
        /// <summary>
        /// SendConfirmationEmails
        /// </summary>
        /// <param name="confList"></param>
        /// <returns></returns>
        public bool SendConfirmationEmails(List<vrmConference> confList)
        {
            try
            {
                //2075
                DateTime serverTime = DateTime.Now;
                serverTime = serverTime.AddMinutes(-5);
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);

                for (int k = 0; k < confList.Count; k++)
                {


                    if (confList[k].confdate < serverTime)
                        continue;

                    conf = confList[k];
                    break;
                }
                //conf = confList[0];     /
                //2075

                if (conf == null)
                    return false;

                emailObject = new sConferenceEmail();
                SetWebsiteURL();

                confHost = m_IuserDao.GetByUserId(conf.owner);

                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid); 

                SetHostLangFolder(confHost.Language);

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;

                emailObject.MailLogo = AttachEmailLogo(organizationID);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));

                if (conf.instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionList.Add(Expression.Eq("instanceid", 1));

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionList);//FB 2636


                SetConfRoomString();
                SetConfGuestRoomString();//FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "")
                    Password = "N/A";
                emailObject.Password = Password;
                
                //FB 2376  Starts
                emailObject.ExternalBridge = "N/A";
                emailObject.InternalBridge = "N/A";

                
                //if (conf.isVMR == 1)
                if (conf.isVMR > 0) //FB 2620 //FB 2957
                {
                    if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                    {
                        emailObject.ExternalBridge = confAVParam[0].externalBridge;
                    }

                    if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                    {
                        emailObject.InternalBridge = confAVParam[0].internalBridge;
                    }
                }
                //FB 2376 Ends

                emailInvitation();

                //FB 2670 Start
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));

                if (conf.instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionList.Add(Expression.Eq("instanceid", 1));

                ConfVNOCList = m_ConfVNOCOperatorDAO.GetByCriteria(criterionList);

                //FB 2670 End
                if (conf.ConfMode != (int)eEmailMode.CONF_DENY && orgInfo.SendConfirmationEmail ==0 )//FB 2470
                {
                    if (ConfVNOCList.Count > 0) //FB 2501 FB 2670
                        sendVNOCAdminEmail();
                    if (conf.conftype != vrmConfType.RooomOnly && conf.conftype != vrmConfType.P2P)//FB 2350
                    {
                        if (conf.mcuList.Count > 0)
                            sendMCUAdminEmail();

                        //sendICALforCISCOEndpoints(confList, "C");  //Cisco ICAL FB 1602 //Removed for FB 1675
                    }
                    emailLocations();
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SendConfirmationEmail", e);
                throw e;
            }
        }
        #endregion

        #region  BuildEmailBody
        /// <summary>
        /// BuildEmailBody
        /// </summary>
        public void BuildEmailBody()
        {
            try
            {
                String PartyLink = "", VMR = "", Password = ""; //FB 2419 start //FB 2448 2262
                string VMRExternalBridge = "", VMRInternalBridge = ""; //FB 2690
                if (IsParty)
                {
                    if (orgInfo.EnableAcceptDecline == 1)
                    {
                        PartyLink = "<table border=\"0\" cellpadding=\"4\" cellspacing=\"4\">"
                                                + "<tr>"
                                                //+ "<td bgcolor=\"#00ff00\" width=\"40\">"
                                                + "</td>"
                                                + "<td>"
                                                    + "<span style=\"font-family: Verdana; font-size: 10pt\">"
                                                   // + "<span style=\"color: #008000\"><strong></strong></span>"
                                                    //+ "<a href=\"" + emailObject.WebsiteURL + "\"><strong>ACCEPT </strong></a><span style=\"color: #008000\">"
                                                    + "<a href=\"" + emailObject.WebsiteURL + "\" style=\"color: #008000\"><strong>ACCEPT</strong></a>"
                                                    + "</span>"
                                                + "</td>"
                                            + "</tr>"
                                            + "<tr>"
                                                //+ "<td bgcolor=\"#ff0000\" width=\"40\">"
                                                + "</td>"
                                                + "<td>"
                                                    + "<span style=\"font-family: Verdana; font-size: 10pt\">"
                                                   // + "<span style=\"color: #ff0000\"><strong></strong></span>"
                                                    //+ "<a href=\"" + emailObject.WebsiteURL + "\"><strong>DECLINE</strong></a><span style=\"color: ##ff0000\">"
                                                    + "<a href=\"" + emailObject.WebsiteURL + "\" style=\"color: #ff0000\"><strong>DECLINE</strong></a>"
                                                    + "</span>"
                                                + "</td>"
                                            + "</tr>"
                                        + "</table>";

                    }
                }

                //if (conf.isVMR == 1 && orgInfo.EnableVMR == 1) //FB 2448
                if (conf.isVMR == 1) //FB 2620
                {
                    //VMR = "<td align=\"right\" style=\"width: 20%; color: #000000; height: 100%\">VMR:</td>"
                    //        + "<td style=\"padding-right: 0px; padding-left: 0px; padding-bottom: 0px; color: #000000;padding-top: 0px; height: 100%\">"
                    //                        + "<strong>Internal Bridge: </strong>"
                    //                     + emailObject.InternalBridge + "<strong>  External Bridge: </strong>"
                    //                     + emailObject.ExternalBridge + "</td>";

                    VMRInternalBridge = "<tr>"
                                       + "<td align=\"right\" nowrap=\"nowrap\" style=\"width: 30%; height: 30px\">"
                                           + "VMR Internal Bridge: "
                                       + "</td>"
                                       + "<td style=\"color: #000000; height: 30px\"><strong>" + emailObject.InternalBridge + "</strong></td>"
                                   + "</tr>";

                    VMRExternalBridge = "<tr>"
                                        + "<td align=\"right\" nowrap=\"nowrap\" style=\"width: 30%; height: 50%\">"
                                            + "VMR External Bridge: "
                                        + "</td>"
                                        + "<td style=\"color: #000000\"><strong>" + emailObject.ExternalBridge + "</strong></td>"
                                    + "</tr>";

                    /*VMR = "<td align=\"right\" style=\"width: 20%; color: #000000; height: 100%\">VMR:</td>"
                            + "<td style=\"padding-right: 0px; padding-left: 0px; padding-bottom: 0px; color: #000000;padding-top: 0px; height: 100%\">"
                                + "<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\" "
                                   + "style=\"font-size: 10pt; color: #000000;font-family: Verdana; height: 100%\" width=\"100%\">"
                                    + "<tr>"
                                        + "<td align=\"left\" nowrap=\"nowrap\" style=\"width: 30%; height: 30px\">"
                                            + "<strong>Internal Bridge: </strong>"
                                        + "</td>"
                                        + "<td style=\"color: #000000; height: 30px\">" + emailObject.InternalBridge + "</td>"
                                    + "</tr>"
                                    + "<tr>"
                                        + "<td align=\"left\" nowrap=\"nowrap\" style=\"width: 30%; height: 50%\">"
                                            + "<strong>External Bridge: </strong>"
                                        + "</td>"
                                        + "<td style=\"color: #000000\">" + emailObject.ExternalBridge + "</td>"
                                    + "</tr>"
                                + "</table>"
                            + "</td>";*/
                    //FB 2480 - Starts
				 }
                //if (conf.isVMR == 1 && orgInfo.EnableVMR == 2)//FB 2448
                if (conf.isVMR == 2)//FB 2620
                {
                    //VMR = "<td align=\"right\">Virtual Meeting<br /> Room(s):<br /></td>"
                    //        + "<td>"+ emailObject.VirtualMeetingRooms
                    //        + "</td>";

                    VMRInternalBridge = "<tr>"
                                       + "<td align=\"right\" nowrap=\"nowrap\" style=\"width: 30%; height: 30px\">"
                                           + "VMR Internal Bridge: "
                                       + "</td>"
                                       + "<td style=\"color: #000000; height: 30px\"><strong>" + RoomInternalNum + "</strong></td>"
                                   + "</tr>";

                    VMRExternalBridge = "<tr>"
                                        + "<td align=\"right\" nowrap=\"nowrap\" style=\"width: 30%; height: 50%\">"
                                            + "VMR External Bridge: "
                                        + "</td>"
                                        + "<td style=\"color: #000000\"><strong>" + RoomExternalNum + "</strong></td>"
                                    + "</tr>";
                }
                //else if (conf.isVMR == 1 && orgInfo.EnableVMR ==3)//FB 2448
                else if (conf.isVMR == 3) //FB 2620
                {
                    //confHost.PrivateVMR = String.Format(confHost.PrivateVMR, confHost.VidyoURL, confHost.Extension, confHost.Pin);
                    if (conf.CloudConferencing == 1) //FB 2777
                        VMR = confHost.VidyoURL;
                    else
                        VMR = confHost.PrivateVMR;
                }

                if (orgInfo.EnableConfPassword == 1)
                {
                      Password = "<td align=\"right\">Password:</td>"
                              + "<td><strong>" + emailObject.Password + "</strong></td>";
                }
                //FB 2149 end

                //FB 1888 Starts
                if (emailObject.Recipient != null)
                    emailObject.Recipient = emailObject.Recipient.Replace("!!", "'").Replace("||", "\"");

                if (emailObject.Person_in_charge != null)
                    emailObject.Person_in_charge = emailObject.Person_in_charge.Replace("!!", "'").Replace("||", "\"");

                if (emailObject.LocationInfo != null)
                    emailObject.LocationInfo = emailObject.LocationInfo.Replace("!!", "'").Replace("||", "\"");

                if (emailObject.GuestLocations != null) //FB 2426
                    emailObject.GuestLocations = emailObject.GuestLocations.Replace("!!", "'").Replace("||", "\"");

                if(emailObject.VirtualMeetingRooms != null) //FB 2448
                    emailObject.VirtualMeetingRooms = emailObject.VirtualMeetingRooms.Replace("!!", "'").Replace("||", "\"");
                                
                if (emailObject.Host != null)
                    emailObject.Host = emailObject.Host.Replace("!!", "'").Replace("||", "\"");

                //FB 2419
                if (emailObject.Requestor != null)
                    emailObject.Requestor = emailObject.Requestor.Replace("!!", "'").Replace("||", "\"");

                if (emailObject.Description != null)
                    emailObject.Description = emailObject.Description.Replace("!!", "'").Replace("||", "\"");

                if (emailObject.RoomsAssigned != null)
                    emailObject.RoomsAssigned = emailObject.RoomsAssigned.Replace("!!", "'").Replace("||", "\"");

                if (emailObject.TechContact != null)
                    emailObject.TechContact = emailObject.TechContact.Replace("!!", "'").Replace("||", "\"");

                if (emailObject.Name != null)
                    emailObject.Name = emailObject.Name.Replace("!!", "'").Replace("||", "\""); //MCU Alert to MCU Admin

                if (emailObject.Email != null)
                    emailObject.Email = emailObject.Email.Replace("!!", "'").Replace("||", "\""); //MCU Alert to MCU Admin
                // FB 1888 Ends

                if (emailObject.RoomVMRLink != null)
                    emailObject.RoomVMRLink = emailObject.RoomVMRLink.Replace("!!", "'").Replace("||", "\"").Replace("&amp;","&"); //FB 2727
                
                if (emailContent != "")
                {
                    //emailContent = string.Format(emailContent, "", emailObject.MailLogo, emailObject.Recipient, emailObject.MCUNames, emailObject.ConferenceName, emailObject.UniqueId, emailObject.StartDatetime, emailObject.Setup, emailObject.Teardown, emailObject.Duration, emailObject.Host, emailObject.Description, emailObject.RecurrencePattern, emailObject.LocationInfo, emailObject.MCUInfo, emailObject.AudioInfo, emailObject.CustomOptions, emailObject.ConfDatetime, emailObject.PendingStatus, emailObject.Workordername, emailObject.RoomsAssigned, emailObject.InventorySetused, emailObject.Person_in_charge, emailObject.Start_byDatetime, emailObject.End_byDatetime, emailObject.Deliverytype, emailObject.Deliverycost, emailObject.Servicecharge, emailObject.Currentstatus, emailObject.Requestitem, emailObject.Totalcost, emailObject.Tobecompleted, emailObject.WorkorderComment, emailObject.ServiceType, emailObject.Cateringmenus, emailObject.DeliveryDatetime, emailObject.WebsiteURL, emailObject.TechContact, emailObject.TechEmail, emailObject.TechPhone, emailObject.Attachments);
                    //emailContent = emailContent.Replace("{43}", emailObject.Name);  //MCU Alert to MCU Admin
                    //emailContent = emailContent.Replace("{44}", emailObject.Email); //MCU Alert to MCU Admin
                    //emailContent = emailContent.Replace("{57}", emailObject.ConnectionDetails); //Party Acceptance Email
                                        
                    emailContent = emailContent.Replace("{1}", emailObject.MailLogo);
                    emailContent = emailContent.Replace("{2}", emailObject.Recipient);
                    emailContent = emailContent.Replace("{3}", emailObject.MCUNames);
                    emailContent = emailContent.Replace("{4}", emailObject.ConferenceName);
                    emailContent = emailContent.Replace("{5}", emailObject.UniqueId);
                    emailContent = emailContent.Replace("{6}", emailObject.StartDatetime);
                    emailContent = emailContent.Replace("{7}", emailObject.Setup);
                    emailContent = emailContent.Replace("{8}", emailObject.Teardown);
                    emailContent = emailContent.Replace("{9}", emailObject.Duration);
                    emailContent = emailContent.Replace("{10}", emailObject.Requestor);
                    emailContent = emailContent.Replace("{65}", emailObject.Host); //FB 2419
                    emailContent = emailContent.Replace("{11}", emailObject.Description);
                    emailContent = emailContent.Replace("{12}", emailObject.RecurrencePattern);
                    emailContent = emailContent.Replace("{13}", emailObject.LocationInfo);
                    emailContent = emailContent.Replace("{14}", emailObject.MCUInfo);
                    emailContent = emailContent.Replace("<td>{15}</td>", emailObject.AudioInfo); //FB 2439
                    emailContent = emailContent.Replace("<td>{16}</td>", emailObject.CustomOptions); //FB 2439
                    emailContent = emailContent.Replace("<td>{63}</td>", emailObject.ConciergeSupport); //FB 2632
                    emailContent = emailContent.Replace("{17}", emailObject.ConfDatetime);
                    emailContent = emailContent.Replace("{18}", emailObject.PendingStatus);
                    emailContent = emailContent.Replace("{19}", emailObject.Workordername);
                    emailContent = emailContent.Replace("{20}", emailObject.RoomsAssigned);
                    emailContent = emailContent.Replace("{21}", emailObject.InventorySetused);
                    emailContent = emailContent.Replace("{22}", emailObject.Person_in_charge); 
                    emailContent = emailContent.Replace("{23}", emailObject.Start_byDatetime);
                    emailContent = emailContent.Replace("{24}", emailObject.End_byDatetime);
                    emailContent = emailContent.Replace("{25}", emailObject.Deliverytype);
                    emailContent = emailContent.Replace("{26}", emailObject.Deliverycost);
                    emailContent = emailContent.Replace("{27}", emailObject.Servicecharge);
                    emailContent = emailContent.Replace("{28}", emailObject.Currentstatus);
                    emailContent = emailContent.Replace("{29}", emailObject.Requestitem);
                    emailContent = emailContent.Replace("{30}", emailObject.Totalcost);
                    emailContent = emailContent.Replace("{31}", emailObject.Tobecompleted);
                    emailContent = emailContent.Replace("{32}", emailObject.WorkorderComment);
                    emailContent = emailContent.Replace("{33}", emailObject.ServiceType);
                    emailContent = emailContent.Replace("{34}", emailObject.Cateringmenus);
                    emailContent = emailContent.Replace("{35}", emailObject.DeliveryDatetime);
                    emailContent = emailContent.Replace("{36}", emailObject.WebsiteURL);
                    emailContent = emailContent.Replace("{37}", emailObject.TechContact); 
                    emailContent = emailContent.Replace("{38}", emailObject.TechEmail);
                    emailContent = emailContent.Replace("{39}", emailObject.TechPhone);
                    emailContent = emailContent.Replace("{40}", emailObject.Attachments);
                    emailContent = emailContent.Replace("{43}", emailObject.Name);
                    emailContent = emailContent.Replace("{44}", emailObject.Email);
                    emailContent = emailContent.Replace("{57}", emailObject.ConnectionDetails); //Party Acceptance Email
                    emailContent = emailContent.Replace("{58}", emailObject.CurrencyFormat); //FB 1933
                    emailContent = emailContent.Replace("<td>{61}</td>", Password); //FB 2051 //FB 2419 //FB 2439
                    emailContent = emailContent.Replace("{64}", emailObject.SurveyURL); //FB 2348
                    emailContent = emailContent.Replace("{66}", PartyLink); //FB 2419
					emailContent = emailContent.Replace("<td>{67}</td>", emailObject.PersonalAudioInfo); //FB 2439
                    emailContent = emailContent.Replace("{68}", emailObject.GuestLocations); //FB 2426
                    emailContent = emailContent.Replace("{62}", VMR); //FB 2419 //FB 24139
                    emailContent = emailContent.Replace("{70}", emailObject.CallLaunch); //FB 2522
                    emailContent = emailContent.Replace("{71}", emailObject.ConferenceDialinNumber); // FB 2675
                    emailContent = emailContent.Replace("{79}", VMRInternalBridge); // FB 2690 //FB 2659
                    emailContent = emailContent.Replace("{73}", VMRExternalBridge); // FB 2690 
					emailContent = emailContent.Replace("{78}", emailObject.VirtualMeetingRooms); // FB 2690
                    // FB 2693 Starts
                    emailContent = emailContent.Replace("{77}", emailObject.PCDetails);
                    emailContent = emailContent.Replace("{78}", emailObject.RoomVMRLink); //FB 2727


                    if (emailContent.Contains("<br /><br /><br /><br/><br /><br/>")) 
                        emailContent = emailContent.Replace("<br /><br /><br /><br/><br /><br/>", "");
                    // FB 2693 Ends
					emailContent = emailContent.Replace("{72}", emailObject.DesktopLink); // FB 2659
                    //FB 2501 Starts
                    if(emailContent.Contains("<tr><td></td></tr>")) 
                        emailContent = emailContent.Replace("<tr><td></td></tr>", "");
                    
                    if (emailContent.Contains("<tr></tr>")) 
                        emailContent = emailContent.Replace("<tr></tr>", "");
                    //FB 2501 Ends
                }
            }
            catch (Exception e)
            {
                m_log.Error("Email-BuildEmailBody: ", e);
            }
        }
        #endregion

        #region SetConfRoomString
        /// <summary>
        /// SetConfRoomString
        /// </summary>
        private void SetConfRoomString()
        {
            StringBuilder locs = new StringBuilder();
            vrmRoom Loc = null;
            vrmUser asst = null;
            vrmState objState = null;
            vrmCountry objCountry = null;
            string ipisdnaddress,bridgeIP="",BridgeEXTNo = "N/A",bridgeisdnprfix = "",confLocs;
            string roomDateInfo = "", endpointInfo = "", roomAddress = "", assFirstName="";
            string assistantInfo = "<BR>", assLastName, assEmail = "";
            int roomTimeZone = 0,assTimeZone=0;
            List<vrmConfBridge> vConfbridgeList = null; //FB 2636
            List<ICriterion> ConfBridgeList = null;
            try
            {
                List<string> Locations = new List<string>();
                List<string> VMRooms = new List<string>(); //FB 2448

                if (confRoomList == null)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf.confid));
                    
                    if (conf.instanceid > 0)
                        criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    else
                        criterionList.Add(Expression.Eq("instanceid", 1));

                    confRoomList = m_vrmConfRoomDAO.GetByCriteria(criterionList);
                    
                }
                //Changes to the Room list: 10-24-07 (AG)
                //		Tiers for each room will be specified. 
                //		Conference time in room�s time zone will be specified for each room. This will be calculated from  
                //		the assistant-in-charge time zone.
                //		For each room the assistant-in-charge name, email, phone number will be displayed.
                //		For each room�s endpoint, the connection information would be displayed. This will only be shown 
                //			(audio-only & audio/video conference.)

                // FB 2675 Starts   
                ConfBridgeList = new List<ICriterion>();
                if (confRoomList.Count > 0)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    ConfBridgeList.Add(Expression.Eq("ConfID", conf.confid));
                    if (conf.instanceid > 0)
                        ConfBridgeList.Add(Expression.Eq("InstanceID", conf.instanceid));
                    else
                        ConfBridgeList.Add(Expression.Eq("InstanceID", 1));
                    vConfbridgeList = m_IconfBridge.GetByCriteria(ConfBridgeList);
                }

                if (vConfbridgeList.Count > 0)
                {
                    BridgeEXTNo = "";
                    if (vConfbridgeList[0].E164Dialnumber!= "")//FB 2659
                        BridgeEXTNo = vConfbridgeList[0].E164Dialnumber.ToString();
                    if (vConfbridgeList.Count > 1)
                    {
                        BridgeEXTNo = "";
                        for (int i = 0; i < vConfbridgeList.Count; i++)
                        {
                            if (vConfbridgeList[i].E164Dialnumber != "")//FB 2659
                                BridgeEXTNo += vConfbridgeList[i].BridgeName + " - " + vConfbridgeList[i].E164Dialnumber.ToString() + "; ";
                        }
                    }
                }
                emailObject.ConferenceDialinNumber = BridgeEXTNo.ToString();
                vConfbridgeList = new List<vrmConfBridge>();
                ConfBridgeList = new List<ICriterion>();

                if (sysSettings.EnableCloudInstallation > 0)//2659
                    FetchDesktopURLandDialNumber();

                // FB 2675 Ends

                foreach (vrmConfRoom room in confRoomList)
                {
                    ConfBridgeList = new List<ICriterion>();
                    ConfBridgeList.Add(Expression.Eq("ConfID", conf.confid));
                    if (conf.instanceid > 0)
                        ConfBridgeList.Add(Expression.Eq("InstanceID", conf.instanceid));
                    else
                        ConfBridgeList.Add(Expression.Eq("InstanceID", 1));
                    Loc = m_roomDAO.GetById(room.roomId);
                    if (Loc.Extroom == 0) //FB 2426
                    {
                        int assistant = Loc.assistant;
                        //FB 2469 Starts
                        roomTimeZone = Loc.TimezoneID;
                        DateTime roomDate = conf.confdate;
                        timeZone.GMTToUserPreferedTime(roomTimeZone, ref roomDate);
                        timeZoneData roomTz = new timeZoneData();
                        timeZone.GetTimeZone(roomTimeZone, ref roomTz);

                        roomDateInfo = " (" + roomDate.ToString("g") + " " + roomTz.TimeZone + ")"; 
                        //FB 2469 Ends

                        if (assistant == 0)
                            assistant = 11;

                        assistantInfo = "<BR>";// FB 2631
                        endpointInfo = "";
                        roomAddress = "";//FB 2189

                        if (orgInfo != null && orgInfo.EnableRoomAdminDetails > 0)// FB 2631
                        {
                            asst = m_IuserDao.GetByUserId(assistant);
                            if (asst != null)
                            {
                                assFirstName = asst.FirstName;
                                assLastName = asst.LastName;
                                assEmail = asst.Email;
                                assTimeZone = asst.TimeZone;


                                // FB 140 time zones in user time (AG 04/14/08)
                                timeZone.GMTToUserPreferedTime(assTimeZone, ref roomDate);

                                assEmail.Trim();
                                assFirstName.Trim();
                                assLastName.Trim();

                                timeZoneData aTz = new timeZoneData();
                                timeZone.GetTimeZone(assTimeZone, ref aTz);

                                //  roomDateInfo = " (" + roomDate.ToString("g") + " " + aTz.TimeZone + ")<BR>"; //Email Text Changes
                                assistantInfo = assFirstName + " " + assLastName + "(";

                                assistantInfo += "Email - " + assEmail;
                                assistantInfo += ")<BR/>"; //FB 1860

                            }
                        }

                        // if audio only or audio video or point to point
                        if (conf.conftype == 6 || conf.conftype == 2 || conf.conftype == 4) //FB 2401)
                        {

                            ipisdnaddress = room.ipisdnaddress;
                            bridgeIP = room.bridgeIPISDNAddress;
                             
                            //FB 2610 
                            if (room.bridgeid > 0) //FB 2636
                            {
                                ConfBridgeList.Add(Expression.Eq("BridgeID", room.bridgeid));
                                vConfbridgeList = m_IconfBridge.GetByCriteria(ConfBridgeList);
                            }

                            BridgeEXTNo = "N/A"; 
                            if (room.BridgeExtNo != "")
                                BridgeEXTNo = room.BridgeExtNo;

                            if (vConfbridgeList.Count > 0)
                            {
                                if (vConfbridgeList[0].E164Dialnumber !="")//FB 2636//FB 2659
                                    BridgeEXTNo = vConfbridgeList[0].E164Dialnumber.ToString();
                                if (!String.IsNullOrEmpty(conf.DialString) && conf.DialString.Trim() != "")//FB 2441 II
                                    BridgeEXTNo = conf.DialString;
                            }

                            // FB 1946 Starts //FB 2610 Starts
                            if (room.connectionType == 1)
                            {
                                if (room.bridgeAddressType == 4)
                                {
                                    bridgeisdnprfix = "";
                                    if (room.prefix != null)
                                        bridgeisdnprfix = room.prefix + " - " + room.bridgeIPISDNAddress;
                                    else
                                        bridgeisdnprfix = "N/A";
                                    endpointInfo = " Endpoint (Endpoint Address - " + ipisdnaddress + "; Connect Type - Dial In to MCU";
                                    if (orgInfo.IsBridgeExtNo == 1) //FB 2610
                                        endpointInfo += "; Bridge  # - " + BridgeEXTNo + ")<BR>"; 
                                    else
                                        endpointInfo += ")<BR>";
                                }
                                else
                                {
                                    endpointInfo = " Endpoint (Endpoint Address - " + ipisdnaddress + "; Connect Type - Dial In to MCU";
                                    if (orgInfo.IsBridgeExtNo == 1) //FB 2610
                                        endpointInfo += "; Bridge  # - " + BridgeEXTNo + ")<BR>";
                                    else
                                        endpointInfo += ")<BR>";
                                }
                            }
                            else
                            {
                                if (room.bridgeAddressType == 4)
                                    endpointInfo = " Endpoint (Endpoint Address - " + ipisdnaddress + "; Connect Type - Dial Out from MCU)<BR>";
                                else
                                    endpointInfo = " Endpoint (Endpoint Address - " + ipisdnaddress + "; Connect Type - Dial Out from MCU)<BR>";
                            }
                            // FB 1946 Ends
                        }
                        //connection info...
                        confLocs = "";
                        //FB 2189 Starts
                        objState = null;
                        objCountry = null;
                        if (Loc.State > 0)
                            objState = m_IStateDAO.GetById(Loc.State);
                        if (Loc.Country > 0)
                            objCountry = m_ICountryDAO.GetById(Loc.Country);
                        if (Loc.RoomFloor != "")
                            roomAddress = Loc.RoomFloor;
                        if (Loc.RoomNumber != null && Loc.RoomNumber != "") //FB 2778
                            roomAddress += "  # " + Loc.RoomNumber + ", ";
                        if (Loc.Address1 != null && Loc.Address1 != "") //FB 2778
                            roomAddress += Loc.Address1 + ", ";
                        if (Loc.Address2 != null && Loc.Address2 != "") //FB 2778
                            roomAddress += Loc.Address2 + ", " + "<br />";
                        if (Loc.City != null && Loc.City != "") //FB 2778
                            roomAddress += Loc.City + ", ";
                        if (objState != null)
                        {
                            if (objState.State.ToString() != "")
                                roomAddress += objState.State.ToString() + ", ";
                        }
                        if (objCountry != null)
                        {
                            if (objCountry.CountryName.ToString() != "")
                                roomAddress += objCountry.CountryName.ToString();
                        }
                        if (Loc.Zipcode != null && Loc.Zipcode != "") //FB 2778
                            roomAddress += " - " + Loc.Zipcode + "<br />";

                        //roomAddress = Loc.RoomFloor + " , # " + Loc.RoomNumber + " ," + Loc.Address1 + " , " + Loc.Address2 + "<br />"; 
                        //roomAddress += Loc.City + " , " + objState.State.ToString() + " ," + objCountry.CountryName.ToString() + " - " + Loc.Zipcode + "<br />";
                        if (Loc.Maplink != null)
                        {
                            if (Loc.Maplink != "")
                            {
                                roomAddress += " Map Link : Please <b>click </b><a href=" + Loc.Maplink + "><b>here</b></a> ";
                            }
                        }
                        //FB 2189 Ends
                       
                        confLocs = "<strong>"; //FB 2448 start
                        if (Loc.IsVMR == 0)
                        {
                            confLocs += Loc.tier2.tier3.Name + " > "
                                     + Loc.tier2.Name + " > ";
                        }
                        confLocs += Loc.Name;

                        if (Loc.ExternalNumber == "")
                            Loc.ExternalNumber = "N/A";
                        if (Loc.InternalNumber == "")
                            Loc.InternalNumber = "N/A";

                        if (Loc.IsVMR == 1)
                        {
                            confLocs += " > ExternalNumber: " + Loc.ExternalNumber
                                      + " > InternalNumber: " + Loc.InternalNumber;

                            emailObject.RoomVMRLink += Loc.VMRLink.Trim();
                        }

                        confLocs += "</strong> ";  //FB 2448 end

                        if (Loc.IsVMR == 1) //FB 2690
                        {
                            RoomInternalNum = Loc.Name + " > " + Loc.InternalNumber;
                            RoomExternalNum = Loc.Name + " > " + Loc.ExternalNumber;
                        }
						if (Loc.IsVMR == 0)// FB 2690
                        {
	                        if (orgInfo.EnableConfTZinLoc.ToString() == "1") //FB 2469
	                            confLocs += roomDateInfo; 
	                        confLocs += "<br/>" + roomAddress + "<br/>"; //FB 2189
						}

                        if (orgInfo.EnableEPDetails.ToString() == "1") //FB 2401
                        {
                            if (Loc.IsVMR == 0)
                            {
                                if (endpointInfo.Length > 0)
                                    confLocs += endpointInfo;
                                confLocs += assistantInfo;
                            }
                        }

                        if (Loc.IsVMR == 1) //FB 2448
                            VMRooms.Add(confLocs);
                        else
                            Locations.Add(confLocs);
                    }
                }

                for (int i = 0; i < Locations.Count; i++)
                {
                    if (i > 0)
                        locs.Append("<br>");

                    locs.Append(Locations[i]);

                    if (i > 0)
                        locs.Append("<br />");
                }
                emailObject.LocationInfo = locs.ToString();
                //FB 2448 start
                locs = new StringBuilder();
                for (int i = 0; i < VMRooms.Count; i++)
                {
                    if (i > 0)
                        locs.Append("<br>");

                    locs.Append(VMRooms[i]);

                    if (i > 0)
                        locs.Append("<br />");
                }
                emailObject.VirtualMeetingRooms = locs.ToString();

                if (emailObject.VirtualMeetingRooms == "")
                    emailObject.VirtualMeetingRooms = "N/A";
                //FB 2448 end
            }
            catch (Exception e)
            {
                m_log.Error("Email-SetConfRoomString: ", e);
            }
        }
        #endregion

        //FB 2426 Start

        #region SetConfGuestRoomString
        /// <summary>
        /// SetConfRoomString
        /// </summary>
        private void SetConfGuestRoomString()
        {
            StringBuilder locs = new StringBuilder();
            List<int> gusetrooms = new List<int>();
            try
            {
                List<string> Locations = new List<string>();

                if (confRoomList == null)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf.confid));

                    if (conf.instanceid > 0)
                        criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    else
                        criterionList.Add(Expression.Eq("instanceid", 1));

                    criterionList.Add(Expression.Eq("Extroom", 1));
                    confRoomList = m_vrmConfRoomDAO.GetByCriteria(criterionList);
                }

                if (confRoomList != null)
                {
                    if (confRoomList.Count > 0)
                    {
                        foreach (vrmConfRoom room in confRoomList)
                        {
                            vrmRoom Loc = m_roomDAO.GetById(room.roomId);
                            //FB 2469 Starts
                            int roomTimeZone = Loc.TimezoneID;
                            DateTime guestroomDate = conf.confdate;
                            timeZone.GMTToUserPreferedTime(roomTimeZone, ref guestroomDate);
                            timeZoneData roomTz = new timeZoneData();
                            timeZone.GetTimeZone(roomTimeZone, ref roomTz);

                            string roomDateInfo = " (" + guestroomDate.ToString("g") + " " + roomTz.TimeZone + ")";
                            //FB 2469 Ends
                            if (Loc.Extroom == 1) //FB 2426
                            {
                                int assistant = Loc.assistant;

                                // assistant info....
                                if (assistant == 0)
                                    assistant = 11;

                                string assistantInfo = "<BR>";// FB 2631
                                string endpointInfo = "";
                                string roomAddress = "";
                                string roomPhone = "";

                                if (orgInfo != null && orgInfo.EnableRoomAdminDetails > 0)// FB 2631
                                {

                                    vrmUser asst = m_IuserDao.GetByUserId(assistant);
                                    if (asst != null)
                                    {
                                        string assFirstName = asst.FirstName;
                                        string assLastName = asst.LastName;
                                        string assEmail = asst.Email;
                                        int assTimeZone = asst.TimeZone;

                                        DateTime roomDate = conf.confdate;

                                        timeZone.GMTToUserPreferedTime(assTimeZone, ref roomDate);

                                        assEmail.Trim();
                                        assFirstName.Trim();
                                        assLastName.Trim();

                                        timeZoneData aTz = new timeZoneData();
                                        timeZone.GetTimeZone(assTimeZone, ref aTz);

                                        //assistantInfo = assFirstName + " " + assLastName + "(";

                                        assistantInfo = "<BR/>Contact Email > " + assEmail;
                                        assistantInfo += "<BR/>";

                                    }
                                }

                                // if audio only or audio video or point to point
                                if (conf.conftype == 6 || conf.conftype == 2 || conf.conftype == 4) //FB 2401)
                                {

                                    string ipisdnaddress = room.ipisdnaddress;
                                    string bridgeIP = room.bridgeIPISDNAddress;

                                    if (room.connectionType == 1)
                                    {
                                        if (room.bridgeAddressType == 4)
                                        {
                                            string bridgeisdnprfix = "";
                                            if (room.prefix != null)
                                                bridgeisdnprfix = room.prefix + " - " + room.bridgeIPISDNAddress;
                                            else
                                                bridgeisdnprfix = "N/A";
                                            endpointInfo = " Endpoint (Endpoint - " + ipisdnaddress + "; Connect Type - Dial In to MCU";
                                            if (bridgeIP.Trim() != "")
                                                endpointInfo += "; Bridge - " + bridgeisdnprfix + ")<BR>";
                                            else
                                                endpointInfo += ")<BR>";
                                        }
                                        else
                                        {
                                            endpointInfo = " Endpoint (Endpoint - " + ipisdnaddress + "; Connect Type - Dial In to MCU";
                                            if (bridgeIP.Trim() != "")
                                                endpointInfo += "; Bridge - " + bridgeIP + ")<BR>";
                                            else
                                                endpointInfo += ")<BR>";
                                        }
                                    }
                                    else
                                    {
                                        if (room.bridgeAddressType == 4)
                                            endpointInfo = " Endpoint (Endpoint - " + ipisdnaddress + "; Connect Type - Dial Out from MCU)<BR>";
                                        else
                                            endpointInfo = " Endpoint (Endpoint - " + ipisdnaddress + "; Connect Type - Dial Out from MCU)<BR>";
                                    }
                                }
                                //connection info...
                                string confLocs;
                                //FB 2189 Starts
                                vrmState objState = null;
                                vrmCountry objCountry = null;
                                if (Loc.State > 0)
                                    objState = m_IStateDAO.GetById(Loc.State);
                                if (Loc.Country > 0)
                                    objCountry = m_ICountryDAO.GetById(Loc.Country);
                                if (Loc.RoomFloor != "")
                                    roomAddress = Loc.RoomFloor;
                                if (Loc.RoomNumber != null && Loc.RoomNumber != "") //FB 2778
                                    roomAddress += "  # " + Loc.RoomNumber;
                                if (Loc.Address1 != null && Loc.Address1 != "") //FB 2778
                                    roomAddress += " , " + Loc.Address1;
                                if (Loc.Address2 != null && Loc.Address2 != "") //FB 2778
                                    roomAddress += " , " + Loc.Address2 + "<br />";
                                if (Loc.City != null && Loc.City != "") //FB 2778
                                    roomAddress += " , " + Loc.City;
                                if (objState != null)
                                {
                                    if (objState.State.ToString() != "")
                                        roomAddress += " , " + objState.State.ToString();
                                }
                                if (objCountry != null)
                                {
                                    if (objCountry.CountryName.ToString() != "")
                                        roomAddress += " , " + objCountry.CountryName.ToString();
                                }
                                if (Loc.Zipcode != null && Loc.Zipcode != "") //FB 2778
                                    roomAddress += " - " + Loc.Zipcode + "<br />";

                                if (Loc.Maplink != null)
                                {
                                    if (Loc.Maplink != "")
                                    {
                                        roomAddress += " Map Link : Please <b>click </b><a href=" + Loc.Maplink + "><b>here</b></a> ";
                                    }
                                }
                                roomPhone = Loc.RoomPhone.ToString().Trim();

                                confLocs = "<strong>" +
                                            Loc.tier2.tier3.Name + " > " +
                                            Loc.tier2.Name + " > " +
                                            Loc.Name + "</strong>";
                                if (orgInfo.EnableConfTZinLoc.ToString() == "1") //FB 2469
                                    confLocs += roomDateInfo; 
                                if (roomPhone != "")
                                    confLocs += "<br/>Contact Phone >" + roomPhone;

                                if (assistantInfo != "")
                                    confLocs += assistantInfo;

                                if (orgInfo.EnableEPDetails.ToString() == "1")
                                {
                                    if (room.ipisdnaddress != "")
                                        confLocs += "Endpoint Address > " + room.ipisdnaddress + "<br/>";
                                }
                                Locations.Add(confLocs);
                            }
                        }
                        for (int i = 0; i < Locations.Count; i++)
                        {
                            if (i > 0)
                                locs.Append("<br>");

                            locs.Append(Locations[i]);

                            if (i > 0)
                                locs.Append("<br />");
                        }
                        emailObject.GuestLocations = locs.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("Email-SetConfGuestRoomString: ", e);
            }
        }
        #endregion

        //FB 2426 End

        #region SetConfMCUsString
        /// <summary>
        /// SetConfMCUsString
        /// </summary>
        private void SetConfMCUsString()
        {
            try
            {
                StringBuilder mcuString = new StringBuilder();

                /**************     Code added for FB# 1017 - start   ***************/
                /* This is to display the MCU details in the Email Body         */
                if (conf.mcuList != null)
                {
                    if (conf.mcuList.Count > 0)
                    {
                        for(int lp=0;lp< conf.mcuList.Count;lp++)
                        {
                            if (lp > 0)
                                mcuString.Append("<br>");

                            mcuString.Append(conf.mcuList[lp].BridgeName);

                            if (lp > 0)
                                mcuString.Append("<br />");
                        }
                    }
                }
                emailObject.MCUInfo = mcuString.ToString();
            }
            catch (Exception e)
            {
                m_log.Error("Email-SetConfMCUsString: ", e);
            }
        }
        #endregion

        #region SetRecurrencePatern
        /// <summary>
        /// SetRecurrencePatern
        /// </summary>
        private void SetRecurrencePatern()
        {
            try
            {
                string recurring = "N/A";
                List<ICriterion> recurInfo = new List<ICriterion>();

                recurInfo.Add(Expression.Eq("confid", conf.confid));
                List<vrmRecurInfo> recurPattern = m_vrmConfRecurDAO.GetByCriteria(recurInfo, true);
                if (recurPattern.Count > 0)
                {
                    if (recurPattern[0].RecurringPattern != null)//FB 1044
                        recurring = recurPattern[0].RecurringPattern;
                }
                emailObject.RecurrencePattern = recurring;
            }
            catch (Exception e)
            {
                m_log.Error("Email-SetRecurrencePatern: ", e);
            }
        }
        #endregion

        #region emailLocations
        /// <summary>
        /// emailLocations
        /// </summary>
        /// <returns></returns>
        private bool emailLocations()
        {
            try
            {
                if (orgInfo == null) //FB 2632
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                emailObject.CustomOptions = addConfCustomAttr("RoomAdmin");

                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                    emailObject.ConciergeSupport = ConciergeSupport();

                //FB 2693 Starts
                if (conf.isPCconference > 0)
                {
                    List<ICriterion> criterionPCList = new List<ICriterion>();
                    criterionPCList.Add(Expression.Eq("userid", conf.owner));
                    criterionPCList.Add(Expression.Eq("PCId", conf.pcVendorId));
                    confPCLst = m_IUserPCDAO.GetByCriteria(criterionPCList);
                }
                if (confPCLst != null && confPCLst.Count > 0)
                    emailObject.PCDetails = BuildPCContent(confPCLst[0]);
                //FB 2693 Ends

                vrmUser objUser = null;
                String locationString = "";
                string AsstEmail = "";
                int userEmailLang = 1;  //default en
                int userLanguage = 1;   //default en

                if (confRoomList == null)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf.confid));
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    confRoomList = m_IconfRoom.GetByCriteria(criterionList);
                }
                vrmConfRoom cr = null;
                for (int r = 0; r < confRoomList.Count; r++ )
                {
                    cr = confRoomList[r];

                    locationString = "";

                    locationString = "<strong>" + cr.Room.tier2.tier3.Name + ">" +
                        cr.Room.tier2.Name + ">" + cr.Room.Name + "</strong>";

                    if (cr.Extroom == 0 && cr.Room.IsVMR == 0)//FB 2426 //FB 2448
                        emailObject.LocationInfo = locationString;

                    objUser = new vrmUser();
                    objUser = m_IuserDao.GetByUserId(cr.Room.assistant);

                    AsstEmail = objUser.Email;
                    emailObject.Recipient = objUser.FirstName + " " + objUser.LastName; //FB 1675
					emailObject.Recipient = emailObject.Recipient.Replace("!!","'").Replace("||","\""); //Recipient Name // FB 1888//FB 1675
                    userEmailLang = objUser.EmailLangId;
                    userLanguage = objUser.Language;
                    //userDateFormat = ((objUser.DateFormat.Trim() == "") ? "MM/dd/yyyy" : objUser.DateFormat.Trim());
                    SetEmailDateFormat(objUser.DateFormat.Trim(),ref userDateFormat);//FB 2555
                    //userTimeFormat = ((objUser.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                    SetEmailTimeFormat(objUser.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                    tzDisplay = ((objUser.Timezonedisplay.Trim() == "") ? "0" : objUser.Timezonedisplay.Trim());
                    usrTimezone = objUser.TimeZone;

                    SetEmailLanguage(ref userLanguage, ref userEmailLang);
                    FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.RoomAdmin);
                    SetConfDateTime(eEmailType.RoomAdmin);
                    BuildEmailBody();
                    string roomMailBody = emailContent;
                    
                    vrmEmail theEmail = new vrmEmail();
                    theEmail.emailTo = AsstEmail;
                    theEmail.emailFrom = confHost.Email;
                    theEmail.Subject = emailSubject;
                    theEmail.Message = emailContent;
                    theEmail.UUID = getEmailUUID();
                    theEmail.orgID = conf.orgId;

                    if (theEmail.emailTo.Trim().Length > 0)
                        m_IemailDao.Save(theEmail);

                    string strNotifyEmails = cr.Room.notifyemails;
                    if (strNotifyEmails.Length > 1)
                    {
                        roomMailBody = roomMailBody.Replace("{2}", "");    //Remove Recipient Placeholder for notify email addresses
                        while (strNotifyEmails.Length > 0)
                        {
                            int x = strNotifyEmails.IndexOf(";");
                            string value = string.Empty;
                            if (x > 0)
                            {
                                value = strNotifyEmails.Substring(0, x);
                                strNotifyEmails = strNotifyEmails.Substring(x + 1, strNotifyEmails.Length - (x + 1));
                            }
                            else
                            {
                                value = strNotifyEmails;
                                strNotifyEmails = string.Empty;
                            }
                            if (value.Length > 0 && !AsstEmail.ToLower().Equals(value.ToLower()))
                            {
                                theEmail = new vrmEmail();
                                theEmail.emailTo = value;
                                theEmail.emailFrom = confHost.Email;
                                theEmail.Message = roomMailBody;
                                theEmail.Subject = emailSubject;
                                theEmail.UUID = getEmailUUID();
                                theEmail.orgID = conf.orgId; //Added for FB 1710

                                m_IemailDao.Save(theEmail);
                            }
                        }
                    }
                    //FB 2415 Start
                    string AVsupportEmail = cr.Room.AVOnsiteSupportEmail;
                    if (AVsupportEmail.Length > 1)
                    {
                        if (conf.OnSiteAVSupport > 0) // (onsiteAV == true) //FB 2632 Starts
                        {
                            FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.AVOnsitesupportMail);
                            SetConfDateTime(eEmailType.AVOnsitesupportMail);
                            emailContent = emailContent.Replace("{2}", "");
                            BuildEmailBody();
                            theEmail = new vrmEmail();
                            theEmail.emailTo = AVsupportEmail;
                            theEmail.emailFrom = confHost.Email;
                            theEmail.Message = emailContent;
                            theEmail.Subject = emailSubject;
                            theEmail.UUID = getEmailUUID();
                            theEmail.orgID = conf.orgId;
                            m_IemailDao.Save(theEmail);
                        }
                    }
                    //FB 2415 End
                }
            }
            catch (Exception ex)
            {
                m_log.Error("Cannot save email Locations", ex);
                throw ex;
            }
            return true;
        }
        #endregion

        #region sendMCUAdminEmail
        /// <summary>
        /// sendMCUAdminEmail
        /// </summary>
        /// <returns></returns>
        private bool sendMCUAdminEmail()
        {
            try
            {
                if (orgInfo == null) //FB 2632
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                int userPrefLang = 1;
                int userEmailLang = 1;
                string confcodepin = "";
                
                if (conf.conftype == 2)
                    confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "M", 0); //FB 2381 //FB 2419

                emailObject.AudioInfo = confcodepin;  //FB 1642
                emailObject.CustomOptions = addConfCustomAttr("McuAdmin"); //FB 1718, 1767

                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                   emailObject.ConciergeSupport = ConciergeSupport();

                //FB 2693 Starts
                if (conf.isPCconference > 0)
                {
                    List<ICriterion> criterionPCList = new List<ICriterion>();
                    criterionPCList.Add(Expression.Eq("userid", conf.owner));
                    criterionPCList.Add(Expression.Eq("PCId", conf.pcVendorId));
                    confPCLst = m_IUserPCDAO.GetByCriteria(criterionPCList);
                }
                if (confPCLst != null && confPCLst.Count > 0)
                    emailObject.PCDetails = BuildPCContent(confPCLst[0]);
                //FB 2693 Ends

                vrmUser user = null;
                vrmMCU mcu = null;
                vrmEmail theEmail = null;
                for (int m = 0; m < conf.mcuList.Count;m++)
                {
                    //FB 1920 Starts
                        if (organizationID != 11 && conf.mcuList[m].isPublic.ToString() == "1")
                                conf.mcuList[m].BridgeName = conf.mcuList[m].BridgeName + "(*)";
                    
                    emailObject.MCUInfo = conf.mcuList[m].BridgeName ;
                    // FB 1920 Ends

                    if (conf.StartMode == 0) //FB 2522
                        callLaunch = "Yes";
                    emailObject.CallLaunch = callLaunch;

                    mcu = conf.mcuList[m];
                    user = m_IuserDao.GetByUserId(mcu.Admin);

                    userEmailLang = user.EmailLangId;
                    userPrefLang = user.Language;
                    //userDateFormat = ((user.DateFormat.Trim() == "") ? "MM/dd/yyyy" : user.DateFormat.Trim());
                    SetEmailDateFormat(user.DateFormat.Trim(),ref userDateFormat);//FB 2555
                    //userTimeFormat = ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                    SetEmailTimeFormat(user.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                    tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                    usrTimezone = user.TimeZone;

                    emailObject.Recipient = user.FirstName + " " + user.LastName;

                    SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                    FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.MCUAdmin);
                    SetConfDateTime(eEmailType.MCUAdmin);
                    BuildEmailBody();

                    /***    Code Commented to meet FB#: 1017    ***/
                    //Website URL link display codelines are commented

                    theEmail = new vrmEmail();
                    theEmail.emailTo = user.Email;
                    theEmail.emailFrom = confHost.Email;
                    theEmail.Message = emailContent;
                    theEmail.Subject = emailSubject;
                    theEmail.orgID = conf.orgId; //Added for FB 1710

                    theEmail.UUID = getEmailUUID();
                    m_IemailDao.Save(theEmail);
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Cannot save email", e);
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region sendWorkOrderDeleteEmail
        /// <summary>
        /// sendWorkOrderDeleteEmail
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="wkList"></param>
        /// <returns></returns>
        public bool sendWorkOrderDeleteEmail(List<WorkOrder> wkList)
        {
            try
            {
                if (wkList == null)
                    return false;

                if (wkList.Count <= 0)
                    return false;

                conf = m_vrmConfDAO.GetByConfId(wkList[0].ConfID, wkList[0].InstanceID);
                
                if (conf == null)
                    return false;

                confHost = m_IuserDao.GetByUserId(conf.owner);

                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);
                organizationID = conf.orgId;

                emailObject = new sConferenceEmail();

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                //FB 2189 Starts
                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;
                //FB 2189 Ends

               
                emailObject.MailLogo = AttachEmailLogo(organizationID);

                SetConfRoomString();
                SetConfGuestRoomString();//FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "") 
                    Password = "N/A";
                emailObject.Password = Password;

                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;

                int userPrefLang = 1;
                int userEmailLang = 1;

                WorkOrder work;
                vrmUser user;
                vrmEmail email;

                ArrayList ins = new ArrayList();//FB 2284
                ins.Add(wkList[0].InstanceID);    
                for (int w = 1; w < wkList.Count; w++)
                {
                    if(!(ins.Contains(wkList[w].InstanceID)))
                        ins.Add(wkList[w].InstanceID);
                }

                for (int w = 0; w < wkList.Count; w++)
                {
                    work = wkList[w];
                    if (ins.Count == 1 || (ins.Count > 1 && work.InstanceID == wkList[0].InstanceID))//FB 2284
                    {
                        user = m_IuserDao.GetByUserId(work.AdminID);
                        userEmailLang = user.EmailLangId;
                        userPrefLang = user.Language;
                        //userDateFormat = ((user.DateFormat.Trim() == "") ? "MM/dd/yyyy" : user.DateFormat.Trim());
                        SetEmailDateFormat(user.DateFormat.Trim(),ref userDateFormat);//FB 2555
                        //userTimeFormat = ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                        SetEmailTimeFormat(user.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                        tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                        usrTimezone = user.TimeZone;

                        emailObject.Recipient = user.FirstName + " " + user.LastName;
                        SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                        //Catering & HK email content are same as Inventory
                        //FB 2189 Starts
                        if (conf.ConfMode == 4)
                            conf.ConfMode = 3;
                        if (work.Type == woConstant.CATEGORY_TYPE_AV)
                        {
                            FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.InventoryAdmin);
                            SetConfDateTime(eEmailType.InventoryAdmin);
                        }
                        else if (work.Type == woConstant.CATEGORY_TYPE_HK)
                        {
                            FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.HKAdmin);
                            SetConfDateTime(eEmailType.HKAdmin);
                        }
                        else if (work.Type == woConstant.CATEGORY_TYPE_CA)
                        {
                            FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.CateringAdmin);
                            SetConfDateTime(eEmailType.CateringAdmin);
                        }
                        //FB 2189 Ends
                        BuildEmailBody();

                        email = new vrmEmail();
                        // FB 278, 279 email from, to was not being set (AG 04/29/08)
                        email.emailFrom = confHost.Email;
                        email.emailTo = user.Email;
                        email.Subject = emailSubject;
                        email.Message = emailContent;
                        email.UUID = getEmailUUID();
                        email.orgID = organizationID;
                        m_IemailDao.Save(email);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendWorkOrderDeleteEmail: ", e);
                return false;
            }
        }
        #endregion

        #region sendLevelEmail
        /// <summary>
        ///  sendLevelEmail
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="level"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool sendLevelEmail(vrmConference confObj, int level, vrmUser user)
        {
            try
            {
                int userEmailLang = 1;  //default en
                int userLanguage = 1;   //default en

                emailObject = new sConferenceEmail();
                conf = confObj;

                if (conf == null)
                    return false;

                SetWebsiteURL();
                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);
                SetHostLangFolder(confHost.Language);

                organizationID = conf.orgId;

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;
                
                emailObject.MailLogo = AttachEmailLogo(organizationID);
                emailObject.Recipient = user.FirstName + " " + user.LastName;

                //userDateFormat = ((user.DateFormat.Trim() == "") ? "MM/dd/yyyy" : user.DateFormat.Trim());
                SetEmailDateFormat(user.DateFormat.Trim(),ref userDateFormat);//FB 2555
                //userTimeFormat = ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                SetEmailTimeFormat(user.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "1" : user.Timezonedisplay.Trim());
                usrTimezone = user.TimeZone;
                userEmailLang = user.EmailLangId;
                userLanguage = user.Language;

                SetEmailLanguage(ref userLanguage, ref userEmailLang);

                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                    emailObject.ConciergeSupport = ConciergeSupport();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "")
                    Password = "N/A";
                emailObject.Password = Password;

                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;

                switch (level)
                {
                    case (int)LevelEntity.ROOM:

                        FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.RoomAppRequest);
                        SetConfDateTime(eEmailType.RoomAppRequest);
                        emailObject.CustomOptions = addConfCustomAttr("RoomApp"); //FB 1767
                        break;
                    case (int)LevelEntity.MCU:
                        
                        FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.MCUAppRequest);
                        SetConfDateTime(eEmailType.MCUAppRequest);
                        emailObject.CustomOptions = addConfCustomAttr("McuApp"); //FB 1767
                        int x = 0;
                        string mcuNames = "";
                        StringBuilder mcuString = new StringBuilder();
                        for (int m = 0; m < conf.mcuList.Count; m++)
                        {
                            if (m > 0)
                                mcuString.Append("<br />");

                            if (organizationID != 11 && conf.mcuList[m].isPublic.ToString() == "1" && conf.mcuList[m].BridgeName.IndexOf("(*)") < 0) //FB 1920
                                        conf.mcuList[m].BridgeName = conf.mcuList[m].BridgeName + "(*)";
                            
                            mcuString.Append(conf.mcuList[m].BridgeName);

                            if (m > 0)
                                mcuString.Append("<br />");

                            bool addit = false;
                            for (int a = 0; a < conf.mcuList[m].MCUApprover.Count; a++)
                            {
                                if (conf.mcuList[m].MCUApprover[a].approverid == user.userid)
                                    addit = true;
                                break;
                            }
                            if (addit)
                            {
                                if (x > 0)
                                    mcuNames += ", " + conf.mcuList[m].BridgeName;
                                else
                                    mcuNames = conf.mcuList[m].BridgeName;
                                x++;
                            }
                        }
                        emailObject.MCUInfo = mcuString.ToString();
                        emailObject.MCUNames = mcuNames;
                        break;
                    case (int)LevelEntity.DEPT:
                        //Not Handled in our system
                        break;
                    case (int)LevelEntity.SYSTEM:
                        FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.SystemAppRequest);
                        SetConfDateTime(eEmailType.SystemAppRequest);
                        emailObject.CustomOptions = addConfCustomAttr("SystemApp"); //FB 1767
                        break;
                }
                
                emailObject.WebsiteURL = websiteURL;
                BuildEmailBody();

                vrmEmail theEmail = new vrmEmail();
                theEmail.emailTo = user.Email;
                theEmail.emailFrom = confHost.Email;
                theEmail.Message = emailContent;
                theEmail.Subject = emailSubject;
                theEmail.orgID = conf.orgId; //Added for FB 1710

                theEmail.UUID = getEmailUUID();
                m_IemailDao.Save(theEmail);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendLevelEmail: (Cannot save email)- ", e);
                throw e;
            }
        }
        #endregion

        #region  sendBridgeBookedEmail
        /// <summary>
        /// sendBridgeBookedEmail
        /// </summary>
        /// <param name="baseConf"></param>
        /// <param name="vMCU"></param>
        /// <returns></returns>
        public bool sendBridgeBookedEmail(vrmConference baseConf, vrmMCUResources vMCU)
        {
            try
            {
                int userEmailLang = 1;  //default en
                int userLanguage = 1;   //default en

                conf = baseConf;
                if (conf == null)
                    return false;

                emailObject = new sConferenceEmail();

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;
                emailObject.MailLogo = AttachEmailLogo(organizationID);
                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;

                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();

                if (confHost == null)
                 confHost = m_IuserDao.GetByUserId(conf.owner); //FB 2419

                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "") 
                    Password = "N/A";
                emailObject.Password = Password;

               
                vrmUser sysAdmin = m_IuserDao.GetByUserId(11); //VRM administrator - myvrm default site admin
                confHost = m_IuserDao.GetByUserId(conf.owner); //Host Info
                
                vrmUser mcuAdmin = m_IuserDao.GetByUserId(vMCU.adminId);

                //Email to Host
                //FB 1920 start
                vrmMCU MCUDetail = m_ImcuDao.GetById(vMCU.bridgeId);
                if (organizationID != 11 && MCUDetail.isPublic == 1)
                    vMCU.bridgeName = vMCU.bridgeName + "(*)";
                
                //FB 1920 send
                emailObject.MCUNames = vMCU.bridgeName;
                emailObject.Name = mcuAdmin.FirstName + " " + mcuAdmin.LastName + " ";
                emailObject.Email = mcuAdmin.Email;

                //userDateFormat = ((confHost.DateFormat.Trim() == "") ? "MM/dd/yyyy" : confHost.DateFormat.Trim());
                SetEmailDateFormat(confHost.DateFormat.Trim(), ref userDateFormat); //FB 2555
                //userTimeFormat = ((confHost.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                SetEmailTimeFormat(confHost.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                tzDisplay = ((confHost.Timezonedisplay.Trim() == "") ? "1" : confHost.Timezonedisplay.Trim());
                usrTimezone = confHost.TimeZone;
                userEmailLang = confHost.EmailLangId;
                userLanguage = confHost.Language;
                
                emailObject.Recipient = confHost.FirstName + " " + confHost.LastName;

                SetEmailLanguage(ref userLanguage, ref userEmailLang);
                FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.MCUAlert_Host);
                SetConfDateTime(eEmailType.MCUAlert_Host);
                BuildEmailBody();
                if (orgInfo.MCUAlert == (int)eEmailMCUAlert.Host || orgInfo.MCUAlert == (int)eEmailMCUAlert.Both)//FB 2472
                {
                    vrmEmail email = new vrmEmail();
                    email.emailFrom = sysAdmin.Email;
                    email.emailTo = confHost.Email;
                    email.Subject = emailSubject;
                    email.Message = emailContent;
                    email.UUID = getEmailUUID();
                    email.orgID = conf.orgId; //Added for FB 1710
                    m_IemailDao.Save(email);
                }
                //FB 1013 Send email to MCU assist
                //userDateFormat = ((mcuAdmin.DateFormat.Trim() == "") ? "MM/dd/yyyy" : mcuAdmin.DateFormat.Trim());
                SetEmailDateFormat(mcuAdmin.DateFormat.Trim(), ref userDateFormat); //FB 2550
                //userTimeFormat = ((mcuAdmin.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                SetEmailTimeFormat(mcuAdmin.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                tzDisplay = ((mcuAdmin.Timezonedisplay.Trim() == "") ? "1" : mcuAdmin.Timezonedisplay.Trim());
                usrTimezone = mcuAdmin.TimeZone;
                userEmailLang = mcuAdmin.EmailLangId;
                userLanguage = mcuAdmin.Language;

                emailObject.Recipient = mcuAdmin.FirstName + " " + mcuAdmin.LastName;
                SetEmailLanguage(ref userLanguage, ref userEmailLang);
                FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.MCUAlert_MCUAdmin);
                SetConfDateTime(eEmailType.MCUAlert_MCUAdmin);
                BuildEmailBody();
                if (orgInfo.MCUAlert == (int)eEmailMCUAlert.Mcu_Admin || orgInfo.MCUAlert == (int)eEmailMCUAlert.Both) //FB 2472
                {
                    vrmEmail email = new vrmEmail();
                    email.emailFrom = sysAdmin.Email;
                    email.emailTo = mcuAdmin.Email;
                    email.Message = emailContent;
                    email.Subject = emailSubject;
                    email.UUID = getEmailUUID();
                    email.orgID = conf.orgId;
                    m_IemailDao.Save(email);
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendBridgeBookedEmail: ", e);
                return false;
            }

        }
        #endregion

        #region sendAcceptanceEmail
        /// <summary>
        /// sendAcceptanceEmail
        /// </summary>
        /// <param name="confUser"></param>
        /// <param name="bConf"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public bool sendAcceptanceEmail(ref vrmConfUser confUser, ref vrmConference bConf, ref vrmUser host) //FB 1830 recur issue
        {
            string userEmail = "";
            string usrName = "";
            int userEmailLang = 1;  //default en
            int userLanguage = 1;   //default en
            int imageid = 0; //FB 2136
            try
            {
                if (bConf == null)
                    return false;
			
				//FB 1830 recur issue strat
                if (confUser == null)
                    return false;

                if (host == null) 
                    return false;

                confHost = host; 
                //FB 1830 recur issue end
                conf = bConf;
                emailObject = new sConferenceEmail();

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;

                emailObject.MailLogo = AttachEmailLogo(organizationID);

                List<ICriterion> criterionListVMR = new List<ICriterion>();
                criterionListVMR.Add(Expression.Eq("confid", conf.confid));

                if (conf.instanceid > 0)
                    criterionListVMR.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionListVMR.Add(Expression.Eq("instanceid", 1));

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionListVMR);//FB 2636
                
                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();
                SetWebsiteURL(); //2136

                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid); 

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();
                emailObject.CustomOptions = addConfCustomAttr("Party");

                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                    emailObject.ConciergeSupport = ConciergeSupport();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "") 
                    Password = "N/A";
                emailObject.Password = Password;
                //FB 2376
                emailObject.ExternalBridge = "N/A";
                emailObject.InternalBridge = "N/A";

                
                //if (conf.isVMR == 1)
                if (conf.isVMR > 0) //FB 2620 //FB 2957
                {
                    if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                    {
                        emailObject.ExternalBridge = confAVParam[0].externalBridge;
                    }

                    if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                    {
                        emailObject.InternalBridge = confAVParam[0].internalBridge;
                    }
                }
                //FB 2376 Ends

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userid", confUser.userid));
                List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList);

                if(userList.Count == 0)
                {
                    List<vrmGuestUser> guestList = m_IguestUserDao.GetByCriteria(criterionList);
                    if (guestList.Count > 0)
                    {
                        //userDateFormat = ((guestList[0].DateFormat.Trim() == "") ? "MM/dd/yyyy" : guestList[0].DateFormat.Trim());
                        SetEmailDateFormat(guestList[0].DateFormat.Trim(), ref userDateFormat);//FB 255
                        //userTimeFormat = ((guestList[0].TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                        SetEmailTimeFormat(guestList[0].TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                        tzDisplay = ((guestList[0].Timezonedisplay.Trim() == "") ? "1" : guestList[0].Timezonedisplay.Trim());
                        usrTimezone = guestList[0].TimeZone;
                        userEmail = guestList[0].Email;
                        usrName = guestList[0].FirstName.Trim() + " " + guestList[0].LastName.Trim();
                    }
                }
                else
                {
                    //userDateFormat = ((userList[0].DateFormat.Trim() == "") ? "MM/dd/yyyy" : userList[0].DateFormat.Trim());
                    SetEmailDateFormat(userList[0].DateFormat.Trim(), ref userDateFormat);//FB 2555
                    //userTimeFormat = ((userList[0].TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                    SetEmailTimeFormat(userList[0].TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                    tzDisplay = ((userList[0].Timezonedisplay.Trim() == "") ? "0" : userList[0].Timezonedisplay.Trim());
                    usrTimezone = userList[0].TimeZone;
                    userEmail = userList[0].Email;
                    usrName = userList[0].FirstName.Trim() + " " + userList[0].LastName.Trim();
                    userLanguage = userList[0].Language;
                    userEmailLang = userList[0].EmailLangId;
                }
                emailObject.Recipient = usrName;
                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;

                SetEmailLanguage(ref userLanguage, ref userEmailLang);
                FetchEmailString(emailLanguage,0,eEmailType.PartyAcceptanceEmail);
                SetConfDateTime(eEmailType.Participant); //To remove buffer period

                StringBuilder eBody = new StringBuilder();
               
                if (confUser.invitee == 2) // invitee. Coming into room              
                {
                    if (confUser.roomId > 0 && confUser.roomId != vrmConfType.Phantom)
                    {
                        vrmRoom Loc = m_roomDAO.GetById(confUser.roomId);
                        imageid = Loc.SecurityImage1Id; //FB 2136
                        eBody.Append("<span style=\"font-size: 10pt; font-family: Verdana\">");
                        eBody.Append("You have chosen to attend the conference ");
                        eBody.Append("from the conference room : " + Loc.Name.Replace("||","\"").Replace("!!","'")); // FB 1888
                        eBody.Append("</span><BR>");
                    }
                    emailObject.AudioInfo = "";
                }
                else if (confUser.invitee == 1) //invited
                {
                    // FB 1642 Audio Add On starts
                    if (conf.conftype == 2 && confUser.audioOrVideo == 1) //Code Changed For FB 1744
                    {
                        emailObject.AudioInfo = FetchAudioDialString(conf.confid, conf.instanceid, "P", confUser.userid);
                    }
                    // FB 1642 Audio Add On ends

                    eBody.Append("<table width=\"400\" style=\"font-size: 10pt; font-family: Verdana; \" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">");

                    string pro, ctype;
                    if (confUser.defVideoProtocol == 1)
                        pro = "IP";
                    else
                    {
                        pro = "ISDN";
                    }
                    if (confUser.connectionType == dialingOption.dialOut)
                        ctype = "Dial-out";
                    else if (confUser.connectionType == dialingOption.dialIn)
                        ctype = "Dial-in";
                    else if (confUser.connectionType == dialingOption.direct)
                        ctype = "Direct";
                    else
                        ctype = "N/A";

                    eBody.Append("<tr>");
                    eBody.Append("<td align=\"right\" >");
                    eBody.Append("Video protocol: </td>");
                    eBody.Append("<td >");
                    eBody.Append("<strong>" + pro + "</strong></td>");
                    eBody.Append("</tr>");

                    eBody.Append("<tr>");
                    eBody.Append("<td align=\"right\" >");
                    eBody.Append("Your IP/ISDN address: </td>");
                    eBody.Append("<td>");
                    eBody.Append("<strong>" + confUser.ipisdnaddress + "</strong></td>");
                    eBody.Append("</tr>");

                    eBody.Append("<tr>");
                    eBody.Append("<td align=\"right\">");
                    eBody.Append("Connection type: </td>");
                    eBody.Append("<td>");
                    eBody.Append("<strong>" + ctype + "</strong></td>");
                    eBody.Append("</tr>");

                    if (confUser.connectionType == 1) // Dial in
                    {
                        eBody.Append("<tr>");
                        eBody.Append("<td align=\"right\">");
                        eBody.Append("Bridge IP/ISDN address: </td>");
                        eBody.Append("<td >");
                        eBody.Append("<strong>" + confUser.bridgeIPISDNAddress + "</strong></td>");
                        eBody.Append("</tr>");
                        List<vrmAddressType> addressType = vrmGen.getAddressType();
                        vrmAddressType at;
                        for(int a=0; a < addressType.Count; a++)
                        {
                            at = addressType[a];
                            if (at.Id == confUser.bridgeAddressType)
                            {
                                eBody.Append("<tr>");
                                eBody.Append("<td align=\"right\">");
                                eBody.Append("Address Type: </td>");
                                eBody.Append("<td >");
                                eBody.Append("<strong>" + at.name + "</strong></td>");
                                eBody.Append("</tr>");
                                break;
                            }
                        }
                    }
                    eBody.Append("</table>");
                }
                else if (confUser.invitee == 3)//FB 2347
                {
                    List<int> MId = new List<int>();
                    vrmMCU MCU = null;
                    eBody.Append("<span style=\"font-size: 10pt; font-family: Verdana\">");
                    //eBody.Append("<BR>MCU Details <BR><br><br>");
                    eBody.Append("<table width=\"400\" style=\"font-size: 10pt; font-family: Verdana;");
                    eBody.Append("border=\"1\" cellpadding=\"5\" cellspacing=\"0\">");
                    SetConfMCUName();

                    for (int m = 0; m < conf.mcuList.Count; m++)
                    {
                        MCU = m_ImcuDao.GetById(conf.mcuList[m].BridgeID);
                            eBody.Append("<tr>");
                            eBody.Append("<td align=\"right\">");
                            eBody.Append("MCU Name: </td>");
                            eBody.Append("<td>");
                            eBody.Append("<strong>" + MCU.BridgeName + "</strong></td>");
                            eBody.Append("</tr>");
                            eBody.Append("<tr>");
                            eBody.Append("<td align=\"right\" >");
                            eBody.Append("MCU Address: </td>");
                            eBody.Append("<td >");
                            eBody.Append("<strong>" + MCU.BridgeAddress + "</strong></td>");
                            eBody.Append("</tr>");

                    }
                    if (conf.mcuList.Count == 0)
                    {
                         eBody.Append("<tr>");
                         eBody.Append("<td align=\"left\">");
                         eBody.Append("PC Attendee</td>");
                         eBody.Append("</tr>");
                    }
                    eBody.Append("</table></span>");
                }
                emailObject.ConnectionDetails = eBody.ToString();

                //Should invoke if security badge is created
                //FB 2136 start
                String secDeskEmail = "";
                if (orgInfo.EnableSecurityBadge == 1) 
                {
                    //if (imageid != 0)
                    //{
                        emailObject.SecImage = SecurityBadgeAtt(conf.confnumname, confUser.userid); //FB 2136
                    //}

                    if (orgInfo.SecurityBadgeType > 1 && orgInfo.SecurityDeskEmailId != null && imageid > 0)
                    {
                        if (orgInfo.SecurityBadgeType == 3)
                        {
                            secDeskEmail = orgInfo.SecurityDeskEmailId.Trim();
                        }
                        else
                        {
                            userEmail = orgInfo.SecurityDeskEmailId.Trim();
                        }
                    }
                }
                //FB 2136 End
                BuildEmailBody();
				//FB 2136
                if (emailObject.SecImage != "")
                    emailContent = emailContent + emailObject.SecImage;

                vrmEmail theEmail = new vrmEmail();
                theEmail.emailTo = userEmail;
                theEmail.emailFrom = confHost.Email;
                theEmail.Message = emailContent;
                theEmail.Subject = emailSubject;
                theEmail.orgID = organizationID;
                theEmail.UUID = getEmailUUID();
                m_IemailDao.Save(theEmail);
               
				//FB 2136 Start
                if (secDeskEmail.Trim() != "")
                {
                    theEmail.emailTo = secDeskEmail;
                    theEmail.UUID = getEmailUUID();
                    m_IemailDao.Save(theEmail);
                }
                //FB 2136 End
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendAcceptanceEmail: ", e);
                throw e;
            }

        }
        #endregion

        //FB 2347 Starts
        #region SetConfMCUName
        /// <summary>
        ///SetConfMCUName
        /// </summary>
        private void SetConfMCUName()
        {

            List<ICriterion> RoomcriterionList = null;
            vrmMCU MCU = null;
            List<int> bId = new List<int>();
            List<vrmMCU> mcuList = null;
            StringBuilder eMCUBody = new StringBuilder();

            try
            {
                if (confRoomList == null)
                {
                    RoomcriterionList = new List<ICriterion>();
                    RoomcriterionList.Add(Expression.Eq("confid", conf.confid));

                    if (conf.instanceid > 0)
                        RoomcriterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    else
                        RoomcriterionList.Add(Expression.Eq("instanceid", 1));

                    confRoomList = m_vrmConfRoomDAO.GetByCriteria(RoomcriterionList);
                }

                foreach (vrmConfRoom cr in confRoomList)
                {
                    if (cr.bridgeid > 0)
                    {
                        if (!bId.Contains(cr.bridgeid))
                            bId.Add(cr.bridgeid);
                    }
                }

                if (confUserLst == null)
                {
                    RoomcriterionList = new List<ICriterion>();
                    RoomcriterionList.Add(Expression.Eq("confid", conf.confid));

                    if (conf.instanceid > 0)
                        RoomcriterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    else
                        RoomcriterionList.Add(Expression.Eq("instanceid", 1));

                    confUserLst = m_vrmConfUserDAO.GetByCriteria(RoomcriterionList);
                }


                foreach (vrmConfUser cu in confUserLst)
                {
                    if (cu.bridgeid > 0 && cu.invitee == 1) // external only
                    {
                        if (!bId.Contains(cu.bridgeid))
                            bId.Add(cu.bridgeid);
                    }
                }

                             
                for (int m = 0; m < bId.Count; m++)
                {
                    if (mcuList == null)
                        mcuList = new List<vrmMCU>();

                    MCU = m_ImcuDao.GetById(bId[m]);
                    mcuList.Add(MCU);
                    
                }
                if(mcuList != null)
                    conf.mcuList = mcuList;
            }
            catch (Exception e)
            {
                m_log.Error("Email-SetConfRoomString: ", e);
            }
        }

        #endregion

        #region sendChangeRequest
        /// <summary>
        /// sendChangeRequest
        /// </summary>
        /// <param name="baseConf"></param>
        /// <param name="element"></param>
        /// <param name="emailaddres"></param>
        /// <returns></returns>
        public bool sendChangeRequest(vrmConference baseConf, ref XmlElement element, string emailaddres)
        {
            string newStartDate, newStartTime;
            string newDurationMin, newComment;
            try
            {
                int userEmailLang = 1;  //default en
                int userLanguage = 1;   //default en
                int newTimezone = 26; //default EST

                conf = baseConf;
                if (conf == null)
                    return false;

                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                emailObject = new sConferenceEmail();

                organizationID = conf.orgId;

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                //FB 2189 
                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                userEmailLang = confHost.EmailLangId;
                userLanguage = confHost.Language;
                SetEmailLanguage(ref userLanguage, ref userEmailLang);
                FetchEmailString(emailLanguage, 0, eEmailType.PartyRequestChange);

                // Get User Requested time details
                newStartDate = element.GetElementsByTagName("StartDate")[0].InnerText;
                newStartTime = element.GetElementsByTagName("StartTime")[0].InnerText;
                newDurationMin = element.GetElementsByTagName("Duration")[0].InnerText;
                newComment = element.GetElementsByTagName("Comment")[0].InnerText;

                int.TryParse(element.GetElementsByTagName("Timezone")[0].InnerText, out newTimezone);
                if (newTimezone <= 0)
                    newTimezone = conf.timezone;

                timeZoneData tz = new timeZoneData();
                timeZone.GetTimeZone(newTimezone, ref tz);

                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{2}", confHost.FirstName.Replace("||", "\"").Replace("!!", "'") + " " + confHost.LastName.Replace("||", "\"").Replace("!!", "'")); // FB 1888
                emailContent = emailContent.Replace("{4}", conf.externalname);
                emailContent = emailContent.Replace("{5}", conf.confnumname.ToString());
                emailContent = emailContent.Replace("{55}", newStartDate + " " + newStartTime +" "+ tz.TimeZone);
                emailContent = emailContent.Replace("{56}", newDurationMin);
                emailContent = emailContent.Replace("{50}", newComment);
                //FB 2189 Starts
                emailContent = emailContent.Replace("{37}", sysTech.name);
                emailContent = emailContent.Replace("{38}", sysTech.email);
                emailContent = emailContent.Replace("{39}", sysTech.phone);
                //FB 2189 Ends                               
                vrmEmail email = new vrmEmail();
                email.UUID = getEmailUUID();
                email.emailFrom = emailaddres;
                email.emailTo = confHost.Email;
                email.Subject = emailSubject;
                email.Message = emailContent;
                email.orgID = organizationID;
                m_IemailDao.Save(email);
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendChangeRequest: ", e);
                return false;
            }

        }
        //FB 2020
        public bool sendChangeRequest(List<vrmConference> baseConf, string emailaddres)
        {
            try
            {
                int userEmailLang = 1;  //default en
                int userLanguage = 1;   //default en

                conf = baseConf[0];
                if (conf == null)
                    return false;

                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                emailObject = new sConferenceEmail();

                organizationID = conf.orgId;

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                //FB 2189 
                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                userEmailLang = confHost.EmailLangId;
                userLanguage = confHost.Language;
                SetEmailLanguage(ref userLanguage, ref userEmailLang);
                FetchEmailString(emailLanguage, 0, eEmailType.PartyMultiRequestChange);
                
                String emailRequestChange = "";
                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{2}", confHost.FirstName.Replace("||", "\"").Replace("!!", "'") + " " + confHost.LastName.Replace("||", "\"").Replace("!!", "'")); // FB 1888
                emailContent = emailContent.Replace("{4}", conf.externalname);

                foreach (vrmConference confnew in baseConf)
                {
                    emailRequestChange += "<tr><td><strong>" + confnew.confnumname.ToString() + "</strong></td>" +
                            "<td><strong>" + confnew.NewStartDate + " " + confnew.NewStartTime + " " + confnew.NewTimezone + "</strong></td>" +
                            "<td align='center'><strong>" + confnew.NewDurationMin + "</strong></td>" +
                            "<td align='left'><strong>" + confnew.NewComment + "</strong></td>" +
                            "</tr>";
                }
                emailContent = emailContent.Replace("{80}", emailRequestChange);

                //FB 2189 Starts
                emailContent = emailContent.Replace("{37}", sysTech.name);
                emailContent = emailContent.Replace("{38}", sysTech.email);
                emailContent = emailContent.Replace("{39}", sysTech.phone);

                //FB 2189 Ends                               
                vrmEmail email = new vrmEmail();
                email.UUID = getEmailUUID();
                email.emailFrom = emailaddres;
                email.emailTo = confHost.Email;
                email.Subject = emailSubject;
                email.Message = emailContent;
                email.orgID = organizationID;
                m_IemailDao.Save(email);

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendChangeRequest: ", e);
                return false;
            }

        }
        #endregion

        #region  FeedbackEmail
        /// <summary>
        /// FeedbackEmail
        /// </summary>
        /// <param name="feedback"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool FeedbackEmail(vrmEmailFeedback feedback, ref string outXml)
        {
            bool bRet = true;
            try
            {
                vrmUser user = m_IuserDao.GetByUserId(feedback.userID);
                DateTime dtTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref dtTime);
                timeZone.GMTToUserPreferedTime(sysSettings.TimeZone, ref dtTime); //FB 1906 - Changed to system timezone as mail is being sent to Support

                organizationID = user.companyId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;
                
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                FetchEmailString(orgInfo.EmailLangId, 0, eEmailType.Feedback);
                //emailSubject = emailSubject + feedback.subject;
                
                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{43}", feedback.name.Replace("||", "\"").Replace("!!", "'")); // FB 1888
                emailContent = emailContent.Replace("{44}", feedback.email);
                emailContent = emailContent.Replace("{45}", dtTime.ToString("f"));
                emailContent = emailContent.Replace("{46}", feedback.parentPage);
                emailContent = emailContent.Replace("{47}", feedback.browserAgent);
                emailContent = emailContent.Replace("{48}", feedback.browserLanguage);
                emailContent = emailContent.Replace("{49}", feedback.ipAddress);
                emailContent = emailContent.Replace("{50}", feedback.comment);
                //FB 2189 Starts
                emailContent = emailContent.Replace("{2}", sysTech.name);
                emailContent = emailContent.Replace("{37}", sysTech.name);
                emailContent = emailContent.Replace("{38}", sysTech.email);
                emailContent = emailContent.Replace("{39}", sysTech.phone);
                //FB 2189 Ends
                                
                if (sysTech != null)     //Organization Module
                {
                    vrmEmail theEmail = new vrmEmail();
                    theEmail.emailTo = sysTech.email;
                    theEmail.emailFrom = user.Email;
                    theEmail.Message = emailContent;
                    theEmail.Subject = emailSubject;
                    theEmail.UUID = getEmailUUID();
                    theEmail.orgID = user.companyId; //Added for FB 1710

                    m_IemailDao.Save(theEmail);
                }
                else // No support email entry found in the database so throw an error
                {
                    myVRMException e = new myVRMException(425);
                    m_log.Error("vrmException", e);
                    outXml = myVRMException.toXml("Unable to find Support email address in the database.");
                    return false;
                }
            }
            catch (Exception e)
            {
                m_log.Error("FeedbackEmail: ", e);
                throw e;
            }
            return bRet;
        }
        #endregion

        #region  SendReminderEmails
        /// <summary>
        /// SendReminderEmails
        /// </summary>
        /// <param name="confList"></param>
        /// <returns></returns>
        public bool SendReminderEmails(List<vrmConference> confList,int partyID)
        {
            try
            {
                emailObject = new sConferenceEmail();
                conf = confList[0];

                if (conf == null)
                    return false;

                SetWebsiteURL();

                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                SetHostLangFolder(confHost.Language);

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;

                emailObject.MailLogo = AttachEmailLogo(organizationID);

                List<ICriterion> criterionListVMR = new List<ICriterion>();
                criterionListVMR.Add(Expression.Eq("confid", conf.confid));

                if (conf.instanceid > 0)
                    criterionListVMR.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionListVMR.Add(Expression.Eq("instanceid", 1));

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionListVMR);//FB 2636


                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "")
                    Password= "N/A";
                emailObject.Password = Password;

                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                    emailObject.ConciergeSupport = ConciergeSupport();

                //FB 2376 Starts
                emailObject.ExternalBridge = "N/A";
                emailObject.InternalBridge = "N/A";

               
                //if (conf.isVMR == 1)
                if (conf.isVMR > 0) //FB 2620 //FB 2957
                {
                    if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                    {
                        emailObject.ExternalBridge = confAVParam[0].externalBridge;
                    }

                    if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                    {
                        emailObject.InternalBridge = confAVParam[0].internalBridge;
                    }
                }
                //FB 2376 Ends
                //GetAttachementsStr(); FB 2154

                //Fetch Participants list
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                criterionList.Add(Expression.Eq("userid", partyID));
                List<vrmConfUser> uList = m_vrmConfUserDAO.GetByCriteria(criterionList);

                string confcodepin = ""; //FB 1642
                vrmConfUser us;
                //Sending mail to participants
                FetchAudioBridgeDialString(conf.confid, conf.instanceid); //FB 2439
                for (int i = 0; i < uList.Count; i++)
                {
                    us = uList[i]; //FB 1830
                    if (us.partyNotify == 1) //FB 1830
                    {
                        confcodepin = "";
                        if (conf.conftype == 2 && us.audioOrVideo == 1) //FB 1744 //FB 2381
                        {
                            confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "P", us.userid);
                        }
                        //FB 2439 start
                        emailObject.PersonalAudioInfo = confcodepin;
                        emailObject.AudioInfo = partyAudioBridgeDt;
                        //FB 2439 end
                        emailObject.CustomOptions = addConfCustomAttr("Party"); //FB 2381

                        if (!sendParticipantEmail(us.userid, us.invitee,eEmailType.PartyRemainder))
                            m_log.Error("Cannot send participant remainder email [userid]" + us.userid.ToString());
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SendReminderEmails", e);
                throw e;
            }
        }
        #endregion

        #region sendAVAndHKWOEmails
        /// <summary>
        /// sendAVAndHKWOEmails
        /// </summary>
        /// <param name="confList"></param>
        /// <param name="WorkOrderList"></param>
        /// <returns></returns>
        public bool sendAVAndHKWOEmails(List<vrmConference> confList, List<WorkOrder> WorkOrderList)
        {
            try
            {
                //FB 2027 SetConference - start
                if (confList == null)
                    return false;

                if (confList.Count <= 0)
                    return false;

                if (WorkOrderList == null)
                    return false;

                if (WorkOrderList.Count <= 0)
                    return false;
                //FB 2027 SetConference - end

                m_woDAO = new WorkOrderDAO(m_configPath, m_log);
                m_InvCategoryDAO = m_woDAO.GetCategoryDAO();
                m_IAVItemDAO = m_woDAO.GetAVItemDAO();
                m_IHKItemDAO = m_woDAO.GetHKItemDAO();
                m_InvListDAO = m_woDAO.GetInvListDAO();
                m_InvWorkOrderDAO = m_woDAO.GetWorkOrderDAO();
                m_InvWorkItemDAO = m_woDAO.GetWorkItemDAO();
                m_InvWorkChargeDAO = m_woDAO.GetWorkChargeDAO();
                m_InvRoomDAO = m_woDAO.GetInvRoomDAO();

                emailObject = new sConferenceEmail();
                conf = confList[0];
                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                organizationID = conf.orgId;

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                //FB 2189 Starts
                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;
                //FB 2189 Ends
                emailObject.MailLogo = AttachEmailLogo(organizationID);

                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "") 
                    Password = "N/A";
                emailObject.Password = Password;

                // FB 324,325,327 Work order emails got to the host if notify is on
                // they ALWAYS go to the work order admin. 
                WorkOrder work = null;
                for(int w=0; w< WorkOrderList.Count; w++)
                {
                    work = WorkOrderList[w];
                    if (confList.Count == 1 && orgInfo.SendConfirmationEmail == 0) //FB 2284
                        sendWorkOrderMails(ref work);
                    else if (work.InstanceID == confList[0].instanceid && orgInfo.SendConfirmationEmail==0)
                        sendWorkOrderMails(ref work);
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendAVAndHKWOEmails", e);
                throw e;
            }
        }
        #endregion

        #region sendWorkOrderMails
        /// <summary>
        /// sendWorkOrderMails
        /// </summary>
        /// <param name="work"></param>
        /// <returns></returns>
        private bool sendWorkOrderMails(ref WorkOrder work)
        {
            int userPrefLang = 1;
            int userEmailLang = 1;
            string timeFormat = "HH:mm", tzName = ""; //FB 2588
            
            try
            {
                vrmUser user = m_IuserDao.GetByUserId(work.AdminID);

                userEmailLang = user.EmailLangId;
                userPrefLang = user.Language;
                //userDateFormat = ((user.DateFormat.Trim() == "") ? "MM/dd/yyyy" : user.DateFormat.Trim());
                SetEmailDateFormat(user.DateFormat.Trim(), ref userDateFormat);
                //userTimeFormat = ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                SetEmailTimeFormat(user.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                usrTimezone = user.TimeZone;

                emailObject.Recipient = user.FirstName + " " + user.LastName;
                SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                
                DateTime dt = work.CompletedBy;
                timeZone.userPreferedTime(usrTimezone, ref dt);

                vrmRoom Loc = m_roomDAO.GetById(work.IRoom.roomId);
                emailObject.Workordername = work.Name;
                emailObject.RoomsAssigned = Loc.tier2.tier3.Name.Replace("||", "\"").Replace("!!", "'") + "&gt;" + Loc.tier2.Name.Replace("||", "\"").Replace("!!", "'") + "&gt;" + Loc.Name.Replace("||", "\"").Replace("!!", "'");// FB 1888

                if (conf.StartMode == 0)//FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;

                // in cases of new work orders we do not have inventoy set name 
                string inventorySet = "";

                if (work.IC.Name == null)
                {
                    if (work.ItemList.Count > 0)
                    {
                        WorkItem a_wi = (WorkItem)work.ItemList[0];
                        if (work.Type == woConstant.CATEGORY_TYPE_AV)
                        {
                            AVInventoryItemList AV = m_IAVItemDAO.GetById(a_wi.ItemID);
                            inventorySet = AV.ICategory.Name;
                        }
                        else if(work.Type == woConstant.CATEGORY_TYPE_HK)
                        {
                            HKInventoryItemList HK = m_IHKItemDAO.GetById(a_wi.ItemID);
                            inventorySet = HK.ICategory.Name;
                        }
                    }
                }
                else
                {
                    inventorySet = work.IC.Name;
                }
                emailObject.InventorySetused = inventorySet;
                emailObject.Person_in_charge = user.FirstName + " " + user.LastName +
                    "(<a href='mailto:" + user.Email + "' target='_blank'>" + user.Email + "</a>)"; //FB 1948
                
                timeZoneData aTz = new timeZoneData();
                timeZone.GetTimeZone(user.TimeZone, ref aTz);

                //For Host Email
                timeZoneData hTz = new timeZoneData();
                timeZone.GetTimeZone(confHost.TimeZone, ref hTz);

                DateTime woDate = work.startBy;
                timeZone.userPreferedTime(user.TimeZone, ref woDate);
                //FB 2588 Starts
                SetEmailTimeFormat(user.TimeFormat.Trim(),ref timeFormat); 
                tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                tzName = "(" + aTz.TimeZone + ")";
                if (tzDisplay == "0")
                    tzName = "";
                emailObject.Start_byDatetime = woDate.ToString(user.DateFormat) + " " + woDate.ToString(timeFormat) + tzName;

                //For Host
                woDate = work.startBy;
                timeZone.userPreferedTime(confHost.TimeZone, ref woDate);
                SetEmailTimeFormat(confHost.TimeFormat.Trim(), ref timeFormat);
                tzDisplay = ((confHost.Timezonedisplay.Trim() == "") ? "0" : confHost.Timezonedisplay.Trim());
                tzName = "(" + hTz.TimeZone + ")";
                if (tzDisplay == "0")
                    tzName = "";
                string hostStartByDate = woDate.ToString(confHost.DateFormat) + " " + woDate.ToString(timeFormat) + tzName;
                                
                woDate = work.CompletedBy;
                timeZone.userPreferedTime(user.TimeZone, ref woDate);
                SetEmailTimeFormat(user.TimeFormat.Trim(), ref timeFormat);
                tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                tzName = "(" + aTz.TimeZone + ")";
                if (tzDisplay == "0")
                    tzName = "";
                emailObject.End_byDatetime = woDate.ToString(user.DateFormat) + " " + woDate.ToString(timeFormat) + tzName;

                //For Host
                woDate = work.CompletedBy;
                timeZone.userPreferedTime(confHost.TimeZone, ref woDate);
                SetEmailTimeFormat(confHost.TimeFormat.Trim(), ref timeFormat);
                tzDisplay = ((confHost.Timezonedisplay.Trim() == "") ? "0" : confHost.Timezonedisplay.Trim());
                tzName = "(" + hTz.TimeZone + ")";
                if (tzDisplay == "0")
                    tzName = "";
                string hostEndByDate = woDate.ToString(confHost.DateFormat) + " " + woDate.ToString(timeFormat) + tzName;
                //FB 2588 Ends
                InvDeliveryTypeDAO deliveryTypeDAO = m_woDAO.GetDeliveryTypeDAO();
                DeliveryType delType = deliveryTypeDAO.GetById(work.deliveryType);

                emailObject.Deliverytype = delType.deliveryType;
                // FB 1933 Starts
                CultureInfo cInfo = null;
                if (user.DateFormat.ToString() == "dd/MM/yyyy") 
                    cInfo = new CultureInfo("fr-FR");
                else
                    cInfo = new CultureInfo("en-US");

                emailObject.Deliverycost = work.deliveryCost.ToString("n", cInfo);
                emailObject.Servicecharge = work.serviceCharge.ToString("n", cInfo);
                if (user.DateFormat.ToString() == "MM/dd/yyyy" || user.DateFormat.ToString() == "0") 
                    emailObject.CurrencyFormat = "$";
                else
                    emailObject.CurrencyFormat = "�";
                // FB 1933 Ends
                
                string status = "Pending";
                if (work.Status != 0)
                    status = "Completed";

                emailObject.Currentstatus = status;

                string Name = "";
                double cost = 0.0;
                string serialNumber = "";
                int qty = 0; ;
                double serviceCharge = 0, deliveryCost = 0;
                emailObject.Requestitem = ""; //ZD 100237
                foreach (WorkItem wi in work.ItemList)
                {
                    qty = wi.quantity;
                    //ZD 100237 start
                    string sn = "N/A";
                    if (serialNumber.Length > 0)
                        sn = serialNumber;
                    //ZD 100237 End

                    if (work.Type == woConstant.CATEGORY_TYPE_AV)
                    {
                        AVInventoryItemList AV = m_IAVItemDAO.GetById(wi.ItemID);
                        Name = AV.name;
                        cost = AV.price;
                        serialNumber = AV.serialNumber;
                        //ZD 100237 start
                        serviceCharge = wi.serviceCharge; 
                        deliveryCost = wi.deliveryCost; 
                        // FB 1933 Starts
                        emailObject.Requestitem += "<tr><td>" + Name + "</td>" + //FB 2120 - Case 1
                            "<td align='center'>" + sn + "</td>" +
                            "<td align='center'>" + qty.ToString() + "</td>" +
                            "<td align='center'>" + serviceCharge.ToString("n", cInfo) + "</td>" +
                            "<td align='center'>" + deliveryCost.ToString("n", cInfo) + "</td>" +
                            "<td align='center'>" + cost.ToString("n", cInfo) + "</td>" +
                            "</tr>";
                        //ZD 100237 End
                    }
                    else if (work.Type == woConstant.CATEGORY_TYPE_HK)
                    {
                        HKInventoryItemList HK = m_IHKItemDAO.GetById(wi.ItemID);
                        Name = HK.name;
                        cost = HK.price;
                        serialNumber = HK.serialNumber;
                        //ZD 100237 start
                        //serviceCharge = HK.serviceCharge; 
                        //deliveryCost = HK.deliveryCost; 
                        // FB 1933 Starts
                        emailObject.Requestitem += "<tr><td>" + Name + "</td>" + //FB 2120 - Case 1
                            "<td align='center'>" + sn + "</td>" +
                            "<td align='center'>" + qty.ToString() + "</td>" +
                            "<td align='center'>" + cost.ToString("n", cInfo) + "</td>" +
                            "</tr>";
                    }
                    //ZD 100237 End
                    
                    
                }
                //FB 1685
                emailObject.Totalcost = work.woTtlCost.ToString("n", cInfo); // FB 1933 Ends

                emailObject.Tobecompleted = dt.ToString(userDateFormat) +
                    " " + dt.ToString((userTimeFormat.Trim()));  //FB 1906

                if (work.Type == woConstant.CATEGORY_TYPE_AV)
                {
                    if(work.sendReminder) //FB 2189
                        FetchEmailString(emailLanguage, 0, eEmailType.InventoryRemainder);
                    else                        
                        FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.InventoryAdmin);

                    SetConfDateTime(eEmailType.InventoryAdmin);
                }
                else if(work.Type == woConstant.CATEGORY_TYPE_HK)
                {
                    if(work.sendReminder) //FB 2189
                        FetchEmailString(emailLanguage, 0, eEmailType.HKRemainder);
                    else
                        FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.HKAdmin);

                    SetConfDateTime(eEmailType.HKAdmin);
                }
                BuildEmailBody();

                vrmEmail theEmail = new vrmEmail();
                theEmail.emailTo = user.Email;
                theEmail.emailFrom = confHost.Email;
                theEmail.Message = emailContent;
                theEmail.Subject = emailSubject;
                theEmail.orgID = conf.orgId;

                theEmail.UUID = getEmailUUID();
                m_IemailDao.Save(theEmail);

                if (work.Notify != 0)
                {
                    if (work.AdminID != confHost.userid)
                    {
                        userPrefLang = confHost.Language;
                        userEmailLang = confHost.EmailLangId;
                        //userDateFormat = ((confHost.DateFormat.Trim() == "") ? "MM/dd/yyyy" : confHost.DateFormat.Trim());
                        SetEmailDateFormat(confHost.DateFormat.Trim(), ref userDateFormat);//FB 2555
                        //userTimeFormat = ((confHost.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                        SetEmailTimeFormat(confHost.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                        tzDisplay = ((confHost.Timezonedisplay.Trim() == "") ? "0" : confHost.Timezonedisplay.Trim());
                        usrTimezone = confHost.TimeZone;

                        SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                        emailObject.Recipient = confHost.FirstName + " " + confHost.LastName;
                        emailObject.Start_byDatetime = hostStartByDate;
                        emailObject.End_byDatetime = hostEndByDate;
                        emailObject.Tobecompleted = hostEndByDate;

                        if (work.Type == woConstant.CATEGORY_TYPE_AV)
                        {
                            if(work.sendReminder) //FB 2189
                                FetchEmailString(emailLanguage, 0, eEmailType.InventoryRemainder);
                            else
                                FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.InventoryAdmin);
                            
                            SetConfDateTime(eEmailType.InventoryAdmin);
                        }
                        else if (work.Type == woConstant.CATEGORY_TYPE_HK)
                        {
                            if(work.sendReminder) //FB 2189
                                FetchEmailString(emailLanguage, 0, eEmailType.HKRemainder);
                            else
                                FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.HKAdmin);

                            SetConfDateTime(eEmailType.HKAdmin);
                        }
                        BuildEmailBody();

                        theEmail = new vrmEmail();
                        theEmail.emailTo = confHost.Email;
                        theEmail.emailFrom = confHost.Email;
                        theEmail.Message = emailContent;
                        theEmail.Subject = emailSubject;
                        theEmail.orgID = conf.orgId;

                        theEmail.UUID = getEmailUUID();
                        m_IemailDao.Save(theEmail);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendWorkOrderMails: ", e);
                return false;
            }
        }
        #endregion

        #region sendCateringWOEmails
        /// <summary>
        /// sendCateringWOEmails
        /// </summary>
        /// <param name="confList"></param>
        /// <param name="WorkOrderList"></param>
        /// <returns></returns>
        public bool sendCateringWOEmails(List<vrmConference> confList, List<WorkOrder> WorkOrderList)
        {
            try
            {
                //FB 2027 SetConference - start
                if (confList == null)
                    return false;
                if (confList.Count <= 0)
                    return false;
                if (WorkOrderList == null)
                    return false;
                if (WorkOrderList.Count <= 0)
                    return false;
                //FB 2027 SetConference - end

                m_woDAO = new WorkOrderDAO(m_configPath, m_log);
                m_InvCategoryDAO = m_woDAO.GetCategoryDAO();
                m_ICAItemDAO = m_woDAO.GetCAItemDAO();
                m_InvWorkItemDAO = m_woDAO.GetWorkItemDAO();
                m_InvWorkChargeDAO = m_woDAO.GetWorkChargeDAO();
                m_InvRoomDAO = m_woDAO.GetInvRoomDAO();

                emailObject = new sConferenceEmail();
                conf = confList[0];
                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                //FB 2189 Starts
                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;
                //FB 2189 Ends
                emailObject.MailLogo = AttachEmailLogo(organizationID);

                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");//FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "") 
                    Password = "N/A";
                emailObject.Password = Password;

                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;

                // FB 324,325,327 Work order emails got to the host if notify is on
                // they ALWAYS go to the work order admin. 
                WorkOrder emWork;
                for (int w = 0; w < WorkOrderList.Count; w++ )
                {
                    emWork = WorkOrderList[w];
                    if (emWork.InstanceID < 2 && orgInfo.SendConfirmationEmail ==0 )//FB 2284 //FB 2470
                        sendCateringEmail(ref emWork);
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sendCateringWOEmails", e);
                throw e;
            }
        }
        #endregion

        #region sendCateringEmail
        // this email is unique to the new catering module
        private bool sendCateringEmail(ref WorkOrder work)
        {
            int userPrefLang = 1;
            int userEmailLang = 1;
            vrmEmail theEmail;
            string timeFormat = "HH:mm", tzName = ""; //FB 2588
            try
            {

                vrmUser user = new vrmUser();
                InvMenuDAO menuDAO = m_woDAO.GetMenuDAO();
                // this is a 2 pass method. First make a hashtable with all of the category id's
                // then enum through the table and creat 1 email for each provide admin that has ALL
                // of their menu's on it. Note if the admin is responsible for more than one provider he/she
                // will get an email for each provider.

                Hashtable providerId = new Hashtable();
                foreach (WorkItem wi in work.ItemList)
                {
                    Menu m = menuDAO.GetById(wi.ItemID);
                    if (!providerId.ContainsKey(m.ICategory.ID))
                        providerId.Add(m.ICategory.ID, m.ICategory.ID);

                }
                InvServiceDAO serviceDAO = m_woDAO.GetInvServiceDAO();
                InventoryService Service = serviceDAO.GetById(work.deliveryType);
                string serviceName = Service.name;

                IDictionaryEnumerator iEnum = providerId.GetEnumerator();
                while (iEnum.MoveNext())
                {
                    int iKey = (int)iEnum.Value;

                    foreach (WorkItem wi in work.ItemList)
                    {
                        Menu menu = menuDAO.GetById(wi.ItemID);
                        if (menu.categoryID == iKey)
                        {
                            DateTime dt = work.CompletedBy;
                            timeZone.userPreferedTime(conf.timezone, ref dt);

                            emailObject.Workordername = work.Name;
                            vrmRoom Loc = m_roomDAO.GetById(work.IRoom.roomId);
                            emailObject.RoomsAssigned = Loc.tier2.tier3.Name + "&gt;" + Loc.tier2.Name + "&gt;" + Loc.Name;

                            // in cases of new work orders we do not have inventoy set name 
                            string inventorySet = "";

                            //FB 1685
                            //if (work.toUserID > 0)
                            //    user = uDAO.GetByUserId(work.toUserID);
                            //else if (menu.ICategory.AdminID > 0)
                            //    user = uDAO.GetByUserId(menu.ICategory.AdminID);
                            //else
                            //    user = uDAO.GetByUserId(work.AdminID);
                            
                            //if (work.email.emailTo.Length == 0)
                            //    work.email.emailTo = user.Email;

                            // FB 1685
                            if (menu.ICategory.AdminID > 0)
                                user = m_IuserDao.GetByUserId(menu.ICategory.AdminID);
                            else
                                user = m_IuserDao.GetByUserId(work.AdminID);


                            emailObject.Person_in_charge = user.FirstName + " " + user.LastName +
                                "(<a href='mailto:" + user.Email + "' target='_blank'>" + user.Email + "</a>)"; //FB 1948

                            emailObject.WorkorderComment = work.Comment;
                            emailObject.ServiceType = serviceName;

                            menuDAO = m_woDAO.GetMenuDAO();
                            emailObject.Cateringmenus = menu.name;
                            
                            DateTime woDate = work.startBy;
                            timeZone.userPreferedTime(user.TimeZone, ref woDate);
                            
                            // FB 1933 Starts
                            CultureInfo cInfo = null;
                            if (user.DateFormat.ToString() == "dd/MM/yyyy")
                                cInfo = new CultureInfo("fr-FR");
                            else
                                cInfo = new CultureInfo("en-US");
                            
                            emailObject.Totalcost = work.woTtlCost.ToString("n", cInfo);
                            if (user.DateFormat.ToString() == "MM/dd/yyyy" || user.DateFormat.ToString() == "0") 
                                emailObject.CurrencyFormat = "$";
                            else
                                emailObject.CurrencyFormat = "�";
                            // FB 1933 Ends

                            if (conf.StartMode == 0)//FB 2522
                                callLaunch = "Yes";
                            emailObject.CallLaunch = callLaunch;
                                                        
                            List<ICriterion> criterionList = new List<ICriterion>();
                            ICriterion criterium = Expression.Eq("categoryID", wi.ItemID);
                            criterionList.Add(criterium);

                            List<CAInventoryItemList> result = m_ICAItemDAO.GetByCriteria(criterionList, true);

                            string catMenustr = "";
                            foreach (CAInventoryItemList ca in result)
                            {
                                string comment = "N/A";
                                if (ca.comment.Length > 0)
                                    comment = ca.comment;

                                catMenustr += "<tr><td>" + ca.name + "</td>" + //FB 2120 - Case 1
                                    "<td align='center'>" + wi.ItemID.ToString() + "</td>" +
                                    "<td align='center'>" + wi.quantity.ToString() + "</td>" +
                                    "<td align='center'>" + comment + "</td>" +
                                    "<td align='center'>" + work.woTtlCost.ToString("n", cInfo) +"</td>" +  //ca.price.ToString("n", cInfo) // FB 1933
                                    "</tr>";
                            }
                            emailObject.Requestitem = catMenustr;
                        }
                    }

                    userEmailLang = user.EmailLangId;
                    userPrefLang = user.Language;
                    //userDateFormat = ((user.DateFormat.Trim() == "") ? "MM/dd/yyyy" : user.DateFormat.Trim());
                    SetEmailDateFormat(user.DateFormat.Trim(), ref userDateFormat);//FB 2550
                    //userTimeFormat = ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                    SetEmailTimeFormat(user.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                    tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                    usrTimezone = user.TimeZone;
                    // FB 1933 Starts
                    timeZoneData hTz = new timeZoneData();
                    timeZone.GetTimeZone(confHost.TimeZone, ref hTz);

                    timeZoneData aTzu = new timeZoneData();
                    timeZone.GetTimeZone(user.TimeZone, ref aTzu);

                    DateTime woDateu = work.startBy;
                    timeZone.userPreferedTime(user.TimeZone, ref woDateu);
                    //FB 2588 Starts
                    SetEmailTimeFormat(user.TimeFormat.Trim(), ref timeFormat);
                    tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                    tzName = "(" + aTzu.TimeZone + ")";
                    if (tzDisplay == "0")
                        tzName = "";
                    emailObject.Start_byDatetime = woDateu.ToString(user.DateFormat) + " " + woDateu.ToString(timeFormat) + tzName;

                    woDateu = work.CompletedBy;
                    timeZone.userPreferedTime(user.TimeZone, ref woDateu);
                    SetEmailTimeFormat(user.TimeFormat.Trim(), ref timeFormat);
                    tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                    tzName = "(" + aTzu.TimeZone + ")";
                    if (tzDisplay == "0")
                        tzName = "";
                    emailObject.End_byDatetime = woDateu.ToString(user.DateFormat) + " " + woDateu.ToString(timeFormat) + tzName;

                    DateTime woDateh = work.startBy;
                    timeZone.userPreferedTime(confHost.TimeZone, ref woDateh);
                    SetEmailTimeFormat(confHost.TimeFormat.Trim(), ref timeFormat);
                    tzDisplay = ((confHost.Timezonedisplay.Trim() == "") ? "0" : confHost.Timezonedisplay.Trim());
                    tzName = "(" + hTz.TimeZone + ")";
                    if (tzDisplay == "0")
                        tzName = "";
                    string hostStartByDate = woDateh.ToString(confHost.DateFormat) + " " + woDateh.ToString(timeFormat) + tzName;

                    woDateh = work.CompletedBy;
                    timeZone.userPreferedTime(confHost.TimeZone, ref woDateh);
                    SetEmailTimeFormat(confHost.TimeFormat.Trim(), ref timeFormat);
                    tzDisplay = ((confHost.Timezonedisplay.Trim() == "") ? "0" : confHost.Timezonedisplay.Trim());
                    tzName = "(" + hTz.TimeZone + ")";
                    if (tzDisplay == "0")
                        tzName = "";
                    string hostEndByDate = woDateh.ToString(confHost.DateFormat) + " " + woDateh.ToString(timeFormat) + tzName;

                    string hostDelivryDate = woDateh.ToString(confHost.DateFormat) + " " + woDateh.ToString(timeFormat) + tzName;

                    SetEmailTimeFormat(user.TimeFormat.Trim(), ref timeFormat);
                    tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                    tzName = "(" + aTzu.TimeZone + ")";
                    if (tzDisplay == "0")
                        tzName = "";
                    emailObject.DeliveryDatetime = woDateu.ToString(user.DateFormat) + " " + woDateu.ToString(timeFormat) + tzName;
                    // FB 1933 Ends

                    emailObject.Recipient = user.FirstName + " " + user.LastName;
                    emailObject.Tobecompleted = woDateu.ToString(user.DateFormat) + " " + woDateu.ToString(timeFormat) + tzName;
                    //FB 2588 Ends

                    SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                    if(work.sendReminder) //FB 2189
                        FetchEmailString(emailLanguage, 0, eEmailType.CateringRemainder);
                    else
                        FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.CateringAdmin);
                    
                    SetConfDateTime(eEmailType.CateringAdmin);
                    BuildEmailBody();

                    theEmail = new vrmEmail();
                    theEmail.emailTo = user.Email;
                    theEmail.emailFrom = confHost.Email;
                    theEmail.Message = emailContent;
                    theEmail.Subject = emailSubject;
                    theEmail.orgID = conf.orgId;

                    theEmail.UUID = getEmailUUID();
                    m_IemailDao.Save(theEmail);


                    //FB 1933 Starts
                    if (work.Notify != 0)
                    {
                        if (work.AdminID != confHost.userid)
                        {
                            userPrefLang = confHost.Language;
                            userEmailLang = confHost.EmailLangId;
                            //userDateFormat = ((confHost.DateFormat.Trim() == "") ? "MM/dd/yyyy" : confHost.DateFormat.Trim());
                            SetEmailDateFormat(confHost.DateFormat.Trim(), ref userDateFormat); //FB 2555
                            //userTimeFormat = ((confHost.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                            SetEmailTimeFormat(confHost.TimeFormat.Trim(), ref userTimeFormat);//FB 2588
                            tzDisplay = ((confHost.Timezonedisplay.Trim() == "") ? "0" : confHost.Timezonedisplay.Trim());
                            usrTimezone = confHost.TimeZone;

                            SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                            emailObject.Recipient = confHost.FirstName + " " + confHost.LastName;
                            emailObject.Start_byDatetime = hostStartByDate;
                            emailObject.End_byDatetime = hostEndByDate;
                            emailObject.Tobecompleted = hostEndByDate;
                            emailObject.DeliveryDatetime = hostDelivryDate;
                            if (work.Type == woConstant.CATEGORY_TYPE_CA)
                            {
                                if(work.sendReminder) //FB 2189
                                    FetchEmailString(emailLanguage, 0, eEmailType.CateringRemainder);
                                else
                                    FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.CateringAdmin);
                                
                                SetConfDateTime(eEmailType.CateringAdmin);
                            }
                            BuildEmailBody();
                                                        theEmail = new vrmEmail();
                            theEmail.emailTo = confHost.Email;
                            theEmail.emailFrom = confHost.Email;
                            theEmail.Message = emailContent;
                            theEmail.Subject = emailSubject;
                            theEmail.orgID = conf.orgId;

                            theEmail.UUID = getEmailUUID();
                            m_IemailDao.Save(theEmail);
                        }
                    }
                    //FB 1933 Ends
                }

                //if (work.AdminID != confHost.userid)
                //{
                //    theEmail = new vrmEmail();
                //    theEmail.emailTo = confHost.Email;
                //    theEmail.emailFrom = confHost.Email;
                //    theEmail.Message = emailContent;
                //    theEmail.Subject = emailSubject;
                //    theEmail.orgID = conf.orgId;
                //    theEmail.UUID = getEmailUUID();
                //    m_IemailDao.Save(theEmail);
                    
                //} 
               
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
    
        #region  PasswordRequest
        /// <summary>
        /// Password Request
        /// </summary>
        /// <param name="vrmuser"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool PasswordRequest(vrmUser requestor, ref vrmDataObject obj)
        {
            bool bRet = true;
            cryptography.Crypto crypto = null;
            //int userEmailLang = 1;  //default en
            //int userLanguage = 1;   //default en
            string URL = "";//ZD 100263
            try
            {
                if (requestor != null)
                {

                    organizationID = requestor.companyId;
                    if (organizationID < 11)
                        organizationID = defaultOrgId;

                    if (orgInfo == null)
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (sysTech == null)
                        sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                    //userEmailLang =   requestor.EmailLangId;
                    //userLanguage  = requestor.Language;

                    //SetEmailLanguage(ref userLanguage, ref userEmailLang); //Decision for email language

                    SetWebsiteURL(); //ZD 100263
                    FetchEmailString(orgInfo.EmailLangId, 0, eEmailType.ForgotPassword);

                    emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                    emailContent = emailContent.Replace("{2}", requestor.FirstName.Replace("||", "\"").Replace("!!", "'") + " " + requestor.LastName.Replace("||", "\"").Replace("!!", "'")); // FB 1888
                    //ZD 100263 Start
                    URL = websiteURL + "/en/ChangePassword.aspx?t=" + requestor.RequestID;

                    emailContent = emailContent.Replace("{81}", URL);
                    //ZD 100263
                    //FB 2189 Starts
                    emailContent = emailContent.Replace("{37}", sysTech.name);
                    emailContent = emailContent.Replace("{38}", sysTech.email);
                    emailContent = emailContent.Replace("{39}", sysTech.phone);
                    //FB 2189 Ends
					// Commented for ZD 100263
                    //crypto = new cryptography.Crypto();
                    //emailContent = emailContent.Replace("{42}", crypto.decrypt(requestor.Password));

                    if (sysTech != null)     //Organization Module
                    {
                        vrmEmail theEmail = new vrmEmail();
                        theEmail.emailTo = requestor.Email;
                        theEmail.emailFrom = sysTech.email;
                        theEmail.Message = emailContent;
                        theEmail.Subject = emailSubject;
                        theEmail.UUID = getEmailUUID();
                        theEmail.orgID = requestor.companyId; //Added for FB 1710

                        m_IemailDao.Save(theEmail);
                    }
                    else // No support email entry found in the database so throw an error
                    {
                        myVRMException e = new myVRMException(425);
                        m_log.Error("vrmException", e);
                        obj.outXml = myVRMException.toXml("Unable to find Support email address in the database.");
                        return false;
                    }

                    obj.outXml = "<emailLoginInfo><firstName>" + requestor.FirstName + "</firstName><lastName>" + requestor.LastName + "</lastName></emailLoginInfo>";
                }
            }
            catch (Exception e)
            {
                m_log.Error("PasswordRequest: ", e);
                throw e;
            }
            return bRet;
        }
        #endregion

        #region  RequestVRMAccount
        /// <summary>
        /// RequestVRMAccount
        /// </summary>
        /// <param name="reqVal"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RequestVRMAccount(ref StringDictionary reqVal, ref vrmDataObject obj)
        {
            try
            {
                if (reqVal == null)
                {
                    obj.outXml = "Input xml error";
                    return false;
                }
                int.TryParse(reqVal["orgid"], out organizationID);
                
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                int usrPrefLang = orgInfo.Language;
                int usrEmailLang = orgInfo.EmailLangId;
                SetEmailLanguage(ref usrPrefLang, ref usrEmailLang);
                FetchEmailString(emailLanguage, 0, eEmailType.RequestAccount);

                //1,2,44,51,52,53,54
                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{2}", sysTech.name);
                emailContent = emailContent.Replace("{44}", reqVal["email"]);
                emailContent = emailContent.Replace("{51}", reqVal["fname"].Replace("||", "\"").Replace("!!", "'")); // FB 1888
                emailContent = emailContent.Replace("{52}", reqVal["lname"].Replace("||", "\"").Replace("!!", "'")); // FB 1888
                emailContent = emailContent.Replace("{53}", reqVal["login"]);
                emailContent = emailContent.Replace("{54}", reqVal["addinfo"]);
                //FB 2189 Starts
                emailContent = emailContent.Replace("{37}", sysTech.name);
                emailContent = emailContent.Replace("{38}", sysTech.email);
                emailContent = emailContent.Replace("{39}", sysTech.phone);
                //FB 2189 Ends

                if (sysTech != null)
                {
                    vrmEmail theEmail = new vrmEmail();
                    theEmail.emailTo = sysTech.email;
                    theEmail.emailFrom = reqVal["email"];
                    theEmail.Message = emailContent;
                    theEmail.Subject = emailSubject;
                    theEmail.UUID = getEmailUUID();
                    theEmail.orgID = organizationID;
                    m_IemailDao.Save(theEmail);
                }
                else // No support email entry found in the database so throw an error
                {
                    myVRMException e = new myVRMException(425);
                    m_log.Error("vrmException", e);
                    obj.outXml = myVRMException.toXml("Unable to find Support email address in the database.");
                    return false;
                }
                obj.outXml = "<success>1</success>";
            }
            catch (Exception e)
            {
                m_log.Error("PasswordRequest: ", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion

        //New methods added for FB 1830 end

        //FB 1860

        #region  Get Blocked Emails
        /// <summary>
        /// GetBlockedEmails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetBlockedEmails(ref OrgData org, ref vrmDataObject obj)
        {
            List<vrmEmail> orgEmails = null; 
            List<ICriterion> criterionList = null;
            StringBuilder m_email = new StringBuilder();
            String message = "";
            try
            {
                if (org == null)
                {
                    obj.outXml = "Input xml error";
                    return false;
                }
                organizationID = org.OrgId;

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (org.MailBlocked == 1)
                {

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("orgID", organizationID));
                    criterionList.Add(Expression.Eq("Iscalendar", 0));
                    criterionList.Add(Expression.Lt("RetryCount", 25));
                    criterionList.Add(Expression.Eq("Release", 0));//Release Email
                    orgEmails = m_IemailDao.GetByCriteria(criterionList);
                    m_email.Append("<Emails>");
                    if (orgEmails.Count > 0)
                    {
                        for (int usrmailscnt = 0; usrmailscnt < orgEmails.Count; usrmailscnt++)
                        {
                            message = orgEmails[usrmailscnt].Message;
                            message = StripTagsCharArray(message);



                            m_email.Append("<Email>");
                            m_email.Append("<uuid>" + orgEmails[usrmailscnt].UUID.ToString() + "</uuid>");
                            m_email.Append("<emailFrom>" + orgEmails[usrmailscnt].emailFrom + "</emailFrom>");
                            m_email.Append("<emailTo>" + orgEmails[usrmailscnt].emailTo + "</emailTo>");
                            m_email.Append("<cc>" + orgEmails[usrmailscnt].cc + "</cc>");
                            m_email.Append("<BCC>" + orgEmails[usrmailscnt].BCC + "</BCC>");
                            m_email.Append("<Subject>" + orgEmails[usrmailscnt].Subject + "</Subject>");
                            m_email.Append("<DateTimeCreated>" + orgEmails[usrmailscnt].DateTimeCreated + "</DateTimeCreated>");
                            m_email.Append("<Message>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(message.ToString()) + "</Message>"); //FB 2236
                            m_email.Append("<RetryCount>" + orgEmails[usrmailscnt].RetryCount + "</RetryCount>");
                            m_email.Append("<LastRetryDateTime>" + orgEmails[usrmailscnt].LastRetryDateTime + "</LastRetryDateTime>");
                            m_email.Append("<Attachment>" + orgEmails[usrmailscnt].Attachment + "</Attachment>");
                            m_email.Append("<Iscalendar>" + orgEmails[usrmailscnt].Iscalendar + "</Iscalendar>");
                            m_email.Append("<orgID>" + orgEmails[usrmailscnt].orgID + "</orgID>");
                            m_email.Append("</Email>");
                        }

                    }
                    m_email.Append("</Emails>");
                    obj.outXml = m_email.ToString();

                }
            }
            catch (Exception e)
            {
                m_log.Error("PasswordRequest: ", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion

        #region  Get Blocked Emails
        /// <summary>
        /// GetBlockedEmails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetBlockedEmails(ref vrmUser usr, ref vrmDataObject obj)
        {
            List<vrmEmail> usrEmails = null; 
            List<ICriterion> criterionList = null;
            StringBuilder m_email = new StringBuilder();
            String uid = "";
            Int32 uuid = 0;
            String message = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/uuid");
                if (node != null)
                    uid = node.InnerXml.Trim();

                Int32.TryParse(uid, out uuid);

                if (usr == null)
                {
                    obj.outXml = "Input xml error";
                    return false;
                }
                organizationID = usr.companyId;

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (usr.MailBlocked == 1 || uuid != 0)
                {

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("emailFrom", usr.Email));
                    criterionList.Add(Expression.Eq("orgID", organizationID));
                    criterionList.Add(Expression.Eq("Iscalendar", 0));
                    criterionList.Add(Expression.Lt("RetryCount", 25));
                    criterionList.Add(Expression.Eq("Release", 0));//Release email

                    if (uuid != 0)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("UUID", uuid));
                    }
                    usrEmails = m_IemailDao.GetByCriteria(criterionList);
                    m_email.Append("<Emails>");
                    if (usrEmails.Count > 0)
                    {
                        for (int usrmailscnt = 0; usrmailscnt < usrEmails.Count; usrmailscnt++)
                        {
                            message = usrEmails[usrmailscnt].Message;

                            if (uuid <= 0)
                                message = StripTagsCharArray(message);


                            m_email.Append("<Email>");
                            m_email.Append("<uuid>" + usrEmails[usrmailscnt].UUID.ToString() + "</uuid>");
                            m_email.Append("<emailFrom>" + usrEmails[usrmailscnt].emailFrom + "</emailFrom>");
                            m_email.Append("<emailTo>" + usrEmails[usrmailscnt].emailTo + "</emailTo>");
                            m_email.Append("<cc>" + usrEmails[usrmailscnt].cc + "</cc>");
                            m_email.Append("<BCC>" + usrEmails[usrmailscnt].BCC + "</BCC>");
                            m_email.Append("<Subject>" + usrEmails[usrmailscnt].Subject + "</Subject>");
                            m_email.Append("<DateTimeCreated>" + usrEmails[usrmailscnt].DateTimeCreated + "</DateTimeCreated>");
                            m_email.Append("<Message>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(message.ToString()) + "</Message>"); //FB 2236
                            m_email.Append("<RetryCount>" + usrEmails[usrmailscnt].RetryCount + "</RetryCount>");
                            m_email.Append("<LastRetryDateTime>" + usrEmails[usrmailscnt].LastRetryDateTime + "</LastRetryDateTime>");
                            m_email.Append("<Attachment>" + usrEmails[usrmailscnt].Attachment + "</Attachment>");
                            m_email.Append("<Iscalendar>" + usrEmails[usrmailscnt].Iscalendar + "</Iscalendar>");
                            m_email.Append("<orgID>" + usrEmails[usrmailscnt].orgID + "</orgID>");
                            m_email.Append("</Email>");
                        }

                    }
                    m_email.Append("</Emails>");
                    obj.outXml = m_email.ToString();
                    
                }

                
                
            }
            catch (Exception e)
            {
                m_log.Error("PasswordRequest: ", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion

        #region  DeleteEmails
        /// <summary>
        /// GetBlockedEmails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteEmails(ref vrmDataObject obj)
        {
            vrmEmail deleteEmail = null; 
            String Uid = "";
            Int32 Uuid = 0;
            //Release Email
            String ReleaseUid = "";
            Int32 ReleaseUuid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNodeList Emails = xd.SelectNodes("//DeleteEmailsList/DeleteEmail");

                m_log.Error("Moving to Delete");

                if (Emails != null)
                {
                    for (Int32 emailCnt = 0; emailCnt < Emails.Count;emailCnt ++)
                    {
                        XmlNode node = Emails[emailCnt];
                        Uid = "";

                        if(node.SelectSingleNode("uuid") != null)
                            Uid = node.SelectSingleNode("uuid").InnerText;

                        Int32.TryParse(Uid,out Uuid);

                        deleteEmail = m_IemailDao.GetById(Uuid);
                        if (deleteEmail != null)
                        {
                            deleteEmail.RetryCount = 25;
                            m_IemailDao.Save(deleteEmail);
                        }

                    }
                }

                //Release Email
                m_log.Error("Moving to release");

                XmlNodeList releaseEmails =  xd.SelectNodes("//DeleteEmailsList/ReleaseEmail");

                if (releaseEmails != null)
                {
                    m_log.Error("Moving to release count" + releaseEmails.Count.ToString());
                    XmlNode nodeRelease = null;
                    for (Int32 relemailCnt = 0; relemailCnt < releaseEmails.Count; relemailCnt++)
                    {
                        nodeRelease = releaseEmails[relemailCnt];
                        ReleaseUid = "";

                        if (nodeRelease.SelectSingleNode("uuid") != null)
                            ReleaseUid = nodeRelease.SelectSingleNode("uuid").InnerText;

                        Int32.TryParse(ReleaseUid, out ReleaseUuid);

                        deleteEmail = m_IemailDao.GetById(ReleaseUuid);
                        if (deleteEmail != null)
                        {
                            deleteEmail.Release = 1;
                            m_IemailDao.Save(deleteEmail);
                        }

                    }
                }
             

                obj.outXml = "<success>1</success>";
            }
            catch (Exception e)
            {
                m_log.Error("PasswordRequest: ", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion

        #region Remove HTML

        public string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        #endregion


        //FB 1860

        //FB 1926

        #region  SendReminderEmails
        /// <summary>
        /// SendReminderEmails
        /// </summary>
        /// <param name="confList"></param>
        /// <returns></returns>
        public bool SendReminderEmails()
        {
            try
            {
                emailObject = new sConferenceEmail();

                if (conf == null)
                    return false;

                SetWebsiteURL();

                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                SetHostLangFolder(confHost.Language);

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;

                emailObject.MailLogo = AttachEmailLogo(organizationID);

                List<ICriterion> criterionListVMR = new List<ICriterion>();
                criterionListVMR.Add(Expression.Eq("confid", conf.confid));

                if (conf.instanceid > 0)
                    criterionListVMR.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionListVMR.Add(Expression.Eq("instanceid", 1));

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionListVMR);//FB 2636

                SetConfRoomString();
                SetConfGuestRoomString(); //FB 2426
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");//FB 2236
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();

                Password = conf.password;//FB 2051
                if (conf.password.Trim() == "")
                    Password = "N/A";
                emailObject.Password = Password;

                //GetAttachementsStr(); FB 2154

                //Fetch Participants list
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                List<vrmConfUser> uList = m_vrmConfUserDAO.GetByCriteria(criterionList,true);

                string confcodepin = ""; //FB 1642
                vrmConfUser us;
                //Sending mail to participants
                for (int i = 0; i < uList.Count; i++)
                {
                    us = uList[i]; //FB 1830
                    if (us.partyNotify == 1) //FB 1830
                    {
                        confcodepin = "";
                        if (conf.conftype == 2 && us.audioOrVideo == 1) //FB 1744 //FB 2381
                        {
                            confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "P", us.userid);
                        }
                        emailObject.AudioInfo = confcodepin;

                        if (!sendParticipantEmail(us.userid, us.invitee, eEmailType.PartyRemainder))
                            m_log.Error("Cannot send participant remainder email [userid]" + us.userid.ToString());
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SendReminderEmails", e);
                throw e;
            }
        }
        #endregion

        #region  Send Automated Emails
        /// <summary>
        /// Send Automated Emails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public void SendAutomatedReminders(Int32 orgID,ref List<Reminder> remiders)
        {
            Int32 updtMask = 0;
            DateTime ConfTIme = DateTime.Now;
            List<ICriterion> ConfReminder;
            List<vrmConference> conferences;
            try
            {
                if (orgID >= 11)
                {
                    organizationID = orgID;

                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (orgInfo.ReminderMask > 0)
                    {
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref ConfTIme);
                        ConfTIme = ConfTIme.Subtract(new TimeSpan(0, 0, ConfTIme.Second));
                        ConfTIme = ConfTIme.AddSeconds(45);

                        

                        foreach (Reminder reminder in remiders)
                        {
                            DateTime reminderUplimit = ConfTIme;
                            DateTime reminderLwlimit = ConfTIme;

                            if (Convert.ToBoolean(orgInfo.ReminderMask & reminder.ReminderType))
                            {
                                reminderLwlimit = reminderLwlimit.AddMinutes(reminder.ReminderLowerLimit);
                                reminderUplimit = reminderUplimit.AddMinutes(reminder.ReminderUpperLimit);

                                ConfReminder = new List<ICriterion>();
                                ConfReminder.Add(Expression.Ge("isReminder", 16));
                                ConfReminder.Add(Expression.Ge("confdate", reminderLwlimit));
                                ConfReminder.Add(Expression.Le("confdate", reminderUplimit));
                                ConfReminder.Add(Expression.Eq("status", 0));
                                ConfReminder.Add(Expression.Eq("orgId", organizationID));
                                ConfReminder.Add(Restrictions.Lt(
                                                Projections.SqlProjection("(isReminder & "+ reminder.ReminderType.ToString() +") as bitWiseResult", new[] { "bitWiseResult" }, new NHibernate.Type.IType[] { NHibernateUtil.Int32 }), 1)
                                                         );

                                conferences = m_vrmConfDAO.GetByCriteria(ConfReminder,true);

                                
                                

                                foreach (vrmConference reminderConf in conferences)
                                {
                                    conf = reminderConf;
                                    if(!SendReminderEmails())
                                    {
                                        m_log.Error("Error Sending Automated Reminder Emails:");
                                        continue;
                                    }

                                    updtMask = 0;
                                    updtMask = reminderConf.isReminder;
                                    updtMask += reminder.ReminderType;
                                    reminderConf.isReminder = updtMask;
                                    

                                }

                                if (conferences.Count > 0)
                                    m_vrmConfDAO.SaveOrUpdateList(conferences);
                            }
                        }
 
                    }

                    
                }
                
            }
            catch (Exception e)
            {
                m_log.Error("Send Automated Emails: ", e);
            }
        }
        #endregion

        //FB 2027 - SetTerminalCtrl
        #region SendThresholdEmail
        /// <summary>
        /// SendThresholdEmail
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="conf"></param>
        /// <param name="currIsdnCost"></param>
        /// <param name="isdnthresholdCost"></param>
        /// <returns></returns>
        public bool SendThresholdEmail(vrmMCU mcu, vrmConference conf, Double currIsdnCost, Double isdnthresholdCost)
        {
            try
            {
                int userLanguage=0,userEmailLang=0;
                vrmEmail theEmail = new vrmEmail();
                vrmUser mcuApp = null;
                CultureInfo cInfo = null;
                String usrDateFormat = "MM/dd/yyyy";

                if (organizationID < defaultOrgId)
                    organizationID = 11;

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);
                
                if(mcu.MCUApprover.Count > 0)
                    mcuApp = m_IuserDao.GetByUserId(mcu.MCUApprover[0].approverid);

                if (mcuApp != null)
                {
                    theEmail.emailTo = mcuApp.Email;
                    userLanguage = mcuApp.Language;
                    userEmailLang = mcuApp.EmailLangId;
                    SetEmailLanguage(ref userLanguage, ref userEmailLang);

                    usrDateFormat = mcuApp.DateFormat;
                }
                else
                    theEmail.emailTo = sysTech.email;
                
                FetchEmailString(emailLanguage, 0, eEmailType.MCU_Threshold_Email);

                
                if (usrDateFormat.ToLower() == "dd/mm/yyyy")
                {
                    cInfo = new CultureInfo("fr-FR");
                     emailObject.CurrencyFormat = "�";
                }
                else
                {
                    cInfo = new CultureInfo("en-US");
                    emailObject.CurrencyFormat = "$";
                }
                         
                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{3}", mcu.BridgeName);
                emailContent = emailContent.Replace("{59}", isdnthresholdCost.ToString("n", cInfo));
                emailContent = emailContent.Replace("{60}", currIsdnCost.ToString("n", cInfo));
                emailContent = emailContent.Replace("{5}", conf.confnumname.ToString());
                //FB 2189 Starts
                emailContent = emailContent.Replace("{37}", sysTech.name);
                emailContent = emailContent.Replace("{38}", sysTech.email);
                emailContent = emailContent.Replace("{39}", sysTech.email);
                //FB 2189 Ends
                if (sysTech != null)
                {
                    theEmail.emailFrom = sysTech.email;
                    theEmail.Message = emailContent;
                    theEmail.Subject = emailSubject;
                    theEmail.UUID = getEmailUUID();
                    theEmail.orgID = organizationID;
                    m_IemailDao.Save(theEmail);
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("SendThresholdEmail", ex);
                return false;
            }
        }
        #endregion

        //FB 2154
        private Boolean CheckEmailDomain(String emailAddress)
        {
            
            List<vrmEmailDomain> emailDomains = null;
            string usrDomain = "";
            bool inDomain = false;

            try
            {

                if (emailAddress != "")
                {
                    m_IEmailDomainDAO = m_OrgDAO.GetEmailDomainDAO();
                    emailDomains = m_IEmailDomainDAO.GetActiveEmailDomainbyOrgId(organizationID);

                    if (emailAddress.Split('@').Length > 1)
                    {
                        usrDomain = emailAddress.Split('@')[1];

                        if (usrDomain != "")
                        {
                            var eDomains = from orgDomain in emailDomains
                                           where orgDomain.Emaildomain.ToUpper().Trim() == usrDomain.ToUpper().Trim()
                                           select orgDomain;

                            foreach (vrmEmailDomain domain in eDomains)
                                inDomain = true;
 
                        }
                    }
                    
                }

            }
            catch (Exception spEX1)
            {

                m_log.Error(spEX1);
                inDomain = false;
            }

            return inDomain;

        }

        //FB 2158
        #region  SendMailtoNewUser
        /// <summary>
        /// SendMailtoNewUser
        /// </summary>
        /// <param name="newUser"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SendMailtoNewUser(ref StringDictionary newUser, ref vrmDataObject obj)
        {
            try
            {
                if (newUser == null)
                {
                    obj.outXml = "Input xml error";
                    return false;
                }
                Int32.TryParse(newUser["orgid"], out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                SetWebsiteURL();
                FetchEmailString(orgInfo.EmailLangId, 0, eEmailType.WelcomeToMyVRM);

                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{2}", newUser["fname"].Replace("||", "\"").Replace("!!", "'") + " " + newUser["lname"].Replace("||", "\"").Replace("!!", "'"));
                emailContent = emailContent.Replace("{36}", websiteURL);
                emailContent = emailContent.Replace("{41}", newUser["email"]);
                emailContent = emailContent.Replace("{42}", newUser["password"]);

                if (sysTech != null)
                {
                    vrmEmail theEmail = new vrmEmail();
                    theEmail.emailTo = newUser["email"];
                    theEmail.emailFrom = sysTech.email;
                    theEmail.Message = emailContent;
                    theEmail.Subject = emailSubject;
                    theEmail.UUID = getEmailUUID();
                    theEmail.orgID = organizationID;
                    theEmail.Release = 0; //FB 2473


                    m_IemailDao.Save(theEmail);
                }
                else // No support email entry found in the database so throw an error
                {
                    myVRMException e = new myVRMException(425);
                    m_log.Error("vrmException", e);
                    obj.outXml = myVRMException.toXml("Unable to find Support email address in the database.");
                    return false;
                }

                obj.outXml = "<NewUserLoginInfo><firstName>" + newUser["fname"] + "</firstName><lastName>" + newUser["lname"] + "</lastName></NewUserLoginInfo>";
            }
            catch (Exception e)
            {
                m_log.Error("SendMailtoNewUser: ", e);
                throw e;
            }
            return true;
        }
        #endregion

        //FB 2218
        #region RecurringDates
        /// <summary>
        /// RecurringDates
        /// </summary>
        /// <param name="xd"></param>
        /// <param name="errNo"></param>
        /// <param name="recurDates"></param>
        /// <param name="recurInfo"></param>
        /// <returns></returns>
        private bool CustomRecurringDates(ref vrmConference conf, ref int errNo, ref List<RecurrenceDates> deletedIns, ref List<vrmConference> changedIns, bool isParticipant, ref List<vrmRecurInfoDefunct> defunctRecur,ref DateTime setup,ref DateTime teradown,ref TimeSpan dur)
        {

            List<ICriterion> criterionlst = null;
            vrmRecurInfoDefunct recurInfo = null;
            RecurrenceFactory recFactory = null;
            DateTime strtDate, stUpDate, trDwnDate, endDate = DateTime.MinValue;
            bool isNotDeleted = false;
            int rIndex = 0;
            List<sRecurrDates> recurDates = null; 
            List<DateTime> delDatetimes = null;
            DateTime today = DateTime.Now;
            iCalDateTime evticaldate = null;
            RecurrenceDates rdate = null;
            List<vrmConference> confs = null;
            Config configs = null;
            ArrayList mappings = null;
            DateTime removeDate = DateTime.MinValue;
           

            try
            {
                criterionlst = new List<ICriterion>();
                criterionlst.Add(Expression.Eq("confid", conf.confid));

                confs = m_vrmConfDAO.GetByCriteria(criterionlst);

                if (defunctRecur.Count <= 0)
                {
                    
                    recurInfo = m_confDAO.GetConfRecurDefunctDAO().GetByCriteria(criterionlst, true)[0];
                }
                else
                    recurInfo = defunctRecur[0];



                if (recurInfo.recurType < 5) //Regular Recurrence Pattern
                {
                    teradown = setup = DateTime.MinValue;

                    #region Regular Pattern

                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref today);

                    recFactory = new RecurrenceFactory();
                    configs = new Config(m_configPath);
                    configs.Initialize();
                    recFactory.maxRecurInstance = configs.ConfReccurance; //Default instance limit



                    recFactory.recurInfo = MapRecurProperties(recurInfo);
                    recFactory.startHour = recurInfo.startTime.ToString("HH");
                    recFactory.startMin = recurInfo.startTime.ToString("mm"); ;
                    recFactory.startSet = recurInfo.startTime.ToString("tt"); ;
                    recFactory.setDuration = recurInfo.SetupDuration;
                    recFactory.tearDuration = recurInfo.TeardownDuration;

                    recurDates = new List<sRecurrDates>();

                    switch (recurInfo.recurType)
                    {
                        case 1:    // recurring daily
                            {
                                recurDates = recFactory.GetDailyRecurDates();
                                break;
                            }
                        case 2:   // weekly
                            {
                                recurDates = recFactory.GetWeeklyRecurDates();
                                break;
                            }
                        case 3: // monthly
                            {
                                recurDates = recFactory.GetMonthlyRecurDates();
                                break;
                            }
                        case 4: //yearly
                            {
                                recurDates = recFactory.GetYearlyRecurDates();
                                break;
                            }
                    }
                    #endregion


                }

                for (int rcnt = 0; rcnt < recurDates.Count; rcnt++)
                {
                    isNotDeleted = false;
                    rIndex = -1;

                    strtDate = recurDates[rcnt].RecDatetime;
                    stUpDate = recurDates[rcnt].RecDatetime.Add(new TimeSpan(0, 0, recurDates[rcnt].SetDuration, 0));
                    endDate = recurDates[rcnt].RecDatetime.Add(new TimeSpan(0, 0, recurDates[rcnt].Duration, 0));
                    trDwnDate = endDate.Subtract(new TimeSpan(0, 0, recurDates[rcnt].TearDuration, 0));

                    if (teradown == DateTime.MinValue && setup == DateTime.MinValue)
                    {
                        teradown = trDwnDate;
                        setup = stUpDate;
                        dur = trDwnDate.Subtract(stUpDate);
                    }

                    for (int ccnt = 0; ccnt < confs.Count; ccnt++)
                    {
                        
                      
                        if (strtDate.Year == confs[ccnt].confdate.Year && strtDate.Month == confs[ccnt].confdate.Month && strtDate.Day == confs[ccnt].confdate.Day)
                            isNotDeleted = true;

                        if (isNotDeleted)
                        {
                            if (mappings == null)
                                mappings = new ArrayList();

                            if (!mappings.Contains(confs[ccnt]))
                                mappings.Add(confs[ccnt]);
                            else
                                continue;

                            rIndex = ccnt;

                            if (stUpDate.Subtract(confs[rIndex].SetupTime).Minutes != 0 || trDwnDate.Subtract(confs[rIndex].TearDownTime).Minutes != 0)
                            {
                                confs[rIndex].settingtime = stUpDate;
                                if (changedIns == null)
                                    changedIns = new List<vrmConference>();

                                changedIns.Add(confs[rIndex]);

                            }

                            break;
                        }

                    }

                    if (rIndex < 0 && !isNotDeleted)
                    {
                        if (deletedIns == null)
                            deletedIns = new List<RecurrenceDates>();

                        if (delDatetimes == null)
                            delDatetimes = new List<DateTime>();

                        evticaldate = new iCalDateTime(stUpDate.Year, stUpDate.Month, stUpDate.Day, 00, 00, 00);
                        rdate = new RecurrenceDates();
                        rdate.Add(evticaldate);
                        deletedIns.Add(rdate);
                        delDatetimes.Add(stUpDate);

                    }


                }


                for (int ccnt = 0; ccnt < confs.Count; ccnt++)
                {
                    removeDate = DateTime.MinValue;

                    if (mappings == null)
                        mappings = new ArrayList();

                    if (mappings.Contains(confs[ccnt]))
                        continue;

                    for (int delcnt = 0; delcnt < delDatetimes.Count; delcnt++)
                    {
                        removeDate = delDatetimes[delcnt];
                        confs[ccnt].settingtime = delDatetimes[delcnt];

                        if (changedIns == null)
                            changedIns = new List<vrmConference>();

                        changedIns.Add(confs[ccnt]);

                        break;
                    }

                    if (removeDate > DateTime.MinValue) 
                    {
                        if (delDatetimes.Contains(removeDate))
                        {
                            evticaldate = new iCalDateTime(removeDate.Year, removeDate.Month, removeDate.Day, 00, 00, 00);
                            rdate = new RecurrenceDates();
                            rdate.Add(evticaldate);
                            if (deletedIns.Contains(rdate))
                                deletedIns.Remove(rdate);

                            delDatetimes.Remove(removeDate);
                        }
                    }

                }

            }
            catch (myVRMException me)
            {
                m_log.Error("MyvrmException-RecurringDates: ", me);
                errNo = 200;
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SystemException-RecurringDates: ", e);
                errNo = 200;
                return false;
            }
            return true;
        }
        #endregion

        private vrmRecurInfo MapRecurProperties(vrmRecurInfoDefunct defunctinfo)
        {
            vrmRecurInfo recinfo = null;
            try
            {

                recinfo = new vrmRecurInfo();
                recinfo.RecurringPattern = defunctinfo.RecurringPattern;
                recinfo.recurType = defunctinfo.recurType;
                recinfo.occurrence = defunctinfo.occurrence;
                recinfo.gap = defunctinfo.gap;
                recinfo.subType = defunctinfo.subType;
                recinfo.dayno = defunctinfo.dayno;
                recinfo.days = defunctinfo.days;
                recinfo.endTime = defunctinfo.endTime;
                recinfo.endType = defunctinfo.endType;
                recinfo.startTime = defunctinfo.startTime;
                recinfo.duration = defunctinfo.duration;
                recinfo.timezoneid = defunctinfo.timezoneid;

            }
            catch (Exception e)
            {

                m_log.Error("SystemException-RecurringDates: ", e);
                recinfo = null;
            }

            return recinfo;

        }
        //FB 2218

        //FB 2268
        #region iPhoneHelpRequest
        /// <summary>
        /// iPhone Help Request Mail
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool PhoneHelpRequestMail(ref vrmDataObject obj)
        {
            try
            {
                Double longitude = 0.0, latitude = 0.0;
                myVRMException myvrmEx = null;
                String[] helpemails = null;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int userID = 0;
                node = xd.SelectSingleNode("//HelpRequestMail/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//HelpRequestMail/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//HelpRequestMail/longitude");
                if (node != null)
                    Double.TryParse(node.InnerText.Trim(), out  longitude);

                node = xd.SelectSingleNode("//HelpRequestMail/latitude");
                if (node != null)
                    Double.TryParse(node.InnerText.Trim(), out latitude);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);
                
                vrmUser user = new vrmUser();
                user = m_IuserDao.GetByUserId(userID);

                String PhoneNumber = "", Email = "";

                PhoneNumber = user.CellPhone;
                if (user.HelpReqEmailID != null)
                {
                    Email = user.HelpReqEmailID;
                    helpemails = Email.Split('�');//ALT 0214
                }

                var marker = new StaticMap.Marker();
                var map = new StaticMap
                {
                    Width = 200,
                    Height = 200,
                    Zoom = 15,
                    LatCenter = latitude,
                    LngCenter = longitude
                };

                // add marker to centre point 
                marker.Lat = map.LatCenter;
                marker.Lng = map.LngCenter;
                marker.Size = StaticMap.mSize.Mid;
                marker.Color = StaticMap.mColor.Blue;
                marker.Character = "1";
                map.Markers.Add(marker);

                // render map
                // change type and render mobile
                map.Type = StaticMap.mType.Mobile;
                websiteURL = map.Render();

                FetchEmailString(orgInfo.EmailLangId, 0, eEmailType.HelpRequest);

                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{2}", user.FirstName.Replace("||", "\"").Replace("!!", "'") + " " +user.LastName.Replace("||", "\"").Replace("!!", "'"));
                emailContent = emailContent.Replace("{37}", sysTech.name);
                emailContent = emailContent.Replace("{38}", sysTech.email);
                emailContent = emailContent.Replace("{39}", sysTech.phone);
                emailContent = emailContent.Replace("{40}", websiteURL);


                if (helpemails != null)
                {
                    vrmEmail theEmail = new vrmEmail();
                    theEmail.emailFrom = user.Email;
                    theEmail.Message = emailContent;
                    theEmail.Subject = emailSubject;
                    theEmail.orgID = organizationID;
                    theEmail.Release = 1;

                    for (int i = 0; i < helpemails.Length; i++ )
                    {
                        if (helpemails[i].Trim() != "")
                        {
                            theEmail.emailTo = helpemails[i].Trim();
                            theEmail.UUID = getEmailUUID();

                            m_IemailDao.Save(theEmail);
                        }
                    }
                    if(helpemails.Length <= 0 )// No Help Request email entry found in the database so throw an error
                    {
                        myvrmEx = new myVRMException(606);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                else // No Help Request email entry found in the database so throw an error
                {
                    myvrmEx = new myVRMException(606);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                obj.outXml = "<success>1</success>";

            }
            catch (Exception e)
            {
                m_log.Error("HelpRequestMail", e);
                obj.outXml = ""; ;
                return false;
            }
            return true;
        }
        #endregion
        
        //FB 2136 Security Badge logic ... start
        #region SecurityBadgeAtt
        /// <summary>
        /// SecurityBadgeAtt
        /// </summary>
        public string SecurityBadgeAtt(int confuid, int partyid)
        {
            try
            {
                string att = "";
                string logoName = "";
                string testString = "\\en\\" + LanguageUpload + "\\";//FB 2154
                string quote = "\"";
                string slashQuote = "/";
                string secBadgeName = "SecImg_" + confuid + "_" + partyid + ".jpg";
                string logoId = "SecImg_" + confuid + "_" + partyid;

                logoName = quote + websiteURL + slashQuote + "en" + slashQuote + LanguageUpload + slashQuote + secBadgeName + quote;

                Config configs = new Config(m_configPath);
                configs.Initialize();

                Byte[] srcBT;
                String srcFile = configs.mailLogoPath + secBadgeName;

                FileStream sr = new FileStream(srcFile, FileMode.Open);

                srcBT = new byte[sr.Length];
                sr.Read(srcBT, 0, (int)sr.Length);
                sr.Close();

                String strbyte = Convert.ToBase64String(srcBT);

                att = "<table width=\"800px\"><tr><td><span style=\"font-family: Arial; font-size: 10pt\"><br/><strong>Security Badge</strong></span><br/></td>";
                att += "</tr><tr><td align=\"left\" ><img id=\"" + logoId + "\" src=\"" + secBadgeName + "\" alt=\"" + strbyte + "\" /></td></tr></table>";
                
                return att;
            }
            catch (Exception e)
            {
                return "";
                m_log.Error("SecurityBadgeAtt - Email", e);
            }
        }
        #endregion

        //FB 2136
        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                m_log.Error("WriteToFile", ex);                
            }
        }
        #endregion

        //FB 2348 Start

        #region SendSurveyMail
        /// <summary>
        /// SendSurveyMail
        /// </summary>
        /// <param name="surveyDuration"></param>
        /// <returns></returns>
        internal bool SendSurveyMail(int orgId)
        {
            List<vrmConference> confList = new List<vrmConference>();
            List<ICriterion> criterionList = null;
            ICriterion criterium = null;
            List<int> confsList = null;
            List<vrmConfUser> cfusrlist = null;
            try
            {
                DateTime Today = DateTime.Now;
                DateTime conffrom = new DateTime();
                DateTime confEnd = new DateTime();

                if (orgId >= 11)
                {
                    organizationID = orgId;
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (orgInfo.EnableSurvey == 0)
                        return false;

                    if (orgInfo.SurveyOption == 1) //1- Internal , 2-External .. Internal will be handled in Phase 2
                        return false;


                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("Survey", 1));
                    cfusrlist = m_vrmConfUserDAO.GetByCriteria(criterionList);

                    for (int confcnt = 0; confcnt < cfusrlist.Count; confcnt++)
                    {
                        if (confsList == null)
                            confsList = new List<int>();

                        if (!confsList.Contains(cfusrlist[confcnt].confuId))
                            confsList.Add(cfusrlist[confcnt].confuId);
                    }

                    if (confsList.Count > 0)
                    {

                        Today = DateTime.Now.AddMinutes(-orgInfo.TimeDuration);
                        
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref Today);

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Le("confEnd", Today));
                        criterionList.Add(Expression.Eq("deleted", 0));
                        criterionList.Add(Expression.Eq("sentSurvey", 0));
                        criterionList.Add(Expression.Eq("orgId", organizationID)); //FB 2659
                        criterionList.Add(Expression.In("confnumname", confsList));

                        confList = m_vrmConfDAO.GetByCriteria(criterionList, true);

                        if (confList.Count > 0)
                            SendAutomatedSurvey(confList);
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region  SendAutomatedSurvey
        /// <summary>
        /// SendAutomatedSurvey
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SendAutomatedSurvey(List<vrmConference> confList)
        {
            string userEmail = "";
            int userEmailLang = 1;  //default en
            int userLanguage = 1;   //default en
            try
            {
                List<ICriterion> criterionList = null;
                emailObject = new sConferenceEmail();
                confRoomList = new List<vrmConfRoom>();
                List<vrmConfUser> Confusers = null;
                vrmConfUser confuser = null;
                vrmGuestUser vrmguest = null;
                vrmUser vrmuser = null;
                vrmEmail theEmail = null;

                SetWebsiteURL();
                for (int confcnt = 0; confcnt < confList.Count; confcnt++)
                {
                    conf = confList[confcnt];

                    organizationID = conf.orgId;
                    if (organizationID < 11)
                        organizationID = defaultOrgId;

                    if (orgInfo == null || (confcnt != 0 && organizationID != confList[confcnt - 1].orgId))
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (sysTech == null || (confcnt != 0 && organizationID != confList[confcnt - 1].orgId))
                    {
                        sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                        emailObject.TechContact = sysTech.name;
                        emailObject.TechEmail = sysTech.email;
                        emailObject.TechPhone = sysTech.phone;
                        emailObject.MailLogo = AttachEmailLogo(organizationID);
                    }

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf.confid));
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    Confusers = m_vrmConfUserDAO.GetByCriteria(criterionList);
                    confRoomList = m_vrmConfRoomDAO.GetByCriteria(criterionList);

                    SetConfRoomString();
                    SetConfGuestRoomString(); //FB 2426
                    SetRecurrencePatern();
                    emailObject.SurveyURL = orgInfo.SurveyURL;

                    for (int i = 0; i < Confusers.Count; i++)
                    {
                        confuser = Confusers[i];

                        if (confuser.Survey != 1)
                            continue;

                        vrmuser = m_IuserDao.GetByUserId(confuser.userid);
                        if (vrmuser == null)
                        {
                            vrmguest = m_IguestUserDao.GetByUserId(confuser.userid);
                            if (vrmguest == null)
                                continue;

                            userEmail = vrmguest.Email;
                            emailObject.Recipient = vrmguest.FirstName + " " + vrmguest.LastName;
                        }
                        else
                        {
                            userLanguage = vrmuser.Language;
                            userEmailLang = vrmuser.EmailLangId;
                            userEmail = vrmuser.Email;
                            emailObject.Recipient = vrmuser.FirstName + " " + vrmuser.LastName;
                        }

                        confHost = m_IuserDao.GetByUserId(conf.owner);
                        SetEmailLanguage(ref userLanguage, ref userEmailLang);
                        FetchEmailString(emailLanguage, 0, eEmailType.SurveyEmail);
                        BuildEmailBody();

                        theEmail = new vrmEmail();
                        theEmail.emailTo = userEmail;
                        theEmail.emailFrom = confHost.Email;
                        theEmail.Message = emailContent;
                        theEmail.Subject = emailSubject;
                        theEmail.orgID = organizationID;
                        theEmail.UUID = getEmailUUID();
                        m_IemailDao.Save(theEmail);
                    }

                    conf.sentSurvey = 1;
                    m_vrmConfDAO.Update(conf);
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SendAutomatedSurvey: ", e);
                throw e;
            }

        }
        #endregion

        //FB 2348 End

        //FB 2363
        #region SendESReport

        internal bool SendESReport(String toAdd, ref String filePath, String type)
        {
            try
            {
                string sAttachment = filePath, att = "", newFileName = "";
                string testString = "\\en\\" + LanguageUpload + "\\";

                SetWebsiteURL();
                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(11);
                
                if(type == "U")
                    FetchEmailString(1, 0, eEmailType.UserReportMail);
                else if(type == "E")
                    FetchEmailString(1, 0, eEmailType.ErrorEventReport);

                att = "<span style=\"font-family: Verdana;font-size: 10pt\">";
                att += "<BR><U>Attachments: </U><BR />";

                int j = sAttachment.IndexOf(testString);
                if (j > 0)
                {
                    sAttachment = sAttachment.Substring(j + testString.Length, sAttachment.Length - (j + testString.Length));
                    sAttachment.Replace("\\", "/");
                    newFileName = sAttachment;
                    string quote = "\"";
                    string slashQuote = "/";
                    att += "<br /><a href= " + quote + websiteURL + slashQuote + "en" + slashQuote + LanguageUpload + slashQuote + //FB 2154
                    sAttachment + quote + ">" + sAttachment + "</a>";
                }
                emailContent += att;

                vrmEmail theEmail = new vrmEmail();
                theEmail.emailTo = toAdd;
                theEmail.emailFrom = sysTech.email;
                theEmail.Message = emailContent;
                theEmail.Subject = emailSubject;
                theEmail.UUID = getEmailUUID();
                theEmail.orgID = 11;

                m_IemailDao.Save(theEmail);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2501 Starts
        #region sendVNOCAdminEmail
        /// <summary>
        /// sendVNOCAdminEmail
        /// </summary>
        /// <returns></returns>
        private bool sendVNOCAdminEmail()
        {
            try
            {
                if (orgInfo == null) //FB 2632
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);


                int userPrefLang = 1 ;
                int i = 0;//FB 2670
                int userEmailLang = 1;
                string confcodepin = "";

                if (conf.conftype == 2)
                    confcodepin = FetchAudioDialString(conf.confid, conf.instanceid, "M", 0);

                if (conf.StartMode == 0) //FB 2522
                    callLaunch = "Yes";
                emailObject.CallLaunch = callLaunch;

                emailObject.AudioInfo = confcodepin;
                emailObject.CustomOptions = addConfCustomAttr("VNOCOperator");

                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                    emailObject.ConciergeSupport = ConciergeSupport();

                vrmUser user = null;
                vrmEmail theEmail = null;
                
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                if (conf.instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionList.Add(Expression.Eq("instanceid", 1));

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionList);//FB 2636
	
				ConfVNOCList = m_ConfVNOCOperatorDAO.GetByCriteria(criterionList); //FB 2670
                //FB 2693 Starts
                if (conf.isPCconference > 0)
                {
                    List<ICriterion> criterionPCList = new List<ICriterion>();
                    criterionPCList.Add(Expression.Eq("userid", conf.owner));
                    criterionPCList.Add(Expression.Eq("PCId", conf.pcVendorId));
                    confPCLst = m_IUserPCDAO.GetByCriteria(criterionPCList);
                }
                if (confPCLst != null && confPCLst.Count > 0)
                    emailObject.PCDetails = BuildPCContent(confPCLst[0]);
                //FB 2693 Ends


                if (ConfVNOCList.Count > 0)
                {
                    //emailObject.MCUInfo = "";
                    //for (int m = 0; m < conf.mcuList.Count; m++)
                    //{
                    //    if (m > 0)
                    //        emailObject.MCUInfo += "<br />";

                    //    if (organizationID != 11 && conf.mcuList[m].isPublic.ToString() == "1")
                    //        conf.mcuList[m].BridgeName = conf.mcuList[m].BridgeName + "(*)";

                    //    emailObject.MCUInfo += conf.mcuList[m].BridgeName;
                    //}
                    for (i = 0; i < ConfVNOCList.Count; i++)
                    {
                        user = m_IuserDao.GetByUserId(ConfVNOCList[i].vnocId);

                        userEmailLang = user.EmailLangId;
                        userPrefLang = user.Language;
                        //userDateFormat = ((user.DateFormat.Trim() == "") ? "MM/dd/yyyy" : user.DateFormat.Trim());
                        SetEmailDateFormat(user.DateFormat.Trim(), ref userDateFormat); //FB 2555
                        userTimeFormat = ((user.TimeFormat.Trim() == "0") ? "HH:mm" : "hh:mm tt");
                        tzDisplay = ((user.Timezonedisplay.Trim() == "") ? "0" : user.Timezonedisplay.Trim());
                        usrTimezone = user.TimeZone;

                        emailObject.Recipient = user.FirstName + " " + user.LastName;

                        SetEmailLanguage(ref userPrefLang, ref userEmailLang);
                        FetchEmailString(emailLanguage, conf.ConfMode, eEmailType.VNOC_Operator);
                        SetConfDateTime(eEmailType.VNOC_Operator);
                        BuildEmailBody();


                        theEmail = new vrmEmail();
                        theEmail.emailTo = user.Email;
                        theEmail.emailFrom = confHost.Email;
                        theEmail.Message = emailContent;
                        theEmail.Subject = emailSubject;
                        theEmail.orgID = conf.orgId;

                        theEmail.UUID = getEmailUUID();
                        m_IemailDao.Save(theEmail);
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Cannot save sendVNOCAdminEmail email", e);
                throw e;
            }
        }
        #endregion

        //FB 2501 Ends

        //FB 2550 Starts
        #region  SendPublicParty
        /// <summary>
        /// SendPublicParty
        /// </summary>
        /// <param name="confList"></param>
        /// <returns></returns>
        public bool SendPublicParty(List<vrmConference> confList, int partyID)
        {
            try
            {
                int imageid = 0;
                emailObject = new sConferenceEmail();
                conf = confList[0];

                if (conf == null)
                    return false;

                SetWebsiteURL();

                confHost = m_IuserDao.GetByUserId(conf.owner);
                if (conf.owner == conf.userid)
                    confRequstor = confHost;
                else
                    confRequstor = m_IuserDao.GetByUserId(conf.userid);

                SetHostLangFolder(confHost.Language);

                organizationID = conf.orgId;
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                emailObject.TechContact = sysTech.name;
                emailObject.TechEmail = sysTech.email;
                emailObject.TechPhone = sysTech.phone;

                emailObject.MailLogo = AttachEmailLogo(organizationID); 
                
                List<ICriterion> criterionListVMR = new List<ICriterion>();
                criterionListVMR.Add(Expression.Eq("confid", conf.confid));

                if (conf.instanceid > 0)
                    criterionListVMR.Add(Expression.Eq("instanceid", conf.instanceid));
                else
                    criterionListVMR.Add(Expression.Eq("instanceid", 1));

                confAVParam = m_confAdvAvParamsDAO.GetByCriteria(criterionListVMR);//FB 2636

                SetConfRoomString();
                SetConfGuestRoomString(); 
                SetRecurrencePatern();

                if ((conf.description.Length < 1) || (conf.description == "(none)") || (conf.description == "none"))
                    conf.description = "N/A";

                emailObject.Host = confHost.FirstName.Trim() + " " + confHost.LastName.Trim();
                emailObject.Requestor = confRequstor.FirstName.Trim() + " " + confRequstor.LastName.Trim();
                emailObject.Description = conf.description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                emailObject.ConferenceName = conf.externalname;
                emailObject.UniqueId = conf.confnumname.ToString();


                if (orgInfo.EnableCncigSupport > 0) //FB 2632
                    emailObject.ConciergeSupport = ConciergeSupport();

                Password = conf.password;
                if (conf.password.Trim() == "")
                    Password = "N/A";
                emailObject.Password = Password;

                emailObject.ExternalBridge = "N/A";
                emailObject.InternalBridge = "N/A";
                //if (conf.isVMR == 1)
                if (conf.isVMR > 0) //FB 2620 //FB 2957
                {
                    if (!string.IsNullOrEmpty(confAVParam[0].externalBridge))
                    {
                        emailObject.ExternalBridge = confAVParam[0].externalBridge;
                    }

                    if (!string.IsNullOrEmpty(confAVParam[0].internalBridge))
                    {
                        emailObject.InternalBridge = confAVParam[0].internalBridge;
                    }
                }
                //Fetch Participants list
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                criterionList.Add(Expression.Eq("userid", partyID));
                List<vrmConfUser> uList = m_vrmConfUserDAO.GetByCriteria(criterionList);

                vrmConfUser us;
                StringBuilder eBody = new StringBuilder();
                for (int i = 0; i < uList.Count; i++)
                {
                    us = uList[i]; //FB 1830
                    if (us.partyNotify == 1) //FB 1830
                    {
                        emailObject.CustomOptions = addConfCustomAttr("Party"); //FB 2381

                        if (us.invitee == 2 || us.invitee ==4 ) // Room Attendee or VMR Room Attendee
                        {
                            if (us.roomId > 0 && us.roomId != vrmConfType.Phantom)
                            {
                                vrmRoom Loc = m_roomDAO.GetById(us.roomId);
                                imageid = Loc.SecurityImage1Id;
                                eBody.Append("Connection details:<br /><span style=\"font-size: 10pt; font-family: Verdana\">");
                                eBody.Append("You have chosen to attend the conference ");
                                eBody.Append("from the conference room : " + Loc.Name.Replace("||", "\"").Replace("!!", "'")); // FB 1888
                                eBody.Append("</span><BR>");
                            }
                        }
                        else if (us.invitee == 1) //invited
                        {

                            eBody.Append("Connection details:<br /><table width=\"400\" style=\"font-size: 10pt; font-family: Verdana; \" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">");

                            string pro, ctype;
                            if (us.defVideoProtocol == 1)
                                pro = "IP";
                            else
                            {
                                pro = "ISDN";
                            }
                            if (us.connectionType == dialingOption.dialOut)
                                ctype = "Dial-out";
                            else if (us.connectionType == dialingOption.dialIn)
                                ctype = "Dial-in";
                            else if (us.connectionType == dialingOption.direct)
                                ctype = "Direct";
                            else
                                ctype = "N/A";

                            eBody.Append("<tr>");
                            eBody.Append("<td align=\"right\" >");
                            eBody.Append("Video protocol: </td>");
                            eBody.Append("<td >");
                            eBody.Append("<strong>" + pro + "</strong></td>");
                            eBody.Append("</tr>");

                            eBody.Append("<tr>");
                            eBody.Append("<td align=\"right\" >");
                            eBody.Append("Your IP/ISDN address: </td>");
                            eBody.Append("<td>");
                            eBody.Append("<strong>" + us.ipisdnaddress + "</strong></td>");
                            eBody.Append("</tr>");

                            eBody.Append("<tr>");
                            eBody.Append("<td align=\"right\">");
                            eBody.Append("Connection type: </td>");
                            eBody.Append("<td>");
                            eBody.Append("<strong>" + ctype + "</strong></td>");
                            eBody.Append("</tr>");

                            if (us.connectionType == 1) // Dial in
                            {
                                eBody.Append("<tr>");
                                eBody.Append("<td align=\"right\">");
                                eBody.Append("Bridge IP/ISDN address: </td>");
                                eBody.Append("<td >");
                                eBody.Append("<strong>" + us.bridgeIPISDNAddress + "</strong></td>");
                                eBody.Append("</tr>");
                                List<vrmAddressType> addressType = vrmGen.getAddressType();
                                vrmAddressType at;
                                for (int a = 0; a < addressType.Count; a++)
                                {
                                    at = addressType[a];
                                    if (at.Id == us.bridgeAddressType)
                                    {
                                        eBody.Append("<tr>");
                                        eBody.Append("<td align=\"right\">");
                                        eBody.Append("Address Type: </td>");
                                        eBody.Append("<td >");
                                        eBody.Append("<strong>" + at.name + "</strong></td>");
                                        eBody.Append("</tr>");
                                        break;
                                    }
                                }
                            }
                            eBody.Append("</table>");
                        }
                        
                        emailObject.ConnectionDetails = eBody.ToString();

                        if (!sendParticipantEmail(us.userid, us.invitee, eEmailType.Public_Participant))
                            m_log.Error("Cannot send Public participant email [userid]" + us.userid.ToString());
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SendPublicParty", e);
                throw e;
            }
        }
        #endregion
        //FB 2550 Ends

        //FB 2555 Starts
        #region SetEmailDateFormat
        /// <summary>
        /// SetEmailDateFormat
        /// </summary>
        /// <param name="UserFormat"></param>
        /// <param name="EmailDateFormat"></param>
        /// <returns></returns>
        private void SetEmailDateFormat(string userFormat, ref string EmailDateFormat)
        {
            try
            {
                EmailDateFormat = userFormat;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                {
                    if (orgInfo.EmailDateFormat == (int)eEmailDateFormat.EuropeanDateFormat)
                        EmailDateFormat = "dd MMM yyyy";
                }

            }
            catch (Exception e)
            {
                m_log.Error("SetEmailDateFormat", e);
            }
        }
        #endregion
        //FB 2555 Ends

        //FB 2588 Starts
        #region SetEmailTimeFormat
        /// <summary>
        /// SetEmailTimeFormat
        /// </summary>
        /// <param name="userTimeFormat"></param>
        /// <param name="EmailTimeFormat"></param>
        private void SetEmailTimeFormat(string userTimeFormat, ref string EmailTimeFormat)
        {
            try
            {
                EmailTimeFormat = ((userTimeFormat == "0") ? "HH:mm" : "hh:mm tt");

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                {
                    if (orgInfo.EnableZulu == 1)
                        EmailTimeFormat = "HHmmZ";
                }

            }
            catch (Exception e)
            {
                m_log.Error("SetEmailTimeFormat", e);
            }
        }
        #endregion
        //FB 2588 Ends

        //FB 2632 Starts
        #region Concierge Support

        private string ConciergeSupport()
        {
            try
            {
                int confType;
                confType = conf.conftype;
                string COnfCongSupport = "No", cngSupport = "", cngSupportAtt = "";
                if (confType != 8)//FB 3007
                {
                    cngSupport = "<td align=\"right\" valign=\"top\"> Conference Support:</td><td>"; //FB 3023
                    if (orgInfo == null)
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (orgInfo.EnableOnsiteAV != 0)//FB 2670
                    {
                        if (orgInfo.AVOnsiteinEmail > 0)
                        {
                            if (conf.OnSiteAVSupport > 0)
                                COnfCongSupport = "Yes";
                            cngSupportAtt = "<strong>On-Site A/V Support: </strong>" + COnfCongSupport + "<br /><br />";

                        }
                    }

                    if (orgInfo.EnableMeetandGreet != 0)//FB 2670
                    {
                        COnfCongSupport = "No";
                        if (orgInfo.MeetandGreetinEmail > 0)
                        {
                            if (conf.MeetandGreet > 0)
                                COnfCongSupport = "Yes";
                            cngSupportAtt += "<strong>Meet and Greet: </strong>" + COnfCongSupport + "<br /><br />";
                        }
                    }

                    if (orgInfo.EnableConciergeMonitoring != 0)//FB 2670
                    {
                        COnfCongSupport = "No";
                        if (orgInfo.CncigMoniteringinEmail > 0)
                        {
                            if (conf.ConciergeMonitoring > 0)
                                COnfCongSupport = "Yes";
                            cngSupportAtt += "<strong>Call Monitoring: </strong>" + COnfCongSupport + "<br /><br />";//FB 3023
                        }
                    }

                    if (orgInfo.EnableDedicatedVNOC != 0)//FB 2670
                    {
                        COnfCongSupport = "No";
                        if (orgInfo.VNOCinEmail > 0)
                        {
                            if (conf.DedicatedVNOCOperator > 0)
                                COnfCongSupport = "Yes";
                            cngSupportAtt += "<strong>Dedicated VNOC Operator: </strong>" + COnfCongSupport + "<br /><br />";
                        }
                    }

                    if (cngSupportAtt != "")
                    {
                        cngSupport += cngSupportAtt;
                    }
                    else
                    {
                        cngSupport += "<b>N/A</b></td>";
                    }
                }
                return cngSupport;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        #endregion
        //FB 2632 Ends
        //FB 2410 Starts
        #region SendBatchreport
        internal bool SendBatchreport(String toAdd, int organizationID, ref String filePath, String type, string startDate, String endDate, String ReportName)
        {
            try
            {
                MailMessage app = new MailMessage();
                SetWebsiteURL();
                if (sysTech == null)
                    sysTech = m_ISysTechDAO.GetTechByOrgId(organizationID);

                if (type == "U")
                    FetchEmailString(1, 0, eEmailType.UserReportMail);
                else if (type == "E")
                    FetchEmailString(1, 0, eEmailType.ErrorEventReport);
                else
                    FetchEmailString(1, 0, eEmailType.BatchReport); //FB 2410


                vrmEmail theEmail = new vrmEmail();
                theEmail.emailTo = toAdd;
                theEmail.emailFrom = sysTech.email;
                theEmail.Subject = emailSubject.Replace("{0}", type); //FB 2410
                emailContent = emailContent.Replace("{1}", AttachEmailLogo(organizationID));
                emailContent = emailContent.Replace("{37}", sysTech.name);
                emailContent = emailContent.Replace("{38}", sysTech.email);
                emailContent = emailContent.Replace("{39}", sysTech.phone);
                emailContent = emailContent.Replace("{74}", startDate);
                emailContent = emailContent.Replace("{75}", endDate);
                emailContent = emailContent.Replace("{76}", ReportName);
                theEmail.Message = emailContent;

                theEmail.UUID = getEmailUUID();
                theEmail.Attachment = filePath;
                theEmail.orgID = 11;
                m_IemailDao.Save(theEmail);

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion
        //FB 2410 Ends
		//FB 2693 Starts
        private string BuildPCContent(vrmUserPC ConfPCDeatils)
        {
            try
            {
                string PC = "";
                StringBuilder PCContent = null;
                if (ConfPCDeatils != null)
                {
                    switch (ConfPCDeatils.PCId)
                    {

                        case (int)eEmailPCType.BlueJeans:
                            PCContent = new StringBuilder();
                            PCContent.Append("<table><tr><td>");
                            PCContent.Append("---------------------------------------------------------------------------------------<br />");
                            PCContent.Append("To join or start the meeting,go to:<br /><a href=" + ConfPCDeatils.Description + ">" + ConfPCDeatils.Description + "</a><br /><br />Or join directly with the following options:<br />");
                            PCContent.Append("<br /></td></tr><tr><td>Skype:<br /><a href=" + ConfPCDeatils.SkypeURL + ">" + ConfPCDeatils.SkypeURL + "</a><br /><br /></td></tr><tr><td>Video Conferencing Systems:</td></tr><tr><td>");
                            PCContent.Append("Dial-in IP: " + ConfPCDeatils.VCDialinIP + "<br />Meeting ID: " + ConfPCDeatils.VCMeetingID + "<br /><br /></td></tr><tr><td>Phone Conferencing:<br />");
                            PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "<br />Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "<br />Meeting ID: " + ConfPCDeatils.PCMeetingID + "<br /><br />");
                            PCContent.Append("</td></tr><tr><td>***********************************************************<br />");
                            PCContent.Append("<br />First time joining a Blue Jeans Video Meeting?<br />For detailed instructions: <a href=" + ConfPCDeatils.Intructions + ">" + ConfPCDeatils.Intructions + "</a><br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("Test your video connection,talk to Jean our video tester by clicking here<br /><br /><br /><a href="+ConfPCDeatils.Intructions + ">" + ConfPCDeatils.Intructions + "</a><br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("(c) Blue Jeans Network 2012");
                            PCContent.Append("</td></tr></table>");
                            break;
                        case (int)eEmailPCType.Jabber:
                            PCContent = new StringBuilder();
                            PCContent.Append("<table><tr><td>");
                            PCContent.Append("---------------------------------------------------------------------------------------<br />");
                            PCContent.Append("To join or start the meeting,go to:<br /><a href=" + ConfPCDeatils.Description + ">" + ConfPCDeatils.Description + "</a><br /><br />Or join directly with the following options:<br />");
                            PCContent.Append("<br /></td></tr><tr><td>Skype:<br /><a href=" + ConfPCDeatils.SkypeURL + ">" + ConfPCDeatils.SkypeURL + "</a><br /><br /></td></tr><tr><td>Video Conferencing Systems:</td></tr><tr><td>");
                            PCContent.Append("Dial-in IP: " + ConfPCDeatils.VCDialinIP + "<br />Meeting ID: " + ConfPCDeatils.VCMeetingID + "<br /><br /></td></tr><tr><td>Phone Conferencing:<br />");
                            PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "<br />Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "<br />Meeting ID: " + ConfPCDeatils.PCMeetingID + "<br /><br />");
                            PCContent.Append("</td></tr><tr><td>***********************************************************<br />");
                            PCContent.Append("<br />First time joining a Jabber Video Meeting?<br />For detailed instructions: <a href=" + ConfPCDeatils.Intructions + ">" + ConfPCDeatils.Intructions + "</a><br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("Test your video connection,talk to our video tester by clicking here<br /><br /><br /><a href="+ConfPCDeatils.Intructions + ">" + ConfPCDeatils.Intructions + "</a><br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("(c) Jabber Network 2012");
                            PCContent.Append("</td></tr></table>");
                            break;
                        case (int)eEmailPCType.Lync:
                            PCContent = new StringBuilder();
                            PCContent.Append("<table><tr><td>");
                            PCContent.Append("---------------------------------------------------------------------------------------<br />");
                            PCContent.Append("To join or start the meeting,go to:<br /><a href=" + ConfPCDeatils.Description + ">" + ConfPCDeatils.Description + "</a><br /><br />Or join directly with the following options:<br />");
                            PCContent.Append("<br /></td></tr><tr><td>Skype:<br /><a href=" + ConfPCDeatils.SkypeURL + ">" + ConfPCDeatils.SkypeURL + "</a><br /><br /></td></tr><tr><td>Video Conferencing Systems:</td></tr><tr><td>");
                            PCContent.Append("Dial-in IP: " + ConfPCDeatils.VCDialinIP + "<br />Meeting ID: " + ConfPCDeatils.VCMeetingID + "<br /><br /></td></tr><tr><td>Phone Conferencing:<br />");
                            PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "<br />Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "<br />Meeting ID: " + ConfPCDeatils.PCMeetingID + "<br /><br />");
                            PCContent.Append("</td></tr><tr><td>***********************************************************<br />");
                            PCContent.Append("<br />First time joining a Lync Video Meeting?<br />For detailed instructions: <a href=" + ConfPCDeatils.Intructions + ">" + ConfPCDeatils.Intructions + "</a><br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("Test your video connection,talk to our video tester by clicking here<br /><br /><br /><a href="+ConfPCDeatils.Intructions + ">" + ConfPCDeatils.Intructions + "</a><br /><br />");
                            PCContent.Append("***********************************************************<br /><br />");
                            PCContent.Append("(c) Lync Network 2012");
                            PCContent.Append("</td></tr></table>");
                            break;
                        case (int)eEmailPCType.Vidtel:
                            PCContent = new StringBuilder();
                            PCContent.Append("<table><tr><td>");
                            PCContent.Append("---------------------------------------------------------------------------------------<br />");
                            PCContent.Append("To join or start the meeting,go to:<br /><a href=" + ConfPCDeatils.Description + ">" + ConfPCDeatils.Description + "</a><br /><br />Or join directly with the following options:<br />");
                            PCContent.Append("<br /></td></tr><tr><td>Skype:<br /><a href=" + ConfPCDeatils.SkypeURL + ">" + ConfPCDeatils.SkypeURL + "</a><br /><br /></td></tr><tr><td>Video Conferencing Systems:</td></tr><tr><td>");
                            PCContent.Append(" Dial-in SIP:" + ConfPCDeatils.VCDialinSIP + "<br />Dial-in H.323: " + ConfPCDeatils.VCDialinH323 + "<br />PIN: " + ConfPCDeatils.VCPin + "<br /><br /></td></tr><tr><td>Phone Conferencing:<br />");
                            PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "<br />Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "<br />PIN: " + ConfPCDeatils.PCPin + "<br /><br />");
                            PCContent.Append("</td></tr><tr><td>");//***********************************************************<br />");
                            //PCContent.Append("Test your video connection,talk to our video tester by clicking here<br /><br /><br /><a href="+ConfPCDeatils.Intructions + ">" + ConfPCDeatils.Intructions + "</a><br /><br />");
                            //PCContent.Append("***********************************************************<br />");
                            PCContent.Append("<br />(c) Vidtel Network 2012");
                            PCContent.Append("</td></tr></table>");
                            break;
                        default:

                            break;
                    }

                    PC = PCContent.ToString();
                }
                return PC;
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }
        //FB 2693 Ends

		//FB 2659 Starts
        private void FetchDesktopURLandDialNumber()
        {
            List<vrmConfBridge> vConfbridgeList = null;
            List<ICriterion> ConfBridgeList = null;
            string BridgeEXTNo = "";
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmMCUOrgSpecificDetails> orgSpecificdetails = null;//FB 2659-TDB
            IMCUOrgSpecificDetailsDao m_IMCUOrgSpecificDetailsDao = null;
            try
            {
                ConfBridgeList = new List<ICriterion>();
                ConfBridgeList.Add(Expression.Eq("ConfID", conf.confid));
                if (conf.instanceid > 0)
                    ConfBridgeList.Add(Expression.Eq("InstanceID", conf.instanceid));
                else
                    ConfBridgeList.Add(Expression.Eq("InstanceID", 1));
                vConfbridgeList = m_IconfBridge.GetByCriteria(ConfBridgeList);

                if (vConfbridgeList.Count > 0)
                {
                    BridgeEXTNo = "";
                    if (vConfbridgeList[0].E164Dialnumber != "")
                        BridgeEXTNo = vConfbridgeList[0].E164Dialnumber.ToString();

                    emailObject.ConferenceDialinNumber = BridgeEXTNo.ToString();

                    m_IMCUOrgSpecificDetailsDao = m_Hardware.GetMCUOrgSpecificDetailsDao();//FB 2659-TDB
                    {

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeID", vConfbridgeList[0].BridgeID));
                        criterionList.Add(Expression.Eq("orgID", organizationID));
                        criterionList.Add(Expression.Eq("propertyName", "ScopiaDesktopURL"));
                        
                        orgSpecificdetails = m_IMCUOrgSpecificDetailsDao.GetByCriteria(criterionList, true);

                        if (orgSpecificdetails.Count > 0)
                        {
                            for (int cntDtls = 0; cntDtls < orgSpecificdetails.Count; cntDtls++)
                                if (orgSpecificdetails[cntDtls].propertyName.Trim() == "ScopiaDesktopURL")
                                    emailObject.DesktopLink = orgSpecificdetails[cntDtls].propertyValue.Trim();
                        }
                    }
                }

                sDektopURL = emailObject.DesktopLink;
                sE164DialNumber = emailObject.ConferenceDialinNumber;

            }
            catch (Exception)
            {
             
            }
        }
        //FB 2659 End

        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

    }
}


