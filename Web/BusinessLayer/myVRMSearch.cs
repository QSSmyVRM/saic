//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using NHibernate;
using NHibernate.Criterion;

using log4net; 
using myVRM.DataLayer;
using System.IO; //FB 2639 
using System.Xml.XPath; //FB 2639



namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Summary description for myVRMSearch
    /// </summary>
    /// 

    // refrence for detached query
    //http://www.ayende.com/Blog/archive/7055.aspx
    //

    //FB 2501 starts
    #region StartMode
    public enum StartMode { Automatic = 0, Manual = 1 };
    #endregion
    //FB 2501 ends
    public class myVRMSearch
    {
        #region Private Members

        private static log4net.ILog m_log;
        private string m_configPath;

        private LocationDAO m_locDAO;
        private userDAO m_userDAO;
        private deptDAO m_deptDAO;
        private conferenceDAO m_confDAO;
        private hardwareDAO m_Hardware;
        private GeneralDAO m_generalDAO;
        
        private IInactiveUserDao m_IInactiveuserDAO; //FB 1644
        private IUserDao m_IuserDAO;
        private IDeptDao m_IdeptDAO;
        private IMCUDao m_vrmMCU;
        private IEptDao m_vrmEpt;
        private IUserDeptDao m_IuserDeptDAO;
        private IT3RoomDAO m_IT3DAO;
        private IT2RoomDAO m_IT2DAO;
        private IRoomDAO m_IRoomDAO;
        private ILocApprovDAO m_ILocApprovDAO;
        private ILocDeptDAO m_IlocDeptDAO;
        private IConferenceDAO m_IconfDAO;
        private IConfRoomDAO m_IconfRoom;
        private IConfUserDAO m_IconfUser;
        private ICountryDAO m_ICountryDAO;
        private IStateDAO m_IStateDAO;
        private IOrgDAO m_IOrgDAO;//FB 2274

        private SystemDAO m_systemDAO; // fb 1048
        private ISystemDAO m_IsystemDAO; //FB 1048
        private IConfRecurDAO m_vrmConfRecurDAO;//FB 1391

        private imageDAO m_imageDAO; //Image Project
        private IImageDAO m_IImageDAO;  //Image Project
        private imageFactory vrmImg = null; //FB 2136
        private UtilFactory m_utilFactory; //FB 2236

        // max records per page
        private int m_iMaxRecords;
        private vrmDataObject m_obj;
        private int m_level;

        private List<int> confsList; //FB 1158
        private bool isApprovalPending = false;  //FB 1158
        private bool isCustomAttrAvailable = false; //Custom Attribute Fixes

        private IConfAttrDAO m_IconfAttrDAO;  //Custom Attribute Fix
        private ISysMailDAO m_ISysMailDAO; //Rss Feed

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private ISysApproverDAO m_ISysApproverDAO;
        internal OrgData orgInfo;
        private const int defaultOrgId = 11;  //Default organization
        internal int organizationID = 0;
        internal int multiDepts = 1;
        private WorkOrderDAO m_woDAO;//Code added fro Room search
        private InvListDAO m_InvListDAO;//Code added fro Room search
        private InvCategoryDAO m_InvCategoryDAO;//Code added fro Room search
        private int openDept; //FB 1672 
        //FB 2027 - Starts
        private ITempUserDAO m_TempUserDAO;
        private IUserRolesDao m_IUserRolesDao;
        private IGuestUserDao m_IGuestUserDao;
        private ITemplateDAO m_vrmTempDAO;
        //FB 2027 - End
        private vrmFactory m_vrmFactor;//FB 2027

        //FB 2136
        private ISecBadgeDao m_ISecBadgeDao;
        private secBadgeDao m_SecBadgeDao;
        private IConfMessageDao m_IConfMessageDAO;//FB 2486
        private IMCUParamsDao m_IMcuParamDAO;//FB 2501 call monitoring
		private IESPublicRoomDAO m_IESPublicRoomDAO; //FB 2392-WhyGO
		private IConfCascadeDAO m_IconfCascade;//FB 2560
        private IConfBridgeDAO m_IconfBridge; //FB 2616
        // FB 2639 Start  
        XmlWriter xWriter = null;
        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        XmlWriterSettings xSettings = null;
        // FB 2639 End
		private IConfVNOCOperatorDAO m_ConfVNOCOperatorDAO; //FB 2670
        bool isDeleted = false; //FB 1942 TIK# 100037
        #endregion

        #region vrmSearchType

        private class vrmSearchType
        {
            public const int Past = 0;
            public const int Ongoing = 1;
            public const int Today = 2;
            public const int ThisWeek = 3;
            public const int ThisMonth = 4;
            public const int Cutom = 5;
            public const int Yesterday = 6;
            public const int Tomorrow = 7;
            public const int Future = 8;
        }
        #endregion

        //FB 2595 starts
        #region NetworkState
        private class NetworkState 
        { 
           public const int UnSecured = 0;
           public const int Secured = 1;
        }
        #endregion
        //FB 2595 ends

        #region vrmPublicType
        private class vrmPublicType
        {
            public const int Private = 0;
            public const int Public = 1;
            public const int Both = 2;
        }
        #endregion

        #region vrmSortBy
        private class vrmSortBy
        {
            public const int UniqueID = 1;
            public const int ConfName = 2;
            public const int ConfDate = 3;
            public const int StartMode = 4;//FB 2501
            public const int SiloName = 5;//FB 2822
        }
        #endregion

        //FB 2607
        #region CustomOptions 
        private class vrmCustomOptions
        {
            public const int CheckBox = 2;
            public const int RadioButton = 3;
            public const int TextBox = 4;
            public const int ListBox = 5;
            public const int DropDownList = 6;
            public const int URLTextBox = 7;
            public const int RadioButtonList = 8;
            public const int MultiLine = 10;
        }
        #endregion

        //private ITemplateDAO m_ItempDAO;

        #region Constructor
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        /// 
        public myVRMSearch(vrmDataObject obj)
        {
            try
            {
                vrmImg = new imageFactory(ref obj); //FB 2136
                m_vrmFactor = new vrmFactory(ref obj);

                init(obj.ConfigPath, obj.log);
                m_utilFactory = new UtilFactory(ref obj);//FB 2236

                //FB 2136
                m_SecBadgeDao = new secBadgeDao(m_configPath, m_log);
                m_ISecBadgeDao = m_SecBadgeDao.GetSecImageDao();
                m_IconfCascade = m_confDAO.GetConfCascadeDao();//FB 2560
				m_IconfBridge = new confBridgeDAO(obj.ConfigPath); //FB 2616
				m_ConfVNOCOperatorDAO = m_confDAO.GetConfVNOCOperatorDao(); //FB 2670
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        public myVRMSearch(string ConfigPath, log4net.ILog log)
        {
            try
            {
                init(ConfigPath, log);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Page Init
        private bool init(string ConfigPath, log4net.ILog log)
        {
            try
            {
                m_configPath = ConfigPath;
                m_log = log;

                m_userDAO = new userDAO(m_configPath, log);
                m_deptDAO = new deptDAO(m_configPath, log);
                m_locDAO = new LocationDAO(m_configPath, log);
                m_confDAO = new conferenceDAO(m_configPath, log);
                m_Hardware = new hardwareDAO(m_configPath, log);
                m_generalDAO = new GeneralDAO(m_configPath, log);
                m_systemDAO = new SystemDAO(m_configPath, m_log); //FB 1048
                m_IsystemDAO = m_systemDAO.GetSystemDao(); //FB 1048
                m_OrgDAO = new orgDAO(m_configPath, m_log); //Organization Module Fixes

                m_vrmConfRecurDAO = m_confDAO.GetConfRecurDao();//FB 1391

                m_IdeptDAO = m_deptDAO.GetDeptDao();
                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao();
                m_IuserDAO = m_userDAO.GetUserDao();
                m_IInactiveuserDAO = m_userDAO.GetInactiveUserDao(); //FB 1644
                m_IlocDeptDAO = m_locDAO.GetLocDeptDAO();// Got Removed in Previous Check-in
                m_IT3DAO = m_locDAO.GetT3RoomDAO();
                m_IT2DAO = m_locDAO.GetT2RoomDAO();
                m_IRoomDAO = m_locDAO.GetRoomDAO();
                m_ILocApprovDAO = m_locDAO.GetLocApprovDAO();
                m_ICountryDAO = m_generalDAO.GetCountryDAO();
                m_IStateDAO = m_generalDAO.GetStateDAO();
                m_IconfRoom = m_confDAO.GetConfRoomDao();
                m_IconfUser = m_confDAO.GetConfUserDao();
                m_IconfDAO = m_confDAO.GetConferenceDao();
                m_vrmMCU = m_Hardware.GetMCUDao();
                m_vrmEpt = m_Hardware.GetEptDao();
                m_IconfAttrDAO = m_confDAO.GetConfAttrDao();//Custom Attribute Fix
                m_ISysMailDAO = m_systemDAO.GetSysMailDao();//Rss Feed
                m_IOrgDAO = m_OrgDAO.GetOrgDao(); //FB 2274

                m_woDAO = new WorkOrderDAO(m_configPath, log);//Code added for room search
                m_InvListDAO = m_woDAO.GetInvListDAO();//Code added for room search
                m_InvCategoryDAO = m_woDAO.GetCategoryDAO();//Code added for room search

                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();   //Organization Module Fixes
                m_ISysApproverDAO = m_OrgDAO.GetSysApproverDao();

                m_imageDAO = new imageDAO(m_configPath, log); //Image Project
                m_IImageDAO = m_imageDAO.GetImageDao();

                m_log = log;
                m_iMaxRecords = 20;
                //FB 2027 - Starts
                m_TempUserDAO = m_confDAO.GetTempUserDAO();
                m_IGuestUserDao = m_userDAO.GetGuestUserDao();
                m_IUserRolesDao = m_userDAO.GetUserRolesDao();
                m_vrmTempDAO = m_confDAO.GetTemplateDao();
                //FB 2027 - End
                m_IConfMessageDAO = m_confDAO.GetConfMessageDao();//FB 2486
                m_IMcuParamDAO = m_Hardware.GetMCUParamsDao();//FB 2501 call Monitoring
				m_IESPublicRoomDAO = m_locDAO.GetESPublicRoomDAO(); //FB 2392-WhyGO
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Room Commands

        #region GetRoomProfile
        /// <summary>
        /// <GetRoomProfile>
        ///   <UserID></UserID>
        ///   <RoomID></RoomID>
        /// </GetRoomProfile>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomProfile(ref vrmDataObject obj)
        {
            vrmRoom locRoom = null;
            vrmUser userInfo = null;
            vrmInactiveUser inActiveUserInfo = null;  //FB 1644
            vrmImg = new imageFactory(ref obj); //FB 2136
            StringBuilder publicFields = null; //FB 2392
            ESPublicRoom PublicRoomField = null;
            String stateName = "", city = "";
            
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                StringBuilder OutXML = new StringBuilder(); //FB 2607 String Builder
                XmlNode node;

                node = xd.SelectSingleNode("//GetRoomProfile/UserID");
                string userID = node.InnerXml.Trim();

                int roomID =0;
                node = xd.SelectSingleNode("//GetRoomProfile/RoomID");
                 int.TryParse(node.InnerXml.Trim(),out roomID);

                locRoom = m_IRoomDAO.GetByRoomId(roomID);
                if (locRoom == null)
                    throw new Exception("No data matching to this roomID " + roomID);

                PublicRoomField = m_IESPublicRoomDAO.GetBymyVRMRoomId(locRoom.roomId);//FB 2392

                OutXML.Append("<GetRoomProfile>");
                OutXML.Append("<RoomID>" + locRoom.roomId.ToString() + "</RoomID>");
                OutXML.Append("<RoomName>" + locRoom.Name + "</RoomName>");
                OutXML.Append("<RoomQueue>" + locRoom.RoomQueue + "</RoomQueue>"); //FB 2342
                OutXML.Append("<RoomUID>" + locRoom.RoomUID + "</RoomUID>"); //ZD 100196
                OutXML.Append("<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>");
                OutXML.Append("<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>");
                OutXML.Append("<MaximumConcurrentPhoneCalls>" + locRoom.MaxPhoneCall.ToString() + "</MaximumConcurrentPhoneCalls>");
                OutXML.Append("<SetupTime>" + locRoom.SetupTime.ToString() + "</SetupTime>");
                OutXML.Append("<TeardownTime>" + locRoom.TeardownTime.ToString() + "</TeardownTime>");
                OutXML.Append("<AssistantInchargeID>" + locRoom.assistant.ToString() + "</AssistantInchargeID>");
                userInfo = m_IuserDAO.GetByUserId(locRoom.assistant);
                
                //FB 1644
                if (userInfo != null)
                    OutXML.Append("<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>");
                else
                {
                    inActiveUserInfo = m_IInactiveuserDAO.GetByUserId(locRoom.assistant);
                    if (inActiveUserInfo != null)
                        OutXML.Append("<AssistantInchargeName>" + inActiveUserInfo.FirstName + " " + inActiveUserInfo.LastName + "</AssistantInchargeName>");
                    OutXML.Append("<UserStaus>I</UserStaus>");
                }

                OutXML.Append("<MultipleAssistantEmails>" + locRoom.notifyemails + "</MultipleAssistantEmails>");
                OutXML.Append("<Tier1ID>" + locRoom.tier2.tier3.ID.ToString() + "</Tier1ID>");
                OutXML.Append("<Tier1Name>" + locRoom.tier2.tier3.Name + "</Tier1Name>");
                OutXML.Append("<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>");
                OutXML.Append("<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>");
                OutXML.Append("<CatererFacility>" + locRoom.Caterer.ToString() + "</CatererFacility>");
                OutXML.Append("<DynamicRoomLayout>" + locRoom.DynamicRoomLayout.ToString() + "</DynamicRoomLayout>");
                OutXML.Append("<Projector>" + locRoom.ProjectorAvailable.ToString() + "</Projector>");
                OutXML.Append("<isTelepresence>" + locRoom.isTelepresence.ToString() + "</isTelepresence>");//FB 2170
                OutXML.Append("<ServiceType>" + locRoom.ServiceType.ToString() + "</ServiceType>");//FB 2219
                OutXML.Append("<DedicatedVideo>" + locRoom.DedicatedVideo + "</DedicatedVideo>");//FB 2334
                OutXML.Append("<DedicatedCodec>" + locRoom.DedicatedCodec + "</DedicatedCodec>");//FB 2390
                OutXML.Append("<AVOnsiteSupportEmail>" + locRoom.AVOnsiteSupportEmail + "</AVOnsiteSupportEmail>");//FB 2415
                OutXML.Append("<Video>" + locRoom.VideoAvailable.ToString() + "</Video>");
                OutXML.Append("<Floor>" + locRoom.RoomFloor + "</Floor>");
                OutXML.Append("<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>");
                OutXML.Append("<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>");
                OutXML.Append("<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>");
                OutXML.Append("<City>" + locRoom.City + "</City>");
                OutXML.Append("<State>" + locRoom.State.ToString() + "</State>");
                //FB 2426 Start
                OutXML.Append("<Extroom>" + locRoom.Extroom.ToString() + "</Extroom>");
                OutXML.Append("<LoginUserId>" + locRoom.adminId + "</LoginUserId>");
                //FB 2426 End
                OutXML.Append("<IsVMR>" + locRoom.IsVMR + "</IsVMR>"); //FB 2448 start
                OutXML.Append("<InternalNumber>" + locRoom.InternalNumber + "</InternalNumber>");
                OutXML.Append("<ExternalNumber>" + locRoom.ExternalNumber + "</ExternalNumber>"); //FB 2448 end
                OutXML.Append("<VMRLink>" + locRoom.VMRLink + "</VMRLink>"); //FB 2727

                if (locRoom.State > 0)
                {
                    vrmState objState = m_IStateDAO.GetById(locRoom.State);
                    OutXML.Append("<StateName>" + objState.StateCode.ToString() + "</StateName>");
                }
                else
                {
                    stateName = ""; //FB 2392
                    if (PublicRoomField != null)
                        stateName = PublicRoomField.StateName;

                    OutXML.Append("<StateName>" + stateName + "</StateName>");
                }
                OutXML.Append("<ZipCode>" + locRoom.Zipcode + "</ZipCode>");
                OutXML.Append("<Country>" + locRoom.Country.ToString() + "</Country>");
                if (locRoom.Country > 0)
                {
                    vrmCountry objCountry = m_ICountryDAO.GetById(locRoom.Country);
                    OutXML.Append("<CountryName>" + objCountry.CountryName + "</CountryName>");
                }
                else
                {
                    OutXML.Append("<CountryName></CountryName>");
                }
                OutXML.Append("<MapLink>" + locRoom.Maplink + "</MapLink>");
                OutXML.Append("<ParkingDirections>" + locRoom.ParkingDirections + "</ParkingDirections>");
                OutXML.Append("<AdditionalComments>" + locRoom.AdditionalComments + "</AdditionalComments>");
                OutXML.Append("<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>");
                if (locRoom.TimezoneID > 0)
                {
                    timeZoneData tz = new timeZoneData();
                    timeZone.GetTimeZone(locRoom.TimezoneID, ref tz);
                    OutXML.Append("<TimezoneName>" + tz.TimeZone + "</TimezoneName>");
                }
                else
                {
                    OutXML.Append("<TimezoneName></TimezoneName>");
                }
                OutXML.Append("<Longitude>" + locRoom.Longitude + "</Longitude>");
                OutXML.Append("<Latitude>" + locRoom.Latitude + "</Latitude>");

                //FB 2543 Starts
                string stmt2 = "";
                stmt2 = "select approverid from myVRM.DataLayer.vrmLocApprover where roomid = " + locRoom.roomId.ToString();
                IList appr = m_IconfRoom.execQuery(stmt2);

                if (appr.Count > 0)
                    OutXML.Append("<Approval>1</Approval>");
                else
                    OutXML.Append("<Approval>0</Approval>"); 
                //FB 2543 Ends

                OutXML.Append("<Approvers>");
                int i = 1;
                foreach (vrmLocApprover locRoomApprov in locRoom.locationApprover)
                {
                    userInfo = m_IuserDAO.GetByUserId(locRoomApprov.approverid);
                    OutXML.Append("<Approver" + i.ToString() + "ID>" + locRoomApprov.approverid.ToString() + "</Approver" + i.ToString() + "ID>");
                    OutXML.Append("<Approver" + i.ToString() + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "  </Approver" + i.ToString() + "Name>");
                    i++;
                }
                if (i == 1)
                {
                    OutXML.Append("<Approver1ID></Approver1ID>");
                    OutXML.Append("<Approver1Name></Approver1Name>");
                    OutXML.Append("<Approver2ID></Approver2ID>");
                    OutXML.Append("<Approver2Name></Approver2Name>");
                    OutXML.Append("<Approver3ID></Approver3ID>");
                    OutXML.Append("<Approver3Name></Approver3Name>");
                }
                else if (i == 2)
                {
                    OutXML.Append("<Approver2ID></Approver2ID>");
                    OutXML.Append("<Approver2Name></Approver2Name>");
                    OutXML.Append("<Approver3ID></Approver3ID>");
                    OutXML.Append("<Approver3Name></Approver3Name>");
                }
                else if (i == 3)
                {
                    OutXML.Append("<Approver3ID></Approver3ID>");
                    OutXML.Append("<Approver3Name></Approver3Name>");
                }
                OutXML.Append("</Approvers>");
                OutXML.Append("<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>");
                List<vrmEndPoint> eptList = new List<vrmEndPoint>();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("endpointid", locRoom.endpointid));
                criterionList.Add(Expression.Eq("isDefault", 1));//FB 2602
                criterionList.Add(Expression.Eq("deleted", 0));
                eptList = m_vrmEpt.GetByCriteria(criterionList);
                if (eptList.Count > 0)
                {
                    foreach (vrmEndPoint ept in eptList)
                    {
                        OutXML.Append("<EndpointName>" + ept.name + "</EndpointName>");
                        OutXML.Append("<EndpointIP>" + ept.address + "</EndpointIP>");
                        OutXML.Append("<isTelePresEndPoint>" + ept.isTelePresence + "</isTelePresEndPoint>"); //FB 2400 start
                        OutXML.Append("<MultiCodec>");
                        if (ept.MultiCodecAddress != null)
                        {
                            String[] multiCodec = ept.MultiCodecAddress.Split('Ö');
                            for (int a = 0; a < multiCodec.Length; a++)
                            {
                                if (multiCodec[a].Trim() != "")
                                    OutXML.Append("<Address>" + multiCodec[a].Trim() + "</Address>");

                            }
                        }
                        OutXML.Append("</MultiCodec>");
                        break;
                    }
                }
                else
                {
                    OutXML.Append("<EndpointName></EndpointName>");
                    OutXML.Append("<EndpointIP></EndpointIP>");
                    OutXML.Append("<MultiCodec></MultiCodec>");
                    OutXML.Append("<isTelePresEndPoint>0</isTelePresEndPoint>"); //FB 2400 end
                }
                /*
                OutXML.Append("<RoomImage>" + locRoom.RoomImage + "</RoomImage>");

                OutXML.Append("<Images>");
                OutXML.Append("<Map1>" + locRoom.MapImage1 + "</Map1>");
                OutXML.Append("<Map2>" + locRoom.MapImage2 + "</Map2>");
                OutXML.Append("<Security1>" + locRoom.SecurityImage1 + "</Security1>");
                OutXML.Append("<Security2>" + locRoom.SecurityImage2 + "</Security2>");
                OutXML.Append("<Misc1>" + locRoom.MiscImage1 + "</Misc1>");
                OutXML.Append("<Misc2>" + locRoom.MiscImage2 + "</Misc2>");
                OutXML.Append("</Images>");
                */

                //Image Project codelines start...

                OutXML.Append("<RoomImages>");

                string roomImagesids = locRoom.RoomImageId;
                string roomImagesnames = locRoom.RoomImage;

                vrmImage imObj = null;
                string imgDt = "";
                string imagename = "";
                int imageid = 0;
                if (roomImagesids != null && roomImagesnames != null)
                {
                    if (roomImagesids != "" && roomImagesnames != "")
                    {
                        imgDt = "";
                        imObj = null;
                        string[] idArr = roomImagesids.Split(',');
                        string[] nameArr = roomImagesnames.Split(',');
                        string fileext = "";
                        if (idArr.Length > 0 && nameArr.Length > 0)
                        {
                            for (int lp = 0, k = 0; lp < idArr.Length; lp++, k++)
                            {
                                imagename = "";
                                fileext = "jpg";

                                //if(k < nameArr.Length)
                                imagename = nameArr[k].ToString();

                                if (imagename != "")
                                    fileext = imagename.Substring(imagename.LastIndexOf(".") + 1);

                                Int32.TryParse(idArr[lp].ToString(), out imageid);

                                imObj = m_IImageDAO.GetById(imageid);
                                if (imObj == null)
                                    continue;

                                imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                                OutXML.Append("<ImageDetails>");
                                OutXML.Append("<ImageName>" + imagename + "</ImageName>");
                                OutXML.Append("<Imagetype>" + fileext + "</Imagetype>");
                                OutXML.Append("<Image>" + imgDt + "</Image>");
                                OutXML.Append("</ImageDetails>");
                            }
                        }
                    }
                }
                OutXML.Append("</RoomImages>");

                OutXML.Append("<Images>");

                OutXML.Append("<Map1>" + locRoom.MapImage1 + "</Map1>");
                OutXML.Append("<Map1Image>" + GetRoomImage(locRoom.MapImage1Id) + "</Map1Image>");

                OutXML.Append("<Map2>" + locRoom.MapImage2 + "</Map2>");
                OutXML.Append("<Map2Image>" + GetRoomImage(locRoom.MapImage2Id) + "</Map2Image>");

                //FB 2136 start
                OutXML.Append("<Security1>" + locRoom.SecurityImage1 + "</Security1>");
                OutXML.Append("<Security1ImageId>" + locRoom.SecurityImage1Id + "</Security1ImageId>");
                OutXML.Append("<Security1Image>" + GetSecImage(locRoom.SecurityImage1Id) + "</Security1Image>"); 
                //OutXML.Append("<Security1ImageId>" + locRoom.SecurityImage1Id + "</Security1ImageId>");

                //OutXML.Append("<Security2>" + locRoom.SecurityImage2 + "</Security2>");
                //OutXML.Append("<Security2Image>" + GetRoomImage(locRoom.SecurityImage2Id) + "</Security2Image>");
                //FB 2136 end

                OutXML.Append("<Misc1>" + locRoom.MiscImage1 + "</Misc1>");
                OutXML.Append("<Misc1Image>" + GetRoomImage(locRoom.MiscImage1Id) + "</Misc1Image>");

                OutXML.Append("<Misc2>" + locRoom.MiscImage2 + "</Misc2>");
                OutXML.Append("<Misc2Image>" + GetRoomImage(locRoom.MiscImage2Id) + "</Misc2Image>");

                OutXML.Append("</Images>");

                //Image Project codelines end...

                OutXML.Append("<Custom1>" + locRoom.Custom1 + "</Custom1>");
                OutXML.Append("<Custom2>" + locRoom.Custom2 + "</Custom2>");
                OutXML.Append("<Custom3>" + locRoom.Custom3 + "</Custom3>");
                OutXML.Append("<Custom4>" + locRoom.Custom4 + "</Custom4>");
                OutXML.Append("<Custom5>" + locRoom.Custom5 + "</Custom5>");
                OutXML.Append("<Custom6>" + locRoom.Custom6 + "</Custom6>");
                OutXML.Append("<Custom7>" + locRoom.Custom7 + "</Custom7>");
                OutXML.Append("<Custom8>" + locRoom.Custom8 + "</Custom8>");
                OutXML.Append("<Custom9>" + locRoom.Custom9 + "</Custom9>");
                OutXML.Append("<Custom10>" + locRoom.Custom10 + "</Custom10>");

                OutXML.Append("<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>");
                OutXML.Append("<isVIP>" + locRoom.isVIP.ToString() + "</isVIP>");//FB w
                OutXML.Append("<RoomCategory>" + locRoom.RoomCategory + "</RoomCategory>"); //FB 2694

                OutXML.Append("<LastModifiedDate>" + locRoom.Lastmodifieddate.ToString() + "</LastModifiedDate>");//Code added for last modified date
                OutXML.Append("<LastModififeduserID>" + locRoom.adminId.ToString() + "</LastModififeduserID>");//Code added for last modified user
                userInfo = m_IuserDAO.GetByUserId(locRoom.adminId);
                if(userInfo != null)//FB 1954
                    OutXML.Append("<LastModififeduserName>" + userInfo.FirstName + " " + userInfo.LastName + "</LastModififeduserName>");//Code added for last modified user
                else//FB 1954
                    OutXML.Append("<LastModififeduserName></LastModififeduserName>");


                OutXML.Append("<Departments>");
                foreach (vrmLocDepartment locDept in locRoom.locationDept)
                {
                    vrmDept objDept = m_IdeptDAO.GetById(locDept.departmentId);
                    OutXML.Append("<Department>");
                    OutXML.Append("<ID>" + locDept.departmentId.ToString() + "</ID>");
                    OutXML.Append("<Name>" + objDept.departmentName + "</Name>");
                    OutXML.Append("<SecurityKey>" + objDept.securityKey + "</SecurityKey>");
                    OutXML.Append("</Department>");
                }
                OutXML.Append("</Departments>");
                //FB 2392 start -WhyGo
                OutXML.Append("<DefaultEquipmentID>" + locRoom.DefaultEquipmentid.ToString() + "</DefaultEquipmentID>");
                OutXML.Append("<isPublic>" + locRoom.isPublic + "</isPublic>");
                OutXML.Append("<PublicFields>");

                int Speed = 384;
                
                if (PublicRoomField != null)
                {
                    publicFields = new StringBuilder();
                    if (eptList.Count > 0)
                        Speed = eptList[0].linerateid;

                     publicFields.Append("<WhygoRoomID>" + PublicRoomField.WhygoRoomId + "</WhygoRoomID>");
                     publicFields.Append("<Address>" + PublicRoomField.address + "</Address>");
                     publicFields.Append("<State>" + PublicRoomField.StateName + "</State>");
                     publicFields.Append("<Country>" + PublicRoomField.Country + "</Country>");
                     publicFields.Append( "<AUXEquipment>" + PublicRoomField.AUXEquipment + "</AUXEquipment>");
                     publicFields.Append( "<Speed>" + Speed + "</Speed>");
                     publicFields.Append( "<Type>" + PublicRoomField.Type + "</Type>");
                     publicFields.Append( "<Description>" + PublicRoomField.RoomDescription + "</Description>");
                     publicFields.Append( "<ExtraNotes>" + PublicRoomField.ExtraNotes + "</ExtraNotes>");
                     publicFields.Append( "<GenericSellPrice>" + PublicRoomField.genericSellPrice + "</GenericSellPrice>");
                     publicFields.Append( "<GeoCodeAddress>" + PublicRoomField.geoCodeAddress + "</GeoCodeAddress>");
                     publicFields.Append( "<InternetBiller>" + PublicRoomField.internetBiller + "</InternetBiller>");
                     publicFields.Append( "<InternetPriceCurrency>" + PublicRoomField.internetPriceCurrency + "</InternetPriceCurrency>");
                     publicFields.Append( "<IsAutomatic>" + PublicRoomField.isAutomatic + "</IsAutomatic>");
                     publicFields.Append( "<IsHDCapable>" + PublicRoomField.isHDCapable + "</IsHDCapable>");
                     publicFields.Append( "<IsInternetCapable>" + PublicRoomField.isInternetCapable + "</IsInternetCapable>");
                     publicFields.Append( "<IsInternetFree>" + PublicRoomField.isInternetFree + "</IsInternetFree>");
                     publicFields.Append("<IsISDNCapable>" + PublicRoomField.isISDNCapable + "</IsISDNCapable>");
                     publicFields.Append("<ImgLink>" + PublicRoomField.MapLink + "</ImgLink>");
                     publicFields.Append("<IsIPCapable>" + PublicRoomField.isIPCapable + "</IsIPCapable>");
                     publicFields.Append( "<IsIPCConnectionCapable>" + PublicRoomField.isIPCConnectionCapable + "</IsIPCConnectionCapable>");
                     publicFields.Append( "<IsIPDedicated>" + PublicRoomField.isIPDedicated + "</IsIPDedicated>");
                     publicFields.Append( "<IsTP>" + PublicRoomField.isTP + "</IsTP>");
                     publicFields.Append( "<isVCCapable>" + PublicRoomField.isVCCapable + "</isVCCapable>");
                     publicFields.Append("<CurrencyType>" + PublicRoomField.CurrencyType + "</CurrencyType>");
                     publicFields.Append( "<IsEarlyHoursEnabled>" + PublicRoomField.IsEarlyHoursEnabled + "</IsEarlyHoursEnabled>");
                     publicFields.Append( "<EarlyHoursStart>" + PublicRoomField.EHStartTime + "</EarlyHoursStart>");
                     publicFields.Append( "<EarlyHoursEnd>" + PublicRoomField.EHEndTime + "</EarlyHoursEnd>");
                     publicFields.Append( "<EarlyHoursCost>" + PublicRoomField.EHCost + "</EarlyHoursCost>");
                     publicFields.Append( "<IsEarlyHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsEarlyHoursFullyAuto>"); //FB 2543
                     publicFields.Append( "<OpenHour>" + PublicRoomField.openHours + "</OpenHour>");
                     publicFields.Append( "<OfficeHoursStart>" + PublicRoomField.OHStartTime + "</OfficeHoursStart>");
                     publicFields.Append( "<OfficeHoursEnd>" + PublicRoomField.OHEndTime + "</OfficeHoursEnd>");
                     publicFields.Append( "<OfficeHoursCost>" + PublicRoomField.OHCost + "</OfficeHoursCost>");
                     publicFields.Append( "<IsAfterHourEnabled>" + PublicRoomField.IsAfterHourEnabled + "</IsAfterHourEnabled>");
                     publicFields.Append( "<AfterHoursStart>" + PublicRoomField.AHStartTime + "</AfterHoursStart>");
                     publicFields.Append( "<AfterHoursEnd>" + PublicRoomField.AHEndTime + "</AfterHoursEnd>");
                     publicFields.Append( "<AfterHoursCost>" + PublicRoomField.AHCost + "</AfterHoursCost>");
                     publicFields.Append( "<IsAfterHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsAfterHoursFullyAuto>"); //FB 2543
                     publicFields.Append( "<IsCrazyHoursSupported>" + PublicRoomField.isCrazyHoursSupported + "</IsCrazyHoursSupported>");
                     publicFields.Append( "<CrazyHoursStart>" + PublicRoomField.CHtartTime + "</CrazyHoursStart>");
                     publicFields.Append( "<CrazyHoursEnd>" + PublicRoomField.CHEndTime + "</CrazyHoursEnd>");
                     publicFields.Append( "<CrazyHoursCost>" + PublicRoomField.CHCost + "</CrazyHoursCost>");
                     publicFields.Append("<IsCrazyHoursFullyAuto>" + PublicRoomField.CHFullyAuto + "</IsCrazyHoursFullyAuto>"); //FB 2543
                     publicFields.Append( "<Is24HoursEnabled>" + PublicRoomField.Is24HoursEnabled + "</Is24HoursEnabled>");
                     publicFields.Append("<CateringOptions>" + PublicRoomField.CateringOptionsAvailable + "</CateringOptions>");
                     publicFields.Append("<AUXEquipment>" + PublicRoomField.AUXEquipment + "</AUXEquipment>");
                     publicFields.Append("<Layout>" + PublicRoomField.Layout + "</Layout>");
                     publicFields.Append("<DefaultEquipment>" + PublicRoomField.DefaultEquipment + "</DefaultEquipment>");
                     publicFields.Append("<SiteCordinator>");
                     publicFields.Append("<Name>" + PublicRoomField.SitecordinatorName + "</Name>");
                     publicFields.Append("<Email>" + PublicRoomField.SitecordinatorEmail + "</Email>");
                     publicFields.Append("<Phone>" + PublicRoomField.SitecordinatorPhone + "</Phone>");
                     publicFields.Append("</SiteCordinator>");
                     publicFields.Append("<Manager>");
                     publicFields.Append("<Name>" + PublicRoomField.ManagerName + "</Name>");
                     publicFields.Append("<Email>" + PublicRoomField.ManagerEmail + "</Email>");
                     publicFields.Append("<Phone>" + PublicRoomField.ManagerPhone + "</Phone>");
                     publicFields.Append("</Manager>");
                     publicFields.Append("<TechnicalContact>");
                     publicFields.Append("<Name>" + PublicRoomField.TechnicalContactName + "</Name>");
                     publicFields.Append("<Email>" + PublicRoomField.TechnicalContactEmail + "</Email>");
                     publicFields.Append("<Phone>" + PublicRoomField.TechnicalContactPhone + "</Phone>");
                     publicFields.Append("</TechnicalContact>");
                     publicFields.Append("<IP>");
                     publicFields.Append("<Speed>" + PublicRoomField.IPSpeed + "</Speed>");
                     publicFields.Append("<Address>" + PublicRoomField.IPAddress + "</Address>");
                     publicFields.Append("</IP>");
                     publicFields.Append("<ISDN>");
                     publicFields.Append("<Speed>" + PublicRoomField.ISDNSpeed + "</Speed>");
                     publicFields.Append("<Address>" + PublicRoomField.ISDNNumber + "</Address>");
                     publicFields.Append("</ISDN>");
                     publicFields.Append("<PublicRoomLastModified>" + PublicRoomField.PublicRoomLastModified.ToString() + "</PublicRoomLastModified>");
                     OutXML.Append(publicFields.ToString());

                }
                OutXML.Append("</PublicFields>");
                //FB 2392 end-WhyGo
                OutXML.Append("<RoomIconTypeId>" + locRoom.RoomIconTypeId + "</RoomIconTypeId>"); //FB 2065
                OutXML.Append("</GetRoomProfile>");

                obj.outXml = OutXML.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region SetRoomProfile
        /// <summary>
        /// SetRoomProfile
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetRoomProfile(ref vrmDataObject obj)
        {
            List<ICriterion> criterionLst = null; //FB 1143 Checking DuPlicate Room Name
            vrmRoom locRoom = null;
            vrmRoom locRoomUpd = null;
            List<vrmTier2> t2RoomList = null;
            int rmLimit = 0;
            int isPublic = 0;//FB 2392
            vrmUser userInfo = null;//Code added for last modified
            int imageId = 0;
            string roomimageids = "";
            bool disabledEdit = false;//COde added for 1600
            int isTelepresence = 0;//FB 2170
            string roomemail = "", roomUID = "";//FB 2342 //ZD 100196
            string RmName = ""; bool checkRmName = false;//FB 2616
            try
            {
                locRoom = new vrmRoom();

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                m_obj = obj;//FB 1171
                XmlNode node;

                node = xd.SelectSingleNode("//SetRoomProfile/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetRoomProfile/RoomID");
                string roomID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetRoomProfile/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                string video = "0";
                node = xd.SelectSingleNode("//SetRoomProfile/Video");
                video = node.InnerXml.Trim();

                int isVMR = 0;//FB 2586
                node = xd.SelectSingleNode("//SetRoomProfile/IsVMR");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out isVMR);
                locRoom.IsVMR = isVMR;

                //FB 2694 Starts
                int RoomCategory = 0;
                node = xd.SelectSingleNode("//SetRoomProfile/RoomCategory");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out RoomCategory);
                locRoom.RoomCategory = RoomCategory;
                //FB 2694 Ends

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (!roomID.ToLower().Contains("new"))
                {
                    
                    locRoomUpd = m_IRoomDAO.GetByRoomId(Int32.Parse(roomID));
                    locRoom.RoomID = locRoomUpd.RoomID;
                    locRoom.disabled = locRoomUpd.disabled;//Code added for 1600
                    RmName = locRoomUpd.Name;//FB 2616
                    if (locRoomUpd.disabled == 1)
                        disabledEdit = true;

                    isPublic = locRoomUpd.isPublic;//FB 2392
                    locRoom.DefaultEquipmentid = locRoomUpd.DefaultEquipmentid;//FB 2392
					//FB 2717 Starts
                    if (locRoomUpd.RoomCategory == 5)
                    {
                        locRoom = locRoomUpd;
                    }
					//FB 2717 End
                }
                criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("orgId", organizationID));

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                string errMess = "";
                
                //FB 2594 Starts
                int PublicRoom = 0;
                node = xd.SelectSingleNode("//SetRoomProfile/isPublic");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out PublicRoom);
                //FB 2594 Ends
                int currVideoAvail = 0;
                Int32.TryParse(video, out currVideoAvail);
                int errID = 200;//FB 1881
                criterionLst.Add(Expression.Eq("Disabled", 0));
                criterionLst.Add(Expression.Eq("Extroom", 0));//FB 2426
                criterionLst.Add(Expression.Eq("isPublic", 0));//FB 2594
                if (video == "2" && locRoom.IsVMR == 0 && RoomCategory != 4) //Code Changed For FB 1744 //FB 2594 //FB 2586 //FB 2694
                {
                    criterionLst.Add(Expression.Eq("VideoAvailable", 2)); //Code Changed For FB 1744
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomCategory", 4))); //FB 2694
                    criterionLst.Add(Expression.Eq("IsVMR", 0));//FB 2694
                    rmLimit = orgdt.MaxVideoRooms;
                    errMess = "Video rooms limit exceeded";
                    errID = 455;//FB 1881
                }
                else if (locRoom.IsVMR == 1 && RoomCategory != 4) //FB 2586 Start //FB 2694
                {
                    criterionLst.Add(Expression.Eq("VideoAvailable", 2));
                    criterionLst.Add(Expression.Eq("IsVMR", 1));
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomCategory", 4))); //FB 2694
                    rmLimit = orgdt.MaxVMRRooms;
                    errMess = "VMR rooms limit exceeded";
                    errID = 656;
                } //FB 2586 End
                //FB 2694 Starts
                else if (RoomCategory == 4 && (video == "1" || video == "0"))
                {
                    criterionLst.Add(Expression.Lt("VideoAvailable", 2));
                    criterionLst.Add(Expression.Eq("RoomCategory", 4));
                    criterionLst.Add(Expression.Eq("IsVMR", 0));//FB 2694
                    rmLimit = orgdt.MaxROHotdesking;
                    errMess = "RO Hotdesking rooms limit exceeds VRM license.";
                    errID = 694;
                }
                else if (RoomCategory == 4 && video == "2")
                {
                    criterionLst.Add(Expression.Eq("VideoAvailable", 2));
                    criterionLst.Add(Expression.Eq("RoomCategory", 4));
                    criterionLst.Add(Expression.Eq("IsVMR", 0));//FB 2694
                    rmLimit = orgdt.MaxVCHotdesking;
                    errMess = "VC Hotdesking rooms limit exceeds VRM license.";
                    errID = 693;
                }
                //FB 2694 Ends
                else if((video == "0" || video == "1") && locRoom.IsVMR == 0 && RoomCategory != 4)
                {
                    criterionLst.Add(Expression.Lt("VideoAvailable", 2)); //Code Changed For FB 1744
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomCategory", 4)));
                    criterionLst.Add(Expression.Eq("IsVMR", 0));//FB 2694
                    rmLimit = orgdt.MaxNonVideoRooms;
                    errMess = "Non-Video rooms limit exceeded";
                    errID = 456;//FB 1881
                }

                if (locRoomUpd != null)
                {
                    if (locRoomUpd.VideoAvailable == currVideoAvail)
                    {
                        rmLimit += 1;
                    }
                    else if (locRoomUpd.VideoAvailable < 2 && currVideoAvail < 2)//Code Changed For FB 1744
                    {
                        rmLimit += 1;
                    }
                    else if (locRoomUpd.VideoAvailable == currVideoAvail && locRoom.IsVMR == 1)//FB 2586
                    {
                        rmLimit += 1;
                    }
                }

                if (!disabledEdit)
                {

                    List<vrmRoom> checkRoomCount = m_IRoomDAO.GetByCriteria(criterionLst);
                    if (checkRoomCount.Count >= rmLimit)
                    {
                        m_log.Error(errMess);
                        //FB 1881 start
                        //obj.outXml = myVRMException.toXml(errMess);
                        myVRMException myvrmEx = new myVRMException(errID);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }
                }
                

                locRoom.adminId = Int32.Parse(userID);
                locRoom.ResponseTime = Int32.Parse("60");

                node = xd.SelectSingleNode("//SetRoomProfile/RoomName");
                string roomName = node.InnerXml.Trim();
                locRoom.Name = roomName;
                //FB 1143 Checking DuPlicate Room Name - Starts
                criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("Name", roomName));
                criterionLst.Add(Expression.Eq("orgId", organizationID));
                criterionLst.Add(Expression.Eq("Extroom", 0));//FB 2426
                if (!roomID.ToLower().Contains("new"))
                {
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomID", Int32.Parse(roomID))));
                }
                List<vrmRoom> roomChkList = m_IRoomDAO.GetByCriteria(criterionLst);
                if (roomChkList.Count > 0)
                {
                    myVRMException myVRMEx = new myVRMException(256);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                //FB 1143 Checking DuPlicate Room Name - Ends
                
                //FB 2616 Start
                if (RmName == roomName)
                    checkRmName = true;
                //FB 2616 End

                // FB 2342 starts
                node = xd.SelectSingleNode("//SetRoomProfile/RoomQueue"); 
                if(node != null)
                    roomemail = node.InnerXml.Trim();

                locRoom.RoomQueue = roomemail;
                if (roomemail != "")
                {
                    criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("RoomQueue", roomemail));

                    if (!roomID.ToLower().Contains("new"))
                    {
                        criterionLst.Add(Expression.Not(Expression.Eq("RoomID", Int32.Parse(roomID))));
                    }
                    List<vrmRoom> roomChkList1 = m_IRoomDAO.GetByCriteria(criterionLst);
                    if (roomChkList1.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(265);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }
                // FB 2342 end

                //ZD 100196 starts
                node = xd.SelectSingleNode("//SetRoomProfile/RoomUID");
                if (node != null)
                    roomUID = node.InnerXml.Trim();

                locRoom.RoomUID = roomUID;
                if (roomUID != "")
                {
                    criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("RoomUID", roomUID));

                    if (!roomID.ToLower().Contains("new"))
                    {
                        criterionLst.Add(Expression.Not(Expression.Eq("RoomID", Int32.Parse(roomID))));
                    }
                    List<vrmRoom> roomChkList1 = m_IRoomDAO.GetByCriteria(criterionLst);
                    if (roomChkList1.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(265);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100196 end
                

                locRoom.orgId = organizationID;

                node = xd.SelectSingleNode("//SetRoomProfile/RoomPhoneNumber");
                string roomPhoneNumber = node.InnerXml.Trim();
                locRoom.RoomPhone = roomPhoneNumber;

                string handi = "0";
                node = xd.SelectSingleNode("//SetRoomProfile/Handicappedaccess");
                handi = node.InnerXml.Trim();
                locRoom.HandiCappedAccess = Int32.Parse(handi);

                string isVIP = "0";//FB w
                node = xd.SelectSingleNode("//SetRoomProfile/isVIP");
                if (node != null)
                {
                    if (node.InnerText != "")
                        isVIP = node.InnerText;
                }

                locRoom.isVIP = Int32.Parse(isVIP);

                //FB 2170
                node = xd.SelectSingleNode("//SetRoomProfile/isTelepresence");
                if (node != null)
                {
                    if (node.InnerText != "")
                        if (!Int32.TryParse(node.InnerText, out isTelepresence))
                            isTelepresence = 0;

                }
                locRoom.isTelepresence = isTelepresence;

                int ServiceType = -1;
                node = xd.SelectSingleNode("//SetRoomProfile/ServiceType");//FB 2219
                if (node != null)
                    if (node.InnerText.Trim() != "")
                        Int32.TryParse(node.InnerText, out ServiceType);
                locRoom.ServiceType = ServiceType;

                node = xd.SelectSingleNode("//SetRoomProfile/DedicatedVideo");//FB 2334
                string DedicatedVideo = node.InnerXml.Trim();
                locRoom.DedicatedVideo = DedicatedVideo;

                node = xd.SelectSingleNode("//SetRoomProfile/DedicatedCodec");//FB 2390
                string DedicatedCodec = node.InnerXml.Trim();
                locRoom.DedicatedCodec = DedicatedCodec;

                node = xd.SelectSingleNode("//SetRoomProfile/AVOnsiteSupportEmail");//FB 2415
                string AVOnsiteSupportEmail = node.InnerXml.Trim();
                locRoom.AVOnsiteSupportEmail = AVOnsiteSupportEmail;

                node = xd.SelectSingleNode("//SetRoomProfile/MaximumCapacity");
                string maxCapacity = node.InnerXml.Trim();
                locRoom.Capacity = Int32.Parse(maxCapacity);

                node = xd.SelectSingleNode("//SetRoomProfile/MaximumConcurrentPhoneCalls");
                string maxCCPC = node.InnerXml.Trim();
                locRoom.MaxPhoneCall = Int32.Parse(maxCCPC);

                node = xd.SelectSingleNode("//SetRoomProfile/SetupTime");
                string setupTime = node.InnerXml.Trim();
                locRoom.SetupTime = Int32.Parse(setupTime);

                node = xd.SelectSingleNode("//SetRoomProfile/TeardownTime");
                string teardownTime = node.InnerXml.Trim();
                locRoom.TeardownTime = Int32.Parse(teardownTime);

                node = xd.SelectSingleNode("//SetRoomProfile/AssistantInchargeID");
                string assistantInchargeID = node.InnerXml.Trim();
                //FB 2539 Start
                int adminId = Int32.Parse(assistantInchargeID);
                locRoom.assistant = adminId; //Commented - FB 2539
                //vrmUser user = m_IuserDAO.GetByUserId(adminId);
                //if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                //else
                //{
                //    myVRMException myVRMEx = new myVRMException(629);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}
                //FB 2539 End
                node = xd.SelectSingleNode("//SetRoomProfile/MultipleAssistantEmails");
                string multipleAssistantEmails = node.InnerXml.Trim();
                locRoom.notifyemails = multipleAssistantEmails;

                node = xd.SelectSingleNode("//SetRoomProfile/Tier1ID");
                string tier1ID = node.InnerXml.Trim();
                locRoom.L3LocationId = Int32.Parse(tier1ID);

                node = xd.SelectSingleNode("//SetRoomProfile/Tier2ID");
                string tier2ID = node.InnerXml.Trim();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("ID", Int32.Parse(tier2ID)));
                criterionList.Add(Expression.Eq("disabled", 0));
                t2RoomList = m_IT2DAO.GetByCriteria(criterionList);
                if (t2RoomList.Count > 0)
                    locRoom.tier2 = (vrmTier2)t2RoomList[0];

                node = xd.SelectSingleNode("//SetRoomProfile/Floor");
                string floor = node.InnerXml.Trim();
                locRoom.RoomFloor = floor;

                node = xd.SelectSingleNode("//SetRoomProfile/RoomNumber");
                string roomNumber = node.InnerXml.Trim();
                locRoom.RoomNumber = roomNumber;

                node = xd.SelectSingleNode("//SetRoomProfile/StreetAddress1");
                string streetAddress1 = node.InnerXml.Trim();
                locRoom.Address1 = streetAddress1;

                node = xd.SelectSingleNode("//SetRoomProfile/StreetAddress2");
                string StreetAddress2 = node.InnerXml.Trim();
                locRoom.Address2 = StreetAddress2;

                node = xd.SelectSingleNode("//SetRoomProfile/City");
                string city = node.InnerXml.Trim();
                locRoom.City = city;

                node = xd.SelectSingleNode("//SetRoomProfile/State");
                string state = node.InnerXml.Trim();
                if (state.Trim() != "") // FB 1423
                    locRoom.State = Int32.Parse(state);

                node = xd.SelectSingleNode("//SetRoomProfile/ZipCode");
                string zipCode = node.InnerXml.Trim();
                locRoom.Zipcode = zipCode;

                node = xd.SelectSingleNode("//SetRoomProfile/Country");
                string country = node.InnerXml.Trim();
                if (country.Trim() != "") // FB 1423
                    locRoom.Country = Int32.Parse(country);

                node = xd.SelectSingleNode("//SetRoomProfile/MapLink");
                string mapLink = node.InnerXml.Trim();
                locRoom.Maplink = mapLink;

                node = xd.SelectSingleNode("//SetRoomProfile/ParkingDirections");
                string parkingDirections = node.InnerXml.Trim();
                locRoom.ParkingDirections = parkingDirections;

                node = xd.SelectSingleNode("//SetRoomProfile/AdditionalComments");
                string additionalComments = node.InnerXml.Trim();
                locRoom.AdditionalComments = additionalComments;

                node = xd.SelectSingleNode("//SetRoomProfile/TimezoneID");
                string timezoneID = node.InnerXml.Trim();
                locRoom.TimezoneID = Int32.Parse(timezoneID);

                node = xd.SelectSingleNode("//SetRoomProfile/Longitude");
                string longitude = node.InnerXml.Trim();
                locRoom.Longitude = longitude;

                node = xd.SelectSingleNode("//SetRoomProfile/Latitude");
                string latitude = node.InnerXml.Trim();
                locRoom.Latitude = latitude;

                node = xd.SelectSingleNode("//SetRoomProfile/CatererFacility");
                string catererFacility = node.InnerXml.Trim();
                locRoom.Caterer = catererFacility;

                node = xd.SelectSingleNode("//SetRoomProfile/DynamicRoomLayout");
                string dynamicRoomLayout = node.InnerXml.Trim();
                locRoom.DynamicRoomLayout = dynamicRoomLayout;

                node = xd.SelectSingleNode("//SetRoomProfile/Projector");
                string projector = node.InnerXml.Trim();
                locRoom.ProjectorAvailable = Int32.Parse(projector);

                locRoom.VideoAvailable = Int32.Parse(video);

                //FB 2717 Starts
                if (locRoomUpd != null && locRoomUpd.RoomCategory == 5 && locRoomUpd.IsVMR == 0)
                {
                    locRoom.InternalNumber = locRoomUpd.InternalNumber;
                    locRoom.ExternalNumber = locRoomUpd.ExternalNumber;
                }
                else
                {
                    locRoom.InternalNumber = "";
                    node = xd.SelectSingleNode("//SetRoomProfile/InternalNumber");
                    if (node != null)
                        locRoom.InternalNumber = node.InnerXml.Trim();

                    locRoom.ExternalNumber = "";
                    node = xd.SelectSingleNode("//SetRoomProfile/ExternalNumber");
                    if (node != null)
                        locRoom.ExternalNumber = node.InnerXml.Trim();
                    //FB 2448 end
                }
                //FB 2717 End
 				//FB 2727
                locRoom.VMRLink = "";
                node = xd.SelectSingleNode("//SetRoomProfile/VMRLink");
                if (node != null)
                    locRoom.VMRLink = node.InnerXml.Trim();

                int RoomIconTypeId = 0; //FB 2065                
                node = xd.SelectSingleNode("//SetRoomProfile/RoomIconTypeId");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out RoomIconTypeId);
                locRoom.RoomIconTypeId = RoomIconTypeId;
                    

                // Image Project Code Lines starts...

                XmlNodeList rmnodes = xd.SelectNodes("//SetRoomProfile/RoomImages/Image");
                if (rmnodes != null)
                {
                    string actualImage = "";
                    roomimageids = "";
                    foreach (XmlNode nd in rmnodes)
                    {
                        actualImage = nd.SelectSingleNode("ActualImage").InnerText.Trim();
                        if (actualImage != "")
                        {
                            imageId = SetRoomImage(actualImage, 1);
                            if (imageId > 0)
                            {
                                if (roomimageids == "")
                                    roomimageids = imageId.ToString();
                                else
                                    roomimageids += "," + imageId.ToString();
                            }
                            actualImage = "";
                            imageId = 0;
                        }
                    }
                }

                node = xd.SelectSingleNode("//SetRoomProfile/RoomImageName");

                //ZD 100263 Starts
                string roomImgNames = "";
                if (node != null)
                    roomImgNames = node.InnerXml.Trim();

                if (roomimageids != "" && roomImgNames != "")
                {
                    if (!CheckFileWhiteList(roomImgNames))
                    {
                        myVRMException myvrmEx = new myVRMException(707);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }  
                }

                locRoom.RoomImage = roomImgNames;
                locRoom.RoomImageId = roomimageids;
                //ZD 100263 End

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Map1");
                string map1name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Map1Image");
                string map1Image = node.InnerXml.Trim();

                //ZD 100263 Start
                if (map1name != "" && map1Image != "")
                {
                    if (!CheckFileWhiteList(map1name))
                    {
                        myVRMException myvrmEx = new myVRMException(707);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100263 End

                imageId = 0;
                if (map1Image != "")
                    imageId = SetRoomImage(map1Image, 2);

                if (imageId > 0)
                {
                    locRoom.MapImage1 = map1name;
                    locRoom.MapImage1Id = imageId;
                }

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Map2");
                string map2name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Map2Image");
                string map2Image = node.InnerXml.Trim();

                //ZD 100263 Start
                if (map2name != "" && map2Image != "")
                {
                    if (!CheckFileWhiteList(map2name))
                    {
                        myVRMException myvrmEx = new myVRMException(707);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100263 End

                imageId = 0;
                if (map2Image != "")
                    imageId = SetRoomImage(map2Image, 3);

                if (imageId > 0)
                {
                    locRoom.MapImage2 = map2name;
                    locRoom.MapImage2Id = imageId;
                }

                //FB 2136 Starts
                imageId = 0;
                string security1name = "";
                node = xd.SelectSingleNode("//SetRoomProfile/Images/Security1");
                if (node != null)
                    security1name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Security1ImageId");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out imageId);

                //ZD 100263 Start
                if (security1name != "" && imageId > 0)
                {
                    if (!CheckFileWhiteList(security1name))
                    {
                        myVRMException myvrmEx = new myVRMException(707);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100263 End

                if (imageId > 0)
                {
                    locRoom.SecurityImage1 = security1name;
                    locRoom.SecurityImage1Id = imageId;
                }
                //FB 2136 Ends

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Misc1");
                string misc1name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Misc1Image");
                string misc1Image = node.InnerXml.Trim();

                //ZD 100263 Start
                if (misc1name != "" && misc1Image != "")
                {
                    if (!CheckFileWhiteList(misc1name))
                    {
                        myVRMException myvrmEx = new myVRMException(707);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100263 End


                imageId = 0;
                if (misc1Image != "")
                    imageId = SetRoomImage(misc1Image, 6);

                if (imageId > 0)
                {
                    locRoom.MiscImage1 = misc1name;
                    locRoom.MiscImage1Id = imageId;
                }

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Misc2");
                string misc2name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetRoomProfile/Images/Misc2Image");
                string misc2Image = node.InnerXml.Trim();

                //ZD 100263 Start
                if (misc2name != "" && misc2Image != "")
                {
                    if (!CheckFileWhiteList(misc2name))
                    {
                        myVRMException myvrmEx = new myVRMException(707);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100263 End

                imageId = 0;
                if (misc2Image != "")
                    imageId = SetRoomImage(misc2Image, 7);

                if (imageId > 0)
                {
                    locRoom.MiscImage2 = misc2name;
                    locRoom.MiscImage2Id = imageId;
                }
                // Image Project Code Lines end...

                node = xd.SelectSingleNode("//SetRoomProfile/Approvers/Approver1ID");
                string approver1ID = node.InnerXml.Trim();

                //FB 2539 
                if (!CheckApproverRights(approver1ID))
                {
                    myVRMException myVRMEx = new myVRMException(632);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetRoomProfile/Approvers/Approver2ID");
                string approver2ID = node.InnerXml.Trim();

                //FB 2539
                if (!CheckApproverRights(approver2ID))
                {
                    myVRMException myVRMEx = new myVRMException(632);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }


                node = xd.SelectSingleNode("//SetRoomProfile/Approvers/Approver3ID");
                string approver3ID = node.InnerXml.Trim();

                //FB 2539
                if (!CheckApproverRights(approver3ID))
                {
                    myVRMException myVRMEx = new myVRMException(632);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }


                node = xd.SelectSingleNode("//SetRoomProfile/EndpointID");
                string endpointID = node.InnerXml.Trim();
                if (endpointID.Length == 0)
                    endpointID = "0";
                locRoom.endpointid = Int32.Parse(endpointID);

                //locRoom.DefaultEquipmentid = Int32.Parse("-1");//FB 2392

                node = xd.SelectSingleNode("//SetRoomProfile/Custom1");
                string custom1 = node.InnerXml.Trim();
                locRoom.Custom1 = custom1;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom2");
                string custom2 = node.InnerXml.Trim();
                locRoom.Custom2 = custom2;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom3");
                string custom3 = node.InnerXml.Trim();
                locRoom.Custom3 = custom3;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom4");
                string custom4 = node.InnerXml.Trim();
                locRoom.Custom4 = custom4;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom5");
                string custom5 = node.InnerXml.Trim();
                locRoom.Custom5 = custom5;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom6");
                string custom6 = node.InnerXml.Trim();
                locRoom.Custom6 = custom6;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom7");
                string custom7 = node.InnerXml.Trim();
                locRoom.Custom7 = custom7;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom8");
                string custom8 = node.InnerXml.Trim();
                locRoom.Custom8 = custom8;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom9");
                string custom9 = node.InnerXml.Trim();
                locRoom.Custom9 = custom9;

                node = xd.SelectSingleNode("//SetRoomProfile/Custom10");
                string custom10 = node.InnerXml.Trim();
                locRoom.Custom10 = custom10;

                locRoom.isPublic = isPublic;//FB 2392

                userInfo = m_IuserDAO.GetByUserId(locRoom.adminId);

                DateTime modDate = DateTime.Now;

                timeZone.changeToGMTTime(sysSettings.TimeZone, ref modDate);//Code added for last modified date

                locRoom.Lastmodifieddate = modDate;

                XmlNodeList nodes = xd.SelectNodes("//SetRoomProfile/Departments/Department");

                m_IRoomDAO.clearFetch();

                if (roomID.ToLower().Contains("new"))
                {
                    m_IRoomDAO.Save(locRoom);
                    roomID = getMaxRoomID().ToString();
                }
                else
                    m_IRoomDAO.SaveOrUpdate(locRoom);

                //ZD 100196 Start
                if(roomUID == "")
                {
                    string def = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    Random rnd = new Random();
                    StringBuilder ret = new StringBuilder();
                    for (int i = 0; i < 6; i++)
                        ret.Append(def.Substring(rnd.Next(def.Length), 1));

                    locRoom.RoomUID = ret.ToString();
                    m_IRoomDAO.SaveOrUpdate(locRoom);
                }
                //ZD 100196 End

                if (!SaveLocApprover(locRoomUpd, approver1ID, approver2ID, approver3ID, roomID))
                {
                    locRoom = m_IRoomDAO.GetByRoomId(Int32.Parse(roomID));
                    m_IRoomDAO.clearFetch();
                    m_IRoomDAO.Delete(locRoom);
                    return false;
                }

                if (!SaveLocDepts(nodes, roomID, locRoomUpd))
                {
                    locRoom = m_IRoomDAO.GetByRoomId(Int32.Parse(roomID));
                    m_IRoomDAO.clearFetch();
                    m_IRoomDAO.Delete(locRoom);
                    return false;
                }
                //FB 2616 Start
                if (!ChangePartyName(locRoom.Name, locRoom.roomId, 0))
                {
                    m_log.Error("Issue in updating Conf_room_d table");
                }
                //FB 2616 End
                /*** FB 1756 ***/
                StringBuilder inXML = new StringBuilder(); //FB 2607 StringBuilder
                inXML.Append("<GetAllRoomsBasicInfo>");//FB 1756
                inXML.Append("<UserID>" + userID + "</UserID>");
                inXML.Append("<organizationID>" + organizationID + "</organizationID>");
                inXML.Append("<RoomID>"+ roomID +"</RoomID>");
                inXML.Append("</GetAllRoomsBasicInfo>"); //FB 1756

                obj.inXml = inXML.ToString();
                GetAllRoomsBasicInfo(ref obj);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in SetRoomProfile", ex);
                return false;
            }
        }

        /// <summary>
        /// SetRoomImage
        /// </summary>
        /// <param name="imgName"></param>
        /// <param name="imgSource"></param>
        /// <returns></returns>
        /// Image Attribute types
        ///room = 1 / roommap1 = 2 / roommap2 =3/ roomsec1 = 4 /roomsec2 = 5
        ///roommisc =6/ roommisc2 = 7/ av = 8 / catering = 9 / hk =10
        ///banner = 11/ highresbanner = 12/ companylogo = 13
        private int SetRoomImage(string actualImage, int attrType)
        {
            int imgId = 0;
            byte[] imageData = null;
            try
            {
                imageData = vrmImg.ConvertBase64ToByteArray(actualImage); //FB 2136
                vrmImage imgObj = new vrmImage();
                imgObj.OrgId = organizationID;
                imgObj.AttributeType = attrType;
                imgObj.AttributeImage = imageData;

                m_vrmFactor.SetImage(imgObj, ref imgId);
                return imgId;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in SetRoomProfile", ex);
                return 0;
            }
        }

        //ZD 100263 Starts
        private bool CheckFileWhiteList(string actualImageName)
        {
            try
            {
                string filextn = "";
                string[] roomNameList = null;
                List<string> strExtension = null;
                bool filecontains = false;

                if (actualImageName != "")
                {
                    roomNameList = actualImageName.Split(',');
                    for (int k = 0; k < roomNameList.Length; k++)
                    {
                        filextn = roomNameList[k].Split('.')[roomNameList[k].Split('.').Length - 1];
                        strExtension = new List<string> { "jpg", "jpeg", "png", "bmp", "gif" };
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in CheckFileWhiteList", ex);
                return false;
            }
        }
        //ZD 100263 End


        #endregion

        /** FB 2392 Whygo **/

        #region SetPrivatePublicRoomProfile
        /// <summary>
        /// SetPrivatePublicRoomProfile
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetPrivatePublicRoomProfile(ref vrmDataObject obj)
        {
            List<ICriterion> criterionLst = null; 
            vrmRoom locRoom = null;
            vrmRoom locRoomUpd = null;
            List<vrmTier2> t2RoomList = null;
            Int32 rmLimit = 0;
            Int32 isPublic = 0;
            vrmUser userInfo = null;
            int imageId = 0;
            string roomimageids = "";
            Boolean disabledEdit = false;
            string errMess = "";
            int deptID = 0;
            String deptName = "";
            ESPublicRoom PublicRoomField = null;
            string sName = "";
            string sPhone = "";
            string sEmail = "";
            List<vrmEndPoint> guestEndpoints = null;
            List<vrmRoom> roomChkList = null;
            List<vrmRoom> checkRoomCount = null;
            string sIPspeed = "384";
            string sIPaddress = "";
            string sISDNspeed = "384";
            string sISDNaddress = "";
            int pId = 1;
            int eptID = 0;
            int iSPeed = 384;
            string userID = "";
            string roomID = "";
            string orgid = "";
            string video = "0";
            try
            {
                locRoom = new vrmRoom();

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                m_obj = obj;
                XmlNode node;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/userID");
                userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/roomID");
                roomID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/organizationID"); 
                
                if (node != null)
                    orgid = node.InnerXml.Trim();

                
                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Video");
                video = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (!roomID.ToLower().Contains("new"))
                {
                    locRoomUpd = m_IRoomDAO.GetByRoomId(Int32.Parse(roomID));
                    locRoom.RoomID = locRoomUpd.RoomID;
                    locRoom.disabled = locRoomUpd.disabled;
                    locRoom.endpointid = locRoomUpd.endpointid;

                    if (locRoomUpd.disabled == 1)
                        disabledEdit = true;

                    isPublic = locRoomUpd.isPublic;

                }
                criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("orgId", organizationID));

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                int currVideoAvail = 0;
                Int32.TryParse(video, out currVideoAvail);
                int errID = 200;
                criterionLst.Add(Expression.Eq("Disabled", 0));
                criterionLst.Add(Expression.Eq("Extroom", 0));
                criterionLst.Add(Expression.Eq("isPublic", 0));//FB 2594
                if (video == "2") 
                {
                    criterionLst.Add(Expression.Eq("VideoAvailable", 2)); 
                    rmLimit = orgInfo.MaxVideoRooms;
                    errMess = "Video rooms limit exceeded";
                    errID = 455;
                }
                else
                {
                    criterionLst.Add(Expression.Lt("VideoAvailable", 2)); 
                    rmLimit = orgInfo.MaxNonVideoRooms;
                    errMess = "Non-Video rooms limit exceeded";
                    errID = 456;
                }

                if (locRoomUpd != null)
                {
                    if (locRoomUpd.VideoAvailable == currVideoAvail)
                    {
                        rmLimit += 1;
                    }
                    else if (locRoomUpd.VideoAvailable < 2 && currVideoAvail < 2)
                    {
                        rmLimit += 1;
                    }
                }

                if (!disabledEdit)
                {

                    checkRoomCount = m_IRoomDAO.GetByCriteria(criterionLst);
                    if (checkRoomCount.Count >= rmLimit)
                    {
                        m_log.Error(errMess);
                        myVRMException myvrmEx = new myVRMException(errID);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }


                locRoom.adminId = Int32.Parse(userID);
                locRoom.ResponseTime = Int32.Parse("60");

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/RoomName");
                string roomName = node.InnerXml.Trim();
                locRoom.Name = roomName;
                criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("Name", roomName));
                criterionLst.Add(Expression.Eq("orgId", organizationID));
                criterionLst.Add(Expression.Eq("Extroom", 0));
                if (!roomID.ToLower().Contains("new"))
                {
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomID", Int32.Parse(roomID))));
                }
                roomChkList = m_IRoomDAO.GetByCriteria(criterionLst);
                if (roomChkList.Count > 0)
                {
                    myVRMException myVRMEx = new myVRMException(256);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/MaximumCapacity");
                string maxCapacity = node.InnerXml.Trim();
                locRoom.Capacity = Int32.Parse(maxCapacity);

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/City");
                string city = node.InnerXml.Trim();
                locRoom.City = city;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/ZipCode");
                string zipCode = node.InnerXml.Trim();
                locRoom.Zipcode = zipCode;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/DefaultEquipment");
                string EquipmentName = node.InnerXml.Trim();
                locRoom.DefaultEquipmentid = 24;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/TimezoneID");
                string timezoneID = node.InnerXml.Trim();
                locRoom.TimezoneID = Int32.Parse(timezoneID);

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Longitude");
                string longitude = node.InnerXml.Trim();
                locRoom.Longitude = longitude;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Latitude");
                string latitude = node.InnerXml.Trim();
                locRoom.Latitude = latitude;
                // Image Project Code Lines starts...

                XmlNodeList rmnodes = xd.SelectNodes("//SetPrivatePublicRoomProfile/RoomImages/Image");
                if (rmnodes != null)
                {
                    string actualImage = "";
                    roomimageids = "";
                    foreach (XmlNode nd in rmnodes)
                    {
                        actualImage = nd.SelectSingleNode("ActualImage").InnerText.Trim();
                        if (actualImage != "")
                        {
                            imageId = SetRoomImage(actualImage, 1);
                            if (imageId > 0)
                            {
                                if (roomimageids == "")
                                    roomimageids = imageId.ToString();
                                else
                                    roomimageids += "," + imageId.ToString();
                            }
                            actualImage = "";
                            imageId = 0;
                        }
                    }
                }

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/RoomImageName");
                string roomImgNames = node.InnerXml.Trim();
                if (roomimageids != "" && roomImgNames != "")
                {
                    locRoom.RoomImage = roomImgNames;
                    locRoom.RoomImageId = roomimageids;
                }


                locRoom.OwnerID = locRoom.adminId;
                locRoom.orgId = organizationID;                
                locRoom.HandiCappedAccess = 0;
                locRoom.isVIP = 0;
                locRoom.isTelepresence = 0;
                locRoom.ServiceType = -1;
                locRoom.DedicatedVideo = "0";
                locRoom.DedicatedCodec = "0";
                locRoom.AVOnsiteSupportEmail = "";
                locRoom.MaxPhoneCall = 0;
                locRoom.SetupTime = 0;
                locRoom.TeardownTime = 0;
                locRoom.assistant = locRoom.adminId;
                locRoom.notifyemails = "";
                locRoom.L3LocationId = orgInfo.OnflyTopTierID;
                string tier2ID = orgInfo.OnflyMiddleTierID.ToString();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("ID", Int32.Parse(tier2ID)));
                criterionList.Add(Expression.Eq("disabled", 0));
                t2RoomList = m_IT2DAO.GetByCriteria(criterionList);
                if (t2RoomList.Count > 0)
                    locRoom.tier2 = (vrmTier2)t2RoomList[0];
                locRoom.Maplink = "";
                locRoom.ParkingDirections = "";
                locRoom.AdditionalComments = ""; 
                locRoom.Caterer = "1";
                locRoom.DynamicRoomLayout = "0";
                locRoom.ProjectorAvailable = 0;
                locRoom.VideoAvailable = Int32.Parse(video);
                locRoom.isPublic = 1;
                userInfo = m_IuserDAO.GetByUserId(locRoom.adminId);
                DateTime modDate = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref modDate);
                locRoom.Lastmodifieddate = modDate;
                deptName = userInfo.Email.Trim().Replace("@","");
                if (m_vrmFactor == null)
                    m_vrmFactor = new vrmFactory(ref obj);
                m_vrmFactor.organizationID = organizationID;
                m_vrmFactor.UpdateManagePrivateDepartment(ref deptID, deptName, locRoom.adminId);

                if (video == "2")
                {
                    if(locRoom.endpointid > 0)
                        eptID = locRoom.endpointid;

                    if (xd.SelectSingleNode("//SetPrivatePublicRoomProfile/IP") != null)
                    {
                        node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/IP/Address");
                        if (node != null && node.InnerXml.ToString().Trim() != "")
                            sIPaddress = node.InnerXml.ToString().Trim();

                        node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/IP/Speed");
                        if (node != null && node.InnerXml.ToString().Trim() != "")
                            sIPspeed = node.InnerXml.ToString().Trim();

                        if (sIPaddress.Trim() != "") 
                        {
                            iSPeed =  Int32.Parse(sIPspeed);
                            AddEndpointProfile(ref guestEndpoints, pId, "IP", locRoom.Name, sIPaddress, 1, 1, 2, iSPeed, locRoom.DefaultEquipmentid);
                            pId = pId + 1;
                        }
                    }

                    if (xd.SelectSingleNode("//SetPrivatePublicRoomProfile/ISDN") != null)
                    {


                        node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/ISDN/Speed");
                        if (node != null && node.InnerXml.ToString().Trim() != "")
                            sISDNspeed = node.InnerXml.ToString().Trim();

                        node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/ISDN/Address");
                        if (node != null && node.InnerXml.ToString().Trim() != "")
                            sISDNaddress = node.InnerXml.ToString().Trim();

                        if (sISDNaddress.Trim() != "")
                        {
                            iSPeed = Int32.Parse(sISDNspeed);
                            AddEndpointProfile(ref guestEndpoints, pId, "ISDN", locRoom.Name, sISDNaddress, 4, 0, 2, iSPeed, locRoom.DefaultEquipmentid);
                        }
                    }

                    if (guestEndpoints != null && guestEndpoints.Count > 0)
                    {
                        if(!SetPrivateEndpoint(ref guestEndpoints,ref eptID))
                        {

                        }
                    }

                    locRoom.endpointid = eptID;
 
                }

                m_IRoomDAO.clearFetch();

                if (roomID.ToLower().Contains("new"))
                {
                    m_IRoomDAO.Save(locRoom);
                    roomID = getMaxRoomID().ToString();
                }
                else
                    m_IRoomDAO.SaveOrUpdate(locRoom);

                if (!SaveLocDepts(deptID, roomID, locRoomUpd))
                {
                    locRoom = m_IRoomDAO.GetByRoomId(Int32.Parse(roomID));
                    m_IRoomDAO.clearFetch();
                    m_IRoomDAO.Delete(locRoom);
                    return false;
                }
               
                /** Now come the public fields **/
                PublicRoomField = m_IESPublicRoomDAO.GetBymyVRMRoomId(locRoom.roomId);
                if (PublicRoomField == null)
                    PublicRoomField = new ESPublicRoom();

                PublicRoomField.RoomID = locRoom.roomId;
                PublicRoomField.WhygoRoomId = -1;
                PublicRoomField.PublicRoomLastModified = modDate;
                PublicRoomField.IsAfterHourEnabled = 0;
                PublicRoomField.IsEarlyHoursEnabled = 0;
                PublicRoomField.isCrazyHoursSupported = 0;
                PublicRoomField.Is24HoursEnabled = 0;
                PublicRoomField.DefaultEquipment = EquipmentName;
                //FB 2543 Starts
                PublicRoomField.CHFullyAuto = 0;
                PublicRoomField.AHFullyAuto = 0;
                PublicRoomField.EHFullyAuto = 0;
                //FB 2543 Ends

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/State");
                PublicRoomField.StateName = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Country");
                PublicRoomField.Country = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/CateringOptions");
                string sCateringOptions = "";
                if(node != null)
                    sCateringOptions = node.InnerXml.Trim();
                PublicRoomField.CateringOptionsAvailable = sCateringOptions;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/AUXEquipment");
                string sAUXEquipment = "";
                if(node != null)
                    sAUXEquipment = node.InnerXml.Trim();
                PublicRoomField.AUXEquipment = sAUXEquipment;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Description");
                string sDescription = "";
                if (node != null)
                    sDescription = node.InnerXml.Trim();
                PublicRoomField.RoomDescription = sDescription;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/ExtraNotes");
                string sExtraNotes = "";
                if (node != null)
                    sExtraNotes = node.InnerXml.Trim();
                PublicRoomField.ExtraNotes = sExtraNotes;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Layout");
                string sLayout = "";
                if (node != null)
                    sLayout = node.InnerXml.Trim();
                PublicRoomField.Layout = sLayout;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Address");
                string sAddress = "";
                if (node != null)
                    sAddress = node.InnerXml.Trim();
                PublicRoomField.address = sAddress;

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/SiteCordinator");
                if (node != null)
                {
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/SiteCordinator/Name");
                    if (node != null)
                        sName = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/SiteCordinator/Phone");
                    if (node != null)
                        sPhone = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/SiteCordinator/Email");
                    if (node != null)
                        sEmail = node.InnerXml.Trim();

                    PublicRoomField.SitecordinatorEmail = sEmail;
                    PublicRoomField.SitecordinatorName = sName;
                    PublicRoomField.SitecordinatorPhone = sPhone;
                    PublicRoomField.SitecordinatorID = -1;
                }

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Manager");
                if (node != null)
                {
                    sName = ""; sPhone = ""; sEmail = "";
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Manager/Name");
                    if (node != null)
                        sName = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Manager/Phone");
                    if (node != null)
                        sPhone = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Manager/Email");
                    if (node != null)
                        sEmail = node.InnerXml.Trim();

                    PublicRoomField.ManagerEmail = sEmail;
                    PublicRoomField.ManagerName = sName;
                    PublicRoomField.ManagerPhone = sPhone;
                    PublicRoomField.ManagerID = -1;
                }

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/TechnicalContact");
                if (node != null)
                {
                    sName = ""; sPhone = ""; sEmail = "";
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/TechnicalContact/Name");
                    if (node != null)
                        sName = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/TechnicalContact/Phone");
                    if (node != null)
                        sPhone = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/TechnicalContact/Email");
                    if (node != null)
                        sEmail = node.InnerXml.Trim();

                    PublicRoomField.TechnicalContactEmail = sEmail;
                    PublicRoomField.TechnicalContactName = sName;
                    PublicRoomField.TechnicalContactPhone = sPhone;
                    PublicRoomField.TechnicalContactID = -1;
                }

                PublicRoomField.IPAddress = sIPaddress;
                PublicRoomField.ISDNNumber = sISDNaddress;
                PublicRoomField.IPSpeed = int.Parse(sIPspeed);
                PublicRoomField.ISDNSpeed = int.Parse(sISDNspeed);               

                node = xd.SelectSingleNode("//SetPrivatePublicRoomProfile/Type");
                string sType = "";
                if (node != null)
                    sType = node.InnerXml.Trim();
                PublicRoomField.Type = sType;

                m_IESPublicRoomDAO.SaveOrUpdate(PublicRoomField);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in SetPrivatePublicRoomProfile", ex);
                return false;
            }
        }
            
        #endregion

        #region GetPrivatePublicRoomProfile
        /// <summary>
        /// GetPrivatePublicRoomProfile
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetPrivatePublicRoomProfile(ref vrmDataObject obj)
        {
            vrmRoom locRoom = null;
            vrmUser userInfo = null;
            vrmInactiveUser inActiveUserInfo = null;
            vrmImg = new imageFactory(ref obj);
            StringBuilder publicFields = null;
            ESPublicRoom PublicRoomField = null;
            String stateName = "";
            List<vrmRoom> checkRoomCount = null;
            XmlDocument xd = null;
            XmlNode node;
            int organizationID = 0;
            int OwnerId = 0;
            List<ICriterion> criterionLst = null;
            int j = 0;
            try
            {
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                node = xd.SelectSingleNode("//GetPrivatePublicRooms/organizationID");
                organizationID = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//GetPrivatePublicRooms/UserID");
                OwnerId = Int32.Parse(node.InnerXml.Trim());

                criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("orgId", organizationID));
                criterionLst.Add(Expression.Eq("OwnerID", OwnerId));
                criterionLst.Add(Expression.Eq("Disabled", 0));
                checkRoomCount = m_IRoomDAO.GetByCriteria(criterionLst);
                obj.outXml += "<GetPrivatePublicRoomProfiles>";
                for (j = 0; j < checkRoomCount.Count; j++)
                {
                    locRoom = m_IRoomDAO.GetByRoomId(checkRoomCount[j].roomId);
                    if (locRoom == null)
                        throw new Exception("No data matching to this roomID " + checkRoomCount[j].roomId);

                    PublicRoomField = m_IESPublicRoomDAO.GetBymyVRMRoomId(locRoom.roomId);//FB 2392

                    obj.outXml += "<GetPrivatePublicRoomProfile>";
                    obj.outXml += "<RoomID>" + locRoom.roomId.ToString() + "</RoomID>";
                    obj.outXml += "<RoomName>" + locRoom.Name + "</RoomName>";
                    obj.outXml += "<RoomQueue>" + locRoom.RoomQueue + "</RoomQueue>"; //FB 2342
                    obj.outXml += "<RoomUID>" + locRoom.RoomUID + "</RoomUID>"; //ZD 100196
                    obj.outXml += "<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>";
                    obj.outXml += "<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>";
                    obj.outXml += "<MaximumConcurrentPhoneCalls>" + locRoom.MaxPhoneCall.ToString() + "</MaximumConcurrentPhoneCalls>";
                    obj.outXml += "<SetupTime>" + locRoom.SetupTime.ToString() + "</SetupTime>";
                    obj.outXml += "<TeardownTime>" + locRoom.TeardownTime.ToString() + "</TeardownTime>";
                    obj.outXml += "<AssistantInchargeID>" + locRoom.assistant.ToString() + "</AssistantInchargeID>";
                    userInfo = m_IuserDAO.GetByUserId(locRoom.assistant);

                    //FB 1644
                    if (userInfo != null)
                        obj.outXml += "<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>";
                    else
                    {
                        inActiveUserInfo = m_IInactiveuserDAO.GetByUserId(locRoom.assistant);
                        if (inActiveUserInfo != null)
                            obj.outXml += "<AssistantInchargeName>" + inActiveUserInfo.FirstName + " " + inActiveUserInfo.LastName + "</AssistantInchargeName>";
                        obj.outXml += "<UserStaus>I</UserStaus>";
                    }

                    obj.outXml += "<MultipleAssistantEmails>" + locRoom.notifyemails + "</MultipleAssistantEmails>";
                    obj.outXml += "<Tier1ID>" + locRoom.tier2.tier3.ID.ToString() + "</Tier1ID>";
                    obj.outXml += "<Tier1Name>" + locRoom.tier2.tier3.Name + "</Tier1Name>";
                    obj.outXml += "<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>";
                    obj.outXml += "<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>";
                    obj.outXml += "<CatererFacility>" + locRoom.Caterer.ToString() + "</CatererFacility>";
                    obj.outXml += "<DynamicRoomLayout>" + locRoom.DynamicRoomLayout.ToString() + "</DynamicRoomLayout>";
                    obj.outXml += "<Projector>" + locRoom.ProjectorAvailable.ToString() + "</Projector>";
                    obj.outXml += "<isTelepresence>" + locRoom.isTelepresence.ToString() + "</isTelepresence>";//FB 2170
                    obj.outXml += "<ServiceType>" + locRoom.ServiceType.ToString() + "</ServiceType>";//FB 2219
                    obj.outXml += "<DedicatedVideo>" + locRoom.DedicatedVideo + "</DedicatedVideo>";//FB 2334
                    obj.outXml += "<DedicatedCodec>" + locRoom.DedicatedCodec + "</DedicatedCodec>";//FB 2390
                    obj.outXml += "<AVOnsiteSupportEmail>" + locRoom.AVOnsiteSupportEmail + "</AVOnsiteSupportEmail>";//FB 2415
                    obj.outXml += "<Video>" + locRoom.VideoAvailable.ToString() + "</Video>";
                    obj.outXml += "<Floor>" + locRoom.RoomFloor + "</Floor>";
                    obj.outXml += "<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>";
                    obj.outXml += "<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>";
                    obj.outXml += "<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>";
                    obj.outXml += "<City>" + locRoom.City + "</City>";
                    obj.outXml += "<State>" + locRoom.State.ToString() + "</State>";
                    //FB 2426 Start
                    obj.outXml += "<Extroom>" + locRoom.Extroom.ToString() + "</Extroom>";
                    obj.outXml += "<LoginUserId>" + locRoom.adminId + "</LoginUserId>";
                    //FB 2426 End
                    if (locRoom.State > 0)
                    {
                        vrmState objState = m_IStateDAO.GetById(locRoom.State);
                        obj.outXml += "<StateName>" + objState.StateCode.ToString() + "</StateName>";
                    }
                    else
                    {
                        stateName = ""; //FB 2392
                        if (PublicRoomField != null)
                            stateName = PublicRoomField.StateName;

                        obj.outXml += "<StateName>" + stateName + "</StateName>";
                    }
                    obj.outXml += "<ZipCode>" + locRoom.Zipcode + "</ZipCode>";
                    obj.outXml += "<Country>" + locRoom.Country.ToString() + "</Country>";
                    if (locRoom.Country > 0)
                    {
                        vrmCountry objCountry = m_ICountryDAO.GetById(locRoom.Country);
                        obj.outXml += "<CountryName>" + objCountry.CountryName + "</CountryName>";
                    }
                    else
                    {
                        obj.outXml += "<CountryName></CountryName>";
                    }
                    obj.outXml += "<MapLink>" + locRoom.Maplink + "</MapLink>";
                    obj.outXml += "<ParkingDirections>" + locRoom.ParkingDirections + "</ParkingDirections>";
                    obj.outXml += "<AdditionalComments>" + locRoom.AdditionalComments + "</AdditionalComments>";
                    obj.outXml += "<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>";
                    if (locRoom.TimezoneID > 0)
                    {
                        timeZoneData tz = new timeZoneData();
                        timeZone.GetTimeZone(locRoom.TimezoneID, ref tz);
                        obj.outXml += "<TimezoneName>" + tz.TimeZone + "</TimezoneName>";
                    }
                    else
                    {
                        obj.outXml += "<TimezoneName></TimezoneName>";
                    }
                    obj.outXml += "<Longitude>" + locRoom.Longitude + "</Longitude>";
                    obj.outXml += "<Latitude>" + locRoom.Latitude + "</Latitude>";

                    obj.outXml += "<Approvers>";
                    int i = 1;
                    foreach (vrmLocApprover locRoomApprov in locRoom.locationApprover)
                    {
                        userInfo = m_IuserDAO.GetByUserId(locRoomApprov.approverid);
                        obj.outXml += "<Approver" + i.ToString() + "ID>" + locRoomApprov.approverid.ToString() + "</Approver" + i.ToString() + "ID>";
                        obj.outXml += "<Approver" + i.ToString() + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "  </Approver" + i.ToString() + "Name>";
                        i++;
                    }
                    if (i == 1)
                    {
                        obj.outXml += "<Approver1ID></Approver1ID>";
                        obj.outXml += "<Approver1Name></Approver1Name>";
                        obj.outXml += "<Approver2ID></Approver2ID>";
                        obj.outXml += "<Approver2Name></Approver2Name>";
                        obj.outXml += "<Approver3ID></Approver3ID>";
                        obj.outXml += "<Approver3Name></Approver3Name>";
                    }
                    else if (i == 2)
                    {
                        obj.outXml += "<Approver2ID></Approver2ID>";
                        obj.outXml += "<Approver2Name></Approver2Name>";
                        obj.outXml += "<Approver3ID></Approver3ID>";
                        obj.outXml += "<Approver3Name></Approver3Name>";
                    }
                    else if (i == 3)
                    {
                        obj.outXml += "<Approver3ID></Approver3ID>";
                        obj.outXml += "<Approver3Name></Approver3Name>";
                    }
                    obj.outXml += "</Approvers>";
                    obj.outXml += "<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>";
                    List<vrmEndPoint> eptList = new List<vrmEndPoint>();
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("endpointid", locRoom.endpointid));
                    criterionList.Add(Expression.Eq("deleted", 0));
                    eptList = m_vrmEpt.GetByCriteria(criterionList);
                    if (eptList.Count > 0)
                    {
                        foreach (vrmEndPoint ept in eptList)
                        {
                            obj.outXml += "<EndpointName>" + ept.name + "</EndpointName>";
                            obj.outXml += "<EndpointIP>" + ept.address + "</EndpointIP>";
                            obj.outXml += "<isTelePresEndPoint>" + ept.isTelePresence + "</isTelePresEndPoint>"; //FB 2400 start
                            obj.outXml += "<MultiCodec>";
                            if (ept.MultiCodecAddress != null)
                            {
                                String[] multiCodec = ept.MultiCodecAddress.Split('Ö');
                                for (int a = 0; a < multiCodec.Length; a++)
                                {
                                    if (multiCodec[a].Trim() != "")
                                        obj.outXml += "<Address>" + multiCodec[a].Trim() + "</Address>";

                                }
                            }
                            obj.outXml += "</MultiCodec>";
                            break;
                        }
                    }
                    else
                    {
                        obj.outXml += "<EndpointName></EndpointName>";
                        obj.outXml += "<EndpointIP></EndpointIP>";
                        obj.outXml += "<MultiCodec></MultiCodec>";
                        obj.outXml += "<isTelePresEndPoint>0</isTelePresEndPoint>"; //FB 2400 end
                    }
                    obj.outXml += "<RoomImages>";

                    string roomImagesids = locRoom.RoomImageId;
                    string roomImagesnames = locRoom.RoomImage;

                    vrmImage imObj = null;
                    string imgDt = "";
                    string imagename = "";
                    int imageid = 0;
                    if (roomImagesids != null && roomImagesnames != null)
                    {
                        if (roomImagesids != "" && roomImagesnames != "")
                        {
                            imgDt = "";
                            imObj = null;
                            string[] idArr = roomImagesids.Split(',');
                            string[] nameArr = roomImagesnames.Split(',');
                            string fileext = "";
                            if (idArr.Length > 0 && nameArr.Length > 0)
                            {
                                for (int lp = 0, k = 0; lp < idArr.Length; lp++, k++)
                                {
                                    imagename = "";
                                    fileext = "jpg";

                                    //if(k < nameArr.Length)
                                    imagename = nameArr[k].ToString();

                                    if (imagename != "")
                                        fileext = imagename.Substring(imagename.LastIndexOf(".") + 1);

                                    Int32.TryParse(idArr[lp].ToString(), out imageid);

                                    imObj = m_IImageDAO.GetById(imageid);
                                    if (imObj == null)
                                        continue;

                                    imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                                    obj.outXml += "<ImageDetails>";
                                    obj.outXml += "<ImageName>" + imagename + "</ImageName>";
                                    obj.outXml += "<Imagetype>" + fileext + "</Imagetype>";
                                    obj.outXml += "<Image>" + imgDt + "</Image>";
                                    obj.outXml += "</ImageDetails>";
                                }
                            }
                        }
                    }
                    obj.outXml += "</RoomImages>";

                    obj.outXml += "<Images>";

                    obj.outXml += "<Map1>" + locRoom.MapImage1 + "</Map1>";
                    obj.outXml += "<Map1Image>" + GetRoomImage(locRoom.MapImage1Id) + "</Map1Image>";

                    obj.outXml += "<Map2>" + locRoom.MapImage2 + "</Map2>";
                    obj.outXml += "<Map2Image>" + GetRoomImage(locRoom.MapImage2Id) + "</Map2Image>";

                    //FB 2136 start
                    obj.outXml += "<Security1>" + locRoom.SecurityImage1 + "</Security1>";
                    obj.outXml += "<Security1ImageId>" + locRoom.SecurityImage1Id + "</Security1ImageId>";
                    obj.outXml += "<Security1Image>" + GetSecImage(locRoom.SecurityImage1Id) + "</Security1Image>";
                    //FB 2136 end

                    obj.outXml += "<Misc1>" + locRoom.MiscImage1 + "</Misc1>";
                    obj.outXml += "<Misc1Image>" + GetRoomImage(locRoom.MiscImage1Id) + "</Misc1Image>";

                    obj.outXml += "<Misc2>" + locRoom.MiscImage2 + "</Misc2>";
                    obj.outXml += "<Misc2Image>" + GetRoomImage(locRoom.MiscImage2Id) + "</Misc2Image>";

                    obj.outXml += "</Images>";

                    //Image Project codelines end...

                    obj.outXml += "<Custom1>" + locRoom.Custom1 + "</Custom1>";
                    obj.outXml += "<Custom2>" + locRoom.Custom2 + "</Custom2>";
                    obj.outXml += "<Custom3>" + locRoom.Custom3 + "</Custom3>";
                    obj.outXml += "<Custom4>" + locRoom.Custom4 + "</Custom4>";
                    obj.outXml += "<Custom5>" + locRoom.Custom5 + "</Custom5>";
                    obj.outXml += "<Custom6>" + locRoom.Custom6 + "</Custom6>";
                    obj.outXml += "<Custom7>" + locRoom.Custom7 + "</Custom7>";
                    obj.outXml += "<Custom8>" + locRoom.Custom8 + "</Custom8>";
                    obj.outXml += "<Custom9>" + locRoom.Custom9 + "</Custom9>";
                    obj.outXml += "<Custom10>" + locRoom.Custom10 + "</Custom10>";

                    obj.outXml += "<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>";
                    obj.outXml += "<isVIP>" + locRoom.isVIP.ToString() + "</isVIP>";

                    obj.outXml += "<LastModifiedDate>" + locRoom.Lastmodifieddate.ToString() + "</LastModifiedDate>";
                    obj.outXml += "<LastModififeduserID>" + locRoom.adminId.ToString() + "</LastModififeduserID>";
                    userInfo = m_IuserDAO.GetByUserId(locRoom.adminId);
                    if (userInfo != null)
                        obj.outXml += "<LastModififeduserName>" + userInfo.FirstName + " " + userInfo.LastName + "</LastModififeduserName>";
                    else
                        obj.outXml += "<LastModififeduserName></LastModififeduserName>";


                    obj.outXml += "<Departments>";
                    foreach (vrmLocDepartment locDept in locRoom.locationDept)
                    {
                        vrmDept objDept = m_IdeptDAO.GetById(locDept.departmentId);
                        obj.outXml += "<Department>";
                        obj.outXml += "<ID>" + locDept.departmentId.ToString() + "</ID>";
                        obj.outXml += "<Name>" + objDept.departmentName + "</Name>";
                        obj.outXml += "<SecurityKey>" + objDept.securityKey + "</SecurityKey>";
                        obj.outXml += "</Department>";
                    }
                    obj.outXml += "</Departments>";
                    //FB 2392 start -WhyGo
                    obj.outXml += "<DefaultEquipmentID>" + locRoom.DefaultEquipmentid.ToString() + "</DefaultEquipmentID>";
                    obj.outXml += "<isPublic>" + locRoom.isPublic + "</isPublic>";
                    obj.outXml += "<PublicFields>";

                    int Speed = 384;

                    if (PublicRoomField != null)
                    {
                        publicFields = new StringBuilder();
                        if (eptList.Count > 0)
                            Speed = eptList[0].linerateid;

                        publicFields.Append("<WhygoRoomID>" + PublicRoomField.WhygoRoomId + "</WhygoRoomID>");
                        publicFields.Append("<Address>" + PublicRoomField.address + "</Address>");
                        publicFields.Append("<State>" + PublicRoomField.StateName + "</State>");
                        publicFields.Append("<Country>" + PublicRoomField.Country + "</Country>");
                        publicFields.Append("<AUXEquipment>" + PublicRoomField.AUXEquipment + "</AUXEquipment>");
                        publicFields.Append("<Speed>" + Speed + "</Speed>");
                        publicFields.Append("<Type>" + PublicRoomField.Type + "</Type>");
                        publicFields.Append("<Description>" + PublicRoomField.RoomDescription + "</Description>");
                        publicFields.Append("<ExtraNotes>" + PublicRoomField.ExtraNotes + "</ExtraNotes>");
                        publicFields.Append("<GenericSellPrice>" + PublicRoomField.genericSellPrice + "</GenericSellPrice>");
                        publicFields.Append("<GeoCodeAddress>" + PublicRoomField.geoCodeAddress + "</GeoCodeAddress>");
                        publicFields.Append("<InternetBiller>" + PublicRoomField.internetBiller + "</InternetBiller>");
                        publicFields.Append("<InternetPriceCurrency>" + PublicRoomField.internetPriceCurrency + "</InternetPriceCurrency>");
                        publicFields.Append("<IsAutomatic>" + PublicRoomField.isAutomatic + "</IsAutomatic>");
                        publicFields.Append("<ImgLink>" + PublicRoomField.isAutomatic + "</ImgLink>");
                        publicFields.Append("<IsISDNCapable>" + PublicRoomField.isISDNCapable + "</IsISDNCapable>");
                        publicFields.Append("<IsHDCapable>" + PublicRoomField.isHDCapable + "</IsHDCapable>");
                        publicFields.Append("<IsInternetCapable>" + PublicRoomField.isInternetCapable + "</IsInternetCapable>");
                        publicFields.Append("<IsInternetFree>" + PublicRoomField.isInternetFree + "</IsInternetFree>");
                        publicFields.Append("<IsIPCapable>" + PublicRoomField.isIPCapable + "</IsIPCapable>");
                        publicFields.Append("<IsIPCConnectionCapable>" + PublicRoomField.isIPCConnectionCapable + "</IsIPCConnectionCapable>");
                        publicFields.Append("<IsIPDedicated>" + PublicRoomField.isIPDedicated + "</IsIPDedicated>");
                        publicFields.Append("<IsTP>" + PublicRoomField.isTP + "</IsTP>");
                        publicFields.Append("<isVCCapable>" + PublicRoomField.isVCCapable + "</isVCCapable>");
                        publicFields.Append("<CurrencyType>" + PublicRoomField.CurrencyType + "</CurrencyType>");
                        publicFields.Append("<IsEarlyHoursEnabled>" + PublicRoomField.IsEarlyHoursEnabled + "</IsEarlyHoursEnabled>");
                        publicFields.Append("<EarlyHoursStart>" + PublicRoomField.EHStartTime + "</EarlyHoursStart>");
                        publicFields.Append("<EarlyHoursEnd>" + PublicRoomField.EHEndTime + "</EarlyHoursEnd>");
                        publicFields.Append("<EarlyHoursCost>" + PublicRoomField.EHCost + "</EarlyHoursCost>");
                        publicFields.Append("<IsEarlyHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsEarlyHoursFullyAuto>"); //FB 2543
                        publicFields.Append("<OpenHour>" + PublicRoomField.openHours + "</OpenHour>");
                        publicFields.Append("<OfficeHoursStart>" + PublicRoomField.OHStartTime + "</OfficeHoursStart>");
                        publicFields.Append("<OfficeHoursEnd>" + PublicRoomField.OHEndTime + "</OfficeHoursEnd>");
                        publicFields.Append("<OfficeHoursCost>" + PublicRoomField.OHCost + "</OfficeHoursCost>");
                        publicFields.Append("<IsAfterHourEnabled>" + PublicRoomField.IsAfterHourEnabled + "</IsAfterHourEnabled>");
                        publicFields.Append("<AfterHoursStart>" + PublicRoomField.AHStartTime + "</AfterHoursStart>");
                        publicFields.Append("<AfterHoursEnd>" + PublicRoomField.AHEndTime + "</AfterHoursEnd>");
                        publicFields.Append("<AfterHoursCost>" + PublicRoomField.AHCost + "</AfterHoursCost>");
                        publicFields.Append("<IsAfterHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsAfterHoursFullyAuto>"); //FB 2543
                        publicFields.Append("<IsCrazyHoursSupported>" + PublicRoomField.isCrazyHoursSupported + "</IsCrazyHoursSupported>");
                        publicFields.Append("<CrazyHoursStart>" + PublicRoomField.CHtartTime + "</CrazyHoursStart>");
                        publicFields.Append("<CrazyHoursEnd>" + PublicRoomField.CHEndTime + "</CrazyHoursEnd>");
                        publicFields.Append("<CrazyHoursCost>" + PublicRoomField.CHCost + "</CrazyHoursCost>");
                        publicFields.Append("<IsCrazyHoursFullyAuto>" + PublicRoomField.CHFullyAuto + "</IsCrazyHoursFullyAuto>"); //FB 2543
                        publicFields.Append("<Is24HoursEnabled>" + PublicRoomField.Is24HoursEnabled + "</Is24HoursEnabled>");
                        publicFields.Append("<CateringOptions>" + PublicRoomField.CateringOptionsAvailable + "</CateringOptions>");
                        publicFields.Append("<AUXEquipment>" + PublicRoomField.AUXEquipment + "</AUXEquipment>");
                        publicFields.Append("<DefaultEquipment>" + PublicRoomField.DefaultEquipment + "</DefaultEquipment>");
                        publicFields.Append("<Layout>" + PublicRoomField.Layout + "</Layout>");
                        publicFields.Append("<SiteCordinator>");
                        publicFields.Append("<Name>" + PublicRoomField.SitecordinatorName + "</Name>");
                        publicFields.Append("<Email>" + PublicRoomField.SitecordinatorEmail + "</Email>");
                        publicFields.Append("<Phone>" + PublicRoomField.SitecordinatorPhone + "</Phone>");
                        publicFields.Append("</SiteCordinator>");
                        publicFields.Append("<Manager>");
                        publicFields.Append("<Name>" + PublicRoomField.ManagerName + "</Name>");
                        publicFields.Append("<Email>" + PublicRoomField.ManagerEmail + "</Email>");
                        publicFields.Append("<Phone>" + PublicRoomField.ManagerPhone + "</Phone>");
                        publicFields.Append("</Manager>");
                        publicFields.Append("<TechnicalContact>");
                        publicFields.Append("<Name>" + PublicRoomField.TechnicalContactName + "</Name>");
                        publicFields.Append("<Email>" + PublicRoomField.TechnicalContactEmail + "</Email>");
                        publicFields.Append("<Phone>" + PublicRoomField.TechnicalContactPhone + "</Phone>");
                        publicFields.Append("</TechnicalContact>");
                        publicFields.Append("<IP>");
                        publicFields.Append("<Speed>" + PublicRoomField.IPSpeed + "</Speed>");
                        publicFields.Append("<Address>" + PublicRoomField.IPAddress + "</Address>");
                        publicFields.Append("</IP>");
                        publicFields.Append("<ISDN>");
                        publicFields.Append("<Speed>" + PublicRoomField.ISDNSpeed + "</Speed>");
                        publicFields.Append("<Address>" + PublicRoomField.ISDNNumber + "</Address>");
                        publicFields.Append("</ISDN>");
                        publicFields.Append("<PublicRoomLastModified>" + PublicRoomField.PublicRoomLastModified.ToString() + "</PublicRoomLastModified>");
                        obj.outXml += publicFields.ToString();

                    }
                    obj.outXml += "</PublicFields>";
                    //FB 2392 end-WhyGo
                    obj.outXml += "</GetPrivatePublicRoomProfile>";
                }
                obj.outXml += "</GetPrivatePublicRoomProfiles>";
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetPrivatePublicRoomProfiles", ex);
                return false;
            }
        }

        #endregion

        #region GetPrivatePublicRoomID
        /// <summary>
        /// GetPrivatePublicRoomID
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetPrivatePublicRoomID(ref vrmDataObject obj)
        {
            List<vrmRoom> checkRoomCount = null;
            XmlDocument xd = null;
            XmlNode node;
            int organizationID = 0;
            int OwnerId = 0;
            List<ICriterion> criterionLst = null;
            int j = 0;
            try
            {
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                node = xd.SelectSingleNode("//GetPrivatePublicRooms/organizationID");
                organizationID = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//GetPrivatePublicRooms/UserID");
                OwnerId = Int32.Parse(node.InnerXml.Trim());

                criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("orgId", organizationID));
                criterionLst.Add(Expression.Eq("OwnerID", OwnerId));
                criterionLst.Add(Expression.Eq("Disabled", 0));
                checkRoomCount = m_IRoomDAO.GetByCriteria(criterionLst);
                obj.outXml += "<GetPrivatePublicRoomList>";
                for (j = 0; j < checkRoomCount.Count; j++)
                {
                    obj.outXml += "<Room>";
                    obj.outXml += "<RoomID>" + checkRoomCount[j].roomId + "</RoomID>";
                    obj.outXml += "<RoomName>" + checkRoomCount[j].Name + "</RoomName>";
                    obj.outXml += "</Room>";                 
                }
                obj.outXml += "</GetPrivatePublicRoomList>";
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetPrivatePublicRoomList", ex);
                return false;
            }
        }

        #endregion

        /** FB 2392 Whygo **/

        #region getMaxRoomID
        /// <summary>
        /// getMaxRoomID
        /// </summary>
        /// <returns></returns>
        private int getMaxRoomID()
        {
            try
            {
                string qString = "select max(LA.roomId) from myVRM.DataLayer.vrmRoom LA";

                IList list = m_IRoomDAO.execQuery(qString);

                string sMax;
                if (list[0] != null)
                    sMax = list[0].ToString();
                else
                    sMax = "0";

                int id = Int32.Parse(sMax);
                return id;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SaveLocDepts
        /// <summary>
        /// SaveLocDepts
        /// </summary>
        /// <param name="objNodes"></param>
        /// <param name="RoomID"></param>
        /// <param name="objRoom"></param>
        /// <returns></returns>
        public bool SaveLocDepts(XmlNodeList objNodes, string RoomID, vrmRoom objRoom)
        {
            vrmLocDepartment objLocDept = null;
            XmlTextReader xtr = null;
            try
            {
                if (objRoom != null)
                {
                    if (objRoom.locationDept.Count > 0)
                    {
                        foreach (vrmLocDepartment locDept in objRoom.locationDept)
                        {
                            vrmLocDepartment locDeptSave = new vrmLocDepartment();
                            locDeptSave.roomId = locDept.roomId;
                            locDeptSave.departmentId = locDept.departmentId;
                            locDeptSave.id = locDept.id;
                            m_IlocDeptDAO.Delete(locDeptSave);
                        }
                    }
                }

                if (objNodes.Count > 0)
                {
                    System.Data.DataSet ds = new System.Data.DataSet();

                    foreach (XmlNode node in objNodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, System.Data.XmlReadMode.InferSchema);
                    }
                    if (ds.Tables.Count > 0)
                    {
                        foreach (System.Data.DataTable dt in ds.Tables)
                        {
                            foreach (System.Data.DataRow dr in dt.Rows)
                            {
                                objLocDept = new vrmLocDepartment();
                                objLocDept.roomId = Int32.Parse(RoomID);
                                objLocDept.departmentId = Int32.Parse(dr["ID"].ToString());
                                m_IlocDeptDAO.Save(objLocDept);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {

                m_log.Error("SystemException in SaveLocDept", ex);
                return false;
            }
        }

        /*** 2392 whygo ***/
        public bool SaveLocDepts(int deptID, String RoomID, vrmRoom objRoom)
        {
            vrmLocDepartment objLocDept = null;
            XmlTextReader xtr = null;
            try
            {
                if (objRoom != null)
                {
                    if (objRoom.locationDept.Count > 0)
                    {
                        foreach (vrmLocDepartment locDept in objRoom.locationDept)
                        {
                            vrmLocDepartment locDeptSave = new vrmLocDepartment();
                            locDeptSave.roomId = locDept.roomId;
                            locDeptSave.departmentId = locDept.departmentId;
                            locDeptSave.id = locDept.id;
                            m_IlocDeptDAO.Delete(locDeptSave);
                        }
                    }


                    objLocDept = new vrmLocDepartment();
                    objLocDept.roomId = Int32.Parse(RoomID);
                    objLocDept.departmentId = deptID;
                    m_IlocDeptDAO.Save(objLocDept);


                }


                
                return true;
            }
            catch (Exception ex)
            {

                m_log.Error("SystemException in SaveLocDept", ex);
                return false;
            }
        }

        #endregion

        #region SaveLocApprover
        /// <summary>
        /// SaveLocApprover(as room id is given no org id is required
        /// )
        /// </summary>
        /// <param name="locRoomUpd"></param>
        /// <param name="approver1ID"></param>
        /// <param name="approver2ID"></param>
        /// <param name="approver3ID"></param>
        /// <param name="roomID"></param>
        /// <returns></returns>
        public bool SaveLocApprover(vrmRoom locRoomUpd, string approver1ID, string approver2ID, string approver3ID, string roomID)
        {
            IConfApprovalDAO IApprover; //To Check Conf_approval Table//FB 1171
            List<ICriterion> criterionList;//FB 1171
            bool blnDelete = true; //FB 1171
            DateTime currentDate = DateTime.Now; //FB 1158
            try
            {
                if (locRoomUpd != null)
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentDate);   //FB 1158

                    if (locRoomUpd.locationApprover.Count > 0)
                    {
                        foreach (vrmLocApprover locAppr in locRoomUpd.locationApprover)
                        {
                            //FB 1171 - Starts
                            criterionList = new List<ICriterion>();
                            List<vrmConference> confList = new List<vrmConference>();
                            criterionList.Add(Expression.Eq("deleted", 0));
                            criterionList.Add(Expression.Eq("status", (int)vrmConfStatus.Pending));
                            criterionList.Add(Expression.Ge("confdate", currentDate)); //FB 1158
                            confList = m_IconfDAO.GetByCriteria(criterionList);

                            foreach (vrmConference conf in confList)
                            {
                                foreach (vrmConfRoom confRm in conf.ConfRoom)
                                {
                                    if (locRoomUpd.roomId == confRm.roomId)
                                    {
                                        IApprover = m_confDAO.GetConfApprovalDao();
                                        criterionList = new List<ICriterion>();
                                        criterionList.Add(Expression.Eq("approverid", locAppr.approverid));
                                        criterionList.Add(Expression.Eq("entityid", locRoomUpd.roomId));
                                        criterionList.Add(Expression.Eq("entitytype", (int)LevelEntity.ROOM));
                                        criterionList.Add(Expression.Eq("confid", confRm.confid));
                                        criterionList.Add(Expression.Eq("instanceid", confRm.instanceid));
                                        criterionList.Add(Expression.Eq("decision", 0)); //Pending
                                        List<vrmConfApproval> ConfApprList = IApprover.GetByCriteria(criterionList);
                                        if (ConfApprList.Count > 0)
                                        {
                                            blnDelete = false;
                                            break;
                                        }
                                    }
                                }
                                if (!blnDelete)
                                    break;
                            }
                            if (!blnDelete)
                            {
                                //FB 1881 start
                                //m_obj.outXml = "<error><message>There are conferences pending for approval from this user. &lt;br&gt;Please approve conferences in List>View Approval Pending prior to deleting this user.</message></error>";  //FB 1173
                                myVRMException myVRMEx = new myVRMException(437);
                                m_obj.outXml = myVRMEx.FetchErrorMsg();
                                //FB 1881 end
                                return true;
                            }
                            else
                            {
                                vrmLocApprover locApprovSave = new vrmLocApprover();
                                locApprovSave.id = locAppr.id;
                                locApprovSave.roomid = locAppr.roomid;
                                locApprovSave.approverid = locAppr.approverid;
                                m_ILocApprovDAO.Delete(locApprovSave);
                                //break;    //FB 1158 - code line commented to delete all approvers
                            }
                        }
                        //FB 1171 - Ends
                    }
                }

                if (approver1ID.Length > 0)
                {
                    vrmLocApprover locApprovSave = new vrmLocApprover();
                    locApprovSave.roomid = Int32.Parse(roomID);
                    locApprovSave.approverid = Int32.Parse(approver1ID);
                    m_ILocApprovDAO.Save(locApprovSave);
                }
                if (approver2ID.Length > 0)
                {
                    vrmLocApprover locApprovSave = new vrmLocApprover();
                    locApprovSave.roomid = Int32.Parse(roomID);
                    locApprovSave.approverid = Int32.Parse(approver2ID);
                    m_ILocApprovDAO.Save(locApprovSave);
                }
                if (approver3ID.Length > 0)
                {
                    vrmLocApprover locApprovSave = new vrmLocApprover();
                    locApprovSave.roomid = Int32.Parse(roomID);
                    locApprovSave.approverid = Int32.Parse(approver3ID);
                    m_ILocApprovDAO.Save(locApprovSave);
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("SystemException in SaveLocApprover", ex);
                return false;
            }
        }
        #endregion

        #region DeleteTier1
        /// <summary>
        /// DeleteTier1
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteTier1(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = null;
            List<vrmTier3> t3RoomList = null;
            List<vrmTier2> t2RoomList = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//DeleteTier1/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//DeleteTier1/ID");
                string tier1ID = node.InnerXml.Trim();

                //FB 3064 - Start
                node = xd.SelectSingleNode("//DeleteTier1/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("ID", Int32.Parse(tier1ID)));
                criterionList.Add(Expression.Eq("disabled", 0)); 
                t3RoomList = m_IT3DAO.GetByCriteria(criterionList);
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (t3RoomList.Count < 1 || orgInfo.OnflyTopTierID.ToString().Trim() == tier1ID.Trim())
                {
                    myVRMException myVRMEx = new myVRMException(417);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }//FB 3064 - End
                else
                {
                    vrmTier3 t3Room = (vrmTier3)t3RoomList[0];
                    if (t3Room.tier2.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(417);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    else
                    {
                        t3Room = new vrmTier3();
                        t3Room.ID = Int32.Parse(tier1ID);
                        t3Room.disabled = 1;
                        m_IT3DAO.SaveOrUpdate(t3Room);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in DeleteTier1", ex);
                return false;
            }
        }
        #endregion

        #region DeleteTier2
        /// <summary>
        /// DeleteTier2
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteTier2(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = null;
            List<vrmTier2> t2RoomList = null;
            List<vrmRoom> roomList = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//DeleteTier2/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//DeleteTier2/ID");
                string tier2ID = node.InnerXml.Trim();

                //FB 3064 - Start
                node = xd.SelectSingleNode("//DeleteTier2/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);

                roomList = m_IRoomDAO.GetByT2Id(Int32.Parse(tier2ID));
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (roomList.Count > 0 || orgInfo.OnflyMiddleTierID.ToString().Trim() == tier2ID.Trim())                
                {
                    myVRMException myVRMEx = new myVRMException(417);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }//FB 3064 - End
                else
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("ID", Int32.Parse(tier2ID)));
                    criterionList.Add(Expression.Eq("disabled", 0));
                    t2RoomList = m_IT2DAO.GetByCriteria(criterionList);
                    if (t2RoomList.Count > 0)
                    {
                        vrmTier2 t2Room = new vrmTier2();
                        t2Room.ID = Int32.Parse(tier2ID);
                        t2Room.disabled = 1;
                        m_IT2DAO.SaveOrUpdate(t2Room);
                    }
                    else
                    {
                        obj.outXml = "<error>-1</error>";
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in DeleteTier2", ex);
                return false;
            }

        }
        #endregion

        #region SetTier1
        /// <summary>
        /// InXML:
        /// &lt;SetTier1&gt;
        ///   &lt;UserID&gt;&lt;/UserID&gt;
        ///   &lt;ID&gt;&lt;/ID&gt;
        ///   &lt;Name&gt;&lt;/Name&gt;
        /// &lt;/SetTier1&gt;
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetTier1(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = null;
            List<vrmTier3> t3RoomList = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SetTier1/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);


                node = xd.SelectSingleNode("//SetTier1/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetTier1/ID");
                string tier1ID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetTier1/Name");
                string tier1Name = node.InnerXml.Trim();

                //string outputXml = "<error>-1</error>";
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Name", tier1Name));
                criterionList.Add(Expression.Eq("orgId", organizationID));//Organization Module Fixes


                if (tier1ID.ToLower().Contains("new")) // New Entry
                {
                    t3RoomList = m_IT3DAO.GetByCriteria(criterionList);
                    if (t3RoomList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(427);  //FB 1617
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    vrmTier3 t3Room = new vrmTier3();
                    t3Room.Name = tier1Name;
                    t3Room.disabled = 0;
                    t3Room.orgId = organizationID;//Organization
                    m_IT3DAO.Save(t3Room);
                    obj.outXml = "<SetTier1><Tier1Name>"+tier1Name+"</Tier1Name><Tier1ID>" + t3Room.ID.ToString() + "</Tier1ID></SetTier1>";//FB 2426
                }
                else    // Edit
                {
                    criterionList.Add(Expression.Not(Expression.Eq("ID", Int32.Parse(tier1ID))));
                    t3RoomList = m_IT3DAO.GetByCriteria(criterionList);
                    if (t3RoomList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(427);  //FB 1617
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    vrmTier3 t3Room = new vrmTier3();
                    t3Room.ID = Int32.Parse(tier1ID);
                    t3Room.Name = tier1Name;
                    t3Room.orgId = organizationID;//Organization
                    t3Room.disabled = 0;

                    m_IT3DAO.SaveOrUpdate(t3Room);
                    obj.outXml = "<SetTier1><Tier1Name>" + tier1Name + "</Tier1Name><Tier1ID>" + t3Room.ID.ToString() + "</Tier1ID></SetTier1>";//FB 2426
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in SetTier1", ex);
                return false;
            }
        }
        #endregion

        #region SetTier2
        /// <summary>
        /// InXML:
        /// &lt;SetTier2&gt;
        ///   &lt;UserID&gt;&lt;/UserID&gt;
        ///   &lt;Tier1ID&gt;&lt;/Tier1ID&gt;
        ///   &lt;ID&gt;&lt;/ID&gt;
        ///   &lt;Name&gt;&lt;/Name&gt;
        /// &lt;/SetTier2&gt;
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetTier2(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = null;
            List<vrmTier2> t2RoomList = null;
            List<vrmTier3> t3RoomList = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SetTier2/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetTier2/Tier1ID");
                string tier1ID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetTier2/ID");
                string tier2ID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetTier2/Name");
                string tier2Name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetTier2/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);

                vrmTier3 tier3 = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("ID", Int32.Parse(tier1ID)));
                t3RoomList = m_IT3DAO.GetByCriteria(criterionList);
                if (t3RoomList.Count < 1)
                {
                    myVRMException myVRMEx = new myVRMException(256); 
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                //string outputXml = "<error>-1</error>";
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Name", tier2Name));
                criterionList.Add(Expression.Eq("orgId", organizationID)); //FB 1617
                criterionList.Add(Expression.Eq("L3LocationId", Int32.Parse(tier1ID))); //FB 2362
                if (tier2ID.ToLower().Contains("new")) // New Entry
                {
                    t2RoomList = m_IT2DAO.GetByCriteria(criterionList);
                    if (t2RoomList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(428);  //FB 1617
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    vrmTier2 t2Room = new vrmTier2();
                    t2Room.tier3 = (vrmTier3)t3RoomList[0];
                    t2Room.Name = tier2Name;
                    t2Room.disabled = 0;
                    t2Room.orgId = organizationID;//Organization
                    m_IT2DAO.Save(t2Room);
                    obj.outXml = "<SetTier2><Tier2Name>" + tier2Name + "</Tier2Name><Tier2ID>" + t2Room.ID.ToString() + "</Tier2ID></SetTier2>";//FB 2426
                }
                else    // Edit
                {
                    criterionList.Add(Expression.Not(Expression.Eq("ID", Int32.Parse(tier2ID))));
                    t2RoomList = m_IT2DAO.GetByCriteria(criterionList);
                    if (t2RoomList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(428);  //FB 1617
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    vrmTier2 t2Room = new vrmTier2();
                    t2Room.ID = Int32.Parse(tier2ID);
                    t2Room.tier3 = (vrmTier3)t3RoomList[0];
                    t2Room.Name = tier2Name;
                    t2Room.disabled = 0;
                    t2Room.orgId = organizationID;//Organization
                    m_IT2DAO.SaveOrUpdate(t2Room);
                    obj.outXml = "<SetTier2><Tier2Name>" + tier2Name + "</Tier2Name><Tier2ID>" + t2Room.ID.ToString() + "</Tier2ID></SetTier2>";//FB 2426
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in SetTier2", ex);
                return false;
            }
        }
        #endregion

        #region GetLocations2
        /// <summary>
        /// INXML:
        /// &lt;GetLocations2&gt;
        ///    &lt;UserID&gt;&lt;/UserID&gt;
        ///    &lt;Tier1ID&gt;&lt;/Tier1ID&gt;
        /// &lt;/GetLocations2&gt;
        /// OutXML:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool GetLocations2(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetLocations2/UserID");
                string userID = node.InnerXml.Trim();
                
                int tier2ID =0;
                node = xd.SelectSingleNode("//GetLocations2/Tier1ID");
                int.TryParse(node.InnerXml.Trim(),out tier2ID);


                StringBuilder outputXml = new StringBuilder();
                outputXml.Append("<GetLocations2>");
                outputXml.Append("<UserID>" + userID + "</UserID>");
                StringBuilder SelectedXML = new StringBuilder();
                if (GetT2Rooms(ref SelectedXML, tier2ID))
                    outputXml.Append(SelectedXML);

                outputXml.Append("</GetLocations2>");
                obj.outXml = outputXml.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetLocations2", ex);
                return false;
            }
        }
        #endregion

        #region GetLocations
        /// <summary>
        /// GetLocations
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLocations(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetLocations/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocations/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                int.TryParse(orgid, out organizationID);

                StringBuilder outputXml = new StringBuilder();
                outputXml.Append("<GetLocations>");

                StringBuilder SelectedXML = new StringBuilder();
                if (GetT3Rooms(ref SelectedXML, organizationID))
                    outputXml.Append(SelectedXML);

                outputXml.Append("</GetLocations>");
                obj.outXml = outputXml.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetLocations", ex);
                return false;
            }
        }
        #endregion

        #region GetSelectedLevel2Locations
        /// <summary>
        /// GetSelectedLevel2Locations
        /// </summary>
        /// <param name="oXML"></param>
        /// <returns></returns>
        public bool GetSelectedLevel2Locations(ref StringBuilder oXML)
        {
            bool blnRet = true;
            Hashtable htL2LocationID = null;
            try
            {
                oXML = new StringBuilder();
                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmConference> confList = new List<vrmConference>();
                htL2LocationID = new Hashtable();
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Ge("confdate", "getutcdate()"));
                confList = m_IconfDAO.GetByCriteria(criterionList);

                foreach (vrmConference conf in confList)
                {
                    foreach (vrmConfRoom confRm in conf.ConfRoom)
                    {
                        vrmRoom theRoom = m_IRoomDAO.GetById(confRm.roomId);
                        if (!htL2LocationID.ContainsKey(theRoom.L2LocationId))
                        {
                            oXML.Append("<tier1ID>" + theRoom.L2LocationId.ToString() + "</tier1ID>");
                            htL2LocationID.Add(theRoom.L2LocationId, theRoom.L2LocationId);
                        }
                    }
                }

                return blnRet;
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException in GetSelectedLevel2Locations", ex);
                return false;
            }
        }
        #endregion

        #region GetT2Rooms
        /// <summary>
        /// GetT2Rooms
        /// </summary>
        /// <param name="oXML"></param>
        /// <param name="T3RoomID"></param>
        /// <returns></returns>
        public bool GetT2Rooms(ref StringBuilder oXML, int T3RoomID)
        {
            bool blnRet = true;
            try
            {
                oXML = new StringBuilder();
                vrmTier3 t3Room = m_IT3DAO.GetById(T3RoomID);
                foreach (vrmTier2 T2Room in t3Room.tier2)
                {
                    if (T2Room.disabled == 0)
                    {
                        oXML.Append("<Location>");
                        oXML.Append("<ID>" + T2Room.ID.ToString() + "</ID>");
                        oXML.Append("<Name>" + T2Room.Name + "</Name>");
                        oXML.Append("</Location>");
                    }
                }

                return blnRet;
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException in GetT2Rooms", ex);
                return false;
            }
        }
        #endregion

        #region GetT3Rooms
        /// <summary>
        /// GetT3Rooms
        /// </summary>
        /// <param name="oXML"></param>
        /// <returns></returns>
        public bool GetT3Rooms(ref StringBuilder oXML, int orgID)
        {
            bool blnRet = true;
            try
            {
                oXML = new StringBuilder();
                m_IT3DAO.addOrderBy(Order.Asc("Name"));

                List<ICriterion> criterionList = new List<ICriterion>();//COde added for organization
                criterionList.Add(Expression.Eq("orgId", orgID));

                List<vrmTier3> T3Rooms = m_IT3DAO.GetByCriteria(criterionList);
                m_IT3DAO.clearOrderBy();

                foreach (vrmTier3 T3Room in T3Rooms)
                {
                    if (T3Room.disabled == 0)
                    {
                        oXML.Append("<Location>");
                        oXML.Append("<ID>" + T3Room.ID.ToString() + "</ID>");
                        oXML.Append("<Name>" + T3Room.Name + "</Name>");
                        oXML.Append("</Location>");
                    }
                }

                return blnRet;
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException in GetT3Rooms", ex);
                return false;
            }
        }
        #endregion

        #region GetRoomTree
        /// <summary>
        /// GetRoomTree
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="RoomList"></param>
        /// <returns></returns>
        public bool GetRoomTree(int userid, ref List<vrmTier3> RoomList)
        {
            bool bRet = true;

            try
            {
                vrmUser user = m_IuserDAO.GetByUserId(userid);

                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;    //Organization Module

                m_IT3DAO.addOrderBy(Order.Asc("Name"));
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("disabled", 0));
                RoomList = m_IT3DAO.GetByCriteria(criterionList);
                m_IT3DAO.clearOrderBy();

                bool m_bSuper = false;
                bool hasDepartment = false;//FB 1672
                Hashtable DeptList = new Hashtable();
                if (user.isSuperAdmin() || multiDepts == 0) //Organization Module
                {
                    m_bSuper = true;
                }
                else
                {
                    List<vrmLocDepartment> locDeptList = GetLocVsUsrDepartmentList(user);
                    if (locDeptList.Count > 0) //FB 1672
                        hasDepartment = true;

                    foreach (vrmLocDepartment locDept in locDeptList)
                        if (!DeptList.ContainsKey(locDept.roomId))
                            DeptList.Add(locDept.roomId, 1);
                    //FB 1672 -Starts
                    if (!hasDepartment && openDept==0)
                    {
                        string sqlSt = "SELECT rm.roomId FROM myVRM.DataLayer.vrmRoom rm ";
                        sqlSt += " WHERE rm.roomId <> 11 and rm.orgId='" + organizationID + "'";
                        sqlSt += " and rm.Disabled = 0 and rm.roomId not in (";
                        sqlSt += " SELECT vr.roomId FROM myVRM.DataLayer.vrmRoom vr, myVRM.DataLayer.vrmLocDepartment ld ";
                        sqlSt += " WHERE vr.roomId <> 11 and vr.orgId='" + organizationID + "'";
                        sqlSt += " and vr.Disabled = 0 and vr.roomId = ld.roomId)";

                        IList openRooms = m_IRoomDAO.execQuery(sqlSt);
                        if (openRooms != null)
                        {
                            if (openRooms.Count > 0)
                            {
                                for (int lp = 0; lp < openRooms.Count; lp++)
                                {
                                    if (openRooms[lp] != null)
                                    {
                                        int rmId = 0;
                                        if (openRooms[lp].ToString() != "")
                                            Int32.TryParse(openRooms[lp].ToString(), out rmId);

                                        if (rmId > 0)
                                        {
                                            if (!DeptList.ContainsKey(rmId))
                                                DeptList.Add(rmId, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //FB 1672 -End 
                foreach (vrmTier3 t3 in RoomList)
                {

                    bool b_t2 = false;
                    foreach (vrmTier2 t2 in t3.tier2)
                    {
                        bool b_room = false;
                        foreach (vrmRoom rm in t2.room)
                        {
                            if (m_bSuper)
                            {
                                if (rm.disabled == 0)
                                    b_room = true;
                            }
                            else
                            {
                                if (!DeptList.ContainsKey(rm.roomId))
                                    rm.m_display = false;//FB 2027
                                    //rm.b_display = false;
                                else
                                    if (rm.disabled == 0)
                                        b_room = true;
                            }
                        }
                        if (b_room)
                        {
                            t2.lowLevelCount++;
                            b_t2 = true;
                        }
                    }
                    if (b_t2)
                    {
                        t3.lowLevelCount++;
                    }
                }
                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        public bool GetRoomTreeOptimized(int userid, ref List<vrmTier3> RoomList, string cnfRooms) // FB 2594
        {
            bool bRet = true;

            try
            {
                vrmUser user = m_IuserDAO.GetByUserId(userid);

                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;    //Organization Module

                m_IT3DAO.addOrderBy(Order.Asc("Name"));
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("disabled", 0));
                RoomList = m_IT3DAO.GetByCriteria(criterionList);
                m_IT3DAO.clearOrderBy();

                bool m_bSuper = false;
                bool hasDepartment = false;//FB 1672
                Hashtable DeptList = new Hashtable();
                if (user.isSuperAdmin() || multiDepts == 0 || user.Admin == vrmUserConstant.VNOCADMIN) //Organization Module //FB 2670
                {
                    m_bSuper = true;
                }
                else
                {
                    List<vrmLocDepartment> locDeptList = GetLocVsUsrDepartmentList(user);
                    if (locDeptList.Count > 0) //FB 1672
                        hasDepartment = true;

                    foreach (vrmLocDepartment locDept in locDeptList)
                        if (!DeptList.ContainsKey(locDept.roomId))
                            DeptList.Add(locDept.roomId, 1);
                    //FB 1672 -Starts
                    if (!hasDepartment && openDept == 0)
                    {
                        string sqlSt = "SELECT rm.roomId FROM myVRM.DataLayer.vrmRoom rm ";
                        sqlSt += " WHERE rm.roomId <> 11 and rm.orgId='" + organizationID + "'";
                        sqlSt += " and rm.Disabled = 0 and rm.roomId not in (";
                        sqlSt += " SELECT vr.roomId FROM myVRM.DataLayer.vrmRoom vr, myVRM.DataLayer.vrmLocDepartment ld ";
                        sqlSt += " WHERE vr.roomId <> 11 and vr.orgId='" + organizationID + "'";
                        sqlSt += " and vr.Disabled = 0 and vr.roomId = ld.roomId)";
                        if(cnfRooms.Trim() != "")
                            sqlSt += " and rm.roomId in ("+ cnfRooms +")";

                        IList openRooms = m_IRoomDAO.execQuery(sqlSt);
                        if (openRooms != null)
                        {
                            if (openRooms.Count > 0)
                            {
                                for (int lp = 0; lp < openRooms.Count; lp++)
                                {
                                    if (openRooms[lp] != null)
                                    {
                                        int rmId = 0;
                                        if (openRooms[lp].ToString() != "")
                                            Int32.TryParse(openRooms[lp].ToString(), out rmId);

                                        if (rmId > 0)
                                        {
                                            if (!DeptList.ContainsKey(rmId))
                                                DeptList.Add(rmId, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //FB 1672 -End 
                foreach (vrmTier3 t3 in RoomList)
                {

                    bool b_t2 = false;
                    foreach (vrmTier2 t2 in t3.tier2)
                    {
                        bool b_room = false;
                        foreach (vrmRoom rm in t2.room)
                        {
                            if (m_bSuper)
                            {
                                if (rm.disabled == 0)
                                    b_room = true;
                            }
                            else
                            {
                                if (!DeptList.ContainsKey(rm.roomId))
                                    rm.m_display = false;//FB 2027
                                //rm.b_display = false;
                                else
                                    if (rm.disabled == 0)
                                        b_room = true;
                            }
                        }
                        if (b_room)
                        {
                            t2.lowLevelCount++;
                            b_t2 = true;
                        }
                    }
                    if (b_t2)
                    {
                        t3.lowLevelCount++;
                    }
                }
                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetRoomList
        /// <summary>
        /// GetRoomList
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool GetRoomList(string userid, ref StringBuilder outXml)
        {
            bool bRet = true;

            try
            {
                outXml = new StringBuilder();
                outXml.Append("<level3List>");
                List<vrmTier3> RoomList = new List<vrmTier3>();
                if (GetRoomTree(Int32.Parse(userid), ref RoomList))
                {
                    foreach (vrmTier3 t3 in RoomList)
                    {
                        if (t3.lowLevelCount > 0)
                        {
                            outXml.Append("<level3>");
                            outXml.Append("<level3ID>" + t3.ID.ToString() + "</level3ID>");
                            outXml.Append("<level3Name>" + t3.Name + "</level3Name> ");
                            outXml.Append("<level2List>");

                            if (t3.tier2.Count > 0)
                            {
                                foreach (vrmTier2 t2 in t3.tier2)
                                {
                                    if (t2.lowLevelCount > 0)
                                    {
                                        outXml.Append("<level2>");
                                        outXml.Append("<level2ID>" + t2.ID.ToString() + "</level2ID> ");
                                        outXml.Append("<level2Name>" + t2.Name + "</level2Name> ");
                                        outXml.Append("<level1List>");

                                        if (t2.room.Count > 0)
                                        {
                                            foreach (vrmRoom rm in t2.room)
                                            {
                                                if (rm.m_display && rm.disabled == 0) //FB 1830 //FB 2205-s( rm.b_display )
                                                {
                                                    outXml.Append("<level1>");
                                                    outXml.Append("<level1ID>" + rm.roomId.ToString() + "</level1ID>");
                                                    outXml.Append("<level1Name>" + rm.Name + "</level1Name>");
                                                    outXml.Append("<capacity>" + rm.Capacity.ToString() + "</capacity>");
                                                    outXml.Append("<disabled>" + rm.disabled + "</disabled>");   //Fb 1438
                                                    outXml.Append("<projector>" + rm.ProjectorAvailable.ToString() + "</projector>");
                                                    outXml.Append("<maxNumConcurrent>" +
                                                                               rm.MaxPhoneCall.ToString() + "</maxNumConcurrent>");
                                                    outXml.Append("<videoAvailable>" +
                                                                               rm.VideoAvailable.ToString() + "</videoAvailable>");
                                                    outXml.Append("<FlyRoom>" + rm.Extroom.ToString() + "</FlyRoom>"); //FB 2426
                                                    outXml.Append("<IsVMR>" + rm.IsVMR + "</IsVMR>"); //FB 2448
                                                    outXml.Append("</level1>");
                                                }
                                            }
                                        }
                                        outXml.Append("</level1List>");
                                        outXml.Append("</level2>");
                                    }
                                }
                            }
                            outXml.Append("</level2List>");
                            outXml.Append("</level3>");
                        }
                    }
                }

               outXml.Append("</level3List>");
                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetDepartmentList
        /// <summary>
        /// GetDepartmentList
        /// </summary>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool GetDepartmentList(ref StringBuilder outXml, int orgID)
        {
            try
            {

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));//Organization Module Fixes

                //IList deptList = m_IdeptDAO.GetActive();
                IList deptList = m_IdeptDAO.GetByCriteria(criterionList);
                outXml.Append(GetDeptListXml(deptList));

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetDepartmentList
        /// <summary>
        /// GetDepartmentList
        /// </summary>
        /// <param name="user"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool GetDepartmentList(vrmUser user, ref StringBuilder outXml)
        {
            try
            {
                IList deptList = GetUserDeptVsDepartmentList(user);
                outXml.Append(GetDeptListXml(deptList));
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }

        //FB 2639_GetOldConf Start
        public bool GetDepartmentList(vrmUser user, ref XmlWriter xWriteXml)
        {
            try
            {
                IList deptList = GetUserDeptVsDepartmentList(user);
                GetDeptListXml(deptList, ref xWriteXml);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        //FB 2639_GetOldConf End
        #endregion

        #region GetDeptListXml
        /// <summary>
        /// GetDeptListXml
        /// </summary>
        /// <param name="deptList"></param>
        /// <returns></returns>
        private StringBuilder GetDeptListXml(IList deptList)
        {
            StringBuilder outputXml = new StringBuilder();
            outputXml.Append("<departments>");

            foreach (vrmDept dept in deptList)
            {
                outputXml.Append("<department>");
                outputXml.Append("<id>" + dept.departmentId.ToString() + "</id>");
                outputXml.Append("<name>" + dept.departmentName + "</name>");
                outputXml.Append("<securityKey>" + dept.securityKey + "</securityKey>");
                outputXml.Append("</department>");
            }
           outputXml.Append("</departments>");
            return outputXml;
        }
        //FB 2639_GetOldConf Start
        private bool GetDeptListXml(IList deptList, ref XmlWriter xwriter)
        {
            vrmDept dept = null;

            xwriter.WriteStartElement("departments");
            for (int i = 0; i < deptList.Count; i++)
            {
                dept = (vrmDept)deptList[i];
                xwriter.WriteStartElement("department");
                xwriter.WriteElementString("id", dept.departmentId.ToString());
                xwriter.WriteElementString("name", dept.departmentName);
                xwriter.WriteElementString("securityKey", dept.securityKey);
                xwriter.WriteFullEndElement(); //ZD 100116
            }
            xwriter.WriteFullEndElement();//ZD 100116

            return true;
        }
        //FB 2639_GetOldConf End
        #endregion

        #region GetVideoSessionList
        /// <summary>
        /// GetVideoSessionList
        /// </summary>
        /// <param name="videoSession"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool GetVideoSessionList(int videoSession, ref StringBuilder outputXML)
        {
            outputXML = new StringBuilder();
            outputXML.Append("<videoSession>");
            try
            {

                IList videoSessionList = vrmGen.getVideoSession();

                if (videoSessionList.Count == 0)
                    return false;

                StringBuilder tempXML = new StringBuilder();
                foreach (vrmVideoSession vs in videoSessionList)
                {
                    if (videoSession == 0)
                    {
                        outputXML.Append("<session>");
                        outputXML.Append("<videoSessionID>" + vs.Id.ToString() + "</videoSessionID>");
                        outputXML.Append("<videoSessionName>" + vs.VideoSessionType + "</videoSessionName>");
                        outputXML.Append("</session>");
                    }
                    else
                    {
                        if (videoSession == vs.Id)
                        {
                            outputXML.Append("<session>");
                            outputXML.Append("<videoSessionID>" + vs.Id.ToString() + "</videoSessionID>");
                            outputXML.Append("<videoSessionName>" + vs.VideoSessionType + "</videoSessionName>");
                            outputXML.Append("</session>");
                        }
                        else
                        {
                            tempXML.Append("<osession>");
                            tempXML.Append("<videoSessionID>" + vs.Id.ToString() + "</videoSessionID>");
                            tempXML.Append("<videoSessionName>" + vs.VideoSessionType + "</videoSessionName>");
                            tempXML.Append("</osession>");
                        }
                    }

                }
                if (videoSession != 0)
                    outputXML.Append(tempXML);

                outputXML.Append("</videoSession>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetLineRateList
        /// <summary>
        /// GetLineRateList
        /// </summary>
        /// <param name="lineRate"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool GetLineRateList(int lineRate, ref StringBuilder outputXML)
        {
            outputXML = new StringBuilder();
            outputXML.Append("<lineRate>");
            try
            {

                IList lineRateList = vrmGen.getLineRate();

                if (lineRateList.Count == 0)
                    return false;

                StringBuilder tempXML = new StringBuilder();
                foreach (vrmLineRate lr in lineRateList)
                {
                    if (lineRate == 0)
                    {
                        outputXML.Append("<rate>");
                        outputXML.Append("<lineRateID>" + lr.Id.ToString() + "</lineRateID>");
                        outputXML.Append("<lineRateName>" + lr.LineRateType + "</lineRateName>");
                        outputXML.Append("</rate>");
                    }
                    else
                    {
                        if (lineRate == lr.Id)
                        {
                            outputXML.Append("<rate>");
                            outputXML.Append("<lineRateID>" + lr.Id.ToString() + "</lineRateID>");
                            outputXML.Append("<lineRateName>" + lr.LineRateType + "</lineRateName>");
                            outputXML.Append("</rate>");
                        }
                        else
                        {
                            tempXML.Append("<rate>");
                            tempXML.Append("<lineRateID>" + lr.Id.ToString() + "</lineRateID>");
                            tempXML.Append("<lineRateName>" + lr.LineRateType + "</lineRateName>");
                            tempXML.Append("</rate>");
                        }
                    }

                }
                if (lineRate != 0)
                    outputXML.Append(tempXML);

                outputXML.Append("</lineRate>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetVideoEquipment
        /// <summary>
        /// GetVideoEquipment
        /// </summary>
        /// <param name="videoEquipment"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool GetVideoEquipment(int videoEquipment, ref StringBuilder outputXML)
        {
            outputXML = new StringBuilder();
            outputXML.Append("<videoEquipment>");
            try
            {

                IList videoEquipmentList = vrmGen.getVideoEquipment();

                if (videoEquipmentList.Count == 0)
                    return false;

                if (videoEquipment > 0)
                    outputXML.Append("<selectedEquipment>" + videoEquipment.ToString() + "</selectedEquipment>");

                foreach (vrmVideoEquipment ve in videoEquipmentList)
                {
                    outputXML.Append("<equipment>");
                    outputXML.Append("<videoEquipmentID>" + ve.Id.ToString() + "</videoEquipmentID>");
                    outputXML.Append("<videoEquipmentName>" + ve.VEName + "</videoEquipmentName>");
                    outputXML.Append("</equipment>");
                }

                outputXML.Append("</videoEquipment>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetAudioList
        /// <summary>
        /// GetAudioList
        /// </summary>
        /// <param name="audio"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool GetAudioList(int audio, ref StringBuilder outputXML)
        {

            outputXML.Append("<audioAlgorithm>");
            try
            {

                IList AudioList = vrmGen.getAudioAlg();
                if (AudioList.Count == 0)
                    return false;

                StringBuilder tempXML = new StringBuilder();
                foreach (vrmAudioAlg al in AudioList)
                {
                    if (audio == 0)
                    {
                        outputXML.Append("<audio>");
                        outputXML.Append("<audioAlgorithmID>" + al.Id.ToString() + "</audioAlgorithmID>");
                        outputXML.Append("<audioAlgorithmName>" + al.AudioType + "</audioAlgorithmName>");
                        outputXML.Append("</audio>");
                    }
                    else
                    {
                        if (audio == al.Id)
                        {
                            outputXML.Append("<audio");
                            outputXML.Append("<audioAlgorithmID>" + al.Id.ToString() + "</audioAlgorithmID>");
                            outputXML.Append("<audioAlgorithmName>" + al.AudioType + "</audioAlgorithmName>");
                            outputXML.Append("</audio>");
                        }
                        else
                        {
                            tempXML.Append("<oaudio");
                            tempXML.Append("<audioAlgorithmID>" + al.Id.ToString() + "</audioAlgorithmID>");
                            tempXML.Append("<audioAlgorithmName>" + al.AudioType + "</audioAlgorithmName>");
                            tempXML.Append("</oaudio>");
                        }
                    }

                }
                if (audio != 0)
                    outputXML.Append(tempXML);

                outputXML.Append("</audioAlgorithm>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region ConfVideoProList
        /// <summary>
        /// ConfVideoProList
        /// </summary>
        /// <param name="videoProtocol"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool ConfVideoProList(int videoProtocol, ref StringBuilder outputXML)
        {

            outputXML.Append("<videoProtocol>");

            try
            {

                IList InterfaceTypeList = vrmGen.getInterfaceType();

                if (InterfaceTypeList.Count == 0)
                    return false;

                StringBuilder tempXML = new StringBuilder();
                foreach (vrmInterfaceType it in InterfaceTypeList)
                {
                    if (videoProtocol == 0)
                    {
                        outputXML.Append("<video>");
                        outputXML.Append("<videoProtocolID>" + it.Id.ToString() + "</videoProtocolID>");
                        outputXML.Append("<videoProtocolName>" + it.Interface + "</videoProtocolName>");
                        outputXML.Append("</video>");
                    }
                    else
                    {
                        if (videoProtocol == it.Id)
                        {
                            outputXML.Append("<video>"); //FB 2027 SetConference
                            outputXML.Append("<videoProtocolID>" + it.Id.ToString() + "</videoProtocolID>");
                            outputXML.Append("<videoProtocolName>" + it.Interface + "</videoProtocolName>");
                            outputXML.Append("</video>");
                        }
                        else
                        {
                            tempXML.Append("<ovideo>"); //FB 2027 SetConference
                            tempXML.Append("<videoProtocolID>" + it.Id.ToString() + "</videoProtocolID>");
                            tempXML.Append("<videoProtocolName>" + it.Interface + "</videoProtocolName>");
                            tempXML.Append("</ovideo>");
                        }
                    }

                }
                if (videoProtocol != 0)
                    outputXML.Append(tempXML);

                outputXML.Append("</videoProtocol>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetLocVsUsrDepartmentList
        /// <summary>
        /// GetLocVsUsrDepartmentList
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<vrmLocDepartment> GetLocVsUsrDepartmentList(vrmUser user) //FB 2027
        {
            ArrayList deptID = new ArrayList();

            if (user.isSuperAdmin())
                return m_IlocDeptDAO.GetActive();

            DetachedCriteria userDept = DetachedCriteria.For(typeof(vrmUserDepartment), "depts")
                                        .Add(Expression.Eq("userId", user.userid))
                                        .SetFetchMode("vrmLocDepartment", FetchMode.Eager);

            IList ld = m_IlocDeptDAO.Search(userDept);
            openDept = ld.Count;//FB 1672
            foreach (vrmUserDepartment ud in ld)
                deptID.Add(ud.departmentId);


            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.In("departmentId", deptID));

            return m_IlocDeptDAO.GetByCriteria(criterionList);

        }
        #endregion

        #region GetUserDeptVsDepartmentList
        /// <summary>
        /// GetUserDeptVsDepartmentList
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private IList GetUserDeptVsDepartmentList(vrmUser user)
        {
            try
            {
                if (user.isAdmin())
                    return m_IdeptDAO.GetActive();

                DetachedCriteria query = DetachedCriteria.For(typeof(vrmUserDepartment), "dept");
                query.SetProjection(Projections.Property("departmentId"));
                query.Add(Expression.Eq("userId", user.userid));
                query.Add(Property.ForName("userDept.departmentId")
                                  .EqProperty("dept.departmentId"));

                DetachedCriteria userDept = DetachedCriteria.For(typeof(vrmDept), "userDept");
                userDept.Add(Subqueries.Exists((query)));
                return m_IdeptDAO.Search(userDept);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetUserDepartmentList
        // this routine will return all userdept records with userid ....
        private List<vrmUserDepartment> GetUserDepartmentList(vrmUser user)
        {
            try
            {
                //if (user.isAdmin()) //FB 1499
                //    return m_IuserDeptDAO.GetAll(); //FB 1499

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userId", user.userid));//FB 1499

                return m_IuserDeptDAO.GetByCriteria(criterionList);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetDepartmentSelectList
        // returns a list of all userdepartments in department list
        private List<vrmUserDepartment> GetDepartmentSelectList(List<vrmUserDepartment> deptList)
        {
            try
            {

                List<int> deptID = new List<int>();

                foreach (vrmUserDepartment ud in deptList)
                    deptID.Add(ud.departmentId);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("departmentId", deptID));

                return m_IuserDeptDAO.GetByCriteria(criterionList);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        //Modified for FB 2027 - start(goc)
        #region GetPartyList
        /// <summary>
        /// GetPartyList
        /// </summary>
        /// <param name="Conf"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool GetPartyList(vrmConference Conf, ref StringBuilder outputXML)
        {
            try
            {
                string fname="",lname="",email="",level="",title="";
                vrmGuestUser Gust = null;
                vrmUser usr = null;
                List<vrmRoom> locRoom = new List<vrmRoom>();//FB 2101
                List<ICriterion> critlist = null;//FB 2101

                outputXML.Append("<partys>");
                for(int i=0; i < Conf.ConfUser.Count; i++)
                {
                    usr = m_IuserDAO.GetByUserId(Conf.ConfUser[i].userid);
                    if(usr == null)
                    {
                        Gust = m_IGuestUserDao.GetByUserId(Conf.ConfUser[i].userid);
                        fname = Gust.FirstName;
                        lname = Gust.LastName;
                        email = Gust.Email;
                        level = Gust.LevelID.ToString();
                        title = Gust.Title;
                    }
                    else
                    {
                        fname = usr.FirstName;
                        lname = usr.LastName;
                        email = usr.Email;
                        level = usr.LevelID.ToString();
                        title = usr.Title;
                    }

                    outputXML.Append("<party>");
                    outputXML.Append("<partyID>" + Conf.ConfUser[i].userid + "</partyID>");
                    outputXML.Append("<partyFirstName>" + fname + "</partyFirstName>");
                    outputXML.Append("<partyLastName>" + lname + "</partyLastName>");
                    outputXML.Append("<partyEmail>" + email + "</partyEmail>");
                    outputXML.Append("<partyInvite>" + Conf.ConfUser[i].invitee + "</partyInvite>");
                    outputXML.Append("<partyNotify>" + Conf.ConfUser[i].partyNotify + "</partyNotify>");
                    outputXML.Append("<partyLevel>" + level + "</partyLevel>");
                    outputXML.Append("<partyTitle>" + title + "</partyTitle>");
                    outputXML.Append("<partyAudVid>" + Conf.ConfUser[i].audioOrVideo + "</partyAudVid>");
                    outputXML.Append("<partyProtocol>" + Conf.ConfUser[i].interfaceType + "</partyProtocol>");
                    outputXML.Append("<partyAddress>" + Conf.ConfUser[i].ipisdnaddress + "</partyAddress>");
                    outputXML.Append("<partyAddressType>" + Conf.ConfUser[i].addressType + "</partyAddressType>");
                    outputXML.Append("<partyConnectionType>" + Conf.ConfUser[i].connectionType + "</partyConnectionType>");
                    outputXML.Append("<partyStatus>" + Conf.ConfUser[i].status + "</partyStatus>");
                    outputXML.Append("<partyIsOutside>" + Conf.ConfUser[i].outsideNetwork + "</partyIsOutside>");
                    outputXML.Append("<partyPublicVMR>"+ Conf.ConfUser[i].PublicVMRParty +"</partyPublicVMR>"); //FB 2550
                    //FB 2101 start
                    critlist = new List<ICriterion>();
                    critlist.Add(Expression.Eq("RoomID",Conf.ConfUser[i].roomId));
                    locRoom = m_IRoomDAO.GetByCriteria(critlist);
                    
                    if(locRoom.Count > 0)
                        outputXML.Append("<partyRoom>" + locRoom[0].Name + "</partyRoom>");
                    else
                        outputXML.Append("<partyRoom></partyRoom>");
                    outputXML.Append("<Survey>" + Conf.ConfUser[i].Survey + "</Survey>");//FB 2348
                    //FB 2101 end                    
                    outputXML.Append("</party>");
                }
                outputXML.Append("</partys>");
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        //FB 2639_GetOldConf Start
        public bool GetPartyList(vrmConference Conf, ref XmlWriter outputXML)
        {
            try
            {
                string fname = "", lname = "", email = "", level = "", title = "";
                vrmGuestUser Gust = null;
                vrmUser usr = null;
                List<vrmRoom> locRoom = new List<vrmRoom>();//FB 2101
                List<ICriterion> critlist = null;//FB 2101

                outputXML.WriteStartElement("partys");
                for (int i = 0; i < Conf.ConfUser.Count; i++)
                {
                    usr = m_IuserDAO.GetByUserId(Conf.ConfUser[i].userid);
                    if (usr == null)
                    {
                        Gust = m_IGuestUserDao.GetByUserId(Conf.ConfUser[i].userid);
                        fname = Gust.FirstName;
                        lname = Gust.LastName;
                        email = Gust.Email;
                        level = Gust.LevelID.ToString();
                        title = Gust.Title;
                    }
                    else
                    {
                        fname = usr.FirstName;
                        lname = usr.LastName;
                        email = usr.Email;
                        level = usr.LevelID.ToString();
                        title = usr.Title;
                    }

                    outputXML.WriteStartElement("party");
                    outputXML.WriteElementString("partyID", Conf.ConfUser[i].userid.ToString());
                    outputXML.WriteElementString("partyFirstName", fname);
                    outputXML.WriteElementString("partyLastName", lname);
                    outputXML.WriteElementString("partyEmail", email);
                    outputXML.WriteElementString("partyInvite", Conf.ConfUser[i].invitee.ToString());
                    outputXML.WriteElementString("partyNotify", Conf.ConfUser[i].partyNotify.ToString());
                    outputXML.WriteElementString("partyLevel", level);
                    outputXML.WriteElementString("partyTitle", title);
                    outputXML.WriteElementString("partyAudVid", Conf.ConfUser[i].audioOrVideo.ToString());
                    outputXML.WriteElementString("partyProtocol", Conf.ConfUser[i].interfaceType.ToString());
                    outputXML.WriteElementString("partyAddress", Conf.ConfUser[i].ipisdnaddress);
                    outputXML.WriteElementString("partyAddressType", Conf.ConfUser[i].addressType.ToString());
                    outputXML.WriteElementString("partyConnectionType", Conf.ConfUser[i].connectionType.ToString());
                    outputXML.WriteElementString("partyStatus", Conf.ConfUser[i].status.ToString());
                    outputXML.WriteElementString("partyIsOutside", Conf.ConfUser[i].outsideNetwork.ToString());
                    outputXML.WriteElementString("partyPublicVMR", Conf.ConfUser[i].PublicVMRParty.ToString()); //FB 2550
                    //FB 2101 start
                    critlist = new List<ICriterion>();
                    critlist.Add(Expression.Eq("RoomID", Conf.ConfUser[i].roomId));
                    locRoom = m_IRoomDAO.GetByCriteria(critlist);

                    if (locRoom.Count > 0)
                        outputXML.WriteElementString("partyRoom", locRoom[0].Name);
                    else
                        outputXML.WriteElementString("partyRoom", "");
                    outputXML.WriteElementString("Survey", Conf.ConfUser[i].Survey.ToString());//FB 2348
                    //FB 2101 end                    
                    outputXML.WriteFullEndElement(); //ZD 100116
                }
                outputXML.WriteFullEndElement(); //ZD 100116
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        //FB 2639_GetOldConf End
        #endregion

        #region GetPartyList
        /// <summary>
        /// GetPartyList
        /// </summary>
        /// <param name="Temp"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool GetPartyList(vrmTemplate Temp, ref StringBuilder outputXML)
        {
            try
            {
                List<ICriterion> criteria = new List<ICriterion>();
                List<vrmTempUser> tempUsrs = new List<vrmTempUser>();
                string fname = "", lname = "", email = "", level = "", title = "", isoutsite = "";
                vrmGuestUser Gust = null;
                vrmUser usr = null;
                
                criteria.Add(Expression.Eq("TmpID", Temp.tmpId));
                tempUsrs = m_TempUserDAO.GetByCriteria(criteria);

                outputXML.Append("<partys>");
                for (int i = 0; i < tempUsrs.Count; i++)
                {
                    usr = m_IuserDAO.GetByUserId(tempUsrs[i].userid);
                    if (usr == null)
                    {
                        Gust = m_IGuestUserDao.GetByUserId(tempUsrs[i].userid);
                        fname = Gust.FirstName;
                        lname = Gust.LastName;
                        email = Gust.Email;
                        level = Gust.LevelID.ToString();
                        title = Gust.Title;
                        isoutsite = Gust.outsidenetwork.ToString();
                    }
                    else
                    {
                        fname = usr.FirstName;
                        lname = usr.LastName;
                        email = usr.Email;
                        level = usr.LevelID.ToString();
                        title = usr.Title;
                        isoutsite = usr.outsidenetwork.ToString();
                    }
                    outputXML.Append("<party>");
                    outputXML.Append("<partyID>" + tempUsrs[i].userid + "</partyID>");
                    outputXML.Append("<partyFirstName>" + fname + "</partyFirstName>");
                    outputXML.Append("<partyLastName>" + lname + "</partyLastName>");
                    outputXML.Append("<partyEmail>" + email + "</partyEmail>");
                    outputXML.Append("<partyInvite>" + tempUsrs[i].invitee.ToString() + "</partyInvite>");
                    outputXML.Append("<partyNotify>" + tempUsrs[i].partyNotify.ToString() + "</partyNotify>");
                    outputXML.Append("<partyLevel>" + level + "</partyLevel>");
                    outputXML.Append("<partyTitle>" + title + "</partyTitle>");
                    outputXML.Append("<partyProtocol>" + tempUsrs[i].defVideoProtocol.ToString() + "</partyProtocol>"); //FB 3041
                    outputXML.Append("<partyAddress>" + tempUsrs[i].IPISDNAddress + "</partyAddress>");
                    outputXML.Append("<partyAddressType>" + tempUsrs[i].AddressType + "</partyAddressType>");//FB 3041
                    outputXML.Append("<partyConnectionType>" + tempUsrs[i].connectionType + "</partyConnectionType>");
                    outputXML.Append("<partyStatus>" + tempUsrs[i].status + "</partyStatus>");
                    outputXML.Append("<partyAudVid>" + tempUsrs[i].audioOrVideo + "</partyAudVid>");
                    outputXML.Append("<partyIsOutside>" + isoutsite + "</partyIsOutside>");
                    outputXML.Append("<Survey>" + tempUsrs[i].Survey.ToString() + "</Survey>");//FB 2348
                    outputXML.Append("<partyPublicVMR>0</partyPublicVMR>");//FB 2550
                    outputXML.Append("</party>");
                }
                outputXML.Append("</partys>");
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        //FB 2639_GetOldConf Start
        public bool GetPartyList(vrmTemplate Temp, ref XmlWriter outputXML)
        {
            try
            {
                List<ICriterion> criteria = new List<ICriterion>();
                List<vrmTempUser> tempUsrs = new List<vrmTempUser>();
                string fname = "", lname = "", email = "", level = "", title = "", isoutsite = "";
                vrmGuestUser Gust = null;
                vrmUser usr = null;

                criteria.Add(Expression.Eq("TmpID", Temp.tmpId));
                tempUsrs = m_TempUserDAO.GetByCriteria(criteria);

                outputXML.WriteStartElement("partys");
                for (int i = 0; i < tempUsrs.Count; i++)
                {
                    usr = m_IuserDAO.GetByUserId(tempUsrs[i].userid);
                    if (usr == null)
                    {
                        Gust = m_IGuestUserDao.GetByUserId(tempUsrs[i].userid);
                        fname = Gust.FirstName;
                        lname = Gust.LastName;
                        email = Gust.Email;
                        level = Gust.LevelID.ToString();
                        title = Gust.Title;
                        isoutsite = Gust.outsidenetwork.ToString();
                    }
                    else
                    {
                        fname = usr.FirstName;
                        lname = usr.LastName;
                        email = usr.Email;
                        level = usr.LevelID.ToString();
                        title = usr.Title;
                        isoutsite = usr.outsidenetwork.ToString();
                    }
                    outputXML.WriteStartElement("party");
                    outputXML.WriteElementString("partyID", tempUsrs[i].userid.ToString());
                    outputXML.WriteElementString("partyFirstName", fname);
                    outputXML.WriteElementString("partyLastName", lname);
                    outputXML.WriteElementString("partyEmail", email);
                    outputXML.WriteElementString("partyInvite", tempUsrs[i].invitee.ToString());
                    outputXML.WriteElementString("partyNotify", tempUsrs[i].partyNotify.ToString());
                    outputXML.WriteElementString("partyLevel", level);
                    outputXML.WriteElementString("partyTitle", title);
                    outputXML.WriteElementString("partyProtocol", tempUsrs[i].defVideoProtocol.ToString()); //FB 3041
                    outputXML.WriteElementString("partyAddress", tempUsrs[i].IPISDNAddress.ToString());
                    outputXML.WriteElementString("partyAddressType", tempUsrs[i].AddressType.ToString()); //FB 3041
                    outputXML.WriteElementString("partyConnectionType", tempUsrs[i].connectionType.ToString());
                    outputXML.WriteElementString("partyStatus", tempUsrs[i].status.ToString());
                    outputXML.WriteElementString("partyAudVid", tempUsrs[i].audioOrVideo.ToString());
                    outputXML.WriteElementString("partyIsOutside", isoutsite);
                    outputXML.WriteElementString("Survey", tempUsrs[i].Survey.ToString());//FB 2348
                    outputXML.WriteElementString("partyPublicVMR", "0");//FB 2550
                    outputXML.WriteFullEndElement(); //ZD 100116
                }
                outputXML.WriteFullEndElement(); //ZD 100116
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        //FB 2639_GetOldConf End
        #endregion

        //Modified for FB 2027 - End(goc)

        #region getAddressType
        /// <summary>
        /// getAddressType
        /// </summary>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool getAddressType(ref StringBuilder outputXML)
        {

            outputXML.Append("<addressType>");

            try
            {

                IList AddressTypeList = vrmGen.getAddressType();

                if (AddressTypeList.Count == 0)
                    return false;

                foreach (vrmAddressType at in AddressTypeList)
                {
                    outputXML.Append("<type>");
                    outputXML.Append("<ID>" + at.Id.ToString() + "</ID>");
                    outputXML.Append("<name>" + at.name + "</name>");
                    outputXML.Append("</type>");
                }
                outputXML.Append("</addressType>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetDepartmentsForLocation
        /// <summary>
        /// GetDepartmentsForLocation
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetDepartmentsForLocation(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                StringBuilder OutXML = null;
                XmlNode node;

                node = xd.SelectSingleNode("//GetDepartmentsForLocation/UserID");
                string userID = node.InnerXml.Trim();
                vrmUser objUser = m_IuserDAO.GetByUserId(Int32.Parse(userID));

                node = xd.SelectSingleNode("//GetDepartmentsForLocation/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                if (objUser.isSuperAdmin())//FB 1251
                {
                    OutXML = new StringBuilder();
                    OutXML.Append("<GetDepartmentsForLocation>");
                    List<ICriterion> deptCriterionList = new List<ICriterion>();
                    deptCriterionList.Add(Expression.Eq("deleted", 0));  //FB 1445
                    deptCriterionList.Add(Expression.Eq("orgId", organizationID.ToString()));  //Code added for organization

                    //
                    // if multi departments is enabled apply roles based security. 
                    // if not then anyone can see all depts 
                    //

                    List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                    List<int> deptIn = new List<int>();
                    if (multiDepts == 1 && objUser.Admin != vrmUserConstant.SUPER_ADMIN) //Organization Module Fixes
                    {

                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("userId", objUser.userid));
                        deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                        foreach (vrmUserDepartment dept in deptList)
                        {
                            deptIn.Add(dept.departmentId);
                        }
                        deptCriterionList.Add(Expression.In("departmentId", deptIn));
                    }
                    List<vrmDept> selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);

                    OutXML.Append("<multiDepartment>" + multiDepts.ToString() + "</multiDepartment>");   //Organization Module Fixes
                    OutXML.Append("<departments>");

                    foreach (vrmDept dept in selectedDept)
                    {
                        OutXML.Append("<department>");
                        OutXML.Append("<id>" + dept.departmentId.ToString() + "</id>");
                        OutXML.Append("<name>" + dept.departmentName + "</name>");
                        OutXML.Append("<securityKey>" + dept.securityKey + "</securityKey>");
                        OutXML.Append("</department>");
                    }
                    OutXML.Append("</departments>");

                    OutXML.Append("</GetDepartmentsForLocation>");

                    obj.outXml = OutXML.ToString();
                    return true;
                }//FB 1251
                else//FB 1251
                {
                    OutXML = new StringBuilder();
                    OutXML.Append("<GetDepartmentsForLocation>");
                    OutXML.Append("<multiDepartment>");
                    OutXML.Append(multiDepts.ToString()); //Organization Module Fixes
                    OutXML.Append("</multiDepartment>");
                    if (multiDepts > 0) //Organization Module Fixes
                    {
                        if (!GetDepartmentList(objUser, ref OutXML))
                            throw new Exception("Exception occured in getting the Department list");
                    }
                    OutXML.Append("</GetDepartmentsForLocation>");
                    obj.outXml = OutXML.ToString();
                    return true;
                }//FB 1251
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException in GetDepartmentsForLocation", ex);
                return false;
            }
        }
        #endregion

        #region Commented Region
        /*
         * 
        CString CCommonFetchs::FetchACGroups1(CString confid, CString userid,int mode,int &err)
       {
           CString outputXML;
           SACommand cmd;
           SAString stmt;
           SQLQueryExecutor SEx(m_pDBConn);
           ErrorHandle globalErrHandle(m_pDBConn);

	
           CString groupid,groupname,numParticipants;
           //---- Select all AGroups in that conference 
           if(mode==1)
           {
           stmt = "Select g.groupid,gd.name from Conf_Group_D g,Grp_Detail_D gd \
               where g.CC = 0 and g.groupid = gd.groupid And g.ConfID = ";
           stmt += confid;
           }else
           {
           stmt = "Select g.groupid,gd.name from Tmp_Group_D g,Grp_Detail_D gd \
               where g.CC = 0 and g.groupid = gd.groupid And g.tmpid = ";
           stmt += confid;
           }
           //			outputXML+= stmt;
           try
           {
               SEx.GenericCommand((CString) __FILE__, (int) __LINE__, cmd, (CString) stmt);
               outputXML+="<defaultAGroups>";
               while (cmd.FetchNext() )
               {	
                   groupid =cmd.Field(1).asString();
                   groupname	=cmd.Field(2).asString();
			
                   outputXML+="<groupID>";
                   outputXML+= groupid; 
                   outputXML+="</groupID>";
			
                   outputXML+="<groupName>";
                   outputXML+=groupname;
                   outputXML+="</groupName>";
			
                   numParticipants=FetchNumGroupParticipants(groupid);
                   outputXML+="<numParticipants>"+numParticipants+"</numParticipants>";
                   outputXML+=FetchGroupUsers(groupid);
			
               }	
               outputXML+="</defaultAGroups>";
               if(mode==1)
               {
               stmt = "Select g.groupid,gd.name from Conf_Group_D g,Grp_Detail_D gd \
                   where g.CC = 1 and g.groupid = gd.groupid And g.ConfID = ";
               stmt += confid;
               }else
               {
               stmt = "Select g.groupid,gd.name from Tmp_Group_D g,Grp_Detail_D gd \
                   where g.CC = 1 and g.groupid = gd.groupid And g.tmpid = ";
               stmt += confid;
               }

               SEx.GenericCommand((CString) __FILE__, (int) __LINE__, cmd, (CString) stmt);
               outputXML+="<defaultCGroups>";			
               while (cmd.FetchNext() )
               {	
                   groupid =cmd.Field(1).asString();
                   groupname=cmd.Field(2).asString();
			
                   outputXML+="<groupID>"+groupid+"</groupID>";
                   outputXML+="<groupName>"+groupname+"</groupName>";
			
                   numParticipants=FetchNumGroupParticipants(groupid);
                   outputXML+="<numParticipants>"+numParticipants+"</numParticipants>";
                   outputXML+=FetchGroupUsers(groupid);
			
               }
               outputXML+="</defaultCGroups>";
		
               if(mode==1)
               {
                   stmt = " Select gd.groupid,gd.name from  Grp_Detail_D gd ";
                   stmt+= " where gd.groupid not in (select groupid from Conf_Group_D where confid="+confid+") ";
                   stmt += " and (gd.private=0 or gd.owner=" + userid + ")";
               }else
               {
                   stmt = "Select gd.groupid,gd.name from Grp_Detail_D gd ";
                   stmt +=	"where gd.groupid not in (select groupid from Tmp_Group_D where tmpid = "+ confid+")";
                   stmt += " and (gd.private=0 or gd.owner=" + userid + ")";
               }
 
               SEx.GenericCommand((CString) __FILE__, (int) __LINE__, cmd, (CString) stmt);
               outputXML+="<groups>";
               while(cmd.FetchNext())
               {
                   groupid =cmd.Field(1).asString();
                   groupname=cmd.Field(2).asString();
                   outputXML+="<group>";			
                   outputXML+="<groupID>"+groupid+"</groupID>";
                   outputXML+="<groupName>"+groupname+"</groupName>";
			
                   numParticipants=FetchNumGroupParticipants(groupid);
                   outputXML+="<numParticipants>"+numParticipants+"</numParticipants>";
                   outputXML+=FetchGroupUsers(groupid);
                   outputXML+="</group>";
               }
               outputXML+="</groups>";
           }
           catch(SAException &e) {
               outputXML = globalErrHandle.FetchErrorMessage(305);
               globalErrHandle.WriteMessage(0, 305, (CString)e.ErrText()+"comUser::ModifyUser(CString &inputXML)");
               goto quit;
           } ;
       quit:
           return outputXML;
       }

         * 
         
        private IList GetUserDeptVsDepartmentList(vrmUser user)
        {
            try
            {
                if (user.isAdmin())
                    return m_IdeptDAO.GetActive();

                DetachedCriteria query = DetachedCriteria.For(typeof(vrmUserDepartment), "dept");
                query.SetProjection(Projections.Property("departmentId"));
                query.Add(Expression.Eq("userId", user.userid));
                query.Add(Property.ForName("userDept.departmentId")
                                  .EqProperty("dept.departmentId"));

                DetachedCriteria userDept = DetachedCriteria.For(typeof(vrmDept), "userDept");
                userDept.Add(Subqueries.Exists((query)));
                return m_IdeptDAO.Search(userDept);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        
        // this routine will return all userdept records with userid ....
        private List<vrmUserDepartment> GetUserDepartmentList(vrmUser user)
        {
            try
            {
                if (user.isAdmin())
                    return m_IuserDeptDAO.GetAll();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userid", user.userid));

                return m_IuserDeptDAO.GetByCriteria(criterionList);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        
        // returns a list of all userdepartments in department list
        private List<vrmUserDepartment> GetDepartmentSelectList(List<vrmUserDepartment> deptList)
        {
            try
            {

                List<int> deptID = new List<int>();

                foreach (vrmUserDepartment ud in deptList)
                    deptID.Add(ud.departmentId);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("departmentId", deptID));

                return m_IuserDeptDAO.GetByCriteria(criterionList);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        
        public bool GetPartyList(vrmConference Conf, ref string outputXML)
        {
            outputXML += "<partys>";
            try
            {
                foreach (vrmConfUser cUser in Conf.ConfUser)
                {
                    //outputxml += "<party>";
                    //outputxml += "<partyid>" + cuser.userid.tostring() + "</partyid>";
                    //outputxml += "<partyfirstname>" + cuser.user.firstname + "</partyfirstname>";
                    //outputxml += "<partylastname>" + cuser.user.lastname + "</partylastname>";
                    //outputxml += "<partyemail>" + cuser.user.email + "</partyemail>";
                    //outputxml += "<partyinvite>" + cuser.invitee.tostring() + "</partyinvite>";
                    //outputxml += "<partynotify>" + cuser.partynotify.tostring() + "</partynotify>";
                    //outputxml += "<partylevel>" + cuser.user.levelid.tostring() + "</partylevel>";
                    //outputxml += "<partytitle>" + cuser.user.title + "</partytitle>";
                    //outputxml += "<partyprotocol>" + cuser.interfacetype.tostring() + "</partyprotocol>";
                    //outputxml += "<partyaddress>" + cuser.ipisdnaddress + "</partyaddress>";
                    //outputxml += "<partyaddresstype>" + cuser.addresstype.tostring() + "</partyaddresstype>";
                    //outputxml += "<partyconnectiontype>" + cuser.connectiontype + "</partyconnectiontype>";

                    outputXML += "<partyStatus>" + cUser.status + "</partyStatus>";


                    outputXML += "<partyAudVid>" + cUser.connectionType + "</partyAudVid>";
                    outputXML += "<partyIsOutside>" + cUser.outsideNetwork.ToString() + "</partyIsOutside>";

                    outputXML += "</party>";
                }
                outputXML += "</partys>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
         * 
        public bool GetPartyList(vrmTemplate Temp, ref string outputXML)
        {
            outputXML += "<partys>";
            try
            {
                foreach (vrmTempUser cUser in Temp.TempUser)
                {
                    outputXML += "<party>";
                    outputXML += "<partyID>" + cUser.userid.ToString() + "</partyID>";
                    outputXML += "<partyFirstName>" + cUser.User.FirstName + "</partyFirstName>";
                    outputXML += "<partyLastName>" + cUser.User.LastName + "</partyLastName>";
                    outputXML += "<partyEmail>" + cUser.User.Email + "</partyEmail>";
                    outputXML += "<partyInvite>" + cUser.invitee.ToString() + "</partyInvite>";
                    outputXML += "<partyNotify>" + cUser.partyNotify.ToString() + "</partyNotify>";
                    outputXML += "<partyLevel>" + cUser.User.LevelID.ToString() + "</partyLevel>";
                    outputXML += "<partyTitle>" + cUser.User.Title + "</partyTitle>";
                    outputXML += "<partyProtocol>" + cUser.interfaceType.ToString() + "</partyProtocol>";
                    outputXML += "<partyAddress>" + cUser.IPISDNAddress + "</partyAddress>";
                    outputXML += "<partyAddressType></partyAddressType>";
                    outputXML += "<partyConnectionType>" + cUser.connectionType + "</partyConnectionType>";

                    outputXML += "<partyStatus>" + cUser.status + "</partyStatus>";


                    outputXML += "<partyAudVid></partyAudVid>";
                    outputXML += "<partyIsOutside></partyIsOutside>";

                    outputXML += "</party>";
                }
                outputXML += "</partys>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
         public bool getAddressType(ref string outputXML)
        {

            outputXML += "<addressType>";

            try
            {

                IList AddressTypeList = vrmGen.getAddressType();

                if (AddressTypeList.Count == 0)
                    return false;

                foreach (vrmAddressType at in AddressTypeList)
                {
                    outputXML += "<type>";
                    outputXML += "<ID>" + at.Id.ToString() + "</ID>";
                    outputXML += "<name>" + at.name + "</name>";
                    outputXML += "</type>";
                }
                outputXML += "</addressType>";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }*/
        #endregion

        #region SearchConference
        /// <summary>
        
        /// This command is going to be used for all the List page options also with the SearchConfernce page.
        /// It is also used to get the instances of the conferences.
        /// 
        /// Commands included is 
        /// 1. SearchConference
        /// 2. GetApproveConference
        /// 3. GetApproveInstances
        /// 4. GetInstances
        /// 
        /// Screens options
        /// 1. Reservations
        /// 2. Pending
        /// 3. Approval Pending
        /// 4. SearchConference
        /// 5. View public Conference
        /// 
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool SearchConference(ref vrmDataObject obj)
        {
            Boolean IsSuperAdmin = false;
            String confActualStatus = "";//Added for Dashboard
            StringBuilder searchOutXml = new StringBuilder(); //String concatenation changed to StringBuilder for Performance - FB 1787
            List<ICriterion> ICrietrionconfbridge = new List<ICriterion>();//FB 2870
            List<int> confnumnamelist = new List<int>(); //FB 2870
            try
            {
                DateTime systemDatetimeUpdate = DateTime.Now;// FB 1848
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref systemDatetimeUpdate);//FB 1848
                //Hashtable CustomSearch = new Hashtable(); //FB 2894
                //int customCnt = 0; //FB 2894
                int optionType = 0;//FB 2607

                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SearchConference/UserID");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/organizationID"); //Organization Module Fixes
                string orgid = "";
                
                if (node != null)
                    orgid = node.InnerXml.Trim();
                
                if (orgid != "")//Hack to get all Conferences for service
                {
                    organizationID = defaultOrgId;
                    int.TryParse(orgid, out organizationID);

                    if (organizationID < 11)
                    {
                        myVRMException myVRMEx = new myVRMException(423);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    if (orgInfo != null)
                        multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes
                }
                
                node = xd.SelectSingleNode("//SearchConference/ConferenceID");
                string conferenceID = ""; int ConfID = 0;
                if (node != null)
                {
                    conferenceID = node.InnerXml.Trim();
                    int.TryParse(node.InnerXml.Trim(),out ConfID);
                }

                node = xd.SelectSingleNode("//SearchConference/ConferenceName");
                string ConferenceName = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceUniqueID");
                string ConferenceUniqueID = node.InnerXml.Trim();

                //FB 1942 Starts TIK# 100037
                int isDeletedConf = 0; ;
                node = xd.SelectSingleNode("//SearchConference/SearchDeletedConf"); //FB 2870
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out isDeletedConf);
                    if (isDeletedConf > 0)
                        isDeleted = true;
                }
                //FB 1942 Ends
                 
                string CTSNumericID=""; //FB 3001
                node = xd.SelectSingleNode("//SearchConference/CTSNumericID"); //FB 2870
                if (node != null)
                    CTSNumericID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceSearchType");
                string ConferenceSearchType = node.InnerXml.Trim();
                if (ConferenceSearchType.Length == 0)
                    ConferenceSearchType = "-1";
                int iSearchType = 0;
                int.TryParse(ConferenceSearchType, out iSearchType);

                XmlNodeList statusList = xd.GetElementsByTagName("ConferenceStatus");
                List<int> confStatus = new List<int>();
                foreach (XmlNode innerNode in statusList)
                {
                    int iStatus = 0;
                    int.TryParse(innerNode.InnerXml.Trim(), out iStatus);
                    confStatus.Add(iStatus);
                    if ((iStatus == vrmConfStatus.Pending || iStatus == vrmConfStatus.Scheduled) &&
                        iSearchType < vrmSearchType.Past)
                    {
                        iSearchType = vrmSearchType.Future;
                    }
                }
                node = xd.SelectSingleNode("//SearchConference/ApprovalPending");
                string approvalPending = "";

                isApprovalPending = false;    //FB 1158

                if (node != null)
                    approvalPending = node.InnerXml.Trim();
                if (approvalPending.Length > 0)
                {
                    if (approvalPending == "1")
                        isApprovalPending = true;
                }

                node = xd.SelectSingleNode("//SearchConference/DateFrom");
                string DateFrom = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/DateTo");
                string DateTo = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceHost");
                string ConferenceHost = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceParticipant");
                string ConferenceParticipant = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/Public");
                string Public = node.InnerXml.Trim();
                if (Public.Length == 0)
                    Public = vrmPublicType.Both.ToString();

                node = xd.SelectSingleNode("//SearchConference/RecurrenceStyle");
                int RecurrentStyle = 1;
                if (node != null)
                    RecurrentStyle = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//SearchConference/Location/SelectionType");
                string SelectionType = node.InnerXml.Trim();

                List<int> selectedRooms = new List<int>();
                if (SelectionType == "2")
                {
                    XmlNodeList itemList;

                    itemList = xd.GetElementsByTagName("Selected");
                    foreach (XmlNode innerNode in itemList)
                    {
                        selectedRooms.Add(Int32.Parse(innerNode.InnerXml.Trim()));
                    }
                }
                node = xd.SelectSingleNode("//SearchConference/PageNo");
                string PageNo = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/SortBy");
                string SortBy = node.InnerXml.Trim();
                if (SortBy.Length == 0)
                    SortBy = "0";

                //FB 2822
				string SortingOrder = "0";
                if (xd.SelectSingleNode("//SearchConference/SortingOrder") != null)
                {
                    node = xd.SelectSingleNode("//SearchConference/SortingOrder");
                    SortingOrder = node.InnerXml.Trim();
                }
                
                //FB 2014
                int utcEnabled = 0;
                if (xd.SelectSingleNode("//SearchConference/utcEnabled") != null)
                    int.TryParse(xd.SelectSingleNode("//SearchConference/utcEnabled").InnerText.Trim(), out utcEnabled);

                //FB 2632 Starts
                String strConcSpp = ""; //FB 2670
                int AVSupport = 0, MeetGrt = 0, CongMonitr = 0, DedicatedVNOC = 0;
                node = xd.SelectSingleNode("//SearchConference/ConciergeSupport/OnSiteAVSupport");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out AVSupport); strConcSpp = AVSupport == 1 ? strConcSpp + "O," : strConcSpp; //FB 2670

                node = xd.SelectSingleNode("//SearchConference/ConciergeSupport/MeetandGreet");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MeetGrt); strConcSpp = MeetGrt == 1 ? strConcSpp + "M," : strConcSpp; //FB 2670

                node = xd.SelectSingleNode("//SearchConference/ConciergeSupport/ConciergeMonitoring");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out CongMonitr); strConcSpp = CongMonitr == 1 ? strConcSpp + "C," : strConcSpp; //FB 2670

                node = xd.SelectSingleNode("//SearchConference/ConciergeSupport/DedicatedVNOCOperator");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out DedicatedVNOC); strConcSpp = DedicatedVNOC == 1 ? strConcSpp + "D," : strConcSpp; //FB 2670
                //FB 2670
                String concSuppCondtion = "0";
                node = xd.SelectSingleNode("//SearchConference/ConciergeSupport/ConciergeCondtion");
                if (node != null)
                    concSuppCondtion = node.InnerText.Trim();
                
                //FB 2670
                if (strConcSpp.ToString() != "")
                    strConcSpp = strConcSpp.Substring(0, strConcSpp.Length - 1);
                //FB 2501 Starts
                List<int> confVNOC =  new List<int>();
                int ConfVNOCID = 0;
                XmlNodeList VNOCNodes;
                string vnocid = "";
                if (xd.SelectNodes("//SearchConference/ConciergeSupport/ConfVNOCOperators/VNOCOperatorID") != null)
                {
                    VNOCNodes = xd.SelectNodes("//SearchConference/ConciergeSupport/ConfVNOCOperators/VNOCOperatorID");

                    if (VNOCNodes.Count > 0)
                    {
                        for (int i = 0; i < VNOCNodes.Count; i++)
                        {
                            int.TryParse(VNOCNodes[i].InnerText.Trim(), out ConfVNOCID);
                            if (!confVNOC.Contains(ConfVNOCID))
                                confVNOC.Add(ConfVNOCID);

                            if (VNOCNodes[i] == VNOCNodes[0])
                                vnocid = VNOCNodes[i].InnerText.Trim();
                            else
                                vnocid += "," + VNOCNodes[i].InnerText.Trim();
                        }
                    }
                }
                //if (DedicatedVNOC > 0 && confVNOC < 11)
                //{
                //    myVRMException myvrmEx  = new myVRMException(634);
                //    obj.outXml = myvrmEx.FetchErrorMsg();
                //    return false;
                //}
                //if (!CheckApproverRights(confVNOC.ToString()))
                //{
                //    myVRMException myvrmEx = new myVRMException(630);
                //    obj.outXml = myvrmEx.FetchErrorMsg();
                //    return false;
                //}

                //vrmUser vnocOperator = m_IuserDAO.GetByUserId(confVNOC); //FB 2501 VNOC

                //FB 2632 End
 				//FB 2729 Starts
                int VNOCStatus = 0;
                node = xd.SelectSingleNode("//SearchConference/ConciergeSupport/ConfVNOCStatus");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out VNOCStatus);
                //FB 2729 Ends

                //FB 2694 - Start
                String hLastName = "";
                String hFirstName = "";
                String hEmail = "";
                String hotDesking = "";

                node = xd.SelectSingleNode("//SearchConference/LastName");
                if (node != null)
                    hLastName = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/FirstName");
                if (node != null)
                    hFirstName = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/Email");
                if (node != null)
                    hEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/SearchType");
                if (node != null)
                    hotDesking = node.InnerXml.Trim();

                //FB 2694 - End

                #region Custom Attributes
                //Code added for Custom Attributes -- Start
                List<int> customAttIDs = new List<int>();
                if (xd.SelectSingleNode("//SearchConference/CustomAttributesList") != null)
                {
                    node = xd.SelectSingleNode("//SearchConference/CustomAttributesList");

                    XmlNodeList customList = null;

                    if (xd.SelectNodes("//SearchConference/CustomAttributesList/CustomAttribute") != null)
                        customList = xd.SelectNodes("//SearchConference/CustomAttributesList/CustomAttribute");

                    List<vrmConfAttribute> customAttlist = null;
                    //isCustomAttrAvailable = false;  //FB 2894
                    if (customList != null)
                    {
                        foreach (XmlNode innerNode in customList)
                        {
                            isCustomAttrAvailable = true;//FB 2894
                            int.TryParse(innerNode.SelectSingleNode("Type").InnerText.Trim(), out optionType);
                            switch (optionType)
                            {
                                case vrmCustomOptions.DropDownList:
                                case vrmCustomOptions.ListBox:
                                case vrmCustomOptions.RadioButtonList: //FB 1718
                                    {
                                        int customAttrID = 0;
                                        int optionID = 0 ;
                                        //isCustomAttrAvailable = true;   //FB 2894

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                int.TryParse(innerNode.SelectSingleNode("CustomAttributeID").InnerText, out customAttrID);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionID").InnerText != "")
                                            {
                                                int.TryParse(innerNode.SelectSingleNode("OptionID").InnerText, out optionID);
                                            }
                                        }

                                        List<ICriterion> cusCriterionList = new List<ICriterion>();

                                        cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                        cusCriterionList.Add(Expression.Eq("SelectedOptionId", optionID));

                                        customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                        //if (optionID > 0)    //FB 2894
                                        //    customCnt++;

                                        foreach (vrmConfAttribute att in customAttlist)
                                        {
                                            //FB 2894 Starts
                                            if (customAttIDs != null)
                                            {
                                                if (!customAttIDs.Contains(att.ConfId))
                                                    customAttIDs.Add(att.ConfId);
                                            }
                                            else
                                            {
                                                customAttIDs = new List<int>(); //FB 2607
                                                customAttIDs.Add(att.ConfId);
                                            }
                                            //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                            //    CustomSearch.Add(att.ConfId, 1);
                                            //else
                                            //{
                                            //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                            //}
                                            //FB 2894 End
                                        }

                                        break;
                                    }

                                case vrmCustomOptions.TextBox:
                                case vrmCustomOptions.URLTextBox: //FB 1612
                                case vrmCustomOptions.MultiLine: //FB 1718
                                    {
                                        int customAttrID = 0;
                                        string selectedValue = "";
                                        //isCustomAttrAvailable = true;   //FB 2894

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                int.TryParse(innerNode.SelectSingleNode("CustomAttributeID").InnerText, out customAttrID);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionValue") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionValue").InnerText != "")
                                            {
                                                selectedValue = innerNode.SelectSingleNode("OptionValue").InnerText;
                                            }
                                        }

                                        List<ICriterion> cusCriterionList = new List<ICriterion>();

                                        cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                        cusCriterionList.Add(Expression.Like("SelectedValue", "%%" + selectedValue + "%%").IgnoreCase());

                                        customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                        //if (selectedValue != "")    //FB 2894
                                        //    customCnt++;

                                        foreach (vrmConfAttribute att in customAttlist)
                                        {
                                            // FB 2607 Starts
                                            if (customAttIDs != null)
                                            {
                                                if (!customAttIDs.Contains(att.ConfId))
                                                    customAttIDs.Add(att.ConfId);
                                            }
                                            else
                                            {
                                                customAttIDs = new List<int>(); // FB 2607
                                                customAttIDs.Add(att.ConfId);
                                            }
                                            //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                            //    CustomSearch.Add(att.ConfId, 1);
                                            //else
                                            //{
                                            //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                            //}
                                            // FB 2607 End
                                        }

                                        break;
                                    }

                                case vrmCustomOptions.CheckBox://FB 2377
                                    {
                                        int customAttrID = 0; //FB 2607
                                        int optionValue = 0;

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                int.TryParse(innerNode.SelectSingleNode("CustomAttributeID").InnerText, out customAttrID);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionValue") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionValue").InnerText != "")
                                            {
                                                int.TryParse(innerNode.SelectSingleNode("OptionValue").InnerText.Trim(), out optionValue);
                                            }
                                        }

                                        if (optionValue > 0) //FB 2607
                                        {
                                            //customCnt++;
                                            //isCustomAttrAvailable = true;   //FB 2894


                                            List<ICriterion> cusCriterionList = new List<ICriterion>();

                                            cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                            cusCriterionList.Add(Expression.Eq("SelectedValue", "1")); //FB 2607 Checked values alone

                                            customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                            foreach (vrmConfAttribute att in customAttlist)
                                            {
                                                if (customAttIDs != null)
                                                {
                                                    if (!customAttIDs.Contains(att.ConfId)) // FB 2607
                                                        customAttIDs.Add(att.ConfId);
                                                }
                                                else
                                                {
                                                    customAttIDs = new List<int>(); // FB 2607
                                                    customAttIDs.Add(att.ConfId);
                                                }
                                                //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                                //    CustomSearch.Add(att.ConfId, 1);
                                                //else
                                                //{
                                                //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                                //}
                                            }
                                        }

                                        break;
                                    }
                                case vrmCustomOptions.RadioButton:
                                    {
                                        int customAttrID = 0;
                                        string optionValue = "";

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                int.TryParse(innerNode.SelectSingleNode("CustomAttributeID").InnerText,out customAttrID);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionValue") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionValue").InnerText != "")
                                            {
                                                optionValue = innerNode.SelectSingleNode("OptionValue").InnerText;
                                            }
                                        }

                                        List<ICriterion> cusCriterionList = new List<ICriterion>();

                                        //if (optionValue == "1")    //FB 2894
                                        //{
                                        //    customCnt++;
                                        //    isCustomAttrAvailable = true;   //FB 2607
                                        //}

                                        cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                        cusCriterionList.Add(Expression.Eq("SelectedValue", optionValue));

                                        customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                        foreach (vrmConfAttribute att in customAttlist)
                                        {
                                            if (customAttIDs != null)
                                            {
                                                if (!customAttIDs.Contains(att.ConfId))
                                                    customAttIDs.Add(att.ConfId);
                                            }
                                            else
                                            {
                                                customAttIDs = new List<int>(); //FB 2607
                                                customAttIDs.Add(att.ConfId);
                                            }
                                            //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                            //    CustomSearch.Add(att.ConfId, 1);
                                            //else
                                            //{
                                            //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                            //}
                                        }

                                        break;
                                    }
                            }
                        }
                        //FB 2894 Starts

                        //foreach (DictionaryEntry DE in CustomSearch)
                        //{
                        //    if (customCnt == Convert.ToInt32(DE.Value))
                        //    {
                        //        if (!customAttIDs.Contains(Convert.ToInt32(DE.Key)))
                        //            customAttIDs.Add(Convert.ToInt32(DE.Key));
                        //    }
                        //}

                        //FB 2894 Ends
                    }
                }
                //Code added for Custom Attributes -- End
                #endregion

                List<ICriterion> userCriterion = new List<ICriterion>();
                userCriterion.Add(Expression.Eq("userid", Int32.Parse(userId)));
                List<vrmUser> userList = m_IuserDAO.GetByCriteria(userCriterion);
                vrmUser user = userList[0];

                timeZone.GMTToUserPreferedTime(user.TimeZone, ref systemDatetimeUpdate);//FB 1848

                //Added to find the requested user is Superadmin 
                //If he is a superadmin change the boolean true
                if (user.isSuperAdmin())
                    IsSuperAdmin = true;

                int ttlPages = 0;
                long ttlRecords = 0;
                int BConfsCt = -1; //FB 1920

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmConference> confList = new List<vrmConference>();

                if (orgid != "")//Hack to get all Conferences for service
                    criterionList.Add(Expression.Eq("orgId", organizationID));//Code added for organization

                if (ConferenceUniqueID.Trim().Length > 0 || CTSNumericID.Trim().Length > 0) //FB 2870
                {
                    //FB 2870 Start
                    confnumnamelist = new List<int>();
                    // user criteria to trap bad id
                    if (ConferenceUniqueID.Trim().Length > 0)
                    {
                        criterionList.Add(Expression.Eq("confnumname", Int32.Parse(ConferenceUniqueID)));
                        confnumnamelist.Add(Int32.Parse(ConferenceUniqueID));
                    }
                    else if (CTSNumericID.Trim().Length > 0)
                    {
                        ICrietrionconfbridge = new List<ICriterion>();

                        ICrietrionconfbridge.Add(Expression.Eq("E164Dialnumber", CTSNumericID.Trim()));
                        List<vrmConfBridge> confbridgelist = m_IconfBridge.GetByCriteria(ICrietrionconfbridge, true);
                        for (int b = 0; b < confbridgelist.Count; b++)
                        {
                            confnumnamelist.Add(confbridgelist[b].confuId);
                        }
                        criterionList.Add(Expression.In("confnumname", confnumnamelist));
                    }
                    //FB 2870 End
                    // FB 408 do not show deleted conferences 
                    if (!isDeleted) //FB 1942 TIK# 100037
                        criterionList.Add(Expression.Eq("deleted", 0));
                    // FB 366 have to apply security to this search too!
                    ICriterion deptCriterium = null;

                    if (multiDepts == 1)    //Organization Module
                    {
                        if (getDeptLevelCriterion(user, ref deptCriterium, confnumnamelist)) //FB 1787 //FB 2870
                        {
                            if (deptCriterium != null)//FB 1322,1387
                                criterionList.Add(deptCriterium);
                        }
                    }
                    //FB 1787 - following block is moved to getDeptLevelCriterion as the same is lost during FB 1499
                    /* *** FB 1158 - Approval Issues ... start *** */
                    //if (confsList != null)
                    //{
                    //    if (confsList.Count > 0)
                    //        criterionList.Add(Expression.In("confid", confsList));
                    //}
                    /* *** FB 1158 - Approval Issues ... end *** */

                    confList = m_IconfDAO.GetByCriteria(criterionList);

                    if (confList.Count == 0) //FB 2870
                    {
                        //FB 1033 Start
                        //obj.outXml = myVRMException.toXml("Error no conference with that id found", 314);
                        obj.outXml = "<SearchConference><Conferences></Conferences></SearchConference>"; //Changed during FB 1787
                        //FB 1033 End

                        return false;
                    }
                    if (!CheckConfAuthorization(confList[0], user))
                    {
                        confList.Clear();
                        ttlPages = 1;
                        ttlRecords = 1;
                    }
                    else
                    {
                        ttlPages = 1;
                        ttlRecords = 1;
                    }
                }
                else
                {
                    //Added for the Merging all commands. 
                    //If the conference id is passed then it is for GetInstances which is from the List page not for search page
                    if (conferenceID.Trim().Length > 0)
                    {
                        criterionList.Add(Expression.Eq("confid", ConfID));
                    }
                    ICriterion criterium = null;
                    ICriterion criterium2 = null;//FB 2501
                    ICriterion criterium3 = null;//FB 2501
                    if (confStatus.Count > 0)
                    {
                        if (confStatus.Count == 1)
                            criterionList.Add(Expression.Eq("status", confStatus[0]));
                        else
                            criterionList.Add(Expression.In("status", confStatus));
                    }

                    DateTime confFrom = new DateTime();
                    DateTime confEnd = new DateTime();

                    if (DateFrom.Length > 0)
                        confFrom = DateTime.Parse(DateFrom);
                    if (DateTo.Length > 0)
                        confEnd = DateTime.Parse(DateTo);
                    // ge today

                    int a_userId = sysSettings.TimeZone;
                    if (iSearchType == vrmSearchType.Cutom)
                        a_userId = user.TimeZone;

                    if (iSearchType == vrmSearchType.Past || iSearchType == vrmSearchType.Future)
                    {
                        confFrom = DateTime.Now;
                        timeZone.changeToGMTTime(a_userId, ref confFrom);
                        if (iSearchType == vrmSearchType.Past)
                            criterium = Expression.Le("confEnd", confFrom);
                        else
                            criterium = Expression.Ge("confdate", confFrom);
                        criterionList.Add(criterium);

                    }
                    else if (iSearchType == 9) //FB 2501 VNOC
                    {
                        confFrom = DateTime.Now;
                        timeZone.changeToGMTTime(a_userId, ref confFrom);
                        criterium3 = Expression.Ge("confdate", confFrom);
                       // criterionList.Add(criterium);
                        if (getSearchDateRange(1, ref confEnd, ref confFrom, 0))//FB 2595
                        {
                            timeZone.changeToGMTTime(a_userId, ref confFrom);
                            timeZone.changeToGMTTime(a_userId, ref confEnd);
                            criterium = Expression.Le("confdate", confEnd);
                            criterium2 = Expression.And(criterium, Expression.Ge("confEnd", confFrom));
                        }

                        criterionList.Add(Expression.Or(criterium3, criterium2));
                    }
                    else
                    {
                        if (getSearchDateRange(iSearchType, ref confEnd, ref confFrom, 0))//FB 2595
                        {
                            timeZone.changeToGMTTime(a_userId, ref confFrom);
                            timeZone.changeToGMTTime(a_userId, ref confEnd);
                            criterium = Expression.Le("confdate", confEnd);
                            criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                        }
                    }
                    criterium = null; //FB 1499
                    if (multiDepts == 1 && iSearchType != 9) //Organization Module FB 2501 VNOC
                    {
                        if (getDeptLevelCriterion(user, ref criterium, confnumnamelist)) //FB 1787 //FB 2870
                        {
                            if (criterium != null)//FB 1499 Error
                                criterionList.Add(criterium);
                        }
                    }

                    //Below block is moved to getDeptLevelCriterion Method as the same is lost during FB 1499
                    /* *** FB 1158 - Approval pending confs are not displayed in the general user - start *** */
                    //if (!IsSuperAdmin) //FB 1787
                    //{
                    //    if (isApprovalPending)
                    //        GetConfsListForApproval(user.userid.ToString());

                    //    if (confsList != null)
                    //    {
                    //        if (confsList.Count > 0)
                    //            criterionList.Add(Expression.In("confid", confsList));
                    //    }
                    //}
                    /* *** FB 1158 - Approval pending confs are not displayed in the general user - end *** */

                    // I cant think of any other way to do this.
                    // you have to get the conf's for this search citerea and select only thos confid's that
                    // match (this also applies to the participant tags....)
                    Hashtable confId = new Hashtable();
                    List<int> confUniqueId = new List<int>();
                    List<vrmUser> selectedUser = new List<vrmUser>();

                    if (ConferenceParticipant.Length > 0)
                    {
                        List<ICriterion> userCriterionList = new List<ICriterion>();
                        ICriterion uc = Expression.Like("LastName", "%%" + ConferenceParticipant + "%%").IgnoreCase();
                        uc = Expression.Or(uc, Expression.Like("FirstName", "%%" + ConferenceParticipant + "%%").IgnoreCase());
                        userCriterionList.Add(uc);
                        selectedUser = m_IuserDAO.GetByCriteria(userCriterionList);
                    }
                    if (ConferenceHost.Length > 0)
                    {
                        List<ICriterion> userCriterionList = new List<ICriterion>();
                        List<ICriterion> hostCriterionList = new List<ICriterion>();
                        ICriterion uc;
                        //
                        // FB 11,653 if there is a space then it is firstname lastname
                        //                    
                        int i = ConferenceHost.IndexOf(" ");
                        if (i > 0)
                        {
                            string firstName = ConferenceHost.Substring(0, i);
                            firstName = firstName.Trim();
                            string lastName = ConferenceHost.Substring(i, ConferenceHost.Length - i);
                            lastName = lastName.Trim();
                            if (lastName.Length > 0)
                            {
                                uc = Expression.Like("LastName", "%%" + lastName + "%%").IgnoreCase();
                                uc = Expression.Or(uc, Expression.Like("FirstName", "%%" + firstName + "%%").IgnoreCase());
                            }
                            else
                            {
                                uc = Expression.Like("FirstName", "%%" + firstName + "%%").IgnoreCase();
                            }
                        }
                        else
                        {
                            uc = Expression.Like("LastName", "%%" + ConferenceHost + "%%").IgnoreCase(); // FB 1952
                            uc = Expression.Or(uc, Expression.Like("FirstName", "%%" + ConferenceHost + "%%").IgnoreCase()); // FB 1952
                        }
                        hostCriterionList.Add(uc); // FB 1952
                        List<vrmUser> selectedHost = m_IuserDAO.GetByCriteria(hostCriterionList);

                        List<int> hostList = new List<int>();
                        foreach (vrmUser host in selectedHost)
                            hostList.Add(host.userid);

                        criterionList.Add(Expression.In("owner", hostList));
                    }
                    //FB 2694
                    if (hFirstName != "" || hLastName != "" || hEmail != "")
                    {
                        Int32 c = 0;
                        ICriterion hc = null;
                        List<ICriterion> hostCriterionList = new List<ICriterion>();

                        if (hLastName != "")
                        {
                            hc = Expression.Like("LastName", "%%" + hLastName + "%%").IgnoreCase();
                            c = c + 1;
                        }

                        if (hFirstName != "")
                        {
                            if (c > 0)
                                hc = Expression.Or(hc, Expression.Like("FirstName", "%%" + hFirstName + "%%").IgnoreCase());
                            else
                            {
                                hc = Expression.Like("FirstName", "%%" + hFirstName + "%%").IgnoreCase(); 
                                c = c + 1;
                            }
                        }

                        if (hEmail != "")
                        {
                            if (c > 0)
                                hc = Expression.Or(hc, Expression.Like("Email", "%%" + hEmail + "%%").IgnoreCase());
                            else
                                hc = Expression.Like("Email", "%%" + hEmail + "%%").IgnoreCase();
                        }

                        hostCriterionList.Add(hc);
                        List<vrmUser> selectedHost = m_IuserDAO.GetByCriteria(hostCriterionList);

                        List<int> hostList = new List<int>();
                        foreach (vrmUser host in selectedHost)
                            hostList.Add(host.userid);

                        criterionList.Add(Expression.In("owner", hostList));
                    }

                    int isPublic = 0;
                    int.TryParse(Public, out isPublic);
                    if (isPublic == vrmPublicType.Public || isPublic == vrmPublicType.Private)
                    {
                        criterionList.Add(Expression.Eq("isPublic", isPublic));
                    }
                    //FB 2321 Starts
                    string ConfName = ConferenceName.ToLower(); 
                    if (ConfName.Contains("["))
                        ConfName = "%%" + ConfName.Replace("[", "\"[") + "%%"; 
                    else
                        ConfName = "%%" + ConfName + "%%";
                    if (ConferenceName.Length > 0)
                        criterionList.Add(Expression.Like("externalname", ConfName, MatchMode.Anywhere, '\"'));
                        //criterionList.Add(Expression.Like("externalname", "%%" + ConferenceName.ToLower() + "%%").IgnoreCase());
                    //FB 2321 Ends

                    if (!isDeleted) //FB 1942 TIK# 100037
                        criterionList.Add(Expression.Eq("deleted", 0));
                    
                    //FB 2670
                    if (strConcSpp != "" && concSuppCondtion == "1")
                    {
                        Hashtable ht = new Hashtable();
                        ht.Add("O", "OnSiteAVSupport");
                        ht.Add("M", "MeetandGreet");
                        ht.Add("C", "ConciergeMonitoring");
                        ht.Add("D", "DedicatedVNOCOperator");

                        String[] strConcAry = strConcSpp.Split(',');
                        String Expr = "";
                        for (int c = 0; c < strConcAry.Length; c++)
                        {
                            if (Expr == "")
                                Expr = "(" + ht[strConcAry[c]].ToString() + " = 1";
                            else
                                Expr += " or " + ht[strConcAry[c]].ToString() + " = 1";
                        }

                        if (Expr != "")
                            criterionList.Add(Expression.Sql(Expr + ")"));
                    }
                    else
                    {
                        //FB 2632 - Starts
                        if (AVSupport == 1)
                            criterionList.Add(Expression.Eq("OnSiteAVSupport", AVSupport));
                        if (MeetGrt == 1)
                            criterionList.Add(Expression.Eq("MeetandGreet", MeetGrt));
                        if (CongMonitr == 1)
                            criterionList.Add(Expression.Eq("ConciergeMonitoring", CongMonitr));
                        //if (DedicatedVNOC == 1)
                        //    criterionList.Add(Expression.Eq("DedicatedVNOCOperator", DedicatedVNOC));
                        //FB 2632 - End
                    }
                    if (VNOCStatus == 1) //FB 2729 Starts
                    {
                        if (confVNOC.Count > 0) //FB 2670
                        {
                            IList result = null;
                            List<int> confsList = new List<int>();

                            string stmt = "SELECT confid FROM myVRM.DataLayer.vrmConfVNOCOperator WHERE vnocId In ( " + vnocid + ") and instanceid = 1 ";

                            result = m_ConfVNOCOperatorDAO.execQuery(stmt);
                            if (result.Count > 0)
                                confsList = result.OfType<int>().ToList();

                            criterionList.Add(Expression.In("confid", confsList));
                        }
                            criterionList.Add(Expression.Eq("DedicatedVNOCOperator", 1));
                    }
                    else if (VNOCStatus == 2)
                        criterionList.Add(Expression.Eq("DedicatedVNOCOperator", 0));
                    else
                    {
                        if (confVNOC.Count > 0) 
                        {
                            IList result = null;
                            List<int> confsList = new List<int>();

                            string stmt = "SELECT confid FROM myVRM.DataLayer.vrmConfVNOCOperator WHERE vnocId In ( " + vnocid + ") and instanceid = 1 ";

                            result = m_ConfVNOCOperatorDAO.execQuery(stmt);
                            if (result.Count > 0)
                                confsList = result.OfType<int>().ToList();

                            criterionList.Add(Expression.In("confid", confsList));
                        }
                    }
                    //FB 2729 Starts

                    //FB 2894 Starts
                    if (isCustomAttrAvailable)
                    {
                        if (customAttIDs.Count > 0)
                        {
                            criterionList.Add(Restrictions.In("confid", customAttIDs)); // FB 2607
                            confList = m_IconfDAO.GetByCriteria(criterionList);
                        }
                        else
                            confList = new List<vrmConference>();
                    }
                    else
                        confList = m_IconfDAO.GetByCriteria(criterionList);

                    //FB 2894 End

                    //Changed foreach to for loop during FB 1787 for performance issue
                    //foreach (vrmConference conf in confList) 
                    vrmConference conf = null;
                    for(int lp=0; lp < confList.Count; lp++)
                    {
                        conf = confList[lp];

                        // all searches must meet one of three conditions here. 
                        // all rooms (everything)
                        // No rooms (confernces with NO rooms)
                        // selected rooms (conferences that have rooms in the selected list)
                        switch (Int32.Parse(SelectionType))
                        {
                            case 0: // no rooms 
                                if (conf.ConfRoom.Count == 0)
                                {
                                    if (CheckConfAuthorization(conf, user))
                                        // two differnce styles of display. The first (0) displays 
                                        // each instance. The second displays recurring as a group. 
                                        if (RecurrentStyle == 0)
                                            confId.Add(conf.confnumname, conf.confnumname);
                                        else
                                            if (!confId.ContainsKey(conf.confid))
                                            {
                                                confId.Add(conf.confid, conf.confnumname);
                                                break;
                                            }
                                }
                                break;
                            case 2: // selected rooms
                                if (selectedRooms.Count > 0)
                                {
                                    if (!confId.ContainsKey(conf.confid))
                                    {
                                        foreach (vrmConfRoom rm in conf.ConfRoom)
                                        {
                                            for (int idx = 0; idx < selectedRooms.Count; idx++)
                                            {
                                                if (selectedRooms[idx] == rm.roomId)
                                                {
                                                    if (CheckConfAuthorization(conf, user))
                                                    {
                                                        if (RecurrentStyle == 0)
                                                        {
                                                            if (!confId.ContainsKey(conf.confnumname))
                                                                confId.Add(conf.confnumname, conf.confnumname);
                                                        }
                                                        else
                                                        {
                                                            if (!confId.ContainsKey(conf.confid))
                                                            {
                                                                confId.Add(conf.confid, conf.confnumname);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            case 3:
                                //FB 2501 VNOC Start
                                #region VNOC Operator
                                if (CheckConfVNOCAuthorization(conf, user))
                                    if (RecurrentStyle == 0)
                                    {
                                        if (!confId.ContainsKey(conf.confnumname))
                                            confId.Add(conf.confnumname, conf.confnumname);
                                    }
                                    else
                                    {
                                        if (!confId.ContainsKey(conf.confid))
                                        {
                                            confId.Add(conf.confid, conf.confnumname);
                                            break;
                                        }
                                    }
                                break;
                                #endregion
                                //FB 2501 VNOC End
                            default: // all rooms (all confs)
                                //FB 2694
                                if (hotDesking == "H")
                                {
                                    foreach (vrmConfRoom rm in conf.ConfRoom)
                                    {
                                        Boolean isConti = false;

                                        if (hotDesking == "H")
                                        {
                                            if (rm.Room.RoomCategory == 4)
                                                isConti = true;
                                        }
                                        else
                                            isConti = true;

                                        if (isConti)
                                        {
                                            if (CheckConfAuthorization(conf, user))
                                            {
                                                if (RecurrentStyle == 0)
                                                {
                                                    if (!confId.ContainsKey(conf.confnumname))
                                                        confId.Add(conf.confnumname, conf.confnumname);
                                                }
                                                else
                                                {
                                                    if (!confId.ContainsKey(conf.confid))
                                                    {
                                                        confId.Add(conf.confid, conf.confnumname);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (CheckConfAuthorization(conf, user))
                                        if (RecurrentStyle == 0)
                                        {
                                            if (!confId.ContainsKey(conf.confnumname))
                                                confId.Add(conf.confnumname, conf.confnumname);
                                        }
                                        else
                                        {
                                            if (!confId.ContainsKey(conf.confid))
                                            {
                                                confId.Add(conf.confid, conf.confnumname);
                                                break;
                                            }
                                        }
                                }
                                break;
                        }
                        if (selectedUser.Count > 0)
                        {
                            bool bFound = false;
                            if (confId.ContainsKey(conf.confid))
                            {
                                foreach (vrmConfUser us in conf.ConfUser)
                                {
                                    for (int idx = 0; idx < selectedUser.Count; idx++)
                                    {
                                        if (selectedUser[idx].userid == us.userid)
                                        {
                                            bFound = true;
                                            break;
                                        }
                                    }
                                    if (bFound)
                                        break;
                                }
                                if (!bFound)
                                    confId.Remove(conf.confid);
                            }
                        }
                    }
                    IDictionaryEnumerator iEnum = confId.GetEnumerator();
                    while (iEnum.MoveNext())
                    {
                        confUniqueId.Add((int)iEnum.Value);
                    }

                    if ((confUniqueId.Count == 0) && !(isApprovalPending)) //Providea
                        confUniqueId.Add(0);

                    if (confUniqueId.Count < 2100)
                    {
                        if (confUniqueId.Count > 0 || SelectionType == "2") //FB 1612
                            criterionList.Add(Expression.In("confnumname", confUniqueId));
                    }


                    m_IconfDAO.pageSize(m_iMaxRecords);
                    int iPageNo = Int32.Parse(PageNo);
                    if (iPageNo == 0)
                        iPageNo = 1;

                    m_IconfDAO.pageNo(iPageNo);

                    ttlRecords = m_IconfDAO.CountByCriteria(criterionList);

                    /*
                    Search Fixes - FB 1787 - start
                    if (ConferenceUniqueID.Trim().Length <= 0)
                    {
                        if (isCustomAttrAvailable)
                        {
                            ttlRecords = customAttIDs.Count;
                        }
                    }

                    ttlPages = (int)(ttlRecords / m_iMaxRecords);
                    */
                    // and modulo remainder...
                    /*
                    if (ttlRecords % m_iMaxRecords > 0)
                        ttlPages++;
                    */
                    //Search Fixes - FB 1787 - end
                    //FB 2822 - Start
                    // add sort order (if any)
                    switch (Int32.Parse(SortBy))
                    {
                        case vrmSortBy.UniqueID:
                            if(SortingOrder == "0")
                                m_IconfDAO.addOrderBy(Order.Asc("confnumname"));
                            else 
                                m_IconfDAO.addOrderBy(Order.Desc("confnumname"));
                            break;
                        case vrmSortBy.ConfName:
                            if (SortingOrder == "0")
                                m_IconfDAO.addOrderBy(Order.Asc("externalname"));
                            else
                                m_IconfDAO.addOrderBy(Order.Desc("externalname"));
                            break;
                        case vrmSortBy.ConfDate:
                            if (SortingOrder == "0")
                                m_IconfDAO.addOrderBy(Order.Asc("confdate"));
                            else
                                m_IconfDAO.addOrderBy(Order.Desc("confdate"));
                            break;
                        case vrmSortBy.StartMode:
                            if (SortingOrder == "0")
                                m_IconfDAO.addOrderBy(Order.Asc("StartMode"));//FB 2501
                            else
                                m_IconfDAO.addOrderBy(Order.Desc("StartMode"));
                            break;
                        case vrmSortBy.SiloName:
                            if (SortingOrder == "0")
                                m_IconfDAO.addOrderBy(Order.Asc("externalname"));
                            else
                                m_IconfDAO.addOrderBy(Order.Desc("externalname"));
                            break; 
						//FB 2822 - End                   
                    }
                    confList = m_IconfDAO.GetByCriteria(criterionList);

                    //FB 1920 start
                    if (isApprovalPending == true && multiDepts == 1 && organizationID == defaultOrgId)
                    {
                        criterium = null;
                        if (getPublicMCUConfs(user, ref criterium))
                        {
                            List<ICriterion> criteria = new List<ICriterion>();
                            List<vrmConference> PublicMCUConfs = new List<vrmConference>();
                            confId = new Hashtable();
                            confUniqueId = new List<int>();

                            if (criterium != null)
                            {
                                criteria.Add(criterium);
                                criteria.Add(criterionList[1]); //Confid - If click show all link( Recur ) in list page ( or ) status of conf 
                                criteria.Add(criterionList[2]);
                                PublicMCUConfs = m_IconfDAO.GetByCriteria(criteria);

                                if (PublicMCUConfs != null)
                                {
                                    if (PublicMCUConfs.Count > 0)
                                    {
                                        if (conferenceID.Trim().Length <= 0)
                                        {
                                            for (int i = 0; i < PublicMCUConfs.Count; i++)
                                            {
                                                if (!confId.ContainsKey(PublicMCUConfs[i].confid))
                                                    confId.Add(PublicMCUConfs[i].confid, PublicMCUConfs[i].confnumname);
                                            }
                                            iEnum = confId.GetEnumerator();
                                            while (iEnum.MoveNext())
                                            {
                                                confUniqueId.Add((int)iEnum.Value);
                                            }

                                            criteria = new List<ICriterion>();
                                            if (confUniqueId.Count > 0)
                                                criteria.Add(Expression.In("confnumname", confUniqueId));

                                            PublicMCUConfs = m_IconfDAO.GetByCriteria(criteria);
                                        }
                                        BConfsCt = confList.Count;
                                        confList.InsertRange(confList.Count, PublicMCUConfs);
                                    }
                                }
                            }
                        }
                    }
                    //FB 1920 end
                }
                List<ICriterion> PublicVMRcriterionList = null; //FB 2550
                //String concatenation is done using StringBuilder during FB 1787
                searchOutXml.Append("<SearchConference>");

                searchOutXml.Append("<Conferences>");
                int cc = 0; //FB 1787
                int p2pCount = 0;//FB 2437
                //foreach (vrmConference conf in confList)
                for(int i=0; i < confList.Count; i++)
                {
                    vrmConference conf = confList[i];
                    DateTime confDateTime = conf.confdate;
                    DateTime todayNow = DateTime.Now; //FB 2437

                    if (utcEnabled == 0)//FB 2014
                        timeZone.userPreferedTime(user.TimeZone, ref confDateTime);

                    //FB 2437 - Starts
                    /*
                    if (conf.conftype == vrmConfType.P2P && sysSettings.EnableLaunchBufferP2P == 0 && confStatus.Contains(vrmConfStatus.Ongoing)) 
                    {
                        if (confDateTime > todayNow)
                        {
                            p2pCount++;
                            continue;
                        }
                    }*/
                    //FB 2437 - End
                    
                    // FB 2894 Starts
                    //Custom Attribute Fix -- Start
                    /*if (ConferenceUniqueID.Trim().Length <= 0)
                    {
                        if (isCustomAttrAvailable)
                        {
                            if (customAttIDs != null)
                            {
                                if (customAttIDs.Count > 0)
                                {
                                    if (!customAttIDs.Contains(conf.confid))
                                        continue;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }*/
                    //Custom Attribute Fix -- End
                    // FB 2894 End 

                    cc++;   //FB 1787

                    int isHost = 0;
                    int isParticipant = 0;

                    if (user.userid == conf.owner)
                        isHost = 1;
                    foreach (vrmConfUser confUser in conf.ConfUser)
                    {
                        if (confUser.userid == user.userid)
                        {
                            isParticipant = 1;
                            break;
                        }
                    }
                    confActualStatus = conf.status.ToString(); //Added for Dashboard
                    // FB 390 AG check search type NOT selection type 
                    switch (Int32.Parse(ConferenceSearchType))
                    {
                        case vrmSearchType.Ongoing:
                            conf.status = vrmConfStatus.Ongoing;
                            break;
                        case vrmSearchType.Yesterday:
                            conf.status = vrmConfStatus.Completed;
                            break;
                        case vrmSearchType.Tomorrow:
                            break;
                        default:
                            // check date/duration and see if it is past
                            DateTime testStart = conf.confdate;
                            timeZone.userPreferedTime(sysSettings.TimeZone, ref testStart);
                            DateTime testEnd = testStart.AddMinutes(conf.duration);
                            //FB 2694
                            DateTime currDate = DateTime.Now;
                            timeZone.changeToGMTTime(conf.timezone, ref currDate);
                            timeZone.userPreferedTime(conf.timezone, ref currDate);
                            if (currDate >= testStart && currDate < testEnd)
                                conf.status = vrmConfStatus.Ongoing;
                            else if (testEnd < DateTime.Now)
                                conf.status = vrmConfStatus.Completed;
                            
                            break;
                    }
                    //FB 1179
                    //FB 2274 Starts
                    vrmOrganization vrmOrg = m_IOrgDAO.GetById(conf.orgId);
                    if (vrmOrg == null)
                    {
                        obj.outXml = "<error>Invalid Organization ID</error>";
                        return false;
                    }
                    //FB 2274 Ends
                    //FB 2550 - For MultiSilo
                    if (orgInfo == null)
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(conf.orgId);

                    StringBuilder levelXML = new StringBuilder();
                    bool isApprover = false;
                    if (isApprovalPending && conf.status == vrmConfStatus.Pending)
                        getConfApprovInfo(conf, IsSuperAdmin, userId, ref levelXML);

                    if (IsSuperAdmin && isApprovalPending == false) //FB 1920
                        isApprover = true;
                    else if (levelXML.ToString().Contains("<Entity>"))
                        isApprover = true;

                    if (isApprover || (!isApprovalPending || conf.status != vrmConfStatus.Pending))
                    {
                        if (isApprovalPending && BConfsCt != -1 && i >= BConfsCt) //FB 1920
                            conf.externalname = conf.externalname.ToString() + "{P}";

                        //FB 1179 
                        searchOutXml.Append("<Conference>");
                        searchOutXml.Append("<organizationName>" + vrmOrg.orgname.ToString() + "</organizationName>");//FB 2274
                        searchOutXml.Append("<ConferenceID>" + conf.confid.ToString() + "," +
                                conf.instanceid.ToString() + "</ConferenceID>");
                        searchOutXml.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                        searchOutXml.Append("<ConferenceName>" + conf.externalname.ToString() + "</ConferenceName>");
                        searchOutXml.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                        searchOutXml.Append("<ConferenceDateTime>" + confDateTime.ToString("g") + "</ConferenceDateTime>");
                        searchOutXml.Append("<ConferenceDuration>" + conf.duration.ToString() + "</ConferenceDuration>");
                        searchOutXml.Append("<isVMR>" + conf.isVMR + "</isVMR>");//FB 2448
                        searchOutXml.Append("<VMRType>" + orgInfo.EnableVMR.ToString() + "</VMRType>");//FB 2550
                        searchOutXml.Append("<PublicVMRCount>" + orgInfo.MaxPublicVMRParty.ToString() + "</PublicVMRCount>");//FB 2550
                        searchOutXml.Append("<StartMode>" + conf.StartMode.ToString() + "</StartMode>");//FB 2501
                        // if this is a recurring conference check the first instance if it is past then set the 
                        // status to 7 
                        vrmConference cf = conf;
                        if (conf.recuring > 0)
                        {
                            if (conf.instanceid > 1) //Diagnostics
                            {
                                m_IconfDAO = m_confDAO.GetConferenceDao();//FB 1211
                                cf = m_IconfDAO.GetByConfId(conf.confid, conf.instanceid);
                            }
                            DateTime tempTime = cf.confdate;
                            // timeZone.userPreferedTime(user.TimeZone, ref tempTime);
                            DateTime checkTime = DateTime.Now;
                            // timeZone.changeToGMTTime(sysSettings.TimeZone, ref checkTime);
                            // timeZone.userPreferedTime(user.TimeZone, ref checkTime);
                            //Condition added if we pass the conferenceid then it is for GetInstances
                            if ((tempTime <= checkTime) && (conferenceID.Trim().Length < 1) && (!isApprovalPending))
                                conf.status = 7;
                        }
                        searchOutXml.Append("<ConferenceStatus>" + conf.status.ToString() + "</ConferenceStatus>");
                        searchOutXml.Append("<ConferenceActualStatus>" + confActualStatus + "</ConferenceActualStatus>"); //Added for Dashboard
                        searchOutXml.Append("<OpenForRegistration>" + conf.dynamicinvite.ToString() + "</OpenForRegistration>");
                        //FB 2550 Starts
                        PublicVMRcriterionList = new List<ICriterion>();
                        PublicVMRcriterionList.Add(Expression.Eq("confid", conf.confid));
                        PublicVMRcriterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                        PublicVMRcriterionList.Add(Expression.Eq("PublicVMRParty", 1));
                        List<vrmConfUser> uList = m_IconfUser.GetByCriteria(PublicVMRcriterionList);
                        int remVMRParty = 0;
                        remVMRParty = orgInfo.MaxPublicVMRParty - uList.Count;
                        if (remVMRParty < 0)
                            remVMRParty = 0;
                        searchOutXml.Append("<RemPublicVMRCount>" + remVMRParty.ToString() + "</RemPublicVMRCount>");
						//if (conf.isVMR == 1)
                        if (conf.isVMR > 0) //FB 2620
                        {
                            if (remVMRParty > 0)
                                searchOutXml.Append("<isVMRJoin>1</isVMRJoin>");
                            else
                                searchOutXml.Append("<isVMRJoin>0</isVMRJoin>");
                        }
                        else
                            searchOutXml.Append("<isVMRJoin>1</isVMRJoin>");
                        //FB 2550 Ends
                        /* *** Buffer Zone Fixes - start *** */

                        DateTime setupTime;
                        DateTime tearDownTime;

                        if (conf.SetupTime <= DateTime.MinValue)
                            setupTime = conf.confdate;
                        else
                            setupTime = conf.SetupTime;

                        if (conf.TearDownTime <= DateTime.MinValue)
                            tearDownTime = conf.confdate.AddMinutes(conf.duration);
                        else
                            tearDownTime = conf.TearDownTime;

                        if (utcEnabled == 0)//FB 2014
                        {
                            timeZone.userPreferedTime(user.TimeZone, ref setupTime);
                            timeZone.userPreferedTime(user.TimeZone, ref tearDownTime);
                        }

                        searchOutXml.Append("<SetupTime>" + setupTime.ToString("g") + "</SetupTime>");
                        searchOutXml.Append("<TearDownTime>" + tearDownTime.ToString("g") + "</TearDownTime>");

                        TimeSpan span = setupTime.Subtract(conf.confdate);
                        int setupDur = Convert.ToInt32(span.TotalMinutes);

                        span = tearDownTime.Subtract(conf.confdate);
                        double tempDur = span.TotalMinutes;
                        int tearDownDur = conf.duration - Convert.ToInt32(tempDur);

                        searchOutXml.Append("<SetupDur>" + setupDur.ToString() + "</SetupDur>");
                        searchOutXml.Append("<TearDownDur>" + tearDownDur.ToString() + "</TearDownDur>");

                        /* *** Buffer Zone Fixes - end *** */

                        vrmUser host = m_IuserDAO.GetByUserId(conf.owner);

                        if (host != null) // Diagnostics
                        {
                            searchOutXml.Append("<ConferenceHost>" + host.FirstName + " " + host.LastName + "</ConferenceHost>");
                            searchOutXml.Append("<ConferenceHostEmail>" + host.Email + "</ConferenceHostEmail>"); //FB 2617 (user email)
                        }
                        
                        // FB 2694
                        vrmUser requestor = m_IuserDAO.GetByUserId(conf.userid);
                        if (requestor != null) 
                        {
                            searchOutXml.Append("<ConferenceRequestor>" + requestor.FirstName + " " + requestor.LastName + "</ConferenceRequestor>");
                            searchOutXml.Append("<ConferenceRequestorEmail>" + requestor.Email + "</ConferenceRequestorEmail>");
                        }

                        //Code added for RSS Feed   -- Start
                        sysMailData sysMailData = new sysMailData();
                        sysMailData = m_ISysMailDAO.GetById(1);
                       // searchOutXml.Append("<ConferenceHostEmail>" + host.Email + "</ConferenceHostEmail>"); //FB 2617 Commented
                        searchOutXml.Append("<ConfDescription>" + m_utilFactory.ReplaceOutXMLSpecialCharacters(conf.description) + "</ConfDescription>"); //FB 2236
                        searchOutXml.Append("<WebsiteURL>" + sysMailData.websiteURL + "</WebsiteURL>");
                        //Code added for RSS Feed   -- End

                        searchOutXml.Append("<IsHost>" + isHost.ToString() + "</IsHost>");
                        searchOutXml.Append("<IsParticipant>" + isParticipant.ToString() + "</IsParticipant>");
                        if (RecurrentStyle == 0)
                            searchOutXml.Append("<IsRecur>" + RecurrentStyle.ToString() + "</IsRecur>");
                        else
                            searchOutXml.Append("<IsRecur>" + conf.recuring.ToString() + "</IsRecur>");

                        searchOutXml.Append("<Location>");
                        String locList = "";//FB 2694
                        foreach (vrmConfRoom room in conf.ConfRoom)
                        {
                            searchOutXml.Append("<Selected>");
                            searchOutXml.Append("<ID>" + room.roomId.ToString() + "</ID>");
                            searchOutXml.Append("<Name>" + room.Room.Name + "</Name>");
                            searchOutXml.Append("<isVMR>" + room.Room.IsVMR + "</isVMR>"); //FB 2550
                            searchOutXml.Append("</Selected>");
                            //FB 2694
                            if (locList == "")
                                locList = room.Room.tier2.tier3.Name + ">" + room.Room.tier2.Name + ">" + room.Room.Name;
                            else
                                locList = locList + "~ " + room.Room.tier2.tier3.Name + ">" + room.Room.tier2.Name + ">" + room.Room.Name;
                        }
                        searchOutXml.Append("</Location>");
                        //FB 2694
                        if (locList != "")
                            searchOutXml.Append("<LocationList>" + locList.ToString() + " </LocationList>");
                        else
                            searchOutXml.Append("<LocationList>No Rooms</LocationList>");


                        if (isApprovalPending && conf.status == vrmConfStatus.Pending)
                        {
                            //String levelXML = "";//FB 1179
                            //Added for approval pending tags for merging the commands
                            searchOutXml.Append("<ApprovalPending>");
                            searchOutXml.Append("<Entities>");
                            //if (getConfApprovInfo(conf, IsSuperAdmin, userId, ref levelXML))//FB 1179
                            //{//FB 1179
                            searchOutXml.Append(levelXML);
                            //}//FB 1179
                            searchOutXml.Append("</Entities>");
                            searchOutXml.Append("</ApprovalPending>");
                        }
                        //}
                        //Idx++;
                        #region Added for the DashBoard 
                        if (conf.LastRunDateTime == DateTime.MinValue)
                            searchOutXml.Append("<LastRunDate></LastRunDate>");
                        else
                            searchOutXml.Append("<LastRunDate>" + conf.LastRunDateTime.ToString() + "</LastRunDate>");
                        #endregion
                        searchOutXml.Append("</Conference>");
                    }//FB 1179
                }
                searchOutXml.Append("</Conferences>");
                //FB 2894 Starts
                //Search Fixes - FB 1787 - start
                //if (ConferenceUniqueID.Trim().Length <= 0)
                //{
                //    if (isCustomAttrAvailable)
                //    {
                //        ttlRecords = cc;
                //    }
                //}
                //FB 2894 End
                if (p2pCount > 0) //FB 2437
                    ttlRecords = ttlRecords - p2pCount;

                ttlPages = (int)(ttlRecords / m_iMaxRecords);

                // and modulo remainder...

                if (ttlRecords % m_iMaxRecords > 0)
                    ttlPages++;
                //Search Fixes - FB 1787 - end

                timeZoneData tz = new timeZoneData();
                timeZone.GetTimeZone(user.TimeZone, ref tz);
                searchOutXml.Append("<Timezone>" + tz.TimeZone + "</Timezone>");
                searchOutXml.Append("<PageNo>" + PageNo + "</PageNo>");
                searchOutXml.Append("<TotalPages>" + ttlPages.ToString() + "</TotalPages>");
                searchOutXml.Append("<TotalRecords>" + ttlRecords.ToString() + "</TotalRecords>");
                searchOutXml.Append("<UserCurrentTime>" + systemDatetimeUpdate.ToString() + "</UserCurrentTime>");  // FB 1848
                searchOutXml.Append("<SortBy>" + SortBy + "</SortBy>");

                searchOutXml.Append("</SearchConference>");
                obj.outXml = searchOutXml.ToString();
            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message); //FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        /*Code added for FB issue 826 Start */

        #region SearchAllConference
        /// <summary>
        /// SearchAllConference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SearchAllConference(ref vrmDataObject obj)
        {
            bool IsSuperAdmin = false;
            string confActualStatus = "";//Added for Dashboard
            int optionType = 0;//FB 2607
            //Hashtable CustomSearch = new Hashtable(); //FB 2894
            //int customCnt = 0; //FB 2894
            StringBuilder searchOutXml = new StringBuilder(); //String concatenation changed to StringBuilder for Performance - FB 1787
            List<ICriterion> ICrietrionconfbridge = new List<ICriterion>();//FB 2870
            List<int> confnumnamelist = new List<int>(); //FB 2870
            try
            {
                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SearchConference/UserID");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                if (orgid != "")//Hack to get all Conferences for service
                {
                    if (node != null)
                        orgid = node.InnerXml.Trim();
                    organizationID = defaultOrgId;
                    Int32.TryParse(orgid, out organizationID);

                    if (organizationID < 11)
                    {
                        myVRMException myVRMEx = new myVRMException(423);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    if (orgInfo != null)
                        multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes
                }
                node = xd.SelectSingleNode("//SearchConference/ConferenceID");
                string conferenceID = "";
                if (node != null)
                    conferenceID = node.InnerXml.Trim();
                
                string ConferenceName="";
                node = xd.SelectSingleNode("//SearchConference/ConferenceName");
                if (node != null)
                    ConferenceName = node.InnerXml.Trim();

                int startMode = 0;
                node = xd.SelectSingleNode("//SearchConference/StartMode");//FB 2501
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out startMode);

                node = xd.SelectSingleNode("//SearchConference/ConferenceUniqueID");
                string ConferenceUniqueID = node.InnerXml.Trim();
                
                string CTSNumericID = ""; //FB 3001
                node = xd.SelectSingleNode("//SearchConference/CTSNumericID"); //FB 2870
                if(node != null)
                    CTSNumericID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceSearchType");
                string ConferenceSearchType = node.InnerXml.Trim();
                if (ConferenceSearchType.Length == 0)
                    ConferenceSearchType = "-1";
                int iSearchType = Int32.Parse(ConferenceSearchType);

                XmlNodeList statusList = xd.GetElementsByTagName("ConferenceStatus");
                List<int> confStatus = new List<int>();
                foreach (XmlNode innerNode in statusList)
                {
                    int iStatus = Int32.Parse(innerNode.InnerXml.Trim());
                    confStatus.Add(iStatus);
                    if ((iStatus == vrmConfStatus.Pending || iStatus == vrmConfStatus.Scheduled) &&
                        iSearchType < vrmSearchType.Past)
                    {
                        iSearchType = vrmSearchType.Future;
                    }

                }
                node = xd.SelectSingleNode("//SearchConference/ApprovalPending");
                string approvalPending = "";
                isApprovalPending = false;  //FB 1158
                if (node != null)
                    approvalPending = node.InnerXml.Trim();
                if (approvalPending.Length > 0)
                {
                    if (approvalPending == "1")
                        isApprovalPending = true;
                }

                node = xd.SelectSingleNode("//SearchConference/DateFrom");
                string DateFrom = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/DateTo");
                string DateTo = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceHost");
                string ConferenceHost = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceParticipant");
                string ConferenceParticipant = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/Public");
                string Public = node.InnerXml.Trim();
                if (Public.Length == 0)
                    Public = vrmPublicType.Both.ToString();

                node = xd.SelectSingleNode("//SearchConference/RecurrenceStyle");
                int RecurrentStyle = 1;
                if (node != null)
                    RecurrentStyle = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//SearchConference/Location/SelectionType");
                string SelectionType = node.InnerXml.Trim();

                List<int> selectedRooms = new List<int>();
                if (SelectionType == "2")
                {
                    XmlNodeList itemList;

                    itemList = xd.GetElementsByTagName("Selected");
                    foreach (XmlNode innerNode in itemList)
                    {
                        selectedRooms.Add(Int32.Parse(innerNode.InnerXml.Trim()));
                    }
                }
                node = xd.SelectSingleNode("//SearchConference/PageNo");
                string PageNo = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/SortBy");
                string SortBy = node.InnerXml.Trim();
                if (SortBy.Length == 0)
                    SortBy = "0";

                //FB 2014
                int utcEnabled = 0;
                if (xd.SelectSingleNode("//SearchConference/utcEnabled") != null)
                    Int32.TryParse(node.SelectSingleNode("//SearchConference/utcEnabled").InnerText.Trim(), out utcEnabled);
                
                //Code added for Custom Attributes -- Start
                List<int> customAttIDs = new List<int>();
                if (xd.SelectSingleNode("//SearchConference/CustomAttributesList") != null)
                {
                    node = xd.SelectSingleNode("//SearchConference/CustomAttributesList");

                    XmlNodeList customList = null;

                    if (xd.SelectNodes("//SearchConference/CustomAttributesList/CustomAttribute") != null)
                        customList = xd.SelectNodes("//SearchConference/CustomAttributesList/CustomAttribute");

                    List<vrmConfAttribute> customAttlist = null;
                    //isCustomAttrAvailable = false;  //FB 2894
                    if (customList != null)
                    {
                        foreach (XmlNode innerNode in customList)
                        {
                            isCustomAttrAvailable = true;//FB 2894
                            int.TryParse(innerNode.SelectSingleNode("Type").InnerText.Trim(),out optionType); //FB 2607
                            switch (optionType)
                            {
                                case vrmCustomOptions.DropDownList:
                                case vrmCustomOptions.ListBox:
                                    {
                                        int customAttrID = 0;
                                        int optionID = 0;
                                        //isCustomAttrAvailable = true;   //FB 2894

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                customAttrID = Convert.ToInt32(innerNode.SelectSingleNode("CustomAttributeID").InnerText);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionID").InnerText != "")
                                            {
                                                optionID = Convert.ToInt32(innerNode.SelectSingleNode("OptionID").InnerText);
                                            }
                                        }

                                        List<ICriterion> cusCriterionList = new List<ICriterion>();

                                        cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                        cusCriterionList.Add(Expression.Eq("SelectedOptionId", optionID));

                                        customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                        //if (optionID > 0)    //FB 2894
                                        //    customCnt++;

                                        foreach (vrmConfAttribute att in customAttlist)
                                        {
                                            //FB 2894 Starts
                                            if (customAttIDs != null)
                                            {
                                                if (!customAttIDs.Contains(att.ConfId))
                                                    customAttIDs.Add(att.ConfId);
                                            }
                                            else
                                            {
                                                customAttIDs = new List<int>(); //FB 2607
                                                customAttIDs.Add(att.ConfId);
                                            }

                                            //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                            //    CustomSearch.Add(att.ConfId, 1);
                                            //else
                                            //{
                                            //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                            //}
                                            //FB 2894 End
                                        }

                                        break;
                                    }

                                case vrmCustomOptions.TextBox:
                                case vrmCustomOptions.URLTextBox://FB 1612
                                    {
                                        int customAttrID = 0;
                                        string selectedValue = "";
                                        //isCustomAttrAvailable = true;   //FB 2607

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                customAttrID = Convert.ToInt32(innerNode.SelectSingleNode("CustomAttributeID").InnerText);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionValue") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionValue").InnerText != "")
                                            {
                                                selectedValue = innerNode.SelectSingleNode("OptionValue").InnerText;
                                            }
                                        }

                                        //if (selectedValue != "")    //FB 2894
                                        //    customCnt++;

                                        List<ICriterion> cusCriterionList = new List<ICriterion>();

                                        cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                        cusCriterionList.Add(Expression.Like("SelectedValue", "%%" + selectedValue + "%%").IgnoreCase());

                                        customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                        foreach (vrmConfAttribute att in customAttlist)
                                        {
                                            //FB 2894 Starts
                                            if (customAttIDs != null)
                                            {
                                                if (!customAttIDs.Contains(att.ConfId))
                                                    customAttIDs.Add(att.ConfId);
                                            }
                                            else
                                            {
                                                customAttIDs = new List<int>(); //FB 2607
                                                customAttIDs.Add(att.ConfId);
                                            }
                                            //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                            //    CustomSearch.Add(att.ConfId, 1);
                                            //else
                                            //{
                                            //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                            //}
                                            //FB 2894 End

                                        }

                                        break;
                                    }

                                case vrmCustomOptions.CheckBox://FB 2377
                                    {
                                        int customAttrID = 0;
                                        int optionValue = 0;

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                customAttrID = Convert.ToInt32(innerNode.SelectSingleNode("CustomAttributeID").InnerText);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionValue") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionValue").InnerText != "")
                                            {
                                                int.TryParse(innerNode.SelectSingleNode("OptionValue").InnerText, out optionValue);
                                            }
                                        }
                                        if (optionValue > 0) //FB 2607
                                        {
                                            //customCnt++;
                                            //isCustomAttrAvailable = true;   //FB 2607

                                            List<ICriterion> cusCriterionList = new List<ICriterion>();

                                            cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                            cusCriterionList.Add(Expression.Eq("SelectedValue", "1")); //FB 2607 Checked values alone

                                            customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                            foreach (vrmConfAttribute att in customAttlist)
                                            {
                                                //FB 2894 Starts
                                                if (customAttIDs != null)
                                                {
                                                    if (!customAttIDs.Contains(att.ConfId))
                                                        customAttIDs.Add(att.ConfId);
                                                }
                                                else
                                                {
                                                    customAttIDs = new List<int>(); //FB 2607
                                                    customAttIDs.Add(att.ConfId);
                                                }
                                                //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                                //    CustomSearch.Add(att.ConfId, 1);
                                                //else
                                                //{
                                                //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                                //}
                                                //FB 2894 End
                                            }
                                        }

                                        break;
                                    }
                                case vrmCustomOptions.RadioButton:
                                    {
                                        int customAttrID = 0;
                                        string optionValue = "";

                                        if (innerNode.SelectSingleNode("CustomAttributeID") != null)
                                        {
                                            if (innerNode.SelectSingleNode("CustomAttributeID").InnerText != "")
                                            {
                                                customAttrID = Convert.ToInt32(innerNode.SelectSingleNode("CustomAttributeID").InnerText);
                                            }
                                        }
                                        if (innerNode.SelectSingleNode("OptionValue") != null)
                                        {
                                            if (innerNode.SelectSingleNode("OptionValue").InnerText != "")
                                            {
                                                optionValue = innerNode.SelectSingleNode("OptionValue").InnerText;
                                            }
                                        }

                                        List<ICriterion> cusCriterionList = new List<ICriterion>();

                                        cusCriterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                                        cusCriterionList.Add(Expression.Eq("SelectedValue", optionValue));

                                        customAttlist = m_IconfAttrDAO.GetByCriteria(cusCriterionList);

                                        //if (optionValue == "1")    //FB 2894
                                        //{
                                        //    customCnt++;
                                        //    isCustomAttrAvailable = true;   //FB 2894
                                        //}

                                        foreach (vrmConfAttribute att in customAttlist)
                                        {
                                            //FB 2894 Starts
                                            if (customAttIDs != null)
                                            {
                                                if (!customAttIDs.Contains(att.ConfId))
                                                    customAttIDs.Add(att.ConfId);
                                            }
                                            else
                                            {
                                                customAttIDs = new List<int>(); //FB 2607
                                                customAttIDs.Add(att.ConfId);
                                            }
                                            //if (!CustomSearch.Contains(att.ConfId)) //FB 2607
                                            //    CustomSearch.Add(att.ConfId, 1);
                                            //else
                                            //{
                                            //    CustomSearch[att.ConfId] = Convert.ToInt32(CustomSearch[att.ConfId]) + 1;
                                            //}
                                            //FB 2894 End
                                        }

                                        break;
                                    }
                            }
                        }
                        //FB 2894 Starts
                        //foreach (DictionaryEntry DE in CustomSearch)
                        //{
                        //    if (customCnt == Convert.ToInt32(DE.Value))
                        //    {
                        //        if (!customAttIDs.Contains(Convert.ToInt32(DE.Key)))
                        //            customAttIDs.Add(Convert.ToInt32(DE.Key));
                        //    }
                        //}
                        //FB 2894 Ends
                    }
                }
                //Code added for Custom Attributes -- End

                List<ICriterion> userCriterion = new List<ICriterion>();
                userCriterion.Add(Expression.Eq("userid", Int32.Parse(userId)));
                List<vrmUser> userList = m_IuserDAO.GetByCriteria(userCriterion);
                vrmUser user = userList[0];

                //Added to find the requested user is Superadmin 
                //If he is a superadmin change the boolean true
                if (user.isSuperAdmin())
                    IsSuperAdmin = true;

                int ttlPages = 0;
                long ttlRecords = 0;

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmConference> confList = new List<vrmConference>();

                if (orgid != "")//Hack to get all Conferences for service
                    criterionList.Add(Expression.Eq("orgId", organizationID));//Code added for organization

                if (ConferenceUniqueID.Trim().Length > 0 || CTSNumericID.Trim().Length > 0) //FB 2870
                {
                    //FB 2870 Start
                    confnumnamelist = new List<int>();
                    // user criteria to trap bad id
                    if (ConferenceUniqueID.Trim().Length > 0)
                    {
                        criterionList.Add(Expression.Eq("confnumname", Int32.Parse(ConferenceUniqueID)));
                        confnumnamelist.Add(Int32.Parse(ConferenceUniqueID));
                    }
                    else if (CTSNumericID.Trim().Length > 0)
                    {
                        ICrietrionconfbridge = new List<ICriterion>();

                        ICrietrionconfbridge.Add(Expression.Eq("E164Dialnumber", CTSNumericID.Trim()));
                        List<vrmConfBridge> confbridgelist = m_IconfBridge.GetByCriteria(ICrietrionconfbridge, true);
                        for (int b = 0; b < confbridgelist.Count; b++)
                        {
                            confnumnamelist.Add(confbridgelist[b].confuId);
                        }
                        criterionList.Add(Expression.In("confnumname", confnumnamelist));
                    }
                    //FB 2870 End
                    // FB 408 do not show deleted conferences 
                    criterionList.Add(Expression.Eq("deleted", 0));
                    // FB 366 have to apply security to this search too!
                    ICriterion deptCriterium = null;

                    if (multiDepts == 1) //Organization Module
                    {
                        if (getDeptLevelCriterion(user, ref deptCriterium, confnumnamelist)) //FB 1787 //FB 2870
                        {
                            if (deptCriterium != null)//FB 1322,1387
                                criterionList.Add(deptCriterium);
                        }
                    }
                    //FB 1787 - following block is moved to getDeptLevelCriterion as the same is lost during FB 1499
                    /* *** FB 1158 - Approval Issues ... start *** */
                    //if (confsList != null)
                    //{
                    //    if (confsList.Count > 0)
                    //        criterionList.Add(Expression.In("confid", confsList));
                    //}
                    /* *** FB 1158 - Approval Issues ... end *** */

                    confList = m_IconfDAO.GetByCriteria(criterionList);

                    if (confList.Count != 1)
                    {
                        //FB 1033 Start
                        //obj.outXml = myVRMException.toXml("Error no conference with that id found", 314);
                        obj.outXml = "<SearchConference><Conferences></Conferences></SearchConference>"; //Changed during FB 1787
                        //FB 1033 End

                        return false;
                    }
                    if (!CheckConfAuthorization(confList[0], user))
                    {
                        confList.Clear();
                        ttlPages = 1;
                        ttlRecords = 1;
                    }
                    else
                    {
                        ttlPages = 1;
                        ttlRecords = 1;
                    }
                }
                else
                {
                    //Added for the Merging all commands. 
                    //If the conference id is passed then it is for GetInstances which is from the List page not for search page
                    if (conferenceID.Trim().Length > 0)
                    {
                        criterionList.Add(Expression.Eq("confid", Int32.Parse(conferenceID)));
                    }
                    ICriterion criterium = null;

                    if (confStatus.Count > 0)
                    {
                        if (confStatus.Count == 1)
                            criterionList.Add(Expression.Eq("status", confStatus[0]));
                        else
                            criterionList.Add(Expression.In("status", confStatus));
                    }

                    DateTime confFrom = new DateTime();
                    DateTime confEnd = new DateTime();

                    if (DateFrom.Length > 0)
                        confFrom = DateTime.Parse(DateFrom);
                    if (DateTo.Length > 0)
                        confEnd = DateTime.Parse(DateTo);
                    // ge today

                    int a_userId = sysSettings.TimeZone;

                    if (iSearchType == vrmSearchType.Cutom)
                        a_userId = user.TimeZone;

                    if (iSearchType == vrmSearchType.Past || iSearchType == vrmSearchType.Future)
                    {
                        confFrom = DateTime.Now;
                        timeZone.changeToGMTTime(a_userId, ref confFrom);
                        if (iSearchType == vrmSearchType.Past)
                            criterium = Expression.Le("confEnd", confFrom);
                        else
                            criterium = Expression.Ge("confdate", confFrom);
                        criterionList.Add(criterium);

                    }
                    else
                    {
                        if (getSearchDateRange(iSearchType, ref confEnd, ref confFrom, 0))//FB 2595
                        {
                            timeZone.changeToGMTTime(a_userId, ref confFrom);
                            timeZone.changeToGMTTime(a_userId, ref confEnd);
                            criterium = Expression.Le("confdate", confEnd);
                            criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                        }
                    }
                    criterium = null; //FB 1499
                    if (multiDepts == 1) //Organization Module
                    {
                        if (getDeptLevelCriterion(user, ref criterium, confnumnamelist)) //FB 1787 //FB 2870
                        {
                            if (criterium != null)//FB 1499 Error
                                criterionList.Add(criterium);
                        }
                    }

                    //Below block is moved to getDeptLevelCriterion Method as the same is lost during FB 1499
                    /* *** FB 1158 - Approval pending confs are not displayed in the general user - start *** */
                    //if (!IsSuperAdmin)
                    //{
                    //    if (isApprovalPending)
                    //        GetConfsListForApproval(user.userid.ToString());

                    //    if (confsList != null)
                    //    {
                    //        if (confsList.Count > 0)
                    //            criterionList.Add(Expression.In("confid", confsList));
                    //    }
                    //}
                    /* *** FB 1158 - Approval pending confs are not displayed in the general user - end *** */

                    // I cant think of any other way to do this.
                    // you have to get the conf's for this search citerea and select only thos confid's that
                    // match (this also applies to the participant tags....)
                    Hashtable confId = new Hashtable();
                    List<int> confUniqueId = new List<int>();
                    List<vrmUser> selectedUser = new List<vrmUser>();

                    if (ConferenceParticipant.Length > 0)
                    {
                        List<ICriterion> userCriterionList = new List<ICriterion>();
                        ICriterion uc = Expression.Like("LastName", "%%" + ConferenceParticipant + "%%").IgnoreCase();
                        uc = Expression.Or(uc, Expression.Like("FirstName", "%%" + ConferenceParticipant + "%%").IgnoreCase());
                        userCriterionList.Add(uc);
                        selectedUser = m_IuserDAO.GetByCriteria(userCriterionList);
                    }
                    if (ConferenceHost.Length > 0)
                    {
                        List<ICriterion> userCriterionList = new List<ICriterion>();
                        List<ICriterion> hostCriterionList = new List<ICriterion>();
                        ICriterion uc;
                        //
                        // FB 11,653 if there is a space then it is firstname lastname
                        //                    
                        int i = ConferenceHost.IndexOf(" ");
                        if (i > 0)
                        {
                            string firstName = ConferenceHost.Substring(0, i);
                            firstName = firstName.Trim();
                            string lastName = ConferenceHost.Substring(i, ConferenceHost.Length - i);
                            lastName = lastName.Trim();
                            if (lastName.Length > 0)
                            {
                                uc = Expression.Like("LastName", "%%" + lastName + "%%").IgnoreCase();
                                uc = Expression.Or(uc, Expression.Like("FirstName", "%%" + firstName + "%%").IgnoreCase());
                            }
                            else
                            {
                                uc = Expression.Like("FirstName", "%%" + firstName + "%%").IgnoreCase();
                            }
                        }
                        else
                        {
                            uc = Expression.Like("LastName", "%%" + ConferenceHost + "%%").IgnoreCase(); // FB 1952
                            uc = Expression.Or(uc, Expression.Like("FirstName", "%%" + ConferenceHost + "%%").IgnoreCase()); // FB 1952
                        }
                        hostCriterionList.Add(uc); // FB 1952
                        List<vrmUser> selectedHost = m_IuserDAO.GetByCriteria(hostCriterionList);

                        List<int> hostList = new List<int>();
                        foreach (vrmUser host in selectedHost)
                            hostList.Add(host.userid);

                        criterionList.Add(Expression.In("owner", hostList));
                    }
                    int isPublic = Int32.Parse(Public);
                    if (iSearchType != 9) //FB 2501 
                    {
                        if (isPublic == vrmPublicType.Public || isPublic == vrmPublicType.Private)
                        {
                            criterionList.Add(Expression.Eq("isPublic", isPublic));
                        }
                    }
                    //FB 2321 Starts
                    string ConfName = ConferenceName.ToLower();
                    if (ConfName.Contains("["))
                        ConfName = "%%" + ConfName.Replace("[", "|[") + "%%";
                    else
                        ConfName = "%%" + ConfName + "%%";
                    if (ConferenceName.Length > 0)
                        criterionList.Add(Expression.Like("externalname", ConfName, MatchMode.Anywhere, '|')); 
                        //criterionList.Add(Expression.Like("externalname", "%%" + ConferenceName.ToLower() + "%%").IgnoreCase());
                    //FB 2321 Ends
                    criterionList.Add(Expression.Eq("deleted", 0));

                    //FB 2894 Starts
                    if (isCustomAttrAvailable)
                    {
                        if (customAttIDs.Count > 0)
                        {
                            criterionList.Add(Restrictions.In("confid", customAttIDs)); // FB 2607
                            confList = m_IconfDAO.GetByCriteria(criterionList);
                        }
                        else
                            confList = new List<vrmConference>();
                    }
                    else
                        confList = m_IconfDAO.GetByCriteria(criterionList);

                    //FB 2894 End
                    
                    confList = m_IconfDAO.GetByCriteria(criterionList);

                    //Changed foreach to for loop during FB 1787 for performance issue
                    //foreach (vrmConference conf in confList) 
                    vrmConference conf = null;
                    for (int lp = 0; lp < confList.Count; lp++)
                    {
                        conf = confList[lp];

                        // all searches must meet one of three conditions here. 
                        // all rooms (everything)
                        // No rooms (confernces with NO rooms)
                        // selected rooms (conferences that have rooms in the selected list)
                        switch (Int32.Parse(SelectionType))
                        {
                            case 0: // no rooms 
                                if (conf.ConfRoom.Count == 0)
                                {
                                    if (CheckConfAuthorization(conf, user))
                                        // two differnce styles of display. The first (0) displays 
                                        // each instance. The second displays recurring as a group. 
                                        if (RecurrentStyle == 0)
                                            confId.Add(conf.confnumname, conf.confnumname);
                                        else
                                            if (!confId.ContainsKey(conf.confid))
                                            {
                                                confId.Add(conf.confid, conf.confnumname);
                                                break;
                                            }
                                }
                                break;
                            case 2: // selected rooms
                                if (selectedRooms.Count > 0)
                                {
                                    if (!confId.ContainsKey(conf.confid))
                                    {
                                        foreach (vrmConfRoom rm in conf.ConfRoom)
                                        {
                                            for (int idx = 0; idx < selectedRooms.Count; idx++)
                                            {
                                                if (selectedRooms[idx] == rm.roomId)
                                                {
                                                    if (CheckConfAuthorization(conf, user))
                                                    {
                                                        if (RecurrentStyle == 0)
                                                        {
                                                            if (!confId.ContainsKey(conf.confnumname))
                                                                confId.Add(conf.confnumname, conf.confnumname);
                                                        }
                                                        else
                                                        {
                                                            if (!confId.ContainsKey(conf.confid))
                                                            {
                                                                confId.Add(conf.confid, conf.confnumname);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            default: // all rooms (all confs)
                                if (CheckConfAuthorization(conf, user))
                                    if (RecurrentStyle == 0)
                                    {
                                        if (!confId.ContainsKey(conf.confnumname))
                                            confId.Add(conf.confnumname, conf.confnumname);
                                    }
                                    else
                                    {
                                        if (!confId.ContainsKey(conf.confid))
                                        {
                                            confId.Add(conf.confid, conf.confnumname);
                                            break;
                                        }
                                    }
                                break;
                        }
                        if (selectedUser.Count > 0)
                        {
                            bool bFound = false;
                            if (confId.ContainsKey(conf.confid))
                            {
                                foreach (vrmConfUser us in conf.ConfUser)
                                {
                                    for (int idx = 0; idx < selectedUser.Count; idx++)
                                    {
                                        if (selectedUser[idx].userid == us.userid)
                                        {
                                            bFound = true;
                                            break;
                                        }
                                    }
                                    if (bFound)
                                        break;
                                }
                                if (!bFound)
                                    confId.Remove(conf.confid);
                            }
                        }
                    }
                    IDictionaryEnumerator iEnum = confId.GetEnumerator();
                    while (iEnum.MoveNext())
                    {
                        confUniqueId.Add((int)iEnum.Value);
                    }

                    if ((confUniqueId.Count == 0) && !(isApprovalPending)) //Providea
                        confUniqueId.Add(0); 

                    if (confUniqueId.Count > 0 || SelectionType == "2") //FB 1612
                        criterionList.Add(Expression.In("confnumname", confUniqueId));
                    m_IconfDAO.pageSize(confList.Count);

                    int iPageNo = Int32.Parse(PageNo);
                    if (iPageNo == 0)
                        iPageNo = 1;

                    m_IconfDAO.pageNo(iPageNo);

                    ttlRecords = m_IconfDAO.CountByCriteria(criterionList);

                    /*
                    Search Fixes - FB 1787 - start
                    if (ConferenceUniqueID.Trim().Length <= 0)
                    {
                        if (isCustomAttrAvailable)
                        {
                            ttlRecords = customAttIDs.Count;
                        }
                    }

                    ttlPages = (int)(ttlRecords / m_iMaxRecords);
                    */
                    // and modulo remainder...
                    /*
                    if (ttlRecords % m_iMaxRecords > 0)
                        ttlPages++;
                    */
                    //Search Fixes - FB 1787 - end

                    // add sort order (if any)
                    switch (Int32.Parse(SortBy))
                    {
                        case vrmSortBy.UniqueID:
                            m_IconfDAO.addOrderBy(Order.Asc("confnumname"));
                            break;
                        case vrmSortBy.ConfName:
                            m_IconfDAO.addOrderBy(Order.Asc("externalname"));
                            break;
                        case vrmSortBy.ConfDate:
                            m_IconfDAO.addOrderBy(Order.Asc("confdate"));
                            break;
                        case vrmSortBy.StartMode:
                            m_IconfDAO.addOrderBy(Order.Asc("StartMode"));//FB 2501
                            break;
                    }

                    //criterionList.Add(Expression.Eq("orgId", organizationID));//Code added for organization //NEEDS Code review

                    confList = m_IconfDAO.GetByCriteria(criterionList);

                }

                searchOutXml.Append("<SearchConference>"); //Changed during FB 1787

                searchOutXml.Append("<Conferences>");
                int cc = 0; //FB 1787
                int p2pCount = 0;//FB 2437
                foreach (vrmConference conf in confList)
                {

                    DateTime confDateTime = conf.confdate;
                    DateTime todayNow = DateTime.Now; //FB 2437

                    if (utcEnabled == 0)//FB 2014
                        timeZone.userPreferedTime(user.TimeZone, ref confDateTime);

                    //FB 2437 - Starts
                    if (conf.conftype == vrmConfType.P2P && sysSettings.EnableLaunchBufferP2P == 0 && confStatus.Contains(vrmConfStatus.Ongoing)) 
                    {
                        if (confDateTime > todayNow)
                        {
                            p2pCount++;
                            continue;
                        }
                    }
                    //FB 2437 - End
                    
                    //Custom Attribute Fix -- Start
                    // FB 2894 Start
                    /*
                    if (ConferenceUniqueID.Trim().Length <= 0)
                    {
                        if (isCustomAttrAvailable)
                        {
                            if (customAttIDs != null)
                            {
                                if (customAttIDs.Count > 0)
                                {
                                    if (!customAttIDs.Contains(conf.confid))
                                        continue;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                     */
                    // FB 2894 End
                    //Custom Attribute Fix -- End

                    cc++;   //FB 1787

                    int isHost = 0;
                    int isParticipant = 0;

                    if (user.userid == conf.owner)
                        isHost = 1;
                    foreach (vrmConfUser confUser in conf.ConfUser)
                    {
                        if (confUser.userid == user.userid)
                        {
                            isParticipant = 1;
                            break;
                        }
                    }
                    confActualStatus = conf.status.ToString(); //Added for Dashboard
                    // FB 390 AG check search type NOT selection type 
                    switch (Int32.Parse(ConferenceSearchType))
                    {
                        case vrmSearchType.Ongoing:
                            conf.status = vrmConfStatus.Ongoing;
                            break;
                        case vrmSearchType.Yesterday:
                            conf.status = vrmConfStatus.Completed;
                            break;
                        case vrmSearchType.Tomorrow:
                            break;
                        default:
                            // check date/duration and see if it is past
                            DateTime testStart = conf.confdate;
                            timeZone.userPreferedTime(sysSettings.TimeZone, ref testStart);
                            DateTime testEnd = testStart.AddMinutes(conf.duration);
                            if (testEnd < DateTime.Now)
                                conf.status = vrmConfStatus.Completed;
                            break;
                    }
                    //FB 1179
                    StringBuilder levelXML = new StringBuilder();
                    bool isApprover = false;
                    if (isApprovalPending && conf.status == vrmConfStatus.Pending)
                        getConfApprovInfo(conf, IsSuperAdmin, userId, ref levelXML);

                    if (IsSuperAdmin)
                        isApprover = true;
                    else if (levelXML.ToString().Contains("<Entity>"))
                        isApprover = true;

                    if (isApprover || (!isApprovalPending || conf.status != vrmConfStatus.Pending))
                    {
                        //FB 1179 
                        searchOutXml.Append("<Conference>");
                        searchOutXml.Append("<ConferenceID>" + conf.confid.ToString() + "," +
                                conf.instanceid.ToString() + "</ConferenceID>");
                        searchOutXml.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                        searchOutXml.Append("<ConferenceName>" + conf.externalname.ToString() + "</ConferenceName>");
                        searchOutXml.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                        searchOutXml.Append("<ConferenceDateTime>" + confDateTime.ToString("g") + "</ConferenceDateTime>");
                        searchOutXml.Append("<ConferenceDuration>" + conf.duration.ToString() + "</ConferenceDuration>");
                        searchOutXml.Append("<StartMode>" + conf.StartMode.ToString() + "</StartMode>");//FB 2501

                        // if this is a recurring conference check the first instance if it is past then set the 
                        // status to 7 
                        vrmConference cf = conf;
                        if (conf.recuring > 0)
                        {
                            if (conf.instanceid > 1) //Diagnostics
                            {
                                m_IconfDAO = m_confDAO.GetConferenceDao();//FB 1211
                                cf = m_IconfDAO.GetByConfId(conf.confid, conf.instanceid);
                            }
                            DateTime tempTime = cf.confdate;
                            // timeZone.userPreferedTime(user.TimeZone, ref tempTime); //Needs Code review
                            DateTime checkTime = DateTime.Now;
                            // timeZone.changeToGMTTime(sysSettings.TimeZone, ref checkTime);//Needs Code review
                            // timeZone.userPreferedTime(user.TimeZone, ref checkTime);//Needs Code review
                            //Condition added if we pass the conferenceid then it is for GetInstances
                            if ((tempTime <= checkTime) && (conferenceID.Trim().Length < 1) && (!isApprovalPending))
                                conf.status = 7;
                        }
                        searchOutXml.Append("<ConferenceStatus>" + conf.status.ToString() + "</ConferenceStatus>");
                        searchOutXml.Append("<ConferenceActualStatus>" + confActualStatus + "</ConferenceActualStatus>"); //Added for Dashboard
                        searchOutXml.Append("<OpenForRegistration>" + conf.dynamicinvite.ToString() + "</OpenForRegistration>");

                        /* *** Buffer Zone Fixes - start *** */

                        DateTime setupTime;
                        DateTime tearDownTime;

                        if (conf.SetupTime <= DateTime.MinValue)
                            setupTime = conf.confdate;
                        else
                            setupTime = conf.SetupTime;

                        if (conf.TearDownTime <= DateTime.MinValue)
                            tearDownTime = conf.confdate.AddMinutes(conf.duration);
                        else
                            tearDownTime = conf.TearDownTime;

                        if (utcEnabled == 0)//FB 2014
                        {
                            timeZone.userPreferedTime(user.TimeZone, ref setupTime);
                            timeZone.userPreferedTime(user.TimeZone, ref tearDownTime);
                        }

                        searchOutXml.Append("<SetupTime>" + setupTime.ToString("g") + "</SetupTime>");
                        searchOutXml.Append("<TearDownTime>" + tearDownTime.ToString("g") + "</TearDownTime>");

                        TimeSpan span = setupTime.Subtract(conf.confdate);
                        int setupDur = Convert.ToInt32(span.TotalMinutes);

                        span = tearDownTime.Subtract(conf.confdate);
                        double tempDur = span.TotalMinutes;
                        int tearDownDur = conf.duration - Convert.ToInt32(tempDur);

                        searchOutXml.Append("<SetupDur>" + setupDur.ToString() + "</SetupDur>");
                        searchOutXml.Append("<TearDownDur>" + tearDownDur.ToString() + "</TearDownDur>");

                        /* *** Buffer Zone Fixes - end *** */

                        vrmUser host = m_IuserDAO.GetByUserId(conf.owner);

                        if (host != null) // Diagnostics
                            searchOutXml.Append("<ConferenceHost>" + host.FirstName + " " + host.LastName + "</ConferenceHost>");

                        //Code added for RSS Feed   -- Start
                        sysMailData sysMailData = new sysMailData();
                        sysMailData = m_ISysMailDAO.GetById(1);
                        searchOutXml.Append("<ConferenceHostEmail>" + host.Email + "</ConferenceHostEmail>");
                        searchOutXml.Append("<ConfDescription>" + m_utilFactory.ReplaceOutXMLSpecialCharacters(conf.description).ToString() + "</ConfDescription>"); //FB 2236
                        searchOutXml.Append("<WebsiteURL>" + sysMailData.websiteURL + "</WebsiteURL>");
                        //Code added for RSS Feed   -- End

                        searchOutXml.Append("<IsHost>" + isHost.ToString() + "</IsHost>");
                        searchOutXml.Append("<IsParticipant>" + isParticipant.ToString() + "</IsParticipant>");
                        if (RecurrentStyle == 0)
                            searchOutXml.Append("<IsRecur>" + RecurrentStyle.ToString() + "</IsRecur>");
                        else
                            searchOutXml.Append("<IsRecur>" + conf.recuring.ToString() + "</IsRecur>");

                        searchOutXml.Append("<Location>");
                        foreach (vrmConfRoom room in conf.ConfRoom)
                        {
                            searchOutXml.Append("<Selected>");
                            searchOutXml.Append("<ID>" + room.roomId.ToString() + "</ID>");
                            searchOutXml.Append("<Name>" + room.Room.Name + "</Name>");
                            searchOutXml.Append("</Selected>");
                        }
                        searchOutXml.Append("</Location>");

                        if (isApprovalPending && conf.status == vrmConfStatus.Pending)
                        {
                            //String levelXML = "";//FB 1179
                            //Added for approval pending tags for merging the commands
                            searchOutXml.Append("<ApprovalPending>");
                            searchOutXml.Append("<Entities>");
                            //if (getConfApprovInfo(conf, IsSuperAdmin, userId, ref levelXML))//FB 1179
                            //{//FB 1179
                            searchOutXml.Append(levelXML);
                            //}//FB 1179
                            searchOutXml.Append("</Entities>");
                            searchOutXml.Append("</ApprovalPending>");
                        }
                        //}
                        //Idx++;
                        #region Added for the DashBoard
                        if (conf.LastRunDateTime == DateTime.MinValue)
                            searchOutXml.Append("<LastRunDate></LastRunDate>");
                        else
                            searchOutXml.Append("<LastRunDate>" + conf.LastRunDateTime.ToString() + "</LastRunDate>");
                        #endregion
                        searchOutXml.Append("</Conference>");
                    }//FB 1179
                }
                searchOutXml.Append("</Conferences>");
                //FB 2894 Start
                //Search Fixes - FB 1787 - start
                //if (ConferenceUniqueID.Trim().Length <= 0)
                //{
                //    if (isCustomAttrAvailable)
                //    {
                //        ttlRecords = cc;
                //    }
                //}
                //FB 2894 End
                if (p2pCount > 0) //FB 2437
                    ttlRecords = ttlRecords - p2pCount;

                ttlPages = (int)(ttlRecords / m_iMaxRecords);

                // and modulo remainder...

                if (ttlRecords % m_iMaxRecords > 0)
                    ttlPages++;
                //Search Fixes - FB 1787 - end

                timeZoneData tz = new timeZoneData();
                timeZone.GetTimeZone(user.TimeZone, ref tz);
                searchOutXml.Append("<Timezone>" + tz.TimeZone + "</Timezone>");
                searchOutXml.Append("<PageNo>" + PageNo + "</PageNo>");
                searchOutXml.Append("<TotalPages>" + ttlPages.ToString() + "</TotalPages>");
                searchOutXml.Append("<TotalRecords>" + ttlRecords.ToString() + "</TotalRecords>");
                searchOutXml.Append("<SortBy>" + SortBy + "</SortBy>");

                searchOutXml.Append("</SearchConference>");
                obj.outXml = searchOutXml.ToString();
            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetConfBaseDetails
        /// <summary>
        /// GetConfBaseDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConfBaseDetails(ref vrmDataObject obj)
        {
            bool IsSuperAdmin = false;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmConference> confList = new List<vrmConference>();
                StringBuilder Outxml = new StringBuilder(); //FB 2607 StringBuilder

                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SearchConference/UserID");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SearchConference/ConferenceID");
                string conferenceID = "";
                if (node != null)
                    conferenceID = node.InnerXml.Trim();

                CConfID ConfMode = new CConfID(conferenceID);



                node = xd.SelectSingleNode("//SearchConference/ApprovalPending");
                string approvalPending = "";
                isApprovalPending = false;  //FB 1158

                if (node != null)
                    approvalPending = node.InnerXml.Trim();
                if (approvalPending.Length > 0)
                {
                    if (approvalPending == "1")
                        isApprovalPending = true;
                }

                node = xd.SelectSingleNode("//SearchConference/RecurrenceStyle");
                int RecurrentStyle = 1;
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out RecurrentStyle);

                List<ICriterion> userCriterion = new List<ICriterion>();
                userCriterion.Add(Expression.Eq("userid", Int32.Parse(userId)));
                List<vrmUser> userList = m_IuserDAO.GetByCriteria(userCriterion);
                vrmUser user = userList[0];

                //Added for the Merging all commands. 
                //If the conference id is passed then it is for GetInstances which is from the List page not for search page
                if (ConfMode.ID.ToString().Trim().Length > 0)
                {
                    criterionList.Add(Expression.Eq("confid", ConfMode.ID));
                }

                confList = m_IconfDAO.GetByCriteria(criterionList);


                Outxml.Append("<SearchConference>");

                Outxml.Append("<Conferences>");

                foreach (vrmConference conf in confList)
                {
                    
                    DateTime confDateTime = conf.confdate;
                    timeZone.userPreferedTime(user.TimeZone, ref confDateTime);

                    int isHost = 0;
                    int isParticipant = 0;

                    if (user.userid == conf.owner)
                        isHost = 1;
                    foreach (vrmConfUser confUser in conf.ConfUser)
                    {
                        if (confUser.userid == user.userid)
                        {
                            isParticipant = 1;
                            break;
                        }
                    }

                    StringBuilder levelXML = new StringBuilder();
                    bool isApprover = false;
                    if (isApprovalPending && conf.status == vrmConfStatus.Pending)
                        getConfApprovInfo(conf, IsSuperAdmin, userId, ref levelXML);

                    if (IsSuperAdmin)
                        isApprover = true;
                    else if (levelXML.ToString().Contains("<Entity>"))
                        isApprover = true;

                    if (isApprover || (!isApprovalPending || conf.status != vrmConfStatus.Pending))
                    {
                        
                        //FB 1179 
                        Outxml.Append("<Conference>");
                        Outxml.Append("<ConferenceID>" + conf.confid.ToString() + "," +
                                conf.instanceid.ToString() + "</ConferenceID>");
                        Outxml.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                        Outxml.Append("<ConferenceName>" + conf.externalname.ToString() + "</ConferenceName>");
                        Outxml.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                        Outxml.Append("<ConferenceDateTime>" + confDateTime.ToString("g") + "</ConferenceDateTime>");
                        Outxml.Append("<ConferenceDuration>" + conf.duration.ToString() + "</ConferenceDuration>");

                        // if this is a recurring conference check the first instance if it is past then set the 
                        // status to 7 
                        vrmConference cf = conf;
                        if (conf.recuring > 0)
                        {
                            if (conf.instanceid != 1)
                            {
                                m_IconfDAO = m_confDAO.GetConferenceDao();//FB 1211
                                cf = m_IconfDAO.GetByConfId(conf.confid, 1);
                            }
                            DateTime tempTime = cf.confdate;
                            timeZone.userPreferedTime(user.TimeZone, ref tempTime);
                            DateTime checkTime = DateTime.Now;
                            timeZone.changeToGMTTime(sysSettings.TimeZone, ref checkTime);
                            timeZone.userPreferedTime(user.TimeZone, ref checkTime);
                            //Condition added if we pass the conferenceid then it is for GetInstances
                            if ((tempTime <= checkTime) && (conferenceID.Trim().Length < 1) && (!isApprovalPending))
                                conf.status = 7;
                        }
                        Outxml.Append("<ConferenceStatus>" + conf.status.ToString() + "</ConferenceStatus>");
                        Outxml.Append("<OpenForRegistration>" + conf.dynamicinvite.ToString() + "</OpenForRegistration>");
                        Outxml.Append("<IsPublic>" + conf.isPublic.ToString() + "</IsPublic>");

                        vrmUser host = m_IuserDAO.GetByUserId(conf.owner);

                        Outxml.Append("<ConferenceHost>" + host.FirstName + " " + host.LastName + "</ConferenceHost>");
                        Outxml.Append("<IsHost>" + isHost.ToString() + "</IsHost>");
                        Outxml.Append("<IsParticipant>" + isParticipant.ToString() + "</IsParticipant>");
                        if (RecurrentStyle == 0)
                            Outxml.Append("<IsRecur>" + RecurrentStyle.ToString() + "</IsRecur>");
                        else
                            Outxml.Append("<IsRecur>" + conf.recuring.ToString() + "</IsRecur>");

                        Outxml.Append("<Location>");
                        foreach (vrmConfRoom room in conf.ConfRoom)
                        {
                            Outxml.Append("<Selected>");
                            Outxml.Append("<ID>" + room.roomId.ToString() + "</ID>");
                            Outxml.Append("<Name>" + room.Room.Name + "</Name>");
                            Outxml.Append("</Selected>");
                        }
                        Outxml.Append("</Location>");

                        if (isApprovalPending && conf.status == vrmConfStatus.Pending)
                        {
                            //String levelXML = "";//FB 1179
                            //Added for approval pending tags for merging the commands
                            Outxml.Append("<ApprovalPending>");
                            Outxml.Append("<Entities>");
                            //if (getConfApprovInfo(conf, IsSuperAdmin, userId, ref levelXML))//FB 1179
                            //{//FB 1179
                            Outxml.Append(levelXML);
                            //}//FB 1179
                            Outxml.Append("</Entities>");
                            Outxml.Append("</ApprovalPending>");
                        }
                        //}
                        //Idx++;
                        Outxml.Append("</Conference>");
                    }//FB 1179
                }
                Outxml.Append("</Conferences>");


                Outxml.Append("</SearchConference>");
                obj.outXml = Outxml.ToString();
            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message); //FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        /*Code added for FB issue 826 End */

        #region getConfApprovInfo
        /// <summary>
        /// getConfApprovInfo
        /// </summary>
        /// <param name="objConf"></param>
        /// <param name="isSA"></param>
        /// <param name="userID"></param>
        /// <param name="entityXML"></param>
        /// <returns></returns>
        public bool getConfApprovInfo(vrmConference objConf, bool isSA, string userID, ref StringBuilder entityXML)
        {
            entityXML = new StringBuilder();
            ArrayList roomAppList = new ArrayList();   //FB 1158
            try
            {
                vrmConfApproval confApprove = new vrmConfApproval();

                /* *** FB 1158 - Approval Issues ... start *** */

                //IList<vrmConfApproval> appList = m_confDAO.GetConfApprovalByInstanceID(objConf.confid, objConf.instanceid);

                IList<vrmConfApproval> appList = m_confDAO.GetPendingConfByInstanceID(objConf.confid, objConf.instanceid);

                m_level = 0;
                foreach (vrmConfApproval confAppr in appList)
                {
                    StringBuilder tempXML = new StringBuilder();

                    string roomAppIDs = "";
                    roomAppIDs = confAppr.entitytype + "|" + confAppr.entityid;

                    if (m_level == 0)
                    {
                        m_level = confAppr.entitytype;
                        entityXML.Append("<Level>" + m_level.ToString() + "</Level>");
                    }
                    else if (m_level != confAppr.entitytype)
                    {
                        m_level = confAppr.entitytype;
                        entityXML.Append("<Level>" + m_level.ToString() + "</Level>");
                    }

                    if (!roomAppList.Contains(roomAppIDs))
                    {
                        roomAppList.Add(roomAppIDs);
                    }
                    else
                    {
                        continue;
                    }

                    if (getEntityLevelApprover(objConf, m_level, isSA, userID, ref tempXML, confAppr))
                    {
                        entityXML.Append(tempXML);
                    }
                    else
                        tempXML = new StringBuilder();
                }

                /* *** FB 1158 - Approval Issues ... end *** */
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return false;
            }
            return true;
        }
        #endregion

        #region getEntityLevelApprover
        /// <summary>
        /// getEntityLevelApprover
        /// </summary>
        /// <param name="objConf"></param>
        /// <param name="level"></param>
        /// <param name="isSA"></param>
        /// <param name="UserID"></param>
        /// <param name="OutXML"></param>
        /// <param name="confAppr"></param>
        /// <returns></returns>
        private bool getEntityLevelApprover(vrmConference objConf, int level, bool isSA, string UserID, ref StringBuilder OutXML, vrmConfApproval confAppr)
        {
            try
            {
                OutXML = new StringBuilder();
                List<ICriterion> criterionList;
                switch (level)
                {
                    case (int)LevelEntity.ROOM:
                        vrmRoom theRoom = m_IRoomDAO.GetById(confAppr.entityid);
                        IList<vrmLocApprover> locApprList = theRoom.locationApprover;
                        foreach (vrmLocApprover locAppr in locApprList)
                        {
                            if (Int32.Parse(UserID) == locAppr.approverid || isSA)
                            {

                                OutXML.Append("<Entity>");
                                OutXML.Append("<Name>" + theRoom.Name.ToString() + "</Name>");
                                OutXML.Append("<ID>" + theRoom.RoomID.ToString() + "</ID>");
                                OutXML.Append("<Message>" + confAppr.responsemessage + "</Message>");
                                OutXML.Append("</Entity>");
                                break;//FB 1158
                            }
                        }
                        break;
                    case (int)LevelEntity.MCU:
                        vrmMCU mcu = m_vrmMCU.GetById(confAppr.entityid);
                        foreach (vrmMCUApprover mcuAppr in mcu.MCUApprover)
                        {
                            if (Int32.Parse(UserID) == mcuAppr.approverid || isSA)
                            {
                               if (mcu.orgId == organizationID) //FB 1920
                                {
                                    OutXML.Append("<Entity>");
                                    OutXML.Append("<Name>" + mcu.BridgeName.ToString() + "</Name>");
                                    OutXML.Append("<ID>" + mcu.BridgeID.ToString() + "</ID>");
                                    OutXML.Append("<Message>" + confAppr.responsemessage + "</Message>");
                                    OutXML.Append("</Entity>");
                                }
                                break;//FB 1158
                            }
                        }
                        break;
                    case (int)LevelEntity.DEPT:
                        if (objConf.ConfDeptID != 0)
                        {
                            deptDAO m_deptDAO = new deptDAO(m_configPath, m_log);
                            IDeptApproverDao m_IuserDeptDAO = m_deptDAO.GetDeptApproverDao();
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("departmentid", confAppr.entityid));
                            List<vrmDeptApprover> deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                            IDeptDao m_IDptDAO = m_deptDAO.GetDeptDao();
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("departmentid", confAppr.entityid));
                            List<vrmDept> dptList = m_IDptDAO.GetByCriteria(criterionList);
                            foreach (vrmDept dpt in dptList)
                            {
                                foreach (vrmDeptApprover deptApprov in deptList)
                                {
                                    if (Int32.Parse(UserID) == deptApprov.approverid || isSA)
                                    {
                                        OutXML.Append("<Entity>");
                                        OutXML.Append("<Name>" + dpt.departmentName.ToString() + "</Name>");
                                        OutXML.Append("<ID>" + dpt.departmentId.ToString() + "</ID>");
                                        OutXML.Append("<Message>" + confAppr.responsemessage + "</Message>");
                                        OutXML.Append("</Entity>");
                                        break;//FB 1158
                                    }
                                }
                            }
                        }
                        break;
                    case (int)LevelEntity.SYSTEM:

                        if (organizationID < 11)    //Organization Module
                            organizationID = defaultOrgId;

                        if (objConf.orgId != organizationID) //FB 1920
                            break;

                        if (orgInfo == null)
                            orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                        IList<sysApprover> sysApproverList = m_ISysApproverDAO.GetSysApproversByOrgId(organizationID);  //Organization Module

                        foreach (sysApprover sysApp in sysApproverList)
                        {
                            if (Int32.Parse(UserID) == sysApp.approverid || isSA)
                            {
                                OutXML.Append("<Entity>");
                                OutXML.Append("<Name></Name>");
                                OutXML.Append("<ID></ID>");
                                OutXML.Append("<Message>" + confAppr.responsemessage + "</Message>");
                                OutXML.Append("</Entity>");
                                break;
                            }
                        }
                        break;
                    default:
                        return false;
                        break;
                }
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return false;
            }
            return true;
        }
        #endregion

        #region getSearchDateRange
        /// <summary>
        /// getSearchDateRange
        /// </summary>
        /// <param name="SelectionType"></param>
        /// <param name="toDate"></param>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        public bool getSearchDateRange(int SelectionType, ref DateTime toDate, ref DateTime fromDate,double secs) //FB 2595
        {
            DateTime today = DateTime.Now;
            DateTime to = DateTime.Now;
            DateTime from = DateTime.Now;

            int fHr = 0, fMin = 0, fSec = 0;
            int tHr = 23, tMin = 59, tSec = 59;
            bool setTime = true;

            switch (SelectionType)
            {
                // immediate conference
                case vrmSearchType.Ongoing:
                    fromDate = today;
                    toDate = today.AddMinutes(sysSettings.LaunchBuffer + 1);//FB 2007
                    toDate = toDate.AddSeconds(secs);//FB 2595
                    setTime = false;
                    break;
                case vrmSearchType.Today:
                    from = today;
                    to = today;
                    break;
                case vrmSearchType.ThisWeek:
                    int thisDay = (int)today.DayOfWeek;
                    from = today.AddDays(-thisDay);
                    to = from.AddDays(6);
                    break;
                case vrmSearchType.ThisMonth:
                    from = timeZone.GetFirstDayOfMonth(today.Month);
                    to = timeZone.GetLastDayOfMonth(today.Month);
                    break;
                case vrmSearchType.Cutom:
                    from = fromDate;
                    to = toDate;
                    break;
                case vrmSearchType.Yesterday:
                    from = today.AddDays(-1);
                    to = today.AddDays(-1);
                    break;
                case vrmSearchType.Tomorrow:
                    from = today.AddDays(1);
                    to = today.AddDays(1);
                    break;
                default:
                    return false;

            }
            if (setTime)
            {
                fromDate = new DateTime(from.Year, from.Month, from.Day, fHr, fMin, fSec);
                toDate = new DateTime(to.Year, to.Month, to.Day, tHr, tMin, tSec);
            }
            return true;
        }
        #endregion

        #region getDeptLevelCriterion
        /// <summary>
        /// getDeptLevelCriterion
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criterium"></param>
        /// <returns></returns>
        #region Not Used
        /*
        private bool getDeptLevelCriterion_X(vrmUser user, ref ICriterion criterium)
        {
            try
            {
                // superadmin, nothing to do!
                if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                    return false;
                string stmt = "SELECT DISTINCT c.confid FROM myVRM.DataLayer.vrmConfUser c WHERE c.userid = " + user.userid.ToString();
                IList result = m_IconfDAO.execQuery(stmt);
                //List<int> userList = new List<int>(); //FB 1158
                confsList = new List<int>();    //FB 1158
                if (result.Count > 0)
                {
                    foreach (object obj in result)
                    {//FB 1499
                        if (!confsList.Contains((int)obj))//FB 1499
                            confsList.Add((int)obj);    //FB 1158
                        //criterium = Expression.In("confid", confsList); //FB 1158
                    }//FB 1499
                }


                stmt = "SELECT DISTINCT c.confid FROM myVRM.DataLayer.vrmConference c WHERE c.owner = " + user.userid.ToString();
                result.Clear();
                result = m_IconfDAO.execQuery(stmt);
                if (result.Count > 0)
                {
                    foreach (object obj in result)
                    {//FB 1499
                        if (!confsList.Contains((int)obj))//FB 1499
                            confsList.Add((int)obj);    //FB 1158
                    }//FB 1499
                    criterium = Expression.In("confid", confsList); //FB 1158 //FB 1499
                }
                else//FB 1499
                {//FB 1499
                    if (confsList.Count > 0)//FB 1499
                        criterium = Expression.In("confid", confsList); //FB 1499
                }//FB 1499
                if (user.Admin == vrmUserConstant.ADMIN)
                {
                    List<vrmUserDepartment> deptList =
                        GetDepartmentSelectList(GetUserDepartmentList(user));

                    List<int> deptUser = new List<int>();

                    foreach (vrmUserDepartment ud in deptList)
                        if (ud.userId != user.userid)
                            deptUser.Add(ud.userId);

                    if (deptUser.Count < 1)//FB 1499
                        deptUser.Add(user.userid);//FB 1499

                    if (criterium != null)//FB 1322,1387
                        criterium = Expression.Or(criterium, Expression.In("owner", deptUser));
                    else//FB 1322,1387
                        criterium = Expression.In("owner", deptUser);//FB 1322,1387 
                }
                // FB 434 allow public conferences
                //criterium = Expression.Or(criterium,Expression.Eq("isPublic",1));//FB 1322,1387
                //confsList = null;//FB 1499
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        */
        #endregion

        private bool getDeptLevelCriterion(vrmUser user, ref ICriterion criterium, List<int> confnumname) //FB 2870
        {
            try
            {
                // superadmin, nothing to do!
                if ((user.Admin == vrmUserConstant.SUPER_ADMIN) || (user.Admin == vrmUserConstant.VNOCADMIN)) //FB 2670
                    return false;

                string stmt = null;
                //Conference can be shown if the user is participant

                if (confnumname.Count > 0) //FB 2870
                    stmt = "SELECT DISTINCT c.confid FROM myVRM.DataLayer.vrmConfUser c, myVRM.DataLayer.vrmConference vc " +
                              " WHERE vc.confnumname in (" + confnumname + ") and vc.deleted=0 and c.userid = " + user.userid.ToString() + //FB 2870
                              " and c.confid=vc.confid";
                else
                    stmt = "SELECT DISTINCT c.confid FROM myVRM.DataLayer.vrmConfUser c, myVRM.DataLayer.vrmConference vc " +
                              " WHERE vc.deleted=0 and c.userid = " + user.userid.ToString() +
                              " and c.confid=vc.confid";

                IList result = m_IconfDAO.execQuery(stmt);

                List<int> userList = new List<int>();
                if (result.Count > 0)
                {
                    for (int i = 0; i < result.Count; i++) //FB 1787
                    {
                        if (!userList.Contains((int)result[i]))
                            userList.Add((int)result[i]);
                    }
                    criterium = Expression.In("confid", userList);
                }
                
                /* *** FB 1158 - Approval pending confs are not displayed in the general user - start *** */
                //Need to show the conference if user is approver
                //Following block is added here during FB 1787 as this functionality is lost during FB 1499
                //Method to get the list of conferences pending for the selected users approval
                if (isApprovalPending)
                {
                    stmt = "SELECT DISTINCT a.confid FROM myVRM.DataLayer.vrmConfApproval a, " +
                           " myVRM.DataLayer.vrmConference vc WHERE a.approverid = " + user.userid.ToString() + " and decision=0" +
                           " and vc.deleted=0 and vc.status = 1 and vc.confid = a.confid"; //FB 1716

                    result.Clear();
                    result = m_IconfDAO.execQuery(stmt);
                    
                    if (result.Count > 0)
                    {
                        confsList = new List<int>();
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (!confsList.Contains((int)result[i]))
                                confsList.Add((int)result[i]);

                            if (!userList.Contains((int)result[i]))
                                userList.Add((int)result[i]);
                        }
                        criterium = Expression.In("confid", userList);
                    }
                }
                /* *** FB 1158 - Approval pending confs are not displayed in the general user - end *** */

                //Following block of code is commented for FB 1787
                /*
                stmt = "SELECT DISTINCT c.confid FROM myVRM.DataLayer.vrmConference c WHERE c.owner = " + user.userid.ToString();
                result.Clear();
                result = m_IconfDAO.execQuery(stmt);
                if (result.Count > 0)
                {
                    foreach (object obj in result)
                        userList.Add((int)obj);

                    criterium = Expression.In("confid", userList);
                }
                */

                //Conference can be shown if the user is host or conference belongs to user department in case of Admin

                List<int> deptUser = new List<int>();
                deptUser.Add(user.userid); //This is to check whether user is owner of the conference

                if (user.Admin == vrmUserConstant.ADMIN)
                {
                    //Codes Commented for FB 1787
                    /*
                    List<vrmUserDepartment> deptList =
                        GetDepartmentSelectList(GetUserDepartmentList(user));

                    Codes Commented for FB 1787
                    foreach (vrmUserDepartment ud in deptList)
                        if (ud.userId != user.userid)
                            deptUser.Add(ud.userId);
                    */

                    //FB 1787
                    stmt = "SELECT DISTINCT u.userId FROM myVRM.DataLayer.Usr_Dept_D u WHERE u.departmentId in " +
                        "(SELECT DISTINCT ud.departmentId FROM myVRM.DataLayer.Usr_Dept_D ud, myVRM.DataLayer.vrmDept d " +
                        " d.Deleted=0 and ud.departmentId=d.departmentId)";

                    result.Clear();
                    result = m_IuserDeptDAO.execQuery(stmt);
                    
                    if (result.Count > 0)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (!deptUser.Contains((int)result[i]))
                                deptUser.Add((int)result[i]);
                        }
                    }
                }

                //Criterium orders changed during FB 1787
                if (criterium == null)
                    criterium = Expression.Eq("isPublic", 1); //FB 434 allow public conferences
                else
                    criterium = Expression.Or(criterium, Expression.Eq("isPublic", 1));//FB 1322,1387

                criterium = Expression.Or(criterium, Expression.In("owner", deptUser)); //FB 1322,1387
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SearchConference-getDeptLevelCriterion: ", e);
                return false;
            }
        }
        #endregion

        //FB 1920 start
        #region getPublicMCUConfs
        //Fetching Confs which has been made with public MCUs
        /// <summary>
        /// getPublicMCUConfs
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criterium"></param>
        /// <returns></returns>
        private bool getPublicMCUConfs(vrmUser user, ref ICriterion criterium)
        {
            try
            {
                string stmt = "";
                if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                {
                    stmt = "select DISTINCT a.confid from myVRM.DataLayer.vrmConfApproval a,myVRM.DataLayer.vrmConference c" +
                           " where a.decision=0 and c.deleted=0 and c.status = 1 and c.confid = a.confid and c.orgId <> 11 " +
                           " and a.entitytype = 2 and a.entityid in (select BridgeID from myVRM.DataLayer.vrmMCU where isPublic=1 and deleted=0 and orgid=11)";
                }
                else
                {
                    //Need to show the list of conferences pending for the selected users approval
                    stmt = "SELECT DISTINCT a.confid FROM myVRM.DataLayer.vrmConfApproval a, " +
                           " myVRM.DataLayer.vrmConference c WHERE a.approverid = " + user.userid.ToString() + " and decision=0" +
                           " and c.orgId <> 11 and a.entitytype = 2 and c.deleted=0 and c.status = 1 and c.confid = a.confid";
                }

                if (stmt.Trim() == "")
                    return false;

                m_IconfDAO.clearFetch();
                IList result = m_IconfDAO.execQuery(stmt);
                if (result.Count > 0)
                {
                    confsList = new List<int>();
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (!confsList.Contains((int)result[i]))
                            confsList.Add((int)result[i]);
                    }
                    criterium = Expression.In("confid", confsList);
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SearchConference-getPublicMCUConf: ", e);
                return false;
            }
        }
        #endregion
        //FB 1920 end

        #region GetLocationsList
        /// <summary>
        /// GetLocationsList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLocationsList(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetLocationsList/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsList/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                StringBuilder outputXml = new StringBuilder();
                if (!GetRoomList(userID, ref outputXml))
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("Error in getting room list");
                    m_log.Error("Error in getting room list");
                    obj.outXml = "";
                    //FB 1881 end
                    return false;
                }
                obj.outXml = outputXml.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2027 Start

        #region GetSearchTemplateList
        /// <summary>
        /// GetSearchTemplateList(COM to .NET Convertion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSearchTemplateList(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            myVRMException myVRMEx = null;
            bool bRet = true;
            int userid = 0;
            int orgid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                IUserSearchTemplateDao searchTemplate = m_userDAO.GetUserSearchTemplateDao();
                List<ICriterion> templCriterion = new List<ICriterion>();
                templCriterion.Add(Expression.Eq("orgId", orgid));
                templCriterion.Add(Expression.Eq("userid", userid));//FB 2988
                List<vrmUserSearchTemplate> templateList = searchTemplate.GetByCriteria(templCriterion);
                outXml.Append("<getSearchTemplateList>");
                outXml.Append("<searchTemplates>");
                
                for(int t=0; t<templateList.Count; t++)
                {
                    outXml.Append("<searchTemplate>");
                    outXml.Append("<ID>" + templateList[t].id.ToString() + "</ID>");
                    outXml.Append("<name>" + templateList[t].name + "</name>");
                    outXml.Append("</searchTemplate>");
                }

                outXml.Append("</searchTemplates>");
                outXml.Append("</getSearchTemplateList>");
                obj.outXml = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2027 End

        #region SetSearchTemplate
        /// <summary>
        /// SetSearchTemplate
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetSearchTemplate(ref vrmDataObject obj)
        {
            try
            {
                string name, template;
                int id = 0;

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//SetSearchTemplate/TemplateID");
                string ID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetSearchTemplate/login");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetSearchTemplate/organizationID");
                string orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < 11)
                {
                    obj.outXml = myVRMException.toXml("Invalid Organization");
                    return false;
                }

                if (ID.Length < 1 || ID.ToLower() != "new")
                    int.TryParse(ID, out id); //FB 2670

                node = xd.SelectSingleNode("//SetSearchTemplate/TemplateName");
                name = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetSearchTemplate/SearchConference");
                template = node.OuterXml;

                vrmUserSearchTemplate temp = new vrmUserSearchTemplate();

                temp.id = id;
                temp.userid = Int32.Parse(userId);
                temp.name = name;
                temp.template = template;
                temp.orgId = organizationID;

                IUserSearchTemplateDao searchTemplate = m_userDAO.GetUserSearchTemplateDao();

                searchTemplate.SaveOrUpdate(temp);

                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                obj.outXml = string.Empty;
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetSearchTemplate
        /// <summary>
        /// GetSearchTemplate
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSearchTemplate(ref vrmDataObject obj)
        {
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetSearchTemplate/TemplateID");
                string ID = node.InnerXml.Trim();

                IUserSearchTemplateDao searchTemplate = m_userDAO.GetUserSearchTemplateDao();
                vrmUserSearchTemplate temp = searchTemplate.GetById(Int32.Parse(ID));

                obj.outXml = "<SearchTemplate>";
                obj.outXml += "<TemplateID>" + temp.id.ToString() + "</TemplateID>";
                obj.outXml += "<TemplateName>" + temp.name + "</TemplateName>";
                obj.outXml += temp.template;
                obj.outXml += "</SearchTemplate>";


                return true;
            }
            catch (Exception e)
            {
                obj.outXml = string.Empty;
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 1048 -- START

        #region GetRoomTimeZoneMapping
        /// <summary>
        /// GetRoomTimeZoneMapping
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomTimeZoneMapping(ref vrmDataObject obj)
        {
            DateTime SystemStartTime = DateTime.Now;
            DateTime SystemEndTime = DateTime.Now;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetRoomTimeZoneMapping/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                //FB 2274 Starts
                node = xd.SelectSingleNode("//GetRoomTimeZoneMapping/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                {
                    SystemStartTime = orgInfo.SystemStartTime;
                    SystemEndTime = orgInfo.SystemEndTime;
                }//Organization Module Fixes

                node = xd.SelectSingleNode("//GetRoomTimeZoneMapping/RoomID");
                int roomid = Convert.ToInt32(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//GetRoomTimeZoneMapping/BaseTimeZoneID");
                int baseTimeZoneID = Convert.ToInt32(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//GetRoomTimeZoneMapping/StartDateTime");
                DateTime ConferenceTimeFrom = Convert.ToDateTime(node.InnerXml.Trim());
                DateTime StartTimeFrom = Convert.ToDateTime(node.InnerXml.Trim());
                DateTime StartTimeFrom1 = Convert.ToDateTime(node.InnerXml.Trim());
                DateTime RoomStartTime = StartTimeFrom1;

                vrmRoom Loc = m_IRoomDAO.GetById(roomid);
                string RoomName = Loc.Name;
                string RoomQueue = Loc.RoomQueue; //FB 2342
                int roomID = Loc.RoomID;
                int romTimeZoneID = Loc.TimezoneID;

                string OffConfDate = ConferenceTimeFrom.AddHours(12).ToShortDateString();//minus 12 hrs because conference starttime is actual time -12hrs
                string OffSysTime = SystemStartTime.ToShortTimeString();
                string OffSysEndTime = SystemEndTime.ToShortTimeString();

                string OffStartDateTime = OffConfDate + " " + OffSysTime;
                DateTime OffStartDateTime1 = Convert.ToDateTime(OffStartDateTime);

                DateTime OffStartDateTime2 = Convert.ToDateTime(OffStartDateTime);
                DateTime CheckOffStartDateTime = Convert.ToDateTime(OffStartDateTime);
                string OffEndDateTime = OffConfDate + " " + OffSysEndTime;
                DateTime CheckOffEndDateTime = Convert.ToDateTime(OffEndDateTime);

                int SysStartTime = SystemStartTime.Hour;
                int SysEndTime = SystemEndTime.Hour;

                timeZoneData timezone = new timeZoneData();
                timeZone.GetTimeZone(romTimeZoneID, ref timezone);
                string RoomTimeZoneName = timezone.StandardName;

                timeZone.GetTimeZone(baseTimeZoneID, ref timezone);
                string ConfTimeZoneName = timezone.StandardName;

                timeZone.changeToGMTTime(baseTimeZoneID, ref StartTimeFrom1); //convert base time zone to GMT
                timeZone.changeToGMTTime(romTimeZoneID, ref StartTimeFrom);  //convert Room time zone to GMT

                TimeSpan timediff = StartTimeFrom1 - StartTimeFrom; //get the difference between two timezone times
                RoomStartTime = RoomStartTime + timediff;
                OffStartDateTime1 = OffStartDateTime1 + timediff; //Office Hours Start Time

                TimeSpan timeDiff = StartTimeFrom - StartTimeFrom1; //get the difference between two timezone times

                int timediff1 = timeDiff.Hours;
                int timediff2 = timeDiff.Minutes;
                //DateTime offtime = RoomStartTime;

                obj.outXml += "<GetRoomTimeZoneMapping>";
                obj.outXml += "<RoomID>" + roomID + "</RoomID>";
                obj.outXml += "<RoomName>" + RoomName + "</RoomName>";
                obj.outXml += "<RoomTimeZoneName>" + RoomTimeZoneName + "</RoomTimeZoneName>";
                obj.outXml += "<ConfTimeZoneName>" + ConfTimeZoneName + "</ConfTimeZoneName>";
                obj.outXml += "<SystemStartTime>" + OffStartDateTime + "</SystemStartTime>";
                obj.outXml += "<SystemEndTime>" + OffEndDateTime + "</SystemEndTime>";
                obj.outXml += "<HourDifference>" + timediff1 + "</HourDifference>";
                obj.outXml += "<MinDifference>" + timediff2 + "</MinDifference>";


                List<int> suitableStatus = new List<int>(); //More Suitable time list 9 AM to 5 PM
                int suitabletime = 9;
                for (int i = 0; i < 9; i++)
                {
                    suitableStatus.Add(suitabletime);
                    suitabletime = suitabletime + 1;
                }
                List<int> notsuitableStatus = new List<int>();//not suitable time list 12 AM to 6 AM
                int notsuitabletime = 0;
                for (int i = 0; i <= 6; i++)
                {
                    notsuitableStatus.Add(notsuitabletime);
                    notsuitabletime = notsuitabletime + 1;
                }
                List<int> notsuitableStatus1 = new List<int>(); //not suitable time list 8 PM to 11 PM   
                int notsuitabletime1 = 20;
                for (int i = 0; i <= 3; i++)
                {
                    notsuitableStatus1.Add(notsuitabletime1);
                    notsuitabletime1 = notsuitabletime1 + 1;
                }
                List<DateTime> checkSysAvailstatus = new List<DateTime>();

                for (int i = 0; CheckOffStartDateTime <= CheckOffEndDateTime; i++)
                {
                    checkSysAvailstatus.Add(CheckOffStartDateTime);
                    CheckOffStartDateTime = CheckOffStartDateTime.AddMinutes(30);
                }

                obj.outXml += "<Cells>";

                for (int i = 1; i <= 24; i++)
                {
                    int isOfficeHour = 0;
                    int timeStatus = 3;

                    obj.outXml += "<Cell>";
                    obj.outXml += "<StartTime>" + RoomStartTime + "</StartTime>";
                    obj.outXml += "<ConferenceTimeFrom>" + ConferenceTimeFrom + "</ConferenceTimeFrom>";


                    int roomtime = RoomStartTime.Hour;
                    int conftime = ConferenceTimeFrom.Hour;
                    int offTimeStatus = 3;

                    IsOfficeHour(checkSysAvailstatus, OffStartDateTime2, ref isOfficeHour);
                    if (isOfficeHour == 1) //For return the office Hour Datetime and status
                    {
                        int confTime = OffStartDateTime1.Hour;
                        if (MostSuitableTime(confTime, suitableStatus))
                            offTimeStatus = 2;
                        if (NotSuitableTime(confTime, notsuitableStatus, notsuitableStatus1))
                            offTimeStatus = 1;
                        obj.outXml += "<OffStartTime>" + OffStartDateTime1 + "</OffStartTime>";
                        obj.outXml += "<OffStatus>" + offTimeStatus + "</OffStatus>";
                        int offConftime = OffStartDateTime1.Hour;
                        OffStartDateTime1 = OffStartDateTime1.AddHours(1);
                    }

                    if (MostSuitableTime(roomtime, suitableStatus))
                        timeStatus = 2;

                    if (NotSuitableTime(roomtime, notsuitableStatus, notsuitableStatus1))
                        timeStatus = 1;

                    OffStartDateTime2 = OffStartDateTime2.AddHours(1);
                    obj.outXml += "<IsOfficeHour>" + isOfficeHour + "</IsOfficeHour>";
                    obj.outXml += "<Status>" + timeStatus + "</Status>";
                    ConferenceTimeFrom = ConferenceTimeFrom.AddHours(1);
                    RoomStartTime = RoomStartTime.AddHours(1);
                    obj.outXml += "</Cell>";
                }
                obj.outXml += "</Cells>";

                obj.outXml += "</GetRoomTimeZoneMapping>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region IsOfficeHour
        /// <summary>
        /// IsOfficeHour
        /// </summary>
        /// <param name="checkSysAvailstatus"></param>
        /// <param name="RoomStartTime"></param>
        /// <param name="isOfficeHour"></param>
        public void IsOfficeHour(List<DateTime> checkSysAvailstatus, DateTime RoomStartTime, ref int isOfficeHour)
        {

            if (checkSysAvailstatus.Contains(RoomStartTime))
                isOfficeHour = 1;

            //if (RoomStartTime >= SystemStartTime && RoomStartTime <= SystemEndTime)
            //    isOfficeHour = 1;
        }
        #endregion

        #region MostSuitableTime
        /// <summary>
        /// MostSuitableTime
        /// </summary>
        /// <param name="RoomStartTime"></param>
        /// <param name="suitableStatus"></param>
        /// <returns></returns>
        public bool MostSuitableTime(int RoomStartTime, List<int> suitableStatus)
        {

            if (suitableStatus.Contains(RoomStartTime))
                return true;

            return false;

        }
        #endregion

        #region NotSuitableTime
        /// <summary>
        /// NotSuitableTime
        /// </summary>
        /// <param name="RoomStartTime"></param>
        /// <param name="notsuitableStatus"></param>
        /// <param name="notsuitableStatus1"></param>
        /// <returns></returns>
        public bool NotSuitableTime(int RoomStartTime, List<int> notsuitableStatus, List<int> notsuitableStatus1)
        {

            if (notsuitableStatus.Contains(RoomStartTime) || notsuitableStatus1.Contains(RoomStartTime))
                return true;

            return false;
        }
        #endregion

        #region DeleteSearchTemplate
        /// <summary>
        /// DeleteSearchTemplate
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteSearchTemplate(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//DeleteSearchTemplate/TemplateID");
                string ID = node.InnerXml.Trim();

                IUserSearchTemplateDao searchTemplate = m_userDAO.GetUserSearchTemplateDao();
                IUserDao userDAO = m_userDAO.GetUserDao();
                IInactiveUserDao userDAOInactive = m_userDAO.GetInactiveUserDao();
                // check if search template is bieng used if it is return an error, else delete it (FB 181)
                vrmUserSearchTemplate temp = new vrmUserSearchTemplate();
                temp = searchTemplate.GetById(Int32.Parse(ID));

                List<ICriterion> Criterion = new List<ICriterion>();
                Criterion.Add(Expression.Eq("searchId", temp.id));

                // FB 181 do not allow search template delete if bieng used by user and inactive user
                List<vrmUser> users = userDAO.GetByCriteria(Criterion);
                if (users.Count > 0)
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("Error template is in use. Cannot delete");
                    myVRMException myVRMEx = new myVRMException(438);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
                else
                {
                    //FB 2617

                    //// now search inactive also 
                    //List<vrmInactiveUser> InactiveUsers = userDAOInactive.GetByCriteria(Criterion);
                    //if (InactiveUsers.Count > 0)
                    //{
                    //    //FB 1881 start
                    //    //obj.outXml = myVRMException.toXml("Error template is in use. Cannot delete");
                    //    myVRMException myVRMEx = new myVRMException(438);
                    //    obj.outXml = myVRMEx.FetchErrorMsg();
                    //    //FB 1881 end
                    //    return false;
                    //}


                    // ok to delete     
                    searchTemplate.Delete(temp);
                    return true;
                }
            }
            catch (Exception e)
            {
                obj.outXml = string.Empty;
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region CheckConfAuthorization
        /// <summary>
        /// CheckConfAuthorization
        /// check if user is authorized to see this conference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private bool CheckConfAuthorization(vrmConference conf, int userId)
        {
            try
            {
                vrmUser user = m_IuserDAO.GetByUserId(userId);
                return CheckConfAuthorization(conf, user);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region CheckConfAuthorization
        /// <summary>
        /// CheckConfAuthorization
        /// user is authorized if 
        /// 1. they are SUPER_ADMIN
        /// 2. if it is their conference (owner)
        /// 3. if they are a participant (in confuser list)
        /// 4. if conference is public
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool CheckConfAuthorization(vrmConference conf, vrmUser user) //ZD 100263
        {
            try
            {
                // super admin needs now security / Admin has already been applied. 
                if (user.Admin == vrmUserConstant.SUPER_ADMIN || user.Admin == vrmUserConstant.ADMIN || user.Admin == vrmUserConstant.VNOCADMIN) //FB 2670
                    return true;
                if (conf.owner == user.userid)
                    return true;
                if (conf.userid == user.userid) //ZD 100263
                    return true;

                if (conf.isPublic == 1)
                    return true;
                
                foreach (vrmConfUser cu in conf.ConfUser)
                    if (cu.userid == user.userid)
                        return true;

                foreach (vrmConfVNOCOperator cv in conf.ConfVNOCOperator) //FB 2670
                    if (cv.vnocId == user.userid)
                        return true;

                /* *** FB 1158 - Approval Issues  ... start *** */
                if (isApprovalPending)
                {
                    if (confsList != null)
                    {
                        if (confsList.Contains(conf.confid))
                            return true;
                    }
                }
                /* *** FB 1158 - Approval Issues  ... end *** */

                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2501 VNOC Start
        //Method changed for FB 2670
        #region CheckConfVNOCAuthorization
        /// <summary>
        /// CheckConfAuthorization
        /// user is authorized if 
        /// 1. they are SUPER_ADMIN
        /// 2. if it is their conference (owner)
        /// 3. if they are a participant (in confuser list)
        /// 4. if conference is public
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool CheckConfVNOCAuthorization(vrmConference conf, vrmUser user)
        {
            try
            {
                if (conf.ConfVNOCOperator.Count > 0)
                {
                    for (int vn = 0; vn < conf.ConfVNOCOperator.Count; vn++)
                    {
                        vrmConfVNOCOperator ConfVNOCList = conf.ConfVNOCOperator[vn];
                        if (ConfVNOCList.vnocId != 0)
                        {
                            // super admin needs now security / Admin has already been applied. 
                            if (user.Admin == vrmUserConstant.SUPER_ADMIN || user.Admin == vrmUserConstant.ADMIN)
                                return true;
                            if (ConfVNOCList.vnocId == user.userid)
                                return true;

                            /* *** Not IN USE *** */
                            if (isApprovalPending)
                            {
                                if (confsList != null)
                                {
                                    if (confsList.Contains(conf.confid))
                                        return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2501 VNOC End
        
        //Method added for Reccurence fixes -- Start

        #region GetIfDirtyorPast
        /// <summary>
        /// GetIfDirtyorPast
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetIfDirtyorPast(ref vrmDataObject obj)
        {

            bool IsDirty = false;
            bool pastSet = false;
            bool IspastSet = false;
            string outXml = "";
            string appntXml = "";
            string appntDateXml = "";
            string presentInstance = "";
            string srtInstance = "";
            int insCnt = 0;
            Double minDiff = 0;
            bool isFutureIns = false;
            string appntxml1 = "";//FB 2438

            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmConference> confList = new List<vrmConference>();

                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("conference/userInfo/userId");
                string userId = "";
                if (node != null)
                {
                    userId = node.InnerXml.Trim();

                    XmlNode node2 = xd.SelectSingleNode("conference/userInfo");
                    node2.RemoveChild(node);
                }
                else
                {
                    obj.outXml = xd.InnerXml;
                    return true;
                }
                node = xd.SelectSingleNode("conference/confInfo/confID");
                string conferenceID = "";
                if (node != null)
                    conferenceID = node.InnerXml.Trim();
                else
                {
                    obj.outXml = xd.InnerXml;
                    return true;
                }

                List<ICriterion> userCriterion = new List<ICriterion>();
                userCriterion.Add(Expression.Eq("userid", Int32.Parse(userId)));
                List<vrmUser> userList = m_IuserDAO.GetByCriteria(userCriterion);
                vrmUser user = userList[0];

                node = xd.SelectSingleNode("conference/confInfo/recurrencePattern/startDates");
                if (node != null)
                {
                    srtInstance = node.InnerXml.Trim();
                }
                else
                {
                    node = xd.SelectSingleNode("conference/confInfo/recurrenceRange/startDate");
                    if (node != null)
                    {
                        srtInstance = node.InnerXml.Trim();
                    }

                    node = xd.SelectSingleNode("conference/confInfo/appointmentTime/startHour");
                    if (node != null)
                        srtInstance += " " + node.InnerXml.Trim();
                    else
                        srtInstance += " 00";

                    node = xd.SelectSingleNode("conference/confInfo/appointmentTime/startMin");
                    if (node != null)
                        srtInstance += ":" + node.InnerXml.Trim();
                    else
                        srtInstance += ":00";

                    node = xd.SelectSingleNode("conference/confInfo/appointmentTime/startSet");
                    if (node != null)
                        srtInstance += " " + node.InnerXml.Trim();
                    else
                        srtInstance += " AM";

                    DateTime frstDate = DateTime.Now;
                    DateTime nowDate = DateTime.Now;

                    if (srtInstance != "")
                        DateTime.TryParse(srtInstance, out frstDate);

                    //timeZone.userPreferedTime(user.TimeZone, ref frstDate);
                    //timeZone.userPreferedTime(user.TimeZone, ref nowDate);

                    TimeSpan span = nowDate.Subtract(frstDate);
                    minDiff = span.TotalMinutes;

                    if (minDiff < 5)
                        isFutureIns = true;
                }

                CConfID ConfMode = new CConfID(conferenceID);
                if (ConfMode.ID.ToString().Trim().Length > 0)
                {
                    criterionList.Add(Expression.Eq("confid", ConfMode.ID));
                }

                confList = m_IconfDAO.GetByCriteria(criterionList);

                if (confList.Count <= 0)
                {
                    obj.outXml = xd.InnerXml;
                    return true;
                }

                DateTime checkTime = DateTime.Now;
                DateTime serverTime = DateTime.Now;//FB 1390
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);//FB 1390
                timeZone.userPreferedTime(user.TimeZone, ref serverTime);//FB 1390

                string bufferXml = "";  //Buffer zone

                IspastSet = false;

                foreach (vrmConference conf in confList)
                {
                    if ((conf.status == 0 || conf.status == 1) && conf.deleted == 0)
                    {

                        DateTime confDateTime = conf.confdate;
                        timeZone.userPreferedTime(user.TimeZone, ref confDateTime);
                        DateTime confTimezonedate = confDateTime;  //Code for timezone issue


                        if (confDateTime < serverTime)//FB 1390
                        {
                            IspastSet = true;
                            continue;
                        }

                        insCnt++;
						//FB 2438 - Start
                        if (confList.FirstOrDefault().ConfRoom.Count == conf.ConfRoom.Count)
                        {
                            for (int i = 0; i < conf.ConfRoom.Count; i++)
                            {
                                if (confList.FirstOrDefault().ConfRoom[i].roomId != Convert.ToInt32(conf.ConfRoom[i].roomId))
                                {
                                    if(!outXml.Contains("<RoomInstance>1</RoomInstance>"))
                                        appntxml1 += "<RoomInstance>1</RoomInstance>";
                                }
                            }
                        }
                        else
                        {
                            if (!outXml.Contains("<RoomInstance>1</RoomInstance>"))
                                appntxml1 += "<RoomInstance>1</RoomInstance>";
                        }
						//FB 2438 - End                     
                        if (!pastSet)
                        {
                            pastSet = true;

                            checkTime = confDateTime; 
                            
                            timeZone.changeToGMTTime(user.TimeZone, ref confTimezonedate); //Code for timezone issue
                            timeZone.userPreferedTime(conf.timezone, ref confTimezonedate);
                            presentInstance = confTimezonedate.ToString("MM/dd/yyyy"); 


                            /* *** Buffer zone start *** */

                            TimeSpan span = conf.SetupTime.Subtract(conf.confdate);
                            int setupDur = Convert.ToInt32(span.TotalMinutes);

                            span = conf.TearDownTime.Subtract(conf.confdate);
                            double tempDur = span.TotalMinutes;
                            int tearDownDur = conf.duration - Convert.ToInt32(tempDur);

                            if (tearDownDur < 0)
                                tearDownDur = 0;

                            if (setupDur < 0)
                                setupDur = 0;

                            bufferXml += "<SetupDur>" + setupDur.ToString() + "</SetupDur>";
                            bufferXml += "<TearDownDur>" + tearDownDur.ToString() + "</TearDownDur>";

                            /* *** Buffer zone end *** */

                            appntXml = "<timeZone>" + conf.timezone.ToString() + "</timeZone>";
                            appntXml += "<startHour>" + confTimezonedate.ToString("hh") + "</startHour>";  //Code for  timezone issue
                            appntXml += "<startMin>" + confTimezonedate.Minute + "</startMin>";  //Code for  timezone issue
                            appntXml += "<startSet>" + confTimezonedate.ToString("tt") + "</startSet>"; //Code for  timezone issue
                            appntXml += "<durationMin>" + conf.duration.ToString() + "</durationMin>";

                            appntXml += "<durationMin>" + conf.duration.ToString() + "</durationMin>";

                            //Set the latest information
                            if (xd.SelectSingleNode("/conference/confInfo/Status") != null)
                            {
                                xd.SelectSingleNode("/conference/confInfo/Status").InnerText = conf.status.ToString();
                            }

                            if (xd.SelectSingleNode("/conference/confInfo/confName") != null)
                            {
                                xd.SelectSingleNode("/conference/confInfo/confName").InnerText = conf.externalname;
                            }

                            if (xd.SelectSingleNode("/conference/confInfo/confPassword") != null)
                            {
                                xd.SelectSingleNode("/conference/confInfo/confPassword").InnerText = conf.password;
                            }
                            if (xd.SelectSingleNode("/conference/confInfo/confUniqueID") != null)
                            {
                                xd.SelectSingleNode("/conference/confInfo/confUniqueID").InnerText = conf.confnumname.ToString();
                            }
                            //if (xd.SelectSingleNode("/conference/confInfo/hostId") != null)
                            //{
                            //    xd.SelectSingleNode("/conference/confInfo/hostId").InnerText = conf.owner.ToString();
                            //}

                            //FB 2438
                            //if (isFutureIns)
                            //{
                            //    obj.outXml = xd.InnerXml;
                            //    return true;
                            //}
                        }

                        if (!isFutureIns) //FB 2438
                            if (confDateTime.ToString("HH:mm:ss") != checkTime.ToString("HH:mm:ss"))
                                IsDirty = true;                                                     

                        outXml += "<instance>";
                        outXml += "<startDate>" + confDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                        outXml += "<startHour>" + confDateTime.ToString("hh") + "</startHour>";
                        outXml += "<startMin>" + confDateTime.Minute + "</startMin>";
                        outXml += "<startSet>" + confDateTime.ToString("tt") + "</startSet>";
                        outXml += " <durationMin>" + conf.duration.ToString() + "</durationMin>";
                        outXml += "</instance>";


                        appntDateXml += "<startDate>" + confDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                    }
                    else
                        IspastSet = true;

                }

                if (IsDirty || IspastSet) //FB 1770( || !isFutureIns) //Diagonstics )
                {
                    if(outXml != "" && appntXml != "" && appntDateXml != "") //FB 1770
                    {
                        if (IsDirty)
                        {
                            outXml = "<customInstance>1</customInstance><instances>" + outXml + "</instances>";
                            appntXml += outXml;

                        }

                        node = xd.SelectSingleNode("conference/confInfo/recurrenceRange/occurrence");
                        if (node != null)
                        {
                            node.InnerText = "";
                            node.InnerText = insCnt.ToString().Trim();
                        }

                        node = xd.SelectSingleNode("conference/confInfo/recurrencePattern/startDates");
                        if (node != null)
                        {
                            node.InnerXml = "";
                            node.InnerXml = appntDateXml;
                        }

                        node = xd.SelectSingleNode("conference/confInfo/recurrenceRange/startDate");
                        if (node != null)
                        {
                            node.InnerText = "";
                            node.InnerText = presentInstance;

                        }

                        node = xd.SelectSingleNode("conference/confInfo/appointmentTime");
                        if (node != null)
                        {
                            node.InnerXml = "";
                            node.InnerXml = appntXml;
                        }

                        node = xd.SelectSingleNode("conference/confInfo/bufferZone");
                        if (node != null)
                        {
                            node.InnerXml = "";
                            node.InnerXml = bufferXml;
                        }
                    }
                }
                //FB 2438 - Start
                node = xd.SelectSingleNode("conference/confInfo/locationList/selected");
                if (node != null)
                {
                    //ZD #100222 Start
                    //node.InnerXml = "";
                    node.InnerXml = appntxml1 + node.InnerXml;
                    //ZD #100222 End
                }
                //FB 2438 - End

                obj.outXml = xd.InnerXml;
            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message); //FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }

        #endregion

        /* *** FB 1391 -- Start *** */

        #region GetIfDirty
        /// <summary>
        /// GetIfDirty
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetIfDirty(ref vrmDataObject obj)
        {

            string IsDirty = "0";
            bool pastSet = false;

            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmConference> confList = new List<vrmConference>();

                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("confID");
                string conferenceID = "";

                if (node != null)
                    conferenceID = node.InnerXml.Trim();



                CConfID ConfMode = new CConfID(conferenceID);

                if (ConfMode.instance <= 0)
                {
                    if (ConfMode.ID.ToString().Trim().Length > 0)
                        criterionList.Add(Expression.Eq("confid", ConfMode.ID));

                    if (criterionList.Count > 0)
                    {

                        List<vrmRecurInfo> recurPattern = m_vrmConfRecurDAO.GetByCriteria(criterionList, true);

                        if (recurPattern.Count > 0)
                        {

                            if (recurPattern[0].recurType == 5 || recurPattern[0].dirty == 1)
                            {

                                confList = m_IconfDAO.GetByCriteria(criterionList);

                                if (confList.Count > 0)
                                {
                                    DateTime checkTime = DateTime.Now;
                                    int duration = 0;

                                    pastSet = false;

                                    foreach (vrmConference conf in confList)
                                    {
                                        if ((conf.status == 0 || conf.status == 1) && conf.deleted == 0)
                                        {
                                            DateTime confDateTime = conf.confdate;
                                            if (!pastSet)
                                            {
                                                duration = conf.duration;
                                                checkTime = confDateTime;
                                                pastSet = true;
                                            }

                                            if (confDateTime.ToString("HH:mm:ss") != checkTime.ToString("HH:mm:ss") || duration != conf.duration)
                                            {
                                                IsDirty = "1";
                                                break;
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }
                }

                obj.outXml = "<IsDirty>" + IsDirty + "</IsDirty>";
            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        /* *** FB 1391 -- End *** */

        /* *** Method added for Reccurence -- End *** */

        /* *** FB 1158 - Approval Issues ... start *** */

        #region GetConfsListForApproval
        /// <summary>
        /// Method to get the list of conferences pending for the selected users approval
        /// </summary>
        /// <param name="approverID"></param>
        private void GetConfsListForApproval(string approverID)
        {
            if (approverID == "")
                return;

            string qry = "SELECT DISTINCT c.confid FROM myVRM.DataLayer.vrmConfApproval c WHERE c.approverid = " + approverID + " and decision=0";

            IList confs = m_IconfDAO.execQuery(qry);
            if (confs.Count > 0)
            {
                if (confsList == null)
                    confsList = new List<int>();

                foreach (object obj in confs)
                    confsList.Add((int)obj);
            }
        }
        #endregion
        /* *** FB 1158 - Approval Issues ... end *** */
        /** Code added for Activation -- Start **/
        #region GetEncrpytedText
        /// <summary>
        /// Method to get the encryptedText
        /// </summary>
        public bool GetEncrpytedText(ref vrmDataObject obj)
        {

            string cipherText = "";
            try
            {
                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("System/Cipher");
                if (node != null)
                    cipherText = node.InnerXml.Trim();

                if (cipherText != "")
                {
                    cryptography.Crypto crypt = new cryptography.Crypto();
                    cipherText = crypt.encrypt(cipherText);

                }
                else
                    throw new Exception("Invalid Details");

                obj.outXml = "<System><Cipher>" + cipherText + "</Cipher></System>";

                return true;

            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
        /** Code added for Activation -- End **/

        #region GetAllRooms
        /// <summary>
        /// <GetRoomProfile>
        ///   <UserID></UserID>
        ///   <RoomID></RoomID>
        /// </GetRoomProfile>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// //FB 1756 - Changed the outXML String to StringBuilder
        public bool GetAllRooms(ref vrmDataObject obj)
        {

            List<vrmRoom> locRooms = null;
            vrmUser userInfo = null;
            string avComps = "";
            string rmName = "";
            int u = 0;
            string nne = "1";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node = xd.SelectSingleNode("//GetAllRooms/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);


                List<ICriterion> criterionListorg = new List<ICriterion>();
                criterionListorg.Add(Expression.Eq("orgId", organizationID));

                locRooms = m_IRoomDAO.GetByCriteria(criterionListorg);

                StringBuilder strOutXML = new StringBuilder();

                strOutXML.Append("<Rooms>");

                foreach (vrmRoom locRoom in locRooms)
                {
                    avComps = "";

                    if (locRoom.Name != "Phantom Room")
                    {
                        strOutXML.Append("<Room>");

                        strOutXML.Append("<RoomID>" + locRoom.roomId.ToString() + "</RoomID>");
                        strOutXML.Append("<RoomName>" + locRoom.Name + "</RoomName>");
                        strOutXML.Append("<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>");
                        strOutXML.Append("<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>");
                        strOutXML.Append("<MaximumConcurrentPhoneCalls>" + locRoom.MaxPhoneCall.ToString() + "</MaximumConcurrentPhoneCalls>");
                        strOutXML.Append("<SetupTime>" + locRoom.SetupTime.ToString() + "</SetupTime>");
                        strOutXML.Append("<TeardownTime>" + locRoom.TeardownTime.ToString() + "</TeardownTime>");
                        strOutXML.Append("<AssistantInchargeID>" + locRoom.assistant.ToString() + "</AssistantInchargeID>");
                        userInfo = m_IuserDAO.GetByUserId(locRoom.assistant);
                        if (userInfo != null)
                        {
                            strOutXML.Append("<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>");
                        }
                        else
                        {
                            strOutXML.Append("<AssistantInchargeName></AssistantInchargeName>");
                        }
                        strOutXML.Append("<MultipleAssistantEmails>" + locRoom.notifyemails + "</MultipleAssistantEmails>");
                        if (locRoom.tier2 != null) //Fix for Glowpoint client
                        {
                            if (locRoom.tier2.tier3 != null)
                            {
                                strOutXML.Append("<Tier1ID>" + locRoom.tier2.tier3.ID.ToString() + "</Tier1ID>");
                                strOutXML.Append("<Tier1Name>" + locRoom.tier2.tier3.Name + "</Tier1Name>");
                            }
                            else
                            {
                                strOutXML.Append("<Tier1ID></Tier1ID>");
                                strOutXML.Append("<Tier1Name></Tier1Name>");
                            }
                            strOutXML.Append("<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>");
                            strOutXML.Append("<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>");
                        }
                        else
                        {
                            strOutXML.Append("<Tier1ID></Tier1ID>");
                            strOutXML.Append("<Tier1Name></Tier1Name>");
                            strOutXML.Append("<Tier2ID></Tier2ID>");
                            strOutXML.Append("<Tier2Name></Tier2Name>");
                        }
                        strOutXML.Append("<CatererFacility>" + locRoom.Caterer.ToString() + "</CatererFacility>");
                        strOutXML.Append("<DynamicRoomLayout>" + locRoom.DynamicRoomLayout.ToString() + "</DynamicRoomLayout>");
                        strOutXML.Append("<ProjectorDefault>" + locRoom.ProjectorAvailable.ToString() + "</ProjectorDefault>");
                        strOutXML.Append("<Video>" + locRoom.VideoAvailable.ToString() + "</Video>");
                        strOutXML.Append("<Floor>" + locRoom.RoomFloor + "</Floor>");
                        strOutXML.Append("<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>");
                        strOutXML.Append("<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>");
                        strOutXML.Append("<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>");
                        strOutXML.Append("<City>" + locRoom.City + "</City>");
                        strOutXML.Append("<State>" + locRoom.State.ToString() + "</State>");
                        strOutXML.Append("<Disabled>" + locRoom.disabled.ToString() + "</Disabled>");
                        strOutXML.Append("<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>");
                        strOutXML.Append("<isVIP>" + locRoom.isVIP.ToString() + "</isVIP>");
                        if (locRoom.State > 0)
                        {
                            vrmState objState = m_IStateDAO.GetById(locRoom.State);
                            strOutXML.Append("<StateName>" + objState.StateCode.ToString() + "</StateName>");
                        }
                        else
                        {
                            strOutXML.Append("<StateName></StateName>");
                        }
                        strOutXML.Append("<ZipCode>" + locRoom.Zipcode + "</ZipCode>");
                        strOutXML.Append("<Country>" + locRoom.Country.ToString() + "</Country>");
                        if (locRoom.Country > 0)
                        {
                            vrmCountry objCountry = m_ICountryDAO.GetById(locRoom.Country);
                            strOutXML.Append("<CountryName>" + objCountry.CountryName + "</CountryName>");
                        }
                        else
                        {
                            strOutXML.Append("<CountryName></CountryName>");
                        }
                        strOutXML.Append("<MapLink>" + locRoom.Maplink + "</MapLink>");
                        strOutXML.Append("<ParkingDirections>" + locRoom.ParkingDirections + "</ParkingDirections>");
                        strOutXML.Append("<AdditionalComments>" + locRoom.AdditionalComments + "</AdditionalComments>");
                        strOutXML.Append("<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>");
                        if (locRoom.TimezoneID > 0)
                        {
                            timeZoneData tz = new timeZoneData();
                            timeZone.GetTimeZone(locRoom.TimezoneID, ref tz);
                            strOutXML.Append("<TimezoneName>" + tz.TimeZone + "</TimezoneName>");
                        }
                        else
                        {
                            strOutXML.Append("<TimezoneName></TimezoneName>");
                        }
                        strOutXML.Append("<Longitude>" + locRoom.Longitude + "</Longitude>");
                        strOutXML.Append("<Latitude>" + locRoom.Latitude + "</Latitude>");

                        int i = 1;
                        foreach (vrmLocApprover locRoomApprov in locRoom.locationApprover)
                        {
                            userInfo = m_IuserDAO.GetByUserId(locRoomApprov.approverid);
                            strOutXML.Append("<Approver" + i.ToString() + "ID>" + locRoomApprov.approverid.ToString() + "</Approver" + i.ToString() + "ID>");
                            strOutXML.Append("<Approver" + i.ToString() + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "  </Approver" + i.ToString() + "Name>");
                            i++;
                        }
                        if (i == 1)
                        {
                            strOutXML.Append("<Approver1ID></Approver1ID>");
                            strOutXML.Append("<Approver1Name></Approver1Name>");
                            strOutXML.Append("<Approver2ID></Approver2ID>");
                            strOutXML.Append("<Approver2Name></Approver2Name>");
                            strOutXML.Append("<Approver3ID></Approver3ID>");
                            strOutXML.Append("<Approver3Name></Approver3Name>");
                            strOutXML.Append("<ApprovalReq>No</ApprovalReq>");
                        }
                        else if (i == 2)
                        {
                            strOutXML.Append("<Approver2ID></Approver2ID>");
                            strOutXML.Append("<Approver2Name></Approver2Name>");
                            strOutXML.Append("<Approver3ID></Approver3ID>");
                            strOutXML.Append("<Approver3Name></Approver3Name>");
                            strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                        }
                        else if (i == 3)
                        {
                            strOutXML.Append("<Approver3ID></Approver3ID>");
                            strOutXML.Append("<Approver3Name></Approver3Name>");
                            strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                        }
                        strOutXML.Append("<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>");
                        List<vrmEndPoint> eptList = new List<vrmEndPoint>();
                        List<ICriterion> criterionListept = new List<ICriterion>();
                        criterionListept.Add(Expression.Eq("endpointid", locRoom.endpointid));
                        criterionListept.Add(Expression.Eq("deleted", 0));
                        eptList = m_vrmEpt.GetByCriteria(criterionListept);
                        if (eptList.Count > 0)
                        {
                            foreach (vrmEndPoint ept in eptList)
                            {
                                strOutXML.Append("<EndpointName>" + ept.name + "</EndpointName>");
                                strOutXML.Append("<EndpointIP>" + ept.address + "</EndpointIP>");
                                break;
                            }
                        }
                        else
                        {
                            strOutXML.Append("<EndpointName></EndpointName>");
                            strOutXML.Append("<EndpointIP></EndpointIP>");
                        }

                        //Image Project codelines start...

                        //strOutXML.Append( "<RoomImages>");

                        string roomImagesids = locRoom.RoomImageId;
                        string roomImagesnames = locRoom.RoomImage;

                        vrmImage imObj = null;
                        string imgDt = "";
                        string imagename = "";
                        int imageid = 0;
                        string fileext = "";

                        if (roomImagesids != null && roomImagesnames != null)
                        {
                            if (roomImagesids != "" && roomImagesnames != "")
                            {
                                imgDt = "";
                                imObj = null;
                                string[] idArr = roomImagesids.Split(',');
                                string[] nameArr = roomImagesnames.Split(',');
                                if (idArr.Length > 0 && nameArr.Length > 0)
                                {
                                    for (int lp = 0, k = 0; lp < 1; lp++, k++)
                                    {
                                        imagename = "";
                                        fileext = "jpg";

                                        //if(k < nameArr.Length)
                                        imagename = nameArr[k].ToString();

                                        if (imagename != "")
                                            fileext = imagename.Substring(imagename.LastIndexOf(".") + 1);

                                        Int32.TryParse(idArr[lp].ToString(), out imageid);

                                        imObj = m_IImageDAO.GetById(imageid);
                                        if (imObj == null)
                                            continue;

                                        imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136
                                    }
                                }
                            }
                        }
                        strOutXML.Append("<ImageName>" + imagename + "</ImageName>");
                        strOutXML.Append("<Imagetype>" + fileext + "</Imagetype>");
                        strOutXML.Append("<Image>" + imgDt + "</Image>");

                        //strOutXML.Append( "<Images>");

                        strOutXML.Append("<Map1>" + locRoom.MapImage1 + "</Map1>");
                        strOutXML.Append("<Map1Image>" + GetRoomImage(locRoom.MapImage1Id) + "</Map1Image>");

                        strOutXML.Append("<Map2>" + locRoom.MapImage2 + "</Map2>");
                        strOutXML.Append("<Map2Image>" + GetRoomImage(locRoom.MapImage2Id) + "</Map2Image>");

                        //FB 2136 Start
                        strOutXML.Append("<Security1>" + locRoom.SecurityImage1 + "</Security1>");
                        strOutXML.Append("<Security1ImageId>" + locRoom.SecurityImage1Id + "</Security1ImageId>");

                        //strOutXML.Append("<Security2>" + locRoom.SecurityImage2 + "</Security2>");
                        //strOutXML.Append("<Security2Image>" + GetRoomImage(locRoom.SecurityImage2Id) + "</Security2Image>");
                        //FB 2136 End

                        strOutXML.Append("<Misc1>" + locRoom.MiscImage1 + "</Misc1>");
                        strOutXML.Append("<Misc1Image>" + GetRoomImage(locRoom.MiscImage1Id) + "</Misc1Image>");

                        strOutXML.Append("<Misc2>" + locRoom.MiscImage2 + "</Misc2>");
                        strOutXML.Append("<Misc2Image>" + GetRoomImage(locRoom.MiscImage2Id) + "</Misc2Image>");

                        //strOutXML.Append( "</Images>");

                        //Image Project codelines end...

                        strOutXML.Append("<Custom1>" + locRoom.Custom1 + "</Custom1>");
                        strOutXML.Append("<Custom2>" + locRoom.Custom2 + "</Custom2>");
                        strOutXML.Append("<Custom3>" + locRoom.Custom3 + "</Custom3>");
                        strOutXML.Append("<Custom4>" + locRoom.Custom4 + "</Custom4>");
                        strOutXML.Append("<Custom5>" + locRoom.Custom5 + "</Custom5>");
                        strOutXML.Append("<Custom6>" + locRoom.Custom6 + "</Custom6>");
                        strOutXML.Append("<Custom7>" + locRoom.Custom7 + "</Custom7>");
                        strOutXML.Append("<Custom8>" + locRoom.Custom8 + "</Custom8>");
                        strOutXML.Append("<Custom9>" + locRoom.Custom9 + "</Custom9>");
                        strOutXML.Append("<Custom10>" + locRoom.Custom10 + "</Custom10>");
                        strOutXML.Append("<Department>");

                        string depts = "";

                        foreach (vrmLocDepartment locDept in locRoom.locationDept)
                        {
                            if (depts == "")
                                depts = locDept.departmentId.ToString();
                            else
                                depts += " , " + locDept.departmentId.ToString();

                        }
                        strOutXML.Append(depts + "</Department>");
                        string qString;
                        qString = "from myVRM.DataLayer.InventoryCategory  Cat, ";
                        qString += "myVRM.DataLayer.InventoryRoom Rm ";
                        qString += "where Rm.ICategory.ID = Cat.ID and Rm.IRoom.roomId = " + locRoom.roomId + " and Cat.deleted = 0";
                        qString += "and Cat.Type = 1";

                        IList CategoryList = m_InvCategoryDAO.execQuery(qString);
                        if (CategoryList.Count > 0)
                        {
                            foreach (object[] roomSets in CategoryList)
                            {

                                InventoryCategory ic = (InventoryCategory)roomSets[0];

                                foreach (AVInventoryItemList CatItem in ic.AVItemList)
                                {
                                    if (CatItem.deleted == 0)
                                        avComps += ", " + CatItem.name;
                                }
                            }
                        }

                        List<ICriterion> criterionListWO = new List<ICriterion>();
                        criterionListWO.Add(Expression.Eq("deleted", 0));
                        criterionListWO.Add(Expression.Eq("Type", 1));

                        List<InventoryList> InvList = m_InvListDAO.GetByCriteria(criterionListWO);

                        nne = "1";

                        if (InvList.Count > 0)
                        {
                            foreach (InventoryList it in InvList)
                            {
                                u = 0;
                                if (avComps.Contains(it.Name))
                                {
                                    u = 1;
                                    nne = "0";
                                }

                                if (it.Type == 1 && it.deleted == 0)
                                {

                                    rmName = it.Name;

                                    if (it.Name.Split(' ').Length > 1)
                                        rmName = it.Name.Split(' ')[0] + it.Name.Split(' ')[1];


                                    strOutXML.Append("<" + rmName + ">" + u.ToString() + "</" + rmName + ">");
                                }
                            }
                        }

                        strOutXML.Append("<None>" + nne + "</None>");
                        strOutXML.Append("</Room>");
                    }
                }

                strOutXML.Append("</Rooms>");

                obj.outXml = strOutXML.ToString();

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetAllRooms: " + ex.Message);
                return false;
            }
        }
        //Image Project
        private string GetRoomImage(int imageid)
        {
            vrmImage imObj = null;
            string imgDt = "";
            try
            {
                imObj = m_IImageDAO.GetById(imageid);
                if (imObj != null)
                    imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                return imgDt;
            }
            catch (Exception e)
            {
                m_log.Error("GetRoomImage: " + e.Message);
                return "";
            }
        }
        #endregion
		
		//FB 2136
        #region GetSecImage

        private string GetSecImage(int imageid)
        {
            vrmSecurityImage imObj = new vrmSecurityImage();
            string imgDt = "";
            try
            {
                imObj = m_ISecBadgeDao.GetByBadgeId(imageid);
                if (imObj != null)
                    imgDt = Convert.ToBase64String(imObj.BadgeImage);

                return imgDt;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException in GetSecImage", e);
                return imgDt;
            }
        }

        #endregion


        //FB 1756
        #region GetAllRoomsBasicInfo
        /// <summary>
        /// <GetRoomProfile>
        ///   <UserID></UserID>
        ///   <RoomID></RoomID>
        /// </GetRoomProfile>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// 
        //FB 1756 - Changed the outXML String to StringBuilder
        public bool GetAllRoomsBasicInfo(ref vrmDataObject obj)
        {
            List<vrmRoom> locRooms = null;
            vrmUser userInfo = null;
            string avComps = "";
            string rmName = "";
            int u = 0;
            string nne = "1";
            string roomIDs = "";
            int RoomID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node = xd.SelectSingleNode("//GetAllRoomsBasicInfo/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                
                // FB 2449 starts
                node = xd.SelectSingleNode("//GetAllRoomsBasicInfo/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;
                // FB 2449 end
                node = xd.SelectSingleNode("//GetAllRoomsBasicInfo/RoomID");
                roomIDs = "";
                if (node != null)
                {
                    roomIDs = node.InnerXml.Trim();
                    if(roomIDs != "")
                        Int32.TryParse(roomIDs, out RoomID);
                }


                List<ICriterion> criterionListorg = new List<ICriterion>();
                criterionListorg.Add(Expression.Eq("orgId", organizationID));

                if (RoomID > 0)
                    criterionListorg.Add(Expression.Eq("RoomID", RoomID));

                locRooms = m_IRoomDAO.GetByCriteria(criterionListorg);

                StringBuilder strOutXML = new StringBuilder();

                strOutXML.Append("<Rooms>");

                foreach (vrmRoom locRoom in locRooms)
                {
                    avComps = "";

                    if (locRoom.Name != "Phantom Room")
                    {
                        strOutXML.Append("<Room>");

                        strOutXML.Append("<RoomID>" + locRoom.roomId.ToString() + "</RoomID>");
                        strOutXML.Append("<RoomName>" + locRoom.Name + "</RoomName>");
                        strOutXML.Append("<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>");
                        strOutXML.Append("<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>");
                        strOutXML.Append("<MaximumConcurrentPhoneCalls>" + locRoom.MaxPhoneCall.ToString() + "</MaximumConcurrentPhoneCalls>");
                        strOutXML.Append("<SetupTime>" + locRoom.SetupTime.ToString() + "</SetupTime>");
                        strOutXML.Append("<TeardownTime>" + locRoom.TeardownTime.ToString() + "</TeardownTime>");
                        strOutXML.Append("<AssistantInchargeID>" + locRoom.assistant.ToString() + "</AssistantInchargeID>");
                        userInfo = m_IuserDAO.GetByUserId(locRoom.assistant);
                        if (userInfo != null)
                        {
                            strOutXML.Append("<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>");
                        }
                        else
                        {
                            strOutXML.Append("<AssistantInchargeName></AssistantInchargeName>");
                        }
                        strOutXML.Append("<MultipleAssistantEmails>" + locRoom.notifyemails + "</MultipleAssistantEmails>");
                        if (locRoom.tier2 != null) //Fix for Glowpoint client
                        {
                            if (locRoom.tier2.tier3 != null)
                            {
                                strOutXML.Append("<Tier1ID>" + locRoom.tier2.tier3.ID.ToString() + "</Tier1ID>");
                                strOutXML.Append("<Tier1Name>" + locRoom.tier2.tier3.Name + "</Tier1Name>");
                            }
                            else
                            {
                                strOutXML.Append("<Tier1ID></Tier1ID>");
                                strOutXML.Append("<Tier1Name></Tier1Name>");
                            }
                            strOutXML.Append("<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>");
                            strOutXML.Append("<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>");
                        }
                        else
                        {
                            strOutXML.Append("<Tier1ID></Tier1ID>");
                            strOutXML.Append("<Tier1Name></Tier1Name>");
                            strOutXML.Append("<Tier2ID></Tier2ID>");
                            strOutXML.Append("<Tier2Name></Tier2Name>");
                        }
                        strOutXML.Append("<CatererFacility>" + locRoom.Caterer.ToString() + "</CatererFacility>");
                        strOutXML.Append("<DynamicRoomLayout>" + locRoom.DynamicRoomLayout.ToString() + "</DynamicRoomLayout>");
                        strOutXML.Append("<ProjectorDefault>" + locRoom.ProjectorAvailable.ToString() + "</ProjectorDefault>");
                        strOutXML.Append("<Video>" + locRoom.VideoAvailable.ToString() + "</Video>");
                        strOutXML.Append("<Floor>" + locRoom.RoomFloor + "</Floor>");
                        strOutXML.Append("<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>");
                        strOutXML.Append("<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>");
                        strOutXML.Append("<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>");
                        strOutXML.Append("<City>" + locRoom.City + "</City>");
                        strOutXML.Append("<State>" + locRoom.State.ToString() + "</State>");
                        strOutXML.Append("<Disabled>" + locRoom.disabled.ToString() + "</Disabled>");
                        strOutXML.Append("<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>");
                        strOutXML.Append("<isTelePresence>" + locRoom.isTelepresence.ToString() + "</isTelePresence>");//FB 2400
                        if (locRoom.State > 0)
                        {
                            vrmState objState = m_IStateDAO.GetById(locRoom.State);
                            strOutXML.Append("<StateName>" + objState.StateCode.ToString() + "</StateName>");
                        }
                        else
                        {
                            strOutXML.Append("<StateName></StateName>");
                        }
                        strOutXML.Append("<ZipCode>" + locRoom.Zipcode + "</ZipCode>");
                        strOutXML.Append("<Country>" + locRoom.Country.ToString() + "</Country>");
                        if (locRoom.Country > 0)
                        {
                            vrmCountry objCountry = m_ICountryDAO.GetById(locRoom.Country);
                            strOutXML.Append("<CountryName>" + objCountry.CountryName + "</CountryName>");
                        }
                        else
                        {
                            strOutXML.Append("<CountryName></CountryName>");
                        }
                        strOutXML.Append("<MapLink>" + locRoom.Maplink + "</MapLink>");
                        strOutXML.Append("<ParkingDirections>" + locRoom.ParkingDirections + "</ParkingDirections>");
                        strOutXML.Append("<AdditionalComments>" + locRoom.AdditionalComments + "</AdditionalComments>");
                        strOutXML.Append("<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>");
                        if (locRoom.TimezoneID > 0)
                        {
                            timeZoneData tz = new timeZoneData();
                            timeZone.GetTimeZone(locRoom.TimezoneID, ref tz);
                            strOutXML.Append("<TimezoneName>" + tz.TimeZone + "</TimezoneName>");
                        }
                        else
                        {
                            strOutXML.Append("<TimezoneName></TimezoneName>");
                        }
                        strOutXML.Append("<Longitude>" + locRoom.Longitude + "</Longitude>");
                        strOutXML.Append("<Latitude>" + locRoom.Latitude + "</Latitude>");

                        int i = 1;
                        foreach (vrmLocApprover locRoomApprov in locRoom.locationApprover)
                        {
                            userInfo = m_IuserDAO.GetByUserId(locRoomApprov.approverid);
                            strOutXML.Append("<Approver" + i.ToString() + "ID>" + locRoomApprov.approverid.ToString() + "</Approver" + i.ToString() + "ID>");
                            strOutXML.Append("<Approver" + i.ToString() + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "  </Approver" + i.ToString() + "Name>");
                            i++;
                        }
                        if (i == 1)
                        {
                            strOutXML.Append("<Approver1ID></Approver1ID>");
                            strOutXML.Append("<Approver1Name></Approver1Name>");
                            strOutXML.Append("<Approver2ID></Approver2ID>");
                            strOutXML.Append("<Approver2Name></Approver2Name>");
                            strOutXML.Append("<Approver3ID></Approver3ID>");
                            strOutXML.Append("<Approver3Name></Approver3Name>");
                            strOutXML.Append("<ApprovalReq>No</ApprovalReq>");
                        }
                        else if (i == 2)
                        {
                            strOutXML.Append("<Approver2ID></Approver2ID>");
                            strOutXML.Append("<Approver2Name></Approver2Name>");
                            strOutXML.Append("<Approver3ID></Approver3ID>");
                            strOutXML.Append("<Approver3Name></Approver3Name>");
                            strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                        }
                        else if (i == 3)
                        {
                            strOutXML.Append("<Approver3ID></Approver3ID>");
                            strOutXML.Append("<Approver3Name></Approver3Name>");
                            strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                        }
                        strOutXML.Append("<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>");
                        List<vrmEndPoint> eptList = new List<vrmEndPoint>();
                        List<ICriterion> criterionListept = new List<ICriterion>();
                        criterionListept.Add(Expression.Eq("endpointid", locRoom.endpointid));
                        criterionListept.Add(Expression.Eq("deleted", 0));
                        eptList = m_vrmEpt.GetByCriteria(criterionListept);
                        if (eptList.Count > 0)
                        {
                            foreach (vrmEndPoint ept in eptList)
                            {
                                strOutXML.Append("<EndpointName>" + ept.name + "</EndpointName>");
                                strOutXML.Append("<EndpointIP>" + ept.address + "</EndpointIP>");
                                break;
                            }
                        }
                        else
                        {
                            strOutXML.Append("<EndpointName></EndpointName>");
                            strOutXML.Append("<EndpointIP></EndpointIP>");
                        }

                        //Image Project codelines start...
                        string roomImagesids = locRoom.RoomImageId;
                        string roomImagesnames = locRoom.RoomImage;

                        vrmImage imObj = null;
                        string imgDt = "";
                        string imagename = "";
                        int imageid = 0;
                        string fileext = "";

                        if (roomImagesids != null && roomImagesnames != null)
                        {
                            if (roomImagesids != "" && roomImagesnames != "")
                            {
                                imgDt = "";
                                imObj = null;
                                string[] idArr = roomImagesids.Split(',');
                                string[] nameArr = roomImagesnames.Split(',');

                                if (idArr.Length > 0 && nameArr.Length > 0)
                                {
                                    fileext = "jpg";

                                    imagename = nameArr[0].ToString();
                                    if (imagename != "")
                                        fileext = imagename.Substring(imagename.LastIndexOf(".") + 1);
                                }
                            }
                        }
                        strOutXML.Append("<ImageName>" + imagename + "</ImageName>");
                        strOutXML.Append("<Imagetype>" + fileext + "</Imagetype>");
                        //strOutXML.Append("<Image>"+ imgDt +"</Image>");

                        strOutXML.Append("<Custom1>" + locRoom.Custom1 + "</Custom1>");
                        strOutXML.Append("<Custom2>" + locRoom.Custom2 + "</Custom2>");
                        strOutXML.Append("<Custom3>" + locRoom.Custom3 + "</Custom3>");
                        strOutXML.Append("<Custom4>" + locRoom.Custom4 + "</Custom4>");
                        strOutXML.Append("<Custom5>" + locRoom.Custom5 + "</Custom5>");
                        strOutXML.Append("<Custom6>" + locRoom.Custom6 + "</Custom6>");
                        strOutXML.Append("<Custom7>" + locRoom.Custom7 + "</Custom7>");
                        strOutXML.Append("<Custom8>" + locRoom.Custom8 + "</Custom8>");
                        strOutXML.Append("<Custom9>" + locRoom.Custom9 + "</Custom9>");
                        strOutXML.Append("<Custom10>" + locRoom.Custom10 + "</Custom10>");
                        strOutXML.Append("<Department>");

                        string depts = "";

                        foreach (vrmLocDepartment locDept in locRoom.locationDept)
                        {
                            if (depts == "")
                                depts = locDept.departmentId.ToString();
                            else
                                depts += " , " + locDept.departmentId.ToString();

                        }
                        strOutXML.Append(depts + "</Department>");

                        string qString;
                        qString = "from myVRM.DataLayer.InventoryCategory  Cat, ";
                        qString += "myVRM.DataLayer.InventoryRoom Rm ";
                        qString += "where Rm.ICategory.ID = Cat.ID and Rm.IRoom.roomId = " + locRoom.roomId + " and Cat.deleted = 0 ";
                        qString += "and Cat.Type = 1";

                        IList CategoryList = m_InvCategoryDAO.execQuery(qString);
                        if (CategoryList.Count > 0)
                        {
                            foreach (object[] roomSets in CategoryList)
                            {

                                InventoryCategory ic = (InventoryCategory)roomSets[0];

                                foreach (AVInventoryItemList CatItem in ic.AVItemList)
                                {
                                    if (CatItem.deleted == 0)
                                        avComps += ", " + CatItem.name;
                                }
                            }
                        }

                        List<ICriterion> criterionListWO = new List<ICriterion>();
                        criterionListWO.Add(Expression.Eq("deleted", 0));
                        criterionListWO.Add(Expression.Eq("Type", 1));

                        List<InventoryList> InvList = m_InvListDAO.GetByCriteria(criterionListWO);

                        nne = "1";

                        if (InvList.Count > 0)
                        {
                            foreach (InventoryList it in InvList)
                            {
                                u = 0;
                                if (avComps.Contains(it.Name))
                                {
                                    u = 1;
                                    nne = "0";
                                }

                                if (it.Type == 1 && it.deleted == 0)
                                {

                                    rmName = it.Name;

                                    if (it.Name.Split(' ').Length > 1)
                                        rmName = it.Name.Split(' ')[0] + it.Name.Split(' ')[1];


                                    strOutXML.Append("<" + rmName + ">" + u.ToString() + "</" + rmName + ">");
                                }
                            }
                        }

                        strOutXML.Append("<None>" + nne + "</None>");
                        strOutXML.Append("<ServiceType>" + locRoom.ServiceType.ToString() + "</ServiceType>");//FB 2219
                        strOutXML.Append("<DedicatedVideo>" + locRoom.DedicatedVideo + "</DedicatedVideo>");//FB 2334
                        strOutXML.Append("<DedicatedCodec>" + locRoom.DedicatedCodec + "</DedicatedCodec>");//FB 2390
						strOutXML.Append("<AVOnsiteSupportEmail>" + locRoom.AVOnsiteSupportEmail + "</AVOnsiteSupportEmail>");//FB 2415
                        //FB 2426 Start
                        strOutXML.Append("<Extroom>" + locRoom.Extroom + "</Extroom>");
                        strOutXML.Append("<LoginUserId>" + locRoom.adminId + "</LoginUserId>");
                        //FB 2426 End
                        strOutXML.Append("<isVMR>" + locRoom.IsVMR + "</isVMR>"); //FB 2448
                        strOutXML.Append("<RoomCategory>" + locRoom.RoomCategory + "</RoomCategory>"); //FB 2694
                        strOutXML.Append("<OwnerID>" + locRoom.OwnerID + "</OwnerID>"); //FB 2262
                        strOutXML.Append("<DefaultEquipmentID>" + locRoom.DefaultEquipmentid.ToString() + "</DefaultEquipmentID>"); //FB 2594
                        strOutXML.Append("<isPublic>" + locRoom.isPublic + "</isPublic>");
                        //FB 2065 - Start
                        strOutXML.Append("<RoomIconTypeId>" + locRoom.RoomIconTypeId + "</RoomIconTypeId>");
                        
                        List<ICriterion> criterionList = new List<ICriterion>();
                        if (locRoom.RoomIconTypeId != null)
                        {
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("AttributeType", locRoom.RoomIconTypeId));
                            List<vrmImage> IconList = m_IImageDAO.GetByCriteria(criterionList);

                            if (IconList != null && IconList.Count > 0)
                            {
                                imgDt = vrmImg.ConvertByteArrToBase64(IconList[0].AttributeImage);
                                strOutXML.Append("<AttributeImage>" + imgDt + "</AttributeImage>");
                            }
                        }
                        //FB 2065 - End

                        strOutXML.Append("</Room>");
                    }
                }

                strOutXML.Append("</Rooms>");

                obj.outXml = strOutXML.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetAllRoomsBasicInfo: " + ex.Message);
                return false;
            }
        }
        #endregion

        #region GetRoomsDepts

        public bool GetRoomsDepts(ref vrmDataObject obj)
        {
            string qry = "";
            string dqry = "";//FB 1672
            bool bRet = false;
            string rmIDs = "";
            string deptIDs = "";//FB 1672
            vrmUser userInfo = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/UserID");
                string userID = node.InnerXml.Trim();

                if (userID == "")
                    userID = "11";

                int userid = 11;

                Int32.TryParse(userID, out userid);

                userInfo = m_IuserDAO.GetByUserId(userid);

                bool m_bSuper = false;
                bRet = true;
                qry = "select crd.roomId from myVRM.DataLayer.vrmLocDepartment  crd, myVRM.DataLayer.vrmUserDepartment UD where crd.departmentId = UD.departmentId and UD.userId = " + userID;
                IList result = m_IRoomDAO.execQuery(qry);
                if (result.Count > 0)
                {
                    foreach (object objs in result)
                    {
                        if (rmIDs == "")
                            rmIDs = objs.ToString();
                        else
                            rmIDs += ", " + objs.ToString();
                    }
                }
                // FB 1672 - Starts
                dqry = "SELECT c.departmentId FROM myVRM.DataLayer.vrmUserDepartment c WHERE  c.userId = " + userID;
                IList dresult = m_IuserDeptDAO.execQuery(dqry); 
                 
                if (dresult.Count > 0)
                {
                    foreach (object objd in dresult)
                    {
                        if (deptIDs == "")
                            deptIDs = objd.ToString();
                        else
                            deptIDs += ", " + objd.ToString();
                    }
                }
                // FB 1672 - End
                obj.outXml = "<Rooms>";
                obj.outXml += "<user>" + userID + "</user>"; // FB 1672
                obj.outXml += "<userDept>" + deptIDs + "</userDept>";// FB 1672
                obj.outXml += "<roomIDs>" + rmIDs + "</roomIDs>";
                obj.outXml += "<favourite>" + userInfo.PreferedRoom + "</favourite>";
                obj.outXml += "</Rooms>";





                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetBusyRooms

        public bool GetBusyRooms(ref vrmDataObject obj)
        {
            bool bRet = false;
            string rmIDs = "";
            string durations = "";
            string immediate = "0"; //FB 2534
            string ConfType = "";//FB 2694
            int ishotdesking = 0; //FB 2694
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("conferenceTime/userId");
                string userId = "11";
                if (node != null)
                {
                    userId = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("conferenceTime/ConfType"); //FB 2694
                if (node != null)
                    ConfType = node.InnerXml.Trim();

                if (ConfType == "8")
                    ishotdesking = 1;
                //int userid = 11;

                // Int32.TryParse(userId, out userid);

                // userInfo = m_IuserDAO.GetByUserId(userid);
                
				node = xd.SelectSingleNode("conferenceTime/immediate"); //FB 2534
                if (node != null)
                    immediate = node.InnerXml.Trim();
                //FB 2594
                int isPublicRoom = 0;
                node = xd.SelectSingleNode("conferenceTime/enablePublicRoom");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out isPublicRoom);

                node = xd.SelectSingleNode("conferenceTime/confID");
                string confID = "";
                if (node != null)
                {
                    confID = node.InnerXml.Trim();
                }
                string srtInstance = "";
                node = xd.SelectSingleNode("conferenceTime/startDate");
                if (node != null)
                {
                    srtInstance = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("conferenceTime/startHour");
                if (node != null)
                    srtInstance += " " + node.InnerXml.Trim();
                else
                    srtInstance += " 00";

                node = xd.SelectSingleNode("conferenceTime/startMin");
                if (node != null)
                    srtInstance += ":" + node.InnerXml.Trim();
                else
                    srtInstance += ":00";

                node = xd.SelectSingleNode("conferenceTime/startSet");
                if (node != null)
                    srtInstance += " " + node.InnerXml.Trim();
                else
                    srtInstance += " AM";

                node = xd.SelectSingleNode("conferenceTime/durationMin");
                if (node != null)
                    durations = node.InnerXml.Trim();

                node = xd.SelectSingleNode("conferenceTime/timeZone");
                string tzone = "26";
                if (node != null)
                {
                    tzone = node.InnerXml.Trim();
                }

                int tzoneID = 33;

                Int32.TryParse(tzone, out tzoneID);


                if (durations == "")
                    durations = "0";

                Double dur = Double.MinValue;

                Double.TryParse(durations, out dur);

                if (immediate == "1") //FB 2534
                    tzoneID = sysSettings.TimeZone;

                DateTime confdate = DateTime.Now;
                
                if (immediate == "0") //FB 2534
                    DateTime.TryParse(srtInstance, out confdate);

                timeZone.changeToGMTTime(tzoneID, ref confdate);

                DateTime endDate = confdate.AddMinutes(dur);

                endDate = endDate.AddSeconds(30);//FB 1783
                confdate = confdate.AddSeconds(50);//FB 1783

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmConfRoom> confRoomList = new List<vrmConfRoom>();
                List<vrmConference> confList = new List<vrmConference>();

                CConfID ConfMode = new CConfID(confID);

                // FB 1796
                string sqlFilter = "(StartDate < '"+ endDate.ToString() +"' and StartDate >= '"+ confdate.ToString() +"')or('" + confdate.ToString()
                    + "' >= StartDate and '" + confdate.ToString() + "' < dateadd(minute, duration,startdate))"; //FB 1587
                
                //criterionList.Add(Expression.Not(Expression.In("confid", ConfMode.ID)));
                //criterionList.Add(Expression.Not(Expression.Eq("instanceid", ConfMode.instance))); //Commented for FB 1711
                //criterionList.Add(Expression.Lt("StartDate", endDate));  //FB 1587 - Commented
                //criterionList.Add(Expression.Ge("StartDate", confdate)); //FB 1587 - Commented
                criterionList.Add(Expression.Sql(sqlFilter)); //FB 1587

                confRoomList = m_IconfRoom.GetByCriteria(criterionList);

                confRoomList = confRoomList.Where(r => r.isDoubleBooking == ishotdesking).ToList();//FB 2694_UP

                foreach (vrmConfRoom confRm in confRoomList)
                {
                    if (confRm.confid != ConfMode.ID)// FB 1796
                    {
                        ArrayList arrStatus = new ArrayList();
                        arrStatus.Add(vrmConfStatus.Scheduled);
                        arrStatus.Add(vrmConfStatus.Ongoing);
                        arrStatus.Add(vrmConfStatus.Pending);
                        arrStatus.Add(vrmConfStatus.OnMCU);
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", confRm.confid));
                        criterionList.Add(Expression.Eq("instanceid", confRm.instanceid));
                        criterionList.Add(Expression.Eq("deleted", 0));
                        criterionList.Add(Expression.In("status", arrStatus));
                        //FB 2634
                        //if (isPublicRoom == 0) //FB 2594
                        //    criterionList.Add(Expression.Eq("isPublic", isPublicRoom)); 
                        confList = m_IconfDAO.GetByCriteria(criterionList);
                        if (confList.Count > 0)
                        {
                            if (rmIDs == "")
                                rmIDs = confRm.roomId.ToString();
                            else
                                rmIDs += "," + confRm.roomId.ToString();
                        }
                    }
                }
                obj.outXml = "<roomIDs>" + rmIDs + "</roomIDs>";




                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetRoomBasicDetails

        public bool GetRoomBasicDetails(ref vrmDataObject obj)
        {
            List<vrmRoom> locRooms = null;
            string rmids = "";

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node = xd.SelectSingleNode("Room/RoomIds");
                rmids = "11";
                if (node != null)
                {
                    rmids = node.InnerXml.Trim();
                }

                ArrayList roomids = new ArrayList();

                foreach (string s in rmids.Split(','))
                {
                    if (s != "")
                        roomids.Add(s);
                }

                List<ICriterion> criterionListdets = new List<ICriterion>();
                criterionListdets.Add(Expression.In("RoomID", roomids));
                locRooms = m_IRoomDAO.GetByCriteria(criterionListdets);

                obj.outXml = "<Rooms>";

                foreach (vrmRoom locRoom in locRooms)
                {


                    if (locRoom.roomId != 11)
                    {
                        obj.outXml += "<Room>";
                        obj.outXml += "<RoomID>" + locRoom.roomId.ToString() + "</RoomID>";
                        obj.outXml += "<RoomName>" + locRoom.Name + "</RoomName>";
                        obj.outXml += "</Room>";
                    }
                }

                obj.outXml += "</Rooms>";

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region GetDeactivatedRooms

        public bool GetDeactivatedRooms(ref vrmDataObject obj)
        {
            List<vrmRoom> locRooms = null;
            string rmIDs = "";

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/UserID");
                string userID = node.InnerXml.Trim();

                if (userID == "")
                    userID = "11";

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                List<ICriterion> criterionListdets = new List<ICriterion>();
                criterionListdets.Add(Expression.Eq("Disabled", 1));
                criterionListdets.Add(Expression.Eq("orgId", organizationID));
                locRooms = m_IRoomDAO.GetByCriteria(criterionListdets);

                foreach (vrmRoom locRoom in locRooms)
                {
                    if (rmIDs == "")
                        rmIDs = locRoom.roomId.ToString();
                    else
                        rmIDs += "," + locRoom.roomId.ToString();



                }

                obj.outXml = "<roomIDs>" + rmIDs + "</roomIDs>";
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region GetRoomLicenseDetails

        public bool GetRoomLicenseDetails(ref vrmDataObject obj)
        {
            int maximumLimit = 0;
            int maximumvidLimit = 0;
            int maximumnvidLimit = 0;
			int maxPublicRoom = 0;//FB 2594
            Int32 maximumvmrLimit = 0;//FB 2586
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                string maxLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vc where vc.Disabled = 0 and vc.Extroom = 0 and vc.isPublic = 0 and vc.orgId =" + organizationID.ToString(); //FB 2426 //FB 2594
                IList resRec = m_IRoomDAO.execQuery(maxLimit);
                if (resRec != null)
                {
                    if (resRec.Count > 0)
                    {
                        if (resRec[0] != null)
                        {
                            if (resRec[0].ToString() != "")
                                Int32.TryParse(resRec[0].ToString(), out maximumLimit);
                        }
                    }
                }

                maxLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vc where vc.Disabled = 0  and vc.VideoAvailable = 2 and vc.IsVMR =0 and vc.RoomCategory !=4  and vc.Extroom = 0 and vc.isPublic = 0 and  vc.orgId =" + organizationID.ToString(); //FB 1744 //FB 2426 //FB 2594 //FB 2586 //FB 2694
                IList vidresRec = m_IRoomDAO.execQuery(maxLimit);
                if (vidresRec != null)
                {
                    if (vidresRec.Count > 0)
                    {
                        if (vidresRec[0] != null)
                        {
                            if (vidresRec[0].ToString() != "")
                                Int32.TryParse(vidresRec[0].ToString(), out maximumvidLimit);
                        }
                    }
                }
                //FB 2586 Start
                maxLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vc where vc.Disabled = 0  and vc.VideoAvailable = 2 and vc.Extroom = 0 and vc.IsVMR = 1 and vc.RoomCategory != 4 and  vc.orgId =" + organizationID.ToString();
                IList vmrresRec = m_IRoomDAO.execQuery(maxLimit);
                if (vmrresRec != null)
                {
                    if (vmrresRec.Count > 0)
                    {
                        if (vmrresRec[0] != null)
                        {
                            if (vmrresRec[0].ToString() != "")
                                Int32.TryParse(vmrresRec[0].ToString(), out maximumvmrLimit);
                        }
                    }
                }
                //FB 2586 End


                maxLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vc where vc.Disabled = 0 and vc.VideoAvailable < 2 and vc.IsVMR =0 and vc.RoomCategory !=4 and vc.Extroom = 0 and vc.isPublic = 0 and vc.orgId =" + organizationID.ToString(); //FB 1744 //FB 2426 //FB 2594 //FB 2586 //FB 2694
                IList nvidresRec = m_IRoomDAO.execQuery(maxLimit);
                if (nvidresRec != null)
                {
                    if (nvidresRec.Count > 0)
                    {
                        if (nvidresRec[0] != null)
                        {
                            if (nvidresRec[0].ToString() != "")
                                Int32.TryParse(nvidresRec[0].ToString(), out maximumnvidLimit);
                        }
                    }
                }
                //FB 2594 Starts
                string maxPubLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vc where vc.Disabled = 0 and vc.Extroom = 0 and vc.isPublic = 1 and vc.orgId =" + organizationID.ToString();
                IList resPubRec = m_IRoomDAO.execQuery(maxPubLimit);
                if (resPubRec != null)
                {
                    if (resPubRec.Count > 0)
                    {
                        if (resPubRec[0] != null)
                        {
                            if (resPubRec[0].ToString() != "")
                                Int32.TryParse(resPubRec[0].ToString(), out maxPublicRoom);
                        }
                    }
                }

                //FB 2694 Starts
                int UsedROHotDesking = 0;
                string maxROrmCnt = " SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.IsVMR =0 and vo.Extroom=0 and vo.VideoAvailable < 2 and vo.RoomCategory = 4 and vo.isPublic = 0 and  vo.orgId =" + organizationID.ToString();
                IList remainingROHotDesking = m_IRoomDAO.execQuery(maxROrmCnt);
                if (remainingROHotDesking != null && remainingROHotDesking.Count > 0)
                {
                    if (remainingROHotDesking[0] != null && remainingROHotDesking[0].ToString() != "")
                    {
                        int.TryParse(remainingROHotDesking[0].ToString(), out UsedROHotDesking);
                    }
                }

                int UsedVCHotDesking = 0;
                string maxVCrmCnt = " SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.IsVMR =0 and vo.Extroom=0 and vo.VideoAvailable = 2 and vo.RoomCategory = 4 and vo.isPublic = 0 and  vo.orgId =" + organizationID.ToString();
                IList remainingVCHotDesking = m_IRoomDAO.execQuery(maxVCrmCnt);
                if (remainingVCHotDesking != null && remainingVCHotDesking.Count > 0)
                {
                    if (remainingVCHotDesking[0] != null && remainingVCHotDesking[0].ToString() != "")
                    {
                        int.TryParse(remainingVCHotDesking[0].ToString(), out UsedVCHotDesking);
                    }
                }
                //FB 2694 End

                obj.outXml = "<RoomLicenseDetails>";
                obj.outXml += "<MaxRooms>" + maximumLimit.ToString() + "</MaxRooms>";
                obj.outXml += "<LicenseRemaining>" + (orgdt.RoomLimit - maximumLimit).ToString() + "</LicenseRemaining>";
                obj.outXml += "<MaxVideo>" + orgdt.MaxVideoRooms.ToString() + "</MaxVideo>";
                obj.outXml += "<MaxNonvideo>" + orgdt.MaxNonVideoRooms.ToString() + "</MaxNonvideo>";
                obj.outXml += "<MaxVMR>" + orgdt.MaxVMRRooms.ToString() + "</MaxVMR>";//FB 2586
                obj.outXml += "<MaxVideoRemaining>" + (orgdt.MaxVideoRooms - maximumvidLimit).ToString() + "</MaxVideoRemaining>";
                obj.outXml += "<MaxNonvideoRemaining>" + (orgdt.MaxNonVideoRooms - maximumnvidLimit).ToString() + "</MaxNonvideoRemaining>";
                obj.outXml += "<MaxVMRRemaining>" + (orgdt.MaxVMRRooms - maximumvmrLimit).ToString() + "</MaxVMRRemaining>";//FB 2586
                obj.outXml += "<MaxPublicRoom>" + maxPublicRoom.ToString() + "</MaxPublicRoom>"; //FB 2594
                //FB 2694 Starts
                obj.outXml += "<MaxROHotdesking>" + orgdt.MaxROHotdesking + "</MaxROHotdesking>";
                obj.outXml += "<MaxVCHotdesking>" + orgdt.MaxVCHotdesking + "</MaxVCHotdesking>";
                obj.outXml += "<MaxROHotdeskingRemaining>" + (orgdt.MaxROHotdesking - UsedROHotDesking) + "</MaxROHotdeskingRemaining>";
                obj.outXml += "<MaxVCHotdeskingRemaining>" + (orgdt.MaxVCHotdesking - UsedVCHotDesking) + "</MaxVCHotdeskingRemaining>";
                //FB 2694 End
                obj.outXml += "</RoomLicenseDetails>";
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region SearchRooms by Date

        public bool SearchRoomsbyDate(ref vrmDataObject obj)
        {

            vrmUser userInfo = null;
            DateTime modifiedDate = DateTime.Now;
            List<vrmRoom> locRooms = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("SearchRooms/LastModifiedBy");
                string userId = "11";
                if (node != null)
                {
                    userId = node.InnerXml.Trim();
                }

                int userid = 0;

                Int32.TryParse(userId, out userid);

                userInfo = m_IuserDAO.GetByUserId(userid);


                string srtInstance = "";
                node = xd.SelectSingleNode("SearchRooms/LastModifiedDate");
                if (node != null)
                {
                    srtInstance = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("SearchRooms/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (DateTime.TryParse(srtInstance, out modifiedDate))
                {
                    timeZone.changeToGMTTime(userInfo.TimeZone, ref modifiedDate);

                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Ge("Lastmodifieddate", modifiedDate));
                    criterionList.Add(Expression.Ge("adminId", userid)); // Search Room 
                    criterionList.Add(Expression.Eq("Disabled", 0));
                    criterionList.Add(Expression.Ge("orgId", organizationID));

                    locRooms = m_IRoomDAO.GetByCriteria(criterionList);

                    obj.outXml = "<SearchRooms>";

                    foreach (vrmRoom locRoom in locRooms)
                    {
                        obj.outXml += "<RoomID>" + locRoom.RoomID + "</RoomID>";

                    }

                    obj.outXml += "</SearchRooms>";

                }


            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }

            return true;
        }
        #endregion
        
        #endregion
        //FB 1728
        #region Isconference schedulable
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool Isconferenceschedulable(ref vrmDataObject obj)
        {
            
            string sysdatetime = "";
            string confdate = "";
            int timezoneID = 31;
            int userid = 11;
            int hostid = 11;
            int timezonedisplay = 1;
            int skipCheck = 0;
            string oxml = "";
            // FB 1865
            string stmt = null;
            string conferenceID = "";
            string password = "";
            string passwordXML = "<passwordCheck>1</passwordCheck>";
            string skipcheck = "";
            int iRoomSetup = 0, iRoomTear = 0; //FB 2440
            string sMCUxml = "-1";//FB 2440
            try
            {
                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/timeZone");
                string timezone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/hostID");
                string hostID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/timezoneDisplay");
                string timeZoneDisplay = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/skipCheck");
                if (node != null)
                {
                    if (node.InnerText != "")
                        skipcheck = node.InnerText.Trim();
                    Int32.TryParse(skipcheck, out skipCheck);
                }

                /** Fb 2440 **/
                node = xd.SelectSingleNode("//login/mcuSetup");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out iRoomSetup);

                node = xd.SelectSingleNode("//login/mcuTeardonw");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out iRoomTear);
                /** Fb 2440 **/

                DateTime frstDate = DateTime.Now;
                DateTime nowDate = DateTime.Now;

                Int32.TryParse(userId, out userid);
                Int32.TryParse(timezone, out timezoneID);
                Int32.TryParse(hostID, out hostid);
                Int32.TryParse(timeZoneDisplay, out timezonedisplay);

                vrmUser user = m_IuserDAO.GetByUserId(userid);
                vrmUser host = m_IuserDAO.GetByUserId(hostid);


                if (skipCheck > 0)
                {
                    oxml = "True";
                }
                else
                {

                    sysdatetime = xd.SelectSingleNode("//login/systemDate").InnerText + " " + xd.SelectSingleNode("//login/systemTime").InnerText;
                    confdate = xd.SelectSingleNode("//login/confDate").InnerText + " " + xd.SelectSingleNode("//login/confTime").InnerText;

                    

                    if (sysdatetime != "")
                        DateTime.TryParse(sysdatetime, out nowDate);

                    if (confdate != "")
                        DateTime.TryParse(confdate, out frstDate);                    

                    if (timezonedisplay < 1)
                        timezoneID = host.TimeZone;


                    timeZone.changeToGMTTime(user.TimeZone, ref nowDate);
                    timeZone.changeToGMTTime(timezoneID, ref frstDate);
                    oxml = "True";
                    if (frstDate < nowDate)
                        oxml = "False";
                }


                //// FB 1865

                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    conferenceID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/password");
                if (node != null)
                    password = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (password != "" && orgInfo.isUniquePassword == 1)
                {


                    CConfID ConfMode = new CConfID(conferenceID);


                    stmt = "Select c.confid from myVRM.DataLayer.vrmConference c " +
                              " WHERE c.password='"+ password + "' and c.deleted=0 and c.confid <> " + ConfMode.ID.ToString() +
                              " and c.status in (0,1,5,6) and c.confdate > '"+ nowDate.ToString() +"'";


                    IList result = m_IconfDAO.execQuery(stmt);

                    if (result != null)
                    {
                        if(result.Count > 0)
                            passwordXML = "<passwordCheck>0</passwordCheck>";

                    }

                }
                // FB 1865

                /** Fb 2440 **/
                if ((iRoomSetup >= 0 || iRoomTear >= 0) && oxml.ToLower().Equals("true") && orgInfo.MCUBufferPriority <= 0)
                {
                    if (orgInfo.McuSetupTime != 0)
                    {
                        if (iRoomSetup < orgInfo.McuSetupTime)
                        {
                            oxml = "false";
                            sMCUxml = "1";
                        }
                    }
                    if (orgInfo.MCUTeardonwnTime < 0)
                    {
                        if ((iRoomTear + orgInfo.MCUTeardonwnTime) < 0)
                        {
                            oxml = "false";
                            sMCUxml = "2";
                        }

                    }
                }
                /** Fb 2440 **/

                obj.outXml = "<login><result>" + oxml + "</result><hosttimezone>" + host.TimeZone.ToString() + "</hosttimezone>" + passwordXML + "<mcuresult>"+ sMCUxml +"</mcuresult></login>"; // FB 1865 FB 2440

                
            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetImages
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// 
        //FB 1756 - Changed the outXML String to StringBuilder
        public bool GetImages(ref vrmDataObject obj)
        {
            List<vrmRoom> locRooms = null;
            vrmImg = new imageFactory(ref obj); //FB 2136
            string roomIDs = "";
            int RoomID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node = xd.SelectSingleNode("//GetImages/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

               
                node = xd.SelectSingleNode("//GetImages/RoomID");
                roomIDs = "";
                if (node != null)
                {
                    roomIDs = node.InnerXml.Trim();
                    if (roomIDs != "")
                        Int32.TryParse(roomIDs, out RoomID);
                }
                if (RoomID > 0)
                {

                    List<ICriterion> criterionListorg = new List<ICriterion>();
                    criterionListorg.Add(Expression.Eq("orgId", organizationID));


                    criterionListorg.Add(Expression.Eq("RoomID", RoomID));

                    locRooms = m_IRoomDAO.GetByCriteria(criterionListorg);

                    StringBuilder strOutXML = new StringBuilder();

                    strOutXML.Append("<GetImages>");

                    foreach (vrmRoom locRoom in locRooms)
                    {
                        if (locRoom.Name != "Phantom Room")
                        {
                            //Image Project codelines start...
                            string roomImagesids = locRoom.RoomImageId;
                            string roomImagesnames = locRoom.RoomImage;

                            vrmImage imObj = null;
                            string imgDt = "";
                            string imagename = "";
                            int imageid = 0;
                            string fileext = "";

                            if (roomImagesids != null && roomImagesnames != null)
                            {
                                if (roomImagesids != "" && roomImagesnames != "")
                                {
                                    imgDt = "";
                                    imObj = null;
                                    string[] idArr = roomImagesids.Split(',');
                                    string[] nameArr = roomImagesnames.Split(',');

                                    if (idArr.Length > 0 && nameArr.Length > 0)
                                    {
                                        fileext = "jpg";

                                        imagename = nameArr[0].ToString();
                                        if (imagename != "")
                                            fileext = imagename.Substring(imagename.LastIndexOf(".") + 1);

                                        Int32.TryParse(idArr[0].ToString(), out imageid);
                                        imObj = m_IImageDAO.GetById(imageid);
                                        imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136
                                    }
                                }
                            }
                            strOutXML.Append("<ImageName>" + imagename + "</ImageName>");
                            strOutXML.Append("<Imagetype>" + fileext + "</Imagetype>");
                            strOutXML.Append("<Image>" + imgDt + "</Image>");
                        }
                    }

                    strOutXML.Append("</GetImages>");
                    obj.outXml = strOutXML.ToString();
                }
                else
                {
                    obj.outXml = "<error>Room ID is empty</error>";
                }

                
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetAllRoomsBasicInfo: " + ex.Message);
                return false;
            }
        }
        #endregion

        #region GetOngoing
        /// <summary>
        ///FB 1838 This command is going to be used in services to get ongoing congferences
        /// It has a Status Filter Only and conferene time ir returned in GMT
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool GetOngoing(ref vrmDataObject obj)
        {
            StringBuilder searchOutXml = new StringBuilder();
            List<vrmConference> confList = null;
            List<ICriterion> criterionList = null;
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<vrmConfMessage> ConfMessageList = new List<vrmConfMessage>();
            List<ICriterion> mcriterionList = null;
            ICriterion criterium = null;
            vrmConfMessage confMessage = null;
            int messageoverlay = 0, iStatus = 0, iConfType = 0;
            XmlNode innerNode = null;
            try
            {
                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                List<OrgData> orgDts = m_IOrgSettingsDAO.GetAll(); //FB 2595

                XmlNodeList statusList = xd.GetElementsByTagName("ConferenceStatus");
                List<int> confStatus = new List<int>();
                for (int i = 0; i < statusList.Count; i++)
                {
                    innerNode = statusList[i];
                    int.TryParse(innerNode.InnerXml.Trim(), out iStatus);
                    confStatus.Add(iStatus);
                }

                //FB 2434 Starts
                XmlNodeList ConfTypeList = xd.GetElementsByTagName("ConfType");
                List<int> confType = new List<int>();
                for (int j = 0; j < ConfTypeList.Count; j++)
                {
                    innerNode = ConfTypeList[j];
                    int.TryParse(innerNode.InnerXml.Trim(), out iConfType);
                    confType.Add(iConfType);
                }
                //FB 2437 - Starts
                bool isP2PServiceCall = false;

                searchOutXml.Append("<SearchConference>");
                searchOutXml.Append("<Conferences>");

                for (int oid = 0; oid < orgDts.Count; oid++)
                {
                    criterionList = new List<ICriterion>();    //FB 1158

                    if (confStatus.Count > 0)
                    {
                        if (confStatus.Count == 1)
                            criterionList.Add(Expression.Eq("status", confStatus[0]));
                        else
                            criterionList.Add(Expression.In("status", confStatus));
                    }

                    if (confType.Count > 0)
                    {
                        if (confType.Count == 1)
                        {
                            criterionList.Add(Expression.Eq("conftype", confType[0]));
                            if (confType[0] == 4)
                                isP2PServiceCall = true;
                        }
                        else
                            criterionList.Add(Expression.In("conftype", confType));
                    }
                    //FB 2434 Ends

                    DateTime confFrom = DateTime.Now;
                    DateTime confEnd = DateTime.Now;

                    if (isP2PServiceCall && sysSettings.EnableLaunchBufferP2P == 0)
                    {
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                        criterium = Expression.Le("confdate", confEnd);
                        criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                    }//FB 2437 - End
                    else if (getSearchDateRange(vrmSearchType.Ongoing, ref confEnd, ref confFrom, 0))//FB 2595
                    {
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                        criterium = Expression.Le("confdate", confEnd);
                        criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                    }
                    criterionList.Add(Expression.Eq("deleted", 0));

                    if (sysSettings.Cloud == 0)//FB 2448 //FB 2599 
                        criterionList.Add(Expression.Eq("isVMR", 0));

                    if (orgDts[oid].SecureSwitch == 1)
                    {
                        if (orgDts[oid].NetworkSwitching != 1)
                          criterionList.Add(Expression.Eq("NetworkSwitch", 1));

                        if (orgDts[oid].NetworkCallLaunch == 1) //Secured
                            criterionList.Add(Expression.Eq("Secured", 1));
                        else if (orgDts[oid].NetworkCallLaunch == 2) //Unsecured
                            criterionList.Add(Expression.Eq("Secured", 0));
                    }
                    criterionList.Add(Expression.Eq("orgId", orgDts[oid].OrgId));
                    confList = m_IconfDAO.GetByCriteria(criterionList);

                    //confList = confList.Where(cnfs => cnfs.NetworkSwitch == orgDts.FirstOrDefault(org => org.OrgId == cnfs.orgId).SecureSwitch).ToList();//FB 2595

                    vrmConference conf = null;

                    for (int confcnt = 0; confcnt < confList.Count; confcnt++)
                    {

                        conf = confList[confcnt];
                        searchOutXml.Append("<Conference>");
                        searchOutXml.Append("<ConferenceID>" + conf.confid.ToString() + "," +
                                conf.instanceid.ToString() + "</ConferenceID>");

                        searchOutXml.Append("<ConferenceActualStatus>" + conf.status.ToString() + "</ConferenceActualStatus>");

                        conf.status = vrmConfStatus.Ongoing;

                        searchOutXml.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                        searchOutXml.Append("<ConferenceName>" + conf.externalname.ToString() + "</ConferenceName>");
                        searchOutXml.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                        searchOutXml.Append("<ConferenceDateTime>" + conf.confdate.ToString("g") + "</ConferenceDateTime>");
                        searchOutXml.Append("<ConferenceDuration>" + conf.duration.ToString() + "</ConferenceDuration>");
                        searchOutXml.Append("<ConferenceStatus>" + conf.status.ToString() + "</ConferenceStatus>");
                        searchOutXml.Append("<CallStartMode>" + conf.StartMode.ToString() + "</CallStartMode>"); //FB 2501

                        if (conf.LastRunDateTime == DateTime.MinValue)
                            searchOutXml.Append("<LastRunDate></LastRunDate>");
                        else
                            searchOutXml.Append("<LastRunDate>" + conf.LastRunDateTime.ToString() + "</LastRunDate>");


                        //FB 2560 Code Changes ... Start

                        #region ConfMessage

                        if (conf.isTextMsg == 1)
                        {
                            mcriterionList = new List<ICriterion>();
                            mcriterionList.Add(Expression.Eq("confid", conf.confid));
                            mcriterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                            ConfMessageList = m_IConfMessageDAO.GetByCriteria(mcriterionList, true);
                            for (int confmesscnt = 0; confmesscnt < ConfMessageList.Count; confmesscnt++)
                            {
                                confMessage = ConfMessageList[confmesscnt];
                                DateTime MessageReleaseTime = conf.confEnd;
                                DateTime currentGMTtime = DateTime.Now;
                                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentGMTtime);
                                MessageReleaseTime = MessageReleaseTime.AddSeconds(-confMessage.duration);
                                if (MessageReleaseTime.ToString("MM/dd/yyyy hh:mm") == currentGMTtime.ToString("MM/dd/yyyy hh:mm"))
                                {
                                    messageoverlay = 1;
                                    searchOutXml.Append("<ConferenceTextMessage>" + confMessage.confMessage + "</ConferenceTextMessage>");
                                    continue;
                                }
                            }
                        }

                        searchOutXml.Append("<isTextMsg>" + messageoverlay.ToString() + "</isTextMsg>");

                        #endregion

                        #region Endpoints

                        searchOutXml.Append("<terminals>");

                        foreach (vrmConfRoom rm in conf.ConfRoom)
                        {
                            if (rm.connectStatus != 3)
                            {
                                vrmEndPoint ep = m_vrmEpt.GetByEptId(rm.endpointId);//FB 2027
                                searchOutXml.Append("<terminal>");
                                searchOutXml.Append("<endpointID>" + rm.roomId + "</endpointID>");
                                searchOutXml.Append("<type>2</type>"); // 1 = user , 2 = room, 3 = guest , 4 = cascade	
                                searchOutXml.Append("<address>" + rm.ipisdnaddress + "</address>");
                                searchOutXml.Append("<status>" + rm.OnlineStatus + "</status>");
                                if (ep != null) //FB 2717
                                    searchOutXml.Append("<isCalendarInvite>" + ep.CalendarInvite + "</isCalendarInvite>");
                                else
                                    searchOutXml.Append("<isCalendarInvite>0</isCalendarInvite>");
                                searchOutXml.Append("</terminal>");
                            }
                        }

                        foreach (vrmConfUser ur in conf.ConfUser)
                        {
                            if (ur.invitee == 1 && ur.connectStatus != 3)
                            {
                                searchOutXml.Append("<terminal>");
                                searchOutXml.Append("<type>" + ur.TerminalType + "</type>");
                                searchOutXml.Append("<endpointID>" + ur.userid + "</endpointID>");
                                searchOutXml.Append("<address>" + ur.ipisdnaddress + "</address>");
                                searchOutXml.Append("<status>" + ur.OnlineStatus + "</status>");
                                searchOutXml.Append("<isCalendarInvite>0</isCalendarInvite>");
                                searchOutXml.Append("</terminal>");
                            }
                        }

                        criterionList1.Add(Expression.Eq("confid", conf.confid));
                        if (conf.instanceid > 0)
                            criterionList1.Add(Expression.Eq("instanceid", conf.instanceid));
                        criterionList1.Add(Expression.Not(Expression.Eq("connectStatus", 3)));
                        List<vrmConfCascade> confCascade = m_IconfCascade.GetByCriteria(criterionList1);

                        foreach (vrmConfCascade cs in confCascade)
                        {
                            searchOutXml.Append("<terminal>");
                            searchOutXml.Append("<endpointID>" + cs.cascadeLinkId + "</endpointID>");
                            searchOutXml.Append("<type>4</type>"); // 1 = user , 2 = room, 3 = guest , 4 = cascade	
                            searchOutXml.Append("<address>" + cs.ipisdnAddress + "</address>");
                            searchOutXml.Append("<status>" + cs.OnlineStatus + "</status>");
                            searchOutXml.Append("<isCalendarInvite>0</isCalendarInvite>");
                            searchOutXml.Append("</terminal>");
                        }

                        searchOutXml.Append("</terminals>");

                        #endregion

                        //FB 2560 Code Changes ... End

                        searchOutXml.Append("</Conference>");
                    }
                }
                searchOutXml.Append("</Conferences>");
                searchOutXml.Append("</SearchConference>");
                obj.outXml = searchOutXml.ToString();
            }
            catch (Exception e)
            {
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2027 - Starts
        #region SearchTemplate
        /// <summary>
        /// SearchTemplate (COM to .Net conversion)  
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SearchTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool singleDeptMode = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            myVRMException myVRMEx = new myVRMException();
            StringBuilder outXML = new StringBuilder();
            int userid = 0;
            string orgid = "", tempName = "", party = "", description = "";
            List<int> UserIds = null;
            List<int> TempIds = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/templateName");
                if (node != null)
                    tempName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/includedParticipant");
                if (node != null)
                    party = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/descriptionIncludes");
                if (node != null)
                    description = node.InnerText.Trim();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    user = m_IuserDAO.GetByUserId(userid);
                }

                user = m_IuserDAO.GetByUserId(userid);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(user.roleID); //crossaccess

                criterionList.Add(Expression.Eq("orgId", organizationID));//If User is SuperAdmin
                criterionList.Add(Expression.Like("TmpName", "%%" + tempName + "%%").IgnoreCase());
                criterionList.Add(Expression.Like("TMPDescription", "%%" + description + "%%").IgnoreCase());

                if (singleDeptMode)
                {
                    if (!user.isSuperAdmin())
                    {
                        criterionList.Add(Expression.Or(Expression.Eq("TMPOwner", userid), Expression.Eq("TMPPublic", 1)));
                    }
                }
                else
                {
                    if (!(user.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (user.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_IuserDAO.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_IuserDAO.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                vrmUser users = userList[i];
                                userId.Add(users.userid);
                            }
                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("TMPOwner", userId)), Expression.Eq("TMPPublic", 1)));
                        }
                        else
                        {
                            criterionList.Add(Expression.Or(Expression.Eq("TMPOwner", userid), Expression.Eq("TMPPublic", 1))); //If its Gereral Users
                        }
                    }
                }
                List<vrmTemplate> tempresult = m_vrmTempDAO.GetByCriteria(criterionList);
                outXML.Append("<templates>");

                if (party.Length > 0)
                {
                    List<ICriterion> criterionList4 = new List<ICriterion>();
                    criterionList4.Add(Expression.Or(Expression.Like("FirstName", "%%" + party + "%%").IgnoreCase(), Expression.Like("LastName", "%%" + party + "%%").IgnoreCase()));
                    List<vrmUser> partyNames = m_IuserDAO.GetByCriteria(criterionList4);
                    List<vrmGuestUser> guestNames = m_IGuestUserDao.GetByCriteria(criterionList4);
                    if (guestNames.Count > 0)
                    {
                        for (int j = 0; j < guestNames.Count; j++)
                        {
                            if (UserIds == null)
                                UserIds = new List<int>();

                            UserIds.Add(guestNames[j].userid);
                        }
                    }
                    for (int j = 0; j < partyNames.Count; j++)
                    {
                        if (UserIds == null)
                            UserIds = new List<int>();

                        UserIds.Add(partyNames[j].userid);
                    }
                    if (UserIds != null)
                    {
                        if (UserIds.Count > 0)
                        {
                            List<ICriterion> criterionList3 = new List<ICriterion>();
                            criterionList3.Add(Expression.In("userid", UserIds));
                            List<vrmTempUser> tempUsrs = m_TempUserDAO.GetByCriteria(criterionList3);
                            for (int i = 0; i < tempUsrs.Count; i++)
                            {
                                if (TempIds == null)
                                    TempIds = new List<int>();

                                TempIds.Add(tempUsrs[i].TmpID);
                            }
                        }
                    }
                }

                if (!(party != "" && TempIds == null))
                {
                    for (int i = 0; i < tempresult.Count; i++)
                    {
                        if (TempIds != null)
                            if (TempIds.Count > 0)
                                if (!TempIds.Contains(tempresult[i].tmpId))
                                    continue;
                        vrmUser owner1 = m_IuserDAO.GetByUserId(tempresult[i].TMPOwner);

                        outXML.Append("<template>");
                        outXML.Append("<ID>" + tempresult[i].tmpId + "</ID>");
                        outXML.Append("<name>" + tempresult[i].TmpName + "</name>");
                        if (owner1 != null)//FB 2318
                        {
                            outXML.Append("<owner>");
                            outXML.Append("<ownerID>" + owner1.userid + "</ownerID>");
                            outXML.Append("<firstName>" + owner1.FirstName + "</firstName>");
                            outXML.Append("<lastName>" + owner1.LastName + "</lastName>");
                            outXML.Append("</owner>");
                        }
                        else
                            outXML.Append("<owner><ownerID/><firstName/><lastName/></owner>");

                        outXML.Append("<public>" + tempresult[i].TMPPublic + "</public>");
                        outXML.Append("<description>" + tempresult[i].TMPDescription + "</description>");
                        outXML.Append("<conferenceName>" + tempresult[i].externalname + "</conferenceName>");
                        outXML.Append("<conferenceDescription>" + m_utilFactory.ReplaceOutXMLSpecialCharacters(tempresult[i].confDescription) + "</conferenceDescription>"); //FB 2236
                        outXML.Append("</template>");
                    }
                }
                outXML.Append("</templates>");

                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
                
        #region Fetch3LvLocations
        /// <summary>
        /// Fetch3LvLocations
        /// </summary>
        /// <param name="lv3list"></param>
        /// <param name="userID"></param>
        public void Fetch3LvLocations(ref StringBuilder lv3list, int userID)
        {
           bool isAppend = false;
           StringBuilder str = new StringBuilder();
           try
            {
                List<vrmTier3> RoomList = new List<vrmTier3>();
                GetRoomTree(userID, ref RoomList);

                lv3list.Append("<level3List>");
                for (int t3 = 0; t3 < RoomList.Count; t3++)
                {
                    if (RoomList[t3].tier2.Count == 0) //FB 2027
                        continue;

                    isAppend = false;
                    str = new StringBuilder();

                    str.Append("<level3>");
                    str.Append("<level3ID>" + RoomList[t3].ID.ToString() + "</level3ID>");
                    str.Append("<level3Name>" + RoomList[t3].Name + "</level3Name>");
                    str.Append("<level2List>");
                    for (int t2 = 0; t2 < RoomList[t3].tier2.Count; t2++)
                    {
                        vrmTier2 T2 = (vrmTier2)RoomList[t3].tier2[t2];

                        if (T2.room.Count == 0) //FB 2027
                            continue;

                        str.Append("<level2>");
                        str.Append("<level2ID>" + T2.ID.ToString() + "</level2ID>");
                        str.Append("<level2Name>" + T2.Name + "</level2Name>");
                        str.Append("<level1List>");
                        for (int t1 = 0; t1 < T2.room.Count; t1++)
                        {
                            vrmRoom room = (vrmRoom)T2.room[t1];

                            if (room.m_display == false || room.disabled == 1)
                                continue;

                            if (room.VideoAvailable == 2)
                                room.VideoAvailable = 1;
                            else
                                room.VideoAvailable = 0;

                            str.Append("<level1>");
                            str.Append("<level1ID>" + room.roomId.ToString() + "</level1ID>");
                            str.Append("<level1Name>" + room.Name + "</level1Name>");
                            str.Append("<capacity>" + room.Capacity.ToString() + "</capacity>");
                            str.Append("<projector>" + room.ProjectorAvailable.ToString() + "</projector>");
                            str.Append("<maxNumConcurrent>" + room.MaxPhoneCall.ToString() + "</maxNumConcurrent>");
                            str.Append("<videoAvailable>" + room.VideoAvailable.ToString() + "</videoAvailable>");
                            str.Append("<deleted>" + room.disabled.ToString() + "</deleted>");
                            str.Append("<ExternalRoom>" + room.Extroom.ToString() + "</ExternalRoom>"); //fB 2426
                            str.Append("<IsVMR>" + room.IsVMR + "</IsVMR>"); //FB 2448
                            str.Append("<locked>0</locked>");
                            str.Append("<isPublic>" + room.isPublic + "</isPublic>");

                            if (room.locationApprover.Count > 0)
                                str.Append("<Approval>1</Approval>");
                            else
                                str.Append("<Approval>0</Approval>");

                            str.Append("<Approvers>");
                            for (int j = 1; j < room.locationApprover.Count + 1; j++)
                            {
                                vrmUser userInfo = m_IuserDAO.GetByUserId(room.locationApprover[j - 1].approverid);
                                str.Append("<Approver" + j + "ID>" + room.locationApprover[j - 1].approverid.ToString() + "</Approver" + j + "ID>");
                                str.Append("<Approver" + j + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "</Approver" + j + "Name>");
                            }
                            for (int j = room.locationApprover.Count + 1; j <= 3; j++)
                            {
                                str.Append("<Approver" + j + "ID></Approver" + j + "ID>");
                                str.Append("<Approver" + j + "Name></Approver" + j + "Name>");
                            }
                            str.Append("</Approvers>");
                            str.Append("</level1>");

                            isAppend = true;
                        }
                        str.Append("</level1List>");
                        str.Append("</level2>");
                    }
                    str.Append("</level2List>");
                    str.Append("</level3>");

                    if (isAppend)
                        lv3list.Append(str);
                }
                lv3list.Append("</level3List>");
            }
            catch (Exception ex)
            {
                m_log.Error("Fetch3LvLocations :" + ex.Message);
                throw ex;
            }
        }
        #endregion
        
        #region GetAudioUsage
        /// <summary>
        /// GetAudioUsage (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetAudioUsage(int audioUsage, ref StringBuilder outputXML)
        {
            try
            {
                outputXML = new StringBuilder();
                outputXML.Append("<audioUsage>");
                List<vrmMediaType> mt_List = vrmGen.getMediaTypes();
                foreach (vrmMediaType mt in mt_List)
                {
                    outputXML.Append("<audio>");
                    outputXML.Append("<audioUsageID>" + mt.Id.ToString() + "</audioUsageID>");
                    outputXML.Append("<audioUsageName>" + mt.mediaType + "</audioUsageName> ");
                    outputXML.Append("</audio>");
                }
                outputXML.Append("</audioUsage>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetVideoProto
        /// <summary>
        /// GetVideoProto (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetVideoProto(int VideoProto, ref StringBuilder outputXML)
        {
            try
            {
                outputXML = new StringBuilder();
                outputXML.Append("<videoProtocol>");
                List<vrmVideoProtocol> vp_List = vrmGen.getVideoProtocols();
                foreach (vrmVideoProtocol vp in vp_List)
                {
                    outputXML.Append("<video>");
                    outputXML.Append("<videoProtocolID>" + vp.Id.ToString() + "</videoProtocolID>");
                    outputXML.Append("<videoProtocolName>" + vp.VideoProtocolType + "</videoProtocolName> ");
                    outputXML.Append("</video>");
                }
                outputXML.Append("</videoProtocol>");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion
        
        #region GetSearchConference
        /// <summary>
        /// GetSearchConference(COM to .NET Convertion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSearchConference(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            myVRMException myVRMEx = null;
            bool bRet = true;
            int userid = 11;
            m_vrmFactor = new vrmFactory(ref obj);
            try
            {
                //StringBuilder CustAtt = new StringBuilder();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                outXml.Append("<conference>");
                outXml.Append("<locationList>");
                outXml.Append("<selected></selected>");
                outXml.Append("<mode>0</mode>");
                Fetch3LvLocations(ref outXml, userid); //Fetch Rooms
                outXml.Append("</locationList>");
                vrmUser user = m_IuserDAO.GetByUserId(userid);
                vrmConference conf = new vrmConference();
                m_vrmFactor.organizationID = organizationID; //FB 2045
                m_vrmFactor.FetchCustomAttrs(user, conf, ref outXml); 
                outXml.Append("</conference>");
                obj.outXml = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region McuUsage
        /// <summary>
        /// McuUsage
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool McuUsage(ref vrmDataObject obj)
        {
            bool singleInstanceOnly = false;
            bool bRet = true;
            StringBuilder searchOutXml = new StringBuilder();
            DateTime dateFrom = DateTime.Now;
            DateTime dateTo = DateTime.Now;
            int confUniqueID = 0, confHost = 0, pending = 0, pub = 0, type = 0, userId = 0, confOrigin = 0;
            int confid = 0, confnum = 0, instance = 0, recurring = 0, dyninvite = 0, conftype = 0, duration = 0;
            string datetype = "", confName = "", confPart = "";
            string stmt = "";
            vrmUser user = null;
            List<ICriterion> conf = new List<ICriterion>();
            List<ICriterion> conf1 = new List<ICriterion>();
            List<ICriterion> confusr = new List<ICriterion>();
            List<ICriterion> cfusr = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userId);

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//confSearch/confInfo/confDate/type");
                if (node != null)
                    datetype = node.InnerText.Trim();

                node = xd.SelectSingleNode("//confSearch/confInfo/confDate/from");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out dateFrom);
                DateTime fromDate = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.Day, 0, 0, 0);

                node = xd.SelectSingleNode("//confSearch/confInfo/confDate/to");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out dateTo);
                DateTime toDate = new DateTime(dateTo.Year, dateTo.Month, dateTo.Day, 23, 59, 59);

                node = xd.SelectSingleNode("//confSearch/confInfo/confName");
                if (node != null)
                    confName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//confSearch/confInfo/confUniqueID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out confUniqueID);

                node = xd.SelectSingleNode("//confSearch/confInfo/confHost");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out confHost);

                node = xd.SelectSingleNode("//confSearch/confInfo/pending");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pending);

                node = xd.SelectSingleNode("//confSearch/confInfo/public");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pub);

                node = xd.SelectSingleNode("//confResource/confParticipant");
                if (node != null)
                    confPart = node.InnerText.Trim();

                node = xd.SelectSingleNode("//confResource/confRooms/type");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out type);

                user = m_IuserDAO.GetByUserId(userId);
                int admin = user.Admin;
                searchOutXml.Append("<account>");
                searchOutXml.Append("<conferences>");
                if (!singleInstanceOnly)
                {
                    if (datetype == "5")
                    {
                        List<vrmConference> cf = new List<vrmConference>();
                        m_IconfDAO.clearOrderBy();
                        m_IconfDAO.addOrderBy((Order.Asc("confdate")));

                        conf1.Add(Expression.Eq("deleted", 0));
                        conf1.Add(Expression.Eq("orgId", organizationID));
                        conf1.Add(Expression.Ge("confdate", fromDate));
                        conf1.Add(Expression.Le("confdate", toDate));
                        if (admin != vrmUserConstant.SUPER_ADMIN)
                        {
                            List<vrmConference> cf1 = null;
                            List<ICriterion> conf2 = null;
                            cfusr.Add(Expression.Eq("userid", userId));
                            List<vrmConfUser> vrmconf = m_IconfUser.GetByCriteria(cfusr);
                            ArrayList arrconf = new ArrayList();
                            for (int i = 0; i < vrmconf.Count; i++)
                            {
                                arrconf.Add(vrmconf[i].confid);
                                conf2 = new List<ICriterion>();
                                conf2.Add(Expression.Eq("confid", vrmconf[i].confid));
                                conf2.Add(Expression.Eq("instanceid", vrmconf[i].instanceid));
                                conf2.AddRange(conf1);

                                cf1 = m_IconfDAO.GetByCriteria(conf2);
                                cf.AddRange(cf1);
                            }
                            conf1.Add(Expression.Or(Expression.Eq("isPublic", 1), Expression.Eq("owner", userId)));
                            conf1.Add(Expression.Not(Expression.In("confid", arrconf)));
                            cf1 = m_IconfDAO.GetByCriteria(conf1);
                            cf.AddRange(cf1);

                        }
                        else
                            cf = m_IconfDAO.GetByCriteria(conf1);

                        for (int i = 0; i < cf.Count; i++)
                        {
                            confid = cf[i].confid;
                            confnum = cf[i].confnumname;
                            instance = cf[i].instanceid;
                            string exname = cf[i].externalname;
                            DateTime confdate = cf[i].confdate;
                            timeZone.userPreferedTime(cf[i].timezone, ref  confdate);
                            recurring = cf[i].recuring;
                            dyninvite = cf[i].dynamicinvite;
                            conftype = cf[i].conftype;
                            duration = cf[i].duration;
                            string conftime = confdate.ToString("hh:mm tt");
                            confOrigin = cf[i].ConfOrigin;

                            searchOutXml.Append("<conference>");
                            if (recurring == 1)
                            {
                                searchOutXml.Append("<confID>" + confid + "," + instance + "</confID>");
                            }
                            else
                            {
                                searchOutXml.Append("<confID>" + confid + "</confID>");
                            }
                            searchOutXml.Append("<confUniqueID>" + confnum + "</confUniqueID>");
                            searchOutXml.Append("<confName>" + exname + "</confName>");
                            searchOutXml.Append("<confDate>" + confdate.ToString("MM/dd/yyyy") + "</confDate>");
                            searchOutXml.Append("<createBy>" + conftype + "</createBy>");  //	' 0: old style (old setting page); 1: new-CreateVideoConference; 2: ScheduleConferenceRooms; 3: ExecuteImmediateConference; 4: Point-to-Point; 5: Using Template;  
                            searchOutXml.Append("<duration>" + duration + "</duration>");


                            stmt = " SELECT TimeZone FROM myVRM.DataLayer.timeZoneData  WHERE ";
                            stmt += " timezoneid =" + cf[i].timezone;
                            IList result1 = m_IconfDAO.execQuery(stmt);
                            string timezone = result1[0].ToString();

                            searchOutXml.Append("<confOrigin>" + confOrigin + "</confOrigin>");
                            searchOutXml.Append("<confTime>" + conftime + " " + timezone + "</confTime>");
                            searchOutXml.Append("<confTime1>" + conftime + "</confTime1>");
                            searchOutXml.Append("<timezone1>" + timezone + "</timezone1>");

                            conf.Add(Expression.Eq("owner", userId));
                            conf.Add(Expression.Eq("confid", confid));
                            List<vrmConference> cflist = m_IconfDAO.GetByCriteria(conf);
                            if (cflist.Count > 0)
                                searchOutXml.Append("<owner>1</owner>");
                            else
                                searchOutXml.Append("<owner>0</owner>");

                            confusr.Add(Expression.Eq("confid", confid));
                            confusr.Add(Expression.Eq("instanceid", instance));
                            confusr.Add(Expression.Eq("userid", userId));
                            List<vrmConfUser> cfusrlist = m_IconfUser.GetByCriteria(confusr);
                            if (cfusrlist.Count > 0)
                                searchOutXml.Append("<partyInvite>" + cfusrlist[0].invitee + "</partyInvite>");
                            else
                                searchOutXml.Append("<partyInvite></partyInvite>");

                            vrmUser usr = null;
                            int owner = cf[i].owner;
                            usr = m_IuserDAO.GetByUserId(owner);
                            string host = usr.FirstName + usr.LastName;
                            string hostemail = usr.Email;

                            searchOutXml.Append("<host>" + host + "</host>");
                            searchOutXml.Append("<hostEmail>" + hostemail + "</hostEmail>");
                            searchOutXml.Append("<dynamicInvite>" + dyninvite + "</dynamicInvite>");

                            int isFuture = 1;
                            DateTime today = DateTime.Now;
                            if (today > confdate)
                                isFuture = 0;

                            searchOutXml.Append("<isFuture>" + isFuture + "</isFuture>");
                            searchOutXml.Append("<confFood></confFood>"); // Conf_FoodOrder_D table is not in use
                            searchOutXml.Append("<confResource></confResource>");// Conf_ResourceOrder_D table is not in use
                            searchOutXml.Append("</conference>");
                        }
                    }
                }
                searchOutXml.Append("</conferences>");
                searchOutXml.Append("</account>");
                obj.outXml = searchOutXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
        //FB 2027 - End

        /*** FB 2392 whygo ***/
        #region AddEndpointProfile
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guestEndpoints"></param>
        /// <param name="pId"></param>
        /// <param name="Profilename"></param>
        /// <param name="Endpointname"></param>
        /// <param name="Address"></param>
        /// <param name="AddressType"></param>
        /// <param name="isDefault"></param>
        /// <param name="ConnectionType"></param>
        /// <param name="LineRateId"></param>
        /// <returns></returns>
        public bool AddEndpointProfile(ref List<vrmEndPoint> guestEndpoints, int pId, String Profilename, String Endpointname, String Address, int AddressType, int isDefault, int ConnectionType, int LineRateId,int EquipmentID)
        {
            vrmEndPoint guestEpt = null;
            try
            {
                guestEpt = new vrmEndPoint();
                if (guestEndpoints == null)
                    guestEndpoints = new List<vrmEndPoint>();
                guestEpt.name = Endpointname;
                guestEpt.profileId = pId;
                guestEpt.profileName = Profilename;
                guestEpt.deleted = 0;
                guestEpt.isDefault = isDefault;
                guestEpt.encrypted = 0;
                guestEpt.addresstype = AddressType;
                guestEpt.password = "";
                guestEpt.address = Address;
                guestEpt.endptURL = "";
                guestEpt.outsidenetwork = 0;
                guestEpt.connectiontype = ConnectionType;
                guestEpt.videoequipmentid = 1;
                if (EquipmentID > 0)
                    guestEpt.videoequipmentid = EquipmentID;
                guestEpt.linerateid = LineRateId;
                guestEpt.bridgeid = -1;
                guestEpt.MCUAddress = "";
                guestEpt.MCUAddressType = -1;
                guestEpt.protocol = 1;
                guestEpt.TelnetAPI = 0;
                guestEpt.ExchangeID = "";
                guestEpt.CalendarInvite = 0;
                guestEpt.ConferenceCode = "";
                guestEpt.LeaderPin = "";
                guestEpt.orgId = organizationID;
                guestEndpoints.Add(guestEpt);


            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }

            return true;

        }
        #endregion

        #region SetPrivateEndpoint
        public bool SetPrivateEndpoint(ref List<vrmEndPoint> guestEndpoints,ref int EndpointID)
        {
            List<ICriterion> selcnt = null;
            List<vrmEndPoint> checkEptCount = null;
            List<ICriterion> checkName = null;
            List<ICriterion> criterionList = null;
            vrmEndPoint ve = null;
            List<vrmEndPoint> epl = null;
            List<vrmEndPoint> ep = null;
            int eId = 0;
            try
            {

                               
                if (EndpointID <= 0)
                {

                    selcnt = new List<ICriterion>();
                    selcnt.Add(Expression.Eq("deleted", 0));
                    selcnt.Add(Expression.Eq("isDefault", 1));
                    selcnt.Add(Expression.Eq("orgId", organizationID));

                    checkEptCount = m_vrmEpt.GetByCriteria(selcnt);
                    
                    if(orgInfo == null)
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (checkEptCount.Count >= orgInfo.MaxEndpoints)
                    {
                        
                        myVRMException ex = new myVRMException(458);
                        m_log.Error("Error EndPoint Limit exceeeded: ", ex);
                        return false;
                    }
                }


                for (int eptCnt = 0; eptCnt < guestEndpoints.Count; eptCnt++)
                {
                    ve = guestEndpoints[eptCnt];
                    
                    if (EndpointID <= 0)
                    {
                        if (eId <= 0)
                        {
                            checkName = new List<ICriterion>();
                            checkName.Add(Expression.Eq("name", ve.name));
                            checkName.Add(Expression.Not(Expression.Eq("profileId", 0)));
                            checkName.Add(Expression.Eq("deleted", 0));
                            checkName.Add(Expression.Eq("orgId", organizationID));
                            ep = m_vrmEpt.GetByCriteria(checkName);

                            if (ep.Count > 0)
                            {
                                myVRMException myVRMEx = new myVRMException(434);
                                m_log.Error("vrmException", myVRMEx);
                                return false;
                            }

                            m_vrmEpt.addProjection(Projections.Max("endpointid"));
                            IList maxId = m_vrmEpt.GetObjectByCriteria(new List<ICriterion>());
                            if (maxId[0] != null)
                                eId = ((int)maxId[0]) + 1;
                            else
                                eId = 1;
                            m_vrmEpt.clearProjection();
                        }

                        ve.endpointid = eId;
                        
                    }
                    else
                    {
                        ve.endpointid = EndpointID;
                        eId = EndpointID;
                        checkName = new List<ICriterion>();
                        checkName.Add(Expression.Eq("name", ve.name));
                        checkName.Add(Expression.Eq("orgId", organizationID));
                        checkName.Add(Expression.Eq("deleted", 0));
                        checkName.Add(Expression.Not(Expression.Eq("profileId", 0)));
                        checkName.Add(Expression.Not(Expression.Eq("endpointid", ve.endpointid)));
                        ep = m_vrmEpt.GetByCriteria(checkName);

                        if (ep.Count > 0)
                        {

                            myVRMException myVRMEx = new myVRMException(434);
                            m_log.Error("vrmException", myVRMEx);
                            return false;
                        }
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.And(
                                          Expression.Eq("endpointid", ve.endpointid),
                                          Expression.Eq("profileId", ve.profileId)));

                        epl = m_vrmEpt.GetByCriteria(criterionList);
                        if (epl.Count > 0)
                        {
                            ve.uId = epl[0].uId;
                        }

                        else
                        {
                            myVRMException e = new myVRMException(508);
                            m_log.Error(e.Message);
                            return false;
                        }
                    }                    
                }

                EndpointID = eId;
                m_vrmEpt.SaveOrUpdateList(guestEndpoints);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }

            return true;

        }
        #endregion

        //FB 2486 Start

        #region GetOngoingMessageOverlay
        ///<summary>
        ///FB 2486 This command is going to be used in services to get ongoing Text Message to participants
        ///It has a Status Filter Only and conferene time is returned in GMT
        ///<SearchConference>
        ///<UserID>11</UserID>
        ///<ConferenceID/>
        ///<ConferenceUniqueID/>
        ///<StatusFilter>
        ///<ConferenceStatus>5</ConferenceStatus>
        ///</StatusFilter>
        ///<TypeFilter>
        ///<ConfType>2</ConfType>
        ///</TypeFilter>
        ///</SearchConference>
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool GetOngoingMessageOverlay(ref vrmDataObject obj)
        {
            StringBuilder searchOutXml = new StringBuilder();
            List<vrmConference> confList = null;
            List<vrmConfMessage> ConfMessageList = null;
            List<vrmConfRoom> ConfRoomList = null;
            List<vrmConfUser> ConfUserList = null;
            List<ICriterion> criterionList = null;
            List<ICriterion> criterionListRoom = null;
            List<ICriterion> criterionListUser = null;
            ArrayList ar = new ArrayList();
            try
            {
                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNodeList statusList = xd.GetElementsByTagName("ConferenceStatus");
                List<int> confStatus = new List<int>();
                foreach (XmlNode innerNode in statusList)
                {
                    int iStatus = Int32.Parse(innerNode.InnerXml.Trim());
                    confStatus.Add(iStatus);
                }

                criterionList = new List<ICriterion>();

                if (confStatus.Count > 0)
                {
                    if (confStatus.Count == 1)
                        criterionList.Add(Expression.Eq("status", confStatus[0]));
                    else
                        criterionList.Add(Expression.In("status", confStatus));
                }

               
                XmlNodeList ConfTypeList = xd.GetElementsByTagName("ConfType");
                List<int> confType = new List<int>();
                foreach (XmlNode innerNode in ConfTypeList)
                {
                    int iConfType = Int32.Parse(innerNode.InnerXml.Trim());
                    confType.Add(iConfType);
                }
               
                if (confType.Count > 0)
                {
                    if (confType.Count == 1)
                    {
                        criterionList.Add(Expression.Eq("conftype", confType[0]));
                    }
                    else
                        criterionList.Add(Expression.In("conftype", confType));
                }
                
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("isVMR", 0));
                criterionList.Add(Expression.Eq("isTextMsg", 1));                        
                //
                confList = m_IconfDAO.GetByCriteria(criterionList);
                vrmConference conf = null;
                vrmConfMessage confMessage = null;
                searchOutXml.Append("<SearchConference>");
                searchOutXml.Append("<Conferences>");

                for (int confcnt = 0; confcnt < confList.Count; confcnt++)
                {
                    conf = confList[confcnt];

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf.confid));
                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                    ConfMessageList = m_IConfMessageDAO.GetByCriteria(criterionList, true);

                    for (int confmesscnt = 0; confmesscnt < ConfMessageList.Count; confmesscnt++)
                    {
                        confMessage = ConfMessageList[confmesscnt];
                        DateTime MessageReleaseTime = conf.confEnd;
                        DateTime currentGMTtime = DateTime.Now;
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentGMTtime);
                        //timeZone.changeToGMTTime(sysSettings.TimeZone, ref MessageReleaseTime);
                        MessageReleaseTime = MessageReleaseTime.AddSeconds(-confMessage.duration);
                        if (MessageReleaseTime.ToString("MM/dd/yyyy hh:mm") == currentGMTtime.ToString("MM/dd/yyyy hh:mm"))
                        {
                            searchOutXml.Append("<Conference>");
                            searchOutXml.Append("<ConferenceID>" + conf.confid.ToString() + "," +
                                    conf.instanceid.ToString() + "</ConferenceID>");

                            searchOutXml.Append("<ConferenceEndpoints>");

                            criterionListRoom = new List<ICriterion>();
                            criterionListRoom.Add(Expression.Eq("confid", conf.confid));
                            criterionListRoom.Add(Expression.Eq("instanceid", conf.instanceid));
                            criterionListRoom.Add(Expression.Eq("isTextMsg", 1)); 
                            ConfRoomList = m_IconfRoom.GetByCriteria(criterionListRoom, true);

                            if (ConfRoomList.Count > 0)
                            {
                                for (int confroomcnt = 0; confroomcnt < ConfRoomList.Count; confroomcnt++)
                                {
                                    searchOutXml.Append("<ConferenceEndpoint>");
                                    searchOutXml.Append("<EndpointID>" + ConfRoomList[confroomcnt].roomId.ToString() + "</EndpointID>");
                                    searchOutXml.Append("<TerminalType>2</TerminalType>");
                                    searchOutXml.Append("</ConferenceEndpoint>");
                                }
                            }

                            criterionListUser = new List<ICriterion>();
                            criterionListUser.Add(Expression.Eq("confid", conf.confid));
                            criterionListUser.Add(Expression.Eq("instanceid", conf.instanceid));
                            criterionListUser.Add(Expression.Eq("invitee", 1));
                            criterionListUser.Add(Expression.Not(Expression.Eq("connectStatus", 3)));
                            criterionListUser.Add(Expression.Eq("isTextMsg", 1)); 
                            ConfUserList = m_IconfUser.GetByCriteria(criterionListUser, true);
                            List<vrmUser> activeuser = m_IuserDAO.GetAll();
                            
                            for (int j = 0; j < activeuser.Count; j++)
                                ar.Add(activeuser[j].userid);
                   
                            if (ConfUserList.Count > 0)
                            {
                                for (int confusrcnt = 0; confusrcnt < ConfUserList.Count; confusrcnt++)
                                {
                                    searchOutXml.Append("<ConferenceEndpoint>");
                                    searchOutXml.Append("<EndpointID>" + ConfUserList[confusrcnt].userid.ToString() + "</EndpointID>");
                                    if (ar.Contains(ConfUserList[confusrcnt].userid))
                                    {
                                        searchOutXml.Append("<TerminalType>1</TerminalType>");
                                    }
                                    else
                                    {
                                        searchOutXml.Append("<TerminalType>3</TerminalType>");
                                    }
                                    searchOutXml.Append("</ConferenceEndpoint>");
                                }
                            }

                            searchOutXml.Append("</ConferenceEndpoints>");

                            searchOutXml.Append("<ConferenceActualStatus>" + conf.status.ToString() + "</ConferenceActualStatus>");

                            conf.status = vrmConfStatus.Ongoing;

                            searchOutXml.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                            searchOutXml.Append("<ConferenceName>" + conf.externalname.ToString() + "</ConferenceName>");
                            searchOutXml.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                            searchOutXml.Append("<ConferenceDateTime>" + conf.confdate.ToString("g") + "</ConferenceDateTime>");
                            searchOutXml.Append("<ConferenceDuration>" + conf.duration.ToString() + "</ConferenceDuration>");
                            searchOutXml.Append("<ConferenceStatus>" + conf.status.ToString() + "</ConferenceStatus>");

                            if (conf.LastRunDateTime == DateTime.MinValue)
                                searchOutXml.Append("<LastRunDate></LastRunDate>");
                            else
                                searchOutXml.Append("<LastRunDate>" + conf.LastRunDateTime.ToString() + "</LastRunDate>");

                            searchOutXml.Append("<ConferenceTextMessage>" + confMessage.confMessage + "</ConferenceTextMessage>");
                            searchOutXml.Append("</Conference>");
                        }
                    }
                }
                searchOutXml.Append("</Conferences>");
                searchOutXml.Append("</SearchConference>");
                obj.outXml = searchOutXml.ToString();
            }
            catch (Exception e)
            {
                obj.outXml = ""; 
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2486 End

        /** FB 2501 - Call Monitoring Start**/

        #region GetCallsforMonitor
        /// <summary>
        ///FB 2501 This command is going to be used in Call monito to get ongoing congferences
        /// 
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool GetCallsforMonitor(ref vrmDataObject obj)
        {
            StringBuilder searchOutXml = new StringBuilder();
            List<vrmConference> confList = null;
            List<vrmConference> mcuConfList = null;
            //List<vrmConference> mcuRoomConfList = null; //FB 2566 24 Dec,2012
            //List<vrmConference> mcuUserConfList = null; //FB 2566 24 Dec,2012
            List<ICriterion> criterionList = null;
            ICriterion criterium = null;
            List<vrmMCU> activeMCUList = null;
            string orgID = "";
            bool siteLevel = false;
            bool cascadeReturn = false;
            XmlNodeList nodeList = null;
            List<int> confParamList = null;
            int confParam = 0, UserID = 11, usercount = 0;
            DateTime confFrom = DateTime.Now;
            DateTime confEnd = DateTime.Now;
            vrmConference conf = null;
            XmlDocument xd = null;
            int Totalports = 0, Usedports = 0;//FB 2580
            bool availableports = false;//FB 2580
            try
            {
                m_obj = obj;
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                #region of Get active MCU



                if (xd.SelectSingleNode("//CallMonitor/organizationID") != null)
                    orgID = xd.SelectSingleNode("//CallMonitor/organizationID").InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgID, out organizationID);
                if (organizationID < 11)
                    siteLevel = true;



                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Sql("((DMAMonitorMCU = 1 and BridgeTypeid =13) or (DMAMonitorMCU = 0 and BridgeTypeid !=13))")); // FB 2441
                criterionList.Add(Expression.Eq("deleted", 0));
                if (!siteLevel)
                    criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1)));
                activeMCUList = m_vrmMCU.GetByCriteria(criterionList);



                #endregion

                if (xd.SelectSingleNode("//CallMonitor/CascadeReturn") != null)
                    Boolean.TryParse(xd.SelectSingleNode("//CallMonitor/CascadeReturn").InnerXml.Trim(), out cascadeReturn);

                if (xd.SelectSingleNode("//CallMonitor/UserID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//CallMonitor/UserID").InnerXml.Trim(), out UserID);

                vrmUser user = m_IuserDAO.GetByUserId(UserID);

                nodeList = xd.GetElementsByTagName("ConferenceStatus");
                confParamList = new List<int>();
                foreach (XmlNode innerNode in nodeList)
                {
                    confParam = 0;
                    confParam = Int32.Parse(innerNode.InnerXml.Trim());
                    confParamList.Add(confParam);
                }

                criterionList = new List<ICriterion>();
                if (confParamList.Count > 0)
                {
                    if (confParamList.Count == 1)
                        criterionList.Add(Expression.Eq("status", confParamList[0]));
                    else
                        criterionList.Add(Expression.In("status", confParamList));
                }
                if (user.Admin == 0) //FB 2996
                {
                    criterionList.Add(Expression.Or(Expression.Eq("userid", user.userid), Expression.Eq("owner", user.userid)));
                }

                nodeList = xd.GetElementsByTagName("ConfType");
                confParamList = new List<int>();
                foreach (XmlNode innerNode in nodeList)
                {
                    confParam = 0;
                    confParam = Int32.Parse(innerNode.InnerXml.Trim());
                    confParamList.Add(confParam);
                }


                if (confParamList.Count > 0)
                {
                    if (confParamList.Count == 1)
                        criterionList.Add(Expression.Eq("conftype", confParamList[0]));
                    else
                        criterionList.Add(Expression.In("conftype", confParamList));
                }

                if (getSearchDateRange(vrmSearchType.Ongoing, ref confEnd, ref confFrom, 0))//FB 2595
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                    criterium = Expression.Le("confdate", confEnd);
                    criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                }
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("isVMR", 0));
                if (!siteLevel)
                    criterionList.Add(Expression.Eq("orgId", organizationID));

             
                confList = m_IconfDAO.GetByCriteria(criterionList);

                //confList = confList.Where(s => (s.ConfRoom.Count > 0 || s.ConfUser.Count > 0)).ToList(); //FB 2566 24 Dec,2012

                if (!cascadeReturn)
                    confList = confList.Where(s => s.ConfCascade.Count <= 0).ToList();

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(searchOutXml, xSettings))
                {

                    xWriter.WriteStartElement("McuList");
                    xWriter.WriteElementString("McuTotalListCount", activeMCUList.Count.ToString());

                    for (int actMCUCnt = 0; actMCUCnt < activeMCUList.Count; actMCUCnt++)
                    {
                        Totalports = 0; Usedports = 0; //FB 2580
                        mcuConfList = new List<vrmConference>();
                        //FB 2646 Starts
                        organizationID = activeMCUList[actMCUCnt].orgId;
                        vrmOrganization vrmOrg = m_IOrgDAO.GetById(organizationID);
                        //FB 2646 Ends


                        //FB 2566 24 Dec,2012 Start
                        mcuConfList = confList.Where(s => s.ConfBridge.Count(r => r.BridgeID == activeMCUList[actMCUCnt].BridgeID) > 0).ToList();
                        //mcuRoomConfList = confList.Where(s => s.ConfRoom.Count(r => r.bridgeid == activeMCUList[actMCUCnt].BridgeID) > 0).ToList();
                        //mcuUserConfList = confList.Where(s => s.ConfBridge.Count(r => r. == activeMCUList[actMCUCnt].BridgeID) > 0).ToList();
                        //mcuConfList = mcuConfList.Concat(mcuRoomConfList).Concat(mcuUserConfList).Distinct().ToList();
                        //FB 2566 24 Dec,2012 End

                        xWriter.WriteStartElement("Mcu");
                        xWriter.WriteElementString("McuID", activeMCUList[actMCUCnt].BridgeID.ToString());
                        xWriter.WriteElementString("name" , activeMCUList[actMCUCnt].BridgeName);
                        xWriter.WriteElementString("SiloName" ,vrmOrg.orgname.ToString()); //FB 2646
                        // FB 2441 Starts
                        if (activeMCUList[actMCUCnt].MCUType.id == 13)
                            xWriter.WriteElementString("Address", activeMCUList[actMCUCnt].DMAURL);
                        else
                            xWriter.WriteElementString("Address", activeMCUList[actMCUCnt].BridgeAddress);
                        // FB 2441 Ends
                        xWriter.WriteElementString("McuType", activeMCUList[actMCUCnt].MCUType.name);
                        xWriter.WriteElementString("McuFavorite", activeMCUList[actMCUCnt].setFavourite.ToString());
                        //FB 2580 Start
                        Totalports = activeMCUList[actMCUCnt].portsAudioTotal + activeMCUList[actMCUCnt].portsVideoTotal;
                        Usedports = activeMCUList[actMCUCnt].portsAudioFree + activeMCUList[actMCUCnt].portsVideoFree;
                        if (Usedports == 0)
                            availableports = true;
                        //FB 2580 End
                        xWriter.WriteElementString("McuStatus", activeMCUList[actMCUCnt].Status.ToString());
                        vrmMCUParams mcuparam = m_IMcuParamDAO.GetByBridgeTypeId(activeMCUList[actMCUCnt].MCUType.id);

                        if (mcuparam != null)
                            MCUParamsOptions(mcuparam, activeMCUList[actMCUCnt], ref xWriter); //ZD 100113

                        xWriter.WriteStartElement("Conferences");
                        xWriter.WriteElementString("ConferenceCount", mcuConfList.Count.ToString());
                        for (int confcnt = 0; confcnt < mcuConfList.Count; confcnt++)
                        {

                            conf = mcuConfList[confcnt];

                            xWriter.WriteStartElement("Conference");
                            xWriter.WriteElementString("ConferenceID", conf.confid.ToString() + "," + conf.instanceid.ToString());
                            xWriter.WriteElementString("ConferenceOrgID", conf.orgId.ToString()); //FB 2646
                            xWriter.WriteElementString("ConferenceMcuID", activeMCUList[actMCUCnt].BridgeID.ToString());
                            xWriter.WriteElementString("ConferenceActualStatus", conf.status.ToString());

                            conf.status = vrmConfStatus.Ongoing;

                            xWriter.WriteElementString("ConferenceUniqueID", conf.confnumname.ToString());
                            if (conf.externalname.Trim().Length > 20)
                            {
                                // truncate to 20 chars 
                                conf.externalname = conf.externalname.Substring(0, 19).Trim();
                                conf.externalname += "...";
                            }
                            xWriter.WriteElementString("ConferenceName", conf.externalname.ToString());
                            xWriter.WriteElementString("ConferenceType", conf.conftype.ToString());
                            DateTime ServerStartDateTime = DateTime.Now;
                            DateTime confStartDateTime = conf.confdate;
                            timeZone.GMTToUserPreferedTime(user.TimeZone, ref confStartDateTime);
                            timeZone.changeToGMTTime(sysSettings.TimeZone, ref ServerStartDateTime);
                            timeZone.GMTToUserPreferedTime(user.TimeZone, ref ServerStartDateTime);
                            xWriter.WriteElementString("ConferenceDateTime", confStartDateTime.ToString());
                            xWriter.WriteElementString("ServerDateTime", ServerStartDateTime.ToString());
                            xWriter.WriteElementString("ConferenceDuration", conf.duration.ToString());
                            xWriter.WriteElementString("ConferenceStatus", conf.status.ToString());
                            xWriter.WriteElementString("CallStartMode", conf.StartMode.ToString());

                            if (conf.LastRunDateTime == DateTime.MinValue)
                                xWriter.WriteElementString("LastRunDate","");
                            else
                                xWriter.WriteElementString("LastRunDate", conf.LastRunDateTime.ToString());
                            // searchOutXml.Append("<NetworkState>" + conf.Secured.ToString() + "</NetworkState>");//FB 2595

                            if (mcuparam != null)
                                ConfMCUParams(mcuparam, conf, ref xWriter);

                            xWriter.WriteStartElement("PartyList");
                            foreach (vrmConfRoom rm in conf.ConfRoom)
                            {
                                if (rm.bridgeid != activeMCUList[actMCUCnt].BridgeID)
                                    continue;
                                xWriter.WriteStartElement("Party");
                                vrmRoom room = m_IRoomDAO.GetByRoomId(rm.roomId);
                                xWriter.WriteElementString("PartyId", room.roomId.ToString());
                                xWriter.WriteElementString("Name", room.Name);
                                xWriter.WriteElementString("Address", rm.ipisdnaddress.Trim());
                                xWriter.WriteElementString("Connectstatus", rm.OnlineStatus.ToString());
                                xWriter.WriteElementString("TerminalType","2"); // 1 = user , 2 = room, 3 = guest , 4 = cascade	
                                xWriter.WriteElementString("MCUAddress", activeMCUList[actMCUCnt].BridgeAddress);
                                if (mcuparam != null)
                                    PartyMCUParams(mcuparam, rm, null,ref xWriter);

                                xWriter.WriteEndElement();
                                usercount = usercount + 1;

                            }

                            foreach (vrmConfUser ur in conf.ConfUser)
                            {
                                if (ur.invitee == 1)
                                {
                                    if (ur.bridgeid != activeMCUList[actMCUCnt].BridgeID)
                                        continue;
                                    string name = "";
                                    xWriter.WriteStartElement("Party");
                                    if (ur.TerminalType == vrmConfTerminalType.NormalUser)
                                    {
                                        vrmUser actUsr = m_IuserDAO.GetByUserId(ur.userid);
                                        name = actUsr.FirstName + " " + actUsr.LastName;
                                        xWriter.WriteElementString("TerminalType", ur.TerminalType.ToString());
                                    }
                                    else
                                    {
                                        vrmGuestUser GstUsr = m_IGuestUserDao.GetByUserId(ur.userid);
                                        name = GstUsr.FirstName + " " + GstUsr.LastName;
                                        xWriter.WriteElementString("TerminalType", ur.TerminalType.ToString());
                                    }
                                    xWriter.WriteElementString("PartyId", ur.userid.ToString());
                                    xWriter.WriteElementString("Name", name);
                                    xWriter.WriteElementString("Address", ur.ipisdnaddress.Trim());
                                    xWriter.WriteElementString("Connectstatus", ur.OnlineStatus.ToString());
                                    xWriter.WriteElementString("MCUAddress", activeMCUList[actMCUCnt].BridgeAddress);
                                    if (mcuparam != null)
                                        PartyMCUParams(mcuparam, null, ur, ref xWriter);

                                    xWriter.WriteEndElement();
                                    usercount = usercount + 1;
                                }
                            }
                            xWriter.WriteElementString("PartyCount", usercount.ToString());
                            xWriter.WriteEndElement();
                            xWriter.WriteEndElement();
                            //FB 2580 Start
                            if (availableports)
                                Usedports += usercount;
                            //FB 2580 End
                            usercount = 0;
                        }
                        xWriter.WriteEndElement();
                        xWriter.WriteElementString("McuPorts", Usedports + "!" + Totalports);//FB 2580
                        xWriter.WriteEndElement();
                    }
                    xWriter.WriteEndElement();
                    xWriter.Flush();
                    obj.outXml = searchOutXml.ToString();
                }
                
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;
        }

        #endregion

        #region Conference MCU Params

        private bool ConfMCUParams(vrmMCUParams MCUParam, vrmConference conf, ref XmlWriter xWriter)
        {
            string linerate = "";
            try
            {
                xWriter.WriteElementString("conflockunlock", conf.ConfAdvAvParams.ConfLockUnlock.ToString());
                xWriter.WriteElementString("confmessage","0");
                xWriter.WriteElementString("confAudioTx", conf.ConfAdvAvParams.MuteTxaudio.ToString());
                xWriter.WriteElementString("confAudioRx", conf.ConfAdvAvParams.MuteRxaudio.ToString());
                xWriter.WriteElementString("confVideoTx", conf.ConfAdvAvParams.MuteTxvideo.ToString());
                xWriter.WriteElementString("confVideoRx", conf.ConfAdvAvParams.MuteRxvideo.ToString());
                xWriter.WriteElementString("confLayout" , conf.ConfAdvAvParams.videoLayoutID.ToString()); //Tamil
                xWriter.WriteElementString("confcamera", conf.ConfAdvAvParams.feccMode.ToString());

                linerate = ConvertToMcuLineRate(conf.linerate);
                xWriter.WriteElementString("confbandwidth" , linerate);
                xWriter.WriteElementString("confpacketloss" , conf.ConfAdvAvParams.PacketLoss);
                xWriter.WriteElementString("confrecord", conf.ConfAdvAvParams.Recording.ToString());

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException in GetSecImage", e);
                return false;
            }
        }

        #endregion

        #region Party MCU Params

        private bool PartyMCUParams(vrmMCUParams MCUParam, vrmConfRoom Room,vrmConfUser user,ref XmlWriter xWriter)
        {
            string URL = "";
            try
            {
                if (Room != null)
                {
                    string linerate = ConvertToMcuLineRate(Room.defLineRate);
                    xWriter.WriteElementString("partybandwidth", linerate);
                    xWriter.WriteElementString("partysetfocus", Room.Setfocus.ToString());
                    xWriter.WriteElementString("partymessage","0");
                    xWriter.WriteElementString("partyAudioTx", Room.mute.ToString());
                    xWriter.WriteElementString("partyAudioRx", Room.MuteRxaudio.ToString());
                    xWriter.WriteElementString("partyVideoTx", Room.MuteTxvideo.ToString());
                    xWriter.WriteElementString("partyVideoRx",Room.MuteRxvideo.ToString());
                    xWriter.WriteElementString("partyLayout", Room.layout.ToString());
                    xWriter.WriteElementString("partycamera", Room.Camera.ToString());
                    xWriter.WriteElementString("partypacketloss", Room.Packetloss.ToString());
                    if (Room.Stream == null ||  Room.OnlineStatus != 2) // FB 2501 Dec5 
                        Room.Stream = "";
                    
                    URL = Room.Stream.Replace("&", "&amp;");
                    xWriter.WriteElementString("rxPreviewURL", URL);
                    xWriter.WriteElementString("partylockunlock", Room.LockUnLock.ToString());
                    xWriter.WriteElementString("partyrecord", Room.Record.ToString());
                    xWriter.WriteElementString("partylecturemode", Room.isLecturer.ToString());
                    xWriter.WriteElementString("partychairperson", Room.ChairPerson.ToString()); //FB 2553-RMX
                }
                else
                {
                    string linerate = ConvertToMcuLineRate(user.defLineRate);
                    xWriter.WriteElementString("partybandwidth", linerate);
                    xWriter.WriteElementString("partysetfocus" , user.Setfocus.ToString());
                    xWriter.WriteElementString("partymessage","0");
                    xWriter.WriteElementString("partyAudioTx", user.mute.ToString());
                    xWriter.WriteElementString("partyAudioRx", user.MuteRxaudio.ToString());
                    xWriter.WriteElementString("partyVideoTx", user.MuteTxvideo.ToString());
                    xWriter.WriteElementString("partyVideoRx", user.MuteRxvideo.ToString());
                    xWriter.WriteElementString("partyLayout", user.layout.ToString());
                    xWriter.WriteElementString("partycamera", user.Camera.ToString());
                    xWriter.WriteElementString("partypacketloss", user.Packetloss.ToString());
                    if (user.Stream == null || user.OnlineStatus != 2)// FB 2501 Dec5 
                        user.Stream = "";
                    
                    URL = user.Stream.Replace("&", "&amp;");
                    xWriter.WriteElementString("rxPreviewURL", URL );
                    xWriter.WriteElementString("partylockunlock", user.LockUnLock.ToString());
                    xWriter.WriteElementString("partyrecord", user.Record.ToString());
                    xWriter.WriteElementString("partylecturemode", user.isLecturer.ToString());
                    xWriter.WriteElementString("partychairperson", user.ChairPerson.ToString()); //FB 2553-RMX
                }
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException in PartyMCUParams", e);
                return false;
            }
        }

        #endregion

        #region MCU Params options

        private bool MCUParamsOptions(vrmMCUParams MCUParam, vrmMCU MCU, ref XmlWriter xWriter)
        {
            string passphrase = "", Password = "", loginURL = "";
            try
            {
                xWriter.WriteStartElement("SupportMCUParams");
                xWriter.WriteElementString("conflockunlock", MCUParam.ConfLockUnLock.ToString());
                xWriter.WriteElementString("confmessage", MCUParam.ConfMessage.ToString());
                xWriter.WriteElementString("confAudioTx", MCUParam.confAudioTx.ToString());
                xWriter.WriteElementString("confAudioRx", MCUParam.confAudioRx.ToString());
                xWriter.WriteElementString("confVideoTx", MCUParam.confVideoTx.ToString());
                xWriter.WriteElementString("confVideoRx", MCUParam.confVideoRx.ToString());
                xWriter.WriteElementString("confLayout", MCUParam.confLayout.ToString());
                xWriter.WriteElementString("confcamera", MCUParam.confCamera.ToString());
                xWriter.WriteElementString("confpacketloss", MCUParam.confPacketloss.ToString());
                xWriter.WriteElementString("confrecord", MCUParam.confRecord.ToString());
                xWriter.WriteElementString("partybandwidth", MCUParam.partyBandwidth.ToString());
                xWriter.WriteElementString("partysetfocus", MCUParam.partySetfocus.ToString());
                xWriter.WriteElementString("partymessage", MCUParam.PartyMessage.ToString());
                xWriter.WriteElementString("partyAudioTx", MCUParam.partyAudioTx.ToString());
                xWriter.WriteElementString("partyAudioRx", MCUParam.partyAudioRx.ToString());
                xWriter.WriteElementString("partyVideoTx", MCUParam.partyVideoTx.ToString());
                xWriter.WriteElementString("partyVideoRx", MCUParam.partyVideoRx.ToString());
                xWriter.WriteElementString("partyLayout", MCUParam.partyLayout.ToString());
                xWriter.WriteElementString("partycamera", MCUParam.partyCamera.ToString());
                xWriter.WriteElementString("partypacketloss", MCUParam.partyPacketloss.ToString());
                xWriter.WriteElementString("partyimagestream", MCUParam.partyImagestream.ToString());
                xWriter.WriteElementString("partylock", MCUParam.partyLockUnLock.ToString());
                xWriter.WriteElementString("partyrecord", MCUParam.partyRecord.ToString());
                xWriter.WriteElementString("partylecturemode", MCUParam.partyLecturemode.ToString());
                // FB 2652 Starts
                xWriter.WriteElementString("confMuteAllExcept", MCUParam.confMuteAllExcept.ToString());
                xWriter.WriteElementString("confUnmuteAll", MCUParam.confUnmuteAll.ToString());
                //FB 2652 Ends
                xWriter.WriteElementString("partyLeader", MCUParam.partyLeader.ToString());//FB 2553
                //ZD 100113 Start
                if (MCU.URLAccess == 1)
                    loginURL = "https://" + MCU.BridgeAddress + "/" + MCUParam.LoginURL;
                else
                    loginURL = "http://" + MCU.BridgeAddress + "/" + MCUParam.LoginURL;

                xWriter.WriteElementString("LoginURL", loginURL);

                if (MCU.BridgePassword != "") 
                {
                    cryptography.Crypto crypt = new cryptography.Crypto();
                    Password = crypt.decrypt(MCU.BridgePassword);
                    passphrase = string.Format(MCUParam.PassPhrase, MCU.BridgeLogin, Password);
                }
                xWriter.WriteElementString("PassPhrase", passphrase);
                //ZD 100113 End
                xWriter.WriteEndElement();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException in MCUParamsOptions", e);
                return false;
            }
        }

        #endregion

        #region MCU Params options

        private string ConvertToMcuLineRate(int line_rate)
		{
			try
			{
				switch(line_rate)
				{
					case 56: 
					{
						return "56 kbps";
					}
					case 64: 
					{
                        return "64 kbps";
					}
					case 112: 
					{
                        return "112 kbps";
					}
					case 127: 
					{
                        return "127 kbps";
					}
					case 128 :
					{
                        return "128 kbps";
					}
                    case 192:
                    {
                        return "192 kbps";
                    }
					case 256 :
					{
                        return "256 kbps";
					}
                    case 320:
                    {
                        return "320 kbps";
                    }			
					case 512:
					{
                        return "512 kbps";
					}				
					case 768:
					{
                        return "768 kbps";
					}
                    case 1024:
                    {
                        return "1 Mbps";
                    }
					case 1152:
					{
                        return "1.152 Mbps";
					}
                    case 1250:
                    {
                        return "1.25 Mbps";
                    }
					case 1472:
					{
                        return "1.472 Mbps";
					}
                    case 1536:
                    {
                        return "1.5 Mbps";
                    }
                    case 1792:
                    {
                        return "1.75 Mbps";
                    }
                    case 1920:
                    {
                        return "1.92 Mbps";
                    }
                    case 2048:
                    {
                        return "2 Mbps";
                    }
                    case 2560:
                    {
                        return "2.5 Mbps";
                    }
                    case 3072:
                    {
                        return "3 Mbps";
                    }
                    case 3584:
                    {
                        return "3.5 Mbps";
                    }
                    case 4096:
                    {
                        return "4 Mbps";
                    }
					case 384:
					default :
					{
                       return "No Bandwidth";
					}
				}
			}
            catch (Exception e)
            {
                m_log.Error("sytemException in ConvertToMcuLineRate", e);
                return "";
            }
        }

        #endregion

        /** FB 2501 - Call Monitoring End**/

        /** FB 2501 - Point-to-Point Start**/

        #region GetP2PCallsforMonitor
        /// <summary>
        ///FB 2501 This command is going to be used in Call monitor to get ongoing P2P conferences
        /// 
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool GetP2PCallsforMonitor(ref vrmDataObject obj)
        {
            StringBuilder searchOutXml = new StringBuilder();
            List<vrmConference> confList = null;
            List<ICriterion> criterionList = null;
            ICriterion criterium = null;
            int UserID = 11;
            bool cascadeReturn = false;
            bool siteLevel = false; //FB 2646
            string orgID = "";
            try
            {
                m_obj = obj;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                if (xd.SelectSingleNode("//P2PCallMonitor/organizationID") != null)
                    orgID = xd.SelectSingleNode("//P2PCallMonitor/organizationID").InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgID, out organizationID);
                if (organizationID < 11) //FB 2646
                    siteLevel = true;

                if (xd.SelectSingleNode("//P2PCallMonitor/CascadeReturn") != null)
                    Boolean.TryParse(xd.SelectSingleNode("//P2PCallMonitor/CascadeReturn").InnerXml.Trim(), out cascadeReturn);

                if (xd.SelectSingleNode("//P2PCallMonitor/UserID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//P2PCallMonitor/UserID").InnerXml.Trim(), out UserID);

                vrmUser user = m_IuserDAO.GetByUserId(UserID);

                XmlNodeList statusList = xd.GetElementsByTagName("ConferenceStatus");
                List<int> confStatus = new List<int>();
                foreach (XmlNode innerNode in statusList)
                {
                    int iStatus = Int32.Parse(innerNode.InnerXml.Trim());
                    confStatus.Add(iStatus);
                }

                criterionList = new List<ICriterion>();


                if (confStatus.Count > 0)
                {
                    if (confStatus.Count == 1)
                        criterionList.Add(Expression.Eq("status", confStatus[0]));
                    else
                        criterionList.Add(Expression.In("status", confStatus));
                }
                if (user.Admin == 0) //FB 2996
                {
                    criterionList.Add(Expression.Or(Expression.Eq("userid", user.userid), Expression.Eq("owner", user.userid)));
                    //criterionList.Add(Expression.Eq("userid", user.userid));
                    //criterionList.Add(Expression.Eq("owner", user.userid));
                }
                string confType = "";
                int iConfType = 0;

                XmlNode node = xd.SelectSingleNode("//P2PCallMonitor/TypeFilter/ConfType");
                if (node != null)
                    confType = node.InnerXml.Trim();

                Int32.TryParse(confType, out iConfType);

                criterionList.Add(Expression.Eq("conftype", iConfType));

                DateTime confFrom = DateTime.Now;
                DateTime confEnd = DateTime.Now;

                if (sysSettings.EnableLaunchBufferP2P == 0)
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                    criterium = Expression.Le("confdate", confEnd);
                    criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                }
                else if (getSearchDateRange(vrmSearchType.Ongoing, ref confEnd, ref confFrom, 0))//FB 2595
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                    criterium = Expression.Le("confdate", confEnd);
                    criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                }

                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("isVMR", 0));
                if(!siteLevel) //FB 2646
                    criterionList.Add(Expression.Eq("orgId", organizationID));

                

                confList = m_IconfDAO.GetByCriteria(criterionList);

                vrmConference conf = null;

                searchOutXml.Append("<SearchConference>");
                searchOutXml.Append("<Conferences>");
                 searchOutXml.Append("<ConferenceCount>" + confList.Count + "</ConferenceCount>"); //Tamil1
                for (int confcnt = 0; confcnt < confList.Count; confcnt++)
                {
                    //FB 2646 Starts
                    organizationID = confList[confcnt].orgId;
                    vrmOrganization vrmOrg = m_IOrgDAO.GetById(organizationID);
                    //FB 2646 Ends

                    conf = confList[confcnt];
                    searchOutXml.Append("<Conference>");                  
                    searchOutXml.Append("<ConferenceID>" + conf.confid.ToString() + "," +
                            conf.instanceid.ToString() + "</ConferenceID>");
                    //FB 2646 Starts
                    searchOutXml.Append("<SiloName>" + vrmOrg.orgname.ToString() + "</SiloName>"); 
                    searchOutXml.Append("<ConferenceOrgID>" + vrmOrg.orgId.ToString() + "</ConferenceOrgID>");
                    //FB 2646 Ends
                    searchOutXml.Append("<ConferenceActualStatus>" + conf.status.ToString() + "</ConferenceActualStatus>");

                    conf.status = vrmConfStatus.Ongoing;

                    searchOutXml.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                    if (conf.externalname.Trim().Length > 20)
                    {
                        // truncate to 20 chars 
                        conf.externalname = conf.externalname.Substring(0, 19).Trim();
                        conf.externalname += "...";
                    }
                    searchOutXml.Append("<ConferenceName>" + conf.externalname.ToString() + "</ConferenceName>");
                    searchOutXml.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                    //Tamil1 Start
                    DateTime ServerStartDateTime = DateTime.Now;
                    DateTime confStartDateTime = conf.confdate;
                    timeZone.GMTToUserPreferedTime(user.TimeZone, ref confStartDateTime);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref ServerStartDateTime);
                    timeZone.GMTToUserPreferedTime(user.TimeZone, ref ServerStartDateTime);
                    searchOutXml.Append("<ConferenceDateTime>" + confStartDateTime + "</ConferenceDateTime>");
                    searchOutXml.Append("<ServerDateTime>" + ServerStartDateTime + "</ServerDateTime>");
                    //Tamil1 End
                    searchOutXml.Append("<ConferenceDuration>" + conf.duration.ToString() + "</ConferenceDuration>");
                    searchOutXml.Append("<ConferenceStatus>" + conf.status.ToString() + "</ConferenceStatus>");
                    searchOutXml.Append("<CallStartMode>" + conf.StartMode.ToString() + "</CallStartMode>");

                    if (conf.LastRunDateTime == DateTime.MinValue)
                        searchOutXml.Append("<LastRunDate></LastRunDate>");
                    else
                        searchOutXml.Append("<LastRunDate>" + conf.LastRunDateTime.ToString() + "</LastRunDate>");
                    //searchOutXml.Append("<NetworkState>" + conf.Secured.ToString() + "</NetworkState>");//FB 2595

                    searchOutXml.Append("<PartyList>");
                    foreach (vrmConfRoom rm in conf.ConfRoom)
                    {
                        searchOutXml.Append("<Party>");
                        vrmRoom room = m_IRoomDAO.GetByRoomId(rm.roomId);
                        searchOutXml.Append("<PartyId>" + room.roomId + "</PartyId>");
                        searchOutXml.Append("<Name>" + room.Name + "</Name>");
                        searchOutXml.Append("<Address>" + rm.ipisdnaddress + "</Address>");
                        searchOutXml.Append("<Connectstatus>" + rm.OnlineStatus + "</Connectstatus>");
                        searchOutXml.Append("<CallType>" + rm.connect2 + "</CallType>");
                        searchOutXml.Append("<TerminalType>2</TerminalType>"); // 1 = user , 2 = room, 3 = guest , 4 = cascade	
                        searchOutXml.Append("<rxPreviewURL>" + rm.Stream + "</rxPreviewURL>");
                        searchOutXml.Append("</Party>");
                    }

                    foreach (vrmConfUser ur in conf.ConfUser)
                    {
                        if (ur.invitee == 1)
                        {
                            string name = "";
                            searchOutXml.Append("<Party>");
                            if (ur.TerminalType == vrmConfTerminalType.NormalUser)
                            {
                                vrmUser actUsr = m_IuserDAO.GetByUserId(ur.userid);
                                name = actUsr.FirstName + " " + actUsr.LastName;
                                searchOutXml.Append("<TerminalType>" + ur.TerminalType + "</TerminalType>");
                            }
                            else
                            {
                                vrmGuestUser GstUsr = m_IGuestUserDao.GetByUserId(ur.userid);
                                name = GstUsr.FirstName + " " + GstUsr.LastName;
                                searchOutXml.Append("<TerminalType>" + ur.TerminalType + "</TerminalType>");
                            }
                            searchOutXml.Append("<PartyId>" + ur.userid + "</PartyId>");
                            searchOutXml.Append("<Name>" + name + "</Name>");
                            searchOutXml.Append("<Address>" + ur.ipisdnaddress + "</Address>");
                            searchOutXml.Append("<Connectstatus>" + ur.OnlineStatus + "</Connectstatus>");
                            searchOutXml.Append("<CallType>" + ur.connect2 + "</CallType>");
                            searchOutXml.Append("<rxPreviewURL>" + ur.Stream + "</rxPreviewURL>");
                            searchOutXml.Append("</Party>");
                        }
                    }
                    searchOutXml.Append("</PartyList>");
                    searchOutXml.Append("</Conference>");
                }
                searchOutXml.Append("</Conferences>");


                searchOutXml.Append("</SearchConference>");
                obj.outXml = searchOutXml.ToString();
            }
            catch (Exception e)
            {
                obj.outXml = "";
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }

        #endregion

        /** FB 2501 - Point-to-Point End**/


        //FB 2539 Start

        #region CheckApproverRights
       /// <summary>
        /// CheckApproverRights
       /// </summary>
       /// <param name="userid"></param>
       /// <returns></returns>
        public bool CheckApproverRights(string userid)
        {
            bool hasCalendar = true;
            int id = 0;
            try
            {
                if (userid != null && userid != "")
                {
                    Int32.TryParse(userid, out id);
                    if (id > 0)
                    {
                        vrmUser user = m_IuserDAO.GetByUserId(id);
                        if (user.roleID != 7 && user.roleID != 8)
                        {
                            string[] mary = user.MenuMask.ToString().Split('-');
                            string[] mmary = mary[1].Split('+');
                            string[] ccary = mary[0].Split('*');
                            int topMenu = Convert.ToInt32(ccary[1]);
                            int adminMenu = Convert.ToInt32(mmary[0].Split('*')[1]);
                            int orgMenu = Convert.ToInt32(mmary[1].Split('*')[1]);
                            hasCalendar = Convert.ToBoolean(topMenu & 64);
                        }
                        else
                        {
                            hasCalendar = false;
                        }
                    }
                }
                return hasCalendar;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        #endregion

        //FB 2539 End

        //FB 2594 Starts

        #region GetRoomList
        /// <summary>
        /// GetRoomList
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool GetConfRoomList(string userid, List<int> Confrooms, ref string outXml)
        {
            bool bRet = true;

            try
            {
                outXml += "<level3List>";
                List<vrmTier3> RoomList = new List<vrmTier3>();
                List<vrmRoom> ConfRoomList = new List<vrmRoom>(); //FB 2594
                List<ICriterion> ConfRooms = new List<ICriterion>();
                if (GetRoomTree(Int32.Parse(userid), ref RoomList))
                {
                    foreach (vrmTier3 t3 in RoomList)
                    {
                        if (t3.lowLevelCount > 0)
                        {
                            outXml += "<level3>";
                            outXml += "<level3ID>" + t3.ID.ToString() + "</level3ID>";
                            outXml += "<level3Name>" + t3.Name + "</level3Name> ";
                            outXml += "<level2List>";

                            if (t3.tier2.Count > 0)
                            {
                                foreach (vrmTier2 t2 in t3.tier2)
                                {
                                    if (t2.lowLevelCount > 0)
                                    {
                                        outXml += "<level2>";
                                        outXml += "<level2ID>" + t2.ID.ToString() + "</level2ID> ";
                                        outXml += "<level2Name>" + t2.Name + "</level2Name> ";
                                        outXml += "<level1List>";

                                        if (t2.room.Count > 0)
                                        {

                                            ConfRoomList = new List<vrmRoom>(); //FB 2594
                                            ConfRooms = new List<ICriterion>(); //FB 2594
                                            ConfRooms.Add(Expression.In("RoomID", Confrooms)); //FB 2594
                                            ConfRooms.Add(Expression.Sql("L2LocationId = " + t2.ID.ToString())); //FB 2594
                                            ConfRoomList = m_IRoomDAO.GetByCriteria(ConfRooms);
                                            for (int i = 0; i < ConfRoomList.Count; i++)
                                            {
                                                vrmRoom rm = ConfRoomList[i];
                                                if (rm.m_display && rm.disabled == 0) //FB 1830 //FB 2205-s( rm.b_display )
                                                {
                                                    outXml += "<level1>";                                                    
                                                    outXml += "<level1ID>" + rm.roomId.ToString() + "</level1ID>";
                                                    outXml += "<level1Name>" + rm.Name + "</level1Name>";
                                                    outXml += "<capacity>" + rm.Capacity.ToString() + "</capacity>";
                                                    outXml += "<disabled>" + rm.disabled + "</disabled>";   //Fb 1438
                                                    outXml += "<projector>" + rm.ProjectorAvailable.ToString() + "</projector>";
                                                    outXml += "<maxNumConcurrent>" +
                                                                               rm.MaxPhoneCall.ToString() + "</maxNumConcurrent>";
                                                    outXml += "<videoAvailable>" +
                                                                               rm.VideoAvailable.ToString() + "</videoAvailable>";
                                                    outXml += "<FlyRoom>" + rm.Extroom.ToString() + "</FlyRoom>"; //FB 2426
                                                    outXml += "<IsVMR>" + rm.IsVMR + "</IsVMR>"; //FB 2448
                                                    outXml += "</level1>";
                                                }
                                            }
                                        }
                                        outXml += "</level1List>";
                                        outXml += "</level2>";
                                    }
                                }
                            }
                            outXml += "</level2List>";
                            outXml += "</level3>";
                        }
                    }
                }

                outXml += "</level3List>";
                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        public bool GetConfRoomListOptimized(string userid, List<int> Confrooms, ref StringBuilder outXml)// FB 2594
        {
            bool bRet = true;
            List<vrmTier3> RoomList = null;
            List<vrmRoom> ConfRoomList = null; //FB 2594
            List<ICriterion> ConfRooms = null;
            vrmRoom rm = null;
            string cnfRooms = "";
            try
            {
                if (Confrooms.Count > 0)
                {
                    for (int rms = 0; rms < Confrooms.Count; rms++)
                    {
                        if (cnfRooms.Trim() == "")
                            cnfRooms = Confrooms[rms].ToString();
                        else
                            cnfRooms += "," + Confrooms[rms].ToString();
                    }
                }

                outXml.Append("<level3List>");
                RoomList = new List<vrmTier3>();
                ConfRoomList = new List<vrmRoom>(); //FB 2594
                ConfRooms = new List<ICriterion>();
                if (GetRoomTreeOptimized(Int32.Parse(userid), ref RoomList, cnfRooms))
                {
                    foreach (vrmTier3 t3 in RoomList)
                    {
                        if (t3.lowLevelCount > 0)
                        {
                            outXml.Append("<level3>");
                            outXml.Append("<level3ID>" + t3.ID.ToString() + "</level3ID>");
                            outXml.Append("<level3Name>" + t3.Name + "</level3Name> ");
                            outXml.Append("<level2List>");

                            if (t3.tier2.Count > 0)
                            {
                                foreach (vrmTier2 t2 in t3.tier2)
                                {
                                    if (t2.lowLevelCount > 0)
                                    {
                                        outXml.Append("<level2>");
                                        outXml.Append("<level2ID>" + t2.ID.ToString() + "</level2ID>");
                                        outXml.Append("<level2Name>" + t2.Name + "</level2Name>");
                                        outXml.Append("<level1List>");

                                        if (t2.room.Count > 0)
                                        {
                                            ConfRoomList = new List<vrmRoom>(); //FB 2594
                                            ConfRooms = new List<ICriterion>(); //FB 2594
                                            ConfRooms.Add(Expression.In("RoomID", Confrooms)); //FB 2594
                                            ConfRooms.Add(Expression.Sql("L2LocationId = " +t2.ID.ToString() )); //FB 2594
                                            ConfRoomList = m_IRoomDAO.GetByCriteria(ConfRooms);
                                            for (int i = 0; i < ConfRoomList.Count; i++)
                                            {
                                                rm = ConfRoomList[i];
                                                if (rm.m_display && rm.disabled == 0) //FB 1830 //FB 2205-s( rm.b_display )
                                                {
                                                    outXml.Append("<level1>");
                                                    outXml.Append("<level1ID>" + rm.roomId.ToString() + "</level1ID>");
                                                    outXml.Append("<level1Name>" + rm.Name + "</level1Name>");
                                                    outXml.Append("<capacity>" + rm.Capacity.ToString() + "</capacity>");
                                                    outXml.Append("<disabled>" + rm.disabled + "</disabled>");   //Fb 1438
                                                    outXml.Append("<projector>" + rm.ProjectorAvailable.ToString() + "</projector>");
                                                    outXml.Append("<maxNumConcurrent>" +
                                                                               rm.MaxPhoneCall.ToString() + "</maxNumConcurrent>");
                                                    outXml.Append("<videoAvailable>" +
                                                                               rm.VideoAvailable.ToString() + "</videoAvailable>");
                                                    outXml.Append("<FlyRoom>" + rm.Extroom.ToString() + "</FlyRoom>"); //FB 2426
                                                    outXml.Append("<IsVMR>" + rm.IsVMR + "</IsVMR>"); //FB 2448
                                                    outXml.Append("</level1>");
                                                }
                                            }
                                        }
                                        outXml.Append("</level1List>");
                                        outXml.Append("</level2>");
                                    }
                                }
                            }
                            outXml.Append("</level2List>");
                            outXml.Append("</level3>");
                        }
                    }
                }

                outXml.Append("</level3List>");
                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        //FB 2639_GetOldConf Start
        public bool GetConfRoomListOptimized(string userid, List<int> Confrooms, ref XmlWriter outXml)
        {
            bool bRet = true;
            List<vrmTier3> RoomList = null;
            List<vrmRoom> ConfRoomList = null; //FB 2594
            List<ICriterion> ConfRooms = null;
            vrmRoom rm = null;
            string cnfRooms = "";
            try
            {
                if (Confrooms.Count > 0)
                {
                    for (int rms = 0; rms < Confrooms.Count; rms++)
                    {
                        if (cnfRooms.Trim() == "")
                            cnfRooms = Confrooms[rms].ToString();
                        else
                            cnfRooms += "," + Confrooms[rms].ToString();
                    }
                }

                outXml.WriteStartElement("level3List");
                RoomList = new List<vrmTier3>();
                ConfRoomList = new List<vrmRoom>(); //FB 2594
                ConfRooms = new List<ICriterion>();
                if (GetRoomTreeOptimized(Int32.Parse(userid), ref RoomList, cnfRooms))
                {
                    foreach (vrmTier3 t3 in RoomList)
                    {
                        if (t3.lowLevelCount > 0)
                        {
                            outXml.WriteStartElement("level3");
                            outXml.WriteElementString("level3ID", t3.ID.ToString());
                            outXml.WriteElementString("level3Name", t3.Name);
                            outXml.WriteStartElement("level2List");

                            if (t3.tier2.Count > 0)
                            {
                                foreach (vrmTier2 t2 in t3.tier2)
                                {
                                    if (t2.lowLevelCount > 0)
                                    {
                                        outXml.WriteStartElement("level2");
                                        outXml.WriteElementString("level2ID", t2.ID.ToString());
                                        outXml.WriteElementString("level2Name", t2.Name);
                                        outXml.WriteStartElement("level1List");

                                        if (t2.room.Count > 0)
                                        {
                                            ConfRoomList = new List<vrmRoom>(); //FB 2594
                                            ConfRooms = new List<ICriterion>(); //FB 2594
                                            ConfRooms.Add(Expression.In("RoomID", Confrooms)); //FB 2594
                                            ConfRooms.Add(Expression.Sql("L2LocationId = " + t2.ID.ToString())); //FB 2594
                                            ConfRoomList = m_IRoomDAO.GetByCriteria(ConfRooms);
                                            for (int i = 0; i < ConfRoomList.Count; i++)
                                            {
                                                rm = ConfRoomList[i];
                                                if (rm.m_display && rm.disabled == 0) //FB 1830 //FB 2205-s( rm.b_display )
                                                {
                                                    outXml.WriteStartElement("level1");
                                                    outXml.WriteElementString("level1ID", rm.roomId.ToString());
                                                    outXml.WriteElementString("level1Name", rm.Name);
                                                    outXml.WriteElementString("capacity", rm.Capacity.ToString());
                                                    outXml.WriteElementString("disabled", rm.disabled.ToString());   //Fb 1438
                                                    outXml.WriteElementString("projector", rm.ProjectorAvailable.ToString());
                                                    outXml.WriteElementString("maxNumConcurrent", rm.MaxPhoneCall.ToString());
                                                    outXml.WriteElementString("videoAvailable", rm.VideoAvailable.ToString());
                                                    outXml.WriteElementString("FlyRoom", rm.Extroom.ToString()); //FB 2426
                                                    outXml.WriteElementString("IsVMR", rm.IsVMR.ToString()); //FB 2448
                                                    outXml.WriteEndElement();
                                                }
                                            }
                                        }
                                        outXml.WriteEndElement();
                                        outXml.WriteEndElement();
                                    }
                                }
                            }
                            outXml.WriteEndElement();
                            outXml.WriteEndElement();
                        }
                    }
                }

                outXml.WriteEndElement();
                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        //FB 2639_GetOldConf End

        #endregion

        //FB 2594 Ends

        //FB 2595 Starts

        #region GetCallsForSwitching
        /// <summary>
        /// This command is going to be used in services to get Secured/UnSecured ongoing congferences
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool GetCallsForSwitching(ref vrmDataObject obj)
        {
            StringBuilder searchOutXml = new StringBuilder();
            List<vrmConference> confList = null;
            List<ICriterion> criterionList = null;
            List<ICriterion> criterionList1 = new List<ICriterion>();
            ICriterion criterium = null;
            List<OrgData> orgDts = null;
            DateTime confFrom = DateTime.Now;
            DateTime confEnd = DateTime.Now;
            List<int> confStatus = new List<int>();
            List<int> confType = new List<int>();
            XmlNodeList ConfTypeList = null;
            XmlNodeList statusList = null;
            XmlDocument xd = null;
            vrmConference conf = null;
            XmlNode innerNode = null;
            int iStatus = 0, iConfType = 0;
            bool isP2PServiceCall = false;
            double secs = 0;
            try
            {
                m_obj = obj;
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                orgDts = m_IOrgSettingsDAO.GetAll();
                orgDts = orgDts.Where(org => org.SecureSwitch == 1 && org.NetworkSwitching != 1).ToList();

                statusList = xd.GetElementsByTagName("ConferenceStatus");

                for (int i = 0; i < statusList.Count; i++)
                {
                    innerNode = statusList[i];
                    int.TryParse(innerNode.InnerXml.Trim(), out iStatus);
                    confStatus.Add(iStatus);
                }

                ConfTypeList = xd.GetElementsByTagName("ConfType");

                for (int j = 0; j < ConfTypeList.Count; j++)
                {
                    innerNode = ConfTypeList[j];
                    int.TryParse(innerNode.InnerXml.Trim(), out iConfType);
                    confType.Add(iConfType);
                }

                searchOutXml.Append("<SearchConference>");
                searchOutXml.Append("<Conferences>");
                for (int o = 0; o < orgDts.Count; o++)
                {
                    criterionList = new List<ICriterion>();

                    if (confStatus.Count > 0)
                    {
                        if (confStatus.Count == 1)
                            criterionList.Add(Expression.Eq("status", confStatus[0]));
                        else
                            criterionList.Add(Expression.In("status", confStatus));
                    }

                    if (confType.Count > 0)
                    {
                        if (confType.Count == 1)
                        {
                            criterionList.Add(Expression.Eq("conftype", confType[0]));
                            if (confType[0] == 4)
                                isP2PServiceCall = true;
                        }
                        else
                            criterionList.Add(Expression.In("conftype", confType));
                    }

                    if (orgDts[o].SecureLaunchBuffer > 0)
                        secs = orgDts[o].SecureLaunchBuffer;

                    if (isP2PServiceCall && sysSettings.EnableLaunchBufferP2P == 0)
                    {
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                        criterium = Expression.Le("confdate", confEnd);
                        criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                    }
                    else if (getSearchDateRange(vrmSearchType.Ongoing, ref confEnd, ref confFrom, secs))
                    {
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                        criterium = Expression.Le("confdate", confEnd);
                        criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                    }

                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("isVMR", 0));
                    criterionList.Add(Expression.Eq("NetworkSwitch", 0));
                    criterionList.Add(Expression.Eq("orgId", orgDts[o].OrgId));

                    if (orgDts[o].NetworkSwitching == 2) //Secured
                        criterionList.Add(Expression.Eq("Secured", 1));
                    else if (orgDts[o].NetworkSwitching == 3) //Unsecured
                        criterionList.Add(Expression.Eq("Secured", 0));

                    confList = m_IconfDAO.GetByCriteria(criterionList);

                    for (int confcnt = 0; confcnt < confList.Count; confcnt++)
                    {
                        conf = confList[confcnt];
                        searchOutXml.Append("<Conference>");
                        searchOutXml.Append("<ConferenceID>" + conf.confid.ToString() + "," + conf.instanceid.ToString() + "</ConferenceID>");
                        searchOutXml.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                        searchOutXml.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                        searchOutXml.Append("<CallNetworkState>" + conf.Secured.ToString() + "</CallNetworkState>");
                        searchOutXml.Append("<CallRequestTimeout>" + orgDts[o].ResponseTimeout + "</CallRequestTimeout>"); //FB 2993
                        searchOutXml.Append("</Conference>");
                    }
                }
                searchOutXml.Append("</Conferences>");
                searchOutXml.Append("</SearchConference>");
                obj.outXml = searchOutXml.ToString();
            }
            catch (Exception e)
            {
                obj.outXml = "";
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2595 Ends
		//FB 2616 Starts

        #region GetCallsforFMS
        /// <summary>
        ///This command is going to be used in FRMS to get ongoing congferences
        /// It has a Status Filter of Ongoing and hour after conferene time it returned in GMT
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool GetCallsforFMS(ref vrmDataObject obj)
        {
            StringBuilder searchOutXml = new StringBuilder();
            List<vrmConference> confList = null;
            List<ICriterion> criterionList = null;
            ICriterion criterium = null;
            XmlNodeList nodeList = null;
            List<int> confParamList = null;
            int confParam = 0;
            DateTime confFrom = DateTime.Now;
            DateTime confEnd = DateTime.Now;
            vrmConference conf = null;
            vrmConfRoom rm = null;
            vrmConfUser ur = null;
            XmlDocument xd = null;
            string UserEmail = "", Type = "", Confmode = "", MCUname = "";
            vrmUser user = null;
            vrmRoom room = null;
            vrmOrganization org = null;
            List<vrmConfBridge> vConfbridgelist = null;
            List<int> Statuslist = null;
            int sec = 3600, status = 0;
            XmlNode innerNode = null;
            vrmConfVNOCOperator ConfVNoc = null;//FB 2670
            string VNOCName = "", VNOCEmail = ""; //FB 2670
            try
            {
                m_obj = obj;
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                if (xd.SelectSingleNode("//SearchConference/UserEmail") != null)
                    UserEmail = xd.SelectSingleNode("//SearchConference/UserEmail").InnerXml.Trim();

                user = m_IuserDAO.GetByUserEmail(UserEmail);

                nodeList = xd.GetElementsByTagName("ConferenceStatus");
                confParamList = new List<int>();
                for (int i = 0; i < nodeList.Count; i++)
                {
                    innerNode = nodeList[i];
                    confParam = 0;
                    confParam = Int32.Parse(innerNode.InnerXml.Trim());
                    confParamList.Add(confParam);
                }

                criterionList = new List<ICriterion>();
                if (confParamList.Count > 0)
                {
                    if (confParamList.Count == 1)
                        criterionList.Add(Expression.Eq("status", confParamList[0]));
                    else
                        criterionList.Add(Expression.In("status", confParamList));
                }


                nodeList = xd.GetElementsByTagName("ConfType");

                confParamList = new List<int>();
                for (int j = 0; j < nodeList.Count; j++)
                {
                    innerNode = nodeList[j];
                    confParam = 0;
                    confParam = Int32.Parse(innerNode.InnerXml.Trim());
                    confParamList.Add(confParam);
                }


                if (confParamList.Count > 0)
                {
                    if (confParamList.Count == 1)
                        criterionList.Add(Expression.Eq("conftype", confParamList[0]));
                    else
                        criterionList.Add(Expression.In("conftype", confParamList));
                }

                if (getSearchDateRange(vrmSearchType.Ongoing, ref confEnd, ref confFrom, sec))
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                    criterium = Expression.Le("confdate", confEnd);
                    criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                }

                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("isVMR", 0));

                
                confList = m_IconfDAO.GetByCriteria(criterionList);

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;

                using (xWriter = XmlWriter.Create(searchOutXml, xSettings))
                {
                    xWriter.WriteStartElement("SearchConference");
                    xWriter.WriteStartElement("Conferences");
                    for (int confcnt = 0; confcnt < confList.Count; confcnt++)
                    {
                        Statuslist = new List<int>();
                        conf = confList[confcnt];
                        xWriter.WriteStartElement("Conference");
                        xWriter.WriteElementString("confid", conf.confid.ToString());
                        xWriter.WriteElementString("instanceid", conf.instanceid.ToString());
                        xWriter.WriteElementString("ActualStatus", conf.status.ToString());
                        org = m_IOrgDAO.GetById(conf.orgId);

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("ConfID", conf.confid));
                        criterionList.Add(Expression.Eq("InstanceID", conf.instanceid));
                        criterionList.Add(Expression.Eq("confuId", conf.confnumname));
                        vConfbridgelist = m_IconfBridge.GetByCriteria(criterionList);

                        for (int b = 0; b < vConfbridgelist.Count; b++)
                        {
                            if (b == 0)
                                MCUname = vConfbridgelist[b].BridgeName;
                            else
                                MCUname += " ," + vConfbridgelist[b].BridgeName;

                        }
                        xWriter.WriteElementString("confMCU", MCUname);
                        xWriter.WriteElementString("sitesiloname", org.orgname);
                        xWriter.WriteElementString("confnumname", conf.confnumname.ToString());
                        xWriter.WriteElementString("title", conf.externalname);

                        if (conf.owner > 0)
                            xWriter.WriteElementString("confHostname", conf.HostName);
                        else
                            xWriter.WriteElementString("confHostname", "");

                        if (conf.conftype == 2)
                            Type = "Video";
                        else
                            Type = "P2P";

                        xWriter.WriteElementString("Conftype", Type);
                        xWriter.WriteElementString("confdate", conf.confdate.ToString());
                        xWriter.WriteElementString("duration", conf.duration.ToString());

                        if (conf.StartMode == 0)
                            Confmode = "Automatic";
                        else
                            Confmode = "Manual";

                        xWriter.WriteElementString("startmode", Confmode);
                        xWriter.WriteElementString("settingtime", conf.settingtime.ToString());
                        xWriter.WriteElementString("lastmodified", conf.LastRunDateTime.ToString());

                        if (conf.ConfVNOCOperator.Count > 0)
                        {
                            VNOCName = ""; VNOCEmail = "";
                            for (int i = 0; i < conf.ConfVNOCOperator.Count; i++)
                            {
                                ConfVNoc = conf.ConfVNOCOperator[i];
                                user = m_IuserDAO.GetByUserId(ConfVNoc.vnocId);
                                if (user != null)
                                {
                                    if (VNOCName == "")
                                    {
                                        VNOCName = user.FirstName + " " + user.LastName;
                                        VNOCEmail = user.Email;
                                    }
                                    else
                                    {
                                        VNOCName += ", " + user.FirstName + " " + user.LastName;
                                        VNOCEmail += ", " + user.Email;
                                    }
                                }
                            }

                            xWriter.WriteElementString("VNOCOperator", VNOCName);
                            xWriter.WriteElementString("VNOCOperatorEmail", VNOCEmail);

                        }
                        else
                        {
                            xWriter.WriteElementString("VNOCOperator", "");
                            xWriter.WriteElementString("VNOCOperatorEmail", "");
                        }

                        if (conf.DedicatedVNOCOperator.ToString() == "1")
                            xWriter.WriteElementString("DedicatedVNOCOperator", "Yes");
                        else
                            xWriter.WriteElementString("DedicatedVNOCOperator", "No");

                        if (conf.MeetandGreet.ToString() == "1")
                            xWriter.WriteElementString("MeetandGreet", "Yes");
                        else
                            xWriter.WriteElementString("MeetandGreet", "No");

                        if (conf.ConciergeMonitoring.ToString() == "1")
                            xWriter.WriteElementString("ConciergeMonitoring", "Yes");
                        else
                            xWriter.WriteElementString("ConciergeMonitoring", "No");

                        for (int r = 0; r < conf.ConfRoom.Count; r++)
                        {
                            rm = conf.ConfRoom[r];
                            xWriter.WriteStartElement("ConferenceEndpoints");
                            room = m_IRoomDAO.GetByRoomId(rm.roomId);
                            xWriter.WriteElementString("confuId", conf.confnumname.ToString());
                            xWriter.WriteElementString("PartyId", room.roomId.ToString());
                            xWriter.WriteElementString("Name", rm.PartyName);
                            xWriter.WriteElementString("Toptier", room.tier2.tier3.Name);
                            xWriter.WriteElementString("Middletier", room.tier2.Name);
                            xWriter.WriteElementString("Address", rm.ipisdnaddress.Trim());
                            xWriter.WriteElementString("Connectstatus", rm.OnlineStatus.ToString());
                            Statuslist.Add(rm.OnlineStatus);
                            xWriter.WriteElementString("Stream", rm.Stream);
                            xWriter.WriteElementString("CallerType", rm.connect2.ToString());
                            xWriter.WriteElementString("TerminalType", "2"); // 1 = user , 2 = room, 3 = guest , 4 = cascade	

                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("endpointid", rm.endpointId));
                            criterionList.Add(Expression.Eq("profileId", rm.profileId));
                            criterionList.Add(Expression.Eq("deleted", 0));

                            List<vrmEndPoint> epList = m_vrmEpt.GetByCriteria(criterionList);

                            if (epList.Count > 0)
                            {
                                for (int e = 0; e < epList.Count; e++)
                                {
                                    xWriter.WriteElementString("EM7OnlineStatus", epList[e].EptOnlineStatus.ToString());
                                    xWriter.WriteElementString("EM7CurrentStatus", epList[e].EptCurrentStatus.ToString());
                                    xWriter.WriteElementString("EM7OverallHealth", epList[e].EptCurrentStatus.ToString());
                                }
                            }
                            else
                            {
                                xWriter.WriteElementString("EM7OnlineStatus","1");
                                xWriter.WriteElementString("EM7CurrentStatus", "0");
                                xWriter.WriteElementString("EM7OverallHealth", "0");
                            }
                            xWriter.WriteEndElement();
                        }
                        for (int u = 0; u < conf.ConfUser.Count; u++)
                        {
                            ur = conf.ConfUser[u];
                            if (ur.invitee == 1)
                            {
                                xWriter.WriteStartElement("ConferenceEndpoints");
                                xWriter.WriteElementString("confuId", conf.confnumname.ToString());
                                xWriter.WriteElementString("PartyId", ur.userid.ToString());
                                xWriter.WriteElementString("Name", ur.PartyName);
                                xWriter.WriteElementString("Toptier", "");
                                xWriter.WriteElementString("Middletier", "");
                                xWriter.WriteElementString("Address", ur.ipisdnaddress.Trim());
                                xWriter.WriteElementString("Connectstatus", ur.OnlineStatus.ToString());
                                Statuslist.Add(ur.OnlineStatus);
                                xWriter.WriteElementString("Stream", ur.Stream);
                                xWriter.WriteElementString("CallerType", ur.connect2.ToString());
                                xWriter.WriteElementString("TerminalType", ur.TerminalType.ToString());
                                xWriter.WriteElementString("EM7OnlineStatus", "1");
                                xWriter.WriteElementString("EM7CurrentStatus", "0");
                                xWriter.WriteElementString("EM7OverallHealth", "0");
                                xWriter.WriteEndElement();
                            }
                        }
                        if (Statuslist.Count > 0)
                        {
                            status = Statuslist.Min();
                            if (status == -1)
                                status = 0;
                        }
                        else
                            status = 0;
                        xWriter.WriteElementString("CallStatus", status.ToString());
                        xWriter.WriteEndElement();
                    }
                    xWriter.WriteEndElement();
                    xWriter.WriteEndElement();
                    xWriter.Flush();
                    obj.outXml = searchOutXml.ToString();
                }
            }
            catch (Exception e)
            {
                obj.outXml = e.Message;
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region Change Room Name
        /// <summary>
        ///This command is going to be used in FRMS to get ongoing congferences
        /// It has a Status Filter of Ongoing and hour after conferene time it returned in GMT
        /// </summary>
        /// <param name="obj" type="vrmDataObject"></param>
        /// <returns></returns>
        public bool ChangePartyName(string PartyName,int roomid,int userid)
        {
            List<vrmConfRoom> ConfRoomList = null;
            vrmConfRoom confroom = null;
            vrmConfUser ConfUser = null;
            List<vrmConfUser> ConfUserList = null;
            List<ICriterion> criterionList = null;
            try
            {
                criterionList = new List<ICriterion>();
                if (roomid > 0)
                {
                    criterionList.Add(Expression.Eq("roomId", roomid));
                    ConfRoomList = m_IconfRoom.GetByCriteria(criterionList);
                    for (int i = 0; i < ConfRoomList.Count; i++)
                    {
                        confroom = ConfRoomList[i];
                        confroom.PartyName = PartyName;
                    }
                    m_IconfRoom.SaveOrUpdateList(ConfRoomList);
                }
                if (userid > 0)
                {
                    criterionList.Add(Expression.Eq("userid", userid));
                    ConfUserList = m_IconfUser.GetByCriteria(criterionList);
                    for (int i = 0; i < ConfUserList.Count; i++)
                    {
                        ConfUser = ConfUserList[i];
                        ConfUser.PartyName = PartyName;
                    }
                    m_IconfUser.SaveOrUpdateList(ConfUserList);
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion


        //FB 2616 Ends


    }
}

