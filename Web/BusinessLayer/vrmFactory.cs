//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Diagnostics;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

using NHibernate;
using NHibernate.Criterion;

using log4net;

using myVRM.DataLayer;

namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Data Layer Logic for loading/saving General data objects (similar to vrmContain)
    /// </summary>
    /// 
    public class vrmFactory
    {
        /// <summary>
        /// construct vrm factory 
        /// </summary>

        private static log4net.ILog m_log;
        private string m_configPath;

        private IUserDao m_IuserDAO;
        private IDeptDao m_IdeptDAO;
        private IDeptApproverDao m_IApproverDeptDAO;
        private IUserDeptDao m_IuserDeptDAO;
        private userDAO m_userDAO;
        private deptDAO m_deptDAO;

        private ICountryDAO m_ICountryDAO;
        //FB 2671
        private IPublicCountryDAO m_IPublicCountryDAO;
        private IPublicCountryStateDAO m_IPublicCountryStateDAO;
        private IPublicCountryStateCityDAO m_IPublicCountryStateCityDAO;
        //FB 2671
        private IStateDAO m_IStateDAO;
        private GeneralDAO m_generalDAO;

        private IDeptCustomAttrOptionDao m_IDeptCustOptDAO; //Custom Attribute Fixes
        private IDeptCustomAttrDao m_IDeptCustDAO;  //Custom Attribute Fixes
        private IConfAttrDAO m_IconfAttrDAO;  //Custom Attribute Fix
        private conferenceDAO m_confDAO;  //Custom Attribute Fix
        private IConferenceDAO m_vrmConfDAO;    //Custom Attribute Fix
        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private OrgData orgInfo;
        private imageDAO m_imageDAO; //Image Project
        private IImageDAO m_IImageDAO;  //Image Project
        //FB 1830 start
        private IEmailTypeDao m_IEmailTypeDAO;
        private IEmailContentDao m_IEmailContentDAO;
        private IEmailLanguageDao m_IEmailLanguageDAO;
        private IEPlaceHoldersDao m_IEPlaceHoldersDAO;
        //FB 1830 end
        private IEmailDao m_IemailDao; //FB 1860
        //FB 1970 start
        private ILanguageDAO m_ILanguageDAO;
        private ICustomLangDao m_ICustomAttrLangDAO;
        //FB 1970 end
        private const int defaultOrgId = 11;
        internal int organizationID; //FB 2045
        private int multiDepts = 1;
        private myVRMException myvrmEx; //FB 1881
        List<vrmDeptCustomAttr> custAttrs = null; //FB 2027
        private IIconsRefDAO m_IIconsRefDAO;//NewLobby
        private IUserLobbyDAO m_IUserLobbyDao; //NewLobby
        private imageFactory m_imgfactory; //FB 2136       
        
        public vrmFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                m_userDAO = new userDAO(m_configPath, m_log);
                m_deptDAO = new deptDAO(m_configPath, m_log);
                m_generalDAO = new GeneralDAO(m_configPath, m_log);
                m_confDAO = new conferenceDAO(m_configPath, m_log); //Custom Attribute Fixes
                m_imageDAO = new imageDAO(m_configPath, m_log);//Image Project

                m_IuserDAO = m_userDAO.GetUserDao();
                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao();
                m_ICountryDAO = m_generalDAO.GetCountryDAO();
                //FB 2671
                m_IPublicCountryDAO = m_generalDAO.GetPublicCountryDAO();
                m_IPublicCountryStateDAO = m_generalDAO.GetPublicCountryStateDAO();
                m_IPublicCountryStateCityDAO = m_generalDAO.GetPublicCountryStateCityDAO();
                //FB 2671
                m_IStateDAO = m_generalDAO.GetStateDAO();
                m_IdeptDAO = m_deptDAO.GetDeptDao();

                m_IDeptCustOptDAO = m_deptDAO.GetDeptCustomAttrOptionDao(); //Custom Attribute Fixes
                m_IDeptCustDAO = m_deptDAO.GetDeptCustomAttrDao();  //Custom Attribute Fixes
                m_IconfAttrDAO = m_confDAO.GetConfAttrDao(); //Custom Attribute Fix
                m_vrmConfDAO = m_confDAO.GetConferenceDao(); //Custom Attribute Fix

                m_OrgDAO = new orgDAO(m_configPath, m_log); //Organization Module Fixes
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();
                m_IImageDAO = m_imageDAO.GetImageDao(); //Image Project
                //FB 1830 start
                m_IEmailTypeDAO = m_confDAO.GetEmailTypeDao();
                m_IEmailContentDAO = m_confDAO.GetEmailContentDao();
                m_IEmailLanguageDAO = m_confDAO.GetEmailLanguageDao();
                m_IEPlaceHoldersDAO = m_confDAO.GetEPlaceHoldersDao();
                //FB 1830 end
                m_IemailDao = m_userDAO.GetUserEmailDao(); //FB 1860
                //FB 1970 start
                m_ILanguageDAO = m_generalDAO.GetLanguageDAO();
                m_ICustomAttrLangDAO = m_deptDAO.GetCustomLangDao();
                //FB 1970 end
                m_IIconsRefDAO = m_generalDAO.GetIconRefDAO(); //NewLobby
                m_IUserLobbyDao = m_userDAO.GetUserLobbyIconsDAO(); //NewLobby
                m_imgfactory = new imageFactory(ref obj); //FB 2136
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #region GetCountryCodes
        public bool GetCountryCodes(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetCountryCodes/UserID");
                string userID = node.InnerXml.Trim();

                obj.outXml = "<GetCountryCodes>";
                m_ICountryDAO.addOrderBy(Order.Desc("Selected"));
                m_ICountryDAO.addOrderBy(Order.Asc("CountryName"));//Changed for FB 1712
                List<vrmCountry> countries = m_ICountryDAO.GetActive();
                m_ICountryDAO.clearOrderBy();
                if (countries.Count < 1)
                    throw new Exception("No data available in the Gen_Country_S table.");
                foreach (vrmCountry country in countries)
                {
                    obj.outXml += "<Country>";
                    obj.outXml += "<ID>" + country.CountryID.ToString() + "</ID>";
                    obj.outXml += "<Name>" + country.CountryName + "</Name>";
                    obj.outXml += "</Country>";
                }
                obj.outXml += "</GetCountryCodes>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        //FB 2671 Starts
        #region GetPublicCountries
        public bool GetPublicCountries(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                StringBuilder sbBuilder = new StringBuilder();

                obj.outXml = string.Empty;
                sbBuilder.Append("<GetPublicCountries>");

                List<vrmPublicCountry> countries = m_IPublicCountryDAO.GetCountries();

                foreach (vrmPublicCountry country in countries)
                {
                    sbBuilder.Append("<Country>");
                    sbBuilder.Append("<ID>" + country.CountryID + "</ID>");
                    if (country.CountryName.StartsWith("trinidad", StringComparison.InvariantCultureIgnoreCase) &&
                            country.CountryName.EndsWith("tobago", StringComparison.InvariantCultureIgnoreCase))
                        sbBuilder.Append("<Name>Trinidad and Tobago</Name>");
                    else
                        sbBuilder.Append("<Name>" + country.CountryName + "</Name>");
                    sbBuilder.Append("</Country>");
                }
                sbBuilder.Append("</GetPublicCountries>");
                obj.outXml = sbBuilder.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetPublicCountryStates
        public bool GetPublicCountryStates(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//GetPublicCountryStates/CountryId");
                string countryId = node.InnerXml.Trim();

                StringBuilder sbBuilder = new StringBuilder();
                obj.outXml = string.Empty;
                sbBuilder.Append("<GetPublicCountryStates>");
                if (!string.IsNullOrEmpty(countryId))
                {
                    int id = 0;
                    int.TryParse(countryId, out id);
                    List<vrmPublicCountryState> states = m_IPublicCountryStateDAO.GetCountryStates(id);

                    foreach (vrmPublicCountryState state in states)
                    {
                        sbBuilder.Append("<State>");
                        sbBuilder.Append("<ID>" + state.StateID + "</ID>");
                        sbBuilder.Append("<Name>" + state.StateName + "</Name>");
                        sbBuilder.Append("</State>");
                    }
                }
                sbBuilder.Append("</GetPublicCountryStates>");
                obj.outXml = sbBuilder.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }

            return bRet;

        }
        #endregion
        
        #region GetPublicCountryStateCities
        public bool GetPublicCountryStateCities(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                StringBuilder sbBuilder = new StringBuilder();
                obj.outXml = string.Empty;

                sbBuilder.Append("<GetPublicCountryStateCities>");
                node = xd.SelectSingleNode("//GetPublicCountryStateCities/CountryId");
                string countryId = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetPublicCountryStateCities/StateId");
                string stateId = node.InnerXml.Trim();
                if (!string.IsNullOrEmpty(countryId) && !string.IsNullOrEmpty(stateId))
                {
                    int cid = 0;
                    int.TryParse(countryId, out cid);
                    int sid = 0;
                    int.TryParse(stateId, out sid);

                    List<vrmPublicCountryStateCity> cities = m_IPublicCountryStateCityDAO.GetCountryStateCities(cid, sid);

                    foreach (vrmPublicCountryStateCity city in cities)
                    {
                        sbBuilder.Append("<City>");
                        sbBuilder.Append("<ID>" + city.CityID.ToString() + "</ID>");
                        sbBuilder.Append("<Name>" + city.CityName + "</Name>");
                        sbBuilder.Append("</City>");
                    }
                }
                sbBuilder.Append("</GetPublicCountryStateCities>");
                obj.outXml += sbBuilder.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }

            return bRet;
        }
        #endregion
        //FB 2671 Ends

        #region GetCountryStates
        public bool GetCountryStates(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetCountryStates/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetCountryStates/CountryCode");
                string countryId = node.InnerXml.Trim();

                //FB 2671 Starts
                StringBuilder sbBuilder = new StringBuilder();
                obj.outXml = string.Empty;

                sbBuilder.Append("<GetCountryStates>"); 
                vrmCountry country = m_ICountryDAO.GetCountryById(Int32.Parse(countryId));
                if (country == null)
                    throw new Exception("No matching record available in the Gen_Country_S table for the country id " + countryId);
                foreach (vrmState state in country.States)
                {
                    sbBuilder.Append("<State>");
                    sbBuilder.Append("<ID>" + state.Id.ToString() + "</ID>");
                    sbBuilder.Append("<Code>" + state.StateCode + "</Code>");
                    sbBuilder.Append("<Name>" + state.State + "</Name>"); //Report Fixes - changed to match with LoadList common method in Netfunction.cs
                    sbBuilder.Append("</State>");
                }
                sbBuilder.Append("</GetCountryStates>");
                obj.outXml = sbBuilder.ToString();
                //FB 2671 Ends

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = string.Empty; 
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = string.Empty; 
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetTimezones
        public bool GetTimezones(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetTimezones/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTimezones/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//GetTimezones/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                Int32.TryParse(orgid, out organizationID); //Organization Module Fixes

                Hashtable timeZones = new Hashtable();

                vrmUser user = m_IuserDAO.GetByUserId(Int32.Parse(userID));

                obj.outXml += "<Timezones>";
                if (user != null)//Code added for Error 200
                    obj.outXml += "<selected>" + user.TimeZone.ToString() + "</selected>";

                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);    //Organization Module
                m_OrgFactory.organizationID = organizationID;

                StringBuilder outputXML = new StringBuilder();
                m_OrgFactory.timeZonesToXML(ref outputXML);
                obj.outXml += outputXML;
                obj.outXml += "</Timezones>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        /*** Code added for organization ***/
        #region GetAllTimezones
        public bool GetAllTimezones(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

               
                obj.outXml += "<Timezones>";

                List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                obj.outXml += "<timezones>";
                foreach (timeZoneData time in timeZoneList)
                {
                        obj.outXml += "<timezone>";
                        obj.outXml += "<timezoneID>" + time.TimeZoneID.ToString() + "</timezoneID>";
                        obj.outXml += "<timezoneName>" + time.TimeZoneDiff.ToString() + " ";
                        obj.outXml += time.TimeZone + "</timezoneName>";
                        obj.outXml += "</timezone>";
                }
                obj.outXml += "</timezones>";


                obj.outXml += "</Timezones>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion
        /**** Code added for organization ***/

        #region GetDialingOptions
        public bool GetDialingOptions(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetDialingOption/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmDialingOption> p_List = vrmGen.getDialingOption();
                obj.outXml += "<GetDialingOption>";
                foreach (vrmDialingOption p in p_List)
                {
                    obj.outXml += "<DialingOption>";
                    obj.outXml += "<ID>" + p.ID.ToString() + "</ID>";
                    obj.outXml += "<Name>" + p.DialingOption + "</Name> ";
                    obj.outXml += "</DialingOption>";
                }
                obj.outXml += "</GetDialingOption>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region Feedback
        public bool Feedback(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                vrmEmailFeedback feedback = new vrmEmailFeedback();
                node = xd.SelectSingleNode("//login/userID");
                feedback.userID = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/name");
                feedback.name = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/email");
                feedback.email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/subject");
                feedback.subject = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/comment");
                feedback.comment = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/parentPage");
                feedback.parentPage = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/browserAgent");
                feedback.browserAgent = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/browserLanguage");
                feedback.browserLanguage = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/IPAddress");
                feedback.ipAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/hostAddress");
                feedback.hostAddress = node.InnerXml.Trim();

                emailFactory m_Email = new emailFactory(ref obj);
                string outxml = string.Empty;
                if (m_Email.FeedbackEmail(feedback, ref outxml))
                {
                    return true;
                }
                else
                {
                    obj.outXml = outxml;
                    return false;
                }

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetManageDepartment
        public bool GetManageDepartment(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;   //Organization Module Fixes

                vrmUser user = m_IuserDAO.GetByUserId(Int32.Parse(userId));

                obj.outXml = "<getManageDepartment>";
                List<ICriterion> deptCriterionList = new List<ICriterion>();
                deptCriterionList.Add(Expression.Eq("deleted", 0));   //FB 1445
                deptCriterionList.Add(Expression.Eq("orgId", organizationID.ToString())); //Organization Module Fixes
                //
                // if multi departments is enabled apply roles based security. 
                // if not then anyone can see all depts 
                //
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
                if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN && user.Admin != vrmUserConstant.VNOCADMIN) //FB 2670  //Organization Module Fixes
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", user.userid));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        deptIn.Add(dept.departmentId);
                    }
                    deptCriterionList.Add(Expression.In("departmentId", deptIn));
                }
                List<vrmDept> selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);
                
                obj.outXml += "<multiDepartment>" + multiDepts.ToString() + "</multiDepartment>"; //Organization Module Fixes
                obj.outXml += "<departments>";

                foreach (vrmDept dept in selectedDept)
                {
                    obj.outXml += "<department>";
                    obj.outXml += "<id>" + dept.departmentId.ToString() + "</id>";
                    obj.outXml += "<name>" + dept.departmentName + "</name>";
                    obj.outXml += "<securityKey>" + dept.securityKey + "</securityKey>";
                    obj.outXml += "</department>";
                }
                obj.outXml += "</departments>";

                obj.outXml += "</getManageDepartment>";

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
	#endregion
        //FB 2047
        public bool GetAllDepartments(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userId = node.InnerXml.Trim();

                vrmUser user = m_IuserDAO.GetByUserId(Int32.Parse(userId));

                obj.outXml = "<GetAllDepartments>";
                List<ICriterion> deptCriterionList = new List<ICriterion>();
                deptCriterionList.Add(Expression.Eq("deleted", 0));                

                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                m_IdeptDAO.addOrderBy(Order.Asc("departmentId"));
                List<vrmDept> selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);
                m_IdeptDAO.clearOrderBy();

                obj.outXml += "<departments>";

                foreach (vrmDept dept in selectedDept)
                {
                    obj.outXml += "<department>";
                    obj.outXml += "<id>" + dept.departmentId.ToString() + "</id>";
                    obj.outXml += "<name>" + dept.departmentName + "</name>";
                    obj.outXml += "<orgId>" + dept.orgId.ToString() + "</orgId>";
                    obj.outXml += "</department>";
                }
                obj.outXml += "</departments>";
                obj.outXml += "</GetAllDepartments>";

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; 
                return false;
            }
        }

        #region UpdateManageDepartment
        public bool UpdateManageDepartment(ref vrmDataObject obj)
        {
            try
            {
                m_IApproverDeptDAO = m_deptDAO.GetDeptApproverDao();
                m_IdeptDAO = m_deptDAO.GetDeptDao();

                List<vrmDept> deptList = new List<vrmDept>();
                List<vrmDept> delDeptList = new List<vrmDept>();
                List<vrmDeptApprover> deptApprover = new List<vrmDeptApprover>();

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                int userID = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                XmlNodeList NodeList = xd.SelectNodes(@"/login/edit");
                foreach (XmlNode Node in NodeList)
                {
                    vrmDept dept = new vrmDept();
                    if (Node.SelectSingleNode("departmentID") != null)
                    {
                        string sId = Node.SelectSingleNode("departmentID").InnerText;
                        if (sId.ToLower() == "new")
                            dept.departmentId = 0;
                        else
                            dept.departmentId = Int32.Parse(sId);

                        dept.orgId = organizationID.ToString(); //organization module
                        dept.departmentName = Node.SelectSingleNode("departmentName").InnerText;
                        dept.deleted = 0;
                        if (dept.departmentId == 0 || dept.departmentId != Int32.MinValue)//Edited for FB 1474 Issues 
                        {
                            // New department
                            // First make sure that the name doesn't exist
                            List<ICriterion> deptCriterionList = new List<ICriterion>();

                            ICriterion dc = Expression.Eq("departmentName", dept.departmentName).IgnoreCase();
                            deptCriterionList.Add(Expression.Eq("orgId", dept.orgId));   //FB-1660
                            deptCriterionList.Add(dc);

                            //Added for FB 1474 Issues -- START
                            dc = Expression.Not(Expression.Eq("departmentId", dept.departmentId));
                            deptCriterionList.Add(dc);
                            List<vrmDept> selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);
                            //Added for FB 1474 Issues -- END

                            if (selectedDept.Count > 0)
                            {
                                // There already exists a dept with the name specified
                                myVRMException e = new myVRMException(412);
                                //obj.outXml = myVRMException.toXml(e.Message + " Department name already exists");//FB 1881
                                obj.outXml = e.FetchErrorMsg();//FB 1881
                                m_log.Error("vrmException", e);
                                return false;
                            }

                        }
                        m_IdeptDAO.SaveOrUpdate(dept);
                        XmlNodeList childNodeList = Node.SelectNodes("approvers/approver");
                        foreach (XmlNode childNode in childNodeList)
                        {
                            vrmDeptApprover aDept = new vrmDeptApprover();
                            string appId = childNode.SelectSingleNode("ID").InnerText;
                            if (appId.Length > 0)
                            {
                                aDept.approverid = Int32.Parse(appId);
                                aDept.departmentid = dept.departmentId;
                                m_IApproverDeptDAO.SaveOrUpdate(aDept);
                            }
                        }
                    }
                }

                NodeList = xd.SelectNodes(@"/login/delete");
                foreach (XmlNode Node in NodeList)
                {
                    bool err = false;
                    vrmDept dept = new vrmDept();
                    if (Node.SelectSingleNode("departmentID") != null)
                    {
                        string sId = Node.SelectSingleNode("departmentID").InnerText;
                        dept.departmentId = Int32.Parse(sId);
                        //
                        // check if dept is in use. If so cannot delete
                        //
                        List<ICriterion> checkCriterionList = new List<ICriterion>();
                        checkCriterionList.Add(Expression.Eq("departmentId", dept.departmentId));

                        ILocDeptDAO a_IlocDeptDAO;
                        LocationDAO a_locDAO = new LocationDAO(m_configPath, m_log);

                        a_IlocDeptDAO = a_locDAO.GetLocDeptDAO(); ;

                        List<vrmLocDepartment> locDeptList = a_IlocDeptDAO.GetByCriteria(checkCriterionList);

                        if (locDeptList.Count > 0)
                        {
                            err = true;
                        }
                        else
                        {
                            IConferenceDAO a_IconfDAO;
                            conferenceDAO a_confDAO = new conferenceDAO(m_configPath, m_log);
                            a_IconfDAO = a_confDAO.GetConferenceDao();

                            List<ICriterion> confCriterionList = new List<ICriterion>();
                            confCriterionList.Add(Expression.Eq("ConfDeptID", dept.departmentId));
                            List<vrmConference> conf = a_IconfDAO.GetByCriteria(confCriterionList);

                            if (conf.Count > 0)
                            {
                                err = true;
                            }
                            else
                            {
                                IDeptCustomAttrOptionDao customAttr = m_deptDAO.GetDeptCustomAttrOptionDao();
                                List<ICriterion> customCriterionList = new List<ICriterion>();
                                customCriterionList.Add(Expression.Eq("DeptID", dept.departmentId));
                                List<vrmDeptCustomAttrOption> option = customAttr.GetByCriteria(customCriterionList);
                                if (option.Count > 0)
                                    err = true;
                            }
                        }
                        if (err)
                        {
                            myVRMException e = new myVRMException(406);
                            m_log.Error("vrmException", e);
                            //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                            obj.outXml = e.FetchErrorMsg();//FB 1881
                            return false;
                        }
                        else
                        {
                            dept.deleted = 1;
                            m_IdeptDAO.Update(dept);
                        }
                    }
                }
                //
                // Check and set the MultipleDepartments flag in Sysref table
                //
                List<ICriterion> deptCrit = new List<ICriterion>();
                deptCrit.Add(Expression.Eq("deleted", 0));    //FB 1445
                List<vrmDept> checkDept = m_IdeptDAO.GetByCriteria(deptCrit);
                if (deptCrit.Count > 0)
                {
                    if (organizationID < 11)    //Organization Module
                        organizationID = defaultOrgId;

                    if (orgInfo == null)
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (orgInfo != null)
                    {
                        orgInfo.MultipleDepartments = 1;
                        m_IOrgSettingsDAO.Update(orgInfo);  //Organization Module
                    }
                }
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion
        /* *** Custom Attribute Fixes - start *** */

        #region Check whether custom attribute is linked to any conferences
        /// <summary>
        /// Check whether custom attribute is linked to any conferences
        /// </summary>
        /// <returns></returns>
        private bool ChecCustomAttrLinkToConf(Int32 customAttrID)
        {
            try
            {
                if (customAttrID <= 0)
                    return true;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                List<vrmConfAttribute> customAttlist = m_IconfAttrDAO.GetByCriteria(criterionList);
                criterionList = null;

                if (customAttlist != null)
                {
                    if (customAttlist.Count > 0)
                    {
                        List<int> confsList = new List<int>();
                        foreach (vrmConfAttribute confAttr in customAttlist)
                        {
                            confsList.Add(confAttr.ConfId);
                        }

                        if (confsList.Count > 0)
                        {
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.In("confid", confsList));
                            criterionList.Add(Expression.Eq("deleted", 0));
                            List<vrmConference> conf_list = new List<vrmConference>();
                            conf_list = m_vrmConfDAO.GetByCriteria(criterionList);

                            if (conf_list != null)
                            {
                                if (conf_list.Count > 0)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Check whether custom attribute is linked to any conferences
        /// <summary>
        /// Check whether custom attribute is linked to any conferences
        /// </summary>
        /// <returns></returns>
        // InXML: <CustomAttribute><ID></ID></CustomAttribute>
        public bool IsCustomAttrLinkedToConf(ref vrmDataObject obj)
        {
            int customAttrID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//CustomAttribute/ID");
                string customAttributeID = node.InnerXml.Trim();

                if (customAttributeID == "")
                {
                    obj.outXml = "<error>Invalid Custom Attribute ID</error>";
                    return false;
                }
                Int32.TryParse(customAttributeID, out customAttrID);
                if (customAttrID <= 0)
                {
                    obj.outXml = "<error>Invalid Custom Attribute ID</error>";
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                List<vrmConfAttribute> customAttlist = m_IconfAttrDAO.GetByCriteria(criterionList);
                criterionList = null;

                if (customAttlist != null)
                {
                    if (customAttlist.Count > 0)
                    {
                        List<int> confsList = new List<int>();
                        foreach (vrmConfAttribute confAttr in customAttlist)
                        {
                            confsList.Add(confAttr.ConfId);
                        }

                        if (confsList.Count > 0)
                        {
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.In("confid", confsList));
                            criterionList.Add(Expression.Eq("deleted", 0));
                            List<vrmConference> conf_list = new List<vrmConference>();
                            conf_list = m_vrmConfDAO.GetByCriteria(criterionList);

                            if (conf_list != null)
                            {
                                if (conf_list.Count > 0)
                                {
                                    //FB 1881 start
                                    //obj.outXml = "<error>The selected custom option is linked with conference. It cannot be edited/deleted.</error>";
                                    myvrmEx = new myVRMException(483);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    //FB 1881 end
                                    return false;
                                }
                            }
                        }
                    }
                }
                obj.outXml = "<Success>1</Success>";
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException", ex);
                //obj.outXml = myVRMException.toXml(ex.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            return true;
        }
        #endregion

        #region Fetches all the conferences linked to Custom Attribute
        /// <summary>
        /// Fetches all the conferences linked to Custom Attribute
        /// </summary>
        /// <returns></returns>
        // InXML: <CustomAttribute><ID></ID></CustomAttribute>
        public bool GetConfListByCustOptID(ref vrmDataObject obj)
        {
            int customAttrID=0;
            bool isFutureConf=false;
            string futureXml="";
            string pastXml="";
            int pastcnt = 0;
            int futurecnt = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//CustomAttribute/ID");
                string customAttributeID = node.InnerXml.Trim();

                if (customAttributeID == "")
                {
                    obj.outXml = "<error>Invalid Custom Attribute ID</error>";
                    return false;
                }
                Int32.TryParse(customAttributeID, out customAttrID);
                if (customAttrID <= 0)
                {
                    obj.outXml = "<error>Invalid Custom Attribute ID</error>";
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                List<vrmConfAttribute> customAttlist = m_IconfAttrDAO.GetByCriteria(criterionList);
                //FB 2349 - Start
                List<vrmDeptCustomAttr> customAttlist1 = m_IDeptCustDAO.GetByCriteria(criterionList);
                criterionList = null;
                //FB 2349 - End
                if (customAttlist != null)
                {
                    if (customAttlist.Count > 0)
                    {
                        if (customAttlist1[0].CreateType != 1)//FB 2349
                        {
                            //Code Commented for FB 1750 

                            //List<int> confsList = new List<int>();
                            //foreach (vrmConfAttribute confAttr in customAttlist)
                            //{
                            //    confsList.Add(confAttr.ConfId);
                            //}

                            //if (confsList.Count > 0)
                            //{
                            //criterionList = new List<ICriterion>();
                            //criterionList.Add(Expression.In("confid", confsList));
                            //criterionList.Add(Expression.Eq("deleted", 0));

                            List<vrmConference> conf_list = new List<vrmConference>();

                            String futStmt = "SELECT DISTINCT vc.confid, vc.instanceid, vc.confdate, vc.confnumname, vc.externalname " +
                             " FROM myVRM.DataLayer.vrmConfAttribute c, myVRM.DataLayer.vrmConference vc " +
                             " WHERE vc.deleted=0 and c.CustomAttributeId = " + customAttrID.ToString() +
                             " and c.ConfId = vc.confid and Datediff(minute,dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",confdate), getdate()) < 5";

                            String pastStmt = "SELECT DISTINCT vc.confid, vc.instanceid, vc.confdate, vc.confnumname, vc.externalname " +
                                 " FROM myVRM.DataLayer.vrmConfAttribute c, myVRM.DataLayer.vrmConference vc " +
                                 " WHERE vc.deleted=0 and c.CustomAttributeId = " + customAttrID.ToString() +
                                 " and c.ConfId = vc.confid and Datediff(minute,dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",confdate), getdate()) > 5";

                            IList futList = m_vrmConfDAO.execQuery(futStmt);
                            IList pastList = m_vrmConfDAO.execQuery(pastStmt);

                            futurecnt = futList.Count;
                            pastcnt = pastList.Count;

                            vrmConference confDetails = null;
                            foreach (object[] cl in futList)
                            {
                                confDetails = new vrmConference();

                                confDetails.confid = Int32.Parse(cl[0].ToString());
                                confDetails.instanceid = Int32.Parse(cl[1].ToString());
                                confDetails.confdate = Convert.ToDateTime(cl[2].ToString());
                                confDetails.confnumname = Int32.Parse(cl[3].ToString());
                                confDetails.externalname = cl[4].ToString();

                                conf_list.Add(confDetails);
                            }
                            StringBuilder strFuture = new StringBuilder();
                            if (conf_list != null)
                            {
                                if (conf_list.Count > 0)
                                {
                                    strFuture.Append("<future>");

                                    foreach (vrmConference conf in conf_list)
                                    {
                                        String tempFXML = "";
                                        strFuture.Append("<conference>");
                                        strFuture.Append("<confid>" + conf.confid + "</confid>");
                                        strFuture.Append("<confstatus>" + conf.status + "</confstatus>");
                                        strFuture.Append("<confnumname>" + conf.confnumname + "</confnumname>");
                                        strFuture.Append("<confname>" + conf.externalname + "</confname>");
                                        strFuture.Append("</conference>");
                                    }

                                    strFuture.Append("</future>");

                                    futureXml = strFuture.ToString();
                                }
                            }

                            confDetails = null;
                            conf_list = new List<vrmConference>();
                            foreach (object[] cl in pastList)
                            {
                                confDetails = new vrmConference();

                                confDetails.confid = Int32.Parse(cl[0].ToString());
                                confDetails.instanceid = Int32.Parse(cl[1].ToString());
                                confDetails.confdate = Convert.ToDateTime(cl[2].ToString());
                                confDetails.confnumname = Int32.Parse(cl[3].ToString());
                                confDetails.externalname = cl[4].ToString();

                                conf_list.Add(confDetails);
                            }

                            if (conf_list != null)
                            {
                                if (conf_list.Count > 0)
                                {
                                    StringBuilder strPast = new StringBuilder();

                                    strPast.Append("<past>");
                                    foreach (vrmConference conf in conf_list)
                                    {
                                        strPast.Append("<conference>");
                                        strPast.Append("<confid>" + conf.confid + "</confid>");
                                        strPast.Append("<confstatus>" + conf.status + "</confstatus>");
                                        strPast.Append("<confnumname>" + conf.confnumname + "</confnumname>");
                                        strPast.Append("<confname>" + conf.externalname + "</confname>");
                                        strPast.Append("</conference>");
                                    }

                                    strPast.Append("</past>");

                                    pastXml = strPast.ToString();
                                }
                            }

                            string errMes = "";
                            if (pastcnt > 0)
                            {
                                if (pastcnt > 1)
                                    errMes = pastcnt + " conference(s) in the past";
                                else
                                    errMes = pastcnt + " conference in the past";
                            }

                            if (futurecnt > 0)
                            {
                                if (errMes == "")
                                {
                                    if (futurecnt > 1)
                                        errMes = futurecnt + " conference(s) in the future";
                                    else
                                        errMes = futurecnt + " conference in the future";
                                }
                                else
                                {
                                    if (futurecnt > 1)
                                        errMes += " and " + futurecnt + " conference(s) in the future";
                                    else
                                        errMes += " and " + futurecnt + " conference in the future";
                                }
                            }

                            if (errMes != "")
                                errMes += " are linked to the selected custom option and data will be lost.";

                            if (pastcnt > 0 || futurecnt > 0) //FB 1970
                            {
                                obj.outXml = "<error>";
                                obj.outXml += "<message>" + errMes + "</message>";
                                obj.outXml += "<conferencelist>";
                                obj.outXml += pastXml;
                                obj.outXml += futureXml;
                                obj.outXml += "<pastcount>" + pastcnt + "</pastcount>";
                                obj.outXml += "<futurecount>" + futurecnt + "</futurecount>";
                                obj.outXml += "</conferencelist>";
                                obj.outXml += "</error>";
                                return false;
                            }
                        }
                    }
                }
                obj.outXml = "<Success>1</Success>";

                /* //FB 1750
                if (conf_list != null)
                {
                    if (conf_list.Count > 0)
                    {
                        bool isConnectedToCA = false;
                        DateTime nowDate = DateTime.Now;
                        IList<vrmConfAttribute> custAttrList = null;
                        pastXml = "<past>";
                        futureXml = "<future>";

                        foreach (vrmConference conf in conf_list)
                        {
                            custAttrList = m_confDAO.GetConfCustAttributes(conf.confid, conf.instanceid);
                            if (custAttrList != null)
                            {
                                if (custAttrList.Count > 0)
                                {
                                    isConnectedToCA = false;
                                    foreach (vrmConfAttribute vcf in custAttrList)
                                    {
                                        if (vcf.CustomAttributeId == customAttrID)
                                        {
                                            isConnectedToCA = true;
                                            break;
                                        }
                                    }

                                    if (isConnectedToCA)
                                    {
                                        isConnectedToCA = false;
                                        isFutureConf = false;
                                        nowDate = DateTime.Now;
                                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref nowDate);

                                        TimeSpan span = nowDate.Subtract(conf.confdate);
                                        double minDiff = span.TotalMinutes;

                                        if (minDiff < 5)
                                            isFutureConf = true;

                                        if (isFutureConf)
                                            futurecnt++;
                                        else
                                            pastcnt++;

                                        if (isFutureConf)
                                        {
                                            futurecnt++;

                                            futureXml += "<conference>";
                                            futureXml += "<confid>" + conf.confid + "</confid>";
                                            futureXml += "<confstatus>" + conf.status + "</confstatus>";
                                            futureXml += "<confnumname>" + conf.confnumname + "</confnumname>";
                                            futureXml += "<confname>" + conf.externalname + "</confname>";
                                            futureXml += "</conference>";
                                        }
                                        else
                                        {
                                            pastcnt++;

                                            pastXml += "<conference>";
                                            pastXml += "<confid>" + conf.confid + "</confid>";
                                            pastXml += "<confstatus>" + conf.status + "</confstatus>";
                                            pastXml += "<confnumname>" + conf.confnumname + "</confnumname>";
                                            pastXml += "<confname>" + conf.externalname + "</confname>";
                                            pastXml += "</conference>";
                                        }
                                    }
                                }
                            }
                        }

                        pastXml += "</past>";
                        futureXml += "</future>";

                        string errMes = "";
                        if (pastcnt > 0)
                        {
                            if (pastcnt > 1)
                                errMes = pastcnt + " conference(s) in the past";
                            else
                                errMes = pastcnt + " conference in the past";
                        }

                        if (futurecnt > 0)
                        {
                            if (errMes == "")
                            {
                                if (futurecnt > 1)
                                    errMes = futurecnt + " conference(s) in the future";
                                else
                                    errMes = futurecnt + " conference in the future";
                            }
                            else
                            {
                                if (futurecnt > 1)
                                    errMes += " and " + futurecnt + " conference(s) in the future";
                                else
                                    errMes += " and " + futurecnt + " conference in the future";
                            }
                        }

                        if (errMes != "")
                            errMes += " are linked to the selected custom option and data will be lost.";

                        obj.outXml = "<error>";
                        obj.outXml += "<message>" + errMes + "</message>";
                        obj.outXml += "<conferencelist>";
                        obj.outXml += pastXml;
                        obj.outXml += futureXml;
                        obj.outXml += "<pastcount>" + pastcnt + "</pastcount>";
                        obj.outXml += "<futurecount>" + futurecnt + "</futurecount>";
                        obj.outXml += "</conferencelist>";
                        obj.outXml += "</error>";
                        return false;
                    }
                }
               } 
            }
            }
            obj.outXml = "<Success>1</Success>"; */
                //FB 1750
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException", ex);
                //obj.outXml = myVRMException.toXml(ex.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            return true;
        }
        #endregion

        #region Get the list of conferences linked to custom attribute
        /// <summary>
        /// Get the list of conferences linked to custom attribute
        /// </summary>
        /// <returns></returns>
        private string FetchConfListByCustOpt(Int32 customAttrID)
        {
            bool isFutureConf = false;
            string outXML = "";
            string futureXml = "";
            string pastXml = "";
            int pastcnt = 0;
            int futurecnt = 0;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeId", customAttrID));
                List<vrmConfAttribute> customAttlist = m_IconfAttrDAO.GetByCriteria(criterionList);
                criterionList = null;

                if (customAttlist != null)
                {
                    if (customAttlist.Count > 0)
                    {
                        //Code Commented for FB 1750 
                        //List<int> confsList = new List<int>();
                        //foreach (vrmConfAttribute confAttr in customAttlist)
                        //{
                        //    confsList.Add(confAttr.ConfId);
                        //}

                        //if (confsList.Count > 0)
                        //{
                        //    criterionList = new List<ICriterion>();
                        //    criterionList.Add(Expression.In("confid", confsList));
                        //    criterionList.Add(Expression.Eq("deleted", 0));
                        List<vrmConference> conf_list = new List<vrmConference>();
                        //conf_list = m_vrmConfDAO.GetByCriteria(criterionList);                        

                        String futStmt = "SELECT DISTINCT vc.confid, vc.instanceid, vc.confdate, vc.confnumname, vc.externalname " +
                         " FROM myVRM.DataLayer.vrmConfAttribute c, myVRM.DataLayer.vrmConference vc " +
                         " WHERE vc.deleted=0 and c.CustomAttributeId = " + customAttrID.ToString() +
                         " and c.ConfId = vc.confid and Datediff(minute,dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",confdate), getdate()) < 5";

                        String pastStmt = "SELECT DISTINCT vc.confid, vc.instanceid, vc.confdate, vc.confnumname, vc.externalname " +
                             " FROM myVRM.DataLayer.vrmConfAttribute c, myVRM.DataLayer.vrmConference vc " +
                             " WHERE vc.deleted=0 and c.CustomAttributeId = " + customAttrID.ToString() +
                             " and c.ConfId = vc.confid and Datediff(minute,dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",confdate), getdate()) > 5";

                        IList futList = m_vrmConfDAO.execQuery(futStmt);
                        IList pastList = m_vrmConfDAO.execQuery(pastStmt);

                        futurecnt = futList.Count;
                        pastcnt = pastList.Count;

                        vrmConference confDetails = null;
                        foreach (object[] cl in futList)
                        {
                            confDetails = new vrmConference();

                            confDetails.confid = Int32.Parse(cl[0].ToString());
                            confDetails.instanceid = Int32.Parse(cl[1].ToString());
                            confDetails.confdate = Convert.ToDateTime(cl[2].ToString());
                            confDetails.confnumname = Int32.Parse(cl[3].ToString());
                            confDetails.externalname = cl[4].ToString();

                            conf_list.Add(confDetails);
                        }
                        StringBuilder strFuture = new StringBuilder();
                        if (conf_list != null)
                        {
                            if (conf_list.Count > 0)
                            {
                                strFuture.Append("<future>");

                                foreach (vrmConference conf in conf_list)
                                {
                                    String tempFXML = "";
                                    strFuture.Append("<conference>");
                                    strFuture.Append("<confid>" + conf.confid + "</confid>");
                                    strFuture.Append("<confstatus>" + conf.status + "</confstatus>");
                                    strFuture.Append("<confnumname>" + conf.confnumname + "</confnumname>");
                                    strFuture.Append("<confname>" + conf.externalname + "</confname>");
                                    strFuture.Append("</conference>");
                                }

                                strFuture.Append("</future>");

                                futureXml = strFuture.ToString();
                            }
                        }

                        confDetails = null;
                        conf_list = new List<vrmConference>();
                        foreach (object[] cl in pastList)
                        {
                            confDetails = new vrmConference();

                            confDetails.confid = Int32.Parse(cl[0].ToString());
                            confDetails.instanceid = Int32.Parse(cl[1].ToString());
                            confDetails.confdate = Convert.ToDateTime(cl[2].ToString());
                            confDetails.confnumname = Int32.Parse(cl[3].ToString());
                            confDetails.externalname = cl[4].ToString();

                            conf_list.Add(confDetails);
                        }

                        if (conf_list != null)
                        {
                            if (conf_list.Count > 0)
                            {
                                StringBuilder strPast = new StringBuilder();

                                strPast.Append("<past>");
                                foreach (vrmConference conf in conf_list)
                                {
                                    strPast.Append("<conference>");
                                    strPast.Append("<confid>" + conf.confid + "</confid>");
                                    strPast.Append("<confstatus>" + conf.status + "</confstatus>");
                                    strPast.Append("<confnumname>" + conf.confnumname + "</confnumname>");
                                    strPast.Append("<confname>" + conf.externalname + "</confname>");
                                    strPast.Append("</conference>");
                                }

                                strPast.Append("</past>");

                                pastXml = strPast.ToString();
                            }
                        }

                        string errMes = "";
                        if (pastcnt > 0)
                        {
                            if (pastcnt > 1)
                                errMes = pastcnt + " conference(s) in the past";
                            else
                                errMes = pastcnt + " conference in the past";
                        }

                        if (futurecnt > 0)
                        {
                            if (errMes == "")
                            {
                                if (futurecnt > 1)
                                    errMes = futurecnt + " conference(s) in the future";
                                else
                                    errMes = futurecnt + " conference in the future";
                            }
                            else
                            {
                                if (futurecnt > 1)
                                    errMes += " and " + futurecnt + " conference(s) in the future";
                                else
                                    errMes += " and " + futurecnt + " conference in the future";
                            }
                        }

                        if (errMes != "")
                            errMes += " are linked to the selected custom option and data will be lost.";

                        outXML = "<error>";
                        outXML += "<message>" + errMes + "</message>";
                        outXML += "<conferencelist>";
                        outXML += pastXml;
                        outXML += futureXml;
                        outXML += "<pastcount>" + pastcnt + "</pastcount>";
                        outXML += "<futurecount>" + futurecnt + "</futurecount>";
                        outXML += "</conferencelist>";
                        outXML += "</error>";

                    }
                }

                //Code Commented for FB 1750 
                /*
                if (conf_list != null)
                {
                    if (conf_list.Count > 0)
                    {
                        bool isConnectedToCA = false;
                        DateTime nowDate = DateTime.Now;
                        IList<vrmConfAttribute> custAttrList = null;
                        pastXml = "<past>";
                        futureXml = "<future>";
                        foreach (vrmConference conf in conf_list)
                        {
                            custAttrList = m_confDAO.GetConfCustAttributes(conf.confid, conf.instanceid);
                            if (custAttrList != null)
                            {
                                if (custAttrList.Count > 0)
                                {
                                    isConnectedToCA = false;
                                    foreach (vrmConfAttribute vcf in custAttrList)
                                    {
                                        if (vcf.CustomAttributeId == customAttrID)
                                        {
                                            isConnectedToCA = true;
                                            break;
                                        }
                                    }

                                    if (isConnectedToCA)
                                    {
                                        isConnectedToCA = false;
                                        isFutureConf = false;
                                        nowDate = DateTime.Now;
                                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref nowDate);

                                        TimeSpan span = nowDate.Subtract(conf.confdate);
                                        double minDiff = span.TotalMinutes;

                                        if (minDiff < 5)
                                            isFutureConf = true;

                                        if (isFutureConf)
                                        {
                                            futurecnt++;

                                            futureXml += "<conference>";
                                            futureXml += "<confid>" + conf.confid + "</confid>";
                                            futureXml += "<confstatus>" + conf.status + "</confstatus>";
                                            futureXml += "<confnumname>" + conf.confnumname + "</confnumname>";
                                            futureXml += "<confname>" + conf.externalname + "</confname>";
                                            futureXml += "</conference>";
                                        }
                                        else
                                        {
                                            pastcnt++;

                                            pastXml += "<conference>";
                                            pastXml += "<confid>" + conf.confid + "</confid>";
                                            pastXml += "<confstatus>" + conf.status + "</confstatus>";
                                            pastXml += "<confnumname>" + conf.confnumname + "</confnumname>";
                                            pastXml += "<confname>" + conf.externalname + "</confname>";
                                            pastXml += "</conference>";
                                        }
                                    }
                                }
                            }
                        }
                        pastXml += "</past>";
                        futureXml += "</future>";

                        string errMes ="";
                        if (pastcnt > 0)
                        {
                            if (pastcnt > 1)
                                errMes = pastcnt + " conference(s) in the past";
                            else
                                errMes = pastcnt + " conference in the past";
                        }

                        if (futurecnt > 0)
                        {
                            if (errMes == "")
                            {
                                if (futurecnt > 1)
                                    errMes = futurecnt + " conference(s) in the future";
                                else
                                    errMes = futurecnt + " conference in the future";
                            }
                            else
                            {
                                if (futurecnt > 1)
                                    errMes += " and " + futurecnt + " conference(s) in the future";
                                else
                                    errMes += " and " + futurecnt + " conference in the future";
                            }
                        }

                        if(errMes != "")
                            errMes += " are linked to the selected custom option and data will be lost.";
                        
                        outXML = "<error>";
                        outXML += "<message>"+ errMes +"</message>";
                        outXML += "<conferencelist>";
                        outXML += pastXml;
                        outXML += futureXml;
                        outXML += "<pastcount>" + pastcnt + "</pastcount>";
                        outXML += "<futurecount>" + futurecnt + "</futurecount>";
                        outXML += "</conferencelist>";
                        outXML += "</error>";
                    }
                }
                }
                    }
                }
                 **/
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException", ex);
                //outXML = "<error>System Error. Please contact VRM Administrator.</error>";//FB 1881
                outXML = ""; //FB 1881
            }
            return outXML;
        }
        #endregion

        #region Delete Conferences linked to custom attributes
        /// <summary>
        /// Delete Conferences linked to custom attributes
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <CustomAttribute><ID></ID></CustomAttribute>
        public bool DeleteConfsAttributeByID(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//CustomAttribute/ID");
                string customAttributeID = node.InnerXml.Trim();

                if (customAttributeID == "")
                {
                    obj.outXml = "<error>Invalid Custom Option ID</error>";
                    return false;
                }

                Int32 custAttrId = 0;
                Int32.TryParse(customAttributeID, out custAttrId);
                if (custAttrId <= 0)
                {
                    obj.outXml = "<error>Invalid Custom Option ID</error>";
                    return false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/mode");
                string prgMode = node.InnerXml.Trim();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeId", custAttrId));
                List<vrmConfAttribute> customAttlist = m_IconfAttrDAO.GetByCriteria(criterionList);
                criterionList = null;

                if (customAttlist != null)
                {
                    if (customAttlist.Count > 0)
                    {
                        foreach (vrmConfAttribute confAttr in customAttlist)
                        {
                            m_IconfAttrDAO.clearFetch();
                            m_IconfAttrDAO.Delete(confAttr);
                        }
                    }
                }
                bRet = true;

                if (prgMode.ToUpper() == "D")
                {
                    bRet = DeleteCustomOption(custAttrId);
                }

                if (bRet)
                    obj.outXml = "<success>1</success>";
                else
                {
                    //FB 1881 Start
                    //obj.outXml = "<error>Error in deleting the custom attribute</error>";
                    myvrmEx = new myVRMException(484);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            return bRet;
        }
        #endregion
                
        #region DeleteCustomAttribute
        /// <summary>
        /// Delete Custom Attribute
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <CustomAttribute><ID></ID></CustomAttribute>
        public bool DeleteCustomAttribute(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//CustomAttribute/ID");
                string customAttributeID = node.InnerXml.Trim();

                if (customAttributeID == "")
                {
                    obj.outXml = "<error>Invalid Custom Option ID</error>";
                    return false;
                }

                Int32 custAttrId = 0;
                Int32.TryParse(customAttributeID, out custAttrId);
                if (custAttrId <= 0)
                {
                    obj.outXml = "<error>Invalid Custom Option ID</error>";
                    return false;
                }

                string confOutXml = "";
                confOutXml = FetchConfListByCustOpt(custAttrId);
                if (confOutXml != "")
                {
                    obj.outXml = confOutXml;
                    return false;
                }
                bRet = DeleteCustomOption(custAttrId);

                if (bRet)
                    obj.outXml = "<success>1</success>";
                else
                {
                    //FB 1881 start
                    //obj.outXml = "<error>Error in deleting the custom attribute</error>";
                    myvrmEx = new myVRMException(484);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            return bRet;
        }
        #endregion

        #region DeleteCustomOption
        /// <summary>
        /// Delete Custom Attribute
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <CustomAttribute><ID></ID></CustomAttribute>
        private bool DeleteCustomOption(Int32 custAttrID)
        {
            try
            {
                if (custAttrID <= 0)
                    return false;

                //Deletes the options of the Custom Attribute ID
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeID", custAttrID));
                List<vrmDeptCustomAttrOption> custOptions = m_IDeptCustOptDAO.GetByCriteria(criterionList);

                foreach (vrmDeptCustomAttrOption CAOpt in custOptions)
                {
                    m_IDeptCustOptDAO.clearFetch();
                    m_IDeptCustOptDAO.Delete(CAOpt);
                }
                criterionList = null;

                //Deletes the Custom Attribute
                criterionList = new List<ICriterion>();
                List<vrmDeptCustomAttr> custAttributeList = new List<vrmDeptCustomAttr>();
                criterionList.Add(Expression.Eq("CustomAttributeId", custAttrID));
                custAttributeList = m_IDeptCustDAO.GetByCriteria(criterionList);

                foreach (vrmDeptCustomAttr deptCA in custAttributeList)
                    m_IDeptCustDAO.Delete(deptCA);

                List<VrmCustomLanguage> CustAttrTitles = m_ICustomAttrLangDAO.GetCustomAttrTitlesByID(custAttrID);//FB 1970
                for (int i = 0; i < CustAttrTitles.Count; i++)//FB 1970
                    m_ICustomAttrLangDAO.Delete(CustAttrTitles[i]);

                    return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region Get Custom Attributes
        /// <summary>
        /// Get Custom Attributes
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <CustomAttribute><DeptID></DeptID></CustomAttribute>
        public bool GetCustomAttributes(ref vrmDataObject obj)
        {
            bool bRet = false;
            try
            {
                XmlDocument xd = new XmlDocument();//Organization Module Fixes
                xd.LoadXml(obj.inXml);

                XmlNode node;


                obj.outXml = "";

                m_IDeptCustDAO.clearFetch();
                List<vrmDeptCustomAttr> customAttributes = new List<vrmDeptCustomAttr>();

                node = xd.SelectSingleNode("//CustomAttribute/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11) //FB 1779
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //FB 1779
                if (orgInfo == null) //FB 1779
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                m_IDeptCustDAO.addOrderBy(Order.Desc("CreateType"));   //FB 1779
                m_IDeptCustDAO.addOrderBy(Order.Asc("DisplayTitle"));   //FB 1779

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID.ToString()));
                criterionList.Add(Expression.Eq("deleted",0)); //FB 2501

                customAttributes = m_IDeptCustDAO.GetByCriteria(criterionList); //Organization Module Fixes

                obj.outXml = "<CustomAttributesList>";
                obj.outXml += "<CustomAttrLimit>" + orgInfo.CustomAttributeLimit + "</CustomAttrLimit>"; //FB 1779

                if (customAttributes != null)
                {
                    foreach (vrmDeptCustomAttr deptCA in customAttributes)
                    {
                        obj.outXml += "<CustomAttribute>";
                        obj.outXml += "<CustomAttributeID>" + deptCA.CustomAttributeId + "</CustomAttributeID>";
                        obj.outXml += "<Title>" + deptCA.DisplayTitle + "</Title>";
                        obj.outXml += "<Mandatory>" + deptCA.Mandatory + "</Mandatory>";
                        obj.outXml += "<Description>" + deptCA.Description + "</Description>";
                        obj.outXml += "<Type>" + deptCA.Type + "</Type>";
                        obj.outXml += "<Status>" + deptCA.status + "</Status>";
                        obj.outXml += "<IncludeInEmail>" + deptCA.IncludeInEmail + "</IncludeInEmail>";
                        obj.outXml += "<CreateType>" + deptCA.CreateType + "</CreateType>";  //FB 1779

                        obj.outXml += FetchCustomAttributeOption(deptCA.CustomAttributeId);

                        obj.outXml += "</CustomAttribute>";
                    }
                }
                obj.outXml += "</CustomAttributesList>";
                bRet = true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region Fetch CustomAttribute Option
        /// <summary>
        /// FetchCustomAttributeOption
        /// </summary>
        /// <param name="customAttId"></param>
        /// <returns></returns>
        private String FetchCustomAttributeOption(Int32 customAttId)
        {
            string outXml = "";
            try
            {
                m_IDeptCustOptDAO.clearFetch();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeID", customAttId));
                List<vrmDeptCustomAttrOption> custOptions = m_IDeptCustOptDAO.GetByCriteria(criterionList);

                outXml += "<OptionList>";
                foreach (vrmDeptCustomAttrOption deptCAOpt in custOptions)
                {
                    vrmLanguage language = m_ILanguageDAO.GetLanguageById(deptCAOpt.LanguageID); //FB 1970
                    outXml += "<Option>";
                    outXml += "<OptionID>" + deptCAOpt.OptionID + "</OptionID>";
                    outXml += "<DisplayValue>" + deptCAOpt.OptionValue + "</DisplayValue>";
                    outXml += "<DisplayCaption>" + deptCAOpt.Caption + "</DisplayCaption>";
                    outXml += "<HelpText>" + deptCAOpt.HelpText + "</HelpText>";
                    outXml += "<ID>" + deptCAOpt.LanguageID + "</ID>"; //FB 1970
                    outXml += "<name>" + language.Name.ToString() + "</name>"; //FB 1970
                    outXml += "</Option>";
                }
                outXml += "</OptionList>";
            }
            catch (Exception ex)
            {
                m_log.Error(ex);
            }
            return outXml;
        }
        #endregion

        #region Get Custom Attribute By ID
        /// <summary>
        /// Get Custom Attribute By ID
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <CustomAttribute><ID></ID></CustomAttribute>
        public bool GetCustomAttributeByID(ref vrmDataObject obj)
        {
            bool bRet = false;
            int customAttId = 0;
            string customAttributeId = "";
            try
            {
                vrmLanguage language = new vrmLanguage(); //FB 1970
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//CustomAttribute/ID");
                if (node != null)
                    customAttributeId = node.InnerText;

                if (customAttributeId == "")
                {
                    obj.outXml = "<error>Invalid Custom Option ID</error>";
                    return false;
                }

                Int32.TryParse(customAttributeId, out customAttId);
                m_IDeptCustDAO.clearFetch();
                vrmDeptCustomAttr deptCA = m_IDeptCustDAO.GetById(customAttId);

                obj.outXml = "<CustomAttribute>";
                if (deptCA != null)
                {
                    obj.outXml += "<CustomAttributeID>" + deptCA.CustomAttributeId + "</CustomAttributeID>";
                    obj.outXml += "<Title>" + deptCA.DisplayTitle + "</Title>";
                    
                    //FB 1970 start
                    List<VrmCustomLanguage> custTitlelist = m_ICustomAttrLangDAO.GetCustomAttrTitlesByID(deptCA.CustomAttributeId);
                    for (int i = 0; i < custTitlelist.Count; i++)
                    {
                        if (custTitlelist[i].LanguageID == 1) //Default LanguageId
                            continue;

                        language = m_ILanguageDAO.GetLanguageById(custTitlelist[i].LanguageID);
                        obj.outXml += "<TitleList>";
                        obj.outXml += "<ID>" + custTitlelist[i].LanguageID + "</ID>";
                        obj.outXml += "<name>" + language.Name.ToString() + "</name>";
                        obj.outXml += "<Title>" + custTitlelist[i].DisplayTitle + "</Title>";
                        obj.outXml += "</TitleList>";
                    }
                    //FB 1970 end

                    obj.outXml += "<CreateType>" + deptCA.CreateType + "</CreateType>"; //FB 2349
                    obj.outXml += "<Mandatory>" + deptCA.Mandatory + "</Mandatory>";
                    obj.outXml += "<Description>" + deptCA.Description + "</Description>";
                    obj.outXml += "<Type>" + deptCA.Type + "</Type>";
                    obj.outXml += "<Status>" + deptCA.status + "</Status>";
                    obj.outXml += "<IncludeInEmail>" + deptCA.IncludeInEmail + "</IncludeInEmail>";
                    obj.outXml += "<IncludeInCalendar>" + deptCA.IncludeInCalendar + "</IncludeInCalendar>"; //FB 2013
                    //FB 1767 start
                    obj.outXml += "<MailToHost>" + deptCA.Host + "</MailToHost>";
                    obj.outXml += "<MailToScheduler>" + deptCA.Scheduler + "</MailToScheduler>";
                    obj.outXml += "<MailToParty>" + deptCA.Party + "</MailToParty>";
                    obj.outXml += "<MailToRoomAdmin>" + deptCA.RoomAdmin + "</MailToRoomAdmin>";
                    obj.outXml += "<MailToMCUAdmin>" + deptCA.McuAdmin + "</MailToMCUAdmin>";
                    obj.outXml += "<MailToRoomApp>" + deptCA.RoomApp + "</MailToRoomApp>";
                    obj.outXml += "<MailToMCUApp>" + deptCA.McuApp + "</MailToMCUApp>";
                    obj.outXml += "<MailToSysApp>" + deptCA.SystemApp + "</MailToSysApp>";
                    //FB 1767 end
                    obj.outXml += "<MailToVNOCOp>" + deptCA.VNOCOperator + "</MailToVNOCOp>"; //FB 2501
                    if (deptCA.Type == 5 || deptCA.Type == 6 || deptCA.Type == 8) //FB 1718
                        obj.outXml += FetchCustomAttributeOption(deptCA.CustomAttributeId);
                }
                obj.outXml += "</CustomAttribute>";
                bRet = true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetCustomAttributeDescription - Not Used
        public bool GetCustomAttributeDescription(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetEntityCodeDescription/CustomAttributeID");
                string customAttributeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetEntityCodeDescription/OptionType");
                string optionType = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetEntityCodeDescription/OptionID");
                string optionID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetEntityCodeDescription/Caption");
                string caption = node.InnerXml.Trim();

                List<vrmDeptCustomAttrOption> custAttributeList = new List<vrmDeptCustomAttrOption>();

                criterionList.Add(Expression.Eq("OptionType", Int32.Parse(optionType)));
                criterionList.Add(Expression.Eq("OptionID", Int32.Parse(optionID)));
                criterionList.Add(Expression.Eq("CustomAttributeID", Int32.Parse(customAttributeID)));
                criterionList.Add(Expression.Eq("Caption", caption));
                custAttributeList = m_IDeptCustOptDAO.GetByCriteria(criterionList);
                foreach (vrmDeptCustomAttrOption ca in custAttributeList)
                {
                    obj.outXml += "<EntityCode>";
                    obj.outXml += "<CustomAttributeID>" + ca.CustomAttributeID.ToString() + "</CustomAttributeID>";
                    obj.outXml += "<Type>" + ca.OptionType.ToString() + "</Type>";
                    obj.outXml += "<OptionID>" + ca.OptionID.ToString() + "</OptionID>";
                    obj.outXml += "<DisplayValue>" + ca.OptionValue + "</DisplayValue>";
                    obj.outXml += "<DisplayCaption>" + ca.Caption + "</DisplayCaption>";
                    obj.outXml += "<HelpText>" + ca.HelpText + "</HelpText>";
                    obj.outXml += "</EntityCode>";
                }
                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region Set CustomAttribute Details
        /// <summary>
        /// Set CustomAttribute Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetCustomAttribute(ref vrmDataObject obj)
        {
            string title = "";
            string description = "";
            string fieldType = "";
            string CAstatus = ""; //0-enable 1-disabled
            string includeInEmail = "";
            string isMandatory = "";
            string customAttrId = "";
            int customAttributeId = 0, includeinCalendar = 0; //FB 2013
            int host = 0, scheduler = 0, party = 0, mcuAdmin = 0, roomAdmin = 0, roomApp = 0; //FB 1767
            int mcuApp = 0,SysApp = 0, vnocOperator = 0; //FB 1767 //FB 2501
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//CustomAttribute/CustomAttributeID");
                if (node != null)
                    customAttrId = node.InnerText;

                if (customAttrId == "")
                    customAttrId = "new";

                node = xd.SelectSingleNode("//CustomAttribute/Title");
                if (node != null)
                    title = node.InnerText;

                if (title == "")
                {
                    obj.outXml = "<error>Invalid InXml</error>";
                    return false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/Mandatory");
                if (node != null)
                    isMandatory = node.InnerText;
                if (isMandatory == "")
                    isMandatory = "0";

                node = xd.SelectSingleNode("//CustomAttribute/Description");
                if (node != null)
                    description = node.InnerText;

                node = xd.SelectSingleNode("//CustomAttribute/Type");
                if (node != null)
                    fieldType = node.InnerText;

                if (fieldType == "")
                    fieldType = "4";    //Text Box

                node = xd.SelectSingleNode("//CustomAttribute/Status");
                if (node != null)
                    CAstatus = node.InnerText;
                if (CAstatus == "")
                    CAstatus = "0"; //enabled 

                node = xd.SelectSingleNode("//CustomAttribute/IncludeEmail");
                if (node != null)
                    includeInEmail = node.InnerText;
                if (includeInEmail == "")
                    includeInEmail = "0";

                node = xd.SelectSingleNode("//CustomAttribute/IncludeInCalendar");
                if (node != null)
                    int.TryParse(node.InnerText.Trim().ToString(),out includeinCalendar);

                //FB 1767 start
                node = xd.SelectSingleNode("//CustomAttribute/MailToHost");
                if (node != null)
                    Int32.TryParse(node.InnerText, out host); 

                node = xd.SelectSingleNode("//CustomAttribute/MailToScheduler");
                if (node != null)
                    Int32.TryParse(node.InnerText, out scheduler);

                node = xd.SelectSingleNode("//CustomAttribute/MailToParty");
                if (node != null)
                    Int32.TryParse(node.InnerText, out party);

                node = xd.SelectSingleNode("//CustomAttribute/MailToRoomAdmin");
                if (node != null)
                    Int32.TryParse(node.InnerText, out roomAdmin);

                node = xd.SelectSingleNode("//CustomAttribute/MailToMCUAdmin");
                if (node != null)
                    Int32.TryParse(node.InnerText, out mcuAdmin);

                node = xd.SelectSingleNode("//CustomAttribute/MailToRoomApp");
                if (node != null)
                    Int32.TryParse(node.InnerText, out roomApp);

                node = xd.SelectSingleNode("//CustomAttribute/MailToMCUApp");
                if (node != null)
                    Int32.TryParse(node.InnerText, out mcuApp);

                node = xd.SelectSingleNode("//CustomAttribute/MailToSysApp");
                if (node != null)
                    Int32.TryParse(node.InnerText, out SysApp);
                    
                //FB 1767 end

                //FB 2501 Starts
                node = xd.SelectSingleNode("//CustomAttribute/MailToVNOCOp");
                if (node != null)
                    Int32.TryParse(node.InnerText, out vnocOperator);
                //FB 2501 Ends
                
                node = xd.SelectSingleNode("//CustomAttribute/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11) //FB 1779
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //FB 1779
                if (orgInfo == null) //FB 1779
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                int optionType = 0;
                Int32.TryParse(fieldType, out optionType);

                int mandatory = 0;
                Int32.TryParse(isMandatory, out mandatory);

                int dispStat = 0;
                Int32.TryParse(CAstatus, out dispStat);

                int emailReq = 0;
                Int32.TryParse(includeInEmail, out emailReq);

                bool isSystemEdit = false; //FB 2349

                vrmDeptCustomAttr vrmDeptCA = new vrmDeptCustomAttr();

                vrmDeptCA.Mandatory = mandatory;
                vrmDeptCA.status = dispStat;
                vrmDeptCA.IncludeInEmail = emailReq;
                vrmDeptCA.IncludeInCalendar = includeinCalendar; //FB 2013
                //Fb 1767 start
                vrmDeptCA.Host = host;
                vrmDeptCA.Scheduler = scheduler;
                vrmDeptCA.Party = party;
                vrmDeptCA.RoomAdmin = roomAdmin;
                vrmDeptCA.McuAdmin = mcuAdmin;
                vrmDeptCA.RoomApp = roomApp;
                vrmDeptCA.McuApp = mcuApp;
                vrmDeptCA.SystemApp = SysApp;
                //Fb 1767 end
                vrmDeptCA.VNOCOperator = vnocOperator; //FB 2501

                vrmDeptCA.orgId = organizationID.ToString();//Organization Module Fixes
                if (customAttrId.ToLower() == "new")
                {
                    //FB 2349 code changes - start
                    vrmDeptCA.CreateType = 0; //User FB 1779
                    vrmDeptCA.DeptID = 0;
                    vrmDeptCA.deleted = 0;
                    vrmDeptCA.Description = description;
                    vrmDeptCA.DisplayTitle = title;
                    vrmDeptCA.Type = optionType;
                    //FB 2349 code changes - end

                    int recordCount = 0;
                    string cusAttNameCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmDeptCustomAttr vca WHERE vca.DisplayTitle='" + title.Trim() + "' and orgId = '"+ organizationID.ToString() +"'";
                    IList recCnt = m_IDeptCustDAO.execQuery(cusAttNameCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    Int32.TryParse(recCnt[0].ToString(), out recordCount);
                            }
                        }
                    }
                    if (recordCount > 0)
                    {
                        //FB 1881 start
                        //obj.outXml = "<error>Custom option title already exists.</error>";
                        myvrmEx = new myVRMException(485);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }

                    int maximumLimit = 0;
                    string maxLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmDeptCustomAttr vc WHERE vc.CreateType=0 and orgId='" + organizationID.ToString() + "'"; //User limit is 10 //FB 1779
                    IList resRec = m_IDeptCustDAO.execQuery(maxLimit);
                    if (resRec != null)
                    {
                        if (resRec.Count > 0)
                        {
                            if (resRec[0] != null)
                            {
                                if (resRec[0].ToString() != "")
                                    Int32.TryParse(resRec[0].ToString(), out maximumLimit);
                            }
                        }
                    }
                    if (maximumLimit >= orgInfo.CustomAttributeLimit) //FB 1779
                    {
                        //FB 1881 start
                        //obj.outXml = "<error>Maximum limit for the custom options is " + orgInfo.CustomAttributeLimit + ".</error>"; //FB 1779
                        myvrmEx = new myVRMException(486);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }

                    string checkAttributeId = "SELECT max(vc.CustomAttributeId) FROM myVRM.DataLayer.vrmDeptCustomAttr vc";
                    IList Result = m_IDeptCustDAO.execQuery(checkAttributeId);
                    customAttributeId = 0;

                    if (Result != null)
                    {
                        if (Result.Count > 0)
                        {
                            if (Result[0] != null)
                            {
                                if (Result[0].ToString().Trim() != "")
                                    Int32.TryParse(Result[0].ToString(), out customAttributeId);
                            }
                        }
                    }
                    customAttributeId = customAttributeId + 1;

                    vrmDeptCA.CustomAttributeId = customAttributeId;
                    m_IDeptCustDAO.Save(vrmDeptCA);
                }
                else
                {
                    Int32.TryParse(customAttrId, out customAttributeId);
                    vrmDeptCA.CustomAttributeId = customAttributeId;
                    
                    //FB 2349 code changes - start
                    vrmDeptCustomAttr custOpt = m_IDeptCustDAO.GetById(customAttributeId);
                    if (custOpt != null)
                    {
                        if (custOpt.CreateType == 1)
                        {
                            isSystemEdit = true;
                            vrmDeptCA.CreateType = custOpt.CreateType;
                            vrmDeptCA.DeptID = custOpt.DeptID;
                            vrmDeptCA.deleted = custOpt.deleted;
                            vrmDeptCA.Description = custOpt.Description;
                            vrmDeptCA.DisplayTitle = custOpt.DisplayTitle;
                            vrmDeptCA.Type = custOpt.Type;
                        }
                        else
                        {
                            vrmDeptCA.CreateType = 0; //User FB 1779
                            vrmDeptCA.DeptID = 0;
                            vrmDeptCA.deleted = 0;
                            vrmDeptCA.Description = description;
                            vrmDeptCA.DisplayTitle = title;
                            vrmDeptCA.Type = optionType;
                        }


                        int custId = 0;
                        string cusAttNameID = "SELECT vca.CustomAttributeId FROM myVRM.DataLayer.vrmDeptCustomAttr vca WHERE vca.DisplayTitle='" + title.Trim() + "' and vca.orgId='" + organizationID.ToString() + "'"; //FB 2349
                        IList recCnt = m_IDeptCustDAO.execQuery(cusAttNameID);
                        if (recCnt != null)
                        {
                            if (recCnt.Count > 0)
                            {
                                if (recCnt[0].ToString().Trim() != "")
                                {
                                    Int32.TryParse(recCnt[0].ToString(), out custId);

                                    if ((custId != customAttributeId) && (custId > 0))
                                    {
                                        //FB 1881 start
                                        myvrmEx = new myVRMException(485);
                                        obj.outXml = myvrmEx.FetchErrorMsg();
                                        //FB 1881 end
                                        return false;
                                    }
                                }
                            }
                        }
                        m_IDeptCustDAO.SaveOrUpdate(vrmDeptCA);
                    }
                    //FB 2349 code changes - end
                }
                //FB 1970 start
                //FB 2349 code changes - start
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeID", customAttributeId));
                if (!isSystemEdit) //FB 2349 end
                {
                    VrmCustomLanguage custTitle = null;
                    XmlNodeList nodeList = null;
                    if (xd.SelectNodes("//CustomAttribute/TitleList") != null)
                    {
                        nodeList = xd.SelectNodes("//CustomAttribute/TitleList");
                        List<VrmCustomLanguage> custLangTitle = m_ICustomAttrLangDAO.GetByCriteria(criterionList);
                        for (int i = 0; i < custLangTitle.Count && nodeList.Count > 0; i++)
                            m_ICustomAttrLangDAO.Delete(custLangTitle[i]);

                        for (int i = 0; i < nodeList.Count; i++)
                        {
                            int LangID = 1;
                            if (nodeList[i].SelectSingleNode("ID") != null)
                                int.TryParse(nodeList[i].SelectSingleNode("ID").InnerText.Trim(), out LangID);
                            if (nodeList[i].SelectSingleNode("Title") != null)
                                title = nodeList[i].SelectSingleNode("Title").InnerText.Trim();

                            custTitle = new VrmCustomLanguage();
                            custTitle.LanguageID = LangID;
                            custTitle.DisplayTitle = title;
                            custTitle.CustomAttributeID = customAttributeId;
                            m_ICustomAttrLangDAO.Save(custTitle);

                        }
                    }
                } //FB 2349
                //FB 1970 end
                if (optionType == 5 || optionType == 6 || optionType == 8) //FB 1718
                {
                    XmlNodeList optNodes = xd.SelectNodes("//CustomAttribute/OptionList/Option");
                    if (optNodes != null)
                    {
                        List<vrmDeptCustomAttrOption> custOptions = m_IDeptCustOptDAO.GetByCriteria(criterionList);

                        foreach (vrmDeptCustomAttrOption CAOpt in custOptions)
                        {
                            m_IDeptCustOptDAO.clearFetch();
                            m_IDeptCustOptDAO.Delete(CAOpt);
                        }

                        SetCustAttributeOptions(optNodes, customAttributeId, optionType, ref obj); //FB 2045
                    }
                }
                obj.outXml = "<Success>Sccess</Success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region Set Custom Attribute Options
        /// <summary>
        /// Set Custom Attribute Options
        /// </summary>
        /// <param name="node"></param>
        /// <param name="custAttrID"></param>
        /// <returns></returns>
        private bool SetCustAttributeOptions(XmlNodeList nodes, Int32 custAttrID, Int32 optionType, ref vrmDataObject obj) //FB 2045
        {
            try
            {
                int optionIdNew = 0,langOptValue = 0; //FB 1970
                String OptionID = ""; //FB 2045
                List<ICriterion> criterionList = new List<ICriterion>(); 

                if (nodes == null && custAttrID <= 0)
                    return false;

                foreach (XmlNode node in nodes)
                {
                    //FB 1970 start
                    int nodeOptValue = 1, optionLangID = 1; 
                    string OptionValue = node.SelectSingleNode("OptionValue").InnerText.Trim();
                    int.TryParse(OptionValue, out nodeOptValue);
                    
                    string Caption = node.SelectSingleNode("OptionCaption").InnerText.Trim();
                    string HelpText = node.SelectSingleNode("HelpText").InnerText.Trim();
                    
                    if (node.SelectSingleNode("LangID") != null)
                        int.TryParse(node.SelectSingleNode("LangID").InnerText.Trim(), out optionLangID);

                    //FB 2045 - Starts
                    OptionID = node.SelectSingleNode("OptionID").InnerText.Trim(); 

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("CustomAttributeID", custAttrID));
                    criterionList.Add(Expression.Eq("Caption", Caption.ToUpper()));
                    criterionList.Add(Expression.Eq("LanguageID", optionLangID));
                    
                    if (OptionID == "new")
                    {
                        IList<vrmDeptCustomAttrOption> OptionIds = m_IDeptCustOptDAO.GetByCriteria(criterionList);
                        if (OptionIds.Count > 0)
                        {
                            myvrmEx = new myVRMException(488); //Duplicate Option Name.
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2045 - End
                    //FB 1970 end
                    vrmDeptCustomAttrOption vca = new vrmDeptCustomAttrOption();
                    vca.CustomAttributeID = custAttrID;
                    vca.DeptID = 0;
                    vca.OptionType = optionType;
                    vca.OptionValue = OptionValue;
                    vca.Caption = Caption;
                    vca.HelpText = HelpText;
                    vca.LanguageID = optionLangID; //FB 1970

                    string checkoptionId = "SELECT max(vc.OptionID) FROM myVRM.DataLayer.vrmDeptCustomAttrOption vc ";
                    IList Result = m_IDeptCustOptDAO.execQuery(checkoptionId);
                    //FB 1970 start
                    if (langOptValue < nodeOptValue)
                    {
                        langOptValue = nodeOptValue;
                        optionIdNew = 0;
                        if (Result != null)
                        {
                            if (Result.Count > 0)
                            {
                                if (Result[0] != null && Result[0].ToString() != "")
                                    optionIdNew = Int32.Parse(Result[0].ToString());
                            }
                        }
                        optionIdNew = optionIdNew + 1;
                    }
                    vca.OptionID = optionIdNew;
                    //FB 1970end
                    m_IDeptCustOptDAO.Save(vca);
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("SystemException", ex);
                return false;
            }
        }
        #endregion

        /* *** Custom Attribute Fixes - end *** */
        #region Delete All Data

        public bool DeleteAllData(ref vrmDataObject obj)
        {
            bool bRet = true;
            ns_SqlHelper.SqlHelper sqlCon = null;
            String strSQL = "";
            Int32 strExec = -1;
            String path = "";
            EvtLog e_log = new EvtLog();
            try
            {
                lock (this)
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(obj.inXml);
                    XmlNode node;

                    node = xd.SelectSingleNode("//DeletePastConference/Path");
                    path = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//DeletePastConference/organizationID"); //Organization Module Fixes
                    string orgid = "";
                    if (node != null)
                        orgid = node.InnerXml.Trim();

                    if (orgid == "")//Code added for organization
                    {
                        myVRMException myVRMEx = new myVRMException(423);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    organizationID = defaultOrgId;
                    Int32.TryParse(orgid, out organizationID);



                    sqlCon = new ns_SqlHelper.SqlHelper(path);

                    sqlCon.OpenConnection();

                    sqlCon.OpenTransaction();

                    e_log.LogEvent("DeleteAllData Command Initiated Successfully.");

                    strSQL = "Delete from Acc_Balance_D where userid <> 11 And UserID in (Select UserID from Usr_List_D where companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Acc_Group_D";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    // strSQL = "Delete from Acc_Scheme_S";
                    // strExec = sqlCon.ExecuteNonQuery(strSQL);Static

                    strSQL = "Delete from Conf_AdvAVParams_D where ConfID in (Select ConfID from Conf_Conference_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Alerts_D where ConfID in (Select ConfID from Conf_Conference_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Approval_D where ConfID in (Select ConfID from Conf_Conference_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Attachments_D where ConfID in (Select ConfID from Conf_Conference_D where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Conf_Bridge_D where ConfID in (Select ConfID from Conf_Conference_D where   orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Cascade_D where ConfID in (Select ConfID from Conf_Conference_D where   orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_CustomAttr_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_CustomAttrEx_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_FoodOrder_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Group_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Location_D";// where ConfID in (Select ConfID from Conf_Conference_D where   orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Monitor_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_RecurInfo_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_ResourceOrder_D where ConfID in (Select ConfID from Conf_Conference_D where   orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Room_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_RoomSplit_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_User_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);


                    strSQL = "Delete from Conf_Conference_D where confid <> 11 and  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);
                    
                    strSQL = "Delete from Custom_HKaw_ReportTier2_D";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Dept_Approver_D";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Dept_CustomAttr_Option_D where customAttributeId in (select customAttributeId from Dept_CustomAttr_D where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Dept_CustomAttr_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Dept_List_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    //strSQL = "Delete from Email_Queue_D";
                    //strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Ept_List_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    //strSQL = "Delete from Err_Log_D";
                    //strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Grp_Detail_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Grp_Participant_D where  UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemList_AV_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemList_CA_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemList_HK_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemService_D where menuId in (Select id from Inv_Menu_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "'))";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_Menu_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_List_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_Room_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_Category_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemCharge_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_WorkItem_D where WorkOrderID in (Select ID from Inv_WorkOrder_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_WorkOrder_D where orgId='" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    //strSQL = "Delete from Ldap_BlankUser_D";
                    //strExec = sqlCon.ExecuteNonQuery(strSQL);

                    //strSQL = "Delete from Ldap_ServerConfig_D";
                    //strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Approver_D where roomid in (Select roomID from Loc_Room_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Department_D where roomid in (Select roomID from Loc_Room_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Room_D where orgId = '" + organizationID.ToString() + "' and roomid <> 11";// FB 1725
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Tier2_D where orgId = '" + organizationID.ToString() + "' and id <> 0";// FB 1725
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Tier3_D where orgId = '" + organizationID.ToString() + "' and id <> 0";// FB 1725
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_Approver_D where McuId in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_CardList_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_IPServices_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_ISDNServices_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_MPIServices_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_Tokens_D where McuId in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_List_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Sys_Approver_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Sys_LoginAudit_D";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_AdvAVParams_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_Group_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_Participant_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_Room_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_List_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_Accord_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_Dept_D where UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_GuestList_D where companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_LotusNotesPrefs_D where UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_SearchTemplates_D where UserID in (Select UserID from Usr_List_D   where  companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_TemplatePrefs_D where UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_List_D where [UserID] <> 11 And  companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "update Usr_List_D set searchID =null,endpointid = 0 where [UserID] = 11";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_Inactive_D where companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Usr_Templates_D where companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    // FB 1725 starts
                    strSQL = "update Usr_List_D set defaulttemplate =0 where UserID = 11";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from img_list_d where orgid = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "update sys_settings_d set sitelogoid = null,stdbannerid= null,highbannerid = null, LoginBackgroundId = null "; // FB 2719
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "update org_settings_d set maillogo = null,logoimageid = null,lobytopimageid = null,lobytophighimageid = null,footerimage = null,FooterMessage = null where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);


                    // FB 1725 ends

                    sqlCon.CommitTransaction();

                    sqlCon.CloseConnection();
                    e_log.LogEvent("DeleteAllData Command Completed Successfully.");

                    bRet = true;
                }
            }
            catch (myVRMException e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion

        #region GetSQLServerInfo

        public bool GetSQLServerInfo(ref vrmDataObject obj)
        {
            bool bRet = true;
            ns_SqlHelper.SqlHelper sqlCon = null;
            String strSQL = "";
            Int32 strExec = -1;
            String path = "";
            EvtLog e_log = new EvtLog();
            String outXML = "";
            String[] descAry = null;
            String strValue = "";
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//ServerInfo/Path");
                path = node.InnerXml.Trim();

                sqlCon = new ns_SqlHelper.SqlHelper(path);

                sqlCon.OpenConnection();

                strSQL = "exec sp_configure 'show advanced options', 1 reconfigure exec sp_configure 'xp_cmdshell', 1 reconfigure exec xp_cmdshell 'systeminfo' exec sp_configure 'xp_cmdshell', 0 reconfigure exec sp_configure 'show advanced options', 0 reconfigure";
                System.Data.DataSet ds = sqlCon.ExecuteDataSet(strSQL);

                outXML = "<ServerInformation>";

                Boolean isHotFix = false;
                String hotOutXML = "";
                foreach (System.Data.DataRow row in ds.Tables[0].Rows)
                {
                    if (row[0].ToString().Trim() != "")
                    {
                        if (row[0].ToString().Trim().StartsWith("Hotfix") == true || (isHotFix && row[0].ToString().Trim().StartsWith("[")))
                        {
                            hotOutXML += "<ServerInfo>";
                            strValue = "";
                            descAry = row[0].ToString().Split(':');

                            hotOutXML += "<Item>" + descAry[0] + "</Item>";
                            for (Int32 i = 1; i < descAry.Length; i++)
                                strValue += descAry[i].ToString().Trim().Replace("\t", "").Replace("\r\n", "<br>").Replace("\n", "<br>").Replace("&", "amp;");

                            hotOutXML += "<Description>" + strValue + "</Description>";
                            hotOutXML += "</ServerInfo>";

                            isHotFix = true;
                        }
                        else
                            isHotFix = false;

                        if (!isHotFix)
                            outXML += "<ServerInfo>";

                        if (row[0].ToString().IndexOf(":") > 0 && isHotFix == false)
                        {
                            strValue = "";
                            descAry = row[0].ToString().Split(':');

                            outXML += "<Item>" + descAry[0] + "</Item>";
                            for (Int32 i = 1; i < descAry.Length; i++)
                                strValue += descAry[i].ToString().Trim().Replace("\t", "").Replace("\r\n", "<br>").Replace("\n", "<br>").Replace("&", "amp;");

                            outXML += "<Description>" + strValue + "</Description>";
                        }
                        else if (isHotFix == false)
                            outXML += "<Item>" + row[0].ToString() + "</Item><Description></Description>";


                        if (!isHotFix)
                            outXML += "</ServerInfo>";
                    }
                }

                if (hotOutXML != "")
                    outXML += hotOutXML;

                outXML += "</ServerInformation>";

                obj.outXml = outXML;

                sqlCon.CloseConnection();
                e_log.LogEvent("GetSQLServerInfo Command Completed Successfully.");
                return true;

            }
            catch (myVRMException e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion

        //Image project codes start
        #region GetAllImages
        /// <summary>
        /// GetAllImages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML
        //<GetAllImages>
        //<userid></userid>
        //<organizationID></organizationID>
        //<attributetype></attributetype>
        //</GetAllImages>
        public bool GetAllImages(ref vrmDataObject obj)
        {
            myVRMException myVRMEx;
            organizationID = 0;
            int attributeType = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                obj.outXml = "";

                node = xd.SelectSingleNode("//GetAllImages/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetAllImages/attributetype");
                Int32.TryParse(node.InnerXml.Trim(), out attributeType);
                
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                criterionList.Add(Expression.Eq("AttributeType", attributeType));
                List <vrmImage> tmpImages = m_IImageDAO.GetByCriteria(criterionList);

                obj.outXml = "<GetAllImages>";
                byte[] tempImg = null;
                string tempImgStr = "";

                if (tmpImages != null)
                {
                    foreach (vrmImage imgObj in tmpImages)
                    {
                        obj.outXml += "<image>";
                        obj.outXml += "<imageid>" + imgObj.ImageId + "</imageid>";
                        tempImg = imgObj.AttributeImage;
                        tempImgStr = m_imgfactory.ConvertByteArrToBase64(tempImg); //FB 2136
                        obj.outXml += "<attributeimage>" + tempImgStr + "</attributeimage>";
                        obj.outXml += "</image>";

                        tempImg = null;
                        tempImgStr = "";
                    }
                }
                obj.outXml += "</GetAllImages>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region Get Images
        //site logo...
        public string GetImage(int imageid)
        {
            vrmImage imObj = null;
            string imgDt = "";
            try
            {
                imObj = m_IImageDAO.GetById(imageid);
                if (imObj != null)
                    imgDt = Convert.ToBase64String(imObj.AttributeImage);

                return imgDt;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException in GetRoomProfile", e);
                return "";
            }
        }
        #endregion

        #region ConvertByteArrToBase64
        /// <summary>
        /// Converting the bytearray into base64 string.
        /// </summary>
        /// <param name="imageData"></param>
        /// <returns></returns>
        internal String ConvertByteArrToBase64(Byte[] imageData)
        {
            string imageString = "";
            try
            {
                if (imageData == null)
                    return "";

                if (imageData.Length <= 0)
                    return "";

                imageString = Convert.ToBase64String(imageData);
            }
            catch (Exception e)
            {
                m_log.Error("vrmException", e);
            }
            return imageString;
        }
        #endregion

        #region ConvertBase64ToByteArray
        /// <summary>
        /// Converting base64 string into byte array
        /// </summary>
        /// <param name="ImagePath"></param>
        /// <returns></returns>
        internal byte[] ConvertBase64ToByteArray(String imageData)
        {
            byte[] bytThumb = null;
            try
            {
                if (imageData == null)
                    return null;

                if (imageData.Trim() == "")
                    return null;

                bytThumb = new Byte[imageData.Length];
                bytThumb = Convert.FromBase64String(imageData);
            }
            catch (Exception e)
            {
                m_log.Error("vrmException", e);
            }
            return bytThumb;
        }
        #endregion

        #region SetImage
        /// <summary>
        /// SetImages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML
        //<SetImage>
        //<organizationID></organizationID>
        //<image>...</image>
        //</SetImage>
        //imagetypes - jpg = "1",jpeg = "2",gif = "3",bmp = "4";
        //Attribute types
        /// Image Attribute types
        ///room = 1 / roommap1 = 2 / roommap2 =3/ roomsec1 = 4 /roomsec2 = 5
        ///roommisc =6/ roommisc2 = 7/ av = 8 / catering = 9 / hk =10
        ///banner = 11/ highresbanner = 12/ companylogo = 13
        ///maillogo = 14/ sitelogo = 15
        public bool SetImage(ref vrmDataObject obj)
        {
            myVRMException myVRMEx;
            organizationID = 0;
            int userid = 11;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                obj.outXml = "";

                node = xd.SelectSingleNode("//SetImage/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                
                byte[] tempImg = null;
                string tempImgStr = "";
                int imageid = 0;
                string attrImage = "";
                int attrType = 1; //room as default
                                
                node = xd.SelectSingleNode("//SetImage/image/attributetype");
                Int32.TryParse(node.InnerText, out attrType);
                if (attrType <= 0)
                    attrType = 1;

                node = xd.SelectSingleNode("//SetImage/image/attributeimage");
                attrImage = node.InnerText;
                tempImg = m_imgfactory.ConvertBase64ToByteArray(attrImage); //FB 2136

                if (tempImg == null)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (tempImg.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                vrmImage imObj = new vrmImage();
                imObj.AttributeImage = tempImg;
                //imObj.ImageAssignedTo = 0;  //0-image is unassigned; 1- assigned
                imObj.AttributeType = attrType;
                imObj.OrgId = organizationID;
                
                string imgQry = "SELECT max(vi.ImageId) FROM myVRM.DataLayer.vrmImage vi";
                IList Result = m_IImageDAO.execQuery(imgQry);
                imageid = 0;

                if (Result != null)
                {
                    if (Result.Count > 0)
                    {
                        if (Result[0] != null)
                        {
                            if (Result[0].ToString().Trim() != "")
                                Int32.TryParse(Result[0].ToString(), out imageid);
                        }
                    }
                }
                imageid += 1;
                imObj.ImageId = imageid;
                m_IImageDAO.Save(imObj);
                
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }

        internal bool SetImage(vrmImage imgObj, ref int imgId)
        {
            try
            {
                imgId = 0;
                m_IImageDAO.Save(imgObj);

                imgId = imgObj.ImageId; 
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region DeleteImage
        /// <summary>
        /// DeleteImage
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML
        //<DeleteImage>
        //<imageid>...</imageid>
        //</DeleteImage>
        public bool DeleteImage(ref vrmDataObject obj)
        {
            myVRMException myVRMEx;
            int imageid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                obj.outXml = "";

                node = xd.SelectSingleNode("//DeleteImage/imageid");
                string imgid = "";
                if (node != null)
                    imgid = node.InnerXml.Trim();

                Int32.TryParse(imgid, out imageid);

                vrmImage imgObj = m_IImageDAO.GetById(imageid);
                if (imgObj == null)
                {
                    myVRMEx = new myVRMException(401);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                m_IImageDAO.Delete(imgObj);

                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }

        internal bool DeleteImage(int imageid)
        {
            myVRMException myVRMEx;
            try
            {
                vrmImage imgObj = m_IImageDAO.GetById(imageid);
                if (imgObj == null)
                {
                    return false;
                }

                m_IImageDAO.Delete(imgObj);

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
        //Image project codes end

        //Methods added for FB 1830 start
        #region GetEmailLanguages
        /// <summary>
        /// GetEmailLanguages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <GetEmailLanguages><userid></userid><organizationID>11</organizationID><languageid>1</languageid></GetEmailLanguages>
        public bool GetEmailLanguages(ref vrmDataObject obj)
        {
            bool bRet = false;
            StringBuilder outXML = new StringBuilder();
            int languageid = 1;
            myVRMException myVRMEx;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetEmailLanguages/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetEmailLanguages/languageid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out languageid);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgid", organizationID));
                criterionList.Add(Expression.Eq("LanguageId", languageid));

                List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterionList);
                outXML.Append("<emaillanguages>");

                if (emailLangs != null)
                {
                    for (int k = 0; k < emailLangs.Count; k++)
                    {
                        outXML.Append("<emaillang>");
                        outXML.Append("<emaillangid>" + emailLangs[k].EmailLangId + "</emaillangid>");
                        outXML.Append("<emaillanguage>" + emailLangs[k].EmailLanguage + "</emaillanguage>");
                        outXML.Append("</emaillang>");
                    }
                }
                outXML.Append("</emaillanguages>");

                obj.outXml = outXML.ToString();
                bRet = true;
            }
            catch (myVRMException e)
            {
                m_log.Error("GetEmailTypes", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetEmailTypes - System: ", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }
        #endregion
        
        #region Get Email Types
        /// <summary>
        /// GetEmailTypes
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <GetEmailTypes><userid></userid></GetEmailTypes>
        public bool GetEmailTypes(ref vrmDataObject obj)
        {
            bool bRet = false;
            StringBuilder outXML = new StringBuilder();
            try
            {
                List<vrmEmailType> emailTypes = m_IEmailTypeDAO.GetAll();

                outXML.Append("<emailtypes>");

                if (emailTypes != null)
                {
                    for(int k=0; k<emailTypes.Count; k++)
                    {
                        outXML.Append("<mailtype>");
                        outXML.Append("<emailtypeid>" + emailTypes[k].emailid + "</emailtypeid>");
                        outXML.Append("<emailtype>" + emailTypes[k].emailtype + "</emailtype>");
                        outXML.Append("<emailcategory>" + emailTypes[k].emailcategory + "</emailcategory>");
                        outXML.Append("</mailtype>");
                    }
                }
                outXML.Append("</emailtypes>");

                obj.outXml = outXML.ToString();
                bRet = true;
            }
            catch (myVRMException e)
            {
                m_log.Error("GetEmailTypes", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetEmailTypes - System: ", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetEmailContent
        /// <summary>
        /// GetEmailContent
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <GetEmailContent>
        //<userid>11</userid>
        //<organizationID>11</organizationID>
        //<emaillang>1</emaillang>
        //<emailmode>1</emailmode>
        //<emailtype>1</emailtype>
        //</GetEmailContent>
        public bool GetEmailContent(ref vrmDataObject obj)
        {
            bool bRet = false;
            StringBuilder outXML = new StringBuilder();
            int emailMode=0, emailType=1, emailLang = 1;
            myVRMException myVRMEx;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetEmailContent/emaillangid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out emailLang);

                node = xd.SelectSingleNode("//GetEmailContent/emailtype");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out emailType);

                node = xd.SelectSingleNode("//GetEmailContent/emailmode");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out emailMode);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("EmailLangId", emailLang));
                criterionList.Add(Expression.Eq("Emailtypeid", emailType));
                criterionList.Add(Expression.Eq("EmailMode", emailMode));

                List<vrmEmailContent> emailCont = m_IEmailContentDAO.GetByCriteria(criterionList);
                
                outXML.Append("<emailcontent>");
                if (emailCont != null)
                {
                    if(emailCont.Count > 0)
                    {
                        outXML.Append("<contentid>" + emailCont[0].EmailContentId + "</contentid>");
                        outXML.Append("<emaillangid>" + emailCont[0].EmailLangId + "</emaillangid>");
                        outXML.Append("<emaillanguage>" + emailCont[0].EmailLanguage + "</emaillanguage>");
                        outXML.Append("<emailtype>" + emailCont[0].Emailtypeid  + "</emailtype>");
                        outXML.Append("<emailmode>" + emailCont[0].EmailMode + "</emailmode>");
                        outXML.Append("<subject>" + emailCont[0].EmailSubject + "</subject>");
                        outXML.Append("<placeholders>");
                        outXML.Append("<placeholderstr>" + emailCont[0].Placeholders + "</placeholderstr>");
                        outXML.Append(GetPlaceHolders(emailCont[0].Placeholders));
                        outXML.Append("</placeholders>");
                        outXML.Append("<body>" + emailCont[0].EmailBody + "</body>");
                    }
                }
                outXML.Append("</emailcontent>");

                obj.outXml = outXML.ToString();
                bRet = true;
            }
            catch (myVRMException e)
            {
                m_log.Error("GetEmailContent", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetEmailContent - System: ", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        private string GetPlaceHolders(string placeHolders)
        {
            StringBuilder placeHoldStr = new StringBuilder();
            List<int>placeHoldlst = new List<int>();
            try
            {
                if (placeHolders != null)
                {
                    if (placeHolders.Trim() != "")
                    {
                        string[] placeHolds = placeHolders.Split(',');
                        if (placeHolds != null)
                        {
                            int plholdId = 0;
                            for (int k = 0; k < placeHolds.Length; k++)
                            {
                                int.TryParse(placeHolds[k], out plholdId);

                                if (!placeHoldlst.Contains(plholdId))
                                    placeHoldlst.Add(plholdId);
                            }
                            List<ICriterion> criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.In("PlaceHolderId", placeHoldlst));
                            List<vrmEPlaceHolders> placeHoldLst = m_IEPlaceHoldersDAO.GetByCriteria(criterionList);
                            if (placeHoldLst != null)
                            {
                                for (int p = 0; p < placeHoldLst.Count; p++)
                                {
                                    placeHoldStr.Append("<placeholder>");
                                    placeHoldStr.Append("<placeholderid>" + placeHoldLst[p].PlaceHolderId + "</placeholderid>");
                                    placeHoldStr.Append("<description>" + placeHoldLst[p].Description.Trim() + "</description>");
                                    placeHoldStr.Append("</placeholder>");
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {throw e;}
            
            return placeHoldStr.ToString();
        }
        #endregion

        #region SetEmailContent
        /// <summary>
        /// SetEmailContent
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <SetEmailContent>
        //<userid>11</userid>
        //<organizationID>11</organizationID>
        //<contentid>Mail_Content_ID</contentid>
        //<emaillangid>1</emaillangid>
        //<emaillanguage>en</emaillanguage>
        //<emailmode>1</emailmode>
        //<emailtype>1</emailtype>
        //<subject>Mail_Subject</subject>
        //<body>Mail_Message</body>
        //<langOrigin>User (or) Organisation Page</langOrigin>
        //<createType>1 (or) 2</createType>
        //<placeHolders>Mail_PlaceHolders</placeHolders>
        //</SetEmailContent>
        public bool SetEmailContent(ref vrmDataObject obj)
        {
            StringBuilder outXML = new StringBuilder();
            String emailLang = "1", subject = "", body = "", langOrigin = "", placeHolders = "";
            int emailMode = 0, emailType = 1, emaillangid = 0, contentid = 0, userID = 0, createType = 0;
            myVRMException myVRMEx;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SetEmailContent/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                
                node = xd.SelectSingleNode("//SetEmailContent/userid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//SetEmailContent/langOrigin");
                if (node != null)
                    langOrigin = node.InnerText.Trim().ToString();

                node = xd.SelectSingleNode("//SetEmailContent/createType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out createType);

                node = xd.SelectSingleNode("//SetEmailContent/contentid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out contentid);   

                node = xd.SelectSingleNode("//SetEmailContent/emaillangid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out emaillangid);

                node = xd.SelectSingleNode("//SetEmailContent/emaillanguage");
                if (node != null)
                    emailLang = node.InnerText.Trim().ToString();

                node = xd.SelectSingleNode("//SetEmailContent/emailtype");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out emailType);

                node = xd.SelectSingleNode("//SetEmailContent/emailmode");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out emailMode);

                node = xd.SelectSingleNode("//SetEmailContent/subject");
                if (node != null)
                    subject = node.InnerText.Trim().ToString();

                node = xd.SelectSingleNode("//SetEmailContent/body");
                if (node != null)
                    body = node.InnerXml.Trim();

                // Href issue in Email Customization - start
                string emailBody = body;
                string orignal = "<a href=\"{36}\" >";
                int startindex;
                int lastindex = 0;
                do
                {
                    startindex = emailBody.IndexOf("<a href", lastindex);
                    if (startindex >= 0)
                    {
                        lastindex = emailBody.IndexOf(">", startindex);
                        string ahref = emailBody.Substring(startindex, (lastindex + 1) - startindex);
                        if(ahref.Contains("%7B36%7D")) //FB 2409
                            emailBody = emailBody.Replace(ahref, orignal);
                    }
                }
                while (startindex >= 0);
                body = emailBody;
                // Href issue in Email Customization - end

                node = xd.SelectSingleNode("//SetEmailContent/placeholderstr");
                if (node != null)
                    placeHolders = node.InnerText.Trim().ToString();

                vrmEmailLanguage vrmEmailLang = new vrmEmailLanguage();
                
                vrmEmailContent VrmEmailCont = new vrmEmailContent();
                VrmEmailCont.EmailBody = body;
                VrmEmailCont.EmailSubject = subject;
                VrmEmailCont.EmailMode = emailMode;
                VrmEmailCont.Emailtypeid = emailType;
                VrmEmailCont.EmailLanguage = emailLang;
                VrmEmailCont.Placeholders = placeHolders;
                obj.outXml = "<SetEmailContent>"; //ZD 100187
                if (createType == 1) //new - only one new email language can be created by a user
                {
                    vrmEmailLang.EmailLanguage = emailLang;
                    vrmEmailLang.LanguageId = emaillangid; //Base Language
                    vrmEmailLang.OrgId = organizationID;
                    m_IEmailLanguageDAO.Save(vrmEmailLang); 

                    VrmEmailCont.EmailLangId = vrmEmailLang.EmailLangId; 
                    m_IEmailContentDAO.Save(VrmEmailCont);

                    //int eContentId = VrmEmailCont.EmailContentId; FB 2409
                    
                    //Insert all emailtypes contents into Email_Content_D table for the newly created language
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("EmailLangId", emaillangid));
                    criterionList.Add(Expression.Not(Expression.Eq("EmailContentId", contentid))); //FB 2409
                    List<vrmEmailContent> emailContlist = m_IEmailContentDAO.GetByCriteria(criterionList);
                    
                    if (emailContlist != null)
                    {
                        for (int k = 0; k < emailContlist.Count; k++)
                        {
                            VrmEmailCont = new vrmEmailContent();
                            VrmEmailCont.EmailBody = emailContlist[k].EmailBody;
                            VrmEmailCont.EmailSubject = emailContlist[k].EmailSubject;
                            VrmEmailCont.EmailMode = emailContlist[k].EmailMode;
                            VrmEmailCont.Emailtypeid = emailContlist[k].Emailtypeid;
                            VrmEmailCont.EmailLanguage = emailLang;
                            VrmEmailCont.Placeholders = emailContlist[k].Placeholders;
                            VrmEmailCont.EmailLangId = vrmEmailLang.EmailLangId;
                            m_IEmailContentDAO.Save(VrmEmailCont);
                        }
                    }

                    if (langOrigin.ToLower() == "u") //User
                    {
                        if (userID > 0)
                        {
                            vrmUser user = m_IuserDAO.GetByUserId(userID);
                            user.EmailLangId = VrmEmailCont.EmailLangId;
                            m_IuserDAO.Update(user);
                            obj.outXml += "<UserEmailLangID>" + VrmEmailCont.EmailLangId + "</UserEmailLangID>"; //ZD 100187
                        }
                    }
                    else if (langOrigin.ToLower() == "c") //Company
                    {
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                        orgInfo.EmailLangId = VrmEmailCont.EmailLangId;
                        m_IOrgSettingsDAO.Update(orgInfo);
                        obj.outXml += "<OrgEmailLangID>" + VrmEmailCont.EmailLangId + "</OrgEmailLangID>"; //ZD 100187
                    }

                }
                else //Update existing email language
                {
                    VrmEmailCont.EmailLangId = emaillangid;
                    VrmEmailCont.EmailContentId = contentid;
                    m_IEmailContentDAO.Update(VrmEmailCont);
                }
                obj.outXml += "<EmailLangID>" + VrmEmailCont.EmailLangId + "</EmailLangID>"; //ZD 100183
                obj.outXml += "</SetEmailContent>"; //ZD 100187
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("SetEmailContent", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SetEmailContent - System: ", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        //FB 1830 - DeleteEmailLang start
        #region DeleteEmailLanguage
        /// <summary>
        /// DeleteEmailLanguage
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <DeleteEmailLang>
        //<userid>11</userid>
        //<organizationID>11</organizationID>
        //<emaillangid>1</emaillangid>
        //<langOrigin>User (or) Organisation Page</langOrigin>
        //</DeleteEmailLang>
        public bool DeleteEmailLanguage(ref vrmDataObject obj)
        {
            ns_SqlHelper.SqlHelper sqlCon = null;
            myVRMException myVRMEx;
            String strSQL = "", langOrigin = "";
            int emaillangid = 0, userID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//DeleteEmailLang/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//DeleteEmailLang/userid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//DeleteEmailLang/langOrigin");
                if (node != null)
                    langOrigin = node.InnerText.Trim().ToString();

                node = xd.SelectSingleNode("//DeleteEmailLang/emaillangid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out emaillangid);

                sqlCon = new ns_SqlHelper.SqlHelper(obj.ConfigPath);
                sqlCon.OpenConnection();
                sqlCon.OpenTransaction();

                strSQL = "Delete from Email_Content_D where EmailLangId ='" + emaillangid.ToString() + "'";
                sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Email_Language_D where EmailLangId ='" + emaillangid.ToString() + "'";
                sqlCon.ExecuteNonQuery(strSQL);

                if (langOrigin.ToLower() == "u") //User
                {
                    if (userID > 0)
                    {
                        strSQL = "update Usr_List_D set EmailLangId=0 where UserID ='" + userID.ToString() + "'"; //FB 2502
                        sqlCon.ExecuteNonQuery(strSQL);
                    }
                }
                else if (langOrigin.ToLower() == "o") //Company
                {
                    strSQL = "update Org_Settings_D set EmailLangId=Language where OrgId ='" + organizationID.ToString() + "'";
                    sqlCon.ExecuteNonQuery(strSQL);
                }
                sqlCon.CommitTransaction();
                sqlCon.CloseConnection();
                obj.outXml = "<Success>success</Success>";
                return true;
            }
            catch (myVRMException e)
            {
                sqlCon.RollBackTransaction();
                sqlCon.CloseConnection();
                m_log.Error("DeleteEmailLang", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                sqlCon.RollBackTransaction();
                sqlCon.CloseConnection();
                m_log.Error("DeleteEmailLang - System: ", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion
        //FB 1830 - DeleteEmailLang end

        # region GetLanguages
        /// <summary>
        /// GetLanguages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLanguages(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetLanguages/UserID");
                string userID = node.InnerXml.Trim();
                StringBuilder stroutXml = new StringBuilder();
                stroutXml.Append("<GetLanguages>");
                IList languages = vrmGen.getLanguage();
                if (languages.Count < 1)
                    throw new Exception("No data available in the Gen_Language_S table.");
                String strLang = "";
                foreach (vrmLanguage language in languages)
                {
                    if (strLang != language.Name)
                    {
                        stroutXml.Append("<Language>");
                        stroutXml.Append("<ID>" + language.Id + "</ID>");
                        stroutXml.Append("<Name>" + language.Name + "</Name>");
                        stroutXml.Append("</Language>");
                    }

                    strLang = language.Name;
                }
                stroutXml.Append("</GetLanguages>");
                obj.outXml = stroutXml.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        # endregion
        //Methods added for FB 1830 end

        //FB 1860
        #region Get  Emails BLock Status
        public bool GetEmailsBlockStatus(ref vrmDataObject obj)
        {
            bool bRet = true;
            Int32 orgID = 0;
            string orgid = "";
            String blckStatus = "0";
            DateTime blockDate = DateTime.Now;
            Int32 usrID = 0;
            string userID = "";
            String blocklvl = "";
            try
            {


                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out orgID);

                if (orgID <= 0)
                {
                    throw new Exception("Invalid Organization");
                }

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(orgID);

                node = xd.SelectSingleNode("//login/user/userID");
                if (node != null)
                    userID = node.InnerXml.Trim();

                Int32.TryParse(userID, out usrID);

                if (usrID <= 0)
                {
                    throw new Exception("Invalid User");
                }

                vrmUser Usr = m_IuserDAO.GetByUserId(usrID);

                if (orgdt.MailBlocked == 1)
                {                    
                    blckStatus = "1";
                    blockDate = orgdt.MailBlockedDate;
                    timeZone.GMTToUserPreferedTime(Usr.TimeZone, ref blockDate);
                    blocklvl = "Organization Email";
                   
                }

                if (Usr.MailBlocked == 1)
                {
                    blckStatus = "1";
                    blockDate = Usr.MailBlockedDate;
                    timeZone.GMTToUserPreferedTime(Usr.TimeZone, ref blockDate);                    
                    blocklvl = "Your Email";
                }

                obj.outXml = "<login><emailBlockStatus>" + blckStatus + "</emailBlockStatus><emailBlockDate>" + blockDate.ToString() + "</emailBlockDate><Level>"+ blocklvl +"</Level></login>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region  DeleteEmails
        /// <summary>
        /// DeleteEmails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteEmails(ref vrmDataObject obj)
        {
            vrmEmail deleteEmail = null;
            String Uid = "";
            Int32 Uuid = 0;
            //Release Email
            String ReleaseUid = "";
            Int32 ReleaseUuid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNodeList Emails = xd.SelectNodes("//DeleteEmailsList/DeleteEmail");

                if (Emails != null)
                {
                    for (Int32 emailCnt = 0; emailCnt < Emails.Count; emailCnt++)
                    {
                        XmlNode node = Emails[emailCnt];
                        Uid = "";

                        if (node.SelectSingleNode("uuid") != null)
                            Uid = node.SelectSingleNode("uuid").InnerText;

                        Int32.TryParse(Uid, out Uuid);

                        deleteEmail = m_IemailDao.GetById(Uuid);
                        if (deleteEmail != null)
                        {
                            deleteEmail.RetryCount = 25;
                            m_IemailDao.Update(deleteEmail);
                        }
                    }
                }
                //Release Email
                Emails = xd.SelectNodes("//DeleteEmailsList/ReleaseEmail");

                if (Emails != null)
                {
                    
                    XmlNode nodeRelease = null;
                    for (Int32 relemailCnt = 0; relemailCnt < Emails.Count; relemailCnt++)
                    {
                        nodeRelease = Emails[relemailCnt];
                        ReleaseUid = "";

                        if (nodeRelease.SelectSingleNode("uuid") != null)
                            ReleaseUid = nodeRelease.SelectSingleNode("uuid").InnerText;

                        Int32.TryParse(ReleaseUid, out ReleaseUuid);

                        deleteEmail = m_IemailDao.GetById(ReleaseUuid);
                        if (deleteEmail != null)
                        {
                            deleteEmail.Release = 1;
                            m_IemailDao.Update(deleteEmail);
                        }

                    }
                }

                obj.outXml = "<success>1</success>";
            }
            catch (Exception e)
            {
                m_log.Error("PasswordRequest: ", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion

        #region  SetBlockEmail
        /// <summary>
        /// SetBlockEmail
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetBlockEmail(ref vrmDataObject obj)
        {
            vrmEmail updateEmail = new vrmEmail();
            Int32 uuid = 0;
            String uid = "";
            String message = "";
            try
            {
                XmlDocument xd = new XmlDocument();

                message = GetStringInBetween("<Message>", "</Message>", obj.inXml, false, false);
                obj.inXml = obj.inXml.Replace(message, "");
                xd.LoadXml(obj.inXml);
                XmlNodeList emailsList = xd.SelectNodes("//SetBlockEmail");


                if (emailsList != null)
                {
                    for (Int32 emailCnt = 0; emailCnt < emailsList.Count; emailCnt++)
                    {
                        XmlNode node = emailsList[emailCnt];

                        uid = node.SelectSingleNode("uuid").InnerText;
                        Int32.TryParse(uid, out uuid);

                        updateEmail = m_IemailDao.GetById(uuid);

                        if (node.SelectSingleNode("emailFrom") != null)
                            updateEmail.emailFrom = node.SelectSingleNode("emailFrom").InnerText;
                        if (node.SelectSingleNode("emailTo") != null)
                            updateEmail.emailTo = node.SelectSingleNode("emailTo").InnerText;
                        if (node.SelectSingleNode("Subject") != null)
                            updateEmail.Subject = node.SelectSingleNode("Subject").InnerText;
                        if (node.SelectSingleNode("Message") != null)
                            updateEmail.Message = message.Replace("amp;", "&");

                        if (updateEmail != null)
                        {
                            m_IemailDao.Update(updateEmail);
                        }
                    }
                }

                obj.outXml = "<success>1</success>";
            }
            catch (Exception e)
            {
                m_log.Error("PasswordRequest: ", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion

        #region GetString Between

        public string GetStringInBetween(string strBegin, string strEnd, string strSource, bool includeBegin, bool includeEnd)
        {
            string result = "";
            try
            {

                int iIndexOfBegin = strSource.IndexOf(strBegin);
                if (iIndexOfBegin != -1)
                {

                    if (includeBegin)
                        iIndexOfBegin -= strBegin.Length;
                    strSource = strSource.Substring(iIndexOfBegin
                        + strBegin.Length);
                    int iEnd = strSource.IndexOf(strEnd);
                    if (iEnd != -1)
                    {
                        if (includeEnd)
                            iEnd += strEnd.Length;
                        result = strSource.Substring(0, iEnd);

                    }
                }
                else
                    result = strSource;
            }
            catch (Exception ex)
            {
                result = "";
            }
            return result;
        }

        #endregion

        //FB 1860

        //FB 2027
        # region GetFeedback
        /// <summary>
        /// GetFeedback - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetFeedback(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                int userID = 0;
                string Emailid = "";
                myVRMException myVRMEx = null;
                XmlNode node;
                StringBuilder OutXMLid = new StringBuilder();

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }


                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_IuserDAO.GetByUserId(userID);
                Emailid = user.Email.ToString();
              
                OutXMLid.Append("<userInfo>");
                OutXMLid.Append("<userEmail>" + Emailid + "</userEmail>");
                OutXMLid.Append("</userInfo>");

                obj.outXml = OutXMLid.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; 
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; 
                bRet = false;
            }

            return bRet;
        }
        # endregion

        //Method added for FB 2027
        #region FetchCustomAttrs
        /// <summary>
        /// FetchCustomAttrs
        /// </summary>
        /// <param name="usr"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        public void FetchCustomAttrs(vrmUser usr, vrmConference conf, ref StringBuilder CustAtt)
        {
            ArrayList selOptIDs = new ArrayList();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmConfAttribute> confAtts = new List<vrmConfAttribute>();
            if (orgInfo == null) //FB 2547
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
            try
            {
                if (custAttrs == null)
                {
                    m_IDeptCustDAO.clearFetch();
                    m_IDeptCustDAO.clearOrderBy();
                    //m_IDeptCustDAO.addOrderBy(Order.Asc("CustomAttributeId"));
                    m_IDeptCustDAO.addOrderBy(Order.Desc("DisplayTitle")); //FB 2501
                    criterionList.Add(Expression.Eq("orgId", organizationID.ToString())); //FB 2045
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("status", 0));
                    if (orgInfo.EnableCustomOption == "0") //FB 2547
                        criterionList.Add(Expression.Eq("CreateType", 1));
                    custAttrs = m_IDeptCustDAO.GetByCriteria(criterionList);
                }

                CustAtt.Append("<CustomAttributesList>");
                if (custAttrs != null)
                {
                    m_IDeptCustOptDAO.clearOrderBy();
                    m_IDeptCustOptDAO.addOrderBy(Order.Asc("OptionID"));

                    for (int i = 0; i < custAttrs.Count; i++)
                    {
                        String strSelVal = "";
                        m_IDeptCustOptDAO.clearFetch();
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("CustomAttributeID", custAttrs[i].CustomAttributeId));
                        criterionList.Add(Expression.Eq("LanguageID", usr.Language));
                        List<VrmCustomLanguage> custmLangTt = m_ICustomAttrLangDAO.GetByCriteria(criterionList);
                        List<vrmDeptCustomAttrOption> custOptions = m_IDeptCustOptDAO.GetByCriteria(criterionList);

                        if (custmLangTt.Count > 0)
                            custAttrs[i].DisplayTitle = custmLangTt[0].DisplayTitle;
                        //FB 2377
                        if (custAttrs[i].Type == 2 || custAttrs[i].Type == 3 || custAttrs[i].Type == 4 || custAttrs[i].Type == 5 || custAttrs[i].Type == 6 || custAttrs[i].Type == 7 || custAttrs[i].Type == 8 || custAttrs[i].Type == 10)
                        {
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("CustomAttributeId", custAttrs[i].CustomAttributeId));
                            criterionList.Add(Expression.Eq("ConfId", conf.confid));
                            criterionList.Add(Expression.Eq("InstanceId", conf.instanceid));
                            confAtts = m_IconfAttrDAO.GetByCriteria(criterionList);
                            for (int cnt = 0; cnt < confAtts.Count; cnt++)
                            {
                                if (!selOptIDs.Contains(confAtts[cnt].SelectedOptionId))
                                    selOptIDs.Add(confAtts[cnt].SelectedOptionId);
                            }
                        }
                        CustAtt.Append("<CustomAttribute>");
                        CustAtt.Append("<CustomAttributeID>" + custAttrs[i].CustomAttributeId + "</CustomAttributeID>");
                        CustAtt.Append("<Title>" + custAttrs[i].DisplayTitle + "</Title>");
                        CustAtt.Append("<Mandatory>" + custAttrs[i].Mandatory + "</Mandatory>");
                        CustAtt.Append("<Description>" + custAttrs[i].Description + "</Description>");
                        CustAtt.Append("<Type>" + custAttrs[i].Type + "</Type>");
                        CustAtt.Append("<Status>" + custAttrs[i].status + "</Status>");
                        CustAtt.Append("<IncludeInEmail>" + custAttrs[i].IncludeInEmail + "</IncludeInEmail>");
                        CustAtt.Append("<CreateType>" + custAttrs[i].CreateType + "</CreateType>");//FB 2377
                        CustAtt.Append("<LangID>" + usr.Language + "</LangID>");
                        CustAtt.Append("<IncludeInCalendar>" + custAttrs[i].IncludeInCalendar + "</IncludeInCalendar>");
                        if (confAtts.Count > 0)
                        {
                            CustAtt.Append("<Selected>1</Selected>");//FB 2377
                            if (custAttrs[i].Type == 2 || custAttrs[i].Type == 3 || custAttrs[i].Type == 4 || custAttrs[i].Type == 7 || custAttrs[i].Type == 8 || custAttrs[i].Type == 10)
                                strSelVal = confAtts[0].SelectedValue;
                        }
                        else
                            CustAtt.Append("<Selected>0</Selected>");

                        CustAtt.Append("<SelectedValue>" + strSelVal + "</SelectedValue>");
                        CustAtt.Append("<OptionList>");

                        for (int cnt = 0; cnt < custOptions.Count; cnt++)
                        {
                            CustAtt.Append("<Option>");
                            CustAtt.Append("<OptionID>" + custOptions[cnt].OptionID + "</OptionID>");
                            CustAtt.Append("<DisplayValue>" + custOptions[cnt].OptionValue + "</DisplayValue>");
                            CustAtt.Append("<DisplayCaption>" + custOptions[cnt].Caption + "</DisplayCaption>");
                            if (selOptIDs.Contains(custOptions[cnt].OptionID))
                                CustAtt.Append("<Selected>1</Selected>");
                            else
                                CustAtt.Append("<Selected>0</Selected>");

                            CustAtt.Append("<HelpText>" + custOptions[cnt].HelpText + "</HelpText>");
                            CustAtt.Append("</Option>");
                        }

                        CustAtt.Append("</OptionList>");
                        CustAtt.Append("</CustomAttribute>");
                    }
                }
                CustAtt.Append("</CustomAttributesList>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchConfCustomAttr" + ex.Message);
                throw ex;
            }
        }
        #endregion

        //ZD 100151
        #region FetchCalenderCustomAttrs
        /// <summary>
        /// FetchCalenderCustomAttrs
        /// </summary>
        /// <param name="usr"></param>
        /// <param name="conf"></param>
        /// <param name="CustAtt"></param>
        public void FetchCalenderCustomAttrs(vrmBaseUser usr, vrmConference conf, ref XmlWriter CustAtt)
        {
            ArrayList selOptIDs = new ArrayList();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmConfAttribute> confAtts = new List<vrmConfAttribute>();

            try
            {
                if (custAttrs == null)
                {
                    m_IDeptCustDAO.clearFetch();
                    m_IDeptCustDAO.clearOrderBy();
                    m_IDeptCustDAO.addOrderBy(Order.Desc("DisplayTitle")); //FB 2501
                    criterionList.Add(Expression.Eq("orgId", organizationID.ToString())); //FB 2045
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("status", 0));
                    criterionList.Add(Expression.Eq("IncludeInCalendar", 1));
                    custAttrs = m_IDeptCustDAO.GetByCriteria(criterionList);
                }

                CustAtt.WriteStartElement("CustomAttributesList");
                if (custAttrs != null)
                {
                    m_IDeptCustOptDAO.clearOrderBy();
                    m_IDeptCustOptDAO.addOrderBy(Order.Asc("OptionID"));

                    for (int i = 0; i < custAttrs.Count; i++)
                    {
                        String strSelVal = "";
                        m_IDeptCustOptDAO.clearFetch();
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("CustomAttributeID", custAttrs[i].CustomAttributeId));
                        criterionList.Add(Expression.Eq("LanguageID", usr.Language));
                        List<VrmCustomLanguage> custmLangTt = m_ICustomAttrLangDAO.GetByCriteria(criterionList);
                        List<vrmDeptCustomAttrOption> custOptions = m_IDeptCustOptDAO.GetByCriteria(criterionList);

                        if (custmLangTt.Count > 0)
                            custAttrs[i].DisplayTitle = custmLangTt[0].DisplayTitle;
                        //FB 2377
                        if (custAttrs[i].Type == 2 || custAttrs[i].Type == 3 || custAttrs[i].Type == 4 || custAttrs[i].Type == 5 || custAttrs[i].Type == 6 || custAttrs[i].Type == 7 || custAttrs[i].Type == 8 || custAttrs[i].Type == 10)
                        {
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("CustomAttributeId", custAttrs[i].CustomAttributeId));
                            criterionList.Add(Expression.Eq("ConfId", conf.confid));
                            criterionList.Add(Expression.Eq("InstanceId", conf.instanceid));
                            confAtts = m_IconfAttrDAO.GetByCriteria(criterionList);
                            for (int cnt = 0; cnt < confAtts.Count; cnt++)
                            {
                                if (!selOptIDs.Contains(confAtts[cnt].SelectedOptionId))
                                    selOptIDs.Add(confAtts[cnt].SelectedOptionId);
                            }
                        }
                        CustAtt.WriteStartElement("CustomAttribute");
                        CustAtt.WriteElementString("CustomAttributeID", custAttrs[i].CustomAttributeId.ToString());
                        CustAtt.WriteElementString("Title", custAttrs[i].DisplayTitle);
                        CustAtt.WriteElementString("Mandatory", custAttrs[i].Mandatory.ToString());
                        CustAtt.WriteElementString("Description", custAttrs[i].Description);
                        CustAtt.WriteElementString("Type", custAttrs[i].Type.ToString());
                        CustAtt.WriteElementString("Status", custAttrs[i].status.ToString());
                        CustAtt.WriteElementString("IncludeInEmail", custAttrs[i].IncludeInEmail.ToString());
                        CustAtt.WriteElementString("CreateType", custAttrs[i].CreateType.ToString());//FB 2377
                        CustAtt.WriteElementString("LangID", usr.Language.ToString());
                        CustAtt.WriteElementString("IncludeInCalendar", custAttrs[i].IncludeInCalendar.ToString());
                        if (confAtts.Count > 0)
                        {
                            CustAtt.WriteElementString("Selected", "1");//FB 2377
                            if (custAttrs[i].Type == 2 || custAttrs[i].Type == 3 || custAttrs[i].Type == 4 || custAttrs[i].Type == 7 || custAttrs[i].Type == 8 || custAttrs[i].Type == 10)
                                strSelVal = confAtts[0].SelectedValue;
                        }
                        else
                            CustAtt.WriteElementString("Selected", "0");

                        CustAtt.WriteElementString("SelectedValue", strSelVal);
                        CustAtt.WriteStartElement("OptionList");

                        for (int cnt = 0; cnt < custOptions.Count; cnt++)
                        {
                            CustAtt.WriteStartElement("Option");
                            CustAtt.WriteElementString("OptionID", custOptions[cnt].OptionID.ToString());
                            CustAtt.WriteElementString("DisplayValue", custOptions[cnt].OptionValue);
                            CustAtt.WriteElementString("DisplayCaption", custOptions[cnt].Caption);
                            if (selOptIDs.Contains(custOptions[cnt].OptionID))
                                CustAtt.WriteElementString("Selected", "1");
                            else
                                CustAtt.WriteElementString("Selected", "0");

                            CustAtt.WriteElementString("HelpText", custOptions[cnt].HelpText);
                            CustAtt.WriteEndElement();
                        }

                        CustAtt.WriteEndElement();
                        CustAtt.WriteEndElement();
                    }
                }
                CustAtt.WriteEndElement();
            }
            catch (Exception ex)
            {
                m_log.Error("FetchCalenderCustomAttrs" + ex.Message);
                throw ex;
            }
        }

        #endregion
        //ZD 100151

		 //FB 2639_GetOldConf Start

        #region FetchCustomAttrs
        /// <summary>
        /// FetchCustomAttrs
        /// </summary>
        /// <param name="usr"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        public void FetchCustomAttrs(vrmUser usr, vrmConference conf, ref XmlWriter CustAtt)
        {
            ArrayList selOptIDs = new ArrayList();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmConfAttribute> confAtts = new List<vrmConfAttribute>();
            List<VrmCustomLanguage> custmLangTt = null;
            List<vrmDeptCustomAttrOption> custOptions = null;
            String strSelVal = "";
            if (orgInfo == null) //FB 2547
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
            try
            {
                if (custAttrs == null)
                {
                    m_IDeptCustDAO.clearFetch();
                    m_IDeptCustDAO.clearOrderBy();
                    m_IDeptCustDAO.addOrderBy(Order.Desc("DisplayTitle")); //FB 2501
                    criterionList.Add(Expression.Eq("orgId", organizationID.ToString())); //FB 2045
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("status", 0));
                    if (orgInfo.EnableCustomOption == "0") //FB 2547
                        criterionList.Add(Expression.Eq("CreateType", 1));
                    custAttrs = m_IDeptCustDAO.GetByCriteria(criterionList);
                }

                CustAtt.WriteStartElement("CustomAttributesList");
                if (custAttrs != null)
                {
                    m_IDeptCustOptDAO.clearOrderBy();
                    m_IDeptCustOptDAO.addOrderBy(Order.Asc("OptionID"));

                    for (int i = 0; i < custAttrs.Count; i++)
                    {
                        strSelVal = "";
                        m_IDeptCustOptDAO.clearFetch();
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("CustomAttributeID", custAttrs[i].CustomAttributeId));
                        criterionList.Add(Expression.Eq("LanguageID", usr.Language));
                        custmLangTt = m_ICustomAttrLangDAO.GetByCriteria(criterionList);
                        custOptions = m_IDeptCustOptDAO.GetByCriteria(criterionList);

                        if (custmLangTt.Count > 0)
                            custAttrs[i].DisplayTitle = custmLangTt[0].DisplayTitle;
                        //FB 2377
                        if (custAttrs[i].Type == 2 || custAttrs[i].Type == 3 || custAttrs[i].Type == 4 || custAttrs[i].Type == 5 || custAttrs[i].Type == 6 || custAttrs[i].Type == 7 || custAttrs[i].Type == 8 || custAttrs[i].Type == 10)
                        {
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("CustomAttributeId", custAttrs[i].CustomAttributeId));
                            criterionList.Add(Expression.Eq("ConfId", conf.confid));
                            criterionList.Add(Expression.Eq("InstanceId", conf.instanceid));
                            confAtts = m_IconfAttrDAO.GetByCriteria(criterionList);
                            for (int cnt = 0; cnt < confAtts.Count; cnt++)
                            {
                                if (!selOptIDs.Contains(confAtts[cnt].SelectedOptionId))
                                    selOptIDs.Add(confAtts[cnt].SelectedOptionId);
                            }
                        }
                        CustAtt.WriteStartElement("CustomAttribute");
                        CustAtt.WriteElementString("CustomAttributeID", custAttrs[i].CustomAttributeId.ToString());
                        CustAtt.WriteElementString("Title", custAttrs[i].DisplayTitle);
                        CustAtt.WriteElementString("Mandatory", custAttrs[i].Mandatory.ToString());
                        CustAtt.WriteElementString("Description", custAttrs[i].Description);
                        CustAtt.WriteElementString("Type", custAttrs[i].Type.ToString());
                        CustAtt.WriteElementString("Status", custAttrs[i].status.ToString());
                        CustAtt.WriteElementString("IncludeInEmail", custAttrs[i].IncludeInEmail.ToString());
                        CustAtt.WriteElementString("CreateType", custAttrs[i].CreateType.ToString());
                        CustAtt.WriteElementString("LangID", usr.Language.ToString());
                        CustAtt.WriteElementString("IncludeInCalendar", custAttrs[i].IncludeInCalendar.ToString());

                        if (confAtts.Count > 0)
                        {
                            CustAtt.WriteElementString("Selected", "1");//FB 2377
                            if (custAttrs[i].Type == 2 || custAttrs[i].Type == 3 || custAttrs[i].Type == 4 || custAttrs[i].Type == 7 || custAttrs[i].Type == 8 || custAttrs[i].Type == 10)
                                strSelVal = confAtts[0].SelectedValue;
                        }
                        else
                            CustAtt.WriteElementString("Selected", "0");

                        CustAtt.WriteElementString("SelectedValue", strSelVal);
                        CustAtt.WriteStartElement("OptionList");

                        for (int cnt = 0; cnt < custOptions.Count; cnt++)
                        {
                            CustAtt.WriteStartElement("Option");
                            CustAtt.WriteElementString("OptionID", custOptions[cnt].OptionID.ToString());
                            CustAtt.WriteElementString("DisplayValue", custOptions[cnt].OptionValue);
                            CustAtt.WriteElementString("DisplayCaption", custOptions[cnt].Caption);
                            if (selOptIDs.Contains(custOptions[cnt].OptionID))
                                CustAtt.WriteElementString("Selected", "1");
                            else
                                CustAtt.WriteElementString("Selected", "0");

                            CustAtt.WriteElementString("HelpText", custOptions[cnt].HelpText);
                            CustAtt.WriteFullEndElement(); //ZD 100116
                        }

                        CustAtt.WriteFullEndElement();//ZD 100116
                        CustAtt.WriteFullEndElement();//ZD 100116
                    }
                }
                CustAtt.WriteEndElement();
            }
            catch (Exception ex)
            {
                m_log.Error("FetchConfCustomAttr" + ex.Message);
                throw ex;
            }
        }
        #endregion

        //FB 2639_GetOldConf End
        //NewLobby Start
        #region GetIconsReference
        /// <summary>
        /// GetIconReference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <GetIconReference><OrganisationID></OrganisationID><userid></userid></GetIconReference>
        public bool GetIconsReference(ref vrmDataObject obj)
        {
            StringBuilder outXML = new StringBuilder();
            try
            {
                int userID = 11;
                String usrLang = "en";
                XmlDocument xmldoc = new XmlDocument();
                List<ICriterion> critList = new List<ICriterion>();

                xmldoc.LoadXml(obj.inXml);

                if (xmldoc.SelectSingleNode("//GetIconReference/userid") != null)
                    if (xmldoc.SelectSingleNode("//GetIconReference/userid").InnerText.Trim() != "")
                        Int32.TryParse(xmldoc.SelectSingleNode("//GetIconReference/userid").InnerText.Trim(), out userID);

                if (xmldoc.SelectSingleNode("//GetIconReference/userLang") != null)
                    if (xmldoc.SelectSingleNode("//GetIconReference/userLang").InnerText.Trim() != "")
                        usrLang = xmldoc.SelectSingleNode("//GetIconReference/userLang").InnerText;


                List<vrmIconsRef> IconsRef = m_IIconsRefDAO.GetAll();
                outXML.Append("<IconsReference>");
                if (IconsRef != null)
                {
                    outXML.Append("<Icons>");
                    for (int i = 0; i < IconsRef.Count; i++)
                    {
                        outXML.Append("<Icon>");
                        outXML.Append("<userID>" + userID + "</userID>");
                        outXML.Append("<iconID>" + IconsRef[i].IconID + "</iconID>");
                        outXML.Append("<iconUri>" + IconsRef[i].IconUri + "</iconUri>");
                        outXML.Append("<defaultLabel>" + IconsRef[i].Label + "</defaultLabel>");
                        outXML.Append("<iconTarget>javascript: parent.openPage(&#39"
                                        + IconsRef[i].IconTarget + "&#39)</iconTarget>");

                        critList = new List<ICriterion>();
                        critList.Add(Expression.Eq("UserID", userID));
                        critList.Add(Expression.Eq("IconID", IconsRef[i].IconID));
                        List<vrmUserLobby> usrLobby = m_IUserLobbyDao.GetByCriteria(critList);
                        if (usrLobby.Count > 0)
                        {
                            outXML.Append("<userLabel>" + usrLobby[0].Label + "</userLabel>");
                            outXML.Append("<gridPosition>" + usrLobby[0].GridPosition + "</gridPosition>");
                        }
                        else
                        {
                            outXML.Append("<userLabel></userLabel>");
                            outXML.Append("<gridPosition>0</gridPosition>");
                        }
                        outXML.Append("</Icon>");
                    }
                    outXML.Append("</Icons>");
                }
                outXML.Append("</IconsReference>");

                obj.outXml = outXML.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("GetIconsReference: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region SetIconReference
        /// <summary>
        /// SetIconReference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <SetIconReference>
        //<OrganisationID></OrganisationID>
        //<userid></userid>
        //<Icon> (Node for Each icon - start)
        //<IconTarget></IconTarget>
        //<Label></Label>
        //<IconUri></IconUri>
        //</Icon> (Node for Each icon - end)
        //</SetIconReference>
        public bool SetIconReference(ref vrmDataObject obj)
        {
            try
            {
                List<vrmIconsRef> iconsRef = new List<vrmIconsRef>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNodeList nodes;

                nodes = xd.SelectNodes("//SetIconsRef/Icon");

                if (nodes != null)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        if (nodes[i].SelectSingleNode("IconTarget") != null)
                            iconsRef[i].IconTarget = nodes[i].SelectSingleNode("IconTarget").InnerText;

                        if (nodes[i].SelectSingleNode("Label") != null)
                            iconsRef[i].Label = nodes[i].SelectSingleNode("Label").InnerText;

                        if (nodes[i].SelectSingleNode("IconUri") != null)
                            iconsRef[i].IconUri = nodes[i].SelectSingleNode("IconUri").InnerText;
                    }

                    if (nodes.Count > 0)
                        m_IIconsRefDAO.SaveOrUpdateList(iconsRef);
                }
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("GetEmailTypes - System: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //NewLobby End

        //FB 2153
        #region Upload files from API
        /// <summary>
        /// SetImages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool SaveFiles(ref vrmDataObject obj)
        {
            myVRMException myVRMEx;
            organizationID = 0;
            int userid = 11;
            XmlDocument xd = null;
            string orgid = "";
            XmlNode node = null;
            byte[] tempImg = null;
            string tempImgStr = "";
            int imageid = 0;
            string fAppendStr = "";
            string fName = "";
            string fPath = "";
            string confUpload = "";
            Boolean isConfUpload = false;
            XmlNodeList nodeList = null;
            string completePath = "";
            try
            {
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                
                obj.outXml = "";

                node = xd.SelectSingleNode("//SaveFiles/organizationID");
               
                if (node != null)
                    orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SaveFiles/conferenceuploadpath");

                if (node != null)
                {
                    confUpload = node.InnerXml.Trim();
                    isConfUpload = true;
                }


                nodeList = xd.SelectNodes("//SaveFiles/Files/File");

                obj.outXml = "<SaveFiles><Files>";

                for (int i = 0; i < nodeList.Count; i++)
                {
                    tempImg = null;
                    tempImgStr = fAppendStr = fName = fPath= completePath = "";
                    imageid = 0;
                    node = nodeList[i];
                    imageid = i+1;
                    fPath = confUpload;

                    if (node.SelectSingleNode("FileasString") != null)
                        if (node.SelectSingleNode("FileasString").InnerText != "")
                            tempImgStr = node.SelectSingleNode("FileasString").InnerText;

                    if (tempImgStr != "")
                    {
                        if (node.SelectSingleNode("FileName") != null)
                            if (node.SelectSingleNode("FileName").InnerText != "")
                                fName = node.SelectSingleNode("FileName").InnerText;

                        if (node.SelectSingleNode("FileAppendStr") != null)
                            if (node.SelectSingleNode("FileAppendStr").InnerText != "")
                                fAppendStr = node.SelectSingleNode("FileAppendStr").InnerText;

                        if (!isConfUpload)
                        {
                            if (node.SelectSingleNode("FilePath") != null)
                                if (node.SelectSingleNode("FilePath").InnerText != "")
                                    fPath = node.SelectSingleNode("FilePath").InnerText; 
                        }

                       completePath =  AvailableFilename(fPath,fName);

                       // completePath = fPath + "\\" + fName;  //fAppendStr + "_" + 

                       tempImg = m_imgfactory.ConvertBase64ToByteArray(tempImgStr); //FB 2136

                        if (tempImg != null)
                            WriteToFile(completePath, ref tempImg);

                        obj.outXml += "<File SeqNum= \"" + imageid.ToString() + "\">" + completePath + "</File>";

 
                    }


                }
                
                 obj.outXml += "</Files></SaveFiles>";

                //obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
            }
        }

        #region WriteToFile
        internal void WriteToFile(string strPath, ref byte[] Buffer)
        {
            FileStream newFile = null;
            try
            {
                if (File.Exists(strPath))
                    File.Delete(strPath);

                // Create a file
                newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
                newFile.Dispose();
                newFile = null;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                if (newFile != null)
                {
                    newFile.Close();
                    newFile.Dispose();
                    newFile = null;
                }
            }
        }
        #endregion

        public string AvailableFilename(string directory, string fileName)
        {
            string numberPattern = " ({0})";
            string srchPattern = "*";
            string srchFiles = "";
            string[] files = null;
            string path = String.Empty;
            Int32 fileNum = 0;
            try
            {
                if (!directory.Trim().Equals(""))
                {
                    path = directory + "\\" + fileName;


                    if (!File.Exists(path))
                        return path;

                    if (Path.HasExtension(path))
                        path = path.Insert(path.LastIndexOf(Path.GetExtension(path)), numberPattern);
                    else
                        path = path + numberPattern;


                    srchFiles = fileName + srchPattern;

                    if (Path.HasExtension(fileName))
                        srchFiles = fileName.Insert(fileName.LastIndexOf(Path.GetExtension(fileName)), srchPattern);

                    files = Directory.GetFiles(directory, srchFiles);

                    if (files.Length > 0)
                    {
                        fileNum = files.Length;

                        while (File.Exists(string.Format(path, fileNum)))
                            fileNum++;

                    }

                    path = string.Format(path, fileNum);
                }

            }
            catch (Exception ex)
            {

            }

            return path;
        }

        #region getUploadFilePath
        protected string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            string[] fa = null;
            try
            {

                if (fpn.Equals(""))
                    fPath = "";
                else
                {
                    
                    fa = fpn.Split('\\');
                    if (fa.Length.Equals(0))
                        fPath = "";
                    else
                        fPath = fa[fa.Length - 1];
                }


            }
            catch (Exception ex)
            { }

            return fPath;
        }
        #endregion

        #endregion

        //FB 2045 - Starts

        #region SetEntityCode
        /// <summary>
        /// SetEntityCode
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetEntityCode(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                int customAttributeId = 0, optionType = 0;
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetEntityCode/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetEntityCode/optionType");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out optionType);

                string checkAttributeId = "SELECT CustomAttributeId FROM myVRM.DataLayer.vrmDeptCustomAttr where DisplayTitle = 'Entity Code' AND orgId = " + organizationID + " ";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);
                if (result != null)
                {
                    if (result.Count > 0)
                        if (result[0] != null)
                            int.TryParse(result[0].ToString(), out customAttributeId);
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeID", customAttributeId));

                XmlNodeList optNodes = xd.SelectNodes("//SetEntityCode/OptionList/Option");
                if (optNodes != null)
                    if (!SetCustAttributeOptions(optNodes, customAttributeId, optionType, ref obj)) //FB 2045
                        return false;
                obj.outXml = "<success>1</success>";

            }
            catch (Exception ex)
            {
                m_log.Error("SystemException", ex);
                return bRet;
            }
            return bRet;

        }
        #endregion

        #region UpdateEntityCode
        /// <summary>
        /// UpdateEntityCode
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateEntityCode(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int optionId = 0, optionLangID = 0;
                int customAttributeId = 0, optionType = 0;
                int langOptValue = 0;

                node = xd.SelectSingleNode("//UpdateEntityCode/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//UpdateEntityCode/OptionList/Option/OptionID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out optionId);
                else
                {
                    myvrmEx = new myVRMException(200);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//UpdateEntityCode/optionType");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out optionType);

                string checkAttributeId = "SELECT CustomAttributeId FROM myVRM.DataLayer.vrmDeptCustomAttr where DisplayTitle = 'Entity Code' AND orgId = " + organizationID + " ";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);
                if (result != null)
                {
                    if (result.Count > 0)
                        if (result[0] != null)
                            int.TryParse(result[0].ToString(), out customAttributeId);
                }

                XmlNodeList nodes = null;

                criterionList.Add(Expression.Eq("CustomAttributeID", customAttributeId));
                criterionList.Add(Expression.Eq("OptionID", optionId));
                List<vrmDeptCustomAttrOption> optionList = m_IDeptCustOptDAO.GetByCriteria(criterionList);
                foreach (vrmDeptCustomAttrOption optionID in optionList)
                {
                    m_IDeptCustOptDAO.Delete(optionID);
                }

                nodes = xd.SelectNodes("//UpdateEntityCode/OptionList/Option");
                if (nodes == null && customAttributeId <= 0)
                    return false;

                foreach (XmlNode Node in nodes)
                {
                    int nodeOptValue = 1;
                    int OptionID = 0;

                    string OptionId = Node.SelectSingleNode("OptionID").InnerText.Trim();
                    int.TryParse(OptionId, out OptionID);

                    string OptionValue = Node.SelectSingleNode("OptionValue").InnerText.Trim();
                    int.TryParse(OptionValue, out nodeOptValue);

                    string Caption = Node.SelectSingleNode("OptionCaption").InnerText.Trim();
                    string HelpText = Node.SelectSingleNode("HelpText").InnerText.Trim();

                    if (Node.SelectSingleNode("LangID") != null)
                        int.TryParse(Node.SelectSingleNode("LangID").InnerText.Trim(), out optionLangID);

                    criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("CustomAttributeID", customAttributeId));
                    criterionList1.Add(Expression.Eq("Caption", Caption.ToUpper()));
                    criterionList1.Add(Expression.Eq("LanguageID", optionLangID));

                    IList<vrmDeptCustomAttrOption> OptionIds = m_IDeptCustOptDAO.GetByCriteria(criterionList1);
                    if (OptionIds.Count > 0)
                    {
                        myvrmEx = new myVRMException(488); //Duplicate Option Name.
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    vrmDeptCustomAttrOption vca = new vrmDeptCustomAttrOption();
                    vca.CustomAttributeID = customAttributeId;
                    vca.OptionID = OptionID;
                    vca.DeptID = 0;
                    vca.OptionType = optionType;
                    vca.OptionValue = OptionValue;
                    vca.Caption = Caption;
                    vca.HelpText = HelpText;
                    vca.LanguageID = optionLangID; 
                    m_IDeptCustOptDAO.Save(vca);
                }

                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetEntityCode
        /// <summary>
        /// GetEntityCode
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetEntityCode(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                int CustomAttributeId = 0;
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetEntityCode/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                
                string checkAttributeId = "SELECT CustomAttributeId FROM myVRM.DataLayer.vrmDeptCustomAttr where DisplayTitle = 'Entity Code' AND orgId = " + organizationID + " ";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);
                if (result != null)
                {
                    if (result.Count > 0)
                        if (result[0] != null)
                            int.TryParse(result[0].ToString(), out CustomAttributeId);
                }

                StringBuilder outXML = new StringBuilder();

                outXML.Append("<GetEntityCode>");
                outXML.Append(FetchCustomAttributeOption(CustomAttributeId));
                outXML.Append("</GetEntityCode>");

                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;

        }
        #endregion

        #region DeleteEntityCode
        /// <summary>
        /// DeleteOptionDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteEntityCode(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            int customAttributeId = 0, optionId = 0, futurecnt = 0, pastcnt = 0;
            int Mandatory = -1;//FB 2349
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//DeleteEntityCode/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//DeleteEntityCode/delete/optionId");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out optionId);
                else
                {
                    myvrmEx = new myVRMException(200);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                //FB 2349 - Start
                string checkAttributeId = "SELECT CustomAttributeId,Mandatory FROM myVRM.DataLayer.vrmDeptCustomAttr where DisplayTitle = 'Entity Code' AND orgId = " + organizationID + " ";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);
                if (result != null)
                {
                    if (result.Count > 0)
                        foreach (object[] custom in result)
                        {
                            if (custom[0] != null)
                                int.TryParse(custom[0].ToString(), out customAttributeId);
                            if (custom[1] != null)
                                int.TryParse(custom[1].ToString(), out Mandatory);
                        }
                }
                //FB 2349 - End
                if (optionId <= 0 || customAttributeId <=0)
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                //FB 2349 - Start
                if (Mandatory == 1)
                {
                    List<ICriterion> criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("CustomAttributeID", customAttributeId));
                    criterionList1.Add(Expression.Eq("LanguageID", 1));
                    List<vrmDeptCustomAttrOption> customAttlist1 = m_IDeptCustOptDAO.GetByCriteria(criterionList1);
                    if (customAttlist1.Count <= 1)
                    {
                        myvrmEx = new myVRMException(609);//FB 2377
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2349 - End
                GetConfCountByEntityOptId(ref futurecnt, ref pastcnt, customAttributeId, optionId);

                if (futurecnt > 0 || pastcnt > 0)
                {
                    myvrmEx = new myVRMException(483);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                criterionList.Add(Expression.Eq("OptionID", optionId));
                List<vrmDeptCustomAttrOption> optionList = m_IDeptCustOptDAO.GetByCriteria(criterionList);
                if (optionList.Count > 0)
                    foreach (vrmDeptCustomAttrOption optionID in optionList)
                        m_IDeptCustOptDAO.Delete(optionID);
                else
                {
                    myvrmEx = new myVRMException(548);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;

        }
        #endregion

        #region Fetches all the conferences linked to Entity Code Options
        /// <summary>
        /// Fetches all the conferences linked to Entity Code Options
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConfListByEntityOptID(ref vrmDataObject obj)
        {
            bool bRet = true;
            int customAttributeId = 0, optionId = 0;
            int pastcnt = 0, futurecnt = 0;
            StringBuilder outXML = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetConfListByEntityOptID/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetConfListByEntityOptID/entitycode/optionID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out optionId);

                string checkAttributeId = "SELECT CustomAttributeId FROM myVRM.DataLayer.vrmDeptCustomAttr where DisplayTitle = 'Entity Code' AND orgId = " + organizationID + " ";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);
                if (result != null)
                {
                    if (result.Count > 0)
                        if (result[0] != null)
                            int.TryParse(result[0].ToString(), out customAttributeId);
                }
               
                if (customAttributeId <= 0 || optionId <= 0)
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                GetConfCountByEntityOptId(ref futurecnt, ref pastcnt, customAttributeId, optionId);

                if (pastcnt > 0 || futurecnt > 0) 
                {
                    outXML.Append("<error>");
                    outXML.Append("<conferencelist>");
                    outXML.Append("<completedConfcount>" + pastcnt + "</completedConfcount>");
                    outXML.Append("<futureConfcount>" + futurecnt + "</futureConfcount>");
                    outXML.Append("</conferencelist>");
                    outXML.Append("</error>");

                    obj.outXml = outXML.ToString();
                    return false;
                }

                obj.outXml = "<Success>1</Success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetConfCountByEntityOptId
        /// <summary>
        /// GetConfCountByEntityOptId
        /// </summary>
        /// <param name="futurecnt"></param>
        /// <param name="pastcnt"></param>
        /// <param name="customAttributeId"></param>
        /// <param name="optionId"></param>
        private void GetConfCountByEntityOptId(ref int futurecnt, ref int pastcnt, int customAttributeId, int optionId)
        {
            try
            {
                String futStmt = "SELECT DISTINCT vc.confid, vc.instanceid, vc.confdate, vc.confnumname, vc.externalname " +
                 " FROM myVRM.DataLayer.vrmConfAttribute c, myVRM.DataLayer.vrmConference vc " +
                 " WHERE vc.deleted=0 and c.CustomAttributeId = " + customAttributeId.ToString() + " and c.SelectedOptionId = " + optionId.ToString() +
                 " and c.ConfId = vc.confid and Datediff(minute,dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",confdate), getdate()) < 5";

                String pastStmt = "SELECT DISTINCT vc.confid, vc.instanceid, vc.confdate, vc.confnumname, vc.externalname " +
                     " FROM myVRM.DataLayer.vrmConfAttribute c, myVRM.DataLayer.vrmConference vc " +
                     " WHERE vc.deleted=0 and c.CustomAttributeId = " + customAttributeId.ToString() + " and c.SelectedOptionId = " + optionId.ToString() +
                     " and c.ConfId = vc.confid and Datediff(minute,dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",confdate), getdate()) > 5";

                IList futList = m_vrmConfDAO.execQuery(futStmt);
                IList pastList = m_vrmConfDAO.execQuery(pastStmt);

                futurecnt = futList.Count;
                pastcnt = pastList.Count;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }

        }
        #endregion

        #region Delete Conferences linked to Entity Code Options
        /// <summary>
        /// Delete Conferences linked to Entity Code Options
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <CustomAttribute><ID></ID></CustomAttribute>
        public bool DeleteConfEntityOptionID(ref vrmDataObject obj)
        {
            bool bRet = true;
            int customAttributeId = 0, optionId = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/entitycode/optionID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out optionId);

                string checkAttributeId = "SELECT CustomAttributeId FROM myVRM.DataLayer.vrmDeptCustomAttr where DisplayTitle = 'Entity Code' AND orgId = " + organizationID + " ";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);
                if (result != null)
                {
                    if (result.Count > 0)
                        if (result[0] != null)
                            int.TryParse(result[0].ToString(), out customAttributeId);
                }

                if (customAttributeId <= 0 || optionId <= 0)
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (!DeleteConfByEntityOptionID(customAttributeId, optionId))
                {
                    myvrmEx = new myVRMException(549);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeID", customAttributeId));
                criterionList.Add(Expression.Eq("OptionID", optionId));
                List<vrmDeptCustomAttrOption> optionList = m_IDeptCustOptDAO.GetByCriteria(criterionList);
                if (optionList.Count > 0)
                    foreach (vrmDeptCustomAttrOption optionID in optionList)
                        m_IDeptCustOptDAO.Delete(optionID);
                
                obj.outXml = "<success>1</success>";
               
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; 
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; 
                return false;
            }
            return bRet;
        }
        #endregion

        #region DeleteConfByEntityOptionID
        /// <summary>
        /// DeleteConfByEntityOptionID
        /// </summary>
        /// <param name="customAttributeId"></param>
        /// <param name="optionId"></param>
        /// <returns></returns>
        private bool DeleteConfByEntityOptionID(int customAttributeId, int optionId)
        {
            bool bRet = true;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CustomAttributeId", customAttributeId));
                criterionList.Add(Expression.Eq("SelectedOptionId", optionId));
                List<vrmConfAttribute> customAttlist = m_IconfAttrDAO.GetByCriteria(criterionList);
                criterionList = null;

                if (customAttlist != null)
                {
                    if (customAttlist.Count > 0)
                    {
                        foreach (vrmConfAttribute confAttr in customAttlist)
                        {
                            m_IconfAttrDAO.clearFetch();
                            m_IconfAttrDAO.Delete(confAttr);
                        }
                    }
                    else
                        bRet = false;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region EditConfEntityOption
        /// <summary>
        /// EditConfEntityOption
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool EditConfEntityOption(ref vrmDataObject obj)
        {
            bool bRet = true;
            int customAttributeId = 0, optionId = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/entitycode/optionID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out optionId);

                string checkAttributeId = "SELECT CustomAttributeId FROM myVRM.DataLayer.vrmDeptCustomAttr where DisplayTitle = 'Entity Code' AND orgId = " + organizationID + " ";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);
                if (result != null)
                {
                    if (result.Count > 0)
                        if (result[0] != null)
                            int.TryParse(result[0].ToString(), out customAttributeId);
                }

                if (customAttributeId <= 0 || optionId <= 0)
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (!DeleteConfByEntityOptionID(customAttributeId, optionId))
                {
                    myvrmEx = new myVRMException(549);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
        
        //FB 2045 - End

        //FB 2268
        #region HelpRequestMail
        /// <summary>
        /// iPhone Help Request Mail
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool HelpRequestMail(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                emailFactory m_Email = new emailFactory(ref obj);
                if (m_Email.PhoneHelpRequestMail(ref obj))
                    return true;
                else
                    return false;

            }
            catch (myVRMException e)
            {
                m_log.Error("HelpRequestMail", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;

        }

        #endregion

        //FB 2136 - Starts
        #region GetSecurityBadgeType
        /// <summary>
        /// GetSecurityBadgeType
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSecurityBadgeType(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXML = new StringBuilder();
            try
            {
                List<vrmSecurityBadgeType> sec_List = vrmGen.getSecurityBadgeTypes();
                outXML.Append("<GetSecurityBadgeType>");
                foreach (vrmSecurityBadgeType secTypes in sec_List)
                {
                    outXML.Append("<SecurityBadgeType>");
                    outXML.Append("<ID>" + secTypes.Id + "</ID>");
                    outXML.Append("<Name>" + secTypes.SecBadgeType + "</Name>");
                    outXML.Append("</SecurityBadgeType>");
                }
                outXML.Append("</GetSecurityBadgeType>");
                
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("GetSecurityBadgeType", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetSecurityBadgeType", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
        //FB 2136 - End

        /** FB 2392 **/
        #region UpdateManagePrivateDepartment
        public bool UpdateManagePrivateDepartment(ref int deptID, string deptName, int userID)
        {
            vrmDept dept = null;
            List<ICriterion> deptCriterionList = null;
            List<vrmDept> selectedDept = null;
            try
            {
                m_IApproverDeptDAO = m_deptDAO.GetDeptApproverDao();
                m_IdeptDAO = m_deptDAO.GetDeptDao();
                if (organizationID < 11)
                    organizationID = defaultOrgId;

                dept = new vrmDept();
                dept.departmentId = deptID;
                dept.orgId = organizationID.ToString();
                dept.departmentName = deptName;
                dept.deleted = 0;
                if (dept.departmentId == 0 || dept.departmentId != Int32.MinValue)
                {
                    deptCriterionList = new List<ICriterion>();

                    ICriterion dc = Expression.Eq("departmentName", dept.departmentName).IgnoreCase();
                    deptCriterionList.Add(Expression.Eq("orgId", dept.orgId));   //FB-1660
                    deptCriterionList.Add(dc);


                    dc = Expression.Not(Expression.Eq("departmentId", dept.departmentId));
                    deptCriterionList.Add(dc);
                    selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);

                    if (selectedDept.Count > 0)
                    {
                        deptID = selectedDept[0].departmentId;
                        return true;

                    }

                }
                m_IdeptDAO.SaveOrUpdate(dept);
                deptID = dept.departmentId;
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2693 Starts
        #region GetPCDetails
        public bool GetPCDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                StringBuilder Outxml = new StringBuilder();
                List<vrmPCDetails> ExtVMR = vrmGen.getPCTypes();

                Outxml.Append("<PCDetails>");
                for (int i = 0; i < ExtVMR.Count; i++)
                {
                    Outxml.Append("<PCDetail>");
                    Outxml.Append("<Id>" + ExtVMR[i].Id.ToString() + "</Id>");
                    Outxml.Append("<Name>" + ExtVMR[i].PCType.ToString() + "</Name>");
                    Outxml.Append("<ImgPath>" + ExtVMR[i].Absolutepath.ToString() + "</ImgPath>");
                    Outxml.Append("</PCDetail>");
                }
                Outxml.Append("</PCDetails>");

                obj.outXml = Outxml.ToString();

                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;

        }
        #endregion
        //FB 2693 Ends
    }
    #region EvtLog
    public class EvtLog
    {
        private string errMsg = null;

        public EvtLog()
        {
        }
        private bool Log(string sSource, string sLog, string sEvent)
        {
            try
            {
                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);

                EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Information);

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                return false;
            }

        }
        /// <summary>
        /// Log an event in Windows Event Logs (Not used so far)
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool LogEvent(String message)
        {
            try
            {
                // log a event in the event viewer 
                string sSource = "myVRM";
                string sLog = "myVRM";
                bool ret = this.Log(sSource, sLog, message);
                if (!ret)
                {
                    return false;
                }
                return true;
            }
            catch (Exception) { return false; }
        }
    }
    #endregion
}
