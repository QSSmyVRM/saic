//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using NHibernate;
using NHibernate.Criterion;
using log4net;
using System.DirectoryServices;
using myVRM.DataLayer;
using System.Threading;

namespace myVRM.BusinessLayer
{
    public class vrmLDAPFactory
    {
        private SystemDAO m_systemDAO;

        private static log4net.ILog m_log;
        private string m_configPath;
        private vrmDataObject m_obj;
        private userDAO m_userDAO;
        private ILDAPConfigDAO m_ILDAPConfigDAO;
        private DirectorySearcher dirSearcher;
        private SearchResultCollection m_objSearchResults;
        private DirectoryEntry dirEntry;
        private IUserDao m_IuserDao;
        private IInactiveUserDao m_IinactiveUserDao;
        private IVRMLdapUserDao m_IUsrldapDao;

        private DirectoryEntry DE;
        private myVRMLDAP LDAP;
        private myVRMDirectoryEntryCollection myVRMde;
        private Int32 _itemCnt = 0;

        private Queue m_objLldapUserList = null;
        private int m_lpdatSyncState = -1;
        System.Threading.Thread m_objLDAPSyncThreadFromLDAP = null;
        System.Threading.Thread m_objLDAPSyncThreadTOmyVRMDB = null;
        int m_iLDAPTotalUserCount = 0;
        int m_iTotalUserNotImported = 0;
        string m_strLDAPErrorInfo = null;
        private myVRMException myvrmEx; //FB 1881

        enum LDAP_SYNC_STATE
        {
            LDAP_SYNC_INITIALIZE = 0,
            LDAP_SYNC_STARTED = 1,
            LDAP_SYNC_RUNNING = 2,
            LDAP_SYNC_FINISHED = 3,
            LDAP_SYNC_ERROR = 4,
            LDAP_SYNC_NOTSTARTED = 5
        }

        public vrmLDAPFactory(ref vrmDataObject obj)
        {
            try
            {
                m_obj = obj;
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                m_systemDAO = new SystemDAO(m_configPath, m_log);
                m_userDAO = new userDAO(obj.ConfigPath, obj.log);
                m_ILDAPConfigDAO = m_systemDAO.GetLDAPConfigDao();
                dirEntry = new DirectoryEntry();
                dirSearcher = new DirectorySearcher(dirEntry);

                m_IuserDao = m_userDAO.GetUserDao();
                m_IinactiveUserDao = m_userDAO.GetInactiveUserDao();
                m_IUsrldapDao = m_userDAO.GetLDAPUserDao();
                m_objLldapUserList = new Queue();
                m_objLDAPSyncThreadFromLDAP = new Thread(new ThreadStart(SyncFroLDAPThreadProc));
                m_objLDAPSyncThreadTOmyVRMDB = new Thread(new ThreadStart(SyncTomyVRMDBThreadProc));
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_NOTSTARTED;
                m_strLDAPErrorInfo = "";
                this.LDAP = new ActiveDirectory();
            }
            catch (Exception e)
            {
                //FB 1881 start
                m_log.Error("sytemException in LDAP Factory " + e.Message);
                //obj.outXml = myVRMException.toXml("sytemException in LDAP Factory " + e.Message);
                obj.outXml = "";
                //FB 1881 end
                return;
            }
        }

        public bool BuildLDAPFormyVRM(ref vrmDataObject obj)
        {
            String outXML = "";
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                // parse the inxml 
                // get the user name and password
                string _domain = xd.SelectSingleNode("//login/domain").InnerXml.Trim();
                string _userName = xd.SelectSingleNode("//login/userName").InnerXml.Trim();
                string _password = xd.SelectSingleNode("//login/userPassword").InnerXml.Trim();


                LDAP.UserName = _userName;
                LDAP.Password = _password;
                LDAP.HostName = _domain;

                LDAP.SetNamingContext();

                DE = LDAP.GetRootObject();
                outXML += "<LDAPResult>";
                outXML += GetChildrens(DE);
                outXML += "</LDAPResult>";
                obj.outXml = outXML;
                return true;
            }
            catch (Exception ex)
            {
                //FB 1881 start
                m_log.Error(ex.Message);
                //obj.outXml = myVRMException.toXml(ex.Message);
                obj.outXml = "";
                //FB 1881 end
                return false;
            }
            
        }

        public String GetChildrens(DirectoryEntry de)
        {
            String outXML = "";
            myVRMDirectoryEntryCollection children;
            try
            {

                ArrayList arrProperty = new ArrayList();
                arrProperty.Add("givenname");
                arrProperty.Add("sn");
                arrProperty.Add("name");
                arrProperty.Add("mail");
                arrProperty.Add("cn");
                arrProperty.Add("countrycode");
                arrProperty.Add("samaccountname");
                arrProperty.Add("whenchanged");
                arrProperty.Add("whencreated");
                arrProperty.Add("telephonenumber");
                arrProperty.Add("distinguishedName");
                this.LDAP.AddChildrens(de);
                if (LDAP.Entrycoll.Count > 0)
                    children = LDAP.Entrycoll;
                else
                    children = new myVRMDirectoryEntryCollection();
                outXML += "<childrens>";
                for (int i = 0; i < children.Count; i++)
                {
                    outXML += "<children>";
                    try
                    {
                        myVRMDirectoryEntry entry = (myVRMDirectoryEntry)children[i];
                        outXML += "<tagid>" + entry.DE.NativeGuid.ToString() + "</tagid>";
                        outXML += "<parent>" + entry.DE.Parent.NativeGuid.ToString() + "</parent>";
                        outXML += "<objectClass>" + entry.ObjectClass + "</objectClass>";
                        outXML += "<objectName>" + entry.ObjectName + "</objectName>";
                        if (entry.DE.Properties.Count > 0)
                        {
                            //outXML += "<properties>";
                            foreach (String proprty in arrProperty)
                            {
                                //outXML += "<property>";
                                //outXML += "<name>" + proprty + "</name>";
                                outXML += "<" + proprty + ">" + LDAP.getProperties(entry.DE, proprty) + "</" + proprty + ">";
                                //outXML += "<value>" + LDAP.getProperties(entry.DE,proprty) + "</value>";
                                //outXML += "</property>";
                            }
                            //outXML += "</properties>";
                        }
                    }
                    catch (Exception)
                    {
                    }
                    outXML += "</children>";
                }
                outXML += "</childrens>";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return outXML;
        }

        public bool AuthenticateUser(vrmLDAPConfig ldapSettings)//(string userName,string userPassword)
        {
            string uName = "";//LDAP SYNC
            try
            {
                uName = ldapSettings.serverLogin;
                if (ldapSettings.domainPrefix != "")
                    uName = ldapSettings.domainPrefix + uName;//LDAP SYNC

                // specify the ldap server info
                string ldapPath = "LDAP://" + ldapSettings.serveraddress + ":" + ldapSettings.connPort;
                this.dirEntry.Path = ldapPath;

                //check to see if domain prefix exists
                if (ldapSettings.domainPrefix != null)
                    ldapSettings.serverLogin = ldapSettings.domainPrefix + ldapSettings.serverLogin;// prefix the domain before the username 

                // specify ldap settings
                this.dirEntry.Username = ldapSettings.serverLogin.Trim();
                this.dirEntry.Password = ldapSettings.serverPassword.Trim();
                // set auth type to "None"
                this.dirEntry.AuthenticationType = AuthenticationTypes.None;

                this.dirSearcher.Filter = ("(&" + ldapSettings.SearchFilter + "(" + ldapSettings.LoginKey + "=" + uName.Trim() + "))");//("(" + ldapSettings.LoginKey + "=" + ldapSettings.login.Trim() + ")");//LDAP SYNC
                //this.dirSearcher.Filter = ldapSettings.SearchFilter;								
                dirSearcher.PropertiesToLoad.Clear();
                this.dirSearcher.PropertiesToLoad.Add(ldapSettings.LoginKey);

                // run the search. check the result count to see there is only one record.
                // if multiple records are returned then username entered should not be considered as valid.
                //SearchResultCollection objSearchResults = this.dirSearcher.FindAll();
                SearchResult objSearchResult = this.dirSearcher.FindOne();
                if (objSearchResult == null)
                {
                    //FB 1881 start
                    //m_obj.outXml = myVRMException.toXml("Connection successful. However, no matching ldap records were found.");
                    myvrmEx = new myVRMException(505);
                    m_obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    return true;
                }
                else
                {
                    m_obj.outXml = "<success>Connection Successful.</success>";
                    return true;
                }
            }
            catch (Exception e)
            {
                //FB 1881 start
                m_log.Error(e.Message);
                //m_obj.outXml = myVRMException.toXml(e.Message);
                m_obj.outXml = "";
                //FB 1881 end
                return false;
            }
            finally
            {
                dirSearcher.Dispose();
            }
        }

        public bool AuthLdapUser(ref vrmDataObject obj)
        {
            try
            {

                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                // parse the inxml 
                // get the user name and password
                string userName = xd.SelectSingleNode("//login/userName").InnerXml.Trim();
                string userPassword = xd.SelectSingleNode("//login/userPassword").InnerXml.Trim();

                if (userName != "admin")
                {
                    vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                    ldapSettings = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);
                    if (ldapSettings.serveraddress.Trim().Length < 1)
                    {
                        //FB 1881 start
                        //obj.outXml = myVRMException.toXml("Error in fetching ldap settings from database.");
                        m_log.Error("Error in fetching ldap settings from database.");
                        obj.outXml = "";
                        //FB 1881 end
                        return false;
                    }

                    // Replace the server login/pwd with the info of the user who is trying to authenticate
                    ldapSettings.login = userName;
                    ldapSettings.serverLogin = userName;
                    ldapSettings.password = userPassword;
                    ldapSettings.serverPassword = ldapSettings.password;
                    bool ret = AuthenticateUser(ldapSettings);
                    if (!ret)
                    {
                        //FB 1881 start
                        //obj.outXml = myVRMException.toXml("Authentication Failed! No matching ldap records were found.");
                        myvrmEx = new myVRMException(506);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }
                }
                obj.outXml = "<success>1</success>";
                return true;

            }
            catch (Exception e)
            {
                m_log.Error(e.Message); //FB 1881
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
        }

       

        private bool SyncWithLDAP(bool bIsSyncCall)
        {
            try
            {
                // First start the Data import from LDAP.start the thread for Sync
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_INITIALIZE;
                m_objLDAPSyncThreadFromLDAP.Start();

                // give some time for sync thread to start working
                Thread.Sleep(2000);

                // Once th LDAP Sync thread is started then start the myVRM DB updation thread to update
                // LDAP users to myVRM DB
                m_objLDAPSyncThreadTOmyVRMDB.Start();

                // give some time for myvrm Sync thread to start working
                Thread.Sleep(2000);

                // If the call is not synchronous then return SUCCESS.
                if (bIsSyncCall == false)
                    return true;

                // Now check the status of the myVRM update thread.
                // if it is alive then wait till it finishes, if finished
                // return error or success.
                while (m_objLDAPSyncThreadTOmyVRMDB.IsAlive == true)
                {
                    Thread.Sleep(5000);
                }
                if (m_strLDAPErrorInfo.Contains("SUCCESS"))
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                m_log.Error(e.Message); //FB 1881
                //m_obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                m_obj.outXml = ""; //FB 1881
                return false;
            }
        }

        private void SyncFroLDAPThreadProc()
        {
            try
            {
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_STARTED;

                // Database settings
                vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                ldapSettings = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);
                if (ldapSettings.serveraddress.Trim().Length < 1)
                {
                    //FB 1881 end
                    //m_strLDAPErrorInfo = "Error fetching ldap settings";
                    myvrmEx = new myVRMException(502);
                    m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
                }

                ldapSettings.serverLogin = ldapSettings.login;
                //decrypt pwd
                cryptography.Crypto crypto = new cryptography.Crypto();
                ldapSettings.serverPassword = crypto.decrypt(ldapSettings.password);

                // Authenticate user 
                bool ret = AuthenticateUser(ldapSettings);
                if (!ret)
                {
                    m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
                    //FB 1881 start
                    //m_strLDAPErrorInfo = "Error authenticating ldap user."; 
                    myvrmEx = new myVRMException(503);
                    m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    //m_objLDAPSyncThreadFromLDAP.Abort();
                }
                else
                {
                    //// specify the search string
                    dirSearcher = new DirectorySearcher(dirEntry);
                    dirSearcher.PropertiesToLoad.Clear();
                    this.dirSearcher.PropertiesToLoad.Add("givenname");
                    this.dirSearcher.PropertiesToLoad.Add("sn");
                    this.dirSearcher.PropertiesToLoad.Add("name");
                    this.dirSearcher.PropertiesToLoad.Add("mail");
                    this.dirSearcher.PropertiesToLoad.Add("cn");
                    this.dirSearcher.PropertiesToLoad.Add("countrycode");
                    this.dirSearcher.PropertiesToLoad.Add("samaccountname");
                    this.dirSearcher.PropertiesToLoad.Add("whenchanged");
                    this.dirSearcher.PropertiesToLoad.Add("whencreated");
                    this.dirSearcher.PropertiesToLoad.Add("telephonenumber");
                    this.dirSearcher.PropertiesToLoad.Add("IsDeleted");
                    
                    
                    //// over-ride the search filter as we need to fetch all users and not only one user.
                    this.dirSearcher.Filter = ldapSettings.SearchFilter; //("(objectClass=person)");
                    this.dirSearcher.PageSize = 250; // 1k is the max in AD server.				
                    m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_RUNNING;
                    //// run the search and get the results in 
                    m_objSearchResults = this.dirSearcher.FindAll();
                    m_iLDAPTotalUserCount = m_objSearchResults.Count;
                    ResultPropertyCollection propCollection;

                    int iLoopCount = 0;

                    // cycle thru the search records
                    while (iLoopCount < m_iLDAPTotalUserCount) //FB 1040
                    {
                        //Release the CPU
                        Thread.Sleep(10);
                        vrmLDAPUser ldapUser = new vrmLDAPUser();
                        propCollection = m_objSearchResults[iLoopCount].Properties;
                        try
                        {
                            foreach (string key in propCollection.PropertyNames)
                            {
                                foreach (Object collection in propCollection[key])
                                {
                                    if (key == "givenname")
                                    {
                                        ldapUser.FirstName = collection.ToString();
                                        ldapUser.FirstName = ReplaceInvalidChars(ldapUser.FirstName);
                                        break;
                                    }
                                    else if (key == "sn")
                                    {
                                        ldapUser.LastName = collection.ToString();
                                        ldapUser.LastName = ReplaceInvalidChars(ldapUser.LastName);
                                        break;
                                    }
                                    else if (key == "mail")
                                    {
                                        ldapUser.Email = collection.ToString();
                                        ldapUser.Email = ReplaceInvalidChars(ldapUser.Email);
                                        break;
                                    }
                                    else if (key == "telephonenumber")
                                    {
                                        ldapUser.Telephone = collection.ToString();
                                        ldapUser.Telephone = ReplaceInvalidChars(ldapUser.Telephone);
                                        break;
                                    }
                                    else if (key == "whencreated")
                                    {
                                        ldapUser.whencreated = DateTime.Parse(collection.ToString());
                                        ldapUser.whencreated = ldapUser.whencreated;
                                        break;
                                    }
                                    else if (key == "cn")
                                    {
                                        ldapUser.Login = collection.ToString();
                                        ldapUser.Login = ReplaceInvalidChars(ldapUser.Login);
                                        break;
                                    }
                                }
                            }
                            //if (ldapUser.Email != null)
                                m_objLldapUserList.Enqueue(ldapUser);
                        }
                        catch (Exception e)
                        {
                            m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
                            //m_strLDAPErrorInfo = "Error fetching user record. Message = " + e.Message; //FB 1881
                            m_log.Error("Error fetching user record. Message = " + e.Message); //FB 1881
                            myvrmEx = new myVRMException(200);
                            m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                            // keep continuing fetching other users
                        }
                        iLoopCount++;//FB 1040
                    }
                }
             
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED;
                return;
            }
            catch (Exception e)
            {
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
                m_strLDAPErrorInfo = e.Message;
                return;
            }
            finally
            {
                // free all the resources used by the objects
                this.dirSearcher.Dispose();
                m_objSearchResults.Dispose();
            }
        }

        private void SyncTomyVRMDBThreadProc()
        {
            try
            {
                // check if the LDAP sync is running, If running then wait till the Sync Finishes
                while ((m_objLDAPSyncThreadFromLDAP.IsAlive == true)
                    && ((m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR)
                    || (m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED))
                    )
                {
                    Thread.Sleep(5000);
                    m_iTotalUserNotImported = 0;
                    if (!SyncUniqueLDAPUsersTomyVRMDB())
                    {
                        //FB 1881 start
                        //m_strLDAPErrorInfo = " Error in comparing and loading users in ldap holding area.";
                        myvrmEx = new myVRMException(504);
                        m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        continue;
                    }
                }
                if (m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED)
                {
                    m_strLDAPErrorInfo = "ERROR";
                }

                m_iLDAPTotalUserCount = 0;

                // Update the LDAP sync Time even if the sync was not successful.
                // Update the sync time if the Sync was successful. FB 649
                if (m_lpdatSyncState == (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED)
                {
                    UpdateSyncTime();
                }
                m_objLldapUserList.Clear();
                // Reset the LDAP Sync State.
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_NOTSTARTED;
            }
            catch (Exception ex)
            {
                m_log.Error("Error occured in SyncTomyVRMDBThreadProc", ex);
                m_strLDAPErrorInfo = ex.Message;
                return;
            }
        }
        private bool SyncUniqueLDAPUsersTomyVRMDBOLD()//Ldap Synchronisation
        {
            try
            {
                // check to see if the users are already there in the database.
                // if user records already exists then skip adding the user. 				
                // get all the user email list from the active,inactive,ldap tables				
                //Queue uniqueNewUserList = new Queue();
                if (m_objLldapUserList.Count < 1)
                    return true; // when there are no records in the queue, it is not an error, so returing true.

                vrmLDAPUser newUser = new vrmLDAPUser();

                bool bIsValidUser = false;
                while (m_objLldapUserList.Count > 0)
                {
                    bIsValidUser = false;

                    // Release the CPU for sometime.
                    Thread.Sleep(100);
                    newUser = (vrmLDAPUser)m_objLldapUserList.Dequeue();

                    // check if Login or email are not blank
                    if ((newUser.Login != null)
                        && (newUser.Email != null)
                        && (newUser.Login.Length > 0)
                        && (newUser.Email.Length > 0)
                        && (newUser.Email.IndexOf("@") > 0))
                        bIsValidUser = true;
                    if ((bIsValidUser) && (!DoesUserExists(newUser.Login, newUser.Email)))// check if any user exists with given login and email, if yes then dont add the user
                    {
                        if (!InsertLdapUser(newUser))// found a unique user containing a unique email & login
                        {
                            m_iTotalUserNotImported++;
                            continue;
                        }
                    }
                    else
                        m_iTotalUserNotImported++;
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Exception Occured in SyncUniqueLDAPUsersTomyVRMDB method", e);
                return false;
            }

        }

        private bool SyncUniqueLDAPUsersTomyVRMDB()//Ldap Synchronisation
        {
            try
            {
                // check to see if the users are already there in the database.
                // if user records already exists then skip adding the user. 				
                // get all the user email list from the active,inactive,ldap tables				
                //Queue uniqueNewUserList = new Queue();
                if (m_objLldapUserList.Count < 1)
                    return true; // when there are no records in the queue, it is not an error, so returing true.

                vrmLDAPUser newUser = new vrmLDAPUser();

                bool bIsValidUser = false;
                while (m_objLldapUserList.Count > 0)
                {
                    bIsValidUser = false;

                    // Release the CPU for sometime.
                    Thread.Sleep(100);
                    newUser = (vrmLDAPUser)m_objLldapUserList.Dequeue();

                    // check if Login or email are not blank
                    if ((newUser.Login != null)
                        && (newUser.Email != null)
                        && (newUser.Login.Length > 0)
                        && (newUser.Email.Length > 0)
                        && (newUser.Email.IndexOf("@") > 0))
                        bIsValidUser = true;
                    if ((bIsValidUser))
                    {
                        if (!UpdateLdapUser(newUser))// // check if any user exists with given login and email, if yes then update the user
                        {
                            m_iTotalUserNotImported++;
                            continue;
                        }
                    }
                    else
                        m_iTotalUserNotImported++;
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Exception Occured in SyncUniqueLDAPUsersTomyVRMDB method", e);
                return false;
            }

        }

        /// <summary>
        /// Synchronize with ldap server
        /// </summary>
        /// <returns></returns>
        public bool SyncWithLdap(ref vrmDataObject obj)//FB 2462
        {
            vrmLDAPConfig ldapSettings = null;
            DateTime syncTime = DateTime.Now;
            try
            {
                ldapSettings = new vrmLDAPConfig();
                ldapSettings = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);

                syncTime = DateTime.Parse(syncTime.ToShortDateString() +" "+ ldapSettings.SyncTime.ToShortTimeString());

               
                if (ldapSettings.serveraddress.Trim().Length < 1)
                {
                    return false;
                }

                if (DateTime.Now.Subtract(syncTime).TotalMinutes <= 10)
                {

                    // fetch users from ldap server
                    // SyncWithLDAP requires a parameter , if the call is synchronous then pass true
                    // otherwise pass false.
                    bool ret = SyncWithLDAP(true);
                    if (!ret)
                    {
                        obj.outXml = "<success>Operation succesful.</success>";
                        return false;
                    }

                    // Code for LDAP Sync Status CHECK
                    // End Code for LDAP Status CHECK
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateSyncTime()
        {
            try
            {
                vrmLDAPConfig updLDAPCnfg = new vrmLDAPConfig();
                updLDAPCnfg = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);
                updLDAPCnfg.SyncTime = DateTime.UtcNow;
                m_ILDAPConfigDAO.clearFetch();
                m_ILDAPConfigDAO.SaveOrUpdate(updLDAPCnfg);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in SyncUniqueLDAPUsersTomyVRMDB method", ex);
                return false;
            }

        }

        public bool InsertLdapUser(vrmLDAPUser ldapUser)
        {
            Int32 userId = 0;
            try
            {
                if (ldapUser != null)
                {
                    m_IUsrldapDao.addProjection(Projections.Max("userid"));
                    IList maxId = m_IUsrldapDao.GetObjectByCriteria(new List<ICriterion>());
                    if (maxId[0] != null)
                        userId = ((int)maxId[0]) + 1;
                    else
                        userId = 1;
                    m_ILDAPConfigDAO.clearProjection();

                    ldapUser.userid = userId;
                    if(ldapUser.whencreated == null)
                        ldapUser.whencreated = DateTime.UtcNow;
                    ldapUser.newUser = 1;
                    m_IUsrldapDao.Save(ldapUser);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in InsertLdapUser method", ex);
                return false;
            }

        }

        public bool UpdateLdapUser(vrmLDAPUser ldapUser)// For LDAP SYnchronisation
        {
            Int32 userId = 0;
            int errCount = 0;
            List<vrmUser> results = null;
            List<ICriterion> criterionList = new List<ICriterion>();
            ICriterion criterium = null;

            try
            {
                if (ldapUser != null)
                {

                    m_IuserDao = m_userDAO.GetUserDao();

                    criterium = Expression.Eq("Login", ldapUser.Login.ToUpper()).IgnoreCase();
                    if (ldapUser.Email.Trim().Length > 0)
                        criterium = Expression.Or(criterium, Expression.Like("Email", ldapUser.Email.Trim().ToUpper()).IgnoreCase());

                    criterionList.Add(criterium);
                    results = m_IuserDao.GetByCriteria(criterionList);
                    errCount = results.Count;

                    if (errCount == 1)
                    {
                        if (results[0] != null)
                        {
                            results[0].FirstName = ldapUser.FirstName;
                            results[0].LastName = ldapUser.LastName;
                            //results[0].Login = ldapUser.Login;
                            results[0].Email = ldapUser.Email;
                            results[0].Telephone = ldapUser.Telephone;

                            m_IuserDao.SaveOrUpdateList(results);
                            return true;
                        }

                    }

                }

                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in InsertLdapUser method", ex);
                return false;
            }

        }

        public bool DoesUserExists(string strLogin, string strEmailId)
        {

            try
            {
                // check the active users list
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium;
                m_IuserDao = m_userDAO.GetUserDao();
                m_IinactiveUserDao = m_userDAO.GetInactiveUserDao();
                m_IUsrldapDao = m_userDAO.GetLDAPUserDao();

                criterium = Expression.Eq("Login", strLogin.ToUpper()).IgnoreCase();
                if (strEmailId.Trim().Length > 0)
                    criterium = Expression.Or(criterium, Expression.Like("Email", strEmailId.ToUpper()).IgnoreCase());

                criterionList.Add(criterium);
                List<vrmUser> results = m_IuserDao.GetByCriteria(criterionList);
                int errCount = results.Count;
                if (errCount > 0) // If any user found with given email id or login then return true.
                    return true;

                List<vrmInactiveUser> inactiveResults = m_IinactiveUserDao.GetByCriteria(criterionList);
                errCount = inactiveResults.Count;
                if (errCount > 0) // If any user found with given email id or login then return true.
                    return true;

                List<vrmLDAPUser> LDAPResults = m_IUsrldapDao.GetByCriteria(criterionList);
                errCount = LDAPResults.Count;
                if (errCount > 0) // If any user found with given email id or login then return true.
                    return true;

                return false;
              
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Test the LDAP server connectivity.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool TestLDAPConnection(ref vrmDataObject obj)
        {
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                vrmLDAPConfig ldapSettings = new vrmLDAPConfig();

                // extract the LDAP server info from the InXML
                ldapSettings.login = xd.SelectSingleNode("//login/AccountLogin").InnerXml.Trim();
                ldapSettings.serverLogin = ldapSettings.login;
                ldapSettings.password = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                ldapSettings.serverPassword = ldapSettings.password;
                ldapSettings.serveraddress = xd.SelectSingleNode("//login/ServerAddress").InnerXml.Trim();
                ldapSettings.connPort = Int32.Parse(xd.SelectSingleNode("//login/Port").InnerXml.Trim());
                ldapSettings.sessionTimeout = Int32.Parse(xd.SelectSingleNode("//login/ConnectionTimeout").InnerXml.Trim());
                ldapSettings.LoginKey = xd.SelectSingleNode("//login/LoginKey").InnerXml.Trim();
                ldapSettings.SearchFilter = xd.SelectSingleNode("//login/SearchFilter").InnerXml.Trim();
                ldapSettings.domainPrefix = xd.SelectSingleNode("//login/LDAPPrefix").InnerXml.Trim();

                // Replace "&amp;" with "&" as ldap search filters need that. 
                ldapSettings.SearchFilter = RevertBackTheInvalidChars(ldapSettings.SearchFilter);

                // Test LDAP connection
                bool ret = AuthenticateUser(ldapSettings);
                if (!ret)
                {
                    //this.errMsg = ldap.errMsg;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                //FB 1881 start
                m_log.Error("LDAP connection failed.");
                //obj.outXml = myVRMException.toXml("LDAP connection failed.");
                obj.outXml = "";
                //FB 1881 end
                return false;
            }
        }


        /// <summary>
        /// Test the Exchange server connectivity.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool TestExchangeConnection(ref vrmDataObject obj)
        {
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                ldapSettings.login = xd.SelectSingleNode("//login/AccountLogin").InnerXml.Trim();
                ldapSettings.password = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                ldapSettings.serveraddress = xd.SelectSingleNode("//login/ServerURL").InnerXml.Trim();
                //ldapSettings.connPort = Int32.Parse(xd.SelectSingleNode("//login/Port").InnerXml.Trim());

                // test ldap connection
                bool ret = AuthenticateUser(ldapSettings);
                if (!ret)
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("LDAP connection failed.");
                    myvrmEx = new myVRMException(501);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        // Public common method 
        // TODO : Move it to another central class in future.
        public string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\'", "&apos;;");
            return (input);
        }

        // Public common method 
        // TODO : Move it to another central class in future.
        public string RevertBackTheInvalidChars(string input)
        {
            input = input.Replace("&lt;", "<");
            input = input.Replace("&gt;", ">");
            input = input.Replace("&amp;", "&");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&apos;", "\'");
            return (input);
        }


        public Boolean GetLDAPUsers(ref vrmDataObject obj)
        {
            String outXML = "";
            SearchResultCollection objSearchResults = null;
            String SearchFilter = "";
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                
                XmlNode node;

                // Database settings
                vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                ldapSettings = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);
                if (ldapSettings.serveraddress.Trim().Length < 1)
                    return false;
                
                ldapSettings.serverLogin = ldapSettings.login;
                //decrypt pwd
                cryptography.Crypto crypto = new cryptography.Crypto();
                ldapSettings.serverPassword = crypto.decrypt(ldapSettings.password);
                node = xd.SelectSingleNode("//login/searchFor");
                if (node != null)
                    SearchFilter = node.InnerText.Trim();

                if (!SearchFilter.Equals(""))
                    ldapSettings.SearchFilter = SearchFilter;

                outXML = "<GetLDAPUsers>";
                outXML += "<ServerDetails>";
                outXML += "<ServerAddress>" + ldapSettings.serveraddress.ToString() + "</ServerAddress>";
                outXML += "<Username>" + ldapSettings.login.ToString() + "</Username>";
                outXML += "<Password>" + ldapSettings.serverPassword.ToString() + "</Password>";
                outXML += "<Domain>" + ldapSettings.domainPrefix.ToString() + "</Domain>";
                outXML += "<SearchFilter>" + ldapSettings.SearchFilter.Trim() + "</SearchFilter>"; //FB 2096
                outXML += "<LoginKey>" + ldapSettings.LoginKey.Trim() + "</LoginKey>"; //FB 2096
                outXML += "<AuthenticationType>" + ldapSettings.AuthType + "</AuthenticationType>"; //FB 2993 LDAP 
                outXML += "</ServerDetails>";
                
                // Authenticate user 
                //bool ret = AuthenticateUser(ldapSettings);
                //if (!ret)
                //{
                //    outXML += "<error>";
                //    outXML += "<message>User Authentication failed.</message>";
                //    outXML += "<errorCode>200</errorCode>";
                //    outXML += "</error>";
                //}
                //else
                //{
                    
                //    //// specify the search string
                //    dirSearcher = new DirectorySearcher(dirEntry);
                //    dirSearcher.PropertiesToLoad.Clear();
                //    this.dirSearcher.PropertiesToLoad.Add("givenname");
                //    this.dirSearcher.PropertiesToLoad.Add("sn");
                //    this.dirSearcher.PropertiesToLoad.Add("name");
                //    this.dirSearcher.PropertiesToLoad.Add("mail");
                //    this.dirSearcher.PropertiesToLoad.Add("cn");
                //    this.dirSearcher.PropertiesToLoad.Add("countrycode");
                //    this.dirSearcher.PropertiesToLoad.Add("samaccountname");
                //    this.dirSearcher.PropertiesToLoad.Add("whenchanged");
                //    this.dirSearcher.PropertiesToLoad.Add("whencreated");
                //    this.dirSearcher.PropertiesToLoad.Add("telephonenumber");

                //    //if (ldapSettings.SearchFilter != "")
                //    //    ldapSettings.SearchFilter = "(&" + ldapSettings.SearchFilter + "(UserAccountControl:1.2.840.113556.1.4.803:=2))";
                //    //else
                //    //    ldapSettings.SearchFilter = "(UserAccountControl:1.2.840.113556.1.4.803:=2)";

                //    //// over-ride the search filter as we need to fetch all users and not only one user.
                //    this.dirSearcher.Filter = ldapSettings.SearchFilter; //("(objectClass=person)");
                //    this.dirSearcher.PageSize = 500; // 1k is the max in AD server.				
                //    //// run the search and get the results in 
                //    objSearchResults = this.dirSearcher.FindAll();
                //    m_iLDAPTotalUserCount = objSearchResults.Count;
                //    ResultPropertyCollection propCollection;
                //    int iLoopCount = 0;

                //    outXML += "<SearchResults>";
                //    outXML += "<LDAPUsers>";

                //    // cycle thru the search records
                //    while (iLoopCount < m_iLDAPTotalUserCount) //FB 1040
                //    {
                //        outXML += "<LDAPUserInfo>";
                //        //Release the CPU
                //        Thread.Sleep(10);
                //        propCollection = objSearchResults[iLoopCount].Properties;
                //        try
                //        {
                //            foreach (string key in propCollection.PropertyNames)
                //            {
                //                foreach (Object collection in propCollection[key])
                //                {
                //                    switch (key.Trim())
                //                    {
                //                        case "cn":
                //                        case "givenname":
                //                        case "sn":
                //                        case "name":
                //                        case "mail":
                //                        case "telephonenumber":
                //                        case "whencreated":
                //                        case "whenchanged":
                //                            outXML += "<" + key.ToString().Trim() + ">" + ReplaceInvalidChars(collection.ToString().Trim()) + "</" + key.ToString().Trim() + ">";
                //                            break;
                //                    }
                //                }
                //            }
                //        }
                //        catch (Exception e)
                //        {
                //            // keep continuing fetching other users
                //        }
                //        outXML += "</LDAPUserInfo>";
                //        iLoopCount++;
                //    }
                //    outXML += "</LDAPUsers>";
                //    outXML += "<SearchFilter>" + dirSearcher.Filter.ToString() + "</SearchFilter>";
                //    outXML += "<ResultCount>" + (iLoopCount-1).ToString() + "</ResultCount>";
                //    outXML += "</SearchResults>";
                //}
                outXML += "</GetLDAPUsers>";
                obj.outXml = outXML;
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);//FB 1881
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
            finally
            {
                // free all the resources used by the objects
                //this.dirSearcher.Dispose();
                //objSearchResults.Dispose();
            }
        }
    }

    public class myVRMLDAP
    {
        // Fields
        private string _defaultNamingContext = "";
        private string _hostname = "";
        private string _password = "";
        private string _username = "";
        private DirectoryEntry de;
        private myVRMDirectoryEntryCollection _entrycoll;
        public myVRMDirectoryEntryCollection Entrycoll
        {
            get { return _entrycoll; }
            set { _entrycoll = value; }
        }
        
        // Methods
        [Description("Returns a collection of Directory Entry Objects, that are the children of the passed object")]
        public virtual void AddChildrens(DirectoryEntry de)
        {
            try
            {
                foreach (DirectoryEntry entry in de.Children)
                {
                    try
                    {
                        myVRMDirectoryEntry myVRMDE = this.newDEitem(entry);
                        if (_entrycoll == null)
                            _entrycoll = new myVRMDirectoryEntryCollection();
                        _entrycoll.Add(myVRMDE);
                        AddChildrens(myVRMDE.DE);                        
                        continue;
                    }
                    catch
                    {
                        throw new ApplicationException("AddChildrens: Unable to add directory item to collection: " + entry.Name);
                    }
                }
            }
            catch
            {
                throw new ApplicationException("AddChildrens: Too many items in this collection");
            }
            Int32 cnt = _entrycoll.Count;
        }

        [Description("Returns the property of an Directory Object")]
        public virtual string getProperties(DirectoryEntry objADObject, string objectType)
        {
            string str = "";
            Int32 prptyCnt = 0;
            try
            {
                prptyCnt = objADObject.Properties[objectType].Count;
                if (prptyCnt > 0)
                {
                    for (int i = 0; i < prptyCnt; i++)
                    {
                        if (str == "")
                            str = objADObject.Properties[objectType][i].ToString();
                        else
                            str += "||" + objADObject.Properties[objectType][i].ToString();
                    }
                }
                return str;
                str = "";
            }
            catch
            {
                str = "";
                throw new ApplicationException("getProperties : Error getting property for : " + objectType);
            }
            return str;
        }

        [Description("Returns the root LDAP Object")]
        public virtual DirectoryEntry GetRootObject()
        {
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + this._defaultNamingContext;
            this.de.SchemaEntry.UsePropertyCache = true;
            return this.de;
        }

        [Description("Returns the root LDAP Object")]
        public virtual myVRMDirectoryEntryCollection GetDirectoryEntryCollection()
        {
            myVRMDirectoryEntryCollection deColl = new myVRMDirectoryEntryCollection();
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + this._defaultNamingContext;
            this.de.SchemaEntry.UsePropertyCache = true;
            DirectorySearcher dsr = new DirectorySearcher(this.de);
            dsr.Filter = ("(cn=User)");
            SearchResultCollection objSrchResults = dsr.FindAll();
            foreach (SearchResult srchRslt in objSrchResults)
            {
                deColl.Add(srchRslt.GetDirectoryEntry());
            }
            return deColl;
        }

        public virtual string n()
        {
            _entrycoll = new myVRMDirectoryEntryCollection();
            return "oLDAP";
        }

        private myVRMDirectoryEntry newDEitem(DirectoryEntry de)
        {
            string[] strArray = null;
            char[] separator = "=".ToCharArray();
            try
            {
                strArray = de.Name.Split(separator, 2);
            }
            catch
            {
                strArray[0] = "";
            }
            myVRMDirectoryEntry entry = new myVRMDirectoryEntry();
            entry.DE = de;
            entry.ObjectClass = strArray[0].ToLower();
            entry.ObjectName = de.Name;
            return entry;
        }

        [Description("Sets the default naming context")]
        public virtual void SetNamingContext()
        {
            this._defaultNamingContext = "";
        }

        // Properties
        [Description("Set the hostname")]
        public string HostName
        {
            set
            {
                this._hostname = value;
            }
        }

        [Description("Set the password to logon with Optional")]
        public string Password
        {
            set
            {
                this._password = value;
            }
        }

        [Description("Set the username to logon with Optional")]
        public string UserName
        {
            set
            {
                this._username = value;
            }
        }
    }

    public class myVRMDirectoryEntryCollection : ArrayList
    {
        // Methods
        public myVRMDirectoryEntryCollection()
        { }
    }

    public class myVRMDirectoryEntry
    {
        // Fields
        private DirectoryEntry _de;
        private string _objclass;
        private string _objname;

        // Properties
        public DirectoryEntry DE
        {
            get
            {
                return this._de;
            }
            set
            {
                this._de = value;
            }
        }

        public string ObjectClass
        {
            get
            {
                return this._objclass;
            }
            set
            {
                this._objclass = value;
            }
        }

        public string ObjectName
        {
            get
            {
                return this._objname;
            }
            set
            {
                this._objname = value;
            }
        }
    }

    public class NDS : myVRMLDAP
    {
        // Methods
        public override string n()
        {
            return "NDS";
        }
    }

    public class ActiveDirectory : myVRMLDAP
    {
        // Fields
        //private string _defaultNamingContext;

        // Methods
        public override string n()
        {
            return "AD";
        }

        public override void SetNamingContext()
        {
            base.SetNamingContext();
        }
    }
}
