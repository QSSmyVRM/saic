/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

///
/// This routine handle all CRUD operations 
/// (Create,Read, Update,Delete)
/// Transaction processing is used with all updates.
/// Session is closed on finally.
/// Isolation level is set to ReadCommitted, here (see documentation)
/// (later this may be set in the config file. Must test first)
/// 
namespace myVRM.DataLayer
{
    public abstract class AbstractPersistenceDao<T, IdT> : IDao<T, IdT>
    {
        private static int m_defaultPageSize = 10;
        private ITransaction m_tx;

        private static long transactionKey = 0;
        private static Hashtable transactionSessions;
        private static Hashtable transaction;
   
        private List<Order> m_OrderList = new List<Order>();
        private List<IProjection> m_Projections = new List<IProjection>();
        private List<FetchData> m_FetchData = new List<FetchData>();

        private int m_pageNo, m_pageSize;

        /// <param name="sessionFactoryConfigPath">Fully qualified path of the session factory's config file</param>
        public AbstractPersistenceDao(string sessionFactoryConfigPath)
        {
            m_pageNo   = 0;
            m_pageSize = m_defaultPageSize;
            SessionFactoryConfigPath = sessionFactoryConfigPath;
            m_tx = null;
    
            transactionKey = 0;
          //  transactionSessions 
            transaction = new Hashtable();
            if( transactionKey == 0)
            {
                transactionSessions = new Hashtable();
                transaction = new Hashtable();
                transactionKey = 1;
            }
         }
        ~AbstractPersistenceDao()
        {
            if(NHibernateSession.IsConnected)
                NHibernateSession.Disconnect();
        }

        /// <summary>
        /// Loads an instance of type T from the DB based on its ID.
        /// </summary>
        
        // right now we are not implementing locking. We can later. If should close is true then the connection
        // is closed after the lookup. NOTE: if you are implementing lazy loading then you have to keep the session
        // open. If not you have to be careful during upadates as you cannot update an object across sessions. 
        public T GetById(IdT id) { return GetById(id, false); }
        public T GetById(IdT id, bool shouldClose) {
             T entity;
            ISession session = NHibernateSession;
            try
            {
                if (!session.IsConnected)
                {
                    session.Reconnect();
                }
                entity = (T)session.Load(persitentType, id);
      
                //if (shouldLock)
                //{
                //    entity = (T)session.Load(persitentType, id, LockMode.Upgrade);
                //}
                //else
                //{
                //    entity = (T)session.Load(persitentType, id);
                //}
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if(shouldClose)
                    session.Close();
            }

            return entity;
        }
        /// <summary>
        /// Loads every instance of the requested type with no filtering.
        /// </summary>
        public List<T> GetAll() 
        {
            ICriteria criteria = NHibernateSession.CreateCriteria(persitentType);
            return ConvertToGenericList(criteria.List()) as List<T>;
        }
    
        // this is tricky and trecherous. We almost always want to see only 
        // those records that are active. and  we should always have a deleted
        // field in the database that is consistant. If you are NOT sure do 
        // not use this it may throw an exception. 
        public List<T> GetActive(){
            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.Eq("deleted", 0));   
            return GetByCriteria(criterionList);
        }
        // this will implement dept level and roles based seaches
        public IList Search(DetachedCriteria query)
        {
            return query.GetExecutableCriteria(NHibernateSession).List();  
        }
       /// <summary>
        /// Loads Order by and projections for below also sets paging variables.
        /// </summary>
        public void addOrderBy(Order orderby) { m_OrderList.Add(orderby); }
        public void addProjection(IProjection projection) { m_Projections.Add(projection); }
        public void addFetch(FetchData data) { m_FetchData.Add(data); }
        public void clearOrderBy() { m_OrderList.Clear(); }
        public void clearProjection() { m_Projections.Clear(); }
        public void clearFetch() {m_FetchData.Clear();}
        public void pageNo(int p){ m_pageNo = p;}
        public void pageSize(int p){ m_pageSize = p; }
        public int getPageSize() { return m_pageSize; }
        // this will reset everything.
        public void sledgeHammerReset()
        {
            clearOrderBy();
            clearProjection();
            clearFetch();
            m_pageNo = 0;
            m_pageSize = m_defaultPageSize;
        }
        /// <summary>
        /// Loads every instance of the requested type using the supplied <see cref="ICriterion" />.
        /// If no <see cref="ICriterion" /> is supplied, this behaves like <see cref="GetAll" />.
        /// </summary>
        public List<T> GetByCriteria(List<ICriterion> criterion) 
        {
            return GetByCriteria(criterion, false);
        }
        // this one will close the session 
        public List<T> GetByCriteria(List<ICriterion> criterion, bool shouldClose)
        {
            ISession session = NHibernateSession;
            ICriteria criteria = session.CreateCriteria(persitentType);
            foreach (FetchData data in m_FetchData)
            {
                criteria.SetFetchMode(data.name, data.type);
                if (data.alias.Length > 0)
                    criteria.CreateAlias(data.name, data.alias);
            }
            foreach (ICriterion criterium in criterion)
            {
                criteria.Add(criterium);
            }
            foreach (Order order in m_OrderList)
                criteria.AddOrder(order);

            foreach (IProjection ip in m_Projections)
                criteria.SetProjection(ip);

            if (m_pageNo > 0)
            {
                criteria.SetMaxResults(m_pageSize);
                criteria.SetFirstResult(--m_pageNo * m_pageSize);
            }

            List<T> returnList =  ConvertToGenericList(criteria.List()) as List<T>;
            if(shouldClose)
                session.Close();
            return returnList;

        }
      
        // same as above ACCEPT it returns a generic (ie not typesafe) object. 
        public IList GetObjectByCriteria(List<ICriterion> criterion)
        {
            ISession session = NHibernateSession;
            ICriteria criteria = session.CreateCriteria(persitentType);
            foreach (FetchData data in m_FetchData)
            {
                criteria.SetFetchMode(data.name, data.type);
                if (data.alias.Length > 0)
                    criteria.CreateAlias(data.name, data.alias);
            }
            foreach (ICriterion criterium in criterion)
            {
                criteria.Add(criterium);
            }
             foreach (Order order in m_OrderList)
                criteria.AddOrder(order);
            foreach( IProjection ip in m_Projections)
                criteria.SetProjection(ip);

            if (m_pageNo > 0)
            {
                criteria.SetMaxResults(m_pageSize);
                criteria.SetFirstResult(--m_pageNo * m_pageSize);
            }

            return criteria.List();
        }
     
        //
        // this will return a count of records based on criterea. You can 
        // use a multi criterea query to get both the query results and 
        // the row count in one call. I have chosen to use 2 seperarte 
        // querys as it is more flexible. 
        public long CountByCriteria(List<ICriterion> criterion)
        {
            ICriteria criteria = NHibernateSession.CreateCriteria(persitentType);

            foreach (ICriterion criterium in criterion)
            {
                criteria.Add(criterium);
            }
            criteria.SetProjection(Projections.RowCount());

            IList list = criteria.List();
            return long.Parse(list[0].ToString());
        }
     
        public IList execQuery(string query)
        {
           IQuery IQuery = NHibernateSession.CreateQuery(query);
           IQuery.SetCacheable(true);

           if (m_pageNo > 0)
           {
               IQuery.SetMaxResults(m_pageSize);
               IQuery.SetFirstResult((m_pageNo - 1) * m_pageSize);
           }
           NHibernateSession.Clear();
           return IQuery.List();
        }
     
       
        // here we must convert a non-typed list to a strongly typed (generic) list 
        public List<T> ConvertToGenericList(IList objectList)
        {
            try
            {
                ArrayList notStronglyTypedList = new ArrayList(objectList);
                return new List<T>(notStronglyTypedList.ToArray(typeof(T)) as T[]);
            }
            // NOTE: this will  fail if objectList cannot be cast to type T.
            catch( Exception e)
            {
                throw e;
            }
        }
 
        /// <summary>
        /// For entities that have assigned ID's, you must explicitly call Save to add a new one.
        /// See http://www.hibernate.org/hib_docs/reference/en/html/mapping.html#mapping-declaration-id-assigned.
        /// </summary>
        public T Save(T entity)
        {
            ISession session = NHibernateSession;
            try
            {
                if (!session.IsConnected)
                {
                    session.Reconnect();
                }                        
                m_tx = session.BeginTransaction();
                session.Save(entity);
                m_tx.Commit();
                return entity;
            }
            catch (Exception e)
            {
                if (m_tx != null)
                    m_tx.Rollback();
             
                throw e;
            }
            finally
            {
                session.Close();
            }

        }
        public T Update(T entity)
        {
            ISession session = NHibernateSession;
            try
            {
                if (!session.IsConnected)
                {
                    session.Reconnect();
                }
                m_tx = session.BeginTransaction();
                session.Update(entity);
                m_tx.Commit();
                return entity;
            }
            catch (Exception e)
            {
                if (m_tx != null)
                    m_tx.Rollback();

                throw e;
            }
            finally
            {
                session.Close();
            }

        }

        /// <summary>
        /// For entities with automatatically generated IDs, such as identity, SaveOrUpdate may 
        /// be called when saving a new entity.  SaveOrUpdate can also be called to _update_ any 
        /// entity, even if its ID is assigned.
        /// Save operations are overloaded for syncronzied transactions (see below)
        /// </summary>
        public T SaveOrUpdate(T entity)
        {
            ISession session = NHibernateSession;
            try
            {
                if (!session.IsConnected)
                {
                    session.Reconnect();
                }

                m_tx = session.BeginTransaction();

                session.SaveOrUpdate(entity);
                m_tx.Commit();

                return entity;
            }
            catch (Exception e)
            {
                if (m_tx != null)
                    m_tx.Rollback();
                throw e;
            }
            finally
            {
                session.Close();
            }
        }
        public List<T> SaveOrUpdateList(List<T> entityList)
        {
            ISession session = NHibernateSession;
            try
            {
                if (!session.IsConnected)
                {
                    session.Reconnect();
                }

                m_tx = session.BeginTransaction();
                foreach (T entity in entityList)
                    session.SaveOrUpdate(entity);
                m_tx.Commit();
                return entityList;
            }
            catch (Exception e)
            {
                if (m_tx != null)
                    m_tx.Rollback();
                throw e;
            }
            finally
            {
                session.Close();
            }
        }
   
        public void Delete(T entity) {
            ISession session = NHibernateSession;
            try
            {
                if (!session.IsConnected)
                {
                    session.Reconnect();
                }
                m_tx = session.BeginTransaction();
                session.Delete(entity);
                m_tx.Commit();
                return;
            }
            catch (Exception e)
            {
                if (m_tx != null)
                    m_tx.Rollback();
                throw e;
            }
            finally
            {
                session.Close();
            }
        }
            private ISession NHibernateSession {
            get {
                return SessionManagement.GetSession(SessionFactoryConfigPath);
            }
        }

        private Type persitentType = typeof(T);
        protected readonly string SessionFactoryConfigPath;
    }
}
