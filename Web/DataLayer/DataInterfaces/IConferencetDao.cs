/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IConferenceDAO : IDao<vrmConference, int>
    {
        vrmConference GetByConfId(int confid, int instanceId);
        List<vrmConference> GetByConfId(int confid);
        vrmConference GetByConfUId(int confUid);//Fb 2392
    }
    public interface IConfRecurDAO : IDao<vrmRecurInfo, int> { }
    public interface IConfRecurDefuntDAO : IDao<vrmRecurInfoDefunct, int> { }//FB 2218
    public interface IConfRoomDAO : IDao<vrmConfRoom, int> { }
    public interface IConfUserDAO : IDao<vrmConfUser, int> { }
    public interface IConfCascadeDAO : IDao<vrmConfCascade, int> { }
    public interface IConfApprovalDAO : IDao<vrmConfApproval, int> { }
    public interface IConfVNOCOperatorDAO : IDao<vrmConfVNOCOperator, int> { } //Fb 2670
    public interface IConfAttachmentDAO : IDao<vrmConfAttachments, int> { }
    public interface IConfAttrDAO : IDao<vrmConfAttribute, int> { } //Custom Attribute Fixes
    public interface IConfAdvAvParamsDAO : IDao<vrmConfAdvAvParams, int> { }//FB 2027
    public interface IConfMontiorDAO : IDao<vrmConfMonitor, int> { }//FB 2027 SetterminalCtrl
    public interface IConfBridgeDAO : IDao<vrmConfBridge, int> { } //FB 2566
    // conference templates
    public interface ITemplateDAO : IDao<vrmTemplate, int>
    {
    }
    public interface ITempRoomDAO : IDao<vrmTempRoom, int> { }
    //FB 2027
    public interface ITempAdvAvParamsDAO : IDao<vrmTempAdvAvParams, int> { }
    public interface ITempUserDAO : IDao<vrmTempUser, int> { }
    public interface ITempGroupDAO : IDao<vrmTempGroup, int> { }
    //FB 2027
    // work order interaces
    public interface InvCategoryDAO : IDao<InventoryCategory, int> {}
    public interface IItemListDAO : IDao<AVInventoryItemList, int> { }
    public interface IAVItemDAO : IDao<AVInventoryItemList, int> { }
    public interface ICAItemDAO : IDao<CAInventoryItemList, int> { }
    public interface IHKItemDAO : IDao<HKInventoryItemList, int> { }
    public interface InvListDAO : IDao<InventoryList, int> { }
    public interface InvRoomDAO : IDao<InventoryRoom, int> { }
    public interface InvWorkOrderDAO : IDao<WorkOrder, int> { }
    public interface InvWorkItemDAO : IDao<WorkItem, int> { }
    public interface InvDeliveryTypeDAO : IDao<DeliveryType, int> { }
    public interface InvServiceDAO : IDao<InventoryService, int> { }
    public interface InvMenuDAO : IDao<Menu, int> { }
    public interface InvMenuServiceDAO : IDao<MenuService, int> { }
    public interface InvWorkChargeDAO : IDao<WorkItemCharge, int> { }
    
    public interface IEmailDao : IDao<vrmEmail, int> { }
    //Email - FB 1830 start
    public interface IEmailTypeDao : IDao<vrmEmailType, int> { }
    public interface IEmailContentDao : IDao<vrmEmailContent, int> { }
    public interface IEmailLanguageDao : IDao<vrmEmailLanguage, int> { }
    public interface IEPlaceHoldersDao : IDao<vrmEPlaceHolders, int> { }
    //Email - FB 1830 end
    public interface IConfMessageDao : IDao<vrmConfMessage, int> { } //FB 2486
    public interface IConfSyncMCUadjustmentsDao : IDao<vrmConfSyncMCUadjustments, int> { } //FB 2441
    public interface IConfPCDao : IDao<vrmConfPC, int> { } //FB 2693
	public interface IConfOverbookApprovalDao : IDao<vrmConfOverbookApproval, int> { } //FB 2659
    public interface IESEventsDao : IDao<vrmESEvent, int> { } //ZD 100256
    public interface INWESEventsDao : IDao<vrmNWESEvent, int> { } //ZD 100256
}