/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    public interface IDao<T, IdT>
    {
     
        T GetById(IdT id) ;
        T GetById(IdT id, bool shouldLock);
             
        List<T> GetAll();

        // all of these should go in another interface
        List<T> GetActive();
        IList Search(DetachedCriteria query);

        IList execQuery(string query);

        long CountByCriteria(List<ICriterion> criterion);
 
        // orderby/projections and paging
        void addOrderBy(Order orderby);
        void addProjection(IProjection projection);
        void clearOrderBy();
        void clearProjection();
        void addFetch(FetchData data);
        void clearFetch();

        void pageNo(int p);
        void pageSize(int p);
        int  getPageSize();
        void sledgeHammerReset();
        // to here 

        List<T> GetByCriteria(List<ICriterion> criterion);
        List<T> GetByCriteria(List<ICriterion> criterion, bool shouldClose);
        IList GetObjectByCriteria(List<ICriterion> criterion);
        
        /// <summary>
        /// handle begin transaction/comit at the business layer
        /// allows for atomic transactions accorss multile updates
        /// 
  //      T Save(long key, T entity);
 //       T SaveOrUpdate(long key, T entity);
        /// <summary>
        /// all at once (fire and forget)
        /// 
        T Save(T entity);
        T Update(T entity);
        T SaveOrUpdate(T entity);
        
 //       long BeginTransaction();
  //     void Commit(long key);

        /// <summary>
        /// update a whole list at a time
        /// 
      //  List<T> SaveOrUpdateList(long key, List<T> entityList);
        List<T> SaveOrUpdateList(List<T> entityList);
     
        void Delete(T entity);

    }
}
