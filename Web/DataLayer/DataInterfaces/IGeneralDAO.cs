/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface ICountryDAO : IDao<vrmCountry, int> 
    {
        vrmCountry GetCountryById(int Id);
    }
    //FB 2671 Starts
    public interface IPublicCountryDAO : IDao<vrmPublicCountry, int>
    {
        new List<vrmPublicCountry> GetCountries();
    }

    public interface IPublicCountryStateDAO : IDao<vrmPublicCountryState, int>
    {
        new List<vrmPublicCountryState> GetCountryStates(int countryId);
    }
    public interface IPublicCountryStateCityDAO : IDao<vrmPublicCountryStateCity, int>
    {
        new List<vrmPublicCountryStateCity> GetCountryStateCities(int countryId, int stateId);
    }
    //FB 2671 Ends
    public interface IStateDAO : IDao<vrmState, int> 
    {
        vrmState GetStateById(int Id);
    }
    //FB 1830
    public interface ILanguageDAO : IDao<vrmLanguage, int>
    {
        vrmLanguage GetLanguageById(int Id);
    }
    public interface IIconsRefDAO : IDao<vrmIconsRef, int>{ }

    public interface IPCDao : IDao<vrmPCDetails, int> { } //FB 2693
}
