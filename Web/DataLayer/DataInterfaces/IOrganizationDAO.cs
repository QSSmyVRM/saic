﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IOrgSettingsDAO : IDao<OrgData, int> { OrgData GetByOrgId(int id); }
    public interface IOrgDAO : IDao<vrmOrganization, int> { }
    public interface ISysTechDAO : IDao<sysTechData, int> { sysTechData GetTechByOrgId(int id); }
    public interface IDiagnosticDAO : IDao<Diagnostics, int> { Diagnostics GetDiagnosticByOrgId(int id); }
    public interface ISysApproverDAO : IDao<sysApprover, int> { IList<sysApprover> GetSysApproversByOrgId(int id); }
    public interface IHolidaysDAO : IDao<holidays, int> { IList<holidays> GetHolidaysByOrgId(int id); } // FB 1861
    public interface IHolidaysTypeDAO : IDao<holidaysType, int> { holidaysType GetHolidayTypebyID(int id); } // FB 1861
    public interface IEmailDomainDAO : IDao<vrmEmailDomain, int> { List<vrmEmailDomain> GetEmailDomainOrgId(int id); List<vrmEmailDomain> GetActiveEmailDomainbyOrgId(int id); } //FB 2154
    public interface IOrgLicAgreementDAO : IDao<VrmOrgLicAgreement, int>  //FB 2337
    {
        List<VrmOrgLicAgreement> GetOrgLicAgreementById(int id);
        VrmOrgLicAgreement GetOrgLicAgreementByOrgId(int id); 
    }
	public interface IEM7OrgSettingsDAO : IDao<vrmEM7OrgSilo, int> { IList<vrmEM7OrgSilo> GetByEM7OrgId(int id);} //FB 2501 EM7
    //FB 2599 Start
    public interface IVrmVidyoSettingsDAO : IDao<VrmVidyoSettings, int> 
    {
        VrmVidyoSettings GetVidyoSettingsByOrgId(int id); 
    }
    //FB 2262 
    //FB 2599 End
}
