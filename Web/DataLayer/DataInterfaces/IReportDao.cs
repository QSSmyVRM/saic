/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IReportScheduleDao : IDao<ReportSchedule, int>
    {
        List<ReportSchedule> GetSubmitBetween(DateTime startDate, DateTime endDate);
    }
    public interface IReportTemplateDao : IDao<ReportTemplate, int>
    {
       // List<ReportTemplate> GetSubmitBetween(DateTime startDate, DateTime endDate);
    }
    public interface IReportInputItemsDao : IDao<ReportInputItem, int>{};
    // FB 2343 Start
    public interface IReportMonthlyDaysDAO : IDao<ReportMonthlyDays, int>{}; 
    public interface IReportWeeklyDaysDAO : IDao<ReportWeeklyDays, int> { };
    // FB 2343 End
	//FB 2410 - Start
    public interface IBatchReportSettingsDAO : IDao<BatchReportSettings, int> {};
    public interface IBatchReportDatesDAO : IDao<BatchReportDates, int> {};
	//FB 2410 - End
}
