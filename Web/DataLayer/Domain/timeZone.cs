/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using NHibernate;
using NHibernate.Criterion;

using log4net;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description for TimeZone.
	/// </summary>
	public class timeZoneData
	{
		#region Private Internal Members
	
		private int m_TimeZoneID;

  
		private string m_StandardName, m_TimeZone, m_TimeZoneDiff;
        private float m_Offset, m_sortOrder; 
		private int m_Bias, m_StandardBias, m_DaylightBias, m_systemID;
		private int m_swYear, m_swMonth, m_swDayOfWeek, m_swDay, m_swHour, m_swMinute, m_swSecond;
		private int m_swMilliseconds, m_dwYear, m_dwMonth, m_dwDayOfWeek, m_dwDay, m_dwHour;
		private int m_dwMinute, m_dwSecond, m_dwMilliseconds, m_DST;
		private DateTime m_CurDSTstart,	m_CurDSTend, m_CurDSTstart1, m_CurDSTend1, m_CurDSTstart2, m_CurDSTend2, m_CurDSTstart3;
		private DateTime m_CurDSTend3, m_CurDSTstart4, m_CurDSTend4, m_CurDSTstart5, m_CurDSTend5, m_CurDSTstart6; 
		private DateTime m_CurDSTend6, m_CurDSTstart7, m_CurDSTend7, m_CurDSTstart8, m_CurDSTend8, m_CurDSTstart9, m_CurDSTend9;
            
        #endregion

		#region Public Properties

		public int TimeZoneID
		{
			get {return m_TimeZoneID;}
			set {m_TimeZoneID = value;}
		}
		public string StandardName
		{
			get {return m_StandardName;}
			set {m_StandardName = value;}
		}	
		public string TimeZone
		{
			get {return m_TimeZone;}
			set {m_TimeZone = value;}
		}	
		public string TimeZoneDiff
		{
			get {return m_TimeZoneDiff;}
			set {m_TimeZoneDiff = value;}	
		}	
		public float Offset
		{
			get {return m_Offset;}
			set {m_Offset = value;}	
		}
        public float sortOrder
        {
            get { return m_sortOrder; }
            set { m_sortOrder = value; }
        }
        public int Bias 
		{
			get {return m_Bias ;}
			set {m_Bias  = value;}
		}
		public int StandardBias 
		{
			get {return m_StandardBias;}
			set {m_StandardBias = value;}
		}
		public int DaylightBias 
		{
			get {return m_DaylightBias;}
			set {m_DaylightBias = value;}
		}
		public int systemID
		{
			get {return m_systemID;}
			set {m_systemID = value;}
		}

		public int swYear 
		{
			get {return m_swYear;}
			set {m_swYear = value;}
		}
		public int swMonth
		{
			get {return m_swMonth;}
			set {m_swMonth = value;}
		}
		public int swDayOfWeek 
		{
			get {return m_swDayOfWeek;}
			set {m_swDayOfWeek = value;}
		}
		public int swDay 
		{
			get {return m_swDay;}
			set {m_swDay = value;}
		}
		public int swHour 
		{
			get {return m_swHour;}
			set {m_swHour = value;}
		}
		public int swMinute 
		{
			get {return m_swMinute;}
			set {m_swMinute = value;}
		}
		public int swSecond
		{
			get {return m_swSecond;}
			set {m_swSecond = value;}
		}
		public int swMilliseconds 
		{
			get {return m_swMilliseconds;}
			set {m_swMilliseconds = value;}
		}
		public int dwYear 
		{
			get {return m_dwYear;}
			set {m_dwYear = value;}
		}
		public int dwMonth 
		{
			get {return m_dwMonth;}
			set {m_dwMonth = value;}
		}
		public int dwDayOfWeek 
		{
			get {return m_dwDayOfWeek;}
			set {m_dwDayOfWeek = value;}
		}
		public int dwDay 
		{
			get {return m_dwDay;}
			set {m_dwDay = value;}
		}
		public int dwHour
		{
			get {return m_dwHour;}
			set {m_dwHour = value;}
		}
		public int dwMinute 
		{
			get {return m_dwMinute;}
			set {m_dwMinute = value;}
		}
		public int dwSecond 
		{
			get {return m_dwSecond;}
			set {m_dwSecond = value;}
		}
		public int dwMilliseconds 
		{
			get {return m_dwMilliseconds;}
			set {m_dwMilliseconds = value;}
		}
		public int DST
		{
			get {return m_DST;}
			set {m_DST = value;}
		}
		public DateTime CurDSTstart 
		{
			get {return m_CurDSTstart;}
			set {m_CurDSTstart = value;}
		}
		public DateTime CurDSTend 
		{
			get {return m_CurDSTend;}
			set {m_CurDSTend = value;}
		}
		public DateTime CurDSTstart1 
		{
			get {return m_CurDSTstart1;}
			set {m_CurDSTstart1 = value;}
		}
		public DateTime CurDSTend1 
		{
			get {return m_CurDSTend1;}
			set {m_CurDSTend1 = value;}
		}
		public DateTime CurDSTstart2 
		{
			get {return m_CurDSTstart2;}
			set {m_CurDSTstart2 = value;}
		}
		public DateTime CurDSTend2 
		{
			get {return m_CurDSTend2;}
			set {m_CurDSTend2 = value;}
		}
		public DateTime CurDSTstart3
		{
			get {return m_CurDSTstart3;}
			set {m_CurDSTstart3 = value;}
		}
		public DateTime CurDSTend3 
		{
			get {return m_CurDSTend3;}
			set {m_CurDSTend3 = value;}
		}
		public DateTime CurDSTstart4 
		{
			get {return m_CurDSTstart4;}
			set {m_CurDSTstart4 = value;}
		}
		public DateTime CurDSTend4 
		{
			get {return m_CurDSTend4;}
			set {m_CurDSTend4 = value;}
		}
		public DateTime CurDSTstart5 
		{
			get {return m_CurDSTstart5;}
			set {m_CurDSTstart5 = value;}
		}
		public DateTime CurDSTend5 
		{
			get {return m_CurDSTend5;}
			set {m_CurDSTend5 = value;}
		}
		public DateTime CurDSTstart6 
		{
			get {return m_CurDSTstart6;}
			set {m_CurDSTstart6 = value;}
		}
		public DateTime CurDSTend6 
		{
			get {return m_CurDSTend6;}
			set {m_CurDSTend6 = value;}
		}
		public DateTime CurDSTstart7 
		{
			get {return m_CurDSTstart7;}
			set {m_CurDSTstart7 = value;}
		}
		public DateTime CurDSTend7 
		{
			get {return m_CurDSTend7;}
			set {m_CurDSTend7 = value;}
		}
		public DateTime CurDSTstart8 
		{
			get {return m_CurDSTstart8;}
			set {m_CurDSTstart8 = value;}
		}
		public DateTime CurDSTend8 
		{
			get {return m_CurDSTend8;}
			set {m_CurDSTend8 = value;}
		}
		public DateTime CurDSTstart9 
		{
			get {return m_CurDSTstart9;}
			set {m_CurDSTstart9 = value;}
		}
		public DateTime CurDSTend9
		{
			get {return m_CurDSTend9;}
			set {m_CurDSTend9 = value;}
		}
        #endregion

	}


	/// <summary>
	/// Summary description for TimeZone.
	/// </summary>
	public class timeZone
	{
        protected static List<timeZoneData> m_timeZoneSortedList;
        protected static Hashtable m_timeZoneList = null;
        private static DateTime m_nullTime;
        //
		// timezone list is static and initialized only once
		//
		public timeZone(ref ISession session)
		{
			if( m_timeZoneList == null)
				Init(ref session);
		}
		public timeZone(string configPath)
		{
			try
			{
				if( m_timeZoneList == null)
				{
					SessionManagement.GetSession(configPath);
					ISession session = SessionManagement.GetSession(configPath);
					Init(ref session);
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		
		public timeZone(){}
        public static DateTime nullTime() { return m_nullTime; }
    
		public static bool Init(string configPath)
		{
			try
			{
				if( m_timeZoneList == null)
				{
					SessionManagement.GetSession(configPath);
					ISession session = SessionManagement.GetSession(configPath);
					Init(ref session);
				}
				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		
		//
		// force time zone init
		public static bool Init(ref ISession session)
		{
         	try
			{
                m_timeZoneSortedList = new List<timeZoneData>();
                m_timeZoneList = new Hashtable();
				if(!session.IsConnected)
				{
					session.Reconnect();
				}
				IList timeList = session.CreateCriteria(typeof(timeZoneData))
                                        .AddOrder(Order.Asc("sortOrder"))
										.List();
				session.Disconnect();
                if (timeList == null || timeList.Count == 0)
					throw new Exception("Error in time zone initialization");

                // we need to build a sorted list and a hash table.
                foreach (timeZoneData tm in timeList)
                {
                    // cosmetic change. 
                    if (tm.TimeZoneDiff.Contains("-00:00"))
                        tm.TimeZoneDiff = "(GMT)";
                    m_timeZoneSortedList.Add(tm);
                }
                foreach (timeZoneData td in m_timeZoneSortedList)
                    m_timeZoneList.Add(td.TimeZoneID, td);
               
                m_nullTime = DateTime.Parse("01/01/1980 00:00:00");
			}
			catch (Exception e)
			{
				// must log this exception !!!
				//		outXml  = "<error><errorCode>100</errorCode><errorMessage>" + e.Message + "</errorMessage>";
				throw e;
			}
			return true;
		}

		public static bool GetTimeZone(int tzId, ref timeZoneData tz)
		{
			try
			{
				if( m_timeZoneList == null)
					return false;
		
				tz = (timeZoneData)m_timeZoneList[tzId];
				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
				
		}

        public static bool GetAllTimeZone(ref Hashtable timeZones)
        {
            try
            {
                if (m_timeZoneList == null)
                    return false;
                else
                    timeZones = m_timeZoneList; 
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public static List<timeZoneData>GetTimeZoneList()
        { return m_timeZoneSortedList; }
      
        private static bool isDst(DateTime date, ref int offset, int tzid)
		{
			timeZoneData tz = new timeZoneData();
			bool isDST = false;
			offset = 0;
			if( timeZone.GetTimeZone(tzid, ref tz) )
			{
				if(tz.DST == 1)
				{
					if( date >= tz.CurDSTstart && date <= tz.CurDSTend)
						isDST = true;
					else if( date >= tz.CurDSTstart && date <= tz.CurDSTend)
						isDST = true;
					else if( date >= tz.CurDSTstart1 && date <= tz.CurDSTend1)
						isDST = true;
					else if( date >= tz.CurDSTstart2 && date <= tz.CurDSTend2)
						isDST = true;
					else if( date >= tz.CurDSTstart3 && date <= tz.CurDSTend3)
						isDST = true;
					else if( date >= tz.CurDSTstart4 && date <= tz.CurDSTend4)
						isDST = true;
					else if( date >= tz.CurDSTstart5 && date <= tz.CurDSTend5)
						isDST = true;
					else if( date >= tz.CurDSTstart6 && date <= tz.CurDSTend6)
						isDST = true;
					else if( date >= tz.CurDSTstart7 && date <= tz.CurDSTend7)
						isDST = true;
					else if( date >= tz.CurDSTstart8 && date <= tz.CurDSTend8)
						isDST = true;
                    else if (date >= tz.CurDSTstart9 && date <= tz.CurDSTend9)
                        isDST = true;
                }		
	
				if(isDST)
					offset = tz.DaylightBias;
			}
			else
				throw new Exception("Error in time zone lookup");
			return isDST;

		}

		public static bool changeToGMTTime( int tzid, ref DateTime date )
		{
			try
			{
				timeZoneData tz = new timeZoneData();
				if( timeZone.GetTimeZone(tzid, ref tz) )
				{
					int offset = 0;		
					if(isDst(date, ref offset, tzid))
						date = date.AddMinutes(tz.DaylightBias);
					date = date.AddMinutes(tz.Bias);
					return true;
				}
				else 
					return false;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
           
        // change from GMT to User (port from com)
   		public static bool GMTToUserPreferedTime(int tzid, ref DateTime date )
		{
			try
			{

				timeZoneData tz = new timeZoneData();
				if( timeZone.GetTimeZone(tzid, ref tz) )
				{
					int offset = 0;		
					if(isDst(date, ref offset, tzid))
						date = date.AddMinutes(-tz.DaylightBias);
					date = date.AddMinutes(-tz.Bias);
					return true;
				}
				else 
					return false;
			}
			catch (Exception e)
			{
				throw e;
			}
		}








		public static bool userPreferedTime( int tzid, ref DateTime date )
		{
			try
			{
				timeZoneData tz = new timeZoneData();
				if( GetTimeZone(tzid, ref tz) )
				{
					int offset = 0;		
					if(isDst(date, ref offset,tz.TimeZoneID))
						date = date.AddMinutes(-tz.DaylightBias);
					date = date.AddMinutes(-tz.Bias);
					return true;
				}
				else 
					return false;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
        /// <summary>
        /// First day of the month for a
        /// month passed (as int)
        /// </summary>
        public static DateTime GetFirstDayOfMonth(int iMonth)
        {
            // return DateTime with fist day of month for the month passed (year is current year)
            DateTime theDate = new DateTime(DateTime.Now.Year, iMonth, 1);

            // remove all of the days in the month
            // except the first day and set the
            // variable to hold that date
            theDate = theDate.AddDays(-(theDate.Day - 1));
            return theDate;
        }
        /// <summary>
        /// Last day of the month for a
        /// month passed (as int)
        /// </summary>
        public static DateTime GetLastDayOfMonth(int iMonth)
        {
            // return DateTime with fist day of month for the month passed (year is current year)

            DateTime theDate = new DateTime(DateTime.Now.Year, iMonth, 1);

            // get NEXT month

            theDate = theDate.AddMonths(1);

            // remove all of the days in this month
            // to get back to the last day of the
            // previous month

            theDate = theDate.AddDays(-(theDate.Day));
            return theDate;
        }
     
		
	}
}
