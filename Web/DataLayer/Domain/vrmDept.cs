/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Xml;


namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description for user table Dept_List_D 
	/// </summary>
	public class vrmDeptApprover
	{
		#region Private Internal Members

		private int m_id, m_departmentid, m_approverid;
	
		#endregion
		
		#region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }	
		public int departmentid
		{
			get {return m_departmentid;}
			set {m_departmentid = value;}
		}	
		public int approverid
		{
			get {return m_approverid;}
			set {m_approverid = value;}
		}	
		#endregion
	
	}
    public class vrmDept
    {
        #region Private Internal Members

        private int m_departmentId;
        private string m_departmentName;
        private string m_securityKey;
        private int m_Deleted;
        private int m_responsetime;
        private string m_responsemessage;
        private string m_orgId;

        #endregion

        #region Public Properties

        public int departmentId
        {
            get { return m_departmentId; }
            set { m_departmentId = value; }
        }
        public string departmentName
        {
            get { return m_departmentName; }
            set { m_departmentName = value; }
        }
        public string securityKey
        {
            get { return m_securityKey; }
            set { m_securityKey = value; }
        }
        public int Deleted
        {
            get { return m_Deleted; }
            set { m_Deleted = value; }
        }
        public int deleted
        {
            get { return m_Deleted; }
            set { m_Deleted = value; }
        }
        public int responsetime
        {
            get { return m_responsetime; }
            set { m_responsetime = value; }
        }
        public string responsemessage
        {
            get { return m_responsemessage; }
            set { m_responsemessage = value; }
        }

        public string orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }

        #endregion

    }
    public class vrmUserDepartment
    {
        #region Private Internal Members

        private int m_Id;//FB 2027
        private int m_departmentId;
        private int m_userId;
     
        #endregion

        #region Public Properties
        //FB 2027 Start
        public int ID
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        //FB 2027 End
        public int departmentId
        {
            get { return m_departmentId; }
            set { m_departmentId = value; }
        }
        public int userId
        {
            get { return m_userId; }
            set { m_userId = value; }
        }
        
        #endregion

    }
    public class vrmDeptCustomAttr
    {
        #region Private Internal Members
        private int m_DeptID, m_CustomAttributeId, m_Type, m_Mandatory, m_Deleted, m_status;
        private string m_DiplayTitle, m_Description;
        private int m_IncludeInEmail, m_IncludeInCalendar; //FB 2013//Custom Attribute Fixes
        private string m_orgId;
        private int m_RoomApp, m_McuApp, m_SystemApp, m_Scheduler, m_Host, m_Party, m_McuAdmin, m_RoomAdmin, m_VNOCOperator; //FB 1767 //FB 2501
        private int m_CreateType; //FB 1779

        #endregion

        #region Public Properties
        
        public int DeptID
        {
            get { return m_DeptID; }
            set { m_DeptID = value; }      
        }
        public int CustomAttributeId
        {
            get { return m_CustomAttributeId; }
            set { m_CustomAttributeId = value; }      
        }
        public int Type
        {
            get { return m_Type; }
            set { m_Type = value; }      
        }
        public int Mandatory
        {
            get { return m_Mandatory; }
            set { m_Mandatory = value; }      
        }
        public int deleted
        {
            get { return m_Deleted; }
            set { m_Deleted = value; }      
        }
        public int status
        {
            get { return m_status; }
            set { m_status = value; }      
        }
        public string DisplayTitle
        {
            get { return m_DiplayTitle; }
            set { m_DiplayTitle = value; }      
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }      
        }
        public int IncludeInEmail   //Custom Attribute Fixes
        {
            get { return m_IncludeInEmail; }
            set { m_IncludeInEmail = value; }      
        }

        public int IncludeInCalendar   //FB 2013
        {
            get { return m_IncludeInCalendar; }
            set { m_IncludeInCalendar = value; }      
        }

        public string orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }

        //FB 1767 start
        public int RoomApp
        {
            get { return m_RoomApp; }
            set { m_RoomApp = value; }
        }
        public int McuApp
        {
            get { return m_McuApp; }
            set { m_McuApp = value; }
        }
        public int SystemApp
        {
            get { return m_SystemApp; }
            set { m_SystemApp = value; }
        }
        public int Scheduler
        {
            get { return m_Scheduler; }
            set { m_Scheduler = value; }
        }
        public int Host
        {
            get { return m_Host; }
            set { m_Host = value; }
        }
        public int Party
        {
            get { return m_Party; }
            set { m_Party = value; }
        }
        public int McuAdmin
        {
            get { return m_McuAdmin; }
            set { m_McuAdmin = value; }
        }
        public int RoomAdmin
        {
            get { return m_RoomAdmin; }
            set { m_RoomAdmin = value; }
        }
        //FB 1767 end
        public int CreateType   //FB 1779
        {
            get { return m_CreateType; }
            set { m_CreateType = value; }
        }
        //FB 2501 Starts
        public int VNOCOperator 
        {
            get { return m_VNOCOperator; }
            set { m_VNOCOperator = value; }
        }
        //FB 2501 Ends
        #endregion

    }
    public class vrmDeptCustomAttrEx
    {
        #region Private Internal Members
        private int m_CustomAttributeID, m_OptionID,m_DeptID, m_SubOptionID, m_SubOptionType, m_AttributeID;
        private string m_DisplayValue, m_DisplayCaption;
        #endregion
        #region Public Properties
        
        public int CustomAttributeID
        {
            get { return m_CustomAttributeID; }
            set { m_CustomAttributeID = value; }      
        }
        public int OptionID
        {
            get { return m_OptionID; }
            set { m_OptionID = value; }      
        }
        public int DeptID
        {
            get { return m_DeptID; }
            set { m_DeptID = value; }      
        }
        public int SubOptionID
        {
            get { return m_SubOptionID; }
            set { m_SubOptionID = value; }      
        }
        public int SubOptionType
        {
            get { return m_SubOptionType; }
            set { m_SubOptionType = value; }      
        }
        public int AttributeID
        {
            get { return m_AttributeID; }
            set { m_AttributeID = value; }      
        }

        public string DisplayValue
        {
            get { return m_DisplayValue; }
            set { m_DisplayValue = value; }      
        }
        public string DisplayCaption
        {
            get { return m_DisplayCaption; }
            set { m_DisplayCaption = value; }      
        }
       
        
        #endregion
    }
    public class vrmDeptCustomAttrOption
    {
       #region Private Internal Members
        private int m_CustomAttributeID, m_OptionID, m_DeptID, m_OptionType, m_LanguageID, m_UID; //FB 1970
        //code added for Add FB 1470 
        private string m_OptionValue, m_Caption, m_HelpText;
        
        #endregion
        #region Public Properties
        
        public int CustomAttributeID
        {
            get { return m_CustomAttributeID; }
            set { m_CustomAttributeID = value; }      
        }
        public int OptionID
        {
            get { return m_OptionID; }
            set { m_OptionID = value; }      
        }
        public int OptionType
        {
            get { return m_OptionType; }
            set { m_OptionType = value; }
        }
        public int DeptID
        {
            get { return m_DeptID; }
            set { m_DeptID = value; }      
        }
     
        public string OptionValue
        {
            get { return m_OptionValue; }
            set { m_OptionValue = value; }      
        }
        public string Caption
        {
            get { return m_Caption; }
            set { m_Caption = value; }      
        }
        //code added for Add FB 1470        
        public string HelpText
        {
            get { return m_HelpText; }
            set { m_HelpText = value; }
        }
        public int LanguageID //FB 1970
        {
            get { return m_LanguageID; }
            set { m_LanguageID = value; }
        }
        public int UID //FB 1970
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        #endregion
     }

    //Added for FB 1970
    public class VrmCustomLanguage
    {
        #region Private Internal Members
        private int m_CustomAttributeID, m_LanguageId, m_UID;
        private string m_DisplayTitle;
        #endregion

        #region Public Properties
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int LanguageID
        {
            get { return m_LanguageId; }
            set { m_LanguageId = value; }
        }
        public int CustomAttributeID
        {
            get { return m_CustomAttributeID; }
            set { m_CustomAttributeID = value; }
        }
        public string DisplayTitle
        {
            get { return m_DisplayTitle; }
            set { m_DisplayTitle = value; }
        }
        #endregion
    }
}
