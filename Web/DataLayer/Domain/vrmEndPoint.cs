/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Xml;


namespace myVRM.DataLayer
{
    /// <summary>
    /// Summary description for user table Ept_List_D 
    /// </summary>
    public class vrmEndPoint
    {
        #region Private Internal Members

        private int m_uId;
        private int m_endpointid;
        private string m_name;
        private string m_password;
        private int m_protocol;
        private int m_connectiontype;
        private int m_addresstype;
        private string m_address;
        private int m_deleted;
        private int m_outsidenetwork;
        private int m_videoequipmentid;
        private int m_linerateid;
        private int m_bridgeid;
        private string m_endptURL;
        private int m_profileId;
        private int m_isDefault;
        private int m_encrypted;
        private string m_profileName;
        private string m_MCUAddress;
        private int m_MCUAddressType;
        //Code Added for FB 1422
        private int m_TelnetAPI;
        private string m_ExchangeID; //Cisco Telepresence fix
        private int m_CalendarInvite; //Cisco ICAL FB 1602
        
        private int m_orgID;//code added fro organization module

        private int m_ApiPortNo, m_isTelePresence; //Api port //FB 2400
        private string m_ConferenceCode; // FB 1642-Audio add on
        private string m_LeaderPin, m_RearSecCameraAddress, m_MultiCodecAddress, m_NetworkURL;// FB 1642-Audio add on //FB 2400 //FB 2595
        private int m_Extendpoint, m_PublicEndPoint;// FB 2426 //FB 2594
        private int m_EptOnlineStatus, m_Secured, m_Secureport, m_EptCurrentStatus; // FB 2501 EM7 //FB 2595 //FB 2616
        private string m_GateKeeeperAddress;//ZD 100132
        #endregion

        #region Public Properties

        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int endpointid
        {
            get { return m_endpointid; }
            set { m_endpointid = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string password
        {
            get { return m_password; }
            set { m_password = value; }
        }
        public int protocol
        {
            get { return m_protocol; }
            set { m_protocol = value; }
        }
        public int connectiontype
        {
            get { return m_connectiontype; }
            set { m_connectiontype = value; }
        }
        public int addresstype
        {
            get { return m_addresstype; }
            set { m_addresstype = value; }
        }
        public string address
        {
            get { return m_address; }
            set { m_address = value; }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public int outsidenetwork
        {
            get { return m_outsidenetwork; }
            set { m_outsidenetwork = value; }
        }
        public int videoequipmentid
        {
            get { return m_videoequipmentid; }
            set { m_videoequipmentid = value; }
        }
        public int linerateid
        {
            get { return m_linerateid; }
            set { m_linerateid = value; }
        }
        public int bridgeid
        {
            get { return m_bridgeid; }
            set { m_bridgeid = value; }
        }
        public string endptURL
        {
            get { return m_endptURL; }
            set { m_endptURL = value; }
        }
        public int profileId
        {
            get { return m_profileId; }
            set { m_profileId = value; }
        }
        public int isDefault
        {
            get { return m_isDefault; }
            set { m_isDefault = value; }
        }
        public int encrypted
        {
            get { return m_encrypted; }
            set { m_encrypted = value; }
        }
        public string profileName
        {
            get { return m_profileName; }
            set { m_profileName = value; }
        }
        public string MCUAddress
        {
            get { return m_MCUAddress; }
            set { m_MCUAddress = value; }
        }
        public int MCUAddressType
        {
            get { return m_MCUAddressType; }
            set { m_MCUAddressType = value; }
        }
        //Code Added for FB 1422
        public int TelnetAPI
        {
            get { return m_TelnetAPI; }
            set { m_TelnetAPI = value; }
        }
        //Code added for organization module
        public int orgId
        {
            get { return m_orgID; }
            set { m_orgID = value; }
        }
        public string ExchangeID //Cisco Telepresence fix
        {
            get { return m_ExchangeID; }
            set { m_ExchangeID = value; }
        }
        public int CalendarInvite //Cisco ICAL FB 1602
        {
            get { return m_CalendarInvite; }
            set { m_CalendarInvite = value; }
        }
       
        public int ApiPortNo //Api Port
        {
            get { return m_ApiPortNo; }
            set { m_ApiPortNo = value; }
        }
        //FB 1642-Audio add on - Starts
        public string ConferenceCode
        {
            get { return m_ConferenceCode; }
            set { m_ConferenceCode = value; }
        }
        public string LeaderPin
        {
            get { return m_LeaderPin; }
            set { m_LeaderPin = value; }
        }
        //FB 1642-Audio add on - End
        public string RearSecCameraAddress //FB 2400 start
        {
            get { return m_RearSecCameraAddress; }
            set { m_RearSecCameraAddress = value; }
        }
        public string MultiCodecAddress
        {
            get { return m_MultiCodecAddress; }
            set { m_MultiCodecAddress = value; }
        }
        public int isTelePresence
        {
            get { return m_isTelePresence; }
            set { m_isTelePresence = value; }
        }                                   //FB 2400 end
        //FB 2426 Start
        public int Extendpoint
        {
            get { return m_Extendpoint; }
            set { m_Extendpoint = value; }
        }
        //FB 2426 End

        //FB 2501 EM7 Starts
        public int EptOnlineStatus
        {
            get { return m_EptOnlineStatus; }
            set { m_EptOnlineStatus = value; }
        }
        //FB 2501 EM7 Ends
        //FB 2616 EM7 Starts
        public int EptCurrentStatus
        {
            get { return m_EptCurrentStatus; }
            set { m_EptCurrentStatus = value; }
        }
        //FB 2616 EM7 Ends
        public int PublicEndPoint //FB 2594
        {
            get { return m_PublicEndPoint; }
            set { m_PublicEndPoint = value; }
        }
        //FB 2595 Starts
        public int Secured
        {
            get { return m_Secured; }
            set { m_Secured = value; }
        }
        public int Secureport
        {
            get { return m_Secureport; }
            set { m_Secureport = value; }
        }
        public string NetworkURL 
        {
            get { return m_NetworkURL; }
            set { m_NetworkURL = value; }
        }
        //FB 2595 Ends
        //ZD 100132 START
        public string GateKeeeperAddress
        {
            get { return m_GateKeeeperAddress; }
            set { m_GateKeeeperAddress = value; }
        }
        //ZD 100132 END
        #endregion
    }

}
