/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Globalization;

using NHibernate;
using NHibernate.Criterion;

using log4net;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description for Errors (err_list_S).
	/// </summary>
	public class vrmError
	{
	
		#region Private Internal Members
        private int m_ID, m_Languageid, m_UID; //FB 1881
        private string m_Description, m_Message, m_Level, m_Errortype; //FB 1881
		#endregion

		#region Public Properties
		public vrmError()
		{
		}

        public int UID //FB 1881
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
		public int ID
		{
			get {return m_ID;}
			set {m_ID = value;}
		}
        public string Description
		{
			get {return m_Description;}
			set {m_Description = value;}
		}
        public string Message
		{
			get {return m_Message;}
			set {m_Message = value;}
		}
        public string Level
		{
			get {return m_Level;}
			set {m_Level = value;}
		}
        //FB 1881 start
        public int Languageid
        {
            get { return m_Languageid; }
            set { m_Languageid = value; }
        }
        public string Errortype
        {
            get { return m_Errortype; }
            set { m_Errortype = value; }
        }
        //FB 1881 end
		#endregion
	}

    //FB 2027 Start

    public class vrmErrorLog
    {

        #region Private Internal Members
        private int m_logid, m_moduleerrcode, m_severity, m_line;
        private string m_modulename, m_fil, m_mess;
        private DateTime m_timesubmitted, m_timelogged;
        #endregion

        #region Public Properties
        public vrmErrorLog()
        {
        }

        public int logid
        {
            get { return m_logid; }
            set { m_logid = value; }
        }
        public int moduleerrcode
        {
            get { return m_moduleerrcode; }
            set { m_moduleerrcode = value; }
        }
        public int severity
        {
            get { return m_severity; }
            set { m_severity = value; }
        }
        public int line
        {
            get { return m_line; }
            set { m_line = value; }
        }
        public string modulename
        {
            get { return m_modulename; }
            set { m_modulename = value; }
        }

        public string fil
        {
            get { return m_fil; }
            set { m_fil = value; }
        }
        public string mess
        {
            get { return m_mess; }
            set { m_mess = value; }
        }
        public DateTime timesubmitted
        {
            get { return m_timesubmitted; }
            set { m_timesubmitted = value; }
        }
        public DateTime timelogged
        {
            get { return m_timelogged; }
            set { m_timelogged = value; }
        }
        #endregion
    }
    public interface IvrmErrorLogDAO : IDao<vrmErrorLog, int> { } 

    //FB 2027 End

	public class vrmErrorDAO
	{
        public static Hashtable m_ErrorList = null; //FB 1881
        public static int m_Language = 1; //FB 1881
		//
		// error list is static and initialized only once
		//
		public vrmErrorDAO(ref ISession session)
		{
			if( m_ErrorList == null)
				Init(ref session);
		}

		public vrmErrorDAO(string configPath)
		{
			try
			{
				if( m_ErrorList == null)
				{
					SessionManagement.GetSession(configPath);
					ISession session = SessionManagement.GetSession(configPath);
					Init(ref session);
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		
		}
		
		public vrmErrorDAO(){}

		public static bool Init(string configPath)
		{
			try
			{
				if( m_ErrorList == null)
				{
					SessionManagement.GetSession(configPath);
					ISession session = SessionManagement.GetSession(configPath);
					Init(ref session);
				}
				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		
		//
		// force error zone init
		public static bool Init(ref ISession session)
		{
			try
			{
                if(m_Language <=0)  //FB 1881
                    m_Language = 1;

				m_ErrorList = new Hashtable();
				if(!session.IsConnected)
				{
					session.Reconnect();
				}
				IList eList;
				eList = session.CreateCriteria(typeof(vrmError))
								.AddOrder( Order.Asc("ID") )
								.List();
								session.Disconnect();
				if(eList == null || eList.Count == 0)
					throw new Exception("Error in error logs initialization");
                foreach (vrmError er in eList)
                {
                    if (er.Errortype == "S")
                    {
                        if (!m_ErrorList.Contains(er.ID))//General Fix
                            m_ErrorList.Add(er.ID, er);
                    }

                    if (m_Language == er.Languageid) //FB 1881
                    {
                        if (er.Errortype == "U")
                        {
                            if (!m_ErrorList.Contains(er.ID))//General Fix
                                m_ErrorList.Add(er.ID, er);
                        }
                    }
                }
			}
			catch (Exception e)
			{
				throw e;
			}
			return true;
		}
		public static bool GetError(ref vrmError er, int errID)
		{
			try
			{
				if(m_ErrorList != null && m_ErrorList.Count > 0)
				{
                    if(m_ErrorList.Contains(errID)) //General Fix
					    er = (vrmError)m_ErrorList[errID];
				}
				else
					return false;
			}
			catch (Exception e)
			{
				throw e;
			}
			return true;
		}
	}


}
