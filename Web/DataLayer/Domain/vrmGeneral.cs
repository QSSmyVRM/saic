/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

using log4net;


namespace myVRM.DataLayer
{
    /// <summary>
    /// Summary description for user table General Static/Dynamic tables 
    /// </summary>
    public class vrmVideoSession
    {
        #region Private Internal Members
        private int m_Id;
        private string m_sessionType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string VideoSessionType
        {
            get { return m_sessionType; }
            set { m_sessionType = value; }
        }

        #endregion


    }
    public class vrmAddressType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_name;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        #endregion
    }
    public class vrmLineRate
    {
        #region Private Internal Members
        private int m_Id;
        private string m_lineRate;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string LineRateType
        {
            get { return m_lineRate; }
            set { m_lineRate = value; }
        }

        #endregion


    }
    public class vrmLanguage
    {
        #region Private Internal Members
        private int m_Id;
        private string m_Name, m_Country, m_LanguageFolder; //FB 1830
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        public string LanguageFolder    //FB 1830
        {
            get { return m_LanguageFolder; }
            set { m_LanguageFolder = value; }
        }
        #endregion


    }
    public class vrmVideoEquipment
    {
        #region Private Internal Members
        private int m_Id;
        private string m_veName;
        private int m_Familyid;//FB 2390
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string VEName
        {
            get { return m_veName; }
            set { m_veName = value; }
        }
        //FB 2390
        public int Familyid
        {
            get { return m_Familyid; }
            set { m_Familyid = value; }
        }
        #endregion


    }
    public class vrmAudioAlg
    {
        #region Private Internal Members
        private int m_Id;
        private string m_audioType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string AudioType
        {
            get { return m_audioType; }
            set { m_audioType = value; }
        }

        #endregion
    }
    public class vrmInterfaceType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_interfaceType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string Interface
        {
            get { return m_interfaceType; }
            set { m_interfaceType = value; }
        }

        #endregion
    }
    public class vrmMediaType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_mediaType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string mediaType
        {
            get { return m_mediaType; }
            set { m_mediaType = value; }
        }

        #endregion
    }

    public class vrmVideoMode
    {
        #region Private Internal Members
        private int m_id;
        private string m_modeName;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string modeName
        {
            get { return m_modeName; }
            set { m_modeName = value; }
        }

        #endregion
    }
    public class vrmVideoProtocol
    {
        #region Private Internal Members
        private int m_VideoProtocolID;
        private string m_VideoProtocolType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_VideoProtocolID; }
            set { m_VideoProtocolID = value; }
        }
        public string VideoProtocolType
        {
            get { return m_VideoProtocolType; }
            set { m_VideoProtocolType = value; }
        }

        #endregion


    }
    public class vrmDialingOption
    {
        #region Private Internal Members
        private int m_ID;
        private string m_Option;
        #endregion

        #region Public Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string DialingOption
        {
            get { return m_Option; }
            set { m_Option = value; }
        }

        #endregion


    }
    //NewLobby
    public class vrmIconsRef
    {
        #region Private Internal Members
        private int m_IconID;
        private string m_IconUri, m_Label, m_IconTarget;
        #endregion

        #region Public Properties
        public int IconID
        {
            get { return m_IconID; }
            set { m_IconID = value; }
        }
        public string IconUri
        {
            get { return m_IconUri; }
            set { m_IconUri = value; }
        }
        public string Label
        {
            get { return m_Label; }
            set { m_Label = value; }
        }
        public string IconTarget
        {
            get { return m_IconTarget; }
            set { m_IconTarget = value; }
        }
        #endregion

    }
    //FB 2027 - Start
    public class vrmLogPref
    {

        #region Private Internal Members

        private string m_modulename;
        private int m_loglevel;
        private int m_loglife;
        private string m_LogModule;
        private int m_UID;

        #endregion

        #region Public Properties

        public string modulename
        {
            get { return m_modulename; }
            set { m_modulename = value; }
        }

        public int loglevel
        {
            get { return m_loglevel; }
            set { m_loglevel = value; }
        }

        public int loglife
        {
            get { return m_loglife; }
            set { m_loglife = value; }
        }

        public string LogModule
        {
            get { return m_LogModule; }
            set { m_LogModule = value; }
        }
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }

        #endregion
    }
    //FB 2027 - End
    public class vrmServiceType//FB 2219
    {
        #region Private Internal Members
        private int m_Id;
        private string m_ServiceType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string ServiceType
        {
            get { return m_ServiceType; }
            set { m_ServiceType = value; }
        }

        #endregion
    }
    
    //FB 2136 - Starts
    public class vrmSecurityBadgeType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_SecBadgeType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public string SecBadgeType
        {
            get { return m_SecBadgeType; }
            set { m_SecBadgeType = value; }
        }
        #endregion
    }
    //FB 2136 - End

    //FB 2693 - Starts
    #region vrmPCDetails
    public class vrmPCDetails
    {
        #region Private Internal Members
        private int m_Id;
        private string m_PCType, m_Absolutepath;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string PCType
        {
            get { return m_PCType; }
            set { m_PCType = value; }
        }
        public string Absolutepath
        {
            get { return m_Absolutepath; }
            set { m_Absolutepath = value; }
        }
        #endregion
    }
    #endregion
    //FB 2693 - End

    /// <summary>
    /// Summary description for static General Information Tables.
    /// </summary>
    public class vrmGen
    {
        protected static ArrayList m_AddressTypeList = null;
        protected static ArrayList m_VideoSessionList = null;
        protected static ArrayList m_LineRateList = null;
        protected static ArrayList m_LanguageList = null;
        protected static ArrayList m_VideoEquipmentList = null;
        protected static ArrayList m_AudioAlgList = null;
        protected static ArrayList m_InterfaceTypeList = null;
        protected static ArrayList m_VideoModeList = null;
        protected static ArrayList m_MediaTypeList = null;
        protected static ArrayList m_VideoProtocolList = null;
        protected static ArrayList m_DialingOptionList = null;
        protected static ArrayList m_MCUCardDetail = null;
        protected static ArrayList m_MCUVendor = null;
        protected static ArrayList m_MCUStatus = null;
        protected static ArrayList m_LogPrefList = null;//FB 2027
        protected static ArrayList m_ServiceTypeList = null;//FB 2219
        protected static ArrayList m_SecurityBadgeTypeList = null; //FB 2136
        protected static ArrayList m_PCDetails = null; //FB 2693
      
        //
        // gen_tables are static and loaded only once
        //
        public vrmGen(ref ISession session)
        {
            if (m_VideoSessionList == null)
                Init(ref session);
        }
        public vrmGen(string configPath)
        {
            try
            {
                if (m_VideoSessionList == null)
                {
                    SessionManagement.GetSession(configPath);
                    ISession session = SessionManagement.GetSession(configPath);
                    Init(ref session);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public vrmGen() { }

        public static bool Init(string configPath)
        {
            try
            {
                if (m_VideoSessionList == null)
                {
                    SessionManagement.GetSession(configPath);
                    ISession session = SessionManagement.GetSession(configPath);
                    Init(ref session);
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //
        // force time zone init
        public static bool Init(ref ISession session)
        {
            try
            {
                m_AddressTypeList = new ArrayList();
                m_VideoSessionList = new ArrayList();
                m_LineRateList = new ArrayList();
                m_VideoEquipmentList = new ArrayList();
                m_AudioAlgList = new ArrayList();
                m_LanguageList = new ArrayList();
                m_InterfaceTypeList = new ArrayList();
                m_VideoModeList = new ArrayList();
                m_MediaTypeList = new ArrayList();
                m_VideoProtocolList = new ArrayList();
                m_DialingOptionList = new ArrayList();
                m_MCUCardDetail = new ArrayList();
                m_MCUVendor = new ArrayList();
                m_MCUStatus = new ArrayList();
                m_LogPrefList = new ArrayList();//FB 2027
                m_ServiceTypeList = new ArrayList();//FB 2219
                m_SecurityBadgeTypeList = new ArrayList(); //FB 2136
                m_PCDetails = new ArrayList(); //FB 2693

                if (!session.IsConnected)
                {
                    session.Reconnect();
                }
                m_AddressTypeList = (ArrayList)(session.CreateCriteria(typeof(vrmAddressType))
                                        .AddOrder(Order.Asc("Id"))
                                        .List());
                if (m_AddressTypeList == null || m_AddressTypeList.Count == 0)
                    throw new Exception("Error in Address Type List initialization");
                m_VideoSessionList = (ArrayList)session.CreateCriteria(typeof(vrmVideoSession))
                                        .AddOrder(Order.Asc("Id"))
                                        .List();
                if (m_VideoSessionList == null || m_VideoSessionList.Count == 0)
                    throw new Exception("Error in Video Session List initialization");
                m_LineRateList = (ArrayList)session.CreateCriteria(typeof(vrmLineRate))
                                             .AddOrder(Order.Asc("Id"))
                                             .List();
                if (m_LineRateList == null || m_LineRateList.Count == 0)
                    throw new Exception("Error in Line Rate List initialization");
                m_LanguageList = (ArrayList)session.CreateCriteria(typeof(vrmLanguage))
                                             .AddOrder(Order.Asc("Id"))
                                             .List();
                if (m_LanguageList == null || m_LanguageList.Count == 0)
                    throw new Exception("Error in Language List initialization");
                m_VideoEquipmentList = (ArrayList)session.CreateCriteria(typeof(vrmVideoEquipment))
                                             .AddOrder(Order.Asc("Familyid")) //FB 2390
                                             .AddOrder(Order.Asc("VEName")) //FB 2390
                                             .List();
                if (m_VideoEquipmentList == null || m_VideoEquipmentList.Count == 0)
                    throw new Exception("Error in Video Equipment List initialization");
                m_AudioAlgList = (ArrayList)session.CreateCriteria(typeof(vrmAudioAlg))
                                             .AddOrder(Order.Asc("Id"))
                                             .List();
                if (m_AudioAlgList == null || m_AudioAlgList.Count == 0)
                    throw new Exception("Error in Audio Ald List initialization");
                m_InterfaceTypeList = (ArrayList)session.CreateCriteria(typeof(vrmInterfaceType))
                                             .AddOrder(Order.Asc("Id"))
                                             .List();
                if (m_InterfaceTypeList == null || m_InterfaceTypeList.Count == 0)
                    throw new Exception("Error in Interface Type List initialization");
                m_VideoModeList = (ArrayList)session.CreateCriteria(typeof(vrmVideoMode))
                                   .AddOrder(Order.Asc("Id"))
                                   .List();
                if (m_VideoModeList == null || m_VideoModeList.Count == 0)
                    throw new Exception("Error in Video Mode List initialization");              
                m_MediaTypeList = (ArrayList)session.CreateCriteria(typeof(vrmMediaType))
                                   .AddOrder(Order.Asc("Id"))
                                   .List();
                if (m_MediaTypeList == null || m_MediaTypeList.Count == 0)
                    throw new Exception("Error in Media Type List initialization");
                m_VideoProtocolList = (ArrayList)session.CreateCriteria(typeof(vrmVideoProtocol))
                                   .AddOrder(Order.Asc("Id"))
                                   .List();
                if (m_VideoProtocolList == null || m_VideoProtocolList.Count == 0)
                    throw new Exception("Error in Video Protocol List initialization");

                m_DialingOptionList = (ArrayList)session.CreateCriteria(typeof(vrmDialingOption))
                                    .AddOrder(Order.Asc("ID"))
                                    .List();
                if (m_DialingOptionList == null || m_DialingOptionList.Count == 0)
                    throw new Exception("Error in Dialing Option List initialization");
                m_MCUCardDetail = (ArrayList)session.CreateCriteria(typeof(vrmMCUCardDetail))
                                    .AddOrder(Order.Asc("id"))
                                    .List();
                if (m_MCUCardDetail == null || m_MCUCardDetail.Count == 0)
                    throw new Exception("Error in MCU Card Detail initialization");

                m_MCUStatus = (ArrayList)session.CreateCriteria(typeof(vrmMCUStatus))
                                    .AddOrder(Order.Asc("id"))
                                    .List();
                if (m_MCUStatus == null || m_MCUStatus.Count == 0)
                    throw new Exception("Error in MCU Status Detail initialization");
            
                m_MCUVendor = (ArrayList)session.CreateCriteria(typeof(vrmMCUVendor))
                                    .AddOrder(Order.Asc("id"))
                                    .List();
                if (m_MCUVendor == null || m_MCUVendor.Count == 0)
                    throw new Exception("Error in MCU Vendor Detail initialization");

                m_LogPrefList = (ArrayList)(session.CreateCriteria(typeof(vrmLogPref)).AddOrder(Order.Asc("UID")).List());//FB 2027
                if (m_LogPrefList == null || m_LogPrefList.Count == 0)
                    throw new Exception("Error in Address Type List initialization");

                m_ServiceTypeList = (ArrayList)session.CreateCriteria(typeof(vrmServiceType)).AddOrder(Order.Asc("Id")).List();//FB 2219
                if (m_ServiceTypeList == null || m_ServiceTypeList.Count == 0)
                    throw new Exception("Error in Media Type List initialization");

                m_SecurityBadgeTypeList = (ArrayList)session.CreateCriteria(typeof(vrmSecurityBadgeType)).AddOrder(Order.Asc("Id")).List(); //FB 2136
                if (m_SecurityBadgeTypeList == null || m_SecurityBadgeTypeList.Count == 0)
                    throw new Exception("Error in Security Badge Type List initialization");

                //FB 2693 Starts
                m_PCDetails = (ArrayList)session.CreateCriteria(typeof(vrmPCDetails)).AddOrder(Order.Asc("Id")).List();
                if (m_PCDetails == null || m_PCDetails.Count == 0)
                    throw new Exception("Error in PC Type List initialization");
                //FB 2693 Ends
                
                session.Disconnect();
            }
            catch (Exception e)
            {
                session.Disconnect();
                throw e;
            }
            return true;
        }

        public static List<vrmVideoSession> getVideoSession()
        {
            return new List<vrmVideoSession>(m_VideoSessionList.ToArray(typeof(vrmVideoSession))
              as vrmVideoSession[]);
        }

        public static List<vrmLineRate> getLineRate()
        {
            return new List<vrmLineRate>(m_LineRateList.ToArray(typeof(vrmLineRate))
              as vrmLineRate[]);
        }

        public static List<vrmVideoEquipment> getVideoEquipment()
        {
            return new List<vrmVideoEquipment>(m_VideoEquipmentList.ToArray(typeof(vrmVideoEquipment))
              as vrmVideoEquipment[]);
        }

        public static List<vrmAudioAlg> getAudioAlg()
        {
            return new List<vrmAudioAlg>(m_AudioAlgList.ToArray(typeof(vrmAudioAlg))
              as vrmAudioAlg[]);
        }

        public static List<vrmInterfaceType> getInterfaceType()
        {
            return new List<vrmInterfaceType>(m_InterfaceTypeList.ToArray(typeof(vrmInterfaceType))
              as vrmInterfaceType[]);
        }

        public static List<vrmAddressType> getAddressType()
        {
            return new List<vrmAddressType>(m_AddressTypeList.ToArray(typeof(vrmAddressType))
            as vrmAddressType[]);
        }
        public static List<vrmLanguage> getLanguage()
        {
            return new List<vrmLanguage>(m_LanguageList.ToArray(typeof(vrmLanguage))
            as vrmLanguage[]);
        }
        public static List<vrmVideoMode> getVideoMode()
        {
            return new List<vrmVideoMode>(m_VideoModeList.ToArray(typeof(vrmVideoMode))
            as vrmVideoMode[]);
        }
        public static List<vrmMediaType> getMediaTypes()
        {
            return new List<vrmMediaType>(m_MediaTypeList.ToArray(typeof(vrmMediaType))
            as vrmMediaType[]);
        }
        public static List<vrmVideoProtocol> getVideoProtocols()
        {
            return new List<vrmVideoProtocol>(m_VideoProtocolList.ToArray(typeof(vrmVideoProtocol))
            as vrmVideoProtocol[]);
        }
        public static List<vrmDialingOption> getDialingOption()
        {
            return new List<vrmDialingOption>(m_DialingOptionList.ToArray(typeof(vrmDialingOption))
            as vrmDialingOption[]);
        }
        public static List<vrmMCUCardDetail> getMCUCardDetail()
        {
            return new List<vrmMCUCardDetail>(m_MCUCardDetail.ToArray(typeof(vrmMCUCardDetail))
            as vrmMCUCardDetail[]);
        }
        public static List<vrmMCUVendor> getMCUVendor()
        {
            return new List<vrmMCUVendor>(m_MCUVendor.ToArray(typeof(vrmMCUVendor))
            as vrmMCUVendor[]);
        }
        public static List<vrmMCUStatus> getMCUStatus()
        {
            return new List<vrmMCUStatus>(m_MCUStatus.ToArray(typeof(vrmMCUStatus))
            as vrmMCUStatus[]);
        }
        public static List<vrmLogPref> getLogPref()//FB 2027
        {
            return new List<vrmLogPref>(m_LogPrefList.ToArray(typeof(vrmLogPref))
              as vrmLogPref[]);
        }
        public static List<vrmServiceType> getServiceTypes()//FB 2219
        {
            return new List<vrmServiceType>(m_ServiceTypeList.ToArray(typeof(vrmServiceType))
            as vrmServiceType[]);
        }
        public static List<vrmSecurityBadgeType> getSecurityBadgeTypes() //FB 2136
        {
            return new List<vrmSecurityBadgeType>(m_SecurityBadgeTypeList.ToArray(typeof(vrmSecurityBadgeType))
            as vrmSecurityBadgeType[]);
        }

        //FB 2693 Starts
        public static List<vrmPCDetails> getPCTypes()
        {
            return new List<vrmPCDetails>(m_PCDetails.ToArray(typeof(vrmPCDetails))
              as vrmPCDetails[]);
        }
        //FB 2693 Ends
    }
}
