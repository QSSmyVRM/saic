﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;
using log4net;

namespace myVRM.DataLayer
{
    #region Class vrmImage
    /// <summary>
    /// Summary description for image management.
    /// </summary>
    public class vrmImage
    {
        #region Private Internal Members

        private int m_ImageId, m_OrgId;
        private int m_AttributeType;
        private Byte[] m_AttributeImage;

        #endregion

        #region Public Properties

        public int ImageId
        {
            get { return m_ImageId; }
            set { m_ImageId = value; }
        }

        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        
        public int AttributeType
        {
            get { return m_AttributeType; }
            set { m_AttributeType = value; }
        }

        public Byte[] AttributeImage
        {
            get { return m_AttributeImage; }
            set { m_AttributeImage = value; }
        }
        #endregion
    }
    
    //FB 2136 Starts
    #region Security Badge  
    public class vrmSecurityImage
    {

        #region Private Internal Members

        private int m_BadgeId, m_OrgId, m_DisplayOrder;
        private string m_ImgName, m_TextAxis, m_PhotoAxis, m_BarCodeAxis;
        private Byte[] m_BadgeImage;
        #endregion

        #region Public Properties

        public int BadgeId
        {
            get { return m_BadgeId; }
            set { m_BadgeId = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public int DisplayOrder
        {
            get { return m_DisplayOrder; }
            set { m_DisplayOrder = value; }
        }
        public Byte[] BadgeImage
        {
            get { return m_BadgeImage; }
            set { m_BadgeImage = value; }
        }
        public string ImgName
        {
            get { return m_ImgName; }
            set { m_ImgName = value; }
        }
        public string TextAxis
        {
            get { return m_TextAxis; }
            set { m_TextAxis = value; }
        }
        public string PhotoAxis
        {
            get { return m_PhotoAxis; }
            set { m_PhotoAxis = value; }
        }
        public string BarCodeAxis
        {
            get { return m_BarCodeAxis; }
            set { m_BarCodeAxis = value; }
        }

        #endregion
    }

    #endregion
    //FB 2136 Ends
    
    #endregion
}
