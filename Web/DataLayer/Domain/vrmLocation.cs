/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description for Locations/Rooms.
	/// </summary>
	public class vrmTier
	{
		public vrmTier()
		{

		}
	}
	public class vrmTier2
	{
		#region Private Internal Members
	
		private int m_id, m_l3locationId, m_disabled, m_orgID;//Code added for organization
		private string m_name, m_address, m_comment;
        
        private vrmTier3 m_tier3; 
        private IList m_room;
      
		//private IList m_roomList;
        private int m_lowLevelCount;

		#endregion
		
		#region Public Properties

		public int ID
		{
			get {return m_id;}
			set {m_id = value;}
		}
		public int L3LocationId
		{
			get {return m_l3locationId;}
			set {m_l3locationId = value;}
        }
		public string Name
		{
			get {return m_name;}
			set {m_name = value;}
		}
		public string Comment
		{
			get {return m_comment;}
			set
			{
				if(value == null) m_comment = string.Empty;
				else m_comment = value;
			}
		}		
		public string Address
		{
			get {return m_address;}
			set
			{
				if(value == null) m_address = string.Empty;
				else m_address = value;
			}
		}		
		public int disabled
		{
			get {return m_disabled;}
			set {m_disabled = value;}
		}

        public int orgId //Code added for organization
		{
			get {return m_orgID;}
			set {m_orgID = value;}
		}


        //public IList roomList 
        //{
        //    get {return m_roomList;}
        //    set {m_roomList = value;}
        //}
        public int lowLevelCount
        {
            get { return m_lowLevelCount; }
            set { m_lowLevelCount = value; }
        }

        public vrmTier3 tier3
        {
            get { return m_tier3; }
            set { m_tier3 = value; }
        }
        public IList room
        {
            get { return m_room; }
            set { m_room = value; }
        }
      
		#endregion		
	}
	public class vrmTier3
	{
		#region Private Internal Members

        private int m_id, m_disabled, m_orgID;//Code added for organization
		private string m_name, m_address;
        private int m_lowLevelCount;
        private IList m_tier2;
     
		//private IList m_tier2List;

		#endregion
		
		#region Public Properties

		public int ID
		{
			get {return m_id;}
			set {m_id = value;}
		}
		public string Name
		{
			get {return m_name;}
			set {m_name = value;}
		}
		public string Address
		{
			get {return m_address;}
			set
			{
				if(value == null) m_address = string.Empty;
				else m_address = value;
			}
		}		
		public int disabled
		{
			get {return m_disabled;}
			set {m_disabled = value;}
		}
        //public IList tier2List
        //{
        //    get { return m_tier2List; }
        //    set { m_tier2List = value; }
        //}
        public int lowLevelCount
        {
            get { return m_lowLevelCount; }
            set { m_lowLevelCount = value; }
        }
        public IList tier2
        {
            get { return m_tier2; }
            set { m_tier2 = value; }
        }

        public int orgId //Code added for organization
		{
			get {return m_orgID;}
			set {m_orgID = value;}
		}

		#endregion		
	}

	public class vrmRoom
	{
	    private int m_roomId;
		private int m_projectorAvailable,m_maxPhoneCall, m_l2locationId,m_l3locationId;
		private int m_capacity, m_videoAvailable, m_disabled;
        private int m_endpointid, m_adminId, m_assistant, m_defaultEquipmentid, m_LastModifiedUser; //FB 2724
        private string m_name, m_roomBuilding, m_roomFloor, m_roomNumber, m_roomPhone, m_RoomQueue, m_RoomUID, m_RoomToken; //FB 2342 //ZD 100196
        private string m_assistantPhone;
        private string m_dynamicRoomLayout, m_caterer;
        private int m_responsetime;
        private string m_responsemessage, m_roomimage;
        private int m_setuptime, m_teardowntime, m_costcenterid; 
        private int m_orgId;    //Organization
        
        private string m_notifyemails;
        private string m_address1,m_address2;
	    private string m_city, m_zipcode;
        private int m_state, m_country;
	    private string m_maplink,m_parkingDirections, m_additionalComments;
	    private int m_timezoneID;
        private string m_latitude, m_longitude;
        private string m_mapImage1, m_mapImage2, m_securityImage1, m_secutrityImage2, m_miscImage1, m_miscImage2;
	    private string  m_auxattachments, m_custom1, m_custom2, m_custom3, m_custom4;
        private string m_custom5, m_custom6, m_custom7, m_custom8, m_custom9, m_custom10;
        private vrmTier2 m_tier2;
        private IList<vrmLocApprover> m_locApprover;
        private IList<vrmLocDepartment> m_locDept;
        private DateTime m_modifiedDate;
        private int m_handicappedaccess;
        public bool b_display;
        private int m_MapImage1Id, m_MapImage2Id, m_SecurityImage1Id, m_SecurityImage2Id, m_MiscImage1Id, m_MiscImage2Id, m_Extroom; //FB 2426
        private String m_RoomImageId, m_DedicatedVideo, m_DedicatedCodec, m_AVOnsiteSupportEmail;//FB 2334 //FB 2390 //FB 2415
        private int m_isVIP, m_IsVMR; //FB 2448
        public bool m_display = true; //FB 2027
		private string m_InternalNumber, m_ExternalNumber; //FB 2448
        private int m_isTelepresence, m_serviceType, m_isPublic;//FB 2170, FB 2219 //FB 2392 -WhyGo
        //FB 2599 Start
        //FB 2262
        private string m_Extension, m_Type, m_VidyoURL, m_Pin;
        private int m_EntityID, m_OwnerID, m_isLocked, m_allowCallDirect, m_MemberID, m_allowPersonalMeeting, m_RoomCategory; //FB 2694
        //FB 2599 End
        private string m_VMRLink;//FB 2727
        private int m_RoomIconTypeId; //FB 2065
        public Boolean display
        {
            get { return m_display; }
            set { m_display = value; }
        }

		public int roomId
		{
			get {return m_roomId;}
			set {m_roomId = value;}
		}
        public int RoomID
        {
            get { return m_roomId; }
            set { m_roomId = value; }
        }
        public String Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public String RoomBuilding
        {
            get { return m_roomBuilding; }
            set { m_roomBuilding = value; }
        }
        public String RoomFloor
        {
            get { return m_roomFloor; }
            set { m_roomFloor = value; }
        }
        public String RoomNumber
        {
            get { return m_roomNumber; }
            set { m_roomNumber = value; }
        }
        public String RoomPhone
        {
            get { return m_roomPhone; }
            set { m_roomPhone = value; }
        }

        public int Capacity
        {
            get { return m_capacity; }
            set { m_capacity = value; }
        }
        public String AssistantPhone
        {
            get { return m_assistantPhone; }
            set { m_assistantPhone = value; }
        }
        public int DefaultEquipmentid
        {
            get { return m_defaultEquipmentid; }
            set { m_defaultEquipmentid = value; }
        }
        public String DynamicRoomLayout
        {
            get { return m_dynamicRoomLayout; }
            set { m_dynamicRoomLayout = value; }
        }
        public String Caterer
        {
            get { return m_caterer; }
            set { m_caterer = value; }
        }
        public int Disabled
        {
            get { return m_disabled; }
            set { m_disabled = value; }
        }
        public String ResponseMessage
        {
            get { return m_responsemessage; }
            set { m_responsemessage = value; }
        }
        public String RoomImage
        {
            get { return m_roomimage; }
            set { m_roomimage = value; }
        }
        public int TimezoneID
        {
            get { return m_timezoneID; }
            set { m_timezoneID = value; }
        }
        public int ResponseTime
        {
            get { return m_responsetime; }
            set { m_responsetime = value; }
        }
        public int SetupTime
        {
            get { return m_setuptime; }
            set { m_setuptime = value; }
        }
        public int TeardownTime
        {
            get { return m_teardowntime; }
            set { m_teardowntime = value; }
        }
        public int CostCenterid
        {
            get { return m_costcenterid; }
            set { m_costcenterid = value; }
        }
        

        public int ProjectorAvailable
		{
			get {return m_projectorAvailable;}
			set {m_projectorAvailable = value;}
		}
		public int MaxPhoneCall
		{
			get {return m_maxPhoneCall;}
			set {m_maxPhoneCall = value;}
		}
		public int L2LocationId
		{
			get {return m_l2locationId;}
			set {m_l2locationId = value;}
		}
		public int L3LocationId
		{
			get {return m_l3locationId;}
			set {m_l3locationId = value;}
		}
		public int endpointid
		{
			get {return m_endpointid;}
			set {m_endpointid = value;}
		}
		public int VideoAvailable
		{
			get {return m_videoAvailable;}
			set {m_videoAvailable = value;}
		}
		public int disabled
		{
			get {return m_disabled;}
			set {m_disabled = value;}
		}
		public int adminId
		{
			get {return m_adminId;}
			set {m_adminId = value;}
		}
        public int assistant
		{
			get {return m_assistant;}
			set {m_assistant = value;}
		}
        public string notifyemails
        {
            get { return m_notifyemails; }
            set { m_notifyemails = value; }
        }
        // 
        public string Address1
        {
            get { return m_address1; }
            set { m_address1 = value; }
        }
        public string Address2
        {
            get { return m_address2; }
            set { m_address2 = value; }
        }
        public string City
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public int State
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public int Country
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string Zipcode
        {
            get { return m_zipcode; }
            set { m_zipcode = value; }
        }
        public string Maplink
        {
            get { return m_maplink; }
            set { m_maplink = value; }
        }
        public string ParkingDirections
        {
            get { return m_parkingDirections; }
            set { m_parkingDirections = value; }
        }
        public string AdditionalComments
        {
            get { return m_additionalComments; }
            set { m_additionalComments = value; }
        }
        public string Longitude
        {
            get { return m_longitude; }
            set { m_longitude = value; }
        }
        public string Latitude
        {
            get { return m_latitude; }
            set { m_latitude = value; }
        }
        public string AuxAttachments
        {
            get { return m_auxattachments; }
            set { m_auxattachments = value; }
        }
        public string MapImage1
        {
            get { return m_mapImage1; }
            set { m_mapImage1 = value; }
        }
        public string MapImage2
        {
            get { return m_mapImage2; }
            set { m_mapImage2 = value; }
        }
        public string SecurityImage1
        {
            get { return m_securityImage1; }
            set { m_securityImage1 = value; }
        }
        public string SecurityImage2
        {
            get { return m_secutrityImage2; }
            set { m_secutrityImage2 = value; }
        }
        public string MiscImage1
        {
            get { return m_miscImage1; }
            set { m_miscImage1 = value; }
        }
        public string MiscImage2
        {
            get { return m_miscImage2; }
            set { m_miscImage2 = value; }
        }
        public string Custom1
        {
            get { return m_custom1; }
            set { m_custom1 = value; }
        }
        public string Custom2
        {
            get { return m_custom2; }
            set { m_custom2 = value; }
        }
        public string Custom3
        {
            get { return m_custom3; }
            set { m_custom3 = value; }
        }
        public string Custom4
        {
            get { return m_custom4; }
            set { m_custom4 = value; }
        }
        public string Custom5
        {
            get { return m_custom5; }
            set { m_custom5 = value; }
        }
        public string Custom6
        {
            get { return m_custom6; }
            set { m_custom6 = value; }
        }
        public string Custom7
        {
            get { return m_custom7; }
            set { m_custom7 = value; }
        }
        public string Custom8
        {
            get { return m_custom8; }
            set { m_custom8 = value; }
        }
        public string Custom9
        {
            get { return m_custom9; }
            set { m_custom9 = value; }
        }
        public string Custom10
        {
            get { return m_custom10; }
            set { m_custom10 = value; }
        }
        public int MapImage1Id //Image Project
        {
            get { return m_MapImage1Id; }
            set { m_MapImage1Id = value; }
        }
        public int MapImage2Id //Image Project
        {
            get { return m_MapImage2Id; }
            set { m_MapImage2Id = value; }
        }
        public int SecurityImage1Id //Image Project
        {
            get { return m_SecurityImage1Id; }
            set { m_SecurityImage1Id = value; }
        }
        public int SecurityImage2Id //Image Project
        {
            get { return m_SecurityImage2Id; }
            set { m_SecurityImage2Id = value; }
        }
        public int MiscImage1Id //Image Project
        {
            get { return m_MiscImage1Id; }
            set { m_MiscImage1Id = value; }
        }
        public int MiscImage2Id //Image Project
        {
            get { return m_MiscImage2Id; }
            set { m_MiscImage2Id = value; }
        }
        public String RoomImageId //Image Project
        {
            get { return m_RoomImageId; }
            set { m_RoomImageId = value; }
        }
        public vrmTier2 tier2
		{
			get {return m_tier2;}
			set {m_tier2 = value;}
		}

        public DateTime Lastmodifieddate
        {
            get { return m_modifiedDate; }
            set { m_modifiedDate = value; }
        }

        public IList<vrmLocApprover> locationApprover
        {
            get { return m_locApprover; }
            set { m_locApprover = value; }
        }
        public IList<vrmLocDepartment> locationDept
        {
            get { return m_locDept; }
            set { m_locDept = value; }
        }
        

        public vrmRoom()
        {
            b_display = true;
	        m_locApprover = new List<vrmLocApprover>();
            m_locDept = new List<vrmLocDepartment>();
        }

        //Added for Organization - Start
        public int orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        //Added for Organization - End

        public int HandiCappedAccess
        {
            get { return m_handicappedaccess; }
            set { m_handicappedaccess = value; }
        }

        public int isVIP
        {
            get { return m_isVIP; }
            set { m_isVIP = value; }
        }
        //FB 2170
        public int isTelepresence
        {
            get { return m_isTelepresence; }
            set { m_isTelepresence = value; }
        }
        //FB 2219
        public int ServiceType
        {
            get { return m_serviceType; }
            set { m_serviceType = value; }
        }
        //FB 2334
        public string DedicatedVideo
        {
            get { return m_DedicatedVideo; }
            set { m_DedicatedVideo = value; }
        }
        // FB 2342
        public string RoomQueue   
        {
            get { return m_RoomQueue; }
            set { m_RoomQueue =value;}
           
        }
        //ZD 100196 Start
        public string RoomUID
        {
            get { return m_RoomUID; }
            set { m_RoomUID = value; }

        }
        //ZD 100196 End
        public string RoomToken
        {
            get { return m_RoomToken; }
            set { m_RoomToken = value; }

        }
        public int LastModifiedUser
        {
            get { return m_LastModifiedUser; }
            set { m_LastModifiedUser = value; }

        }
        // FB 2724 End
        //FB 2390
        public string DedicatedCodec
        {
            get { return m_DedicatedCodec; }
            set { m_DedicatedCodec = value; }
        }
        //FB 2415
        public string AVOnsiteSupportEmail
        {
            get { return m_AVOnsiteSupportEmail; }
            set { m_AVOnsiteSupportEmail = value; }
        }
        public int isPublic   //FB 2392 - WhyGo
        {
            get { return m_isPublic; }
            set { m_isPublic = value; }

        }
        //FB 2426 Start
        public int Extroom
        {
            get { return m_Extroom; }
            set { m_Extroom = value; }
        }
        //FB 2426 End
		//FB 2448 Start
        public int IsVMR
        {
            get { return m_IsVMR; }
            set { m_IsVMR = value; }
        }
        public string InternalNumber
        {
            get { return m_InternalNumber; }
            set { m_InternalNumber = value; }
        }
        public string ExternalNumber
        {
            get { return m_ExternalNumber; }
            set { m_ExternalNumber = value; }
        }
        //FB 2448 End

        //FB 2262 //FB 2599 - Starts
        public int EntityID
        {
            get { return m_EntityID; }
            set { m_EntityID = value; }
        }
        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public int OwnerID
        {
            get { return m_OwnerID; }
            set { m_OwnerID = value; }
        }
        public string Extension
        {
            get { return m_Extension; }
            set { m_Extension = value; }
        }
        public string VidyoURL
        {
            get { return m_VidyoURL; }
            set { m_VidyoURL = value; }
        }
        public string Pin
        {
            get { return m_Pin; }
            set { m_Pin = value; }
        }
        public int isLocked
        {
            get { return m_isLocked; }
            set { m_isLocked = value; }
        }
        public int allowCallDirect
        {
            get { return m_allowCallDirect; }
            set { m_allowCallDirect = value; }
        }
        public int MemberID
        {
            get { return m_MemberID; }
            set { m_MemberID = value; }
        }
        public int allowPersonalMeeting
        {
            get { return m_allowPersonalMeeting; }
            set { m_allowPersonalMeeting = value; }
        }
        //FB 2262 //FB 2599 - End

        //FB 2694 Starts
        public int RoomCategory 
        {
            get { return m_RoomCategory; }
            set { m_RoomCategory = value; }
        }
        //FB 2694 Ends
        //FB 2727
        public string VMRLink
        {
            get { return m_VMRLink; }
            set { m_VMRLink = value; }
        }
        //FB 2065
        public int RoomIconTypeId
        {
            get { return m_RoomIconTypeId; }
            set { m_RoomIconTypeId = value; }
        }
        
	}

    public class vrmLocApprover
    {
        #region Private Internal Members
        private int m_id;
        private int m_roomid;
        private int m_approverid;

        #endregion

        #region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int roomid
        {
            get { return m_roomid; }
            set { m_roomid = value; }
        }
        public int approverid
        {
            get { return m_approverid; }
            set { m_approverid = value; }
        }

        #endregion
    } 
   
    public class vrmLocDepartment
    {
        #region Private Internal Members
        private int m_id,m_roomId, m_departmentId;
        #endregion

        #region Public Properties

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int roomId
        {
            get { return m_roomId; }
            set { m_roomId = value; }
        }
        public int departmentId
        {
            get { return m_departmentId; }
            set { m_departmentId = value; }
        }
        #endregion
    }

    //FB 2392 - WhyGO
    #region ES Public Room
    /// <summary>
    /// ESPublicRoom
    /// </summary>
    public class ESPublicRoom
    {
          #region Parameter
          private int m_roomId, m_WhygoRoomId;
          private String m_address, m_AUXEquipment, m_CateringOptionsAvailable, m_Layout, m_ExtraNotes, m_CurrencyType, m_geoCodeAddress, m_internetBiller, m_internetPriceCurrency, m_RoomDescription, m_URL;
          private String m_EHStartTime, m_EHEndTime, m_AHStartTime, m_AHEndTime, m_openHours, m_OHStartTime, m_OHEndTime, m_Type,m_CHtartTime,m_CHEndTime;
          private int m_isAutomatic,m_isHDCapable ,m_isInternetCapable,m_isInternetFree,m_isIPCapable,m_isIPCConnectionCapable;
          private int m_isIPDedicated,m_isTP ,m_isVCCapable,m_IsEarlyHoursEnabled,m_IsAfterHourEnabled,m_Is24HoursEnabled,m_isCrazyHoursSupported;
          private decimal m_genericSellPrice, m_EHCost, m_AHCost, m_OHCost, m_CHCost;
          private String m_SitecordinatorEmail, m_SitecordinatorName, m_SitecordinatorPhone, m_ManagerEmail, m_ManagerName, m_ManagerPhone, m_TechnicalContactEmail, m_TechnicalContactName, m_TechnicalContactPhone;
          private int m_SitecordinatorID, m_ManagerID, m_TechnicalContactID;
          private int m_IPSpeed, m_ISDNSpeed, m_isISDNCapable, m_EHFullyAuto, m_AHFullyAuto, m_CHFullyAuto; //FB 2543
          private String m_IPAddress, m_ISDNNumber, m_DefaultEquipment, m_MapLink;
          private DateTime m_PublicRoomLastModified;
          private String m_City, m_StateName, m_Country;
          #endregion

          #region Properties
          public int RoomID
          {
              get { return m_roomId; }
              set { m_roomId = value; }
          }
          public int WhygoRoomId
          {
              get { return m_WhygoRoomId; }
              set { m_WhygoRoomId = value; }
          }
          public string address
          {
            get { return m_address; }
            set { m_address = value; }
          }
          public string RoomDescription
          {
              get { return m_RoomDescription; }
              set { m_RoomDescription = value; }
          }
          public string ExtraNotes
          {
              get { return m_ExtraNotes; }
              set { m_ExtraNotes = value; }
          }
          public string Type
          {
              get { return m_Type; }
              set { m_Type = value; }
          }
          public string AUXEquipment
          {
            get { return m_AUXEquipment; }
            set { m_AUXEquipment = value; }
          }
          public string CateringOptionsAvailable
          {
              get { return m_CateringOptionsAvailable; }
              set { m_CateringOptionsAvailable = value; }
          }
          public string Layout
          {
              get { return m_Layout; }
              set { m_Layout = value; }
          }
          public string CurrencyType
          {
            get { return m_CurrencyType; }
            set { m_CurrencyType = value; }
          }
          public decimal genericSellPrice
          {
            get { return m_genericSellPrice; }
            set { m_genericSellPrice = value; }
          }
          public string geoCodeAddress
          {
            get { return m_geoCodeAddress; }
            set { m_geoCodeAddress = value; }
          }
          public string internetBiller
          {
            get { return m_internetBiller; }
            set { m_internetBiller = value; }
          }
          public string internetPriceCurrency
          {
              get { return m_internetPriceCurrency; }
              set { m_internetPriceCurrency = value; }
          }
          public int isAutomatic
          {
              get { return m_isAutomatic; }
              set { m_isAutomatic = value; }
          }
          public int isHDCapable 
          {
              get { return m_isHDCapable; }
              set { m_isHDCapable = value; }
          }
          public int isInternetCapable
          {
              get { return m_isInternetCapable; }
              set { m_isInternetCapable = value; }
          }
          public int isInternetFree
          {
              get { return m_isInternetFree; }
              set { m_isInternetFree = value; }
          }
          public int isIPCapable
          {
              get { return m_isIPCapable; }
              set { m_isIPCapable = value; }
          }
          public int isIPCConnectionCapable
          {
              get { return m_isIPCConnectionCapable; }
              set { m_isIPCConnectionCapable = value; }
          }
          public int isIPDedicated
          {
              get { return m_isIPDedicated; }
              set { m_isIPDedicated = value; }
          }
          public int isTP 
          {
              get { return m_isTP; }
              set { m_isTP = value; }
          }
          public int isVCCapable
          {
              get { return m_isVCCapable; }
              set { m_isVCCapable = value; }
          }
          public int IsEarlyHoursEnabled
          {
              get { return m_IsEarlyHoursEnabled; }
              set { m_IsEarlyHoursEnabled = value; }
          }
          public string EHStartTime
          {
              get { return m_EHStartTime; }
              set { m_EHStartTime = value; }
          }
          public string EHEndTime
          {
              get { return m_EHEndTime; }
              set { m_EHEndTime = value; }
          }
          public decimal EHCost
          {
              get { return m_EHCost; }
              set { m_EHCost = value; }
          }
          public int IsAfterHourEnabled
          {
              get { return m_IsAfterHourEnabled; }
              set { m_IsAfterHourEnabled = value; }
          }
          public string AHStartTime
          {
              get { return m_AHStartTime; }
              set { m_AHStartTime = value; }
          }
          public string AHEndTime
          {
              get { return m_AHEndTime; }
              set { m_AHEndTime = value; }
          }
          public decimal AHCost
          {
              get { return m_AHCost; }
              set { m_AHCost = value; }
          }
          public string openHours
          {
              get { return m_openHours; }
              set { m_openHours = value; }
          }
          public string OHStartTime
          {
              get { return m_OHStartTime; }
              set { m_OHStartTime = value; }
          }
          public string OHEndTime
          {
              get { return m_OHEndTime; }
              set { m_OHEndTime = value; }
          }
          public decimal OHCost
          {
              get { return m_OHCost; }
              set { m_OHCost = value; }
          }
          public int isCrazyHoursSupported
          {
              get { return m_isCrazyHoursSupported; }
              set { m_isCrazyHoursSupported = value; }
          }
          public string CHtartTime
          {
              get { return m_CHtartTime; }
              set { m_CHtartTime = value; }
          }
          public string CHEndTime
          {
              get { return m_CHEndTime; }
              set { m_CHEndTime = value; }
          }
          public decimal CHCost
          {
              get { return m_CHCost; }
              set { m_CHCost = value; }
          }
          public int Is24HoursEnabled
          {
              get { return m_Is24HoursEnabled; }
              set { m_Is24HoursEnabled = value; }
          }
          public int SitecordinatorID
          {
              get { return m_SitecordinatorID; }
              set { m_SitecordinatorID = value; }
          }
          public string SitecordinatorEmail
          {
              get { return m_SitecordinatorEmail; }
              set { m_SitecordinatorEmail = value; }
          }
          public string SitecordinatorName
          {
              get { return m_SitecordinatorName; }
              set { m_SitecordinatorName = value; }
          }
          public string SitecordinatorPhone
          {
              get { return m_SitecordinatorPhone; }
              set { m_SitecordinatorPhone = value; }
          }
          public int ManagerID
          {
              get { return m_ManagerID; }
              set { m_ManagerID = value; }
          }
          public string ManagerName
          {
              get { return m_ManagerName; }
              set { m_ManagerName = value; }
          }
          public string ManagerPhone
          {
              get { return m_ManagerPhone; }
              set { m_ManagerPhone = value; }
          }
          public string ManagerEmail
          {
              get { return m_ManagerEmail; }
              set { m_ManagerEmail = value; }
          }
          public int TechnicalContactID
          {
              get { return m_TechnicalContactID; }
              set { m_TechnicalContactID = value; }
          }
          public string TechnicalContactEmail
          {
              get { return m_TechnicalContactEmail; }
              set { m_TechnicalContactEmail = value; }
          }
          public string TechnicalContactName
          {
              get { return m_TechnicalContactName; }
              set { m_TechnicalContactName = value; }
          }
          public string TechnicalContactPhone
          {
              get { return m_TechnicalContactPhone; }
              set { m_TechnicalContactPhone = value; }
          }
          public DateTime PublicRoomLastModified
          {
              get { return m_PublicRoomLastModified; }
              set { m_PublicRoomLastModified = value; }
          }
          public int IPSpeed
          {
              get { return m_IPSpeed; }
              set { m_IPSpeed = value; }
          }
          public string IPAddress
          {
              get { return m_IPAddress; }
              set { m_IPAddress = value; }
          }
          public int ISDNSpeed
          {
              get { return m_ISDNSpeed; }
              set { m_ISDNSpeed = value; }
          }
          public string ISDNNumber
          {
              get { return m_ISDNNumber; }
              set { m_ISDNNumber = value; }
          }
          public string URL
          {
              get { return m_URL; }
              set { m_URL = value; }
          }
          public string City
          {
              get { return m_City; }
              set { m_City = value; }
          }
          public string StateName
          {
              get { return m_StateName; }
              set { m_StateName = value; }
          }
          public string Country
          {
              get { return m_Country; }
              set { m_Country = value; }
          }
          public string DefaultEquipment
          {
              get { return m_DefaultEquipment; }
              set { m_DefaultEquipment = value; }
          }
          public int isISDNCapable
          {
              get { return m_isISDNCapable; }
              set { m_isISDNCapable = value; }
          }
          public string MapLink
          {
              get { return m_MapLink; }
              set { m_MapLink = value; }
          }
          // FB 2543 Starts
          public int AHFullyAuto
          {
              get { return m_AHFullyAuto; }
              set { m_AHFullyAuto = value; }
          }
          public int CHFullyAuto
          {
              get { return m_CHFullyAuto; }
              set { m_CHFullyAuto = value; }
          }
          public int EHFullyAuto
          {
              get { return m_EHFullyAuto; }
              set { m_EHFullyAuto = value; }
          }
        //FB 2543 Ends


         #endregion

    }
    #endregion
}
