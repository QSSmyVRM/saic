﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Xml;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;

using log4net;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Summary description for user table Comp_List_D 
    /// </summary>
    public class vrmOrganization
    {
        #region Private Internal Members

        private int m_OrgId;
        private string m_orgname;
        private int m_Deleted;
        private string m_address1, m_address2;
        private string m_city, m_zipcode;
        private int m_state, m_country;
        private int m_disabled, m_orgStatus;//FB 2678
        private string m_phone, m_fax, m_website, m_email, m_orgExpiryKey;//FB 2678
      
        private static ArrayList m_OrgList = null;

        #endregion

        #region Public Properties

        public int orgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public string orgname
        {
            get { return m_orgname; }
            set { m_orgname = value; }
        }
        public string address1
        {
            get { return m_address1; }
            set { m_address1 = value; }
        }
        public string address2
        {
            get { return m_address2; }
            set { m_address2 = value; }
        }
        public string city
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public int state
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public int country
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string zipcode
        {
            get { return m_zipcode; }
            set { m_zipcode = value; }
        }
        public int deleted
        {
            get { return m_Deleted; }
            set { m_Deleted = value; }
        }
        public int disabled
        {
            get { return m_disabled; }
            set { m_disabled = value; }
        }
        public String phone
        {
            get { return m_phone; }
            set { m_phone = value; }
        }
        public String fax
        {
            get { return m_fax; }
            set { m_fax = value; }
        }
        public String email
        {
            get { return m_email; }
            set { m_email = value; }
        }
        public String website
        {
            get { return m_website; }
            set { m_website = value; }
        }
        public String orgExpiryKey //FB 2678
        {
            get { return m_orgExpiryKey; }
            set { m_orgExpiryKey = value; }
        }
        public int orgStatus //FB 2678
        {
            get { return m_orgStatus; }
            set { m_orgStatus = value; }
        }
        #endregion

        #region Init
        public static bool Init(ref ISession session)
        {
            try
            {
                m_OrgList = new ArrayList();

                m_OrgList = (ArrayList)(session.CreateCriteria(typeof(vrmOrganization))
                                        .AddOrder(Order.Asc("Id"))
                                        .List());
                if (m_OrgList == null || m_OrgList.Count == 0)
                    throw new Exception("Error in Organization List initialization");
            }
            catch (Exception e)
            {
                session.Disconnect();
                throw e;
            }
            return true;
        }
        #endregion

        #region GetOrgNames

        public static List<vrmOrganization> getOrgNames()
        {
            return new List<vrmOrganization>(m_OrgList.ToArray(typeof(vrmOrganization))
              as vrmOrganization[]);
        }
        #endregion

        #region holidays

        public class holidays
        {
            private int m_uid;
            private DateTime m_Date;
            private int m_OrgId, m_EntityID, m_EnitityType;
            private string m_Colour;
            private int m_HolidayType;

            public int UID
            {
                get { return m_uid; }
                set { m_uid = value; }
            }
            public int OrgId
            {
                get { return m_OrgId; }
                set { m_OrgId = value; }
            }
            public int EntityID
            {
                get { return m_EntityID; }
                set { m_EntityID = value; }
            }
            public int EnitityType
            {
                get { return m_EnitityType; }
                set { m_EnitityType = value; }
            }
            public int HolidayType
            {
                get { return m_HolidayType; }
                set { m_HolidayType = value; }
            }
            public String Colour
            {
                get { return m_Colour; }
                set { m_Colour = value; }
            }
            public DateTime Date
            {
                get { return m_Date; }
                set { m_Date = value; }
            }

        }

        #endregion
    }
}
