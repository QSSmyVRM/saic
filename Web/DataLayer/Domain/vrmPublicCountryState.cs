﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
namespace myVRM.DataLayer
{
	public class vrmPublicCountryState
	{
		#region Private Internal Members
		private int m_stateID;
		private string m_stateName;
		private int m_countryID;
		#endregion

		#region Public Properties
		public int StateID
		{
			get { return m_stateID; }
			set { m_stateID = value; }
		}
		public string StateName
		{
			get { return m_stateName; }
			set { m_stateName = value; }
		}
		public int CountryID
		{
			get { return m_countryID; }
			set { m_countryID = value; }
		}
		#endregion
	}
}
