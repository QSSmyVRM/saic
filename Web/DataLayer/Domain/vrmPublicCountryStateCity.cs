﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
namespace myVRM.DataLayer
{
	public class vrmPublicCountryStateCity
	{
		#region Private Internal Members
		private int m_cityID;
		private string m_cityName;
		private int m_countryID;
		private int m_stateID;
		#endregion

		#region Public Properties
		public int CityID
		{
			get { return m_cityID; }
			set { m_cityID = value; }
		}
		public string CityName
		{
			get { return m_cityName; }
			set { m_cityName = value; }
		}
		public int CountryID
		{
			get { return m_countryID; }
			set { m_countryID = value; }
		}
		public int StateID
		{
			get { return m_stateID; }
			set { m_stateID = value; }
		}
		#endregion
	}
}
