/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace myVRM.DataLayer
{
    public class vrmTemplate : vrmConf
    {
     	#region Private Internal Members
		
		private int      m_tmpId;
        private string   m_TmpName, m_TMPDescription;
        private int      m_TMPOwner, m_TMPPublic; 
        private string   m_TmpDescription;
		private int      m_partynotify, m_location;
		private string   m_confDescription;
		private int      m_Orderid, m_priority;
        private int      m_priorityType;
        private int      m_orgId;
	   
		#endregion
		
		#region Public Properties
        
        
        public int tmpId
		{
            get { return m_tmpId; }
            set { m_tmpId = value; }
		}	
		public string TmpName
		{
            get { return m_TmpName; }
            set { m_TmpName = value; }
		}
        public string TMPDescription
		{
            get { return m_TMPDescription; }
            set { m_TMPDescription = value; }
		}	
        public int TMPOwner
		{
            get { return m_TMPOwner; }
            set { m_TMPOwner = value; }
		}
        public string TmpDescription
        {
            get { return m_TmpDescription; }
            set { m_TmpDescription = value; }
        }
        public int TMPPublic
        {
            get { return m_TMPPublic; }
            set { m_TMPPublic = value; }
        }
        public int partynotify
		{
            get { return m_partynotify; }
            set { m_partynotify = value; }
		}
        public int location
        {
            get { return m_location; }
            set { m_location = value; }
        }
        public int Orderid
        {
            get { return m_Orderid; }
            set { m_Orderid = value; }
        }
        public int priority
        {
            get { return m_priority; }
            set { m_priority = value; }
        }
         public int priorityType
        {
            get { return m_priorityType; }
            set { m_priorityType = value; }
        }	
        public string confDescription
        {
            get { return m_confDescription; }
            set { m_confDescription = value; }
        }

        public int orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
     
        #endregion
        #region Private Relationships
        private IList m_vrmTempAdvAvParams;
        private IList m_vrmTempRoom;
        private IList m_vrmTempUser;
        private IList m_vrmTempGroup; //FB 2027
        #endregion

        #region Public Relationships Properties

        public IList TempAdvAvParams
        {
            get { return m_vrmTempAdvAvParams; }
            set { m_vrmTempAdvAvParams = value; }
        }
        public IList TempRoom
        {
            get { return m_vrmTempRoom; }
            set { m_vrmTempRoom = value; }
        }
        public IList TempUser
        {
            get { return m_vrmTempUser; }
            set { m_vrmTempUser = value; }
        }
        public IList TempGroup //FB 2027
        {
            get { return m_vrmTempGroup; }
            set { m_vrmTempGroup = value; }
        }

        #endregion
        public vrmTemplate()
        {
            m_tmpId = 0;
            m_vrmTempAdvAvParams = new ArrayList();
            m_vrmTempRoom = new ArrayList();
            m_vrmTempUser = new ArrayList();
            m_vrmTempGroup = new ArrayList(); //FB 2027
        }

    }
    public class vrmTempRoom
    {

        #region Private Internal Members
        private int m_Id; //FB 2027
        private int m_tmpId;
        private int m_RoomId;
        private int m_DefLineRate;
        private int m_DefVideoProtocol;
        private int m_Duration;
       
        #endregion

        #region Public Properties
        //FB 2027 Start
        public int ID
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        //FB 2027 End
        public int tmpId
        {
            get { return m_tmpId; }
            set { m_tmpId = value; }
        }

        public int RoomID
        {
            get { return m_RoomId; }
            set { m_RoomId = value; }
        }
        public int DefLineRate
        {
            get { return m_DefLineRate; }
            set { m_DefLineRate = value; }
        }
        public int DefVideoProtocol
        {
            get { return m_DefVideoProtocol; }
            set { m_DefVideoProtocol = value; }
        }
        public int Duration
        {
            get { return m_Duration; }
            set { m_Duration = value; }
        }
        #endregion
    }
    public class vrmTempAdvAvParams : vrmAdvAvParams
    {
        #region Private Internal Members

        private int m_tmpId;
   
        #endregion

        #region Public Properties

        public int tmpId
        {
            get { return m_tmpId; }
            set { m_tmpId = value; }
        }
        #endregion
    }
    //FB 2027 start
    public class vrmTempUser 
    {
        #region Private Internal Members
        private int m_ID;
        private int m_TmpID;
        private int m_userid;
        private int m_DefLineRate;
        private int m_partyphoneNumber;
        private int m_roomAssistant;
        private int m_VideoEquipmentID;
        private int m_status;
        private int m_invitee;
        private int m_roomID;
        private int m_defVideoProtocol;
        private int m_ipAddress;
        private int m_connectionType;
        private int m_audioOrVideo;
        private string m_IPISDNAddress; //FB 3041
        private int m_AddressType; //FB 3041
        private int m_interfaceType;
        private int m_partyNotify;
        private int m_survey;//FB 2348
        #endregion

        #region Public Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int TmpID
        {
            get { return m_TmpID; }
            set { m_TmpID = value; }
        }
        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public int status
        {
            get { return m_status; }
            set { m_status = value; }
        }
        public int invitee
        {
            get { return m_invitee; }
            set { m_invitee = value; }
        }
        public int roomID
        {
            get { return m_roomID; }
            set { m_roomID = value; }
        }
        public int defVideoProtocol
        {
            get { return m_defVideoProtocol; }
            set { m_defVideoProtocol = value; }
        }
        public int ipAddress
        {
            get { return m_ipAddress; }
            set { m_ipAddress = value; }
        }
        //FB 3041 Start
        public string IPISDNAddress
        {
            get { return m_IPISDNAddress; }
            set { m_IPISDNAddress = value; }
        }
        public int AddressType
        {
            get { return m_AddressType; }
            set { m_AddressType = value; }
        }
        //FB 3041 End
        public int connectionType
        {
            get { return m_connectionType; }
            set { m_connectionType = value; }
        }
        public int interfaceType
        {
            get { return m_interfaceType; }
            set { m_interfaceType = value; }
        }
        public int partyNotify
        {
            get { return m_partyNotify; }
            set { m_partyNotify = value; }
        }
        public int DefLineRate
        {
            get { return m_DefLineRate; }
            set { m_DefLineRate = value; }
        }
        public int audioOrVideo
        {
            get { return m_audioOrVideo; }
            set { m_audioOrVideo = value; }
        }
        public int partyphoneNumber
        {
            get { return m_partyphoneNumber; }
            set { m_partyphoneNumber = value; }
        }
        public int roomAssistant
        {
            get { return m_roomAssistant; }
            set { m_roomAssistant = value; }
        }
        public int VideoEquipmentID
        {
            get { return m_VideoEquipmentID; }
            set { m_VideoEquipmentID = value; }
        }
        public int Survey //FB 2348
        {
            get { return m_survey; }
            set { m_survey = value; }
        }
        #endregion

        //FB 2027 End

        #region Private Relationships
        private vrmUser m_User;
        #endregion

        #region Public Relationships Properties
        public vrmUser User
        {
            get { return m_User; }
            set { m_User = value; }
        }
        #endregion
    }

    //FB 2027 Start
    public class vrmTempGroup
    {

        #region Private Internal Members

        private int m_TmpID;
        private int m_GroupID;
       
        #endregion

        #region Public Properties

        public int TmpID
        {
            get { return m_TmpID; }
            set { m_TmpID = value; }
        }

        public int GroupID
        {
            get { return m_GroupID; }
            set { m_GroupID = value; }
        }
       
        #endregion
    }
    //FB 2027 End
 
}
