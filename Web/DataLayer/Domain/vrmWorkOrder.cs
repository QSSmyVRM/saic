/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using NHibernate.Collection;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Summary description(s) for WorkOrder.
    /// </summary>
    public class vrmWorkOrderStatus
    {
        public static int Pending = 0;
        public static int Completed = 1;
        public static int Active = 0;
        public static int Deleted = 1;
         }
    /// <summary>
    /// Masks to check WorkOrder roles.
    /// </summary>
    public class vrmWorkOrderMask
    {
        public static int HKMask = 1;
        public static int CAMask = 2;
        public static int AVMask = 4;   
    }
    /// <summary>
    /// Summary ported from asp this will check the user rolse and return true/false
    /// if the user has the athority
    /// </summary>
    public class checkAthority
        
    {

        public checkAthority(string role)
        {
            m_role = role;

            try
            {
                string[] mary = m_role.ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int topMenu = Convert.ToInt32(ccary[1]);
                int adminMenu = Convert.ToInt32(mmary[0].Split('*')[1]);

                int subMenu = Int32.Parse(mmary[5].Split('*')[1]);
                if ((subMenu & 2) > 0 && (adminMenu & 4) >= 0)//FB 1935
                    m_hasAVManagement = true;
                if ((subMenu & 1) > 0 && (adminMenu & 4) >= 0)//FB 1935
                    m_hasAVWO = true;

                subMenu = Convert.ToInt32(mmary[6].Split('*')[1]);
                if ((subMenu & 2) > 0 && (adminMenu & 2) >= 0)  //Code added fro WO bug FB 1935
                    m_hasCATManagement = true;
                if ((subMenu & 1) > 0 && (adminMenu & 2) >= 0)  //Code added fro WO bug FB 1935
                    m_hasCATWO = true;

                subMenu = Convert.ToInt32(mmary[7].Split('*')[1]);
                if ((subMenu & 2) > 0 && (adminMenu & 1) >= 0)//FB 1935
                    m_hasHKManagement = true;
                if ((subMenu & 1) > 0 && (adminMenu & 1) >= 0)//FB 1935
                    m_hasHKWO = true;
            }
            catch (Exception)
            {
            }
        }

        public bool hasAVManagement { get { return m_hasAVManagement; } }
        public bool hasAVWO { get { return m_hasAVWO;} }
        public bool hasCATManagement { get { return m_hasCATManagement; } }
        public bool hasCATWO { get { return m_hasCATWO; } }
        public bool hasHKManagement { get { return m_hasHKManagement; } }
        public bool hasHKWO  { get { return m_hasHKWO;} } 

        private string m_role;

        private bool m_hasAVManagement = false;
        private bool m_hasAVWO = false;
        private bool m_hasCATManagement = false;
        private bool m_hasCATWO = false;
        private bool m_hasHKManagement = false;
        private bool m_hasHKWO = false;
    
    }
    /// <summary>
    /// Summary description for Inventory Category.
    /// </summary>
    public class InventoryCategory
    {
        #region Private Internal Members

        private int m_ID;
        private int m_Type;
        private string m_Name;
        private string m_Comment;
        private int m_AdminID;
        private string m_Notify;
        private string m_AssignedCostCtr;
        private string m_AllowOverBooking;
        private int m_deleted;
        private int m_orgId; //organization module
        private IList<Menu> m_MenuList;
        private IList<HKInventoryItemList> m_HKItemList;
       // private IList<CAInventoryItemList> m_CAItemList;
        private IList<AVInventoryItemList> m_AVItemList;
        private IList<InventoryRoom> m_RoomList;
        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string Comment
        {
            get { return m_Comment; }
            set { m_Comment = value; }
        }
        public int AdminID
        {
            get { return m_AdminID; }
            set { m_AdminID = value; }
        }
        public string Notify
        {
            get { return m_Notify; }
            set
            {
                if (value == null) m_Notify = string.Empty;
                else m_Notify = value;
            }
        }
        public string AssignedCostCtr
        {
            get { return m_AssignedCostCtr; }
            set
            {
                if (value == null) m_AssignedCostCtr = string.Empty;
                else m_AssignedCostCtr = value;
            }
        }
        public string AllowOverBooking
        {
            get { return m_AllowOverBooking; }
            set
            {
                if (value == null) m_AllowOverBooking = string.Empty;
                else m_AllowOverBooking = value;
            }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public int orgId    //organization module
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        public IList<AVInventoryItemList> AVItemList
        {
            get { return m_AVItemList; }
            set { m_AVItemList = value; }
        }
        public IList<Menu> MenuList
        {
            get { return m_MenuList; }
            set { m_MenuList = value; }
        }
        public IList<HKInventoryItemList> HKItemList
        {
            get { return m_HKItemList; }
            set { m_HKItemList = value; }
        }
        public IList<InventoryRoom> RoomList
        {
            get { return m_RoomList; }
            set { m_RoomList = value; }
        }

        #endregion

        public InventoryCategory()
        {
            m_AVItemList = new List<AVInventoryItemList>();
            m_HKItemList = new List<HKInventoryItemList>();
            m_MenuList = new List<Menu>();
            m_RoomList = new List<InventoryRoom>();
        }
    }
    /// <summary>
    /// Summary description for  Inventory room
    /// </summary>
    public class InventoryRoom
    {
        #region Private Internal Members

        private int m_ID;
        private int m_categoryID;
        private int m_itemID;
        private int m_locationID;
        private int m_delete;

        private vrmRoom m_Room;
        private InventoryCategory m_IC;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int categoryID
        {
            get { return m_categoryID; }
            set { m_categoryID = value; }
        }
        public int itemID
        {
            get { return m_itemID; }
            set { m_itemID = value; }
        }

        public int locationID
        {
            get { return m_locationID; }
            set { m_locationID = value; }
        }
        public int delete
        {
            get { return m_delete; }
            set { m_delete = value; }
        }
        public vrmRoom IRoom
        {
            get { return m_Room; }
            set { m_Room = value; }
        }

        public InventoryCategory ICategory
        {
            get { return m_IC; }
            set { m_IC = value; }
        }
        #endregion

        public InventoryRoom()
        {
            m_Room = new vrmRoom();
            m_delete = 1;
        }
    }

    /// <summary>
    /// Summary description for  Item list(s).
    /// </summary>
    public class AVInventoryItemList
    {
        #region Private Internal Members

        private int m_ID;
        private int m_categoryID;
        private string m_name;
        private int m_quantity;
        private float m_price;
        private string m_serialNumber;
        private string m_image;
        private string m_portable;
        private string m_comment;
        private int m_deliveryType;
        private double m_serviceCharge; //FB 1830
        private double m_deliveryCost; //FB 1830
        private string m_description;
        private int m_deleted;
        private int m_ImageId; //Image Project

        private InventoryCategory m_IC;
        IList<WorkItemCharge> m_ItemCharge;
    
        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int categoryID
        {
            get { return m_categoryID; }
            set { m_categoryID = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public int quantity
        {
            get { return m_quantity; }
            set { m_quantity = value; }
        }
        public float price
        {
            get { return m_price; }
            set { m_price = value; }
        }
        public string serialNumber
        {
            get { return m_serialNumber; }
            set
            {
                if (value == null) m_serialNumber = string.Empty;
                else m_serialNumber = value;
            }
        }
        public string image
        {
            get { return m_image; }
            set
            {
                if (value == null) m_image = string.Empty;
                else m_image = value;
            }
        }
        public string portable
        {
            get { return m_portable; }
            set
            {
                if (value == null) m_portable = string.Empty;
                else m_portable = value;
            }
        }
        public string comment
        {
            get { return m_comment; }
            set
            {
                if (value == null) m_comment = string.Empty;
                else m_comment = value;
            }
        }
        public int deliveryType
        {
            get { return m_deliveryType; }
            set { m_deliveryType = value; }
        }
        public double deliveryCost //FB 1830
        {
            get { return m_deliveryCost; }
            set { m_deliveryCost = value; }
        }
        public double serviceCharge //FB 1830
        {
            get { return m_serviceCharge; }
            set { m_serviceCharge = value; }
        }
        public string description
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public int ImageId //Image Project
        {
            get { return m_ImageId; }
            set { m_ImageId = value; }
        }

        public InventoryCategory ICategory
        {
            get { return m_IC; }
            set { m_IC = value; }
        }
        public IList<WorkItemCharge> ItemCharge
        {
            get { return m_ItemCharge; }
            set { m_ItemCharge = value; }
        }
     
        #endregion

        public AVInventoryItemList()
        {
            m_ItemCharge = new List<WorkItemCharge>();
        }
    }
    public class HKInventoryItemList
    {
        #region Private Internal Members

        private int m_ID;
        private int m_categoryID;
        private string m_name;
        private int m_quantity;
        private float m_price;
        private string m_serialNumber;
        private string m_image;
        private string m_portable;
        private string m_comment;
        private int m_deliveryType;
        private double m_serviceCharge; //FB 1830
        private double m_deliveryCost; //FB 1830
        private string m_description;
        private int m_deleted;
        private int m_ImageId; //Image Project

        private InventoryCategory m_IC;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int CategoryID
        {
            get { return m_categoryID; }
            set { m_categoryID = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public int quantity
        {
            get { return m_quantity; }
            set { m_quantity = value; }
        }
        public float price
        {
            get { return m_price; }
            set { m_price = value; }
        }
        public string serialNumber
        {
            get { return m_serialNumber; }
            set
            {
                if (value == null) m_serialNumber = string.Empty;
                else m_serialNumber = value;
            }
        }
        public string image
        {
            get { return m_image; }
            set
            {
                if (value == null) m_image = string.Empty;
                else m_image = value;
            }
        }
        public string portable
        {
            get { return m_portable; }
            set
            {
                if (value == null) m_portable = string.Empty;
                else m_portable = value;
            }
        }
        public string comment
        {
            get { return m_comment; }
            set
            {
                if (value == null) m_comment = string.Empty;
                else m_comment = value;
            }
        }
        public int deliveryType
        {
            get { return m_deliveryType; }
            set { m_deliveryType = value; }
        }
        public double deliveryCost //FB 1830
        {
            get { return m_deliveryCost; }
            set { m_deliveryCost = value; }
        }
        public double serviceCharge //FB 1830
        {
            get { return m_serviceCharge; }
            set { m_serviceCharge = value; }
        }
        public string description
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public int ImageId //Image Project
        {
            get { return m_ImageId; }
            set { m_ImageId = value; }
        }
        public InventoryCategory ICategory
        {
            get { return m_IC; }
            set { m_IC = value; }
        }

        #endregion

        public HKInventoryItemList()
        {
            m_quantity = 0;
            m_price = 0;

            m_serialNumber = string.Empty;
            m_image = string.Empty;
            m_portable = string.Empty;
            m_comment = string.Empty;

        }
    }

    public class CAInventoryItemList
    {
        #region Private Internal Members

        private int m_ID;
        private int m_categoryID;
        private int m_itemId;
        private string m_name;
        private int m_quantity;
        private float m_price;
        private string m_serialNumber;
        private string m_image;
        private string m_portable;
        private string m_comment;
        private int m_deliveryType;
        private double m_serviceCharge;//FB 1830
        private double m_deliveryCost; //FB 1830
        private string m_description;
        private int m_deleted;
        private Menu m_IMenu;
     
        private InventoryCategory m_IC;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int categoryID
        {
            get { return m_categoryID; }
            set { m_categoryID = value; }
        }
        public int itemId
        {
            get { return m_itemId; }
            set { m_itemId = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public int quantity
        {
            get { return m_quantity; }
            set { m_quantity = value; }
        }
        public float price
        {
            get { return m_price; }
            set { m_price = value; }
        }
        public string serialNumber
        {
            get { return m_serialNumber; }
            set
            {
                if (value == null) m_serialNumber = string.Empty;
                else m_serialNumber = value;
            }
        }
        public string image
        {
            get { return m_image; }
            set
            {
                if (value == null) m_image = string.Empty;
                else m_image = value;
            }
        }
        public string portable
        {
            get { return m_portable; }
            set
            {
                if (value == null) m_portable = string.Empty;
                else m_portable = value;
            }
        }
        public string comment
        {
            get { return m_comment; }
            set
            {
                if (value == null) m_comment = string.Empty;
                else m_comment = value;
            }
        }
        public int deliveryType
        {
            get { return m_deliveryType; }
            set { m_deliveryType = value; }
        }
        public double deliveryCost //FB 1830
        {
            get { return m_deliveryCost; }
            set { m_deliveryCost = value; }
        }
        public double serviceCharge //FB 1830
        {
            get { return m_serviceCharge; }
            set { m_serviceCharge = value; }
        }
        public string description
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public InventoryCategory ICategory
        {
            get { return m_IC; }
            set { m_IC = value; }
        }
        public Menu IMenu
        {
            get { return m_IMenu; }
            set { m_IMenu = value; }
        }

        #endregion

        public CAInventoryItemList()
        {
        }
    }

    public class InventoryList
    {
        #region Private Internal Members

        private int m_ID;
        private int m_Type;
        private string m_Name;
        private string m_Image;
        private int m_ImageId; //Image Project
        private int m_deleted, m_ImgResized; //ZD 100175
        private int m_orgId; //organization module

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string Image
        {
            get
            {
                if (m_Image == null) return string.Empty;
                else return m_Image;
            }
            set
            {
                if (value == null) m_Image = string.Empty;
                else m_Image = value;
            }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public int orgId //organization module
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        public int ImageId //Image Project
        {
            get { return m_ImageId; }
            set { m_ImageId = value; }
        }
        public int ImgResized //ZD 100175
        {
            get { return m_ImgResized; }
            set { m_ImgResized = value; }
        }

        #endregion

        public InventoryList()
        {
        }
    }

    public class Menu
    {
        private int m_id, m_catId, m_quantity;
        private float m_price;
        private string m_comments;
        private IList<MenuService> m_MenuServiceList;
        private IList<CAInventoryItemList> m_CAItemList;
        private InventoryCategory m_IC;
        private string m_name;

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int categoryID
        {
            get { return m_catId; }
            set { m_catId = value; }
        }
        public int quantity
        {
            get { return m_quantity; }
            set { m_quantity = value; }
        }
        public float price
        {
            get { return m_price; }
            set { m_price = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string comments
        {
            get { return m_comments; }
            set { m_comments = value; }
        }
        public IList<MenuService> MenuServiceList
        {
            get { return m_MenuServiceList; }
            set { m_MenuServiceList = value; }
        }
        public IList<CAInventoryItemList> CAItemList
        {
            get { return m_CAItemList; }
            set { m_CAItemList = value; }
        }
        public InventoryCategory ICategory
        {
            get { return m_IC; }
            set { m_IC = value; }
        }
        public Menu()
        {
            m_MenuServiceList = new List<MenuService>();
            m_CAItemList = new List<CAInventoryItemList>();
        }
    }

    public class MenuService
    {
        private int m_Id, m_menuId, m_serviceId;
        private Menu m_Menu;

        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public int menuId
        {
            get { return m_menuId; }
            set { m_menuId = value; }
        }
        public int serviceId
        {
            get { return m_serviceId; }
            set { m_serviceId = value; }
        }
        public Menu IMenu
        {
            get { return m_Menu; }
            set { m_Menu = value; }
        }
       
    }

    public class InventoryService
    {
        private int m_id, m_type;
        private string m_name;

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
    }
    /// <summary>
    /// Summary description for  Work order Order
    /// </summary>
    public class WorkOrder
    {
        #region Private Internal Members

        private int m_ID;
        private int m_Type;
        private int m_AdminID;
        private int m_toUserID;
        private int m_CatID;
        private int m_ConfID;
        private int m_InstanceID;
        private DateTime m_CompletedBy;
        private DateTime m_startBy;
        private string m_Name;
        private string m_Comment;
        private int m_Reminder;
        private int m_Notify;
        private int m_Status;
        private int m_WkOType;
        private string m_strData;
        private int m_RoomID;
        private bool m_sendReminder;
        private int m_deliveryType;
        private double m_serviceCharge; //FB 1830
        private double m_deliveryCost; //FB 1830
        private string m_description;
        private float m_woTax;
        private double m_woTtlCost; //FB 1830
        private int m_woTimeZone;
        private int m_deleted;
        private int m_orgId; //organization module
        IList m_ItemList;
        private InventoryCategory m_IC;
        private vrmRoom m_IRoom;
        private vrmEmail m_email;
        
        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public int CatID
        {
            get { return m_CatID; }
            set { m_CatID = value; }
        }
        public int AdminID
        {
            get { return m_AdminID; }
            set { m_AdminID = value; }
        }
        // if the to user of email differs from admin in charge
        public int toUserID
        {
            get { return m_toUserID; }
            set { m_toUserID = value; }
        }
        public int ConfID
        {
            get { return m_ConfID; }
            set { m_ConfID = value; }
        }
        public int InstanceID
        {
            get { return m_InstanceID; }
            set { m_InstanceID = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public DateTime CompletedBy
        {
            get { return m_CompletedBy; }
            set { m_CompletedBy = value; }
        }
        public DateTime startBy
        {
            get { return m_startBy; }
            set { m_startBy = value; }
        }
        public string Comment
        {
            get { return m_Comment; }
            set { m_Comment = value; }
        }
        public int Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }
        public IList ItemList
        {
            get { return m_ItemList; }
            set { m_ItemList = value; }
        }
        public int Reminder
        {
            get { return m_Reminder; }
            set { m_Reminder = value; }
        }
        public int Notify
        {
            get { return m_Notify; }
            set { m_Notify = value; }
        }
        public bool sendReminder
        {
            get { return m_sendReminder; }
            set { m_sendReminder = value; }
        }
        public int deliveryType
        {
            get { return m_deliveryType; }
            set { m_deliveryType = value; }
        }
        public double deliveryCost //FB 1830
        {
            get { return m_deliveryCost; }
            set { m_deliveryCost = value; }
        }
        public double serviceCharge //FB 1830
        {
            get { return m_serviceCharge; }
            set { m_serviceCharge = value; }
        }
        public string description
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public float woTax
        {
            get { return m_woTax; }
            set { m_woTax = value; }
        }
        public double woTtlCost //FB 1830
        {
            get { return m_woTtlCost; }
            set { m_woTtlCost = value; }
        }
        public int woTimeZone
        {
            get { return m_woTimeZone; }
            set { m_woTimeZone = value; }
        }
        public int WkOType
        {
            get { return m_WkOType; }
            set { m_WkOType = value; }
        }
        public string strData
        {
            get { return m_strData; }
            set { m_strData = value; }
        }
        public InventoryCategory IC
        {
            get { return m_IC; }
            set { m_IC = value; }
        }
        public int roomid
        {
            get { return m_RoomID; }
            set { m_RoomID = value; }
        }
        public vrmRoom IRoom
        {
            get { return m_IRoom; }
            set { m_IRoom = value; }
        }
        public vrmEmail email
        {
            get { return m_email; }
            set { m_email = value; }
        }

        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public int orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }

        public bool isNew;

        #endregion

        public WorkOrder()
        {
            m_IC = new InventoryCategory();
            m_IRoom = new vrmRoom();
            m_ItemList = new ArrayList();
            sendReminder = false;
            isNew = false;
            m_toUserID = 0;
        }
    }
    /// <summary>
    /// Summary description for  Work order item.
    /// </summary>
    public class WorkItem
    {
        #region Private Internal Members

        private int m_ID;
        private int m_ItemID;
        private int m_WorkOrderID;
        private int m_quantity;
        private int m_deliveryType;
        private double m_serviceCharge; //FB 1830
        private string m_description;
        private double m_deliveryCost; //FB 1830
        private int m_serviceId;
        private int m_deleted;

        private WorkOrder m_WorkOrder;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int ItemID
        {
            get { return m_ItemID; }
            set { m_ItemID = value; }
        }
        public int WorkOrderID
        {
            get { return m_WorkOrderID; }
            set { m_WorkOrderID = value; }
        }
        public int quantity
        {
            get { return m_quantity; }
            set { m_quantity = value; }
        }
        public double serviceCharge //FB 1830
        {
            get { return m_serviceCharge; }
            set { m_serviceCharge = value; }
        }
        public double deliveryCost //FB 1830
        {
            get { return m_deliveryCost; }
            set { m_deliveryCost = value; }
        }
        public int deliveryType
        {
            get { return m_deliveryType; }
            set { m_deliveryType = value; }
        }
        
        public string description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        public int serviceId
        {
            get { return m_serviceId; }
            set { m_serviceId = value; }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }
        public WorkOrder IWork
        {
            get { return m_WorkOrder; }
            set { m_WorkOrder = value; }
        }
     
        #endregion

        public WorkItem()
        {
            IWork = new WorkOrder();
        }
    }
    /// <summary>
    /// Summary description for  Work order delivery charges.
    /// </summary>
    public class WorkItemCharge
    {
        #region Private Internal Members

        private int m_ID;
        private int m_ItemId;
        private int m_TypeId;
        private double m_serviceCharge; //FB 1830
        private double m_deliveryCharge; //FB 1830
        private int m_orgId; //organization module

        private DeliveryType m_DeliveryType;
        private AVInventoryItemList m_Item;
        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int ItemId
        {
            get { return m_ItemId; }
            set { m_ItemId = value; }
        }
        public int TypeId
        {
            get { return m_TypeId; }
            set { m_TypeId = value; }
        }
        public int orgId    //organization module
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        public double serviceCharge //FB 1830
        {
            get { return m_serviceCharge; }
            set { m_serviceCharge = value; }
        }
        public double deliveryCharge //FB 1830
        {
            get { return m_deliveryCharge; }
            set { m_deliveryCharge = value; }
        }
        public AVInventoryItemList Item
        {
            get { return m_Item; }
            set { m_Item = value; }
        }
        public DeliveryType IDeliveryType
        {
            get { return m_DeliveryType; }
            set { m_DeliveryType = value; }
        }

        #endregion

        public WorkItemCharge()
        {
            Item = new AVInventoryItemList();
            IDeliveryType = new DeliveryType();
        }
    
    }
    /// <summary>
    /// Summary description for  Work order delivery type.
    /// </summary>
    public class DeliveryType
    {
        #region Private Internal Members

        private int m_id;
        private string m_deliveryType;

        #endregion

        #region Public Properties

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string deliveryType
        {
            get { return m_deliveryType; }
            set { m_deliveryType = value; }
        }
        #endregion

    }
    public class woSearch
    {
        private int m_Get_By, m_cID, m_confId, m_uId, m_aId, m_instance, m_Type, m_TimeZone, m_Status, m_SortBy;            
        private string m_FromDate, m_ToDate, m_Name;
        private List<int> m_Rooms;
        private int m_orgId;

        public int Get_By
        {
            get { return m_Get_By; }
            set { m_Get_By = value; }
        }
        public int cID
        {
            get { return m_cID; }
            set { m_cID = value; }
        }
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int aId
        {
            get { return m_aId; }
            set { m_aId = value; }
        }
        public int confId
        {
            get { return m_confId; }
            set { m_confId = value; }
        }
        public int instance
        {
            get { return m_instance; }
            set { m_instance = value; }
        }
        public int Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public int TimeZone
        {
            get { return m_TimeZone; }
            set { m_TimeZone = value; }
        }
        public int Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }
        public string FromDate
        {
            get { return m_FromDate; }
            set { m_FromDate = value; }
        }
        public string ToDate
        {
            get { return m_ToDate; }
            set { m_ToDate = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public int SortBy
        {
            get { return m_SortBy; }
            set { m_SortBy = value; }
        }
        
        public List<int> Rooms
        {
            get { return m_Rooms;}
            set { m_Rooms = value; }
        }
        public int orgId    //organization module
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        public woSearch()
        {
            m_Get_By = 0;
            m_confId = 0;
            m_uId = 0;
            m_cID = 0;
            m_instance = 0;
            m_Type = 0;
            m_SortBy = 3;
            m_Status = -1; // disabled ....
            m_orgId = 11; //default organization

            Rooms = new List<int>();

            m_FromDate = string.Empty;
            m_ToDate = string.Empty;
            m_Name = string.Empty;
        }
    }
    public class woConstant
    {
        public const int CATEGORY_TYPE_NEW = 0;
        public const int CATEGORY_TYPE_AV = 1;  // Audio visual 
        public const int CATEGORY_TYPE_CA = 2;  // Catering
        public const int CATEGORY_TYPE_HK = 3;  // House keeping

        public const int GET_BY_ALL = 0;
        public const int GET_BY_ID = 1;
        public const int GET_BY_CONF_ID = 2;
        public const int GET_BY_USER_ID = 3;
    }


}
