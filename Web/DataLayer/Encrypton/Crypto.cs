/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;
namespace cryptography
{
	public interface IvrmEncryption
	{
		string encrypt (string input);
		string decrypt (string input);
	}
	/// <summary>
	/// Summary description for Crypto.
	/// This class will encrypt and decrypt a string using Rijndael implementation of AES
	/// Memory streams are used for file IO (since cryptostream requires stream I/O)
	/// This class is designed to be called from managed code and must be built with 
	/// the Register Com for Interop setting in properties set to true. This will automatically
	/// build at typelib .tlb file and register it. Please pay attention to deployment issues. 
	/// </summary>
	public class Crypto : IvrmEncryption
	{
		//Create variables to help with read and write.
		private string outString = "";	 // return value 
		private	MemoryStream fout; 
		
		// need a static password as a seed for algorythm
		protected string Password = "myVRM2006";		
		protected Rijndael RijndaelAlg;
		public Crypto()
		{
			// Derive a Key and an IV from the Password and create an algorithm

			PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,

			new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,  0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

			RijndaelAlg = Rijndael.Create();
			RijndaelAlg.Padding = PaddingMode.PKCS7;

			RijndaelAlg.Key = pdb.GetBytes(32);

			RijndaelAlg.IV = pdb.GetBytes(16);		}
		//
		// basically we create an an output stream in memory
		// then were read the instring into a byte array and write it to the 
		// output (crypto) stream to either encrypt or decrypt 
		// 
		public string encrypt(string inString)
		{
			try
			{
				byte[] encryptString = Encoding.Default.GetBytes(inString); 
		
				// Create an in memory stream.
				fout = 
					new MemoryStream();
	
		
	
				// Create a CryptoStream using the memory stream
				// and the passed key and initialization vector (IV).

				CryptoStream cStream = new CryptoStream(fout,
					RijndaelAlg.CreateEncryptor(RijndaelAlg.Key, RijndaelAlg.IV),
					CryptoStreamMode.Write);
				
				try
				{
		
					cStream.Write(encryptString, 0, encryptString.Length);
					cStream.FlushFinalBlock();
					// Convert our encrypted data from a memory stream into a byte array.
					byte[] cipherTextBytes = fout.ToArray();
					int idx = (int)fout.Length;
					
					cStream.Close();
				
					 // convert into Base64 so that the result can be used in xml
					 outString =  System.Convert.ToBase64String(cipherTextBytes, 0, idx);
				
				}
				
				catch (Exception e)
				{
					outString = "ERROR  " + e.Message;
				}
				finally
				{
					//
					// close all streams
					//
					cStream.Close();  
					fout.Close();
				}
			}
			catch (CryptographicException e)
			{
				outString = "ERROR Cryptographic error: " + e.Message;
			}
			catch (UnauthorizedAccessException e)
			{
				outString = "ERROR file error " + e.Message;
			}
			return outString;
		}
		public string decrypt(string inString)
		{
			try
			{
				// get input length 
				byte[] decryptString = System.Convert.FromBase64String(inString);
				
				// Create an in memory stream.
				fout = 
					new MemoryStream();
	
			
				// Create a CryptoStream using the memory stream
				// and the passed key and initialization vector (IV).
				CryptoStream cStream = new CryptoStream(fout,
					RijndaelAlg.CreateDecryptor(RijndaelAlg.Key, RijndaelAlg.IV),
					CryptoStreamMode.Write);
		
				try
				{
	
					cStream.Write(decryptString, 0, decryptString.Length);
					cStream.FlushFinalBlock();
					cStream.Close();
		
					// Convert our encrypted data from a memory stream into a byte array.
					byte[] cipherTextBytes = fout.ToArray();
                
				 	outString = Encoding.Default.GetString(cipherTextBytes);
        		
				}
				
				catch (Exception e)
				{
					outString = "ERROR  " + e.Message;
				}
				finally
				{
					//
					// close all streams
					//
					cStream.Close();  
					fout.Close();
				}
			}
			catch (CryptographicException e)
			{
				outString = "ERROR Cryptographic error: " + e.Message;
			}
			catch (UnauthorizedAccessException e)
			{
				outString = "ERROR file error " + e.Message;
			}
			return outString;
		}

	}
}
