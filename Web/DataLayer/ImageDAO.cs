/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Xml;
using System.Collections.Generic;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Departments.
    /// Implement IDispose to close session
    /// Image management classes
    /// </summary>
    public class imageDAO : vrmDAO
    {
        public imageDAO(string config, log4net.ILog log) : base(config, log) { }

        public class ImageDao :
               AbstractPersistenceDao<vrmImage, int>, IImageDAO
        {
            public ImageDao(string ConfigPath) : base(ConfigPath) { }

            #region GetById
            /// <summary>
            /// GetById
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public vrmImage GetById(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("ImageId", id));
                    List<vrmImage> imgObj = GetByCriteria(criterionList);

                    if (imgObj.Count <= 0)
                        return null;

                    return imgObj[0];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion
        }
        public IImageDAO GetImageDao() { return new ImageDao(m_configPath); }
    }

    //FB 2136 Starts
    public class secBadgeDao : vrmDAO
    {
        public secBadgeDao(string config, log4net.ILog log) : base(config, log) { }

        public class SecBadgeDao :
               AbstractPersistenceDao<vrmSecurityImage, int>, ISecBadgeDao
        {
            public SecBadgeDao(string ConfigPath) : base(ConfigPath) { }

            #region GetByBadgeId
            /// <summary>
            /// GetByBadgeId
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public vrmSecurityImage GetByBadgeId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BadgeId", id));
                    List<vrmSecurityImage> badgeImg = GetByCriteria(criterionList);

                    if (badgeImg.Count <= 0)
                        return null;

                    return badgeImg[0];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion
        }
        public ISecBadgeDao GetSecImageDao() { return new SecBadgeDao(m_configPath); }

    }
    //FB 2136 Ends


}
