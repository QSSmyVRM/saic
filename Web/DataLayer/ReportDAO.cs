/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Data Access Object for Reports.
	/// Implements interfaces for reports and schedules. and (to save space) 
    /// Instantiates Dao objects that implment functionality. 
	/// </summary>
    public class ReportDAO
    {
        private log4net.ILog m_log;
        private string m_configPath;
        private int m_userId;
     
        private vrmUser m_myVrmUser;

        public ReportDAO(string config, log4net.ILog log)
        {
            m_log = log;
            m_configPath = config;
        }

        public ReportDAO(int userid, string config, log4net.ILog log)
        {
            m_log = log;
            m_configPath = config;
            m_userId = userid;
            DaoInit();
        }

        private void DaoInit()
        {
            userDAO ud = new userDAO(m_configPath, m_log);
            IUserDao user = ud.GetUserDao();
            m_myVrmUser = user.GetById(m_userId);
        }

        public IReportScheduleDao GetReportScheduleDao()
        {
            return new ReportScheduleDao(m_configPath);
        }

        public IReportTemplateDao GetReportTemplateDao()
        {
            return new ReportTemplateDao(m_configPath);
        }
        public IReportInputItemsDao GetReportInputItemsDao()
        {
            return new ReportInputItemsDao(m_configPath);
        }
        //FB 2343 Start
        public IReportMonthlyDaysDAO GetReportMonthlyDaysDao()
        {
            return new ReportMonthlyDaysDao(m_configPath);
        }
        public IReportWeeklyDaysDAO GetReportWeeklyDaysDao()
        {
            return new ReportWeeklyDaysDao(m_configPath);
        }
        //FB 2343 End
        //FB 2410 Start
        public IBatchReportSettingsDAO GetBatchReportSettingsDao()
        {
            return new BatchReportSettingsDao(m_configPath);
        }
        public IBatchReportDatesDAO GetBatchReportDatesDao()
        {
            return new BatchReportDatesDao(m_configPath);
        }
        //FB 2410 End
    }
    public class ReportScheduleDao :
               AbstractPersistenceDao<ReportSchedule, int>, IReportScheduleDao
        {
            public ReportScheduleDao(string ConfigPath) : base(ConfigPath) { }

            public List<ReportSchedule> GetSubmitBetween(DateTime dateFrom, DateTime dateTo)
            {
                List <ICriterion> criterionList = new List <ICriterion>();
                DateTime nullDate = new DateTime(0);

                if (dateFrom != nullDate)
                {
                
                    if (dateTo != nullDate)
                    {                            
                        criterionList.Add(Expression.Between("SubmitDate", dateFrom, dateTo));
                        criterionList.Add(Expression.Eq("deleted", 0));
                    }
                    else
                    {
                        criterionList.Add(Expression.Le("SubmitDate", dateFrom));
                        criterionList.Add(Expression.Eq("deleted", 0) );
                    }

                }
                else
                {
                   criterionList.Add(Expression.Eq("status", 0));
                   criterionList.Add(Expression.Eq("deleted", 0));
                }
                return GetByCriteria(criterionList);

            }

            public List<ReportSchedule> GetScheduleBetween(DateTime a_from, DateTime a_to)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                DateTime nullDate = new DateTime(0);

                if (a_from != nullDate)
                {

                    if (a_to != nullDate)
                    {
                        criterionList.Add(Expression.Between("CompleteDate", a_from, a_to));
                        criterionList.Add(Expression.Eq("deleted", 0));
                        criterionList.Add(Expression.Eq("status", 0));
                    }
                    else
                    {
                        criterionList.Add(Expression.Le("submitDate", a_from));
                        criterionList.Add(Expression.Eq("deleted", 0));
                        criterionList.Add(Expression.Eq("status", 0));
                    }

                }
                else
                {
                    criterionList.Add(Expression.Le("submitDate", a_from ));
					criterionList.Add(Expression.Eq("deleted", 0));
					criterionList.Add(Expression.Eq("status", 0));
                }
                return GetByCriteria(criterionList);

            }
        
        }

        public class ReportTemplateDao :
               AbstractPersistenceDao<ReportTemplate, int>, IReportTemplateDao
        {
            public ReportTemplateDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class ReportInputItemsDao :
            AbstractPersistenceDao<ReportInputItem, int>, IReportInputItemsDao
        {
            public ReportInputItemsDao(string ConfigPath) : base(ConfigPath) { }

        }
        //FB 2343 Start
        public class ReportMonthlyDaysDao :
           AbstractPersistenceDao<ReportMonthlyDays, int>, IReportMonthlyDaysDAO
        {
            public ReportMonthlyDaysDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class ReportWeeklyDaysDao :
               AbstractPersistenceDao<ReportWeeklyDays, int>, IReportWeeklyDaysDAO
        {
            public ReportWeeklyDaysDao(string ConfigPath) : base(ConfigPath) { }

        }
        //FB 2343 End
        //FB 2410 Start
        public class BatchReportSettingsDao :
                   AbstractPersistenceDao<BatchReportSettings, int>, IBatchReportSettingsDAO
        {
            public BatchReportSettingsDao(string ConfigPath) : base(ConfigPath) { }
        }
        public class BatchReportDatesDao :
                       AbstractPersistenceDao<BatchReportDates, int>, IBatchReportDatesDAO
        {
            public BatchReportDatesDao(string ConfigPath) : base(ConfigPath) { }

        }
        //FB 2410 End
    }
