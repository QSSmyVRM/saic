/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.IO;
using System.Runtime.Remoting.Messaging;

using System.Web;

using NHibernate;
using NHibernate.Cache;
using NHibernate.Cfg;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description for Class1.
	/// Implement IDispose to close all factories
	/// </summary>
	public class SessionManagement : IDisposable
	{
		private static Hashtable sessionFactories = new Hashtable();
        
 
		public SessionManagement()
		{
		}

		/// <summary>
		/// This method attempts to find a session factory stored in the hashtable based on 
		/// config file path/name if it can not be found a new is created and added to the hashtable
		/// using the cofig file path/name as the key. 
		/// NOTE: Since the SP (service provider) model may have several differnet providers each
		///       provider will have its own session factory. Each user its own session. 
		///       
		/// This method is private and called by the public get session method. 
		/// </summary>
		/// <param name="sessionFactoryConfigPath">Path location and filename of App.config</param>
		private static ISessionFactory GetSessionFactory(string sessionFactoryConfigPath) 
		{
			if(sessionFactoryConfigPath == null || 
				sessionFactoryConfigPath.Length == 0)
			{
				throw new Exception("sessionFactoryConfigPath may not be null nor empty");
			}
			//  Attempt to retrieve the stored SessionFactory from the hashtable.
			ISessionFactory sessionFactory = (ISessionFactory) sessionFactories[sessionFactoryConfigPath];

			//  No match found in  SessionFactory so creat a new one and add it.
			if (sessionFactory == null) 
			{
				string xml = sessionFactoryConfigPath + "app.config.xml";
				if(File.Exists(xml))
				{
					//
					// first configure the xml file
					//
					Configuration cfg = new Configuration();
					cfg.Configure(xml);
			
					//  Now that we have our Configuration object, create a new SessionFactory
					sessionFactory = cfg.BuildSessionFactory();

					if (sessionFactory == null) 
					{
						throw new Exception("cfg.BuildSessionFactory() Could not build session factory");
					}
					// add new session.
                    if(!sessionFactories.ContainsKey(sessionFactoryConfigPath))
					    sessionFactories.Add(sessionFactoryConfigPath, sessionFactory);
				}
				else
				{
					throw new Exception("The config file '" + sessionFactoryConfigPath + "' does not exist"); 
				}
				
			}

			return sessionFactory;
		}
		/// <summary>
		/// This method  a session by first calling the static getSessionFactory 
		/// The session is assigned to each individual user. Beacuse this is a low level function
		/// an exception is thrown if it an error occurs. 
		/// </summary>
		/// <param name="sessionFactoryConfigPath">Path location and filename of App.config</param>
		public static ISession GetSession(string sessionFactoryConfigPath) 
		{
			ISession session;
			try
			{
				session = GetSessionFactory(sessionFactoryConfigPath).OpenSession();
				if (session==null)
				{
					throw new InvalidOperationException("Call to OpenSession() failed");
				}
                session.FlushMode = FlushMode.Commit;
				return session;
			}
			catch (Exception e)
			{
				throw e;
			}					
		}
		/// <summary>
		/// close all factories...
		/// </summary>
		public void Dispose()
		{
			foreach(string key in sessionFactories.Keys)
			{
				ISessionFactory sessionFactory = (ISessionFactory) sessionFactories[key];
				if(sessionFactory != null)
					sessionFactory.Close();
            }
            sessionFactories.Clear();
		}
	}
}
