/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Searches .
    /// Implement IDispose to close session
    /// </summary>
    public class SystemDAO
    {
        private ISession session;
        private log4net.ILog m_log;
        private string m_configPath;
        private ISystemDAO m_SystemDAO;
        private ISysMailDAO m_SysMailDAO;
            
        public SystemDAO(string config, log4net.ILog olog)
        {
            try
            {
                m_log = olog;
                m_configPath = config;

                bool valid = Init();
                if (!valid)
                    throw new Exception("Could not initialize SystemDAO");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        private bool Init()
        {
            try
            {
                m_SystemDAO = new vrmSystemDAO(m_configPath);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        public class vrmSystemDAO :
               AbstractPersistenceDao<sysData, int>, ISystemDAO
        {
            public vrmSystemDAO(string ConfigPath) : base(ConfigPath) { }
            public sysData GetByAdminId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("Admin", id));
                    List<sysData> mySysData = GetByCriteria(criterionList);

                    if (mySysData.Count < 1)
                        throw new Exception("Admin Id not found");
                    return mySysData[0];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ISystemDAO GetSystemDao() { return new vrmSystemDAO(m_configPath); }
            

        public class vrmSysMailDAO :
            AbstractPersistenceDao<sysMailData, int>, ISysMailDAO
        {
            public vrmSysMailDAO(string ConfigPath) : base(ConfigPath) { }
            public sysMailData GetByMailId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("id", id));
                    List<sysMailData> mySysmMail = GetByCriteria(criterionList);

                    if (mySysmMail.Count < 1)
                        throw new Exception("Id not found");
                    return mySysmMail[0];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ISysMailDAO GetSysMailDao() { return new vrmSysMailDAO(m_configPath); }

        #region AccountScheme

        public class vrmAccSchemeDAO :
              AbstractPersistenceDao<sysAccScheme, int>, IAccSchemeDAO
        {
            public vrmAccSchemeDAO(string ConfigPath) : base(ConfigPath) { }
        }
        public IAccSchemeDAO GetAccSchemeDao() { return new vrmAccSchemeDAO(m_configPath); }
        #endregion

        public class vrmSysTimeZonePrefDAO :
              AbstractPersistenceDao<sysTimeZonePref, int>, ISysTimeZonePrefDAO
        {
            public vrmSysTimeZonePrefDAO(string ConfigPath) : base(ConfigPath) { }
            public List<sysTimeZonePref> GetAllPrefTimeZones(string ConfigPath)
            {
                List<sysTimeZonePref> sysTZPList = null;
                try
                {
                    sysTZPList = new List<sysTimeZonePref>();
                    SessionManagement.GetSession(ConfigPath);
                    ISession session = SessionManagement.GetSession(ConfigPath);
                    if (!session.IsConnected)
                        session.Reconnect();

                    IList tzPrefList = session.CreateCriteria(typeof(sysTimeZonePref)).AddOrder(Order.Asc("systemid")).List();

                    if (tzPrefList.Count < 1)
                        throw new Exception("No Records found");
                    foreach(sysTimeZonePref sTZPref in tzPrefList)
                    {
                        sysTZPList.Add(sTZPref);
                    }
                    return sysTZPList;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ISysTimeZonePrefDAO GetSysTimeZonePrefDao() { return new vrmSysTimeZonePrefDAO(m_configPath); }
		//LDAP Fixes
        public class vrmLDAPConfigDAO : AbstractPersistenceDao<vrmLDAPConfig, int>, ILDAPConfigDAO
        {
            public vrmLDAPConfigDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmLDAPConfig GetLDAPConfigDetails(string ConfigPath)
            {
                vrmLDAPConfig sysLDAPConfig = null;
                try
                {
                    sysLDAPConfig = new vrmLDAPConfig();
                    SessionManagement.GetSession(ConfigPath);
                    ISession session = SessionManagement.GetSession(ConfigPath);
                    if (!session.IsConnected)
                        session.Reconnect();

                    IList LDAPConfigList = session.CreateCriteria(typeof(vrmLDAPConfig)).List();

                    if (LDAPConfigList.Count < 1)
                        //Empty DB issue FB 2164
                        return sysLDAPConfig;
                        //throw new Exception("No Records found");
                    foreach (vrmLDAPConfig LDAPCnfg in LDAPConfigList)
                    {
                        sysLDAPConfig = LDAPCnfg;
                    }
                    return sysLDAPConfig;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ILDAPConfigDAO GetLDAPConfigDao() { return new vrmLDAPConfigDAO(m_configPath); }


        //FB 2363
        #region External Scheduling 

        public class vrmExternalSchedulingDAO :AbstractPersistenceDao<sysExternalScheduling, int>, IExternalSchedulingDAO
        {
            public vrmExternalSchedulingDAO(string ConfigPath) : base(ConfigPath) { }
        }
        public IExternalSchedulingDAO GetExternalSchedulingDAO() { return new vrmExternalSchedulingDAO(m_configPath); }
        #endregion

        #region External Scheduling

        public class ESMailUsrRptSettingsDAO : AbstractPersistenceDao<ESMailUsrRptSettings, int>, IESMailUsrRptSettingsDAO
        {
            public ESMailUsrRptSettingsDAO(string ConfigPath) : base(ConfigPath) { }
        }
        public IESMailUsrRptSettingsDAO GetESMailUsrRptSettingsDAO() { return new ESMailUsrRptSettingsDAO(m_configPath); }
        #endregion
        //FB 2392
        #region whygo Scheduling

        public class sysWhygoSettingsDAO : AbstractPersistenceDao<sysWhygoSettings, int>, IsysWhygoSettings
        {
            public sysWhygoSettingsDAO(string ConfigPath) : base(ConfigPath) { }
        }
        public IsysWhygoSettings GetsysWhygoSettingsDAO() { return new sysWhygoSettingsDAO(m_configPath); }
        #endregion


        //FB 2659 - Starts
        public IDefaultLicenseDAO GetDefaultLicenseDao() { return new defaultLicenseDAO(m_configPath); }

        public class defaultLicenseDAO :
                AbstractPersistenceDao<vrmDefaultLicense, int>, IDefaultLicenseDAO
        {
            public defaultLicenseDAO(string ConfigPath) : base(ConfigPath) { }
        }
        //FB 2659 - Starts

        //ZD 100256 Start
        #region Sync Settings

        public IsyncSettingsDAO GetsyncSettingsDAO() { return new vrmsyncSettingsDAO(m_configPath); }

        public class vrmsyncSettingsDAO : AbstractPersistenceDao<syncSettings, int>, IsyncSettingsDAO
        {
            public vrmsyncSettingsDAO(string ConfigPath) : base(ConfigPath) { }
        }
        

        #endregion
        //ZD 100256 End
    }
}
