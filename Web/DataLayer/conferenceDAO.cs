/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;


namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Reports.
    /// Implements interfaces for reports and schedules. and (to save space) 
    /// Instantiates Dao objects that implment functionality. 
    /// </summary>
    public class conferenceDAO
    {
       
        private static Hashtable conferenceDAOFactories = null;

        private log4net.ILog m_log;
        private string m_configPath;
        private IConfRoomDAO m_ConfRoomDAO;
        private IConfUserDAO m_ConfUserDAO;
        private IConfCascadeDAO m_ConfCascadeDAO;
        private IConfAdvAvParamsDAO m_confAdvAvParamsDAO;//FB 2027
        private ITempRoomDAO m_TempDAO;
        //FB 2027
        private ITempAdvAvParamsDAO m_TempAdvAvParamsDAO;
        private ITempUserDAO m_TempUserDAO;
        private ITempGroupDAO m_TempGroupDAO;
        //FB 2027
        private IEmailDao m_emailDAO;
        private IConfApprovalDAO m_approvalDAO;
        private IConfVNOCOperatorDAO m_VNOCOperatorDAO; //FB 2670
        private IConfAttachmentDAO m_UserAccountDAO;
        private IConfAttrDAO m_confAttributeDAO;    //Custom Attribute Fixes
        private IConfMessageDao m_ConfMessageDAO; //FB 2486
        private IConfSyncMCUadjustmentsDao m_ConfSyncMCUadjustmentsDao; //FB 2441
        private IConfPCDao m_ConfPCDao; //FB 2693

        public conferenceDAO(string config, log4net.ILog log)
        {
            m_log = log;
            m_configPath = config;
            
            if (conferenceDAOFactories != null)
            {

                if (conferenceDAOFactories["m_ConfRoomDAO"] != null)
                    m_ConfRoomDAO = (confRoomDAO)conferenceDAOFactories["m_ConfRoomDAO"];
                else
                    m_ConfRoomDAO = new confRoomDAO(m_configPath);

                if (conferenceDAOFactories["m_ConfUserDAO"] != null)
                    m_ConfUserDAO = (confUserDAO)conferenceDAOFactories["m_ConfUserDAO"];
                else
                    m_ConfUserDAO = new confUserDAO(m_configPath);

                if (conferenceDAOFactories["m_ConfCascadeDAO"] != null)
                    m_ConfCascadeDAO = (confCascadeDAO)conferenceDAOFactories["m_ConfCascadeDAO"];
                else
                    m_ConfCascadeDAO = new confCascadeDAO(m_configPath);

                if (conferenceDAOFactories["m_ConfAdvAvParamsDAO"] != null)//FB 2027
                    m_confAdvAvParamsDAO = (confAdvAvParamsDAO)conferenceDAOFactories["m_ConfAdvAvParamsDAO"];
                else
                    m_confAdvAvParamsDAO = new confAdvAvParamsDAO(m_configPath);

                //FB 2027 Start

                if (conferenceDAOFactories["m_TempDAO"] != null)
                    m_TempDAO = (tempRoomDAO)conferenceDAOFactories["m_TempDAO"];
                else
                    m_TempDAO = new tempRoomDAO(m_configPath);
                
                if (conferenceDAOFactories["m_TempAdvAvParamsDAO"] != null)
                    m_TempAdvAvParamsDAO = (tempAdvAvParamsDAO)conferenceDAOFactories["m_TempAdvAvParamsDAO"];
                else
                    m_TempAdvAvParamsDAO = new tempAdvAvParamsDAO(m_configPath);

                if (conferenceDAOFactories["m_TempUserDAO"] != null)
                    m_TempUserDAO = (tempUserDAO)conferenceDAOFactories["m_TempUserDAO"];
                else
                    m_TempUserDAO = new tempUserDAO(m_configPath);

                if (conferenceDAOFactories["m_TempGroupDAO"] != null)
                    m_TempGroupDAO = (tempGroupDAO)conferenceDAOFactories["m_TempGroupDAO"];
                else
                    m_TempGroupDAO = new tempGroupDAO(m_configPath);

                //FB 2027 End

                if (conferenceDAOFactories["m_approvalDAO"] != null)
                    m_approvalDAO = (confApprovalDAO)conferenceDAOFactories["m_approvalDAO"];
                else
                    m_approvalDAO = new confApprovalDAO(m_configPath);

                if (conferenceDAOFactories["m_VNOCOperatorDAO"] != null) //FB 2670
                    m_VNOCOperatorDAO = (confVNOCOperatorDAO)conferenceDAOFactories["m_VNOCOperatorDAO"];
                else
                    m_VNOCOperatorDAO = new confVNOCOperatorDAO(m_configPath);

                if (conferenceDAOFactories["m_UserAccountDAO"] != null)
                    m_UserAccountDAO = (confAttachmentDAO)conferenceDAOFactories["m_UserAccountDAO"];
                else
                    m_UserAccountDAO = new confAttachmentDAO(m_configPath);

                if (conferenceDAOFactories["m_confAttributeDAO"] != null)
                    m_confAttributeDAO = (confAttributeDAO)conferenceDAOFactories["m_confAttributeDAO"];
                else
                    m_confAttributeDAO = new confAttributeDAO(m_configPath);

                if (conferenceDAOFactories["m_emailDAO"] != null)
                    m_emailDAO = (confEmailDAO)conferenceDAOFactories["m_emailDAO"];
                else
                    m_emailDAO = new confEmailDAO(m_configPath);

                if (conferenceDAOFactories["m_ConfMessageDAO"] != null)  //FB 2486
                    m_ConfMessageDAO = (ConfMessageDao)conferenceDAOFactories["m_ConfMessageDAO"];
                else
                    m_ConfMessageDAO = new ConfMessageDao(m_configPath);

                if (conferenceDAOFactories["m_ConfSyncMCUadjustmentsDao"] != null)  //FB 2441
                    m_ConfSyncMCUadjustmentsDao = (ConfSyncMCUadjustmentsDao)conferenceDAOFactories["m_ConfSyncMCUadjustmentsDao"];
                else
                    m_ConfSyncMCUadjustmentsDao = new ConfSyncMCUadjustmentsDao(m_configPath);

                if (conferenceDAOFactories["m_ConfPCDao"] != null)  //FB 2693
                    m_ConfPCDao = (ConfPCDao)conferenceDAOFactories["m_ConfPCDao"];
                else
                    m_ConfPCDao = new ConfPCDao(m_configPath);

            }

            if (conferenceDAOFactories == null)
            {

                conferenceDAOFactories = new Hashtable();
                m_ConfRoomDAO = new confRoomDAO(m_configPath);
                conferenceDAOFactories.Add("m_ConfRoomDAO", m_ConfRoomDAO);
                m_ConfUserDAO = new confUserDAO(m_configPath);
                conferenceDAOFactories.Add("m_ConfUserDAO", m_ConfUserDAO);
                m_ConfCascadeDAO = new confCascadeDAO(m_configPath);
                conferenceDAOFactories.Add("m_ConfCascadeDAO", m_ConfCascadeDAO);
                m_TempDAO = new tempRoomDAO(m_configPath);
                conferenceDAOFactories.Add("m_TempDAO", m_TempDAO);
                m_approvalDAO = new confApprovalDAO(m_configPath);
                conferenceDAOFactories.Add("m_approvalDAO", m_approvalDAO);
                m_VNOCOperatorDAO = new confVNOCOperatorDAO(m_configPath); //FB 2670
                conferenceDAOFactories.Add("m_VNOCOperatorDAO", m_VNOCOperatorDAO);
                m_UserAccountDAO = new confAttachmentDAO(m_configPath);
                conferenceDAOFactories.Add("m_UserAccountDAO", m_UserAccountDAO);
                m_confAttributeDAO = new confAttributeDAO(m_configPath);
                conferenceDAOFactories.Add("m_confAttributeDAO", m_confAttributeDAO);
                m_emailDAO = new confEmailDAO(m_configPath);
                conferenceDAOFactories.Add("m_emailDAO", m_emailDAO);
                //FB 2027 Start
                m_TempAdvAvParamsDAO = new tempAdvAvParamsDAO(m_configPath);
                conferenceDAOFactories.Add("m_TempAdvAvParamsDAO", m_TempAdvAvParamsDAO);
                m_TempUserDAO = new tempUserDAO(m_configPath);
                conferenceDAOFactories.Add("m_TempUserDAO", m_TempUserDAO);
                m_TempGroupDAO = new tempGroupDAO(m_configPath);
                conferenceDAOFactories.Add("m_TempGroupDAO", m_TempGroupDAO);
                //FB 2027 End
                m_confAdvAvParamsDAO = new confAdvAvParamsDAO(m_configPath);//FB 2027
                conferenceDAOFactories.Add("m_confAdvAvParamsDAO", m_confAdvAvParamsDAO);
                m_ConfMessageDAO = new ConfMessageDao(m_configPath);//FB 2486
                conferenceDAOFactories.Add("m_ConfMessageDAO", m_ConfMessageDAO);
                m_ConfSyncMCUadjustmentsDao = new ConfSyncMCUadjustmentsDao(m_configPath);//FB 2441
                conferenceDAOFactories.Add("m_ConfSyncMCUadjustmentsDao", m_ConfSyncMCUadjustmentsDao);//FB 2441
                //FB 2693 Starts  
                m_ConfPCDao = new ConfPCDao(m_configPath);
                conferenceDAOFactories.Add("m_ConfPCDao", m_ConfPCDao);
                //FB 2693 Ends
            }

        }

        public IList<vrmConfRoom> GetConfRoomByID(int confid, int instanceid)
        {
            List<ICriterion> criterionList = new List<ICriterion>();

            if (instanceid <= 0)
                instanceid = 1;

            criterionList.Add(Expression.Eq("confid", confid));
            criterionList.Add(Expression.Eq("instance", instanceid));

            IList<vrmConfRoom> ret = m_ConfRoomDAO.GetByCriteria(criterionList);
            return ret;
        }

        public IConferenceDAO GetConferenceDao()
        {
            return new confDAO(m_configPath);
        }
        public ITemplateDAO GetTemplateDao()
        {
            return new templateDAO(m_configPath);
        }

        public IConfRoomDAO GetConfRoomDao()
        {
            return new confRoomDAO(m_configPath);
        }
        public IConfRecurDAO GetConfRecurDao()
        {
            return new confRecurDAO(m_configPath);
        }
        public IConfRecurDefuntDAO GetConfRecurDefunctDAO()//FB 2218
        {
            return new confRecurDefunctDAO(m_configPath);
        }
        public IEmailDao GetConfEmailDao()
        {
            return new confEmailDAO(m_configPath);
        }
        public IConfApprovalDAO GetConfApprovalDao()
        {
            return new confApprovalDAO(m_configPath);
        }
        public IConfVNOCOperatorDAO GetConfVNOCOperatorDao() //FB 2670
        {
            return new confVNOCOperatorDAO(m_configPath);
        }
        public IConfAttachmentDAO GetConfAttachmentDao()
        {
            return new confAttachmentDAO(m_configPath);
        }

        public IConfUserDAO GetConfUserDao()
        {
            return new confUserDAO(m_configPath);
        }
        public IConfCascadeDAO GetConfCascadeDao()
        {
            return new confCascadeDAO(m_configPath);
        }
        public IConfAttrDAO GetConfAttrDao()    //Custom Attribute Fixes
        {
            return new confAttributeDAO(m_configPath);
        }

        public IConfAdvAvParamsDAO GetConfAdvAvParamsDAO()//FB 2027
        {
            return new confAdvAvParamsDAO(m_configPath);
        }
		//FB 2027 SetTerminalCtrl
        public IConfMontiorDAO GetConfMontiorDAO()//FB 2027 SetTerminalCtrl
        {
            return new confMontiorDAO(m_configPath);
        }

        //FB 2027 Start
        public ITempRoomDAO GetTempRoomDAO()
        {
            return new tempRoomDAO(m_configPath);
        }
        public ITempAdvAvParamsDAO GetTempAdvAvParamsDAO()
        {
            return new tempAdvAvParamsDAO(m_configPath);
        }
        public ITempUserDAO GetTempUserDAO()
        {
            return new tempUserDAO(m_configPath);
        }
        public ITempGroupDAO GetTempGroupDAO()
        {
            return new tempGroupDAO(m_configPath);
        }
        //FB 2027 End
        //FB 1830 start
        public IEmailTypeDao GetEmailTypeDao() { return new emailTypeDAO(m_configPath); } 
        public IEmailContentDao GetEmailContentDao() { return new emailContentDAO(m_configPath); } 
        public IEmailLanguageDao GetEmailLanguageDao() { return new emailLanguageDAO(m_configPath); } 
        public IEPlaceHoldersDao GetEPlaceHoldersDao() { return new ePlaceHoldersDao(m_configPath); } 
        //FB 1830 end
        public IConfMessageDao GetConfMessageDao() { return new ConfMessageDao(m_configPath); } //FB 2486 
        public IConfSyncMCUadjustmentsDao GetConfSyncMCUadjustmentsDao() { return new ConfSyncMCUadjustmentsDao(m_configPath); }//FB 2441
        public IConfPCDao GetConfPCDao() { return new ConfPCDao(m_configPath); }//FB 2693
		public IConfOverbookApprovalDao GetConfOverbookApprovalDao() { return new ConfOverbookApprovalDao(m_configPath); }//FB 2659
        public IESEventsDao GetESEventsDao() { return new ESEventsDao(m_configPath); } //ZD 100256 
        public INWESEventsDao GetNWESEventsDao() { return new NWESEventsDao(m_configPath); } //ZD 100256 

        public class vrmConfEmailDAO :
           AbstractPersistenceDao<vrmEmail, int>, IEmailDao
        { public vrmConfEmailDAO(string ConfigPath) : base(ConfigPath) { } }

        public class vrmConfApprovalDAO :
        AbstractPersistenceDao<vrmConfApproval, int>, IConfApprovalDAO
        { public vrmConfApprovalDAO(string ConfigPath) : base(ConfigPath) { } }

        public IList<vrmConfApproval> GetConfApprovalByInstanceID(int confID, int instanceID)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion conf = Expression.Eq("confid", confID);
                criterionList.Add(conf);
                ICriterion instance = Expression.Eq("instanceid", instanceID);
                criterionList.Add(instance);
                IList<vrmConfApproval> ret = m_approvalDAO.GetByCriteria(criterionList);
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //FB 2670
        public class vrmconfVNOCOperatorDAO :
        AbstractPersistenceDao<vrmConfVNOCOperator, int>, IConfVNOCOperatorDAO
        { public vrmconfVNOCOperatorDAO(string ConfigPath) : base(ConfigPath) { } }

        
        /* *** FB 1158 - Approval Issues ... start *** */
        #region GetPendingConfByInstanceID
        /// <summary>
        /// GetPendingConfByInstanceID
        /// </summary>
        /// <param name="confID"></param>
        /// <param name="instanceID"></param>
        /// <returns></returns>
        public IList<vrmConfApproval> GetPendingConfByInstanceID(int confID, int instanceID)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                string qry = " confid='" + confID + "'";
                qry += " and instanceid='" + instanceID + "' and decision=0 order by entitytype, entityid";
                criterionList.Add(Expression.Sql(qry));
                IList<vrmConfApproval> ret = m_approvalDAO.GetByCriteria(criterionList);
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        /* *** FB 1158 - Approval Issues ... end *** */
        /* *** FB 1782 ... start *** */
        #region GetDeniedConfByInstanceID
        /// <summary>
        /// GetDeniedConfByInstanceID
        /// </summary>
        /// <param name="confID"></param>
        /// <param name="instanceID"></param>
        /// <returns></returns>
        public IList<vrmConfApproval> GetDeniedConfByInstanceID(int confID, int instanceID)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                string qry = " confid='" + confID + "'";
                qry += " and instanceid='" + instanceID + "' and decision=2 order by entitytype, entityid";
                criterionList.Add(Expression.Sql(qry));
                IList<vrmConfApproval> ret = m_approvalDAO.GetByCriteria(criterionList);
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        /* *** FB 11782... end *** */

        #region IsLevelApproved
        /// <summary>
        /// 2027 set approve conference
        /// </summary>
        /// <param name="confID"></param>
        /// <param name="instanceID"></param>
        /// <returns></returns>
        public Boolean IsLevelApproved(int confID, int instanceID, int entityType)
        {
            Boolean bRet = false;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                string qry = " confid='" + confID + "'";
                
                if(instanceID > 0 )
                    qry += " and instanceid='" + instanceID + "'";
                
                qry += " and entitytype = '" + entityType + "' and decision=0 order by entitytype, entityid";
                criterionList.Add(Expression.Sql(qry));
                IList<vrmConfApproval> ret = m_approvalDAO.GetByCriteria(criterionList);
                if (ret.Count <= 0)
                    bRet = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bRet;
        }
        #endregion

        /* *** Custom Attribute Fixes ... start *** */
        #region Get Conf Custom Attributes By ID
        /// <summary>
        /// Get Conf Custom Attributes By ID
        /// </summary>
        /// <param name="confID"></param>
        /// <param name="instanceID"></param>
        /// <returns></returns>
        public IList<vrmConfAttribute> GetConfCustAttributes(int confID, int instanceID)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion conf = Expression.Eq("ConfId", confID);
                criterionList.Add(conf);
                ICriterion instance = Expression.Eq("InstanceId", instanceID);
                criterionList.Add(instance);
                IList<vrmConfAttribute> ret = m_confAttributeDAO.GetByCriteria(criterionList);
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        /* *** Custom Attribute Fixes ... end *** */

        public IList<vrmTempRoom> GetTempRoomByID(int id)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.Eq("tmpId", id));
            IList<vrmTempRoom> ret = m_TempDAO.GetByCriteria(criterionList);
            return ret;
            //return m_TempDAO.GetById(id); 
        }

        //FB 2027 Start
        public IList<vrmTempAdvAvParams> GetTempAdvAvParamsByID(int id)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.Eq("tmpId", id));
            IList<vrmTempAdvAvParams> ret = m_TempAdvAvParamsDAO.GetByCriteria(criterionList);
            return ret;
        }

        public IList<vrmTempUser> GetTempUserByID(int id)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.Eq("TmpID", id));
            IList<vrmTempUser> ret = m_TempUserDAO.GetByCriteria(criterionList);
            return ret;
        }

        public IList<vrmTempGroup> GetTempGroupByID(int id)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.Eq("TmpID", id));
            IList<vrmTempGroup> ret = m_TempGroupDAO.GetByCriteria(criterionList);
            return ret;
        }
        //FB 2027 End
        public IList<vrmConfAdvAvParams> GetConfAdvAvParamsByID(int id)//FB 2027
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.Eq("confid", id));
            IList<vrmConfAdvAvParams> ret = m_confAdvAvParamsDAO.GetByCriteria(criterionList);
            return ret;
        }

    }
    public class confDAO :
           AbstractPersistenceDao<vrmConference, int>, IConferenceDAO
    {
        public confDAO(string ConfigPath) : base(ConfigPath) { }
        // get by confid instanceid retruns a single instance. 
        // get by confid returns a list even it there are no records, or
        // there is only one instance
        //
        // NOTE: confid(instanceid) lookups are only valid for conf_conference_d records. all others are 
        //       linked via the unique ID
        //
        public vrmConference GetByConfId(int id, int instanceid)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();

                ICriterion conf =
                      Expression.Eq("confid", id);
                criterionList.Add(conf);
                if (instanceid <= 0)
                    instanceid = 1;

                ICriterion instance =
                          Expression.Eq("instanceid", instanceid);
                criterionList.Add(instance);

                //
                // there should ALWAYS be an entry...
                // this is used for retrieving a SPECIFIC record...
                // 
                IList<vrmConference> ret;
                ret = GetByCriteria(criterionList);
                if (ret.Count > 0)
                {
                    return ret[0];
                }
                else
                {
                    //FB 2027 SetConference - Starts
                    /*string error = "Conference Record : " + id.ToString();
                    if (instanceid > 0)
                        error += " / " + instanceid.ToString();
                    error += " not found"; 
                    Exception e = new Exception();
                    throw e; */
                    throw new Exception("GetByConfId: Conference does not exist.");
                    //FB 2027 SetConference - End
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public vrmConference GetByConfUId(int id)//FB 2392
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();

                ICriterion conf =
                      Expression.Eq("confnumname", id);
                criterionList.Add(conf);
                //
                // there should ALWAYS be an entry...
                // this is used for retrieving a SPECIFIC record...
                // 
                IList<vrmConference> ret;
                ret = GetByCriteria(criterionList,true);
                if (ret.Count > 0)
                {
                    return ret[0];
                }
                else
                {
                    return null;
                    
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<vrmConference> GetByConfId(int id)
        {
            List<ICriterion> criterionList = new List<ICriterion>();

            criterionList.Add(Expression.Eq("confid", id));
            return GetByCriteria(criterionList);
        }

    }
    public class confRecurDAO :
             AbstractPersistenceDao<vrmRecurInfo, int>, IConfRecurDAO
    {
        public confRecurDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confRecurDefunctDAO ://FB 2218
             AbstractPersistenceDao<vrmRecurInfoDefunct, int>, IConfRecurDefuntDAO
    {
        public confRecurDefunctDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confRoomDAO :
           AbstractPersistenceDao<vrmConfRoom, int>, IConfRoomDAO
    {
        public confRoomDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confUserDAO :
         AbstractPersistenceDao<vrmConfUser, int>, IConfUserDAO
    {
        public confUserDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confCascadeDAO :
         AbstractPersistenceDao<vrmConfCascade, int>, IConfCascadeDAO
    {
        public confCascadeDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confEmailDAO :
         AbstractPersistenceDao<vrmEmail, int>, IEmailDao
    {
        public confEmailDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confApprovalDAO :
         AbstractPersistenceDao<vrmConfApproval, int>, IConfApprovalDAO
    {
        public confApprovalDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confVNOCOperatorDAO : //FB 2670
         AbstractPersistenceDao<vrmConfVNOCOperator, int>, IConfVNOCOperatorDAO
    {
        public confVNOCOperatorDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confAttachmentDAO :
         AbstractPersistenceDao<vrmConfAttachments, int>, IConfAttachmentDAO
    {
        public confAttachmentDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confAttributeDAO : //Custom Attribute Fixes
        AbstractPersistenceDao<vrmConfAttribute, int>, IConfAttrDAO
    {
        public confAttributeDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class confBridgeDAO : //FB 2566
        AbstractPersistenceDao<vrmConfBridge, int>, IConfBridgeDAO
    {
        public confBridgeDAO(string ConfigPath) : base(ConfigPath) { }

    }
   

    public class templateDAO :
          AbstractPersistenceDao<vrmTemplate, int>, ITemplateDAO
    {
        public templateDAO(string ConfigPath) : base(ConfigPath) { }
    }
    public class tempRoomDAO :
           AbstractPersistenceDao<vrmTempRoom, int>, ITempRoomDAO
    {
        public tempRoomDAO(string ConfigPath) : base(ConfigPath) { }

    }
    //FB 2027 Start
    public class tempAdvAvParamsDAO :
           AbstractPersistenceDao<vrmTempAdvAvParams, int>, ITempAdvAvParamsDAO
    {
        public tempAdvAvParamsDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class tempUserDAO :
           AbstractPersistenceDao<vrmTempUser, int>, ITempUserDAO
    {
        public tempUserDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class tempGroupDAO :
           AbstractPersistenceDao<vrmTempGroup, int>, ITempGroupDAO
    {
        public tempGroupDAO(string ConfigPath) : base(ConfigPath) { }

    }
    //FB 2027 End
    //FB 2027
    public class confAdvAvParamsDAO :
           AbstractPersistenceDao<vrmConfAdvAvParams, int>, IConfAdvAvParamsDAO
    {
        public confAdvAvParamsDAO(string ConfigPath) : base(ConfigPath) { }

    }

    //FB 2027 SetTerminalCtrl
    public class confMontiorDAO :
           AbstractPersistenceDao<vrmConfMonitor, int>, IConfMontiorDAO
    {
        public confMontiorDAO(string ConfigPath) : base(ConfigPath) { }

    }

    //FB 1830 start
    public class emailTypeDAO :
         AbstractPersistenceDao<vrmEmailType, int>, IEmailTypeDao
    {
        public emailTypeDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class emailContentDAO :
         AbstractPersistenceDao<vrmEmailContent, int>, IEmailContentDao
    {
        public emailContentDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class emailLanguageDAO :
         AbstractPersistenceDao<vrmEmailLanguage, int>, IEmailLanguageDao
    {
        public emailLanguageDAO(string ConfigPath) : base(ConfigPath) { }

    }
    public class ePlaceHoldersDao :
         AbstractPersistenceDao<vrmEPlaceHolders, int>, IEPlaceHoldersDao
    {
        public ePlaceHoldersDao(string ConfigPath) : base(ConfigPath) { }

    }
    //FB 1830 end
    //FB 2486
    public class ConfMessageDao :
       AbstractPersistenceDao<vrmConfMessage, int>, IConfMessageDao
    {
        public ConfMessageDao(string ConfigPath) : base(ConfigPath) { }

    }

    //FB 2441
    public class ConfSyncMCUadjustmentsDao :
       AbstractPersistenceDao<vrmConfSyncMCUadjustments, int>, IConfSyncMCUadjustmentsDao
    {
        public ConfSyncMCUadjustmentsDao(string ConfigPath) : base(ConfigPath) { }

    }
    //FB 2693 Starts
    public class ConfPCDao :
       AbstractPersistenceDao<vrmConfPC, int>, IConfPCDao
    {
        public ConfPCDao(string ConfigPath) : base(ConfigPath) { }

    }
    //FB 2693 Ends

	//FB 2659
    public class ConfOverbookApprovalDao :
       AbstractPersistenceDao<vrmConfOverbookApproval, int>, IConfOverbookApprovalDao
    {
        public ConfOverbookApprovalDao(string ConfigPath) : base(ConfigPath) { }

    }

    //ZD 100256
    public class ESEventsDao :
       AbstractPersistenceDao<vrmESEvent, int>, IESEventsDao
    {
        public ESEventsDao(string ConfigPath) : base(ConfigPath) { }

    }

    public class NWESEventsDao :
       AbstractPersistenceDao<vrmNWESEvent, int>, INWESEventsDao
    {
        public NWESEventsDao(string ConfigPath) : base(ConfigPath) { }

    }
}
