/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for email.
    /// All email should use this object, ONLY!
    /// </summary>
    ///  
    // to do. do not exppose email other than here (remove from work order and every where else!
    public class emailDAO
    {
        private static Hashtable emailDAOFactories = null;

        private List<vrmEmail> m_emailList;
        private IEmailDao m_emailDAO;
        private log4net.ILog m_log;
        private string m_configPath;

        private conferenceDAO m_confDAO;
        private IConferenceDAO m_vrmConfDAO;

        public emailDAO(ref vrmDataObject obj)
        {
            m_log = obj.log;
            m_configPath = obj.ConfigPath;

            if (emailDAOFactories != null)
            {
                if (emailDAOFactories["m_emailList"] != null)
                    m_emailList = (List<vrmEmail>)emailDAOFactories["m_emailList"];
                else
                    m_emailList = new List<vrmEmail>();

                if (emailDAOFactories["m_emailDAO"] != null)
                    m_emailDAO = (confEmailDAO)emailDAOFactories["m_emailDAO"];
                else
                    m_emailDAO = new confEmailDAO(obj.ConfigPath);

                if (emailDAOFactories["m_confDAO"] != null)
                    m_confDAO = (conferenceDAO)emailDAOFactories["m_confDAO"];
                else
                    m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log);

                if (emailDAOFactories["m_vrmConfDAO"] != null)
                    m_vrmConfDAO = (IConferenceDAO)emailDAOFactories["m_vrmConfDAO"];
                else
                    m_vrmConfDAO = m_confDAO.GetConferenceDao();

 
            }
            else
            {
                emailDAOFactories = new Hashtable();
                m_emailList = new List<vrmEmail>();
                emailDAOFactories.Add("m_emailList", m_emailList);
                m_emailDAO = new confEmailDAO(obj.ConfigPath);
                emailDAOFactories.Add("m_emailDAO", m_emailDAO);
                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log);
                emailDAOFactories.Add("m_confDAO", m_confDAO);
                m_vrmConfDAO = m_confDAO.GetConferenceDao();
                emailDAOFactories.Add("m_vrmConfDAO", m_vrmConfDAO);
            
            }

           
      
        }        
    }
}