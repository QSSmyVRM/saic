/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Generic Data Access Object for all classes.
    /// Implement IDispose to close session
    /// </summary>
    public class vrmDAO
    {
        protected log4net.ILog m_log;
        protected string m_configPath;

        public vrmDAO(string config, log4net.ILog log)
        {
            m_log = log;
            m_configPath = config;
        }
    }   

}
