 Delete from Err_List_S where Languageid = 3


INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (200,N'Error del sistema. Por favor, contacte con su Administrador VRM y d�gale este c�digo de error.','System Error.Please contact your VRM Administrator and supply this error code.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (201,N'Falta la ID de usuario. Operaci�n abortada o ID de usuario no suministrado.','UserID missing.Operation aborted or UserID not supplied.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (202,N'Por favor, introduzca un nombre de conferencia.','Please enter a Conference Name.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (203,N'Por favor, introduzca una fecha y hora v�lidas.','Please enter a valid Date and Time.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (205,N'El usuario no est� registrado en el VRM.','User is not registered in VRM.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (206,N'Por favor, introduzca un nombre de inicio de sesi�n �nico.','Please enter a unique login name.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (207,N'Inicio de sesi�n o contrase�a inv�lidos. Por favor vuelva a intentarlo.','Invalid login or password.Please try again.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (208,N'Su cuenta est� bloqueada debido a las m�ltiples intentos de contrase�a inv�lidas. Por favor, contacte con su administrador VRM.','Your account is locked due to multiple invalid password attempts.Please contact your VRM administrator','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (209,N'Por favor, escriba su direcci�n de correo electr�nico.','Please enter a valid e-mail address.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (210,N'No hay suficientes puertos de IP o T1.','Not enough IP or T1 ports.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (211,N'No hay suficientes puertos de Mux T1','Not enough Mux T1 ports.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (212,N'Por favor, introduzca un nombre �nico de Conferencia.','Please enter a unique Conference Name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (213,N'No hay suficientes puertos de T1','Not enough T1 ports.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (214,N'No hay suficiente tarjetas de audio','Not enough audio cards.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (215,N'No hay suficiente tarjetas de v�deo','Not enough video cards.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (216,N'Error complicado en el c�lculo de recursos.','Complicated error in resource computation.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (217,N'Por favor, introduzca un nombre de grupo �nico.','Please enter a unique group name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (218,N'El saldo de su cuenta no es suficiente para este cambio de la conferencia. Por favor, contacte con su Administrador VRM y d�gale este c�digo de error.','Your account balance is insufficient for this conference change. Please contact your VRM Administrator and supply this error code.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (219,N'Por favor, introduzca el nombre, apellido o direcci�n de correo electr�nico.','Please enter First Name, Last Name, or E-mail address.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (220,N'Nombre de usuario, Inicio de sesi�n y correo-e duplicados. Por favor revise y vuelva a entrar.','Duplicated User Name, Login, E-mail address. Please check and re-enter.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (221,N'La conferencia se ha creado correctamente. "Nuevo usuario" con correo-e participante duplicado <direcci�n de correo-e> no invitado - el usuario que ya est� en la base de datos VRM fue invitado.','Conference was successfully created. "New User" with duplicate participant e-mail <email address> not invited -- user already in VRM database was invited.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (222,N'Por favor, introduzca un nombre de plantilla �nica.','Please enter a unique template name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (223,N'Please enter a valid conference start date / time.','Please enter a valid conference start date / time.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (224,N'S�lo funci�n de Super Administrador. Por favor, p�ngase en contacto con administrador de VRM para m�s informaci�n.','Super Administrator function only. Please contact VRM Administrator for more information.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (225,N'Por favor, acepte su invitaci�n a la conferencia antes de iniciar sesi�n.','Please accept your conference invitation before logging in.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (226,N'Recursos de sistema insuficientes para esta conferencia en particular. Por favor, intente reducir el n�mero de participantes o de salones incluidos en la conferencia. Para m�s ayuda, por favor, contacte con su Administrador VRM y d�gale este c�digo de error.','Insufficient system resources for this particular conference. Please try reducing the number of participants or rooms included in the conference. For more assistance please contact your VRM Administrator and supply this error code.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (227,N'Por favor, introduzca un inicio de sesi�n o contrase�a v�lidos.','Please enter a valid Login or Password.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (228,N'Por favor, compruebe los ajustes de conferencias recurrentes y vuelva a entrar.','Please check recurring conference settings and re-enter.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (229,N'Usted no est� autorizado para esta funci�n. Por favor, contacte con su Administrador VRM y d�gale este c�digo de error para obtener ayuda.','You are not permissioned for this function. Please contact your VRM Administrator and supply this error code for assistance.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (230,N'El saldo de su cuenta no es suficiente para establecer esta conferencia. Por favor, contacte con su Administrador VRM y d�gale este c�digo de error.','Your account balance is insufficient to set up this conference. Please contact your VRM Administrator and supply this error code.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (231,N'Saldo de la cuenta insuficiente para establecer una conferencia. Por favor, contacte con su Administrador VRM y d�gale este c�digo de error para obtener una cuenta de uso.','Insufficient account balance to set up a conference. Please contact your VRM Administrator and supply this error code to obtain a usage account.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (233,N'Error del sistema. Por favor, cierre y reinicie el navegador, y luego vuelva a iniciar sesi�n en VRM para reiniciar la aplicaci�n.','System Error. Please close and restart your browser, and then login to VRM again to reset the application.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (234,N'No hay ning�n grupo que coincida con su criterio de b�squeda.','There is no Group that matches your search criteria.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (237,N'Por favor, introduzca un nombre de puente �nico.','Please enter a unique Bridge Name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (238,N'Error de facturaci�n. Por favor, contacte con su Administrador VRM y d�gale este c�digo de error.','Billing Error. Please contanct your VRM Administrator and supply this error code.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (240,N'Recursos de sistema insuficientes para esta conferencia en particular. No se encontr� ning�n n�mero de tel�fono RDSI �til para el puente. Para m�s asistencia, por favor, contacte con su Administrador VRM y d�gale este c�digo de error.','Insufficient system resources for this particular conference. No usable ISDN phone number was found for the bridge. For more assistance please contact your VRM Administrator and supply this error code.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (241,N'Uno o m�s salones seleccionados ya han sido reservados.','One or more selected rooms have already been booked. ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (242,N'La Conferencia solicitada ya ocurri� o no existe','The requested conference has  either passed or does not exist','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (243,N'La conferencia solicitada ha sido eliminada por el anfitri�n o el administrador.','The requested conference has  been deleted by the host or administrator.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (244,N'Las conferencias Punto a Punto han sido inhabilitadas por el administrador. Por favor, elija una de las otras secciones para establecer una conferencia.','Point to Point conferences have been disabled by the administrator. Please choose from one of the other sections  to setup a conference.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (250,N'Clave de seguridad inv�lida o Tiempo expirado. Por favor, contacte con el administrador VRM para obtener m�s ayuda.','Invalid Security Key or Time Expired.Please contact your VRM Administrator for further assistance.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (252,N'Su licencia de VRM no permite esta operaci�n','Your VRM License doesnot permit this operation','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (253,N'El sistema VRM no est� disponible en la fecha/hora elegida. Por favor, compruebe la disponibilidad del sistema VRM y vuelva a intentarlo.','VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (254,N'Su cuenta ha sido bloqueada. Por favor, p�ngase en contacto con su administrador de VRM para obtener ayuda.','Your account has been locked. Please contact your VRM administrator for assistance.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (255,N'La conferencia solicitada est� pendiente de aprobaci�n. Por favor, p�ngase en contacto con su administrador de VRM para obtener m�s ayuda.','The requested conference is pending approval. Please contact your VRM Administrator for further assistance','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (256,N'El nombre solicitado ya se est� usando. Por favor, elija otro nombre.','The requested  name  has already been taken. Please choose another name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (257,N'El nombre de edificio solicitado para esta ciudad ya se est� usando. Por favor, elija otro nombre para este edifico.','The requested building  name for this city  has already been taken. Please choose another name for this building','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (258,N'El nombre de cuidad solicitado ya se est� usando. Por favor, elija otro nombre.','The requested city name  has already been taken. Please choose another name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (259,N'El participante solicitado ya est� presente en la conferencia.','The requested party is already present in the conference','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (260,N'El nombre de puente solicitado ya ha sido asignado a otro puente','The requested bridgename has already been assigned to another bridge','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (261,N'El nombre de usuario/correo electr�nico solicitado ya est� en uso','The requested username/email is already in use','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (262,N'Uno o m�s salones no tienen punto final asociado.','One or more selected rooms have no endpoint associated.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (263,N'Su cuenta VRM ha expirado y ha sido bloqueada.  Por favor, p�ngase en contacto con su administrador de VRM local para obtener m�s ayuda.','Your VRM account has expired and has been locked. Please contact you local VRM administrator for further assistance','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (264,N'Su cuenta VRM est� a punto de expirar en% s. Por favor, contacte a su administrador  VRM para obtener m�s ayuda. Su cuenta expira en {0} d�as.','Your VRM account is about to expire on %s. Please contact your VRM Administrator for further assistance Your account expires in {0} days.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (265,N'La cola de correo-e del sal�n solicitado ya se est� usando. Por favor, elija otro correo-e.','The requested Room Email Queue has already been taken. Please choose another email.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (302,N'Clave de Seguridad Inv�lida o Tiempo Expirado.','Invalid Security Key or Time Expired.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (305,N'No se pudo ejecutar SQL. Error en el texto.','Unable to execute sql . Error in text.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (306,N'No se pudo leer la BD.','Unable to read DB.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (307,N'La fecha de caducidad de la cuenta no es v�lida.  Por favor, revise la licencia.','Invalid account expiration date. Please check the license.  ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (308,N'La conferencia no se puede eliminar. O ha caducado o ha finalizado.','Conference cannot be deleted. It is either expired or has been terminated.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (309,N'Falta informaci�n de la cuenta de usuario o la base de datos est� corrompida.','User account info missing or corrupted in database.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (310,N'Error en la operaci�n de usuario. Usuario en uso. �No se puede borrar!','Error in user operation,User in use. Cannot delete!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (311,N'Ocurri� un error interno - c�digo de error 311 (error ocurrido, error �roomid� en confuso por el <nombre> de usuario)','An internal error occurred � error code 311 (Error occurred, roomid error in confuser for user <name>)','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (312,N'Error de datos 1 en la zona horaria de la conferencia.','Time Zone data error1 of the certain conference.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (313,N'Error en la tabla de zona de horario','Time Zone table error','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (314,N'Error de b�squeda <b�squeda en base de datos>','Query error<database query>','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (315,N'Reemplazar error de usuario','Replace User error','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (316,N'Imposible buscar [usuario].','Unable to query [user].','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (317,N'Imposible buscar Operador Bounded.','Unable to query Bounded Operator.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (318,N'Seleccione la ID de la comida, nombre de comida del men�','select foodid,foodname from menufood','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (319,N'Obtener la lista de grupos','FetchGroupList','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (320,N'buscar cuenta de Participante de Grupo','query count from GroupParticipant','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (321,N'buscar desde Participante de Grupo, [Usuario]','query from GroupParticipant , [User]','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (322,N'insertar grupo de conferencia <stmt>','insert confgroup <stmt>','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (323,N'Imposible insertar grupo de conferencia','Unable insert confgroup','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (325,N'Eliminar error.','Delete error.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (326,N'Imposible insertar Sal�n de conferencia.','Unable to insert Confroom.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (327,N'Imposible insertar otras ubicaciones en Sal�n de conferencia.','Unable to insert other locations into Confroom.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (328,N'Imposible buscar [Sal�n de conferencia].','Unable to query [ConferenceRoom].','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (330,N'buscar ubicaciones','FetchLocations','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (331,N'Ocurri� un error interno � c�digo de error 331 (seleccione cuenta(*) de confianza)','An internal error occurred � error code 331 (select count(*) from confuser where confid.)','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (334,N'seleccionar error de operador','select operator errors','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (335,N'Duplicate Address.Please enter another address','Duplicate Address.Please enter another address','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (400,N'Error de validaci�n XML','XML Validation Error','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (402,N'Se super� el m�ximo de grupos.','Maximum Groups Exceeded.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (403,N'Se super� el m�ximo de plantillas.','Maximum Templates Exceeded.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (404,N'Error al leer la licencia.','Error reading License.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (406,N'El Departamento est� en uso. �No se puede borrar!','Dept is in use. Cannot delete!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (407,N'Error en la comprobaci�n de licencias. Por favor, p�ngase en contacto con su administrador local o VRM local para obtener m�s ayuda.','License check failed. Please contact your local administrator or myVRM for further assistance. ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (408,N'Rango de fechas seleccionado invalido','Invalid date range selected','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (410,N'Error de creaci�n de conferencia. �El sal�n dividido s�lo puede ser SIN V�DEO!','Conference creation error. Split room can only be NON VIDEO!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (411,N'No se puede borrar MCU. Quite todos los recursos adjuntados e int�ntelo de nuevo.','Cannot delete MCU. Remove all attached resources and try again.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (412,N'Nombre de departamento duplicado. �No se puede crear/editar!','Duplicate department name. Cannot create/edit!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (413,N'Nombre de punto final ya existe. Por favor elija otro nombre','Endpoint name already exists. Please choose another name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (414,N'FALL� el enlace. �Se devolvi� la transacci�n!','Link FAILED. Transaction rolled back! ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (415,N'Nombre de grupo duplicado','Duplicate Group Name.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (416,N'Comando no soportado','Command is not supported.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (417,N'Niveles en uso y no se pueden borrar.','Tier is in use and cannot be deleted.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (418,N'Error al punto final se le debe asignar un puente','Error End Point must be assigned a bridge','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (419,N'Conferencia ha terminado.','Conference has ended.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (420,N'Tipo de categor�a inv�lida','Invalid category type','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (422,N'Operaci�n Inv�lida','Invalid Operation','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (423,N'ID de organizaci�n inv�lida','Invalid Organization ID','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (424,N'Imagen inv�lida o falta la ID','Invalid Image or ID Missing','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (425,N'El sitio no est� activo. Por favor, contacte a su administrador VRM para ayuda','Site is currently not activated.  Please contact your VRM administrator for assistance','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (426,N'El correo electr�nico solicitado ya se est� usando - Estado inactivo','The requested email address/login is already in use - Inactive state','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (427,N'El nombre para el Nivel 1 ya existe','Name already exists for tier1.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (428,N'El nombre para el Nivel 2 ya existe','Name already exists for tier2.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (429,N'Licencia de la plantilla ha expirado. Por favor, contacte a su administrador VRM','Template license expired. Please contact your VRM Administrator.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (430,N'El usuario administrador ha sido borrado','Admin user has been deleted','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (431,N'Error generado el informe','Error in generating report','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (432,N'Una conferencia recurrente debe contener por lo menos (2) casos. Por favor, modifique el patr�n de repetici�n antes de presentar la conferencia.','A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (433,N'Por favor seleccione un sal�n','Please select a Room','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (434,N'Nombre de punto final ya existe.','Endpoint name already exists','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (435,N'Error, no se puede calcular el uso de MCU','ERROR cannot calculate MCU Usage','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (436,N'Error el proveedor no se encuentra en','Error vendor not found in','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (437,N'Hay conferencias esperando aprobaci�n de este usuario. Por favor, apruebe las conferencias en Lista>Ver Aprobaci�n Pendiente antes de borrar este usuario.','There are conferences pending for approval from this user. Please approve conferences in List>View Approval Pending prior to deleting this user.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (438,N'Error la plantilla est� en uso. No se puede eliminar','Error template is in use. Cannot delete','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (439,N'Error en la actualizaci�n de los valores de la organizaci�n.','Error in updating the organization settings.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (440,N'Error en la actualizaci�n del contador de la aprobaci�n de los usuarios.','Error in updating the approval counter of the users.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (441,N'Error en la actualizaci�n de los aprobadores del sistema para la organizaci�n.','Error in updating system approvers for organization.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (442,N'Error en la actualizaci�n de los detalles t�cnicos de la organizaci�n.','Error in updating the organization tech details.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (443,N'Error en la actualizaci�n de los detalles de la organizaci�n.','Error in updating the organization details.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (444,N'Solo administradores del sitio pueden crear/editar la organizaci�n','Only Site administrators can create/edit organization.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (445,N'L�mite de la organizaci�n superado.','Organization limit exceeds.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (446,N'T�tulo de organizaci�n ya existe.','Organization title already exists.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (447,N'Error en la creaci�n de datos por defecto para la organizaci�n.','Error in creating default data for the organization.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (448,N'Error en la creaci�n datos t�cnicos por defecto para la organizaci�n.','Error in creating default tech data for the organization.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (449,N'Error en la creaci�n de atributos personalizados por defecto para la organizaci�n.','Error in creating default custom attributes for the organization.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (450,N'Error en la actualizaci�n de los l�mites de recursos de la organizaci�n.','Error in updating organization resources limits.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (451,N'No se puede habilitar el m�dulo de �Instalaciones�, la cuenta de licencia superada.','Can not enable Facility module, license count exceeded.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (452,N'Can not enable Catering Module, license count exceeded.','Can not enable Catering Module, license count exceeded.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (453,N'Can not enable House Keeping module, license count exceeded.','Can not enable House Keeping module, license count exceeded.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (454,N'No se puede habilitar el m�dulo �API�, la cuenta de licencia superada.','Can not enable API module, license count exceeded.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (455,N'L�mite de salones de v�deo excede la licencia VRM.','Video rooms limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (456,N'L�mite de salones sin-v�deo excede la licencia VRM.','Non-Video rooms limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (457,N'El l�mite MCU excede la licencia VRM.','MCU limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (458,N'El l�mite de puntos finales excede la licencia VRM.','Endpoints limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (459,N'El l�mite de usuarios excede la licencia VRM.','Endpoints limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (460,N'El l�mite de usuarios de intercambio excede la licencia VRM.','Exchange user limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (461,N'El l�mite de usuarios Domino excede la licencia VRM.','Domino user limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (462,N'El l�mite de usuarios debe incluir usuarios de domino e intercambio.','User limit should be inclusive for domino,exchange and mobile users.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (463,N'Por favor, desactive los salones de v�deo para reducir la cuenta. Ya que hay m�s salones de v�deo activos.','Please deactivate video rooms to reduce the count.As there are more active video rooms.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (464,N'Por favor, desactive los salones sin v�deo para reducir la cuenta. Ya que hay m�s salones de v�deo activos.','Please deactivate non-video rooms to reduce the count.As there are more active non-video rooms.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (465,N'Por favor, elimine MCUs para reducir la cuenta. Ya que hay m�s MCU activos.','Please delete mcu to reduce the count.As there are more active mcu.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (466,N'Por favor, elimine puntos finales para reducir la cuenta. Ya que hay m�s puntos finales activos.','Please delete endpoints to reduce the count.As there are more active endpoints.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (467,N'Por favor, elimine usuarios para reducir la cuenta. Ya que hay m�s usuarios activos.','Please delete users to reduce the count.As there are more active users.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (468,N'La organizaci�n predeterminada no se puede borrar.','The default organization cannot be deleted.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (469,N'La organizaci�n tiene la lista de usuarios activos. Elimine los usuarios antes de eliminar la organizaci�n.','Organization has active users list. Delete users before deleting the organization.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (470,N'La organizaci�n tiene lista de salones. Elimine los salones antes de eliminar la organizaci�n.','Organization has rooms list. Delete rooms before deleting the organization.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (471,N'La organizaci�n contiene la lista MCU. Elimine MCU antes de eliminar la organizaci�n.','Organization contains MCU list. Delete MCU before deleting the organization','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (472,N'Error al eliminar la organizaci�n','Error in deleting the organization','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (473,N'No se puede eliminar el informe','Cannot Delete Report','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (474,N'�El informe no existe!','Report does not exist!','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (475,N'�Operaci�n Exitosa!','Operation Sucessful!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (476,N'Error en la conversi�n LDAP. No hay registro de plantillas de usuario','Error in LDAP conversion. No user template record','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (477,N'Error en la conversi�n de LDAP. No hay registro de la funci�n del usuario','Error in LDAP conversion. No user role record','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (478,N'Error el nombre de usuario / correo electr�nico ya existe. No se puede a�adir.','Error user name/email exists. Cannot add','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (479,N'Ocurri� un error interno, debe definir un usuario - c�digo de error 479 (Error, �debe agregar s�lo un USUARIO!)','An internal error occurred; you must define a user � error code 479 (Error; Cannot you must add a USER only!)','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (480,N'Error no se puede convertir la fecha/hora','Error cannot convert datetime','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (481,N'�No se puede borrar! El men� est� en uso con las siguientes �rdenes de trabajo:','Cannot delete! Menu in use by the following work orders:','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (482,N'Por favor, elimine usuarios de intercambio para reducir la cuenta. Ya que hay m�s usuarios de intercambio activos.','Please delete exchange users to reduce the count.As there are more active exchange users.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (483,N'La opci�n personalizada seleccionada est� enlazada con una conferencia. No se puede editar/eliminar.','The selected custom option is linked with conference. It cannot be edited/deleted.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (484,N'Error en la eliminaci�n de los atributos personalizados','Error in deleting the custom attribute','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (485,N'El t�tulo de la opci�n personalizada ya existe.','Custom option title already exists.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (486,N'El l�mite m�ximo para las opciones personalizadas es','Maximum limit for the custom options is','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (487,N'Error el usuario no est� autorizado como administrador encargado. El Inventario NO se ha guardado.','Error user not authorized as administrator in charge. Inventory NOT saved.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (488,N'Error: el nombre ya existe','Error:name already exists','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (489,N'Error el usuario no tiene autorizaci�n del departamento a la sala de administrador (s).','Error user does not have departmental authorization to administrator Room(s).','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (490,N'Error no se puede borrar el sal�n de inventario. Orden de trabajo pendiente','ERROR cannot romove inventory room. Work order pending','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (491,N'Error el usuario no est� autorizado como administrador encargado. La orden de trabajo NO se ha guardado.','Error user not authorized as administrator in charge. Work Order NOT saved.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (492,N'Error el usuario no tiene autorizaci�n del departamento a la orden de trabajo de administrador. La orden de trabajo NO se ha guardado.','Error user does not have departmental authorization to administrator Work Order. Work Order NOT saved.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (493,N'Error no se puede borrar el inventario en uso','Error Cannot delete Inventory in use.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (494,N'Por favor, elimine usuarios domin� para reducir la cuenta. Ya que hay m�s usuarios domin� activos.','Please delete domino users to reduce the count.As there are more active domino users.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (495,N'Hay (0) USUARIO(S) agregado(s) a esta MCU,','There are {0} USER(S) attached to this MCU, ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (496,N'Hay (0) PUNTO(S) FINALE(S) agregado(s) a esta MCU,','There are {0} END POINT(S) are assigned to this MCU , ','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (497,N'El propietario de la conferencia no existe mientras se est� creando la conferencia: Archivo:','Owner of conference does not exist while creating  the Conference:File :','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (498,N'Direcci�n de correo electr�nico Inv�lida','Invalid Email Address','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (499,N'Por favor, establezca los valores para las opciones personalizadas obligatorias.','Please set the values for the mandatory custom options.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (500,N'Error al calcular el inventario en tiempo real','Error in calculating real time inventory','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (501,N'Conexi�n LDAP fallida.','LDAP connection failed.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (502,N'Error al recuperar la configuraci�n LDAP','Error fetching ldap settings','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (503,N'Error autenticando el usuario LDAP','Error authenticating ldap user.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (504,N'Error al comparar y cargar usuarios en el �rea de celebraci�n LDAP.','Error in comparing and loading users in ldap holding area.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (505,N'Conexi�n exitosa.  Sin embargo, no se encontraron registros LDAP coincidentes.','Connection successful. However, no matching ldap records were found.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (506,N'�Fallo de autenticaci�n! No se encontraron registros LDAP coincidentes.','Authentication Failed! No matching ldap records were found.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (507,N'No se puede eliminar el punto final: el punto final est� asociado con otro(s) sal�n(es)','Cannot delete the endpoint: endpoint is associated with the room(s)','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (508,N'�No se encontr� el punto final! No se puede actualizar','Endpoint not found! Cannot update endpoint:','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (509,N'No se puede actualizar el usuario o tabla Idap','Cannot update user or ldap tables','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (510,N'Se encontraron conflictos en algunos casos recurrentes. Por favor, modifique cada caso individualmente','Room conflicts found in some recurring instances. Please modify Date / Time or Service Type or the room cannot have room only reservations ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (511,N'FALLO de verificaci�n de licencia: l�mite de la organizaci�n excedido','License Check FAILED:Organization limit exceeded','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (512,N'FALLO de verificaci�n de licencia: l�mite de salones excedido','License Check FAILED: Room limit exceeded','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (513,N'FALLO de verificaci�n de licencia: l�mite de usuarios excedido','License Check FAILED:  User limit exceeded','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (514,N'FALLO de verificaci�n de licencia: l�mite MCU excedido','License Check FAILED:  MCU limit exceeded','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (515,N'FALLO de verificaci�n de licencia: mala fecha de caducidad','License Check FAILED:   bad expiry date','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (516,N'Error en la actualizaci�n de MCU como privado, se ha utilizado el MCU','Error in Updating MCU as Private,MCU has been Used','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (517,N'Patr�n de recurrencia inv�lido.','Invalid Recurrence Pattern.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (518,N'El usuario est� conectado con una conferencia como Organizador/Anfitri�n/Participante. �No se puede eliminar!','User is linked with a conference either as Scheduler/Host/Participants .Cannot delete!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (519,N'Error en la operaci�n del usuario. El usuario est� asignado como aprobador de un sistema/MCU/Sal�n/Departamento. �No se puede eliminar!','Error in user operation,User is assigned as a System/MCU/Room/Department approver.Cannot delete!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (520,N'Error en la operaci�n del usuario. El usuario est� asignado como encargado de un sistema/MCU/Sal�n/Departamento. �No se puede eliminar!','Error in user operation,User is assigned as a System/MCU/Room/Workorder Incharge.Cannot delete!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (521,N'El usuario est� enlazado con un grupo/departamento. �No se puede eliminar!','User is linked with a Group/Template/Department. Cannot delete!','S',3,'U');


INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (522,N'�Esta versi�n de administrador de miVRM no est� soportada!','This version of the myVRM addin is not supported!','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (523,N'El usuario no tiene licencia de Intercambio.','User does not have Exchange license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (524,N'El usuario no tiene licencia Domin�.','User does not have Domino license,','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (525,N'El usuario no tiene licencia M�vil.','User does not have Mobile license,','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (526,N'El l�mite de usuarios m�viles excede la licencia VRM.','Mobile user limit exceeds VRM license.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (527,N'Por favor, elimine usuarios m�viles para reducir la cuenta. Ya que hay m�s usuarios m�viles activos.','Please delete mobile users to reduce the count.As there are more active mobile users.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (528,N'FALLO de verificaci�n de licencia: Por favor, reduzca el l�mite de usuarios m�viles en cualquiera de las Organizaciones.','License Check FAILED: Please reduce the Mobile User Limit in any of the Organization.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (529,N'Por favor, introduzca la ID del correo-e.','Please enter Email-Id.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (530,N'Por favor, introduzca la contrase�a.','Please enter the password.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (531,N'�La ID del puente est� vac�a!','Bridge ID is Empty!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (534,N'Usted no es un Participante en esta Conferencia o la Conferencia seleccionada ya ha pasado o ha sido eliminada.','You are not a particpant in this conference or the selected conference has passed or has been deleted.','S',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (535,N'Conference are associated with inventory set in quantity greater than the given quantity!','Conference are associated with inventory set in quantity greater than the given quantity!','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (536,N'Por favor, introduzca la duraci�n de la conferencia con un m�nimo de 15 minutos.','Please enter conference duration with minimum of 15 minutes.','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (537,N'Instance conflict in selected custom dates.','Instance conflict in selected custom dates.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (538,N'Instance conflict in selected custom dates.','Room in use ( cannot be deleted as its being used as a preferred room by a user )!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (539,N'Room in use ( cannot be deleted as its being used as preferred room in user template )!','Room in use ( cannot be deleted as its being used as preferred room in user template )!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (540,N'Room in use ( cannot be deleted as its being used in the Workorder )!','Room in use ( cannot be deleted as its being used in the Workorder )!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (541,N'Room in use ( cannot be deleted as its being used in search template )!','Room in use ( cannot be deleted as its being used in search template )!','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (542,N'Las conferencias de Audio/V�deo han sido inhabilitadas por el administrador. Por favor, contacte con su administrador del VRM.','Audio/Video conferences have been disabled by the administrator. Please Contact your VRM Adminstrator.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (543,N'S�lo las conferencias de Audio han sido inhabilitadas por el administrador. Por favor, contacte con su administrador del VRM.','Audio Only conferences have been disabled by the administrator. Please Contact your VRM Adminstrator.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (544,N'Las conferencias de sal�n han sido inhabilitadas por el administrador. Por favor, contacte con su administrador del VRM.','Room conferences have been disabled by the administrator. Please Contact your VRM Adminstrator.','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (545,N'Room is linked with a Template. Cannot deactivate!','Room is linked with a Template. Cannot deactivate!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (546,N'At least one item should have a requested quantity greater than 0','At least one item should have a requested quantity greater than 0','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (547,N'Error cannot create email','Error cannot create email','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (548,N'No Entity Options Found!','No Entity Options Found!','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (549,N'The selected custom option is not linked with any conferences.','The selected custom option is not linked with any conferences.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (550,N'Room is linked with conference.Cannot deactivated','Room is linked with conference.Cannot deactivated','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (551,N'Error in inserting Recurrence details.','Error in inserting Recurrence details.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (552,N'Error in inserting AdvancedAduio Paramerters.','Error in inserting AdvancedAduio Paramerters.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (553,N'Error in inserting AdvancedAduio Paramerters.','Error in adding User details.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (554,N'Error in inserting Conference Custom Attributes records.','Error in inserting Conference Custom Attributes records.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (555,N'Error in inserting Conference Message Overlay records.','Error in inserting Conference Message Overlay records.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (556,N'Error in creating Conference Message for the organization.','Error in creating Conference Message for the organization.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (557,N'Invalid Start Mode.','Invalid Start Mode.','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (558,N'License Check FAILED: Endpoint limit exceeded.','License Check FAILED: Endpoint limit exceeded.','E',3,'U');


INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (604,N'La conferencia ya se elimin� o caduc�.','Conference alreay deleted or Expired','S',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (605,N'Por Favor, introduzca el n�mero de tel�fono','Please enter Phone Number','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (606,N'No se encontr� el correo-e de solicitud de Ayuda.','No Help Request email entry found.','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (607,N'Can not enable PC module, license count exceeded.','Can not enable PC module, license count exceeded.','E',3,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (608,N'Las conferencias actuales est�n enlazadas con Participantes PC, por favor contacte con el administrador para m�s ayuda.','Currently conferences are linked with PC attendees,please contact administrator for further assistance. ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (609,N'Por favor, a�ada al menos una opci�n a la opci�n personalizada.','Please add atleast one option to the custom option.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (610,N'La conferencia Punto a Punto debe tener asignados un Remitente y un Destinatario de la llamada.','Point-To-Point Conference must have a caller and callee assign. ','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (611,N'Por favor, introduzca una fecha/hora v�lida de inicio de la conferencia. Esta podr�a ser debido a la hora de establecimiento por defecto en la Opciones de Organizaci�n','Please enter a valid conference start date / time.This might be due to default setup-time in Organization Options.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (612,N'El l�mite m�ximo es de 24 horas incluyendo el periodo regulador. Esta podr�a ser debido al Periodo de carga por defecto en la Opciones de Organizaci�n','Maximum Limit is 24 hours including buffer period.This might be due to default buffer in Organization Options.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (613,N'La hora de establecimiento no puede ser anterior a la hora de previa al inicio del MCU.','Setup time cannot be less than the MCU pre start time.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (614,N'La hora del desmontaje no puede ser anterior a la hora de previa a la finalizaci�n del MCU.','Teardown time cannot be less than the MCU pre end time.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (615,N'El l�mite de salones de invitados por usuario excede el l�mite de salones de invitados.','Guest Room limit per user exceeds Guest Room limit.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (616,N'L�mite de salones de invitados excede la licencia VRM.','Guest Room limit exceeds VRM license.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (617,N'El sal�n de invitados no ha sido creado ya que excede la licencia del l�mite de salones de invitados.','Guest Room Not Created as Guest Room limit license exceeds.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (618,N'El sal�n de invitados no ha sido creado ya que excede la licencia del l�mite de salones de invitados por usuario.','Guest Room Not Created as Guest Room limit per user license exceeds.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (619,N'Por favor, elimine los salones de invitados para reducir la cuenta. Ya que hay m�s salones de invitados activos.','Please delete Guest Rooms to reduce the count.As there are more active Guest Rooms.','E',3,'U');


INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (620,N'S�lo puede serleccionar sal�n VMR para esta conferencia.','S�lo puede serleccionar sal�n VMR para esta conferencia.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (621,N'Por favor, seleccione s�lo un sal�n VMR.','Por favor, seleccione s�lo un sal�n VMR.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (622,N'Por favor, deseleccione Salones VMR.','Por favor, deseleccione Salones VMR.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (623,N'El Sal�n de Invitados s�lo se permite para Tipos de conferencia de Audio y V�deo.','El Sal�n de Invitados s�lo se permite para Tipos de conferencia de Audio y V�deo.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (624,N'El l�mite MCU mejorado excede la licencia VRM.','El l�mite MCU mejorado excede la licencia VRM.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (625,N'Verificaci�n de Licencia ERR�NEA: El l�mite MCU mejorado excede el MCU est�ndar.','Verificaci�n de Licencia ERR�NEA: El l�mite MCU mejorado excede el MCU est�ndar.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (626,N'El l�mite MCU mejorado excede el MCU est�ndar.','El l�mite MCU mejorado excede el MCU est�ndar.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (627,N'ID de inicio de sesi�n no encontrado. Operaci�n abortada o ID de usuario no suministrado.','ID de inicio de sesi�n no encontrado. Operaci�n abortada o ID de usuario no suministrado.','E',3,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (628,N'Por favor, elimine el MCU mejorado para reducir la cuenta. Ya que hay m�s MCU mejorados activos.','Por favor, elimine el MCU mejorado para reducir la cuenta. Ya que hay m�s MCU mejorados activos.','E',3,'U');





Insert into Err_List_S values(629,'User not authorized as Assistant-in-charge.','User not authorized as Assistant-in-charge.','E',3,'U')

Insert into Err_List_S values(630,'User not authorized as VNOC Operator.','User not authorized as VNOC Operator.','E',3,'U')

Insert into Err_List_S values(631,'User not authorized as Guest Room Assistant-in-charge.','User not authorized as Guest Room Assistant-in-charge.','E',3,'U')

Insert into Err_List_S values(632,'User not authorized as Approver.','User not authorized as Approver.','E',3,'U')

--FB 2609
Insert into Err_List_S values(633,'Conference schedule time is less than Meet and Greet buffer.','Conference schedule time is less than Meet and Greet buffer.','E',3,'U')

--FB 2609 - 07thFeb 2013
Update Err_List_S set Message = 'We�re sorry, but the request for Meet and Greet support does not meet the minimum notice requirement. Please choose another time for the conference, or deselect the Meet and Greet request to complete this conference schedule.', 
Description = 'We�re sorry, but the request for Meet and Greet support does not meet the minimum notice requirement. Please choose another time for the conference, or deselect the Meet and Greet request to complete this conference schedule.' where ID = 633 and Languageid = 3

--FB 2632 - 07thFeb 2013
Insert into Err_List_S values(634,'Please select Dedicated VNOC Operator.','Please select Dedicated VNOC Operator.','E',3,'U')

--FB 2626 - 11thFeb 2013

Insert into Err_List_S values(635,'The item is linked with a Inventory/Catering/Housekeeping Work orders. Cannot delete!','The item is linked with a Inventory/Catering/Housekeeping work orders. Cannot delete!','E',3,'U')
Insert into Err_List_S values(636,'Please associate at least one Menu with this Provider.','>Please associate at least one Menu with this Provider.','E',3,'U')

--FB 2599 - Vidyo - 21stFeb 2013
Insert into Err_List_S values(637,'License Check FAILED:Please reduce organizations limit count to 1.','License Check FAILED:Please reduce Max.Organizations count to 1.','E',3,'U')

Insert into Err_List_S values(638,'License Check FAILED:Cloud should be 1 or 0.','License Check FAILED:Cloud should be 1 or 0.','E',3,'U')


--FB 2594 - WhyGo - 21stFeb 2013
Insert into Err_List_S values(639,'License Check FAILED:Public Room Service should be 1 or 0.','License Check FAILED:Public Room Service should be 1 or 0.','E',3,'U')

Insert into Err_List_S values(640,'License Check FAILED:Public Rooms are linked with conference.','License Check FAILED:Public Rooms are linked with conference.','E',3,'U')

Insert into Err_List_S values(641,'License Check FAILED:Public Endpoint is link with Private Room.','License Check FAILED:Public Endpoint is link with Private Room.','E',3,'U')

Insert into Err_List_S values(642,'Public Rooms are linked with conference.Cannot remove!','Public Rooms are linked with conference.Cannot remove!','E',3,'U')

Insert into Err_List_S values(643,'Public Endpoint is link with Private Room.Cannot remove!','Public Endpoint is link with Private Room.Cannot remove!','E',3,'U')

--FB 2586 & FB 2531- VMR room ENHANCEMENT & Issue in setting License- Start23stFeb 2013

Insert into Err_List_S values(655,'VMR Rooms limit exceeds VRM license.','VMR Rooms limit exceeds VRM license.','E',3,'U')

Insert into Err_List_S values(656,'VMR rooms limit exceeded.','VMR rooms limit exceeded.','E',3,'U')

Insert into Err_List_S values(657,'Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.','Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.','E',3,'U')

--- validation for video room limits--
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (653,'License Check FAILED: Increase video room limit','License Check FAILED : Increase video room limit','E',3,'U')
 
--- validation for non-video room limits-- 
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (650,'License Check FAILED: Increase non-video room limit','License Check FAILED : Increase non-video room limit','E',3,'U')

--validation for vmr rooms limit---

insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
(658,'License Check FAILED: Increase VMR rooms limit','License check FAILED : Increase VMR rooms limit','E',3,'U')
  
 ---- validation for user limits---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (651,'License Check FAILED : Increase user limit', 
 'License Check FAILED : Increase user limit','E',3,'U')
 
 ---- validation for MCU limits---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (652,'License Check FAILED : Increase MCU limit','License Check FAILED : Increase MCU limit','E',3,'U')
  
 
 ---- validation for Guest room limit ----
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (654,'License Check FAILED : Increase guest room limit','License Check FAILED : Increase guest room limit','E',3,'U')
 

 ---- validation for Enhanced MCU Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (659,'License Check FAILED : Increase enhanced MCU limit','License Check FAILED : Increase enhanced MCU Limit','E',3,'U')
 

 ---- validation for GuestRoomLimit Per user Exceeds ----
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (660,'License Check FAILED : Increase GuestRoomLimit Per User Limit','License Check FAILED : Increase GuestRoomLimit Per User Limit','E',3,'U')
 

 ---- validation for Endpoint Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (661,'License Check FAILED : Increase endpoint limit','License Check FAILED : Increase endpoint limit','E',3,'U')
 
 ---- validation for Outlook User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (662,'License Check FAILED : Increase Outlook User Limit','License Check FAILED : Increase Outlook User Limit','E',3,'U')
 
 ---- validation for Notes User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (663,'License Check FAILED : Increase Notes User Limit','License Check FAILED : Increase Notes User Limit','E',3,'U')
 
 ---- validation for Mobile User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (664,'License Check FAILED : Increase Mobile User Limit','License Check FAILED : Increase Mobile User Limit','E',3,'U')
 
---- validation for Total all User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (644,'License Check FAILED : Increase all user limit','License Check FAILED : Increase all user limit','E',3,'U')
 
 ---- validation for Facility Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (645,'License Check FAILED : Facility Module is in use.Please increase count',
 'License Check FAILED : Facility Module is in use.Please increase count','E',3,'U')
 
 
---- validation for House Keeping Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (646,'License Check FAILED : Facility Services Module is in use.Please increase count',
 'License Check FAILED : Facility Services Module is in use.Please increase count','E',3,'U')
 
 ---- validation for Catering Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (647,'License Check FAILED : Catering Module is in use.Please increase count',
 'License Check FAILED : Catering Module is in use.Please increase count','E',3,'U')
 
  ---- validation for API Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (648,'License Check FAILED : API module is in use.Please increase count',
 'License Check FAILED : API module is in use.Please increase count','E',3,'U')
 
   ---- validation for PC Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (649,'License Check FAILED : PC module is in use.Please increase count',
 'License Check FAILED : PC module is in use.please increase count','E',3,'U')


--FB 2586 & FB 2531- VMR room ENHANCEMENT & Issue in setting License-  End23stFeb 2013

--FB 2636
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(666,N'No E.164 Service found.Please add alteast one E.164 Service.',N'No E164 Service.Please add alteast one E164 Service.','E',3,'U')

--FB 2441
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(665,'MCU is linked with conference(s).It cannot be edited.','MCU is linked with conference(s).It cannot be edited.','E',3,'U')

-- FB 2634
update err_list_s set Message = 'Invalid set-up/start time. Please enter a valid conference start date/time.',
Description = 'Invalid set-up/start time. Please enter a valid conference start date/time.' where id = 611 and Languageid =3


update err_list_s set Message = 'Maximum limit is 24 hours including buffer period. Please enter a valid duration.',
Description = 'Maximum limit is 24 hours including buffer period. Please enter a valid duration.' where id = 612 and Languageid =3

-- FB 2678
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(667,'Organization ExpiryDate check failed. Please contact your local administrator or myVRM for further assistance.','Organization ExpiryDate check failed. Please contact your local administrator or myVRM for further assistance. ','E',3,'U')


--FB 2653	
Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid,Errortype) values 
(668,'MCU IP address has been duplicated.','MCU IP address has been duplicated.','E',3,'U')
		
Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid,Errortype) values 
(669,'Note: MCU IP address is already in use.','Note: MCU IP address is already in use.','E',3,'U')
	
--FB 2410
insert into Err_List_S(ID,Message,Description,Level,Languageid,ErrorType)values(671,'The requested job name already exists.Please give another name','The requested job name already exists.Please give another name','S',3,'U')

/************************ FB 2693 10May 2013 **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(682,'PC user limit exceeds VRM license.','PC user limit exceeds VRM license.','E',3,'U')

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(672,'Please delete PC users to reduce the count.As there are more active pc users.','Please delete PC users to reduce the count.As there are more active pc users.','E',3,'U')	

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(673,'Can not enable Blue Jeans , license count exceeded.','Can not enable Blue Jeans , license count exceeded.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(674,'Can not enable Jabber , license count exceeded.','Can not enable Jabber , license count exceeded.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(675,'Can not enable Lync , license count exceeded.','Can not enable Lync , license count exceeded.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(676,'Can not enable Vidtel , license count exceeded.','Can not enable Vidtel , license count exceeded.','E',3,'U')

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(677,'License Check FAILED : Blue Jeans is in use.Please increase count','License Check FAILED : Blue Jeans is in use.Please increase count','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(678,'License Check FAILED : Jabber is in use.Please increase count','License Check FAILED : Jabber is in use.Please increase count','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(679,'License Check FAILED : Lync is in use.Please increase count','License Check FAILED : Lync is in use.Please increase count','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(680,'License Check FAILED : Vidtel is in use.Please increase count','License Check FAILED : Vidtel is in use.Please increase count','E',3,'U')

Update Err_List_S set Message = 'User limit should be inclusive for domino,exchange ,mobile and PC users.', Description='User limit should be inclusive for domino,exchange ,mobile and PC users.' where ID = 462 and Languageid = 3

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(681,'Error in inserting User PC records.','Error in inserting User PC records.','E',3,'S')

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(683,'License Check FAILED : PC User is in use.Please increase count','License Check FAILED : PC User is in use.Please increase count','E',3,'U')

/************************ FB 2693 10May 2013 **********************************/

/************************ FB 2441 Starts II 13May 2013 **********************************/
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(684,N'Password length should be 6 digits for synchronous MCU',N'Password length should be 6 digits for synchronous MCU','E',3,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(685,'Conference could not contain both Synchronous and Asynchronous MCU. Please change MCU','Conference could not contain both Synchronous and Asynchronous MCU. Please change MCU','E',3,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(686,'Synchronous MCU requires atleast two endpoints to schedule a conference','Synchronous MCU requires atleast two endpoints to schedule a conference','E',3,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(687,'Please select same synchronous MCU types to schedule a conference','Please select same synchronous MCU types to schedule a conference','E',3,'U')
/************************ FB 2441 II Ends 13May 2013 **********************************/


/* ************************ FB 2694 - Starts (17 May 2013)************************* */
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(692,'Please deactivate RO Hotdesking rooms to reduce the count.As there are more active RO Hotdesking rooms.','Please deactivate RO Hotdesking rooms to reduce the count.As there are more active RO Hotdesking rooms.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values 
(693,'VC Hotdesking rooms limit exceeds VRM license.','VC Hotdesking limit exceeds VRM license.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values 
(694,'RO Hotdesking rooms limit exceeds VRM license.','RO Hotdesking limit exceeds VRM license.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(695,'Please deactivate VC Hotdesking rooms to reduce the count.As there are more active VC Hotdesking rooms.','Please deactivate VC Hotdesking rooms to reduce the count.As there are more active VC Hotdesking rooms.','E',3,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(688,'License Check FAILED: Increase Hotdesking video room limit.','License Check FAILED: Increase Hotdesking video room limit.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(689,'License Check FAILED: Increase Hotdesking non-video room limit.','License Check FAILED: Increase Hotdesking non-video room limit.','E',3,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(690,'One or more selected hotdesking rooms have no audio-video endpoint associated.','One or more selected hotdesking rooms have no audio-video endpoint associated.','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(691,'Only hotdesking rooms are allowed for this conference type.','Only hotdesking rooms are allowed for this conference type.','E',3,'U')

/* ************************ FB 2694 - End (17 May 2013)************************* */
Update Err_List_S set ErrorType='S' where ID in(200,233,238,325,416,422,431,435,439,440,441,442,443,447,448,449,450,472,480,484,500,502,508,509,517,531,547,548,551,552,553,554,555,556)



/************************ FB 2593-MenuPart(26thJune2013-V2.9) Starts **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(700,'License Check FAILED : Advanced Report is in use. Please increase count','License Check FAILED : Advanced Report is in use. Please increase count','E',3,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(701,'Can not enable Advanced Report, license count exceeded.','Can not enable Advanced Report, license count exceeded.','E',3,'U')

/************************ FB 2593-MenuPart(26thJune2013-V2.9) End **********************************/


/************************ FB 2870- (15thJuly2013-V2.9) Start **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(702,'CTS Numeric ID is Required.','CTS Numeric ID is Required.','E',1,'U')

/************************ FB 2870- (15thJuly2013-V2.9) End **********************************/

/************************ FB 2457- (3rdSep2013-V2.9.2.5) Start **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values 
(703, 'Conference was not created Successfully.', 'Conference was not created Successfully.','E',1,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values 
(704, 'Please provide valid URL for connecting exchange service.', 'Please provide valid URL for connecting exchange service.','E',1,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values 
(705, 'Exchange Version Issue.', 'Exchange Version Issue.','E',1,'U')


/************************ FB 2870- (3rdSep2013-V2.9.2.5) Start **********************************/