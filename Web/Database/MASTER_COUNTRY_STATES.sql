

truncate table Gen_State_S


SET IDENTITY_INSERT [dbo].[Gen_State_S] ON

INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1, 225, N'AL', N'Alabama ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2, 225, N'AK', N'Alaska ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (3, 225, N'AZ', N'Arizona ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (4, 225, N'AR', N'Arkansas ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (5, 225, N'CA', N'California ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (6, 225, N'CO', N'Colorado ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (7, 225, N'CT', N'Connecticut ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (8, 225, N'DE', N'Delaware ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (9, 225, N'DC', N'District of Columbia ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (10, 225, N'FL', N'Florida ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (11, 225, N'GA', N'Georgia ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (12, 225, N'GU', N'Guam ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (13, 225, N'HI', N'Hawaii ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (14, 225, N'ID', N'Idaho ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (15, 225, N'IL', N'Illinois ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (16, 225, N'IN', N'Indiana ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (17, 225, N'IA', N'Iowa ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (18, 225, N'KS', N'Kansas ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (19, 225, N'KY', N'Kentucky ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (20, 225, N'LA', N'Louisiana ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (21, 225, N'ME', N'Maine ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (22, 225, N'MD', N'Maryland ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (23, 225, N'MA', N'Massachusetts ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (24, 225, N'MI', N'Michigan ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (25, 225, N'MN', N'Minnesota ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (26, 225, N'MS', N'Mississippi ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (27, 225, N'MO', N'Missouri ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (28, 225, N'MT', N'Montana ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (29, 225, N'NE', N'Nebraska ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (30, 225, N'NV', N'Nevada ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (31, 225, N'NH', N'New Hampshire ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (32, 225, N'NJ', N'New Jersey ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (33, 225, N'NM', N'New Mexico ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (34, 225, N'NY', N'New York ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (35, 225, N'NC', N'North Carolina ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (36, 225, N'ND', N'North Dakota ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (37, 225, N'OH', N'Ohio ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (38, 225, N'OK', N'Oklahoma ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (39, 225, N'OR', N'Oregon ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (40, 225, N'PA', N'Pennyslvania ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (41, 225, N'PR', N'Puerto Rico ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (42, 225, N'RI', N'Rhode Island ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (43, 225, N'SC', N'South Carolina ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (44, 225, N'SD', N'South Dakota ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (45, 225, N'TN', N'Tennessee ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (46, 225, N'TX', N'Texas ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (47, 225, N'UT', N'Utah ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (48, 225, N'VT', N'Vermont ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (49, 225, N'VA', N'Virginia ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (50, 225, N'VI', N'Virgin Islands ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (51, 225, N'WA', N'Washington ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (52, 225, N'WV', N'West Virginia ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (53, 225, N'WI', N'Wisconsin ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (54, 225, N'WY', N'Wyoming ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (55, 38, N'AB', N'Alberta ', 1)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (56, 38, N'BC', N'British Columbia ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (57, 38, N'MB', N'Manitoba ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (58, 38, N'NB', N'New Brunswick ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (59, 38, N'NL', N'Newfoundland and Labrador ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (60, 38, N'NT', N'Northwest Territories ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (61, 38, N'NS', N'Nova Scotia ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (62, 38, N'NU', N'Nunavut ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (63, 38, N'PE', N'Prince Edward Island ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (64, 38, N'SK', N'Saskatchewan ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (65, 38, N'ON', N'Ontario ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (66, 38, N'QC', N'Quebec ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (67, 38, N'YT', N'Yukon ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (68, 137, N'AGS', N'Aguascalientes ', 1)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (69, 137, N'BCN', N'Baja California Norte ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (70, 137, N'BCS', N'Baja California Sur ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (71, 137, N'CAM', N'Campeche ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (72, 137, N'CHIS', N'Chiapas ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (73, 137, N'CHIH', N'Chihuahua ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (74, 137, N'COAH', N'Coahuila ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (75, 137, N'COL', N'Colima ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (76, 137, N'DF', N'Distrito Federal ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (77, 137, N'DGO', N'Durango ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (78, 137, N'GTO', N'Guanajuato ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (79, 137, N'GRO', N'Guerrero ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (80, 137, N'HGO', N'Hidalgo ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (81, 137, N'JAL', N'Jalisco ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (82, 137, N'EDM', N'M�xico - Estado de ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (83, 137, N'MICH', N'Michoac�n ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (84, 137, N'MOR', N'Morelos ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (85, 137, N'NAY', N'Nayarit ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (86, 137, N'NL', N'Nuevo Le�n ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (87, 137, N'OAX', N'Oaxaca ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (88, 137, N'PUE', N'Puebla ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (89, 137, N'QRO', N'Quer�taro ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (90, 137, N'QROO', N'Quintana Roo ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (91, 137, N'SLP', N'San Luis Potos� ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (92, 137, N'SIN', N'Sinaloa ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (93, 137, N'SON', N'Sonora ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (94, 137, N'TAB', N'Tabasco ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (95, 137, N'TAMPS', N'Tamaulipas ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (96, 137, N'TLAX', N'Tlaxcala ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (97, 137, N'VER', N'Veracruz ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (98, 137, N'YUC', N'Yucat�n ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (99, 137, N'ZAC', N'Zacatecas ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (100, 1, N'KBL', N'Kabul', 0)
--GO
--print 'Processed 100 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (101, 1, N'KAN', N'Kandahar', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (102, 1, N'KDZ', N'Kondoz', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (103, 224, N'UKI', N'London', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (104, 224, N'WMD', N'West Midlands
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (105, 224, N'GTM', N'Greater Manchester
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (106, 224, N'WYK	', N'West Yorkshire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (107, 224, N'KEN	', N'Kent

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (108, 224, N'MSY	', N'Merseyside


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (109, 224, N'ESS', N'Essex', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (110, 224, N'SYK', N'South Yorkshire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (111, 224, N'HAM', N'Hampshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (112, 224, N'SRY', N'Surrey


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (113, 224, N'TWR', N'Tyne and Wear



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (114, 224, N'HRT', N'Hertfordshire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (115, 224, N'LAN', N'Lancashire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (116, 224, N'NTT', N'Nottinghamshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (117, 224, N'CHS', N'Cheshire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (118, 224, N'STS', N'Staffordshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (119, 224, N'DBY', N'Derbyshire


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (120, 224, N'NFK', N'Norfolk



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (121, 224, N'WSX', N'West Sussex
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (122, 224, N'NTH', N'Northamptonshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (123, 224, N'OXF', N'Oxfordshire


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (124, 224, N'DEV', N'Devon



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (125, 224, N'SFK', N'Suffolk




', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (126, 224, N'LIN', N'Lincolnshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (127, 224, N'SGC', N'Gloucestershire


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (128, 224, N'LEC', N'Leicestershire



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (129, 224, N'CAM', N'Cambridgeshire




', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (130, 224, N'ESX', N'East Sussex





', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (131, 224, N'DUR', N'Durham

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (132, 224, N'BST', N'Bristol


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (133, 224, N'WAR', N'Warwickshire



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (134, 224, N'BKM', N'Buckinghamshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (135, 224, N'NYK', N'North Yorkshire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (136, 224, N'BDF', N'Bedfordshire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (137, 224, N'CMA', N'Cumbria

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (138, 224, N'SOM', N'Somerset


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (139, 224, N'CON', N'Cornwall
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (140, 224, N'WIL', N'Wiltshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (141, 224, N'SHR', N'Shropshire


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (142, 224, N'LCE', N'Leicester
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (143, 224, N'WOR', N'Worcestershire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (144, 224, N'KHL', N'Kingston upon Hull


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (145, 224, N'PLY', N'Plymouth
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (146, 224, N'STE', N'Stoke-on-Trent

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (147, 224, N'DBY', N'Derby


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (148, 224, N'DOR', N'Dorset



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (149, 224, N'NGM', N'Nottingham
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (150, 224, N'STH', N'Southampton

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (151, 224, N'BNH', N'Brighton and Hove


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (152, 224, N'HEF', N'Herefordshire
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (153, 224, N'NBL', N'Northumberland

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (154, 224, N'POR', N'Portsmouth
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (155, 224, N'ERY', N'East Riding of Yorkshire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (156, 224, N'LUT', N'Luton


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (157, 224, N'SWD', N'Swindon

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (158, 224, N'SOS', N'Southend-on-Sea
SOS

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (159, 224, N'YOR', N'York
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (160, 224, N'SGC', N'South Gloucestershire

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (161, 224, N'MIK', N'Milton Keynes


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (162, 224, N'BMH', N'Bournemouth



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (163, 224, N'NSM', N'North Somerset

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (164, 224, N'WRT', N'Warrington


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (165, 224, N'PTE', N'Peterborough



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (166, 224, N'RDG', N'Reading




', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (167, 224, N'BPL', N'Blackpool





', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (168, 224, N'NEL', N'North East Lincolnshire






', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (169, 224, N'MDB', N'Middlesbrough






', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (170, 224, N'STT', N'Stockton-on-Tees







', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (171, 224, N'BBD', N'Blackburn with Darwen








', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (172, 224, N'TOB', N'Torbay
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (173, 224, N'POL', N'Poole

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (174, 224, N'WNM', N'Windsor and Maidenhead


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (175, 224, N'NLN', N'North Lincolnshire



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (176, 224, N'BAS', N'Bath and North East Somerset




', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (177, 224, N'SLG', N'Slough





', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (178, 224, N'HAL', N'Halton






', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (179, 224, N'IOW', N'Isle of Wight







', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (180, 224, N'BRC', N'Bracknell Forest








', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (181, 224, N'HPL', N'Hartlepool
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (182, 224, N'DAL', N'Darlington

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (183, 224, N'WBK', N'West Berkshire


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (184, 224, N'RCC', N'Redcar and Cleveland



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (185, 224, N'WOK', N'Wokingham




', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (186, 224, N'RUT', N'Rutland





', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (187, 152, N'AUK', N'Auckland�

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (188, 152, N'CAN', N'Canterbury�


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (189, 152, N'WKO', N'Hamilton


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (190, 152, N'WGN', N'Wellington


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (191, 152, N'NTL', N'Northland


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (192, 152, N'NSN', N'Nelson


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (193, 152, N'MBH', N'Marlborough


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (194, 152, N'GIS', N'Gisborne


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (195, 152, N'CIT', N'Chatham Islands


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (196, 152, N'CAN', N'Canterbury


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (197, 152, N'TKI', N'Taranaki


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (198, 152, N'STL', N'Southland


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (199, 1, N'HER', N'Herat


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (200, 1, N'JOW', N'Jowzjan


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (201, 1, N'LAG', N'Laghman


', 0)
GO
print 'Processed 200 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (202, 1, N'NUR', N'Nurestan


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (203, 1, N'NIM', N'Nimruz


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (204, 1, N'ZAB', N'Zabol


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (205, 1, N'GHA', N'Ghazni


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (206, 1, N'KNR', N'Konar


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (207, 1, N'PAR', N'Parvan


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (208, 3, N'AL', N'Alger



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (209, 3, N'AN', N'Annaba
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (210, 3, N'BT', N'Batna
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (211, 3, N'BC', N'Bechar

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (212, 3, N'BJ', N'Bejaia


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (213, 3, N'BS', N'Biskra



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (214, 3, N'BL', N'Blida




', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (215, 3, N'BU', N'Bouira





', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (216, 3, N'CO', N'Constantine






', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (217, 3, N'DJ', N'Djelfa







', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (218, 3, N'Gl', N'Guelma








', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (219, 3, N'JJ', N'Jijel









', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (220, 3, N'LG', N'Laghouat










', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (221, 3, N'MC', N'Mascara











', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (222, 3, N'MD', N'Medea












', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (223, 3, N'MG', N'Mostaganem













', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (224, 3, N'OR', N'Oran














', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (225, 3, N'OG', N'Ouargla















', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (226, 3, N'SD', N'Saida
















', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (227, 3, N'SF', N'Setif

', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (228, 3, N'SK', N'Skikda


', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (229, 3, N'TB', N'Tebessa



', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (230, 3, N'TR', N'Tiaret




', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (231, 3, N'TL', N'Tlemcen





', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (232, 3, N'TL', N'Tlemcen
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (233, 13, N'JBT', N'Jervis Bay Territory	
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (234, 13, N'ACT', N'Australian Capital Territory', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (235, 13, N'NSW', N'New South Wales', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (236, 13, N'NT', N'Northern Territory', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (237, 13, N'QLD', N'Queensland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (238, 13, N'SA', N'South Australia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (239, 13, N'	TAS', N'Tasmania', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (240, 13, N'	VIC', N'Victoria', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (241, 13, N'WA', N'Western Australia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (242, 13, N'ACI', N'Ashmore and Cartier Islands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (243, 13, N'CSIT', N'Coral Sea Islands Territory', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (244, 14, N'BU', N'Burgenland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (245, 14, N'KA', N'Carinthia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (246, 14, N'NO', N'Lower Austria', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (247, 14, N'SZ', N'Salzburg', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (248, 14, N'ST', N'Styria', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (249, 14, N'Tl', N'Tyrol', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (250, 14, N'OO', N'Upper Austria', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (251, 14, N'WI', N'Vienna', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (252, 14, N'vo', N'Vorarlberg', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (253, 17, N'hd', N'Al Hadd', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (254, 17, N'MN', N'Al Manamah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (255, 17, N'MG', N'Al Mintaqah al Gharbiyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (256, 17, N'MW', N'Al Mintaqah al Wusta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (257, 17, N'MS', N'Al Mintaqah ash Shamaliyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (258, 17, N'MQ', N'Al Muharraq', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (259, 17, N'RF', N'Ar Rifa` wa al Mintaqah al Janubiyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (260, 17, N'JH', N'Jidd Hafs', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (261, 17, N'MH', N'Madinat Hamad', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (262, 17, N'MI', N'Madinat `Isa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (263, 17, N'MJ', N'Mintaqat Juzur Hawar', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (264, 17, N'ST', N'Sitrah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (265, 18, N'BA', N'Barisal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (266, 18, N'CG', N'Chittagong', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (267, 18, N'DA', N'Dhaka', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (268, 18, N'KH', N'Khulna', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (269, 18, N'RS', N'Rajshahi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (270, 18, N'RP', N'Rangpur', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (271, 18, N'SY', N'Sylhet', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (272, 238, N'BU', N'Bulawayo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (273, 238, N'HA', N'Harare', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (274, 238, N'MA', N'Manicaland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (275, 238, N'MC', N'Mashonaland Central', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (276, 238, N'ME', N'Mashonaland East', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (277, 238, N'MW', N'Mashonaland West', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (278, 238, N'MV', N'Masvingo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (279, 238, N'MN', N'Matabeleland North', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (280, 238, N'MS', N'Matabeleland South', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (281, 238, N'MI', N'Midlands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (282, 237, N'CN', N'Central', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (283, 237, N'CB', N'Copperbelt', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (284, 237, N'LK', N'Lusaka', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (285, 223, N'AZ', N'Abu Dhabi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (286, 223, N'AJ', N'Ajman', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (287, 223, N'DU', N'Dubai', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (288, 223, N'FU', N'Fujayrah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (289, 223, N'RK', N'Ras al Khaymah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (290, 223, N'SH', N'Sharjah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (291, 223, N'UQ', N'Umm al Qaywayn', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (292, 199, N'AP', N'Ampara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (293, 199, N'AD', N'Anuradhapura	', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (294, 199, N'BD', N'Badulla', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (295, 199, N'BC', N'Batticaloa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (296, 199, N'CO', N'Colombo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (297, 199, N'Gl', N'Galle', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (298, 199, N'GQ', N'Gampaha', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (299, 199, N'HB', N'Hambantota', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (300, 199, N'JA', N'Jaffna', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (301, 199, N'KT', N'Kalutara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (302, 199, N'KY', N'Kandy', 0)
GO
print 'Processed 300 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (303, 199, N'KE', N'Kegalle', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (304, 199, N'KL', N'Kilinochchi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (305, 199, N'KG', N'Kurunegala', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (306, 199, N'MB', N'Mannar', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (307, 199, N'MT', N'Matale', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (308, 199, N'MH', N'Matara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (309, 199, N'MJ', N'Moneragala', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (310, 199, N'MV', N'Mullaitivu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (311, 199, N'NW', N'Nuwara Eliya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (312, 199, N'PR', N'Polonnaruwa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (313, 199, N'PX', N'Puttalam', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (314, 199, N'RN', N'Ratnapura', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (315, 199, N'TC', N'Trincomalee', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (316, 199, N'VA', N'Vavuniya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (317, 196, N'EC', N'Eastern Cape', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (318, 196, N'FS', N'Free State', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (319, 196, N'GT', N'Gauteng', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (320, 196, N'NL', N'KwaZulu-Natal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (321, 196, N'LP', N'Limpopo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (322, 196, N'MP', N'Mpumalanga', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (323, 196, N'NC', N'Northern Cape', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (324, 196, N'NW', N'North-West', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (325, 196, N'WC', N'Western Cape', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (326, 186, N'BA', N'Al Bahah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (327, 186, N'HS', N'Al ?udud ash Shamaliyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (328, 186, N'JF', N'Al Jawf', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (329, 186, N'MD', N'Al Madinah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (330, 186, N'QS', N'Al Qa?im', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (331, 186, N'RI', N'Al Riya?', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (332, 186, N'SH', N'Ash Sharqiyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (333, 186, N'AS', N'Asir', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (334, 186, N'HA', N'?ail', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (335, 186, N'JZ', N'Jizan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (336, 186, N'MK', N'Makkah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (337, 186, N'NJ', N'Najran', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (338, 186, N'TB', N'Tabuk', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (339, 173, N'DA', N'Ad Dawhah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (340, 173, N'DY', N'Al Daayen', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (341, 173, N'KH', N'Al Khawr', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (342, 173, N'WA', N'Al Wakrah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (343, 173, N'RA', N'Ar Rayyan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (344, 173, N'MS', N'Madinat ach Shamal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (345, 173, N'US', N'Umm Salal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (346, 171, N'BR', N'Braga 
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (347, 171, N'CO', N'Co�mbra 
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (348, 171, N'LB', N'Lisboa 
 
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (349, 171, N'PO', N'Porto 
 
 
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (350, 171, N'CO', N'Coimbra', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (351, 171, N'BR', N'Braga', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (352, 171, N'BE', N'Beja', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (353, 171, N'VL', N'Vila Real', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (354, 171, N'EV', N'�vora', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (355, 161, N'JK', N'Azad Kashmir', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (356, 161, N'BA', N'Balochistan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (357, 161, N'TA', N'Federally Administered Tribal Areas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (358, 161, N'GB', N'Gilgit-Baltistan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (359, 161, N'IS', N'Islamabad', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (360, 161, N'KP', N'Khyber-Pakhtunkhwa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (361, 161, N'PB', N'Punjab', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (362, 161, N'SD', N'Sindh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (363, 148, N'MM', N'Central', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (364, 148, N'PW', N'Eastern', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (365, 148, N'SP', N'Far-Western', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (366, 148, N'MP', N'Mid-Western', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (367, 148, N'PM', N'Western', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (368, 145, N'AY', N'Ayeyarwady', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (369, 145, N'BA', N'Bago', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (370, 145, N'CH', N'Chin', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (371, 145, N'KC', N'Kachin', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (372, 145, N'KH', N'Kayah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (373, 145, N'KN', N'Kayin', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (374, 145, N'MG', N'Magway', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (375, 145, N'MD', N'Mandalay', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (376, 145, N'MO', N'Mon', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (377, 145, N'RA', N'Rakhine', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (378, 145, N'SA', N'Sagaing', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (379, 145, N'SH', N'Shan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (380, 145, N'TN', N'Tanintharyi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (381, 145, N'Ya', N'Yangon', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (382, 135, N'', N'', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (383, 135, N'AG', N'Agalega Islands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (384, 135, N'BL', N'Black River', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (385, 135, N'CC', N'Cargados Carajos', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (386, 135, N'FL', N'Flacq', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (387, 135, N'GP', N'Grand Port', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (388, 135, N'MO', N'Moka', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (389, 135, N'PA', N'Pamplemousses', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (390, 135, N'PW', N'Plaines Wilhems', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (391, 135, N'PL', N'Port Louis', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (392, 135, N'RR', N'Rivi�re du Rempart', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (393, 135, N'Ro', N'Rodrigues', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (394, 135, N'SA', N'Savanne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (395, 128, N'JH', N'Johor', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (396, 128, N'KH', N'Kedah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (397, 128, N'KN', N'Kelantan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (398, 128, N'KL', N'Kuala Lumpur', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (399, 128, N'LA', N'Labuan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (400, 128, N'ME', N'Melaka', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (401, 128, N'NS', N'Negeri Sembilan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (402, 128, N'PH', N'Pahang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (403, 128, N'PK', N'Perak', 0)
GO
print 'Processed 400 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (404, 128, N'PL', N'Perlis', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (405, 128, N'PG', N'Pulau Pinang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (406, 128, N'PJ', N'Putrajaya	', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (407, 128, N'SA', N'Sabah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (408, 128, N'SK', N'Sarawak', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (409, 128, N'SL', N'Selangor', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (410, 128, N'TE', N'Terengganu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (411, 126, N'AV', N'Antananarivo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (412, 126, N'AS', N'Antsiranana', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (413, 126, N'FI', N'Fianarantsoa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (414, 126, N'MA', N'Mahajanga', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (415, 126, N'TM', N'Toamasina', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (416, 126, N'Tl', N'Toliara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (417, 113, N'AH', N'Al Ahmadi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (418, 113, N'FA', N'Al Farwaniyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (419, 113, N'JA', N'Al Farwaniyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (420, 113, N'KU', N'Al Farwaniyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (421, 113, N'HW', N'Hawalli', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (422, 113, N'MU', N'Mubarak Al-Kabir', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (423, 104, N'AG', N'Agrigento', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (424, 104, N'PU', N'Apulia�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (425, 104, N'CB', N'Calabria�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (426, 104, N'CP', N'Campania�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (427, 104, N'ER', N'Emilia-Romagna�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (428, 104, N'FV', N'Friuli-Venezia Giuli�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (429, 104, N'LT', N'Latium�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (430, 104, N'LG', N'Liguria�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (431, 104, N'LB', N'Lombardia�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (432, 104, N'MH', N'Marche�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (433, 104, N'PM', N'Piemonte�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (434, 104, N'SD', N'Sardinia�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (435, 104, N'TT', N'Trentino-Alto Adige�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (436, 104, N'UM', N'Umbria�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (437, 104, N'VN', N'Veneto�
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (438, 104, N'TC', N'Tuscany', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (439, 104, N'VD', N'Valle dAosta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (440, 72, N'AI', N'Ain', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (441, 72, N'AS', N'Aisne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (442, 72, N'AL', N'Allier', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (443, 72, N'AN', N'Ardennes', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (444, 72, N'AD', N'Aude', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (445, 72, N'BD', N'Bouches-du-Rh�ne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (446, 72, N'CV', N'Calvados', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (447, 72, N'CL', N'Cantal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (448, 72, N'CT', N'Charente', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (449, 72, N'DD', N'Dordogne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (450, 72, N'DB', N'Doubs', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (451, 72, N'EU', N'Eure', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (452, 72, N'ES', N'Essonne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (453, 72, N'GE', N'Gers', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (454, 72, N'GA', N'Gard', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (455, 72, N'GI', N'Gironde', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (456, 72, N'JU', N'Jura', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (457, 72, N'LD', N'Landes', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (458, 72, N'LT', N'Loiret', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (459, 72, N'LZ', N'Loz�re', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (460, 72, N'MO', N'Moselle', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (461, 72, N'NO', N'Nord', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (462, 72, N'OI', N'Oise', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (463, 72, N'RH', N'Rh�ne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (464, 72, N'ST', N'Sarthe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (465, 72, N'TA', N'Tarn', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (466, 72, N'Yo', N'Yonne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (467, 5, N'BGO', N'Bengo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (468, 5, N'BGU', N'Benguela', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (469, 5, N'BIE', N'Bi�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (470, 5, N'CAB', N'Cabinda', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (471, 5, N'CNN', N'Cunene', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (472, 5, N'HUA', N'Huambo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (473, 5, N'HUI', N'Hu�la', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (474, 5, N'LUA', N'Luanda', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (475, 5, N'LNO', N'Lunda Norte	', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (476, 5, N'LSU', N'Lunda Sul', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (477, 5, N'MAL', N'Malanje', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (478, 5, N'MOX', N'Moxico', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (479, 5, N'NAM', N'Namibe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (480, 5, N'UIG', N'U�ge', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (481, 5, N'ZAI', N'Zaire', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (482, 10, N'BA', N'Buenos Aires', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (483, 10, N'CT', N'Catamarca', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (484, 10, N'CC', N'Chaco', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (485, 10, N'CH', N'Chubut', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (486, 10, N'CB', N'C�rdoba', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (487, 10, N'CN', N'Corrientes', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (488, 10, N'ER', N'Entre R�os', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (489, 10, N'FM', N'Formosa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (490, 10, N'JY', N'Jujuy', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (491, 10, N'LR', N'La Rioja', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (492, 10, N'MZ', N'Mendoza', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (493, 10, N'MN', N'Misiones', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (494, 10, N'NQ', N'Neuqu�n', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (495, 10, N'RN', N'R�o Negro', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (496, 10, N'SA', N'Salta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (497, 10, N'SC', N'Santa Cruz', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (498, 10, N'SF', N'Santa Fe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (499, 10, N'TF', N'Tierra del Fuego', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (500, 10, N'TM', N'Tucum�n', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (501, 18, N'BA', N'Barisal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (502, 18, N'CG', N'Chittagong', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (503, 18, N'DA', N'Dhaka', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (504, 18, N'KH', N'Khulna', 0)
GO
print 'Processed 500 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (505, 18, N'RS', N'Rajshahi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (506, 18, N'RP', N'Rangpur', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (507, 18, N'SY', N'Sylhet', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (508, 21, N'AN', N'Antwerp', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (509, 21, N'BU', N'Brussels', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (510, 21, N'OV', N'East Flanders', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (511, 21, N'VB', N'Flemish Brabant', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (512, 21, N'HT', N'Hainaut', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (513, 21, N'LG', N'Liege', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (514, 21, N'LI', N'Limburg', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (515, 21, N'LX', N'Luxembourg', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (516, 21, N'NA', N'Namur', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (517, 21, N'BW', N'Walloon Brabant', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (518, 21, N'WV', N'West Flanders', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (519, 24, N'DE', N'Devonshire', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (520, 24, N'HA', N'Hamilton', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (521, 24, N'HC', N'Hamilton municipality', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (522, 24, N'PA', N'Paget', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (523, 24, N'PE', N'Pembroke', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (524, 24, N'SG', N'Saint George municipality', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (525, 24, N'SC', N'Saint Georges', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (526, 24, N'SA', N'Sandys', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (527, 24, N'SM', N'Smiths', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (528, 24, N'SO', N'Southampton', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (529, 24, N'WA', N'Warwick', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (530, 30, N'AC', N'Acre', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (531, 30, N'AL', N'Alagoas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (532, 30, N'AP', N'Amap�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (533, 30, N'AM', N'Amazonas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (534, 30, N'BA', N'Bahia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (535, 30, N'CE', N'Cear�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (536, 30, N'DF', N'Distrito Federal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (537, 30, N'ES', N'Esp�rito Santo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (538, 30, N'GO', N'Goi�s', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (539, 30, N'MA', N'Maranh�o', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (540, 30, N'MT', N'Mato Grosso', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (541, 30, N'MS', N'Mato Grosso do Sul', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (542, 30, N'MG', N'Minas Gerais', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (543, 30, N'PA', N'Par�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (544, 30, N'PB', N'Para�ba', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (545, 30, N'PR', N'Paran�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (546, 30, N'PE', N'Pernambuco', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (547, 30, N'PI', N'Piau�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (548, 30, N'RJ', N'Rio de Janeiro', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (549, 30, N'RN', N'Rio Grande do Norte', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (550, 30, N'RS', N'Rio Grande do Sul', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (551, 30, N'RO', N'Rond�nia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (552, 30, N'RR', N'Roraima', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (553, 30, N'SC', N'Santa Catarina', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (554, 30, N'SP', N'S�o Paulo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (555, 30, N'SE', N'Sergipe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (556, 30, N'TO', N'Tocantins', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (557, 33, N'BL', N'Blagoevgrad', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (558, 33, N'BR', N'Burgas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (559, 33, N'SG', N'Grad Sofiya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (560, 33, N'KK', N'Khaskovo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (561, 33, N'LV', N'Lovech', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (562, 33, N'MT', N'Montana', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (563, 33, N'PN', N'Pernik', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (564, 33, N'PD', N'Plovdiv', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (565, 33, N'RS', N'Ruse', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (566, 33, N'SH', N'Shumen', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (567, 33, N'SL', N'Sliven', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (568, 33, N'TU', N'Turgovishte', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (569, 33, N'YA', N'Yambol', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (570, 44, N'AH', N'Anhui', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (571, 44, N'BJ', N'Beijing', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (572, 44, N'CQ', N'Chongqing', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (573, 44, N'FJ', N'Fujian', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (574, 44, N'GS', N'Gansu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (575, 44, N'GD', N'Guangdong', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (576, 44, N'GX', N'Guangxi Zhuang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (577, 44, N'GZ', N'Guizhou', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (578, 44, N'HN', N'Hainan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (579, 44, N'HB', N'Hebei', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (580, 44, N'HL', N'Heilongjiang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (581, 44, N'HE', N'Henan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (582, 44, N'HU', N'Hubei', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (583, 44, N'HN', N'Hunan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (584, 44, N'JS', N'Jiangsu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (585, 44, N'LN', N'Liaoning', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (586, 44, N'SA', N'Shaanxi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (587, 44, N'SD', N'Shandong', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (588, 44, N'SH', N'Shanghai', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (589, 44, N'SX', N'Shanxi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (590, 44, N'SC', N'Sichuan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (591, 44, N'TJ', N'Tianjin', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (592, 44, N'XJ', N'Xinjiang Uygur', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (593, 44, N'XZ', N'Xizang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (594, 44, N'YN', N'Yunnan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (595, 44, N'ZJ', N'Zhejiang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (596, 47, N'AM', N'Amazonas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (597, 47, N'AN', N'Antioquia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (598, 47, N'AR', N'Arauca', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (599, 47, N'AT', N'Atl�ntico', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (600, 47, N'BL', N'Atl�ntico', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (601, 47, N'BY', N'Boyac�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (602, 47, N'CL', N'Caldas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (603, 47, N'CQ', N'Caquet�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (604, 47, N'CS', N'Casanare', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (605, 47, N'CA', N'Cauca', 0)
GO
print 'Processed 600 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (606, 47, N'CE', N'Cesar', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (607, 47, N'CH', N'Choc�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (608, 47, N'CO', N'C�rdoba', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (609, 47, N'CU', N'Cundinamarca', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (610, 47, N'GN', N'Guain�a', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (611, 47, N'GV', N'Guaviare', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (612, 47, N'HU', N'Huila', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (613, 47, N'LG', N'La Guajira', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (614, 47, N'MA', N'Magdalena', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (615, 47, N'ME', N'Meta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (616, 47, N'NA', N'Nari�o', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (617, 47, N'NS', N'Norte de Santander', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (618, 47, N'PU', N'Putumayo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (619, 47, N'QD', N'Quind�o', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (620, 47, N'RI', N'Risaralda', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (621, 47, N'SA', N'San Andr�s y Providencia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (622, 47, N'ST', N'Santander', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (623, 47, N'SU', N'Sucre', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (624, 47, N'To', N'Tolima', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (625, 47, N'VC', N'Valle del Cauca', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (626, 47, N'VP', N'Vaup�s', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (627, 47, N'VD', N'Vichada', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (628, 55, N'FA', N'Famagusta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (629, 55, N'KY', N'Kyrenia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (630, 55, N'LA', N'Larnaca', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (631, 55, N'LI', N'Limassol', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (632, 55, N'NI', N'Nicosia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (633, 55, N'PA', N'Paphos', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (634, 57, N'HS', N'Capital', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (635, 57, N'MJ', N'Central Jutland	', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (636, 57, N'ND', N'North Jutland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (637, 57, N'SL', N'Zealand', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (638, 57, N'SD', N'South Denmark', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (639, 62, N'DQ', N'Ad Daqahliyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (640, 62, N'BA', N'Al Bahr al Ahmar', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (641, 62, N'BH', N'Al Buhayrah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (642, 62, N'FY', N'Al Fayyum', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (643, 62, N'GH', N'Al Gharbiyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (644, 62, N'IK', N'Al Iskandariyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (645, 62, N'IS', N'Al Isma`iliyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (646, 62, N'JZ', N'Al Jizah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (647, 62, N'MF', N'Al Minufiyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (648, 62, N'MN', N'Al Minya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (649, 62, N'QH', N'Al Qahirah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (650, 62, N'QL', N'Al Qalyubiyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (651, 62, N'UQ', N'Al Uqsur', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (652, 62, N'WJ', N'Al Wadi al Jadid', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (653, 62, N'SQ', N'Ash Sharqiyah', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (654, 62, N'SW', N'As Suways', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (655, 62, N'AN', N'Aswan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (656, 62, N'AT', N'Asyut', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (657, 62, N'BN', N'Bani Suwayf', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (658, 62, N'BS', N'Bur Sa`id', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (659, 62, N'Dt', N'Dumyat', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (660, 62, N'JS', N'Janub Sina', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (661, 62, N'KS', N'Kafr ash Shaykh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (662, 62, N'MT', N'Matruh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (663, 62, N'QN', N'Qina', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (664, 62, N'SS', N'Shamal Sina ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (665, 62, N'SJ', N'Suhaj', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (693, 71, N'CF', N'Central Finland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (694, 71, N'CO', N'Central Ostrobothnia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (695, 71, N'KA', N'Kainuu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (696, 71, N'KH', N'Kanta-H�me', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (697, 71, N'KY', N'Kymenlaakso', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (698, 71, N'LA', N'Lapland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (699, 71, N'NK', N'North Karelia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (700, 71, N'NO', N'North Ostrobothnia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (701, 71, N'NS', N'North Savo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (702, 71, N'OS', N'Ostrobothnia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (703, 71, N'PH', N'P�ij�t-H�me', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (704, 71, N'SA', N'Satakunta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (705, 71, N'SK', N'South Karelia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (706, 71, N'SO', N'South Ostrobothnia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (707, 71, N'SS', N'South Savo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (708, 71, N'SF', N'Southwest Finland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (709, 71, N'TR', N'Tampere Region', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (710, 71, N'US', N'Uusimaa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (711, 79, N'BW', N'Baden-Wurttemberg', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (712, 79, N'BY', N'Bavaria', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (713, 79, N'BE', N'Berlin', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (714, 79, N'BB', N'Brandenburg', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (715, 79, N'HB', N'Bremen', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (716, 79, N'HH', N'Hamburg', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (717, 79, N'HE', N'Hesse', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (718, 79, N'NI', N'Lower Saxony', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (719, 79, N'MV', N'Mecklenburg-West Pomerania', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (720, 79, N'NW', N'North Rhine-Westphalia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (721, 79, N'RP', N'Rhineland-Palatinate', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (722, 79, N'SL', N'Saarland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (723, 79, N'SN', N'Saxony', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (724, 79, N'ST', N'Saxony-Anhalt', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (725, 79, N'SH', N'Schleswig-Holstein', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (726, 79, N'TH', N'Thuringia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (727, 80, N'AH', N'Ashanti', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (728, 80, N'BA', N'Brong-Ahafo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (729, 80, N'CP', N'Central', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (730, 80, N'EP', N'Eastern', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (731, 80, N'AA', N'Greater Accra', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (732, 80, N'NP', N'Northern ', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (733, 80, N'UE', N'Upper East', 0)
GO
print 'Processed 700 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (734, 80, N'UW', N'Upper West', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (735, 80, N'TV', N'Volta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (736, 80, N'WP', N'Western', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (737, 82, N'AI', N'Aegean', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (738, 82, N'AT', N'Attica', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (739, 82, N'CR', N'Crete', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (740, 82, N'EM', N'Epirus and Western Macedonia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (741, 82, N'MH', N'Macedonia and Thrace', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (742, 82, N'MA', N'Mount Athos', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (743, 82, N'PW', N'Peloponnese, Western Greece and the Ionian Islands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (744, 82, N'TC', N'Thessaly and Central Greece', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (745, 83, N'KU', N'Kujalleq', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (746, 83, N'QS', N'Qaasuitsup', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (747, 83, N'QT', N'Qeqqata', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (748, 83, N'SE', N'Sermersooq', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (749, 83, N'UO', N'Unorganized', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (750, 235, N'BO', N'Boujdour', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (751, 235, N'ES', N'Es Semara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (752, 235, N'LA', N'Laayoune', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (753, 235, N'OD', N'Oued el Dahab', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (754, 234, N'AL', N'Alo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (755, 234, N'SI', N'Sigave', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (756, 234, N'UV', N'Uv�a', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (757, 233, N'SC', N'Saint Croix', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (758, 233, N'SJ', N'Saint John', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (759, 233, N'ST', N'Saint Thomas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (760, 232, N'VI', N'British Virgin Islands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (761, 231, N'AG', N'An Giang	', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (762, 231, N'BV', N'Ba Ria-Vung Tau', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (763, 231, N'BN', N'Bac Ninh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (764, 231, N'BD', N'Binh Dinh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (765, 231, N'BU', N'Binh Thuan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (766, 231, N'CN', N'Can Tho', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (767, 231, N'DC', N'Dac Lac', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (768, 231, N'DN', N'Dong Nai', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (769, 231, N'HP', N'Haiphong', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (770, 231, N'HN', N'Hanoi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (771, 231, N'HC', N'Ho Chi Minh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (772, 231, N'KH', N'Khanh Hoa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (773, 231, N'KG', N'Kien Giang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (774, 231, N'LD', N'Lam Dong', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (775, 231, N'ND', N'Nam Dinh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (776, 231, N'NA', N'Nghe An', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (777, 231, N'QB', N'Quang Binh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (778, 231, N'QM', N'Quang Nam', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (779, 231, N'QN', N'Quang Ninh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (780, 231, N'TT', N'Thua Thien-Hue', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (781, 231, N'TG', N'Tien Giang', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (782, 231, N'YB', N'Yen Bai', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (783, 230, N'AN', N'Anzo�tegui', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (784, 230, N'AP', N'Apure', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (785, 230, N'AR', N'Aragua', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (786, 230, N'BA', N'Barinas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (787, 230, N'BO', N'Bol�var', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (788, 230, N'CA', N'Carabobo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (789, 230, N'DF', N'Distrito Capital', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (790, 230, N'FA', N'Falc�n', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (791, 230, N'GU', N'Gu�rico', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (792, 230, N'LA', N'Lara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (793, 230, N'ME', N'M�rida', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (794, 230, N'MI', N'Miranda', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (795, 230, N'MO', N'Monagas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (796, 230, N'PO', N'Portuguesa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (797, 230, N'SU', N'Sucre', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (798, 230, N'TA', N'T�chira', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (799, 230, N'TR', N'Trujillo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (800, 230, N'YA', N'Yaracuy', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (801, 230, N'ZU', N'Zulia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (802, 228, N'AN', N'Andijon', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (803, 228, N'BU', N'Buxoro', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (804, 228, N'FA', N'Farg`ona', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (805, 228, N'JI', N'Jizzax', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (806, 228, N'QR', N'Karakalpakstan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (807, 228, N'QA', N'Kashkadarya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (808, 228, N'NG', N'Namangan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (809, 228, N'NW', N'Navoi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (810, 228, N'SA', N'Samarkand', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (811, 228, N'SI', N'Sirdaryo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (812, 228, N'SU', N'Surxondaryo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (813, 228, N'TO', N'Tashkent', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (814, 228, N'TK', N'Tashkent City', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (815, 228, N'XO', N'Xorazm', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (816, 227, N'AR', N'Artigas', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (817, 227, N'CA', N'Canelones', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (818, 227, N'CL', N'Cerro Largo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (819, 227, N'CO', N'Colonia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (820, 227, N'DU', N'Durazno', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (821, 227, N'FS', N'Flores', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (822, 227, N'FD', N'Florida', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (823, 227, N'LA', N'Lavalleja', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (824, 227, N'MA', N'Maldonado', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (825, 227, N'MO', N'Montevideo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (826, 227, N'PA', N'Paysand�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (827, 227, N'RN', N'R�o Negro', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (828, 227, N'RV', N'Rivera', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (829, 227, N'RO', N'Rocha', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (830, 227, N'SA', N'Salto', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (831, 227, N'SJ', N'San Jos�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (832, 227, N'SO', N'Soriano', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (833, 227, N'TA', N'Tacuaremb�', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (834, 227, N'TT', N'Treinta y Tres', 0)
GO
print 'Processed 800 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (835, 222, N'CK', N'Cherkasy', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (836, 222, N'CH', N'Chernihiv', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (837, 222, N'CV', N'Chernivtsi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (838, 222, N'KR', N'Crimea', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (839, 222, N'DP', N'Dnipropetrovsk', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (840, 222, N'DT', N'Donetsk', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (841, 222, N'IF', N'Ivano-Frankivs', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (842, 222, N'KK', N'Kharkiv', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (843, 222, N'KS', N'Kherson', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (844, 222, N'KM', N'Khmelnytskyy', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (845, 222, N'KV', N'Kiev', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (846, 222, N'KC', N'Kiev City', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (847, 222, N'KL', N'Kirovohrad', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (848, 222, N'LH', N'Luhans', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (849, 222, N'LV', N'Lviv', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (850, 222, N'MY', N'Mykolayiv', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (851, 222, N'OD', N'Odessa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (852, 222, N'PL', N'Poltava', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (853, 222, N'RV', N'Rivne', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (854, 222, N'SC', N'Sevastopol City', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (855, 222, N'SM', N'Sumy', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (856, 222, N'TP', N'Ternopil', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (857, 222, N'ZK', N'Transcarpathia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (858, 222, N'Vi', N'Vinnytsya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (859, 222, N'VO', N'Volyn', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (860, 222, N'ZP', N'Zaporizhzhya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (861, 222, N'ZT', N'Zhytomyr', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (862, 221, N'KM', N'kampala', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (863, 221, N'MV', N'Mukono
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (864, 221, N'GU', N'Gulu
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (865, 221, N'LI', N'Lira
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (866, 221, N'AA', N'Arua
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (867, 221, N'JI', N'Jinja
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (868, 221, N'RR', N'Mbarara
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (869, 221, N'KS', N'Kasese
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (870, 221, N'LW', N'Luwero
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (871, 221, N'MA', N'Masaka
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (872, 221, N'ML', N'Mbale
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (873, 221, N'KN', N'Kalangala
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (874, 221, N'TG', N'Kitgum
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (875, 221, N'IC', N'Iganga
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (876, 221, N'MD', N'Mubende
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (877, 221, N'NB', N'Nebbi
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (878, 221, N'TR', N'Tororo
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (879, 221, N'HO', N'Hoima
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (880, 221, N'SO', N'Soroti
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (881, 221, N'KB', N'Kabarole
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (882, 221, N'BU', N'Busia
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (883, 221, N'RK', N'Rukungiri
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (884, 221, N'PL', N'Pallisa
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (885, 221, N'MZ', N'Masindi
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (886, 221, N'WA', N'Wakiso
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (887, 221, N'PR', N'Pader
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (888, 221, N'BG', N'Bugiri
', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (889, 220, N'FN', N'Funafuti', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (890, 220, N'NG', N'Nanumanga', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (891, 220, N'NN', N'Nanumea', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (892, 220, N'NT', N'Niutao', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (893, 220, N'NU', N'Nui', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (894, 220, N'NF', N'Nukufetau', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (895, 220, N'NL', N'Nukulaelae', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (896, 220, N'VI', N'Vaitupu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (897, 219, N'TC', N'Turks and Caicos Islands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (898, 218, N'AL', N'Ahal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (899, 218, N'AB', N'Ashgabat', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (900, 218, N'BA', N'Balkan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (901, 218, N'DA', N'Dashoguz', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (902, 218, N'LE', N'Lebap', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (903, 218, N'MA', N'Mary', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (904, 199, N'AP', N'Ampara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (905, 199, N'AD', N'Anuradhapura', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (906, 199, N'BD', N'Badulla', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (907, 199, N'BC', N'Batticaloa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (908, 199, N'CO', N'Colombo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (909, 199, N'GL', N'Galle', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (910, 199, N'GQ', N'Gampaha', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (911, 199, N'HB', N'Hambantota', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (912, 199, N'JA', N'Jaffna', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (913, 199, N'KT', N'Kalutara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (914, 199, N'KY', N'Kandy', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (915, 199, N'KE', N'Kegalle', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (916, 199, N'KL', N'Kilinochchi', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (917, 199, N'KG', N'Kurunegala', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (918, 199, N'MB', N'Mannar', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (919, 199, N'MT', N'Matale', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (920, 199, N'MH', N'Matara', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (921, 199, N'MJ', N'Moneragala', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (922, 199, N'MP', N'Mullaitivu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (923, 199, N'NW', N'Nuwara Eliya	', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (924, 199, N'PR', N'Polonnaruwa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (925, 199, N'PX', N'Puttalam', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (926, 199, N'RN', N'Ratnapura', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (927, 199, N'TC', N'Trincomalee', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (928, 199, N'VA', N'Vavuniya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (929, 198, N'AN', N'Andalusia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (930, 198, N'AR', N'Aragon', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (931, 198, N'AS', N'Asturias', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (932, 198, N'IB', N'Balearic Islands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (933, 198, N'PV', N'Basque Country', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (934, 198, N'CN', N'Canary Islands', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (935, 198, N'CB', N'Cantabria', 0)
GO
print 'Processed 900 total records'
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (936, 198, N'CL', N'Castile and Leon', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (937, 198, N'CM', N'Castile-La Mancha', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (938, 198, N'CT', N'Catalonia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (939, 198, N'CE', N'Ceuta', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (940, 198, N'EX', N'Extremadura', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (941, 198, N'GA', N'Galicia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (942, 198, N'LO', N'La Rioja', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (943, 198, N'MD', N'Madrid', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (944, 198, N'ML', N'Melilla', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (945, 198, N'MC', N'Murcia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (946, 198, N'NC', N'Navarra', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (947, 198, N'VC', N'Valencia', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (948, 196, N'EC', N'Eastern Cape', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (949, 196, N'FS', N'Free State', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (950, 196, N'GT', N'Gauteng', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (951, 196, N'NL', N'KwaZulu-Natal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (952, 196, N'LP', N'Limpopo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (953, 196, N'MP', N'Mpumalanga', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (954, 196, N'NC', N'Northern Cape', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (955, 196, N'NW', N'North-West', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (956, 196, N'WC', N'Western Cape', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (957, 195, N'AW', N'Awdal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (958, 195, N'BK', N'Bakool', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (959, 195, N'BN', N'Banaadir', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (960, 195, N'BR', N'Bari', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (961, 195, N'BY', N'Bay', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (962, 195, N'GA', N'Galguduud', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (963, 195, N'GE', N'Gedo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (964, 195, N'HI', N'Hiiraan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (965, 195, N'JD', N'Jubbada Dhexe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (966, 195, N'JH', N'Jubbada Hoose', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (967, 195, N'MU', N'Mudug', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (968, 195, N'NU', N'Nugaal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (969, 195, N'SA', N'Sanaag', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (970, 195, N'SD', N'Shabeellaha Dhexe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (971, 195, N'SH', N'Shabeellaha Hoose', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (972, 195, N'SO', N'Sool', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (973, 195, N'TO', N'Togdheer', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (974, 195, N'WO', N'Woqooyi Galbeed', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (975, 195, N'AW', N'Awdal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (976, 195, N'BK', N'Bakool', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (977, 195, N'BN', N'Banaadir', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (978, 195, N'BR', N'Bari', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (979, 195, N'BY', N'Bay', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (980, 195, N'GA', N'Galguduud', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (981, 195, N'GE', N'Gedo', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (982, 195, N'HI', N'Hiiraan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (983, 195, N'JD', N'Jubbada Dhexe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (984, 195, N'JH', N'Jubbada Hoose', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (985, 195, N'MU', N'Mudug', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (986, 195, N'NU', N'Nugaal', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (987, 195, N'SA', N'Sanaag', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (988, 195, N'SD', N'Shabeellaha Dhexe', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (989, 195, N'SH', N'Shabeellaha Hoose', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (990, 195, N'SO', N'Sool', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (991, 195, N'TO', N'Togdheer', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (992, 195, N'WO', N'Woqooyi Galbeed', 0)



/*3/19/2013 INDIA*/

INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2489, 98, N'AP', N'Andhra Pradesh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (993, 98, N'AR', N'Arunachal Pradesh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (994, 98, N'AS', N'Assam', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (995, 98, N'BR', N'Bihar', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (996, 98, N'CG', N'Chhattisgarh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (997, 98, N'GA', N'Goa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (998, 98, N'GJ', N'Gujarat', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (999, 98, N'HR', N'Haryana', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1000, 98, N'HP', N'Himachal Pradesh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1001, 98, N'JK', N'Jammu and Kashmir', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1002, 98, N'JH', N'Jharkhand', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1003, 98, N'KA', N'Karnataka', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1004, 98, N'KL', N'Kerala', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1005, 98, N'MP', N'Madhya Pradesh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1006, 98, N'MH', N'Maharashtra', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1007, 98, N'MN', N'Manipur', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1008, 98, N'ML', N'Meghalaya', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1009, 98, N'MZ', N'Mizoram', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1010, 98, N'NL', N'Nagaland', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1011, 98, N'OR', N'Orissa', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1012, 98, N'PB', N'Punjab', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1013, 98, N'RJ', N'Rajasthan', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1014, 98, N'SK', N'Sikkim', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1015, 98, N'TN', N'Tamil Nadu', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1016, 98, N'TR', N'Tripura', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1017, 98, N'UP', N'Uttar Pradesh', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1018, 98, N'UK', N'Uttarakhand', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1019, 98, N'WB', N'West Bengal', 0)

--------
-- 236 Yemen
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1020,236,N'AD',N'Aden',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1021,236,N'HD',N'Hadramawt',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1022,236,N'',N'Hodeida',0) not available code
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1023,236,N'IB',N'Ibb',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1024,236,N'',N'Sanaa',0) not available code
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1025,236,N'TA',N'Taizz',0)
--212 togo
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1026,212,N'KA',N'Kara',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1027,212,N'CE',N'Centrale',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1028,212,N'PL',N'Plateaux',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1029,212,N'SA',N'Savanes',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1030,212,N'MA',N'Maritime',0)
--214 Tonga 
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1031,214,N'VA',N'vava',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1032,214,N'HA',N'Ha',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1033,214,N'TO',N'tongatapu',0)
--Thailand 210
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1034,210,N'BM',N'Bangkok',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1035,210,N'CM',N'Chiang Mai',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1036,210,N'KK',N'Khon Kaen',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1037,210,N'NP',N'Nakhon Pathom',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1038,210,N'NR',N'Nakhon Ratchasima',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1039,210,N'NS',N'Nakhon Sawan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1040,210,N'NO',N'Nonthaburi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1041,210,N'SG',N'Songkhla',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1042,210,N'UR',N'Ubon Ratchathani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1043,210,N'UN',N'Udon Thani',0)
--Tanzania 209
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1044,209,N'DS',N'Dar es Salaam',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1045,209,N'MB',N'Mbeya',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1046,209,N'MZ',N'Mwanza',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1047,209,N'SY',N'Shinyanga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1048,209,N'MO',N'Morogoro',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1049,209,N'',N'Zanzibar Urban',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1050,209,N'AS',N'Arusha',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1051,209,N'IG',N'Iringa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1052,209,N'TN',N'Tanga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1053,209,N'KR',N'Kagera',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1054,209,N'KM',N'Kigoma',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1055,209,N'SD',N'Singida',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1056,209,N'MA',N'Mara',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1057,209,N'RV',N'Ruvuma',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1058,209,N'MT',N'Mtwara',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1059,209,N'DO',N'Dodoma',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1060,209,N'TB',N'Tabora',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1061,209,N'KL',N'Kilimanjaro',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1062,209,N'RU',N'Rukwa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1063,209,N'LI',N'Lindi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1064,209,N'MY',N'Manyara',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1065,209,N'PW',N'Pwani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1066,209,N'PN',N'Pemba North',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1067,209,N'PS',N'Pemba South',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1068,209,N'ZN',N'Zanzibar North',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1069,209,N'',N'Zanzibar Central',0)

--Tajikistan 208
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1070,208,N'KL',N'Khatlon',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1071,208,N'',N'Sughd',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1072,208,N'',N'Kuhistoni Badakhshon',0)

--Taiwan 207
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1073,207,N'CHA',N'Changhwa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1074,207,N'CYQ',N'Chiayi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1075,207,N'HSQ',N'Hsinchu�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1076,207,N'HUA',N'Hualien�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1077,207,N'ILA',N'Ilan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1078,207,N'KHQ',N'Kaohsiung�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1079,207,N'CHI',N'Keelung�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1080,207,N'MIA',N'Miaoli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1081,207,N'NAN',N'Nantou�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1082,207,N'PIF',N'Pingtung�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1084,207,N'TXQ',N'Taichung�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1085,207,N'TNQ',N'Tainan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1086,207,N'TPQ',N'Taipei�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1087,207,N'TTT',N'Taitung�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1088,207,N'TAO',N'Taoyuan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1089,207,N'YUN',N'Y�nlin�',0)
--Sudan 200
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1090,200,N'',N'Al Khartum',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1091,200,N'',N'Al Wusta',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1092,200,N'',N'Central Equatoria State',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1093,200,N'',N'Upper Nile',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1094,200,N'',N'Kurdufan',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1095,200,N'',N'Al Istiwa iyah',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1096,200,N'',N'Bahr al Ghazal',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1097,200,N'',N'Ash Sharqiyah',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1098,200,N'',N'Ash Shamaliya',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1099,200,N'',N'Darfur',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1100,200,N'',N'Al Wahadah State',0)

---201 Suriname
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1101,201,N'PM',N'Paramaribo�',0)
-- 203 Swaziland
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1102,203,N'MA',N'Manzini',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1103,203,N'HH',N'Hhohho',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1104,203,N'SH',N'Shiselweni',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1105,203,N'LU',N'Lubombo',0)
--204 Sweden
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1106,204,N'ST',N'Stockholms Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1107,204,N'VG',N'Vastra Gotalan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1108,204,N'SN',N'Skane Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1109,204,N'OG',N'Ostergotlands Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1110,204,N'UP',N'Uppsala Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1111,204,N'VM',N'Vastmanlands Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1112,204,N'JO',N'Jonkopings Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1113,204,N'OR',N'Orebro Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1114,204,N'SD',N'Sodermanlands Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1115,204,N'GV',N'Gavleborgs Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1116,204,N'HA',N'Hallands Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1117,204,N'VB',N'Vasterbottens Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1118,204,N'NB',N'Norrbottens Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1119,204,N'VN',N'Vasternorrlands Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1120,204,N'KO',N'Dalarnas Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1121,204,N'VR',N'Varmlands Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1122,204,N'KA',N'Kalmar Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1123,204,N'KR',N'Kronobergs Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1124,204,N'BL',N'Blekinge Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1125,204,N'JA',N'Jamtlands Lan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1126,204,N'GT',N'Gotlands Lan',0)
---Switzerland 205
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1127,205,N'ZH',N'Zurich',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1128,205,N'BE',N'Bern',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1129,205,N'',N'Vaud',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1130,205,N'GE',N'Geneve',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1131,205,N'AG',N'Aargau',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1132,205,N'BS',N'Basel-Stadt',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1133,205,N'LU',N'Luzern',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1134,205,N'',N'Valais',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1135,205,N'',N'Basel-Landschaft',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1136,205,N'',N'Ticino',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1137,205,N'SG',N'Sankt Gallen',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1138,205,N'',N'Thurgau',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1139,205,N'NE',N'Neuchatel',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1140,205,N'SO',N'Solothurn',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1141,205,N'ZG',N'Zug',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1142,205,N'SZ',N'Schwyz',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1143,205,N'SA',N'Fribourg',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1144,205,N'',N'Graubunden',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1145,205,N'',N'Ausser-Rhoden',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1146,205,N'SH',N'Schaffhause',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1147,205,N'AI',N'Inner-Rhoden',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1148,205,N'OW',N'Obwalden',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1149,205,N'NW',N'Nidwalden',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1150,205,N'GL',N'Glarus',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1151,205,N'UR',N'Uri',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1152,205,N'',N'Jura',0)

--Slovenia 193
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1153,193,N'LJ',N'Osrednjeslovenska�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1154,193,N'PD',N'Podravska�',0)
--Slovakia 192
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1155,192,N'BL',N'Bratislava',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1156,192,N'KI',N'Kosice',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1157,192,N'BC',N'Banska Bystrica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1158,192,N'ZI',N'Zilina',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1159,192,N'PV',N'Presov',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1160,192,N'NI',N'Nitra',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1161,192,N'TC',N'Trencin',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1162,192,N'TA',N'Trnava',0)
--194 Solomon Islands
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1163,194,N'CN',N'Central',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1164,194,N'CH',N'Choiseul',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1165,194,N'IS',N'Isabel',0)
--190 Sierra Leone
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1166,190,N'WE',N'Western�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1167,190,N'NO',N'Northern',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1168,190,N'SO',N'Southern',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1169,190,N'EA',N'Western',0)
--Seychelles 189
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1170,189,N'GM',N'Mah�',0)
-- 187 Senegal 
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1171,187,N'DK',N'Dakar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1172,187,N'TH',N'Thies',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1173,187,N'KC',N'Kaolack',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1174,187,N'ZG',N'Ziguinchor',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1175,187,N'DB',N'Diourbel',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1176,187,N'ST',N'Saint-Louis',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1177,187,N'KO',N'Kolda',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1178,187,N'FK',N'Fatick',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1179,187,N'LG',N'Louga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1180,187,N'TB',N'Tambacounda',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1181,187,N'MT',N'Matam',0)
-- 184 San Marino
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1182,184,N'FA',N'Faetano',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1183,184,N'BM',N'Borgo Maggiore',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1184,184,N'FI',N'Fiorentino',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1185,184,N'DO',N'Domagnano',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1186,184,N'SE',N'Serravalle',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1187,184,N'SM',N'San Marino',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1188,184,N'AC',N'Acquaviva',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1189,184,N'CH',N'Chiesanuova',0)
-- 183 Samoa
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1190,183,N'AL',N'Aiga-i-le-Tai',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1191,183,N'AT',N'Atua',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1192,183,N'FA',N'Fa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1193,183,N'GE',N'Gaga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1194,183,N'GI',N'Gagaifomauga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1195,183,N'PA',N'Palauli',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1196,183,N'SA',N'Satupa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1197,183,N'TU',N'Tuamasaga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1198,183,N'VF',N'Va',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1199,183,N'VS',N'Vaisigano',0)
--180 Saint Lucia
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1200,180,N'CS',N'Castries',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1201,180,N'VF',N'Vieux-Fort',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1202,180,N'MI',N'Micoud',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1203,180,N'GI',N'Gros-Islet',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1204,180,N'DE',N'Dennery',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1205,180,N'CO',N'Soufriere',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1206,180,N'LB',N'Laborie',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1207,180,N'AR',N'Anse-la-Raye',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1208,180,N'CH',N'Choiseul',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1209,180,N'',N'Dauphin',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1210,180,N'',N'Praslin',0)

--Saint Kitts and Nevis 179
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1211,179,N'PL',N'Saint Paul Charlestown',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1212,179,N'GB',N'Saint George Basseterre',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1213,179,N'JF',N'Saint John Figtree',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1214,179,N'JC',N'Saint John Capisterre',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1215,179,N'AS',N'Saint Anne Sandy Point',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1216,179,N'TM',N'Saint Thomas Middle Island',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1217,179,N'MC',N'Saint Mary Cayon',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1218,179,N'CC',N'Christ Church Nichola Town',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1219,179,N'GB',N'Saint Peter Basseterre',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1220,179,N'JW',N'Saint James Windward',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1221,179,N'GG',N'Saint George Gingerland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1222,179,N'TL',N'Saint Thomas Lowland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1223,179,N'JC',N'Saint Paul Capisterre',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1224,179,N'TP',N'Trinity Palmetto Point',0)
--178 Saint Helena
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1225,178,N'AC',N'Ascension',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1226,178,N'SH',N'Saint Helena',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1227,178,N'TA',N'Tristan da Cunha',0)
--Russian Federation 176
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1228,176,N'AD',N'Adygea�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1229,176,N'AL',N'Altai�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1230,176,N'AM',N'Amur�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1231,176,N'AR',N'Arkangeli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1232,176,N'AS',N'Astrahan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1233,176,N'BK',N'Ba�kortostan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1234,176,N'BL',N'Belgorod�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1235,176,N'BR',N'Brjansk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1236,176,N'BU',N'Burjatia�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1237,176,N'DA',N'Dagestan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1238,176,N'KH',N'Habarovsk�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1239,176,N'',N'Hakassia�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1240,176,N'KM',N'Hanti-Mansia�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1241,176,N'IK',N'Irkutsk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1242,176,N'IV',N'Ivanovo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1243,176,N'YS',N'Jaroslavl�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1244,176,N'KB',N'Kabardi-Balkaria�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1245,176,N'KN',N'Kaliningrad�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1246,176,N'KL',N'Kalmykia�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1247,176,N'KG',N'Kaluga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1248,176,N'KQ',N'Kamt�atka�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1249,176,N'',N'Karat�ai-T�erkessia�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1250,176,N'',N'Karjala�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1251,176,N'KE',N'Kemerovo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1252,176,N'KV',N'Kirov�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1253,176,N'KO',N'Komi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1254,176,N'KT',N'Kostroma�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1255,176,N'KD',N'Krasnodar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1256,176,N'KX',N'Krasnojarsk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1257,176,N'KU',N'Kurgan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1258,176,N'KS',N'Kursk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1259,176,N'LP',N'Lipetsk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1260,176,N'MG',N'Magadan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1261,176,N'ME',N'Marinmaa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1262,176,N'MR',N'Mordva�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1263,176,N'MC',N'Moscow (City)�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1264,176,N'MS',N'Moskova�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1265,176,N'MM',N'Murmansk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1266,176,N'NZ',N'Nizni Novgorod�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1267,176,N'NO',N'North Ossetia-Alania�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1268,176,N'NG',N'Novgorod�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1269,176,N'NS',N'Novosibirsk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1270,176,N'OM',N'Omsk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1271,176,N'OB',N'Orenburg�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1272,176,N'',N'Orjol',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1273,176,N'PZ',N'Penza',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1274,176,N'PE',N'Perm�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1275,176,N'',N'Pietari�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1276,176,N'',N'Pihkova�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1277,176,N'PR',N'Primorje�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1278,176,N'RZ',N'Rjazan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1279,176,N'RO',N'Rostov-na-Donu�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1280,176,N'SK',N'Saha (Jakutia)�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1281,176,N'SL',N'Sahalin�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1282,176,N'SA',N'Samara�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1283,176,N'SR',N'Saratov�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1284,176,N'SM',N'Smolensk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1285,176,N'ST',N'Stavropol�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1286,176,N'ST',N'Sverdlovsk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1287,176,N'TB',N'Tambov�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1288,176,N'TT',N'Tatarstan�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1289,176,N'	',N'Tjumen�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1290,176,N'TO',N'Tomsk�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1291,176,N'',N'T�eljabinsk�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1292,176,N'',N'T�et�enia�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1293,176,N'',N'T�ita�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1294,176,N'',N'T�uvassia�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1295,176,N'TL',N'Tula�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1296,176,N'TV',N'Tver�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1953,176,N'TU',N'Tyva�',0) 
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1297,176,N'UD',N'Udmurtia�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1298,176,N'UL',N'Uljanovsk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1299,176,N'VL',N'Vladimir�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1300,176,N'VG',N'Volgograd�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1301,176,N'VO',N'Vologda�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1302,176,N'VR',N'Voronez�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1303,176,N'YN',N'Yamalin Nenetsia�',0)
-- Romania 175
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1304,175,N'AR',N'Arad�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1305,175,N'AG',N'Arges�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1306,175,N'BC',N'Bacau�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1307,175,N'BH',N'Bihor�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1308,175,N'BT',N'Botosani�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1309,175,N'BR',N'Braila�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1310,175,N'BV',N'Brasov�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1311,175,N'BI',N'Bukarest',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1312,175,N'BZ',N'Buzau�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1313,175,N'CS',N'Caras-Severin�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1314,175,N'CJ',N'Cluj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1315,175,N'CT',N'Constanta�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1316,175,N'DB',N'D�mbovita�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1317,175,N'DJ',N'Dolj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1318,175,N'GL',N'Galati�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1319,175,N'GJ',N'Gorj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1320,175,N'IS',N'Iasi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1321,175,N'MM',N'Maramures�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1322,175,N'MH',N'Mehedinti�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1323,175,N'MS',N'Mures�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1324,175,N'NT',N'Neamt�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1325,175,N'PH',N'Prahova�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1326,175,N'SM',N'Satu Mare�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1327,175,N'SB',N'Sibiu�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1328,175,N'SV',N'Suceava�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1329,175,N'TM',N'Timis',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1330,175,N'TL',N'Tulcea�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1331,175,N'VL',N'V�lcea�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1332,175,N'VN',N'Vrancea',0)
--172 Puerto Rico
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1333,172,N'AC',N'Arecibo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1334,172,N'BY',N'Bayam�n�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1335,172,N'CG',N'Caguas�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1336,172,N'CN',N'Carolina�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1337,172,N'GB',N'Guaynabo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1338,172,N'MG',N'Mayag�ez�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1339,172,N'PO',N'Ponce�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1340,172,N'SJ',N'San Juan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1341,172,N'TB',N'Toa Baja�',0)
-- 170 Poland
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1342,170,N'DS',N'Dolnoslaskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1343,170,N'KP',N'Kujawsko-Pomorskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1344,170,N'LD',N'Lodzkie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1345,170,N'LB',N'Lubelskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1346,170,N'LB',N'Lubuskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1347,170,N'MA',N'Malopolskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1348,170,N'MZ',N'Mazowieckie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1349,170,N'OP',N'Opolskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1350,170,N'PK',N'Podkarpackie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1351,170,N'PD',N'Podlaskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1352,170,N'PM',N'Pomorskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1353,170,N'SL',N'Slaskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1354,170,N'SK',N'Swietokrzyskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1355,170,N'WN',N'Warminsko-Mazurskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1356,170,N'WP',N'Wielkopolskie ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1357,170,N'ZP',N'Zachodnio-Pomorskie ',0)
--168	Philippines	
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1358,168,N'',N'ARMM ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1359,168,N'BO',N'Bicol ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1360,168,N'CG',N'Cagayan Valley ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1361,168,N'',N'CAR ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1362,168,N'',N'Caraga ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1363,168,N'',N'Central Luzon ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1364,168,N'',N'Central Mindanao ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1365,168,N'',N'Central Visayas ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1366,168,N'',N'Eastern Visayas ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1367,168,N'IN',N'Ilocos ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1368,168,N'NC',N'National Capital Reg ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1369,168,N'',N'Northern Mindanao ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1370,168,N'',N'Southern Mindanao ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1371,168,N'',N'Southern Tagalog ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1372,168,N'',N'Western Mindanao ',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1373,168,N'',N'Western Visayas ',0)
---167	Peru
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1374,167,N'AN',N'Ancash ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1375,167,N'AR',N'Arequipa ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1376,167,N'AY',N'Ayacucho ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1377,167,N'CJ',N'Cajamarca ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1378,167,N'CL',N'Callao ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1379,167,N'CS',N'Cusco ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1380,167,N'HC',N'Huanuco ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1381,167,N'IC',N'Ica ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1382,167,N'JU',N'Jun�n ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1383,167,N'LL',N'La Libertad ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1384,167,N'LB',N'Lambayeque ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1385,167,N'LR',N'Lima ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1386,167,N'LO',N'Loreto ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1387,167,N'PI',N'Piura ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1388,167,N'PU',N'Puno ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1389,167,N'TA',N'Tacna ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1390,167,N'UC',N'Ucayali ',0)
--166	Paraguay
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1391,166,N'',N'Alto Parana',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1392,166,N'',N'Alto Paraguay',0)
--164 Panama
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1393,164,N'PA',N'Panam�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1394,164,N'',N'San Miguelito�',0)
--162 Palau
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1395,162,N'AM',N'Aimeliik',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1396,162,N'AR',N'Airai',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1397,162,N'AN',N'Angaur',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1398,162,N'HA',N'Hatohobei',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1399,162,N'KA',N'Kayangel',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1400,162,N'KO',N'Koror',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1401,162,N'ME',N'Melekeok',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1402,162,N'ND',N'Ngaraard',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1403,162,N'NC',N'Ngarchelong',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1404,162,N'NM',N'Ngardmau',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1405,162,N'NP',N'Ngatpang',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1406,162,N'NS',N'Ngchesar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1407,162,N'NL',N'Ngeremlengui',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1408,162,N'NW',N'Ngiwal',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1409,162,N'PE',N'Peleliu',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1410,162,N'SO',N'Sonsorol',0)
--160 Oman
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1411,160,N'BN',N'al-Batina�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1412,160,N'MA',N'Masqat�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1413,160,N'JA',N'Zufar',0)
--159	Norway
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1414,159,N'OS',N'Oslo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1415,159,N'HO',N'Hordaland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1416,159,N'RO',N'Rogaland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1417,159,N'ST',N'Sor-Trondelag',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1418,159,N'BU',N'Buskerud',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1419,159,N'NO',N'Nordland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1420,159,N'VA',N'Vest-Agder',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1421,159,N'AK',N'Akershus',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1422,159,N'TR',N'Troms',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1423,159,N'MR',N'More og Romsdal',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1424,159,N'VF',N'Vestfold',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1425,159,N'OP',N'Oppland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1426,159,N'NT',N'Nord-Trondelag',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1427,159,N'HE',N'Hedmark',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1428,159,N'OF',N'Ostfold',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1429,159,N'FI',N'Finnmark',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1430,159,N'SF',N'Sogn og Fjordane',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1431,159,N'TE',N'Telemark',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1432,159,N'AA',N'Aust-Agder',0)
--155	Nigeria	
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1433,155,N'AB',N'Abia',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1434,155,N'AD',N'Adamawa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1435,155,N'AK',N'Akwa Ibom',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1436,155,N'AN',N'Anambra',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1437,155,N'BA',N'Bauchi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1438,155,N'BY',N'Bayelsa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1439,155,N'BE',N'Benue',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1440,155,N'BO',N'Borno',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1441,155,N'CR',N'Cross River',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1442,155,N'DE',N'Delta',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1443,155,N'EB',N'Ebonyi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1444,155,N'ED',N'Edo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1445,155,N'EK',N'Ekiti',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1446,155,N'EN',N'Enugu',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1447,155,N'FC',N'Federal Capital Territory',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1448,155,N'GO',N'Gombe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1449,155,N'IM',N'Imo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1450,155,N'JI',N'Jigawa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1451,155,N'KD',N'Kaduna',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1452,155,N'KN',N'Kano',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1453,155,N'KT',N'Katsina',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1454,155,N'KE',N'Kebbi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1455,155,N'KO',N'Kogi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1456,155,N'KW',N'Kwara',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1457,155,N'LA',N'Lagos',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1458,155,N'NA',N'Nassarawa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1459,155,N'NG',N'Niger',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1460,155,N'OG',N'Ogun',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1461,155,N'ON',N'Ondo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1462,155,N'OS',N'Osun',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1463,155,N'OY',N'Oyo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1464,155,N'PL',N'Plateau',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1465,155,N'RI',N'Rivers',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1466,155,N'SO',N'Sokoto',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1467,155,N'TA',N'Taraba',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1468,155,N'YO',N'Yobe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1469,155,N'ZA',N'Zamfara',0)
-- 154 Niger
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1470,154,N'NI',N'Niamey',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1471,154,N'MA',N'Maradi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1472,154,N'ZI',N'Zinder',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1473,154,N'AG',N'Agadez',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1474,154,N'TH',N'Tahoua',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1475,154,N'DS',N'Dosso',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1476,154,N'DF',N'Diffa',0)
-- 149	Netherlands
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1477,149,N'DR',N'Drenthe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1478,149,N'FL',N'Flevoland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1479,149,N'GE',N'Gelderland�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1480,149,N'GR',N'Groningen�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1481,149,N'LI',N'Limburg�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1482,149,N'NB',N'Noord-Brabant�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1483,149,N'NH',N'Noord-Holland�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1484,149,N'OV',N'Overijssel�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1485,149,N'UT',N'Utrecht�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1486,149,N'ZH',N'Zuid-Holland�',0)
-- 146	Namibia
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1487,146,N'',N'YarenBethanien',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1488,146,N'',N'Boesmanland',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1489,146,N'',N'Karibib',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1490,146,N'',N'Kavango',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1491,146,N'',N'Keetmanshoop',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1492,146,N'',N'Luderitz',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1493,146,N'',N'Maltahohe',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1494,146,N'',N'Mariental',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1495,146,N'',N'Okahandja',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1496,146,N'',N'Omaruru',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1497,146,N'',N'Otjiwarongo',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1498,146,N'',N'Outjo',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1499,146,N'',N'Rehoboth',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1500,146,N'',N'Swakopmund',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1501,146,N'',N'Tsumeb',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1502,146,N'',N'Damaraland',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1503,146,N'',N'Gobabis',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1504,146,N'',N'Grootfontein',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1505,146,N'',N'Kaokoland',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1506,146,N'',N'Karasburg',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1507,146,N'',N'Windhoek',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1508,146,N'ER',N'Erongo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1509,146,N'OD',N'Otjozondjupa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1510,146,N'KA',N'Karas',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1511,146,N'OK',N'Okavango',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1512,146,N'ON',N'Oshana',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1513,146,N'HA',N'Hardap',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1514,146,N'CA',N'Caprivi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1515,146,N'KU',N'Kunene',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1516,146,N'OH',N'Omaheke',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1517,146,N'OT',N'Oshikoto',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1518,146,N'OW',N'Ohangwena',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1519,146,N'OS',N'Omusati',0)
--147	Nauru
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1520,147,N'AI',N'Aiwo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1521,147,N'AB',N'Anabar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1522,147,N'AT',N'Anetan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1523,147,N'AR',N'Anibare',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1524,147,N'BA',N'Baiti',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1525,147,N'BO',N'Boe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1526,147,N'BU',N'Buada',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1527,147,N'DE',N'Denigomodu',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1528,147,N'EW',N'Ewa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1529,147,N'IJ',N'Ijuw',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1530,147,N'ME',N'Meneng',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1531,147,N'NI',N'Nibok',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1532,147,N'UA',N'Uaboe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1533,147,N'YA',N'Yaren',0)
--131 Malta
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1534,131,N'IH',N'Inner Harbour�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1535,131,N'OH',N'Outer Harbour�',0)
--130	Mali
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1536,130,N'BA',N'Bamako',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1537,130,N'SK',N'Sikasso',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1538,130,N'KK',N'Koulikoro',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1539,130,N'KY',N'Kayes',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1540,130,N'SG',N'Segou',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1541,130,N'MO',N'Mopti',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1542,130,N'TB',N'Tombouctou',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1543,130,N'GA',N'Gao',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1544,130,N'KD',N'Kidal',0)
--129	Maldives
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1545,129,N'',N'Vaavu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1546,129,N'',N'Dhaalu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1547,129,N'',N'Faafu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1548,129,N'',N'Gaafu Alifu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1549,129,N'',N'Gaafu Dhaalu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1550,129,N'',N'Gnaviyani',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1551,129,N'',N'Haa Alifu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1552,129,N'',N'Haa Dhaalu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1553,129,N'',N'Kaafu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1554,129,N'',N'Laamu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1555,129,N'',N'Lhaviyani',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1556,129,N'',N'Thaa',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1557,129,N'',N'Baa',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1558,129,N'',N'Meemu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1559,129,N'',N'Noonu',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1560,129,N'',N'Raa',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1561,129,N'',N'Shaviyani',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1562,129,N'',N'Maale',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1563,129,N'',N'Seenu',0)

--127 	Malawi
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1564,127,N'LK',N'Likoma',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1565,127,N'LI',N'Lilongwe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1566,127,N'BL',N'Blantyre',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1567,127,N'ZO',N'Zomba',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1568,127,N'MG',N'Mangochi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1569,127,N'KS',N'Kasungu',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1570,127,N'BA',N'Balaka',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1571,127,N'SA',N'Salima',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1572,127,N'KR',N'Karonga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1573,127,N'MJ',N'Mulanje',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1574,127,N'RU',N'Rumphi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1575,127,N'NK',N'Nkhotakota',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1576,127,N'NS',N'Nsanje',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1577,127,N'MW',N'Mzimba',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1578,127,N'NI',N'Ntchisi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1579,127,N'MC',N'Mchinji',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1580,127,N'DE',N'Dedza',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1581,127,N'NA',N'Nkhata Bay',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1582,127,N'MN',N'Mwanza',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1583,127,N'NS',N'Ntcheu',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1584,127,N'CK',N'Chikwawa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1585,127,N'CT',N'Chitipa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1586,127,N'TH',N'Thyolo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1587,127,N'DO',N'Dowa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1588,127,N'PH',N'Phalombe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1589,127,N'CR',N'Chiradzulu',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1590,127,N'MA',N'Machinga',0)
--125	Macedonia, The Former Yugoslav Republic of
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1591,125,N'CZ',N'Centar Zupa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1592,125,N'GB',N'Gazi Baba',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1593,125,N'CI',N'Cair',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1594,125,N'CE',N'Centar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1595,125,N'KX',N'Karpos',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1596,125,N'UM',N'Kumanovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1597,125,N'TL',N'Bitola',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1598,125,N'PP',N'Prilep',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1599,125,N'ET',N'Tetovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1600,125,N'VE',N'Veles',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1601,125,N'OD',N'Ohrid',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1602,125,N'GT',N'Gostivar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1603,125,N'ST',N'Stip',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1604,125,N'SM',N'Strumica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1605,125,N'KA',N'Kavadarci',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1606,125,N'SG',N'Struga',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1607,125,N'KO',N'Kocani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1608,125,N'KI',N'Kicevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1609,125,N'LI',N'Lipkovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1610,125,N'ZE',N'Zelino',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1611,125,N'AJ',N'Saraj',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1612,125,N'RA',N'Radovis',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1613,125,N'TR',N'Tearce',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1614,125,N'ZR',N'Zrnovci',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1615,125,N'KP',N'Kriva Palanka',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1616,125,N'GG',N'Gevgelija',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1617,125,N'NE',N'Negotino',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1618,125,N'SN',N'Sveti Nikole',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1619,125,N'SU',N'Studenicani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1620,125,N'DB',N'Debar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1621,125,N'NG',N'Negotino-Polosko',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1622,125,N'DL',N'Delcevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1623,125,N'RE',N'Resen',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1624,125,N'IL',N'Ilinden',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1625,125,N'',N'BN',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1626,125,N'KJ',N'Kamenjane',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1627,125,N'BJ',N'Bogovinje',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1628,125,N'BR',N'Berovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1629,125,N'AR',N'Aracinovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1630,125,N'PT',N'Probistip',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1631,125,N'CG',N'Cegrane',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1632,125,N'BS',N'Bosilovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1633,125,N'VL',N'Vasilevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1634,125,N'ZA',N'Zajas',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1635,125,N'VL',N'Valandovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1636,125,N'NS',N'Novo Selo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1637,125,N'DN',N'Dolneni',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1638,125,N'OS',N'Oslomej',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1639,125,N'KY',N'Kratovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1640,125,N'DJ',N'Dolna Banjica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1641,125,N'SC',N'Sopiste',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1642,125,N'RT',N'Rostusa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1643,125,N'LA',N'Labunista',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1644,125,N'VP',N'Vrapciste',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1645,125,N'VS',N'Velesta',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1646,125,N'CS',N'Cucer-Sandevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1647,125,N'BG',N'Bogdanci',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1648,125,N'DG',N'Delogozdi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1649,125,N'PE',N'Petrovec',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1650,125,N'SI',N'Sipkovica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1651,125,N'DZ',N'Dzepciste',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1652,125,N'MK',N'Makedonska Kamenica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1653,125,N'JE',N'Jegunovce',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1654,125,N'DX',N'Demir Hisar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1655,125,N'MU',N'Murtino',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1656,125,N'KG',N'Krivogastani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1657,125,N'MD',N'Makedonski Brod',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1658,125,N'OB',N'Oblesevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1659,125,N'BI',N'Bistrica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1660,125,N'PN',N'Plasnica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1661,125,N'DK',N'Demir Kapija',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1662,125,N'MG',N'Mogila',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1663,125,N'KK',N'Kuklis',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1664,125,N'OZ',N'Orizari',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1665,125,N'SE',N'Staro Nagoricane ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1666,125,N'RM',N'Rosoman',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1667,125,N'RN',N'Rankovce',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1668,125,N'ZK',N'Zelenikovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1669,125,N'KB',N'Karbinci',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1670,125,N'PO',N'Podares',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1671,125,N'GR',N'Gradsko',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1672,125,N'VR',N'Vratnica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1673,125,N'SB',N'Srbinovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1674,125,N'KN',N'Konce',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1675,125,N'SD',N'Star Dojran',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1676,125,N'ZL',N'Zletovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1677,125,N'DR',N'Drugovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1678,125,N'CA',N'Caska',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1679,125,N'LO',N'Lozovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1680,125,N'BE',N'Belcista',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1681,125,N'TO',N'Topolcani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1682,125,N'MI',N'Miravci',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1683,125,N'ME',N'Meseista',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1684,125,N'VV',N'Vevcani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1685,125,N'KQ',N'Kukurecani',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1686,125,N'CN',N'Cesinovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1687,125,N'NO',N'Novaci',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1688,125,N'ZI',N'Zitose',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1689,125,N'SC',N'Sopotnica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1690,125,N'DO',N'Dobrusevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1691,125,N'BL',N'Blatec',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1692,125,N'KL',N'Klecevce',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1693,125,N'SA',N'Samokov',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1694,125,N'LU',N'Lukovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1695,125,N'CP',N'Capari',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1696,125,N'KE',N'Kosel',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1697,125,N'VC',N'Vranestica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1698,125,N'BM',N'Bogomila',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1699,125,N'OR',N'Orasac ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1700,125,N'MA',N'Mavrovi Anovi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1701,125,N'BA',N'Bac',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1702,125,N'VT',N'Vitoliste',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1703,125,N'KF',N'Konopiste',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1704,125,N'SV',N'Staravina',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1705,125,N'VK',N'Vrutok',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1706,125,N'KS',N'Krusevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1707,125,N'SO',N'Suto Orizari',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1708,125,N'KW',N'Kondovo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1709,125,N'VD',N'Kisela Voda',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1710,125,N'VI',N'Vinica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1711,125,N'PH',N'Pehcevo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1712,125,N'IZ',N'Izvor',0)
-- 123	Luxembourg
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1713,123,N'LU',N'Luxembourg',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1714,123,N'DI',N'Diekirch',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1715,123,N'GR',N'Grevenmacher',0)
--122 Lithuania
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1716,122,N'KS',N'Kaunas�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1717,122,N'KP',N'Klaipeda�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1718,122,N'PA',N'Panevezys�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1719,122,N'SH',N'�iauliai�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1720,122,N'VI',N'Vilna�',0)
-- 121 Liechtenstein
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1721,121,N'SN',N'Schaan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1722,121,N'VA',N'Vaduz',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1723,121,N'TN',N'Triesen',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1724,121,N'BA',N'Balzers',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1725,121,N'ES',N'Eschen',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1726,121,N'MA',N'Mauren',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1727,121,N'TB',N'Triesenberg',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1728,121,N'RU',N'Ruggell',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1729,121,N'GA',N'Gamprin',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1730,121,N'SB',N'Schellenberg',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1731,121,N'PL',N'Planken',0)
-- 120	Libyan Arab Jamahiriya
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1732,120,N'TB',N'Tarabulus',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1733,120,N'BG',N'Banghazi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1734,120,N'MS',N'Misratah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1735,120,N'NK',N'An Nuqat al Khams',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1736,120,N'TM',N'Tarhunah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1737,120,N'AJ',N'Ajdabiya',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1738,120,N'TU',N'Tubruq',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1739,120,N'SR',N'Surt',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1740,120,N'SB',N'Sabha',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1741,120,N'ZL',N'Zlitan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1742,120,N'DA',N'Darnah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1743,120,N'MU',N'Murzuq',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1744,120,N'YA',N'Yafran',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1745,120,N'GD',N'Ghadamis',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1746,120,N'JU',N'Al Jufrah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1747,120,N'AW',N'Awbari',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1748,120,N'ZA',N'Az Zawiyah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1749,120,N'FA',N'Al Fatih',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1750,120,N'AZ',N'Al Aziziyah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1751,120,N'GR',N'Gharyan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1752,120,N'SH',N'Ash Shati',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1753,120,N'KU',N'Al Kufrah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1754,120,N'SF',N'Sawfajjin',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1755,120,N'KH',N'Al Khums',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1756,120,N'JA',N'Al Jabal al Akhdar',0)
--119	Liberia
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1757,119,N'',N'Monrovia',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1758,119,N'MO',N'Montserrado',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1759,119,N'BG',N'Bong',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1760,119,N'NI',N'Nimba',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1761,119,N'MY',N'Maryland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1762,119,N'GB',N'Grand Bassa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1763,119,N'GD',N'Grand Gedeh',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1764,119,N'CM',N'Grand Cape Mount',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1765,119,N'RI',N'River Cess',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1766,119,N'RG',N'River Gee',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1767,119,N'LF',N'Lofa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1768,119,N'SI',N'Sino',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1769,119,N'MG',N'Margibi ',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1770,119,N'GP',N'Gbarpolu',0)
-- 118 Lesotho
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1771,118,N'MS',N'Maseru',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1772,118,N'LE',N'Leribe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1773,118,N'MF',N'Mafeteng',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1774,118,N'BE',N'Berea',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1775,118,N'QT',N'Quthing',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1776,118,N'MK',N'Mokhotlong',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1777,118,N'TT',N'Thaba-Tseka',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1778,118,N'QN',N'Qachas Nek',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1779,118,N'MH',N'Mohales Hoek',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1780,118,N'BB',N'Butha-Buthe',0)
--117 Lebanon
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1781,117,N'NA',N'Nabatiye',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1782,117,N'',N'Beyrouth',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1783,117,N'JA',N'Al Janub',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1784,117,N'',N'Mont-Liban',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1785,117,N'BQ',N'Beqaa',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1786,117,N'',N'Liban-Nord',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1787,117,N'',N'Liban-Sud',0)
-- 116 Latvia
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1788,116,N'DP',N'Daugavpils�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1789,116,N'LJ',N'Liepaja�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1790,116,N'RG',N'Riika�',0)
-- 114	Kyrgyzstan
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1791,114,N'KG',N'Ysyk-Kol',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1792,114,N'CU',N'Chuy',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1793,114,N'OS',N'Osh',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1794,114,N'TL',N'Talas',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1795,114,N'GB',N'Bishkek',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1796,114,N'DA',N'Jalal-Abad',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1797,114,N'NA',N'Naryn',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1798,114,N'BA',N'Batken',0)
-- 110	Kiribati
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1799,110,N'GI',N'Gilbert Islands',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1800,110,N'LI',N'Line Islands',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1801,110,N'PI',N'Phoenix Islands',0)
--  109	Kenya
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1802,109,N'NA',N'Nairobi Area',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1803,109,N'CO',N'Coast',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1804,109,N'RV',N'Rift Valley',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1805,109,N'NY',N'Nyanza',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1806,109,N'CE',N'Central',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1807,109,N'WE',N'Western',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1808,109,N'EA',N'Eastern',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1809,109,N'NE',N'North-Eastern',0)
--- 108	Kazakhstan
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1810,108,N'AC',N'Almaty City',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1811,108,N'EK',N'East Kazakhstan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1812,108,N'SK',N'South Kazakhstan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1813,108,N'PA',N'Pavlodar',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1814,108,N'ZM',N'Zhambyl',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1815,108,N'QG',N'Qaraghandy',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1816,108,N'AS',N'Astana',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1817,108,N'QS',N'Qostanay',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1818,108,N'AT',N'Aqtobe',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1819,108,N'AA',N'Almaty',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1820,108,N'MG',N'Mangghystau',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1821,108,N'NK',N'North Kazakhstan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1822,108,N'AR',N'Atyrau',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1823,108,N'AM',N'Aqmola',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1824,108,N'QO',N'Qyzylorda',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1825,108,N'WK',N'West Kazakhstan',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1826,108,N'BY',N'Bayqonyr',0)

-- 107	Jordan
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1827,107,N'IR',N'Irbid',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1828,107,N'AG',N'Amman Governorate',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1829,107,N'MN',N'Ma',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1830,107,N'BA',N'Al Balqa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1831,107,N'AT',N'At Tafilah',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1832,107,N'MA',N'Al Mafraq',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1833,107,N'KA',N'Al Karak',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1834,107,N'AM',N'Amman',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1835,107,N'AZ',N'Az Zarqa',0)
-- 106 	Japan
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1836,106,N'TK',N'Tokyo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1837,106,N'KN',N'Kanagawa',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1838,106,N'OS',N'Osaka',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1839,106,N'AI',N'Aichi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1840,106,N'CH',N'Chiba',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1841,106,N'HG',N'Hyogo',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1842,106,N'ST',N'Saitama',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1843,106,N'HK',N'Hokkaido',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1844,106,N'FO',N'Fukuoka',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1845,106,N'SZ',N'Shizuoka',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1846,106,N'HS',N'Hiroshima',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1847,106,N'KY',N'Kyoto',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1848,106,N'IB',N'Ibaraki',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1849,106,N'MG',N'Miyagi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1850,106,N'NI',N'Niigata',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1851,106,N'TC',N'Tochigi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1852,106,N'NN',N'Nagano',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1853,106,N'OY',N'Okayama',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1854,106,N'GM',N'Gumma',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1855,106,N'ME',N'Mie',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1856,106,N'FS',N'Fukushima',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1857,106,N'GF',N'Gifu',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1858,106,N'YC',N'Yamaguchi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1859,106,N'KS',N'Kagoshima',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1860,106,N'KM',N'Kumamoto',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1861,106,N'SM',N'Shimane',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1862,106,N'YN',N'Yamanashi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1863,106,N'TT',N'Tottori',0)
---105	Jamaica
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1864,105,N'SD',N'St. Andrew�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1865,105,N'SC',N'St. Catherine�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1866,105,N'SJ',N'Saint James',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1867,105,N'MA',N'Manchester',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1868,105,N'CL',N'Clarendon',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1869,105,N'SN',N'Saint Ann',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1870,105,N'PO',N'Portland',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1871,105,N'TR',N'Trelawny',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1872,105,N'SE',N'Saint Elizabeth',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1873,105,N'ST',N'Saint Thomas',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1874,105,N'SM',N'Saint Mary',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1875,105,N'HA',N'Hanover',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1876,105,N'KI',N'Kingston',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1877,105,N'WE',N'Westmoreland',0)
--103 Israel
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1878,103,N'HD',N'Ha Darom�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1879,103,N'HM',N'Ha Merkaz�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1880,103,N'HA',N'Haifa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1881,103,N'JM',N'Jerusalem�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1882,103,N'TA',N'Tel Aviv�',0)
--102	Ireland
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1883,102,N'',N'Leinster�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1884,102,N'',N'Munster�',0)

--101 Iraq
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1885,101,N'AN',N'al-Anbar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1886,101,N'NA',N'al-Najaf�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1887,101,N'QA',N'al-Qadisiya�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1888,101,N'',N'al-Sulaymaniya�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1889,101,N'TS',N'al-Tamim�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1890,101,N'BB',N'Babil�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1891,101,N'BG',N'Baghdad�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1892,101,N'BA',N'Basra�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1893,101,N'DQ',N'DhiQar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1894,101,N'DI',N'Diyala�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1895,101,N'AR',N'Irbil�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1896,101,N'KA',N'Karbala�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1897,101,N'MA',N'Maysan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1898,101,N'NI',N'Ninawa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1899,101,N'WA',N'Wasit�',0)
---100	Iran, Islamic Republic of
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1900,100,N'AR',N'Ardebil�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1901,100,N'BS',N'Bushehr�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1902,100,N'CM',N'Chaharmahal va Bakht�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1903,100,N'EA',N'East Azerbaidzan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1904,100,N'ES',N'Esfahan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1905,100,N'FA',N'Fars�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1906,100,N'GI',N'Gilan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1907,100,N'GO',N'Golestan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1908,100,N'HD',N'Hamadan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1909,100,N'HG',N'Hormozgan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1910,100,N'IL',N'Ilam�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1911,100,N'KE',N'Kerman�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1912,100,N'BK',N'Kermanshah�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1913,100,N'KS',N'Khorasan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1914,100,N'KZ',N'Khuzestan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1915,100,N'KD',N'Kordestan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1916,100,N'LO',N'Lorestan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1917,100,N'MK',N'Markazi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1918,100,N'MN',N'Mazandaran�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1919,100,N'QZ',N'Qazvin�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1920,100,N'QM',N'Qom�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1921,100,N'SM',N'Semnan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1922,100,N'SB',N'Sistan va Baluchesta�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1923,100,N'TE',N'Teheran�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1924,100,N'WA',N'West Azerbaidzan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1925,100,N'YA',N'Yazd�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1926,100,N'ZA',N'Zanjan�',0)
--99	Indonesia
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1927,99,N'AC',N'Aceh�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1928,99,N'BA',N'Bali�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1929,99,N'BE',N'Bengkulu�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1930,99,N'',N'Central Java�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1931,99,N'',N'East Java�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1932,99,N'JK',N'Jakarta Raya�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1933,99,N'JA',N'Jambi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1934,99,N'KB',N'Kalimantan Barat�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1935,99,N'KS',N'Kalimantan Selatan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1936,99,N'KT',N'Kalimantan Tengah�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1937,99,N'KM',N'Kalimantan Timur�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1938,99,N'LA',N'Lampung�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1939,99,N'',N'Molukit�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1940,99,N'NB',N'Nusa Tenggara Barat�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1941,99,N'NT',N'Nusa Tenggara Timur�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1942,99,N'RI',N'Riau�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1943,99,N'SE',N'Sulawesi Selatan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1944,99,N'ST',N'Sulawesi Tengah�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1945,99,N'SG',N'Sulawesi Tenggara�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1946,99,N'SW',N'Sulawesi Utara�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1947,99,N'SR',N'Sumatera Barat�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1948,99,N'SL',N'Sumatera Selatan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1949,99,N'SW',N'Sumatera Utara�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1950,99,N'',N'West Irian�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1951,99,N'',N'West Java�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1952,99,N'YO',N'Yogyakarta�',0)

-- 97	Iceland
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1954,97,N'',N'Norourland Eystra�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1955,97,N'',N'�Vesturland',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1956,97,N'',N'Vestfiroir',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1957,97,N'',N'Suournes',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1958,97,N'',N'�Norourland Vestra',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1959,97,N'',N'Gullbringusysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1960,97,N'',N'�Eyjafjardarsysla',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1961,97,N'',N'Arnessysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1962,97,N'',N'Borgarfjardarsysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1963,97,N'',N'Rangarvallasysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1964,97,N'',N'Sudur-Tingeyjarsysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1965,97,N'',N'Snafellsnes- og Hnappadalssysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1966,97,N'',N'Myrasysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1967,97,N'',N'�Austur-Skaftafellssysla',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1968,97,N'',N'Vestur-Isafjardarsysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1969,97,N'',N'Austur-Hunavatnssysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1970,97,N'',N'�Vestur-Hunavatnssysla',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1971,97,N'',N'Strandasysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1972,97,N'',N'Sudur-Mulasysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1973,97,N'',N'�Nordur-Tingeyjarsysla',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1974,97,N'',N'Skagafjardarsysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1975,97,N'',N'�Vestur-Bardastrandarsysla',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1976,97,N'',N'Vestur-Skaftafellssysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1977,97,N'',N'Kjosarsysla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1978,97,N'',N'Nordur-Mulasysla�',0)

--95	Hong Kong
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1979,95,N'',N'Hongkong��',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1980,95,N'KC',N'Kowloon and New Kowl��',0)

--94	Honduras
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1981,94,N'AT',N'Atl�ntida�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1982,94,N'CR',N'Cort�s�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1983,94,N'',N'Distrito Central��',0)

--91 Haiti
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1984,91,N'AR',N'Artibonite�',0)

---90	Guyana
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1985,90,N'DE',N'Demerara-Mahaica',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1986,90,N'UD',N'Upper Demerara-Berbice�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1987,90,N'EB',N'East Berbice-Corentyne�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1988,90,N'CU',N'Cuyuni-Mazaruni�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1989,90,N'MA',N'Mahaica-Berbice�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1990,90,N'ES',N'Essequibo Islands-West Demerara�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1991,90,N'BA',N'Barima-Waini�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1992,90,N'UT',N'Upper Takutu-Upper Essequibo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1993,90,N'PT',N'Potaro-Siparuni�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1994,90,N'PM',N'Pomeroon-Supenaam�',0)

--88 Guinea
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1995,88,N'',N'�Litoral',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1996,88,N'',N'Bioko Norte�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1997,88,N'',N'Kie-Ntem�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1998,88,N'',N'Centro Sur�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1999,88,N'',N'Bioko Sur�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2000,88,N'',N'Wele-Nzas�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2001,88,N'',N'Annobon�',0)

--87	Guatemala
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2002,87,N'GU',N'Guatemala�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2003,87,N'CM',N'Quetzaltenango�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2004,87,N'ES',N'Escuintla�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2005,87,N'CM',N'Chimaltenango�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2006,87,N'SA',N'Sacatepequez�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2007,87,N'HU',N'Huehuetenango�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2008,87,N'SM',N'San Marcos�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2009,87,N'QC',N'Quiche�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2010,87,N'SA',N'Suchitepequez�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2011,87,N'TO',N'Totonicapan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2012,87,N'SO',N'Solola�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2013,87,N'AV',N'Alta Verapaz',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2014,87,N'PE',N'Peten�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2015,87,N'JU',N'Jutiapa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2016,87,N'SR',N'Santa Rosa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2017,87,N'IZ',N'Izabal�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2018,87,N'RE',N'Retalhuleu�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2019,87,N'ZA',N'Zacapa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2020,87,N'CQ',N'Chiquimula�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2021,87,N'JA',N'Jalapa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2022,87,N'BV',N'Baja Verapaz�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2023,87,N'PR',N'El Progreso�',0)

--85 Guadeloupe
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2024,85,N'BT',N'Basse-Terre',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2025,85,N'PP',N'Grande-Terre�',0)


-- 84	Grenada
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2026,84,N'JO',N'Saint John�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2027,84,N'AN',N'Saint Andrew�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2028,84,N'MA',N'Saint Mark�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2029,84,N'PA',N'Saint Patrick�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2030,84,N'DA',N'Saint David�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2031,84,N'GE',N'Saint George�',0)
--77	Gambia

INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2032,77,N'WE',N'Western�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2033,77,N'LR',N'Lower River�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2034,77,N'BJ',N'Banjul�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2035,77,N'UR',N'Upper River�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2036,77,N'CR',N'Central River�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2037,77,N'NB',N'North Bank�',0)

-- 76	Gabon
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2038,76,N'ES',N'Estuaire��',0)
--73	French Guiana
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2039,73,N'CY',N'Cayenne��',0)
--70 Fiji
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2040,70,N'CE',N'Central�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2041,70,N'EA',N'Eastern�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2042,70,N'NO',N'Northern�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2043,70,N'RO',N'Rotuma�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2044,70,N'WE',N'Western	�',0)
--67	Ethiopia
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2045,67,N'',N'YeDebub Biheroch Bihereseboch na Hizboc�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2046,67,N'AF',N'Afar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2047,67,N'AM',N'Amara�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2048,67,N'BE',N'Binshangul Gumuz',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2049,67,N'AA',N'Adis Abeba�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2050,67,N'GA',N'Gambela Hizboch�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2051,67,N'HA',N'Hareri Hizb�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2052,67,N'OR',N'Oromiya�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2053,67,N'SO',N'Sumale�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2054,67,N'TI',N'Tigray�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2055,67,N'DD',N'Dire Dawa�',0)
--66 Estonia
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2056,66,N'',N'�Narva',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2057,66,N'',N'Kohtla-Jarve�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2058,66,N'',N'�Sillamae',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2059,66,N'PR',N'Parnu�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2060,66,N'',N'Tallinn�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2061,66,N'HA',N'Harjumaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2062,66,N'',N'Ida-Virumaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2063,66,N'',N'Tartumaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2064,66,N'',N'Parnumaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2065,66,N'',N'Laane-Virumaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2066,66,N'',N'Viljandimaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2067,66,N'',N'Jarvamaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2068,66,N'',N'Vorumaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2069,66,N'',N'Jogevamaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2070,66,N'',N'Raplamaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2071,66,N'',N'Valgamaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2072,66,N'',N'Saaremaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2073,66,N'',N'�Laanemaa',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2074,66,N'',N'Polvamaa�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2075,66,N'',N'Hiiumaa�',0)

--65 Eritrea 
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2076,65,N'AN',N'Anseba�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2077,65,N'DU',N'Debub�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2078,65,N'DK',N'Debubawi Keyih Bahri',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2079,65,N'GB',N'Gash Barka�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2080,65,N'MA',N'Maakel',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2081,65,N'SK',N'Semenawi Keyih Bahri',0)
--64	Equatorial Guinea
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2082,64,N'LI',N'Litoral�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2083,64,N'BN',N'Bioko Norte�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2084,64,N'KN',N'Kie-Ntem�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2085,64,N'CS',N'Centro Sur�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2086,64,N'BS',N'Bioko Sur�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2087,64,N'WN',N'Wele-Nzas�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2088,64,N'AN',N'Annobon�',0)

-- 63	El Salvador
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2089,63,N'SS',N'San Salvador�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2090,63,N'LI',N'La Libertad�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2091,63,N'SA',N'Santa Ana�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2092,63,N'SO',N'Sonsonate�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2093,63,N'US',N'Usulutan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2094,63,N'CU',N'Cuscatlan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2095,63,N'PA',N'La Paz�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2096,63,N'AH',N'Ahuachapan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2097,63,N'UN',N'La Union�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2098,63,N'SV',N'San Vicente�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2099,63,N'SM',N'San Miguel�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2100,63,N'MO',N'Morazan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2101,63,N'CH',N'Chalatenango�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2102,63,N'CA',N'Cabanas�',0)
-- 61	Ecuador
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2103,61,N'GY',N'Guayas�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2104,61,N'PC',N'Pichincha�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2105,61,N'MN',N'Manabi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2106,61,N'EO',N'El Oro�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2107,61,N'LR',N'Los Rios�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2108,61,N'AZ',N'Azuay�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2109,61,N'LJ',N'Loja�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2110,61,N'TU',N'Tungurahua�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2111,61,N'ES',N'Esmeraldas�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2112,61,N'IM',N'Imbabura�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2113,61,N'CB',N'Chimborazo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2114,61,N'CT',N'Cotopaxi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2115,61,N'CN',N'Canar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2116,61,N'MS',N'Morona-Santiago�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2117,61,N'BO',N'Bolivar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2118,61,N'ZC',N'Zamora-Chinchipe�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2119,61,N'PA',N'Pastaza�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2120,61,N'NA',N'Napo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2121,61,N'SU',N'Sucumbios�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2122,61,N'GA',N'Galapagos�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2123,61,N'CR',N'Carchi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2124,61,N'OR',N'Orellana�',0)
--60	Dominican Republic
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2125,60,N'NC',N'Distrito Nacional��',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2126,60,N'DU',N'Duarte��',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2127,60,N'RO',N'La Romana��',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2128,60,N'PP',N'Puerto Plata��',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2129,60,N'PM',N'San Pedro de Macor�s��',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2130,60,N'ST',N'Santiago��',0)
-- 59	Dominica
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2131,59,N'GO',N'Saint George�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2132,59,N'PK',N'Saint Patrick�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2133,59,N'AN',N'Saint Andrew�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2134,59,N'DA',N'Saint David�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2135,59,N'JN',N'Saint John�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2136,59,N'PL',N'Saint Paul�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2137,59,N'PR',N'Saint Peter�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2138,59,N'JH',N'Saint Joseph�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2139,59,N'LU',N'Saint Luke�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2140,59,N'MA',N'Saint Mark�',0)
-- 58	Djibouti
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2141,58,N'AS',N'Ali Sabieh�',0)

--56 Czech Republic
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2142,56,N'',N'Hlavni mesto Praha�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2143,56,N'MO',N'Moravskoslezsky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2144,56,N'JM',N'Jihomoravsky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2145,56,N'UK',N'Ustecky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2146,56,N'ST',N'Stredocesky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2147,56,N'JC',N'Jihocesky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2148,56,N'ZK',N'Zlinsky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2149,56,N'OK',N'Olomoucky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2150,56,N'PK',N'Plzensky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2151,56,N'JK',N'Vysocina�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2152,56,N'EK',N'Pardubicky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2153,56,N'KK',N'Karlovarsky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2154,56,N'HR',N'Kralovehradecky kraj�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2155,56,N'LK',N'Liberecky kraj',0)
--54	Cuba
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2156,54,N'CM',N'Camag�ey �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2157,54,N'CA',N'Ciego de �vila �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2158,54,N'CF',N'Cienfuegos �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2159,54,N'GR',N'Granma �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2160,54,N'GU',N'Guant�namo �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2161,54,N'HO',N'Holgu�n �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2162,54,N'CH',N'La Habana �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2163,54,N'LT',N'Las Tunas �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2164,54,N'MA',N'Matanzas �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2165,54,N'PD',N'Pinar del R�o �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2166,54,N'SS',N'Sancti-Sp�ritus �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2167,54,N'SC',N'Santiago de Cuba �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2168,54,N'VC',N'Villa Clara �',0)
-- 49	Congo
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2169,49,N'BN',N'Bandundu �',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2170,49,N'',N'Bas-Za�re �',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2171,49,N'',N'East Kasai �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2172,49,N'ET',N'Equateur �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2173,49,N'HK',N'Haute-Za�re �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2174,49,N'KN',N'Kinshasa �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2175,49,N'',N'North Kivu �',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2176,49,N'',N'Shaba �',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2177,49,N'',N'South Kivu �',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2178,49,N'',N'West Kasai �',0)

--48	Comoros
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2179,48,N'GC',N'Grande Comore�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2180,48,N'AN',N'Anjouan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2181,48,N'MO',N'Moheli�',0)
--43	Chile
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2182,43,N'AN',N'Antofagasta �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2183,43,N'AT',N'Atacama �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2184,43,N'BI',N'B�ob�o �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2185,43,N'CO',N'Coquimbo �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2186,43,N'AR',N'La Araucan�a �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2187,43,N'LG',N'Los Lagos �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2188,43,N'MA',N'Magallanes �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2189,43,N'ML',N'Maule �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2190,43,N'LI',N'O�Higgins �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2191,43,N'RM',N'Santiago �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2192,43,N'TP',N'Tarapac� �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2193,43,N'VS',N'Valpara�so �',0)
-- 42	Chad
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2194,42,N'MO',N'Moyen-Chari�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2195,42,N'LO',N'Logone Occidental�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2196,42,N'CG',N'Chari-Baguirmi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2197,42,N'ME',N'Mayo-Kebbi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2198,42,N'OA',N'Ouaddai�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2199,42,N'TA',N'Tandjile�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2200,42,N'GR',N'Guera�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2201,42,N'BA',N'Batha�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2202,42,N'LO',N'Logone Oriental�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2203,42,N'KM',N'Kanem�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2204,42,N'SA',N'Salamat�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2205,42,N'BR',N'Borkou-Ennedi-Tibesti�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2206,42,N'BI',N'Biltine�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2207,42,N'LC',N'Lac�',0)

--41	Central African Republic
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2208,41,N'BG',N'Bangui�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2209,41,N'HS',N'Mambere-Kadei�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2210,41,N'UK',N'Ouaka�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2211,41,N'MP',N'Ombella-Mpoko�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2212,41,N'LB',N'Lobaye�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2213,41,N'OP',N'Ouham-Pende�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2214,41,N'NM',N'Nana-Mambere�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2215,41,N'MB',N'Mbomou�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2216,41,N'AC',N'Ouham�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2217,41,N'SE',N'Sangha-Mbaere�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2218,41,N'KG',N'Kemo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2219,41,N'HK',N'Haute-Kotto�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2220,41,N'KB',N'Nana-Grebizi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2221,41,N'BK',N'Basse-Kotto�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2222,41,N'HM',N'Haut-Mbomou�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2223,41,N'BB',N'Bamingui-Bangoran�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2224,,N'',N'Cuvette-Ouest	�',0)

-- 8	Antarctica
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2225,8,N'',N'Havlo�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2226,8,N'',N'Victoria�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2227,8,N'',N'North Antarctica�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2228,8,N'',N'Byrdland�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2229,8,N'',N'Newbin�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2230,8,N'',N'Atchabinic�',0)

--40	Cayman Islands
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2231,40,N'',N'Creek�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2232,40,N'',N'Eastern�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2233,40,N'',N'Midland�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2234,40,N'',N'Spot Bay�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2235,40,N'',N'Stake Bay�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2236,40,N'',N'West End�',0)

---39	Cape Verde
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2224,39,N'',N'Tarrafal�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2225,39,N'',N'Brava�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2226,39,N'',N'Maio�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2227,39,N'',N'Mosteiros�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2228,39,N'',N'Paul�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2229,39,N'',N'Praia�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2230,39,N'',N'Ribeira Grande�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2231,39,N'',N'Sal�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2232,39,N'',N'Santa Catarina�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2233,39,N'',N'Santa Cruz�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2234,39,N'',N'Boa Vista�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2235,39,N'',N'Sao Filipe�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2236,39,N'',N'Sao Miguel�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2237,39,N'',N'Sao Nicolau�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2238,39,N'',N'Sao Vicente�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2239,39,N'',N'Sao Domingos�',0)

-- 37 Cameroon
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2240,37,N'CE',N'Centre �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2241,37,N'EN',N'Extr�me-Nord �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2242,37,N'LT',N'Littoral �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2243,37,N'NO',N'Nord �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2244,37,N'NW',N'Nord-Ouest �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2245,37,N'OU',N'Ouest �',0)
--36	Cambodia
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2246,36,N'BA',N'Battambang �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2247,36,N'PP',N'Phnom Penh �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2248,36,N'SI',N'Siem Reap �',0)

--35	Burundi
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2249,35,N'BM',N'Bujumbura�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2250,35,N'MY',N'Muyinga�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2251,35,N'RY',N'Ruyigi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2252,35,N'BR',N'Bururi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2253,35,N'GI',N'Gitega�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2254,35,N'NG',N'Ngozi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2255,35,N'RT',N'Rutana�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2256,35,N'MA',N'Makamba�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2257,35,N'KY',N'Kayanza�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2259,35,N'MV',N'Muramvya�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2260,35,N'CI',N'Cibitoke�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2261,35,N'BB',N'Bubanza�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2262,35,N'KR',N'Karuzi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2263,35,N'CA',N'Cankuzo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2264,35,N'KI',N'Kirundo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2265,35,N'MW',N'Mwaro�',0)

-- 34	Burkina Faso
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2266,34,N'BLK1',N'Boulkiemd� �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2267,34,N'HOU',N'Houet �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2268,34,N'KAD',N'Kadiogo �',0)

-- 32	Brunei Darussalam
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2269,32,N'BM',N'Brunei and Muara �',0)

-- 28	Botswana
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2270,28,N'SE',N'South-East�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2271,28,N'CE',N'Central�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2272,28,N'KW',N'Kweneng�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2273,28,N'NE',N'North-East�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2274,28,N'NW',N'North-West�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2275,28,N'SO',N'Southern�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2276,28,N'KL',N'Kgatleng�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2277,28,N'GH',N'Ghanzi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2278,28,N'KG',N'Kgalagadi�',0)

--27	Bosnia and Herzegovina

INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2279,27,N'BF',N'Federation of Bosnia and Herzegovina�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2280,27,N'SR',N'Republika Srpska�',0)

--26	Bolivia
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2281,26,N'CQ',N'Chuquisaca �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2282,26,N'CB',N'Cochabamba �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2283,26,N'LP',N'La Paz �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2284,26,N'OR',N'Oruro �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2285,26,N'PO',N'Potos� �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2286,26,N'SC',N'Santa Cruz �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2287,26,N'TR',N'Tarija �',0)

-- 25 Bhutan
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2288,25,N'CK',N'Chhukha�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2289,25,N'PN',N'Punakha	�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2290,25,N'SJ',N'Samdrup�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2291,25,N'GE',N'Geylegphug�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2292,25,N'BU',N'Bumthang�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2293,25,N'PR',N'Paro�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2294,25,N'TA',N'Tashigang�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2295,25,N'WP',N'Wangdi Phodrang�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2296,25,N'DA',N'Daga�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2297,25,N'TO',N'Tongsa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2298,25,N'CR',N'Chirang�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2299,25,N'LH',N'Lhuntshi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2300,25,N'PM',N'Pemagatsel�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2301,25,N'HA',N'Ha�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2302,25,N'MO',N'Mongar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2303,25,N'SG',N'Shemgang�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2304,25,N'TM',N'Thimphu�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2305,25,N'SM',N'Samchi�',0)

-- 23	Benin
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2306,23,N'LI',N'Littoral�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2307,23,N'ZO',N'Zou�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2308,23,N'DO',N'Donga�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2309,23,N'BO',N'Borgou�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2310,23,N'AL',N'Alibori�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2311,23,N'CL',N'Collines�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2312,23,N'MO',N'Mono�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2313,23,N'AQ',N'Atlanyique�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2314,23,N'AK',N'Atakora�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2315,23,N'PL',N'Plateau�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2316,23,N'CF',N'Kouffo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2317,23,N'OU',N'Oueme�',0)

-- 22	Belize
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2318,22,N'BZ',N'Belize�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2319,22,N'CY',N'Cayo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2320,22,N'OW',N'Orange Walk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2321,22,N'SC',N'Stann Creek�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2322,22,N'CZ',N'Corozal�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2323,22,N'TO',N'Toledo�',0)

--20	Belarus
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2324,21,N'BR',N'Brest �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2325,21,N'HO',N'Gomel �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2326,21,N'HR',N'Grodno �',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2239,21,N'',N'Horad Minsk �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2327,21,N'MI',N'Minsk �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2328,21,N'MA',N'Mogiljov �',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2329,21,N'VI',N'Vitebsk �',0)

-- 19	Barbados
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2330,19,N'MI',N'Saint Michael�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2331,19,N'PE',N'Saint Peter�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2332,19,N'JS',N'Saint Joseph�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2333,19,N'CC',N'Christ Church�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2334,19,N'JM',N'Saint James�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2335,19,N'AN',N'Saint Andrew�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2336,19,N'LU',N'Saint Lucy�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2337,19,N'TH',N'Saint Thomas�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2338,19,N'GE',N'Saint George�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2339,19,N'JN',N'Saint John�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2340,19,N'PH',N'Saint Philip�',0)

--  16	Bahamas
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2491,16,N'NW',N'New Providence',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2341,16,N'FP',N'Freeport�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2342,16,N'MH',N'Marsh Harbour�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2343,16,N'HR',N'High Rock�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2344,16,N'FC',N'Fresh Creek�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2345,16,N'LI',N'Long Island�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2346,16,N'HB',N'Harbour Island�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2347,16,N'RS',N'Rock Sound�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2348,16,N'BI',N'Bimini�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2349,16,N'SR',N'San Salvador and Rum Cay�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2350,16,N'AC',N'Acklins and Crooked Islands�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2351,16,N'GT',N'Green Turtle Cay�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2352,16,N'IN',N'Inagua�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2353,16,N'NB',N'Nichollstown and Berry Islands�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2354,16,N'MG',N'Mayaguana�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2355,16,N'RI',N'Ragged Island�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2356,16,N'CI',N'Cat Island�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2357,16,N'GH',N'Governor''s Harbour',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2358,16,N'EM',N'Exuma',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2359,16,N'SP',N'Sandy Point�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2360,16,N'KB',N'Kemps Bay�',0)
-- 15	Azerbaijan
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2490,15,N'BK',N'Baki�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2361,15,N'YV',N'Yevlax�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2362,15,N'XZ',N'Xacmaz�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2363,15,N'SL',N'Salyan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2364,15,N'AM',N'Agdam�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2365,15,N'GY',N'Goycay�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2366,15,N'IM',N'Imisli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2367,15,N'SB',N'Sabirabad�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2368,15,N'TO',N'Tovuz�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2369,15,N'AS',N'Agdas�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2370,15,N'NX',N'Naxcivan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2371,15,N'AR',N'Abseron�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2372,15,N'QB',N'Quba�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2373,15,N'AA',N'Astara�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2374,15,N'QZ',N'Qazax�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2375,15,N'SU',N'Susa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2376,15,N'NE',N'Neftcala�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2377,15,N'ZQ',N'Zaqatala�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2378,15,N'BS',N'Bilasuvar�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2379,15,N'',N'Xanlar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2380,15,N'QR',N'Qusar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2381,15,N'AU',N'Agsu�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2382,15,N'UC',N'Ucar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2383,15,N'AF',N'Agstafa�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2384,15,N'QX',N'Qax�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2385,15,N'SQ',N'Sumqayit�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2386,15,N'GR',N'Goranboy�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2387,15,N'LE',N'Lerik�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2388,15,N'NA',N'Naftalan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2389,15,N'SX',N'Samux�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2390,15,N'HA',N'Haciqabul�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2391,15,N'FU',N'Fuzuli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2392,15,N'XD',N'Xocavand�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2393,15,N'LN',N'Lankaran�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2394,15,N'XI',N'Xizi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2395,15,N'BL',N'Balakan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2396,15,N'IS',N'Ismayilli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2397,15,N'KA',N'Kalbacar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2398,15,N'QO',N'Qobustan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2399,15,N'QD',N'Qubadli�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2400,15,N'',N'Qabala�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2401,15,N'ST',N'Saatli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2402,15,N'OG',N'Oguz�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2403,15,N'',N'Saki�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2404,15,N'MI',N'Mingacevir�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2405,15,N'SI',N'Samaxi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2406,15,N'SM',N'Samkir�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2407,15,N'MA',N'Masalli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2408,15,N'SY',N'Siyazan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2409,15,N'LC',N'Lacin�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2410,15,N'KU',N'Kurdamir�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2411,15,N'TA',N'Tartar�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2412,15,N'GA',N'Ganca�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2413,15,N'GD',N'Gadabay�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2414,15,N'',N'Davaci�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2415,15,N'XA',N'Xankandi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2416,15,N'CL',N'Calilabad�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2417,15,N'CB',N'Cabrayil�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2418,15,N'XC',N'Xocali�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2419,15,N'BQ',N'Beylaqan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2420,15,N'YR',N'Yardimli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2421,15,N'BR',N'Barda�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2422,15,N'',N'Ali Bayramli�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2423,15,N'AC',N'Agcabadi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2424,15,N'ZR',N'Zardab�',0)

--12	Aruba	
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2340,12,N'',N'Netherlands Antilles�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2341,12,N'',N'Dominican Republic�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2342,12,N'',N'Haiti�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2343,12,N'',N'Venezuela�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2344,12,N'',N'U.S. Virgin Islands�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2345,12,N'',N'British Virgin Islands�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2346,12,N'',N'Grenada�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2347,12,N'',N'Saint Kitts and Nevis�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2348,12,N'',N'Saint Vincent and the Grenadines�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2349,12,N'',N'Montserrat�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2350,12,N'',N'Anguilla�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2351,12,N'',N'Trinidad and Tobago�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2352,12,N'',N'Dominica�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2353,12,N'',N'Guadeloupe',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2354,12,N'',N'Saint Lucia�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2355,12,N'',N'Martinique�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2356,12,N'',N'Jamaica�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2357,12,N'',N'Colombia�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2358,12,N'',N'Antigua and Barbuda�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2359,12,N'',N'Turks and Caicos Islands�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2360,12,N'',N'Barbados�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2361,12,N'',N'Panama�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2362,12,N'',N'Cuba�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2363,12,N'',N'Costa Rica�',0)
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2364,12,N'',N'Nicaragua �',0)
--11	Armenia
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2425,11,N'ER',N'Yerevan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2426,11,N'SH',N'Shirak�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2427,11,N'LO',N'Lorri�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2428,11,N'AV',N'Armavir',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2429,11,N'KT',N'Kotayk�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2430,11,N'AR',N'Ararat�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2431,11,N'GR',N'Geghark unik',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2432,11,N'SU',N'Syunik',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2433,11,N'AG',N'Aragatsotn�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2434,11,N'TV',N'Tavush�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2435,11,N'VD',N'Vayots Dzor�',0)
--9	Antigua and Barbuda
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2488,9,N'PA',N'Saint Paul�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2436,9,N'PE',N' Saint Peter�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2437,9,N'GE',N' Saint George�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2438,9,N'BB',N'Barbuda�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2439,9,N'MA',N'Saint Mary�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2440,9,N'PH',N'Saint Philip�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2441,9,N'JO',N'Saint John�',0)

--6	Angola
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2442,6,N'LU',N'Luanda�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2443,6,N'BG',N'Benguela�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2444,6,N'HM',N'Huambo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2445,6,N'BI',N'Bie�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2446,6,N'HL',N'Huila�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2447,6,N'ML',N'Malanje�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2448,6,N'NA',N'Namibe�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2449,6,N'ZA',N'Zaire�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2450,6,N'CB',N'Cabinda�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2451,6,N'UI',N'Uige�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2452,6,N'MX',N'Moxico�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2453,6,N'LS',N'Lunda Sul�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2454,6,N'CS',N'Cuanza Sul�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2455,6,N'BO',N'Bengo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2456,6,N'LN',N'Lunda Norte�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2457,6,N'CC',N'Cuando Cubango�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2458,6,N'CN',N'Cuanza Norte�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2459,6,N'CU',N'Cunene�',0)
--2	Albania
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2460,2,N'TI',N' Tirane�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2461,2,N'DU',N'Purres�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2462,2,N'EB',N'Elbasan�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2463,2,N'FI',N'Fier�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2464,2,N'KE',N'Korce�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2465,2,N'VR',N'Vlore�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2466,2,N'SD',N'Shkoder�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2467,2,N'BE',N'Berat�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2468,2,N'LZ',N'Lezhe�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2469,2,N'GK',N'Gjiro Kaster�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2470,2,N'DI',N'Diber�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2471,2,N'KU',N'Kukes�',0)

-- 112	Korea, Republic of
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2472,112,N'SO',N'Seoul-t''ukpyolsi',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2473,112,N'CB',N'Cholla-bukto�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2474,112,N'CN',N'Cholla-namdo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2475,112,N'GB',N'Ch''ungch''ong-bukto�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2476,112,N'GN',N'Ch''ungch''ong-namdo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2477,112,N'KJ',N'Inch''on-jikhalsi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2478,112,N'KW',N'Kangwon-do�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2479,112,N'KJ',N'Kwangju-jikhalsi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2480,112,N'KG',N'Kyonggi-do�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2481,112,N'KB',N'Kyongsang-bukto�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2482,112,N'KN',N'Kyongsang-namdo�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2483,112,N'PU',N'Pusan-jikhalsi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2484,112,N'TG',N'Taegu-jikhalsi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2485,112,N'TJ',N'Taejon-jikhalsi�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2486,112,N'CJ',N'Cheju-do�',0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2487,112,N'UL',N'Ulsan-gwangyoksi	�',0)

SET IDENTITY_INSERT [dbo].[Gen_State_S] OFF
GO

/* ************************* FB 2657 Provide additional flexibility for Country designations in Room Profile - ENDS - 4th April 2013 ***************************** */