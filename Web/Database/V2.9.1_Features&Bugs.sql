/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.1			                            */
/*                                                                                              */
/* ******************************************************************************************** */

/* ************** FB 2856 - Starts  01 Aug 2013 ******************** */
update gen_timezone_s set TimeZone ='Kuwait, Riyadh' where TimeZoneID = 3
update gen_timezone_s set TimeZone ='Abu Dhabi, Muscat' where TimeZoneID = 4
update gen_timezone_s set TimeZone ='Atlantic Time (Canada)' where TimeZoneID = 6
update gen_timezone_s set TimeZone ='Canberra, Melbourne, Sydney' where TimeZoneID =8 
update gen_timezone_s set TimeZone ='Cape Verde Islands' where TimeZoneID = 11
update gen_timezone_s set TimeZone ='Yerevan' where TimeZoneID = 12
update gen_timezone_s set TimeZone ='Central America' where TimeZoneID = 14
update gen_timezone_s set TimeZone ='Astana' where TimeZoneID = 15
update gen_timezone_s set TimeZone ='Belgrade, Bratislava, Budapest, Ljubljana, Prague' where TimeZoneID = 16
update gen_timezone_s set TimeZone ='Sarajevo, Skopje, Warsaw, Zagreb' where TimeZoneID = 17
update gen_timezone_s set TimeZone ='Solomon Islands, New Caledonia' where TimeZoneID = 18
update gen_timezone_s set TimeZone ='Central Time (US and Canada)' where TimeZoneID = 19
update gen_timezone_s set TimeZone ='Beijing, Chongqing, Hong Kong SAR, Urumqi' where TimeZoneID = 20
update gen_timezone_s set TimeZone ='International Date Line West' where TimeZoneID = 21
update gen_timezone_s set TimeZone ='Minsk' where TimeZoneID = 24
update gen_timezone_s set TimeZone ='Eastern Time (US and Canada)' where TimeZoneID = 26
update gen_timezone_s set TimeZone ='Fiji' where TimeZoneID = 29
update gen_timezone_s set TimeZone ='Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius' where TimeZoneID = 30
update gen_timezone_s set TimeZone ='Dublin, Edinburgh, Lisbon, London' where TimeZoneID = 31
update gen_timezone_s set TimeZone ='Monrovia, Reykjavik' where TimeZoneID = 76
update gen_timezone_s set TimeZone ='Chennai, Kolkata, Mumbai, New Delhi' where TimeZoneID = 36
update gen_timezone_s set TimeZone ='Mountain Time (US and Canada)' where TimeZoneID = 42
update gen_timezone_s set TimeZone ='Yangon (Rangoon)' where TimeZoneID = 43
update gen_timezone_s set TimeZone ='Almaty, Novosibirsk', TimeZoneDiff ='(UTC+07:00)',Offset = -7, Bias = -420 where TimeZoneID = 44
update gen_timezone_s set  TimeZoneDiff ='(UTC+06:00)',Offset = -6, Bias = -360 where TimeZoneID = 28
update gen_timezone_s set  Offset = -5.5, Bias = -330, TimezoneDiff='(UTC+05:30)' where TimeZoneID = 61
update gen_timezone_s set TimeZone ='Auckland, Wellington' where TimeZoneID = 46
update gen_timezone_s set TimeZone ='Newfoundland' where TimeZoneID = 47
update gen_timezone_s set TimeZone ='Irkutsk' where TimeZoneID = 48
update gen_timezone_s set TimeZone ='Pacific Time (US and Canada)' where TimeZoneID = 51
update gen_timezone_s set TimeZone ='Brussels, Copenhagen, Madrid, Paris' where TimeZoneID = 52
update gen_timezone_s set TimeZone ='Moscow, St. Petersburg, Volgograd',Offset = -4, Bias = -240, TimezoneDiff='(UTC+04:00)' where TimeZoneID = 53
update gen_timezone_s set TimeZone ='Cayenne, Fortaleza' where TimeZoneID = 54
update gen_timezone_s set TimeZone ='Bogota, Lima, Quito' where TimeZoneID = 55
update gen_timezone_s set TimeZone ='Georgetown, La Paz, Manaus, San Juan' where TimeZoneID = 56
update gen_timezone_s set TimeZone ='Samoa' where TimeZoneID = 57
update gen_timezone_s set TimeZone ='Bangkok, Hanoi, Jakarta' where TimeZoneID = 58
update gen_timezone_s set TimeZone ='Kuala Lumpur, Singapore' where TimeZoneID = 59
update gen_timezone_s set TimeZone ='Harare, Pretoria' where TimeZoneID = 60
update gen_timezone_s set TimeZone ='Sri Jayawardenepura' where TimeZoneID = 61
update gen_timezone_s set TimeZone ='Osaka, Sapporo, Tokyo' where TimeZoneID = 64
update gen_timezone_s set TimeZone ='Arizona' where TimeZoneID = 67
update gen_timezone_s set TimeZone ='West Central Africa' where TimeZoneID = 70
update gen_timezone_s set TimeZone ='Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna' where TimeZoneID = 71
update gen_timezone_s set TimeZone ='Tashkent' where TimeZoneID = 72
update gen_timezone_s set TimeZone ='Guam, Port Moresby' where TimeZoneID = 73
update gen_timezone_s set TimeZone ='Athens, Bucharest' where TimeZoneID = 34
update gen_timezone_s set  Offset = -11, Bias = -660, TimezoneDiff='(UTC+11:00)' where TimeZoneID = 68
update gen_timezone_s set  Offset = -8, Bias = -480, TimezoneDiff='(UTC+08:00)' where TimeZoneID = 49
update gen_timezone_s set  Offset = -9, Bias = -540, TimezoneDiff='(UTC+09:00)' where TimeZoneID = 48
update gen_timezone_s set  Offset = -10, Bias = -600, TimezoneDiff='(UTC+10:00)' where TimeZoneID = 74


insert into gen_timezone_s values (77,'Argentina Standard Time', 3, 'Buenos Aires', '(UTC-03:00)', 180, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (78,'Azerbaijan Standard Time', -4, 'Baku', '(UTC+04:00)', -240, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-03-30 04:00:00.000', '2008-10-26 05:00:00.000', '2009-03-29 04:00:00.000', '2009-10-25 05:00:00.000', '2010-03-28 04:00:00.000', '2010-10-31 05:00:00.000', '2011-03-27 04:00:00.000', '2011-10-30 05:00:00.000', '2012-03-25 04:00:00.000', '2012-10-28 05:00:00.000', '2013-03-31 04:00:00.000', '2013-10-27 05:00:00.000', '2014-03-30 04:00:00.000', '2014-10-26 05:00:00.000', '2015-03-29 04:00:00.000', '2015-10-25 05:00:00.000', '2016-03-27 04:00:00.000', '2016-10-30 05:00:00.000', '2017-03-26 04:00:00.000', '2017-10-29 05:00:00.000')
insert into gen_timezone_s values (79, 'Bangladesh Standard Time' , -6, 'Dhaka', '(UTC+06:00)', -360, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (80,'Central Brazilian Standard Time', 4, 'Cuiaba', '(UTC-04:00)', 240, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-10-19 00:00:00.000', '2009-02-15 00:00:00.000', '2009-10-18 00:00:00.000', '2010-02-21 00:00:00.000', '2010-10-17 00:00:00.000', '2011-02-20 00:00:00.000', '2011-10-16 00:00:00.000', '2012-02-26 00:00:00.000', '2012-10-21 00:00:00.000', '2013-02-17 00:00:00.000', '2013-10-19 00:00:00.000', '2014-02-22 00:00:00.000', '2014-10-18 00:00:00.000', '2015-02-21 00:00:00.000', '2015-10-16 00:00:00.000', '2016-02-19 00:00:00.000', '2016-10-15 00:00:00.000', '2017-02-18 00:00:00.000', '2017-10-21 00:00:00.000', '2018-02-17 00:00:00.000')
insert into gen_timezone_s values (81,'Central Standard Time (Mexico)', 6, 'Guadalajara, Mexico City, Monterrey', '(UTC-06:00)', 360, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-04-06 02:00:00.000', '2008-10-26 02:00:00.000', '2009-04-05 02:00:00.000', '2009-10-25 02:00:00.000', '2010-04-05 02:00:00.000', '2010-10-31 02:00:00.000', '2011-04-03 02:00:00.000', '2011-10-30 02:00:00.000', '2012-04-01 02:00:00.000', '2012-10-28 02:00:00.000', '2013-04-07 02:00:00.000', '2013-10-27 02:00:00.000', '2014-04-06 02:00:00.000', '2014-10-26 02:00:00.000', '2015-04-05 02:00:00.000', '2015-10-25 02:00:00.000', '2016-04-03 02:00:00.000', '2016-10-30 02:00:00.000', '2016-03-06 02:00:00.000', '2016-10-30 02:00:00.000')
insert into gen_timezone_s values (82,'Georgian Standard Time', -4, 'Tbilisi', '(UTC+04:00)', -240, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (83,'Jordan Standard Time', -2, 'Amman', '(UTC+02:00)', -120, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-03-28 00:00:00.000', '2008-10-21 00:00:00.000', '2009-03-27 00:00:00.000', '2009-10-30 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (84,'Kaliningrad Standard Time', -3, 'Kaliningrad', '(UTC+03:00)', -180, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-03-30 02:00:00.000', '2008-10-26 03:00:00.000', '2009-03-29 02:00:00.000', '2009-10-25 03:00:00.000', '2010-03-28 02:00:00.000', '2010-10-31 03:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (85,'Magadan Standard Time', -12, 'Magadan', '(UTC+12:00)', -600, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-03-30 02:00:00.000', '2008-10-26 03:00:00.000', '2009-03-29 02:00:00.000', '2009-10-25 03:00:00.000', '2010-03-28 02:00:00.000', '2010-10-31 03:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (86,'Mauritius Standard Time', -4, 'Port Louis', '(UTC+04:00)', -240, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (87,'Middle East Standard Time', -2, 'Beirut', '(UTC+02:00)', -120, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-03-30 00:00:00.000', '2008-10-26 00:00:00.000', '2009-03-29 00:00:00.000', '2009-10-25 00:00:00.000', '2010-03-28 00:00:00.000', '2010-10-31 00:00:00.000', '2011-03-27 00:00:00.000', '2011-10-30 00:00:00.000', '2012-03-25 00:00:00.000', '2012-10-28 00:00:00.000', '2013-03-31 00:00:00.000', '2013-10-27 03:00:00.000', '2014-03-30 00:00:00.000', '2014-10-26 03:00:00.000', '2015-03-29 00:00:00.000', '2015-10-25 03:00:00.000', '2016-03-27 00:00:00.000', '2016-10-30 03:00:00.000', '2017-03-26 00:00:00.000', '2017-10-29 03:00:00.000')
insert into gen_timezone_s values (88,'Montevideo Standard Time', 3, 'Montevideo', '(UTC-03:00)', 180, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-10-05 02:00:00.000', '2009-03-08 02:00:00.000', '2009-10-04 02:00:00.000', '2010-03-14 02:00:00.000', '2010-10-03 02:00:00.000', '2011-03-13 02:00:00.000', '2011-10-02 02:00:00.000', '2012-03-11 02:00:00.000', '2012-10-07 02:00:00.000', '2013-03-10 02:00:00.000', '2013-10-06 02:00:00.000', '2014-03-09 02:00:00.000', '2014-10-05 02:00:00.000', '2015-03-08 02:00:00.000', '2015-10-04 02:00:00.000', '2016-03-13 02:00:00.000', '2016-10-02 02:00:00.000', '2017-03-12 02:00:00.000', '2017-10-01 02:00:00.000', '2018-03-11 02:00:00.000')
insert into gen_timezone_s values (89,'Mountain Standard Time (Mexico)', 7, 'Chihuahua, La Paz, Mazatlan', '(UTC-07:00)', 420, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-04-06 02:00:00.000', '2008-10-26 02:00:00.000', '2009-04-05 02:00:00.000', '2009-10-25 02:00:00.000', '2010-04-05 02:00:00.000', '2010-10-31 02:00:00.000', '2011-04-03 02:00:00.000', '2011-10-30 02:00:00.000', '2012-04-01 02:00:00.000', '2012-10-28 02:00:00.000', '2013-04-07 02:00:00.000', '2013-10-27 02:00:00.000', '2014-04-06 02:00:00.000', '2014-10-26 02:00:00.000', '2015-04-05 02:00:00.000', '2015-10-25 02:00:00.000', '2016-04-03 02:00:00.000', '2016-10-30 02:00:00.000', '2016-03-06 02:00:00.000', '2016-10-30 02:00:00.000')
insert into gen_timezone_s values (90,'Namibia Standard Time', -1, 'Windhoek', '(UTC+01:00)', -60, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-09-07 02:00:00.000', '2009-04-05 02:00:00.000', '2009-09-06 02:00:00.000', '2010-04-04 02:00:00.000', '2010-09-05 02:00:00.000', '2011-04-03 02:00:00.000', '2011-09-04 02:00:00.000', '2012-04-01 02:00:00.000', '2012-09-02 02:00:00.000', '2013-04-07 02:00:00.000', '2013-09-01 02:00:00.000', '2014-04-06 02:00:00.000', '2014-09-07 02:00:00.000', '2015-04-05 02:00:00.000', '2015-09-06 02:00:00.000', '2016-04-03 02:00:00.000', '2016-09-04 02:00:00.000', '2017-04-02 02:00:00.000', '2017-09-03 02:00:00.000', '2018-04-01 02:00:00.000')
insert into gen_timezone_s values (91,'Pacific Standard Time (Mexico)', 8, 'Baja California', '(UTC-08:00)', 480, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-04-06 02:00:00.000', '2008-10-26 02:00:00.000', '2009-04-05 02:00:00.000', '2009-10-25 02:00:00.000', '2010-03-14 02:00:00.000', '2010-11-07 02:00:00.000', '2011-03-13 02:00:00.000', '2011-11-06 02:00:00.000', '2012-03-11 02:00:00.000', '2012-11-04 02:00:00.000', '2013-03-10 02:00:00.000', '2013-11-03 02:00:00.000', '2014-03-09 02:00:00.000', '2014-11-02 02:00:00.000', '2015-03-08 02:00:00.000', '2015-11-01 02:00:00.000', '2016-03-13 02:00:00.000', '2016-11-06 02:00:00.000', '2017-03-12 02:00:00.000', '2017-11-05 02:00:00.000')
insert into gen_timezone_s values (92,'Pakistan Standard Time', -5, 'Islamabad, Karachi', '(UTC+05:00)', -300, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (93,'Paraguay Standard Time', 4, 'Asuncion', '(UTC-04:00)', 240, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-10-19 00:00:00.000', '2009-03-08 00:00:00.000', '2009-10-18 00:00:00.000', '2010-04-11 00:00:00.000', '2010-10-03 00:00:00.000', '2011-04-10 00:00:00.000', '2011-10-02 00:00:00.000', '2012-04-08 00:00:00.000', '2012-10-07 00:00:00.000', '2013-03-24 00:00:00.000', '2013-10-06 00:00:00.000', '2014-04-13 00:00:00.000', '2014-10-05 00:00:00.000', '2015-04-12 00:00:00.000', '2015-10-04 00:00:00.000', '2016-04-10 00:00:00.000', '2016-10-02 00:00:00.000', '2017-04-09 00:00:00.000', '2017-10-01 00:00:00.000', '2018-04-08 00:00:00.000')
insert into gen_timezone_s values (94,'Syria Standard Time', -2, 'Damascus', '(UTC+02:00)', 120, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-04-04 00:00:00.000', '2008-11-01 00:00:00.000', '2009-03-27 00:00:00.000', '2009-10-30 00:00:00.000', '2010-04-02 00:00:00.000', '2010-10-29 00:00:00.000', '2011-04-01 00:00:00.000', '2011-10-28 00:00:00.000', '2012-03-30 00:00:00.000', '2012-10-26 00:00:00.000', '2013-03-29 00:00:00.000', '2013-11-01 00:00:00.000', '2014-03-28 00:00:00.000', '2014-10-31 00:00:00.000', '2015-03-27 00:00:00.000', '2015-10-30 00:00:00.000', '2016-03-25 00:00:00.000', '2016-10-28 00:00:00.000', '2017-03-31 00:00:00.000', '2017-10-27 00:00:00.000')
insert into gen_timezone_s values (95,'Turkey Standard Time', -2, 'Istanbul', '(UTC+02:00)', 120, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-03-30 03:00:00.000', '2008-10-26 04:00:00.000', '2009-03-29 03:00:00.000', '2009-10-25 04:00:00.000', '2010-03-28 03:00:00.000', '2010-10-31 04:00:00.000', '2011-03-28 03:00:00.000', '2011-10-30 04:00:00.000', '2012-03-25 03:00:00.000', '2012-10-28 04:00:00.000', '2013-03-31 03:00:00.000', '2013-10-27 04:00:00.000', '2014-03-30 03:00:00.000', '2014-10-26 04:00:00.000', '2015-03-29 03:00:00.000', '2015-10-25 04:00:00.000', '2016-03-27 03:00:00.000', '2016-10-30 04:00:00.000', '2017-03-26 03:00:00.000', '2017-10-29 04:00:00.000')
insert into gen_timezone_s values (96,'Ulaanbaatar Standard Time', -8, 'Ulaanbaatar', '(UTC+08:00)', -480, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (97,'UTC', 0, 'Coordinated Universal Time', '(UTC)', 0, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (98,'UTC+12', -12, 'Coordinated Universal Time +12', '(UTC+12:00)', -600, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (99,'UTC-11', 11, 'Coordinated Universal Time -11', '(UTC-11:00)', 540, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (100,'UTC-2', 2, 'Coordinated Universal Time -02', '(UTC-02:00)', 120, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (101,'Venezuela Standard Time', 4.5, 'Caracas', '(UTC-04:30)', 270, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000')
insert into gen_timezone_s values (102,'Morocco Standard Time', 0, 'Casablanca', '(UTC)', 0, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2008-06-01 00:00:00.000', '2008-09-01 00:00:00.000', '2009-06-01 00:00:00.000', '2009-08-21 00:00:00.000', '2010-05-02 00:00:00.000', '2010-08-08 00:00:00.000', '2011-04-03 00:00:00.000', '2011-07-31 00:00:00.000', '2012-04-29 02:00:00.000', '2012-09-30 03:00:00.000', '2013-04-28 02:00:00.000', '2013-09-29 03:00:00.000', '2014-04-27 02:00:00.000', '2014-09-28 03:00:00.000', '2015-04-26 02:00:00.000', '2013-09-27 03:00:00.000', '2016-04-24 02:00:00.000', '2016-09-25 03:00:00.000', '2017-04-30 02:00:00.000', '2017-09-24 03:00:00.000')

update gen_timezone_s set timezonediff = replace(timezonediff,'GMT','UTC') 

/* ************** FB 2856 - Ends  01 Aug 2013 ******************** */

/* ************** FB 2972 - Start 14 Aug 2013 ******************** */

update usr_roles_d set roleName = 'VNOC Operator' where level=3 and createType=1 and crossaccess = 1

/* ************** FB 2972 - End 14 Aug 2013 ******************** */

/* ************** FB 2681 - Start 19 Aug 2013 ******************** */

ALTER TABLE org_settings_d
ALTER COLUMN FooterMessage nvarchar(max) NULL 

/* ************** FB 2681 - End 19 Aug 2013 ******************** */

/* ************** FB 2718 Starts 21 Aug 2013********************************/
/* --After complete full fleged TMS will uncomment this
	
insert into Mcu_Vendor_S (id,name,BridgeInterfaceId,videoparticipants,audioparticipants) values(15,'Cisco TMS',8,50,50)


INSERT INTO MCU_Params_D([BridgeName],[BridgeTypeid],[ConfLockUnLock],[confMessage],[confAudioTx],[confAudioRx],[confVideoTx],[confVideoRx]
           ,[confLayout],[confCamera],[confPacketloss],[confRecord],[partyBandwidth],[partySetfocus],[partyMessage],[partyAudioTx],[partyAudioRx],[partyVideoTx]
           ,[partyVideoRx],[partyLayout],[partyCamera],[partyPacketloss],[partyImagestream],[partyLockUnLock],[partyRecord],[partyLecturemode],[confMuteAllExcept]
           ,[confUnmuteAll],[partyLeader])
     VALUES
           ('Cisco TMS',15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
           
  */
  
/* ************** FB 2718 Ends 21 Aug 2013********************************/

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.2 Begins                             */
/*                                                                                              */
/* ******************************************************************************************** */

/* **************  FB 3002- START  26AUG2013 ******************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.License_Defaults_D ADD
	LastModifiedUser int NULL
GO
COMMIT

/* **************  FB 3002- END  26AUG2013 ******************** */

/* **************  FB 2973 - START  27AUG2013 ******************** */

update Usr_Roles_D set roleMenuMask='8*248-4*15+8*255+5*31+2*3+8*189+2*3+2*3+2*3+1*0-6*14' where level=3 and crossaccess = 1 and createType = 1

update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid

update Usr_Inactive_D set Usr_Inactive_D.menumask = b.rolemenumask from usr_roles_d as b where Usr_Inactive_D.roleid  = b.roleid

/* **************  FB 2973 - END  27AUG2013 ******************** */

/* **************  FB 3017 - START  27AUG2013 ******************** */

Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (50,'Vidyo HD 110',11)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (51,'Vidyo HD 220',11)
update Gen_VideoEquipment_S set Familyid = 12 where VEid = 24

/* **************  FB 3017 - END  27AUG2013 ******************** */

/* ************************* FB 3001 27Aug 2013 Starts ************************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	BrokerRoomNum nvarchar(50) NULL
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	BrokerRoomNum nvarchar(50) NULL
GO
COMMIT


Update Usr_List_D set BrokerRoomNum=''
Update Usr_Inactive_D set BrokerRoomNum=''
/* ************************* FB 3001 27 Aug 2013 Ends ************************** */

/* ********************* Public City State Data Starts ************************* */

--Public_ CountryVocabulary
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Public_CountryDataVocabulary]') AND type in (N'U'))
begin
Delete FROM [dbo].[Public_CountryDataVocabulary]
DROP TABLE [dbo].[Public_CountryDataVocabulary]
end;
CREATE TABLE [dbo].[Public_CountryDataVocabulary](
	[countryID] [int] NOT NULL,
	[countryName] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Public_CountryDataVocabulary] PRIMARY KEY CLUSTERED 
(
	[countryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = 

ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[Public_CountryDataVocabulary]
           ([countryID]
           ,[countryName])
     VALUES
          (1, 'Australia'),(2, 'New Zealand'),(3, 'USA'),(82, 'Pakistan'),(160, 'Argentina'),(161, 'Austria'),(162, 'Barbados'),(163, 'Belgium'),
(164, 'Bolivia'),(165, 'Brazil'),(167, 'Bulgaria'),(168, 'Canada'),(171, 'Chile'),(172, 'China'),(173, 'Colombia'),(175, 'Croatia'),(176, 'Cyprus'),
(177, 'Czech Republic'),(178, 'Denmark'),(179, 'Egypt'),(180, 'Estonia'),(181, 'Fiji'),(182, 'Finland'),(183, 'France'),(184, 'French Guiana'),(185, 'French Polynesia'),
(186, 'Germany'),(187, 'Greece'),(188, 'Guadeloupe'),(189, 'Hungary'),(190, 'India'),(191, 'Indonesia'),(192, 'Ireland, Republic of'),(193, 'Israel'),
(194, 'Italy'),(195, 'Jamaica'),(196, 'Japan'),(197, 'Jordan'),(198, 'Luxembourg'),(199, 'Macedonia'),(200, 'Malaysia'),(201, 'Malta'),(202, 'Martinique'),
(203, 'Mauritius'),(204, 'Morocco'),(205, 'Namibia'),(206, 'Netherlands'),(207, 'Norway'),(208, 'Oman'),(209, 'Panama'),(210, 'Peru'),(211, 'Philippines'),
(212, 'Poland'),(213, 'Portugal'),(214, 'Puerto Rico'),(215, 'Republic of San Marino'),(216, 'La Reunion'),(217, 'Romania'),(218, 'Russian Federation'),
(219, 'Saudi Arabia'),(220, 'Singapore'),(221, 'South Africa'),(222, 'Korea, Republic of'),(223, 'Spain'),(224, 'Sri Lanka'),(225, 'Sweden'),(226, 'Switzerland'),
(227, 'Taiwan'),(228, 'Thailand'),(230, 'Turkey'),(231, 'United Arab Emirates'),(232, 'United Kingdom'),(233, 'Uruguay'),(234, 'Venezuela'),(237, 'Vietnam'),
(238, 'Qatar'),(240, 'Uganda'),(241, 'Trinidad & Tobago'),(242, 'Lebanon'),(243, 'Kuwait'),(244, 'Bangladesh'),(245, 'Tanzania'),(246, 'Ireland, Northern'),
(247, 'Scotland'),(248, 'England'),(249, 'Wales'),(250, 'Bahrain'),(251, 'Guatemala'),(253, 'Slovakia'),(254, 'Tunisia'),(255, 'Ukraine'),(256, 'Dominican Republic'),
(257, 'Latvia'),(258, 'Mexico'),(259, 'Ghana'),(260, 'Botswana'),(261, 'Papua New Guinea'),(262, 'Cambodia'),(263, 'Afghanistan'),(264, 'Mozambique'),
(266, 'Iran'),(267, 'Iraq'),(268, 'Yemen'),(270, 'Syria'),(271, 'Kazakhstan'),(272, 'Mongolia'),(273, 'Costa Rica'),(274, 'Nigeria'),(275, 'Lithuania'),
(276, 'Gibraltar'),(277, 'Bermuda'),(278, 'Bosnia-Herzegovina'),(279, 'Albania'),(280, 'Saint Kitts and Nevis'),(281, 'Maldives'),(282, 'Serbia'),(283, 'Kenya'),
(284, 'Cote d''Ivoire'),(285, 'Monaco'),(286, 'US Virgin Islands'),(287, 'French West Indies'),(288, 'Slovenia'),(289, 'Zimbabwe'),(290, 'Sudan'),
(291, 'Ethiopia'),(292, 'Burkino Faso'),(293, 'Senegal'),(294, 'Mauritania'),(295, 'Ecuador'),(297, 'Netherlands Antilles'),(298, 'Saint Lucia'),(299, 'Palestinian Authority'),
(300, 'Timor Leste'),(301, 'Georgia'),(302, 'Kosovo'),(303, 'Armenia'),(304, 'Azerbaijan'),(305, 'Madagascar'),(306, 'Nicaragua'),(307, 'Benin'),(308, 'Isle of Man'),
(309, 'Cayman Islands'),(310, 'El Salvador'),(312, 'Moldova'),(313, 'Channel Islands'),(314, 'Liechtenstein'),(316, 'Brunei Darussalam'),(317, 'Iceland'),(318, 'Mali'),
(319, 'Cameroon'),(321, 'Libya'),(323, 'Bahamas'),(324, 'Isle of Wight'),(325, 'Nepal'),(326, 'Belarus'),(327, 'Sierra Leone'),(328, 'Zambia'),(329, 'Vanuatu')
GO

--- Public_ CountryVocabulary2005
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Public_CountryDataVocabulary]') AND type in (N'U'))
begin
Delete FROM [dbo].[Public_CountryDataVocabulary]
DROP TABLE [dbo].[Public_CountryDataVocabulary]
end;
CREATE TABLE [dbo].[Public_CountryDataVocabulary](
	[countryID] [int] NOT NULL,
	[countryName] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Public_CountryDataVocabulary] PRIMARY KEY CLUSTERED 
(
	[countryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = 

ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
          (1, 'Australia')
INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(2, 'New Zealand')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(3, 'USA')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(82, 'Pakistan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(160, 'Argentina')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(161, 'Austria')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(162, 'Barbados')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(163, 'Belgium')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(164, 'Bolivia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(165, 'Brazil')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(167, 'Bulgaria')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(168, 'Canada')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(171, 'Chile')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(172, 'China')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(173, 'Colombia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(175, 'Croatia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(176, 'Cyprus')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(177, 'Czech Republic')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(178, 'Denmark')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(179, 'Egypt')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(180, 'Estonia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(181, 'Fiji')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(182, 'Finland')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(183, 'France')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(184, 'French Guiana')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(185, 'French Polynesia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(186, 'Germany')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(187, 'Greece')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(188, 'Guadeloupe')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(189, 'Hungary')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(190, 'India')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(191, 'Indonesia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(192, 'Ireland, Republic of')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(193, 'Israel')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(194, 'Italy')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(195, 'Jamaica')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(196, 'Japan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(197, 'Jordan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(198, 'Luxembourg')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(199, 'Macedonia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(200, 'Malaysia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(201, 'Malta')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(202, 'Martinique')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(203, 'Mauritius')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(204, 'Morocco')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(205, 'Namibia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(206, 'Netherlands')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(207, 'Norway')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(208, 'Oman')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(209, 'Panama')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(210, 'Peru')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(211, 'Philippines')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(212, 'Poland')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(213, 'Portugal')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(214, 'Puerto Rico')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(215, 'Republic of San Marino')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(216, 'La Reunion')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(217, 'Romania')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(218, 'Russian Federation')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(219, 'Saudi Arabia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(220, 'Singapore')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(221, 'South Africa')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(222, 'Korea, Republic of')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(223, 'Spain')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(224, 'Sri Lanka')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(225, 'Sweden')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(226, 'Switzerland')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(227, 'Taiwan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(228, 'Thailand')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(230, 'Turkey')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(231, 'United Arab Emirates')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(232, 'United Kingdom')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(233, 'Uruguay')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(234, 'Venezuela')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(237, 'Vietnam')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(238, 'Qatar')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(240, 'Uganda')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(241, 'Trinidad & Tobago')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(242, 'Lebanon')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(243, 'Kuwait')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(244, 'Bangladesh')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(245, 'Tanzania')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(246, 'Ireland, Northern')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(247, 'Scotland')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(248, 'England')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(249, 'Wales')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(250, 'Bahrain')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(251, 'Guatemala')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(253, 'Slovakia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(254, 'Tunisia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(255, 'Ukraine')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(256, 'Dominican Republic')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(257, 'Latvia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(258, 'Mexico')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(259, 'Ghana')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(260, 'Botswana')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(261, 'Papua New Guinea')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(262, 'Cambodia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(263, 'Afghanistan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(264, 'Mozambique')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(266, 'Iran')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(267, 'Iraq')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(268, 'Yemen')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(270, 'Syria')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(271, 'Kazakhstan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(272, 'Mongolia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(273, 'Costa Rica')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(274, 'Nigeria')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(275, 'Lithuania')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(276, 'Gibraltar')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(277, 'Bermuda')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(278, 'Bosnia-Herzegovina')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(279, 'Albania')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(280, 'Saint Kitts and Nevis')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(281, 'Maldives')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(282, 'Serbia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(283, 'Kenya')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(284, 'Cote d''Ivoire')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(285, 'Monaco')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(286, 'US Virgin Islands')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(287, 'French West Indies')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(288, 'Slovenia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(289, 'Zimbabwe')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(290, 'Sudan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(291, 'Ethiopia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(292, 'Burkino Faso')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(293, 'Senegal')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(294, 'Mauritania')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(295, 'Ecuador')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES                                                     	
(297, 'Netherlands Antilles')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(298, 'Saint Lucia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(299, 'Palestinian Authority')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(300, 'Timor Leste')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(301, 'Georgia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(302, 'Kosovo')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(303, 'Armenia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(304, 'Azerbaijan')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(305, 'Madagascar')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(306, 'Nicaragua')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(307, 'Benin')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(308, 'Isle of Man')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(309, 'Cayman Islands')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(310, 'El Salvador')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(312, 'Moldova')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(313, 'Channel Islands')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(314, 'Liechtenstein')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(316, 'Brunei Darussalam')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(317, 'Iceland')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(318, 'Mali')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(319, 'Cameroon')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(321, 'Libya')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES                                                      	
(323, 'Bahamas')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(324, 'Isle of Wight')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(325, 'Nepal')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(326, 'Belarus')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(327, 'Sierra Leone')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(328, 'Zambia')INSERT INTO [dbo].[Public_CountryDataVocabulary]([countryID],[countryName]) VALUES
(329, 'Vanuatu')

--Public_ StateCountryVocabulary

/*drop table [dbo].[Public_StateCountryDataVocabulary] */
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Public_StateCountryDataVocabulary]') AND type in (N'U'))
begin
Delete FROM [dbo].[Public_StateCountryDataVocabulary]
DROP TABLE [dbo].[Public_StateCountryDataVocabulary]
end;

CREATE TABLE [dbo].[Public_StateCountryDataVocabulary](
	[stateID] [int] NOT NULL,
	[stateName] [nvarchar](150) NOT NULL,
	[countryID] [int] NOT NULL,
 CONSTRAINT [PK_Public_StateCountryDataVocabulary] PRIMARY KEY CLUSTERED 
(
	[stateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  =  
ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[Public_StateCountryDataVocabulary]
           ([stateID]
           ,[stateName]
	     ,[countryID])
     VALUES


( 1, 'New South Wales', 1),( 2, 'Queensland', 1),( 3, 'South Australia', 1),( 4, 'Northern Territory', 1),( 5, 'Western Australia', 1),
( 6, 'Australian Capital Territory', 1),( 7, 'Victoria', 1),( 8, 'Tasmania', 1),( 9, 'North Island', 2),( 10, 'South Island', 2),
( 657, 'Buenos Aires', 160),( 660, 'San Isidro', 160),( 661, 'Feldkirch', 161),( 662, 'Innsbruck', 161),( 663, 'Klagenfurt', 161),
( 664, 'Linz', 161),( 665, 'Salzburg', 161),( 667, 'Wieu', 161),( 669, 'St Michael', 162),( 670, 'Antwerp', 163),( 671, 'Antwerp (Aartselaar)', 163),
( 672, 'Brussels', 163),( 673, 'Charleroi', 163),( 674, 'Vilvoorde', 163),( 675, 'La Paz', 164),( 676, 'Santa Cruz', 164),( 677, 'Belo Horizonte', 165),
( 678, 'Brasilia', 165),( 679, 'Curitiba', 165),( 680, 'Porto Alegre', 165),( 681, 'Rio De Janeiro', 165),( 682, 'Salvador', 165),( 683, 'Sao Paulo', 165),
( 684, 'Vitoria', 165),( 686, 'Sofia', 167),( 696, 'Alberta', 168),( 697, 'British Columbia', 168),( 698, 'New Brunswick', 168),( 699, 'Northwest Territories', 168),
( 700, 'Nova Scotia', 168),( 701, 'Ontario', 168),( 702, 'Yukon Territory', 168),( 704, 'Alderney', 232),( 705, 'Guernsey', 232),( 706, 'Santiago', 171),
( 707, 'Beijing', 172),( 708, 'Hong Kong', 172),( 709, 'Kowloon', 172),( 711, 'Bogota', 173),( 712, 'Cali', 173),( 713, 'Medellin', 173),( 715, 'Sv. Nedjecja', 175),
( 716, 'Zagreb', 175),( 717, 'Nicosia', 176),( 718, 'Jablonec Nad Nisou', 177),( 719, 'Prague', 177),( 720, 'Ballerup', 178),( 722, 'Farum', 178),( 723, 'Cairo', 179),
( 724, 'Giza', 179),( 725, 'Tallinn', 180),( 726, 'Suva', 181),( 727, 'Helsinki', 182),( 728, 'Turku', 182),( 729, 'Agen', 183),( 730, 'Ajaccio', 183),( 731, 'Albi', 183),
( 732, 'Annonay', 183),( 733, 'Auch', 183),( 734, 'Aurillac', 183),( 735, 'Bastia', 183),( 736, 'Belfort', 183),( 737, 'Besancon', 183),( 738, 'Bordeaux', 183),( 739, 'Bordeaux Centre', 183),
( 740, 'Bourges', 183),( 741, 'Brive', 183),( 742, 'Caen', 183),( 743, 'Castres', 183),( 744, 'Clermont-Ferrand', 183),( 746, 'Colmar', 183),( 747, 'Dijon', 183),( 748, 'Grenoble', 183),
( 750, 'Guyanne Cayenne', 183),( 751, 'Guyanne St Laurent', 183),( 752, 'Jonzac', 183),( 754, 'La Reunion St Denis', 183),( 755, 'La Reunion St Pierre', 183),( 757, 'Labege Cedex', 183),
( 758, 'Les Herbiers', 183),( 759, 'Libourne', 183),( 760, 'Lille', 183),( 762, 'Limoges', 183),( 763, 'Loudun (Nord Poitiers)', 183),( 765, 'Marseille', 183),( 766, 'Montpellier', 183),
( 767, 'Nancy', 183),( 768, 'Nantes', 183),( 769, 'Nice', 183),( 771, 'Nice-Antibes', 183),( 772, 'Orsay', 183),( 773, 'Paris', 183),( 774, 'Paris - La Defense', 183),( 775, 'Paris - Les Halles', 183),
( 776, 'Paris 17e', 183),( 777, 'Paris Boulogne Pont de Sevre', 183),( 778, 'Paris Boulogne Porte de St Cloud', 183),( 779, 'Paris Est Porte de Montreuil', 183),( 780, 'Paris Montparnasse 14e', 183),
( 781, 'Paris Suresnes', 183),( 782, 'Pau', 183),( 783, 'Poitiers', 183),( 784, 'Quimper', 183),( 785, 'Reims', 183),( 786, 'Rennes', 183),( 787, 'Rouen', 183),( 788, 'Rouen (Mont Saint Aignan)', 183),
( 789, 'St Brieux', 183),( 790, 'St Malo', 183),( 791, 'Strabourg Sud', 183),( 792, 'Strasbourg', 183),( 793, 'Toulouse', 183),( 794, 'Toulouse center', 183),( 795, 'Toulouse Labege', 183),
( 796, 'Tours', 183),( 797, 'Valence', 183),( 798, 'Vannes', 183),( 799, 'Vouneuil Sous Biard (East Poitiers)', 183),( 800, 'Cayenne', 184),( 801, 'Tahiti Papeete', 185),( 803, 'Berlin', 186),
( 804, 'Berlin - Chalottenstrasse', 186),( 805, 'Berlin-Mitte', 186),( 806, 'Bonn', 186),( 807, 'Bremen', 186),( 808, 'Cologne', 186),( 809, 'Dortmund', 186),( 810, 'Dresden', 186),( 811, 'Dusseldorf', 186),
( 812, 'Erfurt', 186),( 813, 'Eschborn', 186),( 814, 'Frankfurt', 186),( 817, 'Freiburg', 186),( 818, 'Halle', 186),( 819, 'Hamburg', 186),( 820, 'Hannover', 186),( 822, 'Koln', 186),
( 823, 'Leipzig', 186),( 824, 'Mannheim', 186),( 826, 'Munich', 186),( 827, 'Neckarslum', 186),( 828, 'Nuremberg', 186),( 830, 'Saarbrucken', 186),( 831, 'Stuttgart', 186),( 832, 'Wiesbaden', 186),
( 833, 'Zella-Mehlis', 186),( 834, 'Athens', 187),( 835, 'Thessaloniki', 187),( 836, 'Pointe A Pitre', 188),( 837, 'Budapest', 189),( 838, 'Bangalore', 190),( 839, 'Calcutta', 190),
( 840, 'Chennai', 190),( 841, 'Hyderabad', 190),( 842, 'Mumbai', 190),( 843, 'New Delhi', 190),( 844, 'Noida', 190),( 845, 'Pune', 190),( 846, 'Bali', 191),( 847, 'Jakarta', 191),
( 848, 'Cork', 192),( 850, 'Galway', 192),( 851, 'Limerick', 192),( 852, 'Barak', 193),( 853, 'Eshel Hanassi', 193),( 854, 'Jerusalem', 193),( 855, 'Petach Tikva', 193),( 856, 'Rosh Ha-Ayin', 193),
( 857, 'Tel Aviv', 193),( 858, 'Bologna', 194),( 859, 'Catanzaro', 194),( 860, 'Florence', 194),( 861, 'Genoa', 194),( 862, 'Milan', 194),( 864, 'Naples', 194),( 865, 'Padova', 194),( 866, 'Rome', 194),
( 867, 'Salerno', 194),( 868, 'Torino', 194),( 869, 'Trieste', 194),( 870, 'Venice', 194),( 872, 'Osaka', 196),( 873, 'Tokyo', 196),( 874, 'Amman', 197),( 875, 'Luxembourg', 198),( 876, 'Skopje', 199),
( 877, 'Kuala Lumpur', 200),( 878, 'Subang Jaya Selangor', 200),( 879, 'Floriana', 201),( 880, 'Fort-de-France', 202),( 882, 'Port Louis', 203),( 883, 'Mohammedia', 204),( 884, 'Windhoek', 205),
( 887, 'Breda', 206),( 889, 'Groningen', 206),( 894, 'Oslo', 207),( 895, 'Stavanger', 207),( 896, 'Troms', 207),( 897, 'Trondheim', 207),( 898, 'Muscat', 208),( 899, 'Toronto', 168),
( 900, 'Karachi', 82),( 901, 'Lahore', 82),( 902, 'Panama', 209),( 903, 'Lima', 210),( 904, 'Pasig City', 211),( 905, 'Poznan', 212),( 906, 'Warsaw', 212),( 907, 'Lisbon', 213),( 908, 'Porto', 213),
( 909, 'San Juan', 214),( 910, 'Rovereta', 215),( 911, 'Saint-Denis', 216),( 912, 'Bucharest', 217),( 913, 'Moscow', 218),( 914, 'Saint Petersburg', 218),( 916, 'Riyadh', 219),( 917, 'Singapore', 220),
( 938, 'Seoul', 222),( 939, 'Barcelona', 223),( 940, 'Bilbao', 223),( 941, 'Castellon', 223),( 942, 'Granada', 223),( 943, 'Ibiza', 223),( 944, 'Las Palmas', 223),( 946, 'Malaga', 223),( 947, 'Orense', 223),
( 948, 'Palma De Mallorca', 223),( 949, 'Pontevedra', 223),( 950, 'Rocafort', 223),( 951, 'Sabadell', 223),( 952, 'Santa Cruz De Tenerife', 223),( 953, 'Santander', 223),( 954, 'Valencia', 223),( 955, 'Vigo', 223),
( 956, 'Zaragoza', 223),( 957, 'Colombo', 224),( 958, 'Fargelanda', 225),( 959, 'Haparanda', 225),( 960, 'Kalmar', 225),( 961, 'Stockholm', 225),( 962, 'Uppsala', 225),( 963, 'Agno', 226),
( 964, 'Basel', 226),( 965, 'Geneva', 226),( 966, 'Lucerne (Ebikon)', 226),( 967, 'Tagelswangen', 226),( 968, 'Zurich', 226),( 969, 'Kaohsiung', 227),( 970, 'Tainan', 227),( 971, 'Taipei', 227),
( 972, 'Bangkok', 228),( 973, 'Pattaya', 228),( 975, 'Istanbul', 230),( 976, 'Izmir', 230),( 977, 'Abu Dhabi', 231),( 978, 'Dubai', 231),( 979, 'Aberdeen', 232),( 980, 'Aberystwyth', 232),( 981, 'Ascot', 232),
( 982, 'Barnsley', 232),( 983, 'Bedford', 232),( 985, 'Birmingham', 232),( 986, 'Bottisham (Cambridge)', 232),( 988, 'Bournemouth', 232),( 989, 'Bracknell', 232),( 990, 'Branston', 232),( 991, 'Brecon', 232),
( 992, 'Brentford', 232),( 993, 'Brighton', 232),( 994, 'Bristol', 232),( 995, 'Bungay', 232),( 996, 'Burton upon Trent', 232),( 997, 'Caernarfon', 232),( 998, 'Cambridgeshire', 248),( 999, 'Canterbury', 232),
( 1001, 'Cardiff', 232),( 1002, 'Carlisle', 232),( 1004, 'Castle Donnington', 232),( 1005, 'Cheadle', 232),( 1006, 'Chelmsford', 232),( 1007, 'Cheltenham', 232),( 1008, 'Chertsey', 232),( 1009, 'Chester', 232),
( 1010, 'Chippenham', 232),( 1011, 'Colchester', 232),( 1012, 'Coventry', 232),( 1014, 'Croydon', 232),( 1016, 'Dartford', 232),( 1017, 'Doncaster', 232),( 1018, 'Dover', 232),
( 1019, 'Dundee', 232),( 1020, 'Dunfermline', 232),( 1021, 'Ebbow Vale', 232),( 1023, 'Exeter', 232),( 1024, 'Falmouth', 232),( 1025, 'Fetcham', 232),( 1026, 'Fleet', 232),( 1027, 'Gatwick Airport', 232),
( 1028, 'Glasgow', 232),( 1030, 'Guildford', 232),( 1031, 'Hammersmith', 232),( 1032, 'Harrow', 232),( 1033, 'Haverfordwest', 232),( 1034, 'Heathrow', 232),( 1035, 'Hengoed', 232),( 1036, 'High Wycombe', 232),
( 1037, 'Hove', 232),( 1038, 'Inverness', 232),( 1039, 'Kettering', 232),( 1040, 'Lampeter', 232),( 1041, 'Lancaster', 232),( 1042, 'Leamington Spa', 232),( 1043, 'Leatherhead', 232),( 1044, 'Leeds', 232),
( 1045, 'Leicester', 232),( 1046, 'Liverpool', 232),( 1047, 'Llandrindod Wells', 232),( 1048, 'Llangefni', 232),( 1050, 'Luton', 232),( 1051, 'Maidenhead', 232),( 1052, 'Manchester', 232),( 1053, 'Manchester (Sale)', 232),
( 1054, 'Manchester Airport', 232),( 1055, 'Mendlesham', 232),( 1056, 'Middlesborough', 232),( 1057, 'Milton Keynes', 232),( 1058, 'Newcastle', 232),( 1059, 'Newport', 232),( 1061, 'Norwich', 232),
( 1063, 'Okehampton', 232),( 1064, 'Omagh', 232),( 1065, 'Orpington', 232),( 1067, 'Pembroke', 232),( 1068, 'Plymouth', 232),( 1069, 'Pontypool', 232),( 1070, 'Poole', 232),( 1071, 'Reading', 232),( 1072, 'Reigate', 232),
( 1073, 'Rickmansworth', 232),( 1074, 'Rosyth (Dunfermline)', 232),( 1075, 'Rotherham', 232),( 1076, 'Rugby', 232),( 1077, 'Selby', 232),( 1078, 'Sheffield', 232),( 1079, 'Slough', 232),( 1080, 'Southampton (Fareham)', 232),
( 1081, 'St. Albans', 232),( 1083, 'Stirling', 232),( 1084, 'Stockton-on-Tees', 232),( 1085, 'Sudbrooke', 232),( 1086, 'Sunbury on Thames', 232),( 1087, 'Sunderland', 232),( 1088, 'Swindon', 232),( 1089, 'Taunton', 232),
( 1090, 'Thurso', 232),( 1091, 'Warrington', 232),( 1092, 'Watford', 232),( 1093, 'Welshpool', 232),( 1094, 'Wigan', 232),( 1095, 'Witney', 232),( 1096, 'Woking', 232),( 1097, 'Montevideo', 233),( 1098, 'Caracas', 234),
( 1102, 'Mpumalanga', 221),( 1108, 'Alabama', 3),( 1109, 'Alaska', 3),( 1110, 'American Samoa', 3),( 1111, 'Arizona', 3),( 1112, 'Arkansas', 3),( 1113, 'California', 3),( 1114, 'Colorado', 3),( 1115, 'Connecticut', 3),
( 1116, 'Delaware', 3),( 1117, 'District Of Columbia', 3),( 1118, 'Federated States of Micronesia', 3),( 1119, 'Florida', 3),( 1120, 'Georgia', 3),( 1121, 'Guam', 3),( 1122, 'Hawaii', 3),( 1123, 'Idaho', 3),( 1124, 'Illinois', 3),
( 1125, 'Indiana', 3),( 1126, 'Iowa', 3),( 1127, 'Kansas', 3),( 1128, 'Kentucky', 3),( 1129, 'Louisiana', 3),( 1130, 'Maine', 3),( 1131, 'Marshall Islands', 3),( 1132, 'Maryland', 3),( 1133, 'Massachusetts', 3),( 1134, 'Michigan', 3),
( 1135, 'Minnesota', 3),( 1136, 'Mississippi', 3),( 1137, 'Missouri', 3),( 1138, 'Montana', 3),( 1139, 'Nebraska', 3),( 1140, 'Nevada', 3),( 1141, 'New Hampshire', 3),( 1142, 'New Jersey', 3),( 1143, 'New Mexico', 3),( 1144, 'New York', 3),
( 1145, 'North Carolina', 3),( 1146, 'North Dakota', 3),( 1147, 'Northern Mariana Islands', 3),( 1148, 'Ohio', 3),( 1149, 'Oklahoma', 3),( 1150, 'Oregon', 3),( 1151, 'Palau', 3),( 1152, 'Pennsylvania', 3),( 1153, 'Puerto Rico', 3),( 1154, 'Rhode Island', 3),
( 1155, 'South Carolina', 3),( 1156, 'South Dakota', 3),( 1157, 'Tennessee', 3),( 1158, 'Texas', 3),( 1159, 'Utah', 3),( 1160, 'Vermont', 3),( 1161, 'Virgin Islands', 3),( 1162, 'Virginia', 3),( 1163, 'Washington', 3),( 1164, 'West Virginia', 3),
( 1165, 'Wisconsin', 3),( 1166, 'Wyoming', 3),( 1167, 'Hanoi', 237),( 1170, 'Shibuya', 196),( 1171, 'Lyon', 183),( 1172, 'Kyoto', 196),( 1173, 'Danang', 237),( 1175, 'Solihull', 232),( 1176, 'Manila', 211),( 1181, 'Doha', 238),( 1183, 'Kampala', 240),( 1187, 'Friesland', 206),
( 1188, 'Drenthe', 206),( 1189, 'Overijssel', 206),( 1190, 'Flevoland', 206),( 1192, 'Utrecht', 206),( 1195, 'Zeeland', 206),( 1197, 'Limburg', 206),( 1198, 'Gelderland', 206),( 1205, 'Trinidad', 241),( 1206, 'Tobago', 241),( 1209, 'Beirut', 242),( 1210, 'Safat', 243),
( 1211, 'Kuwait City', 243),( 1212, 'Bitola', 199),( 1216, 'Kyushu', 196),( 1218, 'British Virgin Islands', 232),( 1219, 'Dhaka', 244),( 1220, 'Phuket', 228),( 1221, 'Bath', 232),( 1223, 'Dar Es Salaam', 245),( 1224, 'Derby', 232),( 1226, 'Scotland', 247),( 1228, 'South West', 248),
( 1230, 'London', 248),( 1231, 'North', 248),( 1232, 'North East', 248),( 1234, 'East Midlands', 248),( 1235, 'West Midlands', 248),( 1238, 'Wales', 249),( 1239, 'Bahrain', 250),( 1241, 'Guatemala', 251),( 1243, 'Tunisia', 254),( 1251, 'Sydney', 1),( 1253, 'Bergen', 207),( 1256, 'Newfoundland', 168),
( 1257, 'Santo Domingo', 256),( 1258, 'Marne La Vallee', 183),( 1259, 'Gundelfingen', 186),( 1260, 'Mainz', 186),( 1261, 'Walldorf', 186),( 1262, 'Azur', 193),( 1263, 'Riga', 257),( 1264, 'Guadalajara', 258),( 1265, 'Mexico City', 258),( 1268, 'Monterrey', 258),( 1271, 'Puebla', 258),
( 1272, 'Tijuana', 258),( 1273, 'Amsterdam', 206),( 1274, 'Eindhoven', 206),( 1275, 'Rotterdam', 206),( 1278, 'Makati City', 211),( 1280, 'Bratislava', 253),( 1281, 'Marbella', 223),( 1282, 'Gothenburg', 225),( 1283, 'Malmo', 225),( 1284, 'Solna', 225),( 1285, 'Zug', 226),
( 1286, 'Kiev', 255),( 1287, 'Shenzhen', 172),( 1289, 'Gurgaon', 190),( 1290, 'Yokohama', 196),( 1292, 'Belfast', 246),( 1296, 'Edinburgh', 247),( 1303, 'Mid Wales', 249),( 1305, 'Northern Scotland', 247),( 1307, 'South West Wales', 249),( 1308, 'Southern Scotland', 247),
( 1313, 'West Wales', 249),( 1314, 'Yorkshire', 248),( 1315, 'Accra', 259),( 1409, 'Noord-Brabant', 206),( 1477, 'Arnhem', 206),( 1571, 'Brabant', 206),( 1588, 'Nuevo Leon', 256),( 1599, 'Schiphol-Rijk', 206),( 1609, 'Catalunya', 223),( 1645, 'Noord-Holland', 206),( 1653, 'Nove Mesto', 177),
( 1654, 'Zuid-Holland', 206),( 1664, 'Nordrhein-Westfalen', 186),( 1665, 'Bavaria', 186),( 1669, 'Hesse', 186),( 1670, 'Baden-Wuerttemberg', 186),( 1684, 'Hellerup', 178),( 1691, 'Sweden', 225),( 1695, 'Lower-Saxony', 186),( 1701, 'Guangdong', 172),( 1703, 'Karnataka', 190),( 1704, 'Haryana', 190),
( 1706, 'Kanagawa', 196),( 1707, 'Neuilly', 183),( 1709, 'Attica', 187),( 1726, 'Las Condes', 171),( 1751, 'Vlaams Brabant', 163),( 1759, 'East of England', 248),( 1768, 'Bruxelles-Capitale', 163),( 1968, 'Hilversum', 206),( 1987, 'Vienna', 161),( 2013, 'Nordic', 182),( 2032, 'The Hague', 206),
( 2043, 'Madrid', 223),( 2110, 'Espoo', 182),( 2124, 'Quebec', 168),( 2153, 'Copenhagen', 178),( 2168, 'Mazowieckie', 212),( 2250, 'Shanghai', 172),( 2252, 'Limassol', 176),( 2261, 'Brabant Wallon', 163),( 2273, 'Dalian', 172),( 2285, 'Guangzhou', 172),( 2299, 'Tianjin', 172),( 2302, 'Andhra Pradesh', 190),
( 2304, 'Gujarat', 190),( 2306, 'Kerala', 190),( 2307, 'Maharashtra', 190),( 2308, 'Madhya Pradesh', 190),( 2309, 'Punjab', 190),( 2310, 'Rajasthan', 190),( 2311, 'Tamil Nadu', 190),( 2312, 'Uttar Pradesh', 190),( 2313, 'West Bengal', 190),( 2314, 'Gaborone', 260),( 2316, 'Goa', 190),( 2318, 'Saskatchewan', 168),
( 2319, 'Manitoba', 168),( 2320, 'Port Moresby', 261),( 2321, 'Lautoka', 181),( 2322, 'Phnom Penh', 262),( 2324, 'Bihar', 190),( 2326, 'Krakow', 212),( 2327, 'Maputo', 264),( 2328, 'Kathmandu', 325),( 2330, 'Avignon', 183),( 2331, 'The Highlands', 247),( 2332, 'The Midland Valley', 247),( 2333, 'The Southern Uplands', 247),
( 2336, 'North Wales', 249),( 2337, 'South Wales', 249),( 2338, 'Ceredigion', 249),( 2341, 'Shannon West', 192),( 2342, 'Jersey', 232),( 2343, 'Islamabad', 82),( 2345, 'San Jose', 273),( 2346, 'Lagos', 274),( 2347, 'Vilnius', 275),( 2348, 'Gibraltar', 276),( 2349, 'Medan', 191),( 2350, 'Great Yarmouth', 232),
( 2351, 'Bermuda', 277),( 2352, 'Hamilton', 277),( 2353, 'Orissa', 190),( 2356, 'Sarajevo', 278),( 2358, 'Ankara', 230),( 2359, 'Ho Chi Minh City', 237),( 2360, 'Tirana', 279),( 2361, 'Basseterre', 280),( 2362, 'North Male Atoll', 281),( 2363, 'Free State', 221),( 2366, 'Aarhus', 178),( 2367, 'Mahanagar Extension', 190),
( 2368, 'Makassar', 191),( 2369, 'Cosenza', 194),( 2370, 'Damascus', 270),( 2371, 'Catania', 194),( 2372, 'Estillac', 183),( 2373, 'Treviso', 194),( 2374, 'Ancona', 194),( 2375, 'Caserta', 194),( 2376, 'Imola', 194),( 2377, 'Reggio', 194),( 2378, 'Emilia', 194),( 2379, 'Udine', 194),( 2380, 'Vicenza', 194),( 2382, 'Belgrade', 282),
( 2383, 'Nagoya', 196),( 2384, 'Fukuoka', 196),( 2385, 'Sabah', 200),( 2386, 'Kedah', 200),( 2387, 'Penang', 200),( 2388, 'Johor', 200),( 2389, 'Perak', 200),( 2390, 'Melaka', 200),( 2391, 'Sarawak', 200),( 2392, 'Pahang', 200),( 2393, 'Kelantan', 200),( 2394, 'Terengganu', 200),( 2395, 'Selangor', 200),( 2396, 'Negeri Sembilan', 200),
( 2397, 'Wilayah Persekutuan', 200),( 2398, 'Perlis', 200),( 2399, 'Macau', 172),( 2400, 'Graz', 161),( 2401, 'Visakhapatnam', 190),( 2403, 'Kabul', 263),( 2404, 'Alicante', 223),( 2405, 'Nairobi', 283),( 2406, 'Northern Cape', 221),( 2407, 'North West', 221),( 2408, 'Gauteng', 221),( 2409, 'Limpopo', 221),( 2411, 'KwaZulu - Natal', 221),
( 2413, 'Eastern Cape', 221),( 2415, 'Western Cape', 221),( 2417, 'Cagliari', 194),( 2418, 'Bari', 194),( 2419, 'Delhi', 190),( 2420, 'Abidjan', 284),( 2421, 'Monte Carlo', 285),( 2422, 'St. Thomas', 286),( 2423, 'Angers', 183),( 2424, 'Casablanca', 204),( 2425, 'Guadeloupe', 287),( 2426, 'Chihuahua', 258),( 2427, 'Heidelburg', 186),
( 2428, 'Flanders', 163),( 2429, 'Wallonie', 163),( 2430, 'East Anglia', 232),( 2431, 'Odense', 178),( 2433, 'Sichuan Province', 172),( 2434, 'Ljubljana', 288),( 2435, 'Quetta', 82),( 2436, 'Harare', 289),( 2437, 'Lausanne', 226),( 2438, 'Jeddah', 219),( 2439, 'Khartoum', 290),( 2440, 'Addis Ababa', 291),( 2441, 'Ouagadougou', 292),
( 2442, 'Aix en Provence', 183),( 2443, 'Dakar', 293),( 2444, 'Nouakchott', 294),( 2445, 'Quito', 295),( 2446, 'Ireland, Northern', 232),( 2447, 'Bedfordshire', 248),( 2448, 'Berkshire', 248),( 2449, 'Cheshire', 248),( 2450, 'Cornwall', 248),( 2451, 'Cumberland', 248),( 2452, 'Derbyshire', 248),( 2453, 'Devon', 248),( 2454, 'Dorset', 248),
( 2455, 'Durham', 248),( 2456, 'Essex', 248),( 2457, 'Gloucestershire', 248),( 2458, 'Hampshire', 248),( 2459, 'Herefordshire', 248),( 2460, 'Hertfordshire', 248),( 2461, 'Huntingdonshire', 248),( 2462, 'Kent', 248),( 2463, 'Lancashire', 248),( 2464, 'Leicestershire', 248),( 2465, 'Lincolnshire', 248),( 2467, 'Middlesex', 248),( 2468, 'Norfolk', 248),
( 2469, 'Northamptonshire', 248),( 2470, 'Northumberland', 248),( 2471, 'Nottinghamshire', 248),( 2472, 'Oxfordshire', 248),( 2473, 'Rutland', 248),( 2474, 'Shropshire', 248),( 2475, 'Somerset', 248),( 2476, 'Staffordshire', 248),( 2477, 'Suffolk', 248),( 2478, 'Surrey', 248),( 2479, 'Sussex', 248),( 2480, 'Warwickshire', 248),( 2481, 'Westmorland', 248),
( 2482, 'Wiltshire', 248),( 2483, 'Worcestershire', 248),( 2485, 'Buckinghamshire', 248),( 2489, 'Curacao', 297),( 2490, 'La Reunion St Clotide', 183),( 2491, 'St Clotide', 216),( 2493, 'Castries Quarter', 298),( 2494, 'Teheran', 266),( 2495, 'Maastricht', 206),( 2497, 'Pondicherry', 190),( 2498, 'Gaza', 193),( 2499, 'Birzeit', 299),( 2501, 'Delft', 206),
( 2502, 'Cluj - Napoca', 217),( 2503, 'Kaunas', 275),( 2504, 'Cumbria', 248),( 2505, 'Berne', 226),( 2506, 'Peshawar', 82),( 2507, 'Rawalpindi', 82),( 2508, 'Sialkot', 82),( 2509, 'Chengdu', 172),( 2510, 'Hangzhou', 172),( 2511, 'Dili', 300),( 2512, 'Tbilisi', 301),( 2513, 'Pristina', 302),( 2514, 'Yerevan', 303),( 2515, 'Baku', 304),( 2516, 'Antananarivo', 305),
( 2517, 'Chandigarh', 190),( 2518, 'Plimmerton', 2),( 2519, 'Eskisehir', 230),( 2520, 'Prince Edward Island', 168),( 2522, 'Managua', 306),( 2523, 'Cotonou', 307),( 2524, 'Douglas', 308),( 2525, 'Cayman Islands', 309),( 2526, 'Jharkhand', 190),( 2527, 'Mongolia', 272),( 2528, 'San Salvador', 310),( 2530, 'Lugano', 226),( 2531, 'Christ Church', 162),( 2532, 'Ticino', 226),
( 2533, 'Almere', 206),( 2534, 'Nijmegen', 206),( 2535, 'Den Bosch', 206),( 2536, 'Chisinau', 312),( 2537, 'Alexandria', 179),( 2538, 'Jaipur', 190),( 2542, 'Wanganui', 2),( 2543, 'Sofia Antipolis', 183),( 2544, 'Valbonne', 183),( 2545, 'Channel Islands', 313),( 2547, 'Nunavut', 168),( 2549, 'Zwolle', 206),( 2550, 'Triesen', 314),( 2551, 'Austerlitz', 206),
( 2552, 'Jalisco', 258),( 2553, 'Sudbury', 168),( 2554, 'Ebene', 203),( 2555, 'Amstelveen', 206),( 2557, 'Bielefeld', 186),( 2558, 'Esslingen', 186),( 2561, 'Muenster', 186),( 2564, 'Brunei', 316),( 2565, 'Maugio', 183),( 2566, 'Oulu', 182),( 2567, 'Lappeenranta', 182),( 2568, 'Jyvaskyla', 182),( 2569, 'Kuopio', 182),( 2570, 'Vantaa', 182),( 2571, 'Tampere', 182),
( 2572, 'Mabolo', 211),( 2573, 'Almaty', 271),( 2574, 'Astan', 271),( 2577, 'Reykjavik', 317),( 2578, 'Qingdao', 172),( 2579, 'Bamako', 318),( 2580, 'Yaounde', 319),( 2581, 'Vanuatu', 329),( 2582, 'Elst', 206),( 2583, 'Schiedam', 206),( 2584, 'Bolivar', 173),( 2585, 'Tripoli', 321),( 2586, 'Vaslui', 217),( 2587, 'Metz', 183),( 2588, 'Gdansk', 212),( 2589, 'Jiangsu Province', 172),
( 2590, 'Fujian Province', 172),( 2591, 'Hunan Province', 172),( 2592, 'Leeuwarden', 206),( 2593, 'Yurakucho', 196),( 2595, 'Mulhouse', 183),( 2596, 'San Jorge', 164),( 2597, 'Abuja', 274),( 2598, 'Port Harcourt', 274),( 2599, 'Issy-les-Moulineaux', 183),( 2600, 'Nassau', 323),( 2601, 'Cowes', 324),( 2602, 'Ahmedabad', 190),( 2603, 'Lalitpur', 325),( 2604, 'Modena', 194),( 2606, 'Minsk', 326),
( 2607, 'Nanan District', 172),( 2608, 'Freetown', 327),( 2609, 'Brno', 177),( 2610, 'Gent', 163),( 2611, 'Winterthur', 226),( 2612, 'Eysins', 226),( 2615, 'Madeira', 213),( 2616, 'Rijswijk', 206),( 2617, 'Lusaka', 328),( 2618, 'Cannes', 183),( 2619, 'Sendai', 196),( 2620, 'Hiroshima', 196),( 2621, 'Kobe', 196),( 2622, 'Hyogo', 196),( 2624, 'Recife', 165),( 2625, 'Pernambuco', 165),
( 2626, 'Amersfoort', 206),( 2663, 'Anbar Province', 267),( 2666, 'Arbil Province', 267),( 2667, 'Babylon Province', 267),( 2668, 'Baghdad Province', 267),( 2669, 'Basra Province', 267),( 2664, 'Muthanna Province', 267),( 2665, 'Qadisiyyah Province', 267),( 2670, 'Dhi Qar Province', 267),( 2671, 'Diyala Province', 267),( 2672, 'Dohuk Province', 267),( 2673, 'Karbala Province', 267),
( 2674, 'Kirkuk Province', 267),( 2675, 'Maysan Province', 267),( 2676, 'Najaf Province', 267),( 2677, 'Nineveh Province', 267),( 2678, 'Saladin Province', 267),( 2679, 'Sulaymaniyah Province', 267),( 2680, 'Wasit Province', 267),( 2697, 'Abyan', 268),( 2682, 'Ad-Dali''', 268),( 2696, 'Aden', 268),( 2683, 'Al-Bayda''', 268),( 2684, 'Al-Hudaydah', 268),( 2685, 'Al-Jawf', 268),( 2698, 'Al-Mahrah', 268),
( 2686, 'Al-Mahwit', 268),( 2687, 'Amanat Al Asimah', 268),( 2681, 'Amran', 268),( 2688, 'Dhamar', 268),( 2699, 'Hadramaut', 268),( 2689, 'Hajjah', 268),( 2690, 'Ibb', 268),( 2700, 'Lahij', 268),( 2691, 'Ma''rib', 268),( 2692, 'Raymah', 268),( 2693, 'Sa''dah', 268),( 2694, 'Sana''a', 268),( 2701, 'Shabwah', 268),( 2695, 'Taiz', 268)
--Public_ StateCountryVocabulary2005

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Public_StateCountryDataVocabulary]') AND type in (N'U'))
begin
Delete FROM [dbo].[Public_StateCountryDataVocabulary]
DROP TABLE [dbo].[Public_StateCountryDataVocabulary]
end;

CREATE TABLE [dbo].[Public_StateCountryDataVocabulary](
	[stateID] [int] NOT NULL,
	[stateName] [nvarchar](150) NOT NULL,
	[countryID] [int] NOT NULL,
 CONSTRAINT [PK_Public_StateCountryDataVocabulary] PRIMARY KEY CLUSTERED 
(
	[stateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  =  
ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES


( 1, 'New South Wales', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2, 'Queensland', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 3, 'South Australia', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 4, 'Northern Territory', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 5, 'Western Australia', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 6, 'Australian Capital Territory', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 7, 'Victoria', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 8, 'Tasmania', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 9, 'North Island', 2)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 10, 'South Island', 2)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 657, 'Buenos Aires', 160)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 660, 'San Isidro', 160)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 661, 'Feldkirch', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 662, 'Innsbruck', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 663, 'Klagenfurt', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 664, 'Linz', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 665, 'Salzburg', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 667, 'Wieu', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 669, 'St Michael', 162)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 670, 'Antwerp', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 671, 'Antwerp (Aartselaar)', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 672, 'Brussels', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 673, 'Charleroi', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 674, 'Vilvoorde', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 675, 'La Paz', 164)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 676, 'Santa Cruz', 164)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 677, 'Belo Horizonte', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 678, 'Brasilia', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 679, 'Curitiba', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 680, 'Porto Alegre', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 681, 'Rio De Janeiro', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 682, 'Salvador', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 683, 'Sao Paulo', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 684, 'Vitoria', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 686, 'Sofia', 167)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 696, 'Alberta', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 697, 'British Columbia', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 698, 'New Brunswick', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 699, 'Northwest Territories', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 700, 'Nova Scotia', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 701, 'Ontario', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 702, 'Yukon Territory', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 704, 'Alderney', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 705, 'Guernsey', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 706, 'Santiago', 171)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 707, 'Beijing', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 708, 'Hong Kong', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 709, 'Kowloon', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 711, 'Bogota', 173)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 712, 'Cali', 173)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 713, 'Medellin', 173)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 715, 'Sv. Nedjecja', 175)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 716, 'Zagreb', 175)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 717, 'Nicosia', 176)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 718, 'Jablonec Nad Nisou', 177)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 719, 'Prague', 177)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 720, 'Ballerup', 178)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 722, 'Farum', 178)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 723, 'Cairo', 179)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 724, 'Giza', 179)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 725, 'Tallinn', 180)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 726, 'Suva', 181)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 727, 'Helsinki', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 728, 'Turku', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 729, 'Agen', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 730, 'Ajaccio', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 731, 'Albi', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 732, 'Annonay', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 733, 'Auch', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 734, 'Aurillac', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 735, 'Bastia', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 736, 'Belfort', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 737, 'Besancon', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 738, 'Bordeaux', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 739, 'Bordeaux Centre', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 740, 'Bourges', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 741, 'Brive', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 742, 'Caen', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 743, 'Castres', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 744, 'Clermont-Ferrand', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 746, 'Colmar', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 747, 'Dijon', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 748, 'Grenoble', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 750, 'Guyanne Cayenne', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 751, 'Guyanne St Laurent', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 752, 'Jonzac', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 754, 'La Reunion St Denis', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 755, 'La Reunion St Pierre', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 757, 'Labege Cedex', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 758, 'Les Herbiers', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 759, 'Libourne', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 760, 'Lille', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 762, 'Limoges', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 763, 'Loudun (Nord Poitiers)', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 765, 'Marseille', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 766, 'Montpellier', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 767, 'Nancy', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 768, 'Nantes', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 769, 'Nice', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 771, 'Nice-Antibes', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 772, 'Orsay', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 773, 'Paris', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 774, 'Paris - La Defense', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 775, 'Paris - Les Halles', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 776, 'Paris 17e', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 777, 'Paris Boulogne Pont de Sevre', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 778, 'Paris Boulogne Porte de St Cloud', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 779, 'Paris Est Porte de Montreuil', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 780, 'Paris Montparnasse 14e', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 781, 'Paris Suresnes', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 782, 'Pau', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 783, 'Poitiers', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 784, 'Quimper', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 785, 'Reims', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 786, 'Rennes', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 787, 'Rouen', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 788, 'Rouen (Mont Saint Aignan)', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 789, 'St Brieux', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 790, 'St Malo', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 791, 'Strabourg Sud', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 792, 'Strasbourg', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 793, 'Toulouse', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 794, 'Toulouse center', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 795, 'Toulouse Labege', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 796, 'Tours', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 797, 'Valence', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 798, 'Vannes', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 799, 'Vouneuil Sous Biard (East Poitiers)', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 800, 'Cayenne', 184)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 801, 'Tahiti Papeete', 185)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 803, 'Berlin', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 804, 'Berlin - Chalottenstrasse', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 805, 'Berlin-Mitte', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 806, 'Bonn', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 807, 'Bremen', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 808, 'Cologne', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 809, 'Dortmund', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 810, 'Dresden', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 811, 'Dusseldorf', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 812, 'Erfurt', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 813, 'Eschborn', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 814, 'Frankfurt', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 817, 'Freiburg', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 818, 'Halle', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 819, 'Hamburg', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 820, 'Hannover', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 822, 'Koln', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 823, 'Leipzig', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 824, 'Mannheim', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 826, 'Munich', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 827, 'Neckarslum', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 828, 'Nuremberg', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 830, 'Saarbrucken', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 831, 'Stuttgart', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 832, 'Wiesbaden', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 833, 'Zella-Mehlis', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 834, 'Athens', 187)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 835, 'Thessaloniki', 187)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 836, 'Pointe A Pitre', 188)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 837, 'Budapest', 189)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 838, 'Bangalore', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 839, 'Calcutta', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 840, 'Chennai', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 841, 'Hyderabad', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 842, 'Mumbai', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 843, 'New Delhi', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 844, 'Noida', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 845, 'Pune', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 846, 'Bali', 191)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 847, 'Jakarta', 191)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 848, 'Cork', 192)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 850, 'Galway', 192)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 851, 'Limerick', 192)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 852, 'Barak', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 853, 'Eshel Hanassi', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 854, 'Jerusalem', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 855, 'Petach Tikva', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 856, 'Rosh Ha-Ayin', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 857, 'Tel Aviv', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 858, 'Bologna', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 859, 'Catanzaro', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 860, 'Florence', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 861, 'Genoa', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 862, 'Milan', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 864, 'Naples', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 865, 'Padova', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 866, 'Rome', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 867, 'Salerno', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 868, 'Torino', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 869, 'Trieste', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 870, 'Venice', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 872, 'Osaka', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 873, 'Tokyo', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 874, 'Amman', 197)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 875, 'Luxembourg', 198)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 876, 'Skopje', 199)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 877, 'Kuala Lumpur', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 878, 'Subang Jaya Selangor', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 879, 'Floriana', 201)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 880, 'Fort-de-France', 202)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 882, 'Port Louis', 203)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 883, 'Mohammedia', 204)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 884, 'Windhoek', 205)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 887, 'Breda', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 889, 'Groningen', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 894, 'Oslo', 207)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 895, 'Stavanger', 207)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 896, 'Troms', 207)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 897, 'Trondheim', 207)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 898, 'Muscat', 208)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 899, 'Toronto', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 900, 'Karachi', 82)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 901, 'Lahore', 82)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 902, 'Panama', 209)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 903, 'Lima', 210)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 904, 'Pasig City', 211)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 905, 'Poznan', 212)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 906, 'Warsaw', 212)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 907, 'Lisbon', 213)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 908, 'Porto', 213)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 909, 'San Juan', 214)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 910, 'Rovereta', 215)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 911, 'Saint-Denis', 216)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 912, 'Bucharest', 217)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 913, 'Moscow', 218)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 914, 'Saint Petersburg', 218)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 916, 'Riyadh', 219)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 917, 'Singapore', 220)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 938, 'Seoul', 222)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 939, 'Barcelona', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 940, 'Bilbao', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 941, 'Castellon', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 942, 'Granada', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 943, 'Ibiza', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 944, 'Las Palmas', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 946, 'Malaga', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 947, 'Orense', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 948, 'Palma De Mallorca', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 949, 'Pontevedra', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 950, 'Rocafort', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 951, 'Sabadell', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 952, 'Santa Cruz De Tenerife', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 953, 'Santander', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 954, 'Valencia', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 955, 'Vigo', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 956, 'Zaragoza', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 957, 'Colombo', 224)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 958, 'Fargelanda', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 959, 'Haparanda', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 960, 'Kalmar', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 961, 'Stockholm', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 962, 'Uppsala', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 963, 'Agno', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 964, 'Basel', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 965, 'Geneva', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 966, 'Lucerne (Ebikon)', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 967, 'Tagelswangen', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 968, 'Zurich', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 969, 'Kaohsiung', 227)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 970, 'Tainan', 227)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 971, 'Taipei', 227)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 972, 'Bangkok', 228)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 973, 'Pattaya', 228)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 975, 'Istanbul', 230)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 976, 'Izmir', 230)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 977, 'Abu Dhabi', 231)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 978, 'Dubai', 231)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 979, 'Aberdeen', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 980, 'Aberystwyth', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 981, 'Ascot', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 982, 'Barnsley', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 983, 'Bedford', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 985, 'Birmingham', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 986, 'Bottisham (Cambridge)', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 988, 'Bournemouth', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 989, 'Bracknell', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 990, 'Branston', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 991, 'Brecon', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 992, 'Brentford', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 993, 'Brighton', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 994, 'Bristol', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 995, 'Bungay', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 996, 'Burton upon Trent', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 997, 'Caernarfon', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 998, 'Cambridgeshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 999, 'Canterbury', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1001, 'Cardiff', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1002, 'Carlisle', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1004, 'Castle Donnington', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1005, 'Cheadle', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1006, 'Chelmsford', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1007, 'Cheltenham', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1008, 'Chertsey', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1009, 'Chester', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1010, 'Chippenham', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1011, 'Colchester', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1012, 'Coventry', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1014, 'Croydon', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1016, 'Dartford', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1017, 'Doncaster', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1018, 'Dover', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1019, 'Dundee', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1020, 'Dunfermline', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1021, 'Ebbow Vale', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1023, 'Exeter', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1024, 'Falmouth', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1025, 'Fetcham', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1026, 'Fleet', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1027, 'Gatwick Airport', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1028, 'Glasgow', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1030, 'Guildford', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1031, 'Hammersmith', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1032, 'Harrow', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1033, 'Haverfordwest', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1034, 'Heathrow', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1035, 'Hengoed', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1036, 'High Wycombe', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1037, 'Hove', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1038, 'Inverness', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1039, 'Kettering', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1040, 'Lampeter', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1041, 'Lancaster', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1042, 'Leamington Spa', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1043, 'Leatherhead', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1044, 'Leeds', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1045, 'Leicester', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1046, 'Liverpool', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1047, 'Llandrindod Wells', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1048, 'Llangefni', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1050, 'Luton', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1051, 'Maidenhead', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1052, 'Manchester', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1053, 'Manchester (Sale)', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1054, 'Manchester Airport', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1055, 'Mendlesham', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1056, 'Middlesborough', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1057, 'Milton Keynes', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1058, 'Newcastle', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1059, 'Newport', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1061, 'Norwich', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1063, 'Okehampton', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1064, 'Omagh', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1065, 'Orpington', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1067, 'Pembroke', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1068, 'Plymouth', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1069, 'Pontypool', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1070, 'Poole', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1071, 'Reading', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1072, 'Reigate', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1073, 'Rickmansworth', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1074, 'Rosyth (Dunfermline)', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1075, 'Rotherham', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1076, 'Rugby', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1077, 'Selby', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1078, 'Sheffield', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1079, 'Slough', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1080, 'Southampton (Fareham)', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1081, 'St. Albans', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1083, 'Stirling', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1084, 'Stockton-on-Tees', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1085, 'Sudbrooke', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1086, 'Sunbury on Thames', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1087, 'Sunderland', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1088, 'Swindon', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1089, 'Taunton', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1090, 'Thurso', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1091, 'Warrington', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1092, 'Watford', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1093, 'Welshpool', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1094, 'Wigan', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1095, 'Witney', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1096, 'Woking', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1097, 'Montevideo', 233)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1098, 'Caracas', 234)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1102, 'Mpumalanga', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1108, 'Alabama', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1109, 'Alaska', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1110, 'American Samoa', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1111, 'Arizona', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1112, 'Arkansas', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1113, 'California', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1114, 'Colorado', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1115, 'Connecticut', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1116, 'Delaware', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1117, 'District Of Columbia', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1118, 'Federated States of Micronesia', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1119, 'Florida', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1120, 'Georgia', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1121, 'Guam', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1122, 'Hawaii', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1123, 'Idaho', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1124, 'Illinois', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1125, 'Indiana', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1126, 'Iowa', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1127, 'Kansas', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1128, 'Kentucky', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1129, 'Louisiana', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1130, 'Maine', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1131, 'Marshall Islands', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1132, 'Maryland', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1133, 'Massachusetts', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1134, 'Michigan', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1135, 'Minnesota', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1136, 'Mississippi', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1137, 'Missouri', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1138, 'Montana', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1139, 'Nebraska', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1140, 'Nevada', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1141, 'New Hampshire', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1142, 'New Jersey', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1143, 'New Mexico', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1144, 'New York', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1145, 'North Carolina', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1146, 'North Dakota', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1147, 'Northern Mariana Islands', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1148, 'Ohio', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1149, 'Oklahoma', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1150, 'Oregon', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1151, 'Palau', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1152, 'Pennsylvania', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1153, 'Puerto Rico', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1154, 'Rhode Island', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1155, 'South Carolina', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1156, 'South Dakota', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1157, 'Tennessee', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1158, 'Texas', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1159, 'Utah', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1160, 'Vermont', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1161, 'Virgin Islands', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1162, 'Virginia', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1163, 'Washington', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1164, 'West Virginia', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1165, 'Wisconsin', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1166, 'Wyoming', 3)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1167, 'Hanoi', 237)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1170, 'Shibuya', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1171, 'Lyon', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1172, 'Kyoto', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1173, 'Danang', 237)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1175, 'Solihull', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1176, 'Manila', 211)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1181, 'Doha', 238)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1183, 'Kampala', 240)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1187, 'Friesland', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1188, 'Drenthe', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1189, 'Overijssel', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1190, 'Flevoland', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1192, 'Utrecht', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1195, 'Zeeland', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1197, 'Limburg', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1198, 'Gelderland', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1205, 'Trinidad', 241)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1206, 'Tobago', 241)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1209, 'Beirut', 242)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1210, 'Safat', 243)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1211, 'Kuwait City', 243)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1212, 'Bitola', 199)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1216, 'Kyushu', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1218, 'British Virgin Islands', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1219, 'Dhaka', 244)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1220, 'Phuket', 228)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1221, 'Bath', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1223, 'Dar Es Salaam', 245)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1224, 'Derby', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1226, 'Scotland', 247)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1228, 'South West', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1230, 'London', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1231, 'North', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1232, 'North East', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1234, 'East Midlands', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1235, 'West Midlands', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1238, 'Wales', 249)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1239, 'Bahrain', 250)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1241, 'Guatemala', 251)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1243, 'Tunisia', 254)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1251, 'Sydney', 1)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1253, 'Bergen', 207)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1256, 'Newfoundland', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1257, 'Santo Domingo', 256)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1258, 'Marne La Vallee', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1259, 'Gundelfingen', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1260, 'Mainz', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1261, 'Walldorf', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1262, 'Azur', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1263, 'Riga', 257)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1264, 'Guadalajara', 258)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1265, 'Mexico City', 258)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1268, 'Monterrey', 258)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1271, 'Puebla', 258)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1272, 'Tijuana', 258)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1273, 'Amsterdam', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1274, 'Eindhoven', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1275, 'Rotterdam', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1278, 'Makati City', 211)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1280, 'Bratislava', 253)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1281, 'Marbella', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1282, 'Gothenburg', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1283, 'Malmo', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1284, 'Solna', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1285, 'Zug', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1286, 'Kiev', 255)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1287, 'Shenzhen', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1289, 'Gurgaon', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1290, 'Yokohama', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1292, 'Belfast', 246)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1296, 'Edinburgh', 247)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1303, 'Mid Wales', 249)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1305, 'Northern Scotland', 247)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1307, 'South West Wales', 249)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1308, 'Southern Scotland', 247)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1313, 'West Wales', 249)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1314, 'Yorkshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1315, 'Accra', 259)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1409, 'Noord-Brabant', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1477, 'Arnhem', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1571, 'Brabant', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1588, 'Nuevo Leon', 256)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1599, 'Schiphol-Rijk', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1609, 'Catalunya', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1645, 'Noord-Holland', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1653, 'Nove Mesto', 177)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1654, 'Zuid-Holland', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1664, 'Nordrhein-Westfalen', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1665, 'Bavaria', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1669, 'Hesse', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1670, 'Baden-Wuerttemberg', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1684, 'Hellerup', 178)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1691, 'Sweden', 225)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1695, 'Lower-Saxony', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1701, 'Guangdong', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1703, 'Karnataka', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1704, 'Haryana', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1706, 'Kanagawa', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1707, 'Neuilly', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1709, 'Attica', 187)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1726, 'Las Condes', 171)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1751, 'Vlaams Brabant', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1759, 'East of England', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1768, 'Bruxelles-Capitale', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1968, 'Hilversum', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 1987, 'Vienna', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2013, 'Nordic', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2032, 'The Hague', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2043, 'Madrid', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2110, 'Espoo', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2124, 'Quebec', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2153, 'Copenhagen', 178)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2168, 'Mazowieckie', 212)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2250, 'Shanghai', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2252, 'Limassol', 176)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2261, 'Brabant Wallon', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2273, 'Dalian', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2285, 'Guangzhou', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2299, 'Tianjin', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2302, 'Andhra Pradesh', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2304, 'Gujarat', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2306, 'Kerala', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2307, 'Maharashtra', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2308, 'Madhya Pradesh', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2309, 'Punjab', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2310, 'Rajasthan', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2311, 'Tamil Nadu', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2312, 'Uttar Pradesh', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2313, 'West Bengal', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2314, 'Gaborone', 260)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2316, 'Goa', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2318, 'Saskatchewan', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2319, 'Manitoba', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2320, 'Port Moresby', 261)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2321, 'Lautoka', 181)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2322, 'Phnom Penh', 262)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2324, 'Bihar', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2326, 'Krakow', 212)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2327, 'Maputo', 264)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2328, 'Kathmandu', 325)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2330, 'Avignon', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2331, 'The Highlands', 247)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2332, 'The Midland Valley', 247)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2333, 'The Southern Uplands', 247)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2336, 'North Wales', 249)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2337, 'South Wales', 249)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2338, 'Ceredigion', 249)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2341, 'Shannon West', 192)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2342, 'Jersey', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2343, 'Islamabad', 82)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2345, 'San Jose', 273)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2346, 'Lagos', 274)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2347, 'Vilnius', 275)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2348, 'Gibraltar', 276)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2349, 'Medan', 191)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2350, 'Great Yarmouth', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2351, 'Bermuda', 277)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2352, 'Hamilton', 277)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2353, 'Orissa', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2356, 'Sarajevo', 278)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2358, 'Ankara', 230)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2359, 'Ho Chi Minh City', 237)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2360, 'Tirana', 279)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2361, 'Basseterre', 280)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2362, 'North Male Atoll', 281)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2363, 'Free State', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2366, 'Aarhus', 178)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2367, 'Mahanagar Extension', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2368, 'Makassar', 191)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2369, 'Cosenza', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2370, 'Damascus', 270)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2371, 'Catania', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2372, 'Estillac', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2373, 'Treviso', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2374, 'Ancona', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2375, 'Caserta', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2376, 'Imola', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2377, 'Reggio', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2378, 'Emilia', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2379, 'Udine', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2380, 'Vicenza', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2382, 'Belgrade', 282)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2383, 'Nagoya', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2384, 'Fukuoka', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2385, 'Sabah', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2386, 'Kedah', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2387, 'Penang', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2388, 'Johor', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2389, 'Perak', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2390, 'Melaka', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2391, 'Sarawak', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2392, 'Pahang', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2393, 'Kelantan', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2394, 'Terengganu', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2395, 'Selangor', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2396, 'Negeri Sembilan', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2397, 'Wilayah Persekutuan', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2398, 'Perlis', 200)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2399, 'Macau', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2400, 'Graz', 161)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2401, 'Visakhapatnam', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2403, 'Kabul', 263)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2404, 'Alicante', 223)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2405, 'Nairobi', 283)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2406, 'Northern Cape', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2407, 'North West', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2408, 'Gauteng', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2409, 'Limpopo', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2411, 'KwaZulu - Natal', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2413, 'Eastern Cape', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2415, 'Western Cape', 221)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2417, 'Cagliari', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2418, 'Bari', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2419, 'Delhi', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2420, 'Abidjan', 284)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2421, 'Monte Carlo', 285)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2422, 'St. Thomas', 286)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2423, 'Angers', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2424, 'Casablanca', 204)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2425, 'Guadeloupe', 287)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2426, 'Chihuahua', 258)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2427, 'Heidelburg', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2428, 'Flanders', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2429, 'Wallonie', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2430, 'East Anglia', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2431, 'Odense', 178)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2433, 'Sichuan Province', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2434, 'Ljubljana', 288)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2435, 'Quetta', 82)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2436, 'Harare', 289)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2437, 'Lausanne', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2438, 'Jeddah', 219)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2439, 'Khartoum', 290)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2440, 'Addis Ababa', 291)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2441, 'Ouagadougou', 292)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2442, 'Aix en Provence', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2443, 'Dakar', 293)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2444, 'Nouakchott', 294)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2445, 'Quito', 295)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2446, 'Ireland, Northern', 232)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2447, 'Bedfordshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2448, 'Berkshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2449, 'Cheshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2450, 'Cornwall', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2451, 'Cumberland', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2452, 'Derbyshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2453, 'Devon', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2454, 'Dorset', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2455, 'Durham', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2456, 'Essex', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2457, 'Gloucestershire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2458, 'Hampshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2459, 'Herefordshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2460, 'Hertfordshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2461, 'Huntingdonshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2462, 'Kent', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2463, 'Lancashire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2464, 'Leicestershire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2465, 'Lincolnshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2467, 'Middlesex', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2468, 'Norfolk', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2469, 'Northamptonshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2470, 'Northumberland', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2471, 'Nottinghamshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2472, 'Oxfordshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2473, 'Rutland', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2474, 'Shropshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2475, 'Somerset', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2476, 'Staffordshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2477, 'Suffolk', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2478, 'Surrey', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2479, 'Sussex', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2480, 'Warwickshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2481, 'Westmorland', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2482, 'Wiltshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2483, 'Worcestershire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2485, 'Buckinghamshire', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2489, 'Curacao', 297)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2490, 'La Reunion St Clotide', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2491, 'St Clotide', 216)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2493, 'Castries Quarter', 298)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2494, 'Teheran', 266)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2495, 'Maastricht', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2497, 'Pondicherry', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2498, 'Gaza', 193)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2499, 'Birzeit', 299)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2501, 'Delft', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2502, 'Cluj - Napoca', 217)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2503, 'Kaunas', 275)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2504, 'Cumbria', 248)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2505, 'Berne', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2506, 'Peshawar', 82)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2507, 'Rawalpindi', 82)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2508, 'Sialkot', 82)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2509, 'Chengdu', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2510, 'Hangzhou', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2511, 'Dili', 300)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2512, 'Tbilisi', 301)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2513, 'Pristina', 302)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2514, 'Yerevan', 303)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2515, 'Baku', 304)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2516, 'Antananarivo', 305)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2517, 'Chandigarh', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2518, 'Plimmerton', 2)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2519, 'Eskisehir', 230)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2520, 'Prince Edward Island', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2522, 'Managua', 306)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2523, 'Cotonou', 307)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2524, 'Douglas', 308)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2525, 'Cayman Islands', 309)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2526, 'Jharkhand', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2527, 'Mongolia', 272)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2528, 'San Salvador', 310)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2530, 'Lugano', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2531, 'Christ Church', 162)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2532, 'Ticino', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2533, 'Almere', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2534, 'Nijmegen', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2535, 'Den Bosch', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2536, 'Chisinau', 312)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2537, 'Alexandria', 179)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2538, 'Jaipur', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2542, 'Wanganui', 2)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2543, 'Sofia Antipolis', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2544, 'Valbonne', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2545, 'Channel Islands', 313)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2547, 'Nunavut', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2549, 'Zwolle', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2550, 'Triesen', 314)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2551, 'Austerlitz', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2552, 'Jalisco', 258)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2553, 'Sudbury', 168)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2554, 'Ebene', 203)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2555, 'Amstelveen', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2557, 'Bielefeld', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2558, 'Esslingen', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2561, 'Muenster', 186)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2564, 'Brunei', 316)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2565, 'Maugio', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2566, 'Oulu', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2567, 'Lappeenranta', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2568, 'Jyvaskyla', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2569, 'Kuopio', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2570, 'Vantaa', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2571, 'Tampere', 182)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2572, 'Mabolo', 211)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2573, 'Almaty', 271)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2574, 'Astan', 271)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2577, 'Reykjavik', 317)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2578, 'Qingdao', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2579, 'Bamako', 318)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2580, 'Yaounde', 319)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2581, 'Vanuatu', 329)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2582, 'Elst', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2583, 'Schiedam', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2584, 'Bolivar', 173)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2585, 'Tripoli', 321)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2586, 'Vaslui', 217)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2587, 'Metz', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2588, 'Gdansk', 212)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2589, 'Jiangsu Province', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2590, 'Fujian Province', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2591, 'Hunan Province', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2592, 'Leeuwarden', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2593, 'Yurakucho', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2595, 'Mulhouse', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2596, 'San Jorge', 164)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2597, 'Abuja', 274)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2598, 'Port Harcourt', 274)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2599, 'Issy-les-Moulineaux', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2600, 'Nassau', 323)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2601, 'Cowes', 324)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2602, 'Ahmedabad', 190)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2603, 'Lalitpur', 325)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2604, 'Modena', 194)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2606, 'Minsk', 326)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2607, 'Nanan District', 172)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2608, 'Freetown', 327)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2609, 'Brno', 177)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2610, 'Gent', 163)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2611, 'Winterthur', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2612, 'Eysins', 226)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2615, 'Madeira', 213)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2616, 'Rijswijk', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2617, 'Lusaka', 328)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2618, 'Cannes', 183)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2619, 'Sendai', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2620, 'Hiroshima', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2621, 'Kobe', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2622, 'Hyogo', 196)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2624, 'Recife', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2625, 'Pernambuco', 165)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2626, 'Amersfoort', 206)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2663, 'Anbar Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2666, 'Arbil Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2667, 'Babylon Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2668, 'Baghdad Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2669, 'Basra Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2664, 'Muthanna Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2665, 'Qadisiyyah Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2670, 'Dhi Qar Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2671, 'Diyala Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2672, 'Dohuk Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2673, 'Karbala Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2674, 'Kirkuk Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2675, 'Maysan Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2676, 'Najaf Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2677, 'Nineveh Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2678, 'Saladin Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2679, 'Sulaymaniyah Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2680, 'Wasit Province', 267)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2697, 'Abyan', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2682, 'Ad-Dali''', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2696, 'Aden', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2683, 'Al-Bayda''', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2684, 'Al-Hudaydah', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2685, 'Al-Jawf', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2698, 'Al-Mahrah', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2686, 'Al-Mahwit', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2687, 'Amanat Al Asimah', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2681, 'Amran', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2688, 'Dhamar', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2699, 'Hadramaut', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2689, 'Hajjah', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2690, 'Ibb', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2700, 'Lahij', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2691, 'Ma''rib', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2692, 'Raymah', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2693, 'Sa''dah', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2694, 'Sana''a', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2701, 'Shabwah', 268)INSERT INTO [dbo].[Public_StateCountryDataVocabulary] ([stateID],[stateName],[countryID]) VALUES
( 2695, 'Taiz', 268)

--RealPublic_CityStateCountryData
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Public_CityStateCountryData]') AND type in (N'U'))
begin
 DELETE FROM [dbo].[Public_CityStateCountryData]
 DROP TABLE [dbo].[Public_CityStateCountryData] 
end;


CREATE TABLE [dbo].[Public_CityStateCountryData]
(
    [cityID] [int]  IDENTITY(1,1) Unique,
	[cityName] [nvarchar](150) NOT NULL,
	[stateID] [int] NOT NULL,
	[countryID] [int] NOT NULL,
 )

ALTER TABLE [dbo].[Public_CityStateCountryData] ADD CONSTRAINT ndxPublic_CityStateCountryData PRIMARY KEY CLUSTERED (
[countryID], [stateID], [cityName]  
)

Insert Into [dbo].[Public_CityStateCountryData] 
       ([cityName],[stateID],[countryID])
SELECT DISTINCT
       [City]
      ,[StateName]
      ,[Country]      
  FROM [dbo].[ES_PublicRoom_d] where StateName > 0 and Country > 0 and len(City)>0
  
  --RealPublic_StateCountryData
  --USE [whygo]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Public_StateCountryData]') AND type in (N'U'))
begin
Delete FROM [dbo].[Public_StateCountryData]
DROP TABLE [dbo].[Public_StateCountryData]
end;
CREATE TABLE [dbo].[Public_StateCountryData](
	[stateID] [int] NOT NULL,
	[stateName] [nvarchar](150) NOT NULL,
	[countryID] [int] NOT NULL,
 CONSTRAINT [PK_Public_StateCountryData] PRIMARY KEY CLUSTERED 
(
	[stateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = 

ON) ON [PRIMARY]
) ON [PRIMARY]

Insert Into [dbo].[Public_StateCountryData] 
       ([stateID],[stateName],[countryID])
SELECT DISTINCT       
      [stateID] ,[dbo].[Public_StateCountryDataVocabulary].[stateName], [dbo].[Public_StateCountryDataVocabulary].[countryID]     
  FROM [dbo].[ES_PublicRoom_d]  
   join [dbo].[Public_StateCountryDataVocabulary] on [dbo].[Public_StateCountryDataVocabulary].[stateID]=[dbo].[ES_PublicRoom_d].[StateName]
  where Country > 0 
--RealPublicCountries

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Public_CountryData]') AND type in (N'U'))
begin
Delete FROM [dbo].[Public_CountryData]
DROP TABLE [dbo].[Public_CountryData]
end;

CREATE TABLE [dbo].[Public_CountryData](
	[countryID] [int] NOT NULL,
	[countryName] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Public_CountryData] PRIMARY KEY CLUSTERED 
(
	[countryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = 

ON) ON [PRIMARY]
) ON [PRIMARY]

Insert Into [dbo].[Public_CountryData] 
       ([countryID],[countryName])
SELECT DISTINCT       
      [Country] ,[countryName]     
  FROM [dbo].[ES_PublicRoom_d]  
   join [dbo].[Public_CountryDataVocabulary] on [dbo].[Public_CountryDataVocabulary].[countryID]=[dbo].[ES_PublicRoom_d].[Country]
  where Country > 0 

/* ********************* Public City State Data Ends    ************************ */

/* ********************* FB 2998 29AUg  Starts    ************************ */


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MCUSetupDisplay smallint NULL,
	MCUTearDisplay smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_MCUSetupDisplay DEFAULT 0 FOR MCUSetupDisplay
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_MCUTearDisplay DEFAULT 0 FOR MCUTearDisplay
GO
COMMIT

update Org_Settings_D set MCUSetupDisplay = 0, MCUTearDisplay = 0


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	McuSetupTime int NULL,
	MCUTeardonwnTime int NULL
GO
COMMIT


UPDATE  conf_conference_d SET
    McuSetupTime = b.McuSetupTime,
    MCUTeardonwnTime = b.MCUTeardonwnTime
FROM conf_conference_D a , Org_Settings_D b where a.orgid = b.orgid
and a.conftype in(2,6) and confdate >= getdate()


/* ********************* FB 2998 29AUg  Ends    ************************ */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.2 Ends (28th AUg 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */