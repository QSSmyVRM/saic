
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.20 Starts(8 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */

/* ********************** ZD 100263 Starts ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	RequestID nvarchar(MAX) NULL
GO
COMMIT

update Usr_List_D set RequestID=''

/* ********************** ZD 100263 End ********************** */


/* ********************** ZD 100263 Stard 26 Nov 2013 ********************** */
--File upload issue

Delete from Conf_Attachments_D where attachment = ''

/* ********************** ZD 100263 End 26 Nov 2013 ********************** */


/*********** ZD 100510- Hide Unused Languages-Start  *****************/

Delete from Dept_CustomAttr_Option_D where LanguageID in (5,6,7,3)
Delete from Custom_Lang_D where LanguageId in (5,6,7,3)
Delete from Gen_Language_S where LanguageID in(2,5,6,7,3)

/*********** ZD 100510- Hide Unused Languages-End*****************/

/* ************************* ZD 100256 (21st Nov 2013)Start ************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	syncSettings smallint NULL
GO
COMMIT

Update Sys_Settings_D set syncSettings = 0



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Sync_Settings_D
	(
	[UID] int NOT NULL IDENTITY (1, 1),
	[Address] nvarchar(50) NULL,
	[Port] int NULL,
	[Configuration] int NULL,
	[EmailAddress] nvarchar(MAX) NULL
    )  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ES_Event_D ADD
	EmailNotification smallint NULL
GO
COMMIT

Update ES_Event_D set EmailNotification = 0



/* ************************* ZD 100256 (21st Nov 2013)End ************************* */
