
/*   **** NewEnhancementQueries.sql - start  **** */

/* *** Enhancements and Bugs 1.9x version - start *** */


/* (Check and run each and every script till Ticker enhancements for availability in V 1.9x DB) */

/* Custom attribute Changes */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_Option_D ADD
	HelpText nvarchar(4000) NULL
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD
	IncludeInEmail smallint NULL
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_CustomAttr_D ADD
	[ConfAttrID] [int] IDENTITY(1,1) NOT NULL
GO
COMMIT


/* Update queries for Guest */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_GuestList_D ADD
	DateFormat nvarchar(50) NULL,
	enableAV int NULL,
	timezonedisplay int NULL,
	timeformat int NULL
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_DateFormat DEFAULT (N'MM/dd/yyyy') FOR DateFormat
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_enableAV DEFAULT ((0)) FOR enableAV
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_timezonedisplay DEFAULT ((1)) FOR timezonedisplay
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_timeformat DEFAULT ((1)) FOR timeformat
GO
COMMIT

Update Usr_GuestList_D set 
DateFormat = N'MM/dd/yyyy'
where (DateFormat is null or DateFormat = null)

Update Usr_GuestList_D set 
enableAV = 0
where (enableAV is null or enableAV = null)

Update Usr_GuestList_D set 
timezonedisplay = 0
where (timezonedisplay is null or timezonedisplay = null)

Update Usr_GuestList_D set 
timeformat = 0
where (timeformat is null or timeformat = null)

--

/* Update data for bufferzone */

/* Update statement for defaulting the confstarttime to setuptime */
/* for existing old data */

Update conf_conference_D set 
setupTime = confdate
where (setupTime is null or setuptime = null)

/* Update statement for defaulting the confend time to teardown time */
/* for existing old data */

Update conf_conference_D set 
teardownTime = Dateadd(minute,duration,confdate)
where (teardownTime is null or teardownTime = null)


--
/*
   Friday, October 09, 20099:12:01 PM
   User: 
   Server: revathivm\SQLEXPRESS
   Database: myVRM_090109
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	TickerStatus int NULL,
	TickerSpeed int NULL,
	TickerPosition int NULL,
	TickerBackground nchar(10) NULL,
	TickerDisplay int NULL,
	RSSFeedLink nvarchar(50) NULL,
	TickerStatus1 int NULL,
	TickerSpeed1 int NULL,
	TickerPosition1 int NULL,
	TickerBackground1 nchar(10) NULL,
	TickerDisplay1 int NULL,
	RSSFeedLink1 nvarchar(50) NULL
GO
COMMIT


update usr_list_d set tickerstatus=1,tickerspeed=6,TickerPosition=0,
TickerBackground='#660066',TickerDisplay=0 

update usr_list_d set tickerstatus1=1,tickerspeed1=3,TickerPosition1=0,
TickerBackground1='#99ff99',TickerDisplay1=0 

--
/* Activation Module */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	IsDemo smallint NULL,
	ActivatedDate datetime NULL
GO
ALTER TABLE dbo.Sys_Settings_D ADD CONSTRAINT
	DF_Sys_Settings_D_IsDemo DEFAULT 0 FOR IsDemo
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD CONSTRAINT
	DF_Sys_Settings_D_ActivatedDate DEFAULT (01/01/1990) FOR ActivatedDate
GO
COMMIT

Update Sys_Settings_D set ActivatedDate = '01/01/1990'

Update Sys_Settings_D set IsDemo = '0'


/* *** Enhancements and Bugs 1.9x version - end *** */



/* *** Enhancements and Bugs V2.0 version - start *** */

/* Room Search Updates  */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	HandiCappedAccess smallint NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD
	Lastmodifieddate datetime NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_HandiCappedAccess DEFAULT 0 FOR HandiCappedAccess
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_Lastmodifieddate DEFAULT getdate() FOR Lastmodifieddate
GO
COMMIT

Update loc_room_d set HandiCappedAccess = 0
Update loc_room_d set Lastmodifieddate = getdate()

--

/* License SQL Changes */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	enableExchange int NULL,
	enableDomino int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableExchange DEFAULT 0 FOR enableExchange
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableDomino DEFAULT 0 FOR enableDomino
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	enableExchange int NULL,
	enableDomino int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableExchange DEFAULT 0 FOR enableExchange
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableDomino DEFAULT 0 FOR enableDomino
GO
COMMIT


/*Added for New License this is default values*/


/* V2x License Structure */

update sys_settings_d set license='KmzsDo+0yAjTApoYqR4/Egd2oqJZ7iUZgqwBLuJ2YCr1JXM2hSYxfUBxjl+Ct6IKgt4orJRTkvoTqTqS6f92Me6hejM0EatEOXV9ImWU5DnJnwU1BUjV0PU2mE+kSAlB1fZdEsMvuBh7bZkiXmM971jS9X/y6GttPJ+ZVZ9GzOQXzxeGRfbY4tUIOLzvvk80E/yL4SJkRpdv6PGqaF4w71C4uaXj8DIB/1T4hb9cK7fEGioBeOA6vz+Ii2vBPQM3hFyhF64Yht2qMNTypKGtI2jQ4o5wiLWhy+oXbZaDWM3r8atH4t1QNfywopu3ivczMpkRwVFDli1ZqCEkjng5saYNF4Fx/zxU5h1bmhXJWsjzto502eIasDh8EqfPic6lorVSui/aJTq67Gfe07D+1prtbPrC86d5cnvE5ltGJJ/iFXg3u3zKGAvDez4B14WMaK0GaTkE6j6cIo18wlcCE5MaO0Pi13c49FLArg8jT5AjfkHL33/jySVnJyYs0KDxVjNJzcDaFwekIzoj0dApm6TPAUxlTuM/snxTodoLWXWtZjL0QBfJas0GVBKBOiuniQ+CrAXp5HedJrzlNtuubvkAg4HA6yHrY4VQVsugQL4oZ4wttqT79fVx6kLn/nq/Br65SEykTcp9/JRjZRuv+J4Wz7JAqHbVYesdygbrFHaxAIL//c7FsDC9AQHSjDLCg/brQ2MQtV775eBnyI2Vbiw044EaRsrM5exOBPgJYb1EsXdx0ZpTV+Q/bVYkiK+lAWG5XdWRWN360iNdIdMfGVh7AGY4VsXJIVAGOA4++UecSKXVFebXje0ut6Wa2kmzHXEHfAeiPdZPw6wKMrpY8APayDnUeP49AwR2pYSTYchvMuRNeQQ/DhrRnmDtX0YjGVZAa4ABj0x2NGH0nQiIViahRm1ezoSTq7hiAGqUgOSmWocgliOZ/0xL9zV9S6ql3/FZ16ufl5jEsAJvc1Pia6cIAbqv2ImVgpUr3Sqq2dr2cBq25fQ0Wu4FWiIdZNq0KTMCltvPJUDWMmdgL5fGNwlmQA2K05pYE1YPWARnQzj5HL7p5kD77p0WPbiflNZVai516AI+kHEE3syVTzddMQ29UiflmnMgmXjny59mDGMNq7mICpy14MCqSIM5GGeAhehnWv/s4ofQoG+erbJPZ1wmENJzCukK0ENMhd4yO8iqlcjAfoLrOZQEMEZ5RjCF3RoN0QIxLUGgqdFcB8LvjO6RDRInSpESnctrY9Pp1PJiDq3yLKcysRUN9baDVVCcBCMtqbB/RGKOQF7sWrLvjGBhjJN1/HHlqkb39AcZuYMOXpHCIEd7APBHBdWOjbBApUD5JMi3a/ufdZdJ7Ro5IuFpK63Pf2c1L+/AgJC1dig=' where [Admin] = 11
--

/* Endpoint sql changes */



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	ExchangeID nvarchar(200) NULL
GO

ALTER TABLE dbo.Conf_User_D ADD
	ExchangeID nvarchar(200) NULL
GO

COMMIT


Insert into Gen_VideoEquipment_S values (27,'Cisco Telepresence System')

--

/* Image Project Related Script Changes - start */

/****** Object:  Table [dbo].[Img_List_D]    Script Date: 01/06/2010 04:22:22 ******/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[Img_List_D](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[AttributeType] [smallint] NULL,
	[AttributeImage] [varbinary](max) NULL,
	[OrgId] [int] NULL,
 CONSTRAINT [PK_Img_List_D] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

ALTER TABLE Org_Settings_D ADD
	LogoImageId int NULL,
	LobytopImageId int NULL,
	LobytopHighImageId int NULL,
	LogoImageName nvarchar(250) NULL,
	LobytopImageName nvarchar(250) NULL,
	LobytopHighImageName nvarchar(250) NULL
GO


ALTER TABLE Loc_Room_D ADD
	MapImage1Id int NULL,
	MapImage2Id int NULL,
	SecurityImage1Id int NULL,
	SecurityImage2Id int NULL,
	MiscImage1Id int NULL,
	MiscImage2Id int NULL,
	RoomImageId nvarchar(200) NULL

GO

ALTER TABLE Inv_ItemList_AV_D ADD
	ImageId int NULL
GO

ALTER TABLE Inv_ItemList_HK_D ADD
	ImageId int NULL
GO

ALTER TABLE Inv_List_D ADD
	ImageId int NULL
GO
ALTER TABLE Usr_Inactive_D ALTER COLUMN /* FB 1698*/
PreferedRoom varchar(150)
GO

COMMIT

/* Image Project Related Script Changes - end */


/*Account Spell Issue*/
update err_list_s set message ='Insufficient account balance to set up a conference. Please contact your VRM Administrator and supply this error code to obtain a usage account.' where id=231


/* *** Enhancements and Bugs V2.0 version - end *** */

/* *** NewEnhancementQueries.sql end *** */



/* *** Enhancements and Bugs V2x version - start *** */


/* ********************* Dasboard.sql - start ******************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Cascade_D ADD
	OnlineStatus int NULL,
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_Cascade_D ADD CONSTRAINT
	DF_Conf_Cascade_D_OnlineStatus DEFAULT 0 FOR OnlineStatus
GO
ALTER TABLE dbo.Conf_Cascade_D ADD CONSTRAINT
	DF_Conf_Cascade_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Conf_Room_D ADD
	OnlineStatus int NULL,
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_Room_D ADD CONSTRAINT
	DF_Conf_Room_D_OnlineStatus DEFAULT 0 FOR OnlineStatus
GO
ALTER TABLE dbo.Conf_Room_D ADD CONSTRAINT
	DF_Conf_Room_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Conf_User_D ADD
	OnlineStatus int NULL,
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_User_D ADD CONSTRAINT
	DF_Conf_User_D_OnlineStatus DEFAULT 0 FOR OnlineStatus
GO
ALTER TABLE dbo.Conf_User_D ADD CONSTRAINT
	DF_Conf_User_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Sys_MailServer_D ADD
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Sys_MailServer_D ADD CONSTRAINT
	DF_Sys_MailServer_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
COMMIT


update Conf_Cascade_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Conf_Room_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Conf_User_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Conf_Conference_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Sys_MailServer_D set LastRunDateTime = getdate() where LastRunDateTime is null


/* ********************* Dasboard.sql - end ****************** */


/* ********************* RoomSearch.sql - start ****************** */

/*
   Tuesday, December 22, 20098:27:57 AM
   User: 
   Server: YAMUNA\SQL2005
   Database: orgdbfull
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Templates_D
	DROP CONSTRAINT DF_Usr_Templates_D_ExpirationDate
GO
CREATE TABLE dbo.Tmp_Usr_Templates_D
	(
	id int NOT NULL IDENTITY (1, 1),
	name nvarchar(256) NULL,
	connectiontype int NULL,
	ipisdnaddress nvarchar(256) NULL,
	initialtime int NULL,
	timezone int NULL,
	videoProtocol int NULL,
	location nvarchar(150) NULL,
	languageId int NULL,
	lineRateId int NULL,
	bridgeId int NULL,
	deptId int NULL,
	groupId int NULL,
	ExpirationDate datetime NULL,
	emailNotification int NULL,
	outsideNetwork int NULL,
	role int NULL,
	addressBook int NULL,
	equipmentId int NULL,
	deleted int NULL,
	companyId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Usr_Templates_D ADD CONSTRAINT
	DF_Usr_Templates_D_ExpirationDate DEFAULT (((1)/(1))/(1980)) FOR ExpirationDate
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_Templates_D ON
GO
IF EXISTS(SELECT * FROM dbo.Usr_Templates_D)
	 EXEC('INSERT INTO dbo.Tmp_Usr_Templates_D (id, name, connectiontype, ipisdnaddress, initialtime, timezone, videoProtocol, location, languageId, lineRateId, bridgeId, deptId, groupId, ExpirationDate, emailNotification, outsideNetwork, role, addressBook, equipmentId, deleted, companyId)
		SELECT id, name, connectiontype, ipisdnaddress, initialtime, timezone, videoProtocol, location, languageId, lineRateId, bridgeId, deptId, groupId, ExpirationDate, emailNotification, outsideNetwork, role, addressBook, equipmentId, deleted, companyId FROM dbo.Usr_Templates_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_Templates_D OFF
GO
DROP TABLE dbo.Usr_Templates_D
GO
EXECUTE sp_rename N'dbo.Tmp_Usr_Templates_D', N'Usr_Templates_D', 'OBJECT' 
GO
ALTER TABLE dbo.Usr_Templates_D ADD CONSTRAINT
	PK_Usr_Templates_D PRIMARY KEY CLUSTERED 
	(
	id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT

/*
   Tuesday, December 22, 20098:28:45 AM
   User: 
   Server: YAMUNA\SQL2005
   Database: orgdbfull
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Financial
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Admin
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_TimeZone
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Language
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PreferedRoom
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DoubleEmail
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PreferedGroup
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_CCGroup
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_RoomID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PreferedOperator
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_lockCntTrns
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DefLineRate
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DefVideoProtocol
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Deleted
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_EmailClient
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_newUser
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PrefTZSID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DefaultEquipmentId
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_LockStatus
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_LastLogin
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_accountexpiry
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_approverCount
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_BridgeID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_LevelID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_endpointId
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DateFormat
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableAV
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_timezonedisplay
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_timeformat
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableParticipants
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableExchange
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableDomino
GO
CREATE TABLE dbo.Tmp_Usr_List_D
	(
	Id int NOT NULL IDENTITY (1, 1),
	UserID int NOT NULL,
	FirstName nvarchar(256) NULL,
	LastName nvarchar(256) NULL,
	Email nvarchar(512) NULL,
	Financial int NULL,
	Admin smallint NULL,
	Login nvarchar(256) NULL,
	Password nvarchar(1000) NULL,
	Company nvarchar(512) NULL,
	TimeZone smallint NULL,
	Language smallint NULL,
	PreferedRoom varchar(150) NULL,
	AlternativeEmail nvarchar(512) NULL,
	DoubleEmail smallint NULL,
	PreferedGroup smallint NULL,
	CCGroup smallint NULL,
	Telephone nvarchar(50) NULL,
	RoomID int NULL,
	PreferedOperator int NULL,
	lockCntTrns int NULL,
	DefLineRate int NULL,
	DefVideoProtocol int NULL,
	Deleted int NOT NULL,
	DefAudio int NULL,
	DefVideoSession int NULL,
	MenuMask nvarchar(200) NULL,
	initialTime int NULL,
	EmailClient smallint NULL,
	newUser smallint NULL,
	PrefTZSID int NULL,
	IPISDNAddress nvarchar(256) NULL,
	DefaultEquipmentId smallint NOT NULL,
	connectionType smallint NULL,
	companyId int NULL,
	securityKey nvarchar(256) NULL,
	LockStatus smallint NOT NULL,
	LastLogin datetime NULL,
	outsidenetwork smallint NULL,
	emailmask bigint NULL,
	roleID int NULL,
	addresstype smallint NULL,
	accountexpiry datetime NULL,
	approverCount int NULL,
	BridgeID int NOT NULL,
	Title nvarchar(50) NULL,
	LevelID int NULL,
	preferredISDN nvarchar(50) NULL,
	portletId int NULL,
	searchId int NULL,
	endpointId int NOT NULL,
	DateFormat nvarchar(50) NULL,
	enableAV int NULL,
	timezonedisplay int NULL,
	timeformat int NULL,
	enableParticipants int NULL,
	TickerStatus int NULL,
	TickerSpeed int NULL,
	TickerPosition int NULL,
	TickerBackground nchar(10) NULL,
	TickerDisplay int NULL,
	RSSFeedLink nvarchar(50) NULL,
	TickerStatus1 int NULL,
	TickerSpeed1 int NULL,
	TickerPosition1 int NULL,
	TickerBackground1 nchar(10) NULL,
	TickerDisplay1 int NULL,
	RSSFeedLink1 nvarchar(50) NULL,
	enableExchange int NULL,
	enableDomino int NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'TimeZone'
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'Language'
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'PreferedRoom'
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'DoubleEmail'
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Financial DEFAULT ((0)) FOR Financial
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Admin DEFAULT ((0)) FOR Admin
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_TimeZone DEFAULT ((1)) FOR TimeZone
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Language DEFAULT ((1)) FOR Language
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PreferedRoom DEFAULT ((1)) FOR PreferedRoom
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DoubleEmail DEFAULT ((0)) FOR DoubleEmail
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PreferedGroup DEFAULT ((1)) FOR PreferedGroup
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_CCGroup DEFAULT ((1)) FOR CCGroup
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_RoomID DEFAULT ((1)) FOR RoomID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PreferedOperator DEFAULT ((1)) FOR PreferedOperator
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_lockCntTrns DEFAULT ((0)) FOR lockCntTrns
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DefLineRate DEFAULT ((2)) FOR DefLineRate
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DefVideoProtocol DEFAULT ((1)) FOR DefVideoProtocol
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Deleted DEFAULT ((0)) FOR Deleted
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_EmailClient DEFAULT ((0)) FOR EmailClient
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_newUser DEFAULT ((1)) FOR newUser
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PrefTZSID DEFAULT ((0)) FOR PrefTZSID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DefaultEquipmentId DEFAULT ((1)) FOR DefaultEquipmentId
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_LockStatus DEFAULT ((0)) FOR LockStatus
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_LastLogin DEFAULT (getutcdate()) FOR LastLogin
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_accountexpiry DEFAULT (getutcdate()) FOR accountexpiry
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_approverCount DEFAULT ((0)) FOR approverCount
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_BridgeID DEFAULT ((1)) FOR BridgeID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_LevelID DEFAULT ((0)) FOR LevelID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_endpointId DEFAULT ((0)) FOR endpointId
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DateFormat DEFAULT (N'MM/dd/yyyy') FOR DateFormat
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableAV DEFAULT ((0)) FOR enableAV
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_timezonedisplay DEFAULT ((0)) FOR timezonedisplay
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_timeformat DEFAULT ((0)) FOR timeformat
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableParticipants DEFAULT ((0)) FOR enableParticipants
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableExchange DEFAULT ((0)) FOR enableExchange
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableDomino DEFAULT ((0)) FOR enableDomino
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_List_D ON
GO
IF EXISTS(SELECT * FROM dbo.Usr_List_D)
	 EXEC('INSERT INTO dbo.Tmp_Usr_List_D (Id, UserID, FirstName, LastName, Email, Financial, Admin, Login, Password, Company, TimeZone, Language, PreferedRoom, AlternativeEmail, DoubleEmail, PreferedGroup, CCGroup, Telephone, RoomID, PreferedOperator, lockCntTrns, DefLineRate, DefVideoProtocol, Deleted, DefAudio, DefVideoSession, MenuMask, initialTime, EmailClient, newUser, PrefTZSID, IPISDNAddress, DefaultEquipmentId, connectionType, companyId, securityKey, LockStatus, LastLogin, outsidenetwork, emailmask, roleID, addresstype, accountexpiry, approverCount, BridgeID, Title, LevelID, preferredISDN, portletId, searchId, endpointId, DateFormat, enableAV, timezonedisplay, timeformat, enableParticipants, TickerStatus, TickerSpeed, TickerPosition, TickerBackground, TickerDisplay, RSSFeedLink, TickerStatus1, TickerSpeed1, TickerPosition1, TickerBackground1, TickerDisplay1, RSSFeedLink1, enableExchange, enableDomino)
		SELECT Id, UserID, FirstName, LastName, Email, Financial, Admin, Login, Password, Company, TimeZone, Language, PreferedRoom, AlternativeEmail, DoubleEmail, PreferedGroup, CCGroup, Telephone, RoomID, PreferedOperator, lockCntTrns, DefLineRate, DefVideoProtocol, Deleted, DefAudio, DefVideoSession, MenuMask, initialTime, EmailClient, newUser, PrefTZSID, IPISDNAddress, DefaultEquipmentId, connectionType, companyId, securityKey, LockStatus, LastLogin, outsidenetwork, emailmask, roleID, addresstype, accountexpiry, approverCount, BridgeID, Title, LevelID, preferredISDN, portletId, searchId, endpointId, DateFormat, enableAV, timezonedisplay, timeformat, enableParticipants, TickerStatus, TickerSpeed, TickerPosition, TickerBackground, TickerDisplay, RSSFeedLink, TickerStatus1, TickerSpeed1, TickerPosition1, TickerBackground1, TickerDisplay1, RSSFeedLink1, enableExchange, enableDomino FROM dbo.Usr_List_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_List_D OFF
GO
DROP TABLE dbo.Usr_List_D
GO
EXECUTE sp_rename N'dbo.Tmp_Usr_List_D', N'Usr_List_D', 'OBJECT' 
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	PK_Usr_List_D PRIMARY KEY CLUSTERED 
	(
	UserID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


/* ********************* RoomSearch.sql - end   ****************** */



/* *********************  Updateroles.sql - start ****************** */

update usr_roles_d set roleName='General User', roleMenuMask='6*60-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*63' where roleID=1
update usr_roles_d set roleName='Site Administrator', roleMenuMask='6*63-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*1-6*63' where roleID=3
update usr_roles_d set roleName='Catering Administrator', roleMenuMask='6*34-4*0+8*2+3*0+2*0+8*0+2*0+2*3+2*0+1*0-6*63' where roleID=4
update usr_roles_d set roleName='Inventory Administrator', roleMenuMask='6*34-4*15+8*4+3*0+2*0+8*0+2*3+2*0+2*0+1*0-6*63' where roleID=5
update usr_roles_d set roleName='Housekeeping Administrator', roleMenuMask='6*34-4*15+8*1+3*0+2*0+8*0+2*0+2*0+2*3+1*0-6*63' where roleID=6

update usr_roles_d set roleName='Organization Administrator',[level]=2, roleMenuMask='6*62-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*0-6*63' where roleID=2

update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid 

update usr_roles_d set level=0, roleMenuMask='6*60-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*63' where roleID > 6


update Usr_Roles_D set crossaccess=0
update Usr_Roles_D set crossaccess=1 where roleid=3

/* *********************  Updateroles.sql - end   ****************** */



/* ********************* CSS_Changes_Script.sql - start ****************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[Org_Settings_D] ADD[DefaultCssId] [int] NULL CONSTRAINT [DF_Org_Settings_D_DefaultCssId] DEFAULT ((0))

ALTER TABLE [dbo].[Org_Settings_D] ADD[MirrorCssId] [int] NULL CONSTRAINT [DF_Org_Settings_D_MirrorCssId] DEFAULT ((0))

ALTER TABLE [dbo].[Org_Settings_D] ADD[ArtifactsCssId] [int] NULL CONSTRAINT [DF_Org_Settings_D_ArtifactsCssId] DEFAULT ((0))

ALTER TABLE [dbo].[Org_Settings_D] ADD[TextchangeId] [int] NULL CONSTRAINT [DF_Org_Settings_D_TextchangeId] DEFAULT ((0))

GO
COMMIT



/*
   Friday, January 05, 200712:37:32 AM
   User: 
   Server: GANGES
   Database: OrgDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	SiteLogoId int NULL,
	Companymessage varchar(50) NULL,
    	StdBannerId int NULL,
    	HighBannerId int NULL
GO
COMMIT



/* ********************* CSS_Changes_Script.sql - end   ****************** */


/* ********************* EmailLogo.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MailLogo int NULL
GO
COMMIT

Update Org_Settings_D set MailLogo = 0

/* ********************* EmailLogo.sql - end   ****************** */


/* ********************* ICAL_Change.sql - start ****************** */

/*
   Tuesday, January 19, 201010:44:44 PM
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	CalendarInvite int NULL
GO
COMMIT

/*
   Monday, January 01, 200712:28:48 AM
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Queue_D ADD
	Iscalendar smallint NULL
GO
ALTER TABLE dbo.Email_Queue_D ADD CONSTRAINT
	DF_Email_Queue_D_Iscalendar DEFAULT 0 FOR Iscalendar
GO
COMMIT

Update email_queue_d set Iscalendar = 0


/* ********************* ICAL_Change.sql - end   ****************** */


/* ********************* APIPortScript.sql - start ****************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	ApiPortNo int NULL,
	endptURL nvarchar(50) NULL
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	ApiPortNo int NULL,
	endptURL nvarchar(50) NULL
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	ApiPortNo int NULL
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	ApiPortNo int NULL
GO
COMMIT

/* Script to update old data with default APIPort No */

Update MCU_List_D set APIPortNo=80
Update Ept_List_D set APIPortNo=23
Update Conf_User_D set APIPortNo=23
Update Conf_Room_D set APIPortNo=23

/* ********************* APIPortScript.sql - end   ****************** */


/* ********************* DTMF_Change.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	PreConfCode varchar(50) NULL,
	PreLeaderPin varchar(50) NULL,
	PostLeaderPin varchar(50) NULL
GO
COMMIT

/* ********************* DTMF_Change.sql - end   ****************** */


/* ********************* Gen_LineRate.sql - start ****************** */

/* Line rates changed during Line rate fix */

truncate table Gen_LineRate_S

insert into Gen_LineRate_S (LineRateID,LineRateType) values(56,'56 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(64,'64 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(128,'128 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(192,'192 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(256,'256 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(320,'320 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(384,'384 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(512,'512 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(768,'768 Kbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(1024,'1 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(1152,'1.152 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(1250,'1.25 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(1472,'1.472 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(1536,'1.5 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(1792,'1.75 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(1920,'1.92 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(2048,'2.0 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(2560,'2.5 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(3072,'3.0 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(3584,'3.5 Mbps')
insert into Gen_LineRate_S (LineRateID,LineRateType) values(4096,'4.0 Mbps')

/* ********************* Gen_LineRate.sql - end   ****************** */


/* ********************* ADupdatescript.sql - start ****************** */

update usr_list_d set emailclient = '-1'

/* ********************* ADupdatescript.sql - end   ****************** */


/* ********************* audioaddon.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	ConferenceCode nvarchar(50) NULL,
	LeaderPin nvarchar(50) NULL
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
		Audioaddon nvarchar(50) NULL,
        WorkPhone nvarchar(50) NULL,
		CellPhone nvarchar(50) NULL,
		ConferenceCode nvarchar(50) NULL,
		LeaderPin nvarchar(50) NULL
GO 
COMMIT
/*before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	Audioaddon nvarchar(50) NULL,
    WorkPhone nvarchar(50) NULL,
	CellPhone nvarchar(50) NULL,
	ConferenceCode nvarchar(50) NULL,
	LeaderPin nvarchar(50) NULL
	 
GO 
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	ConferenceCode int NULL,
	LeaderPin int NULL,
	AdvAvParams int NULL,
	AudioParams int NULL
GO
COMMIT

/* ********************* audioaddon.sql - end   ****************** */


/* ********************* BlueStatus.sql - start ****************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	remoteEndPointIP nvarchar(50) NULL
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	remoteEndPointIP nvarchar(50) NULL
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Cascade_D ADD
	remoteEndPointIP nvarchar(50) NULL
GO
COMMIT


/* ********************* BlueStatus.sql - end   ****************** */


/* ********************* FB 1710.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Queue_D ADD
	orgID int NULL
GO
COMMIT

/* ** Update existing records if any with default organization id ** */
Update Email_Queue_D set orgID = 11

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	FooterMessage nvarchar(1000) NULL,FooterImage int NULL
GO
COMMIT

Update Org_Settings_D set FooterImage = 0

/* ********************* FB 1710.sql - end   ****************** */


/* ********************* FB 1721-Gen_Interface.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

insert into Gen_InterfaceType_S values(4,'IP,ISDN,SIP,MPI')

GO
COMMIT

/* ********************* FB 1721-Gen_Interface.sql - end   ****************** */


/* ********************* SetDefaultTemplateScript.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	DefaultTemplate smallint NULL

GO

ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DefaultTemplate DEFAULT 0 FOR DefaultTemplate 
GO

COMMIT

/* Query to update existing records */

update Usr_List_D set DefaultTemplate = 0


/* ********************* SetDefaultTemplateScript.sql - end   ****************** */


/* ********************* FB 1744 - start ****************** */

/* ******************************************************* */
/*			FB 1744				   */
/* Participant media type issue. To make consistant across */
/* application, None is removed from conferencesetup       */
/* ******************************************************* */

/*
Table values before FB 1744
1 - None
2 - Audio Only
3 - Audio-Video
*/


update Gen_MediaType_S set MediaType ='Audio-only' where ID = 1

update Gen_MediaType_S set MediaType ='Audio,Video' where ID = 2

delete from Gen_MediaType_S where ID = 3

/*

Table values after FB 1744

1 - Audio Only
2 - Audio-Video
*/


/* Conf_User_D, Conf_Room_D, conf_cascade_D & Loc_Room_D */

-- External Participant Records with value 1 (None) will be treated as Audio itself

Update Conf_User_D set audioorvideo=1 where audioorvideo=2 -- Audio

Update Conf_User_D set audioorvideo=2 where audioorvideo=3 -- Video


-- Room Endpoints

Update Conf_Room_D set audioorvideo=2 where audioorvideo=3 -- Video


-- records with value 1 (None) will be treated as Audio itself

--Update conf_cascade_D set audioorvideo=1 where audioorvideo=2 -- Audio

--Update conf_cascade_D set audioorvideo=2 where audioorvideo=3 -- Video


-- Location Table

Update Loc_Room_D set videoavailable=2 where videoavailable=3 -- Video



/* Restrict USage - AV Common Settings */

-- Records with value None will be treated as Video

-- All records with 1 as value needs to be updated as 5

Update Conf_AdvAVParams_D set mediaID=5 where mediaID=1

Update Conf_AdvAVParams_D set mediaID=1 where mediaID=2 -- Audio 

Update Conf_AdvAVParams_D set mediaID=2 where mediaID=3 -- Video

Update Conf_AdvAVParams_D set mediaID=2 where mediaID=5 -- None is converted to Video


--Tmp_AdvAVParams_D

-- Records with value None will be treated as Video

-- All records with 1 as value needs to be updated as 5

Update Tmp_AdvAVParams_D set mediaID=5 where mediaID=1

Update Tmp_AdvAVParams_D set mediaID=1 where mediaID=2 -- Audio 

Update Tmp_AdvAVParams_D set mediaID=2 where mediaID=3 -- Video

Update Tmp_AdvAVParams_D set mediaID=2 where mediaID=5 -- None is converted to Video

/* ********************* FB 1744 - end   ****************** */


/* ********************* FB 1767.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Dept_CustomAttr_D ADD
	 RoomApp int NULL,
     McuApp int NULL,
	 SystemApp int NULL,
     Scheduler int NULL,
	 Host int NULL,
	 Party int NULL,
     McuAdmin int NULL,
	 RoomAdmin int NULL
GO
COMMIT

GO
Update Dept_CustomAttr_D set RoomApp= 0,McuApp = 0,SystemApp = 0,Scheduler = 0,Host = 0,

  Party = 0,McuAdmin = 0,RoomAdmin = 0 where IncludeInEmail = 0

GO
Update Dept_CustomAttr_D set RoomApp= 0,McuApp = 0,SystemApp = 0,Scheduler = 0,Host = 0,

  Party = 1,McuAdmin = 1,RoomAdmin = 0 where IncludeInEmail = 1


/* ********************* FB 1767.sql - end   ****************** */


/* ********************* FB 1766_20101020.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	EnableIVR int NULL,
	IVRServiceName nvarchar(250) NULL
GO


ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_EnableIVR DEFAULT 0 FOR EnableIVR 
GO

COMMIT

/* Query to update existing records */

update Mcu_List_D set EnableIVR = 0

/* ********************* FB 1766_20101020.sql - end   ****************** */


/* ********************* FB 1781-GermanyTimeZone.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (75, N'W. Europe Standard Time', -1, N'Berlin', N'(GMT+01:00)', -60, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')

GO
COMMIT 

/* ********************* FB 1781-GermanyTimeZone.sql - end   ****************** */


/* ********************* FB 1786 & FB 1710.sql - start ****************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	iCalReqEmailID nvarchar(512) NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_iCalReqEmailID DEFAULT '' FOR iCalReqEmailID
GO
COMMIT

update Org_Settings_D Set iCalReqEmailID=''

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Queue_D
      ADD CONSTRAINT DF_Email_Queue_D_OrgID
          DEFAULT 11 FOR orgID
COMMIT

/* ** Update existing records if any with default organization id ** */
Update Email_Queue_D set orgID = 11


/* ********************* FB 1786 & FB 1710.sql - end   ****************** */


/* ********************* Express Interface Specific.sql - start ****************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO

ALTER TABLE dbo.Org_Settings_D ADD
	CustomAttributeLimit smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_CustomAttributeLimit DEFAULT 10 FOR CustomAttributeLimit
GO
COMMIT


/* Maximum limit is  10 */

Update Org_Settings_D set CustomAttributeLimit=10


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD
	CreateType smallint NULL
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD CONSTRAINT
	DF_Dept_CustomAttr_D_CreateType DEFAULT 0 FOR CreateType
GO
COMMIT


/* CreateType .. System -1 User -0 */
Update Dept_CustomAttr_D set CreateType=0

/*  Express Interface Specific Queries */

/*  User Roles Menumask Updation */

--Make sure the ROLEIDs are correct for the corresponding ROLE.

update usr_roles_d set roleMenuMask='8*240-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID=1

update usr_roles_d set roleMenuMask='8*248-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*0-6*63' where roleID=2

update usr_roles_d set roleMenuMask='8*252-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*1-6*63' where roleID=3

update usr_roles_d set roleMenuMask='8*136-4*0+8*2+3*0+2*0+8*0+2*0+2*3+2*0+1*0-6*63' where roleID=4

update usr_roles_d set roleMenuMask='8*136-4*0+8*4+3*0+2*0+8*0+2*3+2*0+2*0+1*0-6*63' where roleID=5

update usr_roles_d set roleMenuMask='8*136-4*0+8*1+3*0+2*0+8*0+2*0+2*0+2*3+1*0-6*63' where roleID=6


update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid


--Excute the below only once to create Express Profile

Declare @roldid int

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess) values(
@roldid ,'Express Profile','8*3-4*0+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*2',1,0,0)


/* Express Interface - Custom Attributes Specific */

DECLARE @orgID INT

DECLARE cusAttrCursor CURSOR for
SELECT orgID FROM Org_List_D 

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

Declare @id int

set @id = (select isnull(MAX(CustomAttributeId),0)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Work','Host',4,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Cell','Host',4,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Special Instructions','Special Instructions',10,0,0,0,1,@orgID,0,0,0,0,0,0,1,0,1)


/*
set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Session URL','Web Conference Instructions',7,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Web Conference #','Web Conference Instructions',4,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

*/

FETCH NEXT
FROM cusAttrCursor INTO @orgID
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor

/* ********************* Express Interface Specific.sql - end   ****************** */


/* ********************* IcalQueries_FB1782.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
SendIcal smallint NULL
GO
COMMIT

Update dbo.Org_Settings_D set SendIcal = 1

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
SendApprovalIcal smallint NULL
GO
COMMIT

Update dbo.Org_Settings_D set SendApprovalIcal = 1

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
IcalID varchar(MAX) NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
DF_Conf_Conference_D_IcalID DEFAULT '' FOR IcalID
GO
COMMIT

Update dbo.Conf_Conference_D set IcalID =''

/* ********************* IcalQueries_FB1782.sql - end   ****************** */


/* ********************* FB 1830.sql - start ****************** */

/* Gen_Language_S - Folder added */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Gen_Language_S ADD
	languagefolder nvarchar(100) NULL
GO
COMMIT



update Gen_Language_S set languagefolder = 'en' where LanguageID = 1

update Gen_Language_S set languagefolder = 'en-uk' where LanguageID = 2

update Gen_Language_S set languagefolder = 'sp' where LanguageID = 3

update Gen_Language_S set languagefolder = 'ch' where LanguageID = 4

update Gen_Language_S set languagefolder = 'fr' where LanguageID = 5

---------------------------------------------------------------------------

/* Conference Edit Mode */
/* 1 - New 2-Edit */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	confMode smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_confMode DEFAULT 1 FOR confMode
GO
COMMIT

/* Email Language in User Profile */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	EmailLangId int NULL
GO
COMMIT

update Usr_List_D set EmailLangId  = [Language]

/* Email Language in In-Active User */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	EmailLangId int NULL
GO
COMMIT

update Usr_Inactive_D set EmailLangId  = [Language]

/* Email Language in Org_Settings_D */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EmailLangId int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_EmailLangId DEFAULT 1 FOR EmailLangId
GO
COMMIT

update Org_Settings_D set EmailLangId = Language

/* Email_Type_S */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Type_S ADD
	emailcategory smallint NULL
GO
ALTER TABLE dbo.Email_Type_S ADD CONSTRAINT
	DF_Email_Type_S_emailcategory DEFAULT 0 FOR emailcategory
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE [dbo].[Email_Content_D](
	[EmailContentId] [int] IDENTITY(1,1) NOT NULL,
	[EmailLangId] [smallint] NULL CONSTRAINT [DF_Email_Content_D_EmailLangId]  DEFAULT ((1)),
	[EmailSubject] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailBody] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Placeholders] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailMode] [smallint] NULL,
	[Emailtypeid] [int] NULL,
	[EmailLanguage] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Email_Content_D_EmailLanguage]  DEFAULT (N'en')
) ON [PRIMARY]
GO
COMMIT

update Email_Content_D set EmailLanguage='en' where EmailLangId = 1


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Email_PlaceHolder_S
	(
	PlaceHolderId smallint NULL,
	Description nvarchar(100) NULL
	)  ON [PRIMARY]
GO
COMMIT


--Update queries for FB 1830 - Email Language:
----------------------------------------------
update Org_Settings_D set Language = 1
update Org_Settings_D set EmailLangId = Language
update Usr_List_D set EmailLangId = Language


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE [dbo].[Email_Language_D](
	[EmailLangId] [int] IDENTITY(101,1) NOT NULL,
	[EmailLanguage] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Email_Language_D_EmailLanguage]  DEFAULT (N'en'),
	[LanguageId] [smallint] NULL CONSTRAINT [DF_Email_Language_D_LanguageId]  DEFAULT ((1)),
	[orgid] [int] NULL CONSTRAINT [DF_Email_Language_D_orgid]  DEFAULT ((11))
) ON [PRIMARY]
GO
COMMIT

Update Email_Language_D set orgid = 11

-------------------------------------------------------------

/* FB 1830 Email Edit */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	NotifyOnEdit smallint NULL
GO
ALTER TABLE dbo.Conf_User_D ADD CONSTRAINT
	DF_Conf_User_D_NotifyOnEdit DEFAULT 1 FOR NotifyOnEdit
GO

COMMIT



update Conf_User_D  set NotifyOnEdit=1

/* ********************* FB 1830.sql - end   ****************** */


/* ********************* TechPhone.sql - start ****************** */

/* Tech support - Phone no correction */

update Sys_TechSupport_D set phone='888-698-7698' where email='support@myvrm.com'

/* ********************* TechPhone.sql - end   ****************** */


/* ********************* FB 1860_1.sql - start ****************** */

/*
   Monday, January 17, 201111:54:55 AM
   User: myvrm
   Server: gowniyanvm
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	MailBlocked smallint NULL,
	MailBlockedDate datetime NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_MailBlocked DEFAULT 0 FOR MailBlocked
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_MailBlockedDate DEFAULT (getutcdate()) FOR MailBlockedDate
GO
COMMIT

/* ********************* FB 1860_1.sql - end   ****************** */


/* ********************* FB 1860_2.sql - start ****************** */

/*
   Monday, January 17, 201111:52:02 AM
   User: myvrm
   Server: gowniyanvm
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MailBlockedDate datetime NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_MailBlockedDate DEFAULT (getutcdate()) FOR MailBlockedDate
GO
COMMIT

/* ********************* FB 1860_2.sql - end   ****************** */



/* ********************* FB 1860_3.sql - start ****************** */

/*
   Monday, January 17, 201111:46:59 AM
   User: myvrm
   Server: cqa1
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MailBlocked smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_MailBlocked DEFAULT 0 FOR MailBlocked
GO
COMMIT


/* ********************* FB 1860_3.sql - end   ****************** */


/* ********************* FB 1860_4.sql - start ****************** */

Update usr_list_d set mailblockeddate = Getutcdate()
Update Org_Settings_D set mailblockeddate = Getutcdate()

/* ********************* FB 1860_4.sql - end   ****************** */


/* ********************* FB 1861_1.sql - start ****************** */



/****** Object:  Table [dbo].[Holidays_details_D]    Script Date: 01/14/2011 03:23:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holidays_details_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[OrgId] [int] NOT NULL,
	[EntityID] [int] NULL,
	[EnitityType] [int] NULL,
	[Date] [datetime] NOT NULL,
	[HolidayType] [int] NOT NULL,
 CONSTRAINT [PK_Holidays_details_D] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


/* ********************* FB 1861_1.sql - end   ****************** */


/* ********************* FB 1861_2.sql - start ****************** */


/****** Object:  Table [dbo].[Holiday_Type_S]    Script Date: 01/14/2011 03:24:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holiday_Type_S](
	[HolidayType] [int] NOT NULL,
	[HolidayDescription] [nvarchar](50) NOT NULL,
	[Color] [nvarchar](20) NOT NULL,
	[Priority] [int] NOT NULL,
	[OrgId] [int] NOT NULL
	
) ON [PRIMARY]

/* ********************* FB 1861_2.sql - end   ****************** */


/* ********************* FB 1861_3.sql - start ****************** */


/****** Object:  Table [dbo].[Holiday_Entity_S]    Script Date: 01/14/2011 03:24:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holiday_Entity_S](
	[EntityType] [int] NULL,
	[EntityDescription] [nvarchar](50) NULL
) ON [PRIMARY]

/* ********************* FB 1861_3.sql - end   ****************** */


/* ********************* 1864_1.sql - start ****************** */

/*
   Thursday, January 13, 201110:17:52 PM
   User: myvrm
   Server: gowniyanvm
   Database: V2X
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isDedicatedEngineer smallint NULL,
	isLiveAssistant smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isDedicatedEngineer DEFAULT 0 FOR isDedicatedEngineer
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isLiveAssistant DEFAULT 0 FOR isLiveAssistant
GO
COMMIT

/* ********************* 1864_1.sql - end   ****************** */


/* ********************* FB 1864_2.sql - start ****************** */

/*
   Thursday, January 13, 20111:07:03 PM
   User: myvrm
   Server: gowniyanvm
   Database: V2X
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	isVIP smallint NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_isVIP DEFAULT 0 FOR isVIP
GO
COMMIT

update Loc_Room_D set isVIP = 0

/* ********************* FB 1864_2.sql - end   ****************** */


/* ********************* FB 1864_3.sql - start ****************** */
/*
   Thursday, January 13, 20113:43:21 PM
   User: myvrm
   Server: gowniyanvm
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	isVIP smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_isVIP DEFAULT 0 FOR isVIP
GO
COMMIT


/* ********************* FB 1864_3.sql - end   ****************** */


/* ********************* FB 1864_4.sql - start ****************** */

/*
   Thursday, January 13, 201111:28:47 AM
   User: myvrm
   Server: gowniyanvm
   Database: V2X
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isVIP smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isVIP DEFAULT 0 FOR isVIP
GO
COMMIT

update Conf_Conference_D set isVIP = 0

/* ********************* FB 1864_4.sql - end   ****************** */


/* ********************* FB 1865_1.sql - start ****************** */

/*
   Thursday, January 13, 20113:50:39 PM
   User: myvrm
   Server: gowniyanvm
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	isUniquePassword smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_isUniquePassword DEFAULT 0 FOR isUniquePassword
GO
COMMIT

/* ********************* FB 1865_1.sql - end   ****************** */


/* ********************* FB 1779 New Express Role.sql - start ****************** */



--Excute the below only once to create Express Profile

Declare @roldid int

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess) values(
@roldid ,'Express Profile Manage','8*66-4*0+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*2',1,0,0)

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess) values(
@roldid ,'Express Profile Advanced','8*82-4*4+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*2',1,0,0)

/* ********************* FB 1779 New Express Role.sql - end   ****************** */


/* ********************* FB 1901.sql - start ****************** */



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	isAssignedMCU smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_isAssignedMCU DEFAULT 1 FOR isAssignedMCU
GO
COMMIT

Update Org_Settings_D set isAssignedMCU = 1


/* ********************* FB 1901.sql - end   ****************** */


/* ********************* FB 1675.sql - start ****************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	disabled smallint NULL
GO
ALTER TABLE dbo.Conf_Room_D ADD CONSTRAINT
	DF_Conf_Room_D_disabled DEFAULT 0 FOR disabled
GO
COMMIT

Update dbo.Conf_Room_D set disabled = 0

/* ********************* FB 1675.sql - end   ****************** */


/* ********************* FB 1919.Sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_status
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_lecturemode
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_CreateType
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_ConfDeptID
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_ConfOrigin
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_orgId
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_LastRunDateTime
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_IcalID
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_confMode
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_isDedicatedEngineer
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_isLiveAssistant
GO
ALTER TABLE dbo.Conf_Conference_D
	DROP CONSTRAINT DF_Conf_Conference_D_isVIP
GO
CREATE TABLE dbo.Tmp_Conf_Conference_D
	(
	userid int NULL,
	confid int NOT NULL,
	externalname nvarchar(256) NULL,
	internalname nvarchar(256) NULL,
	password nvarchar(256) NULL,
	owner int NULL,
	confdate datetime NULL,
	conftime datetime NULL,
	timezone int NULL,
	immediate int NULL,
	audio int NULL,
	videoprotocol int NULL,
	videosession int NULL,
	linerate int NULL,
	recuring int NULL,
	duration int NULL,
	description nvarchar(2000) NULL,
	[public] int NULL,
	deleted smallint NULL,
	continous int NULL,
	transcoding int NULL,
	deletereason nvarchar(2000) NULL,
	instanceid int NOT NULL,
	advanced int NULL,
	totalpoints int NULL,
	connect2 int NULL,
	settingtime datetime NULL,
	videolayout int NULL,
	manualvideolayout int NULL,
	conftype int NULL,
	confnumname int NOT NULL,
	status int NULL,
	lecturemode int NULL,
	lecturer nvarchar(256) NULL,
	dynamicinvite int NULL,
	CreateType int NULL,
	ConfDeptID int NULL,
	ConfOrigin int NOT NULL,
	SetupTime datetime NULL,
	TearDownTime datetime NULL,
	orgId int NULL,
	LastRunDateTime datetime NULL,
	IcalID varchar(MAX) NULL,
	confMode smallint NULL,
	isDedicatedEngineer smallint NULL,
	isLiveAssistant smallint NULL,
	isVIP smallint NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_status DEFAULT ((0)) FOR status
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_lecturemode DEFAULT ((0)) FOR lecturemode
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_CreateType DEFAULT ((0)) FOR CreateType
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_ConfDeptID DEFAULT ((0)) FOR ConfDeptID
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_ConfOrigin DEFAULT ((0)) FOR ConfOrigin
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_orgId DEFAULT ((1)) FOR orgId
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_LastRunDateTime DEFAULT (getutcdate()) FOR LastRunDateTime
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_IcalID DEFAULT ('') FOR IcalID
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_confMode DEFAULT ((1)) FOR confMode
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isDedicatedEngineer DEFAULT ((0)) FOR isDedicatedEngineer
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isLiveAssistant DEFAULT ((0)) FOR isLiveAssistant
GO
ALTER TABLE dbo.Tmp_Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isVIP DEFAULT ((0)) FOR isVIP
GO
IF EXISTS(SELECT * FROM dbo.Conf_Conference_D)
	 EXEC('INSERT INTO dbo.Tmp_Conf_Conference_D (userid, confid, externalname, internalname, password, owner, confdate, conftime, timezone, immediate, audio, videoprotocol, videosession, linerate, recuring, duration, description, [public], deleted, continous, transcoding, deletereason, instanceid, advanced, totalpoints, connect2, settingtime, videolayout, manualvideolayout, conftype, confnumname, status, lecturemode, lecturer, dynamicinvite, CreateType, ConfDeptID, ConfOrigin, SetupTime, TearDownTime, orgId, LastRunDateTime, IcalID, confMode, isDedicatedEngineer, isLiveAssistant, isVIP)
		SELECT userid, confid, externalname, internalname, password, owner, confdate, conftime, timezone, immediate, audio, videoprotocol, videosession, linerate, recuring, duration, description, [public], deleted, continous, transcoding, deletereason, instanceid, advanced, totalpoints, connect2, settingtime, videolayout, manualvideolayout, conftype, confnumname, status, lecturemode, lecturer, dynamicinvite, CreateType, ConfDeptID, ConfOrigin, SetupTime, TearDownTime, orgId, LastRunDateTime, IcalID, confMode, isDedicatedEngineer, isLiveAssistant, isVIP FROM dbo.Conf_Conference_D WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Conf_Conference_D
GO
EXECUTE sp_rename N'dbo.Tmp_Conf_Conference_D', N'Conf_Conference_D', 'OBJECT' 
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	PK_Conf_Conference_D PRIMARY KEY CLUSTERED 
	(
	confid,
	instanceid
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


/* ********************* FB 1919.Sql - end   ****************** */


/* ********************* Conf_Reminders.sql - start ****************** */

/*
   Saturday, February 19, 20113:51:48 PM
   User: myvrm
   Server: demo2
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isReminder int NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isReminder DEFAULT 0 FOR isReminder
GO
COMMIT


Update  Conf_Conference_d set isReminder = 0

/* ********************* Conf_Reminders.sql - end   ****************** */


/* ********************* Org_Reminders.sql - start ****************** */

/*
   Saturday, February 19, 20113:53:32 PM
   User: myvrm
   Server: demo2
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	ReminderMask int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_ReminderMask DEFAULT 0 FOR ReminderMask
GO
COMMIT

Update Org_Settings_D set ReminderMask = 0

/* ********************* Org_Reminders.sql - end   ****************** */


/* ********************* FB 1939_UserRole.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Roles_D ADD
	createType int NULL
GO
DECLARE @v sql_variant 
SET @v = N'1 - system, 2 - custom'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Usr_Roles_D', N'COLUMN', N'createType'
GO
ALTER TABLE dbo.Usr_Roles_D ADD CONSTRAINT
	DF_Usr_Roles_D_createType DEFAULT 2 FOR createType
GO
COMMIT

Update usr_roles_d set createType = 1 where roleName IN ('General User', 'Organization Administrator', 'Site Administrator', 'Catering Administrator', 'Inventory Administrator', 'Housekeeping Administrator', 'Express Profile', 'Express Profile Manage', 'Express Profile Advanced')

Update usr_roles_d set createType = 2 where  (createType <> 1 or createType is null)

Update usr_roles_d set locked = 1 where createType = 1

/* ********************* FB 1939_UserRole.sql - end   ****************** */


/* ********************* 1907.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	EnableRecording smallint NULL
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_EnableRecording DEFAULT 0 FOR EnableRecording
GO
COMMIT

/* Update existing data with default value 0 */
Update Mcu_List_D Set EnableRecording = 0

/* ********************* 1907.sql - end   ****************** */


/* ********************* FB 1920.sql - start ****************** */

/*
   Wednesday, February 16, 20116:09:09 AM
   User: myvrm
   Server: SERVER2004\SQL2005
   Database: TestDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	isPublic smallint NOT NULL CONSTRAINT DF_Mcu_List_D_Public DEFAULT 0
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Mcu_List_D', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Mcu_List_D', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Mcu_List_D', 'Object', 'CONTROL') as Contr_Per 


/* ********************* FB 1920.sql - end   ****************** */



/* ********************* MSE8000.sql - start ****************** */

UPDATE  Mcu_Vendor_S  SET BridgeInterfaceId = 6  WHERE  id = 6

/* ********************* MSE8000.sql - end   ****************** */



/* ********************* ReleaseEmail.sql - start ****************** */

/*
   Monday, March 14, 20117:45:01 PM
   User: myvrm
   Server: gowniyanvm
   Database: V2X
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Queue_D ADD
	Release smallint NULL
GO
ALTER TABLE dbo.Email_Queue_D ADD CONSTRAINT
	DF_Email_Queue_D_Release DEFAULT 0 FOR Release
GO
COMMIT

Update Email_Queue_D set release = 0

/* ********************* ReleaseEmail.sql - end   ****************** */


/* ********************* FB 1522_View-Only Role.sql - start ****************** */



--Execute the below only once to create View-Only role

Declare @roldid int

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess,createType) values(
@roldid ,'View-Only','8*208-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14',1,0,0,1)


/* ********************* FB 1522_View-Only Role.sql - end   ****************** */


/* ********************* ISDN_Gateway.sql - start ****************** */

/*
   Monday, March 14, 20119:42:38 AM
   User: myvrm
   Server: cqa1
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	ISDNGateway varchar(50) NULL
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_ISDNGateway DEFAULT '' FOR ISDNGateway
GO
COMMIT

/* ********************* ISDN_Gateway.sql - end   ****************** */



/* ********************* FB 1750_SelectCalendarDetails(Alter).sql - (NGC Specfic) - start ****************** */


/* ********************* FB 1750_SelectCalendarDetails(Alter).sql - (NGC Specfic) - end ****************** */


/* ********************* FB 2007 - start ****************** */

/*
   30 March 201106:07:10 AM
   User: 
   Server: staginguk
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	LaunchBuffer int NULL
GO
ALTER TABLE dbo.Sys_Settings_D ADD CONSTRAINT
	DF_Sys_Settings_D_LaunchBuffer DEFAULT 5 FOR LaunchBuffer
GO
COMMIT

/* ********************* FB 2007 - end   ****************** */


/* ********************* ISDN_Prefix.sql - start ****************** */

/*
   Tuesday, March 29, 20114:44:30 AM
   User: myvrm
   Server: VENI\SQLEXPRESS
   Database: cqa1
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	ISDNAudioPrefix varchar(5) NULL,
	ISDNVideoPrefix varchar(5) NULL
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_ISDNAudioPrefix DEFAULT '' FOR ISDNAudioPrefix
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_ISDNVideoPrefix DEFAULT '' FOR ISDNVideoPrefix
GO
COMMIT

/* ********************* ISDN_Prefix.sql - end   ****************** */


/* ********************* FB 1979.sql - start ****************** */

/* **************** FB 1979 - Support for mobile devices in License. ************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

GO
ALTER TABLE dbo.Usr_List_D ADD
	enableMobile int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableMobile DEFAULT 0 FOR enableMobile
GO
COMMIT

  /* Update existing data with default value 0 */

Update Usr_List_D Set enableMobile = 0 



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	enableMobile int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableMobile DEFAULT 0 FOR enableMobile
GO
COMMIT

  /* Update existing data with default value 0 */

Update Usr_Inactive_D Set enableMobile = 0




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MobileUserLimit int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_MobileUserLimit DEFAULT 0 FOR MobileUserLimit
GO
COMMIT

/* Update existing data with default value 0 */

Update Org_Settings_D Set MobileUserLimit = 0 


/* 
Sample License XML:
-------------------

<myVRMSiteLicense><IR>1-myVRMQA</IR><Site><ExpirationDate>11/30/2014</ExpirationDate><MaxOrganizations>5</MaxOrganizations><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID></ID><ID></ID></ProcessorIDs><MACAddresses><ID></ID><ID></ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>150</MaxNonVideoRooms><MaxVideoRooms>400</MaxVideoRooms></Rooms><Hardware><MaxMCUs>25</MaxMCUs><MaxEndpoints>400</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>200</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>10</MaxMobileUsers></Users><Modules><MaxFacilitiesModules>5</MaxFacilitiesModules><MaxCateringModules>5</MaxCateringModules><MaxHousekeepingModules>5</MaxHousekeepingModules><MaxAPIModules>1</MaxAPIModules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Modules></Organizations></myVRMSiteLicense>
*/
/* ********************* FB 1979.sql - end   ****************** */


/* ********************* FB 1970.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Custom_Lang_D
	(
	UID int NOT NULL IDENTITY (1, 1),
	LanguageId smallint NOT NULL,
	CustomAttributeID int NOT NULL,
	DisplayTitle nvarchar(250) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Custom_Lang_D ADD CONSTRAINT
	DF_Custom_Lang_D_LanguageId DEFAULT 1 FOR LanguageId
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_Option_D ADD
	LanguageID smallint NOT NULL CONSTRAINT DF_Dept_CustomAttr_Option_D_LanguageID DEFAULT 1,
	UID int NOT NULL IDENTITY (1, 1)
COMMIT


/* ********************* FB 1970.sql - end   ****************** */


/* ********************* FB 2013.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD
	IncludeInCalendar smallint NULL
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD CONSTRAINT
	DF_Dept_CustomAttr_D_IncludeInCalendar DEFAULT 0 FOR IncludeInCalendar
COMMIT

update Dept_CustomAttr_D set IncludeInCalendar = 1 where createtype=1

update Dept_CustomAttr_D set IncludeInCalendar = 0 where createtype=0

/* ********************* FB 2013.sql - end   ****************** */


/* ********************* FB 2016.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	ConfServiceID int NULL
GO
COMMIT


/* ********************* FB 2016.sql - end   ****************** */



/* ********************* FB 1907 & 2057 .sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	LPR smallint NULL
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_LPR DEFAULT 0 FOR LPR
GO
COMMIT


Update MCU_LIst_D set LPR = 0

/* ********************* FB 1907 & 2057 .sql - end   ****************** */


/* ********************* FB 2027.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Dept_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* QUERY FOR DELETE TEMPLATE COMMAND START*/
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tmp_Room_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tmp_Participant_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT
/* QUERY FOR DELETE TEMPLATE COMMAND END*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Acc_Balance_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Acc_GroupList_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Grp_Participant_D ADD
	ID int NOT NULL IDENTITY (1, 1)
GO
COMMIT




--FB 2027 - SetSuperAdmin start

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ldap_ServerConfig_D ADD
	UID int NOT NULL CONSTRAINT DF_Ldap_ServerConfig_D_UID DEFAULT 1
GO
COMMIT





/* QUERY FOR  SetTerminalControl COMMAND start*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Monitor_D ADD
	Uid int NOT NULL IDENTITY (1, 1)
COMMIT


/* QUERY FOR  SetTerminalControl COMMAND END*/


/* QUERY FOR  GetLogPreferences COMMAND start*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Err_LogPrefs_S ADD
	UID int NOT NULL IDENTITY (1, 1)
GO
COMMIT

/* QUERY FOR  GetLogPreferences COMMAND end*/

/* ********************* FB 2027.sql - end   ****************** */


/* ********************* FB 2140 - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE Icons_Ref_S
	(
	IconID int NOT NULL IDENTITY (1, 1),
	IconUri nvarchar(MAX) NULL,
	Label nvarchar(300) NULL,
	IconTarget nvarchar(MAX) NULL
	) 
GO
ALTER TABLE Icons_Ref_S ADD CONSTRAINT
	PK_Icons_Ref_S PRIMARY KEY CLUSTERED 
	(
	IconID
	) 
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE User_Lobby_D
	(
	UId int NOT NULL IDENTITY (1, 1),
	UserID int NOT NULL default 11,
	IconID int NOT NULL default 0,
	GridPosition int NOT NULL default 0,
	Label nvarchar(300) NULL
	)
COMMIT


insert into Icons_Ref_S values ('../en/img/calendar.jpg', 'Calendar', '../en/personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v')
insert into Icons_Ref_S values ('../en/img/conference_new.jpg', 'New Conference', '../en/ConferenceSetup.aspx?t=n&op=1')
insert into Icons_Ref_S values ('../en/img/preference.jpg', 'My Preferences', '../en/ManageUserProfile.aspx')
insert into Icons_Ref_S values ('../en/img/report.jpg', 'Reports', '../en/GraphicalReport.aspx')
insert into Icons_Ref_S values ('../en/img/template.jpg', 'Manage Template', '../en/ManageTemplate.aspx')
insert into Icons_Ref_S values ('../en/img/group.jpg', 'Manage Groups', '../en/ManageGroup.aspx')
insert into Icons_Ref_S values ('../en/img/codec.jpg', 'Manage My Endpoints', '../en/EndpointList.aspx?t=')
insert into Icons_Ref_S values ('../en/img/diagnostic.jpg', 'My System Diagnostic', '../en/EventLog.aspx')
insert into Icons_Ref_S values ('../en/img/MCU.jpg', 'Manage My MCUs', '../en/ManageBridge.aspx')
insert into Icons_Ref_S values ('../en/img/rooms.jpg', 'Manage Rooms', '../en/manageroom.aspx')
insert into Icons_Ref_S values ('../en/img/location.jpg', 'Manage My Location Tree', '../en/ManageTiers.aspx')
insert into Icons_Ref_S values ('../en/img/activeUsers.jpg', 'Active Users', '../en/ManageUser.aspx?t=1')
insert into Icons_Ref_S values ('../en/img/bulkTool.jpg', 'Bulk Tool', '../en/allocation.aspx')
insert into Icons_Ref_S values ('../en/img/departments.jpg', 'Departments', '../en/ManageDepartment.aspx')
insert into Icons_Ref_S values ('../en/img/guests.jpg', 'Guests', '../en/ManageUser.aspx?t=2')
insert into Icons_Ref_S values ('../en/img/inactiveUsers.jpg', 'Inactive Users', '../en/ManageUser.aspx?t=3')
insert into Icons_Ref_S values ('../en/img/LDAP.jpg', 'LDAP Directory Import', '../en/LDAPImport.aspx')
insert into Icons_Ref_S values ('../en/img/userRoles.jpg', 'Roles', '../en/manageuserroles.aspx')
insert into Icons_Ref_S values ('../en/img/hasAdminTemplates.jpg', 'Templates', '../en/ManageUserTemplatesList.aspx')
insert into Icons_Ref_S values ('../en/img/options.jpg', 'My Organization Options', '../en/mainadministrator.aspx')
insert into Icons_Ref_S values ('../en/img/settings.jpg', 'My Organization Settings', '../en/OrganisationSettings.aspx')
insert into Icons_Ref_S values ('../en/img/inventory.jpg', 'Manage Inventory', '../en/InventoryManagement.aspx?t=1')
insert into Icons_Ref_S values ('../en/img/workorder2.jpg', 'Manage My Inventory Work Order', '../en/ConferenceOrders.aspx?t=1')
insert into Icons_Ref_S values ('../en/img/catering2.jpg', 'Manage Catering', '../en/InventoryManagement.aspx?t=2')
insert into Icons_Ref_S values ('../en/img/catering_order.jpg', 'Manage My Catering Work Order', '../en/ConferenceOrders.aspx?t=2')
insert into Icons_Ref_S values ('../en/img/house_keeping.jpg', 'Manage House Keeping', '../en/InventoryManagement.aspx?t=3')
insert into Icons_Ref_S values ('../en/img/workorder.jpg', 'Manage My House Keeping Work Order', '../en/ConferenceOrders.aspx?t=3')
insert into Icons_Ref_S values ('../en/img/system_setting.jpg', 'My Overall Site Settings', '../en/SuperAdministrator.aspx')
insert into Icons_Ref_S values ('../en/img/contact.jpg', 'Feedback', 'javascript: openFeedback()')
insert into Icons_Ref_S values ('../en/img/help.jpg', 'Help', 'javascript: openhelp()')
insert into Icons_Ref_S values ('../en/img/approval.jpg', 'Approval Conference', '../en/ConferenceList.aspx?t=6')
insert into Icons_Ref_S values ('../en/img/email_mgt.jpg', 'Managing Email Queue', '../en/ViewBlockedMails.aspx?tp=u')
insert into Icons_Ref_S values ('../en/img/lookandfeel.jpg', 'My Organization UI', '../en/UISettings.aspx')
insert into Icons_Ref_S values ('../en/img/audit.jpg', 'System Audit', '../en/SystemAudit.aspx')
insert into Icons_Ref_S values ('../en/img/dashboard.jpg', 'Dashboard', '../en/SettingSelect2.aspx')
insert into Icons_Ref_S values ('../en/img/summary.jpg', 'Search', '../en/SearchConferenceInputParameters.aspx')

/* ********************* FB 2140 - end   ****************** */


/* ********************* MultilingualScripts.sql - start ****************** */


/*
SQL SCRIPTS for Multilingual
*/

/* 0. Org_Settings_D.sql - start */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	isMultiLingual smallint NULL
GO
COMMIT


update Org_Settings_D set isMultiLingual = 1

/* 0. Org_Settings_D.sql - end */



/* *****   1. Launguage_Translation_Text.sql - start **** */


GO
/****** Object:  Table [dbo].[Launguage_Translation_Text]    Script Date: 02/16/2011 01:17:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Launguage_Translation_Text](
	[uid] int NOT NULL IDENTITY (1, 1),
	[LanguageID] [smallint] NULL,
	[Text] [nvarchar](max) NULL,
	[TranslatedText] [nvarchar](max) NULL
) ON [PRIMARY]

/* *****   1. Launguage_Translation_Text.sql - end **** */


/* *****   2. FB 1830 - Translation Text.sql - start **** */

/* FB 1830 - French translated texts */

Truncate table Launguage_Translation_Text

Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,' (Open for Registration)',' (Open for Registration)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,' (Work orders deliver by date/time do not match with conference date/time.)',' (Work orders deliver by date/time do not match with conference date/time.)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,' (Work orders start/end date/time do not match with conference start/end date/time.)',' (Work orders start/end date/time do not match with conference start/end date/time.)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'(No custom options)','(No custom options)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'(No participants)','(No participants)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'(No rooms)','(No rooms)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'(No work orders)','(No work orders)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Address Type and Protocol don�t match. Please verify','Address Type and Protocol don�t match. Please verify')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Aggregate Usage Report','Aggregate Usage Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'All Audio/Visual Work Orders','All Audio/Visual Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'All Housekeeping Work Orders','All Housekeeping Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Apply Theme','Apply Theme')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'At least one item should have a requested quantity greater than 0.','At least one item should have a requested quantity greater than 0.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'At least one menu should have a requested quantity > 0.','At least one menu should have a requested quantity > 0.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'At most 5 profiles can be created.','At most 5 profiles can be created.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Attach Files','Attach Files')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Audio Only Conference','Audio Only Conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Audio/Video Conference','Audio/Video Conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Audio/Visual ','Audio/Visual ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Audio/Visual Work Orders','Audio/Visual Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Auto select...','Auto select...')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Billing Code already exists in the list','Billing Code already exists in the list')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Billing Code description should be within 35 characters','Billing Code description should be within 35 characters')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Cancel','Cancel')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Cascade','Cascade')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Catering ','Catering ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Catering Providers','Catering Providers')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Catering Work Orders','Catering Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'CC','CC')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Click Edit/Create button to save these changes.','Click Edit/Create button to save these changes.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders ','Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Click Update to re-calculate total OR click Edit to update this Workorder.','Click Update to re-calculate total OR click Edit to update this Workorder.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Clone All','Clone All')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Close','Close')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Codian MCU Configuration','Codian MCU Configuration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Compare Conference Room Resource','Compare Conference Room Resource')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Completed','Completed')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Conference Details','Conference Details')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Conference End','Conference End')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Conference Room Resource','Conference Room Resource')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Conference Start','Conference Start')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Conference Title','Conference Title')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Congratulation! Your conference has been submitted successfully and is currently in pending status a','Congratulation! Your conference has been submitted successfully and is currently in pending status awaiting administrative approval.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Congratulations! Your Conference has been scheduled successfully.','Congratulations! Your Conference has been scheduled successfully.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Connect','Connect')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create','Create')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create Group','Create Group')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New','Create New')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New A/V Set','Create New A/V Set')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New Catering Provider','Create New Catering Provider')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New Endpoint','Create New Endpoint')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New H/K Group','Create New H/K Group')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New MCU','Create New MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New Organization','Create New Organization')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New User','Create New User')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New User Template','Create New User Template')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Custom Date Conferences Total Usage Report','Custom Date Conferences Total Usage Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Congratulations! Your conference has been scheduled successfully. Email notifications have been sent','Congratulations! Your conference has been scheduled successfully. Email notifications have been sent to participants')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Congratulations! Your conference has been scheduled successfully. Email notifications have been sent','Congratulations! Your conference has been scheduled successfully. Email notifications have been sent to participants.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Congratulations! Your conference has been submitted successfully and is currently in pending status ','Congratulations! Your conference has been submitted successfully and is currently in pending status awaiting administrative approval.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Custom option description should be within 35 characters','Custom option description should be within 35 characters')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Custom option title already exists in the list','Custom option title already exists in the list')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Custom Option Value','Custom Option Value')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Default Protocol does not match with selected Address Type.','Default Protocol does not match with selected Address Type.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Delete All','Delete All')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Delete Log','Delete Log')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Deleted','Deleted')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Disconnected','Disconnected')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'DLL Name','DLL Name')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Edit','Edit')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Edit Existing User','Edit Existing User')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Edit Group','Edit Group')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Edit Organization Profile','Edit Organization Profile')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Edit User Template','Edit User Template')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Enabled','Enabled')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Enter requested quantity for corresponding items.','Enter requested quantity for corresponding items.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Error deleteting work order.','Error deleteting work order.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Error Uploading Image. Please check your VRM Administrator.','Error Uploading Image. Please check your VRM Administrator.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Event level to be logged','Event level to be logged')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'External Attendee','External Attendee')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'File attachment is greater than 10MB. File has not been uploaded.','File attachment is greater than 10MB. File has not been uploaded.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'File trying to upload already exists','File trying to upload already exists')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Guest','Guest')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Guest Address Book','Guest Address Book')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Housekeeping ','Housekeeping ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Housekeeping Groups','Housekeeping Groups')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Housekeeping Work Orders','Housekeeping Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Illegal Search','Illegal Search')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Image size is greater than 5MB. File has not been uploaded.','Image size is greater than 5MB. File has not been uploaded.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Immediate Conference','Immediate Conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Include in Email','Include in Email')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid Date Range: FROM date should be prior to TO date.','Invalid Date Range: FROM date should be prior to TO date.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid duration','Invalid duration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid Duration.Conference duration should be minimum of 15 mins.','Invalid Duration.Conference duration should be minimum of 15 mins.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid Interface Type and Address Type.','Invalid Interface Type and Address Type.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid IP Address.','Invalid IP Address.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid ISDN Address','Invalid ISDN Address')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid MPI Address','Invalid MPI Address')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid selection for Address Type, Protocol and Connection Type.','Invalid selection for Address Type, Protocol and Connection Type.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please associate at least one menu with each work order.','Please associate at least one menu with each work order.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please check Start and Completed Date/Time for this workorder.','Please check Start and Completed Date/Time for this workorder.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please check the password. It must be unique','Please check the password. It must be unique')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please check the start and end time for this workorder.','Please check the start and end time for this workorder.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please enter a quantity for each selected item. Each item should have a quantity > 0','Please enter a quantity for each selected item. Each item should have a quantity > 0')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI proto','Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI protocol.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid Selection.','Invalid Selection.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid Start and End Range','Invalid Start and End Range')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid Start Date or Time. It should be greater than current time in','Invalid Start Date or Time. It should be greater than current time in')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Invalid User Type','Invalid User Type')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Inventory Set has been changed and no longer belongs to the selected room.','Inventory Set has been changed and no longer belongs to the selected room.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Inventory Sets','Inventory Sets')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Item Name already exists in the list','Item Name already exists in the list')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Last Month Conferences Total Usage Report','Last Month Conferences Total Usage Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Last Updated Date','Last Updated Date')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Last Week Conferences Total Usage Report','Last Week Conferences Total Usage Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Manage Departments','Manage Departments')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Manage Guests','Manage Guests')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Manage Inactive Users','Manage Inactive Users')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Manage MCUs','Manage MCUs')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Manage Tiers','Manage Tiers')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Manage Users','Manage Users')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Max. length for start and end range is 22 characters.','Max. length for start and end range is 22 characters.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'MCU','MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'MCU is mandatory for MPI protocol','MCU is mandatory for MPI protocol')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Modify work order to conform to your room changes. Work orders and Room names are as follows:','Modify work order to conform to your room changes. Work orders and Room names are as follows:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Module Type','Module Type')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'More than 10 IP Services can not be added.','More than 10 IP Services can not be added.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'More than 10 ISDN Services can not be added.','More than 10 ISDN Services can not be added.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'My Incomplete Audio/Visual Work Orders','My Incomplete Audio/Visual Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'My Incomplete Catering Work Orders','My Incomplete Catering Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'My Incomplete Housekeeping Work Orders','My Incomplete Housekeeping Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'My Pending Audio/Visual Work Orders','My Pending Audio/Visual Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'My Pending Catering Work Orders','My Pending Catering Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'My Pending Housekeeping Work Orders','My Pending Housekeeping Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'My Preferences','My Preferences')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'myVRM Address Book','myVRM Address Book')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No','No')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Conference-AV','No Conference-AV')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Custom Options found.','No Custom Options found.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Data Found','No Data Found')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Logs Found.','No Logs Found.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Recurrence','No Recurrence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'None','None')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Not Started','Not Started')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'On MCU','On MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Ongoing','Ongoing')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Only one image can be uploaded for footer information.','Only one image can be uploaded for footer information.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Only two Endpoints can be selected for Point-To-Point Conference.','Only two Endpoints can be selected for Point-To-Point Conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Operation Successful!','Operation Successful')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Operation Successful! Please logout and re-login to see the changes.','Operation Successful! Please logout and re-login to see the changes.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Operation Successful! Reminder email has been sent to selected participant.','Operation Successful! Reminder email has been sent to selected participant.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Operation Successful! Reminder email has been sent to the person-in-charge of the work order.','Operation Successful! Reminder email has been sent to the person-in-charge of the work order.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Option name already exists in the list','Option name already exists in the list')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Overwrite events older than','Overwrite events older than')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Page(s):','Page(s):')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Paused','Paused')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Pending','Pending')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please add atleast one option to the custom option.','Please add atleast one option to the custom option.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please enter email address for test mail','Please enter email address for test mail')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please enter the ExchangeID for the profile name ','Please enter the ExchangeID for the profile name ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please enter TO and FROM dates.','Please enter TO and FROM dates.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please enter valid date range of at most a week.','Please enter valid date range of at most a week.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please fill in all the values for Change Request','Please fill in all the values for Change Request')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please respond to the following conference','Please respond to the following conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please save or cancel current workorder prior to editing or deleting existing workorders.','Please save or cancel current workorder prior to editing or deleting existing workorders.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please save/update or cancel current workorder prior to editing or deleting existing workorders.','Please save/update or cancel current workorder prior to editing or deleting existing workorders.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select a Housekeeping Group now.','Please select a Housekeeping Group now.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select a Room','Please select a Room')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select a valid IP Address.','Please select a valid IP Address.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select a valid ISDN Address.','Please select a valid ISDN Address.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select an Address Type.','Please select an Address Type.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select an entity','Please select an entity')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select at least one room from �Select Rooms� tab in order to create a work order.','Please select at least one room from �Select Rooms� tab in order to create a work order.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select atleast one Department.','Please select atleast one Department.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select atleast one Endpoint.','Please select atleast one Endpoint.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select atleast one MCU.','Please select atleast one MCU.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select atleast one Room.','Please select atleast one Room.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select atleast one Tier.','Please select atleast one Tier.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select MCU.','Please select MCU.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select the corresponding menu for a valid quantity.','Please select the corresponding menu for a valid quantity.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select...','Please select...')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please specify Quantity and either click update to calculate total price for this work order or clic','Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please visit the Advanced Audio/Video Settings tab','Please visit the Advanced Audio/Video Settings tab')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Point-To-Point Conference','Point-To-Point Conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Polycom MGC Configuration','Polycom MGC Configuration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Polycom RMX Configuration','Polycom RMX Configuration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Post Conference End','Post Conference End')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Pre Conference Start','Pre Conference Start')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'PRI Allocation Report','PRI Allocation Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Radvision MCU Configuration','Radvision MCU Configuration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Report is not ready to view.','Report is not ready to view.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Room','Room')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Room Attendee','Room Attendee')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Room Conference','Room Conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Rooms Found :','Rooms Found :')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Running','Running')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Scheduled','Scheduled')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Search Result(s) for Audio/Visual Work Orders','Search Result(s) for Audio/Visual Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Search Result(s) for Catering Work Orders','Search Result(s) for Catering Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Search Result(s) for Housekeeping Work Orders','Search Result(s) for Housekeeping Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Search Results','Search Results')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'See Recurring Pattern','See Recurring Pattern')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Select a Set from the Drop down list.','Select a Set from the Drop down list.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Select Caller/Callee','Select Caller/Callee')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Selected bridge for this template does not support MPI calls.','Selected bridge for this template does not support MPI calls.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Set Customized Instances','Set Customized Instances')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Show Uploaded Files','Show Uploaded Files')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Some Additional Comments fields are missing. Please contact your VRM Administrator.','Some Additional Comments fields are missing. Please contact your VRM Administrator.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Status being Updated.','Status being Updated.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Stopped','Stopped')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Submit','Submit')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Submit Conference','Submit Conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Tandberg MCU Configuration','Tandberg MCU Configuration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Terminate','Terminate')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Terminated','Terminated')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no Audio/Visual work orders associated with this conference.','There are no Audio/Visual work orders associated with this conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no Catering work orders associated with this conference.','There are no Catering work orders associated with this conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no Housekeeping Groups available for this room. Please select another room.','There are no Housekeeping Groups available for this room. Please select another room.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no Housekeeping work orders associated with this conference.','There are no Housekeeping work orders associated with this conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no menus associated with the selected Service Type for this location. Please select a new ','There are no menus associated with the selected Service Type for this location. Please select a new Service Type for this location OR click Cancel to return to the catering work orders summary page for this conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no menus associated with the selected Service Type for this location. Please select a new ','There are no menus associated with the selected Service Type for this location. Please select a new Service Type for this location OR click Cancel to return to the catering work orders summary page for this conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no Workorders associated with this conference','There are no Workorders associated with this conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'This value has already been taken.','This value has already been taken.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'This Week Conferences Total Usage Report','This Week Conferences Total Usage Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Title','Title')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'To add a new work order, click the Add New Work Order button. To modify an existing work order, clic','To add a new work order, click the Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'To add a new work order, click the button below.','To add a new work order, click the button below.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'To add a new work order, click the button below. OR click EDIT to edit an existing work order.','To add a new work order, click the button below. OR click EDIT to edit an existing work order.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'To create a new work order, click on Add New Work Order button.','To create a new work order, click on Add New Work Order button.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'To create a new work order, click on Add New Work Order button. To modify an existing work order, cl','To create a new work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'To create a new work order, click the button below.','To create a new work order, click the button below.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'To create a new work order, please click on the button below.','To create a new work order, please click on the button below.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Today�s Audio/Visual Work Orders','Today�s Audio/Visual Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Today�s Catering Work Orders','Today�s Catering Work Orders')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Today�s Housekeeping Work Order','Today�s Housekeeping Work Order')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Totals:','Totals:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Un Mute','Un Mute')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'UnBlock','UnBlock')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Undefined','Undefined')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Undelete','Undelete')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Update','Update')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Usage By Room','Usage By Room')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'User','User')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Version Numbe','Version Numbe')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Version Number','Version Number')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'View Conference','View Conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'View Conflict','View Conflict')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'View Menus','View Menus')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'VRM Service is not installed','VRM Service is not installed')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'VTC Calendar','VTC Calendar')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'VTC Contact List','VTC Contact List')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'VTC Daily Schedule','VTC Daily Schedule')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'VTC Resource Allocation Report','VTC Resource Allocation Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'We are sorry that you will not be able to join the following conference','We are sorry that you will not be able to join the following conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Workorders can only be created with rooms selected for this conference.','Workorders can only be created with rooms selected for this conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Year To Date Conferences Total Usage Report','Year To Date Conferences Total Usage Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Yes','Yes')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Yesterday Conference Total Usage Report','Yesterday Conference Total Usage Report')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'You have no Pending or Future Conferences.','You have no Pending or Future Conferences.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Items...','No Items...')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'previous page','previous page')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Already on last page','Already on last page')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'next page','next page')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Already on first page','Already on first page')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'N/A','N/A')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Session expired','Session expired')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'VRM system is unavailable during the chosen date and time. Please check VRM system availability and ','VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again. Hours of operation:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Closed','Closed')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Any','Any')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Connecting','Connecting')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Disconnecting','Disconnecting')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,' (Open for Registration)','(Ouvert a tous)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,' (Work orders deliver by date/time do not match with conference date/time.)','Les informations temporels du Bon de Commande ne corresponded pas a celles de la Conf�rence')

/**** French *****/
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,' (Work orders start/end date/time do not match with conference start/end date/time.)','Les informations temporels du Bon de Commande ne corresponded pas a celles de la Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'(No custom options)','(Aucune Personalisation)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'(No participants)','(Auncun Participant)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'(No rooms)','(Aucune Salle de Conf�rence)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'(No work orders)','(Auncum Bon de Commande)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Address Type and Protocol don�t match. Please verify','Le type d�addresse et protocole s�lection�es sont incompatibles. Veuillez corriger')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Aggregate Usage Report','Rapport d�utilisation combin�  ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'All Audio/Visual Work Orders','Liste des Bons de Commande Audio/Visuel')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'All Housekeeping Work Orders','Liste des Bons de Commande Menager')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Apply Theme','Apposer ce th�me')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'At least one item should have a requested quantity greater than 0.','Au moins un �l�ment dois avoir une quantit� sup�rieure a zero')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'At least one menu should have a requested quantity > 0.','Il est indispensable qu�au moins un menu est au moins un �l�ment s�lection�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'At most 5 profiles can be created.','Un maximum de 5 profiles peuvent etre cr�er')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Attach Files','Attcher un fichier')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Audio Only Conference','Conf�rence Audio')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Audio/Video Conference','Conf�rence Audio / Visuel')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Audio/Visual ','Audio / Visuel')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Audio/Visual Work Orders','Bons de Commande Audio/Visuel')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Auto select...','S�lection Automatique�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Billing Code already exists in the list','Ce code existe d�ja')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Billing Code description should be within 35 characters','La description ne dois pas d�passer 35 characteres')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Cancel','Abandonnner')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Cascade','Cascade')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Catering ','Restauration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Catering Providers','Restaurateurs')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Catering Work Orders','Bons de Commande Alimentaire')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'CC','CC')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Click Edit/Create button to save these changes.','Veuillez s�lectionner l�option "Modifier" pour sauvegarder vos modifications.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders ','Veuillez presser "R�viser" pour sauver vos changements. Alternativement selectioner "Abandon" pour retourner a votre Bon de Command pour cette conference')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Click Update to re-calculate total OR click Edit to update this Workorder.','Veuillez presser "R�viser" pour r�actualizer votre Bon de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Clone All','Duplication globale')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Close','Ferm�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Codian MCU Configuration','Configuration duCodian MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Compare Conference Room Resource','Comparaison des resources associ�es a ces Salles de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Completed','Compl�t�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Conference Details','D�tails de votre Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Conference End','Fin de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Conference Room Resource','Resources associ�es a ces Salles de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Conference Start','D�but de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Conference Title','Titre de la Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Congratulation! Your conference has been submitted successfully and is currently in pending status a','F�licitation! Votre conf�rence a �t� cr�e, et est actuellement "en-attente" de validation de votre administrateur.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Congratulations! Your Conference has been scheduled successfully.','F�licitation! Votre conf�rence a �t� cr�e.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Congratulations! Your conference has been scheduled successfully. Email notifications have been sent','F�licitation! Votre conf�rence a �t� cr�e, et tous les emails n�cessaire ont �t� soumis.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Congratulations! Your conference has been scheduled successfully. Email notifications have been sent','F�licitation! Votre conf�rence a �t� cr�e, et tous les emails n�cessaire ont �t� soumis.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Congratulations! Your conference has been submitted successfully and is currently in pending status ','F�licitation! Votre conf�rence a �t� cr�e, et est actuellement "en-attente" de validation de votre administrateur.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Connect','Connecter')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create','Cr�ation')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create Group','Cr�ation d�un groupe')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New','Cr�ation')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New A/V Set','Cr�ation d�un nouveau set Audio/Visuel')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New Catering Provider','Cr�ation d�un nouveau Restaurateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New Endpoint','Cr�ation d�un nouveau Codec')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New H/K Group','TEMPORARY FRENCH TEXT (until I do it)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New MCU','Cr�ation d�un nouveau MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New Organization','Cr�ation d�une nouvelle Organisation')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New User','Cr�ation d�un nouvel utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New User Template','Cr�er un nouveau mod�le d�utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Custom Date Conferences Total Usage Report','Rapport d�utilisation personalis� des conf�rences ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Custom option description should be within 35 characters','La description ne dois pas d�passer 35 characteres')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Custom option title already exists in the list','Le titre de votre personalisation existe d�ja.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Custom Option Value','Option de personalization')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Default Protocol does not match with selected Address Type.','Le protocol s�lection� n�est pas support� par le type de codec s�lection�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Delete All','Tout d�truire')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Delete Log','Detruire les logs')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Deleted','D�truite')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Disconnected','D�connecter')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'DLL Name','Nom de la DLL')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Edit','Modification')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Edit Existing User','Modification d�un utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Edit Group','Modification d�un groupe')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Edit Organization Profile','Modification d�une Organisation')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Edit User Template','Modifier un mod�le d�utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Enabled','Permis')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Enter requested quantity for corresponding items.','Veuillez s�lectioner les quantit�es d�sir�es ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Error deleteting work order.','Probleme durant la destruction du Bon de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Error Uploading Image. Please check your VRM Administrator.','TEMPORARY FRENCH TEXT (until I do it)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Event level to be logged','Niveau des incidents a rapporter')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'External Attendee','Participant ext�rieure')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'File attachment is greater than 10MB. File has not been uploaded.','La taille de votre fichier est sup�rieure a 10 Mg. Veuliiez la r�duire pour le chargement')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'File trying to upload already exists','Le fichier existe d�ja')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Guest','Invit�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Guest Address Book','Carnet d�adresse des invit�s')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Housekeeping ','M�nag�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Housekeeping Groups','Groupe de maintenance Menag�re')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Housekeeping Work Orders','Bons de Commande Menager')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Illegal Search','Incorrecte recherche')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Image size is greater than 5MB. File has not been uploaded.','La taille de votre fichier est sup�rieure a 5 Mg. Veuliiez la r�duire pour le chargement')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Immediate Conference','Conf�rence imm�diate')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Include in Email','Inclus dans votre Email')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid Date Range: FROM date should be prior to TO date.','Incorrect laps de temps')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid duration','Dur�e incorrecte')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid Duration.Conference duration should be minimum of 15 mins.','La dur�e minimum est de 15 mins. Veuillez rectifier')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid Interface Type and Address Type.','Type d�interface et d�adresse invalide')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid IP Address.','IP adresse incorrecte')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid ISDN Address','ISDN adresse incorrecte')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid MPI Address','MPI adresse incorrecte')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid selection for Address Type, Protocol and Connection Type.','Le type d�addresse, de protocole, et de type de connection s�lection�es sont incompatibles. Veuillez corriger')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI proto','S�lection impossible. Il est indispensable d�avoir au moins un MCU s�lection� pour MPI support')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid Selection.','Choi incorrect')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid Start and End Range','Incorrecte valeurs')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid Start Date or Time. It should be greater than current time in','D�but de Conf�rence incorrecte. Veuillez corriger')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Invalid User Type','Type d�utilisateur invlaide')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Inventory Set has been changed and no longer belongs to the selected room.','Le groupe en inventaire a �t� modifi� and n�est plus disponible pour cette Salle de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Inventory Sets','Groupe d�inventaire')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Item Name already exists in the list','TEMPORARY FRENCH TEXT (until I do it)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Last Month Conferences Total Usage Report','Rapport d�utilisation des conf�rences du mois dernier')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Last Updated Date','Date de la derni�re modification')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Last Week Conferences Total Usage Report','Rapport d�utilisation des conf�rences de la semaine derniere')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Manage Departments','Manager vos D�partements')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Manage Guests','Management des Invit�s')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Manage Inactive Users','Management des Utilisateurs d�sactiv�s')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Manage MCUs','Gestion des MCUs')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Manage Tiers','Management des Tiers')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Manage Users','Management des Utilisateurs')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Max. length for start and end range is 22 characters.','La longueur maximum est de 22 caract�res')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'MCU','MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'MCU is mandatory for MPI protocol','Il est indispensable d�avoir au moins un MCU s�lection� pour MPI support')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Modify work order to conform to your room changes. Work orders and Room names are as follows:','Veuillez modifier vos Bons de Commandes en accordance avec vos Salles de Conf�rences s�lection�es')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Module Type','Type de module')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'More than 10 IP Services can not be added.','Un maximum de 10 IP service peuve etre cr�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'More than 10 ISDN Services can not be added.','Un maximum de 10 ISDN service peuve etre cr�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'My Incomplete Audio/Visual Work Orders','Liste des Bons de Commande Audio/Visuel en attente')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'My Incomplete Catering Work Orders','Liste des Bons de Commande Alimentaire incomplet')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'My Incomplete Housekeeping Work Orders','Liste des Bons de Commande Menager incomplet')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'My Pending Audio/Visual Work Orders','Liste des Bons de Commande Alimentaire')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'My Pending Catering Work Orders','Liste des Bons de Commande Alimentaire en attente')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'My Pending Housekeeping Work Orders','Liste des Bons de Commande Menager en attente')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'My Preferences','Mes Pr�f�rences')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'myVRM Address Book','Carnet d�adresse des utilisateurs myVRM')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No','Non')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Conference-AV','Auncu Audio/Visuel d�fini')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Custom Options found.','Auncune personalization d�finie')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Data Found','Aucun �l�ment disponible ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Logs Found.','Aucun log displonible')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Recurrence','Aucune r�p�tition')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'None','Aucun')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Not Started','Pas commenc�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'On MCU','En cours sur le MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Ongoing','En cours')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Only one image can be uploaded for footer information.','Seulement une image est permise�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Only two Endpoints can be selected for Point-To-Point Conference.','Une Conf�rence point-a-point dois avoir deux points de connection')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Operation Successful!','Succ�s')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Operation Successful! Please logout and re-login to see the changes.','Op�ration termin�e. Veuillez voud d�connecter et vous reconnecter pour visualizer le r�sultat.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Operation Successful! Reminder email has been sent to selected participant.','F�licitation! Tous les emails ont �t� envoy�s a vos invit�s')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Operation Successful! Reminder email has been sent to the person-in-charge of the work order.','F�licitation! Votre Bon de Commande a �t� envoy� a l�admistrateur en charge')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Option name already exists in the list','Votre option existe d�ja')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Overwrite events older than','Ecraser incidents plus vieux que ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Page(s):','Page(s);')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Paused','En attente')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Pending','En attente')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please add atleast one option to the custom option.','Veuillex choisir au moins une option pour votre personalisation')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please associate at least one menu with each work order.','Il est imp�ratif d�avoir au moins une option du menu pour chaque Bon de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please check Start and Completed Date/Time for this workorder.','Verulliez v�rifier l�heure de d�but et fin pour ce Bon de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please check the password. It must be unique','Veuillez v�rifier votre Mot de Passe. Il dois etre unique')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please check the start and end time for this workorder.','Veuillez v�rifier l�heure de d�but et fin de ce Bon de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please enter a quantity for each selected item. Each item should have a quantity > 0','Veuillez entrer une quantit�e positive pour chaque �l�ment s�lection�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please enter email address for test mail','Veuillez entrer une adresse email por valider le test')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please enter the ExchangeID for the profile name ','Veuillez s�lectioner l�identificateur MS-Exchange pour ce profile')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please enter TO and FROM dates.','Veuillez entrer votre laps de temps')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please enter valid date range of at most a week.','Veuillez selectioner une plage date inf�rieure a une semaine')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please fill in all the values for Change Request','Veuillez remplir toutes les �l�ments n�cessaire pour cette demande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please respond to the following conference','Veuillez repondre a l�invitation SVP')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select a Housekeeping Group now.','Veuiller s�lectioner votre Groupe M�nager maintenant ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select a Room','Veuillez s�lectioner une Salle de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select a valid IP Address.','Veuillez s�lectioner une IP adresse valide')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select a valid ISDN Address.','Veuillez s�lectioner une ISDN adresse valide')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select an Address Type.','Veuillez s�lectioner un type d�adresse')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please save or cancel current workorder prior to editing or deleting existing workorders.','Veuiller sauver ou abandonner votre bon de commande en cours avant de detruire or modifier any existant ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please save/update or cancel current workorder prior to editing or deleting existing workorders.','Veuillez sauver or annuler voter Bon de Command courant avant de d�truire ou modifier les autres Bons de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select an entity','Veuillez s�lectioner un �l�ment')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select at least one room from �Select Rooms� tab in order to create a work order.','Veuillez choisir au moins une salle de Conf�rence pour cr�er un Bon de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select atleast one Department.','Veuillez s�lectioner au moins un d�partement')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select atleast one Endpoint.','Veuillez s�lectioner au moins un Codec ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select atleast one MCU.','Veuillez s�lectioner au moins un MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select atleast one Room.','Veuillez s�lectioner au moins une Salle de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select atleast one Tier.','Veuillez s�lectioner au moins un Tier')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select MCU.','Veuillez s�lectioner un MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select the corresponding menu for a valid quantity.','Veuillez s�lectioner l�option correspondante pour valider la quantit� d�sir�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select...','Veuillez s�lectioner�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please specify Quantity and either click update to calculate total price for this work order or clic','Veuillez s�lectioner les quantit�es d�sir�es avant d�updater le prix du Bon de Commande')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please visit the Advanced Audio/Video Settings tab','Veuillez v�rifier les d�finitions Audio / Visuel ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Point-To-Point Conference','Conf�rence point-a-point')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Polycom MGC Configuration','Configuration du Polycom MGC')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Polycom RMX Configuration','Configuration du Polycom RMX ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Post Conference End','Apres fin de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Pre Conference Start','Avant d�but de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'PRI Allocation Report','Rapport d�allocation PRI')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Radvision MCU Configuration','Configuration du Radvision MCU ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Report is not ready to view.','Ce rapport n�est pas disponible')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Room','Salle de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Room Attendee','Participant int�rieure')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Room Conference','Salle de Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Rooms Found :','Salles de Conf�rence trouv�es;')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Running','En cours')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Scheduled','Pr�vue')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Search Result(s) for Audio/Visual Work Orders','Bons de Commande Audio/Visuel')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Search Result(s) for Catering Work Orders','Bons de Commande Alimentaire')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Search Result(s) for Housekeeping Work Orders','Bons de Commande Menager')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Search Results','R�sultat')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'See Recurring Pattern','Veuillez v�rifier les r�p�titions')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Select a Set from the Drop down list.','Veuillez s�lectioner un group ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Select Caller/Callee','Veuillez s�lectionner qui appellera qui')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Selected bridge for this template does not support MPI calls.','LE MCU s�lection� pour ce mod�le ne supporte pas le protocol MPI')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Set Customized Instances','Definir les personalizations')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Show Uploaded Files','Visualizer les fichiers charg�s')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Some Additional Comments fields are missing. Please contact your VRM Administrator.','Commentaire absent. Veuillez contacter votre administrator myVRM')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Status being Updated.','Condition mise a jour')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Stopped','Arr�t�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Submit','Soumettre')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Submit Conference','Cr�er la Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Tandberg MCU Configuration','Configuration duTandberg MCU')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Terminate','Termin�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Terminated','Termin�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no Audio/Visual work orders associated with this conference.','Il n�y a aucun Bon de Commande associ� a cette conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no Catering work orders associated with this conference.','Il n�y a aucun Bon de Commande de restauration associ� a cette conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no Housekeeping Groups available for this room. Please select another room.','Il n�y a aucun Groupe M�nager disponible pourt cette salle de Conf�rence. Veuillez choisir une autre Salle de Conf�rence.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no Housekeeping work orders associated with this conference.','Il n�y a aucun Bon de Commande manag� associ� a cette conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no menus associated with the selected Service Type for this location. Please select a new ','Il n�existe aucune option pour le type de service selectione�.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no menus associated with the selected Service Type for this location. Please select a new ','Il n�y a aucune option disponible pour le service s�lection�e a cette endroit. Veuillez s�lectioner un autre service ou retourner a la page principale des Bons de Commande de Restauration')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no Workorders associated with this conference','Il n�y a aucun Bon de Commande associ� a cette conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'This value has already been taken.','Cette s�lection n�est plus disponible')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'This Week Conferences Total Usage Report','Rapport d�utilisation des conf�rences pour cette semaine')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Title','Titre')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'To add a new work order, click the Add New Work Order button. To modify an existing work order, clic','Pour cr�er un Bon de Commande veuiller choisir l�option ci-dessous. ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'To add a new work order, click the button below.','Pour cr�er un Bon de Commande veuiller choisir l�option ci-dessous.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'To add a new work order, click the button below. OR click EDIT to edit an existing work order.','Pour cr�er un Bon de Commande veuiller choisir l�option ci-dessous')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'To create a new work order, click on Add New Work Order button.','Pour cr�er un nouveau Bon de Commande, veuillez choisir l�option ci-dessous')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'To create a new work order, click on Add New Work Order button. To modify an existing work order, cl','Pour cr�er ou modifier un Bon de Commande veuiller choisir l�option Approprier. Pour d�truire, vous devez d�abord d�truire les �l�ments associ�s a ce Bon de Commande.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'To create a new work order, click the button below.','Pour cr�er un Bon de Commande veuiller choisir l�option ci-dessous. ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'To create a new work order, please click on the button below.','Pour cr�er un nouveay Bon de Commande, veuillez choisir l�option ci-dessous')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Today�s Audio/Visual Work Orders','Liste des Bons de Commande Audio/Visuel pour auhourd�hui')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Today�s Catering Work Orders','Liste des Bons de Commande Alimentaire  pour auhourd�hui')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Today�s Housekeeping Work Order','Liste des Bons de Commande Menager  pour auhourd�hui')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Totals:','Totaux')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Un Mute','Mode Silence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'UnBlock','D�bloquer')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Undefined','Ind�finie')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Undelete','Recr�er')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Update','R�viser')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Usage By Room','Utilisation par Salle de conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'User','Utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Version Numbe','Num�ro de version')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Version Number','Num�ro de version')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'View Conference','Visualiser votre Conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'View Conflict','Visualizer les problemes')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'View Menus','TEMPORARY FRENCH TEXT (until I do it)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'VRM Service is not installed','Le Service myVRM n�est pas install�')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'VTC Calendar','Calendrier VTC')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'VTC Contact List','Liste des contacts VTC')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'VTC Daily Schedule','Calendrier journalier VTC')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'VTC Resource Allocation Report','Rapport d�allocation VTC')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'We are sorry that you will not be able to join the following conference','Nous sommes d�sol� que vous ne puiss� pas joindre notre conf�rence')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Workorders can only be created with rooms selected for this conference.','Les Bons de Commandes ne peuvent etre cr�er que pour les salles de conf�rences s�lectioner')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Year To Date Conferences Total Usage Report','Rapport d�utilisation des conf�rences pour l�ann�e courante')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Yes','Oui')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Yesterday Conference Total Usage Report','Rapport d�utilisation des conf�rences d�hier')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'You have no Pending or Future Conferences.','Aucune Conf�rence existante (Future ou en Attente)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Items...','Vide')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'previous page','Page Pr�c�dente')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Already on last page','D�ja pr�sent (Derniere page)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'next page','Page suivante')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Already on first page','D�ja pr�sent (Premiere page)')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'N/A','--')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Session expired','Session expir�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'VRM system is unavailable during the chosen date and time. Please check VRM system availability and ','myVRM n�est pas disponible pour la date et le moment choisi. Veuillex verifier avec votre admninistrator pour heures ouvrable')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Closed','Ferm�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Any','Quelconque')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Connecting','Connect�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Disconnecting','Deconnect�e')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Total Records:','Total Records:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please select a Room first from the drop down below.','Please select a Room first from the drop down below.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'If there are no rooms in the list, click on Select Rooms option available on your left.','If there are no rooms in the list, click on Select Rooms option available on your left.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Click Cancel before you proceed to another step.','Click Cancel before you proceed to another step.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'If there are no rooms in the list, click on �Select Rooms� option available on your left.','If there are no rooms in the list, click on �Select Rooms� option available on your left.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Select a Set from the A/V Set list.','Select a Set from the A/V Set list.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'If there are no items in the list, then there are no sets associated with this room. ','If there are no items in the list, then there are no sets associated with this room. ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Duration:','Duration:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Modify work order to conform to your room changes. Work orders and Room names are as follows:','Modify work order to conform to your room changes. Work orders and Room names are as follows:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please confirm settings below. If changes are required, please edit the fields accordingly OR contac','Please confirm settings below. If changes are required, please edit the fields accordingly OR contact technical support ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conferenc','for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Please confirm settings below. If changes are required, please edit the fields accordingly OR contac','Please confirm settings below. If changes are required, please edit the fields accordingly OR contact technical support ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Locations','No Locations')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conferenc','for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'There are no Audio/Visual work orders associated with this conference.<br />To create a new work ord','There are no Audio/Visual work orders associated with this conference.<br />To create a new work order, click the button below.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Item','Item')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,' is duplicated!',' is duplicated!')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Operation failed: Invalid security permissions on ','Operation failed: Invalid security permissions on ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'You have successfully logged out of myVRM.','You have successfully logged out of myVRM.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Error 122: Please contact your VRM Administrator','Error 122: Please contact your VRM Administrator')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Create New','Create New')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'No Locations','No Locations')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Restore User','Restore User')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (1,'Inactive Users','Inactive Users')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Total Records:','Nombre de dossiers:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please select a Room first from the drop down below.','Veuiller s�lectionner une salle de conf�rence premierement.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'If there are no rooms in the list, click on Select Rooms option available on your left.','Si la liste est vide, veuiller s�lectioner une nouvelle utilisant l�option ci-contre.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Click Cancel before you proceed to another step.','Veuillez selectionner "�Abandon� avant to continuer sinon.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'If there are no rooms in the list, click on �Select Rooms� option available on your left.','Si la liste est vide, veuiller s�lectioner une nouvelle utilisant l�option ci-contre.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Select a Set from the A/V Set list.','Veuiller s�lectioner un set de la liste A/V.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'If there are no items in the list, then there are no sets associated with this room. ','Si la liste est vide, cela signifie qu�il n�y a aucun �l�ment associ� a cette salle')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Duration:','Dur�e:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Modify work order to conform to your room changes. Work orders and Room names are as follows:','Veuiller modifier votre Bon de Commande en accordance a votre salle. Bon de commande(s) and Salle(s) sont:')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please confirm settings below. If changes are required, please edit the fields accordingly OR contac','Veuiller confirmer vos choix SVP.Si besoin est, modifier a votre choix or contacter myVRM support.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conferenc','for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conference.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Please confirm settings below. If changes are required, please edit the fields accordingly OR contac','Veuiller confirmer vos choix SVP.Si besoin est, modifier a votre choix or contacter myVRM support.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Locations','Aucune Localisation')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conferenc','pour aide.<br />Une fois que vous avez valid� vo choix, veuill� resoumettre la conf�rence.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'There are no Audio/Visual work orders associated with this conference.<br />To create a new work ord','Il n�y a aucun Bon de Commande associ�e a cette comference.<br />Pour cr�er un nouveau bon de Commande, veuill� selectionner l�option suivante')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Item','Element ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,' is duplicated!',' existe d�ja')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Operation failed: Invalid security permissions on ','Probleme. Votre niveau de s�curite est insufisant pour acc�der les fichiers; ')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'You have successfully logged out of myVRM.','Vous avez quitt� myVRM.')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Error 122: Please contact your VRM Administrator','Error 122: Veuillez contacter votre VRM Administrateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Create New','Cre�r')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'No Locations','Aucune Localisation')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Restore User','Restauration d�utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, [text], TranslatedText) values (5,'Inactive Users','Utilisateur inactif')

update Launguage_Translation_Text set TranslatedText = replace(TranslatedText,'�',''''),[text] = replace([text],'�','''')

/* *****   2. FB 1830 - Translation Text.sql - end **** */


/* *****   3. Icons Insert Statements.sql - start **** */

Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'A/V Workorders','Bon De Commande A/V')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Bulk Tool Facility','Modification en Masse')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Customize Organization Emails','Management des Emails (Organization)')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Cutomize my Emails','Management des Emails (Personel)')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Dashboard','Ecran de Control')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Express form','Forme Simplifi�e')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'LDAP Directory Import','Int�gration avec LDAP')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Active Users','Management des Utilisateurs')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Catering items','Management des menues alimentaire')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Departments','Management des D�partements')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Email Queue','Management des files d''attente des Emails')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Endpoints','management des Codecs')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Groups','Management des Groupes')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Guests','Management des Invit�s Exterieures')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Housekeeping items','Management des Bons de Commande de Manutention')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Inactive Users','Management des Utilisateus desactiv�')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Inventory Sets','Management de l''inventaire')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage MCU','Management des Bridges')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage my Lobby UI','Management de mon Hall d''entr�e')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Organization Custom Options','Management des Options Personalis�es')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Organization Set-up','Management des Organisation')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Organization UI Text','Management des Textes ')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Roles','Management des Roles')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Rooms','Management des Salles De Conf�rence')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Templates','management des Mod�les')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Manage Tiers','Management des Tiers')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'My Advance Reports','Exploration de donn�es')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'My Organization Options','Management de mes Options')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'My Organization UI','Management des Element d'' Ecrans')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'My Preferences','Mes Pr�f�rences')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'My Reports','Rapports')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'My Settings','Mes Definitions')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'New Conference','Cr�er un Conf�rence')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Restore Users','Restituer un Utilisateur')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Site Settings','Management des D�finition globales')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'Systems Diagnostics','Logs et Diagnostiques')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Approval Pending','Approbation en Attente')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Catering Workorder','Bon de Commande Alimentaire')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Housekeeping workorders','Bon de Commande de Manutention')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Inventory workorders','Bon Commande d''Inventaire')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View My Reservations','Liste des R�servations')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Ongoing','Liste des Conf�rence active')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Pending','Liste des Conf�rence en Attente')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Personal Calendar','Calendrier Personnel')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Public Conferences','List des Conf�erences Publiques')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Room Calendar','Calendrier des Salles de Conf�rence')
Insert into [Launguage_Translation_Text] (LanguageID, Text, TranslatedText) values (5,'View Scheduled Calls','Liste des R�servations a venir')


/* *****   3. Icons Insert Statements.sql - end **** */


/* *****   4. IconsTranslationText.sql - start **** */

truncate table Icons_Ref_S 

Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/BULKTOOLicon.png','Bulk Tool Facility','allocation.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/OngoingIcon.png','View Ongoing','ConferenceList.aspx?t=2')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/MyReservationsIcon.jpg','View My Reservations','ConferenceList.aspx?t=3')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/PublicConferenceIcon.png','View Public Conferences','ConferenceList.aspx?t=4')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/PendingIcon[2].png','View Pending','ConferenceList.aspx?t=5')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ApprovalPendingIcon.png','View Approval Pending','ConferenceList.aspx?t=6')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/InventoryWorkorderIcon.png','View Inventory workorders','ConferenceOrders.aspx?t=1')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/CateringWorkorderIcon.png','View Catering Workorder','ConferenceOrders.aspx?t=2')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/HousekeepingWorkordersIcon.png','View Housekeeping workorders','ConferenceOrders.aspx?t=3')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageInventorySetsIcon.png','Manage Inventory Sets','InventoryManagement.aspx?t=1')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageCateringIcon[1].png','Manage Catering items','InventoryManagement.aspx?t=2')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManagineHousekeepingItemsIcon.png','Manage Housekeeping items','InventoryManagement.aspx?t=3')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/NewConferenceIcon.png','New Conference','ConferenceSetup.aspx?t=n&op=1')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/DashboardIcon.png','Dashboard','Dashboard.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/CustomOrganizationEmailIcon.png','Customize Organization Emails','EmailCustomization.aspx?tp=o')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/CustomizeEmail.png','Cutomize my Emails','EmailCustomization.aspx?tp=u')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageEndpointIcons[1].png','Manage Endpoints','EndpointList.aspx?t=')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/SystemsDiagnosticsIcon.png','Systems Diagnostics','EventLog.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ScheduledCallsIcon[2].png','View Scheduled Calls','ExpressConference.aspx?t=n')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/REPORTicon[2].png','My Reports','GraphicalReport.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/AdvanceReportIcon.png','My Advance Reports','managereports.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/DirectoryImportIcon.png','LDAP Directory Import','LDAPImport.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/LobbyIcon.png','Manage my Lobby UI','LobbyManagement.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/OrganizationOptionsIcon.png','My Organization Options','mainadministrator.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageMCUIcon.png','Manage MCU','ManageBridge.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageDepartmentIcon[1].png','Manage Departments','ManageDepartment.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageGroupsIcon[1].png','Manage Groups','ManageGroup.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/OrganizationSetupIcon.png','Manage Organization Set-up','ManageOrganization.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/SiteSettingsIcon[2].png','Site Settings','SuperAdministrator.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageRooms.png','Manage Rooms','manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageTiersIcon.png','Manage Tiers','ManageTiers.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageTemplatesIcon.png','Manage Templates','ManageTemplate.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageActiveUserIcon[2].png','Manage Active Users','ManageUser.aspx?t=1')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageGuestsIcon[1].png','Manage Guests','ManageUser.aspx?t=2')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageInactiveUsersIcon.png','Manage Inactive Users','ManageUser.aspx?t=3')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/MyPreferencesIcons[1].png','My Preferences','ManageUserProfile.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageRolesIcon.png','Manage Roles','manageuserroles.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/SETTINGSicon[2].png','My Settings','OrganisationSettings.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageOrganizationsUITextIcon.png','Manage Organization UI Text','UITextChange.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/MyOrganizationUIicon[1].png','My Organization UI','UISettings.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageEmailQueue[1].png','Manage Email Queue','ViewBlockedMails.aspx?tp=o')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/ManageOrganizationCustomOptionsIcon.jpg','Manage Organization Custom Options','ViewCustomAttributes.aspx')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/CalendarIcon.png','View Personal Calendar','personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/RoomCalendarIcon[1].png','View Room Calendar','roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=')
Insert into Icons_Ref_S (IconUri, Label, IconTarget) values ('../en/img/expressicon.png','Express Conference','ExpressConference.aspx?t=n&op=1')

/* *****   4. IconsTranslationText.sql - end   **** */


/* ********************* MultilingualScripts.sql - end   ****************** */


/* ********************* FB 2141 Org_Settings_D_PluginConfirmations.sql - start ****************** */

/*
   Wednesday, June 29, 20113:18:04 PM
   User: myvrm
   Server: demo1
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	PluginConfirmations smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_PluginConfirmations DEFAULT 0 FOR PluginConfirmations
GO
COMMIT

Update Org_Settings_D set PluginConfirmations  = 1


/* ********************* FB 2141 Org_Settings_D_PluginConfirmations.sql - end   ****************** */


/* ********************* FB 2141 Usr_List_D_PluginConfirmations.sql - start ****************** */

/*
   Wednesday, June 29, 20113:04:56 PM
   User: myvrm
   Server: demo1
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	PluginConfirmations smallint NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PluginConfirmations DEFAULT 0 FOR PluginConfirmations
GO
COMMIT

Update Usr_List_D  set PluginConfirmations  = 1


/* ********************* FB 2141 Usr_List_D_PluginConfirmations.sql - end   ****************** */


/* ********************* 2164.sql(Updated on_28july) - start ****************** */

update Loc_Tier3_D set disabled = 1 where Id = 0
update Loc_Tier2_D set disabled = 1 where L3LocationId = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	PluginConfirmations smallint NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_PluginConfirmations DEFAULT 0 FOR PluginConfirmations
GO
COMMIT

Update Usr_Inactive_D  set PluginConfirmations  = 0


/* ********************* 2164.sql(Updated on_28july) - end   ****************** */


/* ********************* FB 2004.sql (V24_18July) - start ****************** */

/********* FB 2004  Starts ***********/
GO
CREATE TABLE [dbo].[Conf_CreateType_S]
(
[CreateID] [smallint] NOT NULL,
[VendorName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Version] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
ALTER TABLE [dbo].[Conf_CreateType_S] ADD CONSTRAINT [PK_Conf_CreateType_S] PRIMARY KEY CLUSTERED  ([CreateID])


-- Add 7 rows to [dbo].[Conf_CreateType_S]
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (0, N'VRM', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (1, N'Outlook York', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (2, N'Outlook Generic', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (3, N'Lotus York', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (4, N'Lotus Generic', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (5, N'Ipad', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (6, N'Other mobile devices', N'')


/********* FB 2004  Ends ***********/

/* ********************* FB 2004.sql (V24_18July) - end   ****************** */


/* ********************* Consolidated.sql - start ****************** */

/*
   Monday, July 18, 20112:50:14 PM
   User: myvrm
   Server: sql2k5
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	TelepresenceFilter smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_TelepresenceFilter DEFAULT 0 FOR TelepresenceFilter
GO
COMMIT

/*
   Monday, July 18, 20112:52:39 PM
   User: myvrm
   Server: sql2k5
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	isTelepresence smallint NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_isTelepresence DEFAULT 0 FOR isTelepresence
GO
COMMIT

update Org_Settings_D set TelepresenceFilter = isnull(TelepresenceFilter,0)

update loc_room_d set isTelepresence = isnull(isTelepresence,0)

----------------------------------
--FB 2141

/*
   Wednesday, June 29, 20113:18:04 PM
   User: myvrm
   Server: demo1
   Database: myvrm
   Application: 
*/

/*
   Wednesday, June 29, 20113:04:56 PM
   User: myvrm
   Server: demo1
   Database: myvrm
   Application: 
*/

--FB 2154
/*
   Wednesday, July 06, 20111:36:09 PM
   User: myvrm
   Server: sql2k5
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	SendAttachmentsExternal smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_SendAttachmentsExternal DEFAULT 0 FOR SendAttachmentsExternal
GO
COMMIT


/****** Object:  Table [dbo].[Email_Domain_D]    Script Date: 07/06/2011 13:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email_Domain_D](
	[DomainID] [int] IDENTITY(1,1) NOT NULL,
	[Companyname] [nvarchar](50) NULL,
	[Emaildomain] [nvarchar](50) NOT NULL,
	[Active] [smallint] NOT NULL CONSTRAINT [DF_Table_1_Active]  DEFAULT ((1)),
	[orgId] [smallint] NULL,
 CONSTRAINT [PK_Email_Domain_D] PRIMARY KEY CLUSTERED 
(
	[DomainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


/* ********************* Consolidated.sql - end   ****************** */


/* ********************* FB 2227.sql (V24_19Sep) - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	InternalVideoNumber nvarchar(50) NOT NULL default '',
	ExternalVideoNumber nvarchar(50) NOT NULL default ''
GO
COMMIT




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	InternalVideoNumber nvarchar(50) NOT NULL default '',
	ExternalVideoNumber nvarchar(50) NOT NULL default ''
GO
COMMIT

/* ********************* FB 2227.sql (V24_19Sep) - end   ****************** */

--FB 2219
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE [dbo].[Gen_ServiceType_S](
	[ID] [int] NOT NULL,
	[ServiceType] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
) ON [PRIMARY]
GO
COMMIT

Insert into Gen_ServiceType_S (ID, ServiceType) values (1,'Gold')
Insert into Gen_ServiceType_S (ID, ServiceType) values (2,'Platinum')
Insert into Gen_ServiceType_S (ID, ServiceType) values (3,'Silver')



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	ServiceType smallint 
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	ServiceType smallint 
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnableRoomServiceType smallint 
GO
COMMIT 


Update dbo.Conf_Conference_D SET ServiceType = -1

Update dbo.Loc_Room_D SET ServiceType = -1

update err_list_s set message = 'Room conflict, please modify Date / Time or Service Type.' where id = 241

update err_list_s set message = 'Room conflicts found in some recurring instances. Please modify Date / Time or Service Type.' where id = 510

/* ********************* FB 2045.sql (V24_20Sep) - start ****************** */

/* ********************* Insert Entity Code as System Cust Option (FB 2045) - start ****************** */
DECLARE @orgID int

DECLARE cusAttrCursor CURSOR FOR 

SELECT orgID FROM Org_List_D 

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

Declare @id int

set @id = (select isnull(MAX(CustomAttributeId),0)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType],[IncludeInCalendar])
VALUES
(0,@id,'Entity Code','Entity Code',6,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1,1)

select * from Dept_CustomAttr_D where orgid = 11

FETCH NEXT FROM cusAttrCursor INTO @orgID
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor

/* ********************* Insert Entity Code as System Cust Option (FB 2045) - end ****************** */


/* (FB 2045) NGC client specific stored procedures start */

/****** Object:  StoredProcedure [dbo].[GetEntityCodes]    Script Date: 09/30/2010 05:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[GetEntityCodes] As

SELECT Distinct Caption AS EntityCode, OptionID as OptionID
FROM Dept_CustomAttr_Option_D a, Dept_CustomAttr_D b 
where Caption is not null 
and b.CreateType = 1 
and b.DisplayTitle = 'Entity Code'
and a.CustomAttributeId = b.CustomAttributeId

GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


/* ************************************************************************************************** */


/****** Object:  StoredProcedure [dbo].[GetRooms]    Script Date: 09/30/2010 05:36:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetRooms] As

Select Distinct c.roomid, l.name From conf_room_d c, loc_room_d l
Where c.roomid = l.roomid Order by l.name


/* ************************************************************************************************** */


/****** Object:  StoredProcedure [dbo].[GetTimeZoneDefault]    Script Date: 09/30/2010 05:37:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[GetTimeZoneDefault] As

Declare @tid int
Set @tid = 0
Select @tid = timezoneid from gen_timezone_s
Where 'central standard time' LIKE standardname
If @tid > 0 Select @tid as tzdef else Select top 1 timezoneid as tzdef from gen_timezone_s


/* ************************************************************************************************** */

/****** Object:  StoredProcedure [dbo].[GetTimeZoneDisplay]    Script Date: 09/30/2010 05:37:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[GetTimeZoneDisplay] As

Select timezoneid, standardname + ', ' + timezone + ' ' + timezonediff as timezonedisplay from gen_timezone_s
Order By timezonedisplay


/* ************************************************************************************************** */

/* ************* SelectCalendarDetails ********** */

/****** Object:  StoredProcedure [dbo].[SelectCalendarDetails]    Script Date: 10/20/2011 17:12:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[SelectCalendarDetails] 
(
	-- Add the parameters for the stored procedure here
	@timezone			int=null,
	@startTime			datetime=null,
	@endTime			datetime=null,
	@specificEntityCode int=null,
	@codeType			int=null,
	@tablename			varchar(200)=null
)

As
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@timezone is null)
	begin
		return
	end
	
	if (@codeType is null)
	begin
		return
	end

	/* Temp table for Calendar details */

	CREATE TABLE #TempCalendarDT(
	[Date] datetime,
	Start varchar(50),
	[End] varchar(50), 
	Duration int, 
	Room varchar(500),
	[Meeting Title] varchar(1000),
	[Last Name] varchar(100),
	First varchar(100),
	email varchar(200),
	Phone varchar(50),
	State varchar(100),
	City varchar(100),
	[Conf #] varchar(50), 
	[Meeting Type] varchar(100),
	Category varchar(50),
	Status varchar(50),
	Remarks varchar(2500),
	conftype int,
	connectionType int,
	defVideoProtocol int,
	vidProtocol varchar(20),
	TypeFlag char(1),
	CustomAttributeId int, 
	SelectedOptionId int,
	addresstype int,
	[Entity Code] varchar(50),
	[Entity Name] varchar(200)
)

	declare @ecode as varchar(50)
	declare @optionid as int
	declare @calqry as varchar(4000)
	declare @where as varchar(2000)
	declare @cond as varchar(1000)
	declare @filter as varchar(1000)

	declare @seloptionid as int
	--FB 2045 start	
	declare @syscustattrid as varchar(50)
	SELECT @syscustattrid = CustomAttributeId FROM Dept_CustomAttr_D 
    where CreateType = 1 and DisplayTitle = 'Entity Code'
    --FB 2045 end

	if(@codeType = 1) -- None (Need to identify code)
		begin
			set @seloptionid = ''
			set @ecode = 'NA'			
		end
	else if(@codeType = 2) -- Specific
		begin
			set @seloptionid = @specificEntityCode	
			SELECT top 1 @ecode = Caption FROM Dept_CustomAttr_Option_D where OptionId=@specificEntityCode and Caption is not null
		end
	
	set @filter = ''

	if (@startTime is not null And @endTime is not null)
		begin
			set @filter = @filter + ' AND c.confdate BETWEEN dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@startTime as varchar(40)) +''') AND dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@endTime as varchar(40)) +''')'
		end

	if (@tablename <> '' AND @tablename is not null)
		begin
			if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
			 begin
				set @filter = @filter +' AND cu.roomid IN (Select roomid from '+ @tablename +')'
			 end
		end

/* Details Part */
	set @calqry = 'Select a.*, isnull(d.[Entity Code],''NA'') as [Entity Code], isnull(d.[Entity Name],''NA'') as [Entity Name] From '
	set @calqry = @calqry + '(SELECT dbo.changeTime('+ cast(@timezone as varchar(2)) +',c.confdate) AS Date,'
	set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate)))),13,8) as Start,'
	set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate)) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate))))),13,8) as [End],'
	set @calqry = @calqry + ' c.Duration, l.name AS Room, c.externalname AS [Meeting Title], u.lastname as [Last Name], '
	set @calqry = @calqry + ' u.firstname as First, u.email,'
	set @calqry = @calqry + ' Case when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))>0)) Then (l.AssistantPhone + '', '' + u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))=0)) Then (l.AssistantPhone) '
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))>0)) Then (u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))=0)) Then ('''')'
	set @calqry = @calqry + ' End   AS Phone ' 
	set @calqry = @calqry + ' ,l3.name AS State, l2.name AS City, '
	set @calqry = @calqry + ' c.confnumname AS [Conf #],'
	set @calqry = @calqry + ' case c.conftype '
	set @calqry = @calqry + ' when 1 then ''Video'' '
	set @calqry = @calqry + ' when 2 then ''AudioVideo'' '
	set @calqry = @calqry + ' when 3 then ''Immediate'' '
	set @calqry = @calqry + ' when 4 then ''Point to Point'' '
	set @calqry = @calqry + ' when 5 then ''Template'' '
	set @calqry = @calqry + ' when 6 then ''AudioOnly'' '
	set @calqry = @calqry + ' when 7 then ''RoomOnly'' '
	set @calqry = @calqry + ' when 11 then ''Phantom'' '
	set @calqry = @calqry + ' end AS [Meeting Type],'

/*	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Outbound'' '
	set @calqry = @calqry + ' else ''Inbound'' '
	set @calqry = @calqry + ' end AS Category,'
*/
	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Dial-out from MCU'' '
	set @calqry = @calqry + ' when 1 then ''Dial-in to MCU'' '
	set @calqry = @calqry + ' when 3 then ''Direct to MCU'' '
	set @calqry = @calqry + ' end AS Category,'

	set @calqry = @calqry + ' case c.Status '
	set @calqry = @calqry + ' when 0 then ''Confirmed'' '
	set @calqry = @calqry + ' else ''Pending'''
	set @calqry = @calqry + ' end AS ''Status'', c.description  as Remarks'
	set @calqry = @calqry + ',c.conftype,cu.connectionType,cu.defVideoProtocol,'
	
	--set @calqry = @calqry + ' case cu.defVideoProtocol '
	
	set @calqry = @calqry + ' case cu.addresstype '
	set @calqry = @calqry + ' when 4 then ''ISDN'''
	set @calqry = @calqry + ' else ''IP'''
	set @calqry = @calqry + ' end AS vidProtocol, ''D'' as TypeFlag'
	set @calqry = @calqry + ' ,CustomAttributeId, SelectedOptionId, cu.addresstype'
/* Where Part */

	set @where = ' FROM '
	set @where = @where + ' conf_conference_d c,'
	set @where = @where + ' usr_list_d u, '
	set @where = @where + ' conf_room_d cu, '
	set @where = @where + ' loc_room_d l, '
	set @where = @where + ' loc_tier2_d l2,'
	set @where = @where + ' loc_tier3_d l3,'
	set @where = @where + ' conf_customattr_d ca'
	set @where = @where + ' WHERE (c.confid = cu.confid AND c.instanceid = cu.instanceid)'
	set @where = @where + ' AND (c.deleted = 0)'
	set @where = @where + ' AND c.owner = u.userid'
	set @where = @where + ' AND l.roomid = cu.roomid'
	set @where = @where + ' AND l2.id = l.l2locationid'
	set @where = @where + ' AND l3.id = l.l3locationid'
	set @where = @where + ' AND (c.confid = ca.confid AND c.instanceid = ca.instanceid)'

	if(len(@filter) > 1)
		set @where = @where + @filter

	set @where = @where + ' )a'
	set @where = @where + ' left outer join '
	set @where = @where + ' (Select Caption AS [Entity Code], helpText as [Entity Name], '
	set @where = @where + ' OptionID, CustomAttributeId from Dept_CustomAttr_Option_D '
	--set @where = @where + ' Where CustomAttributeId=1'
    --set @where = @where + ' Where CustomAttributeId='''+ @syscustattrid +'''' --FB 2045
	--Set @where = @where + ' And OptionType=6'
	--Set @where = @where + ' And DeptId=0'
	--Set @where = @where + ' And OptionValue=0)D '
	set @where = @where + ' )D on a.CustomAttributeId = D.CustomAttributeId'
	set @where = @where + ' AND a.SelectedOptionId=D.OptionID'

	declare @cmdsql as varchar(8000)

	if(@codeType=0) -- All
		Begin
			set @seloptionid = ''
			Declare cursor1 Cursor for
				
				SELECT Distinct Caption AS EntityCode, OptionID as OptionID
				FROM Dept_CustomAttr_Option_D where Caption is not null

			Open cursor1

			Fetch Next from cursor1 into @ecode,@optionid

			While @@Fetch_Status = 0
			Begin
								
				set @cond = ''
				Set @cond = ' Where a.SelectedOptionId ='+ Cast(@optionid as varchar(10))
												
				Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
				--print @cmdsql
				exec (@cmdsql)
				set @cond = ''
				set @cmdsql = ''

				Fetch Next from cursor1 into @ecode,@optionid
			End

			Close cursor1
			Deallocate cursor1

			/* Need to identify the entity codes */
			
			set @cond = ''
			Set @cond = ' Where a.SelectedOptionId ='''' OR a.SelectedOptionId = -1'
						
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			--print @cmdsql
			exec (@cmdsql)
			set @cond = ''
			set @cmdsql = ''

		End
	else 
		Begin
			
			set @cond = ''
			if @codetype = 1
				set @cond = ' Where [Entity code] is null or [Entity code] = ''-1'''
			else
				set @cond = ' Where a.SelectedOptionId ='+ cast(@seloptionid as varchar(10))
			
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			print @cmdsql
			exec (@cmdsql)
			set @cmdsql = ''			
			set @cond = ''
		End

/* ** Updates the duration as per requirement **

conftype = 11 phantom
conftype = 4  point-point
conftype = 7  room only

7. If a conference type is room only then the value posted in the duration field will always be equal to 0
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where conftype in(7,11) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

/*
8. Due to the method of connectivity by NGC, in a point-to-point defined conference there will be three rooms with the same meeting title, and the same date/time displayed, however,  one of the rooms is a phantom room and should be assigned a duration of 0.
   All point-to-point calls are to be billed as ISDN calls.

 PROGRAMMING NOTE: Any ISDN Room which doesn't belong to State = "ISDN Network" and City = "PacBell PRI's" should have duration as zero (0).
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where (rtrim(State) <> ''ISDN Network'' AND rtrim(City) <> ''PacBell PRIs'' AND addresstype=4) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

set @calqry = ''

if (@tablename <> '' AND @tablename is not null)
begin
	if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
	 begin
		set @cmdsql = ''
		set @cmdsql = 'drop table '+ @tablename
		exec(@cmdsql)
	 end
end
set @cmdsql = ''

/* Detail */
set @cmdsql = null

set @cmdsql = 'Select ltrim(rtrim(convert(char,[Date],101))) as [Date],ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where TypeFlag=''D'';'

/* Consolidated Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as Entity_Code, sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Connects]) as [Connects],'

set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP(Hours)], '
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([VOIP Totals])/sum([Hours]))*100) '
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [IP %],'

set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in (Hours)],'

set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out (Hours)],'
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([ISDN Outbound Totals])/sum([Hours]))*100)'
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [ISDN Dial-out %]'
--ISDN Outbound
set @cmdsql = @cmdsql + ' from '
set @cmdsql = @cmdsql + ' (Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'''
set @cmdsql = @cmdsql + ' group by [Entity Code] '

set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], isnull((sum(isnull(Duration,0))/60),0) as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where addresstype <> 4 and  TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], count(*) as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT  Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]) d group by [Entity Code];'

/* Entity codewise Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql = @cmdsql + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql = @cmdsql + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'
set @cmdsql = @cmdsql + ' from ('
--Pt-Pt Mins
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'' group by [Entity Code]'

--ISDN Outbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'' '
set @cmdsql = @cmdsql + ' group by [Entity Code] '

--ISDN Inbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and  TypeFlag=''D'' group by [Entity Code]'

--IP Dial-Out Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], sum(isnull(Duration,0)) as [VOIP Totals],0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype<>4 AND connectionType=0) and  TypeFlag=''D'' group by [Entity Code]'

--Hours
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

--Minutes
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]'
--Audio-Video Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' group by [Entity Code]'

--IP Dial-in Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], sum(isnull(Duration,0)) as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype <> 4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ') d group by [Entity Code];'

--ISDN Network - seperate tab
--All point-to-point calls are to be billed as ISDN calls.
-- Conftype = 4 (Point-Point)
set @cmdsql = @cmdsql + ' Select ltrim(rtrim(convert(char,[Date],101))) as [Date],ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where ((addresstype=4 and TypeFlag=''D'''
set @cmdsql = @cmdsql + ' and State=''ISDN Network'' and City=''PacBell PRIs'') OR (Conftype=4));'


/* ISDN Summary */
declare @condisdn as varchar (1000)
declare @condisdn1 as varchar (1000)
declare @cmdsql2 as varchar(5000)

set @condisdn1 =  'and ((addresstype=4 and State=''ISDN Network'' and City=''PacBell PRIs''))'

set @cmdsql2 = ' Select ''ISDN'' as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql2 = @cmdsql2 + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'

set @cmdsql2 = @cmdsql2 + ' from ('
--Pt-Pt Mins
set @cmdsql2 = @cmdsql2 + ' Select sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Outbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=0 and  TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Inbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=1 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--Hours
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT Where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--Minutes
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1
--Audio-Video Mins
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--IP Dial-in Mins
set @cmdsql2 = @cmdsql2 + ')d;'

exec(@cmdsql + @cmdsql2)


END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/* ************************************************************************************************** */
/* ********************* FB 2045.sql (V24_20Sep) - end   ****************** */


/* ********************* FB 2052.sql(V24_4Oct) - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Holiday_Type_S
	(
	HolidayType int NOT NULL IDENTITY (1, 1),
	HolidayDescription nvarchar(50) NOT NULL,
	Color nvarchar(20) NOT NULL,
	Priority int NOT NULL,
	OrgId int NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_Holiday_Type_S ON
GO
IF EXISTS(SELECT * FROM dbo.Holiday_Type_S)
	 EXEC('INSERT INTO dbo.Tmp_Holiday_Type_S (HolidayType, HolidayDescription, Color, Priority, OrgId)
		SELECT HolidayType, HolidayDescription, Color, Priority, OrgId FROM dbo.Holiday_Type_S WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Holiday_Type_S OFF
GO
DROP TABLE dbo.Holiday_Type_S
GO
EXECUTE sp_rename N'dbo.Tmp_Holiday_Type_S', N'Holiday_Type_S', 'OBJECT' 
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script 
in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	SpecialRecur smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_SpecialRecur DEFAULT 1 FOR SpecialRecur
GO
COMMIT

Update Org_Settings_D set SpecialRecur = 1

/* ********************* FB 2052.sql(V24_4Oct) - end   ****************** */


/* ********************* FB 2218.sql - start ****************** */

--FB 2181

/****** Object:  Table [dbo].[Conf_RecurInfo_D]    Script Date: 09/25/2011 19:15:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conf_RecurInfoDefunt_D](
	[confid] [int] NOT NULL,
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[confuId] [int] NOT NULL CONSTRAINT [DF_Conf_RecurInfoDefunt_D_confuId]  DEFAULT ((0)),
	[timezoneid] [int] NULL,
	[duration] [int] NULL,
	[recurType] [int] NULL,
	[subType] [int] NULL,
	[yearMonth] [int] NULL,
	[days] [nvarchar](256) NULL,
	[dayNo] [int] NULL,
	[gap] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[endType] [int] NULL,
	[occurrence] [int] NULL,
	[dirty] [smallint] NULL,
	[RecurringPattern] [nvarchar](4000) NULL
) ON [PRIMARY]

/*
   Monday, October 10, 201112:33:30 PM
   User: myvrm
   Server: gowniyanvm
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_RecurInfoDefunt_D ADD
	SetupDuration int NULL,
	TeardownDuration int NULL
GO
COMMIT


/* ********************* FB 2218.sql - end   ****************** */


/* ********************* FB 2268_HelpME.sql - start ****************** */

/*        ************************** Help Me Icon  Start*****************************  */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	HelpReqEmailID nvarchar(512) NULL,
	HelpReqPhone nvarchar(50) NULL
	
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	HelpReqEmailID nvarchar(512) NULL,
	HelpReqPhone nvarchar(50) NULL
GO
COMMIT

/* ************************** Help Me Icon End *****************************  */

/* ********************* FB 2268_HelpME.sql - end   ****************** */


/* ********************* FB 2038.sql - start ****************** */


update Org_Settings_D set TelepresenceFilter = isnull(TelepresenceFilter,0)

update loc_room_d set isTelepresence = isnull(isTelepresence,0)

/* ********************* FB 2038.sql - end   ****************** */


/* ********************* FB 2269.sql - start ****************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	isDeptUser smallint NULL
GO
COMMIT

update Org_Settings_D set isDeptUser = 1

/* ********************* FB 2269.sql - end   ****************** */


/* ********************* FB 2038_1.sql - start ****************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnablePIMServiceType smallint 
GO
COMMIT 

update Org_Settings_D set EnablePIMServiceType= 0

/* ********************* FB 2038_1.sql - end   ****************** */


/* ********************* FB 2283.sql - start ****************** */

update icons_ref_s set IconTarget = 'MasterChildReport.aspx' where iconid = 21

/* ********************* FB 2283.sql - end   ****************** */


/* ************* err.sql  - start ******************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Err_List_S ADD
	UID int NOT NULL IDENTITY (1, 1),
	Languageid smallint NULL,
	ErrorType nchar(1) NULL
GO
ALTER TABLE dbo.Err_List_S ADD CONSTRAINT
	DF_Err_List_S_Languageid DEFAULT 1 FOR Languageid
GO
ALTER TABLE dbo.Err_List_S ADD CONSTRAINT
	DF_Err_List_S_ErrorType DEFAULT N'S' FOR ErrorType
GO
COMMIT


-- update existing error messages with language id as 1 for english
update err_list_s set languageid=1


/* QUERY FOR  CounterInvite COMMAND START*/
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (532,'User ID or Conference ID is invalid!',
'User ID or Conference ID is invalid!','S',1,'U');
/* QUERY FOR  CounterInvite COMMAND END*/

/* **************** err.sql - end ******************** */


/***************** LDAP SETTINGS  Starts***********************************/

Insert into Ldap_ServerConfig_D (serveraddress, login, password, port, timeout, schedule, SyncTime,
SearchFilter, LoginKey, domainPrefix, scheduleTime, scheduleDays) values ('vrmdc01', 'jvideo', 'FNFGkFuJJJ4kSq1DT8zyqg==', 389, 10, 0, '8/19/2011 8:00:00 AM', 'CN=Users', '', 'myvrm\', '8/19/2011 8:00:00 AM', '')

/***************** LDAP SETTINGS  Ends***********************************/
--FB 2027 - SetSuperAdmin end

/* QUERY FOR  SetBulkUserDepartment COMMAND START*/

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (533,'No Department Found !',
'No Department Found !','S',1,'U');

/* QUERY FOR  SetBulkUserDepartment COMMAND END*/

/* ************************** V2.4 sql scripts - end ******************** */