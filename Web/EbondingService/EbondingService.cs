﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End
#region References
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;
using System.IO;
using System.Text;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Collections.Generic;
using System.ServiceProcess;

#endregion

namespace Ebonding
{
    #region Ebonding
    public partial class EbondingService : ServiceBase
    {
        private static DataTable _dtConfEPTs = null;
        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        static String COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
        static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
        static ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        static VRMRTC.VRMRTC obj = null;
        static System.Timers.Timer timerEbonding = new System.Timers.Timer();
        static NS_CONFIG.Config config = null;
        static NS_MESSENGER.ConfigParams configParams = null;
        static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        static string errMsg = null;
        static NS_LOGGER.Log log = null;
        static bool ret = false;

        public EbondingService()
        {
            InitializeComponent();
        }

        #region OnStart
        protected override void OnStart(string[] args)
        {
            double conflauch = 5000;
            try
            {
                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath);
                log = new NS_LOGGER.Log(configParams);

                log.Trace("Into The service started");
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);

                timerEbonding.Elapsed += new System.Timers.ElapsedEventHandler(timerEbonding_Elapsed);
                timerEbonding.Interval = conflauch;
                timerEbonding.Enabled = true;
                timerEbonding.AutoReset = true;
                timerEbonding.Start();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region timerEbonding_Elapsed

        static void timerEbonding_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double conflauch = 5000;
            try
            {
                timerEbonding.Stop();

                if (obj == null)//FB 2363 start
                    obj = new VRMRTC.VRMRTC();

                obj.Operations(RTC_ConfigPath, "TriggerEventService", "<admin>11</admin>");

                GenerateESUserReport(); //FB 2363 end

				//Commented for this, because not deliver for this Phase II delivery
                //GenerateESErrorReport(); 

                timerEbonding.Interval = conflauch;
                timerEbonding.Enabled = true;
                timerEbonding.AutoReset = true;
                timerEbonding.Start();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region GenerateESUserReport
        /// <summary>
        /// GenerateESUserReport
        /// </summary>
        /// <returns></returns>
        private static bool GenerateESUserReport()
        {
            try
            {
                String inXML = "", OutXML = "";

                inXML = "<report>";
                inXML += "<configpath>" + MyVRMServer_ConfigPath + "</configpath>";
                inXML += "<reportType>GU</reportType>";
                inXML += "<export>1</export>";
                inXML += "<Destination>" + configParams.reportFilePath + "</Destination>";
                inXML += "<fileName>UserReport.xls</fileName>";
                inXML += "</report>";

                log.Trace("Error in Generate User Report: " + inXML);

                myvrmCom = new ASPIL.VRMServer();
                OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GenerateESUserReport", inXML);
                if (OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Generate User Report: " + OutXML);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GenerateESErrorReport
        /// <summary>
        /// GenerateESUserReport
        /// </summary>
        /// <returns></returns>
        private static bool GenerateESErrorReport()
        {
            try
            {
                String inXML = "", OutXML = "";

                inXML = "<report>";
                inXML += "<configpath>" + MyVRMServer_ConfigPath + "</configpath>";
                inXML += "<reportType>GE</reportType>";
                inXML += "<export>1</export>";
                inXML += "<Destination>" + configParams.reportFilePath + "</Destination>";
                inXML += "<fileName>ErrorReport.xls</fileName>";
                inXML += "</report>";

                log.Trace("Error in Generate User Report: " + inXML);

                myvrmCom = new ASPIL.VRMServer();
                OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GenerateESErrorReport", inXML);
                if (OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Generate User Report: " + OutXML);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region OnStop

        protected override void OnStop()
        {
            timerEbonding.Enabled = false;
            timerEbonding.AutoReset = false;
            timerEbonding.Stop();
        }
        #endregion
    }
    #endregion
}
