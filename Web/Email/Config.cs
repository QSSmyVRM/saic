//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : Config.cs
 * DESCRIPTION : All configuration file/db parameters are read in this file. 
 * AUTHOR : Kapil M
 */
namespace NS_CONFIG
{
	#region References
	using System;
	using System.Xml;
	using System.Diagnostics;
	using System.IO;
	using cryptography;
	#endregion 

	class Config 
	{	
		internal string errMsg = null;

		internal Config ()
		{
			// constructor	
		}

		internal bool Initialize (string configPath,ref NS_MESSENGER.ConfigParams configParams)
		{			
			configParams.localConfigPath = configPath;

			bool ret = Read_Local_Config_File(ref configParams);
			if (!ret) 
			{
				this.errMsg = "Error reading local config file.";
				return (false);
			}

			ret = false;
			ret = Read_Global_Config_File(ref configParams);
			if (!ret) 
			{
				this.errMsg = "Error reading global config file.";
				return (false);
			}
			
			ret = false;
			ret = Read_Database_Params(ref configParams);
			if (!ret) 
			{
				this.errMsg = "Error reading database params.";
				return (false);
			}

			return(true);
		}

		private bool Read_Local_Config_File(ref NS_MESSENGER.ConfigParams configParams)
		{
			// Read from XML config file 
			try 
			{ 
				// Open an XML file 
				XmlDocument xmlConfig = new XmlDocument();
				xmlConfig.Load(configParams.localConfigPath);

				// main config file path
                try
                {
                    configParams.globalConfigPath = xmlConfig.SelectSingleNode("//EmailConfig/VRM_Config_File_Path").InnerXml.Trim();                  
                }
                catch
                {
                    this.errMsg = "Error reading local config file. VRM_Config_File_Path param is invalid.";
                    return false;
                }
				
                // local component log file path
                try
                {

                    configParams.logFilePath = xmlConfig.SelectSingleNode("//EmailConfig/Log_File_Path").InnerXml.Trim();				    
                }
                catch (Exception)
				{
                    this.errMsg = "Error reading local config file. Log_File_Path param is invalid.";
                    return false;
				}

				// verbose trace switch
                try
                {
                    string debug = xmlConfig.SelectSingleNode("//EmailConfig/Debug").InnerXml.ToUpper().Trim();                   
                    if (debug.Equals("YES"))
                    {
                        configParams.fileTraceEnabled = true;
                    }
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading local config file. Debug param is invalid.";
                    return false;
                }


				
                try
                {
                    configParams.mailLogoPath = xmlConfig.SelectSingleNode("//EmailConfig/MailLogoPath").InnerXml.Trim();
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading local config file. MailLogoPath param is invalid.";
                    return false;
                }
                

				return true;
			}
			catch (Exception e)
			{
				this.errMsg = "Error reading local config file. Exception Message : " + e.Message;				
				return false;
			}			
		}

		private bool Read_Global_Config_File(ref NS_MESSENGER.ConfigParams configParams)
		{
			// Read from XML config file 
			try 
			{ 
				// Open an XML file 
				XmlDocument xmlConfig = new XmlDocument();
				xmlConfig.Load(configParams.globalConfigPath);

                // Database Information

                // Db Server Address
                configParams.databaseServer = xmlConfig.SelectSingleNode("//VRMConfig/Database/Server").InnerXml.Trim();
				
                // Db Name
                configParams.databaseName = xmlConfig.SelectSingleNode("//VRMConfig/Database/Name").InnerXml.Trim();

                // Db Login
				configParams.databaseLogin = xmlConfig.SelectSingleNode("//VRMConfig/Database/Login").InnerXml.Trim();

                // Db Connection Timeout
                try
                {
                    configParams.databaseConTimeout = Int32.Parse(xmlConfig.SelectSingleNode("//VRMConfig/Database/ConnectionTimout").InnerXml.Trim());
                }
                catch (Exception)
                {
                    configParams.databaseConTimeout = 10;
                }
                // Db Trunsted COnnection FB 2700
                try
                {
                    configParams.trustedConnection = xmlConfig.SelectSingleNode("//VRMConfig/Database/trustedConnection").InnerXml.Trim();
                }
                catch (Exception)
                {
                    configParams.trustedConnection = "";
                }


                // Db Password
                string encryptedPwd = xmlConfig.SelectSingleNode("//VRMConfig/Database/Password").InnerXml.Trim();
                // decrypt password
                if (encryptedPwd.Trim() != "")// FB 2700
                {
                    cryptography.Crypto crypto = new Crypto();
                    configParams.databasePwd = crypto.decrypt(encryptedPwd);
                }

                // Db type
				string databaseType = xmlConfig.SelectSingleNode("//VRMConfig/Database/Type").InnerXml.Trim();		
				if (databaseType.CompareTo("SA_SQLServer_Client") !=0)
				{
					// Only MS-SQLSERVER-2000 is supported currently. 
					// Return error if any other string.
					this.errMsg = "This database type is not supported. Only MS SqlServer 2000/2005 are supported.";
					return(false);
				}
				else
				{
					// It is SQL Server .
					configParams.databaseType = NS_MESSENGER.ConfigParams.eDatabaseType.SQLSERVER;
				}
                
                // Client switch
                configParams.client = xmlConfig.SelectSingleNode("//VRMConfig/Client").InnerXml.ToUpper().Trim();								

                // Ldap enabled or disabled
				try
				{
					string ldap= xmlConfig.SelectSingleNode("//VRMConfig/SSOMode").InnerXml.ToUpper().Trim();
					if (ldap.Equals("YES"))
					{
						configParams.ldapEnabled = true;
					}					
				}
				catch (Exception)
				{
                    this.errMsg = "Error reading global config file. SSOMode is invalid.";
                    return false;
				}

				return (true);
			}
			catch (Exception e)
			{
				this.errMsg = "Error reading global config file. Exception Message : " + e.Message;				
				return (false);
			}			
		}


		private bool Read_Database_Params(ref NS_MESSENGER.ConfigParams configParams)
		{
			//fetch the logging params
			try 
			{
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				bool ret = db.FetchLogSettings(ref configParams);
				if (!ret) 
				{
                    this.errMsg = "Invalid log settings in database";
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = "Error reading database settings. Exception Message : " + e.Message;				
				return false;
			}
		}
	}
}	