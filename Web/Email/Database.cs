//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : Database.cs
 * DESCRIPTION : All database-related sql queries are stored in this file.
 * AUTHOR : Kapil M
 */
namespace NS_DATABASE
{
	#region References
	using System;
	using System.Data;
	using System.Data.SqlClient;
	using System.Xml;
	using System.Collections;
	using cryptography;
	using System.Threading;
	#endregion 
	
	class Database
	{
		private NS_LOGGER.Log logger;
		
		private NS_MESSENGER.ConfigParams configParams;
	
		internal string errMsg = null;
		private string conStr = null;

		#region Constructor				
		internal Database(NS_MESSENGER.ConfigParams config)
		{
			// assign the config params
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;

            if (configParams.trustedConnection.Trim().ToLower() == "true") //FB 2700
                conStr = "Trusted_Connection = true ";
            else
            {
                conStr = "User ID = " + configParams.databaseLogin;
                conStr += ";Password = " + configParams.databasePwd;
            }

            // connection string
			conStr += ";database=" + configParams.databaseName ;
			conStr += ";Data Source=" + configParams.databaseServer ;
			conStr += ";Connect Timeout = "+ configParams.databaseConTimeout.ToString();

			logger = new NS_LOGGER.Log(configParams);

		}
       
		#endregion

		#region Query executor
		private bool SelectCommand (string query,ref DataSet ds , string dsTable)
		{
			logger.Trace(query);
            SqlConnection con;
            try
            {
                con = new SqlConnection(conStr);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
			try
			{

				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataAdapter da = new SqlDataAdapter();
				con.Open();	
				da.SelectCommand = cmd;
				da.Fill(ds,dsTable);
                da.Dispose();
                con.Close();

				return (true);
			}
			catch (Exception e)
			{
				logger.Exception(100,"Query = " + query + ", Error = " + e.Message);
				con.Close();
				return(false);
			}
			
		}

		private bool NonSelectCommand (string query)
		{
			logger.Trace(query);
			SqlConnection con;
			try
			{
				con = new SqlConnection(conStr);
			}
			catch (Exception e) 
			{
			    logger.Exception(100,e.Message);				
			    return(false);
			}

			try
			{
				con.Open();	
				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataReader reader = cmd.ExecuteReader();				
				reader.Close();
				con.Close();				
				return (true);
			}
			catch (Exception e)
			{
				logger.Exception(100,"Query = " + query + ", Error = " + e.Message);
				con.Close();
				return(false);
			}	
		}
		#endregion		

		#region Email Methods
		internal bool FetchSmtpServerInfo (ref NS_MESSENGER.Smtp smtp)
		{
			try
			{
				// Retreive smtp server settings
				DataSet ds = new DataSet();
				string dsTable = "SMTP";
				string query = "Select * from Sys_MailServer_D";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}		
				
				if (ds.Tables[dsTable].Rows.Count < 1) 
				{
					logger.Trace("No SMTP Server defined in system.");
					return false;
				}

				smtp.ServerAddress = ds.Tables[dsTable].Rows[0]["ServerAddress"].ToString().Trim(); 
				logger.Trace ("SMTP Server : " + smtp.ServerAddress);

                try
                {
                    smtp.Login = ds.Tables[dsTable].Rows[0]["Login"].ToString().Trim(); //login
                    logger.Trace("SMTP Login : " + smtp.Login);

                    smtp.Password = ds.Tables[dsTable].Rows[0]["Password"].ToString().Trim();
                }
                catch (Exception e)
                {
                    logger.Trace("SMTP Login or Password missing.");
                }
				smtp.PortNumber = Int32.Parse(ds.Tables[dsTable].Rows[0]["PortNo"].ToString().Trim()); 
				smtp.ConnectionTimeOut = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConTimeOut"].ToString().Trim()); 
				smtp.CompanyMailAddress = ds.Tables[dsTable].Rows[0]["CompanyMail"].ToString().Trim();
				smtp.DisplayName = ds.Tables[dsTable].Rows[0]["DisplayName"].ToString().Trim();
                smtp.RetryCount = Int32.Parse(ds.Tables[dsTable].Rows[0]["RetryCount"].ToString().Trim());//FB 2552	
				//smtp.FooterMessage = ds.Tables[dsTable].Rows[0]["MessageTemplate"].ToString().Trim(); //commented for FB 1710

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);	
				return false;
			}		
		}
         
		internal bool FetchNewEmails(ref Queue newEmailQueue)
		{	
			try
			{
				// Retrive all emails.
				DataSet ds = new DataSet();
				string dsTable = "Emails";
                string query = "Select [From],[To],CC,BCC,Subject,Message,UUID,RetryCount,LastRetryDateTime,Attachment,Iscalendar,orgID from Email_Queue_D where (orgID not in (Select orgid from org_settings_d where mailblocked = 1) and [from] not in (Select email from usr_list_d where mailblocked =1)) or release = 1"; //ICAL Fix // orgid Added for FB 1710
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    try
                    {
                        // Retreive email components and add to email object 
                        NS_MESSENGER.Email email = new NS_MESSENGER.Email();                        
                        try
                        {
                            email.From = ds.Tables[dsTable].Rows[i]["From"].ToString().Trim();
                        }
                        catch (Exception)
                        {
                            logger.Trace("No FROM address...");
                        }                        
                        email.To = ds.Tables[dsTable].Rows[i]["To"].ToString().Trim();

                        email.CC = ds.Tables[dsTable].Rows[i]["CC"].ToString().Trim();

                        email.BCC = ds.Tables[dsTable].Rows[i]["BCC"].ToString().Trim();

                        email.Subject = ds.Tables[dsTable].Rows[i]["Subject"].ToString().Trim();

                        email.Body = ds.Tables[dsTable].Rows[i]["Message"].ToString().Trim();

                        email.UUID = Int32.Parse(ds.Tables[dsTable].Rows[i]["UUID"].ToString().Trim());

                        email.orgID = Int32.Parse(ds.Tables[dsTable].Rows[i]["orgID"].ToString().Trim()); //Added for FB 1710

                        email.isCalender = Int32.Parse(ds.Tables[dsTable].Rows[i]["Iscalendar"].ToString().Trim()); //ICAL Fix
                        
                        if (ds.Tables[dsTable].Rows[i]["Attachment"].ToString() != null)
                        {
                            if (ds.Tables[dsTable].Rows[i]["Attachment"].ToString().Length > 3)
                            {
                                email.Attachment = ds.Tables[dsTable].Rows[i]["Attachment"].ToString().Trim();
                                if (email.Attachment.Contains("BEGIN:VCALENDAR") == true) //FB 2410
                                {
                                    email.Attachment = email.Attachment.Remove(0, (email.Attachment.IndexOf("BEGIN:VCALENDAR")));
                                    //email.Attachment = email.Attachment.Remove(email.Attachment.IndexOf("END:VCALENDAR")+12);
                                    email.Attachment = email.Attachment.Replace("` + CHAR(13) + CHAR(10) + `", "\r\n");
                                }
                            }
                        }
                        
                        if (ds.Tables[dsTable].Rows[i]["RetryCount"].ToString().Trim().Length < 1)
                        {
                            email.RetryCount = 0;
                        }
                        else
                        {
                            email.RetryCount = Int32.Parse(ds.Tables[dsTable].Rows[i]["RetryCount"].ToString().Trim());
                        }
                        
                        if (ds.Tables[dsTable].Rows[i]["LastRetryDateTime"].ToString().Trim().Length < 1)
                        {
                            email.LastRetryDateTime = DateTime.Now;
                        }
                        else
                        {
                            email.LastRetryDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["LastRetryDateTime"].ToString().Trim());
                        }
                        
                        //Check for all mandatory fields.
                        // If empty skip and go to next email record.	
                        if ((email.To.Length > 3 || email.CC.Length > 3 || email.BCC.Length > 3) && email.Body.Length > 1)
                        { //email.From.Length > 3 &&
                            if (email.To.Length > 3)
                            {
                                logger.Trace("Valid email. To = " + email.To);
                            }
                            else
                            {
                                if (email.CC.Length > 3)
                                    logger.Trace("Valid email. CC = " + email.CC);
                                else
                                    logger.Trace("Valid email. BCC = " + email.BCC);
                            }
                        
                            newEmailQueue.Enqueue(email);
                        }
                        
                    }
                    catch (Exception e)
                    {
                        logger.Trace("Invalid email. Error = " + e.Message);
                    }
                }
				
				logger.Trace(" Total valid email count = " + newEmailQueue.Count.ToString());
				return (true);
			}
			catch (Exception e) 
			{	
				logger.Exception(100,e.Message);
				return false;
			}						
		}
			
		internal void DeleteEmail(NS_MESSENGER.Email emailObj)
		{
			// Delete the sent emails.
			try 
			{
				string query = "Delete from Email_Queue_D where UUID = " + emailObj.UUID.ToString();
				bool ret = NonSelectCommand(query);
				if (!ret)
				{
					logger.Trace("SQL error");
				}

			}
			catch (Exception e) 
			{	
				logger.Exception(100,e.Message);
			}

		}
		
		internal void UpdateUnsentEmail(NS_MESSENGER.Email email)
		{
			// Update the unsent emails
			try 
			{
				string query = " Declare @UUID int";
				query += " Set @UUID = " + email.UUID.ToString();
				query += " if ((select  max(retrycount) ";
				query += " from email_queue_d where UUID = @UUID) IS NULL )";
				query += " Update Email_Queue_D set RetryCount = 1, LastRetryDateTime = getdate()";
				query += " where UUID = @UUID";
				query += " else ";
				query += " Update Email_Queue_D set RetryCount = RetryCount +1, LastRetryDateTime = getdate()";
				query += " where UUID = @UUID";

				bool ret = NonSelectCommand(query);
				if (!ret)
				{
					logger.Trace("SQL error");
				}

			}
			catch (Exception e) 
			{	
				logger.Exception(100,e.Message);
			}

		}
		#endregion

		#region Logger Methods
		internal void InsertLogRecord (int errorCode,int severity,string file, string function,int line, string message)
		{
			// Insert log record.	
			try
			{
				// query
				string query = "Insert into Err_Log_D (modulename,moduleErrCode,severity,[file],line,timesubmitted,timelogged,message) values (";
				query += "'RTC'," + errorCode.ToString() + "," + severity.ToString() + ",'" + file + "'," + line.ToString() + ",getutcdate(),getutcdate(),'"+ message + "')";
			
				// open a connection and insert the record.
				SqlConnection con;
				con = new SqlConnection(conStr);
				con.Open();	
				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataReader reader = cmd.ExecuteReader();				
				reader.Close();
				con.Close();				
			}
			catch (Exception)
			{
				// do nothing
			}	
		}	
		
		internal bool FetchLogSettings (ref NS_MESSENGER.ConfigParams dbConfigParams)
		{						
			try
			{
				SqlConnection con ;
				DataSet ds = new DataSet();
				string dsTable = "LogSettings";
				con = new SqlConnection(conStr);
				string query = "Select loglevel from Err_LogPrefs_S where LogModule Like 'RTC'";
				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataAdapter da = new SqlDataAdapter();
				con.Open();	
				da.SelectCommand = cmd;
				da.Fill(ds,dsTable);
				con.Close();
				
				//loglevel
				int logLevel = 0;
				if (ds.Tables[dsTable].Rows.Count > 0)

				{
					logLevel = Int32.Parse(ds.Tables[dsTable].Rows[0]["loglevel"].ToString());
				}

				switch (logLevel) 
				{
					case 0: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.SYSTEM_ERROR;
						break;
					}
					case 1: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.USER_ERROR;
						break;
					}
					case 2: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.WARNING;
						break;
					}
					case 3: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.INFO;
						break;
					}
					case 9: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.DEBUG;
						break;
					}
					default :
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.SYSTEM_ERROR;
						break;
					}
                        
				}				
				
				return true;
			}
			catch (Exception)
			{				
				return false;
			}				
		}

		
		internal bool FetchLdapSettings (ref NS_MESSENGER.LdapSettings ldap)
		{						
			try
			{
				string query = "Select * from Ldap_ServerConfig_D";
				DataSet ds = new DataSet();
				string dsTable = "LDAPSettings";
				bool ret = SelectCommand (query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}

				if (ds.Tables[dsTable].Rows.Count < 1)
				{

					logger.Trace ("No LDAP server defined.");
					return false;
				}
				
				// ldap server params
				ldap.serverAddress = ds.Tables[dsTable].Rows[0]["ServerAddress"].ToString().Trim();
				ldap.serverLogin = ds.Tables[dsTable].Rows[0]["Login"].ToString().Trim();
				ldap.serverPassword = ds.Tables[dsTable].Rows[0]["Password"].ToString().Trim();
				
				//decrypt pwd
				cryptography.Crypto crypto = new Crypto();
				ldap.serverPassword = crypto.decrypt(ldap.serverPassword);
				
				ldap.connPort = Int32.Parse(ds.Tables[dsTable].Rows[0]["Port"].ToString().Trim());
				ldap.sessionTimeout = Int32.Parse(ds.Tables[dsTable].Rows[0]["Timeout"].ToString().Trim());
				ldap.loginKey = ds.Tables[dsTable].Rows[0]["LoginKey"].ToString().Trim();
				ldap.searchFilter = ds.Tables[dsTable].Rows[0]["SearchFilter"].ToString().Trim();
				
				// Replace "&amp;" with "&" as ldap search filters need that. 
                ldap.searchFilter = logger.RevertBackTheInvalidChars(ldap.searchFilter);
 
                //domain prefix
                ldap.domainPrefix = ds.Tables[dsTable].Rows[0]["DomainPrefix"].ToString().Trim();

                //schedule time
                ldap.scheduleTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ScheduleTime"].ToString().Trim());
                
				// maneesh pujari 07/29/2008 FB 594.
                // SyncTime
                try
                {
                    // sync time
                    if (ds.Tables[dsTable].Rows[0]["SyncTime"].ToString() != null)
                    {
                        ldap.syncTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["SyncTime"].ToString().Trim());
                    }
                    else
                    {
                        ldap.syncTime = DateTime.Parse("2000-01-01 01:00:00.000");
                    }
                }
                catch (Exception)
                {
                    ldap.syncTime = DateTime.Parse("2000-01-01 01:00:00.000");
                }

                // schedule days
                if (ds.Tables[dsTable].Rows[0]["ScheduleDays"].ToString() != null)
                {
                    ldap.scheduleDays = ds.Tables[dsTable].Rows[0]["ScheduleDays"].ToString().Trim();
                }

				return true;			
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);		
				return false;
			}									
		}

		#endregion

        /*
        private string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\'", "&apos;;");
            return (input);
        }
        
        private string RevertBackTheInvalidChars(string input)
        {
            input = input.Replace("&lt;","<");
            input = input.Replace("&gt;",">");
            input = input.Replace("&amp;","&");
            input = input.Replace("&quot;","\"");
            input = input.Replace("&quot;","\"");
            input = input.Replace("&apos;","\'");
            return (input);
        }
       */         
		

		#region Misc Tasks

		public bool DeleteUnsentOldEmails()
		{
			try 
			{
				// Delete emails with more than 250 retries 				
				string query = " DELETE FROM Email_Queue_D WHERE retrycount > 20";

				// execute the query
				bool ret = false;
				ret = NonSelectCommand(query);
				if (!ret) 
				{
					// log the error 
					logger.Exception (100,"query failed. query =" + query);						
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,"Error deleting unsent emails.Exception = " + e.Message);
				return false;
			}
		}
		
		public bool InsertEmailInQueue(NS_MESSENGER.Email email)
		{
			// Tech Support Info
			/*			DataSet ds = new DataSet();
						string dsTable = "TechSupport";
						string query = "Select name,email,phone,info from Sys_TechSupport_D ";
						bool ret = SelectCommand(query,ref ds,dsTable);
						if (!ret)
						{
							logger.Exception(100,"SQL error");
							return (false);
						}	

						string tsName = ds.Tables[dsTable].Rows[0]["Name"].ToString().Trim();
						string tsEmail = ds.Tables[dsTable].Rows[0]["Email"].ToString().Trim();
						string tsPhone = ds.Tables[dsTable].Rows[0]["Phone"].ToString().Trim();
						string tsInfo = ds.Tables[dsTable].Rows[0]["Info"].ToString().Trim();

						email.Body += "<BR><BR><BR>Technical Support:";
						email.Body += "<BR>---------------------";
						email.Body += "<BR>Name : "+ tsName;
						email.Body += "<BR>Email : "+ tsEmail;
						email.Body += "<BR>Phone : "+ tsPhone;
						if (tsInfo.Length > 1)
						{
							email.Body += "<BR>Additional Info : "+ tsInfo;
						}

						// Disclaimer Text
						DataSet ds1 = new DataSet();
						string dsTable1 = "Email";
						string query1 = "Select MessageTemplate from Sys_MailServer_D ";
						bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
						if (!ret1)
						{
							logger.Exception(100,"SQL error");
							return (false);
						}	

						string disclaimerText = ds1.Tables[dsTable1].Rows[0]["MessageTemplate"].ToString().Trim();
						if (disclaimerText.Length > 3)
						{
							email.Body += "<BR><BR>";
							email.Body += "<BR>---------------";
							email.Body += "<BR>" + disclaimerText;
						}

						// closing the tags
						email.Body += "</body></html>";
			*/
			// insert email in queue
			string query = " Declare @uuid as int";
			query +=" set @uuid = (select max(uuid) from email_queue_d)";
            query += "if (@uuid IS NULL) begin set @uuid = 0 end ";
			query += "Insert  into Email_Queue_D ([from],[to],subject,datetimecreated,message,uuid) values (";
			query += "'" + 	email.From + "','" + email.To + "','" + email.Subject + "',getutcdate(),'" + email.Body + "',(@uuid+1))";

			// execute the query
			bool ret = NonSelectCommand(query);
			if (!ret) 
			{
				// log the error 
				logger.Exception (100,"query failed. query =" + query);						
				return false;
			}		
		
			return true;
		}
		
		#endregion		

		public bool ShrinkDbTransLogFile()
		{
			try 
			{
				// Fetch the db trans log
				DataSet ds = new DataSet();
				string dsTable = "ShrinkLog";
				string query = "Select Name,Filename from sysfiles Where FILEID=2";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}		

				// log file details
				string logicalName = ds.Tables[dsTable].Rows[0]["Name"].ToString().Trim();
				string fileName = ds.Tables[dsTable].Rows[0]["FileName"].ToString().Trim();

				// Truncate the transaction log with no backup taken
				// Hopefully, the SQL Admin is taking regular backups becos on execution of this there is no going back.
				query = "BACKUP LOG " + configParams.databaseName.Trim() + " WITH TRUNCATE_ONLY";
				// Shrink the transaction log to 1 GB = 1024 MB(magic number)
				query += " DBCC SHRINKFILE (" + logicalName.Trim() + ",1024)";
				ret = false;
				ret = NonSelectCommand (query);
				if (!ret)
				{
					logger.Exception(10,"Error executing query.");
					return false;
				}
		
				return true;	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		public bool DeleteLogRecords()
		{
			try 
			{
				// Fetch the db trans log
				DataSet ds = new DataSet();
				string dsTable = "DeleteLog";
				string query = "select logmodule, loglife from Err_logPrefs_S";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}		
				
				// cycle thru each of the modules
				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{				
					try
					{
						// retreive module's log prefs
						string moduleName = ds.Tables[dsTable].Rows[i]["logmodule"].ToString().Trim();
						logger.Trace ("Module name : "+ moduleName);
						logger.Trace ("Log Life (in days) before : "+ ds.Tables[dsTable].Rows[i]["loglife"].ToString().Trim());
						int logLife = 7;
						try
						{
							// TODO: Weird exception being thrown.
							logLife = Int32.Parse(ds.Tables[dsTable].Rows[i]["loglife"].ToString().Trim());	
							logger.Trace ("Log Life (in days) : "+ logLife.ToString("s"));
						}
						catch (Exception e)
						{
							logger.Trace (e.Message+ "---" + e.StackTrace  );
						}
						// 0 = do not delete log records ever
						if (logLife !=0)
						{
							logger.Trace ("Log Life 2 (in days) : "+ logLife.ToString("s"));
							DateTime td = DateTime.Today; 
							TimeSpan ts = new TimeSpan(logLife,0,0,0);						
							DateTime diff = td.Subtract(ts);

							// delete all the logs for the module
							string query1 = "delete from Err_Log_D where timelogged <'" + diff.ToString("G").Trim() + "' and modulename LIKE '" + moduleName + "'";
							ret = false;
							ret = NonSelectCommand (query1);
							if (!ret)
							{
								logger.Exception(10,"Error executing query.");
								return false;
							}
						}
					}
					catch (Exception e)
					{
						logger.Trace("Invalid log setting for module. Error : " + e.Message);
						continue;
					}
				}
		
				return true;	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		
		public bool DeleteAllLogRecords()
		{
			try 
			{
				// just truncate the table so that all records are dropped		
				string query = "delete from err_log_d";
				bool ret = NonSelectCommand(query);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}						
				return true;	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		
		public bool FetchAllUserEmails(ref Queue emailq)
		{
			DataSet ds = new DataSet();
			string dsTable = "Emails";
			string query = "select email from usr_list_d ";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// user object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}
			
			ds.Clear();
			query = "select email from usr_guestlist_d where deleted = 0";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// room object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}
			
			ds.Clear();
			query = "select email from usr_inactive_d";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// room object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}

			ds.Clear();
            query = "select email from ldap_blankuser_d";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}					
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// room object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}

			return true;
		}

        public bool GetTechSupportEmail(ref string strTechSupportEmail)
        {
            // In this case the Email would be send from TechSupport User Account
            // So first Retrieve the Tech Support Email Information
            // FB 377- Maneesh code Start
            DataSet dsTechSupport = new DataSet();
            string strTableName = "TechSupport";
            string queryFetchSupportInfo = "Select email from Sys_TechSupport_D ";
            bool bRetVal = SelectCommand(queryFetchSupportInfo, ref dsTechSupport, strTableName);
            if (!bRetVal)
            {
                logger.Trace("myVRM Tech support information not found in the database.");
                return false;
            }
            // fetch the Support Email ID from the returned data set.
            strTechSupportEmail = dsTechSupport.Tables[strTableName].Rows[0]["email"].ToString().Trim();
            if (strTechSupportEmail.Length <= 0)
            {
                logger.Trace("myVRM Tech support email not found in the database.");
                return false;
            }
            return true;
            // FB 377- Maneesh code End
        }

        internal bool FetchUserLoginHistory(NS_MESSENGER.Party party,DateTime fromDate, DateTime toDate, ref Queue loginDateList)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "GetUserHistory";
                string query = null;

                query = "select logindate from sys_loginaudit_d where userid = " + party.iDbId.ToString();
                query += " and logindate between ('" + fromDate.ToString() + "')";
                query += " AND ('"+ toDate.ToString() +"')";
                query += " order by logindate desc";                 
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {

                    DateTime loginDate = DateTime.Parse(ds.Tables[dsTable].Rows[i][0].ToString().Trim());
                    logger.Trace("Login Date : " + loginDate.ToString());

                    // add to queue 
                    loginDateList.Enqueue(loginDate);
                }

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                return false;
            }
        }
        
        #region Fetch Company Mail Logo
        internal int FetchMailLogo(int orgid, ref byte[] mailLogoImage)
        {
            int mailLogo = 0;
            try
            {
                mailLogoImage = null;
                DataSet ds = new DataSet();
                String dsTable = "OrgSettings";
                String query = "";

                query = "select  a.mailLogo, (select Attributeimage from img_list_d where imageid=a.mailLogo)";
                query += " as MailLogoImage from Org_Settings_d a where orgid=" + orgid;
                                
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return 0;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["mailLogo"] != null)
                            {
                                Int32.TryParse(ds.Tables[0].Rows[0]["mailLogo"].ToString().Trim(), out mailLogo);
                            }

                            if (ds.Tables[0].Rows[0]["MailLogoImage"] != null)
                            {
                                if (ds.Tables[0].Rows[0]["MailLogoImage"].ToString() != "")
                                    mailLogoImage = (byte[])ds.Tables[0].Rows[0]["MailLogoImage"];
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return 0;
            }
            return mailLogo;
        }
        #endregion
        //Embedded Image Ends...

        //Method added for FB 1710
        #region Fetch Footer Message of Organisation
        internal string FetchFooterMessage(int orgid, ref byte[] footerImage)
        {
            String footermessage = "";
            try
            {

                DataSet ds = new DataSet();
                String dsTable = "OrgSettings";
                String query = "";

                query = "select  a.FooterMessage, (select Attributeimage from img_list_d where imageid=a.footerimage)";
                query += " as FooterImg from Org_Settings_d a where orgid=" + orgid;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["FooterMessage"] != null)
                                footermessage = ds.Tables[0].Rows[0]["FooterMessage"].ToString().Trim();

                            if (ds.Tables[0].Rows[0]["FooterImg"] != null)
                            {
                                if (ds.Tables[0].Rows[0]["FooterImg"].ToString() != "")
                                    footerImage = (byte[])ds.Tables[0].Rows[0]["FooterImg"];
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return null;
            }
            return footermessage;
        }

        #endregion

        //Method added for FB 1758 - This method is modified during FB 1830
        #region Fetch SytTech Info
        /// <summary>
        /// FetchTechInfo
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="emailBody"></param>
        internal void FetchTechInfo(int orgid, ref string emailBody)
        {
            try
            {
                DataSet ds = new DataSet();
                String dsTable = "SysTechInfo";
                String query = "";
                string contact = "";
                string emailid = "";
                string phone = "";

                query = "select [Name], email, phone from Sys_TechSupport_D where orgid=" + orgid;
                
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    //return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["Name"] != null)
                            {
                                contact = ds.Tables[0].Rows[0]["Name"].ToString().Trim();
                            }
                            if (ds.Tables[0].Rows[0]["email"] != null)
                            {
                                emailid = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                            }
                            if (ds.Tables[0].Rows[0]["phone"] != null)
                            {
                                phone = ds.Tables[0].Rows[0]["phone"].ToString().Trim();
                            }
                        }
                    }
                }
                emailBody = emailBody.Replace("{37}", contact);
                emailBody = emailBody.Replace("{38}", emailid);
                emailBody = emailBody.Replace("{39}", phone);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return "";
            }
        }
        #endregion

        //Method added for FB 1830 - start
        #region FetchEmailString
        /// <summary>
        /// FetchEmailString
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="emailbody"></param>
        /// <param name="emailsubject"></param>
        internal void FetchEmailString(int orgid, ref string emailBody, ref string emailSubject, int emailType)
        {
            emailBody = "";
            emailSubject = "";
            try
            {
                DataSet ds = new DataSet();
                String dsTable = "EmailContent";
                String query = "";

                if (orgid < 11)
                    orgid = 11; //default company

                query = "select emailbody, emailsubject from email_content_d where emailmode=0 and emailtypeid='" + emailType + "'";
                query += " and emaillangid=(select emaillangid from Org_Settings_d where orgid='" + orgid + "')";
                

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    //return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["emailbody"] != null)
                                emailBody = ds.Tables[0].Rows[0]["emailbody"].ToString().Trim();

                            if (ds.Tables[0].Rows[0]["emailsubject"] != null)
                                emailSubject = ds.Tables[0].Rows[0]["emailsubject"].ToString().Trim();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return null;
            }
        }
        #endregion
        //Method added for FB 1830 - end

        public bool UpdateMailServerLastRunDateTime()
        {
            try
            {
                string query = "update sys_mailserver_d set lastrundatetime = getutcdate()";
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

	}
}

