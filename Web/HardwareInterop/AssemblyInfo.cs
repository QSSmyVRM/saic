//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End
/* FILE : AssemblyInfo.cs
 * DESCRIPTION : General Information about an assembly is controlled through the following 
 * set of attributes. Change these attribute values to modify the information 
 * associated with an assembly.
 * AUTHOR : Kapil M
 */
 
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("myVRMRTC")]
[assembly: AssemblyDescription("myVRM Real-Time Component")]
[assembly: AssemblyConfiguration("Version 2.3")]
[assembly: AssemblyCompany("myVRM.com")]
[assembly: AssemblyProduct("myVRM")]
[assembly: AssemblyCopyright("Copyright 2002-2011 myVRM Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("2.3.0.0")]

// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("vrmrtckey.snk")]
[assembly: AssemblyKeyName("")]
