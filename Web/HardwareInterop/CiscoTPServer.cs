﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : CiscoTPServer.cs
 * DESCRIPTION : All CiscoTPServer MCU api commands are stored in this file.
 */
namespace NS_CiscoTPServer
{
    using System;
    using System.Xml;
    using System.Collections;
    using System.Runtime.InteropServices;
    using vbxmlrpc_Net;
    using System.Linq;
    using System.Collections.Generic;

    class CiscoTPServer
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        private int confEnumerateID = 0;
        private int participantEnumerateID = 0;
        private NS_MESSENGER.ConfigParams configParams;
        private int indexCDR = 0;
        private NS_DATABASE.Database db;

        internal CiscoTPServer(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }

        #region Terminal Control Methods

        #region Struct Creation
        /// <summary>
        /// CreateStruct
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="party"></param>
        /// <param name="conf"></param>
        /// <param name="oStruct"></param>
        /// <returns></returns>
        private bool CreateStruct(string operation, NS_MESSENGER.Party party, NS_MESSENGER.Conference conf, ref vbxmlrpc_Net.XMLRPCStruct oStruct)
        {
            string bridgeIP = "", bridgeLogin = "", bridgePwd = "";
            NS_MESSENGER.MCU cMCU = null;
            vbxmlrpc_Net.XMLRPCStruct oStructParty = null;
            XMLRPCArray oPartyArray = null;
            string partyAddress = "";
            int Gindex = 0;
            int Dindex = 0;
            string phonePrefix = "";
            int audioCount = 0; int videoCount = 0;
            NS_MESSENGER.Party tempParty = null;
            int partyCount = 0;
            int codianLineRate = 384;
            bool ret2 = false;
            try
            {
                //Exception throws for Conference.Status.
                operation = operation.Trim();
                logger.Trace("Entering create struct for operation = " + operation);

                if (party != null && party.cMcu.sIp != null)
                    cMCU = party.cMcu;
                else if (conf != null && conf.cMcu != null)
                    cMCU = conf.cMcu;
                else
                    return false;

                bridgeIP = cMCU.sIp.Trim();
                logger.Trace("Cisco Bridge IP: " + bridgeIP);
                bridgeLogin = cMCU.sLogin.Trim();
                logger.Trace("Cisco Bridge Login: " + bridgeLogin);
                bridgePwd = cMCU.sPwd.Trim();
                logger.Trace("Cisco Bridge Pwd: " + bridgePwd);

                // check if bridge params are not empty
                if (bridgeIP.Length < 1 || bridgeLogin.Length < 1 || bridgePwd.Length < 1)
                {
                    logger.Exception(100, "Bridge details are not correct.");
                    return false;
                }

                // Struct object : Add the authentication parameters
                oStruct.AddString("authenticationUser", bridgeLogin);
                oStruct.AddString("authenticationPassword", bridgePwd);

                // Different method calls on the mcu.
                logger.Trace("Operation = " + operation);
                switch (operation)
                {
                    case "TestMCUConnection":
                        {
                            #region TestMCUConnection
                            //Internally left blank
                            break;
                            #endregion
                        }
                    case "TerminateConference":
                        {
                            #region TerminateConference
                            // Terminate an ongoing conf.
                            oStruct.AddString("conferenceGUID", conf.sGUID);
                            break;
                            #endregion
                        }
                    case "ExtendConferenceEndTime":
                        {
                            #region ExtendConferenceEndTime
                            oStruct.AddString("conferenceGUID", conf.sGUID);
                            // Add the conference duration in "seconds"
                            oStruct.AddInteger("duration", (conf.iDuration * 60));//This parameter is not allowed if persistent is true
                            logger.Trace("duration: " + (conf.iDuration * 60));
                            break;
                            #endregion
                        }
                    case "TerminateParty":
                        {
                            #region Terminate
                            // Terminate a party in an existing conf.
                            oStruct.AddString("conferenceGUID", conf.sGUID);
                            oStruct.AddString("participantListGUID", party.sGUID);
                            //oStruct.AddString("participantList", party.sAddress);
                            break;
                            #endregion
                        }
                    case "MuteAudioReceiveParty":
                        {
                            #region MuteAudioReceiveParty

                            oStruct.AddString("participantGUID", party.sGUID);
                            oStruct.AddBoolean("rxAudioMute", party.bMute);
                            break;

                            #endregion
                        }
                    case "ModifyEndpoint":
                        {
                            #region ModifyEndpoint

                            logger.Trace("Trace1" + party.sGUID + conf.sDbName + party.sName);
                            oStruct.AddString("participantGUID", party.sGUID);
                            break;

                            #endregion
                        }
                    case "ConnectDisconnectParty":
                        {
                            #region Party details for COnnect or Disconnect
                            // Add a participant to existing conf on mcu. 
                            // The conf  ongoing.
                            oPartyArray = new XMLRPCArray();
                            oStructParty = new XMLRPCStruct();
                            Gindex = 0; Dindex = 0;
                            partyAddress = party.sAddress;
							//Prefix Changes Start
                            if (conf != null)
                            {
                                if (conf.bE164)
                                    partyAddress = "sip:" + party.sAddress;
                                else if (conf.bH323)
                                    partyAddress = "h323:" + party.sAddress;
                            }
   							//Prefix Changes End                         
                            logger.Trace("party.etAddressType :" + party.etAddressType);
                            
                            oStruct.AddString("conferenceGUID", conf.sGUID);

                            if (partyAddress.Trim().Contains("G"))
                            {
                                Gindex = partyAddress.IndexOf("G");
                                partyAddress = partyAddress.Substring(0, Gindex);
                            }
                            if (partyAddress.Trim().Contains("D"))
                            {
                                Dindex = partyAddress.IndexOf("D");
                                partyAddress = partyAddress.Substring(0, Dindex);

                                //Audio Dial-in prefix is added before the address
                                //FB 2655 DTMF Start
                                //partyAddress = configParams.audioDialNoPrefix + partyAddress; //Disney New Audio Add on Request
                                partyAddress = party.AudioDialInPrefix + partyAddress; 
                                //FB 2655 DTMF End
                            }
                            else
                            {
                                if (party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                                {
                                    phonePrefix = "";
                                    if (party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                    {
                                        phonePrefix = party.cMcu.iISDNAudioPrefix;
                                    }
                                    else
                                    {
                                        phonePrefix = party.cMcu.iISDNVideoPrefix;
                                    }
                                    if (phonePrefix != "")
                                        partyAddress = phonePrefix + partyAddress;
                                }
                            }

                            oStructParty.AddString("address", partyAddress.Trim());

                            if (party.etVideoEquipment == NS_MESSENGER.Party.eVideoEquipment.RECORDER)
                            {
                                oStructParty.AddBoolean("recordingDevice", true);
                            }
                            else if (party.etAddressType == NS_MESSENGER.Party.eAddressType.ISDN_PHONE_NUMBER && party.cMcu.sisdnGateway.Trim() != "")
                            {
                                oStructParty.AddBoolean("recordingDevice", false);
                            }

                            // check for gateway  
                            if (party.sAddress.Trim().Contains("G"))
                            {
                                Gindex = 0; Dindex = 0;
                                // gateway exists
                                if (party.sAddress.Trim().Contains("D"))
                                {
                                    // dtmf sequence also exists. Hence, gateway value needs to be extracted between G & D.

                                    Gindex = party.sAddress.IndexOf("G");
                                    Dindex = party.sAddress.IndexOf("D");
                                    party.sGatewayAddress = party.sAddress.Substring(Gindex + 1, Dindex - Gindex - 1);
                                    oStruct.AddString("gatewayAddress", party.sGatewayAddress.Trim());
                                }
                                else
                                {
                                    Gindex = party.sAddress.IndexOf("G");
                                    party.sGatewayAddress = party.sAddress.Substring(Gindex + 1);
                                    oStruct.AddString("gatewayAddress", party.sGatewayAddress.Trim());
                                }
                            }
                            // check for dtmf sequence
                            if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty) //FB 2655
                            {
                                // dtmf sequence exists                            
                                Dindex = party.sAddress.IndexOf("D");
                                party.sDTMFSequence = party.sAddress.Substring(Dindex + 1);
                                party.sDTMFSequence = party.preConfCode + party.sDTMFSequence; //FB 2655
                                party.sDTMFSequence = party.sDTMFSequence.Replace("+", party.preLPin); //FB 2655
                                party.sDTMFSequence = party.sDTMFSequence.Trim() + party.postLPin; //FB 2655

                                oStructParty.AddString("dtmf", party.sDTMFSequence.Trim());
                            }

                            codianLineRate = 384;
                            ret2 = EquateLineRate(party.stLineRate.etLineRate, ref codianLineRate);
                            if (!ret2)
                            {
                                logger.Trace("Invalid LineRate.");
                                return false;
                            }

                            oStructParty.AddInteger("maxBitRate", codianLineRate);

                            oPartyArray.AddStruct(oStructParty);

                            oStruct.AddArray("participants", oPartyArray);

                            break;
                            #endregion
                        }
                    case "ConferenceEndpointMessage":
                        {
                            #region SendConferenceEndpointMessage

                            oStruct.AddString("conferenceGUID", conf.sGUID);
                            oStruct.AddString("message", party.sMessage);
                            if (party.iMessPosition > 0)
                                oStruct.AddInteger("position", party.iMessPosition);//1,2or3-top; 4,5or6-Middle; 7,8or9-bottom
                            if (party.iMessDuration > 0)
                                oStruct.AddInteger("duration", party.iMessDuration);
                            oStruct.AddString("participantGUID", party.sGUID);
                            break;

                            #endregion
                        }
                    case "ConferenceMessage":
                        {
                            #region SendConferenceMessage

                            oStruct.AddString("conferenceGUID", conf.sGUID);
                            oStruct.AddString("message", conf.sMessage);
                            if (conf.iMessPosition > 0)
                                oStruct.AddInteger("position", conf.iMessPosition);//1,2or3-top; 4,5or6-Middle; 7,8or9-bottom
                            if (conf.iMessDuration > 0)
                                oStruct.AddInteger("duration", conf.iMessDuration);
                            break;

                            #endregion
                        }
                    case "ConnectedParticipantList":
                        {
                            #region Endpoint Status
                            if (this.participantEnumerateID > 0)
                            {
                                oStruct.AddString("enumerateID", this.participantEnumerateID.ToString());
                            }
                            break;
                            #endregion
                        }
                    case "FetchMCUDetails":
                        {
                            #region FetchMCUDetails
                            //Internally left blank
                            break;
                            #endregion
                        }
                    case "MuteAudioTransmitParty":
                        {
                            #region MuteAudioTransmitParty

                            oStruct.AddString("participantGUID", party.sGUID);
                            oStruct.AddBoolean("txAudioMute", party.bMute);
                            break;

                            #endregion
                        }
                    case "ConferenceCreate":
                        {
                            #region ConferenceCreate
                            oStruct.AddString("conferenceName", conf.sDbName);

                            bool isPersistent = false;

                            logger.Trace("Persistant is set to true: Confernece is not disconnected");
                            oStruct.AddBoolean("persistent", isPersistent);

                            //FB 2636 Start
                            oStruct.AddBoolean("registerWithSIPRegistrar", conf.bE164);
                            oStruct.AddBoolean("registerWithGatekeeper", conf.bH323);
                            //if (conf.bE164 || conf.bH323) Commented for FB 2870
                            if(conf.sDialinNumber != "") //FB 2870
                            {
                                oStruct.AddString("numericId", conf.sDialinNumber.ToString());//FB 2659
                                logger.Trace("Conf DialinNumber ID: " + conf.sDialinNumber.ToString());
                            }
                            else
                            {
                                oStruct.AddString("numericId", conf.iDbNumName.ToString());
                                logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());
                            }
                            //FB 2636 End

                            // for register With Gatekeeper
                            //logger.Trace("registerWithGatekeeper: true ");
                            //oStruct.AddDateTime("registerWithGatekeeper","true");

                            // for sip register
                            //logger.Trace("registerWithSIPRegistrar: : true ");
                            //oStruct.AddDateTime("registerWithSIPRegistrar","true");

                            // Add the conference duration in "seconds"
                            oStruct.AddInteger("duration", (conf.iDuration * 60));

                            // Dual Stream Mode 
                            if (conf.bH239)
                            {
                                oStruct.AddBoolean("h239ContributionEnabled", true);
                            }

                            /* Max audio & video ports
                            if (conf.iMaxAudioPorts > 0)
                            {
                                oStruct.AddInteger("audioPortLimit", conf.iMaxAudioPorts);

                            }
                            if (conf.iMaxVideoPorts > 0)
                            {
                                oStruct.AddInteger("videoPortLimit", conf.iMaxVideoPorts);
                            }*/

                            // Reserved audio & video ports
                            audioCount = 0; videoCount = 0;
                            logger.Trace("Party count: " + conf.qParties.Count.ToString());
                            System.Collections.IEnumerator partyEnumerator;
                            partyEnumerator = conf.qParties.GetEnumerator();
                            partyCount = conf.qParties.Count;
                            for (int i = 0; i < partyCount; i++)
                            {
                                // go to next party in the queue
                                partyEnumerator.MoveNext();

                                tempParty = new NS_MESSENGER.Party();
                                tempParty = (NS_MESSENGER.Party)partyEnumerator.Current;

                                logger.Trace("Party call type: " + tempParty.etCallType.ToString());

                                if (tempParty.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                {
                                    audioCount++;
                                }
                                else
                                {
                                    if (tempParty.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    {
                                        videoCount++;
                                    }
                                }
                            }

                            logger.Trace("Reserved Audio & Video ports: Audio =" + audioCount.ToString() + " , Video= " + videoCount.ToString());
                            oStruct.AddInteger("audioPortLimit", audioCount);
                            oStruct.AddInteger("videoPortLimit", videoCount);

                            break;
                            #endregion
                        }
                    case "AddParty":
                        {
                            #region Add Party
                            // Add a participant to existing conf on mcu. 
                            // The conf  ongoing.

                            oStruct.AddString("conferenceGUID", conf.sGUID);
                            oPartyArray = new XMLRPCArray();
                            while (conf.qParties.Count > 0)
                            {
                                //FB 2501 Call Monitoring Start Tamil
                                string MultiPatyAddress = "";
                                party = (NS_MESSENGER.Party)conf.qParties.Dequeue();
                                if (!party.isTelePresence)
                                {
                                    oStructParty = new XMLRPCStruct();
                                    logger.Trace("Address " + party.sAddress.Trim());
                                    // party is not a cascade link
                                    Gindex = 0;
                                    Dindex = 0;
                                    partyAddress = party.sAddress;

                                    if (conf.bE164) //FB 2672
                                        partyAddress = "sip:" + party.sAddress;
                                    else if (conf.bH323)
                                        partyAddress = "h323:" + party.sAddress;
                                    else
                                        partyAddress = party.sAddress;

                                    logger.Trace("party.etAddressType :" + party.etAddressType);

                                    if (partyAddress.Trim().Contains("G"))
                                    {
                                        Gindex = partyAddress.IndexOf("G");
                                        partyAddress = partyAddress.Substring(0, Gindex);
                                    }
                                    if (partyAddress.Trim().Contains("D"))
                                    {
                                        Dindex = partyAddress.IndexOf("D");
                                        partyAddress = partyAddress.Substring(0, Dindex);

                                        //Audio Dial-in prefix is added before the address
                                        //FB 2655 DTMF Start
                                        //partyAddress = configParams.audioDialNoPrefix + partyAddress; //Disney New Audio Add on Request
                                        partyAddress = party.AudioDialInPrefix + partyAddress;
                                        //FB 2655 DTMF End
                                    }
                                    else  //FB 2003 ISDN Prefix start
                                    {
                                        if (party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                                        {
                                            phonePrefix = "";
                                            if (party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                            {
                                                phonePrefix = party.cMcu.iISDNAudioPrefix;
                                            }
                                            else
                                            {
                                                phonePrefix = party.cMcu.iISDNVideoPrefix;
                                            }
                                            if (phonePrefix != "")
                                                partyAddress = phonePrefix + partyAddress;
                                        }
                                    }
                                    //FB 2003 ISDN Prefix end
                                }
                                else
                                {
                                    oStructParty = new XMLRPCStruct();
                                    MultiPatyAddress = "";
                                    logger.Trace("Address " + party.MulticodecAddress.Trim());
                                    String[] MultiAdds = party.MulticodecAddress.Split('Ö');
                                    for (int a = 0; a < MultiAdds.Length; a++)
                                    {
                                        partyAddress = MultiAdds[a].Trim();
                                        if (party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                                        {
                                            phonePrefix = "";
                                            if (party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                            {
                                                phonePrefix = party.cMcu.iISDNAudioPrefix;
                                            }
                                            else
                                            {
                                                phonePrefix = party.cMcu.iISDNVideoPrefix;
                                            }
                                            if (phonePrefix != "")
                                                partyAddress = phonePrefix + partyAddress;
                                        }


										//Prefix Changes Start
                                        if (conf.bE164)
                                            partyAddress = "sip:" + MultiAdds[a].Trim();
                                        else if (conf.bH323)
                                            partyAddress = "h323:" + MultiAdds[a].Trim();
										//Prefix Changes End
                                        logger.Trace("party.etAddressType :" + party.etAddressType);

                                        if (a == 0)
                                            MultiPatyAddress = partyAddress;
                                        if(a>0)
                                            MultiPatyAddress += "," + partyAddress;
                                    }
                                    partyAddress = MultiPatyAddress;
                                }
                                //FB 2501 Call Monitoring end Tamil
                                oStructParty.AddString("address", partyAddress.Trim());

                                if (party.etVideoEquipment == NS_MESSENGER.Party.eVideoEquipment.RECORDER)
                                {
                                    oStructParty.AddBoolean("recordingDevice", true);
                                }
                                else if (party.etAddressType == NS_MESSENGER.Party.eAddressType.ISDN_PHONE_NUMBER && party.cMcu.sisdnGateway.Trim() != "")
                                {
                                    oStructParty.AddBoolean("recordingDevice", false);
                                }

                                // check for gateway  
                                if (party.sAddress.Trim().Contains("G"))
                                {
                                    Gindex = 0; Dindex = 0;
                                    // gateway exists
                                    if (party.sAddress.Trim().Contains("D"))
                                    {
                                        // dtmf sequence also exists. Hence, gateway value needs to be extracted between G & D.

                                        Gindex = party.sAddress.IndexOf("G");
                                        Dindex = party.sAddress.IndexOf("D");
                                        party.sGatewayAddress = party.sAddress.Substring(Gindex + 1, Dindex - Gindex - 1);
                                        oStruct.AddString("gatewayAddress", party.sGatewayAddress.Trim());
                                    }
                                    else
                                    {
                                        Gindex = party.sAddress.IndexOf("G");
                                        party.sGatewayAddress = party.sAddress.Substring(Gindex + 1);
                                        oStruct.AddString("gatewayAddress", party.sGatewayAddress.Trim());
                                    }
                                }
                                // check for dtmf sequence
                                if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty) //FB 2655
                                {
                                    // dtmf sequence exists                            
                                    Dindex = party.sAddress.IndexOf("D");
                                    party.sDTMFSequence = party.sAddress.Substring(Dindex + 1);
                                    party.sDTMFSequence = party.preConfCode + party.sDTMFSequence; //FB 2655
                                    party.sDTMFSequence = party.sDTMFSequence.Replace("+", party.preLPin); //FB 2655
                                    party.sDTMFSequence = party.sDTMFSequence.Trim() + party.postLPin; //FB 2655

                                    oStructParty.AddString("dtmf", party.sDTMFSequence.Trim());
                                }

                                codianLineRate = 384;
                                ret2 = EquateLineRate(party.stLineRate.etLineRate, ref codianLineRate);
                                if (!ret2)
                                {
                                    logger.Trace("Invalid LineRate.");
                                    return false;
                                }

                                oStructParty.AddInteger("maxBitRate", codianLineRate);

                                oPartyArray.AddStruct(oStructParty);
                            }

                            oStruct.AddArray("participants", oPartyArray);
                            // Equate db line rate value to value expected by mcu.

                            break;
                            #endregion
                        }
                    case "MutePartyVideoTransmit":
                        {
                            #region MutePartyTransmit
                            oStruct.AddString("participantGUID", party.sGUID);
                            oStruct.AddBoolean("txVideoMute", party.bMute);
                            break;

                            #endregion
                        }
                    case "MutePartyVideoReceiver":
                        {
                            #region MutePartyTransmit
                            oStruct.AddString("participantGUID", party.sGUID);
                            oStruct.AddBoolean("rxVideoMute", party.bMute);
                            break;

                            #endregion
                        }
                    case "LockOrUnLockConference":
                        {
                            #region LockOrUnLockConference

                            oStruct.AddString("conferenceGUID", conf.sGUID);

                            if (conf.ilockorunlock == 1)
                            {
                                oStruct.AddBoolean("locked", true);
                                oStruct.AddInteger("lockDuration", conf.iDuration);
                            }
                            else
                                oStruct.AddBoolean("locked", false);
                            break;

                            #endregion
                        }

                    case "ConferenceModify":
                        {
                            #region ConferenceModify

                            oStruct.AddString("conferenceGUID", conf.sGUID);

                            // Add the conference duration in "seconds"

                            oStruct.AddInteger("duration", (conf.iDuration * 60));

                            // Reserved audio & video ports
                            audioCount = 0; videoCount = 0;
                            System.Collections.IEnumerator partyEnumerator;
                            partyEnumerator = conf.qParties.GetEnumerator();
                            partyCount = conf.qParties.Count;
                            for (int i = 0; i < partyCount; i++)
                            {
                                // go to next party in the queue
                                partyEnumerator.MoveNext();

                                // party
                                NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                                party1 = (NS_MESSENGER.Party)partyEnumerator.Current;

                                logger.Trace("Party call type: " + party1.etCallType.ToString());

                                if (party1.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                {
                                    audioCount++;
                                }
                                else
                                {
                                    if (party1.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    {
                                        videoCount++;
                                    }
                                }
                            }

                            oStruct.AddInteger("reservedAudioPorts", audioCount);
                            oStruct.AddInteger("reservedVideoPorts", videoCount);

                            break;
                            #endregion
                        }
                    case "FetchAllConfs":
                        {
                            #region FetchAllConfs
                            if (this.confEnumerateID > 0)
                                oStruct.AddString("enumerateID", this.confEnumerateID.ToString());
                            break;
                            #endregion

                        }
                    case "FetchAllParticipants":
                        {
                            #region FetchAllParticipants
                            if (this.participantEnumerateID > 0)
                            {
                                try
                                {
                                    oStruct.AddString("enumerateID", this.participantEnumerateID.ToString());
                                    vbxmlrpc_Net.XMLRPCArrayClass codianArray = new vbxmlrpc_Net.XMLRPCArrayClass();
                                    codianArray.AddString("currentState");
                                    oStruct.AddArray("operationScope", codianArray);
                                }
                                catch (Exception e)
                                {
                                    logger.Exception(100, e.Message);
                                    return false;
                                }
                            }
                            break;
                            #endregion
                        }
                    case "DisconnectedParticipantList":
                        {
                            #region DisconnectedParticipantList
                            oStruct.AddString("enumerateFilter", "disconnected");
                            if (this.participantEnumerateID > 0)
                            {
                                oStruct.AddString("enumerateID", this.participantEnumerateID.ToString()); // enumeration number to fetch the next page
                            }
                            break;
                            #endregion
                        }
                    case "ConferenceStatus":
                        {
                            #region ConferenceStatus
                            oStruct.AddString("conferenceGUID", conf.sGUID);
                            break;

                            #endregion
                        }
                    case "ParticipantDiagnostics":
                        {
                            #region ParticipantDiagnostics
                            logger.Trace("Trace1" + party.sGUID + conf.sDbName + party.sName);
                            oStruct.AddString("participantGUID", party.sGUID);
                            oStruct.AddString("receiverURI", "http://" + conf.ServerAddress + ":" + conf.PortNumber + "/RPClistener.asmx");
                            break;
                            #endregion
                        }
                    case "CallDetailRecords":
                        {
                            #region CallDetailRecords
                            oStruct.AddInteger("index", indexCDR);
                            #endregion
                        }
                        break;
                    default:
                        {
                            // no match 
                            return false;
                        }
                }

            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                logger.Trace("Error in Create Struct:" + ex.Message);
            }

            return true;
        }
        #endregion

        #region Sending Commands and receive process
        /// <summary>
        /// SendCommand
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="strMethodName"></param>
        /// <param name="oStruct"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private bool SendCommand(NS_MESSENGER.MCU mcu, string strMethodName, vbxmlrpc_Net.XMLRPCStruct oStruct, ref string response)
        {
            try
            {	// Initialize the RPC structures. These would be used to communicate with mcu.
                vbxmlrpc_Net.XMLRPCRequest oRpc = new vbxmlrpc_Net.XMLRPCRequestClass();
                vbxmlrpc_Net.XMLRPCResponse oResponse = new vbxmlrpc_Net.XMLRPCResponseClass();

                // Assign the mcu details
                oRpc.HostName = mcu.sIp;
                oRpc.Username = mcu.sLogin;
                oRpc.Password = mcu.sPwd;
                oRpc.HostURI = "/RPC2";
                oRpc.HostPort = mcu.iHttpPort;//80;Code added for MCU port	
                oRpc.Params.AddStruct(oStruct);

                // Assign the method call 
                oRpc.MethodName = strMethodName;
                logger.Trace(oRpc.XMLToSend.ToString());

                // Send command to mcu.
                oResponse = oRpc.Submit();
                response = oResponse.XMLResponse.ToString();
                logger.Trace(oResponse.XMLResponse.ToString());

                // Check for mcu-returned faults
                if (oResponse.XMLResponse.IndexOf("<fault>", 0) > 0)
                {
                    logger.Trace("SendXml = " + oRpc.XMLToSend.ToString());
                    logger.Trace("ResponseXml = " + oResponse.XMLResponse.ToString());
                    ProcessCodianFaultResponse(mcu.iDbId, strMethodName, oResponse.XMLResponse);
                    return false;
                }
                else
                {
                    // No faults. MCU returned some response.
                    this.errMsg = oResponse.XMLResponse;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private void ProcessCodianFaultResponse(int intMcuID, string strFunctionCall, string strFaultXML)
        {
            try
            {
                string strFaultCode = null, strFaultMsg = null;

                // Load the incoming xml in parser.
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(strFaultXML);

                // Node list.
                XmlNodeList oNodeList = xDoc.SelectNodes("//member");

                // Start retreiving the fault codes.
                foreach (XmlNode oNode in oNodeList)
                {
                    if (oNode.SelectSingleNode("name").InnerText == "faultCode")
                    {
                        strFaultCode = oNode.SelectSingleNode("value/int").InnerText.ToString();
                    }
                    else
                    {
                        if (oNode.SelectSingleNode("name").InnerText == "faultString")
                        {
                            strFaultMsg = oNode.SelectSingleNode("value/string").InnerText.ToString();
                        }
                    }
                }

                // Fault message
                this.errMsg = "Operation failed. Message: " + strFaultMsg + "(Cisco API error code - " + strFaultCode + ").";
                logger.Trace(this.errMsg); ;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
        }

        #endregion

        #region TestMCUConnection
        /// <summary>
        /// TestMCUConnection
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool TestMCUConnection(NS_MESSENGER.MCU mcu)
        {
            try
            {
                logger.Trace("Entering Test MCU Connection method...");

                // Create a empty RPC struct
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                NS_MESSENGER.Conference dummyConf = new NS_MESSENGER.Conference();
                dummyParty.cMcu = mcu;
                bool ret = CreateStruct("TestMCUConnection", dummyParty, dummyConf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending system query cmd to Cisco 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(mcu, "system.info", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Connection to MCU failed. Please recheck the MCU information.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        #region TerminateConference
        /// <summary>
        /// TerminateConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering Terminate Conference method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                dummyParty.cMcu = conf.cMcu;
                bool ret = CreateStruct("TerminateConference", dummyParty, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending terminate conference cmd to Cisco MSE 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "conference.delete", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

        #endregion

        #region ExtendConfEndTime
        /// <summary>
        /// ExtendConfEndTime
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="extendTime"></param>
        /// <returns></returns>
        internal bool ExtendConfEndTime(NS_MESSENGER.Conference conf, int extendTime)
        {
            logger.Trace("Entering Extend Conference Time method...");
            vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
            NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
            dummyParty.cMcu = conf.cMcu;

            DateTime currenttime = DateTime.Now.ToUniversalTime();
            int timediff = (int)currenttime.Subtract(conf.dtStartDateTimeInUTC).TotalMinutes;

            conf.iDuration = conf.iDuration - timediff;
            conf.iDuration = conf.iDuration + extendTime;

            bool ret = CreateStruct("ExtendConferenceEndTime", dummyParty, conf, ref oStruct);
            if (!ret)
            {
                logger.Trace("Struct problem.");
                return false;
            }

            // Send the command to the mcu
            logger.Trace("Sending modify conference cmd to Cisco mcu...");
            ret = false; string strResponse = null;
            ret = SendCommand(conf.cMcu, "conference.set", oStruct, ref strResponse);
            if (!ret)
            {
                logger.Trace("SendCommand failed");
                return false;
            }

            return true;
        }

        #endregion
        //Doubt in sending party
        #region TerminateEndpoint
        /// <summary>
        /// TerminateEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool TerminateEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering TerminateEndpoint method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                bool ret = CreateStruct("TerminateParty", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending terminate party cmd to Cisco MSE 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "conference.uninvite", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion

        #region MuteAudioReceiveEndpoint
        /// <summary>
        /// MuteAudioReceiveEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteAudioReceiveEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering MuteEndpoint method...Mute = " + mute.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.bMute = mute;
                bool ret = CreateStruct("MuteAudioReceiveParty", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending mute cmd to Cisco MSE 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.set", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion
        //Doubt whether this command is available
        #region ModifyEndpoint
        /// <summary>
        /// ModifyEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool ModifyEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering ModifyEndpoint method..");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                bool ret = CreateStruct("ModifyEndpoint", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Error in creating modify struct...");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending ModifyEndpoint cmd to Cisco MSE 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.set", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed for modify endpoint operation");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion

        #region ConnectDisconnectEndpoint
        /// <summary>
        /// ConnectDisconnectEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="connectOrDisconnect"></param>
        /// <returns></returns>
        internal bool ConnectDisconnectEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool connectOrDisconnect)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering ConnectDisconnect Endpoint method...ConnectOrDisconnect = " + connectOrDisconnect.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                //party.bMute = mute;

                bool ret = false;
                
                // Send the command to the mcu
                logger.Trace("Sending connectOrDisconnect cmd to Cisco MSE 8710 mcu...");
                ret = false; string strResponse = null;
                if (connectOrDisconnect)
                {
                    ret = CreateStruct("ConnectDisconnectParty", party, conf, ref oStruct);
                    if (!ret)
                    {
                        logger.Trace("Struct problem.");
                        return false;
                    }

                    ret = SendCommand(party.cMcu, "conference.invite", oStruct, ref strResponse);
                    //FB 2501 Dec6 Start
                    if (!ret)
                    {
                        logger.Trace("add participants to conference fails.");
                        return false;
                    }
                    if (strResponse.Trim() != "")
                    {
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.LoadXml(strResponse);
                        XmlNodeList oInnerNodeList = null;
                        XmlNodeList oNodeList = xDoc.SelectNodes("//member");

                        foreach (XmlNode oNode in oNodeList)
                        {
                            oInnerNodeList = oNode.SelectNodes("value/array/data/value");
                            foreach (XmlNode oInnerNode in oInnerNodeList)
                            {
                                string sAddress = "", sPartyGUID = "";
                                if (oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string") != null)
                                    sAddress = oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string").InnerText.ToString().Trim();
                                logger.Trace("Party Address: " + sAddress);

                                if (sAddress != "")
                                {
                                    party.sAddress = sAddress;


                                    if (sAddress.Split(':').Length > 1) //FB 2989
                                        party.sAddress = sAddress.Split(':')[1];
                                }

                                if (oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string") != null)
                                    sPartyGUID = oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string").InnerText.ToString().Trim();
                                logger.Trace("Party GUID: " + sPartyGUID);

                                if (sPartyGUID != "")
                                    party.sGUID = sPartyGUID;

                                if (party.sAddress != "" && party.sGUID != "")
                                    db.UpdateConferenceEndpointStatus(conf.iDbID, conf.iInstanceID, party.sAddress, party.sGUID);
                            }
                        }
                    }
                    //FB 2501 Dec6 End
                }
                else
                {
                    ret = CreateStruct("TerminateParty", party, conf, ref oStruct);
                    if (!ret)
                    {
                        logger.Trace("Struct problem.");
                        return false;
                    }
                    ret = SendCommand(party.cMcu, "conference.uninvite", oStruct, ref strResponse);
                }
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                else //FB 2989
                    ret = ConferenceStatus(conf, party);
                
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion

        #region SendConferenceEndpointmessage
        /// <summary>
        /// SendConferenceEndpointmessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="Message"></param>
        /// <param name="direction"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        internal bool SendConferenceEndpointmessage(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string Message, int direction, int duration)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering Send Message to Conference Method ...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                //party.cMcu = conf.cMcu; // Commented for FB 2501 Dec5
                party.iMessDuration = duration;
                party.sMessage = Message;
                party.iMessPosition = direction;

                bool ret = CreateStruct("ConferenceEndpointMessage", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending conference messsage cmd to Cisco mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "conference.sendmessage", oStruct, ref strResponse); //FB 2501 Dec5
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        #region SendConferencemessage
        /// <summary>
        /// SendConferencemessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="Message"></param>
        /// <param name="direction"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        internal bool SendConferencemessage(NS_MESSENGER.Conference conf, string Message, int direction, int duration)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering SendConferenceMessage method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                dummyParty.cMcu = conf.cMcu;
                conf.iMessPosition = direction;
                conf.sMessage = Message;
                conf.iMessDuration = duration;

                bool ret = CreateStruct("ConferenceMessage", dummyParty, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending conference messsage cmd to Cisco mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "conference.sendmessage", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion
        //Need to check
        #region GetEndpointStatus
        /// <summary>
        /// GetEndpointStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, NS_MESSENGER.Smtp smtpServerInfo, ref NS_MESSENGER.Party party)
        {
            try
            {
                ConferenceStatus(conf, party);
                //ParticipantDiagnostics(conf, smtpServerInfo, ref party); Commented till we complete RPC listener
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool ProcessXML_FindConnectedParticipant(string strXML, ref NS_MESSENGER.Party party, ref bool isConnected, ref bool foundParty)
        {
            try
            {
                // load the xml doc
                XmlDocument oDOM = new XmlDocument();
                oDOM.LoadXml(strXML);

                //member node list 
                XmlNodeList oNodeList = oDOM.SelectNodes("//member");
                bool isEnumerateIDPresent = false;

                //going thru the "member" array list 
                foreach (XmlNode oNode in oNodeList)
                {
                    // check if there are more participants . to do that need to check if enumerateID > 0.					
                    if (oNode.SelectSingleNode("name").InnerText == "enumerateID")
                    {
                        isEnumerateIDPresent = true;
                        try
                        {
                            this.participantEnumerateID = Int32.Parse(oNode.SelectSingleNode("value/string").InnerText.Trim());
                        }
                        catch (Exception)
                        {
                            logger.Trace("Invalid enumerateID.");
                            this.participantEnumerateID = 0;
                        }
                    }

                    if (isEnumerateIDPresent == false)
                    {
                        this.participantEnumerateID = 0;
                    }

                    //get to the "participants" member and start to retrive the conf list
                    if (oNode.SelectSingleNode("name").InnerText == "participants")
                    {
                        //get to each individual participant
                        XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");
                        foreach (XmlNode oInnerNode in oInnerNodeList)
                        {
                            try
                            {
                                /*participantGUID - string - The GUID of this participant, assigned by the TelePresence Server.
                                participantID - integer - Deprecated. Use participantGUID instead. 
                                 * The unique ID of this participant, assigned by the TelePresence Server.
                                conferenceGUID - string - Globally unique identifier of the conference.
                                conferenceID - integer - Deprecated. Use conferenceGUID instead.
                                 * Unique conference identifier
                                address - string - The address of the item, e.g. endpoint or gateway; may be hostname, IP address or E.164 number.
                                endpointCategory 
                                callProtocol - string - sip or h323.
                                callState - integer - State of the call between the TelePresence Server and this participant.*/

                                foundParty = true;
                                logger.Trace("Endpoint party found !");

                                //participantGUID
                                if (oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string") != null)
                                    party.sGUID = oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string").InnerText.Trim();
                                logger.Trace("Party GUID: " + party.sGUID);

                                //conferenceGUID

                                //address
                                if (oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string") != null)
                                    party.sAddress = oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string").InnerText.Trim();
                                logger.Trace("Party Address: " + party.sAddress);

                                //endpointCategory

                                //callProtocol
                                String protocol = "IP";
                                if (oInnerNode.SelectSingleNode("struct/member[name = \"callProtocol\"]/value/string") != null)
                                    protocol = oInnerNode.SelectSingleNode("struct/member[name = \"callProtocol\"]/value/string").InnerText.Trim();

                                if (protocol == "sip")
                                    party.etProtocol = NS_MESSENGER.Party.eProtocol.SIP;
                                else
                                    party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                                logger.Trace("Party Protocol: " + party.etProtocol);

                                // call state
                                int callState = 0;
                                if (oInnerNode.SelectSingleNode("struct/member[name = \"callState\"]/value/int") != null)
                                    Int32.TryParse(oInnerNode.SelectSingleNode("struct/member[name = \"callState\"]/value/int").InnerText.Trim(), out callState);
                                switch (callState)
                                {
                                    case 0:
                                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                                        break;
                                    case 1:
                                    case 3:
                                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE;
                                        break;
                                    case 4:
                                    case 2:
                                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                        isConnected = true;
                                        break;
                                }
                                logger.Trace("Party callstate:" + callState);
                                return true;
                            }
                            catch (Exception e)
                            {
                                logger.Trace("Invalid party call parameters. Error:" + e.Message);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }

        #endregion

        //FB 2501 Dec6 Start

        #region FetchMCUDetails
        /// <summary>
        /// FetchMCUDetails
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu)
        {
            try
            {
                logger.Trace("Entering FetchMCUDetails function...");

                // Create a empty RPC struct
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                NS_MESSENGER.Conference dummyConf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                int portsVideoTotal = 0, portsVideoFree = 0, portsAudioTotal = 0, portsAudioFree = 0, status = 1;
                dummyParty.cMcu = mcu;
                bool ret = CreateStruct("FetchMCUDetails", dummyParty, dummyConf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending system query cmd to Cisco MSE 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(mcu, "system.info", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Connection to MCU failed. Please recheck the MCU information.");
                    portsVideoTotal = 0; portsVideoFree = 0; portsAudioTotal = 0; portsAudioFree = 0;
                    status = 3;//MCU Status
                }
                else if (strResponse.Trim() != "")
                {
                    XmlDocument oDOM = new XmlDocument();
                    XmlNodeList oNodeList = null;
                    oDOM.LoadXml(strResponse);
                    oNodeList = oDOM.SelectNodes("//member");

                    foreach (XmlNode oNode in oNodeList)
                    {
                        status = 1;
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "portsVideoTotal")
                        {
                            Int32.TryParse(oNode.SelectSingleNode("value/int").InnerText.Trim(), out portsVideoTotal);
                        }
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "portsVideoFree")
                        {
                            Int32.TryParse(oNode.SelectSingleNode("value/int").InnerText.Trim(), out portsVideoFree);
                        }
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "portsAudioTotal")
                        {
                            Int32.TryParse(oNode.SelectSingleNode("value/int").InnerText.Trim(), out portsAudioTotal);
                        }
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "portsAudioFree")
                        {
                            Int32.TryParse(oNode.SelectSingleNode("value/int").InnerText.Trim(), out portsAudioFree);
                        }
                    }
                }
                db.UpdateMcuInfo(mcu.iDbId, portsVideoTotal, portsVideoFree, portsAudioTotal, portsAudioFree, status);
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //FB 2501 Dec6 End

        #region MuteAudioTransmitEndpoint
        /// <summary>
        /// MuteAudioTransmitEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteAudioTransmitEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering MuteEndpoint method...Mute = " + mute.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.bMute = mute;
                bool ret = CreateStruct("MuteAudioTransmitParty", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending mute cmd to Cisco MSE 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.set", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion

        #region SetConference
        /// <summary>
        /// SetConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool SetConference(ref NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            vbxmlrpc_Net.XMLRPCStruct oStruct = null;
            XmlDocument xDoc = null;
            XmlNodeList oInnerNodeList = null;
            String sAddress = "";
            String sPartyGUID = "";
            try
            {
                // Initializing the xml-rpc struct object				
                oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());

                bool ret = CreateStruct("ConferenceCreate", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace(" Create Struct failed...");
                    return false;
                }

                // Sending the conference data over to the MCU
                ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "conference.create", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Conference setup failed on Cisco MCU 8710.");
                    return false;
                }

                xDoc = new XmlDocument();
                xDoc.LoadXml(strResponse);

                XmlNodeList oNodeList = xDoc.SelectNodes("//member");

                foreach (XmlNode oNode in oNodeList)
                {
                    if (oNode.SelectSingleNode("name").InnerText.Trim() == "conferenceGUID")
                    {
                        conf.sGUID = oNode.SelectSingleNode("value/string").InnerText.Trim();
                        logger.Trace("ConfId: " + conf.iDbID.ToString() + "GUID: " + conf.sGUID);
                        db.UpdateConference(conf.iDbID, conf.iInstanceID, conf.sGUID);
                    }
                }

                ret = false;
                logger.Trace("Total parties: " + conf.qParties.Count.ToString());
                oStruct = null;
                if (conf.qParties.Count > 0)
                {
                    oStruct = new XMLRPCStruct();
                    CreateStruct("AddParty", null, conf, ref oStruct);
                }

                if (oStruct != null)
                {
                    strResponse = "";
                    ret = SendCommand(conf.cMcu, "conference.invite", oStruct, ref strResponse);
                    if (!ret)
                    {
                        logger.Trace("add participants to conference fails.");
                        return false;
                    }
                    if (strResponse.Trim() != "")
                    {

                        xDoc = new XmlDocument();
                        xDoc.LoadXml(strResponse);

                        oNodeList = xDoc.SelectNodes("//member");

                        foreach (XmlNode oNode in oNodeList)
                        {
                            oInnerNodeList = oNode.SelectNodes("value/array/data/value");
                            foreach (XmlNode oInnerNode in oInnerNodeList)
                            {
                                sAddress = ""; sPartyGUID = "";
                                if (oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string") != null)
                                    sAddress = oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string").InnerText.ToString().Trim();
                                logger.Trace("Party Address: " + sAddress);
                                if (sAddress != "")
                                {
                                    party.sAddress = sAddress;


                                    if(sAddress.Split(':').Length > 1) //FB 2672
                                        party.sAddress = sAddress.Split(':')[1];
                                }

                                if (oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string") != null)
                                    sPartyGUID = oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string").InnerText.ToString().Trim();
                                logger.Trace("Party GUID: " + sPartyGUID);
                                if (sPartyGUID != "")
                                    party.sGUID = sPartyGUID;

                                if (party.sAddress != "" && party.sGUID != "")
                                    db.UpdateConferenceEndpointStatus(conf.iDbID, conf.iInstanceID, party.sAddress, party.sGUID);
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        internal vbxmlrpc_Net.XMLRPCStruct AddPartyToMcu(NS_MESSENGER.Conference conf)
        {
            vbxmlrpc_Net.XMLRPCStruct oStructAdd = null;
            try
            {
                CreateStruct("AddParty", null, conf, ref oStructAdd);
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return null;
            }
            return oStructAdd;
        }

        #endregion

        #region AddParticipantToMcu
        /// <summary>
        /// AddParticipantToMcu
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool AddParticipantToMcu(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            vbxmlrpc_Net.XMLRPCStruct oStruct = null;
            string strResponse = null;
            try
            {
                oStruct = new XMLRPCStruct();

                if (conf != null)
                {
                    conf.qParties = new Queue();
                    conf.qParties.Enqueue(party);

                    CreateStruct("AddParty", null, conf, ref oStruct);

                    bool ret = SendCommand(conf.cMcu, "conference.invite", oStruct, ref strResponse);
                    if (!ret)
                    {
                        logger.Trace("add participants to conference fails.");
                        return false;
                    }

                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(strResponse);

                    // Node list.
                    XmlNodeList oNodeList = xDoc.SelectNodes("//member");

                    // Start retreiving the fault codes.
                    foreach (XmlNode oNode in oNodeList)
                    {
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "address")
                            party.sAddress = oNode.SelectSingleNode("value/string").InnerText.Trim();
                        logger.Trace("Party Address: " + party.sAddress);

                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "participantGUID")
                            party.sGUID = oNode.SelectSingleNode("value/string").InnerText.Trim();
                        logger.Trace("Party GUID: " + party.sGUID);


                        if (party.sGUID.Trim() != "" && party.sAddress.Trim() != "")
                        {
                            db.UpdateConferenceEndpointStatus(conf.iDbID, conf.iInstanceID, party.sAddress, party.sGUID);
                            party.sAddress = ""; party.sGUID = "";
                        }

                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        #endregion

        #region MuteVideoTransmitEndpoint
        /// <summary>
        /// MuteVideoTransmitEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteVideoTransmitEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering MuteEndpoint method...Mute = " + mute.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.bMute = mute;
                bool ret = CreateStruct("MutePartyVideoTransmit", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending mute cmd to Cisco MCU...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.set", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion

        #region MuteVideoReceiveEndpoint
        /// <summary>
        /// MuteVideoReceiveEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteVideoReceiveEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering MuteEndpoint method...Mute = " + mute.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.bMute = mute;
                bool ret = CreateStruct("MutePartyVideoReceiver", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending mute cmd to Cisco MCU...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.set", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion

        #region LockUnlockConference
        /// <summary>
        /// LockUnlockConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="Duration"></param>
        /// <param name="lockorunlock"></param>
        /// <returns></returns>
        internal bool LockUnlockConference(NS_MESSENGER.Conference conf, int Duration, int lockorunlock)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering LockUnlockConference method...LockOrUnlock = " + lockorunlock.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                dummyParty.cMcu = conf.cMcu;
                conf.iDuration = Duration;
                conf.ilockorunlock = lockorunlock;
                bool ret = CreateStruct("LockOrUnLockConference", dummyParty, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending LockUnlockConference cmd to Cisco MCU...");
                ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "conference.set", oStruct, ref strResponse);

                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        #endregion

        #region EquateLineRate
        /// <summary>
        /// EquateLineRate
        /// </summary>
        /// <param name="lineRateValue"></param>
        /// <param name="codianLineRate"></param>
        /// <returns></returns>
        private bool EquateLineRate(NS_MESSENGER.LineRate.eLineRate lineRateValue, ref int codianLineRate)
        {
            try
            {
                switch (lineRateValue)
                {
                    case NS_MESSENGER.LineRate.eLineRate.K64:
                        {
                            codianLineRate = 64;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K128:
                        {
                            codianLineRate = 128;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K192:
                        {
                            codianLineRate = 192;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K256:
                        {
                            codianLineRate = 256;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K320:
                        {
                            codianLineRate = 320;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K384:
                        {
                            codianLineRate = 384;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K512:
                        {
                            codianLineRate = 512;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K768:
                        {
                            codianLineRate = 768;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1024:
                        {
                            codianLineRate = 1024;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1152:
                        {
                            codianLineRate = 1024;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1250:
                        {
                            codianLineRate = 1250;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1472:
                        {
                            codianLineRate = 1250;
                            break;
                        }

                    case NS_MESSENGER.LineRate.eLineRate.M1536:
                        {
                            codianLineRate = 1536;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1792:
                        {
                            codianLineRate = 1792;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1920:
                        {
                            codianLineRate = 1792;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M2048:
                        {
                            codianLineRate = 2048;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M2560:
                        {
                            codianLineRate = 2560;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M3072:
                        {
                            codianLineRate = 3072;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M3584:
                        {
                            codianLineRate = 3584;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M4096:
                        {
                            codianLineRate = 4096;
                            break;
                        }
                    default:
                        {
                            // default is 384 kbps
                            codianLineRate = 384;
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        #region ParticipantDiagnostics
        /// <summary>
        /// ParticipantDiagnostics
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        private bool ParticipantDiagnostics(NS_MESSENGER.Conference conf, NS_MESSENGER.Smtp smtpServerInfo, ref NS_MESSENGER.Party party)
        {
            try
            {
                // instantiating new objects
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                conf.ServerAddress = smtpServerInfo.ServerAddress;
                conf.PortNumber = smtpServerInfo.PortNumber;
                // create the xml-rpc object
                bool ret = false;
                ret = CreateStruct("ParticipantDiagnostics", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Create Struct failed.");
                    return false;
                }

                // send the cmd to Cisco 8710 MCU
                string strResponse = null; ret = false;
                ret = SendCommand(party.cMcu, "participant.diagnostics", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Send command failed.");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
        #endregion

        #region ConferenceStatus
        /// <summary>
        /// ConferenceStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool ConferenceStatus(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            XmlDocument oDOM = new XmlDocument();
            XmlNodeList oNodeList = null;
            String conferenceGUID = "", locked = "", lockDuration = "", recording = "";
            
            try
            {
                logger.Trace("Entering Conference Status method...");

                Queue partyq = new Queue();
                this.participantEnumerateID = 0;

                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();

                bool ret = CreateStruct("ConferenceStatus", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                logger.Trace("Sending conference status cmd to Cisco 8710 mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "conference.status", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                if (strResponse.Trim() != "")
                {
                    oDOM = new XmlDocument();
                    oNodeList = null;
                    conferenceGUID = ""; locked = ""; lockDuration = ""; recording = "";

                    oDOM.LoadXml(strResponse);

                    oNodeList = oDOM.SelectNodes("//member");
                    foreach (XmlNode oNode in oNodeList)
                    {
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "conferenceGUID") //Globally unique identifier of the conference
                            conferenceGUID = oNode.SelectSingleNode("value/string").InnerText.Trim();

                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "locked") //Defines whether the conference is locked.
                            locked = oNode.SelectSingleNode("value/boolean").InnerText.Trim();

                        //The period of time (in seconds) from now until the conference lock expires. Requires that locked is true and ignored otherwise.
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "lockDuration")
                            lockDuration = oNode.SelectSingleNode("value/int").InnerText.Trim();

                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "recording") //True if this conference is being recorded by a recording device specified in conference.invite.
                            recording = oNode.SelectSingleNode("value/boolean").InnerText.Trim();
                    }

                    ret = false;
                    ret = ProcessXML_AllPartyList(strResponse, ref partyq, party.sGUID);
                    if (!ret)
                    {
                        logger.Trace("Failure in processing the conference Participant list returned by Cisco 8710 MCU");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #endregion

        //Need to Work on it FB 2501

        #region ConfMonitor Methods
        internal bool FetchAllOngoingConfsStatus(NS_MESSENGER.MCU mcu, ref Queue confq)
        {
            try
            {
                bool ret = false;

                // fetch all participants
                Queue partyq = new Queue();
                this.participantEnumerateID = 0;
                do
                {
                    // instantiating new objects
                    vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    NS_MESSENGER.Conference conference = new NS_MESSENGER.Conference();
                    party.cMcu = mcu;
                    conference.cMcu = mcu;

                    // create the xml-rpc object
                    ret = false;
                    ret = CreateStruct("FetchAllParticipants", party, conference, ref oStruct);
                    if (!ret)
                    {
                        logger.Trace("Create Struct failed.");
                        return false;
                    }

                    // send the cmd to Cisco mcu
                    string strResponse = null; ret = false;
                    ret = SendCommand(mcu, "participant.enumerate", oStruct, ref strResponse);
                    if (!ret)
                    {
                        logger.Trace("Send command failed.");
                        return false;
                    }

                    // process the response
                    ret = false;
                    ret = ProcessXML_AllPartyList(strResponse, ref partyq, "");
                    if (!ret)
                    {
                        logger.Trace("Failure in processing the conference list returned by Cisco MCU");
                        return false;
                    }
                }
                while (this.participantEnumerateID > 0); //keep repeating since thr are more confs to be retreived from the Cisco mcu.

                // array to store all conf names
                // magic number = 200 (used becos thats the max number that can be stored in Cisco history)
                int maxArrayCount = 200;
                NS_MESSENGER.Conference[] confList = new NS_MESSENGER.Conference[200];
                logger.Trace("Before the conf name array initalization");
                for (int i = 1; i < maxArrayCount; i++)
                {
                    //array initialization
                    confList[i] = new NS_MESSENGER.Conference();
                    confList[i].sMcuName = "";
                }

                logger.Trace("After the conf name array initalization");

                // combine all participants in conf objects
                int lastElementIndexAddedInArray = 1;
                int partyCount = partyq.Count;
                logger.Trace("Party Count = " + partyCount.ToString());
                for (int i = 0; i < partyCount; i++)
                {
                    logger.Trace("Party#" + i.ToString());

                    // get the party from the queue
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)partyq.Dequeue();

                    if (party.sConfNameOnMcu == null)
                    {
                        logger.Trace("Invalid party. No conf name associated.");
                        continue;
                    }

                    if (party.sConfNameOnMcu.Length < 3)
                    {
                        logger.Trace("Invalid party. Conf name associated is too short.");
                        continue;
                    }

                    logger.Trace("Got the party info.");

                    // compare the confs 
                    bool isConfNameInList = false;
                    for (int j = 1; j < maxArrayCount; j++)
                    {
                        if (party.sConfNameOnMcu.Equals(confList[j].sMcuName))
                        {
                            logger.Trace("Conf exists so skip adding it.");
                            // conf exists in the array so skip adding it
                            isConfNameInList = true;
                            confList[j].qParties.Enqueue(party);
                            break;
                        }
                    }

                    // conf name is not in the list
                    // so add the new conf and attach the participant to the conf list
                    if (isConfNameInList == false)
                    {
                        logger.Trace("New conf found. Added to list.");

                        // add the conf name in list							
                        confList[lastElementIndexAddedInArray].sMcuName = party.sConfNameOnMcu;
                        confList[lastElementIndexAddedInArray].qParties.Enqueue(party);
                        lastElementIndexAddedInArray++;
                    }
                }

                // attach confs in queue
                for (int k = 1; k < lastElementIndexAddedInArray; k++)
                {
                    confq.Enqueue((NS_MESSENGER.Conference)confList[k]);
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        private bool ProcessXML_AllPartyList(string strXML, ref Queue partyq, String PartyGUID)
        {
            try
            {
                XmlDocument oDOM = new XmlDocument();
                oDOM.LoadXml(strXML);
                string endpointCategory = "", callStartMute = "", master = "", callType = "", callProtocol = "", disconnectReason = "", address="";
                string micMute = "", recordingDevice = "", txAudioMute = "", rxAudioMute = "", txVideoMute = "", rxVideoMute = "", isImportant = "";
                string rxPreviewURL = "", txPreviewURL = "", callDuration = "", callDirection = "";
                int callState = 0, callBandwidth = 0;
                NS_MESSENGER.Party party = null;
                XmlNodeList oNodeList = oDOM.SelectNodes("//member/value/array/data/value");

                foreach (XmlNode oInnerNode in oNodeList)
                {
                    try
                    {
                        party = new NS_MESSENGER.Party();

                        // The GUID of this participant, assigned by the TelePresence Server
                        if (oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string") != null)
                            party.sGUID = oInnerNode.SelectSingleNode("struct/member[name = \"participantGUID\"]/value/string").InnerText.Trim();
                        logger.Trace("Participant GUID:" + party.sGUID);

                        if (party.sGUID != PartyGUID)
                            continue;

                        // State of the call between the TelePresence Server and this participant
                        callState = 0;
                        if (oInnerNode.SelectSingleNode("struct/member[name = \"callState\"]/value/int") != null)
                            Int32.TryParse(oInnerNode.SelectSingleNode("struct/member[name = \"callState\"]/value/int").InnerText.Trim(), out callState);
                        logger.Trace("Party Call State:" + callState);
                        switch (callState)
                        {
                            case 0:
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                                break;
                            case 1:
                            case 3:
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE;
                                break;
                            case 4:
                            case 2:
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                break;
                        }
                        if (oInnerNode.SelectSingleNode("struct/member[name = \"endpointCategory\"]/value/string") != null)
                            endpointCategory = oInnerNode.SelectSingleNode("struct/member[name = \"endpointCategory\"]/value/string").InnerText.Trim();
                        logger.Trace("Participant Endpoint Category:" + endpointCategory);

                        if (oInnerNode.SelectSingleNode("struct/member[name = \"callStartMute\"]/value/boolean") != null)
                            callStartMute = oInnerNode.SelectSingleNode("struct/member[name = \"callStartMute\"]/value/boolean").InnerText.Trim();
                        logger.Trace("Participant Call StartMute:" + callStartMute);

                        if (oInnerNode.SelectSingleNode("struct/member[name = \"master\"]/value/boolean") != null)
                            master = oInnerNode.SelectSingleNode("struct/member[name = \"master\"]/value/boolean").InnerText.Trim();
                        logger.Trace("Party master:" + master);

                        //AudioOrVideo in Conf_user_d table
                        if (oInnerNode.SelectSingleNode("struct/member[name = \"callType\"]/value/string") != null)
                            callType = oInnerNode.SelectSingleNode("struct/member[name = \"callType\"]/value/string").InnerText.Trim();
                        logger.Trace("Party callType:" + callType);
                        if (callType == "audio")
                            party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
                        else
                            party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;

                        if (oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string") != null)
                            address = oInnerNode.SelectSingleNode("struct/member[name = \"address\"]/value/string").InnerText.Trim();
                        logger.Trace("Party address:" + address);
                        party.sAddress = address;

                        if (oInnerNode.SelectSingleNode("struct/member[name = \"callProtocol\"]/value/string") != null)
                            callProtocol = oInnerNode.SelectSingleNode("struct/member[name = \"callProtocol\"]/value/string").InnerText.Trim();
                        if (callProtocol == "sip")
                            party.etProtocol = NS_MESSENGER.Party.eProtocol.SIP;
                        else
                            party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                        logger.Trace("Party callProtocol: " + party.etProtocol);

                        //partyStatus 1- Connected , 2 -Disconnected
                        //if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
                        //{
                            //The URL to retrieve a jpeg snapshot of video received from this participant.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"rxPreviewURL\"]/value/string") != null)
                                rxPreviewURL = oInnerNode.SelectSingleNode("struct/member[name = \"rxPreviewURL\"]/value/string").InnerText.Trim();
                            logger.Trace("Participant rxPreviewURL:" + rxPreviewURL);
                            party.sStream = rxPreviewURL;

                            //The URL to retrieve a jpeg snapshot of video sent to this participant
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"txPreviewURL\"]/value/string") != null)
                                txPreviewURL = oInnerNode.SelectSingleNode("struct/member[name = \"txPreviewURL\"]/value/string").InnerText.Trim();
                            logger.Trace("Participant txPreviewURL:" + txPreviewURL);

                            //The duration of the call in seconds.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"callDuration\"]/value/int") != null)
                                callDuration = oInnerNode.SelectSingleNode("struct/member[name = \"callDuration\"]/value/int").InnerText.Trim();
                            logger.Trace("Participant callDuration:" + callDuration);

                            //This parameter is not present if callState is 0 (not connected).
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"callDirection\"]/value/string") != null)
                                callDirection = oInnerNode.SelectSingleNode("struct/member[name = \"callDirection\"]/value/string").InnerText.Trim();
                            logger.Trace("Participant callDirection:" + callDirection);

                            //Call bandwidth in kbps. - Need output
                            bool ret = false;
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"callBandwidth\"]/value/int") != null)
                                Int32.TryParse(oInnerNode.SelectSingleNode("struct/member[name = \"callBandwidth\"]/value/int").InnerText.Trim(), out callBandwidth);
                            logger.Trace("Participant callBandwidth:" + callBandwidth);

                            ret = db.ConvertToMcuLineRate(callBandwidth, ref party.stLineRate.etLineRate);
                            if (!ret)
                            {
                                logger.Trace("Participant address type is incorrect.");
                                return false;
                            }

                            //True if far end microphone is muted.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"micMute\"]/value/boolean") != null)
                                micMute = oInnerNode.SelectSingleNode("struct/member[name = \"micMute\"]/value/boolean").InnerText.Trim();
                            logger.Trace("Participant micMute:" + micMute);

                            if (oInnerNode.SelectSingleNode("struct/member[name = \"recordingDevice\"]/value/boolean") != null)
                                recordingDevice = oInnerNode.SelectSingleNode("struct/member[name = \"recordingDevice\"]/value/boolean").InnerText.Trim();
                            logger.Trace("Participant recordingDevice:" + recordingDevice);

                            //Defines whether the TelePresence Server mutes the audio signal transmitted to this endpoint.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"txAudioMute\"]/value/boolean") != null)
                                txAudioMute = oInnerNode.SelectSingleNode("struct/member[name = \"txAudioMute\"]/value/boolean").InnerText.Trim();
                            logger.Trace("Participant txAudioMute:" + txAudioMute);
                            if (txAudioMute == "1") //FB 2501 Dec6
                                party.bMuteRxaudio = true; //FB 3008
                            else
                                party.bMuteRxaudio = false;//FB 3008

                            //Defines whether the TelePresence Server mutes the audio signal received from this endpoint.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"rxAudioMute\"]/value/boolean") != null)
                                rxAudioMute = oInnerNode.SelectSingleNode("struct/member[name = \"rxAudioMute\"]/value/boolean").InnerText.Trim();
                            if (rxAudioMute == "1")//FB 2501 Dec6
                                party.bMute = true;//FB 3008
                            else
                                party.bMute = false;//FB 3008

                            logger.Trace("Participant rxAudioMute:" + rxAudioMute);

                            //Defines whether the TelePresence Server mutes the video signal transmitted to this endpoint.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"rxVideoMute\"]/value/boolean") != null)
                                txVideoMute = oInnerNode.SelectSingleNode("struct/member[name = \"txVideoMute\"]/value/boolean").InnerText.Trim();
                            if (txVideoMute == "1")//FB 2501 Dec6
                                party.bMuteTxvideo = true;
                            else
                                party.bMuteTxvideo = false; //doubt
                            logger.Trace("Participant txVideoMute:" + txVideoMute);

                            //Defines whether the TelePresence Server mutes the video signal received from this endpoint.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"rxVideoMute\"]/value/boolean") != null)
                                rxVideoMute = oInnerNode.SelectSingleNode("struct/member[name = \"rxVideoMute\"]/value/boolean").InnerText.Trim();
                            if (rxVideoMute == "1")//FB 2501 Dec6
                                party.bMuteRxvideo = true;
                            else
                                party.bMuteRxvideo = false;
                            logger.Trace("Participant rxVideoMute:" + rxVideoMute);

                            //Defines whether the participant is important (i.e. the participant's transmitted video is given preference over others when composing video).
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"isImportant\"]/value/boolean") != null)
                                isImportant = oInnerNode.SelectSingleNode("struct/member[name = \"isImportant\"]/value/boolean").InnerText.Trim();
                            if (isImportant == "1")//FB 2501 Dec6
                                party.bSetFocus = true;
                            else
                                party.bSetFocus = false;

                            logger.Trace("Party isImportant:" + isImportant);
                       // }
                        //else 
                        if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT) //Disconnected
                        {
                            //The reason why the endpoint disconnected.
                            if (oInnerNode.SelectSingleNode("struct/member[name = \"disconnectReason\"]/value/string") != null)
                                disconnectReason = oInnerNode.SelectSingleNode("struct/member[name = \"disconnectReason\"]/value/string").InnerText.Trim();
                            logger.Trace("Party disconnectReason:" + disconnectReason);
                        }
                        logger.Trace("Save party details in Database.");
                        if (party.sGUID == PartyGUID)
                            db.UpdateConfPartyDetails(party);

                        // add party to queue
                        logger.Trace("Adding party to queue...");
                        partyq.Enqueue(party);
                    }
                    catch (Exception e)
                    {
                        logger.Trace("Invalid party call parameters. Error:" + e.Message);
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        #endregion

        #region CallDetailRecords
        /// <summary>
        /// CallDetailRecords
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool CallDetailRecords(NS_MESSENGER.MCU MCU, int index, ref String strResponse)
        {
            vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
            NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
            NS_MESSENGER.Conference dummyConf = new NS_MESSENGER.Conference();
            bool ret = false;
            try
            {
                dummyParty.cMcu = MCU;
                indexCDR = index;
                logger.Trace("Create Struct for cdrlog.enumerate method");
                ret = CreateStruct("CallDetailRecords", dummyParty, dummyConf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Create Struct failed.");
                    return false;
                }
                ret = false;
                ret = SendCommand(dummyParty.cMcu, "cdrlog.enumerate", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Response Error in CDR command.");
                    return false;
                }

                //strResponse = "<methodResponse><params><param><value><struct><member><name>events</name><value><array><data><value><struct><member><name>index</name><value><int>0</int></value></member><member><name>time</name><value><dateTime.iso8601>20120924T10:16:04</dateTime.iso8601></value></member><member><name>type</name><value><string>conferenceStarted</string></value></member><member><name>conferenceStarted</name><value><struct><member><name>conferenceGUID</name><value><string>d92a6f90-0630-11e2-9b0d-000d7c1104b0</string></value></member><member><name>name</name><value><string>MSVC CTX</string></value></member><member><name>numericId</name><value><string>10000</string></value></member><member><name>uris</name><value><array><data><value><struct><member><name>uri</name><value><string>10000</string></value></member><member><name>pinProtected</name><value><string>no</string></value></member></struct></value></data></array></value></member></struct></value></member></struct></value><value><struct><member><name>index</name><value><int>1</int></value></member><member><name>time</name><value><dateTime.iso8601>20120924T10:16:04</dateTime.iso8601></value></member><member><name>type</name><value><string>conferenceStarted</string></value></member><member><name>conferenceStarted</name><value><struct><member><name>conferenceGUID</name><value><string>d92e6730-0630-11e2-9b0d-000d7c1104b0</string></value></member><member><name>name</name><value><string>MSVC CTX</string></value></member><member><name>numericId</name><value><string>10001</string></value></member><member><name>uris</name><value><array><data><value><struct><member><name>uri</name><value><string>10001</string></value></member><member><name>pinProtected</name><value><string>no</string></value></member></struct></value></data></array></value></member></struct></value></member></struct></value></data></array></value></member><member><name>nextIndex</name><value><int>20</int></value></member><member><name>eventsRemaining</name><value><boolean>1</boolean></value></member><member><name>startIndex</name><value><int>0</int></value></member><member><name>currentTime</name><value><dateTime.iso8601>20121022T10:26:02</dateTime.iso8601></value></member></struct></value></param></params></methodResponse>";

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
        #endregion

    }
}

