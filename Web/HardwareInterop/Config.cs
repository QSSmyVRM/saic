//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : Config.cs
 * DESCRIPTION : All configuration file/db parameters are read in this file. 
 * AUTHOR : Kapil M
 */
namespace NS_CONFIG
{
	#region References
	using System;
	using System.Xml;
	using System.Diagnostics;
	using System.IO;
	using cryptography;
	#endregion 

	class Config 
	{	
		internal string errMsg = null;

		internal Config ()
		{
			// constructor	
		}

		internal bool Initialize (string configPath,ref NS_MESSENGER.ConfigParams configParams)
		{			
			configParams.localConfigPath = configPath;

			bool ret = Read_Local_Config_File(ref configParams);
			if (!ret) 
			{
				this.errMsg = "Error reading local config file.";
				return (false);
			}

			ret = false;
			ret = Read_Global_Config_File(ref configParams);
			if (!ret) 
			{
				this.errMsg = "Error reading global config file.";
				return (false);
			}
			
			ret = false;
			ret = Read_Database_Params(ref configParams);
			if (!ret) 
			{
				this.errMsg = "Error reading database params.";
				return (false);
			}

			return(true);
		}

		private bool Read_Local_Config_File(ref NS_MESSENGER.ConfigParams configParams)
		{
			// Read from XML config file 
			try 
			{ 
				// Open an XML file 
				XmlDocument xmlConfig = new XmlDocument();
				xmlConfig.Load(configParams.localConfigPath);

				// main config file path
                try
                {
                    configParams.globalConfigPath = xmlConfig.SelectSingleNode("//RTCConfig/VRM_Config_File_Path").InnerXml.Trim();                  
                }
                catch
                {
                    this.errMsg = "Error reading local config file. VRM_Config_File_Path param is invalid.";
                    return false;
                }
				
                // local component log file path
                try
                {

                    configParams.logFilePath = xmlConfig.SelectSingleNode("//RTCConfig/Log_File_Path").InnerXml.Trim();				    
                }
                catch (Exception)
				{
                    this.errMsg = "Error reading local config file. Log_File_Path param is invalid.";
                    return false;
				}

				// verbose trace switch
                try
                {
                    string debug = xmlConfig.SelectSingleNode("//RTCConfig/Debug").InnerXml.ToUpper().Trim();                   
                    if (debug.Equals("YES"))
                    {
                        configParams.fileTraceEnabled = true;
                    }
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading local config file. Debug param is invalid.";
                    return false;
                }


				// run scheduled reports aspx caller page url
				try
				{
					configParams.scheduledReportsAspxUrl = xmlConfig.SelectSingleNode("//RTCConfig/ScheduledReportsAspxUrl").InnerXml.Trim();
				}
				catch (Exception)
				{
                    this.errMsg = "Error reading local config file. ScheduledReportsAspxUrl param is invalid.";
                    return false;
				}

                // auto restrict db log file growth
                try
                {
                    string autoRestrictDbGrowth = xmlConfig.SelectSingleNode("//RTCConfig/AutoRestrictDbLogFileGrowth").InnerXml.ToUpper().Trim();
                    if (autoRestrictDbGrowth.Equals("YES"))
                    {
                        configParams.bAutoRestrictDbLogFileGrowth = true;
                    }
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading local config file. AutoRestrictDbLogFileGrowth param is invalid.";
                    return false;
                }

                // FB case#582: auto terminate options
                try
                {
                    string autoTerminateCall = xmlConfig.SelectSingleNode("//RTCConfig/AutoTerminateCall/On").InnerXml.ToUpper().Trim();
                    if (autoTerminateCall.Equals("YES"))
                    {
                        configParams.stAutoTerminateCall.On = true;
                        configParams.stAutoTerminateCall.Time_Before_First_Join = Int32.Parse(xmlConfig.SelectSingleNode("//RTCConfig/AutoTerminateCall/Time_Before_First_Join").InnerXml.Trim());
                        configParams.stAutoTerminateCall.Time_After_Last_Quit = Int32.Parse(xmlConfig.SelectSingleNode("//RTCConfig/AutoTerminateCall/Time_After_Last_Quit").InnerXml.Trim());
                    }
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading local config file. AutoTerminateCall params are invalid.";
                    return false;
                }

                //Code Commented for FB 1642 - DTMF
                // FB case#1632: DTMF codes for bridges
                //try
                //{
                //    // MGC-specific DTMF codes
                //    configParams.stDTMF_MGC.PreConfCode = xmlConfig.SelectSingleNode("//RTCConfig/DTMF/MGC/PreConfCode").InnerXml.Trim();
                //    configParams.stDTMF_MGC.PreLeaderPin = xmlConfig.SelectSingleNode("//RTCConfig/DTMF/MGC/PreLeaderPin").InnerXml.Trim();
                //    configParams.stDTMF_MGC.PostLeaderPin = xmlConfig.SelectSingleNode("//RTCConfig/DTMF/MGC/PostLeaderPin").InnerXml.Trim();

                //    // Codian-specific DTMF codes
                //    configParams.stDTMF_Codian.PreConfCode = xmlConfig.SelectSingleNode("//RTCConfig/DTMF/Codian/PreConfCode").InnerXml.Trim();
                //    configParams.stDTMF_Codian.PreLeaderPin = xmlConfig.SelectSingleNode("//RTCConfig/DTMF/Codian/PreLeaderPin").InnerXml.Trim();
                //    configParams.stDTMF_Codian.PostLeaderPin = xmlConfig.SelectSingleNode("//RTCConfig/DTMF/Codian/PostLeaderPin").InnerXml.Trim();
                //}
                //catch (Exception)
                //{
                //    this.errMsg = "Error reading local config file. DTMF params are invalid.";
                //    return false;
                //}
                //FB 1658 - Embedded Image code changes start
                //Mail Logo Path
                try
                {
                    configParams.mailLogoPath = xmlConfig.SelectSingleNode("//RTCConfig/MailLogoPath").InnerXml.Trim();
                }
                catch (Exception)
                {
                    this.errMsg = "Error reading local config file. MailLogoPath param is invalid.";
                    return false;
                }
                //FB 1658 - Embedded Image code changes end

                //Commented for FB 2655 Start
                //Disney New Audio Add ON Request start
                //Domestic or International Audio Dial No Prefix
                //try
                //{
                //    configParams.audioDialNoPrefix = xmlConfig.SelectSingleNode("//RTCConfig/DTMF/AudioDialInPrefix").InnerText.Trim();
                //}
                //catch (Exception)
                //{
                //    this.errMsg = "Error reading local config file. MailLogoPath param is invalid.";
                //    return false;
                //}

                //Disney New Audio Add ON Request end
                //Commented for FB 2655 Start
/*				
				try
				{				
					// redundant now as every pwd has encryption built in. keeping it for legacy purposes.
					node = xmlConfig.SelectSingleNode("//RTCConfig/EncryptionEnabled");
					string encryption = node.InnerXml.Trim();
					if (encryption.Equals("yes") || encryption.Equals("Yes") || encryption.Equals("YES"))
					{
						configParams.encryptionEnabled = true;
					}
				}		
				catch (Exception)
				{
					configParams.encryptionEnabled = true;
				}

				
				#region Ldap parameters
				// ldap filter
				try
				{
					configParams.ldapLoginKey = xmlConfig.SelectSingleNode("//RTCConfig/LdapLoginKey").InnerXml.Trim();
				}
				catch (Exception)
				{
					configParams.ldapLoginKey = "cn";
				}

				// ldap cycle time
				try
				{
					configParams.ldapCycleTime = DateTime.Parse(xmlConfig.SelectSingleNode("//RTCConfig/LdapCycleTime").InnerXml.Trim());
				}
				catch (Exception)
				{
					DateTime dt = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,03,00,00);
					configParams.ldapCycleTime = dt;					
				}
				#endregion
*/
				
				

				return true;
			}
			catch (Exception e)
			{
				this.errMsg = "Error reading local config file. Exception Message : " + e.Message;				
				return false;
			}			
		}

		private bool Read_Global_Config_File(ref NS_MESSENGER.ConfigParams configParams)
		{
			// Read from XML config file 
			try 
			{ 
				// Open an XML file 
				XmlDocument xmlConfig = new XmlDocument();
				xmlConfig.Load(configParams.globalConfigPath);

                // Database Information

                // Db Server Address
                configParams.databaseServer = xmlConfig.SelectSingleNode("//VRMConfig/Database/Server").InnerXml.Trim();
				
                // Db Name
                configParams.databaseName = xmlConfig.SelectSingleNode("//VRMConfig/Database/Name").InnerXml.Trim();

                // Db Login
				configParams.databaseLogin = xmlConfig.SelectSingleNode("//VRMConfig/Database/Login").InnerXml.Trim();

                // Db Connection Timeout
                try
                {
                    configParams.databaseConTimeout = Int32.Parse(xmlConfig.SelectSingleNode("//VRMConfig/Database/ConnectionTimout").InnerXml.Trim());
                }
                catch (Exception)
                {
                    configParams.databaseConTimeout = 10;
                }

                // Db Trunsted COnnection FB 2700
                try
                {
                    configParams.trustedConnection = xmlConfig.SelectSingleNode("//VRMConfig/Database/trustedConnection").InnerXml.Trim();
                }
                catch (Exception)
                {
                    configParams.trustedConnection = "";
                }


                // Db Password
				string encryptedPwd = xmlConfig.SelectSingleNode("//VRMConfig/Database/Password").InnerXml.Trim();				
				// decrypt password
                if (encryptedPwd.Trim() != "")// FB 2700
                {
                    cryptography.Crypto crypto = new Crypto();
                    configParams.databasePwd = crypto.decrypt(encryptedPwd);
                }

                // Db type
				string databaseType = xmlConfig.SelectSingleNode("//VRMConfig/Database/Type").InnerXml.Trim();		
				if (databaseType.CompareTo("SA_SQLServer_Client") !=0)
				{
					// Only MS-SQLSERVER-2000 is supported currently. 
					// Return error if any other string.
					this.errMsg = "This database type is not supported. Only MS SqlServer 2000/2005 are supported.";
					return(false);
				}
				else
				{
					// It is SQL Server .
					configParams.databaseType = NS_MESSENGER.ConfigParams.eDatabaseType.SQLSERVER;
				}
                
                // Client switch
                configParams.client = xmlConfig.SelectSingleNode("//VRMConfig/Client").InnerXml.ToUpper().Trim();								

                // Ldap enabled or disabled
				try
				{
					string ldap= xmlConfig.SelectSingleNode("//VRMConfig/SSOMode").InnerXml.ToUpper().Trim();
					if (ldap.Equals("YES"))
					{
						configParams.ldapEnabled = true;
					}					
				}
				catch (Exception)
				{
                    this.errMsg = "Error reading global config file. SSOMode is invalid.";
                    return false;
				}

				return (true);
			}
			catch (Exception e)
			{
				this.errMsg = "Error reading global config file. Exception Message : " + e.Message;				
				return (false);
			}			
		}


		private bool Read_Database_Params(ref NS_MESSENGER.ConfigParams configParams)
		{
			//fetch the logging params
			try 
			{
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				bool ret = db.FetchLogSettings(ref configParams);
				if (!ret) 
				{
                    this.errMsg = "Invalid log settings in database";
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = "Error reading database settings. Exception Message : " + e.Message;				
				return false;
			}
		}
	}
}	