//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : Ldap.cs
 * DESCRIPTION : All ldap commands are stored in this file. 
 * AUTHOR : Kapil M
 */
namespace NS_LDAP
{
	#region references
	using System;
	using System.Collections;
	using System.DirectoryServices;
	// maneesh pujari 07/29/2008 FB 594.
    using System.Threading;
	#endregion

	class LDAP
	{
		private NS_MESSENGER.ConfigParams configParams;
		private NS_LOGGER.Log logger;		
		private System.DirectoryServices.DirectorySearcher dirSearcher ;
		private System.DirectoryServices.DirectoryEntry dirEntry ;
		internal string errMsg = null;
        
		// maneesh pujari 07/29/2008 FB 594.
        //LDAT related stuff
        private Queue m_objLldapUserList = null;
        private int m_lpdatSyncState = -1;
        System.Threading.Thread m_objLDAPSyncThreadFromLDAP = null;
        System.Threading.Thread m_objLDAPSyncThreadTOmyVRMDB = null;
        int m_iLDAPTotalUserCount = 0;
        int m_iTotalUserNotImported = 0;
        string m_strLDAPErrorInfo = null;

        enum LDAP_SYNC_STATE
        {
            LDAP_SYNC_INITIALIZE = 0,
            LDAP_SYNC_STARTED = 1,
            LDAP_SYNC_RUNNING = 2,
            LDAP_SYNC_FINISHED = 3,
            LDAP_SYNC_ERROR = 4,
            LDAP_SYNC_NOTSTARTED = 5
        }

		internal LDAP(NS_MESSENGER.ConfigParams config)
		{	
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
			dirEntry = new System.DirectoryServices.DirectoryEntry();
			// maneesh pujari 07/29/2008 FB 594.
			dirSearcher = new System.DirectoryServices.DirectorySearcher(dirEntry);

            // LDAP Sync Related stuff
            m_objLldapUserList = new Queue();
            m_objLDAPSyncThreadFromLDAP = new System.Threading.Thread(new System.Threading.ThreadStart(SyncFroLDAPThreadProc));
            m_objLDAPSyncThreadTOmyVRMDB = new System.Threading.Thread(new System.Threading.ThreadStart(SyncTomyVRMDBThreadProc));
            m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_NOTSTARTED;
            m_strLDAPErrorInfo = "";
        }
 		internal bool AuthenticateUser(NS_MESSENGER.LdapSettings ldapSettings)//(string userName,string userPassword)
		{
			try
			{
				logger.Trace ("Authenticating user with ldap server...");

				// specify the ldap server info
				string ldapPath = "LDAP://" + ldapSettings.serverAddress + ":" + ldapSettings.connPort;
				logger.Trace ("Server : " + ldapPath);		
				this.dirEntry.Path = ldapPath;
                
                //check to see if domain prefix exists
                if (ldapSettings.domainPrefix != null)
                {
                    // prefix the domain before the username 
                    ldapSettings.serverLogin = ldapSettings.domainPrefix + ldapSettings.serverLogin;
                }

                // specify ldap settings
                this.dirEntry.Username=ldapSettings.serverLogin.Trim();
				logger.Trace ("UserName : " + ldapSettings.serverLogin.Trim());
				this.dirEntry.Password=ldapSettings.serverPassword.Trim();
				logger.Trace ("Password : " + ldapSettings.serverPassword.Trim());
				// set auth type to "None"
				this.dirEntry.AuthenticationType = 0; 
                //FB 2993 LDAP START
                switch (ldapSettings.AuthenticationType)
                {
                    case 0:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.None;
                        break;
                    case 1:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.Signing;
                        break;
                    case 2:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.Sealing;
                        break;
                    case 3:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.Secure;
                        break;
                    case 4:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.Anonymous;
                        break;
                    case 5:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.Delegation;
                        break;
                    case 6:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.Encryption;
                        break;
                    case 7:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.FastBind;
                        break;
                    case 8:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.ReadonlyServer;
                        break;
                    case 9:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.SecureSocketsLayer;
                        break;
                    case 10:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.ServerBind;
                        break;
                    default:
                        this.dirEntry.AuthenticationType = AuthenticationTypes.None;
                        break;
                }
                //FB 2993 LDAP END

				// specify the search string
				//System.DirectoryServices.DirectorySearcher searcher = new System.DirectoryServices.DirectorySearcher(entry);
				//FORMAT-1 : (&(objectClass=person)(cn=Dev\John Doe)) 
 				//FORMAT-2 : (&(objectClass=person)(cn=John Doe)) 
				//FORMAT-3 : (&(objectClass=person)(samaccountname=Dev2\john)) 
				// There could also be other formats however, the above 3 are the most common ones.
				bool isTryLoginAgain = false;
                do
                {
                    isTryLoginAgain = false;

                    //FB 2096 start
                    if (ldapSettings.searchFilter.Trim() != "")
                        this.dirSearcher.Filter = ("(&" + ldapSettings.searchFilter + "(" + ldapSettings.loginKey + "=" + ldapSettings.serverLogin.Trim() + "))");  //PSU
                    else
                        this.dirSearcher.Filter = "";

				    //this.dirSearcher.Filter = ("(&" + ldapSettings.searchFilter + "(" + ldapSettings.loginKey +"="+ ldapSettings.serverLogin.Trim() + "))");  //PSU
                    //FB 2096 end

                    //this.dirSearcher.Filter = ("(" + ldapSettings.loginKey + "=" + ldapSettings.serverLogin.Trim() + ")"); //PSU 
				    //searcher.Filter = ldapSettings.searchFilter;								
				    logger.Trace ("Search filter : " + this.dirSearcher.Filter);
    				
				    this.dirSearcher.PropertiesToLoad.Add("givenname");
				    this.dirSearcher.PropertiesToLoad.Add("sn");
				    this.dirSearcher.PropertiesToLoad.Add("mail");
				    this.dirSearcher.PropertiesToLoad.Add("cn");
				    this.dirSearcher.PropertiesToLoad.Add("phone");
				    this.dirSearcher.PropertiesToLoad.Add(ldapSettings.loginKey);

				    // run the search. check the result count to see there is only one record.
				    // if multiple records are returned then username entered should not be considered as valid.
				    int resultCount = this.dirSearcher.FindAll().Count;
				    logger.Trace ("Search result count =" + resultCount.ToString()); 
				    if (resultCount == 0)
				    {
					    if (!isTryLoginAgain)
					    {
						    // Weird scenario (which is happening on our server . may not happen on client server)
						    // Strip the starting domain name from the login in the search filter only.
						    int iDomainSlash = ldapSettings.serverLogin.IndexOf("\\");
						    ldapSettings.serverLogin = ldapSettings.serverLogin.Substring(iDomainSlash+1);
						    isTryLoginAgain = true;
						    logger.Trace ("Trying to login again with domain name stripped out this time in search filter...");
    						
					    }

					    this.errMsg = "Connection successful. However, no matching ldap records were found.";
					    //return false;
                        
                        // KM 02/23/09: THIS CHANGE IS TEMPORARY TO MAKE LHRIC PRODUCTION SITE WORK. 
                        // This issue needs to be investigated further. See FB#1252  for more details.
                        return true; 
				    }
				    else
				    {
					    if (resultCount > 1)
					    {
						    this.errMsg = "Connection successful. However, multiple matching ldap records were found.";
						    return false;
					    }
				    }
                }
                while (isTryLoginAgain == true);

			    return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				logger.Exception (100,e.Message);
				return false;
			}			
		}
		// maneesh pujari 07/29/2008 FB 594.
        internal bool SyncWithLDAP(bool bIsSyncCall)
		{
			try
			{
                // First start the Data import from LDAP.start the thread for Sync
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_INITIALIZE;
                m_objLDAPSyncThreadFromLDAP.Start();
                
                // give some time for sync thread to start working
                //Thread.Sleep(2000);

                // Once th LDAP Sync thread is started then start the myVRM DB updation thread to update
                // LDAP users to myVRM DB
                m_objLDAPSyncThreadTOmyVRMDB.Start();
                
                // give some time for myvrm Sync thread to start working
                //Thread.Sleep(2000);

                // If the call is not synchronous then return SUCCESS.
                if (bIsSyncCall == false)
                    return true;

                // Now check the status of the myVRM update thread.
                // if it is alive then wait till it finishes, if finished
                // return error or success.
                while(m_objLDAPSyncThreadTOmyVRMDB.IsAlive ==  true)
                {
                    Thread.Sleep(5000);
                }
                if (m_strLDAPErrorInfo.Contains("SUCCESS"))
                    return true;
                else
                    return false;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}			
		}
        internal void SyncFroLDAPThreadProc()
		{
			try
			{
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_STARTED;
				logger.Trace (" Entering the ldap sub-system...");
				
				// Database settings
				NS_MESSENGER.LdapSettings ldapSettings = new NS_MESSENGER.LdapSettings();
				NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
				bool ret = db.FetchLdapSettings(ref ldapSettings);
				if (!ret)
				{
					logger.Trace ("Error fetching ldap settings");
                    m_strLDAPErrorInfo = "Error fetching ldap settings";
                    m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
				}

				// Authenticate user 
				ret = false;
				ret = AuthenticateUser(ldapSettings);
				if (!ret)
				{
                    m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
                    logger.Trace("Error authenticating ldap user.");
                    m_strLDAPErrorInfo = "Error authenticating ldap user.";
				}

				// specify the search string
				this.dirSearcher.PropertiesToLoad.Add("givenname");
				this.dirSearcher.PropertiesToLoad.Add("sn");
				this.dirSearcher.PropertiesToLoad.Add("mail");				
				this.dirSearcher.PropertiesToLoad.Add("phone");
				this.dirSearcher.PropertiesToLoad.Add(ldapSettings.loginKey);

				// over-ride the search filter as we need to fetch all users and not only one user.
				this.dirSearcher.Filter = ldapSettings.searchFilter; //("(objectClass=person)");
				logger.Trace ("Search filter : " + this.dirSearcher.Filter);
				this.dirSearcher.PageSize = 500 ; // 1k is the max in AD server.				
				logger.Trace ("Search page size : " + this.dirSearcher.PageSize.ToString());
				// maneesh pujari 07/29/2008 FB 594.
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_RUNNING;
                // run the search and get the results in 
                System.DirectoryServices.SearchResultCollection objSearchResults = this.dirSearcher.FindAll();
                m_iLDAPTotalUserCount = objSearchResults.Count;
                logger.Trace("Search result count =" + m_iLDAPTotalUserCount.ToString());

                ResultPropertyCollection propCollection;

                int iLoopCount = 0;

                // cycle thru the search records
				//foreach(System.DirectoryServices.SearchResult result in this.dirSearcher.FindAll()) 
                while (iLoopCount < m_iLDAPTotalUserCount) //FB 1040
                {
                    //Release the CPU
                    Thread.Sleep(10);
					// maneesh pujari 08/04/2008 FB 649.
                    NS_MESSENGER.LdapUser ldapUser = new NS_MESSENGER.LdapUser();
                    propCollection = objSearchResults[iLoopCount].Properties;					
					try
					{
						foreach (string key in propCollection.PropertyNames)
						{	
							logger.Trace("Key:" + key);
							foreach (Object collection in propCollection[key])
							{	
								if (key == "givenname")
									{
										ldapUser.firstName = collection.ToString();
                                        ldapUser.firstName = logger.ReplaceInvalidChars(ldapUser.firstName);                                        
										logger.Trace ("First Name : " + ldapUser.firstName);
										break;
									}
								if (key == "sn")
									{
										ldapUser.lastName = collection.ToString();
                                        ldapUser.lastName = logger.ReplaceInvalidChars(ldapUser.lastName);                                        
										logger.Trace ("Last Name : " + ldapUser.lastName);
										break;
									}
								if (key == "mail")
									{
										ldapUser.email = collection.ToString();
                                        ldapUser.email = logger.ReplaceInvalidChars(ldapUser.email);                                        
										logger.Trace ("Email : " + ldapUser.email);
										break;
									}
								if (key == "phone")
									{
										ldapUser.telephone = collection.ToString();
                                        ldapUser.telephone = logger.ReplaceInvalidChars(ldapUser.telephone);                                        
										logger.Trace ("Telephone : " + ldapUser.telephone);
										break;
									}
								if (key == ldapSettings.loginKey)
									{								
										ldapUser.login = collection.ToString();
                                        ldapUser.login = logger.ReplaceInvalidChars(ldapUser.login);                                        
										logger.Trace ("Login : " + ldapUser.login);
										break;
									}										
							}
						}

						// add only users which have valid email.
						//if (ldapUser.email != null)
						//{
						//	if (ldapUser.email.Length >= 3) 
						//	{
					
							// add user record to queue.
							m_objLldapUserList.Enqueue(ldapUser);
						//	}
					   //}
					}
					catch (Exception e)
                    {
                        m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
						logger.Trace ("Error fetching useer record. Message = " + e.Message);
                        m_strLDAPErrorInfo = "Error fetching user record. Message = " + e.Message;
                        // keep continuing fetching other users
					}
                    iLoopCount++;//FB 1040
				}
				// maneesh pujari 07/29/2008 FB 594.
                // free all the resources used by the objects
                this.dirSearcher.Dispose();
                objSearchResults.Dispose();

                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED;
                logger.Trace("Finished reading Users from LDAP");
                return;
            }
			catch (Exception e)
            {
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR;
				logger.Exception (100,e.Message);
                m_strLDAPErrorInfo = e.Message;
				return;
			}			
		}
		// maneesh pujari 07/29/2008 FB 594.
        internal void SyncTomyVRMDBThreadProc()
        {
            try
            {
                // check if the LDAP sync is running, If running then wait till the Sync Finishes
                while ((m_objLDAPSyncThreadFromLDAP.IsAlive == true) 
                    && ((m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR)
                    || (m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED))
                    )
                {
                    //Thread.Sleep(5000);
                    m_iTotalUserNotImported = 0;
                    if (!SyncUniqueLDAPUsersTomyVRMDB())
                    {
                        logger.Trace(" Error in comparing and loading users in ldap holding area.");
                        m_strLDAPErrorInfo = " Error in comparing and loading users in ldap holding area.";
                        continue;
                    }
                }
                if (m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED)
                {
                    m_strLDAPErrorInfo = "ERROR";
                }

                logger.Trace("LDAP Sync Finished, Total Users Imported: " + (m_iLDAPTotalUserCount - m_iTotalUserNotImported));
                m_iLDAPTotalUserCount = 0;
                
                // Update the LDAP sync Time even if the sync was not successful.
                // Update the sync time if the Sync was successful. FB 649
                if (m_lpdatSyncState == (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED)
                {
                    NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
                    db.UpdateLdapSyncTime();
                }                 
                m_objLldapUserList.Clear();
                // Reset the LDAP Sync State.
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_NOTSTARTED;
            }
            catch (Exception ex)
            {
				logger.Exception (100,ex.Message);
                m_strLDAPErrorInfo = ex.Message;
				return ;
            }
        }
        private bool SyncUniqueLDAPUsersTomyVRMDB()
        {
            try
            {
                // check to see if the users are already there in the database.
                // if user records already exists then skip adding the user. 				
                // get all the user email list from the active,inactive,ldap tables				
                //Queue uniqueNewUserList = new Queue();
                if (m_objLldapUserList.Count > 0)
                    logger.Trace("Starting to insert LDAP users to myVRM DB Total LDAP users:" + m_iLDAPTotalUserCount);
                else
                    return true; // when there are no records in the queue, it is not an error, so returing true.

                NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
                NS_MESSENGER.LdapUser newUser = new NS_MESSENGER.LdapUser();

                bool bIsValidUser =  false;
                while (m_objLldapUserList.Count > 0)
                {
                    bIsValidUser =  false;

                    // Release the CPU for sometime.
                    Thread.Sleep(100);
                    newUser = (NS_MESSENGER.LdapUser)m_objLldapUserList.Dequeue();
                    
					// maneesh pujari 08/04/2008 FB 637.
                    // check if Login or email are not blank
                    if (   (newUser.login != null)
                        && (newUser.email != null) 
                        && (newUser.login.Length > 0) 
                        && (newUser.email.Length > 0) 
                        && (newUser.email.IndexOf("@") > 0))
                        bIsValidUser =  true;
                    
					// maneesh pujari 08/04/2008 FB 637.
                    // check if any user exists with given login and email, if yes then dont add the user
                    if ((bIsValidUser ==  true) && (!db.DoesUserExists(newUser.login, newUser.email)))
                    {
                        // found a unique user containing a unique email & login
                        if (!db.InsertLdapUser(newUser))
                        {
                            m_iTotalUserNotImported++;
                            logger.Trace("Error inserting user with Login: " + newUser.login + " and email: " + newUser.email + " to myVRM");
                            continue;
                        }
                        else
                            logger.Trace("User Login: " + newUser.login + " and email: " + newUser.email + " imported to myVRM successfully");

                    }
                    else
                        m_iTotalUserNotImported++;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }	
#if NOT_IN_USE
		internal bool TestConnection(NS_MESSENGER.LdapSettings ldapSettings)
		{
			try
			{
				logger.Trace ("Testing connection with ldap server...");

				// specify the ldap server info
				string ldapPath = "LDAP://" + ldapSettings.serverAddress + ":" + ldapSettings.connPort;
				logger.Trace ("Server : " + ldapPath);
				System.DirectoryServices.DirectoryEntry entry = new System.DirectoryServices.DirectoryEntry(ldapPath);
				entry.Username=ldapSettings.serverLogin.Trim();
				logger.Trace ("UserName : " + ldapSettings.serverLogin);
				entry.Password=ldapSettings.serverPassword.Trim();
				logger.Trace ("Password : " + ldapSettings.serverPassword);
				// set auth type to "None"
				entry.AuthenticationType = 0; 

				// specify the search string
				System.DirectoryServices.DirectorySearcher searcher = new System.DirectoryServices.DirectorySearcher(entry);
				//FORMAT-1 : (&(objectClass=person)(cn=Dev\John Doe)) 
				//FORMAT-2 : (&(objectClass=person)(cn=John Doe)) 
				//FORMAT-3 : (&(objectClass=person)(samaccountname=Dev2\john)) 
				// There could also be other formats however, the above 3 are the most common ones.
				//searcher.Filter = ldapSettings.searchFilter;								
				searcher.Filter = ("(&" + ldapSettings.searchFilter.Trim() + "(" + ldapSettings.loginKey +"="+ ldapSettings.serverLogin.Trim() + "))"); 				
				logger.Trace ("Search filter : " + searcher.Filter.Trim());
				
				// run the search. check the result count to see there is only one record.
				// if multiple records are returned then username entered should not be considered as valid.
				int resultCount = searcher.FindAll().Count;
				logger.Trace ("Search result count =" + resultCount.ToString()); 
				if (resultCount == 0)
				{
					this.errMsg = "Invalid input. No ldap records returned.";
					return false;
				}
				else
				{
					if (resultCount > 1)
					{
						this.errMsg = "Connection successful. However, multiple ldap records were returned.";
						return false;
					}
				}
				
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				logger.Exception (100,e.Message);
				return false;
			}			
		}
#endif
	}
}
