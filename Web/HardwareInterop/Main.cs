//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : Main.cs
 * DESCRIPTION : Main entry point for the assembly. 
 * AUTHOR : Kapil M
 */

//#define CONSOLE
namespace VRMRTC
{
    #region References
    using System;
    using System.Runtime.InteropServices;
    using System.Threading;
    using cryptography;
    #endregion


#if CONSOLE
	// RTC being used as a console app for testing ....
    class VRMRTC
    {
        private string errMsg = null;

        // Console app entry
        static void Main(string[] args)
        {
           

          //  string address = "123@1.2.3.4G123D...#,";
          //  int Gindex = address.IndexOf("G");
          //  int Dindex = address.IndexOf("D");
          //  string subaddress = address.Substring(Gindex + 1);
          //  Console.WriteLine(subaddress);

            string configPath = "C:\\VRMSchemas_v1.8.3\\VRMRTCConfig.xml";

            VRMRTC rtc = new VRMRTC();
            NS_MESSENGER.ConfigParams objConfigParams = new NS_MESSENGER.ConfigParams();
            NS_CONFIG.Config objConfig = new NS_CONFIG.Config();
            objConfig.Initialize(configPath, ref objConfigParams);
            NS_LOGGER.Log logger = new NS_LOGGER.Log(objConfigParams);
            logger.Trace("####### VRMRTC v1.9.1 ########");

/*
            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(objConfigParams);
            NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
            NS_MESSENGER.Party party = new NS_MESSENGER.Party();
            conf.cMcu.sIp = "64.47.35.148";
            conf.cMcu.sLogin = "vrm";
            conf.cMcu.sPwd = "pwd";
            party.cMcu.sIp = "64.47.35.148";
            party.cMcu.sLogin = "vrm";
            party.cMcu.sPwd = "pwd";
            party.sName = "paviraj507@gmail.com";
            codian.GetEndpointStatus(conf, ref party);
*/
            //NS_OPERATIONS.SendEmails sendEmails1 = new NS_OPERATIONS.SendEmails(objConfigParams);
            //sendEmails1.Send();

            //cryptography.Crypto crypto = new Crypto();
            //string key = crypto.decrypt("CkwYfj4KBDfNi12D+KC1WefbwzAuC//r1hcI2xNsl1nPuF2HWia5Vj5Nuaej8Enkiu87zPNTmh2nSgO538AQoJVRBGZu+Nxvx5kDtRsn2uoCHb81X/wNUyaZmwrMkcwAXcBmKpzMO5hl89Uwv1iduFBTxIeFDMSC4prUsBi+Wlac52MCTDRHpBL7f0IAm1O2y2/Fa87/tsrt1PSw/VSG45YCoyIQVquDgPkSWr3fvYNkw0KZuujdQQ0hlf9kcTiQmm24sEo5+h3TlVlbvlp8Rt0T3EXmLFuB7hv1Ucq34i1p7hML7WQX4HJbIepqaiMfLLYMzfYRgFSTHpN4YsvePlmxXCc3FKRwAmVpCyIUdvmQpiKSt9HnW2Ijflk0KavwKwbHTegUislCnVJcd+N7bxdsSLzxtPgpT5FYwO16CVVwQKsdjy0xZIZYm+RKWUg0hlN5/MLAMfuvWCuA87tzs3f4YEguJWlg1eV2mLTtjMM=");
            //Console.Write(key);
            string connectionStatusOutput = null;
            NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(objConfigParams);
            //telnet.Dial_PolycomCaller(24, "64.253.225.53", "admin", "12345", "64.47.35.148", NS_MESSENGER.LineRate.eLineRate.K384, NS_MESSENGER.Party.eProtocol.IP, NS_TELNET.Telnet.eTelnetOperation.CONNECTION_STATUS , true, "abc", ref connectionStatusOutput);
            //telnet.Dial_TandbergEdgeCaller(23, "64.253.253.161", "admin", "", "64.47.35.148", NS_MESSENGER.LineRate.eLineRate.K384, NS_MESSENGER.Party.eProtocol.IP, NS_TELNET.Telnet.eTelnetOperation.CONNECTION_STATUS, true, "abc", ref connectionStatusOutput);
            //Console.WriteLine(connectionStatusOutput);

            string getTerminalStatus = "<GetTerminalStatus>  <login>      <userID>11</userID>      <confID>232,1</confID>      <endpointID>21</endpointID>      <terminalType>2</terminalType>  </login></GetTerminalStatus>";
            //string testRMXMCUConnection = "<setBridge>  <userID>11</userID>  <bridge>      <bridgeID>1</bridgeID>      <name>Polycom MGC</name>      <login>MyVRM</login>      <password>My-VRM1</password>      <timeZone>26</timeZone>      <maxAudioCalls>1</maxAudioCalls>      <maxVideoCalls>1</maxVideoCalls>      <bridgeType>3</bridgeType>      <bridgeStatus>1</bridgeStatus>      <virtualBridge>0</virtualBridge>      <bridgeAdmin>          <ID>11</ID>      </bridgeAdmin>      <firmwareVersion>7.0</firmwareVersion>      <percentReservedPort>20</percentReservedPort>      <approvers>          <approver>              <ID></ID>          </approver>          <approver>              <ID></ID>          </approver>          <approver>              <ID></ID>          </approver>      </approvers>      <bridgeDetails>      <controlPortIPAddress>12.71.134.98</controlPortIPAddress>      <IPServices>          <IPService>              <SortID>1</SortID>              <name>YTC LAN</name>              <addressType>1</addressType>              <address>132.157.4.99</address>              <networkAccess>3</networkAccess>              <usage>3</usage>          </IPService>      </IPServices>      <ISDNServices>          <ISDNService>              <SortID>1</SortID>              <name>Jan_09</name>              <prefix>732</prefix>              <startRange>4607214</startRange>              <endRange>4607217</endRange>              <RangeSortOrder>0</RangeSortOrder>              <networkAccess>3</networkAccess>              <usage>3</usage>          </ISDNService>      </ISDNServices>      <MPIServices>      </MPIServices>      <MCUCards>          <MCUCard>              <ID>1</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>2</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>3</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>4</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>5</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>6</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>7</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>8</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>9</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>10</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>11</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>12</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>          <MCUCard>              <ID>13</ID>              <MaximumCalls>0</MaximumCalls>          </MCUCard>      </MCUCards>      </bridgeDetails>  <ISDNThresholdAlert>0</ISDNThresholdAlert>      <malfunctionAlert>0</malfunctionAlert>  </bridge></setBridge>";
            //string testRADMCUConnection = "<setBridge><userID>11</userID><bridge><bridgeID>1</bridgeID><name>Polycom RMX</name><login>myvrm</login><password>myvrm2009</password><timeZone>26</timeZone><maxAudioCalls>1</maxAudioCalls><maxVideoCalls>1</maxVideoCalls><bridgeType>8</bridgeType><bridgeStatus>1</bridgeStatus><virtualBridge>0</virtualBridge><bridgeAdmin><ID>18</ID></bridgeAdmin><firmwareVersion>2.02.25</firmwareVersion><percentReservedPort>20</percentReservedPort><approvers><approver><ID></ID></approver><approver><ID></ID></approver><approver><ID></ID></approver></approvers><bridgeDetails><controlPortIPAddress>12.188.16.230</controlPortIPAddress><IPServices><IPService><SortID>1</SortID><name>323</name><addressType>1</addressType><address>12.188.16.228</address><networkAccess>3</networkAccess><usage>3</usage></IPService></IPServices><ISDNServices></ISDNServices><MPIServices></MPIServices><MCUCards><MCUCard><ID>1</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>2</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>3</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>4</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>5</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>6</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>7</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>8</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>9</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>10</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>11</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>12</ID><MaximumCalls>0</MaximumCalls></MCUCard><MCUCard><ID>13</ID><MaximumCalls>0</MaximumCalls></MCUCard></MCUCards></bridgeDetails><ISDNThresholdAlert>0</ISDNThresholdAlert><malfunctionAlert>0</malfunctionAlert></bridge></setBridge>";
            //string testScopiaMCUConnection = "<setBridge><userID>11</userID><bridge><bridgeID>1</bridgeID><name>Rad Scopia</name><login>admin</login><password>123</password><timeZone>26</timeZone><maxAudioCalls>1</maxAudioCalls><maxVideoCalls>1</maxVideoCalls><bridgeType>9</bridgeType><bridgeStatus>1</bridgeStatus><virtualBridge>0</virtualBridge><bridgeAdmin><ID>11</ID></bridgeAdmin><firmwareVersion>5.x</firmwareVersion><percentReservedPort>20</percentReservedPort><approvers><approver><ID/></approver><approver><ID/></approver><approver><ID/></approver></approvers><bridgeDetails><controlPortIPAddress>209.90.209.233</controlPortIPAddress><IPServices><IPService><SortID>1</SortID><name>09821</name><addressType>2</addressType><address>09821</address><networkAccess>3</networkAccess><usage>3</usage></IPService></IPServices><ISDNServices /></bridgeDetails><ISDNThresholdAlert>0</ISDNThresholdAlert><malfunctionAlert>1</malfunctionAlert></bridge></setBridge>";
            //string testScopiaMCUConnection = "<setBridge><userID>11</userID><bridge><bridgeID>1</bridgeID><name>Rad Scopia</name><login>admin</login><password>123</password><timeZone>26</timeZone><maxAudioCalls>1</maxAudioCalls><maxVideoCalls>1</maxVideoCalls><bridgeType>9</bridgeType><bridgeStatus>1</bridgeStatus><virtualBridge>0</virtualBridge><bridgeAdmin><ID>11</ID></bridgeAdmin><firmwareVersion>5.x</firmwareVersion><percentReservedPort>20</percentReservedPort><approvers><approver><ID /></approver><approver><ID /></approver><approver><ID /></approver></approvers><bridgeDetails><controlPortIPAddress>209.90.209.233</controlPortIPAddress><IPServices /><ISDNServices /></bridgeDetails><ISDNThresholdAlert>0</ISDNThresholdAlert><malfunctionAlert>1</malfunctionAlert></bridge></setBridge>";
            //string inXML = "<login><userID>11</userID><confID>166,1</confID><endpointID>23</endpointID><terminalType>2</terminalType><mute>1</mute></login>" ;
            //string inXML = "<login><userID>11</userID><confInfo><confID>168,1</confID><retry>0</retry><extendEndTime>20</extendEndTime></confInfo></login>";
            //string inXML = "<login><userID>11</userID><confID>168,1</confID><endpointID>0</endpointID><terminalType>0</terminalType><displayLayout>02</displayLayout></login>";
            //string muteTerminal = "<login><userID>11</userID><confID>171,1</confID><endpointID>23</endpointID><terminalType>2</terminalType><mute>1</mute></login>";
            //string terminate = "<login><userID>11</userID><conferenceID>237,1</conferenceID></login>";
            //string codianMCUConnec = "<setBridge><userID>11</userID><bridge><bridgeID>4</bridgeID><name>Codian</name><login>vrm</login><password>pwd</password><timeZone>26</timeZone><maxAudioCalls>1</maxAudioCalls><maxVideoCalls>1</maxVideoCalls><bridgeType>4</bridgeType><bridgeStatus>1</bridgeStatus><virtualBridge>0</virtualBridge><bridgeAdmin><ID>11</ID></bridgeAdmin><firmwareVersion>2.x</firmwareVersion><percentReservedPort>20</percentReservedPort><approvers><approver><ID></ID></approver><approver><ID></ID></approver><approver><ID></ID></approver></approvers><bridgeDetails><portA>64.47.35.148</portA><portB></portB></bridgeDetails><ISDNThresholdAlert>0</ISDNThresholdAlert><malfunctionAlert>0</malfunctionAlert></bridge></setBridge>";
            //string testemail = "<login><userID>11</userID><AccountLogin></AccountLogin><AccountPwd></AccountPwd><ServerAddress>relay.mail.uu.net</ServerAddress><CompanyEmail>kapil@myvrm.com</CompanyEmail><DisplayName>VRM</DisplayName><ServerPort>25</ServerPort><SiteURL>http://cqa3/en/genlogin.aspx</SiteURL></login>";
            //string p2p = "<login>  <userID>11</userID>  <confID>206,1</confID>  <endpointID>34</endpointID>  <terminalType>2</terminalType>  <connectOrDisconnect>1</connectOrDisconnect></login>";
            //string testRMX = "<setBridge>  <userID>11</userID>  <bridge>      <bridgeID>5</bridgeID>      <name>RMX</name>      <login>myvrm</login>      <password>myvrm042009</password>      <timeZone>26</timeZone>      <maxAudioCalls>1</maxAudioCalls>      <maxVideoCalls>1</maxVideoCalls>      <bridgeType>8</bridgeType>      <bridgeStatus>1</bridgeStatus>      <virtualBridge>0</virtualBridge>      <bridgeAdmin>          <ID>11</ID>      </bridgeAdmin>      <firmwareVersion>3.xx</firmwareVersion>      <percentReservedPort>20</percentReservedPort>      <approvers>          <approver>              <ID></ID>          </approver>          <approver>              <ID></ID>          </approver>          <approver>              <ID></ID>          </approver>      </approvers>      <bridgeDetails>      <controlPortIPAddress>12.188.16.230</controlPortIPAddress>      <IPServices>      </IPServices>      <ISDNServices>      </ISDNServices>      <MPIServices>      </MPIServices>      <MCUCards>      </MCUCards>      </bridgeDetails>  <ISDNThresholdAlert>0</ISDNThresholdAlert>      <malfunctionAlert>0</malfunctionAlert>  </bridge></setBridge>";
            //string sendmessagetext = "<login>  <userID>12</userID>  <confID>24,1</confID>  <endpointID>19</endpointID>  <terminalType>2</terminalType>  <messageText>this is a test of myvrm</messageText></login>";
            //string p2pconfstatus = "<login>  <userID>11</userID>  <confID>18,1</confID>  <endpointID>13</endpointID>  <terminalType>2</terminalType></login>";
            //string testPSUCodian = "<setBridge>  <userID>11</userID>  <bridge>      <bridgeID>5</bridgeID>      <name>Codian</name>      <login>MYVRMstaging</login>      <password>echo351</password>      <timeZone>26</timeZone>      <maxAudioCalls>40</maxAudioCalls>      <maxVideoCalls>40</maxVideoCalls>      <bridgeType>5</bridgeType>      <bridgeStatus>1</bridgeStatus>      <virtualBridge>0</virtualBridge>      <bridgeAdmin>          <ID>190</ID>      </bridgeAdmin>      <firmwareVersion>2.x</firmwareVersion>      <percentReservedPort>20</percentReservedPort>      <approvers>          <approver>              <ID>190</ID>          </approver>          <approver>              <ID>40</ID>          </approver>          <approver>              <ID></ID>          </approver>      </approvers>      <bridgeDetails>          <portA>128.118.251.197</portA>          <portB></portB>      </bridgeDetails>  <ISDNThresholdAlert>0</ISDNThresholdAlert>      <malfunctionAlert>1</malfunctionAlert>  </bridge></setBridge>";
            string extendtime = "<login>  <userID>11</userID>  <confInfo>      <confID>182,1</confID>      <retry>0</retry>      <extendEndTime>30</extendEndTime>  </confInfo></login>";
            //NS_OPERATIONS.ConfSetup confSetup1 = new NS_OPERATIONS.ConfSetup(objConfigParams);
            //confSetup1.ConfsSetupOnMcu();   
            
            //NS_OPERATIONS.ConfMonitor confMonitor = new NS_OPERATIONS.ConfMonitor(objConfigParams);
            //confMonitor.TerminateP2PConfs(); 

            NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(objConfigParams);
            string outXML = null;
            string msg = null;
            //ops.ExtendConfEndTime(extendtime, ref outXML, ref msg);
            ops.GetTerminalStatus(getTerminalStatus, ref outXML, ref msg);

            //ops.ConnectDisconnectEndpoint(p2p, ref outXML, ref msg);
            //ops.GetP2PConfStatus(p2pconfstatus, ref outXML, ref msg);
            //ops.GetP2PConfStatus(sendmessagetext, ref outXML, ref msg);
            //ops.SendMessageToEndpoint(sendmessagetext, ref outXML, ref msg);
            //ops.ChangeDisplayLayout(inXML,ref outXML,ref msg);
           //ops.TestMCUConnection(testPSUCodian);
            //ops.MuteEndpoint(muteTerminal, ref outXML, ref msg);
            //ops.TestMCUConnection(codianMCUConnec);
            //ops.TerminateConference(terminate,ref outXML,ref msg);
            //ops.TestSMTPConnection(testemail, ref outXML);            

            //string pushConference = "<Conference><confID>2019,1</confID></Conference>";
            //NS_OPERATIONS.ConfSetup conf = new NS_OPERATIONS.ConfSetup(objConfigParams);
            //conf.AddPartyOnMcu();
            //conf.SetConferenceOnMcu(pushConference);


            //Trial_N_Err.Program scopia = new Trial_N_Err.Program();
            //scopia.Scopia(); 
            //NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(objConfigParams);
            //telnet.Dial_PolycomCaller(24, "64.253.253.167", "", "", "64.253.253.182", NS_MESSENGER.LineRate.eLineRate.K384, NS_MESSENGER.Party.eProtocol.IP);
                
            Console.WriteLine("Press \"Enter\" to quit application...");
            Console.ReadLine();
        }
    }
#else
    // RTC being used as a .NET/COM DLL.... 

    public interface IVRMRTC
    {
        string Test(string inXML);
        string Operations(string configPath, string operation, string inXML);
        string RepeatOps(string configPath);
        //string EmailOps (string configPath);
    }

    public class VRMRTC : IVRMRTC
    {
        private string errMsg = null;
        private NS_MESSENGER.ConfigParams configParams1;
        private NS_LOGGER.Log logger1;

        public VRMRTC()
        {
            //	constructor
        }


        // A test method to be used to check if component is installed correctly.
        public string Test(string inXML)
        {
            string ret = "Operation successful. Inside Test Method. Returning back inXML = " + inXML;
            return (ret);
        }


        private bool LoadConfigParams(string configPath, ref NS_MESSENGER.ConfigParams configParams)
        {
            try
            {
                // Config file intialization
                NS_CONFIG.Config config = new NS_CONFIG.Config();
                if (configPath.Length < 1)
                {
                    configPath = "C:\\VRMSchemas_v18\\VRMRTCConfig.xml";
                }

                bool ret = config.Initialize(configPath, ref configParams);
                if (!ret)
                {
                    this.errMsg = config.errMsg;
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Failure in reading configuration params. Contact myVRM Technical Support for assistance. Error = " + e.Message;
                return false;
            }
        }


        // Event-driven Operations - terminal control, mcu test connection etc...
        public string Operations(string configPath, string operation, string inXml)
        {
            string outXml = null;

            try
            {
                configPath = configPath.Trim();

                NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                bool ret = LoadConfigParams(configPath, ref configParams);
                if (!ret)
                {
                    string errorMsg = "<error>";
                    errorMsg += "<errorCode>100</errorCode>";
                    if (this.errMsg == null)
                    {
                        this.errMsg = "Failure in reading configuration params. Contact myVRM Technical Support for assistance.";
                    }
                    errorMsg += "<message>" + this.errMsg + "</message>";
                    errorMsg += "<level>E</level>";
                    errorMsg += "</error>";
                    return (errorMsg);
                }
                this.configParams1 = configParams;
                NS_LOGGER.Log logger = new NS_LOGGER.Log(configParams);

                logger.Trace("###################" + operation);
                logger.Trace("Operation : " + operation);
                logger.Trace("InXML : " + inXml);

                operation = operation.Trim();
                ret = false; string msg = null;
                NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(configParams);

                #region Operations
                switch (operation)
                {

                    case "SetTerminalControl":
                        {
                            ret = ops.ExtendConfEndTime(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }

                    case "DeleteTerminal":
                        {
                            ret = ops.TerminateEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "MuteTerminal":
                        {
                            ret = ops.MuteEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "MuteUnMuteParties": //FB 2441-(muteAllExcept & unMuteAll)
                        {
                            ret = ops.MuteUnMuteParties(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "ModifyTerminal": //FB 2249
                        {
                            ret = ops.ModifyEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "ConnectDisconnectTerminal":
                        {
                            ret = ops.ConnectDisconnectEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "SendMessageToEndpoint":
                        {
                            ret = ops.SendMessageToEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "SendMessageToConference":
                        {
                            ret = ops.SendMessageToConference(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    //case "GetP2PConfStatus": //Commented during Blue status project
                    //    {
                    //        ret = ops.GetP2PConfStatus(inXml, ref outXml, ref msg);
                    //        if (ret)
                    //        {
                    //            logger.Trace(msg);
                    //            return (msg);
                    //        }
                    //        else
                    //        {
                    //            msg = ops.errMsg;
                    //            logger.Trace(msg);
                    //        }
                    //        break;
                    //    }
                    case "GetTerminalStatus":
                        {
                            ret = ops.GetTerminalStatus(inXml, ref outXml, ref msg);
                            if (ret)
                                return (msg);
                            else
                                msg = ops.errMsg;
                            break;
                        }
                    case "DisplayTerminal":
                        {
                            ret = ops.ChangeDisplayLayout(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "TerminateConference":
                        {
                            ret = ops.TerminateConference(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "TestMCUConnection":
                        {
                            ret = ops.TestMCUConnection(inXml);
                            msg = ops.errMsg;
                            break;
                        }

                    case "TestMailConnection":
                        {
                            ret = ops.TestSMTPConnection(inXml, ref outXml);
                            msg = ops.errMsg;
                            break;
                        }
                    case "TestOrgEmail": // FB 1758
                        {
                            ret = ops.TestCompanyMail(inXml);
                            msg = ops.errMsg;
                            break;
                        }

                    case "TestExchangeConnection":
                        {
                            ret = ops.TestExchangeConnection(inXml, ref outXml);
                            msg = ops.errMsg;
                            break;
                        }

                    case "TestLDAPConnection":
                        {
                            ret = ops.TestLDAPConnection(inXml, ref outXml);
                            msg = ops.errMsg;
                            break;
                        }

                    case "SyncLdapNow":
                        {
                            logger.Trace("### Entering the SyncLdapNow ###");
                            ret = ops.SyncWithLdap();
                            msg = ops.errMsg;
                            break;
                        }
                    //FB 2462 START
                    case "SyncWithLdap":
                        {
                            logger.Trace("### Entering the SyncWithLdap ###");
                            ret = ops.SyncWithLdap();
                            msg = ops.errMsg;
                            break;
                        }
                    //FB 2462 END
                    case "GetConferenceAlerts":
                        {
                            ret = ops.GetConferenceAlerts(inXml, ref outXml, ref msg);
                            if (ret)
                            {
                                logger.Trace("OutXML = " + outXml);
                                return (outXml);
                            }
                            break;
                        }
                    case "LogEvent":
                        {
                            logger.Trace("InXML = " + inXml);
                            ret = ops.LogEvent(inXml);
                            msg = ops.errMsg;
                            this.errMsg = msg;
                            if (ret)
                            {
                                msg = "<success>Operation succesful.</success>";
                                logger.Trace("Msg = " + msg);
                                return (msg);
                            }
                            break;
                        }

                    case "ForceConfDelete":
                        {
                            logger.Trace("### Entering ForceConfDelete ###");
                            ret = ops.ForceConfDelete(inXml, ref outXml);
                            msg = ops.errMsg;
                            this.errMsg = msg;
                            if (ret)
                            {
                                logger.Trace("outXml = " + outXml);
                                return (outXml);
                            }
                            break;
                        }

                    case "GetHome":
                        {
                            logger.Trace("### Entering ldap auth user ###");
                            ret = ops.AuthLdapUser(inXml, ref outXml);
                            msg = ops.errMsg;
                            this.errMsg = msg;
                            if (ret)
                            {
                                logger.Trace("outXml = " + outXml);
                                return (outXml);
                            }

                            break;
                        }

                    case "SetConferenceOnMcu":
                        {
                            logger.Trace("### Entering SetConferenceOnMcu ###");
                            NS_OPERATIONS.ConfSetup confSetup = new NS_OPERATIONS.ConfSetup(configParams);
                            ret = confSetup.SetConferenceOnMcu(inXml);
                            msg = confSetup.errMsg;
                            break;
                        }

                    case "BulkLoadUsers":
                        {
                            logger.Trace("### Entering Bulk Load Users ###");
                            ret = ops.BulkLoadUsers(inXml);
                            msg = ops.errMsg;
                            break;
                        }
                    case "EmailTest":
                        {
                            logger.Trace("### Email test ###");
                            SendEmails();
                            break;
                        }

                    case "GetUserHistory":
                        {
                            logger.Trace("### Get User History ###");
                            ret = ops.BulkLoadUsers(inXml);
                            msg = ops.errMsg;
                            break;
                        }
                    case "SendEmails":
                        {
                            logger.Trace("### Send Emails ###");
                            SendEmails();
                            break;
                        }
                    case "TerminateCompletedP2PConfs":
                        {
                            logger.Trace("*** Entering Conf Monitor Sub-system ***");
                            NS_OPERATIONS.ConfMonitor confMonitor = new NS_OPERATIONS.ConfMonitor(this.configParams1);
                            confMonitor.TerminateP2PConfs();
                            confMonitor.TerminateCloudConfs();//Vidyo //FB 2599
                            confMonitor = null;
                            ret = true;
                            break;
                        }
                    /** FB 2261 **/
                    case "AddConferenceEndpoint":
                        {
                            logger.Trace("### Entering Add Endpoint to Conference ###");
                            NS_OPERATIONS.ConfSetup confSetup = new NS_OPERATIONS.ConfSetup(configParams);
                            ret = confSetup.AddConferenceEndpoint(inXml);
                            msg = confSetup.errMsg;
                            break;
                        }
                    /** FB 2363 **/
                    case "TriggerEventService":
                        {
                            logger.Trace("### Entering TriggerEventService ###");
                            NS_YorktelScheduling.YorktelScheduling YTCScheduling = new NS_YorktelScheduling.YorktelScheduling(this.configParams1);
                            ret = YTCScheduling.TriggerEvent();
                            msg = YTCScheduling.errMsg;
                            break;
                        }
                    case "TriggerEvent":
                        {
                            logger.Trace("### Entering TriggerEvent Web ###");
                            NS_YorktelScheduling.YorktelScheduling YTCScheduling = new NS_YorktelScheduling.YorktelScheduling(this.configParams1);
                            ret = YTCScheduling.TriggerEvent(inXml, ref outXml);
                            msg = YTCScheduling.errMsg;
                            break;
                        }
                    case "SetExternalScheduling":
                        {
                            logger.Trace("### Entering Insert event in table ###");
                            NS_YorktelScheduling.YorktelScheduling YTCScheduling = new NS_YorktelScheduling.YorktelScheduling(this.configParams1);
                            ret = YTCScheduling.InsertConferenceEventinmyVRM(inXml, ref outXml);
                            msg = YTCScheduling.errMsg;
                            break;
                        }
                    /** FB 2363 **/
                    //FB 2392 Start - WhyGO
                    case "GetLocationUpdate":
                        {
                            logger.Trace("### GetLocationUpdate from WhyGO ###");
                            NS_WhyGO.WhyGo WhyGoSched = new NS_WhyGO.WhyGo(this.configParams1);
                            if (WhyGoSched.ErrMsg.Trim() == "")
                                ret = WhyGoSched.GetLocationUpdate(ref inXml, ref outXml,false);

                            msg = WhyGoSched.ErrMsg;
                            break;
                        }
                    case "PushtoWhyGO":
                        {
                            logger.Trace("### PushtoWhyGO ###");
                            NS_WhyGO.WhyGo WhyGoSched = new NS_WhyGO.WhyGo(this.configParams1);
                            if (WhyGoSched.ErrMsg.Trim() == "")
                                ret = WhyGoSched.PushtoWhyGO(ref inXml, ref outXml);
                            msg = WhyGoSched.ErrMsg;
                            break;
                        }
                    case "GetPublicRoomsAvailability":
                        {
                            logger.Trace("### GetPublicRoomAvailability from WhyGO ###");
                            NS_WhyGO.WhyGo WhyGoSched = new NS_WhyGO.WhyGo(this.configParams1);
                            if (WhyGoSched.ErrMsg.Trim() == "")
                                ret = WhyGoSched.GetPublicRoomsAvailability(ref inXml, ref outXml);
                            msg = WhyGoSched.ErrMsg;
                            break;
                        }
                    case "GetPublicRoomsPrices":
                        {
                            logger.Trace("### GetPublicRoomAvailability from WhyGO ###");
                            NS_WhyGO.WhyGo WhyGoSched = new NS_WhyGO.WhyGo(this.configParams1);
                            if (WhyGoSched.ErrMsg.Trim() == "")
                                ret = WhyGoSched.GetPublicRoomsPrices(ref inXml, ref outXml);
                            msg = WhyGoSched.ErrMsg;
                            break;
                        }
                    case "CreateUserinWhyGo":
                        {
                            logger.Trace("### CreateUserinWhyGo###");
                            NS_WhyGO.WhyGo WhyGoSched = new NS_WhyGO.WhyGo(this.configParams1);
                            if (WhyGoSched.ErrMsg.Trim() == "")
                                ret = WhyGoSched.CreateUserinWhyGo(ref inXml, ref outXml);
                            msg = WhyGoSched.ErrMsg;
                            break;
                        }
                    case "GetLocationAvailability":
                        {
                            logger.Trace("### GetLocationAvailability from WhyGO ###");
                            NS_WhyGO.WhyGo WhyGoSched = new NS_WhyGO.WhyGo(this.configParams1);
                            if (WhyGoSched.ErrMsg.Trim() == "")
                                ret = WhyGoSched.GetLocationAvailability(ref inXml, ref outXml);
                            msg = WhyGoSched.ErrMsg;
                            break;
                        }
                    case "UpdateConferenceApproval":
                        {
                            logger.Trace("### GetLocationUpdate from WhyGO ###");
                            NS_WhyGO.WhyGo WhyGoSched = new NS_WhyGO.WhyGo(this.configParams1);
                            if (WhyGoSched.ErrMsg.Trim() == "")
                                ret = WhyGoSched.GetLocationUpdate(ref inXml, ref outXml, true);

                            msg = WhyGoSched.ErrMsg;
                            break;
                        }
                    //FB 2392 End - WhyGO
                    case "FetchConfMCUDetails": // FB 2448
                        {
                            ret = ops.FetchConfMCUDetails(inXml);
                            msg = ops.errMsg;
                            break;
                        }
                    //FB 2599 Start
                    case "PollVidyo": //FB Vidyo
                        {
                            logger.Trace("### Entering PollVidyo ###");
                            NS_Vidyo.Vidyo vidyoScheduling = new NS_Vidyo.Vidyo(this.configParams1);
                            ret = vidyoScheduling.PollVidyo(ref inXml, ref outXml);
                            msg = vidyoScheduling.errMsg;
                            break;
						}
                    //FB 2599 End
                    /** FB 2501 Call Monitoring Start **/
                    case "LockTerminal":
                        {
                            ret = ops.LockUnlockConference(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "MuteTransmitTerminal":
                        {
                            ret = ops.MuteTransmitEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "MuteVideoTransmitTerminal":
                        {
                            ret = ops.MuteTransmitVideoEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "MuteVideoReceiveTerminal":
                        {
                            ret = ops.MuteReceiveVideoEndpoint(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "CallDetailRecords":
                        {
                            ret = ops.CallDetailRecords(inXml);
                            msg = ops.errMsg;
                            break;
                        }
                    case "participantDiagnosticsResponse":
                        {
                            ret = ops.UpdateParticipantStatus(inXml);
                            msg = ops.errMsg;
                            break;
                        }
                    case "ParticipantFECC": //FECC Camera Control
                        {
                            ret = ops.ParticipantFECC(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "FetchMCUInfo": //FB 2501 Dec6
                        {
                            ret = ops.FetchMCUInfo(inXml, ref outXml);
                            msg = ops.errMsg;
                            break;
                        }
                    /** FB 2501 Call Monitoring End **/
                    case "GetMCUProfiles"://FB 2591
                        {
                            ret = ops.FetchMCUProfiles(inXml, ref outXml);
                            msg = ops.errMsg;
                            break;
                        }
                    case "SetSwitchingtoConf"://FB 2595
                        {
                            ret = ops.SetSwitchingtoConf(inXml, ref outXml);
                            msg = ops.errMsg;
                            break;
                        }
 					case "ConferenceRecording": //FB 2441
                        {
                            ret = ops.ConferenceRecording(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "CreateUserOnMcu": //FB 2709
                        {
                            logger.Trace("### Entering CreateUserOnMcu ###");
                            ret = ops.CreateUserOnMcu(inXml,ref outXml);
                            msg = ops.errMsg;
                            break;
                        }
					 case "TerminateSyncConference": //FB 2441 II
                        {
                            ret = ops.TerminateSyncConference(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
					 case "GetExtMCUSilo": //FB 2556
                        {
                            ret = ops.FetchExternalMCUSilo(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                     case "GetExtMCUServices": //FB 2556
                        {
                            ret = ops.FetchExternalMCUService(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
					case "SetLeaderParty": //FB 2553-RMX
                        {
                            ret = ops.SetLeaderParty(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    case "SetLectureMode": //FB 2553-RMX
                        {
                            ret = ops.SetLectureMode(inXml, ref outXml, ref msg);
                            msg = ops.errMsg;
                            break;
                        }
                    //ZD 100256 Start
                    case "TriggerSyncEventService": 
                        {
                            logger.Trace("### Entering TriggerEventService ###");
                            NS_Sync.Sync Sync = new NS_Sync.Sync(this.configParams1);
                            ret = Sync.TriggerEvent(inXml, ref outXml);
                            msg = Sync.errMsg;
                            break;
                        }
                    case "UpdateSyncEventService":
                        {
                            logger.Trace("### Entering UpdateSyncEventService ###");
                            NS_Sync.Sync Sync = new NS_Sync.Sync(this.configParams1);
                            ret = Sync.UpdateEvent(inXml, ref outXml);
                            msg = Sync.errMsg;
                            break;
                        }
                    case "FailureSyncEmail":
                        {
                            logger.Trace("### Entering FailureSyncEmail ###");
                            NS_Sync.Sync Sync = new NS_Sync.Sync(this.configParams1);
                            ret = Sync.FailureEvent();
                            msg = Sync.errMsg;
                            break;
                        }
                    //ZD 100256 End
#if CODE_COMMENTED_MAY_USE_IN_FUTURE
                    case "GenerateMcuResourceAllocationReport":
                    {
                        logger.Trace("### GenerateMcuResourceAllocationReport ###");                        
                        string reportOutXml = null;
                        ret = ops.RunMcuResourceAllocationReport(inXml,ref reportOutXml);
                        if (ret)
                        {
                            return (reportOutXml);
                        }
                        else
                        {
                            msg = ops.errMsg;
                        }
                        break;
                    }
#endif
                    default:
                        {
                            logger.Trace("Invalid command");
                            break;
                        }
                }
                #endregion

                if (ret)
                {
                    // success message returned
                    //if(outXml.Trim() == "")//FB 2392
                    if (String.IsNullOrEmpty(outXml))//FB 2558
                        outXml = "<success>Operation succesful.</success>";
                }
                else
                {
                    // error message to be returned back in a specific format. 
                    logger.Trace("Msg = " + msg);

                    if (msg != null)
                    {
                        // msg text over-ride (so that end user can understand it better)
                        if ((msg.IndexOf("transaction") >= 0) && (msg.IndexOf("expired") >= 0))
                        {
                            // msg = transaction expired
                            msg = "MCU not reachable.Operation was unsuccessful.";
                        }

                        if (msg.Length > 1)
                        {
                            outXml = logger.FetchComMsg(100, "E", msg);
                        }
                        else
                        {
                            outXml = logger.FetchComMsg(100, "E", "Operation unsuccesful.");
                        }
                    }
                    else
                    {
                        outXml = logger.FetchComMsg(100, "E", "Operation unsuccessful.");
                    }
                }

                logger.Trace("OutXML = " + outXml);
            }
            catch (Exception)
            {
                outXml = "<error><errorCode>100</errorCode><message>System exception thrown. Operation was unsuccessful. Please try again later or contact myVRM Technical Support for assistance.</message><level>E</level></error>";
            }
            return (outXml);
        }


        // Daemon Operations - repeat every 30-60 seconds
        public string RepeatOps(string configPath)
        {

            try
            {
    #region Load Config Params
                configPath = configPath.Trim();
                NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                bool ret = LoadConfigParams(configPath, ref configParams);
                if (!ret)
                {
                    string errorMsg = "<error>";
                    errorMsg += "<errorCode>100</errorCode>";
                    if (this.errMsg == null)
                    {
                        this.errMsg = "Failure in reading configuration params. Contact myVRM Technical Support for assistance.";
                    }
                    errorMsg += "<message>" + this.errMsg + "</message>";
                    errorMsg += "<level>E</level>";
                    errorMsg += "</error>";
                    return (errorMsg);
                }

                NS_LOGGER.Log logger = new NS_LOGGER.Log(configParams);
                logger.Trace("Log Level = " + configParams.logLevel.ToString());
                logger.Trace("*** Starting RepeatOps - " + DateTime.Now.ToString("f") + "***");

    #endregion

    # region Send Emails
                this.configParams1 = configParams;
                this.SendEmails();
    #endregion
                
    #region  Conference Operations
                this.ConfOps();
    #endregion

    #region Misc tasks
                try
                {
                    logger.Trace("### Misc Tasks ###");
                    NS_OPERATIONS.MiscTasks misc = new NS_OPERATIONS.MiscTasks(configParams);

                    // Delete pending confs
                    logger.Trace("### Misc Task - Delete Pending Confs ###");
                    misc.DeletePendingConfs();

                    // Shrink transaction log file
                    if (this.configParams1.bAutoRestrictDbLogFileGrowth)
                    {
                        logger.Trace("###Misc Task - Shrink transaction log file ###");
                        misc.ShrinkDbTransLogFile();
                    }

                    // Delete old log records
                    logger.Trace("### Misc Task - Delete old log records ###");
                    misc.DeleteLogRecords();

                    // Delete Polycom MCU Tokens
                    logger.Trace("### Misc Task - Delete Polycom MCU Tokens ###");
                    misc.DeletePolycomMcuTokens();
                  
                    // Delete unsent emails
                    logger.Trace("### Misc Task - Delete unsent emails ###");
                    misc.DeleteUnsentOldEmails();

                    // Auto sync ldap
                    logger.Trace("### Misc Task - Auto sync ldap ###");
                    misc.AutoSyncLdap();

                }
                catch (Exception em)
                {
                    logger.Exception(100, em.Message);
                }

    #endregion

                return ("success");
            }
            catch (Exception e)
            {
                return ("<error><errorCode>100</errorCode><message>Operation unsuccesful. Exception Message : " + e.Message + "</message><level>E</level></error>");
            }
        }

        public void SendEmails()
        {
            logger1 = new NS_LOGGER.Log(this.configParams1);
            logger1.Trace("*** Entering Send Email Sub-system ***");
            NS_OPERATIONS.SendEmails sendEmails1 = new NS_OPERATIONS.SendEmails(this.configParams1);
            sendEmails1.Send();

        }
        public void ConfOps()
        {
            logger1 = new NS_LOGGER.Log(this.configParams1);
            logger1.Trace("*** Entering Conf Setup Sub-system ***");
            NS_OPERATIONS.ConfSetup confSetup = new NS_OPERATIONS.ConfSetup(this.configParams1);
            confSetup.ConfsSetupOnMcu();            

            // Monitor confs on mcu
            logger1.Trace("*** Entering Conf Monitor Sub-system ***");
            NS_OPERATIONS.ConfMonitor confMonitor = new NS_OPERATIONS.ConfMonitor(this.configParams1);
            confMonitor.TerminateP2PConfs();
            // Disabled as polling is not required now. 
            // Endpoint status monitoring is in real-time on Manage page via "GetTerminalStatus" cmd.
            //confMonitor.MonitorConfsOnMcu(); 
        }
    }
#endif
}
