//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : Messenger.cs
 * DESCRIPTION : All messenger objects used in the assembly are defined here.
 * AUTHOR : Kapil M
 */
namespace NS_MESSENGER
{
    using System;
    using System.Collections;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;

    [Serializable] //FB 2441
    class MCU
    {
        internal enum eType { ACCORDv6, ACCORDv7, RMX, TANDBERG, RADVISION, CISCO, CODIAN, CMA, Lifesize, CISCOTP, RPRM, RADVISIONIVIEW,TMSScheduling  };//CMA FB 2261 FB 2501 Call Monitoring //FB 2441//FB 2556 //FB 2718
        internal string sName, sIp, sLogin, sPwd, sStation;
        internal string sToken, sUserToken;
        internal eType etType;
        internal int iHttpPort, iMaxConcurrentAudioCalls, iMaxConcurrentVideoCalls, iURLAccess;//ZD 100113
        internal string sSoftwareVer;
        internal double dblSoftwareVer;
        internal int iDbId, iTimezoneID;
        //FB 1642 - DTMF
        //internal string preConfCode, preLPin, postLPin; commented for FB 2655
        internal int iEnableIVR; //FB 1766
        internal string sIVRServiceName; //FB 1766
        internal int iEnableRecording, ConfServiceID ,iLPR; //FB 2016 //FB 1907
        internal string sisdnGateway; //MSE 8000
        internal string iISDNAudioPrefix;  //FB 2003
        internal string iISDNVideoPrefix;  //FB 2003
        internal int iDefaultLO, sMcuType,IViewOrgID;  //FB 2335 //FB 2448 //FB 2556
        internal string sDMADomain,sTemplateName,sDMALogin,sDMAip,sDMAPassword, sAdminName, sAdminEmail, sRPRMLogin, sRPRMEmailaddress,sRPRMDomain; // FB 2441 //FB 2709
        internal int iDMASendmail, iDMAHttpport,iSynchronous, iAdminID, ilogincount;// FB 2441 //FB 2709
        internal int iEnableCDR, iDeleteCDRDays, iOrgId;//FB 2660 //FB 2593
		internal string sDMADialinprefix = ""; //FB 2689

        //FB 2593 Starts
        public class CDRConference
        {
            public string Confid { get; set; }
            public string Instanceid { get; set; }
            public string Orgid { get; set; }
            public string Title { get; set; }
            public string ConfNumName { get; set; }
            public string McuID { get; set; }
            public string McuName { get; set; }
            public string McuTypeID { get; set; }
            public string McuType { get; set; }
            public string McuAddress { get; set; }
            public string McuConfGuid { get; set; }
            public string Host { get; set; }
            public string ScheduledStart { get; set; }
            public string ScheduledEnd { get; set; }
            public string Duration { get; set; }
            public string ConfTimeZone { get; set; }
            public string EventId { get; set; }
            public string EventType { get; set; }
            public string ConfGUID { get; set; }
            public string EventDate { get; set; }
        }
        public class CDRParty
        {
            public string confid { get; set; }
            public string InstanceId { get; set; }
            public string ConfNumName { get; set; }
            public string OrgID { get; set; }
            public string ConfGUID { get; set; }
            public string partyGuid { get; set; }
            public string EndpointName { get; set; }
            public string EndpointAddress { get; set; }
            public string EndpointType { get; set; }
            public string ConnectionType { get; set; }
            public string EndpointDateTime { get; set; }
            public string EndpointConnectStatus { get; set; }
            public string DisconnectReason { get; set; }
            public string EventId { get; set; }
            public string ConfTimeZone { get; set; }
        }
        //FB 2593

        //FB 2683 Start

        public class PolycomRPRMCDR
        {
            public string Calluuid { get; set; }
            public string confuuid { get; set; }
            public string Version { get; set; }
            public string conf_Type { get; set; }
            public string conf_cluster { get; set; }
            public DateTime conf_startTime { get; set; }
            public string conf_Endtime { get; set; }
            public string conf_userID { get; set; }
            public string conf_roomid { get; set; }
            public string conf_Partycount { get; set; }
            public string classofservice { get; set; }
            public string UserDataA { get; set; }
            public string UserDataB { get; set; }
            public string UserDataC { get; set; }
            public string UserDataD { get; set; }
            public string UserDataE { get; set; }
            public string ept_type { get; set; }
            public string ept_callType { get; set; }
            public string ept_dialin { get; set; }
            public string ept_starttime { get; set; }
            public string ept_endtime { get; set; }
            public string ept_orgendpoint { get; set; }
            public string ept_dialstring { get; set; }
            public string ept_destEndpoint { get; set; }
            public string ept_origSignalType { get; set; }
            public string ept_destSignalType { get; set; }
            public string ept_lastForwardEndpoint { get; set; }
            public string ept_cause { get; set; }
            public string ept_causeSource { get; set; }
            public string ept_bitRate { get; set; }
            public string ept_classOfService { get; set; }
            public string ept_ingressCluster { get; set; }
            public string ept_egressCluster { get; set; }
            public string ept_VMRCluster { get; set; }
            public string ept_VEQCluster { get; set; }
            public string ept_userRole { get; set; }

        }

        //FB 2683 End

        internal MCU()
        {
            sName = sIp = sLogin = sPwd = sStation = sToken = sUserToken = sSoftwareVer = null;
            iHttpPort = 80; // default to port 80
            iTimezoneID = 26; // default to Eastern Time 
            iMaxConcurrentAudioCalls = iMaxConcurrentVideoCalls = 0;
            //FB 1642 - DTMF
            //preConfCode = preLPin = postLPin = null; commented for FB 2655
            iEnableIVR = 0; //default to 0-diabled FB 1766
            sIVRServiceName = null; //FB 1766
            iEnableRecording = 0; //default to 0-false FB 1907
            sisdnGateway = "";// MSE 8000
            iISDNAudioPrefix = iISDNVideoPrefix = ""; //FB 2003
            ConfServiceID = 71; //FB 2016
			iLPR = 0; // FB 1907
            iDefaultLO = -1;  //FB 2335

        }
    }
    //FB 2591
    class MCUProfile
    {
        internal int iId;
        internal string sName, sDisplayname;
    }
    [Serializable] //FB 2441

    //FB 2659 - Starts
    class ExtMCUSilo
    {
        internal int iBridgeId, iOrgID, iBridgeTypeId;
        internal string sOrgName, sAlias, sBillingCode;
    }
    [Serializable]
    class ExtMCUService
    {
        internal int iBridgeId, iBridgeTypeId, iMemberId, iServiceId;
        internal string sServiceName, sDescription, sPrefix;
    }
    [Serializable]
    //FB 2659 - End

    class Conference
    {
        // Used for storing conf info.
        [Serializable] //FB 2441
        internal struct sStatus
        {
            internal int iMcuID; //bridge-specific id (to be retreived from the bridge.)
            internal bool bIsEmpty, bIsResourceDeficient;
            internal int iTotalConnectedParties;
        };
        [Serializable] //FB 2441
        internal struct sColorCode
        {
            internal int iRed; //red code
            internal int iGreen; //green code
            internal int iBlue; //blue code
        };
        internal string sDbName, sPwd, sMcuName, sExternalName;
        internal int iDbNumName, iuniqueId; //FB 2797

        //internal enum eLayout {_1x1,_1x2,_2x1,_2x2,_1and5,_3x3};
        internal enum eMediaType { AUDIO, AUDIO_VIDEO, AUDIO_VIDEO_DATA };
        internal enum eVideoSession { SWITCHING, TRANSCODING, CONTINOUS_PRESENCE };
        internal enum eNetwork { IP, ISDN };
        //internal enum eLineRate {K56,K64,K2x56,K2x64,K128,K256,K384,K512,K768,M1152,M1472,M1536,M1920};
        internal enum eAudioCodec { AUTO, G728, G711_56, G722_24, G722_32, G722_56 };
        internal enum eVideoCodec { AUTO, H261, H263, H264 };
        internal enum eType { POINT_TO_POINT, MULTI_POINT, ROOM_CONFERENCE };
        internal enum eMsgServiceType { NONE, WELCOME_ONLY, ATTENDED, IVR };
        internal enum eStatus { SCHEDULED, PENDING, TERMINATED, ONGOING, OnMCU, COMPLETED, DELETED };
        internal int iDbID, iMcuID, iPwd;
        internal int iInstanceID;
        internal Queue qParties;
        internal MCU cMcu; //bridge info
        internal sStatus stStatus;
        internal eMediaType etMediaType;
        //internal eLineRate etLineRate ;
        internal LineRate stLineRate;

        internal eVideoSession etVideoSession;
        internal eAudioCodec etAudioCodec;
        internal eVideoCodec etVideoCodec;
        //internal eLayout etVideoLayout; 
        internal eNetwork etNetwork;
        internal DateTime dtStartDateTime, dtStartDateTimeInUTC, dtStartDateTimeInLocalTZ,
                          dtSetUpDateTime,dtEndDateTime, dtModifiedDateTime;//FB 2363s
        internal string sName_StartDateTimeInLocalTZ;
        internal int iDuration; // in minutes
        internal bool bLectureMode; internal string sLecturer;
        internal bool bMessageOverlay;//FB 2486
        internal bool bAutoTerminate;
        internal bool bH239; //dual stream mode
        internal bool bConferenceOnPort;
        internal bool bEncryption;
        internal bool bEntryNotification;
        internal bool bEndTimeNotification;
        internal bool bExitNotification;
        internal eMsgServiceType etMessageServiceType;
        internal string sMessageServiceName;
        internal sColorCode stLayoutBorderColor;
        internal sColorCode stSpeakerNotation;
        internal int iVideoLayout;
        internal int iMaxAudioPorts;
        internal int iMaxVideoPorts;
        internal int iTimezoneID;
        internal eType etType;
        internal int iHostUserID;
        internal string sHostEmail, sDescription;
        internal bool isSingleDialIn, isVIP; //FB 2363s
        internal bool bEntryQueueAccess;
        internal eStatus etStatus;
        internal string sHostName, ESType, ESId, sMeetandGreet, sDedicatedvnoc, sAvtech; //FB 2363s
        internal int iOrgID;//FB 2335
        internal int iImmediate;//FB 2440
        internal int iIsVMR;//FB 2447
        internal string sInternalBridgeNumber;//FB 2447
        internal string sExternalBridgeNumber;//FB 2447
        //FB 2501 Call Monitoring Start
        internal int ilockorunlock, iMessDuration, iMessPosition;
        internal string sMessage = "", sGUID = "";
        internal int PortNumber, iConfRMXServiceID; //FB 2839
        internal string ServerAddress;
        //FB 2501 Call Monitoring End
        internal bool bE164, bH323;//FB 2636
        internal int iPolycomSendMail, iRPRM, iRecurring,iPushtoExternal,iRPRMUserID, iAccessType, iStatus;//FB 2636 //FB 2441 //FB 2709 //FB 2556T //FB 2710
        internal string sPolycomTemplate, sDialString, sEtag, sRPRMLogin, sRPRMDomain, sDialinNumber;// FB 2441 //FB 2709 //FB 2659
		internal string IViewNumber;//FB 2556
        internal int iEndpointCount; //FB 2556
        internal int iMcuSetupTime, iMCUTeardonwnTime; //FB 2998
        internal Conference()
        {
            // constructor
            sDbName = sPwd = sMcuName = sDescription = sLecturer = null;
            qParties = new Queue();
            cMcu = new MCU();
            iDbID = iInstanceID = iMcuID = iPwd = iVideoLayout = iDuration = 0;
            iMaxAudioPorts = iMaxVideoPorts = 0;
            etType = 0; iTimezoneID = 26;
            bLectureMode = bAutoTerminate = bH239 = bConferenceOnPort = bEncryption = bEntryNotification = bEndTimeNotification = bExitNotification = false;
            stLayoutBorderColor.iBlue = stLayoutBorderColor.iGreen = stLayoutBorderColor.iRed = 0;
            stSpeakerNotation.iBlue = stSpeakerNotation.iGreen = stSpeakerNotation.iRed = 0;
            stStatus.bIsEmpty = stStatus.bIsResourceDeficient = isSingleDialIn = bEntryQueueAccess = bLectureMode = false;
            stStatus.iMcuID = stStatus.iTotalConnectedParties = 0;
            iIsVMR = 0;//FB 2447
            sInternalBridgeNumber = sExternalBridgeNumber = "";//FB 2447
        }


        internal void CopyTo(ref NS_MESSENGER.Conference newConf)
        {
            newConf.iDbID = this.iDbID;
            newConf.iInstanceID = this.iInstanceID;
            newConf.sDbName = this.sDbName;
            newConf.iMcuID = this.iMcuID;
            newConf.iDbNumName = this.iDbNumName;
            newConf.dtStartDateTime = this.dtStartDateTime;
            newConf.dtStartDateTimeInUTC = this.dtStartDateTimeInUTC;
            newConf.sDialinNumber = this.sDialinNumber;//FB 2636//FB 2659
            //newConf.etVideoLayout = this.etVideoLayout;
            newConf.etVideoSession = this.etVideoSession;
            newConf.etMediaType = this.etMediaType;
            newConf.etNetwork = this.etNetwork;
            newConf.sMcuName = this.sMcuName;
            newConf.stLineRate = this.stLineRate;
            newConf.bLectureMode = this.bLectureMode;
            newConf.iDuration = this.iDuration;
            newConf.bAutoTerminate = this.bAutoTerminate;
            newConf.bH239 = this.bH239;
            newConf.bConferenceOnPort = this.bConferenceOnPort;
            newConf.bEncryption = this.bEncryption;
            newConf.iVideoLayout = this.iVideoLayout;

            newConf.cMcu.iDbId = this.cMcu.iDbId;
            newConf.cMcu.sIp = this.cMcu.sIp;
            newConf.cMcu.sLogin = this.cMcu.sLogin;
            newConf.cMcu.sName = this.cMcu.sName;
            newConf.cMcu.sPwd = this.cMcu.sPwd;
            newConf.cMcu.sStation = this.cMcu.sStation;
            newConf.cMcu.etType = this.cMcu.etType;
            newConf.cMcu.dblSoftwareVer = this.cMcu.dblSoftwareVer;
            newConf.cMcu.iTimezoneID = this.cMcu.iTimezoneID;
            newConf.cMcu.iHttpPort = this.cMcu.iHttpPort;
            
            newConf.isSingleDialIn = this.isSingleDialIn;
            newConf.sPwd = this.sPwd;
            newConf.iPwd = this.iPwd;
            newConf.bEntryQueueAccess = this.bEntryQueueAccess;

            //FB 1642 - DTMF Commented for FB 2655
            //newConf.cMcu.postLPin = this.cMcu.postLPin;
            //newConf.cMcu.preLPin = this.cMcu.preLPin;
            //newConf.cMcu.preConfCode = this.cMcu.preConfCode;

            //FB 2636 
            newConf.bH323 = this.bH323;
            newConf.bE164 = this.bE164;

        }


    }
    [Serializable] //FB 2441
    struct LineRate
    {
        internal enum eLineRate { K56, K64, K2x56, K2x64, K128, K192, K256,K320, K384, K512, K768, M1024, M1152,M1250, M1472, M1536, M1792, M1920, M2048,M2560,M3072,M3584, M4096 };
        internal eLineRate etLineRate;
    }
    [Serializable] //FB 2441
    class Party
    {
        // Used for storing party info.
        internal enum eProtocol { IP, ISDN, MPI, SIP }; //FB 2390
        internal enum eConnectionType { DIAL_IN = 1, DIAL_OUT, DIRECT };
        internal enum eOngoingStatus { DIS_CONNECT, PARTIAL_CONNECT, FULL_CONNECT, ONLINE, UNREACHABLE }; //Blue Status
        internal enum eType { USER, ROOM, GUEST, CASCADE_LINK,VMR };//FB 2447
        //internal enum eLineRate {K56,K64,K2x56,K2x64,K128,K256,K384,K512,K768,M1024,M1472};
        internal enum eCascadeRole { MASTER, SLAVE };
        internal enum eVideoProtocol { AUTO, H261, H263, H264 };
        internal enum eCallType { AUDIO, VIDEO };
        internal enum eVideoEquipment { ENDPOINT, RECORDER };
        internal enum eModelType { POLYCOM_VSX, POLYCOM_HDX, TANDBERG, CODIAN_VCR, OTHER, POLYCOM_OTX, POLYCOM_RPX, CISCO, MXP, ViewStation }; //FB 2335 //FB 2390
        internal enum eAddressType { IP_ADDRESS, H323_ID, E_164, ISDN_PHONE_NUMBER, MPI, SIP};//FB 2390
        internal enum eCallerCalleeType { CALLER, CALLEE };
        //internal enum eLineRate { K56, K64, K2x56, K2x64, K128, K256, K384, K512, K768, M1152, M1472, M1536, M1920 };
        internal LineRate stLineRate;
        internal eCallerCalleeType etCallerCallee;
        internal string sMcuName, sName;
        internal int iMcuId, iDbId,iAPIPortNo,iconfnumname,ip2pCallid = -1;//Code added for API Port//FB 2390
        internal string sAddress, sMcuAddress, sGatewayAddress;	 //ip or isdn address	
        internal string sNetworkURL;//FB 2595
        internal int iSecureport;//FB 2595
        //internal eLineRate elrMcuLineRate;
        internal eProtocol etProtocol;
        internal eConnectionType etConnType;
        internal eOngoingStatus etStatus;
        internal eType etType;
        internal MCU cMcu;
        internal eVideoProtocol etVideoProtocol;
        internal bool bMute;
        internal string sDisplayLayout;
        internal eCascadeRole etCascadeRole;
        internal eCallType etCallType;
        internal eVideoEquipment etVideoEquipment;
        internal eModelType etModelType;
        internal eAddressType etAddressType;

        internal bool bIsOutsideNetwork;
        internal string sGateKeeeperAddress = "";//ZD 100132
        internal string sMcuServiceName;
        internal string sLogin, sPwd;
        internal string sConfNameOnMcu;
        internal string sDTMFSequence;
        //internal eLineRate etLineRate;
        internal bool bIsLecturer;
        internal bool bIsMessageOverlay;//FB 2486
        //Polycom CMA Start
        internal bool isRoom,isTelePresence;//FB 2400
        internal string Emailaddress = "",MulticodecAddress="";//FB 2400
        //FB 2501 Call Monitoring Start
        internal string sFECCdirection = "", sMessagedirection = "", sMessage = "", sGUID = "";
        internal int iMessDuration = 0, iAudioMode = 0, iAudioGained = 0, iMessPosition;
        internal String sRxAudioPacketsReceived = "", sRxAudioPacketErrors = "", sRxAudioPacketsMissing = "";
        internal String sRxVideoPacketsReceived = "", sRxVideoPacketErrors = "", sRxVideoPacketsMissing = "";
        internal bool bMuteRxaudio, bMuteTxaudio, bMuteRxvideo, bMuteTxvideo, bSetFocus;
        internal String sStream = "";
        //FB 2501 Call Monitoring End
        //FB 2501 P2P Call Monitoring Start
        internal Int32 iTerminalType = 0;
        //FB 2501 P2P Call Monitoring End
        //Polycom CMA End
        internal string preConfCode, preLPin, postLPin, AudioDialInPrefix; // FB 2655
        internal bool bAudioBridgeParty;// FB 2655
        internal int iEndpointId; //FB 2989 Note: iEndpointId is Table id. iDbId is adding 1000 and 2000. 
        internal Party()
        {
            cMcu = new MCU();
            bMute = false;
            sLogin = sPwd = null;
            isRoom = false;//Polycom CMA
            preConfCode = preLPin = postLPin = AudioDialInPrefix = ""; //FB 2655
            sStream = "";//FB 2640
        }
    }


    class Email
    {
        // Used for storing a single email info.		
        internal string From, To, CC, BCC, Subject, Body, Attachment;
        internal int RetryCount, UUID, isCalender;//ICAL Fixes;
        internal int orgID; //Added for FB 1710
        internal bool isHTML;
        internal DateTime LastRetryDateTime;
        internal Email()
        {
            From = To = CC = BCC = Subject = Body = null;
            RetryCount =UUID=isCalender = 0;//ICAL Fixes
            isHTML = true;
        }
    }


    class Smtp
    {
        // Used for storing smtp server info.
        internal int PortNumber, ConnectionTimeOut , RetryCount; // in millisecs //FB 2552
        internal string ServerAddress, Login, Password, CompanyMailAddress, DisplayName, FooterMessage, WebsiteURL;//Vidyo //FB 2599
        internal Smtp()
        {
            PortNumber = 25; ConnectionTimeOut = 10000;
            ServerAddress = null; Login = null; Password = null; CompanyMailAddress = null; WebsiteURL = "";//Vidyo //FB 2599
        }
    }


    class AdminSettings
    {
        internal int delta;
        internal bool autoTerminate;
        internal bool allowDialOut;
        internal bool allowP2P;
        internal int retries;
        internal AdminSettings()
        {
            delta = 60;
            autoTerminate = false;
            allowDialOut = true;
            allowP2P = false;
            retries = 0;
        }
    }

    class ConfigParams
    {
        internal enum eDatabaseType { SQLSERVER, ORACLE, MYSQL, DB2 };
        internal enum eLogLevel { SYSTEM_ERROR, USER_ERROR, WARNING, INFO, DEBUG };

        // FB case# 582
        internal struct sAutoTerminateCall
        {
            internal bool On;
            internal int Time_Before_First_Join;
            internal int Time_After_Last_Quit;
        }
        internal sAutoTerminateCall stAutoTerminateCall;

        internal string localConfigPath, globalConfigPath, logFilePath, databaseLogin, databaseServer, databasePwd, databaseName, client, rootFilePath, ldapLoginKey, scheduledReportsAspxUrl, trustedConnection; //FB 2700
        internal DateTime ldapCycleTime;
        internal eDatabaseType databaseType;
        internal bool fileTraceEnabled, ldapEnabled, bAutoRestrictDbLogFileGrowth;
        internal eLogLevel logLevel;
        internal int logKeepDays, databaseConTimeout;
        internal string mailLogoPath; //FB 1658 - Embedded Image
        //internal string audioDialNoPrefix = ""; //Disney New Audio Add on Request Commented for FB 2655

        internal struct sDTMF_MGC
        {
            internal string PreConfCode;
            internal string PreLeaderPin;
            internal string PostLeaderPin;
        }
        internal sDTMF_MGC stDTMF_MGC;

        internal struct sDTMF_Codian
        {
            internal string PreConfCode;
            internal string PreLeaderPin;
            internal string PostLeaderPin;
        }
        internal sDTMF_Codian stDTMF_Codian;

        internal ConfigParams()
        {
            fileTraceEnabled = ldapEnabled = bAutoRestrictDbLogFileGrowth = false;
            rootFilePath = null;
            ldapLoginKey = "cn";
            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 03, 00, 00);
            ldapCycleTime = dt;
            logKeepDays = 0;
            databaseConTimeout = 10;
            trustedConnection = "";//FB 2700
            // FB case #582
            stAutoTerminateCall.On = false; stAutoTerminateCall.Time_After_Last_Quit = 0; stAutoTerminateCall.Time_Before_First_Join = 0;

            // Fb case #1632
            stDTMF_MGC.PostLeaderPin = stDTMF_MGC.PreConfCode = stDTMF_MGC.PreLeaderPin = stDTMF_Codian.PostLeaderPin = stDTMF_Codian.PreConfCode = stDTMF_Codian.PreLeaderPin = null;
        }
    }

    class LdapSettings
    {

        internal string serverAddress, serverLogin, serverPassword, loginKey, searchFilter, domainPrefix, scheduleDays;
        internal int connPort, sessionTimeout, AuthenticationType;//FB 2993 LDAP
        internal DateTime scheduleTime, syncTime;

        internal LdapSettings()
        {
            serverAddress = serverLogin = serverPassword = searchFilter = loginKey = domainPrefix = null;
            connPort = 389;
            sessionTimeout = 20;
        }
    }

    class LDAP
    {

        internal string serverAddress, serverLogin, serverPassword, domainPrefix;
        internal int connPort, sessionTimeout, scheduleInterval;
        internal LDAP()
        {
            serverAddress = serverLogin = serverPassword = domainPrefix = null;
            connPort = 389;
            sessionTimeout = 20;
            scheduleInterval = 1440; // 1 day
        }
    }

    class LdapUser
    {
        internal int userid;
        internal string firstName, lastName, email, telephone, login;
        internal DateTime whenCreated;

        internal LdapUser()
        {
            userid = 0;
            firstName = lastName = email = telephone = login = null;
            whenCreated = DateTime.Now;
        }
    }

    class Alert
    {
        internal int confID;
        internal int instanceID;
        internal string message;
        internal DateTime timestamp;
        internal int typeID;

        internal Alert()
        {
            confID = instanceID = typeID = 0;
        }
    }

    class WorkOrder
    {
        internal enum eType { AUDIO_VISUAL, CATERING, HOUSEKEEPING, GENERIC };
        internal int iCategoryType, iAdminUserID, iConfID, iInstanceID, iRoomID;
        internal eType etType;
        internal string sName, sComments;
        internal DateTime dtCompletedBy;
        internal WorkOrder()
        {
        }
    }

    class ESSettings //FB 2363s
    {
        internal string sCustomerName, sCustomerID, sPartnerName, sPartnerEmail;
        internal string sPartnerURL, sUserName, sPassword;
        internal DateTime dModifiedDate;
        internal string sESType;
        internal Int32 sTimeout;

    }
    class sysExtSchedEvent 
    {
        internal string sRequestID, sTypeID, sCustomerID, sStatus;
        internal string sStatusMessage, sRequestCall, sResponseCall,sConfnumname;
        internal string sEventTrigger;
        internal DateTime dEventTime, dStatusDate;
        internal int sRetryCount, sOrgID;
	}
	//FB 2392 - Starts
    class timeZoneDetails
    {       
        internal int iTimeZoneID,iDST;
        internal double iOffset;
    }

    class SysSettings
    {
        internal string WhyGoURL, WhyGoUserName, WhyGoPassword, SyncAddress, SyncEmailAddress;//ZD 100256
        internal int WhygoUserID = 1333, SyncPort, SyncConfig;//ZD 100256
    }
	//FB 2392-End
    class CDREvent
    {
        internal String sConferenceGUID, sParticipantGUID, sEventType, sConfTime, sConfName, sUri, sPinProtected;
        internal String sCallID, sCallDirection, sCallProtocol, sEndpointIPAddress, sEndpointDisplayName,
        sEndpointURI, sEndpointConfiguredName, sTimeInConference, sDisconnectReason,
        sMaxSimultaneousAVParticipants, sMaxSimultaneousAudioOnlyParticipants, sName, sActiveTime, sEncryptedTime,
        sTime, sMessage, sScheduledTime, //FB 2593
        //FB 2797 Start
        sbillingCode, sownername, sScheduleddate, sScheduleddurationinminutes, sDirection, sdn, sh323Alias,
        sEndpointDirection, sMediaEncryptionStatus, sMediaFromResolution, sMediaFromVideoCodec, sMediaFromaudioCodec,
        sMediaToresolution, sMediaTovideoCodec, sMediaToaudioCodec, sregisteredWithGatekeeper;
        //FB 2797 End
        internal DateTime dTime;
        internal int iBridgeId, iBridgeType,iNumericId, iMaxSimultaneousAudioVideoParticipants, iMaxSimultaneousAudioOnlyParticipants, iIndex,
        iTotalAudioVideoParticipants, iTotalAudioOnlyParticipants, iSessionDuration, iMaxBandwidth, iBandwidth, iPacketsReceived, iPacketsLost, iWidth, iHeight,
        iDuration, //FB 2593
         //FB 2797 Start
        iConfnumname, iuniqueId, iparticipantId, itimeInConferenceInMinutes, iMediaFrombandwidth, iMediaTobandwidth, iTotalstreamingParticipantsAllowed, imaxSimultaneousStreaming,
        iAudioVideoParticipants, iTimeInConference, iAudioOnlyParticipants, istreamingParticipantsAllowed, iduration, idurationInMinutes;
        //FB 2797 End
        internal Conference confDetails = null;
        internal enum EptConnectStatus { Connected = 1, Disconnected = 2 };

        public class VMRCDR
        {
            public string MCUConfGUID { get; set; }
            public string VMRAddress { get; set; }
            public string StartDate { get; set; }
            public string TotalPortMinActual { get; set; }
            public string TotalMinActual { get; set; }
            public string MCUid { get; set; }
            public string MCUname { get; set; }
            public string MCUtype { get; set; }
            public string MCUAddress { get; set; }
            public string EventId { get; set; }
            public string OrgId { get; set; }
            public string BridgeTypeID { get; set; } //FB 2593 VMR CDR
            
        }

        internal CDREvent()
        {
            // empty all variables
            sConferenceGUID = " ";
            sParticipantGUID = " ";
            sEventType = " ";
            sConfTime = " ";
            sConfName = " ";
            sUri = " ";
            sPinProtected = " ";
            sCallID = " ";
            sCallDirection = " ";
            sCallProtocol = " ";
            sEndpointIPAddress = " ";
            sEndpointDisplayName = " ";
            sEndpointURI = " ";
            sEndpointConfiguredName = " ";
            sTimeInConference = " ";
            sDisconnectReason = " ";
            sMaxSimultaneousAVParticipants = " ";
            sMaxSimultaneousAudioOnlyParticipants = " ";
            sName = " ";
            sActiveTime = " ";
            sEncryptedTime = " ";
            sTime = " ";
            sMessage = " ";

            iBridgeId = 0;
            iBridgeType = 0;
            iNumericId = 0;
            iMaxSimultaneousAudioVideoParticipants = 0;
            iMaxSimultaneousAudioOnlyParticipants = 0;
            iIndex = 0;
            iTotalAudioVideoParticipants = 0;
            iTotalAudioOnlyParticipants = 0;
            iSessionDuration = 0;
            iMaxBandwidth = 0;
            iBandwidth = 0;
            iPacketsReceived = 0;
            iPacketsLost = 0;
            iWidth = 0;
            iHeight = 0;
        }

    }
    /**  FB 2599 - Start **/
    class VidyoSettings //FB 2262
    {
        internal string sProxyAddress, sPort;
        internal string sPartnerURL, sUserName, sPassword;
        internal DateTime dModifiedDate;
        internal enum callType { VidyoPortalAdminService, VidyoPortalUserService};
        internal String[] namespaceType = new String[2] { "http://portal.vidyo.com/admin/v1_1", "http://portal.vidyo.com/user/v1_1" };

    }
    /**  FB 2599 - End **/
    //FB 2441 Starts
    class ObjectHelper
    {
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

    }
	//FB 2441 End
    //FB 2556 Start
    public enum AccessMode
    {
        FromService = 1, FromWebsite = 0
    };
    //FB 2556 End
}
namespace NS_MESSENGER_CUSTOMIZATIONS
{
    using System;
    using System.Collections;
    class Room
    {
        internal int roomID;
        internal int tier2ID;
        internal int tier1ID;
        internal string name;
        internal string filePath;
        internal string topTierName, timezoneID_Name;
        //2329 start -WhyGo 
        internal Decimal EHCost, AHCost, OHCost, genericSellPrice, CHCost;
        internal int WhygoRoomId, timezoneID, videoAvailable, Capacity;
        internal int isCrazyHoursSupported,IsEarlyHoursEnabled, IsAfterHourEnabled, isAutomatic, isHDCapable, isInternetCapable;
        internal int isVCCapable, isTP, isIPDedicated, isIPCConnectionCapable, isIPCapable, isInternetFree, isISDNCapable;
        internal int Is24HoursEnabled, Open24Cost, speed;
        internal int isDST;
        internal string RoomDescription, ExtraNotes, Country, State, AssitantEmail;
        internal string CountryName, StateName ;
        internal string ipAddress, isdnAddress, IPSpeed, ISDNSpeed;
        internal string Type,Zipcode, Address1, City;
        internal string extraNotes, RoomPhone, Maplink;
        internal string internetBiller, geoCodeAddress, CateringOptionsAvailable, AUXEquipment, CurrencyType, URL;
        internal string internetPriceCurrency;
        internal string CHtartTime, CHEndTime,  EHStartTime, EHEndTime, AHStartTime, AHEndTime, openHours, OHStartTime, OHEndTime;
        internal string latitude, longitude,MiddleTierName;
        internal DateTime PublicRoomLastModified;
        internal double timezoneOffset;
        internal int AHFullyAuto, CHFullyAuto, EHFullyAuto; //FB 2543
        //2329 end -WhyGo
        internal Queue confList;
        internal Room()
        {
            confList = new Queue();
            timezoneID = 26;
        }
    }

    class tierInfo
    {
        internal int l3LocationId, l2LocationId;
        internal String l3LocationName, l2LocationName;

        internal tierInfo()
        {
            l2LocationName = l3LocationName = "";
        }
    }

    class Conference
    {
        internal DateTime startDate;
        internal DateTime startTime;
        internal DateTime endTime;
        internal int uniqueID;
        internal int confID;
        internal int instanceID;
        internal string name;
        internal string description;
        internal Queue roomList;
        internal Queue cateringOrders;
        internal Queue resourceOrders;
        internal Queue participantList;
        internal Queue locationList;
        internal Conference()
        {
            roomList = new Queue();
            cateringOrders = new Queue();
            resourceOrders = new Queue();
            participantList = new Queue();
            locationList = new Queue();
        }
    }

    class GenericRoom //FB 2329
    {
        
        internal string companyName;
        internal string contactName, contactPhone,contactEmail;
        internal string transmissionType, transmissionSpeed, transmissionNumber;
        internal int timeZoneId, numberOfAttendees ;
        internal string attendeeName, attendeePhone, attendeeEmail, ipOrISDN;
        internal string bridgeDetail, extraNotes, speed, City;
        internal int[] catering, equipment;
        internal GenericRoom()
        {
        }
    }



    class Tier2
    {
        internal string name;
        internal int id;
        internal int timezoneId;
        internal string filePath, timezoneId_Name;
    }


}


namespace NS_PERSISTENCE
{
    #region references
    using System;
    using System.Collections;
    #endregion

    class Conference
    {

        internal enum eType { FUTURE_VIDEO_CONF, ROOM_CONF, IMMED_CONF, POINT_TO_POINT, TEMPLATE_CONF };

        internal int iConfID, iInstanceID, iDurationMin, iOwnerID, iTimezoneID;
        internal int iRecur_RecurType, iRecur_DailyType, iRecur_DayGap, iRecur_EndType, iRecur_Occurrence;
        internal DateTime dtStartDateForNonRecurConf, dtStartTime, dtStartDateTime;
        internal eType etType;
        internal bool isImmediate, isPublic, isOpenForRegistration, isRecurring;
        internal string sConfName, sDescription, sPassword;
        internal int iVideoLayout, iVideoSessionID, iManualVideoLayout, iLectureMode, iLineRateID, iAudioalgorithmID, iVideoProtocolID, iConfType, iMediaID;
        internal Queue roomList;
        internal Queue partyList;

        internal Conference()
        {
            iConfID = iInstanceID = 0;
            isImmediate = isPublic = isOpenForRegistration = isRecurring = false;
            sDescription = sPassword = "";
            roomList = new Queue();
            partyList = new Queue();
            iVideoLayout = iVideoSessionID = iManualVideoLayout = iLectureMode = iLineRateID = iAudioalgorithmID = iVideoProtocolID = iConfType = iMediaID = 0;
            etType = 0;
            iRecur_RecurType = iRecur_DailyType = iRecur_DayGap = iRecur_EndType = iRecur_Occurrence = 0;
            dtStartDateForNonRecurConf = dtStartTime = dtStartDateTime = DateTime.Now;
        }
    }
    class Room
    {
        internal int roomID;
        internal string name;

        internal Room()
        {
            roomID = 0;
        }
    }

    class Party
    {
        internal int userID;
        internal string name, email;

        internal Party()
        {
            userID = 0;
            name = email = null;
        }
    }

    class DiffChanges
    {
        internal int eventType, userID, confID, instanceID, userTimezoneID;
        internal string message, userName, userEmail, userTimezone;
        internal DateTime timestamp;

        internal DiffChanges()
        {
            userTimezone = null;
            userTimezoneID = 26;
        }
    }
}
