//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Text;

namespace NS_RADVISION
{
    #region References
    using System;
    using System.Collections;
    using System.Text;
    using System.Net;
    using System.Net.Sockets;
    using System.IO;
    using System.Xml;
    #endregion

    class Radvision
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        private NS_MESSENGER.ConfigParams configParams;        
        private string requestID = "991880601";
        private bool partyFound = false;    //FB 1552
        private String sconfgID = ""; //FB 1998 - Radvision elite

        internal Radvision(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
        }
        private bool SendCommand(NS_MESSENGER.MCU mcu, string sendXML, ref string receiveXML)
        {
            try
            {
                //mcu.iHttpPort = 3336; //Code commented fro API port

                logger.Trace("*********SendXML***********");
                logger.Trace(sendXML);

                // wrapper 
                string inXML = "<?xml version=\"1.0\"?><MCU_XML_API><Version>Ver 3.0</Version><Account>"+ mcu.sLogin+"</Account><Password>"+ mcu.sPwd+"</Password><Request>" + sendXML + "</Request></MCU_XML_API>";
                logger.Trace("*********SendinXML***********");
                logger.Trace(inXML);

                // connect to mcu
                logger.Trace("Connecting to Radvision MCU - "+ mcu.sName + "...");
                TcpClient client = new TcpClient();
                client.Connect(mcu.sIp, mcu.iHttpPort); //("209.90.209.233", 3336);  Code commented for API port                 
                Stream stm = client.GetStream();
                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes(inXML);
                logger.Trace("Transmitting...");
                stm.Write(ba, 0, ba.Length);
                int len = 10000000;
                byte[] bb = new byte[len];
                int k = stm.Read(bb, 0, len);               
                for (int i = 0; i < k; i++)
                    receiveXML += Convert.ToChar(bb[i]).ToString();                
                client.Close();
                logger.Trace("*********ReceiveXML***********");
                logger.Trace(receiveXML);

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        private bool CreateXML_CreateConference(NS_MESSENGER.Conference conf, ref string reservationXml)
        {
            try
			{
                //FB 2016 start
                int confServiceId = 71;

                if (conf.cMcu != null)
                    confServiceId = conf.cMcu.ConfServiceID;
                //FB 2016 end

                reservationXml = "<Create_Conference_Request>";
                reservationXml += "<RequestID>"+ requestID +"</RequestID>";
                reservationXml += "<ConfPassword>" + conf.sPwd + "</ConfPassword>"; 
                string prefix = "0152983";
                reservationXml += "<Name>"+ prefix + conf.iDbNumName.ToString()+"</Name>";
                reservationXml += "<NumName>" + prefix + conf.iDbNumName.ToString() + "</NumName>";
                reservationXml += "<Desc>"+ conf.sDbName+"</Desc>";
                reservationXml += "<ChairPassword></ChairPassword>";
                reservationXml += "<ConfServiceID>" + confServiceId.ToString() + "</ConfServiceID>"; //FB 1998 -Radvision elite & FB 2016
                int durationInSecs = conf.iDuration * 60;
                reservationXml += "<TTL>"+ durationInSecs.ToString() +"</TTL>";
                reservationXml += "<StandByTimeOut>" + durationInSecs.ToString() + "</StandByTimeOut>";
                reservationXml += "<ProfileID></ProfileID>";
                reservationXml += "<InviteO>false</InviteO>";
                reservationXml += "<ChInviteO>false</ChInviteO>";
                reservationXml += "<MinPorts></MinPorts>";
                reservationXml += "<MaxPorts></MaxPorts>";
                reservationXml += "<Reserve_HD_Encoder>None</Reserve_HD_Encoder>";
                reservationXml += "<Scheduler>true</Scheduler>";
                reservationXml += "<CascMaster>true</CascMaster>";
                reservationXml += "<OperatorNumber></OperatorNumber>";
                reservationXml += "<ReconnectReason></ReconnectReason>";
                reservationXml += "<ReconnectDelay>0</ReconnectDelay>";
                reservationXml += "<NumOfRedial>0</NumOfRedial>";
                reservationXml += "<RedialDelay>0</RedialDelay>";
                reservationXml += "<WaitingRoom>false</WaitingRoom>";
                reservationXml += "<StartMuted>false</StartMuted>";
                reservationXml += "<Pref_MP_Vector></Pref_MP_Vector>";
                reservationXml += "</Create_Conference_Request>";
                
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }            
        }
        private bool ResponseXML_CreateConference(string responseXML, ref string confGID)
        {
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                // if description = OK , then party was added successfully
                string description = xd.SelectSingleNode("//Create_Conference_Response/ReturnValue").InnerXml.Trim();                
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Error: " + description;
                    return false; //error
                }
                confGID = xd.SelectSingleNode("//Create_Conference_Response/ConfGID").InnerXml.Trim(); 
      
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        private bool CreateXML_AddParticipant(NS_MESSENGER.Conference conf, string confGID, ref string participantXml)
        {
            try
            {
                participantXml = "<Invite_Participant_Request>";
                participantXml += "<RequestID>"+ requestID +"</RequestID>";
                participantXml +="<ConfGID>"+ confGID+"</ConfGID>";
                participantXml +="<ChairID></ChairID>";
                participantXml +="<Invite_Part_List>";
               
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                for (int i = 0; i < partyCount; i++)
                {
                    // go to next party in the queue
                    partyEnumerator.MoveNext();

                    // party
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;

                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                    {
                        party.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

                    participantXml += "<Invite_Part>";
                    participantXml += "<DialStr>"+party.sAddress+"</DialStr>";
                    participantXml += "<PartName>"+party.sName +"</PartName>";
                    participantXml += "<DisplayName>"+ party.sName +"</DisplayName>";
                    participantXml += "<ViaNode></ViaNode>";
                    participantXml += "<PrefCallRate>-1</PrefCallRate>";
                    participantXml += "<InitVolume>0</InitVolume>";
                    participantXml += "<ReferenceID></ReferenceID>";
                    participantXml += "<Lang></Lang>";
                    participantXml += "<ProtocolType></ProtocolType>";
                    participantXml += "<UserInfo></UserInfo>";
                    participantXml += "<Set_In_Layout_List>";
                        participantXml += "<Set_In_Layout_Params>";
                            participantXml += "<LID></LID>";
                            participantXml += "<LayoutType></LayoutType>";
                            participantXml += "<SbFrIndx>0</SbFrIndx>"; //FB 1998 -Radvision elite
                        participantXml += "</Set_In_Layout_Params>";
                    participantXml += "</Set_In_Layout_List>";
                    participantXml += "<Set_Out_Layout_Params>";
                        participantXml += "<LID></LID>";
                        participantXml += "<LayoutType></LayoutType>";
                        participantXml += "<VideoOutputID></VideoOutputID>";
                    participantXml += "</Set_Out_Layout_Params>";
                    participantXml += "</Invite_Part>";
                }
                participantXml += "</Invite_Part_List>";
                participantXml += "</Invite_Participant_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }            
        }

        //FB 1998 - Radvision elite start..
        private bool CreateXML_AddParticipant(NS_MESSENGER.Conference conf, string confGID, ref string participantXml, NS_MESSENGER.Party party)
        {
            try
            {
                participantXml = "<Invite_Participant_Request>";
                participantXml += "<RequestID>" + requestID + "</RequestID>";
                participantXml += "<ConfGID>" + confGID + "</ConfGID>";
                participantXml += "<ChairID></ChairID>";
                participantXml += "<Invite_Part_List>";
                // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                {
                    party.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                }

                participantXml += "<Invite_Part>";
                participantXml += "<DialStr>" + party.sAddress + "</DialStr>";
                participantXml += "<PartName>" + party.sName + "</PartName>";
                participantXml += "<DisplayName>" + party.sName + "</DisplayName>";
                participantXml += "<ViaNode></ViaNode>";
                participantXml += "<PrefCallRate>-1</PrefCallRate>";
                participantXml += "<InitVolume>0</InitVolume>";
                participantXml += "<ReferenceID></ReferenceID>";
                participantXml += "<Lang></Lang>";
                participantXml += "<ProtocolType></ProtocolType>";
                participantXml += "<UserInfo></UserInfo>";
                participantXml += "<Set_In_Layout_List>";
                participantXml += "<Set_In_Layout_Params>";
                participantXml += "<LID></LID>";
                participantXml += "<LayoutType></LayoutType>";
                participantXml += "<SbFrIndx>0</SbFrIndx>"; //FB 1998 - Radvision elite
                participantXml += "</Set_In_Layout_Params>";
                participantXml += "</Set_In_Layout_List>";
                participantXml += "<Set_Out_Layout_Params>";
                participantXml += "<LID></LID>";
                participantXml += "<LayoutType></LayoutType>";
                participantXml += "<VideoOutputID></VideoOutputID>";
                participantXml += "</Set_Out_Layout_Params>";
                participantXml += "</Invite_Part>";
                participantXml += "</Invite_Part_List>";
                participantXml += "</Invite_Participant_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        //FB 1998 - Radvision elite

        private bool ResponseXML_AddParticipant(string responseXML)
        {
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);
                
                // if description = OK , then party was added successfully
                string description = xd.SelectSingleNode("//Invite_Participant_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Error: " + description;
                    return false; //error
                }                

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        private bool CreateXML_MuteParticipant(string confGID, string partyID, ref string muteXml, bool mute)
        {
            try
            {
                muteXml = "<Part_Media_Chan_Operation_Request>";
                muteXml += "<RequestID>"+ requestID +"</RequestID>";
                    muteXml += "<ConfGID>"+ confGID+"</ConfGID>";
                    muteXml += "<ChairID></ChairID>";
                    muteXml += "<PID>"+ partyID +"</PID>";
                    muteXml += "<ChannelMediaType>Audio</ChannelMediaType>";
                    muteXml += "<ChannelDirection>In</ChannelDirection>";
                    muteXml += "<MuteOn>"+ mute +"</MuteOn>";
                muteXml += "</Part_Media_Chan_Operation_Request>";
                
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        private bool CreateXML_TerminateConference(NS_MESSENGER.Conference conf, ref string terminateXML)
        {
            try
            {
                terminateXML = "<Terminate_Conference_Request>";
                    terminateXML+= "<ConfGID>"+ conf.sMcuName +"</ConfGID>";
                    terminateXML += "<ChairID></ChairID>";
                    terminateXML += "<RequestID>"+ requestID +"</RequestID>";
                terminateXML += "</Terminate_Conference_Request>";
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        private bool CreateXML_ConferenceDisplayLayout(NS_MESSENGER.Conference conf,int displayLayoutID, ref string layoutXML)
        {
            try
            {
                layoutXML = "<All_Participant_Set_Out_Layout_Request>";
                    layoutXML += "<RequestID>"+ requestID +"</RequestID>";
                    layoutXML += "<ConfGID>"+ conf.sMcuName +"</ConfGID>";
                    layoutXML += "<ChairID></ChairID>";
                    
                    // equate the display layouts
                    string strLayout = "1";
                    bool ret = EquateVideoLayout(displayLayoutID, ref strLayout);
                    if (!ret) return false; // error  

                    layoutXML += "<LID>"+ strLayout +"</LID>";
                    layoutXML += "<LayoutType>Video</LayoutType>";
                    layoutXML += "<VideoOutputID>" + strLayout + "</VideoOutputID>";
                layoutXML += "</All_Participant_Set_Out_Layout_Request>";
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        private bool CreateXML_ParticipantDisplayLayout(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int displayLayoutID, ref string layoutXML)
        {
            try
            {
                layoutXML = "<Participant_Set_Out_Layout_Request>";
                    layoutXML += "<RequestID>" + requestID + "</RequestID>";
                    layoutXML += "<ConfGID>" + conf.sMcuName + "</ConfGID>";
                    layoutXML += "<ChairID></ChairID>";
                    layoutXML += "<Out_Layout_Part_List>";
                        layoutXML += "<Out_Layout_Part>";
                            layoutXML += "<PID>"+ party.sMcuName+"</PID>";

                            // equate the display layouts
                            string strLayout = "1";
                            bool ret = EquateVideoLayout(displayLayoutID, ref strLayout);
                            if (!ret) return false; // error  

                            layoutXML += "<LID>" + strLayout + "</LID>";
                            layoutXML += "<LayoutType>Video</LayoutType>";
                            layoutXML += "<VideoOutputID>" + strLayout + "</VideoOutputID>";
                        layoutXML += "</Out_Layout_Part>";
                    layoutXML += "</Out_Layout_Part_List>";
                layoutXML += "</Participant_Set_Out_Layout_Request>";
                
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        // TODO: Get the rad video layout ID equivalence for VRM layout ids
        private bool EquateVideoLayout(int videoLayoutID, ref string radVideoLayoutValue)
        {
            radVideoLayoutValue = "1";
            try
            {
                switch (videoLayoutID)
                {
                    case 1:
                        {
                            radVideoLayoutValue = "1";
                            break;
                        }

                    default:
                        {
                            // layout not supported
                            this.errMsg = "This video layout is not supported on Radvision MCU.";
                            return false;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        private bool ResponseXML_ConferenceDisplayLayout(string responseXML)
        {
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                // if description = OK , then party was added successfully
                string description = xd.SelectSingleNode("//All_Participant_Set_Out_Layout_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Error: " + description;
                    return false; //error
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        private bool ResponseXML_ParticipantDisplayLayout(string responseXML)
        {
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                // if description = OK , then party was added successfully
                string description = xd.SelectSingleNode("//Participant_Set_Out_layout_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Error: " + description;
                    return false; //error
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        private bool ResponseXML_TerminateConference(string responseXML)
        {
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                // if description = OK , then party was added successfully
                string description = xd.SelectSingleNode("//Terminate_Conference_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Error: " + description;
                    return false; //error
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        
        private bool ResponseXML_MuteParticipant(string responseXML)
        {
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                // if description = OK , then party was added successfully
                string description = xd.SelectSingleNode("//Part_Media_Chan_Operation_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Error: " + description;
                    return false; //error
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        internal bool MuteEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // Conf list
                bool ret = FetchOngoingParticipant(conf.cMcu,ref party);

                // Mute cmd
                ret = false; string muteXML = null; 
                ret = CreateXML_MuteParticipant(party.sConfNameOnMcu, party.sMcuName, ref muteXML, mute);
                if (!ret) return false;

                ret = false; string responseXML = null;
                ret = SendCommand(conf.cMcu, muteXML, ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_MuteParticipant(responseXML);
                if (!ret) return false;

                // successful 
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                // Conf list
                bool ret = FetchOngoingConference(conf.cMcu, ref conf);
                if (!ret) return false;

                // Mute cmd
                ret = false; string terminateXML = null; 
                ret = CreateXML_TerminateConference(conf, ref terminateXML);
                if (!ret) return false;

                ret = false; string responseXML = null;
                ret = SendCommand(conf.cMcu, terminateXML, ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_TerminateConference(responseXML);
                if (!ret) return false;

                // successful 
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
        
        private bool FetchOngoingParticipant(NS_MESSENGER.MCU mcu , ref NS_MESSENGER.Party party)
        {
            // Fetch ongoing conf list
            string confListXML = null;
            bool ret = CreateXML_ConferenceList(ref confListXML);
            if (!ret) return false;
            
            // send command to mcu
            ret = false; string responseXML = null;
            ret = SendCommand(mcu, confListXML, ref responseXML);
            if (!ret) return false;

            // parse the response
            ret = false; Queue confList = new Queue();
            ret = ResponseXML_ConferenceList(responseXML, ref confList);
            if (!ret) return false;

            // iterate through the conference list and get the participant list 
            System.Collections.IEnumerator confEnumerator;
            confEnumerator = confList.GetEnumerator();
            int confCount = confList.Count;
            for (int i = 0; i < confCount; i++)
            {
                // go to next conf in the queue
                confEnumerator.MoveNext();

                // conf
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf = (NS_MESSENGER.Conference)confEnumerator.Current;

                // conf participants
                ret = false; string partyListXML = null;
                ret = CreateXML_ConferenceParticipants(conf.sMcuName, ref partyListXML);
                if (!ret) continue;

                // send the cmd to mcu
                ret = false; responseXML = null;
                ret = SendCommand(mcu, partyListXML, ref responseXML);
                if (!ret) continue;

                // get the participants
                ret = false; 
                ret = ResponseXML_ConferenceParticipants(responseXML, ref conf);
                if (!ret) continue;

                // party enumerator
                partyFound = false;    //FB 1552
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                for (int j = 0; j < partyCount; j++)
                {
                    // go to next party in the queue
                    partyEnumerator.MoveNext();

                    // party
                    NS_MESSENGER.Party mcuParty = new NS_MESSENGER.Party();
                    mcuParty = (NS_MESSENGER.Party)partyEnumerator.Current;

                    if (party.sName.CompareTo(mcuParty.sName) == 0)
                    { 
                        // party name matches
                        party.sMcuName = mcuParty.sMcuName; // transfer the PID
                        party.sConfNameOnMcu = conf.sMcuName;
                        party.etStatus = mcuParty.etStatus; //FB 1552
                        partyFound = true;  //FB 1552
                        break;
                    }
                }
            }
            return true;
        }

        private bool FetchOngoingConference(NS_MESSENGER.MCU mcu, ref NS_MESSENGER.Conference conf)
        {
            string targetConfName = conf.sExternalName;

            // Fetch ongoing conf list
            string confListXML = null;
            bool ret = CreateXML_ConferenceList(ref confListXML);
            if (!ret) return false;

            // send command to mcu
            ret = false; string responseXML = null;
            ret = SendCommand(mcu, confListXML, ref responseXML);
            if (!ret) return false;

            // parse the response
            ret = false; Queue confList = new Queue();
            ret = ResponseXML_ConferenceList(responseXML, ref confList);
            if (!ret) return false;

            // iterate through the conference list to find the matching conference
            System.Collections.IEnumerator confEnumerator;
            confEnumerator = confList.GetEnumerator();
            int confCount = confList.Count;
            for (int i = 0; i < confCount; i++)
            {
                // go to next conf in the queue
                confEnumerator.MoveNext();

                // conf
                NS_MESSENGER.Conference tmpConf = new NS_MESSENGER.Conference();
                tmpConf = (NS_MESSENGER.Conference)confEnumerator.Current;

                // compare mcu conf with target conf
                if (conf.sExternalName.CompareTo(targetConfName) == 0)
                    {
                        // conf name matches                        
                        conf.sMcuName = tmpConf.sMcuName; // transfer the confGID 
                        break;
                    }                
            }
            return true;
        }

        internal bool SetConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                string reservationXML = null;
                bool ret = CreateXML_CreateConference(conf, ref reservationXML);
                if (!ret) return false;

                ret = false;string responseXML= null;
                ret = SendCommand(conf.cMcu, reservationXML, ref responseXML);
                if (!ret) return false;

                ret = false;string confGID = null;
                ret = ResponseXML_CreateConference(responseXML, ref confGID);
                if (!ret) return false;

                ret = false; string participantXML = null;
                ret = CreateXML_AddParticipant(conf, confGID, ref participantXML);
                if (!ret) return false;

                ret = false; responseXML = null;
                ret = SendCommand(conf.cMcu, participantXML, ref responseXML);
                if (!ret) return false;

                ret = false; 
                ret = ResponseXML_AddParticipant(responseXML);
                if (!ret) return false;

                // successful 
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        private bool CreateXML_ConferenceList(ref string conferenceList)
        {
            conferenceList = "<Get_Conference_List_Request>";
            conferenceList += "<RequestID>"+ this.requestID+"</RequestID>";
                //conferenceList += "<SnapShotValue>0</SnapShotValue>"; // 0 = send entire list every time... commented for radvision elite
            conferenceList += "</Get_Conference_List_Request>";
            
            return true;
        }
        
        private bool ResponseXML_ConferenceList(string responseXML, ref Queue confList)
        {

            //Load the response xml recd from the bridge and parse it
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(responseXML);

            // if description = OK , then continue
            string description = xd.SelectSingleNode("//Get_Conference_List_Response/ReturnValue").InnerXml.Trim();
            if (description.Length < 0 || description.Contains("OK") != true)
            {
                this.errMsg = "MCU Error: " + description;
                return false; //error
            }                

		    // conf list 		
            XmlNodeList nodelist = xd.SelectNodes("//Get_Conference_List_Response/Conf_List/Conf");
				
			//cycle through each conference one-by-one
            foreach (XmlNode node in nodelist)
            {
                // new conf object to store the each conf ongoing details
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

                //GID of conference
                conf.sMcuName = node.SelectSingleNode("ConfGID").InnerText.Trim();

                //name of conference
                conf.sExternalName = node.SelectSingleNode("Desc").InnerText.Trim();
                
                // TODO: parse the name and extract the UID 

                confList.Enqueue(conf);
            }				
            return true;
        }

        private bool CreateXML_ConferenceParticipants(string confGID, ref string partyListXML)
        {
            partyListXML = "<Get_Participant_List_Request>";
            partyListXML += "<RequestID>991880602</RequestID>";
                partyListXML += "<ConfGID>"+ confGID +"</ConfGID>";
                partyListXML += "<GetExtendedInfo>false</GetExtendedInfo>";
                partyListXML += "<SnapShotValue>0</SnapShotValue>";
                partyListXML += "<ChairID></ChairID>";
                partyListXML += "<ExpandedMCUs>";
                    partyListXML += "<ExpandMCUMode>None</ExpandMCUMode>";
                    partyListXML += "<Expand_MCU_List>";
                        partyListXML += "<McuPartID></McuPartID>";
                    partyListXML += "</Expand_MCU_List>";
                partyListXML += "</ExpandedMCUs>";
            partyListXML += "</Get_Participant_List_Request>";

            return true;
        }

        private bool ResponseXML_ConferenceParticipants(string responseXML, ref NS_MESSENGER.Conference conf)
        {
            //Load the response xml recd from the bridge and parse it
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(responseXML);
            //Added for Radvision elite - FB 1998 start
            XmlNode confgidnode = xd.SelectSingleNode("//Get_Participant_List_Response/ConfGID");
            if (confgidnode != null)
                sconfgID = confgidnode.InnerText;

            //Added for Radvision elite - FB 1998 end
            // conf list 		
            XmlNodeList nodelist = xd.SelectNodes("//Get_Participant_List_Response/Participants_List/Part");

            //cycle through each conference partys one-by-one
            string partyStatus = "";
            foreach (XmlNode node in nodelist)
            {
                // new conf object to store the each conf ongoing details
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();

                //PID of party
                party.sMcuName = node.SelectSingleNode("PID").InnerText.Trim();

                //name of party
                party.sName = node.SelectSingleNode("PartName").InnerText.Trim();

                //FB 1552 start
                partyStatus = "";
                if (node.SelectSingleNode("ConSt") != null)
                {
                    partyStatus = node.SelectSingleNode("ConSt").InnerText.Trim();
                }
                if (partyStatus != "")
                {
                    if (partyStatus.ToLower().IndexOf("disconnect") >= 0)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                    }
                    else if ((partyStatus.ToLower().IndexOf("proceeding") >= 0) || (partyStatus.ToLower().IndexOf("alerting") >= 0))
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                    }
                    else if (partyStatus.ToLower().IndexOf("connected") >= 0)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                    }
                }
                else
                {
                    party.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE;
                }
                //FB 1552 end
                conf.qParties.Enqueue(party);
            }            
            return true;
        }

        // TODO: Add the GetServices in Operations.cs
        internal bool GetServices(NS_MESSENGER.MCU mcu, ref Queue serviceList)
        {
            // service list
            string serviceListXML = null;
            bool ret = CreateXML_GetServices(ref serviceListXML);
 
            // send cmd 
            ret = false; string responseXML = null;
            ret = SendCommand(mcu, serviceListXML, ref responseXML);
            if (!ret) return false;

            // get services
            ret = false; Queue serviceQ = new Queue();
            ret= ResponseXML_GetServices(responseXML, ref serviceQ);
            if (!ret) return false;

            return true;
        }

        private bool CreateXML_GetServices(ref string serviceListXML)
        {
            serviceListXML = "<Get_Services_Request>";
            serviceListXML += "<RequestID>"+ this.requestID +"</RequestID>";
            serviceListXML += "</Get_Services_Request>";

            return true;
        }

        private bool ResponseXML_GetServices(string responseXML,ref Queue serviceList)
        {
            //Load the response xml recd from the bridge and parse it
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(responseXML);

            // conf list 		
            XmlNodeList nodelist = xd.SelectNodes("//Get_Services_Response/Services_List/Service");                       
            
            //cycle through each conference one-by-one
            foreach (XmlNode node in nodelist)
            {
                //service prefix
                string servicePrefix = node.SelectSingleNode("ServicePrefix").InnerText.Trim();

                //service desc
                string serviceDesc = node.SelectSingleNode("ServiceDescription").InnerText.Trim();

                //default chair pwd
                string chairPwd = node.SelectSingleNode("DefaultChairPassword").InnerText.Trim();

                // add to services queue
            }          
            return true;
        }
        
        private bool CreateXML_GetMaxCalls(ref string maxCallsXML)
        {
            maxCallsXML = "<Get_Max_Calls_Request>";
            maxCallsXML += "<RequestID>" + this.requestID + "</RequestID>";
            maxCallsXML += "</Get_Max_Calls_Request>";
            return true;
        }

        private bool ResponseXML_GetMaxCalls(string responseXML, ref int maxCalls)
        {
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                // if description = OK , then party was added successfully
                string description = xd.SelectSingleNode("//Get_Max_Calls_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Error: " + description;
                    return false; //error
                }
                
                maxCalls = Int32.Parse(xd.SelectSingleNode("//Get_Max_Calls_Response/MaxCalls").InnerXml.Trim());

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
            
        }
        
        internal bool TestMCUConnection(NS_MESSENGER.MCU mcu)
        {
            try
            {
                // max calls
                string maxCallsXML = null;
                bool ret = CreateXML_GetMaxCalls(ref maxCallsXML);

                // send cmd 
                ret = false; string responseXML = null;
                ret = SendCommand(mcu, maxCallsXML, ref responseXML);
                if (!ret) return false;

                // get max call count
                ret = false; int maxCalls = 0;
                ret = ResponseXML_GetMaxCalls(responseXML, ref maxCalls);
                if (!ret || maxCalls ==0) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        //FB 1552 - New methods added ... start
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            try
            {
                // Conf list
                partyFound = false;
                conf.cMcu = party.cMcu;//Radvision elite...FB 1998
                bool ret = FetchOngoingParticipant(conf.cMcu, ref party);

                //Add party to conference if he is not in response xml
                if (!partyFound)
                {
                    ret = false; string participantXML = null;
                    logger.Trace("Confgid to add participant " + sconfgID);//Radvision elite ..FB 1998
                    ret = CreateXML_AddParticipant(conf, sconfgID, ref participantXML,party);//Radvision elite .. FB 1998
                    if (!ret) return false;

                    ret = false;string responseXML = null;
                    ret = SendCommand(conf.cMcu, participantXML, ref responseXML);
                    if (!ret) return false;

                    ret = false;
                    ret = ResponseXML_AddParticipant(responseXML);
                    if (!ret) return false;

                    ret = FetchOngoingParticipant(conf.cMcu, ref party);
                }
                // successful 
                return ret;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
    }
}