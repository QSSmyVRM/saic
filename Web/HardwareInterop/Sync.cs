﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace NS_Sync
{
    class Sync
    {
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        public string errMsg = "";
        int timeLimit = 5;// try time for every timieLimit interval

        internal Sync(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }

        internal bool TriggerEvent(String inXml, ref String OutXML)
        {
            DateTime tryDate = DateTime.Now;
            List<NS_MESSENGER.sysExtSchedEvent> eventList = null;
            NS_MESSENGER.sysExtSchedEvent evnt = null;
            int retryCnt = 0, totalTime = 0, totalUplimit = 0, totalLolimit = 0;  
            NS_MESSENGER.SysSettings SysSettings = new NS_MESSENGER.SysSettings ();
            try
            {
                db.FetchSyncSettingsDetails(ref SysSettings);
                db.FetchExternalNewEventList(ref eventList);
                OutXML = "<SyncDetails>";
                OutXML += "<SyncSettings>";
                OutXML += "<SyncAddress>" + SysSettings.SyncAddress + "</SyncAddress>";
                OutXML += "<SyncPort>" + SysSettings.SyncPort + "</SyncPort>";
                OutXML += "<SyncConfig>" + SysSettings.SyncConfig + "</SyncConfig>";
                OutXML += "<SyncEmailAddress>" + SysSettings.SyncEmailAddress + "</SyncEmailAddress>";
                OutXML += "</SyncSettings>";
                if (eventList != null)
                {
                    OutXML += "<SyncConferences>";
                    for (int evtCnt = 0; evtCnt < eventList.Count; evtCnt++)
                    {
                       
                        evnt = eventList[evtCnt];

                        if (evnt.sRetryCount > 3)
                            continue;

                        OutXML += "<SyncConference>";
                        OutXML += "<Confnumname>" + evnt.sConfnumname + "</Confnumname>";
                        OutXML += "<TypeID>" + evnt.sTypeID + "</TypeID>";
                        OutXML += "<EventTime>" + evnt.dEventTime.ToString() + "</EventTime>";
                        OutXML += "<Status>" + evnt.sStatus + "</Status>";
                        OutXML += "<StatusDate>" + evnt.dStatusDate.ToString() + "</StatusDate>";
                        OutXML += "<RequestCall>" + evnt.sRequestCall + "</RequestCall>";
                        OutXML += "<ResponseCall>" + evnt.sResponseCall + "</ResponseCall>";
                        OutXML += "<RetryCount>" + evnt.sRetryCount + "</RetryCount>";
                        OutXML += "<OrgID>" + evnt.sOrgID + "</OrgID>";
                        OutXML += "<StatusMessage>" + evnt.sStatusMessage + "</StatusMessage>";
                        OutXML += "</SyncConference>";
                    }
                    OutXML += "</SyncConferences>";
                }
                OutXML += "</SyncDetails>";
            }
            catch (Exception ex)
            {
                logger.Trace("TriggerEvent" + ex.StackTrace);
                errMsg = ("TriggerEvent" + ex.StackTrace);
                return false;
            }
            return true;
        }

        internal bool UpdateEvent(string INXML,ref string OUTXML)
        {
            XmlDocument xmldoc = null;
            string Message = "Error", Response = "", Type = "", ExtConfID = "";
            string Status = "N";
            int ConfUID = 0,  RetryCount = 0;
            String strQuery = "";
            XmlNodeList nodes = null;
            int NetworkProcessed = 0;
            try
            {
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(INXML);

                nodes = xmldoc.SelectNodes("//UpdateConference/Conferences/Conference");
                if (nodes != null && nodes.Count > 0)
                {
                    logger.Trace("Number of Sync Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {

                        if (nodes[cnfcnt].SelectSingleNode("ConfUID") != null)
                            int.TryParse(nodes[cnfcnt].SelectSingleNode("ConfUID").InnerText.Trim(), out ConfUID);
                        ExtConfID = (nodes[cnfcnt].SelectSingleNode("ExtConfID") != null) ? nodes[cnfcnt].SelectSingleNode("ExtConfID").InnerText.Trim() : "";
                        Response = (nodes[cnfcnt].SelectSingleNode("Message") != null) ? nodes[cnfcnt].SelectSingleNode("Message").InnerText.Trim() : "";
                        Type = (nodes[cnfcnt].SelectSingleNode("TypeID") != null) ? nodes[cnfcnt].SelectSingleNode("TypeID").InnerText.Trim() : "";
                        if (nodes[cnfcnt].SelectSingleNode("RetryCount") != null)
                            int.TryParse(nodes[cnfcnt].SelectSingleNode("RetryCount").InnerText.Trim(), out RetryCount);
                        Status = (nodes[cnfcnt].SelectSingleNode("Status") != null) ? nodes[cnfcnt].SelectSingleNode("Status").InnerText.Trim() : "";
                        if (Status == "TS")
                        {
                            Message = "Transmit Success";
                            NetworkProcessed = 1;
                        }
                        if (RetryCount > 3 && Status == "")
                        {
                            Status = "E";
                            Message = "Transmit Error";
                            NetworkProcessed = 1;
                        }

                        if (ConfUID > 0)
                            UpdateSyncConference(ref ConfUID, ref ExtConfID, ref Response, ref RetryCount, ref Status, ref Message, ref Type, ref strQuery, ref NetworkProcessed);
                    }
                }

                if (strQuery.Trim() != "")
                    db.UpdateSyncConference(strQuery);

            }
            catch (Exception ex)
            {
                logger.Trace("UpdateEvent" + ex.StackTrace);
                errMsg = ("UpdateEvent" + ex.StackTrace);
                return false;
            }
            return true;
        }

        internal bool FailureEvent()
        {
            NS_MESSENGER.SysSettings SysSettings = new NS_MESSENGER.SysSettings();
            List<NS_MESSENGER.sysExtSchedEvent> eventList = null;
            NS_MESSENGER.sysExtSchedEvent evnt = null;
            NS_EMAIL.Email sendEmail = new NS_EMAIL.Email(configParams);
            string[] Emaillist = null;
            try
            {
                db.FetchSyncSettingsDetails(ref SysSettings);
                db.FetchFailureEventList(ref eventList);

                if (SysSettings.SyncEmailAddress != "")
                {
                    Emaillist = SysSettings.SyncEmailAddress.Split(';');

                    if (eventList != null)
                    {
                        for (int evtCnt = 0; evtCnt < eventList.Count; evtCnt++)
                        {
                            evnt = eventList[evtCnt];
                            sendEmail.SendEmailtoSyncAdmin(evnt, Emaillist);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdateEvent" + ex.StackTrace);
                errMsg = ("UpdateEvent" + ex.StackTrace);
                return false;
            }
            return true;
        }

        #region UpdateSyncConference
        /// <summary>
        /// UpdateSyncConference
        /// </summary>
        /// <param name="ConfUID"></param>
        /// <param name="ExtConfID"></param>
        /// <param name="Response"></param>
        /// <param name="RetryCount"></param>
        /// <param name="Status"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public void UpdateSyncConference(ref int ConfUID, ref string ExtConfID, ref string Response, ref int RetryCount, ref string Status, ref string Message, ref string Type, ref string strQuery, ref int NetworkProcessed)
        {
            string query = "";
            try
            {
                query += " Update ES_Event_D SET [Status] = '" + Status + "', RequestID = '" + ExtConfID + "' , StatusDate = '" + DateTime.Now + "', ResponseCall = '" + Response + "', StatusMessage = '" + Message + "', RetryCount = " + RetryCount + " , NetworkProcessed = " + NetworkProcessed + " WHERE ConfNumName = " + ConfUID + " and TypeID = '" + Type + "'; ";
                
                if (ExtConfID.Trim() != "")
                {
                    query += " Update Conf_Conference_D SET  ESId = '" + ExtConfID + "' WHERE ConfNumName = " + ConfUID+" ; ";
                    
                }
            }
            catch (Exception ex)
            {

            }

            strQuery += query;
            
        }
        #endregion

    }
}
