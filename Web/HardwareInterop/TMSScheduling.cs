﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Security.Principal;
using RTC_v181.CiscoAuthService;
using RTC_v181.CiscoSchedulerService;
using System.Collections;



namespace NS_TMS
{

    public class TMSScheduling
    {
        #region Parameters
        internal bool isRecurring = false;
        internal string errMsg = null;
        int TMSconfid = 0;
        bool ret = false;
        string URL = "";
        
        //Myvrm General class declaration starts
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        NS_MESSENGER.Party Party;
        List<NS_MESSENGER.Conference> confList = null;
        //Myvrm Ends


        // Cisco TMS Declaration Starts

        //Booking service asmx Starts
        BookingService Bookingservice;
        Conference conference;
        //Booking service asmx Ends


        //Remote Service Asmx Starts
        RemoteSetupService Remotesetup;
        //Remote Service Asmx Ends

        // Cisco TMS Declaration Ends


        #endregion

        #region constructor
        internal TMSScheduling(NS_MESSENGER.ConfigParams config)
        {
            //Myvrm Declaration Starts
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
            Party = new NS_MESSENGER.Party();
            //Myvrm Declaration Ends



        }
        #endregion

        #region General Method

        #region IntegrateTMSServer
        /// <summary>
        /// Booking service credential integration
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>

        private bool IntegrateTMSBookingService(NS_MESSENGER.MCU mcu, ref BookingService Bookingservice)
        {
            try
            {
                if (!(string.IsNullOrEmpty(mcu.sIp) || string.IsNullOrEmpty(mcu.sLogin) || string.IsNullOrEmpty(mcu.sPwd) || string.IsNullOrEmpty(mcu.sRPRMDomain)))
                {
                    logger.Trace("TMS URL, Login,Password,Domain should not be empty");
                    return false;
                }
                URL = "http://" + mcu.sIp.Trim() + "/tms/external/Booking/BookingService.asmx";

                Bookingservice = new BookingService();

                logger.Trace("TMS Booking URL:" + URL);
                logger.Trace("Login:" + mcu.sLogin + " Password:" + mcu.sPwd + " Domain:" + mcu.sRPRMDomain);

                Bookingservice.Url = URL;

                NetworkCredential Credential = new NetworkCredential(mcu.sLogin, mcu.sPwd, mcu.sRPRMDomain);

                Bookingservice.Credentials = Credential;

                if (Bookingservice.ExternalAPIVersionSoapHeaderValue == null)
                    Bookingservice.ExternalAPIVersionSoapHeaderValue = new RTC_v181.CiscoSchedulerService.ExternalAPIVersionSoapHeader();
                Bookingservice.ExternalAPIVersionSoapHeaderValue.ClientVersionIn = 5;

            }
            catch (Exception ex)
            {
                logger.Trace("IntegrateBookingService Exception:" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region IntegrateTMSRemoteService
        /// <summary>
        /// Remote service Credentail intergration
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        private bool IntegrateTMSRemoteService(NS_MESSENGER.MCU mcu, ref RemoteSetupService Remotesetup)
        {

            try
            {
                if (!(string.IsNullOrEmpty(mcu.sIp) || string.IsNullOrEmpty(mcu.sLogin) || string.IsNullOrEmpty(mcu.sPwd) || string.IsNullOrEmpty(mcu.sRPRMDomain)))
                {
                    logger.Trace("TMS URL, Login,Password,Domain should not be empty");
                    return false;
                }

                URL = "http://" + mcu.sIp.Trim() + "/tms/external/Booking/remotesetup/remotesetupservice.asmx";

                NetworkCredential Credential = new NetworkCredential(mcu.sLogin, mcu.sPwd, mcu.sRPRMDomain);
                Remotesetup = new RemoteSetupService();

                logger.Trace("TMS Booking URL:" + URL);
                logger.Trace("Login:" + mcu.sLogin + " Password:" + mcu.sPwd + " Domain:" + mcu.sRPRMDomain);

                Remotesetup.Credentials = Credential;
                if (Remotesetup.ExternalAPIVersionSoapHeaderValue == null)
                    Remotesetup.ExternalAPIVersionSoapHeaderValue = new RTC_v181.CiscoAuthService.ExternalAPIVersionSoapHeader();
                Remotesetup.ExternalAPIVersionSoapHeaderValue.ClientVersionIn = 5;

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("IntegrateRemoteService Exception:" + ex.Message);
                return false;
            }
        }
        #endregion

        #endregion

        #region PublicMethods


        #region TestMCUConnection
        /// <summary>
        /// TestMCUConnection using is Alive Calling RemoteTMS webservice 
        /// </summary>
        /// <returns></returns>
        internal bool TestMCUConnection(NS_MESSENGER.MCU MCU)
        {
            try
            {       
                ret = false;
                ret = IntegrateTMSRemoteService(MCU, ref Remotesetup);
                if (!ret)
                {
                    logger.Trace("Error in Intergrating TMS Remote Service");
                    return false;
                }
                ret = false;
                ret = Remotesetup.IsAlive();
                if (!ret)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("TestMCUConnection Block:" + ex.Message);
                return false;
            }
        }
        # endregion

        #region GetMCUTime
        /// <summary>
        /// GetMCUTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool GetMCUTime(NS_MESSENGER.MCU mcu)
        {
           
            try
            {
               return true;
            }
            catch (Exception ex)
            { 
                logger.Trace("GetMCUTime:"+ex.Message);
                return false;
            }
        }
        #endregion

        #region SetorUpdateConference
        /// <summary>
        /// SetConference and Update Conference
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="RecieveXML"></param>
        /// <returns></returns>
        internal bool SetorUpdateConference(NS_MESSENGER.Conference conf)
        {
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
            int confid = 0, partyCount = 0;
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(conf);

                if (conf.iRecurring == 1 && isRecurring)
                {
                    DeleteSyncConference(conf);
                    db.FetchConf(ref confList, ref confs, 0); //for new and edit conference need to send delStatus as 0 
                    confList = confs;
                   
                }

                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    Bookingservice = new BookingService();

                    ret = false;
                    ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                    if (!ret)
                    {
                        logger.Trace("Error in Integrating TMS");
                        return false;
                    }
                    conference = new Conference();
                    Party = new NS_MESSENGER.Party();

                    conference = Bookingservice.GetDefaultConference();

                    DateTime StartDate = DateTime.Parse(conf.dtStartDateTime.ToString());
                    DateTime EndDate = StartDate.AddMinutes(conf.iDuration);

                    conference.ConferenceId = -1;
                    if (!string.IsNullOrEmpty(conf.ESId))
                    {
                        Int32.TryParse(conf.ESId, out confid);
                        conference.ConferenceId = confid;
                    }

                    conference.Title = conf.sDbName;
                    conference.StartTimeUTC = StartDate.ToString("yyyy-MM-dd HH:mm:ssZ");
                    conference.EndTimeUTC = StartDate.AddMinutes(conf.iDuration).ToString("yyyy-MM-dd HH:mm:ssZ");



                    Participant[] participants = new Participant[conf.qParties.Count];
                    logger.Trace("Party count: " + conf.qParties.Count.ToString());
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    partyCount = conf.qParties.Count;

                    for (int i = 0; i < partyCount; i++)
                    {
                        // go to next party in the queue
                        partyEnumerator.MoveNext();

                        Party = new NS_MESSENGER.Party();
                        Party = (NS_MESSENGER.Party)partyEnumerator.Current;

                        // Create the elements of the array (the actual participants)
                        participants[i] = new Participant();
                        participants[i].ParticipantCallType = ParticipantType.IPVideo; // Dial-out video
                        participants[i].NameOrNumber = Party.sAddress;

                    }
                    conference.Participants = participants;
                    conference.ConferenceType = ConferenceType.AutomaticCallLaunch;
                    conference.DataConference = DataConferenceMode.IfPossible;
                    
                    conference = Bookingservice.SaveConference(conference);

                    if (conference.ConferenceId > 0)
                    {
                        ret = db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, conference.ConferenceId.ToString(), "TMS", "");
                        if (!ret)
                        {
                            logger.Trace("Error in Update Conf ESID:" + conference.ConferenceId.ToString());
                            return false;
                        }
                        else
                            logger.Trace("Successfully Updated Conf ESID:" + conference.ConferenceId.ToString());
                    }
                    else
                        logger.Trace("Error in Fetching Conf ESID from TMS:" + conference.ConferenceId.ToString());

                }
            }
            catch (Exception ex)
            {
                logger.Trace("SetConference" + ex.StackTrace);
                return false;
            }
            return true;

        }
        #endregion

        #region DeleteSyncConference
        /// <summary>
        /// DeleteSyncConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool DeleteSyncConference(NS_MESSENGER.Conference conf)
        {
            List<NS_MESSENGER.Conference> confListDelete = null;
            try
            {
                confListDelete = new List<NS_MESSENGER.Conference>();
                confListDelete.Add(conf);

                db.FetchSyncAdjustments(ref confListDelete);

                for (int cnfCnt = 0; cnfCnt < confListDelete.Count; cnfCnt++)
                {

                    confListDelete[cnfCnt].cMcu = conf.cMcu;
                    ret = false;
                    ret = DeleteScheduledConference(confListDelete[cnfCnt]);
                    if (!ret)
                        logger.Exception(100, "TerminateConference failed");
                    else
                        db.UpdateSyncStatus(confListDelete[cnfCnt]);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }
        #endregion

        #region TerminateConference
        /// <summary>
        /// DeleteConferenceBy conference ID
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                List<NS_MESSENGER.Conference>  confs = new List<NS_MESSENGER.Conference>();
                confList.Add(conf);
				
                if (conf.iRecurring == 1 && isRecurring)
                {
                    db.FetchConf(ref confList, ref confs, 1); //for delete mode need to send delStatus as 1
                    confList = confs;
                }
				
                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {

                    try
                    {  
                        ret = false;
                        ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                        if (!ret)
                           logger.Trace("Error in Integrating TMS Booking Service");
                            

                        if (!string.IsNullOrEmpty(conf.ESId))
                            int.TryParse(conf.ESId, out TMSconfid);
                        else
                            logger.Trace("TMS Conference ID is Null");
                      
                        Bookingservice.EndConferenceById(TMSconfid);
                    }
                    catch (Exception)
                    {
                        logger.Trace("Error in deleteing the conference:"+confList[cnfCnt].iDbID);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("TerminateConference Block" + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region TerminateScheduledConference
        /// <summary>
        /// DeleteConferenceBy conference ID
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool DeleteScheduledConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                ret = false;
                ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                if (!ret)
                {
                    logger.Trace("Error in Integrating TMS Booking Service");
                    return false;
                }

                if (!string.IsNullOrEmpty(conf.ESId))
                    int.TryParse(conf.ESId, out TMSconfid);
                else
                {
                    logger.Trace("TMS Conference ID is Null");
                    return false;
                }

                Bookingservice.DeleteConferenceById(TMSconfid);

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("TerminateConference Block" + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region GetEndpointStatus
        /// <summary>
        /// GetEndpointStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participant"></param>
        /// <returns></returns>
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party participant)
        {
            DateTime StartDate = new DateTime();
            DateTime EndDate = new DateTime();
            int confESID = 0;
            try
            {
               
                Conference[] conference = new Conference[100];
               
                ret = false;
                ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                if (!ret)
                {
                    //logger.Trace("Error in Integrating TMS");
                    return false;

                }

                StartDate = DateTime.Parse(conf.dtStartDateTimeInUTC.ToString("yyyy-MM-ddTHH:mm:ss+00:00"));
                EndDate = DateTime.Parse(conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration).ToString("yyyy-MM-ddTHH:mm:ss+00:00"));
                int.TryParse(conf.ESId, out confESID);

                conference = Bookingservice.GetConferencesForUser(participant.sName, StartDate, EndDate, ConferenceStatus.Ongoing);


                for (int i = 0; i < conference.Count(); i++)
                {
                    if (conference[0].ConferenceId == confESID)
                    {
                        db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, participant.sAddress.Trim(), conf.ESId, (int)NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT);
                        participant.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                    }
                    else
                    {
                        db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, participant.sAddress.Trim(), conf.ESId, (int)NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT);
                        participant.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                //logger.Trace("GetEndpointStatus:" + ex.Message);
                return false;
            }
        }
        #endregion

        # region GetConferenceDetails
        /// <summary>
        /// GetConferenceDetails
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool GetConferenceDetails(NS_MESSENGER.Conference conf)
        {
            int confid = 0;
            try
            {
                Bookingservice = new BookingService();

                ret = false;
                ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                if (!ret)
                {
                    logger.Trace("Error in Integrating TMS");
                    return false;
                }
                Conference[] confarray = new Conference[100];

                if (!string.IsNullOrEmpty(conf.ESId))
                {
                    Int32.TryParse(conf.ESId, out confid);

                    confarray = Bookingservice.GetConferencesForUser(conf.cMcu.sLogin, DateTime.UtcNow, DateTime.UtcNow.AddMinutes(30), ConferenceStatus.Ongoing);

                    if (confarray.Count() > 0)
                    {
                        for (int i = 0; i < confarray.Count(); i++)
                        {
                            if (confarray[i].ConferenceId.ToString() == conf.ESId)
                            {
                                ret = false;
                                ret = db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, NS_MESSENGER.Conference.eStatus.ONGOING);

                                if (!ret)
                                {
                                    logger.Trace("Error in Update conference status");
                                    return false;

                                }
                            }
                        }
                    }
                    else
                    {
                        logger.Trace("No Ongoing conference in TMS interface");
                        return false;
                    }
                }
                else
                {
                    logger.Trace("Conference Esid is empty");
                    return false;
                }

                

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("GetConferenceDetails :" + ex.Message);
                return false;
            }
        }
        # endregion




     
        #endregion



    }
}
