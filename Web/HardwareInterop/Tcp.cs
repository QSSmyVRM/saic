﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NS_Tcp
{
    #region References
    using System;
    using System.Collections;
    using System.Text;
    using System.Net;
    using System.Net.Sockets;
    using System.IO;
    using System.Xml;
    #endregion

    class Tcp
    {
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_OPERATIONS.Operations operations;
        internal string errMsg = null;

        internal Tcp(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
         }

        #region SendCommand
        /// <summary>
        /// SendCommand
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="sendXML"></param>
        /// <param name="receiveXML"></param>
        /// <returns></returns>
        private bool SendCommand(NS_MESSENGER.Party party, string sendXML, ref string receiveXML, int CallRequestTimeout)//FB 2993
        {
            TcpClient client = null;
            Stream stm = null;
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] msg = null;
            byte[] nxtLine = new byte[] {0x0d,0x0a};
            int len = 10000000;
            byte[] rcvdMsg = null;
            int k = 0 , i =0;
            try
            {
                logger.Trace("*********SendXML***********");
                logger.Trace(sendXML);
                client = new TcpClient();
                logger.Trace("Party NetworkURL:" + party.sNetworkURL);
                logger.Trace("Party SecurePort:" + party.iSecureport);

                if (party.sNetworkURL.Trim() == "")
                    return false;

                client.Connect(party.sNetworkURL, party.iSecureport);

                //FB 2993 Start

                DateTime requet = DateTime.Now.AddSeconds(CallRequestTimeout);
                do
                {
                    stm = client.GetStream();
                    msg = asen.GetBytes(sendXML);
                    logger.Trace("Transmitting...");
                    stm.Write(msg, 0, msg.Length);
                    stm.Write(nxtLine, 0, nxtLine.Length);
                    rcvdMsg = new byte[len];
                    k = stm.Read(rcvdMsg, 0, len);
                    for (i = 0; i < k; i++)
                        receiveXML += Convert.ToChar(rcvdMsg[i]).ToString();

                    if (receiveXML.Trim() != "")
                        break;
                    
                    if (DateTime.Now == requet)
                        break;
                }
                while (receiveXML.Trim().Length <= 0);

                //FB 2993 End

                client.Close();
                logger.Trace("*********ReceiveXML***********");
                logger.Trace(receiveXML);

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
            finally
            {
                if (stm != null)
                {
                    stm.Close();
                    stm.Dispose();
                }
            }
        }

        #endregion

        #region SecureCheckCommand
        /// <summary>
        /// SecureCheckCommand
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="Secure"></param>
        /// <param name="receiveXML"></param>
        /// <returns></returns>
        internal bool SecureCheck(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int Secure, ref string receiveXML, string hadrwareAdminEmail, ref bool error, int CallRequestTimeout) //FB 2993
        {
            string responseXML = "";
            
            bool ret = false;
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            NS_EMAIL.Email sendEmail = new NS_EMAIL.Email(configParams);
            operations = new NS_OPERATIONS.Operations(configParams);
            try
            {


                logger.Trace("*********Check Network Status***********");

                ret = SendCommand(party, "query_networkconnected", ref responseXML, CallRequestTimeout);//FB 2993
                if (!ret)
                    logger.Trace("StatusResponse failed");
              
                logger.Trace("StatusResponse : " + responseXML);

                if ((responseXML.Contains("networkconnected_2") && Secure == 2) || responseXML.Contains("networkconnected_1") && Secure == 3) //FB 2993
                {
                    logger.Trace("Update Conference Switching failed for confid : " + conf.iDbID.ToString());
                    return true;
                }
                else
                {
                    responseXML = "";

                    logger.Trace("*********Switching Operation***********");

                    logger.Trace("Network State :" + Secure.ToString());

                    if (Secure == 2)//FB 2993
                        ret = SendCommand(party, "go_network2", ref responseXML, CallRequestTimeout);//FB 2993
                    else
                        ret = SendCommand(party, "go_network1", ref responseXML, CallRequestTimeout);//FB 2993

                    if (!ret)
                    {
                        error = true;
                        logger.Trace("Switching operation failed");
                        logger.Trace("responseXML :" + responseXML);
                    }

                    logger.Trace("Response : " + responseXML);

                    if (responseXML.Contains("switching") || responseXML.Contains("alreadyconnected") || responseXML.Contains("switchcomplete"))
                    {
                        logger.Trace("SWITCHING has been done for Room id : " + party.iDbId.ToString());
                        return true;
                    }
                    else if (responseXML.Contains("error2") || responseXML.Contains("error4")) //Hardware errors
                    {
                        ret = false; error = true;
                        if (responseXML.Contains("error2"))
                            ret = sendEmail.SendHardwareAdminEmail(conf, hadrwareAdminEmail, "Switch is unable to complete because the device is in maintenance mode");
                        else
                            ret = sendEmail.SendHardwareAdminEmail(conf, hadrwareAdminEmail, "Switch is unable to complete because of a lack of codec communication");

                        if (!ret)
                            logger.Trace("Send mail for Hardware Admin failed for confid : " + conf.iDbID.ToString());

                    }
                    else if (responseXML.Contains("error1") || responseXML.Contains("error3") || responseXML.Contains("error5") || responseXML.Contains("error6")) //Software errors
                    {
                        ret = false; error = true;
                        ret = operations.TerminateConference(conf.iDbID, conf.iInstanceID);
                        if (!ret)
                            logger.Trace("Update Conference Status failed for confid : " + conf.iDbID.ToString());

                        ret = false;
                        ret = sendEmail.SendCancellationEmail(conf);
                        if (!ret)
                            logger.Trace("Send mail for Participant failed for confid : " + conf.iDbID.ToString());

                        return true;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion
    }
}
