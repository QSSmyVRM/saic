//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : TelnetAPI.cs
 * DESCRIPTION : All Telnet api commands are stored in this file. (Point-to-Point) 
 * AUTHOR : Kapil M
 */

namespace NS_TELNET
{
    #region References
    using System;
    using System.Xml;
    using System.Net;
    using System.Collections;
    using Dart.Telnet;
    using Dart.Common;
    using System.Threading;
    using System.Text.RegularExpressions;
    #endregion
    class Telnet
    {
        internal enum eTelnetOperation { CONNECT_HANGUP, DISPLAY_MESSAGE, CONNECTION_STATUS };
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        private NS_MESSENGER.ConfigParams configParams;
        //Dart.Telnet.Telnet telnet_connecter = null;//Code add fro P2P

        internal Telnet(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
        }


        internal bool Dial_PolycomCaller(NS_MESSENGER.Party caller, string destAddress, eTelnetOperation telnetOp, bool connectOrDisconnect, string messageText, ref string connectionStatusOutput, NS_MESSENGER.Conference conf)
        {//, int telnetPort, string srcIPAddress, string srcLogin, string srcPwd, NS_MESSENGER.LineRate.eLineRate lineRate, NS_MESSENGER.Party.eProtocol protocol,int confid,int instanceid
            try
            {
                //FB 2501 P2P Call Monitoring Start
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                //FB 2501 P2P Call Monitoring End

                logger.Trace("In the telnet sub-system...");
                Dart.Telnet.Telnet telnet_connecter = new Dart.Telnet.Telnet(); //GP Issue

                logger.Trace("SrcIPAddress :" + caller.sAddress);
                logger.Trace("Telnet port : " + caller.iAPIPortNo.ToString());
                logger.Trace("SrcLogin : " + caller.sLogin);
                logger.Trace("SrcPwd : " + caller.sPwd);
                logger.Trace("Connecting...");
                telnet_connecter.SocketOption.ReceiveTimeout = 5000;
                telnet_connecter.ServerPort = caller.iAPIPortNo;
                                
                // Connect to a server
                telnet_connecter.Connect(caller.sAddress);
                byte[] bt = new byte[10000];
                Dart.Common.Data data = telnet_connecter.Read(bt);
                logger.Trace("After connecting..." + data.ToString()); //GP Issue

                //FB 2501 P2P Call Monitoring Start
                alert = new NS_MESSENGER.Alert();
                alert.confID = conf.iDbID;
                alert.instanceID = conf.iInstanceID;
                alert.message = data.ToString();
                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                bool ret = false;
                ret = db.InsertConfP2PAlert(alert);
                if (!ret) logger.Trace("Point2Point alert insert failed.");
                //FB 2501 P2P Call Monitoring End

                if (telnet_connecter.State == ConnectionState.Connected)
                {
                    // checks for "Password" prompt. If found, then send the password in cleartext.
                    //if (data.ToString().ToLower().Contains("password"))
                    if (caller.sPwd.Trim() != null)
                    {
                        telnet_connecter.Write(caller.sPwd.Trim() + "\r\n");
                        data = telnet_connecter.Read("SNMP");
                        logger.Trace("Password sent..." + data.ToString());   //GP Issue
                        //FB 2501 P2P Call Monitoring Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = data.ToString();
                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                        ret = false;
                        ret = db.InsertConfP2PAlert(alert);
                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                        //FB 2501 P2P Call Monitoring End
                    }

                    // check if password fails
                    if (data.ToString().ToLower().Contains("password failed"))
                    {
                        telnet_connecter.Write(caller.sPwd.Trim() + "\r\n");
                        data = telnet_connecter.Read("SNMP");
                        logger.Trace("Password second attempt..." + data.ToString());   //GP Issue
                        //FB 2501 P2P Call Monitoring Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = data.ToString();
                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                        ret = false;
                        ret = db.InsertConfP2PAlert(alert);
                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                        //FB 2501 P2P Call Monitoring End
                    }

                    // check if password fails
                    if (data.ToString().ToLower().Contains("password failed"))
                    {
                        logger.Trace("Incorrect password.");
                        return false;
                    }

                    logger.Trace("Telnet Op:" + telnetOp.ToString() + "...");
                    switch (telnetOp)
                    {
                        case eTelnetOperation.CONNECT_HANGUP:
                            {
                                int numConnectTries = 0;

                            RETRY: string speed = "384";
                                EquateWithPolycomLineRate(caller.stLineRate.etLineRate, ref speed);
                                if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                                {
                                    // IP call
                                    string sendStr = null;
                                    if (connectOrDisconnect)
                                    {
                                        // connect call
                                        sendStr = "dial manual " + speed + " " + destAddress + " h323 \r\n";
                                    }
                                    else
                                    {
                                        // disconnect call
                                        sendStr = "hangup all \r\n";
                                    }
                                    logger.Trace("Send cmd : " + sendStr);
                                    if (telnet_connecter != null)
                                    {
                                        telnet_connecter.Write(sendStr);
                                        logger.Trace(telnet_connecter.Read(">").ToString()); //GP Issue
                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = telnet_connecter.Read(">").ToString();
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End
                                    }
                                }
                                else
                                {
                                    if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                                    {
                                        // isdn call 
                                        string sendStr = null;
                                        if (connectOrDisconnect)
                                        {
                                            // connect call
                                            sendStr = "dial manual " + speed + " " + destAddress + " h320 \r\n";
                                        }
                                        else
                                        {
                                            // disconnect call
                                            sendStr = "hangup all \r\n";
                                        }

                                        logger.Trace("Send cmd : " + sendStr);
                                        telnet_connecter.Write(sendStr);
                                        logger.Trace(telnet_connecter.Read(">").ToString()); //GP Issue
                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = telnet_connecter.Read(">").ToString();
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End

                                    }
                                    else
                                    {
                                        this.errMsg = "MPI protocol not supported for calls via Telnet API";
                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = this.errMsg;
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End
                                        return false;
                                    }
                                }
                                //#if CODE_DISABLED_ON_DAN_REQUEST_Jul28 . CODE_ENABLED_AGAIN_ON_DAN_REQUEST_Dec8                                
                                // check call status to see if call connected . 
                                Thread.Sleep(10000); // delay added between status checks
                                if (connectOrDisconnect)
                                {  
                                    string sendStr = "\r\n callinfo all \r\n";
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                    logger.Trace(connectionStatusOutput); //GP Issue
                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = connectionStatusOutput;
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    // If call not connected, retry 2 more times
                                    if (!connectionStatusOutput.Contains(":connected:"))
                                    {
                                        numConnectTries++;
                                        if (numConnectTries <= 2)
                                        {
                                            goto RETRY;
                                        }
                                    }
                                    else //FB 2501 P2P Call Monitoring
                                    {
                                        ret = false;
                                        ret = db.InsertConfPolycomVideoURL(conf);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    }
                                }
                                //#endif
                                break;
                            }
                        case eTelnetOperation.DISPLAY_MESSAGE:
                            {
                                string sendStr = "showpopup \"" + messageText + "\" \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                if (telnet_connecter != null)
                                {
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString()); //GP Issue
                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End
                                }
                                break;
                            }
                        case eTelnetOperation.CONNECTION_STATUS:
                            {
                                // KM: Press "Enter" multiple times to fall in the prompt.
                                string sendStr = null;
                                for (int i = 0; i < 5; i++)
                                {
                                    sendStr = "\r\n";
                                    logger.Trace("Send cmd : " + sendStr);
                                    if (telnet_connecter != null)
                                    {
                                        telnet_connecter.Write(sendStr);
                                        connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                        logger.Trace(connectionStatusOutput); //GP Issue
                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = connectionStatusOutput;
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End
                                    }
                                }
                                sendStr = "callinfo all \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                if (telnet_connecter != null)
                                {
                                    for (int i = 0; i < 3; i++)
                                    {
                                        telnet_connecter.Write(sendStr);
                                        connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                        logger.Trace(connectionStatusOutput); //GP Issue
                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = connectionStatusOutput;
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End
                                    }
                                    
                                }
                                break;
                            }
                    }
                }

                telnet_connecter.Close();
                telnet_connecter = null;
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Point to point connection failed. Error - " + e.Message;
                logger.Exception(100, this.errMsg);
                return false;
            }
        }

        internal bool Dial_TandbergEdgeCaller(NS_MESSENGER.Party caller, string destAddress, eTelnetOperation telnetOp, bool connectOrDisconnect, string messageText, ref string connectionStatusOutput, NS_MESSENGER.Conference conf)
        {
            try
            {
                //FB 2501 P2P Call Monitoring Start
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                //FB 2501 P2P Call Monitoring End

                logger.Trace("In the telnet sub-system...");
                Dart.Telnet.Telnet telnet_connecter = new Dart.Telnet.Telnet(); //GP Issue

                logger.Trace("SrcIPAddress :" + caller.sAddress);
                logger.Trace("Telnet port : " + caller.iAPIPortNo.ToString());
                //logger.Trace("SrcLogin : " + caller.sLogin);
                logger.Trace("SrcPwd : " + caller.sPwd);
                logger.Trace("Connecting...");
                telnet_connecter.SocketOption.ReceiveTimeout = 5000;
                telnet_connecter.ServerPort = caller.iAPIPortNo;

                // Connect to a server
                telnet_connecter.Connect(caller.sAddress);
                byte[] bt = new byte[10000];
                Dart.Common.Data data = telnet_connecter.Read(bt);
                logger.Trace(data.ToString());

                //FB 2501 P2P Call Monitoring Start
                alert = new NS_MESSENGER.Alert();
                alert.confID = conf.iDbID;
                alert.instanceID = conf.iInstanceID;
                alert.message = data.ToString();
                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                bool ret = false;
                ret = db.InsertConfP2PAlert(alert);
                if (!ret) logger.Trace("Point2Point alert insert failed.");
                //FB 2501 P2P Call Monitoring End

                if (telnet_connecter.State == ConnectionState.Connected)
                {
                    if (data.ToString().ToLower().Contains("login"))
                    {
                        telnet_connecter.Write(caller.sLogin + "\r\n");
                        data = telnet_connecter.Read(">");
                        logger.Trace(data.ToString());

                        //FB 2501 P2P Call Monitoring Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = data.ToString();
                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                        ret = false;
                        ret = db.InsertConfP2PAlert(alert);
                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                        //FB 2501 P2P Call Monitoring End
                    }

                    // checks for "Password" prompt. If found, then send the password in cleartext.
                    if (data.ToString().ToLower().Contains("password"))
                    {
                        telnet_connecter.Write(caller.sPwd + "\r\n");
                        data = telnet_connecter.Read(">");
                        logger.Trace(data.ToString());

                        //FB 2501 P2P Call Monitoring Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = data.ToString();
                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                        ret = false;
                        ret = db.InsertConfP2PAlert(alert);
                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                        //FB 2501 P2P Call Monitoring End
                    }

                    // check if password fails
                    if (data.ToString().ToLower().Contains("password failed"))
                    {
                        logger.Trace("Incorrect password.");
                        return false;
                    }

                    switch (telnetOp)
                    {
                        case eTelnetOperation.CONNECT_HANGUP:
                            {
                                int numConnectTries = 0;

                            RETRY: string speed = "384";
                                EquateWithPolycomLineRate(caller.stLineRate.etLineRate, ref speed);
                                if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                                {
                                    // IP call
                                    string sendStr = null;
                                    if (connectOrDisconnect)
                                    {
                                        // connect call
                                        sendStr = "xcommand dial number: " + destAddress + " callrate: " + speed + " \r\n";
                                    }
                                    else
                                    {
                                        // disconnect call
                                        sendStr = "xcommand disconnectcall call:1 \r\n";
                                    }
                                    logger.Trace("Send cmd TANGERG: " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString());

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End
                                }
                                else
                                {
                                    if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                                    {
                                        // isdn call 
                                        string sendStr = null;
                                        if (connectOrDisconnect)
                                        {
                                            // connect call
                                            //sendStr = "dial manual " + speed + " " + destAddress + " h320 \r\n";
                                            sendStr = "xcommand dial number: " + destAddress + " callrate: " + speed + " \r\n";
                                        }
                                        else
                                        {
                                            // disconnect call
                                            //sendStr = "hangup all \r\n";
                                            sendStr = "xcommand disconnectcall call:1 \r\n";
                                        }

                                        logger.Trace("Send cmd TANBERG ISDN: " + sendStr);
                                        telnet_connecter.Write(sendStr);
                                        logger.Trace(telnet_connecter.Read(">").ToString());

                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = telnet_connecter.Read(">").ToString();
                                        logger.Trace("TB alert.message : " + telnet_connecter.Read(">").ToString());
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End
                                    }
                                    else
                                    {
                                        this.errMsg = "MPI protocol not supported for calls via Telnet API";

                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = this.errMsg;
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End

                                        return false;
                                    }
                                }
                                //#if CODE_DISABLED_ON_DAN_REQUEST_Jul28 . CODE_ENABLED_AGAIN_ON_DAN_REQUEST_Dec8                                
                                // check call status to see if call connected . 
                                Thread.Sleep(5000); // delay added between status checks
                                if (connectOrDisconnect)
                                {
                                    string sendStr = "xstatus call 1 \r\n";
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    connectionStatusOutput = telnet_connecter.Read("OK").ToString();
                                    logger.Trace(connectionStatusOutput);

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = connectionStatusOutput;
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    // If call not connected, retry 2 more times
                                    if (!connectionStatusOutput.Contains("Synced"))
                                    {
                                        numConnectTries++;
                                        if (numConnectTries <= 2)
                                        {
                                            goto RETRY;
                                        }
                                    }
                                }
                                //#endif
                                break;
                            }
                        case eTelnetOperation.DISPLAY_MESSAGE:
                            {
                                string sendStr = "xcommand TextDisplay \"" + messageText + "\" \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                logger.Trace(telnet_connecter.Read(">").ToString());

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = telnet_connecter.Read(">").ToString();
                                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End

                                break;
                            }
                        case eTelnetOperation.CONNECTION_STATUS:
                            {
                                string sendStr = "xstatus call 1 \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                logger.Trace(connectionStatusOutput);

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = connectionStatusOutput;
                                alert.typeID = 15; //FB 2569 - P2P Monitoring event 
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End

                                break;
                            }
                    }
                }

                telnet_connecter.Close();
                telnet_connecter = null;
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Point to point connection failed. Error - " + e.Message;
                logger.Exception(100, this.errMsg);
                return false;
            }
        }
        //FB 2390 Start

        internal bool Dial_CiscoCaller(NS_MESSENGER.Party caller, string destAddress, eTelnetOperation telnetOp, bool connectOrDisconnect, string messageText, ref string connectionStatusOutput, ref int p2pCallId, NS_MESSENGER.Conference conf)
        {
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
           
            string callid = "", strp2pCallId = "";
            
            try
            {
                //FB 2501 P2P Call Monitoring Start
               
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                
                db.UpdateConferenceProcessStatus(conf.iDbID, conf.iInstanceID,1); //FB 2710

                String[] add = null;
                string CallStatus = "";
                logger.Trace("In the telnet sub-system...");
                Dart.Telnet.Telnet telnet_connecter = new Dart.Telnet.Telnet(); //GP Issue
                caller.sLogin = "admin";//FB 2710
                logger.Trace("SrcIPAddress :" + caller.sAddress);
                logger.Trace("Telnet port : " + caller.iAPIPortNo.ToString());
                logger.Trace("SrcLogin : " + caller.sLogin);
                logger.Trace("SrcPwd : " + caller.sPwd);
                logger.Trace("Connecting...");
                telnet_connecter.SocketOption.ReceiveTimeout = 15000;
                telnet_connecter.ServerPort = caller.iAPIPortNo;

                // Connect to a server
                telnet_connecter.Connect(caller.sAddress);
                telnet_connecter.Login(caller.sLogin, caller.sPwd, "");
                byte[] bt = new byte[10000];
                Dart.Common.Data data = telnet_connecter.Read(bt);
                //P2P DD ISsue
                if (data.ToString().Trim() == "")//FB 2710
                {

                    logger.Trace("Sleeping");
                    Thread.Sleep(5000);
                    data = telnet_connecter.Read(bt);
                }
                //P2P DD ISsue
                logger.Trace("Data :" + data.ToString().Trim());

                //FB 2501 P2P Call Monitoring Start
                alert = new NS_MESSENGER.Alert();
                alert.confID = conf.iDbID;
                alert.instanceID = conf.iInstanceID;
                alert.message = data.ToString().Trim();
                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                bool ret = false;
                ret = db.InsertConfP2PAlert(alert);
                if (!ret) logger.Trace("Point2Point alert insert failed.");
                //FB 2501 P2P Call Monitoring End

                if (telnet_connecter.State == ConnectionState.Connected)
                {


                    // check if password fails
                    if (data.ToString().ToLower().Contains("password failed"))
                    {
                        logger.Trace("Incorrect password.");
                        return false;
                    }
                    string sendStr = null;
                    logger.Trace("TelnetOperation :" + telnetOp);
                    switch (telnetOp)
                    {
                        case eTelnetOperation.CONNECT_HANGUP:
                            {
                                if (connectOrDisconnect)
                                {
                                    int numConnectTries = 0;
                                RETRY: string speed = "384";
                                    EquateWithPolycomLineRate(caller.stLineRate.etLineRate, ref speed);
                                    if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                                    {
                                        sendStr = "xcommand dial number:" + destAddress + " callrate:" + speed + " Protocol:Sip \r\n";

                                    }
                                    else
                                    {
                                        // IP call
                                        sendStr = "xcommand dial number:" + destAddress + " callrate:" + speed + " Protocol:h323 \r\n";

                                    }

                                    logger.Trace("Command :" + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    CallStatus = telnet_connecter.Read(">").ToString();

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = CallStatus;
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End
                                    //P2P DD ISsue
                                    if (CallStatus.Contains("CallId:"))
                                    {
                                        add = sendStr.Split(':');
                                        if(add.Length > 5)
                                            callid = Regex.Replace(add[5].ToString().ToLower(), "[a-z]", "").Trim();
                                        Int32.TryParse(callid,out p2pCallId);
                                        if (p2pCallId > 0)
                                            strp2pCallId = p2pCallId.ToString();
                                        logger.Trace("p2pCallId: " + p2pCallId);
                                    }
                                    //P2P DD ISsue
                                    // check call status to see if call connected . 
                                    //Thread.Sleep(2000); // delay added between status checks
                                    sendStr = "xStatus Call " + strp2pCallId + " \r\n";
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                    logger.Trace(connectionStatusOutput);

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = connectionStatusOutput;
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    // If call not connected, retry 2 more times
                                    if (!connectionStatusOutput.Contains("Connected"))
                                    {
                                        numConnectTries++;
                                        if (numConnectTries <= 2)
                                        {
                                            goto RETRY;
                                        }
                                    }
                                }
                                else
                                {
                                    // disconnect call
                                    if (p2pCallId < 0)
                                    {
                                        sendStr = "xCommand Call Disconnect CallId:" + p2pCallId.ToString() + " \r\n";
                                    }
                                    else
                                    {
                                        sendStr = "xCommand Call DisconnectAll \r\n";
                                    }
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString());

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End
                                }
                                break;
                            }
                        case eTelnetOperation.DISPLAY_MESSAGE:
                            {
                                sendStr = "xCommand Message TextLine Display Text:\"" + messageText + "\" \r\n"; //Tamil xCommand Message Alert Display Text:
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                logger.Trace(telnet_connecter.Read(">").ToString());

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = telnet_connecter.Read(">").ToString();
                                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End
                                break;
                            }
                        case eTelnetOperation.CONNECTION_STATUS:
                            {
                                strp2pCallId = "";
                                if (p2pCallId > 0)
                                    strp2pCallId = p2pCallId.ToString();

                                //string sendStr = "xStatus Call \r\n";//
                                sendStr = "xStatus Call " + strp2pCallId + " \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                logger.Trace(connectionStatusOutput);

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = connectionStatusOutput;
                                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End
                                break;
                            }
                    }
                }

                telnet_connecter.Close();
                telnet_connecter = null;
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Point to point connection failed. Error - " + e.Message;
                logger.Exception(100, this.errMsg);
                return false;
            }
            finally//FB 2710
            {
                
                db.UpdateConferenceProcessStatus(conf.iDbID, conf.iInstanceID, 0);
            }
        }

        internal bool Dial_MXPCaller(NS_MESSENGER.Party caller, string destAddress, eTelnetOperation telnetOp, bool connectOrDisconnect, string messageText, ref string connectionStatusOutput, ref int p2pCallId, NS_MESSENGER.Conference conf)
        {
            try
            {
                //FB 2501 P2P Call Monitoring Start
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                //FB 2501 P2P Call Monitoring End

                logger.Trace("In the telnet sub-system...");
                Dart.Telnet.Telnet telnet_connecter = new Dart.Telnet.Telnet(); //GP Issue

                logger.Trace("SrcIPAddress :" + caller.sAddress);
                logger.Trace("Telnet port : " + caller.iAPIPortNo.ToString());
                logger.Trace("SrcLogin : " + caller.sLogin);
                logger.Trace("SrcPwd : " + caller.sPwd);
                logger.Trace("Connecting...");
                telnet_connecter.SocketOption.ReceiveTimeout = 5000;
                telnet_connecter.ServerPort = caller.iAPIPortNo;

                // Connect to a server
                telnet_connecter.Connect(caller.sAddress);
                byte[] bt = new byte[10000];
                Dart.Common.Data data = telnet_connecter.Read(bt);
                logger.Trace(data.ToString());

                //FB 2501 P2P Call Monitoring Start
                alert = new NS_MESSENGER.Alert();
                alert.confID = conf.iDbID;
                alert.instanceID = conf.iInstanceID;
                alert.message = data.ToString();
                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                bool ret = false;
                ret = db.InsertConfP2PAlert(alert);
                if (!ret) logger.Trace("Point2Point alert insert failed.");
                //FB 2501 P2P Call Monitoring End

                if (telnet_connecter.State == ConnectionState.Connected)
                {
                    // checks for "Password" prompt. If found, then send the password in cleartext.
                    if (data.ToString().ToLower().Contains("password"))
                    {
                        telnet_connecter.Write(caller.sPwd + "\r\n");
                        data = telnet_connecter.Read(">");
                        logger.Trace(data.ToString());

                        //FB 2501 P2P Call Monitoring Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = data.ToString();
                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                        ret = false;
                        ret = db.InsertConfP2PAlert(alert);
                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                        //FB 2501 P2P Call Monitoring End
                    }

                    // check if password fails
                    if (data.ToString().ToLower().Contains("password failed"))
                    {
                        logger.Trace("Incorrect password.");
                        return false;
                    }

                    switch (telnetOp)
                    {
                        case eTelnetOperation.CONNECT_HANGUP:
                            {
                                int numConnectTries = 0;

                            RETRY: string speed = "384";
                                EquateWithPolycomLineRate(caller.stLineRate.etLineRate, ref speed);
                                if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                                {
                                    // IP call
                                    string sendStr = null;
                                    if (connectOrDisconnect)
                                    {
                                        // connect call
                                        sendStr = "xcommand dial number: " + destAddress + " callrate: " + speed + " \r\n";
                                    }
                                    else
                                    {
                                        // disconnect call
                                        sendStr = "xcommand disconnectcall call:1 \r\n";
                                    }
                                    logger.Trace("Send cmd MXP: " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString());

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End
                                }
                                else
                                {
                                    if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                                    {
                                        // isdn call 
                                        string sendStr = null;
                                        if (connectOrDisconnect)
                                        {
                                            // connect call
                                            //sendStr = "dial manual " + speed + " " + destAddress + " h320 \r\n";
                                            sendStr = "xcommand dial number: " + destAddress + " callrate: " + speed + " \r\n";
                                        }
                                        else
                                        {
                                            // disconnect call
                                            //sendStr = "hangup all \r\n";
                                            sendStr = "xcommand disconnectcall call:1 \r\n";
                                        }

                                        logger.Trace("Send cmd MXP ISDN: " + sendStr);//output as Callref: and LogTag:
                                        telnet_connecter.Write(sendStr);
                                        logger.Trace(telnet_connecter.Read(">").ToString());

                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = telnet_connecter.Read(">").ToString();
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End
                                    }
                                    else
                                    {
                                        this.errMsg = "MPI protocol not supported for calls via Telnet API";
                                        //FB 2501 P2P Call Monitoring Start
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = this.errMsg;
                                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                        ret = false;
                                        ret = db.InsertConfP2PAlert(alert);
                                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                                        //FB 2501 P2P Call Monitoring End
                                        return false;
                                    }
                                }
                                //#if CODE_DISABLED_ON_DAN_REQUEST_Jul28 . CODE_ENABLED_AGAIN_ON_DAN_REQUEST_Dec8                                
                                // check call status to see if call connected . 
                                Thread.Sleep(5000); // delay added between status checks
                                if (connectOrDisconnect)
                                {
                                    string sendStr = "xstatus call 1 \r\n";
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    connectionStatusOutput = telnet_connecter.Read("OK").ToString();
                                    logger.Trace(connectionStatusOutput);

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = connectionStatusOutput;
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    // If call not connected, retry 2 more times
                                    if (!connectionStatusOutput.Contains("Synced"))
                                    {
                                        numConnectTries++;
                                        if (numConnectTries <= 2)
                                        {
                                            goto RETRY;
                                        }
                                    }
                                }
                                //#endif
                                break;
                            }
                        case eTelnetOperation.DISPLAY_MESSAGE:
                            {
                                string sendStr = "xcommand TextDisplay text: \"" + messageText + "\" \r\n"; // xcommand TextDisplay \
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                logger.Trace(telnet_connecter.Read(">").ToString());

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = telnet_connecter.Read(">").ToString();
                                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End

                                break;
                            }
                        case eTelnetOperation.CONNECTION_STATUS:
                            {
                                string sendStr = "xstatus call 1 \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                logger.Trace(connectionStatusOutput);

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = connectionStatusOutput;
                                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End

                                break;
                            }
                    }
                }

                telnet_connecter.Close();
                telnet_connecter = null;
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Point to point connection failed. Error - " + e.Message;
                logger.Exception(100, this.errMsg);
                return false;
            }
        }

        internal bool Dial_ViewStationCaller(NS_MESSENGER.Party caller, string destAddress, eTelnetOperation telnetOp, bool connectOrDisconnect, string messageText, ref string connectionStatusOutput, ref int p2pCallId, NS_MESSENGER.Conference conf)
        {
            try
            {
                //FB 2501 P2P Call Monitoring Start
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                //FB 2501 P2P Call Monitoring End

                String[] add = null;
                string CallStatus = "";
                logger.Trace("In the telnet sub-system...");
                Dart.Telnet.Telnet telnet_connecter = new Dart.Telnet.Telnet(); //GP Issue

                logger.Trace("SrcIPAddress :" + caller.sAddress);
                logger.Trace("Telnet port : " + caller.iAPIPortNo.ToString());
                logger.Trace("SrcLogin : " + caller.sLogin);
                logger.Trace("SrcPwd : " + caller.sPwd);
                logger.Trace("Connecting...");
                telnet_connecter.SocketOption.ReceiveTimeout = 5000;
                telnet_connecter.ServerPort = caller.iAPIPortNo;

                // Connect to a server
                telnet_connecter.Connect(caller.sAddress);
                byte[] bt = new byte[10000];
                Dart.Common.Data data = telnet_connecter.Read(bt);
                logger.Trace(data.ToString());

                //FB 2501 P2P Call Monitoring Start
                alert = new NS_MESSENGER.Alert();
                alert.confID = conf.iDbID;
                alert.instanceID = conf.iInstanceID;
                alert.message = data.ToString();
                alert.typeID = 15; //FB 2569 - P2P Monitoring event
                bool ret = false;
                ret = db.InsertConfP2PAlert(alert);
                if (!ret) logger.Trace("Point2Point alert insert failed.");
                //FB 2501 P2P Call Monitoring End

                if (telnet_connecter.State == ConnectionState.Connected)
                {
                    // checks for "Password" prompt. If found, then send the password in cleartext.
                    if (data.ToString().ToLower().Contains("password"))
                    {
                        telnet_connecter.Write(caller.sPwd + "\r\n");
                        data = telnet_connecter.Read(">");
                        logger.Trace(data.ToString());

                        //FB 2501 P2P Call Monitoring Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = data.ToString();
                        alert.typeID = 15; //FB 2569 - P2P Monitoring event
                        ret = false;
                        ret = db.InsertConfP2PAlert(alert);
                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                        //FB 2501 P2P Call Monitoring End
                    }

                    // check if password fails
                    if (data.ToString().ToLower().Contains("password failed"))
                    {
                        logger.Trace("Incorrect password.");
                        return false;
                    }
                    string sendStr = null;
                    switch (telnetOp)
                    {
                        case eTelnetOperation.CONNECT_HANGUP:
                            {
                                if (connectOrDisconnect)
                                {
                                    int numConnectTries = 0;
                                RETRY: string speed = "384";
                                    EquateWithPolycomLineRate(caller.stLineRate.etLineRate, ref speed);
                                    if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                                    {
                                        // IP call
                                        sendStr = "dial manual 1x" + speed + " " + destAddress + " h323 \r\n";//wrong "dial manual " + speed + " " + destAddress + " h323 \r\n";
                                    }
                                    else
                                    {
                                        sendStr = "dial manual 1x" + speed + " " + destAddress + " isdn \r\n";
                                    }
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString());

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569 - P2P Monitoring event
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    Thread.Sleep(5000); // delay added between status checks
                                    sendStr = "\r\n getcallstate \r\n";
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                    logger.Trace(connectionStatusOutput);

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = connectionStatusOutput;
                                    alert.typeID = 15; //FB 2569-P2P Monitoring
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    // If call not connected, retry 2 more times
                                    if (!connectionStatusOutput.Contains("CONNECTED"))
                                    {
                                        numConnectTries++;
                                        if (numConnectTries <= 2)
                                        {
                                            goto RETRY;
                                        }
                                    }
                                }
                                else
                                {
                                    sendStr = "hangup video \r\n"; //wrong hangup all
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString());

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569-P2P Monitoring
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                }
                                break;
                            }
                        case eTelnetOperation.DISPLAY_MESSAGE:
                            {
                                sendStr = "showpopup \"" + messageText + "\" \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                logger.Trace(telnet_connecter.Read(">").ToString());

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = telnet_connecter.Read(">").ToString();
                                alert.typeID = 15; //FB 2569-P2P Monitoring
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End

                                break;
                            }
                        case eTelnetOperation.CONNECTION_STATUS:
                            {
                                sendStr = "\r\n getcallstate \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                logger.Trace(connectionStatusOutput);

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = connectionStatusOutput;
                                alert.typeID = 15; //FB 2569-P2P Monitoring
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End

                                break;
                            }
                    }
                }

                telnet_connecter.Close();
                telnet_connecter = null;
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Point to point connection failed. Error - " + e.Message;
                logger.Exception(100, this.errMsg);
                return false;
            }
        }

        internal bool Dial_PolycomVSXCaller(NS_MESSENGER.Party caller, string destAddress, eTelnetOperation telnetOp, bool connectOrDisconnect, string messageText, ref string connectionStatusOutput, ref int p2pCallId, NS_MESSENGER.Conference conf)
        {
            try
            {
                //FB 2501 P2P Call Monitoring Start
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                //FB 2501 P2P Call Monitoring End

                //String[] add = null;
                //string CallStatus = "";
                logger.Trace("In the telnet sub-system...");
                Dart.Telnet.Telnet telnet_connecter = new Dart.Telnet.Telnet(); //GP Issue

                logger.Trace("SrcIPAddress :" + caller.sAddress);
                logger.Trace("Telnet port : " + caller.iAPIPortNo.ToString());
                logger.Trace("SrcLogin : " + caller.sLogin);
                logger.Trace("SrcPwd : " + caller.sPwd);
                logger.Trace("Connecting...");
                telnet_connecter.SocketOption.ReceiveTimeout = 5000;
                telnet_connecter.ServerPort = caller.iAPIPortNo;

                // Connect to a server
                telnet_connecter.Connect(caller.sAddress);
                byte[] bt = new byte[10000];
                Dart.Common.Data data = telnet_connecter.Read(bt);
                logger.Trace(data.ToString());

                //FB 2501 P2P Call Monitoring Start
                alert = new NS_MESSENGER.Alert();
                alert.confID = conf.iDbID;
                alert.instanceID = conf.iInstanceID;
                alert.message = data.ToString();
                alert.typeID = 15; //FB 2569-P2P Monitoring
                bool ret = false;
                ret = db.InsertConfP2PAlert(alert);
                if (!ret) logger.Trace("Point2Point alert insert failed.");
                //FB 2501 P2P Call Monitoring End

                if (telnet_connecter.State == ConnectionState.Connected)
                {
                    // checks for "Password" prompt. If found, then send the password in cleartext.
                    if (data.ToString().ToLower().Contains("password"))
                    {
                        telnet_connecter.Write(caller.sPwd + "\r\n");
                        data = telnet_connecter.Read(">");
                        logger.Trace(data.ToString());

                        //FB 2501 P2P Call Monitoring Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = data.ToString();
                        alert.typeID = 15; //FB 2569-P2P Monitoring
                        ret = false;
                        ret = db.InsertConfP2PAlert(alert);
                        if (!ret) logger.Trace("Point2Point alert insert failed.");
                        //FB 2501 P2P Call Monitoring End
                    }

                    // check if password fails
                    if (data.ToString().ToLower().Contains("password failed"))
                    {
                        logger.Trace("Incorrect password.");
                        return false;
                    }
                    string sendStr = null;
                    switch (telnetOp)
                    {
                        case eTelnetOperation.CONNECT_HANGUP:
                            {
                                if (connectOrDisconnect)
                                {
                                    int numConnectTries = 0;
                                RETRY: string speed = "384";
                                    EquateWithPolycomLineRate(caller.stLineRate.etLineRate, ref speed);
                                    if (caller.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                                    {
                                        // IP call
                                        sendStr = "dial manual " + speed + " " + destAddress +" h323 " + destAddress + " \r\n";
                                    }
                                    else
                                    {
                                        //ISDN/SIP calls
                                        sendStr = "dial manual " + speed + " " + destAddress + " isdn " + destAddress + " \r\n";
                                    }
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString());

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569-P2P Monitoring
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    Thread.Sleep(5000); // delay added between status checks
                                    sendStr = "\r\n getcallstate \r\n";
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                    logger.Trace(connectionStatusOutput);

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = connectionStatusOutput;
                                    alert.typeID = 15; //FB 2569-P2P Monitoring
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End

                                    // If call not connected, retry 2 more times
                                    if (!connectionStatusOutput.Contains("connected"))
                                    {
                                        numConnectTries++;
                                        if (numConnectTries <= 2)
                                        {
                                            goto RETRY;
                                        }
                                    }
                                }
                                else
                                {
                                    sendStr = "hangup all \r\n"; 
                                    logger.Trace("Send cmd : " + sendStr);
                                    telnet_connecter.Write(sendStr);
                                    logger.Trace(telnet_connecter.Read(">").ToString());

                                    //FB 2501 P2P Call Monitoring Start
                                    alert = new NS_MESSENGER.Alert();
                                    alert.confID = conf.iDbID;
                                    alert.instanceID = conf.iInstanceID;
                                    alert.message = telnet_connecter.Read(">").ToString();
                                    alert.typeID = 15; //FB 2569-P2P Monitoring
                                    ret = false;
                                    ret = db.InsertConfP2PAlert(alert);
                                    if (!ret) logger.Trace("Point2Point alert insert failed.");
                                    //FB 2501 P2P Call Monitoring End
                                }
                                break;
                            }
                        case eTelnetOperation.DISPLAY_MESSAGE:
                            {
                                sendStr = "showpopup \"" + messageText + "\" \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                logger.Trace(telnet_connecter.Read(">").ToString());

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = telnet_connecter.Read(">").ToString();
                                alert.typeID = 15; //FB 2569-P2P Monitoring event
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End
                                break;
                            }
                        case eTelnetOperation.CONNECTION_STATUS:
                            {
                                sendStr = "\r\n getcallstate \r\n";
                                logger.Trace("Send cmd : " + sendStr);
                                telnet_connecter.Write(sendStr);
                                connectionStatusOutput = telnet_connecter.Read(">").ToString();
                                logger.Trace(connectionStatusOutput);

                                //FB 2501 P2P Call Monitoring Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = connectionStatusOutput;
                                alert.typeID = 15; //FB 2569-P2P Monitoring
                                ret = false;
                                ret = db.InsertConfP2PAlert(alert);
                                if (!ret) logger.Trace("Point2Point alert insert failed.");
                                //FB 2501 P2P Call Monitoring End
                                break;
                            }
                    }
                }

                telnet_connecter.Close();
                telnet_connecter = null;
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Point to point connection failed. Error - " + e.Message;
                logger.Exception(100, this.errMsg);
                return false;
            }
        }

        //FB 2390 End
        internal bool ConnectDisconnectCall(NS_MESSENGER.Conference conf, NS_MESSENGER.Party caller, bool connectOrDisconnect)
        {
            try
            {
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;

                // get the caller videoconferencing details
                for (int i = 0; i < partyCount; i++)
                {
                    // src party 
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    // go to next party in the queue
                    partyEnumerator.MoveNext();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;

                    if (caller == null)
                    {
                        if (party.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                        {
                            caller = new NS_MESSENGER.Party();
                            caller.iDbId = party.iDbId;
                            caller.sAddress = party.sAddress;
                            caller.sLogin = party.sLogin;
                            caller.sPwd = party.sPwd;
                            caller.etProtocol = party.etProtocol;
                            caller.stLineRate.etLineRate = party.stLineRate.etLineRate;
                            caller.etModelType = party.etModelType;
                            caller.iAPIPortNo = party.iAPIPortNo; //Code added for API Port No
                            caller.ip2pCallid = party.ip2pCallid; //FB 2390
                            caller.iconfnumname = party.iconfnumname; //FB 2390
                            logger.Trace("1-Caller model type: " + caller.etModelType.ToString());

                            // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                            // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                            if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                            {
                                caller.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                            }
                            //FB 2501 P2P Call Monitoring Start
                            caller.iTerminalType = party.iTerminalType;
                            //FB 2501 P2P Call Monitoring End
                            break;
                        }
                    }
                    else
                    {
                        if (party.iDbId == caller.iDbId)
                        {
                            // Party same as caller so skip as we have this party's info already.
                            caller.sAddress = party.sAddress;
                            caller.sLogin = party.sLogin;
                            caller.sPwd = party.sPwd;
                            caller.etProtocol = party.etProtocol;
                            caller.stLineRate.etLineRate = party.stLineRate.etLineRate;
                            caller.etModelType = party.etModelType;
                            caller.iAPIPortNo = party.iAPIPortNo; //Code added for API Port No
                            caller.ip2pCallid = party.ip2pCallid; //FB 2390
                            caller.iconfnumname = party.iconfnumname; //FB 2390
                            logger.Trace("2-Caller model type: " + caller.etModelType.ToString());
                            // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                            // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                            if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                            {
                                caller.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                            }
                            //FB 2501 P2P Call Monitoring Start
                            caller.iTerminalType = party.iTerminalType;
                            //FB 2501 P2P Call Monitoring End
                            break;
                        }
                    }
                }

                System.Collections.IEnumerator partyEnumerator1;
                partyEnumerator1 = conf.qParties.GetEnumerator();
                int partyCount1 = conf.qParties.Count;

                // now cycle thru the endpoints to go thru all callee's.
                for (int i = 0; i < partyCount1; i++)
                {
                    // src party 
                    logger.Trace("inside loop: " + i.ToString());
                    logger.Trace("inside count: " + partyCount1.ToString());
                    logger.Trace("Caller Model: " + caller.etModelType);
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    // go to next party in the queue
                    partyEnumerator1.MoveNext();
                    party = (NS_MESSENGER.Party)partyEnumerator1.Current;

                    if (party.iDbId == caller.iDbId)
                    {
                        // Party same as caller so skip as we have this party's info already.                    
                        continue;
                    }

                    // Telnet is port 23
                    string emptyString = null; bool ret = false;
                    //FB 2390 Start
                    if (caller.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_HDX)//Code modified for API port
                    {
                        //ret = Dial_PolycomCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, party.sAddress, caller.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString);
                        ret = Dial_PolycomCaller(caller, party.sAddress,eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, conf);
                        logger.Trace("outside loop: " + i.ToString());
                    }
                    else if (caller.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_VSX)//Code modified for API port
                    {
                        //ret = Dial_PolycomCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, party.sAddress, caller.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString);
                        ret = Dial_PolycomVSXCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                    }
                    else if (caller.etModelType == NS_MESSENGER.Party.eModelType.TANDBERG)
                    {
                        //ret = Dial_TandbergEdgeCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, party.sAddress, caller.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString);
                        ret = Dial_TandbergEdgeCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, conf);
                    }
                    else if (caller.etModelType == NS_MESSENGER.Party.eModelType.CISCO)
                    {
                        ret = Dial_CiscoCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                    }
                    else if (caller.etModelType == NS_MESSENGER.Party.eModelType.MXP)
                    {
                        ret = Dial_MXPCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                    }
                    else if (caller.etModelType == NS_MESSENGER.Party.eModelType.ViewStation)
                    {
                        ret = Dial_ViewStationCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid,conf);
                    }
                    else
                    {
                        this.errMsg = "Incorrect endpoint model type for p2p calls.";
                        logger.Trace("Incorrect endpoint model type for p2p calls.");
                        return false;
                    }

                    if (!ret)
                    {
                        errMsg += "Telnet call failed for endpoint - " + party.sName;
                        logger.Trace(errMsg);
                        logger.Exception(100, errMsg);

                    }
                    //FB 2390 Start
                    logger.Trace("connectOrDisconnect: " + connectOrDisconnect);
                    if (partyCount > 0 && connectOrDisconnect == true)
                    {
                        partyEnumerator = conf.qParties.GetEnumerator();
                        partyEnumerator.MoveNext();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;
                        party.ip2pCallid = caller.ip2pCallid;
                        logger.Trace("caller.ip2pCallid: " + caller.ip2pCallid);
                    }
                    //FB 2390 End
                }
                //FB 2390 end
                

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool EquateWithPolycomLineRate(NS_MESSENGER.LineRate.eLineRate lineRate, ref string polycomLineRate)
        {
            try
            {
                // equating VRM to Polycom endpoint values            
                #region IP linerate mappings
                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                    polycomLineRate = "64";
                else
                {
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                        polycomLineRate = "128";
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                            polycomLineRate = "256";
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                polycomLineRate = "512";
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                    polycomLineRate = "768";
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1024)
                                        polycomLineRate = "1024";
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                            polycomLineRate = "1152";
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                                polycomLineRate = "1472";
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                    polycomLineRate = "1536";
                                                else
                                                {
                                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                        polycomLineRate = "1920";
                                                    else
                                                    {
                                                        if (lineRate >= NS_MESSENGER.LineRate.eLineRate.M2048)
                                                            polycomLineRate = "2048";
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool SendMessageTextToParty(NS_MESSENGER.Party party, string messageText, NS_MESSENGER.Conference conf)
        {
            try
            {
                string emptyString = null;
                bool ret = false;
                try
                {
                    //FB 2390 Start
                    if (party.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_HDX)
                        //ret = Dial_PolycomCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//Code changed FOr API Port NUmber
                        ret = Dial_PolycomCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, conf);
                    else if (party.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_VSX)
                        //ret = Dial_TandbergEdgeCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//Code changed FOr API Port NUmber
                        ret = Dial_PolycomVSXCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                    else if (party.etModelType == NS_MESSENGER.Party.eModelType.TANDBERG)
                        //ret = Dial_TandbergEdgeCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//Code changed FOr API Port NUmber
                        ret = Dial_TandbergEdgeCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, conf);
                    else if (party.etModelType == NS_MESSENGER.Party.eModelType.CISCO)
                        //ret = Dial_TandbergEdgeCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//Code changed FOr API Port NUmber
                        ret = Dial_CiscoCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                    else if (party.etModelType == NS_MESSENGER.Party.eModelType.MXP)
                        //ret = Dial_TandbergEdgeCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//Code changed FOr API Port NUmber
                        ret = Dial_MXPCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                    else if (party.etModelType == NS_MESSENGER.Party.eModelType.ViewStation)
                        //ret = Dial_TandbergEdgeCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//Code changed FOr API Port NUmber
                        ret = Dial_ViewStationCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                    //FB 2390 End
                }
                catch (Exception)
                {
                    //ret = Dial_PolycomCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//COde changed for APi Port Address
                    ret = Dial_PolycomCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, conf);
                }

                if (!ret)
                {
                    errMsg += "Telnet call failed for endpoint - " + party.sName;
                    logger.Exception(100, errMsg);
                    return false;
                }

                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        
        internal bool isEndpointConnected(ref NS_MESSENGER.Party endpoint, NS_MESSENGER.Party otherValidEndpoint,NS_MESSENGER.Conference conf)
        {
            try
            {
                // Call telnet method
                string connectionStatusOutput = null;
                System.Timers.Timer timer = new System.Timers.Timer(60000);
                //timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed); //GP Issue
                timer.Enabled = true;
                timer.Start();
                //FB 2390 Start
                bool ret = false;
                if (endpoint.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_HDX)
                    //ret = Dial_PolycomCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, null, conf.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput); Code changed fro API port Number
                    ret = Dial_PolycomCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, conf);
                else if (endpoint.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_VSX)
                    //ret = Dial_TandbergEdgeCaller(23, party.sAddress, party.sLogin, party.sPwd, null, NS_MESSENGER.LineRate.eLineRate.K384, party.etProtocol, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString);//Code changed FOr API Port NUmber
                    ret = Dial_PolycomVSXCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                else if (endpoint.etModelType == NS_MESSENGER.Party.eModelType.TANDBERG)
                    //ret = Dial_TandbergEdgeCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, null, conf.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput);Code changed for API Port
                    ret = Dial_TandbergEdgeCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, conf);
                else if (endpoint.etModelType == NS_MESSENGER.Party.eModelType.CISCO)
                    //ret = Dial_TandbergEdgeCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, null, conf.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput);Code changed for API Port
                    ret = Dial_CiscoCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                else if (endpoint.etModelType == NS_MESSENGER.Party.eModelType.MXP)
                    //ret = Dial_TandbergEdgeCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, null, conf.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput);Code changed for API Port
                    ret = Dial_MXPCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                else if (endpoint.etModelType == NS_MESSENGER.Party.eModelType.ViewStation)
                    //ret = Dial_TandbergEdgeCaller(23, caller.sAddress, caller.sLogin, caller.sPwd, null, conf.stLineRate.etLineRate, caller.etProtocol, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput);Code changed for API Port
                    ret = Dial_ViewStationCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                else
                {
                    endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE; //Blue status project
                    this.errMsg = "Endpoint model type is not supported for p2p calls.";
                    logger.Trace("Endpoint model type is not supported for p2p calls.");
                    return false;
                }
                //FB 2390 End
                timer.Stop();
                timer = null;
                if (!ret)
                {
                    errMsg += "Telnet call failed for endpoint - " + endpoint.sName;
                    logger.Exception(100, errMsg);
                }

                // parse the call connection details
                if (connectionStatusOutput == null)
                {
                    // status not available
                    endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE; // No color status
                    return true;
                }
                else
                {
                    if (connectionStatusOutput.Contains("system is not in a call"))
                    {
                        // endpoints are not connected
                        endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT; //Red status
                        return true;
                    }
                    else
                    {
                        // connected endpoints 
                        // fetch status 
                        if (connectionStatusOutput.ToLower().Contains("connected") || connectionStatusOutput.Contains("synced"))
                        {
                            if (connectionStatusOutput.Contains(otherValidEndpoint.sAddress))
                            {
                                // endpoint is connected to the correct other endpoint. 
                                endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT; // Green Status
                                return true;
                            }
                            else
                            {
                                // endpoint is online but not connected to the correct other endpoint. 
                                endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.ONLINE; // Blue status
                                return true;
                            }
                        }
                        else //Glowpoint Issue
                        {
                            //endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT; //Red status
                            return false;
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE; // No color status
                logger.Exception(100, e.Message);
                return false;
            }
        }

    }
}
