﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;

namespace NS_Vidyo
{
    class Vidyo
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        NS_MESSENGER.ConfigParams configParams;
        NS_MESSENGER.VidyoSettings vidyoSettings;
        private NS_DATABASE.Database db;
        public enum roomTypes { VidyoRoom, VidyoPanorama, Legacy };
        public enum userTypes { Normal = 1, Operator, Admin };
        Dictionary<string, int> deptList = null;

        XNamespace env = "http://schemas.xmlsoap.org/soap/envelope/";
        XNamespace nmsAdmin = "http://portal.vidyo.com/admin/v1_1";
        XNamespace nmsUser = "http://portal.vidyo.com/user/v1_1";

        string soapCall = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v1=\"{0}\">"
                      + "<soapenv:Header/>"
                      + "<soapenv:Body>"
                      + "{1}"
                      + "</soapenv:Body>"
                      + "</soapenv:Envelope>";


        internal Vidyo(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
        }

        private bool SendRequest(NS_MESSENGER.VidyoSettings.callType callType, String requestCall, ref String responseCall,String userID, String password)
        {
            string strUrl = vidyoSettings.sPartnerURL;
            Uri mcuUri = null;
            NetworkCredential credential = null;
            System.IO.Stream stream = null;
            byte[] arrBytes = null;
            WebResponse resp = null;
            Stream respStream = null;
            StreamReader rdr = null;
            try
            {
                
                if (strUrl == "")
                {
                    logger.Exception(100, "Emtpy Partner URL");
                    return false;
                }

                if (requestCall == "")
                {
                    logger.Exception(100, "Emtpy Partner URL");
                    return false;
                }

                strUrl = strUrl + @"\" + callType + @"\";

                requestCall = String.Format(soapCall, vidyoSettings.namespaceType[(int)callType], requestCall);

                mcuUri = new Uri(strUrl);
                credential = new NetworkCredential();
                credential.UserName = userID;
                credential.Password = password;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(mcuUri);
                req.Credentials = credential;
                req.Timeout = 60000;
                req.ContentType = "text/xml; charset=UTF-8;";
                req.Method = "POST";
                stream = req.GetRequestStream();
                arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(requestCall);
                stream.Write(arrBytes, 0, arrBytes.Length);
                stream.Close();
                resp = req.GetResponse();
                respStream = resp.GetResponseStream();
                rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                responseCall = rdr.ReadToEnd();
            }
            catch (Exception e)
            {
                string debugString = "Error sending data to portal. Error = " + e.Message;
                responseCall = debugString; //FB 2599
                logger.Exception(100, debugString);
                return false;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
                if (respStream != null)
                {
                    respStream.Close();
                    respStream.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                }
            }
            return true;

        }

        private string ProcessError(NS_MESSENGER.VidyoSettings.callType callType, ref string sResponse)
        {
            String retError = "";
            XElement elem = null;
            try
            {
                elem = XElement.Parse(sResponse);
                retError = "Error Code :" + elem.Element(env + "Body").Element(env + "Fault").Element("faultstring");
                if (retError == null)
                    retError = "Internal Server Error";
                retError = retError.Replace("'", "");


            }
            catch (Exception ex)
            {
                retError = ex.Message;

            }
            return retError;
        }

        private string ProcessResponse(NS_MESSENGER.VidyoSettings.callType callType, string sCallName, ref string sResponse)
        {
            String retError = "";
            XElement elem = null;
            XNamespace xnms = null;
            try
            {
                elem = XElement.Parse(sResponse);
                xnms = nmsAdmin;
                if (callType == NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService)
                    xnms = nmsUser;
                retError = elem.Element(env + "Body").Element(xnms + sCallName).Value;
                if (retError == null)
                    retError = "Internal Server Error";
                retError = retError.Replace("'", "");


            }
            catch (Exception ex)
            {
                retError = ex.Message;

            }
            return retError;
        }

        private string ProcessResponse(NS_MESSENGER.VidyoSettings.callType callType, string sCallName, ref string sResponse,ref XElement responseElem)
        {
            String retError = "";
            XElement elem = null;
            XNamespace xnms = null;
            try
            {
                elem = XElement.Parse(sResponse);
                xnms = nmsAdmin;
                if (callType == NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService)
                    xnms = nmsUser;
                retError = elem.Element(env + "Body").Element(xnms + sCallName).ToString();
                responseElem = elem.Element(env + "Body").Element(xnms + sCallName);
                if (retError == null)
                    retError = "Internal Server Error";
                retError = retError.Replace("'", "");
            }
            catch (Exception ex)
            {
                retError = ex.Message;

            }
            return retError;
        }

        private static String encodeString(String data)
        {
            String encoded = "";
            try
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                encoded = System.Convert.ToBase64String(bytes, Base64FormattingOptions.None);


            }
            catch (Exception ex)
            {
            }

            return encoded;

        }

        internal bool PollVidyo(ref String InXML, ref String OutXML)
        {
            XmlDocument xmldoc = null;
            String orgID = "";
            if(db == null)
                db = new NS_DATABASE.Database(configParams);

            String getMemeberRequest = "<v1:GetMembersRequest></v1:GetMembersRequest>";
            String getMemeberResponse = "";
            String getRoomRequest = "<v1:GetRoomsRequest></v1:GetRoomsRequest>";
            String getRoomResponse = "";
            List<VidyoUserRoom> vmrUsers = null;
            List<VidyoUserRoom> vmrRooms = null;
            

            try
            {
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(InXML);
                if (xmldoc.SelectSingleNode("//PollVidyo/organizationID") != null && xmldoc.SelectSingleNode("//PollVidyo/organizationID").InnerText.Trim() != "")
                    orgID = xmldoc.SelectSingleNode("//PollVidyo/organizationID").InnerText.Trim();

                if (vidyoSettings == null)
                {
                    if (!db.FetchVidyoSettings(ref vidyoSettings, orgID))
                    {
                        logger.Trace("Error getting Vidyo settings");
                        errMsg = "Error getting Vidyo settings";
                        return false;
                    }
                }
                //FB 2599 Start
                if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalAdminService, getMemeberRequest, ref getMemeberResponse, vidyoSettings.sUserName, vidyoSettings.sPassword)) 
                {
                    errMsg = getMemeberResponse;
                    return false;
                }
                if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalAdminService, getRoomRequest, ref getRoomResponse, vidyoSettings.sUserName, vidyoSettings.sPassword)) 
                {
                    errMsg = getRoomResponse;
                    return false;
                }
                //FB 2599 End
                ProcessandConsolidateRooms(ref getRoomResponse, ref getMemeberResponse, ref vmrUsers, ref vmrRooms);
                
                db.SetDepartments(ref deptList);
                db.deptList = deptList;

                if (!db.SaveUpdateVidyoUsers(ref vmrUsers, ref errMsg))
                    logger.Trace("errMsg");

                if (!db.SaveUpdateVidyoRooms(ref vmrRooms, ref errMsg))
                     logger.Trace("errMsg");

            }
            catch (Exception ex)
            {

                logger.Trace("Error getting Vidyo settings");
                errMsg = "Error getting Vidyo settings";
                return false;
            }
            return true;
        }

        internal bool SetConfernece(int confID, int instanceID,int orgID, ref String OutXML)
        {
            XmlDocument xmldoc = null;
            if (db == null)
                db = new NS_DATABASE.Database(configParams);

            String sinviteConfernece = "<v1:InviteToConferenceRequest><v1:conferenceID>{0}</v1:conferenceID>{1}</v1:InviteToConferenceRequest>";
            String sInvite = "<v1:invite>{0}</v1:invite>";
            String sEntity = " <v1:entityID>{0}</v1:entityID>";
            String sjoinConfernece = "<v1:JoinConferenceRequest><v1:conferenceID>{0}</v1:conferenceID>{1}</v1:JoinConferenceRequest>";
             String sjoinConferneceResponse = "JoinConferenceResponse";
            String sinviteConferneceResponse = "InviteToConferenceResponse";
            String sPin = "<v1:pin>{0}</v1:pin>";
            String sRequest = "";
            String sResponse = "";
            String sSubRequest = "";
            String sErrRespone = "";
            VidyoParticipant hostParty = null;
            List<VidyoParticipant> userParties = null;
            List<VidyoParticipant> roomParties = null;
            int iCnt = 0;
            List<VidyoParticipant> sRequests = null;
            try
            {              
                if (vidyoSettings == null)
                {
                    if (!db.FetchVidyoSettings(ref vidyoSettings, orgID.ToString()))
                    {
                        logger.Trace("Error getting Vidyo settings");
                        errMsg = "Error getting Vidyo settings";
                        return false;
                    }
                }

                if (!db.FetchConfforVidyo(confID, instanceID, ref hostParty, ref userParties, ref roomParties))
                {
                    logger.Trace("Error getting Vidyo settings");
                    errMsg = "Error getting Vidyo settings";
                    return false;
                }

                if (hostParty.iRoomID <= 0)
                    return false;

                if (hostParty.sPin.Trim() != "")
                    sPin = String.Format(sPin, hostParty.sPin.Trim());
                else
                    sPin = "";

                sRequests = new List<VidyoParticipant>();

                if (userParties != null)
                {
                    for (iCnt = 0; iCnt < userParties.Count; iCnt++)
                    {
                        sRequest = "";
                        if (userParties[iCnt] == null)
                            continue;

                        if (userParties[iCnt].iMemberID > 0)
                        {
                            sRequest = String.Format(sjoinConfernece, hostParty.iRoomID, sPin);
                            userParties[iCnt].sResponseType = sjoinConferneceResponse;

                        }

                        if (sRequest.Trim() != "")
                        {
                            userParties[iCnt].sRequest = sRequest;
                            sRequests.Add(userParties[iCnt]);

                        }

                    }
                }

                if (roomParties != null)
                {
                    for (iCnt = 0; iCnt < roomParties.Count; iCnt++)
                    {
                        sRequest = "";
                        sSubRequest = "";

                        if (roomParties[iCnt] == null)
                            continue;

                        if (roomParties[iCnt].iRoomID > 0)
                            sSubRequest = String.Format(sEntity, roomParties[iCnt].iRoomID);
                        else if (roomParties[iCnt].sAddress != "")
                            sSubRequest = String.Format(sInvite, roomParties[iCnt].sAddress);
                        else
                            continue;

                        sRequest = String.Format(sinviteConfernece, hostParty.iRoomID,sSubRequest);
                        roomParties[iCnt].sResponseType = sinviteConferneceResponse;

                        if (sRequest.Trim() != "")
                        {
                            roomParties[iCnt].sRequest = sRequest;
                            roomParties[iCnt].sLogin = hostParty.sLogin;
                            roomParties[iCnt].sPassword = hostParty.sPassword;
                            sRequests.Add(roomParties[iCnt]);

                        }
                    }
                }
                bool isError = false;//FB 2717
                if (sRequests != null)
                {
                    for (iCnt = 0; iCnt < sRequests.Count; iCnt++)
                    {
                        sResponse = "";
                        sErrRespone = "";
                        if (sRequests[iCnt] == null)
                            continue;

                       
                        if (sRequests[iCnt].sRequest.Trim() != "")
                        {
                            //if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, sRequests[iCnt].sRequest, ref sResponse, vidyoSettings.sUserName, vidyoSettings.sPassword)) //FB 2717
                            if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, sRequests[iCnt].sRequest, ref sResponse, sRequests[iCnt].sLogin, sRequests[iCnt].sPassword)) //Doubt-Vidyo Settings password.
                            {
                                logger.Trace("Error Sending call" + sRequests[iCnt].sRequest);
                                errMsg = "Error Sending call" + sResponse;
                                isError = true;//FB 2717
                            }
                            else
                            {
                                if (!ProcessResponse(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, sRequests[iCnt].sResponseType, ref sResponse).Contains("OK"))
                                {
                                    sErrRespone = ProcessError(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, ref sResponse);

                                    logger.Trace("Error Connecting call" + sErrRespone);
                                    errMsg = "Error Connecting call" + sErrRespone;
                                    isError = true;
                                }
                            }
                        }
                    }
                    if(isError)//FB 2717
                        return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("Error getting Vidyo settings" + ex.Message);
                errMsg = "Error getting Vidyo settings";
                return false;
            }
            return true;
        }

        internal bool SetConferneceStatus(int confID, int instanceID, int orgID, ref String OutXML)
        {
            XmlDocument xmldoc = null;
            if (db == null)
                db = new NS_DATABASE.Database(configParams);

            VidyoParticipant hostRoom = null;
            List<VidyoParticipant> vmrUsers = new List<VidyoParticipant>();
            List<VidyoParticipant> vmrRooms = new List<VidyoParticipant>();
            int iCnt = 0;
            List<int> lEntityIDs = null;
            try
            {
                if (!GetConferneceParticipants(confID, instanceID, orgID, ref vmrUsers, ref vmrRooms, ref hostRoom, ref lEntityIDs))
                {
                    logger.Trace("Error getting Participants");
                    errMsg = "Error getting Vidyo Participants";
                }

                for(iCnt = 0;iCnt < vmrUsers.Count;iCnt ++)
                {
                    if (vmrUsers[iCnt].imyVRMID > 0)
                        db.UpdateConferenceUserStatus(confID, instanceID, vmrUsers[iCnt].imyVRMID, NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT);

                }

                for (iCnt = 0; iCnt < vmrRooms.Count; iCnt++)
                {
                    if (vmrRooms[iCnt].imyVRMID > 0)
                        db.UpdateConferenceRoomStatus(confID, instanceID, vmrRooms[iCnt].imyVRMID, NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT);

                }
                


            }
            catch (Exception ex)
            {

                logger.Trace("Error getting Vidyo settings");
                errMsg = "Error getting Vidyo settings";
                return false;
            }
            return true;
        }

        internal bool TerminateConfernece(int confID, int instanceID, int orgID, ref String OutXML)
        {
            
            if (db == null)
                db = new NS_DATABASE.Database(configParams);
            VidyoParticipant hostRoom = null;
            List<VidyoParticipant> vmrUsers = new List<VidyoParticipant>();
            List<VidyoParticipant> vmrRooms = new List<VidyoParticipant>();
            List<String> lRequests = null;
            List<int> lEntityIDs = null;
            int iCnt = 0;

            String sRequest = "<v1:LeaveConferenceRequest><v1:conferenceID>{0}</v1:conferenceID><v1:participantID>{1}</v1:participantID></v1:LeaveConferenceRequest>";
            String sResponse = "";
            String sSubRequest = "";
            String sErrRespone = "";
            String sResponseType = "LeaveConferenceResponse";

            try
            {
                if (!GetConferneceParticipants(confID, instanceID, orgID, ref vmrUsers, ref vmrRooms, ref hostRoom, ref lEntityIDs))
                {
                    logger.Trace("Error getting Participants");
                    errMsg = "Error getting Vidyo Participants";
                }

                if (hostRoom.iRoomID <= 0)
                    return false;


                for (iCnt = 0; iCnt < lEntityIDs.Count; iCnt++)
                {
                    if (lEntityIDs[iCnt] > 0)
                    {
                        sResponse = "";
                        sErrRespone = "";
                        sSubRequest = String.Format(sRequest, hostRoom.iRoomID, lEntityIDs[iCnt]);

                        if (sSubRequest.Trim() != "")
                        {

                            if (lRequests == null)
                                lRequests = new List<string>();
                            lRequests.Add(sSubRequest);

                        }

                    }
                }

                   
                    for (iCnt = 0; iCnt <lRequests.Count; iCnt++)
                    {
                        if (lRequests[iCnt].Trim() != "")
                        {

                            if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, lRequests[iCnt].Trim(), ref sResponse, hostRoom.sLogin, hostRoom.sPassword))
                            //if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, lRequests[iCnt].Trim(), ref sResponse, vidyoSettings.sUserName, vidyoSettings.sPassword)) //FB 2717
                            {
                                logger.Trace("Error Sending call" + lRequests[iCnt].Trim());
                                errMsg += "Error Sending call" + lRequests[iCnt].Trim();

                            }
                            else
                            {
                                if (!ProcessResponse(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, sResponseType, ref sResponse).Contains("OK"))
                                {
                                    sErrRespone = ProcessError(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, ref sResponse);

                                    logger.Trace("Error Connecting call" + sErrRespone);
                                    errMsg += "Error Connecting call" + sErrRespone;
                                }


                            }
                        }
                    }
            }
            catch (Exception ex)
            {

                logger.Trace("Error getting Vidyo settings");
                errMsg = "Error getting Vidyo settings";
                return false;
            }
            return true;
        }

        internal bool GetConferneceParticipants(int confID, int instanceID, int orgID, ref List<VidyoParticipant> vmrUsers, ref List<VidyoParticipant> vmrRooms, ref VidyoParticipant hostRoom, ref List<int> lEntityIDs)
        {
            
            if (db == null)
                db = new NS_DATABASE.Database(configParams);
            String sRequest = "<v1:GetParticipantsRequest><v1:conferenceID>{0}</v1:conferenceID></v1:GetParticipantsRequest>";
            String sResponse = "";
            String sErrRespone = "";
            String sResponseType = "GetParticipantsResponse";
            VidyoParticipant hostParty = null;
            List<VidyoParticipant> userParties = null;
            List<VidyoParticipant> roomParties = null;
            int iCnt = 0;
            XElement responseElem = null;
            XElement[] entityElem = null;
            int iEntityID = 0;
            try
            {
                if (vidyoSettings == null)
                {
                    if (!db.FetchVidyoSettings(ref vidyoSettings, orgID.ToString()))
                    {
                        logger.Trace("Error getting Vidyo settings");
                        errMsg = "Error getting Vidyo settings";
                        return false;
                    }
                }

                if (!db.FetchConfforVidyo(confID, instanceID, ref hostParty, ref userParties, ref roomParties))
                {
                    logger.Trace("Error getting Vidyo settings");
                    errMsg = "Error getting Vidyo settings";
                    return false;
                }

                if (hostParty.iRoomID <= 0)
                    return false;

                hostRoom = hostParty;

                sRequest = String.Format(sRequest, hostParty.iRoomID);
              
                sResponse = "";
                sErrRespone = "";


                if (sRequest.Trim() != "")
                {
                    //if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, sRequest, ref sResponse, vidyoSettings.sUserName, vidyoSettings.sPassword)) //FB 2717
                    if (!SendRequest(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, sRequest, ref sResponse, hostParty.sLogin, hostParty.sPassword))
                    {
                        logger.Trace("Error Sending call" + sRequest);
                        errMsg += "Error Sending call" + sRequest;

                    }
                    else
                    {
                        if (!ProcessResponse(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, sResponseType, ref sResponse, ref responseElem).Contains("total"))
                        {
                            sErrRespone = ProcessError(NS_MESSENGER.VidyoSettings.callType.VidyoPortalUserService, ref sResponse);

                            logger.Trace("Error Connecting call" + sErrRespone);
                            errMsg += "Error Connecting call" + sErrRespone;
                            return false;
                        }

                        if (responseElem != null)
                        {
                            entityElem = responseElem.Elements(nmsUser + "Entity").ToArray();

                            for (iCnt = 0; iCnt < entityElem.Length; iCnt++)
                            {
                                iEntityID = 0;

                                if (lEntityIDs == null)
                                    lEntityIDs = new List<int>();

                                iEntityID = (int?)entityElem[iCnt].Element(nmsUser + "participantID") ?? -1;

                                lEntityIDs.Add(iEntityID);
                            }
                            /*

                            vmrUsers = (from userParty in userParties
                                        where lEntityIDs.Contains(userParty.iMemberID)
                                        select userParty).ToList();

                            vmrRooms = (from roomParty in roomParties
                                        where lEntityIDs.Contains(roomParty.iMemberID)
                                        select roomParty).ToList();
                             */ 
                            
 
                        }
                        


                    }
                }



            }
            catch (Exception ex)
            {

                logger.Trace("Error getting Vidyo settings");
                errMsg = "Error getting Vidyo settings";
                return false;
            }
            return true;
        }

        internal bool ProcessandConsolidateRooms(ref String roomResponse, ref String memberResponse, ref List<VidyoUserRoom> vmrUsers, ref List<VidyoUserRoom> vmrRooms)
        {
            String retError = "";
            XElement elem = null;
            XElement elemMembers = null;
            XElement elemRooms = null;
            List<XElement> members = null;
            List<XElement> personalRooms = null;
            List<XElement> publicRooms = null;
            XElement member = null;
            XElement room = null;
            XElement Publicroom = null;
            VidyoUserRoom vmrUser = null;
            int cnt = 0;
            String password = "";
            cryptography.Crypto crypto = null;
            try
            {
                if (deptList == null)
                    deptList = new Dictionary<string, int>();

                if (vmrUsers == null)
                    vmrUsers = new List<VidyoUserRoom>();

                elem = XElement.Parse(memberResponse);
                elemMembers = elem.Element(env + "Body").Element(nmsAdmin + "GetMembersResponse");

                if (vmrRooms == null)
                    vmrRooms = new List<VidyoUserRoom>();

                elem = XElement.Parse(roomResponse);
                elemRooms = elem.Element(env + "Body").Element(nmsAdmin + "GetRoomsResponse");

                members = elemMembers.Elements(nmsAdmin + "member").ToList();

                personalRooms = (from rooms in elemRooms.Elements(nmsAdmin + "room").ToList()
                                 where (string)rooms.Element(nmsAdmin + "RoomType") == "Personal"
                                 select rooms).ToList();


                publicRooms = (from rooms in elemRooms.Elements(nmsAdmin + "room").ToList()
                               where (string)rooms.Element(nmsAdmin + "RoomType") == "Public"
                               select rooms).ToList();

                crypto = new cryptography.Crypto();
                password = crypto.encrypt("myvrm");

                for (cnt = 0; cnt < members.Count; cnt++)
                {
                    member = members[cnt];
                    vmrUser = new VidyoUserRoom();
                    vmrUser.iMemberID = (int?)member.Element(nmsAdmin + "memberID") ?? -1;
                    if (vmrUser.iMemberID <= 0)
                        continue;
                    vmrUser.sPassword = password;
                    vmrUser.sEmail = (string)member.Element(nmsAdmin + "emailAddress") ;
                    vmrUser.sDisplayName = (string)member.Element(nmsAdmin + "displayName") ;
                    vmrUser.sName = (string)member.Element(nmsAdmin + "name") ;
                    vmrUser.sExtension = (string)member.Element(nmsAdmin + "extension") ;
                    vmrUser.sLanguage = (string)member.Element(nmsAdmin + "Language") ;
                    vmrUser.sDescription = (string)member.Element(nmsAdmin + "description") ;
                    vmrUser.iAllowDIrectCalls = ((bool?)member.Element(nmsAdmin + "allowCallDirect") ?? false) ? 1 : 0;
                    vmrUser.sRole = (string)member.Element(nmsAdmin + "RoleName") ;
                    vmrUser.iRoleID = 1;
                    vmrUser.iAdminID = 0;
                    if (vmrUser.sRole != null && vmrUser.sRole != "")
                    {
                        if (Enum.IsDefined(typeof(userTypes), vmrUser.sRole.Trim()))
                            vmrUser.iRoleID = (int)(userTypes)Enum.Parse(typeof(userTypes), vmrUser.sRole.Trim());
                        if (vmrUser.iRoleID > 1)
                            vmrUser.iAdminID = 2;
                    }
                    vmrUser.sGroup = (string)member.Element(nmsAdmin + "groupName") ;
                    if (vmrUser.sGroup != null && vmrUser.sGroup != "")
                    {
                        if (!deptList.Keys.Contains(vmrUser.sGroup.Trim()))
                            deptList.Add(vmrUser.sGroup.Trim(), -1);
                    }

                    vmrUser.sProxy = (string)member.Element(nmsAdmin + "proxyName") ;
                    vmrUser.iPersonalMeeting = ((bool?)member.Element(nmsAdmin + "allowPersonalMeeting") ?? false) ? 1 : 0;

                    if (vmrUser.sExtension != null && vmrUser.sExtension != "")
                    {
                        room = (from personalRoom in personalRooms
                                where (string)personalRoom.Element(nmsAdmin + "extension") == vmrUser.sExtension
                                select personalRoom).FirstOrDefault();

                        if (room != null)
                        {
                            vmrUser.sURL = (string)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "roomURL") ;
                            vmrUser.iLocked = ((bool?)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "isLocked") ?? false) ? 1 : 0;
                            vmrUser.bHasPin = (bool?)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "hasPin") ?? false;
                            if (vmrUser.bHasPin)
                                vmrUser.sPin = (string)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "roomPIN");

                            vmrUser.iRoomID = (int?)room.Element(nmsAdmin + "roomID") ?? -1;
                        }
                    }

                    vmrUser.iVMR = 0;//Fb2262T
                    if (Enum.IsDefined(typeof(roomTypes), vmrUser.sRole.Trim()))
                        vmrRooms.Add(vmrUser);
                    else
                        vmrUsers.Add(vmrUser);
                }

                for (cnt = 0; cnt < publicRooms.Count; cnt++)
                {
                    room = publicRooms[cnt];
                    vmrUser = new VidyoUserRoom();
                    vmrUser.iRoomID = (int?)room.Element(nmsAdmin + "roomID") ?? -1;
                    if (vmrUser.iRoomID <= 0)
                        continue;
                    vmrUser.sPassword = password;
                    vmrUser.sRole = (string)room.Element(nmsAdmin + "RoomType") ;
                    vmrUser.iRoleID = 1;
                    vmrUser.iAdminID = 0;
                    vmrUser.sDisplayName = (string)room.Element(nmsAdmin + "name") ;
                    vmrUser.sName = (string)member.Element(nmsAdmin + "name") ;
                    vmrUser.sOwnerName = (string)room.Element(nmsAdmin + "ownerName") ;

                    Publicroom = (from personalRoom in members
                                  where (((string)personalRoom.Element(nmsAdmin + "emailAddress")).ToUpper().Trim() == vmrUser.sEmail.ToUpper().Trim() && (string)personalRoom.Element(nmsAdmin + "RoomType") == "Personal")
                                  select personalRoom).FirstOrDefault();

                    if (Publicroom != null)
                    {
                        vmrUser.iOwnerID = (int?)member.Element(nmsAdmin + "memberID") ?? -1;
                    }

                    vmrUser.sExtension = (string)room.Element(nmsAdmin + "extension") ;
                    vmrUser.sGroup = (string)room.Element(nmsAdmin + "groupName") ;

                    if (vmrUser.sGroup != null && vmrUser.sGroup != "")
                    {
                        if (!deptList.Keys.Contains(vmrUser.sGroup.Trim()))
                            deptList.Add(vmrUser.sGroup.Trim(), -1);

                    }

                    vmrUser.sURL = (string)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "roomURL") ;
                    vmrUser.iLocked = ((bool?)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "isLocked") ?? false) ? 1 : 0;
                    vmrUser.bHasPin = (bool?)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "hasPin") ?? false;
                    if (vmrUser.bHasPin)
                        vmrUser.sPin = (string)room.Element(nmsAdmin + "RoomMode").Element(nmsAdmin + "roomPIN");

                    //Fb2262T
                    //vmrUsers.Add(vmrUser);
                    vmrUser.iVMR = 1;
                    vmrRooms.Add(vmrUser);
                }
            }
            catch (Exception ex)
            {


            }
            return true;

        }
       
    }

    class VidyoUserRoom
    {
        internal int iScheduleMinutes;
        internal int iTimeZoneID;
        internal int iRoleID;
        internal int iAdminID;
        internal int iGroupID;
        internal int iAllowDIrectCalls, iPersonalMeeting, iMemberID, iRoomID, iLanguageID, iEptID, iLocked, iVMR, iOwnerID;//FB 2262T
        internal int[] iDepartmentsIDs;
        internal string sName, sDisplayName, sEmail, sPassword, sRole, sResponseString, sGroup, sExtension, sURL, sPin, sDescription, sLanguage, sProxy, sOwnerName;
        internal DateTime dExpiryDate;
        internal bool bHasPin;
    }

    class VidyoParticipant
    {
        internal int iMemberID, iRoomID, imyVRMID, iParticipantID;
        internal string sPin = "", sType = "", sPassword = "", sLogin = "", sAddress = "", sRequest = "", sResponseType = "";
    }
}

