﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace NS_YorktelScheduling
{
    class YorktelScheduling
    {
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        private NS_MESSENGER.ESSettings SysESSet = null;
        public string errMsg = "";

        string soapCall ="<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
	                  + "<soap:Header>"
		              + "<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
			          + "<wsse:UsernameToken>"
				      + "    <wsse:Username>{0}</wsse:Username>"
				      + "    <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">{1}</wsse:Password>"
			          + "</wsse:UsernameToken>"
		              + "</wsse:Security>"
	                  + "</soap:Header>"
                      + "<soap:Body xmlns:ns1=\"{2}\">"
                      + "{3}"
                      + "</soap:Body>"
                      + "</soap:Envelope>";

        int timeLimit = 5;// try time for every timieLimit interval
                      

        internal YorktelScheduling(NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
		}

        private bool CreateRequest(ref YTSConference conf, ref string confXML)
        {
            String PartyXML = ""; 
            String[] partyArr = null;
            int ESId = 0;
            try
            {
                //conf.sRequestID = SysESSet.sCustomerID + conf.dtModifiedDateTime.ToString("mm") + conf.iDbNumName.ToString();
                conf.sRequestID = Guid.NewGuid().ToString(); //FB 2363

                int.TryParse(conf.sExternalSchedulingID, out ESId);
                partyArr = conf.sRoomString.Split('†'); // FB 2720 

                if (conf.iDeleted > 0 || ( partyArr.Length <= 1 && conf.iPushed > 0) ) 
                {
                    confXML = "<ns1:conferenceMessage	xmlns=\"http://ytcservices.yorktel.com/services/scheduling\" 	xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">"
                         + "&lt;CancelConferenceRequest	xmlns:ns0=\"http://ytcservices.yorktel.com/services/scheduling\"	xmlns=\"http://ytcservices.yorktel.com/services/scheduling\">"
                         + "&lt;ns0:RequestId>" + conf.sRequestID.ToString() + "&lt;/ns0:RequestId>"
                         + "&lt;ns0:PartnerCode>" + SysESSet.sPartnerName + "&lt;/ns0:PartnerCode>"
                         + "&lt;ns0:ConferenceId>" + conf.iDbNumName + "&lt;/ns0:ConferenceId>"
                         + "&lt;ns0:CustomerId>" + SysESSet.sCustomerID + "&lt;/ns0:CustomerId>"
                         + "&lt;ns0:ModificationDateTime>" + conf.dtModifiedDateTime.ToString(@"yyyy-MM-dd\THH:mm:ss\Z") + "&lt;/ns0:ModificationDateTime>"
                         + "&lt;/CancelConferenceRequest>"
                         + "</ns1:conferenceMessage>";
                }
                else
                {
                    Boolean isTelepresence = false;
                    Boolean isAudioAddon = false;
                    //partyArr = conf.sRoomString.Split(',');
                    //for (int fl = 1; fl <= 2; fl++)
                    for (int fl = 1; fl <= 1; fl++) // for now we are sending Room only in Participant List
                    {
                        if (fl == 1 && partyArr.Length <= 1)
                            return false;

                        if (fl == 2)
                            partyArr = conf.sUserString.Split('†'); // FB 2720 

                        for (int i = 0; i < partyArr.Length; i++)
                        {
                            if (partyArr[i].IndexOf('§') > 2) // FB 2720 
                            {
                                PartyXML += "&lt;ns1:Participant xmlns:ns1=\"http://ytcservices.yorktel.com/schemas/types\">"
                                        + "  &lt;ns1:ParticipantName>" + partyArr[i].Trim().Split('§')[0] + "&lt;/ns1:ParticipantName>"  // FB 2720 
                                        + "  &lt;ns1:IsHost>false&lt;/ns1:IsHost>"
                                        + "&lt;/ns1:Participant>";

                                if (fl == 1 && partyArr[i].Trim().Split('§')[2] == "1") // FB 2720 
                                    isTelepresence = true;

                                if (fl == 2 && partyArr[i].Trim().Split('§')[3] == "1") // FB 2720 
                                    isAudioAddon = true;
                            }
                        }
                    }

                    String concerigeOptions = "1";

                    if (conf.sMeetandGreet == "1")
                        concerigeOptions += ",0-MEET &amp;amp; GREET";

                    if (conf.sDedicatedvnoc == "1")
                        concerigeOptions += ",0-DEDICATED VNOC";

                    if (conf.sAvtech == "1")
                        concerigeOptions += ",0-AV TECH";

                    if(isAudioAddon == true)
                        concerigeOptions += ",0-AUDIO CASCADE";

                    partyArr = concerigeOptions.Split(',');

                    for (int i = 1; i < partyArr.Length; i++)
                    {
                        PartyXML += "&lt;ns1:Participant xmlns:ns1=\"http://ytcservices.yorktel.com/schemas/types\">"
                                + "  &lt;ns1:ParticipantName>" + partyArr[i].ToString() + "&lt;/ns1:ParticipantName>"
                                + "  &lt;ns1:IsHost>false&lt;/ns1:IsHost>"
                                + "&lt;/ns1:Participant>";
                    }

                    confXML = "<ns1:conferenceMessage	xmlns=\"http://ytcservices.yorktel.com/services/scheduling\" 	xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">"
                          + "&lt;SaveConferenceRequest	xmlns:ns0=\"http://ytcservices.yorktel.com/services/scheduling\"	xmlns=\"http://ytcservices.yorktel.com/services/scheduling\">"
                          + "&lt;ns0:RequestId>" + conf.sRequestID.ToString() + "&lt;/ns0:RequestId>"
                          + "&lt;ns0:PartnerCode>" + SysESSet.sPartnerName + "&lt;/ns0:PartnerCode>"
                          + "&lt;ns0:ConferenceId>" + conf.iDbNumName + "&lt;/ns0:ConferenceId>"
                          + "&lt;ns0:CustomerId>" + SysESSet.sCustomerID + "&lt;/ns0:CustomerId>"
                          + "&lt;ns0:VIPConference>" + conf.isVIP + "&lt;/ns0:VIPConference>"
                          + "&lt;ns0:ConferenceTitle>" + conf.sDbName.Replace("'", "") + "&lt;/ns0:ConferenceTitle>";

                    if (conf.sDedicatedvnoc == "1")
                        confXML += "&lt;ns0:ConferenceType>CnfDed&lt;/ns0:ConferenceType>";
                    else if (isTelepresence == true)
                        confXML += "&lt;ns0:ConferenceType>TelePres&lt;/ns0:ConferenceType>";
                    else
                        confXML += "&lt;ns0:ConferenceType>CnfStd&lt;/ns0:ConferenceType>";                    
                    
                    confXML += "&lt;ns0:MCUName>" + conf.sMcuName + "&lt;/ns0:MCUName>" //FB 2363
                       + "&lt;ns0:ConferenceChair>" + conf.sHostName.Replace("'", "\''") + "&lt;/ns0:ConferenceChair>" // FB 2720 
                       + "&lt;ns0:ConferenceChairEmail>" + conf.sHostEmail + "&lt;/ns0:ConferenceChairEmail>"
                       + "&lt;ns0:CustomerEmployeeId>1&lt;/ns0:CustomerEmployeeId>"
                       + "&lt;ns0:CostCenter>1&lt;/ns0:CostCenter>"
                       + "&lt;ns0:SetupDateTime>" + conf.dtStartDateTime.AddSeconds(-45).ToString(@"yyyy-MM-dd\THH:mm:ss\Z") + "&lt;/ns0:SetupDateTime>"
                       + "&lt;ns0:StartDateTime>" + conf.dtSetUpDateTime.AddSeconds(-45).ToString(@"yyyy-MM-dd\THH:mm:ss\Z") + "&lt;/ns0:StartDateTime>"
                       + "&lt;ns0:EndDateTime>" + conf.dtEndDateTime.AddSeconds(-45).ToString(@"yyyy-MM-dd\THH:mm:ss\Z") + "&lt;/ns0:EndDateTime>" //FB 2363 dtStartDateTime.AddMinutes(conf.iDuration)
                       + "&lt;ns0:ModificationDateTime>" + conf.dtModifiedDateTime.ToString(@"yyyy-MM-dd\THH:mm:ss\Z") + "&lt;/ns0:ModificationDateTime>"
                       + "&lt;ns0:Comments>" + conf.sDescription + "&lt;/ns0:Comments>"
                       + "&lt;ns0:ParticipantList>" + PartyXML.Replace("'", "") + "&lt;/ns0:ParticipantList>"
                       + "&lt;/SaveConferenceRequest>"
                       + "</ns1:conferenceMessage>";
                }
            }
            catch (Exception ex)
            {
                logger.Trace("CreateStruct:" + ex.StackTrace);
                errMsg = "CreateStruct:" + ex.StackTrace;
                return false;
            }
            return true;
        }

        internal bool InsertConferenceEventinmyVRM(string inXML, ref string outXml)
        {
            List<YTSConference> confs = null;
            String confXML = "";
            YTSConference conference = null;
            NS_MESSENGER.sysExtSchedEvent evnt = null;
            String createType = "Create";
            XmlDocument xmlDOc = null;
            String confID,instanceID = "";
            String statusMessage = "Event logged for {0} of {1}";
            String status = "N";
            String eventTrigger = "myVRM";
            try
            {
                xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(inXML);
                confID = xmlDOc.SelectSingleNode("//SetExternalScheduling/confID").InnerText;

                if(confID == "")
                {
                    logger.Exception(100, "Invalid Conference : Conference ID is empty");
                    outXml = logger.FetchErrorMsg(100, "Invalid Conference : Conference ID is empty");
                    return false;
                }

                if(confID.Split(',').Length > 1)
                    instanceID = confID.Split(',')[1];

                confID = confID.Split(',')[0];

                confs =  new List<YTSConference>();                
                
                bool ret = db.FetchytsConf(ref confs, confID, instanceID);
                String ordID = "";

                for (int i = 0; i < confs.Count; i++)
                {
                    conference = confs[i];

                    if (ordID != conference.iOrgID.ToString())
                    {
                        SysESSet = new NS_MESSENGER.ESSettings();
                        db.FetchSystemESSettings(ref SysESSet, "YTC", conference.iOrgID.ToString());
                    }

                    ret = CreateRequest(ref conference, ref confXML);
                    if (ret && confXML != "")
                    {
                        createType = "Create";

                        if (conference.iDeleted > 0)
                            createType = "Cancel";                            
                        else if (conference.sExternalSchedulingID != "" && conference.sExternalSchedulingType != "")
                            createType = "Update";

                        conference.sRequestString = String.Format(soapCall, SysESSet.sUserName, SysESSet.sPassword, "http://ytcservices.yorktel.com/services/scheduling", confXML);
                        evnt = new NS_MESSENGER.sysExtSchedEvent();
                        evnt.sCustomerID = SysESSet.sCustomerID;
                        evnt.sTypeID = createType;
                        evnt.sRequestID = conference.sRequestID.ToString().Trim();
                        evnt.sStatus = status;
                        evnt.sStatusMessage = String.Format(statusMessage, createType, conference.sExternalName.Replace("'",""));
                        evnt.sRequestCall = conference.sRequestString;
                        evnt.sResponseCall = "";
                        evnt.sConfnumname = conference.iDbNumName.ToString();
                        evnt.sEventTrigger = eventTrigger;
                        evnt.dEventTime = DateTime.Now;
                        evnt.dStatusDate = DateTime.Now;
                        evnt.sOrgID = conference.iOrgID;

                        if (!db.InsertExternalEvent(ref evnt))
                        {
                            logger.Exception(100, "Insert request failed for conference :" + conference.sExternalName);
                            outXml = logger.FetchErrorMsg(100, "Insert request failed for conference :" + conference.sExternalName);
                        }
                    }

                    ordID = conference.iOrgID.ToString();
                }
            }
            catch (Exception ex)
            {
                logger.Trace("InsertConferenceEventinmyVRM" + ex.StackTrace);
                errMsg = ("InsertConferenceEventinmyVRM" + ex.StackTrace);
                return false;
            }
            return true;
        }

        internal bool TriggerEvent(string inXML, ref string outXml)
        {
            
            NS_MESSENGER.sysExtSchedEvent evnt = null;
            XmlDocument xmlDOc = null;
            String status = "N";
            String eventTrigger,requestID = "";
            String requestIds = "";
            String oldRequestId = "";
            String[] requestIdarry = null;
            int iRetryCount = -1;
            try
            {

                xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(inXML);
                requestIds = xmlDOc.SelectSingleNode("//TriggerEvent/RequestID").InnerText;
                eventTrigger = xmlDOc.SelectSingleNode("//TriggerEvent/TriggerFrom").InnerText;
                requestIdarry = requestIds.Split(',');

                for (int rCnt = 0; rCnt < requestIdarry.Length; rCnt++)
                {
                    requestID = requestIdarry[rCnt];

                    if (requestID != "")
                    {
                        oldRequestId = requestID.Replace("'", "");
                        evnt = new NS_MESSENGER.sysExtSchedEvent();
                        evnt.sRequestID = requestID.Replace("'","");
                        if (!db.FetchExternalEventforRequestID(ref evnt))
                        {
                            logger.Exception(100, "Fetch request failed for request  :" + evnt.sRequestID);
                            outXml = logger.FetchErrorMsg(100, "Fetch request failed for request  :" + evnt.sRequestID);
                        }
                        iRetryCount = evnt.sRetryCount;
                        evnt.sStatus = status;
                        evnt.sRetryCount = 0;
                        evnt.sEventTrigger = eventTrigger;
                        evnt.dEventTime = DateTime.Now;
                        evnt.dStatusDate = DateTime.Now;
                        if (iRetryCount == 0)
                        {
                            evnt.sRequestID = Guid.NewGuid().ToString();
                            evnt.sRequestCall = evnt.sRequestCall.Replace(oldRequestId, evnt.sRequestID);

                            if (!db.UpdateExternalEvent(ref evnt, oldRequestId))
                            {
                                logger.Exception(100, "Trigger request failed for request  :" + evnt.sRequestID);
                                outXml = logger.FetchErrorMsg(100, "Fetch request failed for request  :" + evnt.sRequestID);
                            }

                        }
                        else
                        {

                            if (!db.UpdateExternalEvent(ref evnt))
                            {
                                logger.Exception(100, "Trigger request failed for request  :" + evnt.sRequestID);
                                outXml = logger.FetchErrorMsg(100, "Fetch request failed for request  :" + evnt.sRequestID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("TriggerEvent" + ex.StackTrace);
                errMsg = ("TriggerEvent" + ex.StackTrace);
                return false;
            }
            return true;
        }

        internal bool TriggerEvent()
        {
            List<NS_MESSENGER.sysExtSchedEvent> eventList = null;
            NS_MESSENGER.sysExtSchedEvent evnt = null;
            int evtCnt = 0,retryCnt = 0,totalTime = 0,totalUplimit = 0,totalLolimit = 0;            
            DateTime tryDate = DateTime.Now;
            try
            {
                SysESSet = new NS_MESSENGER.ESSettings();
                db.FetchSystemESSettings(ref SysESSet,"YTC","");
                db.FetchExternalNewEventList(ref eventList);
                if (eventList != null)
                {
                    for (evtCnt = 0; evtCnt < eventList.Count; evtCnt++)
                    {
                        retryCnt = 0;
                        totalTime = 0;
                        evnt = eventList[evtCnt];

                        if (evnt.sRetryCount > 0)
                        {
                            //check for every 5 minutes
                            retryCnt = evnt.sRetryCount;
                            totalTime = timeLimit * retryCnt;
                            totalUplimit = totalTime + 1;
                            totalLolimit = totalTime - 1;
							
                            if ((!(tryDate.Subtract(evnt.dStatusDate).TotalMinutes >= totalLolimit && tryDate.Subtract(evnt.dStatusDate).TotalMinutes <= totalUplimit)) && tryDate.Subtract(evnt.dStatusDate).TotalMinutes < 15)
                                continue;
                        }
                        
                        if (!SendRequest(ref evnt))
                            logger.Exception(100, "Send request failed for request ID :" + evnt.sRequestID);

                        if (!ProcessRespose(ref evnt))
                            logger.Exception(100, "Process request failed for request ID :" + evnt.sRequestID);

                        if (!db.UpdateExternalEvent(ref evnt))
                            logger.Exception(100, "Update request failed for request ID :" + evnt.sRequestID);

                        if (!db.UpdateConferenceforEvent(ref evnt, SysESSet.sESType))
                            logger.Exception(100, "Update request failed for request ID :" + evnt.sRequestID);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("TriggerEvent" + ex.StackTrace);
                errMsg = ("TriggerEvent" + ex.StackTrace);
                return false;
            }
            return true;
        }

        private bool SendRequest(ref NS_MESSENGER.sysExtSchedEvent evnt)
        {
            string strUrl = SysESSet.sPartnerURL;
            Uri mcuUri = null;
            NetworkCredential credential = null;
            System.IO.Stream stream = null;
            byte[] arrBytes = null;
            WebResponse resp = null;
            Stream respStream = null;
            StreamReader rdr = null;
            DateTime tryDate = DateTime.Now;
            try
            {
                if (strUrl == "")
                {
                    logger.Exception(100, "Emtpy Partner URL");
                    return false;
                }

                if (evnt.sRequestCall == "")
                {
                    logger.Exception(100, "Emtpy Partner URL");
                    return false;
                }

                mcuUri = new Uri(strUrl);
                credential = new NetworkCredential();
                credential.UserName = SysESSet.sUserName;
                credential.Password = SysESSet.sPassword;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(mcuUri);
                req.Credentials = credential;
                req.Timeout = SysESSet.sTimeout;
                req.ContentType = "text/xml; charset=UTF-8;";
                req.Method = "POST";
                stream = req.GetRequestStream();
                arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(evnt.sRequestCall);
                stream.Write(arrBytes, 0, arrBytes.Length);
                stream.Close();
                tryDate = DateTime.Now;
                resp = req.GetResponse();
                respStream = resp.GetResponseStream();
                rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                evnt.sResponseCall = rdr.ReadToEnd();
            }
            catch (Exception e)
            {
                string debugString = "Error sending data to portal. Error = " + e.Message;
                evnt.sStatusMessage = debugString;
                evnt.sStatus = "N";
                errMsg = debugString;
                evnt.sRetryCount += 1;

                logger.Trace("Retry Duration_" + evnt.sRequestID + ":" + tryDate.Subtract(evnt.dStatusDate).TotalMinutes);
                if (tryDate.Subtract(evnt.dStatusDate).TotalMinutes >= 15 || evnt.sRetryCount > 3)
                {
                    evnt.sStatus = "E"; 
                    SendMail(ref evnt); 
                }
                logger.Exception(100, debugString);
                return false;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
                if (respStream != null)
                {
                    respStream.Close();
                    respStream.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                }
            }
            return true;

        }

        private bool ProcessRespose(ref NS_MESSENGER.sysExtSchedEvent evnt)
        {
            
            String response = "";
            XElement elem =  null;
            XElement parentElem = null;
            XElement childElem = null;
            XNamespace env = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace nms = "http://ytcservices.yorktel.com/services/scheduling";
            XNamespace ytcTypes = "http://ytcservices.yorktel.com/schemas/types";
            String invocationResponse = "";
            String Response = "";
            String request = "SaveConferenceResponse";
            String errCode = "";
            String errMessage = "";
            String stsMessage = "Conference : Created Successfully";
            String status = "C";
            DateTime tryDate = DateTime.Now;
            Int32 erroCode = 0;
                
            try
            {
                if (evnt.sResponseCall.Trim() != "")
                {
                    if (evnt.sTypeID == "Cancel")
                    {
                        request = "CancelConferenceResponse";
                        stsMessage = "Conference : Canceled Successfully";
                    }
                    try
                    {
                        elem = XElement.Parse(evnt.sResponseCall);

                        Response = elem.Element(env + "Body").Value;

                        childElem = XElement.Parse(Response);

                        invocationResponse = childElem.Element(nms + "Result").Element(nms + "InvocationSuccess").Value;

                        if (invocationResponse != "")
                        {
                            if (!Boolean.Parse(invocationResponse))
                            {
                                errCode = childElem.Element(nms + "Result").Element(nms + "Exception").Element(ytcTypes + "ErrorCode").Value;
                                int.TryParse(errCode.Trim(), out erroCode);
                                errMessage = childElem.Element(nms + "Result").Element(nms + "Exception").Element(ytcTypes + "ErrorMessage").Value;
                                errMessage = errMessage.Replace("'", "");

                                stsMessage = "Error Code: " + errCode + " Error: " + errMessage;
                                status = "E";

                                if (erroCode < 1000) // if error code is  less than 1000
                                    throw new Exception(stsMessage);
                                else if (erroCode >= 1000)
                                {
                                    status = "E";
                                    SendMail(ref evnt); 
                                }
                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        // error in processing the request
                        string debugString = "Error sending data to portal. Error = " + ex1.Message;
                        stsMessage = debugString;
                        status = "N";
                        errMsg = debugString;
                        evnt.sRetryCount += 1;

                        logger.Trace("Retry Duration_" + evnt.sRequestID + ":" + tryDate.Subtract(evnt.dStatusDate).TotalMinutes);
                        if (tryDate.Subtract(evnt.dStatusDate).TotalMinutes >= 15 || evnt.sRetryCount > 3)
                        {
                            status = "E";
                            SendMail(ref evnt);                             
                        }
                        logger.Exception(100, debugString);
                    }


                    evnt.sStatus = status;
                    evnt.sStatusMessage = stsMessage;
                    evnt.dStatusDate = DateTime.Now;
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                errMsg = "Process Repsonse" + e.Message;
                return false;
            }
            return true;
        }

        #region FetchConfDetails
        /// <summary>
        /// FetchConfDetails
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        private bool FetchConfDetails(ref NS_MESSENGER.sysExtSchedEvent evnt, ref String Message)
        {
            XElement elem = null;
            XElement parentElem = null;
            XElement childElem = null;
            XNamespace env = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace nms = "http://ytcservices.yorktel.com/services/scheduling";
            XNamespace ytcTypes = "http://ytcservices.yorktel.com/schemas/types";
            String Response = "";
            String Confid = "",ConfName = "",Host = "";
            DateTime Time = DateTime.Now;
            
            try
            {
                if (evnt.sRequestCall.Trim() != "")
                {
                    elem = XElement.Parse(evnt.sRequestCall);
                    Response = elem.Element(env + "Body").Value;
                    childElem = XElement.Parse(Response);

                    Confid = childElem.Element(nms + "ConferenceId").Value;
                    ConfName = childElem.Element(nms + "ConferenceTitle").Value;
                    Host = childElem.Element(nms + "ConferenceChair").Value;
                    DateTime.TryParse(childElem.Element(nms + "StartDateTime").Value,out Time);
                    
                    ConfName = ConfName.Replace("(" + Confid + ")", "");
                    Message = Message.Replace("{4}", ConfName).Replace("{5}", Confid).Replace("{6}", Time.ToString("MM/dd/yyyy hh:mm tt"))
                                     .Replace("{10}", Host);
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                errMsg = "FetchConfDetails" + e.Message;
                return false;
            }
            return true;
        }
        #endregion
		
        #region SendMail
        private void SendMail(ref NS_MESSENGER.sysExtSchedEvent evnt)
        {
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            NS_EMAIL.Email sendEmail = new NS_EMAIL.Email(configParams);            
            String emailBody = "", emailSubject = "";   
            try 
	        {
                if (SysESSet.sPartnerEmail != null && SysESSet.sPartnerEmail.Trim() != "")
                {
                    logger.Trace("SendEsConfFailMail.....");                 
                    db.FetchEmailString(evnt.sOrgID, ref emailBody, ref emailSubject, 6);
                    FetchConfDetails(ref evnt, ref emailBody);

                    if (emailBody.Trim() != "")
                    {
                        email.orgID = evnt.sOrgID;
                        email.To = SysESSet.sPartnerEmail;
                        email.From = "";
                        email.Body = emailBody.Replace("{2}", SysESSet.sPartnerName);
                        email.Subject = emailSubject;
                        bool ret = sendEmail.SendESConfFailureEmail(email);
                        if (!ret)
                        {
                            //errMsg += sendEmail.errMsg;
                            logger.Trace("SMTP connection failed.");
                        }
                    }
                }
	        }
	        catch (Exception e)
	        {
                logger.Exception(100, e.Message);
                errMsg = "Send Email" + e.Message;
	        }
        }
                           
                            #endregion

    }
    class YTSConference : NS_MESSENGER.Conference
    {
        internal int iPushed,iDeleted;
        internal string sExternalSchedulingID, sExternalSchedulingType, sRoomString, sUserString, sRequestString, sResponseString;
        internal String sRequestID;
    }

}
