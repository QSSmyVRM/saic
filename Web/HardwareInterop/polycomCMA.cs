//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/* FILE : PolycomAccord.cs
 * DESCRIPTION : All Polycom MCU api commands are stored in this file. 
 * AUTHOR : Kapil M
 */
namespace NS_POLYCOMCMA
{
    #region References
    using System;
    using System.Xml;
    using System.Net;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using System.Web;
    using System.Diagnostics;
    using System.Xml.Linq;
    #endregion
    class PolycomCMA
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        NS_MESSENGER.ConfigParams configParams;

        internal PolycomCMA(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
        }

        private bool SendTransaction_overXMLHTTP(NS_MESSENGER.MCU mcu, string sendXML, ref string receiveXML, string method, ref string esID)
        {
            // Used for CMA. 
            // Connects to bridge on HTTP . Default is port 80.

            // if string empty return error

            String restURI = @"/api/rest/reservations";
            String[] delimitedArr = { @"\" };
            String error = "";
            String credentials = "";

            if (sendXML.Length < 1)
            {
                logger.Trace("No data being sent so it is delete command");

            }

            try
            {
                if (esID.Trim() != "")
                    restURI += @"/" + esID;

                int tryAgainCount = 0;
                bool tryAgain = false;
                do
                {
                    tryAgain = false;

                    logger.Trace("*********SendXML***********");
                    logger.Trace(sendXML);

                    string strUrl = "http://" + mcu.sIp.Trim() + ":" + mcu.iHttpPort + restURI;//Code added for Api port

                    if (mcu.iHttpPort == 443)
                        strUrl = "https://" + mcu.sIp.Trim() + restURI;


                    Uri mcuUri = new Uri(strUrl);
                    //NetworkCredential mcuCredential = new NetworkCredential();
                    //if (mcu.sLogin.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries).Length > 1)
                    //{
                    //    mcuCredential.UserName = mcu.sLogin.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries)[1];
                    //    mcuCredential.Password = mcu.sPwd;
                    //    mcuCredential.Domain = mcu.sLogin.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries)[0];
                    //}
                    //CredentialCache cache = new CredentialCache();
                    //cache.Add(new Uri(strUrl), "Basic", mcuCredential);
                    credentials = mcu.sLogin + ":" + mcu.sPwd;
                    credentials = encodeString(credentials);

                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                    ServicePointManager.Expect100Continue = false;

                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);
                    //req.Credentials = cache;

                    req.MediaType = "HTTP::/1.1";
                    req.Headers["Authorization"] = "Basic " + credentials;
                    req.Method = method;
                    req.KeepAlive = true;
                    if (sendXML != "")
                    {
                        req.ContentType = "application/vnd.plcm.plcm-reservation+xml";
                        req.Headers.Add("Accept-Encoding", "gzip,deflate");//x-www-form-urlencoded";
                        System.IO.Stream stream = req.GetRequestStream();
                        byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                        stream.Write(arrBytes, 0, arrBytes.Length);
                        stream.Close();
                    }
                    if (method == "PUT")
                        req.Headers[HttpRequestHeader.IfMatch] = GetETagValue(strUrl, credentials);

                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                    if (resp.Headers["Location"] != null)
                    {
                        esID = resp.Headers["Location"].ToString();
                        esID = esID.Split('/')[esID.Split('/').Length - 1];

                    }
                    /*Stream respStream = resp.GetResponseStream();
                    StreamReader rdr = new StreamReader(respStream);
                    receiveXML = rdr.ReadToEnd();
                    logger.Trace("*********ReceiveXML***********");
                    logger.Trace(receiveXML);*/
                }
                while (tryAgain == true);

                return true;

            }

            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }

                if (error == "")
                    throw wex;

                this.errMsg = ProcessError(error);
                return false;



            }

            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error sending data to bridge. Error = " + e.Message;
                debugString = "SendXML = " + sendXML + " , ReceiveXML = " + receiveXML;
                logger.Exception(100, debugString);
                return false;
            }

        }



        internal bool SetConference(NS_MESSENGER.Conference conf)
        {
            logger.Trace("Entering SetConference function...");
            bool ret = false;
            String esID = "";
            try
            {

                string reservationXML = null; ret = false;
                ret = CreateXML_Reservation(ref conf, ref reservationXML);
                if (!ret || reservationXML.Length < 1)
                {
                    logger.Exception(100, "Creation of CreateXML_Reservation failed . commonXML = " + reservationXML);
                    return false;
                }


                //Send the xml to the bridge
                ret = false;
                string responseXml = null;

                ret = SendTransaction_overXMLHTTP(conf.cMcu, reservationXML, ref responseXml, "POST", ref esID);
                if (!ret)
                {
                    logger.Exception(100, "SendTransaction failed . confXml = " + reservationXML);
                    return false;
                }

                // Parse the response.

                if (esID.Trim() != "")
                {

                    ret = false;
                    ret = ProcessXML_Reservation(ref conf, ref esID);
                    if (!ret)
                    {
                        logger.Exception(100, "ProcessXML_Reservation failed . responseXml = " + responseXml);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool ExtendConference(NS_MESSENGER.Conference conf, int extendTime)
        {
            logger.Trace("Entering Extend Conference function...");
            bool ret = false;
            String esID = "";
            try
            {
                if (conf != null)
                {
                    conf.iDuration = conf.iDuration + extendTime;
                    esID = conf.ESId;

                    string reservationXML = null; ret = false;
                    ret = CreateXML_Reservation(ref conf, ref reservationXML);
                    if (!ret || reservationXML.Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed . commonXML = " + reservationXML);
                        return false;
                    }


                    //Send the xml to the bridge
                    ret = false;
                    string responseXml = null;

                    ret = SendTransaction_overXMLHTTP(conf.cMcu, reservationXML, ref responseXml, "PUT", ref esID);
                    if (!ret)
                    {
                        logger.Exception(100, "SendTransaction failed . confXml = " + reservationXML);
                        return false;
                    }

                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #region Reservation

        //FB 2390 Start

        private bool CreateXML_Reservation(ref NS_MESSENGER.Conference conf, ref string reservationXml)
        {
            try
            {
                String[] delimitedArr = { @"\" };
                String confStartTime = "", ConfEndTime = "", domain = "local", username = "sched_004";

                TimeSpan timeDiff = conf.dtStartDateTimeInUTC.Subtract(DateTime.UtcNow);
                if (timeDiff.Days != 0 || timeDiff.Hours != 0 || timeDiff.Minutes >= 5)
                    confStartTime = conf.dtStartDateTimeInUTC.ToString(@"yyyy-MM-dd\THH:mm:ss\Z");    // scheduled reservation
                else
                    confStartTime = DateTime.UtcNow.AddSeconds(15).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");// immediate conf. so it needs adjustment.

                ConfEndTime = conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");

                if (conf.cMcu.sLogin.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries).Length > 1)
                {
                    domain = conf.cMcu.sLogin.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries)[0];
                    username = conf.cMcu.sLogin.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries)[1];
                }


                reservationXml = "<plcm-reservation xmlns=\"urn:com:polycom:api:rest:plcm-reservation\" xmlns:ns2=\"urn:com:polycom:api:rest:plcm-reserved-participant\" xmlns:ns1=\"urn:com:polycom:api:rest:plcm-reserved-participant-list\">"
                              + CreatePartyListXML(ref conf)
                              + "<name>" + conf.sDbName + "</name>";
                if (conf.ESId.Trim() != "")
                    reservationXml += "<reservation-id>" + conf.ESId + "</reservation-id>";

                reservationXml += "<template-name>Factory Template</template-name>"
                              + "<send-email>false</send-email>"
                              + "<supported-language-enum>ENGLISH</supported-language-enum>"
                              + "<start-time>" + confStartTime + "</start-time>"
                              + "<end-time>" + ConfEndTime + "</end-time>" //2012-03-17T02:01:01+05:30                              
                              + "</plcm-reservation>";

            }

            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }//end of Reservation XML

        #region CreatePartyListXML
        /// <summary>
        /// CreatePartyListXML
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        private String CreatePartyListXML(ref NS_MESSENGER.Conference conf)
        {
            String PartyListXML = "", partys = "";
            Boolean setConfowner = false;
            try
            {
                while (conf.qParties.Count > 0)
                {
                    NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                    party1 = (NS_MESSENGER.Party)conf.qParties.Dequeue();

                    if (conf.stLineRate.etLineRate < party1.stLineRate.etLineRate)
                        party1.stLineRate.etLineRate = conf.stLineRate.etLineRate;


                    partys += SinglePartyXML(ref conf, ref party1, ref setConfowner);
                }

                PartyListXML = "<ns1:plcm-reserved-participant-list>"
                             + partys
                             + "</ns1:plcm-reserved-participant-list>";
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return " ";
            }
            return PartyListXML;
        }
        #endregion

        #region SinglePartyXML
        /// <summary>
        /// SinglePartyXML
        /// </summary>
        /// <param name="Conf"></param>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        internal String SinglePartyXML(ref NS_MESSENGER.Conference Conf, ref NS_MESSENGER.Party partyObj, ref Boolean setConfOwner)
        {
            String PartyXML = "", videoBitRate = "", partyAddress = "", phonePrefix = "";
            try
            {
                logger.Trace("Party = " + partyObj.sName);
                if (partyObj.sName == null)
                {
                    this.errMsg = "Invalid participant name.";
                    return "";
                }
                if (partyObj.sName.Length < 3)
                {
                    this.errMsg = "Invalid participant name.";
                    return "";
                }

                EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);
                PartyXML = "<ns2:plcm-reserved-participant>";

                if (!setConfOwner)
                {
                    PartyXML += "<ns2:username>admin_004</ns2:username>"
                              + "<ns2:domain>LOCAL</ns2:domain>";

                }

                PartyXML += "<ns2:participant-name>" + partyObj.sName + "</ns2:participant-name>"
                         + "<ns2:connection-type-enum>" + AddressType(ref partyObj.etAddressType) + "</ns2:connection-type-enum>"
                         + "<ns2:dial-direction-enum>" + DialDirection(ref partyObj.etConnType) + "</ns2:dial-direction-enum>"
                         + "<ns2:dial-number>" + partyObj.sAddress + "</ns2:dial-number>"
                         + "<ns2:chairperson>false</ns2:chairperson>";
                if (partyObj.bIsLecturer)
                {
                    PartyXML += "<ns2:lecturer>true</ns2:lecturer>";
                }
                else
                    PartyXML += "<ns2:lecturer>false</ns2:lecturer>";
                if (!setConfOwner)
                {
                    PartyXML += "<ns2:conf-owner>true</ns2:conf-owner>";
                    setConfOwner = true;
                }
                else
                    PartyXML += "<ns2:conf-owner>false</ns2:conf-owner>";
                PartyXML += "<ns2:email-address>" + partyObj.sName.Replace("(", "").Replace(")", "").Trim() + "@myvrm.com" + "</ns2:email-address>"
                         + "</ns2:plcm-reserved-participant>";



            }
            catch (Exception e)
            {
                logger.Exception(100, e.StackTrace);
                return "";
            }
            return PartyXML;
        }
        #endregion

        #region CallType
        /// <summary>
        /// CallType
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        // <xs:enumeration value="IN_PERSON"/>  
        //<xs:enumeration value="AUDIO"/>  
        //<xs:enumeration value="VIDEO"/> 
        private String CallType(ref NS_MESSENGER.Party.eCallType eType)
        {
            String CallType = "IN_PERSON";
            if (eType == NS_MESSENGER.Party.eCallType.VIDEO)
                CallType = "VIDEO";
            else if (eType == NS_MESSENGER.Party.eCallType.AUDIO)
                CallType = "AUDIO";

            return CallType;
        }
        #endregion

        #region AddressType
        /// <summary>AddressType
        //  <xs:enumeration value="H323"/>  
        //  <xs:enumeration value="ISDN"/>  
        //  <xs:enumeration value="SIP"/>  
        //  <xs:enumeration value="H323_E164"/>  
        //  <xs:enumeration value="H323_ANNEX_O"/>  
        //  <xs:enumeration value="H323_ID"/>  
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        private String AddressType(ref NS_MESSENGER.Party.eAddressType eAddType)
        {
            String AddType = "";

            if (eAddType == NS_MESSENGER.Party.eAddressType.H323_ID)
                AddType = "H323";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.IP_ADDRESS)
                AddType = "H323";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.ISDN_PHONE_NUMBER)
                AddType = "SIP";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.E_164)
                AddType = "SIP";

            return AddType;
        }
        #endregion

        #region DialDirection
        /// <summary>DialDirection
        //<xs:enumeration value="DIAL_OUT"/>  
        // <xs:enumeration value="DIAL_IN"/>   
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        private String DialDirection(ref NS_MESSENGER.Party.eConnectionType ConnType)
        {
            String DialDir = "";

            if (ConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                DialDir = "DIAL_IN";
            else if (ConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                DialDir = "DIAL_OUT";

            return DialDir;
        }
        #endregion

        //FB 2390 End

        private bool ProcessXML_Reservation(ref NS_MESSENGER.Conference conf, ref string esID)
        {
            try
            {
                // update the conf last run date time
                NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
                db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, esID);
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_PartyList(ref NS_MESSENGER.Conference conf, ref string partyList)
        {
            try
            {
                //Create the participant xml list 
                string party = null;
                logger.Trace("Create Party List count =" + conf.qParties.Count.ToString());

                //cycle through each participant.
                while (conf.qParties.Count > 0)
                {
                    //individual party info
                    NS_MESSENGER.Party partyObj = new NS_MESSENGER.Party();
                    partyObj = (NS_MESSENGER.Party)conf.qParties.Dequeue();

                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < partyObj.stLineRate.etLineRate)
                    {
                        partyObj.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

                    bool ret = CreateXML_Party(ref party, partyObj);
                    if (!ret) party = null;

                    // append party info to partylist
                    partyList += party;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool isValidIPAddress(string ip)
        {
            bool ret = true;
            string[] octets = ip.Split('.');
            if (octets.Length != 4)
                ret = false;
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    int dig = Convert.ToInt32(octets[i]);
                    if (dig < 0 || dig > 255)
                    {
                        ret = false;
                        break;
                    }
                }
            }
            return ret;
        }


        private bool CreateXML_Party(ref string partyXml, NS_MESSENGER.Party partyObj)
        {
            logger.Trace("Party = " + partyObj.sName);
            if (partyObj.sName == null)
            {
                this.errMsg = "Invalid participant name.";
                return false;
            }
            if (partyObj.sName.Length < 3)
            {
                this.errMsg = "Invalid participant name.";
                return false;
            }

            partyXml = null;
            try
            {
                if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                {
                    // ip participant 
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";
                    partyXml += "<PHONE_LIST></PHONE_LIST>";
                    partyXml += "<INTERFACE>h323</INTERFACE>";
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    //Meet me method
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>auto</MULTI_RATE>";
                    partyXml += "<NET_CHANNEL_NUMBER>auto</NET_CHANNEL_NUMBER>";

                    //if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    }
                    //else
                    //{
                    //	if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //		partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>"; 
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice

                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    // Check if the address is an alias or an IP

                    if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.H323_ID || partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.IP_ADDRESS)
                    {
                        // IP address or H323.ID 
                        partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                        partyXml += "<IP>" + partyObj.sAddress.Trim() + "</IP>";
                    }
                    else
                    {
                        if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.E_164)
                        {
                            // It is an E164 alias.
                            partyXml += "<ALIAS><NAME>" + partyObj.sAddress + "</NAME>";
                            partyXml += "<ALIAS_TYPE>e164</ALIAS_TYPE></ALIAS>";
                            partyXml += "<IP>0.0.0.0</IP>";
                        }
                        else
                        {
                            logger.Exception(100, "Invalid participant address type");
                        }
                    }
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";
                    partyXml += "<MCU_PHONE_LIST/>";
                    partyXml += "<BONDING_PHONE/>";

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME></SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";

                    //FB 1742 start.
                    string videoBitRate = "automatic";
                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);
                        if (!ret)
                        {
                            logger.Trace("Invalid Video Bit Rate.");
                        }
                    }
                    //FB 1742 end

                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_BIT_RATE>384</VIDEO_BIT_RATE>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_BIT_RATE>" + videoBitRate + "</VIDEO_BIT_RATE>";
                    }
                    partyXml += "<AGC>true</AGC>";

                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                    {
                        partyXml += "<IP_QOS>";
                        partyXml += "<QOS_ACTION>from_service</QOS_ACTION>";
                        partyXml += "<QOS_DIFF_SERV>precedence</QOS_DIFF_SERV>";
                        partyXml += "<QOS_IP_AUDIO>4</QOS_IP_AUDIO>";
                        partyXml += "<QOS_IP_VIDEO>4</QOS_IP_VIDEO>";
                        partyXml += "<QOS_TOS>delay</QOS_TOS>";
                        partyXml += "</IP_QOS>";
                        partyXml += "<RECORDING_PORT>no</RECORDING_PORT>";
                        partyXml += "<LAYOUT_TYPE>conference</LAYOUT_TYPE>";
                        partyXml += "<PERSONAL_LAYOUT>1x1</PERSONAL_LAYOUT>";
                        partyXml += "<PERSONAL_FORCE_LIST />";
                        partyXml += "<VIP>false</VIP> ";
                        partyXml += "<CONTACT_INFO_LIST />";
                        partyXml += "<LISTEN_VOLUME>5</LISTEN_VOLUME>";
                        partyXml += "<SIP_ADDRESS />";
                        partyXml += "<SIP_ADDRESS_TYPE>uri_type</SIP_ADDRESS_TYPE>";
                        partyXml += "<WEB_USER_ID>0</WEB_USER_ID>";
                        partyXml += "<UNDEFINED>false</UNDEFINED>";
                        partyXml += "<BACKUP_SERVICE_NAME />";
                        partyXml += "<BACKUP_SUB_SERVICE_NAME />";
                        partyXml += "<DEFAULT_TEMPLATE>false</DEFAULT_TEMPLATE>";
                        partyXml += "<NODE_TYPE>terminal</NODE_TYPE>";
                        partyXml += "<ENCRYPTION_EX>auto</ENCRYPTION_EX>";
                        partyXml += "<H323_PSTN>false</H323_PSTN>";
                        partyXml += "<EMAIL />";
                        partyXml += "<IS_RECORDING_LINK_PARTY>false</IS_RECORDING_LINK_PARTY>";
                        partyXml += "<USER_IDENTIFIER_STRING />";
                        partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                    }
                    partyXml += "</PARTY>";
                }
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                {
                    // ISDN participant
                    string partyAddress = "";  //FB 1740 - Audio Add-on implementation for Polycom sites
                    string phonePrefix = "";
                    partyAddress = partyObj.sAddress.Trim();

                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code start

                        //partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        if (partyAddress.Trim().Contains("G"))
                        {
                            int Gindex = partyAddress.IndexOf("G");
                            partyAddress = partyAddress.Substring(0, Gindex);
                        }
                        if (partyAddress.Trim().Contains("D"))
                        {
                            int Dindex = partyAddress.IndexOf("D");
                            partyAddress = partyAddress.Substring(0, Dindex);

                            //FB 2655 DTMF Start
                            //Audio Dial-in prefix is added before the address
                            //phonePrefix = configParams.audioDialNoPrefix;
                            phonePrefix = partyObj.AudioDialInPrefix;
                            //FB 2655 DTMF End
                        }

                        //if (partyObj.etVideoEquipment == NS_MESSENGER.Party.eVideoEquipment.RECORDER)
                        //{ 
                        //    //send gatewayAddress
                        //}
                        // check for gateway  
                        if (partyObj.sAddress.Trim().Contains("G"))
                        {
                            // gateway exists
                            if (partyObj.sAddress.Trim().Contains("D"))
                            {
                                // dtmf sequence also exists. Hence, gateway value needs to be extracted between G & D.
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                int Dindex = partyObj.sAddress.IndexOf("D");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1, Dindex - Gindex - 1);
                            }
                            else
                            {
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1);
                            }
                        }
                        // check for dtmf sequence
                        if (partyObj.sAddress.Trim().Contains("D") && partyObj.bAudioBridgeParty)//FB 2655
                        {
                            // dtmf sequence exists                            
                            int Dindex = partyObj.sAddress.IndexOf("D");
                            partyObj.sDTMFSequence = partyObj.sAddress.Substring(Dindex + 1);

                            // FB case#1632
                            // GENERIC STRING: ,,,,,,,,CONFCODE,#,,,,,*,,,,,LEADERPIN,# ,,,,,1 (MGC uses "p" for pauses)

                            // add the pre-ConfCode DTMF codes
                            partyObj.sDTMFSequence = partyObj.preConfCode + partyObj.sDTMFSequence; //FB 2655
                            //partyObj.sDTMFSequence = partyObj.cMcu.preConfCode + ":" + partyObj.sDTMFSequence;

                            // add the pre-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", partyObj.preLPin); //FB 2655
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", (":"+ partyObj.cMcu.preLPin));

                            // add the post-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() + partyObj.postLPin; //FB 2655
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() +":"+ partyObj.cMcu.postLPin;
                        }

                        if (partyObj.sDTMFSequence != null)
                        {

                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + phonePrefix.Trim() + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }
                            else
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }


                        }
                        else
                        {
                            partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        }
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code end
                    }
                    else
                    {
                        partyXml += "<PHONE_LIST></PHONE_LIST>";
                    }

                    // isdn 
                    partyXml += "<INTERFACE>isdn</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>disabled</MULTI_RATE>";

                    // ISDN channels.
                    string polycomLineRate = "channel_6";
                    bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";

                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    //	partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>"; 
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                        {
                            partyXml += "<BONDING_PHONE/>"; //FB 1740 Audio Add-on implementation for Polycom
                        }
                        else
                        {
                            //partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>"; //Commented during FB 1740
                            partyXml += "<BONDING_PHONE>" + partyAddress.Trim() + "</BONDING_PHONE>"; //FB 1740 - Audio Add-on implementation for Polycom
                        }
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName.Trim() + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    if (partyObj.sDTMFSequence != null)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7) //FB 1740
                        {
                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<USER_IDENTIFIER_STRING>" + partyObj.sDTMFSequence.Trim() + "</USER_IDENTIFIER_STRING>";
                            }
                            else
                            {
                                partyXml += "<USER_IDENTIFIER_STRING/>";
                            }
                            //partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                        }
                    }
                    else
                    {
                        partyXml += "<USER_IDENTIFIER_STRING/>";
                    }
                    partyXml += "</PARTY>";
                }
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.MPI)
                {
                    // MPI participant
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    //if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    //    partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                    //else
                    partyXml += "<PHONE_LIST></PHONE_LIST>";

                    // V35 
                    partyXml += "<INTERFACE>mpi</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<CONNECTION>dial_out</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<CONNECTION>direct</CONNECTION>";

                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>disabled</BONDING>";
                    partyXml += "<MULTI_RATE>enabled</MULTI_RATE>";

                    // V35 channels.
                    string polycomLineRate = "channel_6";
                    bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";


                    // Video Protocol
                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //        partyXml += "<VIDEO_PROTOCOL></VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    // Video conf = framed , Audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>";
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }
                    // mcu service name
                    partyXml += "<SERVICE_NAME>CODEC</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    partyXml += "</PARTY>";
                }
                return true;
            }
            catch (Exception e)
            {
                // log the error and trash this participant info.
                string debugString = "Participant Not Added . Error = " + e.Message;
                debugString += ". PartyXML = " + partyXml;
                logger.Exception(100, debugString);
                return false;
            }
        }
        #endregion

        #region Ongoing Conference List

        private bool ProcessXML_OngoingConfList(string responseXml, ref Queue conflist)
        {
            try
            {

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }

        private bool ProcessXML_FindOngoingConf(string responseXml, ref NS_MESSENGER.Conference conf, ref bool searchResult)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool ProcessXML_FindReservationConf(string responseXml, ref NS_MESSENGER.Conference conf, ref bool searchResult)
        {


            try
            {
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);

                // Don't return yet. Maybe the conf is not an ongoing conf but a reservation.
                // So let's check that out as well.
                try
                {

                    //Load the response xml recd from the bridge and parse it
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXml);

                    XmlNodeList nodelist;

                    // if description = OK , then continue processing
                    string description = xd.SelectSingleNode("//RESPONSE_TRANS_RES_LIST/RETURN_STATUS/DESCRIPTION").InnerXml.Trim();

                    if (description.Length < 0 || description.Contains("OK") != true)
                    {
                        this.errMsg = description;
                        return false; //error
                    }

                    //### IGNORE THIS CHECK FOR NOW ###
                    //check for any status change
                    //node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
                    //string changed = node.InnerXml;
                    //changed = changed.Trim();
                    //if (changed.CompareTo("false")==0) 
                    //	return(0); //no change in conf list since prev request so just return

                    // Retreive all conferences	
                    nodelist = xd.SelectNodes("//RESPONSE_TRANS_RES_LIST/GET_RES_LIST/RES_SUMMARY_LS/RES_SUMMARY");

                    //cycle through each conference one-by-one
                    foreach (XmlNode node1 in nodelist)
                    {
                        logger.Trace("in node...");

                        //name of conference
                        string confBridgeName = node1.SelectSingleNode("NAME").InnerText.Trim();
                        logger.Trace("Conf bridge name = " + confBridgeName);
                        conf.sDbName = conf.sDbName.Trim();
                        logger.Trace("Conf db name = " + conf.sDbName);
                        logger.Trace("Comparing conf names...");
                        if (confBridgeName.CompareTo(conf.sDbName) == 0)
                        {
                            logger.Trace(" Conference match successful...");

                            XmlNode id = node1.SelectSingleNode("ID");
                            conf.stStatus.iMcuID = Int32.Parse(id.InnerText);
                            logger.Trace("conf.m_status.m_mcuID = " + conf.stStatus.iMcuID);

                            //### NOT REQUIRED ###
                            //XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
                            //VRM_ERROR_HANDLER.ErrorHandler.DebugToEventLog(conf.InnerText);
                            //XmlNode status = node1.SelectSingleNode("CONF_STATUS");
                            //if (status.ChildNodes.Item(1).InnerText == "false") //CONF_EMPTY = true/false
                            //	conf.m_status.m_isEmpty = false;
                            //else 
                            //	conf.m_status.m_isEmpty = true;
                            //### ADDITIONAL STATUS PARAMS . NOT REQUIRED. ###
                            //if (status.ChildNodes.Item(4).InnerText == "true") //RESOURCES_DEFICIENCY = true/false
                            //	conf.m_status.m_isResourceDeficient = true;
                            //else
                            //	conf.m_status.m_isResourceDeficient = false;
                            //status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
                            //status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
                            //status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
                            //status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
                            //status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	
                            // total number of connected parties
                            //XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
                            //conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);

                            // conference has been found & all info retreived.
                            searchResult = true;
                            return true;
                        }
                    }

                    this.errMsg = "Conference not found on the MCU";
                    return false;
                }
                catch (Exception em)
                {
                    logger.Exception(100, em.Message);
                    return false;
                }
            }
        }

        #endregion

        #region Ongoing Conference
        private bool CreateXML_OngoingConf(ref string confXml, NS_MESSENGER.Conference conf)
        {

            return true;
        }

        private bool ProcessXML_FindOngoingParty(string responseXml, ref NS_MESSENGER.Party party)//string partyNameFromDb,ref int partyIdOnMcu)
        {

            try
            {

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            //FB 1552
            //return true; 
            return true;
        }

        #endregion

        #region Add/Delete Party
        private bool CreateXML_AddParty(ref string addPartyXml, NS_MESSENGER.Party party, string confIdOnMcu)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool ProcessXML_AddParty(ref string responseXml)
        {
            try
            {

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }

        #endregion

        #region Terminal Services Operations
        private bool CreateXML_TerminateParty(ref string deletePartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_ChangeDisplayLayoutOfParty(ref string newLayoutPartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, int layout)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool CreateXML_SetDisplayLayoutTypeToPersonal(ref string newLayoutTypeXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool EquateVideoCodecs(NS_MESSENGER.Conference.eVideoCodec videoCodec, ref string polycomVideoCodec)
        {
            try
            {
                // equating to VRM to Polycom values
                if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.AUTO)
                {
                    polycomVideoCodec = "auto";
                }
                else
                {
                    if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H261)
                    {
                        polycomVideoCodec = "h261";
                    }
                    else
                    {
                        if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H263)
                        {
                            polycomVideoCodec = "h263";
                        }
                        else
                        {
                            polycomVideoCodec = "h264";
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool EquateAudioCodec(NS_MESSENGER.Conference.eAudioCodec audioCodec, ref string polycomAudioCodec)
        {
            try
            {

                if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_56)
                    polycomAudioCodec = "g722_56";
                else
                {
                    if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G728)
                        polycomAudioCodec = "g728";
                    else
                    {
                        if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_32)
                            polycomAudioCodec = "g722_32";
                        else
                        {
                            if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_24)
                                polycomAudioCodec = "g722_24";
                            else
                            {
                                if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G711_56)
                                    polycomAudioCodec = "g711_56";
                                else
                                {
                                    if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.AUTO)
                                        polycomAudioCodec = "auto";
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool EquateLineRate(NS_MESSENGER.LineRate.eLineRate lineRate, bool isIP, ref string polycomLineRate)
        {
            try
            {
                // equating VRM to Polycom values


                if (isIP)
                {
                    #region IP linerate mappings
                    //Modified during FB 1742
                    switch (lineRate)
                    {
                        case NS_MESSENGER.LineRate.eLineRate.K64:
                            {
                                polycomLineRate = "64";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K128:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K192:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K256:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K320:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K384:
                            {
                                polycomLineRate = "384";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K512:
                            {
                                polycomLineRate = "512";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K768:
                            {
                                polycomLineRate = "768";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1024:
                            {
                                polycomLineRate = "1024";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1152:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1250:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1472:
                            {
                                polycomLineRate = "1472";
                                break;
                            }

                        case NS_MESSENGER.LineRate.eLineRate.M1536:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1792:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1920:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2048:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2560:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3072:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3584:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M4096:
                            {
                                polycomLineRate = "4096";
                                break;
                            }
                        default:
                            {
                                // default is 384 kbps
                                polycomLineRate = "384";
                                break;
                            }
                    }
                    #endregion

                    #region IP linerate mappings - Commented during FB 1742

                    //if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                    //    polycomLineRate = "64";
                    //else
                    //{
                    //    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                    //        polycomLineRate = "128";
                    //    else
                    //    {
                    //        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                    //            polycomLineRate = "256";
                    //        else
                    //        {
                    //            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                    //                polycomLineRate = "512";
                    //            else
                    //            {
                    //                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                    //                    polycomLineRate = "768";
                    //                else
                    //                {
                    //                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                    //                        polycomLineRate = "1152";
                    //                    else
                    //                    {
                    //                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                    //                            polycomLineRate = "1472";
                    //                        else
                    //                        {
                    //                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                    //                                polycomLineRate = "1536";
                    //                            else
                    //                            {
                    //                                if (lineRate >= NS_MESSENGER.LineRate.eLineRate.M1920)
                    //                                    polycomLineRate = "1920";
                    //                            }
                    //                        }

                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    #region ISDN linerate mappings
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                        polycomLineRate = "channel_1";
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                            polycomLineRate = "channel_2";
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                                polycomLineRate = "channel_4";
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                    polycomLineRate = "channel_8";
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                        polycomLineRate = "channel_12";
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                            polycomLineRate = "channel_18";
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                                polycomLineRate = "channel_23";
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                    polycomLineRate = "channel_24";
                                                else
                                                {
                                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                        polycomLineRate = "channel_30";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }


        private bool EquateVideoLayout(int videoLayoutID, ref string polycomVideoLayoutValue)
        {

#if COMMENT_DO_NOT_DELETE
Dan's email 10/22/06 - MGC equivalent mappings of Codian layout code :
enumeration 	1x1 Codian # 1 � Classic and Quad Mode 
enumeration 	1x2 No Codian layout avail � Classic and Quad Mode Image 16
enumeration 	2x1 No Codian layout avail � Classic and Quad Mode 
enumeration 	2x2 Codian # 2 � Classic and Quad Mode
enumeration 	1and5 Codian # 5 � Classic Mode
enumeration 	3x3 Codian # 3 � Classic and Quad Mode
enumeration 	1x2Ver Codian # 16 � Classic Mode
enumeration 	1x2Hor Codian # 12 � Classic Mode
enumeration 	1and2Hor No Codian layout avail � Classic Mode Image 61
enumeration 	1and2Ver Codian # 17 � Classic Mode
enumeration 	1and3Hor No Codian layout avail � Classic Mode Image 62
enumeration 	1and3Ver Codian # 18 � Classic Mode
enumeration 	1and4Ver Codian # 19 � Classic Mode
enumeration 	1and4Hor No Codian layout avail � Classic Mode Image 63
enumeration 	1and8Central Codian # 24 � Classic Mode
enumeration 	1and8Upper No Codian layout avail � Classic Mode Image 60
enumeration 	1and2HorUpper Codian # 13 � Classic Mode
enumeration 	1and3HorUpper Codian # 14 � Classic Mode
enumeration 	1and4HorUpper Codian # 15 � Classic Mode
enumeration 	1and8Lower Codian # 20 � Classic Mode
enumeration 	1and7 Codian # 6 � Classic Mode
enumeration 	4x4 Codian # 4 � Quad Mode
enumeration 	2and8 Codian # 25 � Quad Mode
enumeration 	1and12 Codian # 33 � Quad Mode
enumeration 	1x1Qcif No Codian layout avail � Quad Mode
#endif
            polycomVideoLayoutValue = "1x1";
            try
            {
                switch (videoLayoutID)
                {
                    case 1:
                        {
                            polycomVideoLayoutValue = "1x1";
                            break;
                        }
                    case 2:
                        {
                            polycomVideoLayoutValue = "2x2";
                            break;
                        }
                    case 3:
                        {
                            polycomVideoLayoutValue = "3x3";
                            break;
                        }
                    case 4: //FB 2335
                        {
                            polycomVideoLayoutValue = "4x4";
                            break;
                        }
                    case 5:
                        {
                            polycomVideoLayoutValue = "1and5";
                            break;
                        }
                    case 6:
                        {
                            polycomVideoLayoutValue = "1and7";
                            break;
                        }
                    // case 7-11 : layout is not supported 
                    case 12:
                        {
                            polycomVideoLayoutValue = "1x2Hor";
                            break;
                        }
                    case 13:
                        {
                            polycomVideoLayoutValue = "1and2HorUpper";
                            break;
                        }
                    case 14:
                        {
                            polycomVideoLayoutValue = "1and3HorUpper";
                            break;
                        }
                    case 15:
                        {
                            polycomVideoLayoutValue = "1and4HorUpper";
                            break;
                        }
                    case 16:
                        {
                            // Changed from 1x2Ver to 1x2 on BTBoces request (case 1606)
                            polycomVideoLayoutValue = "1x2";
                            break;
                        }
                    case 17:
                        {
                            polycomVideoLayoutValue = "1and2Ver";
                            break;
                        }
                    case 18:
                        {
                            polycomVideoLayoutValue = "1and3Ver";
                            break;
                        }
                    case 19:
                        {
                            polycomVideoLayoutValue = "1and4Ver";
                            break;
                        }
                    case 20:
                        {
                            polycomVideoLayoutValue = "1and8Lower";
                            break;
                        }

                    //case 21-23 : layout is not supported
                    case 24:
                        {
                            polycomVideoLayoutValue = "1and8Central";
                            break;
                        }
                    case 25:
                        {
                            polycomVideoLayoutValue = "2and8";
                            break;
                        }
                    //case 26-32: layout is not supported					
                    case 33:
                        {
                            polycomVideoLayoutValue = "1and12";
                            break;
                        }
                    case 60://FB 2335
                        {
                            polycomVideoLayoutValue = "1and8Upper";
                            break;
                        }
                    case 61://FB 2335
                        {
                            polycomVideoLayoutValue = "1and2Hor";
                            break;
                        }
                    case 62://FB 2335
                        {
                            polycomVideoLayoutValue = "1and3Hor";
                            break;
                        }
                    case 63://FB 2335
                        {
                            polycomVideoLayoutValue = "1and4Hor";
                            break;
                        }





                    //case 33-42: layout is not supported					

                    //case 101-109 are all custom to btboces for MGC accord bridges
                    case 101:
                        {
                            polycomVideoLayoutValue = "1x1";
                            break;
                        }
                    case 102:
                        {
                            polycomVideoLayoutValue = "1x2";//"1x2Ver"; 
                            break;
                        }
                    case 103:
                        {
                            polycomVideoLayoutValue = "1and2HorUpper"; //"1x2"; 
                            break;
                        }
                    case 104:
                        {
                            polycomVideoLayoutValue = "2x2"; //"1and2HorUpper"; 
                            break;
                        }
                    case 105:
                        {
                            polycomVideoLayoutValue = "1and5"; //"1and2Ver"; 
                            break;
                        }
                    case 106:
                        {
                            polycomVideoLayoutValue = "1x2Ver"; //"2x2"; 
                            break;
                        }
                    case 107:
                        {
                            polycomVideoLayoutValue = "1and2Ver";//"1and3Ver"; 
                            break;
                        }
                    case 108:
                        {
                            polycomVideoLayoutValue = "1and3Ver"; //"1and5"; 
                            break;
                        }
                    case 109:
                        {
                            polycomVideoLayoutValue = "1and7";
                            break;
                        }

                    default:
                        {
                            // layout not supported
                            this.errMsg = "This video layout is not supported on Polycom MCU.";
                            return false;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

        private bool CreateXML_ChangeDisplayLayoutOfConference(ref string newLayoutPartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int layout)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_SetLectureMode(ref string lectureModeXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, string lecturer)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool CreateXML_MuteParty(ref string mutePartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool audioMute)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_TerminateConference(ref string terminateXml, NS_MESSENGER.MCU mcu, int confIdOnMcu)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool ProcessXML_CommonConfOps(ref string responseXml)
        {
            try
            {

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }

        private bool CreateXML_ExtendEndTime(ref string extendXml, NS_MESSENGER.MCU mcu, int iConfMcuID, DateTime newConfEndTime)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        internal bool AddPartyToMcu(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }


        internal bool DeletePartyFromMcu(NS_MESSENGER.Party party, string dbConfName)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool OngoingConfOps(string operation, NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref string msg, bool newMuteValue, int newLayoutValue, int extendTime, bool connectOrDisconnect, ref NS_MESSENGER.Party.eOngoingStatus terminalStatus)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }


        #region ConfMonitor Methods
        #region Ongoing conf list
        private bool CreateXML_OngoingConfList(NS_MESSENGER.MCU mcu, ref string ongoingXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_ReservationConfList(NS_MESSENGER.MCU mcu, ref string resXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }


        #endregion

        #region Ongoing conf
        private bool CreateXML_OngoingConf(ref string confXml, NS_MESSENGER.MCU mcu, NS_MESSENGER.Conference conf)
        {

            try
            {
                // xml for fetching individual conf details. 



                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool ProcessXML_OngoingConf(string responseXml, ref NS_MESSENGER.Conference conf)
        {
            // process each conf to get the latest status for each participant
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXml);

                XmlNodeList nodelist;
                XmlNode node;

                // if description = OK , then continue processing
                node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION");
                string description = node.InnerXml;
                description = description.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = description;
                    return false; //error
                }

                // Retreive all conferences	
                nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF/GET/CONFERENCE/ONGOING_PARTY_LIST/ONGOING_PARTY");

                //cycle through each party one-by-one
                foreach (XmlNode node1 in nodelist)
                {
                    // party object 
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    XmlNode partyNode = node1.SelectSingleNode("PARTY");

                    // name , partyID on mcu
                    party.sMcuName = partyNode.ChildNodes.Item(0).InnerText; //NAME
                    party.iMcuId = Int32.Parse(partyNode.ChildNodes.Item(1).InnerText); //ID

                    //interface type  - ip or isdn
                    //XmlNode interfaceType = node1.SelectSingleNode("INTERFACE");
                    //string interfaceTypeVal = interfaceType.InnerText ; 					
                    string interfaceTypeVal = partyNode.ChildNodes.Item(3).InnerText;
                    interfaceTypeVal = interfaceTypeVal.Trim();
                    if (interfaceTypeVal == "h323")
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                    else
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;

                    // connection type - dial-in or dial-out 
                    //XmlNode connectionType = node1.SelectSingleNode("CONNECTION");
                    //string connectionTypeVal = connectionType.InnerText;
                    string connectionTypeVal = partyNode.ChildNodes.Item(4).InnerText;
                    connectionTypeVal = connectionTypeVal.Trim();
                    if (connectionTypeVal == "dial_in")
                        party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_IN;
                    else
                        party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_OUT;

                    // ip or isdn address 
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                    {
                        //XmlNode ipaddress = node1.SelectSingleNode("IP");						
                        //party.m_mcuAddress =ipaddress.InnerText ; 
                        party.sMcuAddress = partyNode.SelectSingleNode("IP").InnerText;
                    }
                    else
                    {
                        // party is connected on ISDN
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;
                        if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        {
                            // Dial-In : Fetch the address from mcu phone list 
                            //XmlNode isdnaddress = node1.SelectSingleNode("MCU_PHONE_LIST");						
                            party.sMcuAddress = partyNode.SelectSingleNode("MCU_PHONE_LIST").SelectSingleNode("PHONE1").InnerText;
                        }
                        else
                        {
                            // Dial-Out : Fetch the address from the participant phone list
                            //XmlNode isdnaddress = node1.SelectSingleNode("");						
                            party.sMcuAddress = partyNode.SelectSingleNode("BONDING_PHONE").InnerText;
                        }
                    }

                    // party ongoing status
                    XmlNode partyStatus = node1.SelectSingleNode("ONGOING_PARTY_STATUS");
                    string ongoingPartyStatus = partyStatus.ChildNodes.Item(1).InnerText; //description
                    ongoingPartyStatus = ongoingPartyStatus.Trim();
                    if (ongoingPartyStatus.CompareTo("connected") == 0)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                    }
                    else
                    {
                        if (ongoingPartyStatus.CompareTo("connecting") == 0)
                        {
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                        }
                        else
                        {
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                        }
                    }
                    logger.Trace("Ongoing party status : " + party.etStatus.ToString());

                    //Audio Mute status 
                    XmlNode partyAudioMute = node1.SelectSingleNode("AUDIO_MUTE");
                    string audioMute = partyAudioMute.InnerXml.Trim();
                    if (audioMute == "true")
                    {
                        party.bMute = true;
                    }
                    else
                    {
                        party.bMute = false;
                    }

                    // individual party details added to the resp conf
                    conf.qParties.Enqueue(party);
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        #endregion

        private bool CreateXML_ConnectDisconnectParty(ref string connectionXML, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool isConnect)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #region Terminate conf
        private bool CreateXML_TerminateConf(ref string terminateXml, NS_MESSENGER.Conference conf)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool ProcessXML_TerminateConf(string responseXml)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }

            // either correct confirmation not recd or conf doesnot exist on the bridge anymore.		
            return false;
        }
        #endregion

        internal bool FetchAllOngoingConfsStatus(NS_MESSENGER.MCU mcu, ref Queue confq)
        {

            try
            {
                // Login to the bridge

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        internal bool CreateXML_GetMcuTime(NS_MESSENGER.MCU mcu, ref string getTimeXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool CreateXML_SetMcuTime(NS_MESSENGER.MCU mcu, ref string setTimeXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool CheckMcuTime(NS_MESSENGER.MCU mcu)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }


            //return true;
        }

        private bool isMcuOutputErroneous(string mcuOutput)
        {
            try
            {
                if (mcuOutput.IndexOf("STATUS_FAIL_TO_LOCATE_USER") > 0)
                {
                    logger.Trace("Mcu error - Tokens are invalid.");
                    return true;
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return false;
        }

        private bool RefreshMcuTokens(NS_MESSENGER.MCU mcu, ref string transXML)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private string ProcessError(string error)
        {
            String retError = "";
            XElement elem = null;
            try
            {
                elem = XElement.Parse(error);

                XNamespace env = "urn:com:polycom:api:rest:plcm-error";

                retError = "Error Code :" + elem.Element(env + "status-code").Value + " Error :" + elem.Element(env + "description").Value;
                retError = retError.Replace("'", "");


            }
            catch (Exception ex)
            {
                retError = error;

            }
            return retError;
        }

        private static String encodeString(String data)
        {
            String encoded = "";
            try
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                encoded = System.Convert.ToBase64String(bytes, Base64FormattingOptions.None);


            }
            catch (Exception ex)
            {
            }

            return encoded;

        }

        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            logger.Trace("Entering Terminateconference function...");
            bool ret = false;
            String esID = "";
            string reservationXML = "";
            string responseXml = "";
            try
            {
                if (conf.ESId != "")
                {
                    esID = conf.ESId;

                    ret = SendTransaction_overXMLHTTP(conf.cMcu, reservationXML, ref responseXml, "DELETE", ref esID);
                    if (!ret)
                    {
                        logger.Exception(100, "SendTransaction failed . confXml = " + reservationXML);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }


        internal String GetETagValue(String URI, string credentials)
        {
            String ETag = "";
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(URI);
                //req.Credentials = cache;

                req.MediaType = "HTTP::/1.1";
                req.Headers["Authorization"] = "Basic " + credentials;
                req.Method = "GET";
                req.KeepAlive = true;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                if (resp.Headers["ETag"] != null)
                    ETag = resp.Headers["ETag"].ToString();

            }
            catch (Exception ex)
            {


            }
            return ETag;
        }
    }
}

