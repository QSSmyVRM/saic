﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End
/* FILE : Database.cs
 * DESCRIPTION : All database-related sql queries are stored in this file.
 */
namespace NS_DATABASE
{
    #region References
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Text;
    using System.Configuration;
    using System.Collections.Specialized;
    using System.Net;
    using System.Data.Sql;
    using System.Data.SqlClient;
    using System.IO;
    using System.Xml;
    using System.Linq;
    using System.Xml.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Security.Cryptography;
    #endregion

    class Database
    {
        internal string errMsg = null;
        string conStr = "";

        System.Configuration.ConnectionStringSettings conSettings = null;
        NameValueCollection appSettings = null;
        MyvrmIntegratedEM7.Logger log = new MyvrmIntegratedEM7.Logger();

        #region Constructor
        internal Database()
        {
            conSettings = ConfigurationManager.ConnectionStrings["MyDBConnectionString"];
            conStr = conSettings.ConnectionString;
            appSettings = ConfigurationManager.AppSettings;
        }

        #region Query executor
        private bool SelectCommand(string query, ref DataSet ds, string dsTable)
        {
            log.WritetoLogfile(query);
            SqlConnection con;
            try
            {
                con = new SqlConnection(conStr);
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                return (false);
            }
            try
            {

                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter();
                con.Open();
                da.SelectCommand = cmd;
                da.Fill(ds, dsTable);
                da.Dispose();
                con.Close();

                return (true);
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                con.Close();
                return (false);
            }

        }

        private bool NonSelectCommand(string query)
        {
            log.WritetoLogfile(query);
            SqlConnection con;
            try
            {
                con = new SqlConnection(conStr);
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                return (false);
            }

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Close();
                con.Close();
                return (true);
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                con.Close();
                return (false);
            }
        }
        #endregion

        #region FecthEM7Credential
        /// <summary>
        /// FecthEM7Credential
        /// </summary>
        /// <param name="EM7SiloId"></param>
        /// <returns></returns>
        public bool FecthEM7Credential(ref DataSet ds)
        {
            String query = "", dsTable = "OrgTable";
            bool ret = false;
            ds = new DataSet();
            try
            {
                query = " Select EM7URI,EM7Username,EM7Password,EM7Port from Sys_Settings_D";
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    log.WritetoLogfile(query);
                    return false;
                }
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region InsertEM7SiloData
        /// <summary>
        /// InsertEM7SiloData
        /// </summary>
        /// <param name="EM7OrgList"></param>
        /// <returns></returns>
        public bool InsertEM7SiloData(List<EM7Poll.EM7Poll.EM7Organization> EM7OrgList)
        {
            String query = "", selquery = "", dsTable = "EM7Orgname";
            bool ret = false;
            DataSet ds = new DataSet();
            try
            {
                if (EM7OrgList != null)
                {
                    for (int i = 0; i < EM7OrgList.Count; i++)
                    {
                        selquery = " Select OrgId from org_EM7Silo_D where OrgId ='" + (EM7OrgList[i].OrgID.Split('/')[3]) + "'";  // "/api/organization/1" splitted the value and get the orgID

                        ret = SelectCommand(selquery, ref ds, dsTable);
                        if (!ret)
                        {
                            log.WritetoLogfile(selquery);
                            return false;
                        }
                        if (ds.Tables[dsTable].Rows.Count > 0)
                            query += " Update org_EM7Silo_D set OrgId= " + (EM7OrgList[i].OrgID.Split('/')[3]) + " , EM7Orgname ='" + EM7OrgList[i].OrgName + "', EM7OrgEmail='', EM7UserName='', EM7Password ='' where OrgId='" + EM7OrgList[i].OrgID + "'"; // "/api/organization/1" splitted the value and get the orgID
                        else
                            query += " Insert into [org_EM7Silo_D] (OrgId, EM7Orgname, EM7OrgEmail, EM7UserName, EM7Password) values ( '" + (EM7OrgList[i].OrgID.Split('/')[3]) + "','" + EM7OrgList[i].OrgName + "','','','')"; // "/api/organization/1" splitted the value and get the orgID
                    }
                }
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    log.WritetoLogfile(query);
                    return false;
                }
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region FetchmyVRMEM7Silo
        /// <summary>
        /// FetchmyVRMEM7Silo
        /// </summary>
        /// <param name="EM7SiloId"></param>
        /// <returns></returns>
        public bool FetchmyVRMEM7Silo(ref List<Int32> EM7SiloId)
        {
            String query = "", dsTable = "OrgTable";
            bool ret = false;
            DataSet ds = new DataSet();
            int SiloId = 0;
            try
            {
                query = " Select distinct EM7OrgID from Org_Settings_d where EM7OrgID != NULL and EM7OrgID != '' ";
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    log.WritetoLogfile(query);
                    return false;
                }
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {
                        if (ds.Tables[dsTable].Rows[i]["EM7OrgID"] != null)
                            Int32.TryParse(ds.Tables[dsTable].Rows[i]["EM7OrgID"].ToString().Trim(), out SiloId);

                        EM7SiloId.Add(SiloId);
                    }
                }
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                return false;
            }
            return true;
        }
        #endregion

        //Method changed for FB 2616 Start

        #region UpdateEndpointStatus
        /// <summary>
        /// UpdateEndpointStatus
        /// </summary>
        /// <param name="EndpointList"></param>
        /// <returns></returns>
        public bool UpdateEndpointStatus(Hashtable EndpointList)
        {
            String query = "", EM7OnlineStatus = "";
            bool ret = false;
            try
            {
                if (EndpointList.Count > 0)
                {
                    foreach (DictionaryEntry eptData in EndpointList)
                    {
                        if (eptData.Value.ToString() == "3")
                            EM7OnlineStatus = "1";
                        else
                            EM7OnlineStatus = "0";

                        query += " Update Ept_List_D Set EptOnlineStatus = " + EM7OnlineStatus + ",Eptcurrentstatus = " + eptData.Value.ToString() + " where address = '" + eptData.Key.ToString() + "' ";
                    }
                }
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    log.WritetoLogfile(query);
                    return false;
                }
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
                return false;
            }
            return true;
        }

        #endregion

        //Method changed for FB 2616 End

        #endregion


    }
}