﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Linq;
using System.Xml.Linq;

namespace MyvrmIntegratedEM7
{
    class Logger
    {
        NameValueCollection appSettings = null;
        String logFilePath = "";
        String EnableLog = "";
        #region WriteLogFile
        public void WritetoLogfile(ref String error)
        {
            StreamWriter sw = null;
            appSettings = ConfigurationManager.AppSettings;
            try
            {
                logFilePath = appSettings[0];
                EnableLog = appSettings[1];
                if (EnableLog.Trim().ToUpper() == "YES")
                {
                    if (!File.Exists(logFilePath))
                    {
                        // file doesnot exist . hence, create a new log file.
                        sw = File.CreateText(logFilePath);
                        sw.Flush();
                        sw.Close();
                    }
                    else
                    {
                        // check if exisiting log file size is greater than 50 MB
                        FileInfo fi = new FileInfo(logFilePath);
                        if (fi.Length > 100000000)
                        {
                            // delete the log file						
                            File.Delete(logFilePath);
                            // create a new log file 
                            sw = File.CreateText(logFilePath);
                            sw.Flush();
                            sw.Close();
                        }
                    }

                    // write the log record.
                    sw = File.AppendText(logFilePath);
                    sw.WriteLine(error);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (System.Exception)
            {
                // do nothing
            }

        }

        public void WritetoLogfile(String error)
        {
            StreamWriter sw = null;
            appSettings = ConfigurationManager.AppSettings;
            try
            {
                logFilePath = appSettings[0];
                EnableLog = appSettings[1];
                if (EnableLog.Trim().ToUpper() == "YES")
                {
                    if (!File.Exists(logFilePath))
                    {
                        // file doesnot exist . hence, create a new log file.
                        sw = File.CreateText(logFilePath);
                        sw.Flush();
                        sw.Close();
                    }
                    else
                    {
                        // check if exisiting log file size is greater than 50 MB
                        FileInfo fi = new FileInfo(logFilePath);
                        if (fi.Length > 100000000)
                        {
                            // delete the log file						
                            File.Delete(logFilePath);
                            // create a new log file 
                            sw = File.CreateText(logFilePath);
                            sw.Flush();
                            sw.Close();
                        }

                    }

                    // write the log record.
                    sw = File.AppendText(logFilePath);
                    sw.WriteLine(error);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (System.Exception)
            {
                // do nothing
            }

        }
        # endregion
    }
}
