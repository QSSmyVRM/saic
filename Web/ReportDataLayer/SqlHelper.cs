using System; 
using System.Data; 
using System.Data.SqlClient; 
using System.Collections; 
using System.Configuration;
using System.IO;
using System.Web;
using System.Xml;


namespace ns_SqlHelper
{
	/// <summary>
	/// The SqlHelper class is intended to encapsulate high performance, 
	/// scalable best practices for common uses of SqlClient.
	/// </summary>
	public sealed class SqlHelper
	{
		/// <summary>
		/// Private members of this class are declared in this region
		/// _connectionString is declared as shared variable
		/// </summary>

        private static string _connectionString = string.Empty;
        private static string _connectionString1 = string.Empty; 
		private CommandType _commandType; 
		private SqlConnection _cnn; 
		private SqlTransaction _transaction; 

		/// <summary>
		/// Default CommandType is set to StoredProcedure
		/// connectionString (for database connection) gets initialized
		/// during the first instantiation of the class
		/// </summary>

        public SqlHelper()
        {

        }
		public SqlHelper(String path) 
		{
            string configPath = path; // HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString();
            
            _commandType = CommandType.Text; // .StoredProcedure; 
			if (_connectionString == string.Empty) 
			{
                string xml = configPath + "app.config.xml";

                XmlTextReader textReader = new XmlTextReader(xml);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(textReader);

                XmlNodeList NodeList = xmlDoc.GetElementsByTagName("session-factory");

                foreach (XmlNode xnode in NodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        if (xnod.LocalName == "property")
                        {
                            if (xnod.Attributes["name"].Value == "connection.connection_string")
                            {
                                _connectionString = xnod.InnerText;
                                break;
                            }
                        }
                    }
                }
             
                //_connectionString1 = ConfigurationSettings.AppSettings.Get("connectionstring"); 
							
			} 
		} 



		#region Public Methods

        #region Database connection related methods

		#region Method to Open Database Connection
		/// <summary>
		/// This method establishes the connection object for the connection 
		/// string which gets updated on a new instance of this class.
		/// </summary>
		public void OpenConnection() 
		{ 
			try 
			{ 
                
				_cnn = new SqlConnection(_connectionString); 
				_cnn.Open(); 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		#endregion

		#region Method to Close Database Connection
		/// <summary>
		/// This method closes the connection object
		/// </summary>
		public void CloseConnection() 
		{ 
			try 
			{ 
				if(_cnn != null)
				{
					if(_cnn.State == ConnectionState.Open)
					{ 
						_cnn.Close(); 
					}
				}
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
			finally 
			{ 
				if(_cnn != null)
				{
					_cnn.Dispose(); 
				}
			} 
		} 
		#endregion

		#endregion

		#region Transactions to Database Connections

		#region Method to create new transaction to the existing connection
		/// <summary>
		/// This method begins the transaction object for the opened connection
		/// </summary>
		public void OpenTransaction() 
		{ 
			try 
			{ 
				if ((_cnn.State == ConnectionState.Open)) 
				{ 
					_transaction = _cnn.BeginTransaction(); 
				} 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		#endregion

		#region Method to commit the transactions to the existing connection
		/// <summary>
		/// This method commits the transaction on successive completion
		/// </summary>
		public void CommitTransaction() 
		{ 
			try 
			{ 
				if(_cnn != null)
				{
					if(_cnn.State == ConnectionState.Open)
					{ 
						_transaction.Commit(); 
					}
				}
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		#endregion

		#region Method to roll back the transactions
		/// <summary>
		/// This method roll back all the transactions once an exception occurs.
		/// After roll backing the transactions it closes the respective connection object
		/// </summary>
		public void RollBackTransaction() 
		{ 
			try 
			{ 
				if(_cnn != null)
				{
					if(_cnn.State == ConnectionState.Open)
					{ 
						_transaction.Rollback(); 
					}
				}
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
			finally 
			{
				if(_cnn != null)
				{
					_cnn.Dispose(); 
				}
			} 
		} 
		#endregion

		#endregion

		#region ExecuteNonQuery
		/// <summary>
		/// This ExecuteNonQuery is for data manipulation commands
		/// Execute a SqlCommand (that returns no resultset) 
		/// against the database specified in the connection string. 
		/// e.g.:  
		/// Int32 result =  ExecuteNonQuery("insert/delete/update query")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input">T-SQL command or stored procedure name</param>
		/// <param name="commandParameters" type="ArrayList" Direction="Input">Array of SqlParamters used to execute the command</param>
		/// <returns type="Integer">An int representing the number of rows affected by the command</returns>
		public int ExecuteNonQuery(string commandText, ArrayList commandParameters) 
		{ 
			try 
			{ 
				SqlCommand cmd; 
				int retval; 
				cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = _commandType; 
				cmd.Transaction = _transaction; 
				cmd.CommandTimeout= 7200;
				//SqlParameter p; 
				foreach (SqlParameter p in commandParameters) 
				{ 
					cmd.Parameters.Add(p); 
				} 
				retval = cmd.ExecuteNonQuery(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		/// <summary>
		/// Execute a SqlCommand (that returns no resultset) 
		/// against the database specified in the connection string.
		/// e.g.: 
		/// Int32 result =  ExecuteNonQuery("insert/delete/update query")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input">T-SQL command or stored procedure name</param>
		/// <param name="sqlCommandType" type="commandType" Direction="Input">the CommandType (stored procedure, text, etc.)</param>
		/// <param name="commandParameters" type="ArrayList" Direction="Input">Array of SqlParamters used to execute the command</param>
		/// <returns type="Integer">An int representing the number of rows affected by the command</returns>
		public int ExecuteNonQuery(string commandText, CommandType sqlCommandType, ArrayList commandParameters) 
		{ 
			try 
			{ 
				SqlCommand cmd; 
				int retval; 
				cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = sqlCommandType; 
				cmd.Transaction = _transaction; 
				cmd.CommandTimeout= 7200;
				//SqlParameter p; 
				foreach (SqlParameter p in commandParameters) 
				{ 
					cmd.Parameters.Add(p); 
				} 
				retval = cmd.ExecuteNonQuery(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		/// <summary>
		/// Execute a SqlCommand (that returns no resultset) 
		/// against the database specified in the connection string.
		/// e.g.: 
		/// Int32 result =  ExecuteNonQuery("insert/delete/update query")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input">T-SQL command or stored procedure name</param>
		/// <returns type="Integer">An int representing the number of rows affected by the command</returns>
		public int ExecuteNonQuery(string commandText) 
		{ 
			try 
			{ 
				SqlCommand cmd; 
				int retval; 
				cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = _commandType; 
				cmd.Transaction = _transaction; 
				cmd.CommandTimeout= 7200;
				retval = cmd.ExecuteNonQuery(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 

		/// <summary>
		/// Execute a SqlCommand (that returns no resultset) 
		/// against the database specified in the connection string.
		/// e.g.: 
		/// Int32 result =  ExecuteNonQuery("insert/delete/update query")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input">T-SQL command or stored procedure name</param>
		/// <param name="sqlCommandType" type="commandType" Direction="Input">the CommandType (stored procedure, text, etc.)</param>
		/// <returns type="Integer">An int representing the number of rows affected by the command</returns>
		public int ExecuteNonQuery(string commandText, CommandType sqlCommandType) 
		{ 
			try 
			{ 
				SqlCommand cmd; 
				int retval; 
				cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = sqlCommandType; 
				cmd.Transaction = _transaction; 
				cmd.CommandTimeout= 7200;
				retval = cmd.ExecuteNonQuery(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		
		/// <summary>
		/// Execute a SqlCommand (that returns no resultset) 
		/// against the database specified in the connection string.
		/// e.g.: 
		/// Int32 result =  ExecuteNonQuery("insert/delete/update query")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input">T-SQL command or stored procedure name</param>
		/// <param name="returnOutput" type="Object" Direction="Output">object reference is being sent which holds the output of storedprocedure</param>
		/// <param name="commandParameters" type="ArrayList" Direction="Input">Array of SqlParamters used to execute the command</param>
		/// <returns type="Integer">An int representing the number of rows affected by the command</returns>
		public int ExecuteNonQuery(string commandText, out object returnOutput, ArrayList commandParameters) 
		{ 
			try 
			{ 
				SqlCommand cmd; 
				int retval; 
				bool isOutputParamExist = false; 
				string strOutParam = string.Empty;
				returnOutput = null; 
			
				cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = _commandType; 
				cmd.Transaction = _transaction;
				cmd.CommandTimeout= 7200;
				//SqlParameter p; 
				foreach (SqlParameter p in commandParameters) 
				{ 
					cmd.Parameters.Add(p); 
					if ((p.Direction == ParameterDirection.Output)) 
					{ 
						isOutputParamExist = true; 
						strOutParam = p.ParameterName; 
					} 
				} 
				retval = cmd.ExecuteNonQuery(); 
				if ((isOutputParamExist)) 
				{ 
					returnOutput = (object)(cmd.Parameters[strOutParam].Value); 
				} 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 

		/// <summary>
		/// Execute a SqlCommand (that returns no resultset) 
		/// against the database specified in the connection string.
		/// e.g.: 
		/// Int32 result =  ExecuteNonQuery("insert/delete/update query")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input">T-SQL command or stored procedure name</param>
		/// <param name="sqlCommandType" type="commandType" Direction="Input">the CommandType (stored procedure, text, etc.)</param>
		/// <param name="commandParameters" type="ArrayList" Direction="Input">Array of SqlParamters used to execute the command</param>
		/// <param name="returnOutput" type="Object" Direction="Output">object reference is being sent which holds the output of storedprocedure</param>
		/// <returns type="Integer">An int representing the number of rows affected by the command</returns>
		public int ExecuteNonQuery(string commandText, CommandType sqlCommandType, ArrayList commandParameters, out object returnOutput) 
		{ 
			try 
			{ 
				SqlCommand cmd; 
				int retval; 
				bool isOutputParamExist = false; 
				String strOutParam = String.Empty; 
				returnOutput = null; 
				cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = sqlCommandType; 
				cmd.Transaction = _transaction;
				cmd.CommandTimeout= 7200;
				//SqlParameter p; 
				foreach (SqlParameter p in commandParameters) 
				{ 
					cmd.Parameters.Add(p); 
					if ((p.Direction == ParameterDirection.Output)) 
					{ 
						isOutputParamExist = true; 
						strOutParam = p.ParameterName; 
					} 
				} 
				retval = cmd.ExecuteNonQuery(); 
				if ((isOutputParamExist)) 
				{ 
					returnOutput = ((object)(cmd.Parameters[strOutParam].Value)); 
				} 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		#endregion

		#region ExecuteDataSet
		/// <summary>
		/// Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
		/// using the provided parameters.
		/// e.g.:
		/// DataSet ds = sqlHelper.ExecuteDataSet("sql select statement")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input"></param>
		/// <returns type="DataSet">a dataset containing the resultset generated by the command</returns>
		public DataSet ExecuteDataSet(string commandText) 
		{ 
			try 
			{ 
				DataSet dataset = new DataSet(); 
				SqlDataAdapter dataAdapter = new SqlDataAdapter(commandText, _connectionString); 
				dataAdapter.SelectCommand.CommandType = _commandType;
                dataAdapter.SelectCommand.CommandTimeout = 480; //FB 1750
				dataAdapter.Fill(dataset); 
				return dataset; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		
		/// <summary>
		/// Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
		/// using the provided parameters.
		/// e.g.:
		/// DataSet ds = sqlHelper.ExecuteDataSet("sql select statement")
		/// </summary>
		/// <param name="commandText" type="String" Direction="Input"></param>
		/// <param name="sqlCommandType" type="commandType" Direction="Input">the CommandType (stored procedure, text, etc.)</param> 
		/// <returns type="DataSet">a dataset containing the resultset generated by the command</returns>
		public DataSet ExecuteDataSet(string commandText, CommandType sqlCommandType) 
		{ 
			try 
			{ 
				DataSet dataset = new DataSet(); 
				SqlDataAdapter dataAdapter = new SqlDataAdapter(commandText, _connectionString); 
				dataAdapter.SelectCommand.CommandType = sqlCommandType;
                dataAdapter.SelectCommand.CommandTimeout = 480; //FB 1750
				dataAdapter.Fill(dataset); 
				return dataset; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 

		/// <summary>
		/// Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
		/// using the provided parameters.
		/// e.g.:
		/// DataSet ds = sqlHelper.ExecuteDataSet("sql select statement")
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="commandParameters"></param>
		/// <returns type="DataSet">a dataset containing the resultset generated by the command</returns>
		public DataSet ExecuteDataSet(string commandText, ArrayList commandParameters) 
		{ 
			try 
			{ 
				DataSet dataSet = new DataSet(); 
				SqlDataAdapter dataAdapter = new SqlDataAdapter(commandText, _connectionString); 
				//SqlParameter p; 
				dataAdapter.SelectCommand.CommandType = _commandType; 
				foreach (SqlParameter p in commandParameters) 
				{ 
					dataAdapter.SelectCommand.Parameters.Add(p); 
				} 
				dataAdapter.Fill(dataSet); 
				return dataSet; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		}
		#endregion

		#region ExecuteScalar
		/// <summary>
		/// Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
		/// using the provided parameters.
		/// e.g.:
		/// Int32 orderCount = Cint(ExecuteScalar("GetOrderCount", new SqlParameter("@prodid", 24)))
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="commandParameters"></param>
		/// <returns type="Object"></returns>
		public object ExecuteScalar(string commandText, ArrayList commandParameters) 
		{ 
			try 
			{ 
				SqlCommand cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = _commandType; 
				
				foreach (SqlParameter p in commandParameters) 
				{ 
					cmd.Parameters.Add(p); 
				} 
				object retval = cmd.ExecuteScalar(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 

		/// <summary>
		/// Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
		/// using the provided parameters.
		/// e.g.:
		/// Int32 orderCount = Cint(ExecuteScalar("GetOrderCount", new SqlParameter("@prodid", 24)))
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="sqlCommandType"></param>
		/// <param name="commandParameters"></param>
		/// <returns type="Object"></returns>
		public object ExecuteScalar(string commandText, CommandType sqlCommandType, ArrayList commandParameters) 
		{ 
			try 
			{ 
				SqlCommand cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = sqlCommandType; 
				
				foreach (SqlParameter p in commandParameters) 
				{ 
					cmd.Parameters.Add(p); 
				} 
				object retval = cmd.ExecuteScalar(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 

		/// <summary>
		/// Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
		/// using the provided parameters.
		/// e.g.:
		/// Int32 orderCount = Cint(ExecuteScalar("GetOrderCount", new SqlParameter("@prodid", 24)))
		/// </summary>
		/// <param name="commandText"></param>
		/// <returns type="Object"></returns>
		public object ExecuteScalar(string commandText) 
		{ 
			try 
			{ 
				SqlCommand cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = _commandType; 
				cmd.Transaction = _transaction; 
				object retval = cmd.ExecuteScalar(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		
		/// <summary>
		/// Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
		/// using the provided parameters.
		/// e.g.:
		/// Int32 orderCount = Cint(ExecuteScalar("GetOrderCount", new SqlParameter("@prodid", 24)))
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="sqlCommandType"></param>
		/// <returns type="Object"></returns>
		public object ExecuteScalar(string commandText, CommandType sqlCommandType) 
		{ 
			try 
			{ 
				SqlCommand cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = sqlCommandType; 
				cmd.Transaction = _transaction; 
				object retval = cmd.ExecuteScalar(); 
				return retval; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		#endregion

		#region ExecuteReader
		/// <summary>
		/// Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction
		/// This method provides no access to output parameters or the stored procedure's return value parameter.
		/// e.g.:
		/// SqlDataReader dr = ExecuteReader("GetOrders")
		/// </summary>
		/// <param name="commandText" type="String"></param>
		/// <returns type="SqlDataReader">SqlDataReader containing the resultset generated by the command</returns>
		public SqlDataReader ExecuteReader(string commandText) 
		{ 
			try 
			{ 
				SqlDataReader dataReader = null; 
				OpenTransaction(); 
				SqlCommand cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = _commandType; 
				cmd.Transaction = _transaction; 
				dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection); 
				return dataReader; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 

		/// <summary>
		/// Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction
		/// This method provides no access to output parameters or the stored procedure's return value parameter.
		/// e.g.:
		/// SqlDataReader dr = ExecuteReader("GetOrders")
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="sqlCommandType"></param>
		/// <returns type="SqlDataReader">SqlDataReader containing the resultset generated by the command</returns>
		public SqlDataReader ExecuteReader(string commandText, CommandType sqlCommandType) 
		{ 
			try 
			{ 
				SqlDataReader dataReader = null; 
				OpenTransaction(); 
				SqlCommand cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = sqlCommandType; 
				cmd.Transaction = _transaction; 
				dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection); 
				return dataReader; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 

		/// <summary>
		/// Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction
		/// This method provides no access to output parameters or the stored procedure's return value parameter.
		/// e.g.:
		/// SqlDataReader dr = ExecuteReader("GetOrders")
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="sqlCommandType"></param>
		/// <param name="commandParameters"></param>
		/// <returns></returns>
		public IDataReader ExecuteReader(string commandText, CommandType sqlCommandType, ArrayList commandParameters) 
		{ 
			try 
			{ 
				SqlDataReader dataReader = null; 
				OpenTransaction(); 
				SqlCommand cmd = new SqlCommand(commandText, _cnn); 
				cmd.CommandType = sqlCommandType; 
				cmd.Transaction = _transaction; 
			
				foreach (SqlParameter p in commandParameters) 
				{ 
					cmd.Parameters.Add(p); 
				} 
				dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection); 
				return dataReader; 
			} 
			catch (Exception ex) 
			{ 
				throw ex; 
			} 
		} 
		#endregion

		#region Method to Get all the tables from the DB
		public DataSet GetDatabase()
		{
			String SqlSelect = "";
			DataSet ds = null;
			DataSet tempDs = null;
			DataTable dt = null;
			try
			{
				SqlSelect = "select * from sysobjects where xtype = 'u' and status > 0 order by 1";
				tempDs = ExecuteDataSet(SqlSelect,CommandType.Text);
				if(tempDs !=null)
				{
					if(tempDs.Tables.Count > 0)
					{
						if(tempDs.Tables[0].Rows.Count > 0)
						{
							ds = new DataSet();
							foreach(DataRow dr in tempDs.Tables[0].Rows)
							{
								String tableName = dr["Name"].ToString();
								SqlSelect = "Select * from " + tableName + " order by 1";
								tempDs = ExecuteDataSet(SqlSelect,CommandType.Text);
								if(tempDs !=null)
								{
									if(tempDs.Tables.Count > 0)
									{
										dt = tempDs.Tables[0];
										dt.TableName = tableName;
										tempDs.Tables.Remove(dt);
										ds.Tables.Add(dt);
									}
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return ds;
		}
		#endregion

		#endregion


	}
}
