﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SYNCSERVICE
{
    class Helper
    {
        #region GetString Between

        public static string GetStringInBetween(string strBegin, string strEnd, string strSource, bool includeBegin, bool includeEnd)
        {
            string result = "";
            try
            {
                int iIndexOfBegin = strSource.IndexOf(strBegin);
                if (iIndexOfBegin != -1)
                {
                    if (includeBegin)
                        iIndexOfBegin -= strBegin.Length;
                    strSource = strSource.Substring(iIndexOfBegin + strBegin.Length);
                    int iEnd = strSource.IndexOf(strEnd);
                    if (iEnd != -1)
                    {
                        if (includeEnd)
                            iEnd += strEnd.Length;
                        result = strSource.Substring(0, iEnd);

                    }
                }
                else
                    result = strSource;
            }
            catch (Exception ex)
            {
                result = "";
            }
            return result;
        }

        #endregion
    }
}
