namespace MESSENGER
{
	#region references
	using System;
	using System.Collections;
	#endregion 

	class ConfigParams
	{
		public string localConfigPath,globalConfigPath,logFilePath,siteUrl,reportFilePath;//FB 2363		
		public string databaseLogin,databaseServer,databasePwd,databaseName;		
		public bool debugEnabled;
        public double activationTimer;
		public ConfigParams()
		{
            activationTimer = Convert.ToDouble(24 * 60 * 60 * 1000);
			debugEnabled = false;
		}
	}

    class Conference
    {
        public string sRequestID, sRequestXML, sResponse, sMessage, sConfid, sTypeID, sEventTime, sStatus, sStatusDate,sExternalConfID;
        public int iConfNumname, iRetryCount, iOrgID;
        public Conference()
        {
            sRequestXML = sResponse = sMessage= sConfid="";
        }
    }

    class EventRequests :Conference
    {
        public string  sInXML, sOutXML, sCommand,sErrorMessage,sProcessErrorMsg;
        public int iOriginConfID , iOriginEventID,iCommandID;
        public EventRequests()
        {
            sInXML = sOutXML= sRequestXML = sResponse = sMessage = sConfid = sRequestID = "";
            sProcessErrorMsg = "Successful processed and inserted";
        }
    }
}