﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Threading;


namespace SYNCSERVICE
{
    class ReceiveMessage
    {
        #region  Private Variables

        // network/system
        private bool _initOk = false;                    
        private string _hostName = null;                   
        private TcpClient _client;                         
        private NetworkStream _stream;                          
        private StreamReader _reader;                           
        private StreamWriter _writer;                            
        private long _sessCount = 0;                    
        private string _sessionID = null;                  
        private long _lastMsgID = -1;                    
        private bool _timedOut = false;                 
        private string _clientIP = null;
        private cmdID _lastCmd = cmdID.invalid;         
        private LOGGER.Log log = null;
        private myVRMOperations myVRMOps = null;
        private bool _earlyTalker = false;
        private string _heloStr = "";
        private string _mailFrom = "";                 
        private List<string> _rcptTo = new List<string>();
        private string _mailBox = ""; 
        private string _mailDom = "";
        private string _errorCode = "";
        private string _errorMessage = "";
        private string _confID = "";
        private List<MESSENGER.Conference> _confs = new List<MESSENGER.Conference>();
        private SendMessage _sendConference = null;
        private SendMessage _sendMessage = null;
        private MESSENGER.ConfigParams _configParams = null;

        const string TIMEOUT_MSG = "442 Connection timed out.";
        const string TEMPFAIL_MSG = "421 Service temporarily unavailable, closing transmission channel.";
        const string ETALKER_MSG = "554 Misbehaved SMTP session (EarlyTalker)";
        const string DIR_TX = "SND";
        const string DIR_RX = "RCV";
        const string BANNER_STR = "220 myVRM MailServ 2.9.2.22; {0}";
        const string HELO_CHARS = "[]0123456789.-abcdefghijklmnopqrstuvwxyz_";
        const string NODATA_MSG = "500 No Data Received.";


        // SMTP command strings
        string[] cmdList = { "\r\n",
                             "HELO",
                             "EHLO",
                             "MAIL FROM:",
                             "RCPT TO:",
                             "DATA",
                             "RSET",
                             "QUIT",
                             "VRFY",
                             "EXPN",
                             "HELP",
                             "NOOP"
                        };

        // command ID mapping codes 
        enum cmdID
        {
            invalid,
            helo,
            ehlo,
            mailFrom,
            rcptTo,
            data,
            rset,
            quit,
            vrfy,
            expn,
            help,
            noop
        }

        #endregion

        #region  Initialize
        /// <summary>
       /// Initialize the calss with client
       /// </summary>
       /// <param name="client"></param>
        public ReceiveMessage(TcpClient client, MESSENGER.ConfigParams configParams, myVRMOperations pmyVRMOps)
        {
            try
            {
                log = new LOGGER.Log(configParams);
                myVRMOps = pmyVRMOps;
                if (myVRMOps == null)
                    myVRMOps = new myVRMOperations(configParams);
                _client = client;
                _clientIP = _client.Client.RemoteEndPoint.ToString();
                int i = _clientIP.IndexOf(':');
                if (-1 != i) _clientIP = _clientIP.Substring(0, i);
                _client.ReceiveTimeout = client.Client.SendTimeout ; // hardcoded for now

                _stream = _client.GetStream();
                _reader = new StreamReader(_stream);
                _writer = new StreamWriter(_stream);
                _writer.NewLine = "\r\n";
                _writer.AutoFlush = true;               
                _initOk = true;
                _configParams = configParams;
                
            }
            catch (Exception ex)
            {
                throw ex;// Bad: Exception in initialization
                
            }
        }
        #endregion

        #region "methods"
        public void handleSession()
        {
            try
            {
                string cmdLine = "?";
                string response = cmd_ok(null);
                cmdID currCmd = cmdID.invalid;
                bool connOk = true;

                connOk = sendLine(cmd_Banner());

                if (false == _initOk)
                {
                    closeSession();
                    return;
                }

               
                
                while ((null != cmdLine) && (true == connOk))
                {
                    if (_lastCmd == cmdID.data)
                    {
                        string mailMsg = recvData();
                        if (_timedOut)
                        {
                            // got a receive timeout during the DATA phase
                            if (connOk) sendLine(TIMEOUT_MSG);
                            closeSession();
                            return;
                        }
                        if (String.IsNullOrEmpty(mailMsg))
                            response = NODATA_MSG;
                        else
                        {
                            StoreConference(mailMsg);
                            response = cmd_dot(null);
                            connOk = sendLine(response);
                        }
                        
                    }
                    else
                    {
                        // read an SMTP command line and deal with the command
                        cmdLine = recvLine();
                        if (null != cmdLine)
                        {
                            log.Trace(DIR_RX +" : "+ cmdLine);
                            currCmd = getCommandID(cmdLine);
                            switch (currCmd)
                            {                                
                                case cmdID.ehlo:            // EHLO
                                    response = cmd_helo(cmdLine);
                                    break;
                                case cmdID.mailFrom:        // MAIL FROM:
                                    response = cmd_mail(cmdLine);
                                    break;
                                case cmdID.rcptTo:          // RCPT TO:
                                    response = cmd_rcpt(cmdLine);
                                    break;
                                case cmdID.data: // Data
                                    response = cmd_data(cmdLine);
                                    break;
                                case cmdID.quit: // Quit
                                    response = cmd_quit(null);
                                    cmdLine = null;
                                    break;
                                default:                    // unkown/unsupported
                                    response = cmd_unknown(cmdLine);
                                    break;
                            }
                        }
                        else
                        {
                            // the read timed out (or we got an error), emit a message and drop the connection
                            response = TIMEOUT_MSG;
                            currCmd = cmdID.quit;
                        }
                    }

                   
                    // add a short delay
                    sleepDown(25);

                    // send out the response
                    connOk = sendLine(response);
                   
                    if (connOk) connOk = _client.Connected;
                } // while null...

                

            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace +", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }

            
        }

        #endregion

        #region "privatecode"
        // retrieves the command ID from command line args
        private cmdID getCommandID(string cmdLine)
        {
            try
            {
                cmdID id = cmdID.invalid;
                string tmpBuff = cmdLine.ToUpperInvariant();

                for (int i = 0; i < cmdList.Length; i++)
                {
                    if (tmpBuff.StartsWith(cmdList[i]))
                    {
                        id = (cmdID)i;
                        break;
                    }
                }
                return id;

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }

        // resets the internal session values
        private void resetSession()
        {
            try
            {
                //Do nothing for now
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // closes the socket, terminates the session
        public void closeSession()
        {
            try
            {
                if (null != _client)
                {
                    if (_client.Connected)
                        sleepDown(25);
                    try { _client.Close(); _client = null; }
                    catch { }
                }
                _initOk = false;
                resetSession();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // banner string (not a real command)
        private string cmd_banner(string cmdLine)
        {
            try
            {
                string banner = String.Format(BANNER_STR, _hostName, DateTime.UtcNow.ToString("R"));
                return banner;

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        // HELO/EHLO BANNER_STR
        private string cmd_helo(string cmdLine)
        {
            try
            {
                cmdID id = getCommandID(cmdLine);
                List<string> parts = parseCmdLine(id, cmdLine);               
                _heloStr = parts[1];
                _lastCmd = id;
                return String.Format("250 Hello {0} ([{1}]), nice to meet you.", parts[1], _clientIP);
                

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        private string cmd_Banner()
        {
            try
            {
               
                return String.Format(BANNER_STR, DateTime.Now);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        // MAIL FROM:
        private string cmd_mail(string cmdLine)
        {
            try
            {
                if (string.IsNullOrEmpty(_heloStr))
                {

                    return "503 HELO/EHLO Command not issued";
                }
                if (!string.IsNullOrEmpty(_mailFrom))
                {

                    // return "503 Nested MAIL command"; We handle nested mail commands
                    
                }
                List<string> parts = parseCmdLine(cmdID.mailFrom, cmdLine);
                if (2 != parts.Count)
                {

                    return String.Format("501 {0} needs argument", parts[0]);
                }
                if (!checkMailAddr(parts[1]))
                {

                    return String.Format("553 Invalid address {0}", parts[1]);
                }
                _mailFrom = parts[1];
                _lastCmd = cmdID.mailFrom;
                return string.Format("250 {0}... Sender OK", parts[1]);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // RCPT TO:
        private string cmd_rcpt(string cmdLine)
        {
            try
            {
                if (string.IsNullOrEmpty(_mailFrom))
                {

                    return "503 Need MAIL before RCPT";
                }
                List<string> parts = parseCmdLine(cmdID.rcptTo, cmdLine);
                if (2 != parts.Count)
                {

                    return String.Format("501 {0} needs argument", parts[0]);
                }
                if (!checkMailAddr(parts[1]))
                {

                    return String.Format("553 Invalid address {0}", parts[1]);
                }
                _rcptTo.Add(parts[1]);
                _lastCmd = cmdID.rcptTo;
                return string.Format("250 {0}... Recipient OK", parts[1]);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // DATA
        private string cmd_data(string cmdLine)
        {
            try
            {
                if (_rcptTo.Count < 1)
                {

                    return "471 Bad or missing RCPT command";
                }
                _lastCmd = cmdID.data;
                return "354 Start mail input; end with <CRLF>.<CRLF>";

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // end of DATA (dot)
        private string cmd_dot(string cmdLine)
        {
            try
            {
                _lastCmd = cmdID.quit;
                return "250 Queued mail for delivery";

            }
            catch (Exception ex)
            {

                throw ex;
            }
          
        }

        private string cmd_doterror(string cmdLine)
        {
            try
            {
                _lastCmd = cmdID.quit;
                return "500 " + _errorCode + ":" + _errorMessage;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        // RSET
        private string cmd_rset(string cmdLine)
        {
            try
            {
                resetSession();
                _lastCmd = cmdID.rset;
                return "250 Reset Ok";

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // QUIT
        private string cmd_quit(string cmdLine)
        {
            try
            {
                _lastCmd = cmdID.quit;
                return "221 Closing connection.";

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // VRFY/EXPN
        private string cmd_vrfy(string cmdLine)
        {
            try
            {
                cmdID id = getCommandID(cmdLine);
                List<string> parts = parseCmdLine(id, cmdLine);
                if (2 != parts.Count)
                {

                    return String.Format("501 {0} needs argument", parts[0]);
                }
                if (!checkMailAddr(parts[1]))
                {

                    return String.Format("553 Invalid address {0}", parts[1]);
                }
                _lastCmd = id;
                if (id == cmdID.vrfy)
                    return "252 Cannot VRFY user; try RCPT to attempt delivery (or try finger)";
                return String.Format("250 {0}", parts[1]);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // NOOP
        private string cmd_noop(string cmdLine)
        {
            try
            {
               
                List<string> parts = parseCmdLine(cmdID.noop, cmdLine);
                if (parts.Count > 1)
                {
                    // NOOP may have args...
                    return string.Format("250 ({0}) OK", parts[1]);
                }
                return "250 OK";

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        // HELP
        private string cmd_help(string cmdLine)
        {
            try
            {
                // dynamically build the help string for our commands list
                string cmd = null;
                int pos = -1;
                string buff = "211";
                for (int i = 1; i < cmdList.Length; i++)
                {
                    cmd = cmdList[i];
                    pos = cmd.IndexOf(' ');
                    if (-1 != pos) cmd = cmd.Substring(0, pos);
                    buff = buff + " " + cmd;
                }
                return buff;

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        // misc command, fake support
        private string cmd_ok(string cmdLine)
        {
            try
            {
                if (!string.IsNullOrEmpty(cmdLine))
                {
                    List<string> parts = parseCmdLine(cmdID.noop, cmdLine);
                    if (parts.Count > 1)
                    {
                        return string.Format("250 {0} OK", parts[0]);
                    }
                }
                return "250 Ok";

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // unknown/unsupported
        private string cmd_unknown(string cmdLine)
        {
            try
            {
                _lastCmd = cmdID.invalid;
                if (string.IsNullOrEmpty(cmdLine))
                    return "500 Command unrecognized";
                else
                    return string.Format("500 Command unrecognized ({0})", cmdLine);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

       
        // sends a line to remote
        private bool sendLine(string line)
        {
            try
            {

                
                _writer.WriteLine(line);
                return true;
            }
            catch //(Exception ex)
            {
                //AppGlobals.writeConsole("sendLine(id={0},ip={1}): {2}", _sessionID, _clientIP, ex.Message);
                return false;
            }
        }

        // checks the receive buffer (used for early talkers)
        private bool recvPeek()
        {
            try
            {
                bool result;

            try { result = _client.GetStream().DataAvailable; }
            catch { result = false; }
            return result;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        // receives a line from remote
        private string recvLine()
        {
            string line = null;

            try
            {
                if (_client.Connected)
                    line = _reader.ReadLine();
            }
            catch //(Exception ex)
            {
                //AppGlobals.writeConsole("recvLine(id={0},ip={1}): {2}", _sessionID, _clientIP, ex.Message);
                _timedOut = true;
                line = null;
            }
            return line;
        }

        // receive a full data buffer from remote
        private string recvData()
        {
            try
            {
                StringBuilder buff = new StringBuilder();
                string line = "?";                
                while (null != line)
                {
                    line = recvLine();
                    if (null != line)
                    {
                        buff.AppendLine(line);                     
                        if (line.Equals(".", StringComparison.InvariantCultureIgnoreCase))
                            line = null;
                    }
                }
              
                return buff.ToString();
            }
            catch //(Exception ex)
            {
                //AppGlobals.writeConsole("recvData(id={0},ip={1}): {2}", _sessionID, _clientIP, ex.Message);
                return null;
            }
        }

        // splits an SMTP command into command and argument(s)
        private List<string> parseCmdLine(cmdID id, string cmdLine)
        {
            try
            {
                List<string> parts = new List<string>();
                if (string.IsNullOrEmpty(cmdLine)) return parts;
                try
                {
                    string cmdStr = cmdList[(int)id];
                    string curCmd = cleanupString(cmdLine);

                    int pos = -1;
                    if (cmdStr.Contains(':'))
                        pos = cmdLine.IndexOf(':');
                    else
                        pos = cmdLine.IndexOf(' ');
                    if (-1 != pos)
                    {
                        string cmd = cleanupString(cmdLine.Substring(0, pos));
                        string arg = cleanupString(cmdLine.Substring(pos + 1));
                        parts.Add(cmd.ToUpper());
                        parts.Add(arg);
                    }
                    else
                        parts.Add(cleanupString(cmdLine).ToUpper());
                }
                catch
                {
                    parts = new List<string>();
                }

                return parts;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // cleans a string
        private string cleanupString(string inputStr)
        {
            try
            {
                // setup...
                if (string.IsNullOrEmpty(inputStr)) return null;
                string strBuff = inputStr.Trim();
                char[] chars = strBuff.ToCharArray();
                char chr;

                // turn control chars into spaces
                for (int c = 0; c < chars.Length; c++)
                {
                    chr = chars[c];
                    if ((char.IsWhiteSpace(chr) || char.IsControl(chr)) && (!chr.Equals(' ')))
                    {
                        chars[c] = ' '; // turn controls/tabs/... into spaces
                    }
                }

                // trim, remove double spaces, trim again
                string result = new string(chars).Trim();
                while (result.Contains("  "))
                    result.Replace("  ", " ");
                return result.Trim();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // check for early talkers, that is clients which won't wait
        // for the response and keep sending in commands/stuff, those
        // are usually spambots or the like, so let's deal with them
        private bool isEarlyTalker()
        {
            try
            {
                bool tooEarly = false;
                if (recvPeek())
                {

                    tooEarly = true;
                }
                return tooEarly;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // "sleeps" for the given time
        private void sleepDown(int milliSeconds)
        {
            Thread.Sleep(milliSeconds);
        }      

        // true = the IP falls into a private/reserved range
        // see RFC-1918, RFC-3330, RFC-3927 for details
        private bool isPrivateIP(string IP)
        {
            try
            {
                // 127/8, 10/8, 192.168/16, 169.254/16, 192.0.2/24
                if (IP.StartsWith("127.") ||
                    IP.StartsWith("10.") ||
                    IP.StartsWith("192.168.") ||
                    IP.StartsWith("169.254.") ||
                    IP.StartsWith("192.0.2.")
                    ) return true;

                // 172.16/12
                string[] octets = IP.Split(".".ToCharArray(), 4);
                if (octets[0].Equals("172"))
                {
                    int octet = int.Parse(octets[1]);
                    if ((octet > 15) && (octet < 32)) return true;
                }

                return false;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // reverse an IPv4 and appends the domain name
        private string buildDnsListQuery(string IP, string domain)
        {

            try
            {
                string[] octets = IP.Split(".".ToCharArray(), 4);

                return joinParts(octets[3], octets[2], octets[1], octets[0], domain);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // joins the given parms using dots as separators
        private string joinParts(params string[] args)
        {
            try
            {
                StringBuilder ret = new StringBuilder();
                foreach (String s in args)
                    ret.AppendFormat("{0}.", s);

                return ret.ToString().Substring(0, ret.ToString().Length - 1);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        private void StoreConference(string msgData)
        {
            String requestCall = "";
            Boolean isSuccess = false;

            try
            {
                requestCall = Helper.GetStringInBetween("<myVRM>", "</myVRM>", msgData, true, true).Trim();
                if (msgData.Trim() != "" && requestCall.Trim() != "")
                    isSuccess = myVRMOps.InsertExternalEvent(ref requestCall, ref msgData);


            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            finally
            {
                if(!isSuccess)
                    log.Trace("There was a error while inserting data, please refer above and below line. Data :"+ msgData);
            }
            
        }

        private bool checkMailAddr(string mailAddr)
        {
            try
            {
                // init
                _mailBox = _mailDom = null;
                string email = cleanupString(mailAddr).ToLowerInvariant();

                // shouldn't be empy and must contain at least a @ and a dot
                if (string.IsNullOrEmpty(email)) return false;
                if (!email.Contains('@')) return false;
                if (!email.Contains('.')) return false;

                // if starting with a "<" must end with a ">"
                char[] chars = email.ToCharArray();
                if ('<' == chars[0])
                {
                    if ('>' != chars[email.Length - 1]) return false;
                    email = email.Replace('<', ' ');
                    email = email.Replace('>', ' ');
                    email = cleanupString(email);
                    if (email.Length < 1) return false;
                }

                // can't contain a space
                if (email.Contains(' ')) return false;

                // the "@" must be unique
                string[] parts = email.Split('@');
                if (2 != parts.Length) return false;

                // cleanup and check parts
                for (int p = 0; p < parts.Length; p++)
                {
                    parts[p] = cleanupString(parts[p]);
                    if (string.IsNullOrEmpty(parts[p])) return false;
                }

                // formally checks domain (and TLD)
                if (!parts[1].Contains('.')) return false;
                if (parts[1].StartsWith(".")) return false;
                if (parts[1].EndsWith(".")) return false;
                string[] domain = parts[1].Split('.');
                if (domain.Length < 2) return false;
                for (int p = 0; p < domain.Length; p++)
                {
                    if (string.IsNullOrEmpty(domain[p])) return false;
                    if (domain[p].StartsWith("-")) return false;
                }
                string TLD = domain[domain.Length - 1];
                if (TLD.Length < 2) return false;

                // store mailbox and domain
                _mailBox = parts[0];
                _mailDom = parts[1];

                return true;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        #endregion
    }
}
