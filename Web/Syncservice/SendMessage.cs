﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Net.Mail;
using System.Threading;


namespace SYNCSERVICE
{
    class SendMessage
    {
        #region  Private Variables

        // network/system
        private bool _initOk = false;
        private string _hostName = null;
        private SmtpClient _client;
        private bool _timedOut = false;
        private string _clientIP = null;
        private LOGGER.Log log = null;
        private myVRMOperations myVRMOps = null;
        private string _mailFrom = "";
        private string _rcptTo = "";
        private string _mailDom = "";
        private string _errorCode = "";
        private string _errorMessage = "";
        private string _confID = "";
        private string _subject = "myVRM Email";
        private MESSENGER.ConfigParams _configparams = null;
        private List<MESSENGER.Conference> _confs = null;
        private MESSENGER.Conference _conf = null;
        Attachment _XMLattachment = null;
        MemoryStream _contentStream = null;
        StreamWriter _contentWriter = null;
        MailMessage _contentMailMessage = null;
        private bool _bSuccess = false;
       

        #endregion

        #region  Initialize
        /// <summary>
        /// Initialize the calss with client
        /// </summary>
        /// <param name="client"></param>
        public SendMessage(String clientIPadress, int listenPort, MESSENGER.ConfigParams configParams, String from, String to, myVRMOperations pmyVRMOps)
        {
            try
            {
                _configparams = configParams;
                log = new LOGGER.Log(configParams);
                myVRMOps = pmyVRMOps;
                if (myVRMOps == null)
                    myVRMOps = new myVRMOperations(configParams);
                _client = new SmtpClient(clientIPadress, listenPort); ;
                _mailFrom = from;
                _rcptTo = to;

                log.Trace("Successful Instanziation");

            }
            catch (Exception ex)
            {
                throw ex;// Bad: Exception in initialization

            }
        }
        #endregion


        #region "methods"
        void SendConferences()
        {
            try
            {
                _bSuccess = false;
                if (_conf == null && _contentMailMessage == null && !String.IsNullOrEmpty(_conf.sRequestXML) && _client == null)
                    return;
                if (!GenerateStreamFromString(_conf.sRequestXML))
                    return;
                log.Trace("Initializing Conf send procedure");
                using (_contentStream)
                {
                    _contentMailMessage = new MailMessage();
                    _contentMailMessage.From = new MailAddress(_mailFrom);
                    _contentMailMessage.To.Add(new MailAddress(_rcptTo));
                    _contentMailMessage.Subject = _subject;
                    _XMLattachment = new Attachment(_contentStream, new System.Net.Mime.ContentType("text/xml"));
                    _XMLattachment.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                    _XMLattachment.Name = "myVRMInfo.xml";
                    _XMLattachment.ContentDisposition.DispositionType = "attachment";
                    _contentMailMessage.Attachments.Add(_XMLattachment);
                    _client.Send(_contentMailMessage);
                    _bSuccess = true;
                    
                }

            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }
            finally
            {
                if (_contentStream != null)
                    _contentStream.Dispose();

                if (_contentWriter != null)
                    _contentWriter.Dispose();

                _contentStream = null;
                _contentWriter = null;

                if (_conf != null)
                {
                    _conf.sStatus = (_bSuccess) ? "TS" : "";
                    _conf.iRetryCount += 1;
                }

                _contentMailMessage.Dispose();
                _contentMailMessage = null;
                
            }


        }
        public void SendConferences(ref String xmlData)
        {
            try
            {
                if (xmlData.Trim() != "")
                {
                    myVRMOps.FillConferenceList(ref _confs, xmlData);

                    if (_confs != null && _confs.Count > 0)
                    {
                        for (int j = 0; j < _confs.Count; j++)
                        {
                            _conf = null;
                            _conf = _confs[j];
                            SendConferences();
                        }

                        ProcessResponse();
                    }
                }

            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }
            finally
            {
                
            }


        }
        void ProcessResponse()
        {
            try
            {
                 if (_confs != null && _confs.Count > 0)
                    myVRMOps.UpdateResponse(ref _confs);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        bool GenerateStreamFromString(string content)
        {
            try
            {
                _contentStream = new MemoryStream();
                _contentWriter = new StreamWriter(_contentStream);
                _contentWriter.Write(content);
                _contentWriter.Flush();
                _contentStream.Position = 0;
                
            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
                return false;
            }
            return true;
        }

        #endregion
    }
}
