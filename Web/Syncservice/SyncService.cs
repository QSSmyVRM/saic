﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End

#region References
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Net.Mail;
using System.Threading;
using System.Configuration;
using System.IO;
using SMTP;
using System.Xml;

#endregion

namespace SYNCSERVICE
{
    public partial class SyncService : ServiceBase
    {
        #region "privatedata"
        private static bool timeToStop = false;
        private static TcpListener mailListener = null;
        public static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        public static String COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
        public static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
        public static ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        static CONFIG.Config config = null;
        static MESSENGER.ConfigParams configParams = null;
        public static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        string errMsg = null;
        static LOGGER.Log log = null;
        static bool ret = false;
        static System.Timers.Timer timerSync = new System.Timers.Timer();
        static System.Timers.Timer timerReceive = new System.Timers.Timer();
        static System.Timers.Timer timerSyncEmail = new System.Timers.Timer();
        static System.Timers.Timer timerProcessReceived = new System.Timers.Timer();
        static Thread clientThread = null;
        static int _listenPort = 25;
        private static String _mailDomain = "myVRM.com";
        private static String _mailFrom = "gsivakumar@myvrm.com";
        private static String _mailTo = "gsivakumar@myvrm.com";
        private String _clientIPadress = "", _clinetConfig = "", _clinetEmail = "";
        myVRMOperations myvrmops = null;
        double SyncTime = 30000;
        double ReceiveSyncTime = 10000;

        #endregion

        public SyncService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
        
            try
            {
                // our internal stuff
                config = new CONFIG.Config();
                configParams = new MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath);
                log = new LOGGER.Log(configParams);
                myvrmops = new myVRMOperations(configParams);
                log.Trace("Into The Sync service started");
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);

                timerSync.Elapsed += new System.Timers.ElapsedEventHandler(timerSync_Elapsed);
                timerSync.Interval = SyncTime;
                timerSync.Enabled = true;
                timerSync.AutoReset = true;
                timerSync.Start();

                timerSyncEmail.Elapsed += new System.Timers.ElapsedEventHandler(timerSyncEmail_Elapsed);
                timerSyncEmail.Interval = SyncTime;
                timerSyncEmail.Enabled = true;
                timerSyncEmail.AutoReset = true;
                timerSyncEmail.Start();

                timerReceive.Elapsed += new System.Timers.ElapsedEventHandler(timerReceive_Elapsed);
                timerReceive.Interval = SyncTime - 5000;
                timerReceive.Enabled = true;
                timerReceive.AutoReset = true;
                timerReceive.Start();

                timerProcessReceived.Elapsed += new System.Timers.ElapsedEventHandler(timerProcessReceived_Elapsed);
                timerProcessReceived.Interval = ReceiveSyncTime;
                timerProcessReceived.Enabled = true;
                timerProcessReceived.AutoReset = true;
                timerProcessReceived.Start();
                
            }
            catch (Exception)
            {

                // we got an error
            }


        }

        protected override void OnStop()
        {
            try
            {
                if (mailListener != null)
                {
                    mailListener.Stop();
                    mailListener = null;
                }

               
            }
            catch (Exception)
            {
                
                // DO nothig
            }
        }

        #region timerSync_Elapsed

        void timerSync_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
            string outXML = "";
            try
            {
                timerSync.Stop();
                myvrmops.GetAllEvents(ref outXML);
                SendEvents(outXML);
                timerSync.Interval = SyncTime;
                timerSync.Enabled = true;
                timerSync.AutoReset = true;
                timerSync.Start();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region timerReceive_Elapsed

        void timerReceive_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timerReceive.Stop();
                MailListener();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region timerProcessReceived_Elapsed

        void timerProcessReceived_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timerProcessReceived.Stop();
                myvrmops.ProcessExternalEvent();
                timerProcessReceived.Interval = ReceiveSyncTime;
                timerProcessReceived.Enabled = true;
                timerProcessReceived.AutoReset = true;
                timerProcessReceived.Start();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region timerSyncEmail_Elapsed

        void timerSyncEmail_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double conflauch = 30000;
            try
            {
                timerSyncEmail.Stop();
                myvrmops.SendFailureSyncEmails();
                timerSyncEmail.Interval = conflauch;
                timerSyncEmail.Enabled = true;
                timerSyncEmail.AutoReset = true;
                timerSyncEmail.Start();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region Send Events
        /// <summary>
        /// GetSyncConfs
        /// </summary>
        /// <param name="pinXML"></param>
        private void SendEvents(String pinXML)
        {
            IPAddress listenAddr = IPAddress.Any;                       
            SendMessage sndMessage = null;
            int listenPort = 25;
            List<MESSENGER.Conference> conflist = new List<MESSENGER.Conference>();
            MESSENGER.Conference conf = new MESSENGER.Conference();
            XmlDocument xmldoc = null;
            try
            {
                log.Trace("Into GetAllConfs:" + DateTime.Now.ToLocalTime());
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(pinXML);
                _clientIPadress = xmldoc.SelectSingleNode("//SyncDetails/SyncSettings/SyncAddress").InnerText.Trim();
                int.TryParse(xmldoc.SelectSingleNode("//SyncDetails/SyncSettings/SyncPort").InnerText.Trim(), out listenPort);
                _clinetConfig = xmldoc.SelectSingleNode("//SyncDetails/SyncSettings/SyncConfig").InnerText.Trim();
                _clinetEmail = xmldoc.SelectSingleNode("//SyncDetails/SyncSettings/SyncEmailAddress").InnerText.Trim();
                if (listenPort <= 0)                
                    listenPort = 25;
                if (_listenPort != listenPort)
                {
                    _listenPort = listenPort;
                    
                    try 
	                {
                        if (mailListener != null)
                        {
                            mailListener.Stop();
                            mailListener = null;
                        }

                        MailListener();

	                }
	                catch (Exception exInner)
	                {

                        log.Trace("Port change Failed " +  exInner.StackTrace + exInner.ToString());
	                }
                    
                }
                sndMessage = new SendMessage(_clientIPadress,_listenPort, configParams, _mailFrom, _mailTo,myvrmops);
                sndMessage.SendConferences(ref pinXML);
                sndMessage = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
            finally
            {
                //com = null;
            }
        }
        #endregion

        #region Start Mail Listener
        /// <summary>
        /// GetSyncConfs
        /// </summary>
        /// <param name="pinXML"></param>
        private void MailListener()
        {
            IPAddress listenAddr = IPAddress.Any;
            IAsyncResult result = null;
            ReceiveMessage rcvdMessage = null;
            try
            {
                log.Trace("Starting Listener:" + DateTime.Now.ToLocalTime());
                try
                {
                    if (_listenPort <= 0)
                        _listenPort = 25;

                    mailListener = new TcpListener(IPAddress.Any, _listenPort);
                    mailListener.Start();
                }
                catch (Exception ex)
                {
                    log.Trace("Error While starting Listener:Where=" + ex.StackTrace + " What=" + ex.Message + " IE=" + ex.InnerException + " When=" + DateTime.Now.ToLocalTime());
                }

                
                    try
                    {
                        while (!timeToStop)
                        {
                            rcvdMessage = new ReceiveMessage(mailListener.AcceptTcpClient(), configParams, myvrmops);
                            ThreadPool.QueueUserWorkItem(HandleIncomingMail, rcvdMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Trace("Error While waiting for incoming:Where=" + ex.StackTrace + " What=" + ex.Message + " IE=" + ex.InnerException + " When=" + DateTime.Now.ToLocalTime());

                    }

            }
            catch (Exception ex)
            {
                log.Trace("Error in Mail Listener:Where=" + ex.StackTrace + " What=" + ex.Message + " IE=" + ex.InnerException + " When=" + DateTime.Now.ToLocalTime());
            }
            finally
            {
                // finalize
                if (null != mailListener)
                {
                    try { mailListener.Stop(); }
                    catch { }
                }
            }
        }
        #endregion

        #region "settings"
        // loads/parses the config values
        private void loadConfig()
        {
            // listen address

            // receive timeout
            int receiveTimeout = int.Parse(ConfigurationManager.AppSettings["ReceiveTimeOut"]);
            if (receiveTimeout < 0)
                receiveTimeout = 0;

            // hostname (for the banner)
            string hostName = ConfigurationManager.AppSettings["HostName"];
            if (string.IsNullOrEmpty(hostName))
                hostName = System.Net.Dns.GetHostEntry("").HostName;

            // true=emits a "tempfail" when receiving the DATA command
            bool doTempFail = bool.Parse(ConfigurationManager.AppSettings["DoTempFail"]);

            // true=stores the email envelope and data into files
            bool storeData = bool.Parse(ConfigurationManager.AppSettings["StoreData"]);

            // max size for a given email message
            long storeSize = long.Parse(ConfigurationManager.AppSettings["MaxDataSize"]);
            if (storeSize < 0) storeSize = 0;

            // max # of messages for a session
            int maxMsgs = int.Parse(ConfigurationManager.AppSettings["MaxMessages"]);
            if (maxMsgs < 1) maxMsgs = 10;

            // path for the email storage
            string storePath = ConfigurationManager.AppSettings["StorePath"];
            if (String.IsNullOrEmpty(storePath))
                storePath = Path.GetTempPath();
            if (!storePath.EndsWith("\\"))
                storePath = storePath + "\\";

            // max # of parallel sessions, further requests will be rejected
            long maxSessions = long.Parse(ConfigurationManager.AppSettings["MaxSessions"]);
            if (maxSessions < 1) maxSessions = 16;

            // path for the log file
            string logPath = ConfigurationManager.AppSettings["LogPath"];
            if (String.IsNullOrEmpty(logPath))
                logPath = Path.GetTempPath();
            if (!logPath.EndsWith("\\"))
                logPath = logPath + "\\";

            // verbose logging
            bool verboseLog = bool.Parse(ConfigurationManager.AppSettings["VerboseLogging"]);

            // early talker detection
            bool earlyTalk = bool.Parse(ConfigurationManager.AppSettings["DoEarlyTalk"]);

            // DNS whitelist providers, empty to not perform the check
            string whiteLists = ConfigurationManager.AppSettings["RWLproviders"];
            string[] RWL = null;
            if (!string.IsNullOrEmpty(whiteLists))
            {
                RWL = whiteLists.Split(',');
            }

            // DNS blacklist providers, empty to not perform the check
            string blackLists = ConfigurationManager.AppSettings["RBLproviders"];
            string[] RBL = null;
            if (!string.IsNullOrEmpty(blackLists))
            {
                RBL = blackLists.Split(',');
            }

            // hardlimits for errors, noop etc..
            int maxErrors = int.Parse(ConfigurationManager.AppSettings["MaxSmtpErrors"]);
            if (maxErrors < 1) maxErrors = 5;
            int maxNoop = int.Parse(ConfigurationManager.AppSettings["MaxSmtpNoop"]);
            if (maxNoop < 1) maxNoop = 7;
            int maxVrfy = int.Parse(ConfigurationManager.AppSettings["MaxSmtpVrfy"]);
            if (maxVrfy < 1) maxVrfy = 10;
            int maxRcpt = int.Parse(ConfigurationManager.AppSettings["MaxSmtpRcpt"]);
            if (maxRcpt < 1) maxRcpt = 100;

            // delays (tarpitting)
            int bannerDelay = int.Parse(ConfigurationManager.AppSettings["BannerDelay"]);
            if (bannerDelay < 0) bannerDelay = 0;
            int errorDelay = int.Parse(ConfigurationManager.AppSettings["ErrorDelay"]);
            if (errorDelay < 0) errorDelay = 0;

            // local domains and mailboxes
            List<string> domains = new List<string>();
            List<string> mailboxes = new List<string>();
            string fileName = ConfigurationManager.AppSettings["LocalDomains"];
            if (!string.IsNullOrEmpty(fileName))
                domains = AppGlobals.loadFile(fileName);
            fileName = ConfigurationManager.AppSettings["LocalMailBoxes"];
            if (!string.IsNullOrEmpty(fileName))
                mailboxes = AppGlobals.loadFile(fileName);

            // set the global values

            AppGlobals.receiveTimeout = receiveTimeout;
            AppGlobals.hostName = hostName.ToLower();
            AppGlobals.doTempFail = doTempFail;
            AppGlobals.storeData = storeData;
            AppGlobals.maxDataSize = storeSize;
            AppGlobals.maxMessages = maxMsgs;
            AppGlobals.storePath = storePath;
            AppGlobals.maxSessions = maxSessions;
            AppGlobals.logPath = logPath;
            AppGlobals.logVerbose = verboseLog;
            AppGlobals.earlyTalkers = earlyTalk;
            AppGlobals.whiteLists = RWL;
            AppGlobals.blackLists = RBL;
            AppGlobals.maxSmtpErr = maxErrors;
            AppGlobals.maxSmtpVrfy = maxVrfy;
            AppGlobals.maxSmtpRcpt = maxRcpt;
            AppGlobals.bannerDelay = bannerDelay;
            AppGlobals.errorDelay = errorDelay;
        }
        #endregion

        #region Client Handling Threads

        private void HandleIncomingMail(object objrcvdMessage)
        {
            ReceiveMessage rcvdMessage = null;
            try
            {
                if (objrcvdMessage != null)
                {
                    rcvdMessage = (ReceiveMessage)objrcvdMessage;
                    if (rcvdMessage != null)
                    {
                        rcvdMessage.handleSession();
                        rcvdMessage.closeSession();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }

        }

        private void HandleIncomingMail(IAsyncResult result)
        {
            ReceiveMessage rcvdMessage = null;
            TcpListener mailListener = null;
            TcpClient mailServer = null;
            try
            {
                mailListener = (TcpListener)result.AsyncState;
                mailServer = mailListener.EndAcceptTcpClient(result);
                rcvdMessage = new ReceiveMessage(mailServer, configParams,myvrmops);
                rcvdMessage.handleSession();
                rcvdMessage.closeSession();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
           
        }
        #endregion
    }
}
