﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using ns_SqlHelper;
using System.Threading;
using System.Data;
using System.Xml.XPath;
using System.Text.RegularExpressions;


namespace SYNCSERVICE
{
    class myVRMOperations
    {
        #region Private Variables

        static String dirPth = SyncService.dirPth;
        String MyVRMServer_ConfigPath = SyncService.MyVRMServer_ConfigPath;
        String COM_ConfigPath = SyncService.COM_ConfigPath;
        String RTC_ConfigPath = SyncService.RTC_ConfigPath;
        string configPath = SyncService.configPath;
        LOGGER.Log log = null;
        System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);
        ASPIL.VRMServer myVRMASPIL = null;
        String sCommand = "";
        VRMRTC.VRMRTC obj = null;
        SqlHelper sqlCon = null;

        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        public String processErrMsg = "";
        enum processType { CUD = 1, PU  }
        static readonly Regex binary = new Regex("^[01]{1,32}$", RegexOptions.Compiled);
        MESSENGER.EventRequests eventRequests = null;
        String xmlPUEnvelope = "<myVRM><commandname>10</commandname><originconfid>{0}</originconfid><origineventid>{1}</origineventid><command>{2}</command></myVRM>";

        public myVRMOperations(MESSENGER.ConfigParams configParams)
        {
            log = new LOGGER.Log(configParams);
            if(sqlCon == null)
                sqlCon = new SqlHelper(MyVRMServer_ConfigPath);
            Thread.CurrentThread.CurrentCulture = globalCultureInfo;

        }

        public myVRMOperations()
        {
            
        }

        public enum MasterSlave
        {
            Master = 1,
            Slave
        }

        #endregion

        #region SetConference
        /// <summary>
        /// Create Conference
        /// </summary>
        /// <param name="inXML"></param>
        /// <param name="errCode"></param>
        /// <param name="errMessage"></param>
        /// <param name="confID"></param>
        public void SetConference(ref String inXML, ref String errCode, ref String errMessage, ref String confID)
        {
            
            String outXML = "";
            XmlDocument xmlout = null;
            try
            {
                sCommand = "SetConference";

                if (String.IsNullOrEmpty(inXML))
                    return ;

                myVRMASPIL = new ASPIL.VRMServer();
                outXML = myVRMASPIL.Operations(MyVRMServer_ConfigPath, sCommand, inXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    ProcessErrorMessage(ref outXML, ref errCode, ref errMessage);
                    return ;
                }

                xmlout = new XmlDocument();
                xmlout.LoadXml(outXML);
                if (xmlout.SelectSingleNode("//setConference/conferences/conference/confID") != null)
                    confID = xmlout.SelectSingleNode("//setConference/conferences/conference/confID").InnerText;

                
            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }
            
        }

        public bool SetConference(ref MESSENGER.EventRequests eventRequests)
        {
            bool ret = false;
            try
            {
                if (eventRequests == null)
                    eventRequests = new MESSENGER.EventRequests();

                sCommand = "SetConference";
                eventRequests.sTypeID = "Edit/Create";

                if (String.IsNullOrEmpty(eventRequests.sInXML))
                    return false;

                myVRMASPIL = new ASPIL.VRMServer();
                eventRequests.sOutXML = myVRMASPIL.Operations(MyVRMServer_ConfigPath, sCommand, eventRequests.sInXML);

                if (eventRequests.sOutXML.IndexOf("<error>") >= 0)
                    ProcessErrorMessage(ref eventRequests.sOutXML, ref eventRequests.sProcessErrorMsg);

                ret = true; 

            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }
            return ret;
        }
        #endregion

        #region DeleteConference
        /// <summary>
        /// Delete Conference
        /// </summary>
        /// <param name="inXML"></param>
        /// <param name="errCode"></param>
        /// <param name="errMessage"></param>
        /// <param name="confID"></param>
        public void DeleteConference(ref String inXML, ref String errCode, ref String errMessage, ref String confID)
        {
            String outXML = "";
            XmlDocument xmlout = null;
            try
            {
                sCommand = "DeleteConference";

                if (String.IsNullOrEmpty(inXML))
                    return;

                myVRMASPIL = new ASPIL.VRMServer();
                outXML = myVRMASPIL.Operations(MyVRMServer_ConfigPath, sCommand, inXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    ProcessErrorMessage(ref outXML, ref errCode, ref errMessage);
                    return;
                }

                xmlout = new XmlDocument();
                xmlout.LoadXml(outXML);
                if (xmlout.SelectSingleNode("//conferences/conference/confID") != null)
                    confID = xmlout.SelectSingleNode("//conferences/conference/confID").InnerText;

            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }
        }

        public bool DeleteConference(ref MESSENGER.EventRequests eventRequests)
        {
            bool ret = false;
            XmlDocument xmlout = null;
            try
            {
                if (eventRequests == null)
                    eventRequests = new MESSENGER.EventRequests();

                sCommand = "DeleteConference";

                eventRequests.sTypeID = "Delete";

                if (String.IsNullOrEmpty(eventRequests.sInXML))
                    return false;

                myVRMASPIL = new ASPIL.VRMServer();
                eventRequests.sOutXML = myVRMASPIL.Operations(MyVRMServer_ConfigPath, sCommand, eventRequests.sInXML);

                if (eventRequests.sOutXML.IndexOf("<error>") >= 0)
                    ProcessErrorMessage(ref eventRequests.sOutXML, ref eventRequests.sProcessErrorMsg);

                ret = true;

            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }
            return ret;
        }
        #endregion

        #region Conference Operation
        /// <summary>
        /// Conference Operation
        /// </summary>
        /// <param name="inXML"></param>
        /// <param name="errCode"></param>
        /// <param name="errMessage"></param>
        /// <param name="confID"></param>
        public void ConferenceOperation(ref String inXML, ref String errCode, ref String errMessage, ref String confID)
        {
            string pXml = "";
            XmlDocument xd = new XmlDocument();
            XmlNode node = null;
            try
            {

                if (String.IsNullOrEmpty(inXML))
                    return;

                inXML = inXML.Remove(inXML.LastIndexOf('.'));



                xd = new XmlDocument();
                xd.LoadXml(inXML);
                node = xd.FirstChild;
                pXml = node.InnerXml;
                pXml += "<FromService>1</FromService>";
                xd.FirstChild.InnerXml = pXml;
                inXML = xd.InnerXml;



                if (inXML.ToLower().Trim().Contains("<delconference>"))
                    DeleteConference(ref inXML, ref errCode, ref errMessage, ref confID);
                else
                    SetConference(ref inXML, ref errCode, ref errMessage, ref confID);

            }
            catch (Exception ex)
            {

                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }
        }

        public bool ConferenceOperation(ref MESSENGER.EventRequests eventRequests)
        {
            string pXml = "";
            XmlDocument xd = new XmlDocument();
            XmlNode node = null;
            bool ret = false;
            try
            {
                if (eventRequests == null)
                    eventRequests = new MESSENGER.EventRequests();

                if (String.IsNullOrEmpty(eventRequests.sInXML))
                    return false;

                xd = new XmlDocument();
                xd.LoadXml(eventRequests.sInXML);
                node = xd.FirstChild;
                pXml = node.InnerXml;
                pXml += "<FromService>1</FromService>";
                xd.FirstChild.InnerXml = pXml;
                eventRequests.sInXML = xd.InnerXml;

                if (eventRequests.sInXML.ToLower().Trim().Contains("<delconference>"))
                    ret = DeleteConference(ref eventRequests);
                else
                    ret = SetConference(ref eventRequests);

            }
            catch (Exception ex)
            {
                eventRequests.sProcessErrorMsg = "Error in ConferenceOperation" + ex.Message;
                log.Trace("Where: " + ex.StackTrace + ", What: " + ex.Message + "(" + ex.InnerException + "), When: " + DateTime.UtcNow.ToString());
            }

            return ret;
        }
        #endregion

        #region ProcessErrorMessage
        /// <summary>
        /// ProcessErrorMessage
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public void ProcessErrorMessage(ref string xmlString, ref String errCode, ref String errMessage)
        {
            XmlDocument xmlDOC = null;
            errMessage = "Please contact your VRM Administrator";
            errCode = "200";
            try
            {
                if (xmlString.IndexOf("<error>") > 0)
                {
                    errMessage = "Please contact your VRM Administrator";
                    errCode = "200";
                    xmlDOC = new XmlDocument();
                    xmlDOC.LoadXml(xmlString);
                    if (xmlDOC.SelectSingleNode("//error/message") != null)
                        errCode = xmlDOC.SelectSingleNode("//error/message").InnerText;
                    if (xmlDOC.SelectSingleNode("//error/errorCode") != null)
                        errMessage += xmlDOC.SelectSingleNode("//error/errorCode").InnerText;
                }

            }
            catch (Exception ex)
            {
                errMessage = "Please contact your VRM Administrator";
                errCode = "112";

                //return "Error 122: Please contact your VRM Administrator";
            }
        }

        public void ProcessErrorMessage(ref string xmlString,ref String sErrMessage)
        {
            XmlDocument xmlDOC = null;
            String errMessage = "Please contact your VRM Administrator";
            String errCode = "200";
            try
            {
                if (xmlString.IndexOf("<error>") > 0)
                {
                    errMessage = "Please contact your VRM Administrator";
                    errCode = "200";
                    xmlDOC = new XmlDocument();
                    xmlDOC.LoadXml(xmlString);
                    if (xmlDOC.SelectSingleNode("//error/message") != null)
                        errMessage = xmlDOC.SelectSingleNode("//error/message").InnerText;
                    if (xmlDOC.SelectSingleNode("//error/errorCode") != null)
                        errCode = xmlDOC.SelectSingleNode("//error/errorCode").InnerText;
                }

                sErrMessage = errCode + " " + errMessage;

            }
            catch (Exception ex)
            {
                errMessage = "Please contact your VRM Administrator";
                errCode = "112";
                sErrMessage += errCode + " " + errMessage;
                //return "Error 122: Please contact your VRM Administrator";
            }
        }
        #endregion

        #region Update Response
        /// <summary>
        /// Update Response
        /// </summary>
        /// <param name="message"></param>
        /// <param name="confNumname"></param>
        /// <param name="externalConfID"></param>
        public void UpdateResponse(ref List<MESSENGER.Conference> confs)
        {
            MESSENGER.Conference conf = null;
            StringBuilder inXML= new StringBuilder();
            string outXML = "";
            try
            {
                if (confs != null && confs.Count > 0)
                {
                    // need to update the retry count and then confid if present
                    if (obj == null)
                        obj = new VRMRTC.VRMRTC();

                    inXML.Append("<UpdateConference>");
                    inXML.Append("<Conferences>");
                    for (int j = 0; j < confs.Count; j++)
                    {
                        conf = confs[j];
                        inXML.Append("<Conference>");
                        inXML.Append("<ConfUID>" + conf.iConfNumname + "</ConfUID>");
                        inXML.Append("<RetryCount>" + conf.iRetryCount + "</RetryCount>");
                        inXML.Append("<TypeID>" + conf.sTypeID + "</TypeID>");
                        inXML.Append("<Status>" + conf.sStatus + "</Status>");
                        inXML.Append("</Conference>");
                    }
                    inXML.Append("</Conferences>");
                    inXML.Append("</UpdateConference>");

                    outXML = obj.Operations(RTC_ConfigPath, "UpdateSyncEventService", inXML.ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        log.Trace("Error in updating conferences, Message : " + outXML);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Fill Conference List
        /// <summary>
        /// Fill Conference List
        /// </summary>
        public void FillConferenceList(ref List<MESSENGER.Conference> confs, string INXML)
        {
            MESSENGER.Conference conf = null;
            XmlDocument xmldoc = null;
            try
            {
                // need to fill list of messenger.conf
                if (confs == null)
                    confs = new List<MESSENGER.Conference>();
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(INXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SyncDetails/SyncConferences/SyncConference");
                if (nodes != null)
                {
                    log.Trace("Number of Sync Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        conf = new MESSENGER.Conference();

                        int.TryParse(nodes[cnfcnt].SelectSingleNode("Confnumname").InnerText.Trim(), out conf.iConfNumname);
                        conf.sTypeID = nodes[cnfcnt].SelectSingleNode("TypeID").InnerText.Trim();
                        conf.sRequestXML = nodes[cnfcnt].SelectSingleNode("RequestCall").InnerXml.Trim();
                        conf.sResponse = nodes[cnfcnt].SelectSingleNode("ResponseCall").InnerText.Trim();
                        conf.sMessage = nodes[cnfcnt].SelectSingleNode("StatusMessage").InnerText.Trim();
                        conf.sEventTime = nodes[cnfcnt].SelectSingleNode("EventTime").InnerText.Trim();
                        conf.sStatus = nodes[cnfcnt].SelectSingleNode("Status").InnerText.Trim();
                        conf.sStatusDate = nodes[cnfcnt].SelectSingleNode("StatusDate").InnerText.Trim();
                        int.TryParse(nodes[cnfcnt].SelectSingleNode("RetryCount").InnerText.Trim(), out conf.iRetryCount);
                        int.TryParse(nodes[cnfcnt].SelectSingleNode("OrgID").InnerText.Trim(), out conf.iOrgID);

                        confs.Add(conf);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void FillConferenceList(ref List<MESSENGER.Conference> confs)
        {
            MESSENGER.Conference conf = null;
            XmlDocument xmldoc = null;
            string INXML = "";
            try
            {
                if (obj == null)
                    obj = new VRMRTC.VRMRTC();

                INXML = obj.Operations(RTC_ConfigPath, "TriggerSyncEventService", "<admin>11</admin>");

                // need to fill list of messenger.conf
                if (confs == null)
                    confs = new List<MESSENGER.Conference>();
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(INXML);
                log.Trace("From Server :" + INXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SyncDetails/SyncConferences/SyncConference");
                if (nodes != null)
                {
                    log.Trace("Number of Sync Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        conf = new MESSENGER.Conference();

                        int.TryParse(nodes[cnfcnt].SelectSingleNode("Confnumname").InnerText.Trim(), out conf.iConfNumname);
                        conf.sTypeID = nodes[cnfcnt].SelectSingleNode("TypeID").InnerText.Trim();
                        conf.sRequestXML = nodes[cnfcnt].SelectSingleNode("RequestCall").InnerXml.Trim();
                        conf.sResponse = nodes[cnfcnt].SelectSingleNode("ResponseCall").InnerText.Trim();
                        conf.sMessage = nodes[cnfcnt].SelectSingleNode("StatusMessage").InnerText.Trim();
                        conf.sEventTime = nodes[cnfcnt].SelectSingleNode("EventTime").InnerText.Trim();
                        conf.sStatus = nodes[cnfcnt].SelectSingleNode("Status").InnerText.Trim();
                        conf.sStatusDate = nodes[cnfcnt].SelectSingleNode("StatusDate").InnerText.Trim();
                        int.TryParse(nodes[cnfcnt].SelectSingleNode("RetryCount").InnerText.Trim(), out conf.iRetryCount);
                        int.TryParse(nodes[cnfcnt].SelectSingleNode("OrgID").InnerText.Trim(), out conf.iOrgID);

                        confs.Add(conf);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Get All Events
        /// <summary>
        /// Fill Conference List
        /// </summary>

        public void GetAllEvents(ref String outXML)
        {
           
            try
            {
                if (obj == null)
                    obj = new VRMRTC.VRMRTC();

                outXML = obj.Operations(RTC_ConfigPath, "TriggerSyncEventService", "<admin>11</admin>");
                if (outXML.Trim().IndexOf("<error>") == 0)
                {
                    log.Trace("Error in getting conferences, Message : " + outXML);
                    outXML = "";
                }
                
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Send Failure Sync Emails
        /// <summary>
        /// Send Failure sync emails..
        /// </summary>

        public void SendFailureSyncEmails()
        {

            try
            {
                if (obj == null)
                    obj = new VRMRTC.VRMRTC();

                string OUTXML = obj.Operations(RTC_ConfigPath, "FailureSyncEmail", "<admin>11</admin>");
                if (OUTXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Sending Mail : " + OUTXML);

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region InsertExternalEvent
        /// <summary>
        /// InsertExternalEvent
        /// </summary>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal bool InsertExternalEvent(ref String message, ref String mailData)
        {
            int ret = 0;
            string query = "";
            try
            {
                query = "Insert into NW_Event_D(StatusDate,[Status],StatusMessage,RequestCall,CompleteEvent, RetryCount, NetworkProcessed, orgId) values ('"
                      + DateTime.UtcNow +"','"
                      + "N','"
                      + "Received not Processed','"
                      + message + "','"
                      + mailData + "',"
                      +"0,"
                      + "0,"
                      + "0"
                      + ")";

                if (sqlCon == null)
                    sqlCon = new SqlHelper(MyVRMServer_ConfigPath);
                sqlCon.OpenConnection();
                ret = sqlCon.ExecuteNonQuery(query);
                sqlCon.CloseConnection();
                if (ret <= 0)
                {
                    log.Exception(100, " Insert event query failed. query =" + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Insert event query failed. :" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region ProcessExternalEvent
        /// <summary>
        /// InsertExternalEvent
        /// </summary>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal bool ProcessExternalEvent()
        {
            int ret = 0;
            string query = "", sStatus = "E", sStatusMessage = "Error processing event";
            DataSet dsEvents = null;
            DataTable dtEvents = null;
            DataRow drEvent = null;
            try
            {
                query = "Select * from NW_Event_D where status = 'N'";
                     

                if (sqlCon == null)
                    sqlCon = new SqlHelper(MyVRMServer_ConfigPath);

                dsEvents = sqlCon.ExecuteDataSet(query);
                query = "";
                if (dsEvents != null && dsEvents.Tables.Count > 0)
                {
                    dtEvents = dsEvents.Tables[0];
                    for (int i = 0; i < dtEvents.Rows.Count; i++)
                    {
                        drEvent = dsEvents.Tables[0].Rows[i];
                        sStatus = "S";
                        sStatusMessage = "Success processing event";
                        eventRequests = new MESSENGER.EventRequests();
                        if (!ProcessExternalEvent(ref drEvent, ref eventRequests))
                        {
                            sStatus = "E";
                            sStatusMessage = eventRequests.sProcessErrorMsg;
                        }
                        query += " Update NW_Event_D set Status = '" + sStatus + "',StatusMessage = '" + sStatusMessage + "',"
                               + " StatusDate = cast('" + DateTime.Now.ToString() + "' as datetime) where UID = " + drEvent["UId"].ToString() +";";
                    }
                    

                }

                if (query.Trim() != "")
                {
                    sqlCon.OpenConnection();
                    ret = sqlCon.ExecuteNonQuery(query);
                    sqlCon.CloseConnection();
                    if (ret <= 0)
                    {
                        log.Exception(100, "query failed. query =" + query);
                        return false;
                    }     
                }
            }
            catch (Exception ex)
            {
                log.Trace("Insert event query failed. :" + ex.Message);
                return false;
            }
            return true;
        }

        private bool ProcessExternalEvent(ref DataRow eventRow, ref MESSENGER.EventRequests eventRequests)
        {
            bool ret = false;
            string query = "", command = "", inXML = "",  originConfID = "", originEventID = "";
            int iCmd = 0, iOriginConfID = 0, iOriginEventID = 0 ;
            try
            {
                if (eventRequests == null)
                    eventRequests = new MESSENGER.EventRequests();

                if (eventRow == null)
                {
                    eventRequests.sErrorMessage = "Event Null is null";
                    return false;
                }

                if (eventRow.IsNull("RequestCall"))
                {
                    eventRequests.sErrorMessage = "Event request element is null";
                    return false;
                }

                if (String.IsNullOrEmpty(eventRow["RequestCall"].ToString()))
                {
                    eventRequests.sErrorMessage = "Event request element is empty";
                    return false;
                }

                using (xStrReader = new StringReader(eventRow["RequestCall"].ToString().Trim()))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//myVRM/commandname");

                        if (xNode != null)
                            command = xNode.Value.Trim();

                        if (String.IsNullOrEmpty(command))
                        {
                            eventRequests.sErrorMessage = "Command is empty";
                            return false;
                        }

                         if (!binary.IsMatch(command))
                        {
                            eventRequests.sErrorMessage = "Invalid command";
                            return false;
                        }
                        
                         iCmd = Convert.ToInt32(command,2);

                        xNode = xNavigator.SelectSingleNode("//myVRM/command");

                        if (xNode != null)
                            inXML = xNode.InnerXml.Trim();


                        if (String.IsNullOrEmpty(inXML))
                        {
                            eventRequests.sErrorMessage = "Command is empty";
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//myVRM/originconfid");

                        if (xNode != null)
                            originConfID = xNode.Value.Trim();


                        if (String.IsNullOrEmpty(originConfID))
                        {
                            eventRequests.sErrorMessage = "Origin Conf ID is empty";
                            return false;
                        }

                        if (!Int32.TryParse(originConfID,out iOriginConfID))
                        {
                            eventRequests.sErrorMessage = "Origin Conf ID is not valid";
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//myVRM/origineventid");

                        if (xNode != null)
                            originEventID = xNode.Value.Trim();


                        if (String.IsNullOrEmpty(originEventID))
                        {
                            eventRequests.sErrorMessage = "Origin Event ID is empty";
                            return false;
                        }

                         if (!Int32.TryParse(originEventID,out iOriginEventID))
                        {
                            eventRequests.sErrorMessage = "Origin Event ID is not valid";
                            return false;
                        }
                        
                    }
                }

                eventRequests.iCommandID = iCmd;
                eventRequests.sInXML = inXML;
                eventRequests.iOriginConfID = iOriginConfID;
                eventRequests.iOriginEventID = iOriginEventID;

                ret = ProcessExternalEvent(ref eventRequests);
               
               
            }
            catch (Exception ex)
            {
                log.Trace("Insert event query failed. :" + ex.Message);
                ret=  false;
            }
            return ret;
        }

        private bool ProcessExternalEvent(ref MESSENGER.EventRequests eventRequests)
        {
            bool ret = false;
            string query = "";
            try
            {
                if (eventRequests == null)
                    eventRequests = new MESSENGER.EventRequests();

                if (String.IsNullOrEmpty(eventRequests.sInXML))
                {
                    eventRequests.sErrorMessage = "Command input is empty";
                    return false;
                }

                switch (eventRequests.iCommandID)
                {

                    case (int)processType.CUD:
                        {
                            ret = ConferenceOperation(ref eventRequests);
                            if (ret)
                                ret = UpdateProcessUpdates(ref eventRequests);
                            break;
                        }

                    case (int)processType.PU:
                        {
                            ret = ProcessUpdates(ref eventRequests);
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                log.Trace("Insert event query failed. :" + ex.Message);
                ret =  false;
            }
            return ret;
        }

        private bool ProcessUpdates(ref MESSENGER.EventRequests eventRequests)
        {
            int ret = 0;
            string query = "", command = "",sConfID = "",sStatus = "S"; 
            int iCmd = 0;
            bool isError = false;
            try
            {
                if (eventRequests == null)
                    eventRequests = new MESSENGER.EventRequests();

                if (String.IsNullOrEmpty(eventRequests.sInXML))
                {
                    eventRequests.sErrorMessage = "Command input is empty";
                    return false;
                }


                if (eventRequests.sInXML.IndexOf("<error>") >= 0)
                {
                    ProcessErrorMessage(ref eventRequests.sInXML, ref eventRequests.sProcessErrorMsg);
                    isError = true;
                    sStatus = "E";

                }
                else
                {

                    using (xStrReader = new StringReader(eventRequests.sInXML))
                    {
                        xDoc = new XPathDocument(xStrReader);
                        {
                            xNavigator = xDoc.CreateNavigator();

                            xNode = xNavigator.SelectSingleNode("//setConference/conferences/conference/confID");

                            if (xNode != null)
                                sConfID = xNode.Value.Trim();
                            else
                            {
                                xNode = xNavigator.SelectSingleNode("//conferences/conference/confID");

                                if (xNode != null)
                                    sConfID = xNode.Value.Trim();
                            }


                            if (sConfID.Trim() == "")
                            {
                                isError = true;
                                eventRequests.sProcessErrorMsg = "Conf ID is empty.";
                                sStatus = "E";
                            }

                        }
                    }
                }

                if (sqlCon == null)
                    sqlCon = new SqlHelper(MyVRMServer_ConfigPath);

                query = " Update ES_Event_D set Status = '" + sStatus + "',StatusMessage = '" + eventRequests.sProcessErrorMsg + "',"
                     + " StatusDate = cast('" + DateTime.Now.ToString() + "' as datetime), RequestID = '" + sConfID + "'"
                     + " where ConfNumName = " + eventRequests.iOriginConfID + " and UID = " + eventRequests.iOriginEventID + " ;"
                     + " Update Conf_Conference_D set ESID = '" + sConfID + "', Pushedtoexternal = 1 where ConfNumName = " + eventRequests.iOriginConfID;
                sqlCon.OpenConnection();
                ret = sqlCon.ExecuteNonQuery(query);
                sqlCon.CloseConnection();
                if (ret <= 0)
                {
                    log.Exception(100, "query failed. query =" + query);
                    return false;
                }        


            }
            catch (Exception ex)
            {
                log.Trace("Insert event query failed. :" + ex.Message);
                return false;
            }
            return true;
        }

        private bool UpdateProcessUpdates(ref MESSENGER.EventRequests eventRequests)
        {
            int ret = 0;
            string query = "", sCommand = "";
            try
            {
                if (eventRequests == null)
                    eventRequests = new MESSENGER.EventRequests();

                if (String.IsNullOrEmpty(eventRequests.sOutXML))
                {
                    eventRequests.sErrorMessage = "Command output is empty";
                    return false;
                }

                sCommand = String.Format(xmlPUEnvelope, eventRequests.iOriginConfID, eventRequests.iOriginEventID, eventRequests.sOutXML);
                
                if (sqlCon == null)
                    sqlCon = new SqlHelper(MyVRMServer_ConfigPath);

                query = "Insert into ES_Event_D(RequestID,TaskType,TypeID,CustomerID,EventTrigger,EventTime,[Status],StatusDate,StatusMessage,RequestCall,ResponseCall,ConfNumName, RetryCount, orgId) values ('"
                      + "','PU"
                      + "','"+ eventRequests.sTypeID
                      + "','"
                      + "','"
                      + "', cast('"
                      + DateTime.Now.ToString() + "' as datetime),'N"
                      + "',cast('"
                      + DateTime.Now.ToString() + "' as datetime),'"
                      + eventRequests.sProcessErrorMsg + "','"
                      + sCommand + "','"
                      + "','"
                      + eventRequests.iConfNumname.ToString() + "',"
                      + "0,"
                      + "11"
                      + ")";
                sqlCon.OpenConnection();
                ret = sqlCon.ExecuteNonQuery(query);
                sqlCon.CloseConnection();
                if (ret <= 0)
                {
                    log.Exception(100, "query failed. query =" + query);
                    return false;
                }


            }
            catch (Exception ex)
            {
                log.Trace("Insert event query failed. :" + ex.Message);
                return false;
            }
            return true;
        }



        #endregion

     }
}
