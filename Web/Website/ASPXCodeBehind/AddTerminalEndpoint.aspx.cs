/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for AddTerminalEndpoint
/// This page will be called from 3 places;
/// 1. When you add a new endpoint from ManageConference and click on "NEW ENDPOINT"
/// 2. When you add a new endpoint from ManageConference and select an existing endpoint and profile
/// 3. When you want to edit an already existing endpoint in conference
/// </summary>
/// 
namespace ns_MyVRM
{
    public partial class AddTerminalEndpoint : System.Web.UI.Page
    {
        #region Server Side controls on form
        protected System.Web.UI.WebControls.Label lblTerminalType;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblNoIPServices;
        protected System.Web.UI.WebControls.Label lblNoISDNServices;

        protected System.Web.UI.WebControls.TextBox txtEndpointID;
        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtEndpointLastName;
        protected System.Web.UI.WebControls.TextBox txtEndpointEmail;
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.TextBox txtURL;
        protected System.Web.UI.WebControls.TextBox txtMCUServiceAddress;
        
        protected System.Web.UI.WebControls.DataGrid dgIPServices;
        protected System.Web.UI.WebControls.DataGrid dgISDNServices;
        
        protected System.Web.UI.WebControls.Table tblEndpointName;
        
        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstMCUAddressType;
        protected System.Web.UI.WebControls.DropDownList lstConnection;
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstProtocol;

        protected System.Web.UI.WebControls.CheckBox chkIsOutside;
        protected System.Web.UI.WebControls.CheckBox chkEncryptionPreferred;

        protected System.Web.UI.WebControls.RequiredFieldValidator reqVideoEquipment;//Code added FB 1475
        protected System.Web.UI.WebControls.TextBox txtExchangeID; //Cisco Telepresence fix
        protected System.Web.UI.WebControls.TextBox txtapiportno; //Api Port...
        protected System.Web.UI.WebControls.TextBox txtconfcode; //FB 2365
        protected System.Web.UI.WebControls.TextBox txtLeaderpin; //FB 2365
        #endregion 

        /* Some classes used to call common functions */
        myVRMNet.NETFunctions obj; //this object will be used to call the functions to call middle layer functions
        ns_Logger.Logger log; // this object will be used to write error logs in schema folders
        StringBuilder RTcinXML = new StringBuilder(); //FB 2249
        private String queryStrtp = "ManageConference.aspx?t="; //FB 2530
        public AddTerminalEndpoint()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            // This will Initialize all components/controls on form
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        #region Page Load
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("addterminalendpoint.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                
                if (!IsPostBack) //this piece of code will be executed when the page is loaded the first time
                {
                    obj.BindBridges(lstBridges); // get all the available bridges from the database
                    obj.BindVideoEquipment(lstVideoEquipment); // get all static values from database for video protocol
                    obj.BindLineRate(lstLineRate); // get all static values from database for Line rate
                    obj.BindAddressType(lstAddressType); // get all static values from database for address types
                    //obj.BindMediaTypes(lstConnection);  get all static values from database for connection type Commented for FB 1475
                    obj.BindVideoProtocols(lstProtocol); //get all static values from database for video protocols
                    obj.BindDialingOptions(lstConnectionType); //get all static values from database for video protocols
                    lblTerminalType.Text = obj.GetTranslatedText("Guest"); // if an endpoint has been added it will be added as a guest.//FB 1830 - Translation
                    txtEndpointID.Text = Session["EndpointID"].ToString();
                    if (!Session["EndpointID"].ToString().ToLower().Trim().Equals("new"))
                        if (!Request.QueryString["t"].ToLower().Equals("tc"))
                            BindData(); //Bind data for this user if any existing endpoint is been used
                    if (Request.QueryString["t"].ToLower().Equals("tc") && !Request.QueryString["epid"].ToString().ToLower().Equals("new"))
                        LoadProfileInformation(); // get the selected profile information for the endpoint
                }
                //FB 2530 Starts
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].Trim().Equals("cc"))
                        queryStrtp = "Dashboard.aspx?";
                }
                //FB 2530 Ends
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        #region LoadProfileInformation
        protected void LoadProfileInformation()
        {
            try
            {
                //Code commented for FB Issue 1175 -- Start
                //tblEndpointName.Visible = false;
                //Code commented for FB Issue 1175 -- End
                //tblEndpointName.Visible = false; // FB 1640
                txtEndpointID.Text = Request.QueryString["epid"].ToString();
                String inXML = "";
                inXML += "<EndpointDetails>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "</EndpointDetails>";
                // the following line will get the endpoint details from database including all profiles
                String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                    txtEndpointName.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText;

                    String[] delimitedArr = { "++" };

                    if (txtEndpointName.Text.IndexOf("++") >= 0)
                    {
                        tblEndpointName.Visible = true;
                        txtEndpointLastName.Text = txtEndpointName.Text.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries)[1]; //FB 1640
                        txtEndpointName.Text = txtEndpointName.Text.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries)[0];
                    }

                    ////Code Added for FB Issue 1175
                    //if (txtEndpointName.Text.IndexOf(",") >= 0)
                    //{
                    //    txtEndpointLastName.Text = txtEndpointName.Text.Split(',')[0];
                    //    txtEndpointName.Text = txtEndpointName.Text.Split(',')[1];
                    //}
                    //Code Added for FB Issue 1175
                    foreach (XmlNode node in nodes)
                    { // only the selected profile's details will be captured and used 
                        if (node.SelectSingleNode("ProfileID").InnerText.Equals(Session["ProfileID"].ToString()))
                        {
                            lstVideoEquipment.ClearSelection();
                            try
                            {
                                lstVideoEquipment.Items.FindByValue(node.SelectSingleNode("VideoEquipment").InnerText.Trim()).Selected = true;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            lstConnectionType.ClearSelection();
                            try
                            {
                                lstConnectionType.Items.FindByValue(node.SelectSingleNode("ConnectionType").InnerText.Trim()).Selected = true;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            lstLineRate.ClearSelection();
                            try
                            {
                                lstLineRate.Items.FindByValue(node.SelectSingleNode("LineRate").InnerText.Trim()).Selected = true;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            lstAddressType.ClearSelection();
                            try
                            {
                                lstAddressType.Items.FindByValue(node.SelectSingleNode("AddressType").InnerText.Trim()).Selected = true;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            lstBridges.ClearSelection();
                            try
                            {
                                lstBridges.Items.FindByValue(node.SelectSingleNode("Bridge").InnerText.Trim()).Selected = true;
                                DisplayBridgeDetails(lstBridges, new EventArgs());
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }

                            txtAddress.Text = node.SelectSingleNode("Address").InnerText.Trim();

                            txtExchangeID.Text = node.SelectSingleNode("ExchangeID").InnerText.Trim(); //Cisco Telepresence fix

                            if (node.SelectSingleNode("IsOutside").InnerText.Trim().Equals("1"))
                                chkIsOutside.Checked = true;
                            else
                                chkIsOutside.Checked = false;
                            lstConnection.ClearSelection();
                            //lstConnection.Items.FindByValue("3").Selected = true; Code changed for FB 1475
                            lstConnection.Items.FindByValue("2").Selected = true;//Code changed for FB 1475 //Code changed for FB 1744
                            lstProtocol.ClearSelection();
                            lstProtocol.Items.FindByValue("1").Selected = true;
                            txtapiportno.Text = node.SelectSingleNode("ApiPortno").InnerText.Trim();//API Port...
                        }
                    }
                    txtEndpointID.Text = "new";
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML); // error returned from business layer will be displayed here
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message); // writes to log the stacktrace and message of exception
            }
        }
        #endregion

        #region BindData
        protected void BindData()
        {
            try
            {
                String inXML = "";
                String outXML = "";
                tblEndpointName.Visible = false;
                txtEndpointName.Enabled = false;
                String tpe = Request.QueryString["tpe"];
                switch (tpe.ToUpper())
                {
                    case ns_MyVRMNet.vrmConfEndpointType.User:
                        lblTerminalType.Text = obj.GetTranslatedText("User");//FB 1830 - Translation
                        break;
                    case ns_MyVRMNet.vrmConfEndpointType.Room:
                        lblTerminalType.Text = obj.GetTranslatedText("Room");//FB 1830 - Translation
                        break;
                    case ns_MyVRMNet.vrmConfEndpointType.Cascade:
                        lblTerminalType.Text = obj.GetTranslatedText("Cascade");//FB 1830 - Translation
                        /* **Code added for FB 1475** */
                        tpe = "C";
                        lstVideoEquipment.Enabled = false;
                        reqVideoEquipment.Enabled = false;
                        /* **Code added for FB 1475** */
                        break;
                    default:
                        lblTerminalType.Text = obj.GetTranslatedText("Guest");//FB 1830 - Translation
                        tpe = ns_MyVRMNet.vrmConfEndpointType.User;
                        //Code changed for FB Issue 1175 -- Start
                        tblEndpointName.Visible = true;
                        //Code changed for FB Issue 1175 -- End
                        break;
                }
                //if (tpe.Equals(ns_MyVRMNet.vrmConfEndpointType.User) || tpe.Equals(ns_MyVRMNet.vrmConfEndpointType.Room))
                //{Code commented for FB 1475
                inXML += "<GetConferenceEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <ConfID>" + Session["confID"].ToString() + "</ConfID>";
                inXML += "  <EndpointID>" + Session["EndpointID"].ToString() + "</EndpointID>";
                inXML += "  <Type>" + tpe + "</Type>";
                inXML += "</GetConferenceEndpoint>";
                outXML = obj.CallMyVRMServer("GetConferenceEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    txtEndpointID.Text = Session["EndpointID"].ToString();
                    txtEndpointName.Text = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/EndpointName").InnerText; // FB 1640
                    //txtEndpointName.Text = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Name").InnerText;  // FB 1640

                    txtExchangeID.Text = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/ExchangeID").InnerText; //Cisco Telepresence fix
                    //Code Added for FB Issue 1175
                    String[] delimitedArr = { "++" };

                    if (txtEndpointName.Text.IndexOf("++") >= 0)
                    {
                        String[] endpointname = txtEndpointName.Text.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries); //FB 2023
                        tblEndpointName.Visible = true;
                        if (endpointname.Length > 1) //FB 2023
                            txtEndpointLastName.Text = endpointname[1]; // FB 1640

                        if (endpointname.Length > 0) //FB 2023                        
                            txtEndpointName.Text = endpointname[0];
                    }
                    if(xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/EndPointEmail") != null)
                        txtEndpointEmail.Text = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/EndPointEmail").InnerText;

                    if (txtEndpointLastName.Text.Trim() == "" && txtEndpointEmail.Text.Trim() == "") //FB 2023
                        tblEndpointName.Visible = false;

                    //Code Added for FB Issue 1175
                    lstAddressType.ClearSelection();
                    String txtTemp = "";
                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/AddressType").InnerText;
                    try
                    {
                        lstAddressType.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/BridgeID").InnerText;
                    try
                    {
                        lstBridges.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/VideoEquipment").InnerText;
                    try
                    {
                        lstVideoEquipment.ClearSelection();
                        lstVideoEquipment.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/connectionType").InnerText;
                    try
                    {
                        lstConnectionType.ClearSelection();
                        lstConnectionType.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Bandwidth").InnerText;
                    try
                    {
                        lstLineRate.ClearSelection();
                        lstLineRate.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/DefaultProtocol").InnerText;
                    try
                    {
                        lstProtocol.ClearSelection();
                        lstProtocol.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Connection").InnerText;
                    try
                    {
                        lstConnection.ClearSelection();
                        lstConnection.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //During API Port starts..
                    try
                    {
                        txtURL.Text = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/URL").InnerText.Trim();
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //During API Port ends..
                    //API Port starts..
                    try
                    {
                        txtapiportno.Text = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/ApiPortno").InnerText.Trim();
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //API Port Ends..
                    txtTemp = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/IsOutside").InnerText;
                    if (txtTemp.Equals(""))
                        txtTemp = "0";
                    if (txtTemp.Equals("1"))
                        chkIsOutside.Checked = true;
                    //FB 2365 start
                    string address = "";
                    address = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Address").InnerText;
                    if (address.Contains("D") && address.Contains("+"))
                    {
                        txtAddress.Text = address.Split('D')[0];//Address
                        if (address.IndexOf('+') > 0)
                        {
                            txtconfcode.Text = address.Split('D')[1].Split('+')[0]; //Conference Code
                            txtLeaderpin.Text = address.Split('D')[1].Split('+')[1]; // Leader Pin
                        }
                    }
                    else if(!address.Contains("D") && address.Contains("+"))
                    {
                        txtAddress.Text = address.Split('+')[0]; //Address
                        txtconfcode.Text = "";//ConferenceCode
                        txtLeaderpin.Text = address.Split('+')[1]; // Leader Pin
                    }
                    else if (address.Contains("D") && !address.Contains("+"))
                    {
                        txtAddress.Text = address.Split('D')[0];//Address
                        txtconfcode.Text = address.Split('D')[1]; //Conference Code
                        txtLeaderpin.Text = ""; // Leader Pin
                    }
                    else if (!address.Contains("D") && !address.Contains("+"))
                    {
                        txtAddress.Text = address;//Address
                        txtconfcode.Text = "";//ConferenceCode
                        txtLeaderpin.Text = ""; // Leader Pin
                    }
                    //FB 2365 end
                    if (lstBridges.SelectedIndex > 0)
                        DisplayBridgeDetails(lstBridges, new EventArgs());

                    if (xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/BridgeAddress").InnerText.Trim() != "" || xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/BridgeAddress").InnerText.Trim() != "0") //FB 2023
                        txtMCUServiceAddress.Text = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/BridgeAddress").InnerText;

                    lstMCUAddressType.ClearSelection();
                    try
                    {
                        lstMCUAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/BridgeAddressType").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                /* Code commented for FB1475
                 * }
                    else //cascade calls are handled by COM so we call com command to get the endpoint details
                    {
                        //Code added for FB Issue 1175 -- Start
                        tblEndpointName.Visible = false;
                        //Code added for FB Issue 1175 -- End
                    
                        inXML += "<login>";
                        inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                        inXML += "  <confID>" + Session["confID"].ToString() + "</confID>";
                        inXML += "  <endpointID>" + Session["EndpointID"].ToString() + "</endpointID>";
                        inXML += "  <terminalType>" + tpe + "</terminalType>";
                        inXML += "</login>";
                        outXML = obj.CallCOM("GetTerminal", inXML, Application["COM_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            txtEndpointID.Text = Session["EndpointID"].ToString();
                        
                            txtEndpointName.Text = xmldoc.SelectSingleNode("//terminal/endpoint/name").InnerText;
                            //Code Added for FB Issue 1175
                            if (txtEndpointName.Text.IndexOf(",") >= 0)
                            {
                                txtEndpointLastName.Text = txtEndpointName.Text.Split(',')[0];
                                txtEndpointName.Text = txtEndpointName.Text.Split(',')[1];
                            }
                            //Code Added for FB Issue 1175
                            String txtTemp = xmldoc.SelectSingleNode("//terminal/endpoint/addressType").InnerText;
                            if (txtTemp.Equals(""))
                                txtTemp = "1";
                            lstAddressType.ClearSelection();
                            lstAddressType.Items.FindByValue(txtTemp).Selected = true;
                            txtTemp = xmldoc.SelectSingleNode("//terminal/endpoint/bridge/bridgeID").InnerText;
                            if (txtTemp.Equals(""))
                                txtTemp = "0";
                            lstBridges.ClearSelection();
                            lstBridges.Items.FindByValue(txtTemp).Selected = true;
                            txtTemp = xmldoc.SelectSingleNode("//terminal/endpoint/video").InnerText;
                            if (txtTemp.Equals("") || txtTemp.Equals("0"))
                                txtTemp = "-1";
                            //Response.Write(txtTemp);
                            lstVideoEquipment.ClearSelection();
                            lstVideoEquipment.Items.FindByValue(txtTemp).Selected = true;
                            txtTemp = xmldoc.SelectSingleNode("//terminal/endpoint/connectionType").InnerText;
                            if (txtTemp.Equals("") || txtTemp.Equals("-1") || txtTemp.Equals("0"))
                                txtTemp = "1";
                            lstConnectionType.ClearSelection();
                            lstConnectionType.Items.FindByValue(txtTemp).Selected = true;
                            txtTemp = xmldoc.SelectSingleNode("//terminal/endpoint/bridge/lineRate").InnerText;
                            if (txtTemp.Equals(""))
                                txtTemp = "384";
                            lstLineRate.ClearSelection();
                            lstLineRate.Items.FindByValue(txtTemp).Selected = true;
                            txtTemp = xmldoc.SelectSingleNode("//terminal/endpoint/protocol").InnerText;
                            if (txtTemp.Equals("") || txtTemp.Equals("-1") || txtTemp.Equals("0"))
                                txtTemp = "1";
                            lstProtocol.ClearSelection();
                            lstProtocol.Items.FindByValue(txtTemp).Selected = true;
                            txtTemp = "1";
                            if (txtTemp.Equals(""))
                                txtTemp = "0";
                            lstConnection.ClearSelection();
                            lstConnection.Items.FindByValue(txtTemp).Selected = true;
                            txtTemp = xmldoc.SelectSingleNode("//terminal/endpoint/bridge/isOutside").InnerText;
                            if (txtTemp.Equals(""))
                                txtTemp = "0";
                            if (txtTemp.Equals("1"))
                                chkIsOutside.Checked = true;
                            if (lstBridges.SelectedIndex > 0)
                                DisplayBridgeDetails(lstBridges, new EventArgs());
                            txtAddress.Text = xmldoc.SelectSingleNode("//terminal/endpoint/address").InnerText;
                            lstMCUAddressType.ClearSelection();
                            txtMCUServiceAddress.Text = xmldoc.SelectSingleNode("//terminal/endpoint/bridge/address").InnerText;
                            //Response.Write(xmldoc.SelectSingleNode("//terminal/endpoint/address").InnerText);
                            lstMCUAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//terminal/endpoint/bridge/addressType").InnerText).Selected = true;
                            //Response.Write(xmldoc.SelectNodes("//terminal/endpoint/bridge/networkService/SortID").Count);
                            if (xmldoc.SelectNodes("//terminal/endpoint/bridge/networkService/SortID").Count > 0)
                            {
                                if (xmldoc.SelectSingleNode("//terminal/endpoint/bridge/networkService/serviceType").InnerText.Equals("1"))
                                    foreach (DataGridItem dgi in dgISDNServices.Items)
                                        if (dgi.Cells[0].Text.Equals(xmldoc.SelectSingleNode("//terminal/endpoint/bridge/networkService/SortID").InnerText))
                                            ((RadioButton)dgi.FindControl("rdISDN")).Checked = true;
                            }
                        }
                        else
                        {
                            errLabel.Visible = true;
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                        }
                    } Code commented for FB1475 */ 
            }
            catch (Exception ex)
            {
               //log.Trace(ex.StackTrace + " : " + ex.Message);ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }
        #endregion

        // if a bridge is associated with an endpoint profile then we get the details of that bridge and associated IP/ISDN services
        #region DisplayBridgeDetails
        protected void DisplayBridgeDetails(Object sender, EventArgs e)
        {
            try
            {
                if (lstBridges.SelectedIndex > 0)
                {
                    String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><bridgeID>" + lstBridges.SelectedValue + "</bridgeID></login>";//Organization Module Fixes
                    String outXML = obj.CallMyVRMServer("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    //Response.Write(obj.Transfer(outXML));
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        String bType = xmldoc.SelectSingleNode("//bridge/bridgeType").InnerText.Trim();
                        if (bType.Equals("1") || bType.Equals("3"))
                        {
                        }
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress") != null)
                            txtMCUServiceAddress.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress").InnerText;
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/portA") != null)
                            txtMCUServiceAddress.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/portA").InnerText;
                        XmlNodeList nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/IPServices/IPService");
                        LoadServices(nodes, dgIPServices, lblNoIPServices);
                        nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/ISDNServices/ISDNService");
                        LoadServices(nodes, dgISDNServices, lblNoISDNServices);
                        obj.BindAddressType(lstMCUAddressType);
                        lstMCUAddressType.ClearSelection();
                        lstMCUAddressType.Items.FindByValue("1").Selected = true;
                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
              //log.Trace(ex.StackTrace + " : " + ex.Message); ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DisplayBridgeDetails:" + ex.Message);//ZD 100263
            }
        }
        #endregion
       
        #region LoadServices
        /// <summary>
        ///this function is used to display the IP and ISDN services associated with the bridge
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="dgServices"></param>
        /// <param name="lblServices"></param>
        protected void LoadServices(XmlNodeList nodes, DataGrid dgServices, Label lblServices)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                /* **Code changed for FB 1475** */
                //DataTable dt = new DataTable();
                DataTable dt = null;

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    lblServices.Visible = false;
                    if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
                    if (dt.Columns.Contains("addressType"))
                        foreach (DataRow dr in dt.Rows)
                            dr["addressType"] = lstAddressType.Items.FindByValue(dr["addressType"].ToString()).Text;
                    if (dt.Columns.Contains("networkAccess"))
                        foreach (DataRow dr in dt.Rows)
                            switch (dr["networkAccess"].ToString())
                            {
                                case "1": dr["networkAccess"] = "Public"; break;
                                case "2": dr["networkAccess"] = "Private"; break;
                                case "3": dr["networkAccess"] = "Both"; break;
                                case "4": dr["networkAccess"] = "None"; break;
                            }
                    if (dt.Columns.Contains("usage"))
                        foreach (DataRow dr in dt.Rows)
                            switch (dr["usage"].ToString())
                            {
                                case "1": dr["usage"] = "Audio"; break;
                                case "2": dr["usage"] = "Video"; break;
                                case "3": dr["usage"] = "Both"; break;
                                case "4": dr["usage"] = "None"; break;
                            }

                    /* **Code changed for FB 1475** */
                    //dgServices.DataSource = dt;
                    //dgServices.DataBind();

                }

                /* **Code changed for FB 1475** */
                dgServices.DataSource = dt;
                dgServices.DataBind();
                if (dt == null)
                    lblServices.Visible = true;
                /* **Code changed for FB 1475** */

                
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;//ZD 100263
                log.Trace("LoadServices:" + ex.Message);//ZD 100263
            }
        }
        #endregion
        
        #region ChangeIPSettings
        protected void ChangeIPSettings(Object sender, EventArgs e)
        {
            try
            {
                RadioButton rdTemp = (RadioButton)sender;
                foreach (DataGridItem dgi in dgIPServices.Items)
                {
                    if (((RadioButton)dgi.FindControl("rdIP")).Equals(rdTemp))
                    {
                        rdTemp.Checked = true;
                        txtMCUServiceAddress.Text = dgi.Cells[3].Text;
                        lstMCUAddressType.Items.FindByText(dgi.Cells[2].Text);
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace("ChangeIPSettings:" + ex.Message);//ZD 100263
            }
        }
        #endregion
        
        #region ChangeISDNSettings
        protected void ChangeISDNSettings(Object sender, EventArgs e)
        {
            try
            {
                RadioButton rdTemp = (RadioButton)sender;
                foreach (DataGridItem dgi in dgISDNServices.Items)
                {
                    if (((RadioButton)dgi.FindControl("rdISDN")).Equals(rdTemp))
                    {
                        rdTemp.Checked = true;
                        txtMCUServiceAddress.Text = dgi.Cells[4].Text;
                        lstMCUAddressType.ClearSelection();
                        lstMCUAddressType.Items.FindByValue("4").Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ChangeISDNSettings:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion
        
        #region SubmitEndpoint
        protected void SubmitEndpoint(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                String outXML = "";//Response.Write(obj.Transfer(inXML));
                //Response.End();                
                String tpe = Request.QueryString["tpe"];
                switch (tpe.ToUpper())
                {
                    case "U":
                        lblTerminalType.Text = obj.GetTranslatedText("User");//FB 1830 - Translation
                        break;
                    case "R":
                        lblTerminalType.Text = obj.GetTranslatedText("Room");//FB 1830 - Translation
                        break;
                    case "4":
                        lblTerminalType.Text = obj.GetTranslatedText("Cascade");//FB 1830 - Translation
                        //tpe = "R";
                        break;
                    default:
                        lblTerminalType.Text = obj.GetTranslatedText("Guest");//FB 1830 - Translation
                        tpe = "U";
                        break;
                }

                //if (tpe.ToUpper().Equals("U") || tpe.ToUpper().Equals("R"))
                //{
                inXML = GenerateInxml(tpe);
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                outXML = obj.CallMyVRMServer("SetConferenceEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //}
                //else
                //{
                //    inXML = GenerateTerminalDetail(tpe);
                //    //Response.Write(obj.Transfer(inXML));
                //    outXML = obj.CallCOM("SetTerminalDetail", inXML, Application["COM_ConfigPath"].ToString());
                //    //Response.Write(obj.Transfer(outXML));
                //}
                if (outXML.IndexOf("<error>") < 0)
                {
                    //FB 2249 start
                    String RtcoutXML = obj.CallCOM2("ModifyTerminal", RTcinXML.ToString(), Application["RTC_ConfigPath"].ToString());
                    errLabel.Visible = true;
                    if (RtcoutXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(RtcoutXML);
                        errLabel.Visible = true;
                    }
                    else //FB 2249 end
                       // Response.Redirect("ManageConference.aspx?t=&m=1");
                        Response.Redirect(queryStrtp + "&m=1&ep=1"); //FB 2530 //FB 118
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace + " : " + ex.Message; ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SubmitEndpoint:" + ex.Message);//ZD 100263
            }
        }
        #endregion
        
        #region GenerateTerminalDetail
        protected String GenerateTerminalDetail(String tpe)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + Session["confID"].ToString() + "</confID>";
                if (tpe.ToUpper().Equals("U"))
                    tpe = "1";
                else if (tpe.ToUpper().Equals("R"))
                    tpe = "2";
                inXML += "  <terminalType>" + tpe + "</terminalType>";
                inXML += "  <endpoint>";
                inXML += "      <ID>" + txtEndpointID.Text + "</ID>";
                inXML += "      <name>" + txtEndpointName.Text + "</name>";
                if (txtEndpointID.Text.ToLower().Equals("new"))
                    inXML += "      <isNew>1</isNew>";
                else
                    inXML += "      <isNew>0</isNew>";

                inXML += "    <guest>";
                inXML += "      <firstName>" + txtEndpointName.Text + "</firstName>";
                inXML += "      <lastName>" + txtEndpointLastName.Text + "</lastName>";
                inXML += "      <email>" + txtEndpointEmail.Text + "</email>";
                inXML += "    </guest>";
                inXML += "    <interfaceType>1</interfaceType>";
                inXML += "    <connectionType>" + lstConnectionType.SelectedValue + "</connectionType>";
                inXML += "    <video>" + lstVideoEquipment.SelectedValue + "</video>";
                inXML += "    <addressType>" + lstAddressType.Text + "</addressType>";
                inXML += "    <address>" + txtAddress.Text + "</address>";
                if (chkIsOutside.Checked)
                    inXML += "          <isOutside>1</isOutside>";
                else
                    inXML += "          <isOutside>0</isOutside>";
                inXML += "    <lineRate>" + lstLineRate.SelectedValue + "</lineRate>";
                inXML += "    <bridge>";
                inXML += "        <bridgeID>" + lstBridges.SelectedValue + "</bridgeID>";
                inXML += "        <addressType>" + lstMCUAddressType.SelectedValue + "</addressType>";
                inXML += "        <address>" + txtMCUServiceAddress.Text + "</address>";
                inXML += "        <networkService>";
                inXML += "            <serviceType>1</serviceType>";
                inXML += "            <serviceID>1</serviceID>";
                inXML += "        </networkService>";
                inXML += "    </bridge>";
                inXML += "   </endpoint>";
                inXML += "</login>";
                return inXML;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GenerateTerminalDetail:" + ex.Message);//ZD 100263
                return "<error></error>";
            }
        }
        #endregion
        
        #region GenerateInxml
        protected String GenerateInxml(String tpe)
        {
            try
            {
                //FB 1640
                //if (txtEndpointName.Text.IndexOf(",") >= 0)
                //{
                //    txtEndpointLastName.Text = txtEndpointName.Text.Split(',')[1];
                //    txtEndpointName.Text = txtEndpointName.Text.Split(',')[0];
                //}
                int endpttype = 0; //FB 2249
                if (Request.QueryString["tpe"] != null)
                    if (Request.QueryString["tpe"].ToString().Trim() != "")
                        Int32.TryParse(Request.QueryString["tpe"].ToString(), out endpttype);

                String inXML = "";
                inXML += "<SetConferenceEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <ConfID>" + Session["ConfID"].ToString() + "</ConfID>";
                inXML += "  <Endpoint>";
                inXML += "      <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "      <EndpointName>" + txtEndpointName.Text + "</EndpointName>";
                inXML += "      <EndpointLastName></EndpointLastName>";//FB 2528
                inXML += "      <EndpointEmail>" + txtEndpointEmail.Text + "</EndpointEmail>";
                inXML += "      <ProfileID>1</ProfileID>";
                inXML += "      <Type>" + tpe + "</Type>";
                //API Port Starts...
                if(txtapiportno.Text != "")
                    inXML += "      <ApiPortno>" + txtapiportno.Text + "</ApiPortno>";
                else
                    inXML += "      <ApiPortno>" + "23" + "</ApiPortno>";
                //API Port Ends...
                if (chkEncryptionPreferred.Checked)
                    inXML += "          <EncryptionPreferred>1</EncryptionPreferred>";
                else
                    inXML += "          <EncryptionPreferred>0</EncryptionPreferred>";
                inXML += "          <AddressType>" + lstAddressType.Text + "</AddressType>";
                //FB 2365 Start
                if (txtconfcode.Text != "" && txtLeaderpin.Text != "")
                {
                    inXML += "<Address>" + txtAddress.Text + "D" + txtconfcode.Text + "+" + txtLeaderpin.Text + "</Address>";
                }
                else if (txtconfcode.Text != "" && txtLeaderpin.Text == "")
                {
                    inXML += "<Address>" + txtAddress.Text + "D" + txtconfcode.Text + "</Address>";
                }
                else if (txtconfcode.Text == "" && txtLeaderpin.Text != "")
                {
                    inXML += "<Address>" + txtAddress.Text + "+" + txtLeaderpin.Text + "</Address>";
                }
                else
                {
                    inXML += "<Address>" + txtAddress.Text + "</Address>";
                }                             
                //FB 2365 End
                       
                inXML += "          <URL>" + txtURL.Text + "</URL>";
                if (chkIsOutside.Checked)
                    inXML += "          <IsOutside>1</IsOutside>";
                else
                    inXML += "          <IsOutside>0</IsOutside>";
                inXML += "          <ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                inXML += "          <VideoEquipment>" + lstVideoEquipment.SelectedValue + "</VideoEquipment>";
                inXML += "          <ExchangeID>" + txtExchangeID.Text + "</ExchangeID>"; //Cisco Fix
                inXML += "          <LineRate>" + lstLineRate.SelectedValue + "</LineRate>";
                inXML += "          <Bridge>" + lstBridges.SelectedValue + "</Bridge>";
                inXML += "          <Connection>" + lstConnection.SelectedValue + "</Connection>";
                inXML += "          <Protocol>" + lstProtocol.SelectedValue + "</Protocol>";
                inXML += "          <BridgeAddress>" + txtMCUServiceAddress.Text + "</BridgeAddress>";
                inXML += "          <BridgeAddressType>" + lstMCUAddressType.SelectedValue + "</BridgeAddressType>";
                inXML += "  </Endpoint>";
                inXML += "</SetConferenceEndpoint>";

                RTcinXML.Append("<login>"); //FB 2249 start
                RTcinXML.Append(obj.OrgXMLElement());
                RTcinXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                RTcinXML.Append("  <confID>" + Session["ConfID"].ToString() + "</confID>");
                RTcinXML.Append("  <endpointID>" + txtEndpointID.Text + "</endpointID>");
                
                if (endpttype > 0 && endpttype < 4)
                    RTcinXML.Append("  <terminalType>" + endpttype.ToString() + "</terminalType>");
                else
                {
                    if (tpe.Trim() == "U")
                        RTcinXML.Append("  <terminalType>1</terminalType>");
                    else
                        RTcinXML.Append("  <terminalType>2</terminalType>");
                }

                RTcinXML.Append("</login>"); //FB 2249 end

                return inXML;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SubmitEndpoint:" + ex.Message);//ZD 100263
                return "<error></error>";
            }
        }
        #endregion

        #region CancelEndpoint
        protected void CancelEndpoint(Object sender, EventArgs e)
        {
            //FB  2530 Starts
            //Response.Redirect("ManageConference.aspx?t=");
            try
            {
                Response.Redirect(queryStrtp + "&ep=1"); //FB 118
            }
            catch (Exception ex)
            {
                log.Trace("CancelEndpoint" + ex.Message);
            }
            //FB 2530 Ends
        }
        #endregion
    }
}