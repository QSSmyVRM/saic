﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;


namespace ns_MyVRM
{
    public partial class AudioAddOnBridge : System.Web.UI.Page
    {
        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        String usrPassword = "bd123";

        #endregion

        #region ProtectDataMember

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndpointID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnUserID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdninitialTime;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblError;
        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.TextBox txtIntVideoMcuNo;
        protected System.Web.UI.WebControls.TextBox txtExtVideoMcuNo;
        protected System.Web.UI.WebControls.TextBox txtLeaderPin;
        protected System.Web.UI.WebControls.TextBox txtConfCode;
        protected System.Web.UI.WebControls.TextBox txtBridgName;
        protected System.Web.UI.WebControls.TextBox txtEmailtoNotify;
        protected System.Web.UI.WebControls.DropDownList lstBridgetimezone;
        protected System.Web.UI.WebControls.DropDownList lstIsOutsideNetwork;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstProtocol;
        protected System.Web.UI.WebControls.ListBox lstBridgeDepts;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUnumber;
        //FB 2655 - DTMF  
        protected System.Web.UI.WebControls.TextBox PreConfCode;
        protected System.Web.UI.WebControls.TextBox PreLeaderPin;
        protected System.Web.UI.WebControls.TextBox PostLeaderPin;
        protected System.Web.UI.WebControls.TextBox AudioDialInPrefix;
        protected System.Web.UI.WebControls.TextBox txtEmailAddress;
        protected System.Web.UI.WebControls.Button btnnewSubmi; //FB 2670
        protected System.Web.UI.WebControls.Button btnnewSubmit;
        protected System.Web.UI.WebControls.Button btnReset;//ZD 100263
        #endregion

        #region AudioAddOnBridge
        /// <summary>
        /// AudioAddOnBridge
        /// </summary>
        public AudioAddOnBridge()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("audioaddonbridge.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if (!IsPostBack)
                {
                    String selTZ = "-1";

                    if (Request.QueryString["t"] != null)
                    {
                        if (Request.QueryString["t"].Trim() != "")
                            hdnUserID.Value = Request.QueryString["t"].Trim();
                    }

                    lstBridgetimezone.ClearSelection();
                    obj.GetTimezones(lstBridgetimezone, ref selTZ);
                    lstBridgeDepts.ClearSelection();
                    obj.GetManageDepartment(lstBridgeDepts);
                    obj.BindVideoProtocols(lstProtocol);
                    obj.BindAddressType(lstAddressType);
                    GetUserProfile();

                    //FB 2588 - Start
                    if (Session["EnableZulu"] != null)
                    {
                        if (Session["EnableZulu"].ToString() == "1")
                        {
                            lstBridgetimezone.SelectedValue = "33";
                            lstBridgetimezone.Enabled = false;
                        }
                        else
                            lstBridgetimezone.Enabled = true;
                    }
                    //FB 2588 End
                }

                if (Application["Client"].ToString().ToUpper() == "DISNEY")
                    trMCUnumber.Visible = false; //FB 2384
                else
                    trMCUnumber.Visible = true; //FB 2384

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    //ZD 100263
                    //btnnewSubmi.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnnewSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    btnnewSubmi.Visible = false;
                    //btnnewSubmi.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    

                    btnReset.Visible = false;
                    btnnewSubmit.Visible = false;
                    //btnnewSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    

                    lblHeader.Text = obj.GetTranslatedText("View Audio Bridge Details");
                }
                // FB 2796 Start
                else
                {
                    btnnewSubmi.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    btnnewSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                }
                // FB 2796 End

            }
            catch (Exception ex)
            {
                lblError.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);
                //lblError.Text = obj.ShowSystemMessage();ZD 100263
            }

        }
        #endregion

        #region SubmitAudioBridge
        /// <summary>
        /// SubmitAudioBridge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitAudioBridge(Object sender, EventArgs e)
        {
            try
            {
                String outXML = "";
                if (Page.IsValid)
                {
                    if (!ValidateAddress())
                        return;

                    StringBuilder inXML = new StringBuilder();
                    txtConfCode.Text = txtConfCode.Text.Replace("D", "").Replace("+", "");
                    txtLeaderPin.Text = txtLeaderPin.Text.Replace("D", "").Replace("+", "");

                    if (hdnUserID.Value.Trim() == "")
                        hdnUserID.Value = "new";

                    if (hdninitialTime.Value.Trim() == "")
                        hdninitialTime.Value = "99999";

                    inXML.Append("<saveUser>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<login>");
                    inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                    inXML.Append("</login>");
                    inXML.Append("<user>");
                    inXML.Append("<userID>" + hdnUserID.Value + "</userID>");
                    inXML.Append("<userName>");
                    inXML.Append("<firstName>" + txtBridgName.Text.Trim() + "</firstName>");
                    inXML.Append("<lastName></lastName>");
                    inXML.Append("</userName>");
                    inXML.Append("<ExchangeID>" + hdnEndpointID.Value + "</ExchangeID>"); //Cisco Telepresence fix
                    inXML.Append("<login>" + txtBridgName.Text.Trim() + "</login>");
                    inXML.Append("<password>" + usrPassword + "</password>");
                    //FB 2655 Start
                    if(txtEmailAddress.Text == "")
                        inXML.Append("<userEmail>" + txtBridgName.Text.Trim() +"_"+ txtAddress.Text + txtConfCode.Text + txtLeaderPin.Text + "@myvrm.vrm" + "</userEmail>");//FB 2023 commented for FB 2655
                    else
                        inXML.Append("<userEmail>" + txtEmailAddress.Text + "</userEmail>");
                    //FB 2655 End
                    inXML.Append("<alternativeEmail></alternativeEmail>");
                    inXML.Append("<workPhone></workPhone>");
                    inXML.Append("<cellPhone></cellPhone>");
                    inXML.Append("<sendBoth>0</sendBoth>");
                    inXML.Append("<emailClient>-1</emailClient>"); //None Adressbook
                    inXML.Append("<SavedSearch>-1</SavedSearch>"); //FB 2617 (Default searchid -1)
                    inXML.Append("<userInterface>2</userInterface>");
                    inXML.Append("<timeZone>" + lstBridgetimezone.SelectedValue + "</timeZone>");
                    inXML.Append("<location>0</location>");
                    inXML.Append("<dateFormat>MM/DD/yyyy</dateFormat>");
                    inXML.Append("<enableAV>0</enableAV>");
                    inXML.Append("<enableParticipants>0</enableParticipants>");
                    inXML.Append("<timeFormat>1</timeFormat>");
                    inXML.Append("<timezoneDisplay>0</timezoneDisplay>");
                    inXML.Append("<tickerStatus>1</tickerStatus>");
                    inXML.Append("<tickerPosition>0</tickerPosition>");
                    inXML.Append("<tickerSpeed>3</tickerSpeed>");
                    inXML.Append("<tickerBackground>#99ff99</tickerBackground>");
                    inXML.Append("<tickerDisplay>0</tickerDisplay>");
                    inXML.Append("<rssFeedLink></rssFeedLink>");
                    inXML.Append("<tickerStatus1>1</tickerStatus1>");
                    inXML.Append("<tickerPosition1>0</tickerPosition1>");
                    inXML.Append("<tickerSpeed1>3</tickerSpeed1>");
                    inXML.Append("<tickerBackground1>#99ff99</tickerBackground1>");
                    inXML.Append("<tickerDisplay1>0</tickerDisplay1>");
                    inXML.Append("<rssFeedLink1></rssFeedLink1>");
                    inXML.Append("<lineRateID>384</lineRateID>");
                    inXML.Append("<videoProtocol>-1</videoProtocol>");
                    inXML.Append("<conferenceCode></conferenceCode>");
                    inXML.Append("<leaderPin></leaderPin>");
                    inXML.Append("<IPISDNAddress></IPISDNAddress>");
                    inXML.Append("<connectionType>-1</connectionType>");
                    inXML.Append("<videoEquipmentID>-1</videoEquipmentID>");
                    inXML.Append("<isOutside>" + lstIsOutsideNetwork.SelectedValue + "</isOutside>");
                    inXML.Append("<addressTypeID>-1</addressTypeID>");
                    inXML.Append("<group>-1</group>");
                    inXML.Append("<ccGroup></ccGroup>");
                    inXML.Append("<roleID>1</roleID>"); //Role for user
                    inXML.Append("<initialTime>" + hdninitialTime.Value.Trim() + "</initialTime>");
                    inXML.Append("<expiryDate>12/31/2012</expiryDate>");
                    inXML.Append("<bridgeID>-1</bridgeID>");
                    inXML.Append("<languageID>1</languageID>");
                    inXML.Append("<EmailLang></EmailLang>");
                    inXML.Append("<departments>");
                    for (int i = 0; i < lstBridgeDepts.Items.Count; i++)
                    {
                        if (lstBridgeDepts.Items[i].Selected.Equals(true))
                            inXML.Append("<id>" + lstBridgeDepts.Items[i].Value + "</id>");
                    }
                    inXML.Append("</departments>");
                    inXML.Append("<status>");
                    inXML.Append("<deleted>0</deleted>");
                    inXML.Append("<locked>0</locked>");
                    inXML.Append("</status>");
                    inXML.Append("<creditCard>0</creditCard>");
                    inXML.Append("<IntVideoNum>"+txtIntVideoMcuNo.Text+"</IntVideoNum>");
                    inXML.Append("<ExtVideoNum>" + txtExtVideoMcuNo.Text + "</ExtVideoNum>");
                    inXML.Append("</user>");
                    inXML.Append("<emailMask>1</emailMask>");
                    inXML.Append("<exchangeUser>0</exchangeUser>");
                    inXML.Append("<dominoUser>0</dominoUser>");
                    inXML.Append("<mobileUser>0</mobileUser>");
                    inXML.Append("<PIMNotification>0</PIMNotification>");
                    inXML.Append("<audioaddon>1</audioaddon>");
                    inXML.Append("<HelpRequsetorEmail></HelpRequsetorEmail>");
                    inXML.Append("<HelpRequsetorPhone></HelpRequsetorPhone>");
                    inXML.Append("<WorldAccNumber></WorldAccNumber>");

                    // FB 2655 - DTMF Start
                    inXML.Append("<DTMF>");
                    inXML.Append("<PreConfCode>" + PreConfCode.Text.Trim() + "</PreConfCode>");
                    inXML.Append("<PreLeaderPin>" + PreLeaderPin.Text.Trim() + "</PreLeaderPin>");
                    inXML.Append("<PostLeaderPin>" + PostLeaderPin.Text.Trim() + "</PostLeaderPin>");
                    inXML.Append("<AudioDialInPrefix>" + AudioDialInPrefix.Text.Trim() + "</AudioDialInPrefix>");
                    inXML.Append("</DTMF>");
                    //FB 2655 - DTMF End

                    inXML.Append("</saveUser>");

                    outXML = obj.CallMyVRMServer("SetUser", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        lblError.Text = obj.ShowErrorMessage(outXML);
                        lblError.Visible = true;
                    }
                    else
                    {
                        if (SetEndpointDetails(outXML))
                        {
                            hdnUserID.Value = "";
                            hdnEndpointID.Value = "";
                            Button btnCtrl = (System.Web.UI.WebControls.Button)sender;
                            if (btnCtrl.ID.Trim().Equals("btnnewSubmit"))
                                Response.Redirect("ManageAudioAddOnBridges.aspx"); //List
                            else
                            {
                                lblError.Text = obj.ShowSuccessMessage();
                                ResetPage(null, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = obj.ShowSystemMessage();
                log.Trace("SubmitAudioBridge:" + ex.Message);
                //lblError.Text = obj.ShowSystemMessage();ZD 100263
            }
        }
        #endregion

        #region SetEndpointDetails
        /// <summary>
        /// SetEndpointDetails
        /// </summary>
        /// <param name="outXML"></param>
        /// <returns></returns>
        protected bool SetEndpointDetails(String outXML)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                hdnUserID.Value = xmldoc.SelectSingleNode("//SetUser/userID").InnerText;

                if (hdnEndpointID.Value.Trim().Equals(""))
                    hdnEndpointID.Value = "new";

                if (txtEndpointName.Text.Trim().Equals(""))
                    txtEndpointName.Text = txtBridgName.Text;

                inXML.Append("<SetEndpoint>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<EndpointID>" + hdnEndpointID.Value + "</EndpointID>");
                inXML.Append("<EndpointName>" + txtEndpointName.Text + "</EndpointName>");
                inXML.Append("<EntityType>U</EntityType>");
                inXML.Append("<UserID>" + hdnUserID.Value + "</UserID>");
                inXML.Append("<Profiles>");
                inXML.Append("  <Profile>");
                inXML.Append("    <ProfileID>0</ProfileID>");
                inXML.Append("    <ProfileName>" + txtEndpointName.Text + "</ProfileName>");
                inXML.Append("    <Default>0</Default>");
                inXML.Append("    <EncryptionPreferred>1</EncryptionPreferred>");
                inXML.Append("    <AddressType>" + lstAddressType.SelectedValue + "</AddressType>");
                inXML.Append("    <Password></Password>");
                inXML.Append("    <Address>" + txtAddress.Text + "</Address>");
                inXML.Append("    <URL></URL>");
                inXML.Append("    <IsOutside>" + lstIsOutsideNetwork.SelectedValue + "</IsOutside>");
                inXML.Append("    <ConnectionType>-1</ConnectionType>");
                inXML.Append("    <VideoEquipment>-1</VideoEquipment>");
                inXML.Append("    <LineRate>384</LineRate>");
                inXML.Append("    <Bridge>-1</Bridge>");
                inXML.Append("    <MCUAddress></MCUAddress>");
                inXML.Append("    <ApiPortno>23</ApiPortno>");
                inXML.Append("    <DefaultProtocol>" + lstProtocol.SelectedValue + "</DefaultProtocol>");
                inXML.Append("    <MCUAddressType>-1</MCUAddressType>");
                inXML.Append("    <Deleted>0</Deleted>");
                inXML.Append("    <ExchangeID></ExchangeID>");
                inXML.Append("    <TelnetAPI>0</TelnetAPI>");
                inXML.Append("    <conferenceCode>" + txtConfCode.Text + "</conferenceCode>");
                inXML.Append("    <leaderPin>" + txtLeaderPin.Text + "</leaderPin>");
                inXML.Append("  </Profile>");
                inXML.Append("</Profiles>");
                inXML.Append("</SetEndpoint>");

                outXML = obj.CallMyVRMServer("SetEndpoint", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                    lblError.Visible = true;
                    return false;
                }
            }
            catch (Exception ex)
            {
               log.Trace("SetEndpointDetails" + ex.StackTrace);
               return false;
            }
            return true;
        }
        #endregion

        #region GetUserProfile
        /// <summary>
        /// GetUserProfile
        /// </summary>
        protected void GetUserProfile()
        {
            try
            {
                if (hdnUserID.Value.Trim() == "")
                    return;

                String inXML = "", outXML = "";
                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID>" + hdnUserID.Value.Trim() + "</userID></user></login>";
                outXML = obj.CallMyVRMServer("GetOldUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    
                    lblHeader.Text = obj.GetTranslatedText("Edit Audio Bridge");

                    if (xmldoc.SelectSingleNode("//oldUser/userName/firstName") != null)
                        txtBridgName.Text = xmldoc.SelectSingleNode("//oldUser/userName/firstName").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/IntVideoNum") != null)
                        txtIntVideoMcuNo.Text = xmldoc.SelectSingleNode("//oldUser/IntVideoNum").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/ExtVideoNum") != null)
                        txtExtVideoMcuNo.Text = xmldoc.SelectSingleNode("//oldUser/ExtVideoNum").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/timeZone") != null)
                        if (xmldoc.SelectSingleNode("//oldUser/timeZone").InnerText != "")
                            lstBridgetimezone.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/timeZone").InnerText).Selected = true;

                    lstBridgeDepts.ClearSelection();
                    XmlNodeList nodes = xmldoc.SelectNodes("//oldUser/departments/selected");
                    for (int i = 0; i < lstBridgeDepts.Items.Count; i++)
                    {
                        for (int j = 0; j < nodes.Count; j++)
                        {
                            if (lstBridgeDepts.Items[i].Value.Trim().Equals(nodes[j].InnerText.Trim()))
                                lstBridgeDepts.Items[i].Selected = true;
                        }
                    }

                    if (xmldoc.SelectSingleNode("//oldUser/initialTime") != null)
                       hdninitialTime.Value = xmldoc.SelectSingleNode("//oldUser/initialTime").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/EndpointID") != null)
                        hdnEndpointID.Value = xmldoc.SelectSingleNode("//oldUser/EndpointID").InnerText;

                    //FB 2655 - DTMF Start 
                    if (xmldoc.SelectSingleNode("//oldUser/userEmail") != null)
                        txtEmailAddress.Text = xmldoc.SelectSingleNode("//oldUser/userEmail").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/DTMF/PreConfCode") != null)
                        PreConfCode.Text = xmldoc.SelectSingleNode("//oldUser/DTMF/PreConfCode").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/DTMF/PreLeaderPin") != null)
                        PreLeaderPin.Text = xmldoc.SelectSingleNode("//oldUser/DTMF/PreLeaderPin").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/DTMF/PostLeaderPin") != null)
                        PostLeaderPin.Text = xmldoc.SelectSingleNode("//oldUser/DTMF/PostLeaderPin").InnerText;

                    if (xmldoc.SelectSingleNode("//oldUser/DTMF/AudioDialInPrefix") != null)
                        AudioDialInPrefix.Text = xmldoc.SelectSingleNode("//oldUser/DTMF/AudioDialInPrefix").InnerText;
                    //FB 2655 - DTMF End 

                    if (!hdnEndpointID.Value.Trim().Equals("0") && !hdnEndpointID.Value.Trim().Equals("-1"))
                        LoadEndpointDetails();
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                //lblError.Text = obj.ShowSystemMessage();ZD 100263
                //log.Trace("GetUserProfile: "+ex.StackTrace);ZD 100263
                lblError.Text = obj.ShowSystemMessage();
                log.Trace("GetUserProfile:" + ex.Message);//ZD 100263
            }
        }
        #endregion

        #region LoadEndpointDetails
        /// <summary>
        /// LoadEndpointDetails
        /// </summary>
        private void LoadEndpointDetails()
        {
            try
            {
                String inXML = "<EndpointDetails>"
                             + obj.OrgXMLElement()
                             + "  <UserID>" + hdnUserID.Value.Trim() + "</UserID>"
                             + "  <EndpointID>" + hdnEndpointID.Value.Trim() + "</EndpointID>"
                             + "  <EntityType>U</EntityType>"
                             + "</EndpointDetails>";

                String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    lstAddressType.ClearSelection();
                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/AddressType") != null)
                        if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/AddressType").InnerText.Trim() != "")
                            lstAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/AddressType").InnerText).Selected = true;

                    lstProtocol.ClearSelection();
                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/DefaultProtocol") != null)
                        if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/DefaultProtocol").InnerText.Trim() != "")
                            lstProtocol.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/DefaultProtocol").InnerText).Selected = true;

                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Address") != null)
                        txtAddress.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Address").InnerText.Trim();

                    txtEndpointName.Text = txtBridgName.Text;
                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ProfileName") != null)
                        if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ProfileName").InnerText.Trim() != "")
                            txtEndpointName.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ProfileName").InnerText;


                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode") != null)
                        txtConfCode.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode").InnerText;

                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin") != null)
                        txtLeaderPin.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin").InnerText;

                    lstIsOutsideNetwork.ClearSelection();
                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/IsOutside") != null)
                        if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/IsOutside").InnerText.Trim() != "")
                            lstIsOutsideNetwork.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/IsOutside").InnerText).Selected = true;
                }
             }
            catch (Exception ex)
            {
                lblError.Visible = true;
                //lblError.Text = obj.ShowSystemMessage();ZD 100263
                //log.Trace("GetUserProfile: "+ex.StackTrace); //ZD 100263
                lblError.Text = obj.ShowSystemMessage();
                log.Trace("LoadEndpointDetails:" + ex.Message);//ZD 100263
            }
        }
        #endregion

        #region GobackToParentPage
        /// <summary>
        /// GobackToParentPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GobackToParentPage(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageAudioAddOnBridges.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("GobackToParentPage" + ex.Message);
            }
        }
        #endregion

        #region ResetPage
        /// <summary>
        /// ResetPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetPage(object sender, EventArgs e)
        {
            try
            {
                if (hdnUserID.Value.Trim() == "" || hdnUserID.Value.Trim() == "new")
                {
                    txtAddress.Text = "";
                    txtBridgName.Text = "";
                    txtConfCode.Text = "";
                    txtLeaderPin.Text = "";
                    txtIntVideoMcuNo.Text = "";
                    txtExtVideoMcuNo.Text = "";
                    txtEndpointName.Text = "";
                    //FB 2655 Start
                    PreConfCode.Text = "";
                    PreLeaderPin.Text = "";
                    PostLeaderPin.Text = "";
                    AudioDialInPrefix.Text = "";
                    txtEmailAddress.Text = "";
                    //FB 2655 Start
                    lstAddressType.ClearSelection();
                    lstBridgeDepts.ClearSelection();
                    lstBridgetimezone.ClearSelection();
                    lstProtocol.ClearSelection();
                    lstIsOutsideNetwork.ClearSelection();
                    if (sender != null)
                        lblError.Text = "";
                }
                else
                    GetUserProfile();
            }
            catch (Exception ex)
            {
                log.Trace("GobackToParentPage" + ex.Message);
            }
        }
        #endregion

        #region ValidateAddress
       /// <summary>
        /// ValidateAddress
       /// </summary>
       /// <returns></returns>
        private bool ValidateAddress()
        {
			//FB 3012 Starts
            string patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
            Regex checkIP = new Regex(patternIP);
            String patternISDN = @"^[0-9]+$";
            Regex checkISDN = new Regex(patternISDN);
            bool valid=true;
            //bool ret = true;
            try
            {                
                switch(lstAddressType.SelectedValue)
                {
                    case ns_MyVRMNet.vrmAddressType.IP:
                        {
                            
                            if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP))
                            {
                                if (!checkIP.IsMatch(txtAddress.Text.Trim(), 0))
                                {
                                    lblError.Text = "<br />" + obj.GetTranslatedText("Invalid IP Address for user");//FB 2272
                                    valid = false;
                                }
                            }
                            else if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                            {
                                lblError.Text = obj.GetTranslatedText("Protocol does not match with selected Address Type.");
                                valid = false;
                            }
                            break;
                        }
                    case ns_MyVRMNet.vrmAddressType.ISDN:
                        {
                            
                            if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                            {
                                if (!checkISDN.IsMatch(txtAddress.Text.Trim(), 0))
                                {
                                    lblError.Text = "<br />" + obj.GetTranslatedText("Invalid ISDN Address for user");//FB 2272
                                    valid = false;
                                }
                            }
                            else
                            {
                                lblError.Text = obj.GetTranslatedText("Protocol does not match with selected Address Type.");
                                valid = false;
                            }
                            break;
                          
                        }

                        // Commemnted because Validation should be kept only for IP and ISDN -- FB 3012.

                    //case ns_MyVRMNet.vrmAddressType.MPI:
                    //    {
                    //        lblError.Text = "<br />" + obj.GetTranslatedText("MPI Connection must be established through the Endpoint profile. Please contact your VRM administrator.");//FB 2272
                    //        ret = false;
                    //    }

                        // Commemnted because Validation should be kept only for IP and ISDN -- FB 3012.                   
                }                
            }
            catch (Exception ex)
            {
                //log.Trace("ValidateAddress" + ex.StackTrace);ZD 100263
                //lblError.Text = obj.ShowSystemMessage();ZD 100263
                lblError.Text = obj.ShowSystemMessage();
                log.Trace("ValidateAddress:" + ex.Message);//ZD 100263
                return false;
            }
            return valid;
			//FB 3012 Ends
        }
        #endregion

    }
}
