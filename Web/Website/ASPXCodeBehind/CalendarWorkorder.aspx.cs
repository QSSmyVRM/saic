/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

/// <summary>
/// Summary description for Calendar
/// </summary>
/// 
namespace ns_CalendarWorkorder
{
    public partial class CalendarWorkorder : Page
    {
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        ns_InXML.InXML objInXML;

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblNoUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.TextBox txtAlpha;
        protected System.Web.UI.WebControls.CheckBoxList lstUsers;
        protected System.Web.UI.WebControls.Label tblNoUsers;
        protected System.Web.UI.WebControls.Button btnDaily;
        protected System.Web.UI.WebControls.Button btnWeekly;
        protected System.Web.UI.WebControls.Button btnMonthly;
        protected DayPilot.Web.Ui.DayPilotScheduler schDaypilot;
        protected System.Web.UI.HtmlControls.HtmlInputHidden startDate;//FB 2305
        protected System.Web.UI.HtmlControls.HtmlInputHidden endDate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden view;
        protected DateTime conf = DateTime.Now;
        protected DateTime systemstarttime;//FB 2087
        protected DateTime systemEndtime;
        protected double Endtime;

        protected String tformat = "hh:mm tt"; //FB 2992
        protected String format = "MM/dd/yyyy";
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            //ViewState["dtUsers"] = null;
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objInXML = new ns_InXML.InXML();
        }
        public CalendarWorkorder()
        {
            //ViewState.Add("dtUsers", null); //,new DataTable());
            //
            // TODO: Add constructor logic here
            //
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = "";
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());
                if (Request.QueryString["t"] != null)
                    confChkArg = Request.QueryString["t"].ToString();
                obj.AccessConformityCheck("calendarworkorder.aspx?t=" + confChkArg);
                // ZD 100263 Ends

                if (Session["timeFormat"].ToString() == "0")
                    schDaypilot.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
                //FB 2992
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                            
                errLabel.Text = "";
                
                if (!DateTime.TryParse(txtSelectedDate.Value, out conf)) //FB 2305
                    conf = DateTime.Today;

                if (!IsPostBack)
                {
                    txtSelectedDate.Value = DateTime.Now.ToShortDateString();
                    txtType.Text = Request.QueryString["t"].ToString();

                    SetCalendarProperties();
                    //obj.GetSystemDateTime(COM_ConfigPath);
                    GetUsers(new Object(), new CommandEventArgs("User", "a"));
                    btnDaily.Enabled = false;
                    btnDaily.Attributes.Add("Class", "btndisable");//FB 2664
                    btnMonthly.Enabled = true;
                    btnMonthly.Attributes.Add("Class", "altMedium0BlueButtonFormat");//FB 2664
                    btnWeekly.Enabled = true;
                    btnWeekly.Attributes.Add("Class", "altMedium0BlueButtonFormat");//FB 2664
                }
                else
                {
                    //Response.Write(Request.Params.Get("__EVENTTARGET").ToString());
                    ////////////Bug Fix Case #373 commented the if /////////////////
                    //if (Request.Params.Get("__EVENTTARGET").ToString().IndexOf("ChangeCalendarDate") >= 0)
                    ChangeCalendarDate(new object(), new EventArgs());
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void SetCalendarProperties()
        {
            try
            {
                if (schDaypilot.Resources.Count > 0)
                {
                    schDaypilot.Visible = true;
                    lblNoUsers.Visible = false;
                }
                else
                {
                    schDaypilot.Visible = false;
                    lblNoUsers.Visible = true;
                }

            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SetCalendarProperties:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        public string AddResource(String UserName, String UserID)
        {
            try
            {
                startDate.Value = txtSelectedDate.Value;
                endDate.Value = txtSelectedDate.Value;
                schDaypilot.StartDate = conf;
                if (view.Value == "2")
                {
                    schDaypilot.StartDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);
                    DateTime dt = new DateTime(conf.Year, conf.Month, conf.Day, 0, 0, 0);
                    while (dt.DayOfWeek.ToString() != "Monday")
                        dt = dt.AddDays(-1);
                    startDate.Value = dt.ToString("MM/dd/yyyy");
                    endDate.Value = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59).AddDays(6).ToString("MM/dd/yyyy");
                }
                else if (view.Value == "3")
                {
                    schDaypilot.StartDate = new DateTime(conf.Year, conf.Month, 1, 0, 0, 0);//FB 2305
                    schDaypilot.Days = conf.AddMonths(1).AddDays(-conf.Day).Day;
                    startDate.Value = new DateTime(conf.Year, conf.Month, 1, 0, 0, 0).ToString("MM/dd/yyyy");
                    endDate.Value = new DateTime(conf.Year, conf.Month, conf.AddMonths(1).AddDays(-conf.Day).Day, 23, 59, 59).ToString("MM/dd/yyyy");
                }

                String inXML = objInXML.SearchConferenceWorkOrders(UserID, "", "", "", startDate.Value, endDate.Value, "12:01 AM", "11:59 PM", "", txtType.Text, "", "1", "1","-1");
                log.Trace("\r\nInxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                return outXML;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;
                //errLabel.Visible = true;
                //return "<error><level>E</level><message>Error Retrieving Data</message></error>";
                log.Trace("AddResource: Error Retrieving Data");//FB 1881
                errLabel.Text  = obj.ShowSystemMessage();
                errLabel.Visible = true;
                return obj.ShowSystemMessage();
            }
        }

        protected void ChangeCalendarDate(Object sender, EventArgs e)
        {
            try
            {
                String[] userID;
                userID = new String[schDaypilot.Resources.Count];
                String[] userName;
                userName = new String[schDaypilot.Resources.Count];
                int resCount = 0;
                foreach (DayPilot.Web.Ui.Resource oldResource in schDaypilot.Resources)
                {
                    userID[resCount] = oldResource.Value;
                    userName[resCount] = oldResource.Name;
                    resCount++;
                    //Response.Write("<br>" + oldResource.Name + " : " + oldResource.Value);
                    //schDaypilot.Resources.Remove(oldResource);
                    //AddUser(oldResource.Name, oldResource.Value);
                }
                schDaypilot.Resources.Clear();
                for (int i = 0; i < resCount; i++)
                {
                    RemoveUser(userID[i]);
                    AddUser(userName[i], userID[i]);
                }
            }
            catch (Exception ex)
            {
                log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
            }
        }


        protected void GetUsers(Object sender, CommandEventArgs e)
        {
            try
            {

                txtAlpha.Text = e.CommandArgument.ToString();
                //FB 2027 Starts
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("  <sortBy>2</sortBy>");
                inXML.Append("  <alphabet>" + txtAlpha.Text + "</alphabet>");
                inXML.Append("  <pageNo>1</pageNo>");
                inXML.Append("</login>");
                log.Trace("\r\nGetUsers InXML: " + inXML);
                //String outXML = obj.CallCOM("GetManageUser", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetManageUser", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 Ends
                log.Trace("\rGetUsers outXML: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                lstUsers.Items.Clear();
                tblNoUsers.Visible = false;
                if (nodes.Count > 0)
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("status/deleted").InnerText.Equals("0") && node.SelectSingleNode("status/locked").InnerText.Equals("0"))
                        {
                            //Window Dressing
                          //  ListItem li = new ListItem("<font class='blackblodtext'>" + node.SelectSingleNode("lastName").InnerText + ", " + node.SelectSingleNode("firstName").InnerText + "</font>", node.SelectSingleNode("userID").InnerText);
                            ListItem li = new ListItem(node.SelectSingleNode("lastName").InnerText + ", " + node.SelectSingleNode("firstName").InnerText, node.SelectSingleNode("userID").InnerText);
                            li.Attributes.Add("title",  node.SelectSingleNode("userID").InnerText ); 
                            if (InDaypilot(li.Value))
                                li.Selected = true;
                            lstUsers.Items.Add(li);
                        }
                    }
                else
                    tblNoUsers.Visible = true;
            }
            catch (Exception ex)
            {
                log.Trace("\r" + ex.Message + " : " + ex.StackTrace);
            }
        }
        protected void GetUserWorkOrders(Object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem li in lstUsers.Items)
                {
                    if (li.Selected && !InDaypilot(li.Value))
                        AddUser(li.Text, li.Value);
                    if (!li.Selected && InDaypilot(li.Value))
                        RemoveUser(li.Value);
                }
            }
            catch (Exception ex)
            {
                log.Trace("\rGetUserWorkorders: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected bool InDaypilot(String userID)
        {
            try
            {
                foreach (DayPilot.Web.Ui.Resource oRes in schDaypilot.Resources)
                    if (oRes.Value.Equals(userID))
                        return true;
                return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }

        protected void AddUser(String userName, String userID)
        {
            try
            {
                log.Trace("\r\nIn AddUser\r\n");
                String outXML = AddResource(userName, userID);
                log.Trace("\rSearchConferenceWorkOrders OutXML: " + outXML);
                DayPilot.Web.Ui.Resource newResource = new DayPilot.Web.Ui.Resource(userName, userID);
                schDaypilot.Resources.Add(newResource);
                lblNoUsers.Visible = false;
                schDaypilot.Visible = true;
                BindXMLToResource(outXML, newResource);
            }
            catch (Exception ex)
            {
                log.Trace("\rAddUser: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void RemoveUser(String userID)
        {
            try
            {
                log.Trace("\r\nIn RemoveUser\r\n");
                foreach (DayPilot.Web.Ui.Resource oldResource in schDaypilot.Resources)
                    if (oldResource.Value.Equals(userID))
                    {
                        schDaypilot.Resources.Remove(oldResource);
                        break;
                    }

                DataTable dt = (DataTable)ViewState["dtUsers"];
                DataTable dtTemp = new DataTable();
                
                if (!dtTemp.Columns.Contains("user")) dtTemp.Columns.Add("user");
                if (!dtTemp.Columns.Contains("userID")) dtTemp.Columns.Add("userID");
                if (!dtTemp.Columns.Contains("start")) dtTemp.Columns.Add("start");
                if (!dtTemp.Columns.Contains("end")) dtTemp.Columns.Add("end");
                if (!dtTemp.Columns.Contains("Name")) dtTemp.Columns.Add("Name");
                if (!dtTemp.Columns.Contains("ID")) dtTemp.Columns.Add("ID");
                if (!dtTemp.Columns.Contains("ConfID")) dtTemp.Columns.Add("ConfID");
                if (!dtTemp.Columns.Contains("ToolText")) dtTemp.Columns.Add("ToolText"); //FB 2992

                //WO Bug Fix
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        if (dr["userID"].ToString().IndexOf("_" + userID + "_") < 0)
                            dtTemp.ImportRow(dr);
                    }
                }

                ViewState["dtUsers"] = dtTemp;
                schDaypilot.DataSource = ViewState["dtUsers"];
                schDaypilot.DataBind();
                if (schDaypilot.Resources.Count > 0)
                {
                    schDaypilot.Visible = true;
                    lblNoUsers.Visible = false;
                }
                else
                {
                    schDaypilot.Visible = false;
                    lblNoUsers.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("\rRemoveUser: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        //FB 2087 - Start
        protected void BeforeTimeHeaderRender(object sender, DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs e)
        {
            try
            {
                if (Session["SystemStartTime"] != null)
                    systemstarttime = DateTime.Parse(Session["SystemStartTime"].ToString());
                if (Session["SystemEndTime"] != null)
                    systemEndtime = DateTime.Parse(Session["SystemEndTime"].ToString());

                schDaypilot.BusinessBeginsHour = systemstarttime.Hour;
                schDaypilot.BusinessEndsHour = systemEndtime.Hour;

                if (systemEndtime.Minute.ToString() != "0")
                    schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;
                if (e.End.TimeOfDay.TotalHours == 0)
                    Endtime = 24;
                else
                    Endtime = e.End.TimeOfDay.TotalHours;

                if ((Endtime <= schDaypilot.BusinessBeginsHour) || (e.Start.TimeOfDay.TotalHours >= schDaypilot.BusinessEndsHour))
                {
                    e.Visible = false;
                }
                else
                {
                    e.Visible = true;
                }

                // FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("2"))
                {
                    string dt;
                    string[] date_arr;
                    DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs)e;

                    if (e.IsColGroup)
                    {
                        dt = DateEvent.InnerHTML;
                        date_arr = dt.Split(' ');
                        if (date_arr[1] == "AM")
                        {

                            if (date_arr[0].Length == 1)
                                date_arr[0] = "0" + date_arr[0];

                            if (date_arr[0] == "12")
                                date_arr[0] = "0000Z";
                            else
                                date_arr[0] = date_arr[0] + "00Z";

                        }
                        else if (date_arr[1] == "PM")
                        {
                            if (date_arr[0].Trim() == "12")
                                date_arr[0] = "1200Z";
                            else
                                date_arr[0] = (Int32.Parse(date_arr[0]) + 12).ToString() + "00Z";
                        }
                        DateEvent.InnerHTML = date_arr[0];
                        DateEvent.ToolTip = date_arr[0];

                    }
                }
                // FB 2588 Ends

            }
            catch (Exception ex)
            {
                log.Trace("\r\n" + ex.Message + " : " + ex.StackTrace);
            }
        }
        //FB 2087 - End
        protected void BindXMLToResource(String outXML, DayPilot.Web.Ui.Resource newResource)
        {
            try
            {
                log.Trace("\r\nIn BindXMLToResource\r\n");
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (!dt.Columns.Contains("user")) dt.Columns.Add("user");
                    if (!dt.Columns.Contains("userID")) dt.Columns.Add("userID");
                    if (!dt.Columns.Contains("start")) dt.Columns.Add("start");
                    if (!dt.Columns.Contains("end")) dt.Columns.Add("end");
                    if (!dt.Columns.Contains("Name")) dt.Columns.Add("Name");
                    if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
                    if (!dt.Columns.Contains("ConfID")) dt.Columns.Add("ConfID");
                    if (!dt.Columns.Contains("ToolText")) dt.Columns.Add("ToolText"); //FB 2992

                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["user"] = newResource.Value;
                        dr["userID"] = "_" + newResource.Value + "_" + dr["ID"].ToString();
                        DateTime end = DateTime.Parse(dr["CompletedByDate"].ToString() + " " + dr["CompletedByTime"].ToString());
                        DateTime start = DateTime.Parse(dr["StartByDate"].ToString() + " " + dr["StartByTime"].ToString()); //end.Subtract(new TimeSpan(2, 30, 0));
                        dr["start"] = start.ToString();//FB 2305
                        dr["end"] = end.ToString();

                        //FB 2992 - Start
                        StringBuilder toolText = new StringBuilder();
                        toolText.Append("<table cellspacing='0' cellpadding='0' border='0' width='310px' class='promptbox' bgColor = '#ccccff'><tr>");
                        toolText.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold;'>" 
                            + dr["Name"].ToString() + " "+ (start.ToString(format) + " " + start.ToString(tformat) + " - " + end.ToString(format)
                            + " " + end.ToString(tformat)) + "</style></td>");
                        toolText.Append("</tr></table>");
                        dr["ToolText"] = toolText.ToString();
                        //FB 2992 - End

                        log.Trace("\r" + dr["user"] + " | " + dr["start"] + " | " + dr["end"] + " | " + dr["Name"]);
                    }
                    if (ViewState["dtUsers"] == null)
                        ViewState.Add("dtUsers", dt);
                    else
                    {
                        DataTable dtTemp = (DataTable)ViewState["dtUsers"];
                        foreach (DataRow dr in dt.Rows)
                            dtTemp.ImportRow(dr);
                        ViewState["dtUsers"] = dtTemp;
                    }
                    schDaypilot.DataSource = ViewState["dtUsers"];
                    schDaypilot.DataBind();
                    if (schDaypilot.Resources.Count > 0)
                    {
                        schDaypilot.Visible = true;
                        lblNoUsers.Visible = false;
                    }
                    else
                    {
                        schDaypilot.Visible = false;
                        lblNoUsers.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("\r\n" + ex.Message + " : " + ex.StackTrace);
            }
        }
		//FB 2992
        #region BubbleRenderhandler
        protected void BubbleRenderhandler(object sender, DayPilot.Web.Ui.Events.Bubble.RenderEventArgs e)
        {
            try
            {
                DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs re = (DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs)e;
                re.InnerHTML = re.Tag["ToolText"];

            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BubbleRenderhandler:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion    

        protected void ChangeView(Object sender, CommandEventArgs e)
        {
            try
            {
                btnDaily.Enabled = true;
                btnDaily.Attributes.Add("Class", "altMedium0BlueButtonFormat");//FB 2664
                btnWeekly.Enabled = true;
                btnWeekly.Attributes.Add("Class", "altMedium0BlueButtonFormat");//FB 2664
                btnMonthly.Enabled = true;
                btnMonthly.Attributes.Add("Class", "altMedium0BlueButtonFormat");//FB 2664
                switch (e.CommandArgument.ToString())//FB 2305
                {
                    case "1":
                        schDaypilot.Days = 1;
                        schDaypilot.CellGroupBy = DayPilot.Web.Ui.Enums.GroupByEnum.Hour;
                        schDaypilot.CellDuration = 30;
                        schDaypilot.CellWidth = 20;
                        btnDaily.Enabled = false;
                        btnDaily.Attributes.Add("Class", "btndisable");//FB 2664
                        view.Value = "1";
                        break;
                    case "2":
                        schDaypilot.Days = 7;
                        schDaypilot.CellGroupBy = DayPilot.Web.Ui.Enums.GroupByEnum.Day;
                        schDaypilot.CellDuration = 1440;
                        schDaypilot.CellWidth = 100;
                        btnWeekly.Enabled = false;
                        btnWeekly.Attributes.Add("Class", "btndisable");//FB 2664
                        view.Value = "2";
                        break;
                    case "3":
                        schDaypilot.CellGroupBy = DayPilot.Web.Ui.Enums.GroupByEnum.Day;
                        schDaypilot.CellDuration = 1440;
                        schDaypilot.CellWidth = 100;
                        btnMonthly.Enabled = false;
                        btnMonthly.Attributes.Add("Class", "btndisable");//FB 2664
                        view.Value = "3";
                        break;
                }
                ChangeCalendarDate(new object(), new EventArgs());
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " | " + ex.Message);
            }
        }
    }
}