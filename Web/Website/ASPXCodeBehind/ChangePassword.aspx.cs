﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
//ZD 100263_T
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_ChangePassword : System.Web.UI.Page
{

    #region Protected Data members
    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.WebControls.TextBox TxtEmail;
    protected System.Web.UI.WebControls.TextBox txtPassword1;
    protected System.Web.UI.WebControls.TextBox txtPassword2;
    protected System.Web.UI.WebControls.Button BtnSubmit;
    protected System.Web.UI.HtmlControls.HtmlForm frmEmailLogin;
    #endregion

    #region Private Data Members

    private String inXML = "", RequestID = "", Emailaddress = "";
    private ns_Logger.Logger log = null;
    private myVRMNet.NETFunctions obj;


    public en_ChangePassword()
    {
        //
        // TODO: Add constructor logic here
        //
        obj = new myVRMNet.NETFunctions();
    }

    #endregion

    #region Page Load
    /// <summary>
    /// Page Load Event Handler 
    /// </summary>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("ChangePassword.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263_T

            this.IntializeUIResources();
            this.CheckUserSession();

            if (!IsPostBack)
            {
                Session["Emailaddress"] = "";
                TxtEmail.Text = "";
                txtPassword1.Text = "";
                txtPassword2.Text = "";

                if (Request.QueryString["t"] != null)
                    RequestID = Request.QueryString["t"];

                if (RequestID == "")
                    Response.Redirect("~/en/genlogin.aspx");

                GetoldData();
            }
        }
        catch (Exception ex)
        {
            log = new ns_Logger.Logger();
            LblError.Visible = true;
            //LblError.Text = "PageLoad: " + ex.Message;ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + " : " + ex.Message);
            log = null;
        }
    }
    #endregion

    #region IntializeUIResources
    /// <summary>
    /// Intialize all Event Handlers used in this Screen
    /// </summary>

    private void IntializeUIResources()
    {
        BtnSubmit.Click += new EventHandler(BtnSubmit_Click);
    }

    #endregion

    #region CheckUserSession
    // <summary>
    /// Checks the User Session and sets application path.
    /// </summary>

    private void CheckUserSession()
    {
        if (Session["userID"] == null)
        {
            Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
            Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
        }
    }

    #endregion

    #region Event Handlers Methods
    /// <summary>
    ///  Event Handlers Method - To Check email Id  with the myvrm account details
    /// </summary>

    private void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            BuildInXML();
            GetOutXML(inXML);
        }
        catch (System.Threading.ThreadAbortException)
        {

        }
        catch (Exception ex)
        {
            //LblError.Text = "Submitting: " + ex.Message;ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("Submitting:" + ex.Message);//ZD 100263
            LblError.Visible = true;
        }

    }

    #endregion

    #region User Defined Methods
    /// <summary>
    /// User Defined Methods
    /// </summary>

    #region BuildInXML
    /// <summary>
    /// User Defined Method - Building InPut XML,which sends to COM
    /// </summary>

    private void BuildInXML()
    {
        Emailaddress = Session["Emailaddress"].ToString();
        if (Emailaddress.Trim().ToUpper() == TxtEmail.Text.Trim().ToUpper())
        {
            inXML = "<login>";
            if (Session["organizationID"] == null) //Organization Module Fixes
                Session["organizationID"] = "11";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "<emailLoginInfo>";
            inXML += "<email>" + TxtEmail.Text.ToString() + "</email>";
            inXML += "<password>" + txtPassword1.Text.ToString() + "</password>";
            inXML += "</emailLoginInfo>";
            inXML += "</login>";
        }
        else
        {
            LblError.Text = "Invalid Credentials";
            LblError.Visible = true;
        }
    }

    #endregion

    #region GetOutXML
    /// <summary>
    /// User Defined Method - Retrieving OutPut XML  from COM
    /// </summary>

    private void GetOutXML(String inXML)
    {
        String cmd = "ChangePasswordRequest";   //FB 1830
        String outXML = obj.CallCommand(cmd, inXML);

        if (outXML.IndexOf("<error>") < 0)
        {
            Session["outXML"] = "";
            Response.Redirect("~/en/genlogin.aspx");
        }
        else
        {
            LblError.Text = "Invalid Credentials";
            LblError.Visible = true;
        }
    }
    #endregion

    #region GetoldData
    /// <summary>
    /// User Defined Method - Building InPut XML,which sends to COM
    /// </summary>

    private void GetoldData()
    {
        inXML = "<login>";
        if (Session["organizationID"] == null) //Organization Module Fixes
            Session["organizationID"] = "11";
        inXML += obj.OrgXMLElement();//Organization Module Fixes
        inXML += "<emailLoginInfo>";
        inXML += "<RequestID>" + RequestID + "</RequestID>";
        inXML += "</emailLoginInfo>";
        inXML += "</login>";

        String outXML = obj.CallCommand("GetRequestIDUser", inXML);

        if (outXML.IndexOf("<error>") < 0)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);

            if (xmldoc.SelectSingleNode("//emailLoginInfo/email") != null)
            {
                Emailaddress = xmldoc.SelectSingleNode("//emailLoginInfo/email").InnerText;
                Session["Emailaddress"] = Emailaddress;
            }
        }
        else
            Response.Redirect("~/en/genlogin.aspx");
    }

    #endregion

    #endregion
}
