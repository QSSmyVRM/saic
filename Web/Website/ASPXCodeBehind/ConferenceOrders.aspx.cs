/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Globalization;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_ConferenceOrders
{
    public partial class ConferenceOrders : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        ns_InXML.InXML objInXML;
        
        protected System.Web.UI.WebControls.Label lblType;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.Label lblCount;
        protected System.Web.UI.WebControls.Table InvMainTable;
        protected System.Web.UI.WebControls.Table tblNoWorkOrders;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.WebControls.DataGrid MainGrid;
        protected System.Web.UI.WebControls.DataGrid WorkOrderMainGrid;
        protected System.Web.UI.WebControls.DropDownList lstViews;
        protected System.Web.UI.WebControls.DropDownList lstStatus;
        protected System.Web.UI.WebControls.RadioButtonList rdRoomOption;
        protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;
        protected System.Web.UI.WebControls.TreeView treeRoomSelection;
        protected System.Web.UI.WebControls.MultiView MultiView1;
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.TextBox txtsWorkorderName;
        protected System.Web.UI.WebControls.TextBox txtWOStartDate;
        protected System.Web.UI.WebControls.TextBox txtWOEndDate;
        protected MetaBuilders.WebControls.ComboBox txtWOStartTime;
        protected MetaBuilders.WebControls.ComboBox txtWOEndTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;//Room Search
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;//Room Search
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        
        protected const String BySearch = "5";
        protected const String ByConference = "1";
        protected const String ByUser = "3";
        protected const String ByAll = "2";
        protected const String ByToday = "4";
        protected const String ByIncomplete = "6";//Code added fro FB 1114
        //Code added by Offshore for FB Issue 1073 -- Start
        protected String format = "MM/dd/yyyy";
        //Code added by Offshore for FB Issue 1073 -- End

        // WO Bug Fix
        String tformat = "hh:mm tt";
        protected string currencyFormat = "";//FB 1830
        //FB 1830
        CultureInfo cInfo = null;
        decimal tmpVal = 0;

        public ConferenceOrders()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objInXML = new ns_InXML.InXML();
            //
            // TODO: Add constructor logic here
            //
        }

        // FB 2050 Start
        #region Page PreInit Method
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
                Page.ClientTarget = "uplevel";
            //if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            //Request.Browser.Adapters.Clear();
        }
        #endregion
        // FB 2050 Ends
        
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = string.Empty;
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());
                if (Request.QueryString["t"] != null)
                    confChkArg = Request.QueryString["t"].ToString();
                obj.AccessConformityCheck("conferenceorders.aspx?t=" + confChkArg);
                // ZD 100263 Ends

                //FB 1830
                cInfo = new CultureInfo(Session["NumberFormat"].ToString());

                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }

                //WO Bug Fix - Start

                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                if (Session["timeFormat"].ToString().Equals("2")) //FB 2588
                    tformat = "HHmmZ";
                //FB 1830 - Starts
                if (Session["CurrencyFormat"] == null)
                    Session["CurrencyFormat"] = ns_MyVRMNet.vrmCurrencyFormat.dollar;

                currencyFormat = Session["CurrencyFormat"].ToString();
                //FB 1830 - End
                
                if (!IsPostBack)
                {
                    txtWOStartTime.Items.Clear();
                    txtWOEndTime.Items.Clear();
                    obj.BindTimeToListBox(txtWOStartTime, true, true);
                    obj.BindTimeToListBox(txtWOEndTime, true, true);

                    txtWOEndTime.Text = "05:00 PM";
                    txtWOStartTime.Text = "08:00 AM";
                }

                if (txtWOStartTime.Text != "")
                    txtWOStartTime.Text = Convert.ToDateTime(myVRMNet.NETFunctions.ChangeTimeFormat(txtWOStartTime.Text)).ToString(tformat);

                if(txtWOEndTime.Text != "")
                    txtWOEndTime.Text = Convert.ToDateTime(myVRMNet.NETFunctions.ChangeTimeFormat(txtWOEndTime.Text)).ToString(tformat); ;
                
                //WO Bug Fix - End
                
                //if (!IsPostBack)
                //{
                if (Request.QueryString["t"] != null)
                    txtType.Text = Request.QueryString["t"].ToString();
                lstViews.Attributes.Add("onchange", "javascript:ChangeView('" + txtType.Text + "',this.value)");
                for (int i = 0; i < rdRoomOption.Items.Count; i++)
                    rdRoomOption.Items[i].Attributes.Add("onclick", "javascript:changeRoomSelection('" + rdRoomOption.Items[i].Value + "');");
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                //Code added by Offshore for FB Issue 1073 -- End
                //if (Request.QueryString["view"] != null)
                //    Response.Write("View: " + Request.QueryString["view"].ToString());
                if (Request.QueryString["view"] != null)
                {
                    switch (Request.QueryString["view"].ToString())
                    {
                        case ByConference:
                            ViewByConference();
                            break;
                        case ByAll:
                            ViewAllWorkOrders();
                            break;
                        case ByUser:
                            ViewMyPendingWorkOrders();
                            break;
                        case ByToday:
                            ViewTodaysWorkOrders();
                            break;
                        case BySearch:
                            ViewSearchResults();
                            break;
                        case ByIncomplete:
                            ViewIncompleteWorkOrders();
                            break;
                    }
                }
                else
                    ViewAllWorkOrders();
                //}
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }
        }

        protected void GetLocations(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";//Organization Module Fixes
                //String outXML = obj.CallCOM("ManageConfRoom", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("ManageConfRoom", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(ManageConfRoom)
                //Response.Write(obj.Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//manageConfRoom/locationList/level3List/level3");
                String topNode = "You have no rooms available";
                if (nodes.Count > 0)
                {
                    topNode = "All Rooms";
                    GenerateLocationList(nodes, topNode);
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetLocations: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void GenerateLocationList(XmlNodeList nodes, String topNode)
        {
            try
            {
                TreeNode tnTop = new TreeNode(topNode, "0");
                tnTop.SelectAction = TreeNodeSelectAction.None;
                foreach (XmlNode node3 in nodes)
                {
                    //Response.Write("<br>" + node3.SelectSingleNode("level3Name").InnerText + " : " + node3.SelectNodes("level2List/level2/level1List/level1").Count);
                    if (node3.SelectNodes("level2List/level2/level1List/level1").Count > 0)
                    {
                        TreeNode tn3 = new TreeNode(node3.SelectSingleNode("level3Name").InnerText, node3.SelectSingleNode("level3ID").InnerText);
                        tnTop.ChildNodes.Add(tn3);
                        XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");
                        tn3.SelectAction = TreeNodeSelectAction.None;
                        foreach (XmlNode node2 in nodes2)
                        {
                            TreeNode tn2 = new TreeNode(node2.SelectSingleNode("level2Name").InnerText, node2.SelectSingleNode("level2ID").InnerText);
                            tn2.SelectAction = TreeNodeSelectAction.None;
                            if (node2.SelectNodes("level1List/level1").Count > 0)
                            {
                                tn3.ChildNodes.Add(tn2);
                                XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                foreach (XmlNode node1 in nodes1)
                                {
                                    if (Convert.ToInt32(node1.SelectSingleNode("deleted").InnerText) != 1)   //Fb 1443
                                    {
                                        TreeNode tn1 = new TreeNode(node1.SelectSingleNode("level1Name").InnerText, node1.SelectSingleNode("level1ID").InnerText);
                                        tn1.NavigateUrl = @"javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');";
                                        tn2.ChildNodes.Add(tn1);
                                        ListItem li = new ListItem(node1.SelectSingleNode("level1Name").InnerText, node1.SelectSingleNode("level1ID").InnerText);
                                        li.Attributes.Add("onclick", "javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');");
                                        lstRoomSelection.Items.Add(li);
                                    } //fb 1443
                                }
                            }
                        }
                    }
                }
                treeRoomSelection.ExpandAll();
                treeRoomSelection.ExpandDepth = 3;
                treeRoomSelection.Nodes.Add(tnTop);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GenerateLocationList:" + ex.Message);//ZD 100263
            }
        }

        private void ViewAllWorkOrders()
        {
            try
            {
                // FB 2570 Starts
                if (txtType.Text.Equals("1")) 
                    lblType.Text = obj.GetTranslatedText("Audiovisual Work Orders");//FB 1830 - Translation
                else if (txtType.Text.Equals("2"))
                    lblType.Text = obj.GetTranslatedText("Catering Work Orders");//FB 1830 - Translation
                else if (txtType.Text.Equals("3"))
                    lblType.Text = obj.GetTranslatedText("Facility Services Work Orders");//FB 1830 - Translation 
                // FB 2570 Ends
                String pageNo1 = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNo1 = Request.QueryString["pageNo"].ToString();
                String inXML = objInXML.SearchConferenceWorkOrders("", "", "", "", "", "", "12:01 AM", "11:59 PM", "", txtType.Text, "", "1", pageNo1,"-1");//FB 1114
                log.Trace("\r\nAll Workorders Inxml: " + inXML);
                String outXML;
                outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                //Response.Write("<br>" + obj.Transfer(inXML));
                //Response.Write("<hr>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                    if (nodes.Count > 0)
                    {
                        BindMyPendingData(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)WorkOrderMainGrid.Controls[0].Controls[WorkOrderMainGrid.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text += xmldoc.SelectSingleNode("//WorkOrderList/TotalRecords").InnerText;
                        int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/TotalPages").InnerText);
                        int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/PageNo").InnerText);
                        if (xmldoc.SelectSingleNode("//WorkOrderList/PageEnable").InnerText.Equals("1"))
                            if (totalPages > 1)
                            {
                                //  Request.QueryString.Remove("pageNo");
                                string qString = Request.QueryString.ToString();
                                if (Request.QueryString["pageNo"] != null)
                                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceOrders.aspx?" + qString);
                            }

                        tblNoWorkOrders.Visible = false;
                    }
                    else
                        tblNoWorkOrders.Visible = true;
                }
                lstViews.Items.FindByValue("2").Selected = true;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ViewAllWorkOrders:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        private void ViewMyPendingWorkOrders()
        {
            try
            {
                if (txtType.Text.Equals("1"))
                    lblType.Text = obj.GetTranslatedText("My Pending Audiovisual Work Orders");//FB 1830 - Translation FB 2570
                else if (txtType.Text.Equals("2"))
                    lblType.Text = obj.GetTranslatedText("My Pending Catering Work Orders");//FB 1830 - Translation
                else if (txtType.Text.Equals("3"))
                    lblType.Text = obj.GetTranslatedText("My Pending Facility Services Work Orders");//FB 1830 - Translation FB 2570

                String pageNo1 = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNo1 = Request.QueryString["pageNo"].ToString();
                String inXML = objInXML.SearchConferenceWorkOrders(Session["userID"].ToString(), "", "", "", "", "", "12:01 AM", "11:59 PM", "", txtType.Text, "", "1", pageNo1,"1");//FB 1114
                log.Trace("\r\nMy Pending Workorders Inxml: " + inXML);

                String outXML;
                outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                //Response.Write("<br>" + obj.Transfer(inXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                    if (nodes.Count > 0)
                    {
                        BindMyPendingData(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)WorkOrderMainGrid.Controls[0].Controls[WorkOrderMainGrid.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text += xmldoc.SelectSingleNode("//WorkOrderList/TotalRecords").InnerText;
                        int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/TotalPages").InnerText);
                        int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/PageNo").InnerText);
                        if (xmldoc.SelectSingleNode("//WorkOrderList/PageEnable").InnerText.Equals("1"))
                            if (totalPages > 1)
                            {
                                //  Request.QueryString.Remove("pageNo");
                                string qString = Request.QueryString.ToString();
                                if (Request.QueryString["pageNo"] != null)
                                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceOrders.aspx?" + qString);
                            }
                        tblNoWorkOrders.Visible = false;
                    }
                    else
                        tblNoWorkOrders.Visible = true;
                }
                lstViews.Items.FindByValue("3").Selected = true;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ViewMyPendingWorkOrders:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        private void ViewIncompleteWorkOrders()
        {
            try
            {
                if (txtType.Text.Equals("1"))
                    lblType.Text = obj.GetTranslatedText("My Incomplete Audiovisual Work Orders");//FB 1830 - Translation FB 2570
                else if (txtType.Text.Equals("2"))
                    lblType.Text = obj.GetTranslatedText("My Incomplete Catering Work Orders");//FB 1830 - Translation
                else if (txtType.Text.Equals("3"))
                    lblType.Text = obj.GetTranslatedText("My Incomplete Facility Services Work Orders");//FB 1830 - Translation FB 2570

                String pageNo1 = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNo1 = Request.QueryString["pageNo"].ToString();
                String inXML = objInXML.SearchConferenceWorkOrders(Session["userID"].ToString(), "", "", "", "", "", "12:01 AM", "11:59 PM", "", txtType.Text, "", "1", pageNo1, "2");//FB 1114
                log.Trace("\r\nMy Pending Workorders Inxml: " + inXML);

                String outXML;
                outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                //Response.Write("<br>" + obj.Transfer(inXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                    if (nodes.Count > 0)
                    {
                        BindMyPendingData(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)WorkOrderMainGrid.Controls[0].Controls[WorkOrderMainGrid.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text += xmldoc.SelectSingleNode("//WorkOrderList/TotalRecords").InnerText;
                        int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/TotalPages").InnerText);
                        int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/PageNo").InnerText);
                        if (xmldoc.SelectSingleNode("//WorkOrderList/PageEnable").InnerText.Equals("1"))
                            if (totalPages > 1)
                            {
                                //  Request.QueryString.Remove("pageNo");
                                string qString = Request.QueryString.ToString();
                                if (Request.QueryString["pageNo"] != null)
                                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceOrders.aspx?" + qString);
                            }
                        tblNoWorkOrders.Visible = false;
                    }
                    else
                        tblNoWorkOrders.Visible = true;
                }
                lstViews.Items.FindByValue("6").Selected = true;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ViewIncompleteWorkOrders:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        private void ViewTodaysWorkOrders()
        {
            try
            {
                if (txtType.Text.Equals("1"))
                    lblType.Text = obj.GetTranslatedText("Today's Audiovisual Work Orders");//FB 1830 - Translation FB 2570
                else if (txtType.Text.Equals("2"))
                    lblType.Text = obj.GetTranslatedText("Today's Catering Work Orders");//FB 1830 - Translation
                else if (txtType.Text.Equals("3"))
                    lblType.Text = obj.GetTranslatedText("Today's Facility Services Work Orders");//FB 1830 - Translation FB 2570

                String pageNo1 = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNo1 = Request.QueryString["pageNo"].ToString();
                String inXML = objInXML.SearchConferenceWorkOrders("", "", "", "", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), "12:01 AM", "11:59 PM", "", txtType.Text, "", "1", pageNo1,"-1"); //FB 1114
                log.Trace("\r\nTodays Workorders Inxml: " + inXML);

                String outXML;
                outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                //Response.Write("<br>" + obj.Transfer(inXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                    if (nodes.Count > 0)
                    {
                        BindMyPendingData(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)WorkOrderMainGrid.Controls[0].Controls[WorkOrderMainGrid.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text += xmldoc.SelectSingleNode("//WorkOrderList/TotalRecords").InnerText;
                        int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/TotalPages").InnerText);
                        int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/PageNo").InnerText);
                        if (xmldoc.SelectSingleNode("//WorkOrderList/PageEnable").InnerText.Equals("1"))
                            if (totalPages > 1)
                            {
                                //  Request.QueryString.Remove("pageNo");
                                string qString = Request.QueryString.ToString();
                                if (Request.QueryString["pageNo"] != null)
                                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceOrders.aspx?" + qString);
                            }
                        tblNoWorkOrders.Visible = false;
                    }
                    else
                        tblNoWorkOrders.Visible = true;
                }
                lstViews.Items.FindByValue("4").Selected = true;
            }
            catch (Exception ex)
            {
                //FB 1881 start
                //errLabel.Text = "Error in getting today's Work orders. Please contact your VRM Administrator.";
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ViewTodaysWorkOrders: Error in getting today's Work orders." + ex.Message);
                //errLabel.Text  = obj.ShowSystemMessage();ZD 100263
                errLabel.Visible = true;
                //log.Trace(ex.Message + " : " + ex.StackTrace);
                //FB 1881 end
            }
        }

        private void ViewSearchResults()
        {
            try
            {
                if (txtType.Text.Equals("1"))
                    lblType.Text = obj.GetTranslatedText("Search Result(s) for Audiovisual Work Orders");//FB 1830 - Translation FB 2570
                else if (txtType.Text.Equals("2"))
                    lblType.Text = obj.GetTranslatedText("Search Result(s) for Catering Work Orders");//FB 1830 - Translation
                else if (txtType.Text.Equals("3"))
                    lblType.Text = obj.GetTranslatedText("Search Result(s) for Facility Services Work Orders");//FB 1830 - Translation FB 2570

                String inXML = Session["inXML"].ToString();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inXML);
                log.Trace("\r\nSearchConferenceWorkOrders Inxml: " + inXML);
                String outXML;
                String pageNo1 = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNo1 = Request.QueryString["pageNo"].ToString();
                inXML = inXML.Replace("<pageNo>" + xmldoc.SelectSingleNode("//login/pageNo").InnerText + "</pageNo>", "<pageNo>" + pageNo1 + "</pageNo>");
                outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                log.Trace("\r\nSearch Workorders Outxml: " + outXML);
                //Response.Write("<br>" + obj.Transfer(inXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                    if (nodes.Count > 0)
                    {
                        BindMyPendingData(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)WorkOrderMainGrid.Controls[0].Controls[WorkOrderMainGrid.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text += xmldoc.SelectSingleNode("//WorkOrderList/TotalRecords").InnerText;
                        int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/TotalPages").InnerText);
                        int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//WorkOrderList/PageNo").InnerText);
                        if (xmldoc.SelectSingleNode("//WorkOrderList/PageEnable").InnerText.Equals("1"))
                            if (totalPages > 1)
                            {
                                //  Request.QueryString.Remove("pageNo");
                                string qString = Request.QueryString.ToString();
                                if (Request.QueryString["pageNo"] != null)
                                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceOrders.aspx?" + qString);
                            }
                        tblNoWorkOrders.Visible = false;
                    }
                    else
                        tblNoWorkOrders.Visible = true;
                }
                lstViews.Items.FindByValue("0").Selected = true;
            }
            catch (Exception ex)
            {
                //FB 1881 start
                //errLabel.Text = "Error in getting Search Result(s). Please contact your VRM Administrator.";
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ViewSearchResults: Error in getting Search Result(s)." + ex.Message);
                //errLabel.Text  = obj.ShowSystemMessage();ZD 100263
                errLabel.Visible = true;
                //log.Trace(ex.Message + " : " + ex.StackTrace);
                //FB 1881 end
            }
        }
        private void ViewByConference()
        {
            try
            {
                if (txtType.Text.Equals("1"))
                    lblType.Text = obj.GetTranslatedText("Audiovisual Work Orders");//FB 1830 - Translation FB 2570
                else if (txtType.Text.Equals("2"))
                    lblType.Text = obj.GetTranslatedText("Catering Work Orders");//FB 1830 - Translation
                else if (txtType.Text.Equals("3"))
                    lblType.Text = obj.GetTranslatedText("Facility Services Work Orders");//FB 1830 - Translation FB 2570
                InvMainTable.Visible = true;

                String inXML = objInXML.SearchConferenceWorkOrders(Session["userID"].ToString(), "", "", "", "", "", "12:01 AM", "11:59 PM", "", txtType.Text, "", "1", "1",""); //Code added for FB 1114
                log.Trace("\r\nMy Pending Workorders Inxml: " + inXML);

                String outXML;
                outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                Session.Add("outXML", outXML);
                BindData();
                

                /*String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID></login>";//Organization Module Fixes
                String outXML;
                outXML = obj.CallCOM("GetAccountPending", inXML, Application["COM_ConfigPath"].ToString());
                Session.Add("outXML", outXML);
                BindData();
                outXML = obj.CallCOM("GetAccountFuture", inXML, Application["COM_ConfigPath"].ToString());
                Session.Add("outXML", outXML);
                BindData();    */
                if (InvMainTable.Rows.Count <= 1)
                    DisplayBlank();
                lstViews.Items.FindByValue("1").Selected = true;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ViewByConference:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        private void DisplayBlank()
        {
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            tc.Text = obj.GetTranslatedText("You have no Pending or Future Conferences.");//FB 1830 - Translation
            tc.ColumnSpan = InvMainTable.Rows[0].Cells.Count;
            tc.HorizontalAlign = HorizontalAlign.Center;
            tr.Cells.Add(tc); tr.Height = 30; tr.BackColor = System.Drawing.Color.LightGray;
            InvMainTable.Rows.Add(tr);
        }
        private void BindData()
        {
            TableRow tr;
            TableCell tc;
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(Session["outxml"].ToString());
                Label temp = new Label();

                XmlNodeList nodes = xmldoc.SelectNodes("/WorkOrderList/Conference");

                int length = nodes.Count;
                int start, end;
                start = InvMainTable.Rows.Count - 1;
                end = start + length;
                for (int i = 0; i < length; i++)
                {
                    System.Drawing.Color bgcolor = System.Drawing.Color.LightGray;
                    string confID = nodes[i].SelectSingleNode("confID").InnerText;
                    tr = new TableRow();
                    tc = new TableCell();
                    tc.CssClass = "tableBody";
                    tr.ID = "Conf" + (start + i);
                    //tr.BackColor = bgcolor;
                    tr.CssClass = "tableHeader";
                    tc = new TableCell();
                    tc.Text = "<img src='image/loc/nolines_plus.gif' title='View accociated work orders' id='viewOrders" + i + "' onclick='javascript:listOrders(\"" + confID + "\",\"" + Request.QueryString["view"].ToString() + "\")'>";
                    if ((Request.QueryString["cmd"] != null) && (Request.QueryString["id"] != null))
                        if ((Request.QueryString["cmd"].Equals("2")) && (Request.QueryString["id"].ToString().Equals(confID)))
                            tc.Text = "<img src='image/loc/nolines_minus.gif' id='hideItem' onclick='javascript:hideOrders()'>&nbsp;";

                    tr.Cells.Add(tc);
                    tc = new TableCell();
                    tc.CssClass = "tableBody";
                    tc.Text = nodes[i].SelectSingleNode("confUniqueID").InnerText;
                    tr.Cells.Add(tc);
                    tc = new TableCell();
                    tc.CssClass = "tableBody";
                    tc.Text = nodes[i].SelectSingleNode("confName").InnerText;
                    tr.Cells.Add(tc);
                    tc = new TableCell();
                    tc.CssClass = "tableBody";
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //tc.Text = nodes[i].SelectSingleNode("confDate").InnerText + " " + nodes[i].SelectSingleNode("confTime").InnerText;
                    //tc.Text = myVRMNet.NETFunctions.GetFormattedDate(nodes[i].SelectSingleNode("confDate").InnerText) + " " + nodes[i].SelectSingleNode("confTime").InnerText;
                    
                    tc.Text = myVRMNet.NETFunctions.GetFormattedDate(nodes[i].SelectSingleNode("confDate").InnerText) + " " 
                        + myVRMNet.NETFunctions.GetFormattedTime(nodes[i].SelectSingleNode("confTime1").InnerText, Session["timeFormat"].ToString())
                        + " " + nodes[i].SelectSingleNode("timezone1").InnerText;  //WO Bug Fix
                    
                    //Code changed by Offshore for FB Issue 1073 -- End
                    tr.Cells.Add(tc);
                    //tc = new TableCell();
                    //int durmin = 10; // Convert.ToInt32(nodes[i].SelectSingleNode("durationMin").InnerText);
                    //tc.Text = durmin.ToString();
                    //tc.Text = (durmin / 60) + " (hrs) " + (durmin % 60) + " (mins)";
                    //tr.Cells.Add(tc);
                    tc = new TableCell();
                    tc.CssClass = "tableBody";
                    tc.Text = "<input type='button' onfocus='this.blur()' value='View' class='altShortBlueButtonFormat' onClick='JavaScript: viewconf(\"" + confID + "\")'>";
                    tr.Cells.Add(tc);
                    ImageButton btnEdit = new ImageButton();

                    tc = new TableCell();                    
                    tc.CssClass = "tableBody";
                    //WO Bug Fix - Start
                    if (Convert.ToDateTime(nodes[i].SelectSingleNode("confDate").InnerText + " " + nodes[i].SelectSingleNode("confTime1").InnerText) < DateTime.Now)
                        tc.Text = "";
                    else
                    {
                        btnEdit.ImageUrl = "image/edit.gif";
                        btnEdit.ID = "btnEdit" + (start + i);
                        btnEdit.OnClientClick = "EditConferenceOrder('" + confID + "','" + i + "')";
                        tc.Controls.Add(btnEdit);
                    }
                    //WO Bug Fix - End
                    tr.Cells.Add(tc);
                    tr.Height = 30;
                    InvMainTable.Rows.Add(tr);

                    if ((Request.QueryString["cmd"] != null) && (Request.QueryString["id"] != null))
                        if ((Request.QueryString["cmd"].Equals("2")) && (Request.QueryString["id"].ToString().Equals(confID)))
                            ViewDetails(confID);
                }
                lblCount.Text = "<span class=blackblodtext>" + obj.GetTranslatedText("Total Records:") +"</span>" + (InvMainTable.Rows.Count - 2);  //CSS Module //FB 1830 - Translation
            }

            catch (Exception ex)
            {
                errLabel.Visible = true;
               // errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }
        #endregion

        protected void ViewDetails(string confID)
        {
            TableRow tr;
            TableCell tc;

            tr = new TableRow();
            tc = new TableCell();
            try
            {
                String inXML = objInXML.SearchConferenceWorkOrders("", confID, "", "", "", "", "12:01 AM", "11:59 PM", "", txtType.Text, "", "1", "1","-1");//FB 1114
                log.Trace("\r\nView Details Workorders Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                //Response.Write(obj.Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("WorkOrderList/WorkOrder");
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                if (ds.Tables.Count == 0)
                {
                    dv = new DataView();
                }
                else
                {
                    dv = new DataView(ds.Tables[0]);
                }
                DataTable dt = dv.Table;

                if (dv.Table != null)
                {
                    if (dt.Columns.Contains("AssignedToName").Equals(false))
                    {
                        dt.Columns.Add("AssignedToName");
                    }
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //WO Bug Fix - Start
                        //if (dt.Rows[i]["Status"].Equals("0"))
                        //    dt.Rows[i]["Status"] = "Pending";
                        //else
                        //    dt.Rows[i]["Status"] = "Completed";
                        if (Convert.ToDateTime(dt.Rows[i]["CompletedByDate"].ToString() + " " + dt.Rows[i]["CompletedByTime"].ToString()) < DateTime.Now)
                            dt.Rows[i]["Status"] = "Completed";
                        else
                            dt.Rows[i]["Status"] = "Pending";

                        //WO Bug Fix - End
                        //Code added by Offshore for FB Issue 1073 -- Start

                        dt.Rows[i]["CompletedByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dt.Rows[i]["CompletedByDate"].ToString());
                        dt.Rows[i]["CompletedByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dt.Rows[i]["CompletedByTime"].ToString(), Session["timeFormat"].ToString());   // WO Bug Fix
                        
                        //Code added by Offshore for FB Issue 1073 -- End
                        //dt.Rows[i]["AssignedToName"] = obj.GetMyVRMUserName(dt.Rows[i]["AssignedToId"].ToString());//"Saima";
                    }
                    MainGrid.DataSource = dv;
                    MainGrid.DataBind();
                    tc = new TableCell(); tr = new TableRow();
                    tc.ColumnSpan = InvMainTable.Rows[0].Cells.Count;
                    //Window Dressing;
                    tc.CssClass = "tableBody";
                    tc.Controls.Add(MainGrid);
                    tr.ID = "detailsRow";
                    tr.Attributes.Add("style", "display:");
                    tr.Cells.Add(tc);
                    InvMainTable.Rows.Add(tr);
                }
                else
                {
                    //Response.Write("here in else");
                    //tblBlankWO.Visible = true;
                    tc = new TableCell(); tr = new TableRow();
                    tc.ColumnSpan = InvMainTable.Rows[0].Cells.Count;
                    //Window Dressing;
                    //tc.BackColor = System.Drawing.Color.LightBlue;
                    tc.CssClass = "lblError";
                    tc.HorizontalAlign = HorizontalAlign.Center;
                    tc.Text = obj.GetTranslatedText("There are no Workorders associated with this conference");//FB 1830 - Translation
                    tr.ID = "detailsRow";
                    tr.Attributes.Add("style", "display:");
                    tr.Cells.Add(tc);
                    InvMainTable.Rows.Add(tr);
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "ViewDetails: " + ex.StackTrace;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ViewDetails:" + ex.Message);//ZD 100263
            }
        }

        protected void BindMyPendingData(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (!dt.Columns.Contains("AssignedToName"))
                        dt.Columns.Add("AssignedToName");
                    foreach (DataRow dr in dt.Rows)
                    {
                        // WO Bug Fix - Start
                        //if (dr["Status"].Equals("0"))
                        //    dr["Status"] = "Pending";
                        //else
                        //    dr["Status"] = "Completed";

                        if (Convert.ToDateTime(dr["CompletedByDate"].ToString() + " " + dr["CompletedByTime"].ToString()) < DateTime.Now)
                            dr["Status"] = "Completed";
                        else
                            dr["Status"] = "Pending";
                        // WO Bug Fix - End
                        //FB 1830
                        decimal.TryParse(dr["TotalCost"].ToString(), out tmpVal);
                        dr["TotalCost"] = tmpVal.ToString("n", cInfo);
                        tmpVal = 0;
                        //dr["TotalCost"] = Double.Parse(dr["TotalCost"].ToString()).ToString("0.00");
                        //Code added by Offshore for FB Issue 1073 -- Start

                        dr["CompletedByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["CompletedByDate"].ToString());
                        dr["CompletedByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["CompletedByTime"].ToString(), Session["timeFormat"].ToString());  // WO Bug Fix

                        //Code added by Offshore for FB Issue 1073 -- End
                    }
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                }
                WorkOrderMainGrid.DataSource = dt;
                WorkOrderMainGrid.DataBind();
                WorkOrderMainGrid.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                //FB 1881 start
                //errLabel.Text = "Error in Getting Data"; // ex.StackTrace();
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindMyPendingData: Error in Getting Data" + ex.Message);
                //errLabel.Text  = obj.ShowSystemMessage();ZD 100263
                errLabel.Visible = true;
                //log.Trace(ex.Message + " : " + ex.StackTrace);
                //FB 1881 end
            }
        }
        protected void WorkOrderMainGrid_Delete(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //Response.Write("in delete");
                string inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <ConferenceID>" + e.Item.Cells[1].Text + "</ConferenceID>";
                inXML += "  <WorkorderID>" + e.Item.Cells[0].Text + "</WorkorderID>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                string outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    Response.Redirect("ConferenceOrders.aspx?m=1&t=" + txtType.Text + "&view=" + lstViews.SelectedValue, false);
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //FB 1881 start
                //errLabel.Text = "Error in Delete. Please contact your VRM Administrator.";
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("WorkOrderMainGrid_Delete:Error in Delete." + ex.Message);
                //errLabel.Text  = obj.ShowSystemMessage();ZD 100263
                //FB 1881 end
            }
        }
        protected void WorkOrderMainGrid_Edit(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //Response.Write("in edit");
                String confID = e.Item.Cells[1].Text;
                if (confID.Equals("11,1"))
                    confID = "PH";
                Response.Redirect("EditConferenceOrder.aspx?t=" + txtType.Text + "&id=" + confID + "&woid=" + e.Item.Cells[0].Text);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //FB 1881 start
                //errLabel.Text = "Error in Edit. Please contact your VRM Administrator.";
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("WorkOrderMainGrid_Edit: Error in Edit." + ex.Message);
                //errLabel.Text  = obj.ShowSystemMessage();ZD 100263
                //FB 1881 end
            }
        }

        protected void lstViews_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if (lstViews.SelectedIndex > 0)
                Response.Redirect("ConferenceOrders.aspx?t=" + txtType.Text + "&view=" + lstViews.SelectedValue, false);
        }
        protected void CancelSearch(Object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }
        protected void SearchWorkorders(Object sender, EventArgs e)
        {
            try
            {
                String selRooms = "";
                /* if (rdRoomOption.SelectedValue.Equals("2"))
                     foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                         if (tn.Depth.Equals(3))
                             selRooms += tn.Value + ",";*/
                if (selectedloc.Value != "")
                {
                    String[] selectLoc = selectedloc.Value.Split(',');
                    for (Int32 i = 0; i < selectLoc.Length; i++)
                    {
                        selRooms += selectLoc[i] + ",";
                    }
                }
                //Code changed by offshore for FB Issue 1073 -- start
                //String inXML = objInXML.SearchConferenceWorkOrders(hdnApprover1.Text, "", txtsWorkorderName.Text, selRooms, txtWOStartDate.Text, txtWOEndDate.Text, txtWOStartTime.Text, txtWOEndTime.Text, lstStatus.SelectedValue, txtType.Text, "", "1", "1", "3");
                String inXML = objInXML.SearchConferenceWorkOrders(hdnApprover1.Text, "", txtsWorkorderName.Text, selRooms, myVRMNet.NETFunctions.GetDefaultDate(txtWOStartDate.Text), myVRMNet.NETFunctions.GetDefaultDate(txtWOEndDate.Text), txtWOStartTime.Text, txtWOEndTime.Text, lstStatus.SelectedValue, txtType.Text, "", "1", "1", "-1");//FB 2526
                //Code changed by offshore for FB Issue 1073 -- end
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                if (Session["inXML"] == null)
                    Session.Add("inXML", inXML);
                else
                    Session["inXML"] = inXML;
                Response.Redirect("ConferenceOrders.aspx?t=" + txtType.Text + "&view=5", false);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SearchWorkorders:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void OpenWOCalendar(Object sender, EventArgs e)
        {
            Response.Redirect("CalendarWorkOrder.aspx?t=" + txtType.Text, false);
        }

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    //FB 2670
                    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                    btnDelete.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Workorder?") + "')) {DataLoading(1); return true;} else {DataLoading('0'); return false;}");//FB japnese

                    if (Session["admin"].ToString().Equals("3"))
                    {
                        LinkButton btnEdit = ((LinkButton)e.Item.FindControl("btnEditInventory"));
                        
                        //btnEdit.Attributes.Remove("onClick");
                        //btnEdit.Style.Add("cursor", "default");
                        //btnEdit.ForeColor = System.Drawing.Color.Gray;
                        btnEdit.Visible = false;
                        
                        //btnDelete.Attributes.Remove("onClick");
                        //btnDelete.Style.Add("cursor", "default");
                        //btnDelete.ForeColor = System.Drawing.Color.Gray;
                        btnDelete.Visible = false;
                        //ZD 100263
                    }
                }
                if (e.Item.ItemType.Equals(ListItemType.EditItem))
                {
                    DropDownList lstTempRooms = (DropDownList)e.Item.FindControl("lstRooms");
                }

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        protected void CreatePhantomWorkorder(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EditConferenceOrder.aspx?id=ph&t=" + txtType.Text, false);
            }
            catch (Exception ex)
            {
                log.Trace("CreatePhantomWorkorder: " + ex.StackTrace + " : " + ex.Message);
            }
        }
    }
}
