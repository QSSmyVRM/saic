/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;//FB 1830
using System.Threading;//FB 1830
using System.Text; //FB 2486
using System.Linq;
using System.Xml.Linq;
using System.Web.Services;
/// <summary>
/// Summary description for ConferenceSetup
/// </summary>
namespace ns_ConferenceSetup
{
    public partial class ConferenceSetup : System.Web.UI.Page
    {

        #region Protected Data Members 

        protected System.Web.UI.WebControls.Button btnAVSendReminder;
        protected System.Web.UI.WebControls.Button btnCATSendReminder;
        protected System.Web.UI.WebControls.Button btnHKSendReminder;
        protected System.Web.UI.WebControls.Button btnAddNewAV;
        protected System.Web.UI.WebControls.Button btnAddNewCAT;
        protected System.Web.UI.WebControls.Button btnAddNewHK;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnCATSubmit;
        protected System.Web.UI.WebControls.Button btnHKSubmit;
        protected System.Web.UI.WebControls.Button btnConfSubmit;
        
        protected System.Web.UI.WebControls.Button openCalendar;
        protected System.Web.UI.WebControls.Button btnLotus;
        protected System.Web.UI.WebControls.Button btnPrev;
        protected System.Web.UI.WebControls.Button btnNext;

        protected System.Web.UI.WebControls.Menu TopMenu;
        protected System.Web.UI.WebControls.Table AVItemsTable;
        protected System.Web.UI.WebControls.Table CATItemsTable;
        protected System.Web.UI.WebControls.Table HKItemsTable;
        protected System.Web.UI.WebControls.Table tblConflict;
        protected System.Web.UI.WebControls.Table tblBillingOptions;
        protected System.Web.UI.WebControls.Table tblEntityCode;
        
        protected System.Web.UI.WebControls.Table tblLHRICCustomAttributes;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblConfHeader;
        protected System.Web.UI.WebControls.Label lblConfID;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        protected System.Web.UI.WebControls.Label lblNewEditAV;
        protected System.Web.UI.WebControls.Label lblNewEditCAT;
        protected System.Web.UI.WebControls.Label lblNewEditHK;
        protected System.Web.UI.WebControls.Label lblAVWOInstructions;
        protected System.Web.UI.WebControls.Label lblCATWOInstructions;
        protected System.Web.UI.WebControls.Label lblHKWOInstructions;
        protected System.Web.UI.WebControls.Label plblConfName;
        protected System.Web.UI.WebControls.Label plblConfStartDateTime;
        protected System.Web.UI.WebControls.Label plblConfEndDateTime;
        protected System.Web.UI.WebControls.Label plblConfRecurrance;
        protected System.Web.UI.WebControls.Label plblPassword;
        protected System.Web.UI.WebControls.Label plblConfType;
        protected System.Web.UI.WebControls.Label plblConfDescription;
        protected System.Web.UI.WebControls.Label plblPartys;
        protected System.Web.UI.WebControls.Label plblLocation;
        protected System.Web.UI.WebControls.Label plblPublic;
        protected System.Web.UI.WebControls.Label plblSlash;//FB 2446
        protected System.Web.UI.WebControls.Label plblOpenForRegistration;
        protected System.Web.UI.WebControls.Label plblICAL;
        protected System.Web.UI.WebControls.Label plblAVInstructions;
        protected System.Web.UI.WebControls.Label lblNoUsers;
        protected System.Web.UI.WebControls.Label lblNoRooms;
        protected System.Web.UI.WebControls.Label plblCATInstructions;
        protected System.Web.UI.WebControls.Label plblHKInstructions;
        protected System.Web.UI.WebControls.Label plblGuestLocation;//FB 2426
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConceirgeSupp; //FB 2341//FB 2377
        protected System.Web.UI.HtmlControls.HtmlInputHidden confPassword;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtHasCalendar;
        protected System.Web.UI.WebControls.TextBox ConferencePassword;
        protected System.Web.UI.WebControls.TextBox ConferencePassword2;
        protected System.Web.UI.WebControls.TextBox confEndDate;
        protected System.Web.UI.WebControls.TextBox confStartDate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurFlag;
        protected System.Web.UI.WebControls.TextBox txtCA1;
        protected System.Web.UI.WebControls.TextBox txtCA2;
        protected System.Web.UI.WebControls.TextBox txtCA3;
        protected System.Web.UI.WebControls.TextBox txtCA4;
        protected System.Web.UI.WebControls.TextBox txtCA5;
        protected System.Web.UI.WebControls.TextBox txtCA6;
        protected System.Web.UI.WebControls.TextBox txtCA7;
        protected System.Web.UI.WebControls.TextBox txtCA8;
        protected System.Web.UI.WebControls.TextBox txtCA9;
        protected System.Web.UI.WebControls.TextBox txtCA10;
        protected System.Web.UI.WebControls.TextBox txtUsersStr;
        protected System.Web.UI.WebControls.TextBox txtPartysInfo; 
        protected System.Web.UI.WebControls.TextBox ConferenceName;
        protected System.Web.UI.WebControls.TextBox ConferenceDescription;
        //protected System.Web.UI.WebControls.TextBox Recur;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Recur;
        protected System.Web.UI.WebControls.TextBox txtWorkOrderName;
        protected System.Web.UI.WebControls.TextBox txtCATWorkOrderName;
        protected System.Web.UI.WebControls.TextBox txtCATWorkOrderID;
        protected System.Web.UI.WebControls.TextBox txtHKWorkOrderID;
        protected System.Web.UI.WebControls.TextBox txtHKWorkOrderName;
        protected System.Web.UI.WebControls.TextBox txtApprover1;
        protected System.Web.UI.WebControls.TextBox txtApprover2;
        protected System.Web.UI.WebControls.TextBox txtApprover3;
        // Code added for the Bug # 74- mpujari
        protected System.Web.UI.WebControls.TextBox txtApprover4;
        protected System.Web.UI.WebControls.TextBox txtApprover7; //FB 2501
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.TextBox hdnApprover2;
        protected System.Web.UI.WebControls.TextBox hdnApprover3;
        // Code added for the Bug # 74- mpujari
        protected System.Web.UI.WebControls.TextBox hdnApprover4;
        protected System.Web.UI.WebControls.TextBox hdnApprover7; //FB 2501
        protected System.Web.UI.WebControls.TextBox txtCompletedBy;
        protected System.Web.UI.WebControls.TextBox txtCATCompletedBy;
        protected System.Web.UI.WebControls.TextBox txtHKCompletedBy;
        protected System.Web.UI.WebControls.TextBox txtWorkOrderID;
        protected System.Web.UI.WebControls.TextBox txtComments;
        protected System.Web.UI.WebControls.TextBox txtCATComments;
        protected System.Web.UI.WebControls.TextBox txtHKComments;
        protected System.Web.UI.WebControls.TextBox HKDefaultRoomID;
        protected System.Web.UI.WebControls.TextBox HKDefaultSetID;
        protected System.Web.UI.WebControls.TextBox HKDefaultQuantity;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.WebControls.TextBox plblAVWorkOrders;
        protected System.Web.UI.WebControls.TextBox plblCateringWorkOrders;
        protected System.Web.UI.WebControls.TextBox plblHouseKeepingWorkOrders;
        protected System.Web.UI.WebControls.TextBox txtModifyType;
        protected System.Web.UI.WebControls.TextBox txtMaxAudioPorts;
        protected System.Web.UI.WebControls.TextBox txtMaxVideoPorts;
        protected System.Web.UI.WebControls.TextBox txtSelectedImage;
        protected System.Web.UI.WebControls.TextBox txtCANGC1;
        protected System.Web.UI.WebControls.TextBox txtTimeCheck;
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurringText;
        protected System.Web.UI.WebControls.TextBox hasVisited;
        protected System.Web.UI.WebControls.TextBox ImagesPath;
        protected System.Web.UI.WebControls.TextBox ImageFiles;
        protected System.Web.UI.WebControls.TextBox ImageFilesBT;
        protected System.Web.UI.WebControls.TextBox txtStartByDate;
        protected System.Web.UI.WebControls.TextBox txtCATStartByDate;
        protected System.Web.UI.WebControls.TextBox txtHKStartByDate;
        protected System.Web.UI.WebControls.TextBox txtDeliveryCost;
        protected System.Web.UI.WebControls.TextBox txtServiceCharges;
        //fb 1116
        protected System.Web.UI.WebControls.Label plblHostName;
        protected System.Web.UI.WebControls.Label plblRequestorName;//FB 2501
        protected System.Web.UI.WebControls.TextBox hdnApproverMail;
        protected System.Web.UI.WebControls.TextBox hdnRequestorMail; //FB 2501

        protected System.Web.UI.WebControls.MultiView Wizard1;
        protected System.Web.UI.WebControls.View SelectParticipants;
        protected System.Web.UI.WebControls.View SelectAV;
        protected System.Web.UI.WebControls.View SelectHousekeeping;
        protected System.Web.UI.WebControls.View SelectCatering;
        protected System.Web.UI.WebControls.View SelectAudioVisual;
        protected System.Web.UI.WebControls.Panel pnlPassword1;
        protected System.Web.UI.WebControls.Panel pnlPassword2;

        protected System.Web.UI.WebControls.DropDownList lstServices;
        protected System.Web.UI.WebControls.DropDownList lstConferenceType;
        protected System.Web.UI.WebControls.DropDownList lstConferenceTZ;
        protected System.Web.UI.WebControls.DropDownList lstRooms;
        protected System.Web.UI.WebControls.DropDownList lstCATRooms;
        protected System.Web.UI.WebControls.DropDownList lstHKRooms;
        protected System.Web.UI.WebControls.DropDownList lstAVSet;
        protected System.Web.UI.WebControls.DropDownList lstCATSet;
        protected System.Web.UI.WebControls.DropDownList lstHKSet;
        protected System.Web.UI.WebControls.DropDownList lstStatus;
        protected System.Web.UI.WebControls.DropDownList lstCATStatus;
        protected System.Web.UI.WebControls.DropDownList lstHKStatus;
        protected System.Web.UI.WebControls.DropDownList lstRoomLayout;
        protected System.Web.UI.WebControls.DropDownList lstRestrictNWAccess;
        protected System.Web.UI.WebControls.DropDownList lstRestrictUsage;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;
        protected System.Web.UI.WebControls.DropDownList lstAudioCodecs;
        protected System.Web.UI.WebControls.DropDownList lstVideoCodecs;
        protected System.Web.UI.WebControls.DropDownList lstVideoMode;
        protected System.Web.UI.WebControls.DropDownList lstDeliveryType;
        protected System.Web.UI.WebControls.DropDownList lstTimezones;
        protected System.Web.UI.WebControls.DropDownList lstCATTimezone;
        protected System.Web.UI.WebControls.DropDownList lstHKTimezone;
        protected System.Web.UI.WebControls.DropDownList lstEndpoints;
        protected System.Web.UI.WebControls.DropDownList lstTemplates; //Added for Create Template
        protected System.Web.UI.WebControls.DropDownList lstStartMode; //FB 2501
        protected System.Web.UI.HtmlControls.HtmlTableRow trStartMode; //FB 2501
        protected System.Web.UI.HtmlControls.HtmlTableRow trStartMode1;
        protected System.Web.UI.WebControls.ListBox Group;
        protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;

        protected System.Web.UI.WebControls.TreeView treeRoomSelection;

        //protected System.Web.UI.WebControls.FileUpload FileUpload1;
        //protected System.Web.UI.WebControls.FileUpload FileUpload2;
        //protected System.Web.UI.WebControls.FileUpload FileUpload3;

        protected System.Web.UI.WebControls.RadioButtonList rdSelView;
        protected System.Web.UI.WebControls.Panel pnlLevelView;
        protected System.Web.UI.WebControls.Panel pnlListView;

        protected System.Web.UI.WebControls.DataGrid AVMainGrid;
        protected System.Web.UI.WebControls.DataGrid CATMainGrid;
        protected System.Web.UI.WebControls.DataGrid HKMainGrid;
        protected System.Web.UI.WebControls.DataGrid itemsGrid;
        protected System.Web.UI.WebControls.DataGrid itemsGridCAT;
        protected System.Web.UI.WebControls.DataGrid itemsGridHK;
        protected System.Web.UI.WebControls.DataGrid dgConflict;
        protected System.Web.UI.HtmlControls.HtmlTableRow recurDIV; //Recurring Edit Dirty Instances
        protected String isInstanceEdit = "";  //Recurrence Fixes - Edit  instance
        protected String isCustomEdit = "";  //Recurrence Fixes - Edit  instance

        protected System.Web.UI.WebControls.DataGrid dgRooms;
        protected System.Web.UI.WebControls.DataGrid dgUsers;

        protected System.Web.UI.WebControls.CheckBox chkPublic;
        protected System.Web.UI.WebControls.CheckBox chkStartNow;
        protected System.Web.UI.WebControls.CheckBox chkCA1;
        protected System.Web.UI.WebControls.CheckBox chkOpenForRegistration;
        protected System.Web.UI.WebControls.CheckBox chkNotify;
        protected System.Web.UI.WebControls.CheckBox chkReminder;// FB 1926
        protected System.Web.UI.WebControls.CheckBox chkCATNotify;
        protected System.Web.UI.WebControls.CheckBox chkCATReminder;
        protected System.Web.UI.WebControls.CheckBox chkHKNotify;
        protected System.Web.UI.WebControls.CheckBox chkHKReminder;
        protected System.Web.UI.WebControls.CheckBox chkDualStreamMode;
        protected System.Web.UI.WebControls.CheckBox chkConfOnPort;
        protected System.Web.UI.WebControls.CheckBox chkEncryption;
        protected System.Web.UI.WebControls.CheckBox chkLectureMode;
        protected System.Web.UI.WebControls.CheckBox chkICAL;
        protected System.Web.UI.WebControls.CheckBox chkSingleDialin;
        protected System.Web.UI.WebControls.CheckBox chkPolycomSpecific;//FB 1229
        protected System.Web.UI.WebControls.CheckBox chkFECC; // FB 2501 FECC

        protected MetaBuilders.WebControls.ComboBox confStartTime;
        protected MetaBuilders.WebControls.ComboBox confEndTime;
        protected MetaBuilders.WebControls.ComboBox lstDuration;
        protected MetaBuilders.WebControls.ComboBox completedByTime;
        protected MetaBuilders.WebControls.ComboBox completedByTimeCAT;
        protected MetaBuilders.WebControls.ComboBox completedByTimeHK;
        protected MetaBuilders.WebControls.ComboBox startByTime;
        protected MetaBuilders.WebControls.ComboBox startByTimeCAT;
        protected MetaBuilders.WebControls.ComboBox startByTimeHK;

        protected System.Web.UI.WebControls.RequiredFieldValidator reqStartTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqStartDate;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqEndTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqEndDate;
        protected System.Web.UI.WebControls.RegularExpressionValidator regConfStartTime;//FB 2634
        protected System.Web.UI.WebControls.RegularExpressionValidator regEndTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator regTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator14;
        protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;
        protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1; //FB 2588
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload1;
        protected System.Web.UI.WebControls.Label lblUpload1;
        protected System.Web.UI.WebControls.Label hdnUpload1;
        protected System.Web.UI.WebControls.Button btnRemove1;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload2;
        protected System.Web.UI.WebControls.Label lblUpload2;
        protected System.Web.UI.WebControls.Label hdnUpload2;
        protected System.Web.UI.WebControls.Button btnRemove2;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload3;
        protected System.Web.UI.WebControls.Label lblUpload3;
        protected System.Web.UI.WebControls.Label hdnUpload3;
        protected System.Web.UI.WebControls.Button btnRemove3;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnextusrcnt; //API Port NO
        protected System.Web.UI.HtmlControls.HtmlInputHidden CreateBy;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtdgUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtLecturer;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetStartNow;//FB 1825
        //FB 2274 Starts
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossrecurEnable;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossdynInvite;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossroomModule;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossfoodModule;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrosshkModule;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossisVIP;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableRoomServiceType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossisSpecialRecur;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossConferenceCode;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossLeaderPin;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossAdvAvParams;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableBufferZone;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableEntity;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossAudioParams;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossdefaultPublic;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossP2PEnable;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableRoomConfType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableAudioVideoConfType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossDefaultConferenceType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableAudioOnlyConfType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossenableAV;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossenableParticipants;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossisMultiLingual;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossroomExpandLevel;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableImmConf;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableAudioBridges;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossDedicatedVideo;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossAddtoGroup;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnablePublicConf;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableConfPassword;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableRoomParam;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableSurvey;//FB 2348
        //public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnablePC;//FB 2347T
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossSetupTime; //FB 2398
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossTearDownTime; //FB 2398
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossMeetGreetBufferTime; //FB 2632
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableLinerate; // FB 2641
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableStartMode;// FB 2641
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnConferenceName;//FB 2694
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableNumericID;//FB 2870
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableProfileSelection;//FB 2947
        protected System.Web.UI.HtmlControls.HtmlTableRow trVIP;
        protected System.Web.UI.HtmlControls.HtmlTableRow trServType;
        protected System.Web.UI.HtmlControls.HtmlTableCell btnCheckAvailDIV;
        //FB 2274 Ends
        //code added for FB 1319 - start
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox selectAllCheckBox;
        //code added for FB 1319 - end
        //Code added for Search Room Error - start
        protected System.Web.UI.HtmlControls.HtmlInputButton btnCompare;
        protected System.Web.UI.WebControls.Panel pnlNoData;
        protected System.Web.UI.WebControls.Button GetAvailableRoom;
        protected System.Web.UI.WebControls.Button MeetingPlanner;
        //Code added for Search Room Error - end

        //code added for FB 1422 - start
        protected System.Web.UI.HtmlControls.HtmlTableRow trAVCommonSettings;
        //code added for FB 1422 - start

        //Virtual Bus - MOJ Phase 2 - Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trFile1;
        protected System.Web.UI.HtmlControls.HtmlTableRow trFile2;
        protected System.Web.UI.HtmlControls.HtmlTableRow trFile3;
        protected System.Web.UI.WebControls.Button btnUploadFiles;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPwd;
        protected System.Web.UI.HtmlControls.HtmlTableRow trType;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPublicOpen;
        //FB 2446 - Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpublic1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpubopen;
        //FB 2446 - End
        protected System.Web.UI.HtmlControls.HtmlTableCell Field4;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRec;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr10; //FB 2023
        //FB 2443 - Start
        protected System.Web.UI.HtmlControls.HtmlInputButton btnAudioparticipant; 
        protected System.Web.UI.HtmlControls.HtmlTableRow traudiobrdige;
        //FB 2443 - End
     
      

        //Virtual Bus - MOJ Phase 2 - End

        //Merging Recurrence - start

        protected System.Web.UI.WebControls.TextBox RecurDurationhr;
        protected System.Web.UI.WebControls.TextBox RecurDurationmi;
        protected System.Web.UI.WebControls.TextBox EndText;
        protected System.Web.UI.WebControls.RadioButtonList RecurType;
        protected System.Web.UI.WebControls.RadioButton DEveryDay;
        protected System.Web.UI.WebControls.TextBox DayGap;
        protected System.Web.UI.WebControls.RadioButton DWeekDay;
        protected System.Web.UI.WebControls.Panel Daily;
        protected System.Web.UI.WebControls.TextBox WeekGap;
        protected System.Web.UI.WebControls.CheckBoxList WeekDay;
        protected System.Web.UI.WebControls.Panel Weekly;
        protected System.Web.UI.WebControls.RadioButton MEveryMthR1;
        protected System.Web.UI.WebControls.TextBox MonthDayNo;
        protected System.Web.UI.WebControls.TextBox MonthGap1;
        protected System.Web.UI.WebControls.RadioButton MEveryMthR2;
        protected System.Web.UI.WebControls.DropDownList MonthWeekDayNo;
        protected System.Web.UI.WebControls.DropDownList MonthWeekDay;
        protected System.Web.UI.WebControls.TextBox MonthGap2;
        protected System.Web.UI.WebControls.Panel Monthly;
        protected System.Web.UI.WebControls.RadioButton YEveryYr1;
        protected System.Web.UI.WebControls.DropDownList YearMonth1;
        protected System.Web.UI.WebControls.TextBox YearMonthDay;
        protected System.Web.UI.WebControls.RadioButton YEveryYr2;
        protected System.Web.UI.WebControls.DropDownList YearMonthWeekDayNo;
        protected System.Web.UI.WebControls.DropDownList YearMonthWeekDay;
        protected System.Web.UI.WebControls.DropDownList YearMonth2;
        protected System.Web.UI.WebControls.Panel Yearly;
        protected System.Web.UI.WebControls.ListBox CustomDate;
        protected System.Web.UI.WebControls.Button btnsortDates;
        protected System.Web.UI.WebControls.Panel Custom;
        protected System.Web.UI.WebControls.TextBox StartDate;
        protected System.Web.UI.WebControls.RadioButton EndType;
        protected System.Web.UI.WebControls.RadioButton REndAfter;
        protected System.Web.UI.WebControls.TextBox Occurrence;
        protected System.Web.UI.WebControls.RadioButton REndBy;
        protected System.Web.UI.WebControls.TextBox EndDate;
        protected System.Web.UI.WebControls.Button Cancel;
        protected System.Web.UI.WebControls.Button Reset;
        protected System.Web.UI.WebControls.Button RecurSubmit;
        protected System.Web.UI.WebControls.Button RecurSubmit1;
        protected System.Web.UI.HtmlControls.HtmlTableRow RangeRow;
        protected System.Web.UI.WebControls.DropDownList RecurSetuphr;
        protected System.Web.UI.WebControls.DropDownList RecurSetupmi;
        protected System.Web.UI.WebControls.DropDownList RecurSetupap;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownhr;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownmi;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownap;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRecurValue;
        
        protected Int32 CustomSelectedLimit = 100;

        protected System.Web.UI.HtmlControls.HtmlTableCell PSndDate;
        protected System.Web.UI.HtmlControls.HtmlTableCell PEndDate;
        protected System.Web.UI.HtmlControls.HtmlControl RoomFrame;//Code added for Room Search
        protected System.Web.UI.WebControls.CheckBox chkRecurrence; //FB 1587
        //Merging Recurrence - end


        //code added for Add FB 1470 
        protected System.Web.UI.WebControls.DropDownList lstEntityCode;
        protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator18;    //FB 1492 work order fixes

        protected System.Web.UI.WebControls.Table tblCustomAttribute;   //custom attribute fixes
        private string custControlIDs = ""; //custom attribute fixes

        protected System.Web.UI.WebControls.RequiredFieldValidator reqFieldApproverAV;//Code added for WO  bug - 1379
        protected System.Web.UI.WebControls.Button btnCancel;//Code added for WO bug
        protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator9;//Code added for WO bug

        // FB 2426 Starts
        protected System.Web.UI.WebControls.Button btnGuestLocation;
        protected AjaxControlToolkit.ModalPopupExtender guestLocationPopup;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPreviewGuestLocation;
        protected System.Web.UI.HtmlControls.HtmlTableRow trGuestLocation;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVideoGuestLocation;
        // FB 2426 Ends
        protected System.Web.UI.HtmlControls.HtmlTable toggleText;//FB 2506
        protected System.Web.UI.HtmlControls.HtmlAnchor displayText; //FB 2506


        //FB 2501 Starts
        protected System.Web.UI.WebControls.TextBox txtVNOCOperator;
        protected System.Web.UI.WebControls.TextBox hdnVNOCOperator;
        protected System.Web.UI.WebControls.Label plblStartMode;
        //FB 2501 Ends
        //FB 2634
        protected System.Web.UI.WebControls.TextBox SetupDuration;
        protected System.Web.UI.WebControls.TextBox TearDownDuration;

        //FB 2998
        protected System.Web.UI.HtmlControls.HtmlTableRow MCUConnectRow;
        protected System.Web.UI.HtmlControls.HtmlTableRow MCUConnectDisplayRow;
        protected System.Web.UI.WebControls.TextBox txtMCUConnect;
        protected System.Web.UI.WebControls.TextBox txtMCUDisConnect;
        protected System.Web.UI.WebControls.CheckBox chkMCUConnect;
        protected System.Web.UI.WebControls.ImageButton imgSetup;
        protected System.Web.UI.WebControls.ImageButton imgTear;
        protected System.Web.UI.WebControls.ImageButton imgMCUConnect;
        protected System.Web.UI.WebControls.ImageButton imgMCUDisconnect;
        protected System.Web.UI.WebControls.Label lblMCUConnect;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUConnect;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUDisconnect;
        protected System.Web.UI.WebControls.Label lblPMCUConnect;
        protected System.Web.UI.WebControls.Label lblPMCUDisconnect;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUConnectDisplay;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUDisconnectDisplay;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUSetupTime;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUTeardownTime;
        public String mcuSetupDisplay = "";
        public String mcuTearDisplay = "";
        public String mcuEnable = "0";
        //2457 exchange round trip starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnicalID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnconfOriginID;
        public string conferenceOrigin = "";
        //2457 exchange round trip ends

        //Code added by Offshore for FB Issue 1073 -- Start
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        //Code added by Offshore for FB Issue 1073 -- End
        public string strConfStartTime;
        public string strConfEndTime;
        public string strDuration;
        protected static string selRooms;
        protected string usersstr;
        protected string partysInfo;
        //FB 2274 Starts
        public string roomExpand;
        public string P2PEnable;
        public string EnableRoomConfType;
        public string DefaultConferenceType;
        public string EnableAudioVideoConfType;
        public string EnableAudioOnlyConfType;
        public string EnableIsVip;
        public string EnableServiceType;
        public string foodModule;
        public string hkModule;
        public string roomModule;
        public string isMulti;
        public string EnablePublicConference;
        //FB 2446 - Start
        public string Enablerecurrence; 
        public string Enableopenforregistration;
        //FB 2446 - End
        public string EnableConferencePassword;
        public string EnableSurvey;//FB 2348
        //public string EnablePC;//FB 2347T
        //FB 2274 Ends
        public string txtConfServiceID = "";//FB 2839 Starts
        public int bridgeType;
        public string mcuName = "";//FB 2839 Ends
    
        protected bool IsDirtyCustom = false;   //Recurring Edit - Dirty Instances

        protected String enableAV = "0";//For AV Switch
        protected String enablePar = "0";//FB 1429
        protected String timeZone = "0";//FB 1425

        /* *** Code added for Buffer Zone *** --Start */
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetupTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBufferStr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTeardownTime;
        protected System.Web.UI.WebControls.Label lblTeardownDateTime;
        protected System.Web.UI.WebControls.Label lblSetupDateTime;
		//FB 2634
        //protected System.Web.UI.WebControls.TextBox SetupDate;
        //protected System.Web.UI.WebControls.TextBox TearDownDate;
        //protected MetaBuilders.WebControls.ComboBox SetupTime;
        //protected MetaBuilders.WebControls.ComboBox TeardownTime;
        //protected System.Web.UI.WebControls.TextBox SetupDateTime;
        //protected System.Web.UI.WebControls.TextBox TearDownDateTime;
        //protected System.Web.UI.WebControls.RegularExpressionValidator regTearDownStartTime;
        //protected System.Web.UI.WebControls.RegularExpressionValidator regSetupStartTime;
        protected System.Web.UI.WebControls.Label plblSetupDTime;
        protected System.Web.UI.WebControls.Label plblTeardownDTime;
        /* *** Code added for Buffer Zone *** --End */

        protected System.Web.UI.HtmlControls.HtmlTableRow NONRecurringConferenceDiv9; // Merging Recurrence
        protected System.Web.UI.WebControls.CheckBox chkEnableBuffer;
        //Organization/CSS Module -- Start
        protected System.Web.UI.HtmlControls.HtmlGenericControl Field1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl Field2;
        protected System.Web.UI.HtmlControls.HtmlTableCell Field3;
        protected System.Web.UI.HtmlControls.HtmlGenericControl Field5; 
        CustomizationUtil.CSSReplacementUtility cssUtil;
        //Organization/CSS Module -- End
        //FB 2620 Start
        protected System.Web.UI.WebControls.DropDownList lstVMR;
        //protected System.Web.UI.WebControls.CheckBox chkVMR;
        //FB 2620 End
        protected System.Web.UI.HtmlControls.HtmlInputHidden isVMR;//FB 2376  
        protected System.Web.UI.HtmlControls.HtmlTableRow trVMR;//FB 2376
        protected System.Web.UI.WebControls.Label plblConfVMR; //FB 2376
        protected System.Web.UI.WebControls.TextBox txtintbridge;//FB 2376
        protected System.Web.UI.WebControls.TextBox txtextbridge;//FB 2376
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnintbridge;//FB 2376 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnextbridge;//FB 2376 
        //FB 2693 Starts
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPCConf;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdBJ;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdJB;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdLync;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdVidtel;
        protected System.Web.UI.HtmlControls.HtmlTable tblPcConf;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdBJ;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdJB;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLy;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVid;
        int PCVendorType = 0;
        //FB 2693 Ends
        protected System.Web.UI.HtmlControls.HtmlInputHidden isPCCOnf;//FB 2819  
        TreeNodeCollection tnc;
        public int selIndex;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        MyVRMNet.Util utilObj; //FB 2236
        bool flag;
        bool flagClone; //FB Case 1029
        DataSet dsInv;
        ArrayList bridgeIds = null; //FB 1462

        //code added for FB 1422 - start
        public int cntTelnetRoom = 0;
        //code added for FB 1422 - End

        protected String enableEntity = "";
        protected String defaultPublic = ""; //FB 2451
        protected String enableBufferZone = "";
        protected String client = "";
        myVRMNet.CustomAttributes CAObj = null;   //custom attribute fixes
        protected bool isCOMError=false; //custom attribute fixes
        protected System.Web.UI.WebControls.Label plblCustomOption; //custom attribute fixes

        myVRMNet.ImageUtil imageUtilObj = null; //Image Project

        //protected System.Web.UI.HtmlControls.HtmlSelect lstCalendar;//For menu changes

        protected System.Web.UI.HtmlControls.HtmlTableRow trp2pLinerate;//Code added for Disney
        protected System.Web.UI.WebControls.DropDownList DrpDwnLstRate;//Code added for Disney
		
		protected String enableaudiobridge = "0"; //FB 2443
		//FB 2426 Start
        protected System.Web.UI.WebControls.DropDownList lstIPlinerate;
        protected System.Web.UI.WebControls.DropDownList lstSIPlinerate;
        protected System.Web.UI.WebControls.DropDownList lstISDNlinerate;
        protected System.Web.UI.WebControls.DropDownList lstIPVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstSIPVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstISDNVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstIPConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstSIPConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstISDNConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstCountries;
        //FB 2426 End
        /* *** Code added for Audio-addon *** */
        protected String enableConferenceCode  = "0";
        protected String enableLeaderPin = "0";
        protected String enableAdvAvParams  = "0";
        protected String enableAudioParams = "0";
        /* *** Code added for Audio-addon *** */
        protected String EnableRoomParam = "0";//FB 2359
        //FB 1716   
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDuration;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChange;     
        public string isEditMode = "0";
        //FB 1830 - Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPrice;
        protected String language = "";
        protected int languageid = 1;
        protected String cFormat = "";//FB 1830
        CultureInfo cInfo = null;
        decimal tmpVal = 0;
        StringDictionary xConfInfo; //FB 1830 Email Edit - start
        string xconfpassword = "";
        DateTime xconfsetup;
        DateTime xconftear;
        //public string emailAlertMes = "";
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnemailalert;
        List<int> xPartys;  //FB 1830 Email Edit - end
        //FB 1830 - End
        bool isToogle = false; //FB 2506 
        bool isCheckTimeError = false; // FB 2692
		//FB 1864
        protected System.Web.UI.WebControls.CheckBox chkisLiveAssitant;
        protected System.Web.UI.WebControls.CheckBox chkisDedicatedEngineer;
        protected System.Web.UI.WebControls.CheckBox chkisVIP;
        //FB 1864
        //FB 1911
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurSpec; 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSpecRec;
        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnParty; //FB 1865
        
		String[] pipeDelim = { "||" }; //FB 1888
        String[] ExclamDelim = { "!!" };//FB 1888
        //FB 1985 - Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMeetingPlanner;
        protected System.Web.UI.HtmlControls.HtmlTable tblMeetPlan;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMeetLinkSt;
        protected System.Web.UI.WebControls.Label lblParNote;
        protected System.Web.UI.WebControls.Label lblAudioNote;
        protected System.Web.UI.WebControls.LinkButton LnkMeetExpand;
        //FB 1985 - End
        protected System.Web.UI.WebControls.DropDownList DrpServiceType;//FB 2219
        protected System.Web.UI.HtmlControls.HtmlTableCell trHdConcSupport;//FB 2341 
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConcSupport;
        protected System.Web.UI.WebControls.ImageButton imgAudioNote; //FB 2023
        protected System.Web.UI.WebControls.ImageButton imgParNote;
        protected CheckBoxList ChklstConcSupport;
        //FB 2359 Start
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAudioBridges; 
        protected System.Web.UI.WebControls.Label lblAudioBridge;
        protected System.Web.UI.WebControls.Label plblConcierge; //FB 2632
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConcierge;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRooms;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRoomsDetails;
        protected System.Web.UI.HtmlControls.HtmlTable tblAVExpand;
        protected System.Web.UI.WebControls.LinkButton LnkAVExpand;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAVSt;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPublic;
        //FB 2359 End

        protected System.Web.UI.WebControls.Table tblConcierge; //FB 2377

        protected int OrgSetupTime = 0, OrgTearDownTime = 0, OrgMeetGrettBuffer = 0; //FB 2398 //FB 2632 //FB 2595
		protected Int32 OrgLineRate = 0; //FB 2429
        //FB 2426 Start
        protected System.Web.UI.WebControls.TextBox txtsiteName;
        protected System.Web.UI.WebControls.TextBox txtApprover5;
        protected System.Web.UI.WebControls.TextBox txtEmailId;
        protected System.Web.UI.WebControls.TextBox txtPhone;
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.TextBox txtState;
        protected System.Web.UI.WebControls.TextBox txtCity;
        protected System.Web.UI.WebControls.TextBox txtZipcode;
        protected System.Web.UI.WebControls.TextBox txtIPAddress;
        protected System.Web.UI.WebControls.TextBox txtIPPassword;
        protected System.Web.UI.WebControls.TextBox txtIPconfirmPassword;
        protected System.Web.UI.WebControls.TextBox txtISDNAddress;
        protected System.Web.UI.WebControls.TextBox txtISDNPassword;
        protected System.Web.UI.WebControls.TextBox txtISDNconfirmPassword;
        protected System.Web.UI.WebControls.TextBox txtSIPAddress;
        protected System.Web.UI.WebControls.TextBox txtSIPPassword;
        protected System.Web.UI.WebControls.TextBox txtSIPconfirmPassword;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestRoom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestRoomID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestloc;
        protected System.Web.UI.WebControls.DataGrid dgOnflyGuestRoomlist;
        protected System.Web.UI.WebControls.Button btnGuestLocationSubmit;
        protected System.Web.UI.HtmlControls.HtmlTableRow OnFlyRowGuestRoom;
        protected System.Web.UI.WebControls.RadioButton radioIsDefault;
        protected System.Web.UI.WebControls.RadioButton radioIsDefault2;
        protected System.Web.UI.WebControls.RadioButton radioIsDefault3;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelectVMRRoom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSmartP2PTotalEps; //FB 2430 start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnconftype;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableSmartP2P; //FB 2430 end
        //FB 2599 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfType; //FB 2262
        internal int isCloudEnabled = 0; //FB 2262-S
        //FB 2599 End
        DataTable onflyGrid = null;
        ArrayList colNames = null;
		
        protected System.Web.UI.WebControls.CheckBox chkmsg1;
        protected System.Web.UI.WebControls.CheckBox chkmsg2;
        protected System.Web.UI.WebControls.CheckBox chkmsg3;
        protected System.Web.UI.WebControls.CheckBox chkmsg4;
        protected System.Web.UI.WebControls.CheckBox chkmsg5;
        protected System.Web.UI.WebControls.CheckBox chkmsg6;
        protected System.Web.UI.WebControls.CheckBox chkmsg7;
        protected System.Web.UI.WebControls.CheckBox chkmsg8;
        protected System.Web.UI.WebControls.CheckBox chkmsg9;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration1;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration2;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration3;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration4;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration5;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration6;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration7;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration8;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration9;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg1;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg2;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg3;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg4;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg5;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg6;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg7;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg8;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg9;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTxtMsgDetails;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTxtMsg;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTxtMsg;
        //FB 2486 Ends
        protected System.Web.UI.HtmlControls.HtmlTableCell tdFECC;//FB 2571
        protected int EnableVNOCselection = 1; //FB 2608
        //FB 2632 - Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trDedicatedVNOCOperator;
        protected System.Web.UI.HtmlControls.HtmlGenericControl plblOnsiteAV;
        protected System.Web.UI.HtmlControls.HtmlGenericControl plblMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlGenericControl plblConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlGenericControl plblDedicatedVNOC;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblDedicatedVNOC;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOCOperator;
        //FB 2632 Ends
        //FB 2595 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trPreviewNetworkState;
        protected System.Web.UI.WebControls.Label lblNetworkState;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSecure;
        //FB 2595 End

        //FB 2441 Starts
        protected System.Web.UI.WebControls.CheckBox chkSendMail;
        //protected System.Web.UI.WebControls.TextBox txt_polycomTemplate; // Commented for ZD 100298
        //FB 2441 Ends
		protected System.Web.UI.HtmlControls.HtmlInputHidden TimeZoneText;//FB 2699
        // FB 2641 Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlinerate;
        protected int EnableLinerate = 0;
        protected int EnableStartMode = 0;
        // FB 2641 End

        //FB 2670 START
        protected System.Web.UI.HtmlControls.HtmlTableRow trOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConciergeSupport;
        protected System.Web.UI.HtmlControls.HtmlGenericControl LabelOnsiteAV;
        protected System.Web.UI.HtmlControls.HtmlGenericControl LabelMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlGenericControl LabelConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOnsiteAV;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDedicatedVNOC;

        //FB 2670 END
		//FB 2694 Starts
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfDesc;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfTitle;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfTemp;
        protected System.Web.UI.HtmlControls.HtmlTableRow SetupRow;
        protected System.Web.UI.HtmlControls.HtmlTableRow TearDownRow;
        protected System.Web.UI.HtmlControls.HtmlTableRow lblConfName;
        protected System.Web.UI.HtmlControls.HtmlTableRow lblConfDesc;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAutParticipant;
        protected System.Web.UI.HtmlControls.HtmlTableRow trparticipant;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVMRcall;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPLBLHKWO;//FB 2694
        protected System.Web.UI.HtmlControls.HtmlTableRow trSendIcalAttachment;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPCConf;
        //FB 2694 Ends
        //FB 2717 Starts
        protected System.Web.UI.WebControls.CheckBox chkCloudConferencing;
        protected System.Web.UI.HtmlControls.HtmlTableRow CloudConfRow;
        //FB 2717 End
		//FB 2659 - Starts
        protected int enableCloudInstallation = 0;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnOverBookConf;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnconfUniqueID;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSeatsAvailable;
        public System.Web.UI.HtmlControls.HtmlTable tblSeatsAvailability;//FB 2659
        //FB 2659 - End
		protected System.Web.UI.WebControls.DropDownList lstMCUProfile;//FB 2839
        //FB 2870 Start
        public string EnableNumericID;
        protected System.Web.UI.WebControls.CheckBox ChkEnableNumericID;
        protected System.Web.UI.WebControls.TextBox txtNumeridID;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrCTNumericID;
        protected System.Web.UI.WebControls.Label lblCTSNumericID;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCTSNumericID;

        //FB 2870 End
        public string EnableProfileSelection; //FB 2947

        //FB 2993 Starts
        protected System.Web.UI.WebControls.DropDownList drpNtwkClsfxtn;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnNetworkSwitching;
        // ZD 100263 Starts
        protected System.Web.UI.WebControls.RegularExpressionValidator regConfDisc;
        protected System.Web.UI.WebControls.RegularExpressionValidator regMaxVideoPorts;
        protected System.Web.UI.WebControls.RegularExpressionValidator regMaxAudioPorts;
        //protected System.Web.UI.WebControls.RegularExpressionValidator reg_polycomTemplate; //ZD 100298
        protected System.Web.UI.WebControls.RegularExpressionValidator reqPhone;
        protected System.Web.UI.WebControls.RegularExpressionValidator reqAddress2;
        protected System.Web.UI.WebControls.RegularExpressionValidator reqState;
        protected System.Web.UI.WebControls.RegularExpressionValidator reqCity  ;
        protected System.Web.UI.WebControls.RegularExpressionValidator reqZipcode;
        protected System.Web.UI.WebControls.RegularExpressionValidator reqSIPAddress2;
        protected System.Web.UI.WebControls.RegularExpressionValidator reqApprover1;
        protected System.Web.UI.WebControls.RegularExpressionValidator reqApprover3;
        // ZD 100263 Ends
        protected int NetworkSwitching = 0;
        //FB 2993 Ends
        protected int bridgeID = 0; //ZD 100298
        public Hashtable ProfileID = new Hashtable(); //ZD 100298
        public int ConfProfileID = 0, DefConfSerID = 0, ChangedSerID = -1, ConfBridge = 0; //ZD 100298
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFileUpload; //ZD 101052
        #endregion

        #region Constructor
        
        public ConferenceSetup()
        {
            //
            // TODO: Add constructor logic here
            //
            strConfStartTime = "";
            strConfEndTime = "";
            strDuration = "";
            selRooms = ", ";
            usersstr = String.Empty;
            partysInfo = String.Empty;
            tnc = new TreeNodeCollection();
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objInXML = new ns_InXML.InXML();
            dsInv = new DataSet();
            flag = false;
            flagClone = false; //FB Case 1029 Revathi
            imageUtilObj = new myVRMNet.ImageUtil(); //Image Project
            utilObj = new MyVRMNet.Util(); //FB 2236
        }

        #endregion

        // FB 2050 Start
        #region Page PreInit Method
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
                Page.ClientTarget = "uplevel";
        }
        #endregion
        // FB 2050 Ends

        #region Methods Executed on Page Load
        private void Page_Init()
        {
            //Response.Write("<br>PageInit: " + DateTime.Now.ToString("hh:mm ss"));
            //hasVisited.Text = "0";
            try
            {
                

                if (Application["Client"] == null)
                    Application["Client"] = "";

                client = Application["Client"].ToString();
                //FB 1830
                cInfo = new CultureInfo(Session["NumberFormat"].ToString());
                
                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.PennState))
                {
                    chkDualStreamMode.Checked = true;
                }
                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.LHRIC))
                    tblLHRICCustomAttributes.Visible = true;

                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl))
                {
                    //Wizard1.Views.Remove(this.SelectParticipants);
                    TopMenu.Items[1].Text = String.Empty;
                    //Wizard1.WizardSteps.Remove(this.selectAdvAVSettings);
                    //this.selectAdvAVSettings.ID = ""; this.selectAdvAVSettings.Title = "";
                    pnlPassword1.Visible = false;
                    pnlPassword2.Visible = false;
                    trPwd.Visible = false; //FB 2446
                    lstConferenceType.Enabled = false;
                    tblBillingOptions.Visible = true;
                }

                /* *** Code added for Audio-addon *** */
                if (Session["ConferenceCode"] != null)
                    enableConferenceCode = Session["ConferenceCode"].ToString();
                if (Session["LeaderPin"] != null)
                    enableLeaderPin = Session["LeaderPin"].ToString();
                if (Session["AdvAvParams"] != null)
                    enableAdvAvParams = Session["AdvAvParams"].ToString();
                if (Session["AudioParams"] != null)
                    enableAudioParams = Session["AudioParams"].ToString();
                //FB 2359 Start
                if (Session["EnableRoomParam"] != null)
                    EnableRoomParam = Session["EnableRoomParam"].ToString();

               
                //FB 2359 End
                


                /* *** Code added for Audio-addon *** */

                if (Session["EnableBufferZone"] == null)//Organization Module Fixes
                {
                    Session["EnableBufferZone"] = "0"; //Organization Module Fixes
                }

                enableBufferZone = Session["EnableBufferZone"].ToString();//Organization Module Fixes

                //if (enableBufferZone == "1")
                //    NONRecurringConferenceDiv9.Visible = true; //FB 2274
                //else
                //    NONRecurringConferenceDiv9.Visible = false; //FB 2274 

                if (Session["EnableEntity"] == null)//Organization Module Fixes
                    Session["EnableEntity"] = "0";//Organization Module Fixes

               

                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.NGC)) //Custom attribute fixes - removed the enableenitiy
                {
                    //tblEntityCode.Visible = true;
                    tblEntityCode.Attributes.Add("style", "Display:Block");
                }
                else
                    tblEntityCode.Attributes.Add("style", "Display:None");

                //Wizard1.Attributes.Add("onblur", "javascript:return CheckFiles()");
                //foreach (WizardStep ws in Wizard1.WizardSteps)
                //    ws.Title = "<a href='#' onclick='javascript:return CheckFiles()'>" + ws.Title + "</a>";

               

                if (Session["defaultPublic"].ToString().Equals("1"))
                {
                    chkPublic.Checked = true;
                }

                // Response.Write("<br>PageInit: " + DateTime.Now.ToString("hh:mm ss"));
                //Code Modified For MOJ Phase2 - Start
                if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    MeetingPlanner.Attributes.Add("style", "display:block");// Code added for FB 1425 QA Bug
                    if (Session["DefaultConferenceType"] != null)//Organization Module Fixes
                    {
                        lstConferenceType.ClearSelection();//FB 2377
                        DefaultConferenceType = Session["DefaultConferenceType"].ToString();
                        if (DefaultConferenceType != "")
                            lstConferenceType.Items.FindByValue(DefaultConferenceType).Selected = true;//Organization Module Fixes
                    }
                }
                else
                {
                    lstConferenceType.ClearSelection();
                    if (lstConferenceType.Items.FindByValue("7") != null)
                        lstConferenceType.Items.FindByValue("7").Selected = true;//7- Room Conference
                }
                //Code Modified For MOJ Phase2 - End
                /* **** Code added for AV switch fixes ,1429,1425 *** */
                if (Session["enableAV"] != null)
                {
                    if (!Session["enableAV"].ToString().Equals(""))
                        enableAV = Session["enableAV"].ToString();
                }

                if (Session["enableParticipants"] != null)//FB 1429
                {
                    if (!Session["enableParticipants"].ToString().Equals(""))
                        enablePar = Session["enableParticipants"].ToString();
                }
                 if (Session["hkModule"] != null)
                    hkModule = Session["hkModule"].ToString();//FB 2835

                if (Session["timeZoneDisplay"] != null) //FB 1425 
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

                /* **** Code added for AV switch fixes *** */
                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                Session["FormatDateType"] = ((Session["FormatDateType"] == null) ? "1" : Session["FormatDateType"]);
                Session["timeZoneDisplay"] = ((Session["timeZoneDisplay"] == null) ? "1" : Session["timeZoneDisplay"]);
                Application["interval"] = ((Application["interval"] == null) ? "30" : Application["interval"]); //Providea
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 Ends
                if (Session["OrgLineRate"] != null)//FB 2429
                {
                    if (!Session["OrgLineRate"].ToString().Equals(""))
                        Int32.TryParse(Session["OrgLineRate"].ToString(), out OrgLineRate);
                }

                //FB 2571 Start
                string DefaultFECC = Session["DefaultFECC"].ToString();
                string EnableFECC = Session["EnableFECC"].ToString();

                if (EnableFECC == "1")
                {
                    tdFECC.Attributes.Add("Style", "Display:Block;");
                    chkFECC.Visible = true;
                }
                else
                {
                    tdFECC.Attributes.Add("Style", "Display:None;");
                    chkFECC.Visible = false;
                }
                if (DefaultFECC == "1")
                    chkFECC.Checked = true;
                else
                    chkFECC.Checked = false;

                if (EnableFECC == "2")
                {
                    tdFECC.Attributes.Add("Style", "Display:None;");
                    chkFECC.Visible = false;
                    chkFECC.Checked = false;
                }
               
                //FB 2571 End
                // FB 2641 Start
                if (Session["EnableStartMode"] != null)
                    if (!string.IsNullOrEmpty(Session["EnableStartMode"].ToString()))
                        int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);

                if (Session["EnableLinerate"] != null)
                    if (!string.IsNullOrEmpty(Session["EnableLinerate"].ToString()))
                        int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);
                // FB 2641 End
                //FB 1985 - Start
                if (client.ToUpper() == "DISNEY")
                {
                    if (enableAdvAvParams == "0")
                    {
                        tblAVExpand.Attributes.Add("style", "display:none");//FB 2359
                        LnkAVExpand.Attributes.Add("style", "display:none");//FB 2359
                    }
                    else
                    {
                        tblAVExpand.Attributes.Add("style", "display:block");//FB 2359
                    }
                    trAVCommonSettings.Attributes.Add("style", "display:none");//FB 2359
                    tblMeetPlan.Attributes.Add("style", "display:block");
                    tdMeetingPlanner.Attributes.Add("style", "display:none");
                    LnkMeetExpand.Attributes.Add("style", "display:none");
                    chkDualStreamMode.Checked = true;

                    if (lstConferenceType.SelectedValue == "2")
                    {
                        //lblAudioNote.Visible = true;
                        //lblAudioNote.Text = "Note: to add an audiobridge to your videoconference, 1) complete the information on this page, "; //FB 2023 start
                        //lblAudioNote.Text += "2) under the Select Participants tab, choose address book and select Domestic Audio Add-on (for example) ";
                        imgAudioNote.Visible = true;
                        imgAudioNote.ToolTip = "Note: to add an audio bridge to your conference, 1) complete the information on this page, 2) under the Select Participants tab, "
                                             + "Choose Add Audio Add-On Bridge button and select the desired audio bridge";
                        imgAudioNote.ToolTip = obj.GetTranslatedText(imgAudioNote.ToolTip);
                        
                        //lblParNote.Visible = true;
                        //lblParNote.Text = "Note: to complete request to add an audio bridge to your videoconference, choose address book and select Domestic";
                        //lblParNote.Text += " Audio Add-on (for example), providing Conference Code and Leader Pin on the Audio Settings tab";
                        imgParNote.Visible = true;
                        imgParNote.ToolTip = "Note: to complete request to add an audio bridge to your conference, "
                                           + "choose Add Audio Add-On Bridge button and select the desired audio bridge";  //FB 2023 end
                        imgParNote.ToolTip = obj.GetTranslatedText(imgParNote.ToolTip);
                        
                    }
                    else
                    {
                        //lblAudioNote.Visible = false;
                        //lblParNote.Visible = false;
                        imgAudioNote.Visible = false;
                        imgParNote.Visible = false;
                    }
                    TopMenu.Items[3].Text = "<div align='center' style='width:123'><b>Audio</b><br><b>Settings</b></div>";
					//if (enableAV == "0" || chkVMR.Checked)//FB 2448
                    if((lstConferenceType.SelectedValue.Equals("7")) || (lstConferenceType.SelectedValue.Equals("4")) || (lstConferenceType.SelectedValue.Equals("8"))) //FB 3022
                        chkPCConf.Checked =false;
                    if (enableAV == "0" || lstVMR.SelectedIndex > 0 || chkPCConf.Checked) //FB 2448 FB 2620 //FB 2819
                        TopMenu.Items[3].Text = "";
                    //FB 2501
                    if (lstConferenceType.SelectedValue.Equals("4") || lstConferenceType.SelectedValue.Equals("7") || lstVMR.SelectedIndex > 0 || lstConferenceType.SelectedValue.Equals("8")) //FB 2694 //FB 2620
                    {
                        trStartMode.Attributes.Add("style", "display:None");//FB 2641
                        trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                    }
                    else
                    {
                        //FB 2641 start
                        if (EnableStartMode == 1)
                        {
                            trStartMode.Attributes.Add("style", "display:");
                            trStartMode1.Attributes.Add("style", "display:");//FB 2717
                        }
                        else
                        {
                            trStartMode.Attributes.Add("style", "display:None");
                            trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                        }
                    }

                    lstRestrictNWAccess.ClearSelection();
                    lstRestrictNWAccess.Items.FindByValue("3").Selected = true;
                    chkDualStreamMode.Checked = true;

                }
                //FB 2429 - Starts
                lstLineRate.ClearSelection();
                if (lstLineRate.Items.Count <= 0)
                    obj.BindLineRate(lstLineRate);
                lstLineRate.SelectedValue = OrgLineRate.ToString();
                if (OrgLineRate <= 0)
                    lstLineRate.SelectedValue = "384";
                //FB 2429 - End

                //FB 2641 start
                if (EnableStartMode == 1 && !lstConferenceType.SelectedValue.Equals("8")) //FB 2694
                    trStartMode.Attributes.Add("Style", "display:;");
                else
                    trStartMode.Attributes.Add("Style", "display:none;");

                if (EnableLinerate == 1)
                {
                    tdlinerate.Attributes.Add("style", "visibility:visible;");
                    lstLineRate.Attributes.Add("Style", "visibility:visible;");
                }
                else
                {
                    tdlinerate.Attributes.Add("style", "visibility:hidden;");
                    lstLineRate.Attributes.Add("Style", "visibility:hidden;");
                }
                //FB 2641 End
                //FB 2670 START
                string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();
                string EnableVNOCselection = Session["EnableVNOCselection"].ToString();

                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0")
                {
                    trConciergeSupport.Attributes.Add("Style", "Display:None;");
                    tdConcierge.Attributes.Add("Style", "Display:None;");
                }
                else
                {
                    trConciergeSupport.Attributes.Add("Style", "Display:Block;");
                    tdConcierge.Attributes.Add("Style", "Display:Block;");
                }
                  

                if (EnableOnsiteAV == "1")
                {
                    trOnSiteAVSupport.Attributes.Add("Style", "Display:Block;");
                    tdOnsiteAV.Attributes.Add("Style", "Display:Block;"); 
                }
                else
                {
                    trOnSiteAVSupport.Attributes.Add("Style", "Display:None;");
                    tdOnsiteAV.Attributes.Add("Style", "Display:None;"); 
                }
                if (EnableMeetandGreet == "1")
                {
                    trMeetandGreet.Attributes.Add("Style", "Display:Block;");
                    tdMeetandGreet.Attributes.Add("Style", "Display:Block;"); 
                }
                else
                {
                    trMeetandGreet.Attributes.Add("Style", "Display:None;");
                    tdMeetandGreet.Attributes.Add("Style", "Display:None;"); 
                }
                if (EnableConciergeMonitoring == "1")
                {
                    trConciergeMonitoring.Attributes.Add("Style", "Display:Block;");
                    tdConciergeMonitoring.Attributes.Add("Style", "Display:Block;");
                }
                else
                {
                    trConciergeMonitoring.Attributes.Add("Style", "Display:None;");
                    tdConciergeMonitoring.Attributes.Add("Style", "Display:None;"); 
                }
                if (EnableDedicatedVNOC == "1")
                {
                    trDedicatedVNOCOperator.Attributes.Add("Style", "Display:Block;");
                    tdDedicatedVNOC.Attributes.Add("Style", "Display:Block;"); 
                }
                else
                {
                    trDedicatedVNOCOperator.Attributes.Add("Style", "Display:None;");
                    tdDedicatedVNOC.Attributes.Add("Style", "Display:None;");
                }

                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "1" && EnableVNOCselection == "0" || EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" && EnableVNOCselection == "1" || EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" && EnableVNOCselection == "0")
                {
                    trConciergeSupport.Attributes.Add("Style", "Display:None;");
                    tdConcierge.Attributes.Add("Style", "Display:None;");
                }
                else
                {
                    trConciergeSupport.Attributes.Add("Style", "Display:Block;");
                    tdConcierge.Attributes.Add("Style", "Display:Block;");
                }
                
                //FB 2670 END

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("conferencesetup.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].Equals("0")) // ZD 100263
                {
                    regConfDisc.Enabled = false;
                    regMaxVideoPorts.Enabled = false;
                    regMaxAudioPorts.Enabled = false;
					// reg_polycomTemplate.Enabled = false; ZD 100298
                    reqPhone.Enabled = false;
                    reqAddress2.Enabled = false;
                    reqState.Enabled = false;
                    reqCity.Enabled = false;
                    reqZipcode.Enabled = false;
                    reqSIPAddress2.Enabled = false;
                    reqApprover1.Enabled = false;
                    reqApprover3.Enabled = false;
                }

                lblAudioNote.Visible = false; //FB 2023
                lblParNote.Visible = false;

                if (hdnconftype.Value == "4" && lstConferenceType.Items.FindByValue("4") != null) //FB 2430
                    lstConferenceType.SelectedValue = "4";

                //FB 2998 - Start
                if (Session["MCUSetupDisplay"] != null)
                    mcuSetupDisplay = Session["MCUSetupDisplay"].ToString();

                if (Session["MCUTearDisplay"] != null)
                    mcuTearDisplay = Session["MCUTearDisplay"].ToString();
                //FB 2998 - End

                //FB 1830 - Starts
                if(Session["CurrencyFormat"] != null)
                    cFormat = Session["CurrencyFormat"].ToString();
                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 1830 - End
               //FB  2486 Starts
                if (Session["languageID"] == null)
                    Session["languageID"] = "1";
                if(Session["languageID"].ToString() !="")
                   Int32.TryParse(Session["languageID"].ToString(),out languageid);
                //FB 2486 Ends

                if (Request.QueryString["t"].ToString() == "")//FB 1716 - FB 1830 Email Edit
                    isEditMode = "1";

                if (Session["EnableSurvey"] != null)//FB 2348
                    if (Session["EnableSurvey"].ToString() != "")
                        EnableSurvey = Session["EnableSurvey"].ToString();

                if (chkPCConf.Checked)
                {
                    tblPcConf.Style.Add("display", "block");
                    isPCCOnf.Value = "1";
                }
                else  //FB 2819
                {
                    tblPcConf.Style.Add("display", "none");
                    isPCCOnf.Value = "0";
                }


                if (Session["EnableBlueJeans"] != null)
                {
                    if (Session["EnableBlueJeans"].ToString().Equals("0"))
                        tdBJ.Attributes.Add("style", "display:none");
                }
                if (Session["EnableJabber"] != null)
                {
                    if (Session["EnableJabber"].ToString().Equals("0"))
                        tdJB.Attributes.Add("style", "display:none");
                }
                if (Session["EnableLync"] != null)
                {
                    if (Session["EnableLync"].ToString().Equals("0"))
                        tdLy.Attributes.Add("style", "display:none");
                }
                if (Session["EnableVidtel"] != null)
                {
                    if (Session["EnableVidtel"].ToString().Equals("0"))
                        tdVid.Attributes.Add("style", "display:none");
                }
                //FB 2693 Ends

                // FB 2608 Start
                if (Session["EnableVNOCselection"] != null)
                {
                    if (CAObj == null)
                        CAObj = new myVRMNet.CustomAttributes();
                    if (Session["EnableVNOCselection"].ToString() == "0" || Session["EnableDedicatedVNOC"].ToString() == "0")//FB 2670
                    {

                        trDedicatedVNOCOperator.Visible = false;//FB 2632
                        EnableVNOCselection = 0;
                        plblDedicatedVNOC.Visible = false;
                        lblDedicatedVNOC.Visible = false; //FB 2670 
                    }
                    else
                    {
                        trDedicatedVNOCOperator.Visible = true;
                        EnableVNOCselection = 1;
                        lblDedicatedVNOC.Visible = true;
                        plblDedicatedVNOC.Visible = true;
                    }
                }
                // FB 2608 End

 				//FB 2694 Starts
                trConfDesc.Visible = true;
                trConfTitle.Visible = true;
                trConfTemp.Visible = true;
                lblConfName.Attributes.Add("style", "visibility:visible");
                lblConfDesc.Attributes.Add("style", "display:");
                //FB 2694 Ends

                //FB 2641 start
                if (Session["EnableStartMode"] != null)
                    if (!string.IsNullOrEmpty(Session["EnableStartMode"].ToString()))
                        int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);

                if (Session["EnableLinerate"] != null)
                    if (!string.IsNullOrEmpty(Session["EnableLinerate"].ToString()))
                        int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);
                //FB 2641 End

                //FB 2599 Start
                if (Session["Cloud"] != null) //FB 2262-S
                    if (Session["Cloud"].ToString().Trim() == "1")
                        isCloudEnabled = 1;
                //FB 2599 End

				//FB 2659 - Starts
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);

                if (enableCloudInstallation == 1)
                {
                    trSeatsAvailable.Visible = true;
                }
                else
                {
                    trSeatsAvailable.Visible = false;
                    hdnOverBookConf.Value = "0";
                }
                //FB 2659 - End
                if (Session["HashProfileIDs"] != null)
                    ProfileID = (Hashtable)Session["HashProfileIDs"];//ZD 100298

                CrossSiloSession(); //FB 2274

                //FB 2641 start
                if (EnableStartMode == 1 && !lstConferenceType.SelectedValue.Equals("8")) //FB 2694
                    trStartMode.Attributes.Add("Style", "Display:;");
                else
                    trStartMode.Attributes.Add("Style", "Display:none;");

                if (EnableLinerate == 1)
                {
                    tdlinerate.Attributes.Add("style", "visibility:visible;");
                    lstLineRate.Attributes.Add("Style", "visibility:visible;");
                }
                else
                {
                    tdlinerate.Attributes.Add("style", "visibility:hidden;");
                    lstLineRate.Attributes.Add("Style", "visibility:hidden;");
                }
                //FB 2641 End
                //FB 3007 START
                if (lstConferenceType.SelectedValue.Equals("8"))
                {
                    trConciergeSupport.Attributes.Add("Style", "Display:None;");
                    tdConcSupport.Attributes.Add("Style", "Display:None;");
                    trHdConcSupport.Attributes.Add("Style", "Display:None;");
                }
                else
                {
                    trConciergeSupport.Attributes.Add("Style", "Display:Block;");
                    tdConcSupport.Attributes.Add("Style", "Display:Block;");
                    trHdConcSupport.Attributes.Add("Style", "Display:Block;");
                }
                string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();
              

                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0")
                    trConciergeSupport.Attributes.Add("Style", "Display:None;");
                else
                    trConciergeSupport.Attributes.Add("Style", "Display:Block;");
                   
                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "1" && EnableVNOCselection == 0 || EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" && EnableVNOCselection == 1 || EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" && EnableVNOCselection == 0)
                    trConciergeSupport.Attributes.Add("Style", "Display:None;");
                else
                    trConciergeSupport.Attributes.Add("Style", "Display:Block;");
                //FB 3007 END
                if (!IsPostBack)
                {
                    TemplateColumn tmpcolumn = itemsGrid.Columns[8] as TemplateColumn;
                    if (tmpcolumn != null)
                        tmpcolumn.HeaderText = obj.GetTranslatedText("Delivery")+"<br>"+obj.GetTranslatedText("Cost")+ " (" + cFormat + ")";
                    tmpcolumn = itemsGrid.Columns[9] as TemplateColumn;
                    if (tmpcolumn != null)
                        tmpcolumn.HeaderText = obj.GetTranslatedText("Service")+"<br>"+obj.GetTranslatedText("Charge")+ "(" + cFormat + ")";
                    tmpcolumn = itemsGrid.Columns[10] as TemplateColumn;
                    if (tmpcolumn != null)
                        tmpcolumn.HeaderText = obj.GetTranslatedText("Price")+" (" + cFormat + ")";
                    tmpcolumn = CATMainGrid.Columns[8] as TemplateColumn;
                    if (tmpcolumn != null)
                        tmpcolumn.HeaderText = obj.GetTranslatedText("Price")+" (" + cFormat + ")";
                    BoundColumn bndcolumn = AVMainGrid.Columns[13] as BoundColumn;
                    if (bndcolumn != null)
                        bndcolumn.HeaderText = obj.GetTranslatedText("Service")+"<br>"+ obj.GetTranslatedText("Charge")+" (" + cFormat + ")";
                    bndcolumn = AVMainGrid.Columns[14] as BoundColumn;
                    if (bndcolumn != null)
                        bndcolumn.HeaderText = obj.GetTranslatedText("Delivery")+"<br>"+obj.GetTranslatedText("Cost")+" (" + cFormat + ")";
                    bndcolumn = AVMainGrid.Columns[15] as BoundColumn;
                    if (bndcolumn != null)
                        bndcolumn.HeaderText = obj.GetTranslatedText("Total")+"<br>"+obj.GetTranslatedText("Cost")+"(" + cFormat + ")";
                    bndcolumn = HKMainGrid.Columns[12] as BoundColumn;
                    if (bndcolumn != null)
                        bndcolumn.HeaderText = obj.GetTranslatedText("Total")+"<br>"+ obj.GetTranslatedText("Cost")+" (" + cFormat + ")";
                    bndcolumn = itemsGridHK.Columns[5] as BoundColumn;
                    if (bndcolumn != null)
                        bndcolumn.HeaderText = obj.GetTranslatedText("Price")+"(" + cFormat + ")";
                }

                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    trFile1.Attributes.Add("style", "display:none");
                    trFile2.Attributes.Add("style", "display:none");
                    trFile3.Attributes.Add("style", "display:none");
                    btnUploadFiles.Visible = false;
                    trPwd.Attributes.Add("style", "display:none");
                    trType.Attributes.Add("style", "display:none");
                    trPublicOpen.Attributes.Add("style", "display:none");
                    TrCTNumericID.Attributes.Add("style", "display:none");//FB 2870
                    Field4.InnerText = "Created By";

                }
                ////MOJ - Phase2 Changes - End

                //FB 1830 - Translation - Starts

                if (isMulti != null) //FB 2274
                {
                    if (isMulti != "1")
                    {
                        //Organization/CSS Module - Create folder for UI Settings --- Strat
                        String fieldText = "";
                        cssUtil = new CustomizationUtil.CSSReplacementUtility();

                        fieldText = cssUtil.GetUITextForControl("ConferenceSetup.aspx", "Field1");
                        Field1.InnerText = fieldText;

                        fieldText = cssUtil.GetUITextForControl("ConferenceSetup.aspx", "Field2");
                        Field2.InnerText = fieldText;

                        fieldText = cssUtil.GetUITextForControl("ConferenceSetup.aspx", "Field3");
                        Field3.InnerText = fieldText;

                        fieldText = cssUtil.GetUITextForControl("ConferenceSetup.aspx", "Field4");
                        Field4.InnerText = fieldText;

                        fieldText = cssUtil.GetUITextForControl("ConferenceSetup.aspx", "Field5");
                        Field5.InnerText = fieldText;

                        fieldText = cssUtil.GetUITextForControl("ConferenceSetup.aspx", "btnConfSubmit");

                        //if (btnConfSubmit.Text.IndexOf("Custom") < 0) //FB 1646
                        if (btnConfSubmit.Text.IndexOf(obj.GetTranslatedText("Customized")) < 0) //FB 1646 //FB JAPAN
                            btnConfSubmit.Text = fieldText;
                    }
                }//FB 1830 - Translation - End
                //Organization/CSS Module - Create folder for UI Settings --- End

                if (Session["roomExpandLevel"] == null)//Organization Module Fixes
                    Session.Add("roomExpandLevel", "1");
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                //Code added by Offshore for FB Issue 1073 -- End
                //Response.Write("<br>load: " + DateTime.Now.ToString("hh:mm ss"));
                //Response.Write(dgUsers.Items.Count);
                errLabel.Text = "";
                //26&3&00&PM&60#5#07/14/2008&07/15/2008&07/16/2008&07/17/2008&07/21/2008&07/22/2008&07/23/2008
                strConfStartTime = confStartTime.Text;
                strConfEndTime = confEndTime.Text;
                strDuration = lstDuration.Text;
                ConferencePassword.Attributes.Add("value", confPassword.Value);
                ConferencePassword2.Attributes.Add("value", confPassword.Value);
                errLabel.Visible = true;
                //Response.Write("admin value is : " + Session["admin"].ToString());

                //FB 2501
                if (lstConferenceType.SelectedValue.Equals("4") || lstConferenceType.SelectedValue.Equals("7") || lstVMR.SelectedIndex > 0 || lstConferenceType.SelectedValue.Equals("8")) //FB 2694 //FB 2620
                {
                      trStartMode.Attributes.Add("style", "display:None");//FB 2641 
                      trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                }
                else
                {
                    //FB 2641 start
                    if (EnableStartMode == 1)
                    {
                        trStartMode.Attributes.Add("style", "display:");
                        trStartMode1.Attributes.Add("style", "display:");//FB 2717
                    }
                    else
                    {
                        trStartMode.Attributes.Add("style", "display:None");
                        trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                    }
                    //FB 2641 End
                   
                }
                //FB 2599 Start
                if (isCloudEnabled == 1) //FB 2717
                    CloudConfRow.Attributes.Add("style", "display:");
                else
                    CloudConfRow.Attributes.Add("style", "display:None");
                //FB 2599 End

                if (!IsPostBack)
                {
                    Session.Remove("HashProfileIDs"); //ZD 100298
                    obj.BindLineRate(DrpDwnLstRate);//Code added for Disney
                    //FB 2426 Start
                    obj.BindLineRate(lstIPlinerate);
                    obj.BindLineRate(lstSIPlinerate);
                    obj.BindLineRate(lstISDNlinerate);
                    obj.BindVideoEquipment(lstIPVideoEquipment);
                    obj.BindVideoEquipment(lstSIPVideoEquipment);
                    obj.BindVideoEquipment(lstISDNVideoEquipment);
                    obj.BindDialingOptions(lstIPConnectionType);
                    obj.BindDialingOptions(lstSIPConnectionType);
                    obj.BindDialingOptions(lstISDNConnectionType);
                    obj.GetCountryCodes(lstCountries);

                    //FB 2486 Starts
                    String inxml = "<GetConfMsg>" + obj.OrgXMLElement() + "<UserID>" + Session["userID"].ToString() + "</UserID><language>1</language></GetConfMsg>";
                    String outxml = obj.CallMyVRMServer("GetConfMsg", inxml, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetConfMsgList/GetConfMsg");

                    obj.LoadOrgList(drpdownconfmsg1, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg2, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg3, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg4, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg5, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg6, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg7, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg8, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg9, nodes, "ConfMsgID", "ConfMsg");

                    obj.SetToolTip(drpdownconfmsg1);                    
                    obj.SetToolTip(drpdownconfmsg2);
                    obj.SetToolTip(drpdownconfmsg3);
                    obj.SetToolTip(drpdownconfmsg4);
                    obj.SetToolTip(drpdownconfmsg5);
                    obj.SetToolTip(drpdownconfmsg6);
                    obj.SetToolTip(drpdownconfmsg7);
                    obj.SetToolTip(drpdownconfmsg8);
                    obj.SetToolTip(drpdownconfmsg9);

                    ArrayList min5 = new ArrayList();
                    ArrayList min2 = new ArrayList();
                    ArrayList minS = new ArrayList();
                    Int32 ii = 0;
                    for (Int32 m = 5; m <= 30; m = m + 5)
                        min5.Insert(ii++, m + "M");

                    drpdownmsgduration1.DataSource = min5;
                    drpdownmsgduration1.DataBind();

                    drpdownmsgduration4.DataSource = min5;
                    drpdownmsgduration4.DataBind();

                    drpdownmsgduration7.DataSource = min5;
                    drpdownmsgduration7.DataBind();

                    ii = 0;
                    for (Int32 m = 2; m <= 20; m = m + 2)
                        min2.Insert(ii++, m + "M");

                    drpdownmsgduration2.DataSource = min2;
                    drpdownmsgduration2.DataBind();

                    drpdownmsgduration5.DataSource = min2;
                    drpdownmsgduration5.DataBind();

                    drpdownmsgduration8.DataSource = min2;
                    drpdownmsgduration8.DataBind();

                    ii = 0;
                    for (Int32 m = 30; m <= 120; m = m + 30)
                        minS.Insert(ii++, m + "s");

                    drpdownmsgduration3.DataSource = minS;
                    drpdownmsgduration3.DataBind();

                    drpdownmsgduration6.DataSource = minS;
                    drpdownmsgduration6.DataBind();

                    drpdownmsgduration9.DataSource = minS;
                    drpdownmsgduration9.DataBind();
                    //FB 2486 Ends

                    lstCountries.Items.FindByValue("225").Selected = true;
                    txtApprover5.Attributes.Add("readonly", "");
                    txtEmailId.Attributes.Add("readonly", "");
                    Session["OnlyFlyRoom"] = null;
					if (Session["GuestRooms"].ToString() == "0")
                    {
                        trGuestLocation.Attributes.Add("style", "display:none");
                        trVideoGuestLocation.Attributes.Add("style", "display:none");
                        trPreviewGuestLocation.Attributes.Add("style", "display:none");
                    }
                    //FB 2426 End

                    txtVNOCOperator.Attributes.Add("readonly", ""); //FB 2501

                    if (lstTemplates.Items.Count == 0)//Added for Template List
                        obj.GetTemplateNames(lstTemplates);

                    for (int i = 0; i <= lstTemplates.Items.Count - 1; i++) //Disney
                        lstTemplates.Items[i].Attributes.Add("Title", lstTemplates.Items[i].Text);

                    /* *** Recurrence Fixes for edititng dirty instances - start *** */

                    Session.Remove("IsInstanceEdit");

                    /* *** Recurrence Fixes for edititng dirty instances - end *** */


                    Session["DS"] = new DataSet();
                    Session["CATMainGridDS"] = null;

                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        Session["HKMainGridDS"] = null;
                        Session["AVMainGridDS"] = null; // FB 2050
                    }
                    else
                    {
                        ViewState["HKMainGridDS"] = null;
                        ViewState["AVMainGridDS"] = null; // FB 2050
                    }


                    Wizard1.ActiveViewIndex = 0;
                    btnPrev.Visible = false;
                    TopMenu.Items[0].Selected = true;
                    //FB 2501 - Start
					//lstDuration.Text = "01:00";
                    if (Session["DefaultConfDuration"] != null)
                    {
                        int ConfDuration = Convert.ToInt32(Session["DefaultConfDuration"]);
                        int ConfHours = ConfDuration / 60;
                        String hrs = (ConfHours < 10) ? "0" + ConfHours.ToString() : ConfHours.ToString();
                        int ConfMinutes = ConfDuration % 60;
                        String Mins = (ConfMinutes < 10) ? "0" + ConfMinutes.ToString() : ConfMinutes.ToString();
                        lstDuration.Text = hrs + ":" + Mins;
                    }
                    //FB 2501 - End   
                   
                    if (Session["hasCalendar"] != null)
                        txtHasCalendar.Value = Session["hasCalendar"].ToString();
                   
                    chkPublic.Attributes.Add("onclick", "javascript:ChangePublic()");
                    ChkEnableNumericID.Attributes.Add("onclick", "javascript:ChangeNumeric()");//FB 2870
					//FB 2634
                    //confEndTime.Attributes.Add("onblur", "javascript:CheckDuration()");
                    //confStartDate.Attributes.Add("onblur", "javascript:ChangeEndDate(0)"); //FB 1715
                    //confEndDate.Attributes.Add("onblur", "javascript:ChangeStartDate()");
                    //confStartTime.Attributes.Add("onblur", "javascript:return document.getElementById('hdnChange').value='ST';formatTime('confStartTime_Text');ChangeEndDate(0);"); //FB 1715  FB 1716
                    //SetupDate.Attributes.Add("onblur", "javascript:ChangeEndDate(0)"); //Buffer Zone //FB 1715
                    //TearDownDate.Attributes.Add("onblur", "javascript:ChangeStartDate()"); //Buffer Zone
                    //confEndTime.Attributes.Add("onblur", "javascript:return document.getElementById('hdnChange').value='ET';formatTime('confEndTime_Text');ChangeStartDate(0);"); //FB 1715 FB 1716
                    ConferencePassword.Attributes.Add("onchange", "javascript:SavePassword()");
                    btnLotus.Attributes.Add("onclick", "javascript:getLotusEmailList(\"" + Session["lnLoginName"].ToString() + "\",\"" + Session["lnLoginPwd"].ToString() + "\",\"" + Session["lnDBPath"].ToString() + "\");return false;");
                    //openCalendar.OnClientClick = "goToCal('" + confStartDate.Text + "');return false;";
                    /* --- Added for Buffer Zone --Start ---- */
					//FB 2634
                    //SetupTime.Items.Clear();
                    //TeardownTime.Items.Clear();
                    //obj.BindTimeToListBox(SetupTime, true, true);
                    //obj.BindTimeToListBox(TeardownTime, true, true);
                    /* --- Added for Buffer Zone --End ---- */
                    String inXML = "";
                    String outXML = ""; // FB 1719
                    confStartTime.Items.Clear();
                    confEndTime.Items.Clear();
                    obj.BindTimeToListBox(confStartTime,true,true);
                    obj.BindTimeToListBox(confEndTime,true,true);
                    if(Session["timeFormat"] != null)
                        if (Session["timeFormat"].ToString().Equals("0"))
                        {
							//FB 2634
                            regEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            regConfStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            regConfStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            /* *** code added for buffer zone --start *** */
                            //regSetupStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            //regSetupStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            //regTearDownStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            //regTearDownStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            /* *** code added for buffer zone --end *** */
                        }
                        else if (Session["timeFormat"].ToString().Equals("2"))
                        {
                            regEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                            regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                            regConfStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                            regConfStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                        }
                    //Merging Recurrence
                    BindRecurrenceDefault();

                    //FB 2634
                    if (enableBufferZone == "1")
                    {
                        SetupDuration.Text = OrgSetupTime.ToString();
                        TearDownDuration.Text = OrgTearDownTime.ToString();
                    }
                    else
                    {
                        SetupDuration.Text = "0";
                        TearDownDuration.Text = "0";
                    }

                    //FB 2998
                    txtMCUConnect.Text = Session["McuSetupTime"].ToString();
                    txtMCUDisConnect.Text = Session["MCUTeardonwnTime"].ToString();
                    imgSetup.ToolTip = obj.GetTranslatedText("The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting.");
                    imgTear.ToolTip = obj.GetTranslatedText("The number of minutes after the conference has ended to prepare the room for the next meeting.");
                    imgMCUConnect.ToolTip = obj.GetTranslatedText("The number of minutes that the MCU is to connect the endpoints, prior to the arrival of the participants into the conference.");
                    imgMCUDisconnect.ToolTip = obj.GetTranslatedText("The number of minutes that the MCU is to disconnect the endpoints, prior to the departure of the participants from the conference.");

                    if(Request.QueryString["confid"] != null)
                    {
                        if (Request.QueryString["confid"].ToString() != "")
                        {
                            Session["confid"] = Request.QueryString["confid"].ToString().Trim();
                            Session["confTempID"] = Request.QueryString["confid"].ToString().Trim(); //FB 1765
                        }
                    }
                    
                    switch (Request.QueryString["t"].ToString())
                    {
                        case "n": // for new conference
                            {
                                Session["multisiloOrganizationID"] = null; //FB 2274 Session Issue
                                //FB 2501 starts
                                txtApprover7.Text = Session["userName"].ToString(); 
                                hdnApprover7.Text = Session["userId"].ToString();
                                hdnRequestorMail.Text = Session["userEmail"].ToString();
                                hdnApproverMail.Text = Session["userEmail"].ToString();
                                //FB 2501 ends
                                txtApprover4.Text = Session["userName"].ToString();
                                hdnApprover4.Text = Session["userId"].ToString();

                                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID></login>";//Organization Module Fixes
                                outXML = obj.CallMyVRMServer("GetNewConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                {
                                    BindDataNew();
                                }
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                break;
                            }
                        case "o": //if you want to clone a conference
                            {
                                Session["multisiloOrganizationID"] = null; //FB 2274 Session Issue
                                inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<selectType>1</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";//Organization Module Fixes
                                //Response.Write(obj.Transfer(inXML));
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataOld();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                break;
                            }
                        case "t": //if you want to create a conference from a template
                            {
                                //FB 1746 START
                                Session["multisiloOrganizationID"] = null; //FB 2274 Session Issue
                                int strConfId = 0;
                                int orgidchanged = 0;
                                if (Session["organizationID"].ToString() != myVRMNet.NETFunctions.defaultOrgID.ToString())
                                {
                                    if (Session["UsrCrossAccess"].ToString() == "1")
                                        orgidchanged = 1;
                                }
                                if (orgidchanged == 1 && Session["confid"] == null) //For Cross Org from Main Menu
                                {
                                    try
                                    {
                                        Response.Redirect("ConferenceSetup.aspx?t=n&op=1");
                                    }

                                    catch (System.Threading.ThreadAbortException)
                                    { }
                                }
                                if (Session["confTempID"] != null) //FB 1765
                                {
                                    Int32.TryParse(Session["confTempID"].ToString(), out strConfId);                                   
                                    Session["confTempID"] = "0";
                                }

                                if (strConfId <= 0)
                                {
                                    if (Session["defaultConfTemp"] != null)
                                    {
                                        Int32.TryParse(Session["defaultConfTemp"].ToString(), out strConfId);
                                        Session["confid"] = null; //FB 1746
                                    }
                                    if (orgidchanged == 1 || strConfId == 0) //FB 1746 //FB 1765
                                    {
                                        try
                                        {
                                            Response.Redirect("ConferenceSetup.aspx?t=n&op=1");
                                        }

                                        catch (System.Threading.ThreadAbortException)
                                        { }
                                    }
                                }
                                //FB 1746 END
                                CheckPCVendor(); //FB 2693
                                inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<selectType>2</selectType><selectID>" + strConfId.ToString() + "</selectID></login>";//Organization Module Fixes
                                //inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<selectType>2</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";//Organization Module Fixes //FB 1746
                                //Response.Write(obj.Transfer(inXML));
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataOld();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }

                                Session["confid"] = null; //FB 1746
                                //if (lstCalendar.Items[2] != null)//Code added for menu changes
                                //    lstCalendar.Items[2].Selected = true;

                                break;
                            }
                        case "": // for edit conference
                            {
                                inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<selectType>1</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";//Organization Module Fixes
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                                outXML = outXML.Replace("& ", "&amp; ");
                                Session.Add("outXML", outXML);
                                //NONRecurringConferenceDiv4.Attributes.Add("style", "display:none");
                                //Response.Write(obj.Transfer(outXML));
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataOld();
                                else
                                {
                                    //errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }

                                if (!(Session["admin"].ToString().Trim() == "2"))//FB 1825
                                {
                                    
                                  hdnSetStartNow.Value = "hide";
                                }
                                    break;
                            }
                    }
                    //FB 2274 Starts
                    CrossSilo();
                    if (roomModule.Equals("0")) 
                    {
                        TopMenu.Items[4].Text = String.Empty;
                    }
                    if (hkModule.Equals("0"))
                    {
                        TopMenu.Items[6].Text = String.Empty;
                    }
                    if (foodModule.Equals("0"))
                    {
                        TopMenu.Items[5].Text = String.Empty;
                    }
                    if (enableAdvAvParams == "0")
                        trAVCommonSettings.Attributes.Add("style", "display:none");

                    if (EnableRoomParam == "0")
                    {
                        trRooms.Attributes.Add("style", "display:none");
                        trRoomsDetails.Attributes.Add("style", "display:none");
                    }
                    if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//FB 2274
                    {
                        if (P2PEnable.Equals("0"))
                            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("4"));
                        if (EnableRoomConfType.Equals("0"))
                            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("7"));
                        if (EnableAudioVideoConfType.Equals("0"))
                            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("2"));
                        if (EnableAudioOnlyConfType.Equals("0"))
                            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("6"));
                    }
                    if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString() != "0")
                        btnCheckAvailDIV.Attributes.Add("style", "display:none");
                    //FB 2274 Ends
                    if (Request.QueryString["op"] != null)
                    {
                        if (Request.QueryString["op"].ToString().Equals("2"))
                        {
                            chkStartNow.Checked = true;
                        }

                    }
                    //Commented for FB 2396
                    //FB 1985 - Start
                    //if (client.ToUpper() == "DISNEY")
                    //{
                    if (lstConferenceType.SelectedValue == "2")
                    {
                        //lblAudioNote.Visible = true;
                        //lblAudioNote.Text = "Note: to add an audiobridge to your videoconference, 1) complete the information on this page, ";//FB 2023 start
                        //lblAudioNote.Text += "2) under the Select Participants tab, choose address book and select Domestic Audio Add-on (for example) ";
                        imgAudioNote.Visible = true;
                        imgAudioNote.ToolTip = obj.GetTranslatedText("Note: to add an audio bridge to your conference, 1) complete the information on this page, 2) under the Select Participants tab, ")
                                             + obj.GetTranslatedText("Choose Add Audio Bridge button and select the desired audio bridge");//FB 2396
                        imgAudioNote.ToolTip = obj.GetTranslatedText(imgAudioNote.ToolTip);
                        //lblParNote.Visible = true;
                        //lblParNote.Text = "Note: to complete request to add an audio bridge to your videoconference, choose address book and select Domestic";
                        //lblParNote.Text += " Audio Add-on (for example), providing Conference Code and Leader Pin on the Audio Settings tab";
                        imgParNote.Visible = true;
                        imgParNote.ToolTip = obj.GetTranslatedText("Note: to complete request to add an audio bridge to your conference, ")
                                           + obj.GetTranslatedText("choose Add Audio Bridge button and select the desired audio bridge");//FB 2396
                        imgParNote.ToolTip = obj.GetTranslatedText(imgParNote.ToolTip);
                    }
                    else
                    {
                        //lblAudioNote.Visible = false;
                        //lblParNote.Visible = false;
                        imgAudioNote.Visible = false;
                        imgParNote.Visible = false;
                    }
                    //}
                    //FB 1985 - End
                }
                else
                {
                    //Response.Write(Request.Params.Get("__EVENTTARGET").ToString().IndexOf("btnConfSubmit"));
                    if (Request.Params.Get("__EVENTTARGET").ToString().IndexOf("btnConfSubmit") >= 0)
                    {
                        if (flag == false)
                        {
                            //Response.Write(Request.Params.Get("__EVENTTARGET").ToString() + " : " + flag + "<br>");
                            SetConference(new Object(), new EventArgs());
                            flag = true;
                        }
                    }
                    //ZD 101052 Starts
                    //if (Request.Params.Get("__EVENTTARGET").ToString().IndexOf("btnUploadFiles1") >= 0)
                    if(hdnFileUpload.Value == "1")
                    {
                        UploadFiles(new Object(), new EventArgs());
                    }
                    //ZD 101052 End
                    //FB 2634	
                    //if ((RecurFlag.Value.Equals(1)) || (chkStartNow.Checked))
                    //{
                    //    //Response.Write("i if");
                    //    reqStartTime.Enabled = false;
                    //    regConfStartTime.Enabled = false;
                    //    reqEndTime.Enabled = false;
                    //    reqEndTime.Enabled = false;
                    //}
                    CheckPasswordFields();
                }
                //FB 2998
                mcuEnable = "0";
                if (hdnMCUConnectDisplay.Value != "")
                {
                    mcuSetupDisplay = hdnMCUConnectDisplay.Value;

                    if (mcuSetupDisplay == "0" && !IsPostBack)
                        txtMCUConnect.Text = hdnMCUSetupTime.Value;
                }
                else
                {
                    if (mcuSetupDisplay == "0" && !IsPostBack)
                        txtMCUConnect.Text = Session["McuSetupTime"].ToString();
                }

                if (txtMCUConnect.Text.Trim() != "")
                {
                    if (Convert.ToInt32(txtMCUConnect.Text.Trim()) > 0)
                        mcuEnable = "1";
                }

                if (hdnMCUDisconnectDisplay.Value != "")
                {
                    mcuTearDisplay = hdnMCUDisconnectDisplay.Value;
                    if (mcuTearDisplay == "0" && !IsPostBack)
                        txtMCUDisConnect.Text = hdnMCUTeardownTime.Value;
                }
                else
                {
                    if (mcuTearDisplay == "0" && !IsPostBack)
                        txtMCUDisConnect.Text = Session["MCUTeardonwnTime"].ToString();
                }

                if (txtMCUDisConnect.Text.Trim() != "")
                {
                    if (Convert.ToInt32(txtMCUDisConnect.Text.Trim()) > 0)
                        mcuEnable = "1";
                }

                if (isEditMode == "1" && mcuEnable == "1" && (mcuSetupDisplay == "1" || mcuTearDisplay == "1") && !IsPostBack)
                {
                    chkMCUConnect.Checked = true;
                    MCUConnectDisplayRow.Attributes.Add("style", "display:");
                }
                chkMCUConnect.Attributes.Add("onclick", "javascript:openMCUConnectRow('" + mcuSetupDisplay + "','" + mcuTearDisplay + "')");
                chkStartNow.Attributes.Add("onclick", "javascript:ChangeImmediate('S');DisplayMCUConnectRow('" + mcuSetupDisplay + "','" + mcuTearDisplay + "','" + enableBufferZone + "');"); //FB 1911 //FB 2998
                lstVMR.Attributes.Add("onchange", "javascript:changeVMR();fnShowHideAVforVMR();DisplayMCUConnectRow('" + mcuSetupDisplay + "','" + mcuTearDisplay + "','" + enableBufferZone + "');"); //FB 2998
                if ((mcuSetupDisplay == "1" || mcuTearDisplay == "1") && (lstConferenceType.SelectedValue == "2" || lstConferenceType.SelectedValue == "6")
                    && enableBufferZone == "1" && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                {
                    if (isEditMode == "1" && mcuEnable == "1")
                    {
                        chkMCUConnect.Checked = true;
                        MCUConnectDisplayRow.Attributes.Add("style", "Display:");
                    }
                    else
                        MCUConnectDisplayRow.Attributes.Add("style", "Display:None");

                    MCUConnectRow.Attributes.Add("style", "Display:");
                }
                else
                {
                    chkMCUConnect.Checked = false;
                    MCUConnectRow.Attributes.Add("style", "Display:None");// .Attributes.Add("style", "Display:none");
                    MCUConnectDisplayRow.Attributes.Add("style", "Display:None");
                }

                if (mcuSetupDisplay == "1" && mcuTearDisplay == "1")
                    lblMCUConnect.Text = "MCU Connect / Disconnect";
                else if (mcuSetupDisplay == "1")
                    lblMCUConnect.Text = "MCU Connect";
                else if (mcuTearDisplay == "1")
                    lblMCUConnect.Text = "MCU Disconnect";

                //FB 2755 Start
                if (lstConferenceType.SelectedValue.Equals("7") || NetworkSwitching != 2 || lstConferenceType.SelectedValue.Equals("8")) //Room Only //FB 2694 //FB 2993
                {
                    trSecure.Attributes.Add("Style", "display:none;");
                    trPreviewNetworkState.Attributes.Add("Style", "display:none;");
                    if (lstConferenceType.SelectedValue.Equals("8"))
                        CloudConfRow.Attributes.Add("style", "display:none");
                    drpNtwkClsfxtn.ClearSelection();
                    drpNtwkClsfxtn.SelectedValue = "0";//FB 2993
                }
                else
                {
                    trSecure.Attributes.Add("Style", "display:;");
                    trPreviewNetworkState.Attributes.Add("Style", "display:;");
                }
                //FB 2755 End
				//FB 2694
                if (lstConferenceType.SelectedValue.Equals("8")) //FB 2694
                {
                    trSecure.Visible = false;
                    trPreviewNetworkState.Visible = false;
                    trPCConf.Attributes.Add("Style", "display:none");
                    trVMR.Attributes.Add("Style", "display:none");
                    trStartMode.Attributes.Add("Style", "display:none");
                    drpNtwkClsfxtn.ClearSelection();
                    drpNtwkClsfxtn.SelectedValue = "0";//FB 2993
                }
                else
                {
                    trSecure.Visible = true;
                    trPreviewNetworkState.Visible = true;
                    trPCConf.Attributes.Add("Style", "display:");
                    //if (lstVMR.SelectedIndex > 0 && !lstConferenceType.SelectedValue.Equals("4")) //FB 2376
                    //    trVMR.Attributes.Add("Style", "display:");
                    //else
                    //    trVMR.Attributes.Add("Style", "display:none");
                }

                if(EnableIsVip != null)
                {
                    if (EnableIsVip.Equals("0") || lstConferenceType.SelectedValue.Equals("8")) //FB 2694 II
                        trVIP.Attributes.Add("Style", "display:none");
                    else
                        trVIP.Attributes.Add("Style", "display:");
                }

                if (EnableServiceType != null)
                {
                    if (EnableServiceType.Equals("0") || lstConferenceType.SelectedValue.Equals("8")) //FB 2694 II
                        trServType.Attributes.Add("Style", "display:none");
                    else
                        trServType.Attributes.Add("Style", "display:");
                }
                //FB 2599 Start
                if (isCloudEnabled == 1 && chkCloudConferencing.Checked)  //FB 2717
                {
                    lstVMR.SelectedValue = "3";
                    lstVMR.Enabled = false;
                    lstConferenceType.ClearSelection();
                    lstConferenceType.SelectedValue = "2";
                    trConfType.Attributes.Add("style", "display:none");//FB 2645
                    btnAudioparticipant.Visible = false;
                    trStartMode.Attributes.Add("style", "display:None");//FB 2641 
					trStartMode1.Attributes.Add("style", "display:none");//FB 2717
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "changeVMR", "changeVMR();", true);
                    lstVMR.Attributes.Add("onclick", "javascript:changeVMR()");
                    trVMR.Attributes.Add("style", "display:");
                    ShowHideAVforVMR(null, null);
                }
                else
                {
                    if (lstConferenceType.SelectedValue == "4" || lstConferenceType.SelectedValue == "7" || lstConferenceType.SelectedValue == "8") //FB 2717
                        trVMR.Attributes.Add("style", "display:none");
                    else
                        trVMR.Attributes.Add("style", "display:");
                    lstVMR.Enabled = true;
                    trConfType.Attributes.Add("style", "display:");
                    if (lstConferenceType.SelectedValue.Equals("4") || lstConferenceType.SelectedValue.Equals("7") || lstConferenceType.SelectedValue.Equals("8") || lstVMR.SelectedIndex > 0) // FB 2620
                    {
                        trStartMode.Attributes.Add("style", "display:None");//FB 2641 
                        trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                    }
                    else
                    {
                        //FB 2641 start
                        if (EnableStartMode == 1)
                        {
                            trStartMode.Attributes.Add("style", "display:");
                            trStartMode1.Attributes.Add("style", "display:");//FB 2717
                        }
                        else
                        {
                            trStartMode.Attributes.Add("style", "display:None");
                            trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                            //FB 2641 End
                        }
                    }
                    if (enableaudiobridge == "1")
                        btnAudioparticipant.Visible = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "changeVMR", "changeVMR();", true);
                    lstVMR.Attributes.Add("onclick", "javascript:changeVMR()");
                    ShowHideAVforVMR(null, null);
                }
                //FB 2599 End
                //FB 2486 Starts
                tdTxtMsg.Visible = false;
                tdTxtMsgDetails.Visible = false;
                if (hdnTxtMsg.Value.Equals("0"))
                {
                    tdTxtMsg.Visible = false;
                    tdTxtMsgDetails.Visible = false;
                }
                else
                {
                    if ((lstConferenceType.SelectedValue == "2") || (lstConferenceType.SelectedValue == "6"))
                    {
                        if (isVMR.Value == "0" || isVMR.Value == "")
                        {
                            tdTxtMsg.Visible = true;
                            tdTxtMsgDetails.Visible = true;
                        }
                    }
                }
                //FB 2486 Ends

               
                //Response.Write("<br>Load: " + DateTime.Now.ToString("hh:mm ss"));
                //Response.Write(dgUsers.Items.Count + " : " + dgRooms.Items.Count);
		        //FB 2443 - Start
		        enableaudiobridge = Session["EnableAudioBridges"].ToString(); 
                if (hdnCrossEnableAudioBridges != null && hdnCrossEnableAudioBridges.Value != "")
                    enableaudiobridge = hdnCrossEnableAudioBridges.Value;
                if (enableaudiobridge == "0" || (isCloudEnabled == 1 && chkCloudConferencing.Checked)) //FB 2262-S //FB 2599 //FB 2717
                {
                    btnAudioparticipant.Visible = false;
                    imgParNote.Visible = false;
                    traudiobrdige.Visible = false;                    
                }
                else
                {
                    btnAudioparticipant.Visible = true;
                    imgParNote.Visible = true;
                    traudiobrdige.Visible = true;
                }
		        //FB 2443 - End

                /* *** Custom Attribute Fixes - start *** */
                //FB 2274 Starts
                enableEntity = Session["EnableEntity"].ToString();
                if (hdnCrossEnableEntity != null && hdnCrossEnableEntity.Value != "") //FB 2274
                    enableEntity = hdnCrossEnableEntity.Value;

                //if (enableEntity == "1")
                //{
                    if (!isCOMError)
                    {
                        custControlIDs = "";
                        FillCustomAttributeTable();
                    }
                //}
                //else
                //{
                //    //foreach (MenuItem mi in TopMenu.Items)
                //    //    if (mi.Value == "7") //Text.ToLower().IndexOf("options") >= 0) //FB JAPAN
                //    //    {
                //    //        int item = Convert.ToInt32(mi.Value);
                //    //        TopMenu.Items[item].Text = "";

                //    //    }
                //    plblCustomOption.Text = ""; //FB 2547     
                //}
                /* *** Custom Attribute Fixes - end *** */

                HideRecurButton();  // Recurrence Fixes - Hide the recur icon on instance edit

                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

                startByTimeHK.Items.Clear();
                obj.BindTimeToListBox(startByTimeHK, true, true);
                if (Session["timeFormat"] != null)
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        RegularExpressionValidator2.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        RegularExpressionValidator2.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    }
                    else if (Session["timeFormat"].ToString().Equals("2"))
                    {
                        RegularExpressionValidator2.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        RegularExpressionValidator2.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    }

                //FB 1492 work order fixes - start
                completedByTimeHK.Items.Clear();
                obj.BindTimeToListBox(completedByTimeHK, true, true);
                if (Session["timeFormat"] != null)
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        RegularExpressionValidator18.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        RegularExpressionValidator18.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    }
                    else if (Session["timeFormat"].ToString().Equals("2"))
                    {
                        RegularExpressionValidator18.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        RegularExpressionValidator18.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    }

                //FB 1492 work order fixes - end

                startByTime.Items.Clear();
                obj.BindTimeToListBox(startByTime, true, true);
                if (Session["timeFormat"] != null)
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        RegularExpressionValidator14.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        RegularExpressionValidator14.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    }
                    else if (Session["timeFormat"].ToString().Equals("2"))
                    {
                        RegularExpressionValidator14.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        RegularExpressionValidator14.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    }
                completedByTime.Items.Clear();
                obj.BindTimeToListBox(completedByTime, true, true);
                if (Session["timeFormat"] != null)
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        regTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    }
                    else if (Session["timeFormat"].ToString().Equals("2"))
                    {
                        regTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        regTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    }
                /* **** Code added for AV switch fixes *** */
                enableAV = Session["enableAV"].ToString(); //FB 2274
                enablePar = Session["enableParticipants"].ToString();
				//if (enableAV.Equals("0") || chkVMR.Checked)//FB 2448
                if ((lstConferenceType.SelectedValue.Equals("7")) || (lstConferenceType.SelectedValue.Equals("4")) || (lstConferenceType.SelectedValue.Equals("8"))) //FB 3022
                        chkPCConf.Checked =false;
                if (enableAV.Equals("0") || lstVMR.SelectedIndex > 0 || chkPCConf.Checked)//FB 2448 FB 2620 //FB 2819
                    TopMenu.Items[3].Text = "";

                if (enablePar.Equals("0"))
                    TopMenu.Items[1].Text = "";
                /* **** Code added for AV switch fixes *** */

                
                if (hdnCrossEnablePublicConf != null && hdnCrossEnablePublicConf.Value != "") //FB 2272
                    EnablePublicConference = hdnCrossEnablePublicConf.Value;
                else if (Session["EnablePublicConf"] != null)
                    EnablePublicConference = Session["EnablePublicConf"].ToString();

                 if (hdnEnableNumericID != null && hdnEnableNumericID.Value != "") //FB 2870
                    EnableNumericID = hdnEnableNumericID.Value;
                 else if (Session["EnableNumericID"] != null)
                    EnableNumericID = Session["EnableNumericID"].ToString();
                //FB 2359 start

                 if (hdnEnableProfileSelection != null && hdnEnableProfileSelection.Value != "")//FB 2947
                     EnableProfileSelection = hdnEnableProfileSelection.Value;
                 else if (Session["EnableProfileSelection"] != null)
                     EnableProfileSelection = Session["EnableProfileSelection"].ToString();

                //FB 2446 - Start                
                if (hdnCrossdynInvite != null && hdnCrossdynInvite.Value != "")
                    Enableopenforregistration = hdnCrossdynInvite.Value;
                else if (Session["dynamicInviteEnabled"] != null)
                    Enableopenforregistration = Session["dynamicInviteEnabled"].ToString();
                if (lstConferenceType.SelectedValue.ToString() != ns_MyVRMNet.vrmConfType.HotDesking)//FB 2694
                {
                    if (hdnConferenceName.Value != "")//FB 2694
                    {
                        ConferenceName.Text = "";
                        hdnConferenceName.Value = "";
                        selectedloc.Value = ""; //FB 2823
                    }
                    if (EnablePublicConference != null && Enableopenforregistration != null)
                        if (EnablePublicConference == "1" && Enableopenforregistration == "0")
                        {
                            trPublic.Visible = true;
                            trPublicOpen.Visible = true;
                            tdpublic1.Visible = true;
                            tdpubopen.Visible = false;
                            plblPublic.Visible = true;
                            plblSlash.Visible = false;
                            plblOpenForRegistration.Visible = false;
                        }
                        else if (EnablePublicConference == "1" && Enableopenforregistration == "1")
                        {
                            trPublic.Visible = true;
                            trPublicOpen.Visible = true;
                            tdpublic1.Visible = false;
                            tdpubopen.Visible = true;
                        }
                        else if (EnablePublicConference == "0")
                        {
                            trPublic.Visible = false;
                            trPublicOpen.Visible = false;
                        }
                    //FB 2870 - Starts

                    if (EnableNumericID == "1")
                    {
                        TrCTNumericID.Visible = true;
                        // FB 2870 Start
                        trCTSNumericID.Attributes.Add("style", "display:");
                        if (txtNumeridID.Text == "")
                            lblCTSNumericID.Text = "N/A";
                        else
                            lblCTSNumericID.Text = obj.ControlConformityCheck(txtNumeridID.Text); // ZD 100263
                        // FB 2870 End
                    }
                    else
                    {
                        TrCTNumericID.Visible = false;
                        ChkEnableNumericID.Checked = false;
                        trCTSNumericID.Attributes.Add("style", "display:none");// FB 2870
                    }

                    HtmlGenericControl MyDiv = (HtmlGenericControl)Page.FindControl("DivNumeridID");

                    if (ChkEnableNumericID.Checked)
                        MyDiv.Attributes.Add("style","display:block");
                    else
                        MyDiv.Attributes.Add("style", "display:none");

                    //FB 2870 - End
                }
                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    trPublic.Visible = false;
                    trPublicOpen.Visible = false;
                    TrCTNumericID.Visible = false;//FB 2870
                }
                //FB 2359 end
                //FB 2446 - End

                //FB 2699
                TimeZoneText.Value = lstConferenceTZ.SelectedItem.Text;
                
                //FB 2889 - Start
                if (TopMenu.Items[0].Selected == true)
                {
                    btnPrev.Visible = false;
                    btnNext.Visible = true;
                }
                else
                {
                    btnNext.Visible = true;
                    btnPrev.Visible = true;
                }
                if (TopMenu.Items[8].Selected == true)
                {
                    btnPrev.Visible = false;
                    btnNext.Visible = false;
                }
                //FB 2889 - End


            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        //Merging Recurrence
        #region BindRecurrenceDefault

        private void BindRecurrenceDefault()
        {
            try
            {
              
                if (!IsPostBack)
                {
                    Int32 r = 0;
                    foreach (myVRMNet.NETFunctions.RecurringPattern myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.RecurringPattern)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272 - Starts
                        RecurType.Items.Insert(r++, li);
                    }

                    r = 0;
                    foreach (myVRMNet.NETFunctions.WeekDays myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.WeekDays)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        WeekDay.Items.Insert(r++, li);
                    }

                    //WeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.WeekDays));
                    //WeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DayCount myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DayCount)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        MonthWeekDayNo.Items.Insert(r++, li);
                    }
                    //MonthWeekDayNo.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DayCount));
                    //MonthWeekDayNo.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DaysType myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DaysType)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        MonthWeekDay.Items.Insert(r++, li);
                    }
                    //MonthWeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DaysType));
                    //MonthWeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DayCount myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DayCount)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonthWeekDayNo.Items.Insert(r++, li);
                    }
                    //YearMonthWeekDayNo.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DayCount));
                    //YearMonthWeekDayNo.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DaysType myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DaysType)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonthWeekDay.Items.Insert(r++, li);
                    }
                    //YearMonthWeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DaysType));
                    //YearMonthWeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.MonthNames myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.MonthNames)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonth1.Items.Insert(r++, li);
                    }
                    //YearMonth1.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                    //YearMonth1.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.MonthNames myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.MonthNames)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonth2.Items.Insert(r++, li);
                    }
                    //YearMonth2.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                    //YearMonth2.DataBind();
					//FB 2272 - End
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        
        #endregion

        private void BindDataNew()
        {
            XmlNode usrNode = null;//FB 2376 
            try
            {
                if (Session["systemDate"] == null) //Login Management
                {
                    Session.Add("systemDate", DateTime.Now.ToString("MM/dd/yyyy"));
                    Session.Add("systemTime", DateTime.Now.ToString(tformat));
                }
                //FB 2027
                //obj.GetSystemDateTime(Application["COM_ConfigPath"].ToString());// FB 1735
                obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());// FB 1735

                lblConfHeader.Text = obj.GetTranslatedText("New");//FB 1830 - Translation FB 2570
                CreateBy.Value = lstConferenceType.SelectedValue.ToString();
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                XmlNodeList nodes = node.SelectNodes("//conference/confInfo/timezones/timezone");
                lblConfID.Text = "new";
                int length = nodes.Count;
                int index = 0;
                lstConferenceTZ.Items.Clear();
                DrpServiceType.Items.Clear();//FB 2219
                DateTime startDateTime = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                if (Request.QueryString["sd"] != null)
                    startDateTime = DateTime.Parse(Request.QueryString["sd"].ToString() + " " + Request.QueryString["st"].ToString());
                else //fogbugz case 204
                {
                    if (startDateTime.Minute < 45)
                        startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                    else
                        startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);
                }
                //FB 2717 Starts
                lstVMR.ClearSelection();
                lstVMR.SelectedIndex = 0; //FB 2780
                //FB 2717 End

                //Code added by Offshore for FB Issue 1073 -- Start
                //FB 2501 starts
                if (Session["StartMode"].ToString() != null)
                    lstStartMode.SelectedValue = Session["StartMode"].ToString(); //FB 2501

                //FB 2501 Ends
                //FB 2634
                if(enableBufferZone == "1")
                    startDateTime = startDateTime.AddMinutes(Double.Parse(OrgSetupTime.ToString())); 

                confStartDate.Text = startDateTime.ToString(format);
                //Code added by Offshore for FB Issue 1073 -- End
                confStartTime.Text = startDateTime.ToString(tformat);
                //FB 2501 - Start
                DateTime endDateTime = DateTime.Now;
                String ConfDuration = Session["DefaultConfDuration"].ToString();
                if (ConfDuration != null)                                      
                    endDateTime = startDateTime.AddMinutes(Int32.Parse(ConfDuration));                 

                //DateTime endDateTime = startDateTime.AddMinutes(60);
                //if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ)) // Code added for MOJ Phase 2 QA Bug
                //    endDateTime = startDateTime.AddMinutes(Int32.Parse(15));
                //FB 2501 - End
                //Code added by Offshore for FB Issue 1073 -- Start
                
                confEndDate.Text = endDateTime.ToString(format);
                //Code added by Offshore for FB Issue 1073 -- End
                confEndTime.Text = endDateTime.ToString(tformat);
                confStartTime.SelectedValue = confStartTime.Text;
                confEndTime.SelectedValue = confEndTime.Text;
                CalculateDuration();
                /* **** Added for Buffer Zone **** --Start */
                //FB 2634
                //SetupDate.Text = startDateTime.ToString(format);
                //SetupTime.Text = startDateTime.ToString(tformat);
                //TearDownDate.Text = endDateTime.ToString(format);
                //TeardownTime.Text = endDateTime.ToString(tformat);
                //SetupTime.SelectedValue = confStartTime.Text;
                //TeardownTime.SelectedValue = confEndTime.Text;
                /* **** Added for Buffer Zone **** --End */

                /* Code Modified For FB 1453 - Start */
                //while (index < length)
                //{
                //    lstconferencetz.items.add(new listitem(nodes.item(index).selectsinglenode("timezonename").innertext, nodes.item(index).selectsinglenode("timezoneid").innertext));
                //    if (nodes.item(index).selectsinglenode("timezoneid").innertext.equals(node.selectsinglenode("/conference/confinfo/timezone").innertext))
                //        selIndex = index;
                //    index++;
                //}
                //lstConferenceTZ.SelectedIndex = selIndex; // node.SelectSingleNode("/conference/confInfo/timeZone").InnerText;

                String selTZ = "-1";
                lstConferenceTZ.ClearSelection();
                obj.GetTimezones(lstConferenceTZ, ref selTZ);
                if (node.SelectSingleNode("/conference/confInfo/timeZone") != null)
                    lstConferenceTZ.Items.FindByValue(node.SelectSingleNode("/conference/confInfo/timeZone").InnerText).Selected = true;
                
                String selST = "-1";//FB 2219
                DrpServiceType.ClearSelection();
                obj.BindServiceType(DrpServiceType);
                if (node.SelectSingleNode("/conference/confInfo/ServiceType") != null)
                    DrpServiceType.Items.FindByValue(node.SelectSingleNode("/conference/confInfo/ServiceType").InnerText).Selected = true;

                /* Code Modified For FB 1453 - End */
                txtModifyType.Text = "0";
                LoadCommonValues(node);
                if (!Request.QueryString["t"].ToString().Trim().Equals(""))
                    txtCA2.Text = Session["userEmail"].ToString();

                /** FB 2376 **/
                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/internalBridge");
                if (usrNode != null)
                {
                    txtintbridge.Text = usrNode.InnerText;
                    hdnintbridge.Value = usrNode.InnerText;
                }

                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/externalBridge");
                if (usrNode != null)
                {
                    txtextbridge.Text = usrNode.InnerText;
                    hdnextbridge.Value = usrNode.InnerText;
                }
                /** FB 2376 **/

                CheckPCVendor(); //FB 2693 Starts                               

                nodes = node.SelectNodes("//conference/confInfo/CustomAttributesList/CustomAttribute");
                if (nodes.Count > 0)
                {
                    if (client.ToString().ToUpper().Equals("LHRIC"))
                        GetCustomAttributesLHRIC(nodes);
                }

                isToogle = false;
                nodes = node.SelectNodes("//conference/confInfo/ConfMessageList/ConfMessage"); //FB 2486
                if (nodes.Count > 0)
                    GetConfMessages(nodes);

                //FB 2506 Starts
                if (isToogle)
                {
                    toggleText.Attributes.Add("Style", "Display:block");
                    displayText.InnerText = obj.GetTranslatedText("Less");
                }
                else
                {
                    toggleText.Attributes.Add("Style", "Display:none");
                    displayText.InnerText = obj.GetTranslatedText("More");
                }
                //FB 2506 Ends
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindDataNew: " + ex.StackTrace);
            }
        }
        public void LoadCommonValues(XmlNode node)
        {
            CrossSilo(); //FB 2274
            XmlNodeList nodes = node.SelectNodes("/conference/confInfo/groups/group");
            Group.Items.Clear();
            usersstr = "";
            for (int i = 0; i < nodes.Count; i++)
            {
                Group.Items.Add(new ListItem(nodes[i].SelectSingleNode("groupName").InnerText, nodes[i].SelectSingleNode("groupID").InnerText));
                //usersstr = usersstr + "|";
                XmlNodeList subnotes = nodes[i].SelectNodes("user");
                int sublength = subnotes.Count;
                for (int j = 0; j < sublength; j++)
                {
                    string userID = subnotes[j].SelectSingleNode("userID").InnerText;
                    string userFirstName = subnotes[j].SelectSingleNode("userFirstName").InnerText;
                    string userLastName = subnotes[j].SelectSingleNode("userLastName").InnerText;
                    string userEmail = subnotes[j].SelectSingleNode("userEmail").InnerText;
                    //13,Intercall,Audio,linda.athmer@1dsney.com, ex-1,rm-0,cc-0, noti-1, video-1, audio-0, 0,0,,0,0,0,;
                    //usersstr += userID + "," + userFirstName + "," + userLastName + "," + userEmail + ",0,1,0,1,0,1,,,3,,,;";
                    usersstr += userID + "!!" + userFirstName + "!!" + userLastName + " !!" + userEmail + "!!0!!1!!0!!1!!0!!1!!!!!!3!!!!!!0!!0!!0!!0!!0||"; //FB 1888 //FB 2550 //FB 2458
                }
                //usersstr += "|";
                usersstr += "``"; //FB 1888
            }
            if (usersstr.Length > 0)
                txtUsersStr.Text = usersstr.Substring(0, usersstr.Length - 1);
            //Response.Write(usersstr);
            lstRoomSelection.Items.Clear();
            treeRoomSelection.Nodes.Clear();
            TreeNode tn = new TreeNode("All");
            tn.Expanded = true;
            treeRoomSelection.Nodes.Add(tn);
            nodes = node.SelectNodes("/conference/confInfo/locationList");
            PreSelectRooms(nodes);
            nodes = node.SelectNodes("/conference/confInfo/locationList/level3List/level3");
            GetLocationList(nodes);

            //Code added for Search Room Error - start
            if (nodes.Count == 0)
            {
                rdSelView.Enabled = false;
                pnlListView.Visible = false;
                pnlLevelView.Visible = false;
                btnCompare.Disabled = true;
                pnlNoData.Visible = true;
                //openCalendar.Enabled = false;
                GetAvailableRoom.Enabled = false;
                //MeetingPlanner.Enabled = false;
            }
            //Code added for Search Room Error - end

            Group.Attributes.Add("onchange", "JavaScript:groupChgNET();");
            Group.Attributes.Add("onDblClick", "JavaScript: getAGroupDetail(1, this, null)");
            if (!lblConfID.Text.ToLower().Equals("new"))
            {
                if (!Request.QueryString["t"].ToString().ToLower().Equals("t") && roomModule.Equals("1"))// && !Session["admin"].ToString().Equals("0")) //FB 2274 //ZD 100263
                {
                    GetAVWorkOrders(); //organization module
                }
                if (!Request.QueryString["t"].ToString().ToLower().Equals("t") && hkModule.Equals("1"))// && !Session["admin"].ToString().Equals("0")) //FB 2274
                {
                    GetHKWorkOrders(); //organization module
                }
                if (!Request.QueryString["t"].ToString().ToLower().Equals("t") && foodModule.Equals("1"))// && !Session["admin"].ToString().Equals("0"))
                {
                    GetCATWorkOrders();
                }
            }
            CheckPasswordFields();
        }

        public void GetAudioVideoSettings(string nodes)
        {
            try
            {
                log.Trace("In GetAudioVideoSettings");
                //<maxAudioPart>0</maxAudioPart><maxVideoPart>0</maxVideoPart><restrictProtocol>0</restrictProtocol><restrictAV>0</restrictAV><videoLayout>1</videoLayout><maxLineRateID>0</maxLineRateID><audioCodec>0</audioCodec><videoCodec>0</videoCodec><dualStream>0</dualStream><confOnPort>0</confOnPort><encryption>0</encryption><lectureMode>0</lectureMode>
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(nodes);
                //Response.Write("<br>" + obj.Transfer(xmldoc.InnerXml));
                txtMaxAudioPorts.Text = xmldoc.SelectSingleNode("//advAVParam/maxAudioPart").InnerText;
                //Response.Write("1");
                txtMaxVideoPorts.Text = xmldoc.SelectSingleNode("//advAVParam/maxVideoPart").InnerText;
                lstRestrictNWAccess.ClearSelection();
                String txtTemp = "-1";
                txtTemp = xmldoc.SelectSingleNode("//advAVParam/restrictProtocol").InnerText.Trim();
                if (txtTemp.Equals("") || txtTemp.Equals("0"))
                //FB 1985 - Start
                {
                    if (client.ToUpper() == "DISNEY")
                        txtTemp = "3";
                    else
                        txtTemp = "1";
                }
                //FB 1985 - End
                lstRestrictNWAccess.Items.FindByValue(txtTemp).Selected = true;

                lstRestrictUsage.ClearSelection();
                txtTemp = xmldoc.SelectSingleNode("//advAVParam/restrictAV").InnerText.Trim();
                if (txtTemp.Equals("") || txtTemp.Equals("0"))
                    txtTemp = "1";
                lstRestrictUsage.Items.FindByValue(txtTemp).Selected = true;
                //                imgVideoDisplay.ImageUrl = ImagesPath.Text + Int32.Parse(xmldoc.SelectSingleNode("//advAVParam/videoLayout").InnerText).ToString("00") + ".gif";
                txtTemp = xmldoc.SelectSingleNode("//advAVParam/videoLayout").InnerText;
                if (txtTemp.Trim().Equals(""))
                    txtTemp = "01";
                txtSelectedImage.Text = Int32.Parse(txtTemp).ToString("00");
                //Response.Write(txtSelectedImage.Text);

                lstLineRate.ClearSelection();
                txtTemp = xmldoc.SelectSingleNode("//advAVParam/maxLineRateID").InnerText.Trim();
                if (txtTemp.Equals("") || txtTemp.Equals("0"))//FB 1985 - Start
                {
                    //FB 2429
                    txtTemp = OrgLineRate.ToString();
                    if (OrgLineRate <= 0)
                        txtTemp = "384";

                    /*if (client.ToUpper() == "DISNEY")
                        txtTemp = "2048";
                    else
                        txtTemp = "384";*/
                }
                //FB 1985 - End
                lstLineRate.Items.FindByValue(txtTemp).Selected = true;

                DrpDwnLstRate.ClearSelection();//Code added for disney
                if (DrpDwnLstRate.Items.FindByValue(txtTemp) != null)//Code added for disney
                    DrpDwnLstRate.Items.FindByValue(txtTemp).Selected = true;

                lstAudioCodecs.ClearSelection();
                txtTemp = xmldoc.SelectSingleNode("//advAVParam/audioCodec").InnerText.Trim();
                if (txtTemp.Equals("") || txtTemp.Equals("0"))
                    txtTemp = "0";
                lstAudioCodecs.Items.FindByValue(txtTemp).Selected = true;

                lstVideoCodecs.ClearSelection();
                txtTemp = xmldoc.SelectSingleNode("//advAVParam/videoCodec").InnerText.Trim();
                if (txtTemp.Equals("") || txtTemp.Equals("0"))
                    txtTemp = "0";
                lstVideoCodecs.Items.FindByValue(txtTemp).Selected = true;

                if (xmldoc.SelectSingleNode("//advAVParam/dualStream").InnerText.Equals("1"))
                    chkDualStreamMode.Checked = true;
                else
                    chkDualStreamMode.Checked = false;

                if (xmldoc.SelectSingleNode("//advAVParam/confOnPort").InnerText.Equals("1")) //FB 1721
                {
                    chkConfOnPort.Checked = true;
                    chkPolycomSpecific.Checked = true;
                }
                else
                {
                    chkConfOnPort.Checked = false;
                }

                if (xmldoc.SelectSingleNode("//advAVParam/encryption").InnerText.Equals("1"))
                    chkEncryption.Checked = true;
                else
                    chkEncryption.Checked = false;

                if (xmldoc.SelectSingleNode("//advAVParam/lectureMode").InnerText.Equals("1"))//FB 1721
                {
                    chkPolycomSpecific.Checked = true;
                    chkLectureMode.Checked = true;
                }
                else
                {
                    chkLectureMode.Checked = false;
                }

                lstVideoMode.ClearSelection();
                txtTemp = xmldoc.SelectSingleNode("//advAVParam/VideoMode").InnerText;
                if (txtTemp.Equals("") || txtTemp.Equals("0"))
                    txtTemp = "-1";
                lstVideoMode.Items.FindByValue(txtTemp).Selected = true;
                if (xmldoc.SelectSingleNode("//advAVParam/SingleDialin").InnerText.Equals("1"))
                    chkSingleDialin.Checked = true;
                else
                    chkSingleDialin.Checked = false;

                /** FB 2376 **/
                if (xmldoc.SelectSingleNode("//advAVParam/internalBridge") != null)
                {
                    txtintbridge.Text = xmldoc.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                    hdnintbridge.Value = xmldoc.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                }

                if (xmldoc.SelectSingleNode("//advAVParam/externalBridge") != null)
                {
                    txtextbridge.Text = xmldoc.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                    hdnextbridge.Value = xmldoc.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                }

                /** FB 2376 **/

                // FB 2501 FECC Starts
                if (xmldoc.SelectSingleNode("//advAVParam/FECCMode").InnerText.Equals("1"))
                    chkFECC.Checked = true;
                else
                    chkFECC.Checked = false;
                // FB 2501 FECC Ends

                //FB 2441 Starts //FB 2697 //Comented for ZD 100298
                //if (xmldoc.SelectSingleNode("//advAVParam/PolycomTemplate") != null)
                //{
                //    txt_polycomTemplate.Text = xmldoc.SelectSingleNode("//advAVParam/PolycomTemplate").InnerText;
                //    if (txt_polycomTemplate.Text != "")
                //        chkPolycomSpecific.Checked = true;

                //}

                if (xmldoc.SelectSingleNode("//advAVParam/PolycomSendEmail").InnerText.Equals("1"))
                {
                    chkSendMail.Checked = true;
                    chkPolycomSpecific.Checked = true;
                }
                else
                    chkSendMail.Checked = false;
                //FB 2441 Ends

                Session["AdvAVParam"] = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }

        //Custom Attribute Fixes    -- Start
        #region Build Custom Attributes 

        public void GetCustomAttributes(XmlNodeList nodes)
        {
            //code added for Add FB 1470 - start
            string eID = "";
            string eCode = "";
            string eDesc = "";
            string desc = "";
            try
            {

                //if (Application["Client"].ToString().ToUpper().Equals("NGC"))
                //{
                    //this.FreeTextBox1.Text = nodes[1].SelectSingleNode("OptionList/Option/DisplayValue").InnerText.ToString();

                    if (nodes[0].SelectSingleNode("Type").InnerText.Equals("6"))
                    {
                        DropDownList lstTemp = (DropDownList)tblEntityCode.FindControl("lstEntityCode");
                        lstTemp.Items.Add(new ListItem(obj.GetTranslatedText("Please select..."), "-1"));
                        foreach (XmlNode node in nodes[0].SelectNodes("OptionList/Option"))
                        {
                            eID = node.SelectSingleNode("OptionID").InnerText;
                            eCode = node.SelectSingleNode("DisplayCaption").InnerText;
                            eDesc = this.GetEntityDescription(eID, eCode);

                            desc = eCode;
                            if (eDesc != "")
                                desc += " - " + eDesc;

                            ListItem li = new ListItem(desc, node.SelectSingleNode("OptionID").InnerText);
                            lstTemp.Items.Add(li);
                            if (node.SelectSingleNode("Selected").InnerText.Equals("1"))    //Custom Attributes Fixes
                            {
                                lstTemp.ClearSelection();
                                li.Selected = true;

                            }

                        }
                    }
                //}

                //if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.NGC))
                //    txtCANGC1.Text = nodes[0].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;

                    //code added for Add FB 1470 - end

                    if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl))
                {
                    txtCA1.Text = nodes[0].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA2.Text = nodes[1].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA3.Text = nodes[2].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA4.Text = nodes[3].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA5.Text = nodes[4].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA6.Text = nodes[5].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA7.Text = nodes[6].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA8.Text = nodes[7].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA9.Text = nodes[8].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    txtCA10.Text = nodes[9].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    if (nodes[10].SelectSingleNode("Type").InnerText.Equals("2"))
                    {
                        if (nodes[10].SelectSingleNode("OptionList/Option/Selected").InnerText.Equals("1"))
                            chkCA1.Checked = true;
                    }
                    if (!Request.QueryString["t"].ToString().Trim().Equals(""))
                        txtCA2.Text = Session["userEmail"].ToString();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
        #endregion

        //Custom Attribute Fixes -- End
        //13,Intercall,Audio,linda.athmer@1dsney.com, ex-1,rm-0,cc-0, noti-1, video-1, audio-0, 0,0,,0,0,0,;
        private void getParty(XmlNodeList nodes, bool isClone)//FB 2550
        {
            string vmrParty = "0";//FB 2376
            string pcParty = "0";//FB 2376
            string address = "0";//FB 2376
            int pLength = nodes.Count;
            partysInfo = "";
            string confPartys = ""; //FB 1830 Email Edit
            for (int i = 0; i < pLength; i++)
            {
                //FB 2550
                if (isClone && nodes[i].SelectSingleNode("partyPublicVMR").InnerText.Equals("1"))
                    continue;

                vmrParty = "0"; //FB 2376
                pcParty = "0";//FB 2348

                //FB 1830 Email Edit - start
                if (confPartys == "")
                    confPartys = nodes[i].SelectSingleNode("partyID").InnerText.Trim();
                else
                    confPartys += "!!" + nodes[i].SelectSingleNode("partyID").InnerText.Trim(); //FB 1888 start
                //FB 1830 Email Edit - end

                if (nodes[i].SelectSingleNode("partyAddress").InnerText.Trim() != "") //FB 2376
                    address = nodes[i].SelectSingleNode("partyAddress").InnerText.Trim();
                    

                partysInfo += nodes[i].SelectSingleNode("partyID").InnerText + "!!";
                partysInfo += nodes[i].SelectSingleNode("partyFirstName").InnerText + "!!"; //FB 1640 .Replace(",", " ") 

                if (nodes[i].SelectSingleNode("partyLastName").InnerText.Trim() == "")//FB 2023
                    partysInfo += " !!"; //FB 2388
                else
                    partysInfo += nodes[i].SelectSingleNode("partyLastName").InnerText + "!!";

                partysInfo += nodes[i].SelectSingleNode("partyEmail").InnerText + "!!";
                //Response.Write("<br>" + nodes[i].SelectSingleNode("partyFirstName").InnerText + ", " + nodes[i].SelectSingleNode("partyInvite").InnerText);
                if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("0")) //CC
                    partysInfo += "0!!0!!1!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("1")) //External Attendee
                    partysInfo += "1!!0!!0!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("2")) //Room attendee
                    partysInfo += "0!!1!!0!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("4")) //VMR 2376
                {
                    vmrParty = "1";
					//if (chkVMR.Checked == true)//FB 2347T //FB 2448-EnableSurvey == "1" && 
                    if (lstVMR.SelectedIndex > 0)//FB 2347T //FB 2448-EnableSurvey == "1" &&  // FB 2620
                        partysInfo += "0!!0!!0!!";
                    else 
                    {
                        vmrParty = "0";
                        partysInfo += "0!!1!!0!!";
                    }
                }
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("3"))
                {
                    pcParty = "1";
                    partysInfo += "0!!0!!0!!";
 
                }
                if (nodes[i].SelectSingleNode("partyNotify").InnerText.Equals("1"))
                    partysInfo += "1!!";
                else
                    partysInfo += "0!!";
                //Code changed for aduio addon
                if (nodes[i].SelectSingleNode("partyAudVid").InnerText.Equals("2")) //AudioVideo //Code changed for FB 1744
                    partysInfo += "1!!0!!";
                else if (nodes[i].SelectSingleNode("partyAudVid").InnerText.Equals("1")) //Audio only //Code changed for FB 1744
                    partysInfo += "0!!1!!";
                else
                    partysInfo += "0!!0!!"; //None
                //Code changed for aduio addon
                if (nodes[i].SelectNodes("partyAddressType").Count > 0)
                {
                    partysInfo += nodes[i].SelectSingleNode("partyProtocol").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyConnectionType").InnerText + "!!";
                    partysInfo += address + "!!";//FB 2376
                    partysInfo += nodes[i].SelectSingleNode("partyAddressType").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyIsOutside").InnerText + "!!";
                }
                else
                    partysInfo += "!!0!!-3!!0!!0!!0!!"; //FB 1888 end  2376
                //Response.Write(partysInfo);
                if (nodes[i].SelectSingleNode("Survey").InnerText.Equals("1"))
                    partysInfo += "0!!1!!";
                else
                    partysInfo += "0!!0!!";
                
                partysInfo += pcParty + "!!" + vmrParty;
                //FB 2550 Starts
                if (nodes[i].SelectSingleNode("partyPublicVMR").InnerText.Equals("1"))
                    partysInfo += "!!1||";
                else
                    partysInfo += "!!0||";
                //FB 2550 Ends

                txtPartysInfo.Text = partysInfo;
            }
            if (!xConfInfo.ContainsKey("partys"))   //FB 1830 Email Edit
                xConfInfo.Add("partys", confPartys.Trim());
        }
        private void BindDataOld()
        {
            try
            {
                xConfInfo = new StringDictionary(); //FB 1830 Email Edit
                //myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
                lblConfHeader.Text = obj.GetTranslatedText("Edit");//FB 1830 - Traslation
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                string recurring = "";
                XmlNodeList ConfVNOCnodes; //FB 2670
                // buffer zone -- Start
                DateTime setupStartDateTime = DateTime.MinValue;
                DateTime teardownStartDateTime = DateTime.MinValue;
                Double setupDuration = Double.MinValue;
                Double teardownDuration = Double.MinValue;
                string setupDur = "0";
                string tearDownDur = "0";
                // buffer zone End
                //string[] tmpstrs;//FB 2341//FB 2377
                //string tmpstr = "";
                //int i, tmpint;

                
                //Response.Write(obj.Transfer(Session["outxml"].ToString()));
                //Response.Write(Request.QueryString["t"].ToString());

                if (!Request.QueryString["t"].ToString().Equals("t"))
                {
                    recurring = node.SelectSingleNode("//conference/confInfo/recurring").InnerText;
                    lblConfID.Text = node.SelectSingleNode("//conference/confInfo/confID").InnerText;
                }

                //2457 exchange round trip starts
                conferenceOrigin = node.SelectSingleNode("//conference/confInfo/confOrigin").InnerText;
                hdnconfOriginID.Value = conferenceOrigin;
                hdnicalID.Value = node.SelectSingleNode("//conference/confInfo/IcalID").InnerText;
                //2457 exchange round trip ends

                //FB 2659 - Starts
                if (node.SelectSingleNode("//conference/confInfo/confUniqueID") != null)
                    hdnconfUniqueID.Value = node.SelectSingleNode("//conference/confInfo/confUniqueID").InnerText.Trim();
                //FB 2659 - End

                //FB 2550 - Starts
                bool isClone = false;
                if (Request.QueryString["t"].ToString().Equals("o"))
                {
                    isClone = true;
                }
                //FB 2550 - End

                //FB 2274 Starts
                string multiOrgID = Session["organizationID"].ToString();
                if (node.SelectSingleNode("/conference/confInfo/ConfOrgID") != null)
                {
                    if (multiOrgID != node.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim())
                    {
                        if (Session["multisiloOrganizationID"] == null)
                        {
                            Session.Add("multisiloOrganizationID", node.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim());
                        }
                        else
                            Session["multisiloOrganizationID"] = node.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim();
                        hdnCrossAddtoGroup.Value = Session["multisiloOrganizationID"].ToString();
                    }
                    else
                        Session["multisiloOrganizationID"] = null;
            	}

                btnGuestLocation.Enabled = true;//FB 2426
                if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString() != "0")
                {
                    if (Session["multisiloOrganizationID"].ToString() != Session["organizationID"].ToString())
                    {
                        lstTemplates.Enabled = false;
                        btnGuestLocation.Enabled = false; //FB 2426
                    }
                }
                //FB 2274 Ends
                //Recurrence Fixes - Edit single instance - start
                if (!Request.QueryString["t"].ToString().Equals("t") && !Request.QueryString["t"].ToString().Equals("o"))
                {
                    if (node.SelectSingleNode("//conference/isinstanceedit") != null)
                    {

                        if (node.SelectSingleNode("//conference/isinstanceedit").InnerText == "1")
                        {
                            isInstanceEdit = "Y";

                            Session.Remove("IsInstanceEdit");
                            Session.Add("IsInstanceEdit", isInstanceEdit);
                        }
                    }
                }
                
                //Recurrence Fixes - Edit single instance  - end

                if (Request.QueryString["t"].ToString() == "")//FB 1716
                    isEditMode = "1";

                //Recurrence Fixes - Edit With Some instances in past (FB 1131) - start
                if (recurring.Equals("1")) //in case of editing a recurring conference
                {
                    XmlNode usrnode = xmlDOC.SelectSingleNode("conference/userInfo");

                    if (usrnode != null)
                        usrnode.InnerXml += "<userId>" + Session["userID"].ToString() + "</userId>";

                    string recOutxml = obj.CallMyVRMServer("GetIfDirtyorPast", xmlDOC.InnerXml, Application["MyVRMServer_ConfigPath"].ToString());

                    if (recOutxml != "")
                    {
                        if (recOutxml.IndexOf("<error>") < 0)
                        {
                            xmlDOC.LoadXml(recOutxml);
                            node = null;
                            node = (XmlNode)xmlDOC.DocumentElement;
                        }
                    }
                }
                //Recurrence Fixes - Edit With Some instances in past (FB 1131) - end

                ConferenceName.Text = node.SelectSingleNode("//conference/confInfo/confName").InnerText;
                //FB 1470
                ConferenceDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters(node.SelectSingleNode("//conference/confInfo/description").InnerText.Replace("N/A", ""),1); //FB 2236 
                //FB 1830 Email Edit - start
                xconfpassword = node.SelectSingleNode("//conference/confInfo/confPassword").InnerText.Trim();

                //FB 2377 - Starts
                /*hdnConceirgeSupp.Value = node.SelectSingleNode("//conference/confInfo/ConceirgeSupport").InnerText; //FB 2023
                tmpstrs = ((tmpstr = node.SelectSingleNode("//conference/confInfo/ConceirgeSupport").InnerText)).Split(',');//FB 2341
                tmpint = ((tmpstr.Trim()).Equals("")) ? -1 : tmpstrs.Length;
                for (i = 0; i < tmpint; i++)
                    ChklstConcSupport.Items[Convert.ToInt16(tmpstrs[i]) - 1].Selected = true;*/
                //FB 2377 - End

                if (!xConfInfo.ContainsKey("password")) //FB 1830 Email Edit - start
                    xConfInfo.Add("password", xconfpassword);

                ConferencePassword.Attributes.Add("value", xconfpassword);
                ConferencePassword2.Attributes.Add("value", xconfpassword);
                //FB 1830 Email Edit - end

                // FB 1864
                if (node.SelectSingleNode("//conference/confInfo/isVIP") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/isVIP").InnerText.Equals("1"))
                        chkisVIP.Checked = true;
                    else
                        chkisVIP.Checked = false;
                }
                if (node.SelectSingleNode("//conference/confInfo/isDedicatedEngineer") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/isDedicatedEngineer").InnerText.Equals("1"))
                        chkisDedicatedEngineer.Checked = true;
                    else
                        chkisDedicatedEngineer.Checked = false;
                }
                if (node.SelectSingleNode("//conference/confInfo/isLiveAssitant") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/isLiveAssitant").InnerText.Equals("1"))
                        chkisLiveAssitant.Checked = true;
                    else
                        chkisLiveAssitant.Checked = false;
                }

                // FB 1864
                //FB 2632 Starts
                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/OnSiteAVSupport") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/OnSiteAVSupport").InnerText.Equals("1"))
                        chkOnSiteAVSupport.Checked = true;
                    else
                        chkOnSiteAVSupport.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/MeetandGreet") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/MeetandGreet").InnerText.Equals("1"))
                        chkMeetandGreet.Checked = true;
                    else
                        chkMeetandGreet.Checked = false;
                }
                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/ConciergeMonitoring") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/ConciergeMonitoring").InnerText.Equals("1"))
                        chkConciergeMonitoring.Checked = true;
                    else
                        chkConciergeMonitoring.Checked = false;
                }
                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator").InnerText.Equals("1"))
                        chkDedicatedVNOCOperator.Checked = true;
                    else
                        chkDedicatedVNOCOperator.Checked = false;
                }
                //FB 2632 Ends

                //FB 2670 Starts
                //FB 2501 Starts
                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator").InnerText.Equals("1"))
                    {

                        ConfVNOCnodes = node.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperatorID");
                        if (ConfVNOCnodes.Count > 0)
                        {
                            hdnVNOCOperator.Text = "";
                            for (int i = 0; i < ConfVNOCnodes.Count; i++)
                            {
                                if (ConfVNOCnodes[i].InnerText != null)
                                {
                                    if (hdnVNOCOperator.Text == "")
                                        hdnVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                                    else
                                        hdnVNOCOperator.Text += "," + ConfVNOCnodes[i].InnerText.Trim();
                                }
                            }
                        }
                    }
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator").InnerText.Equals("1"))
                    {

                        ConfVNOCnodes = null;
                        ConfVNOCnodes = node.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperator");
                        if (ConfVNOCnodes.Count > 0)
                        {
                            txtVNOCOperator.Text = "";
                            for (int i = 0; i < ConfVNOCnodes.Count; i++)
                            {
                                {
                                    if (ConfVNOCnodes[i].InnerText != null)
                                    {
                                        if (hdnVNOCOperator.Text == "")
                                            txtVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                                        else
                                            txtVNOCOperator.Text += "\n" + ConfVNOCnodes[i].InnerText.Trim();
                                    }
                                }
                            }
                        }
                    }
                }
                if (txtVNOCOperator.Text != "")
                    chkDedicatedVNOCOperator.Checked = true;
                //FB 2501 Ends
                //FB 2670 Ends
                
                //FB 2608 
                if (isClone && EnableVNOCselection == 0)
                {
                    txtVNOCOperator.Text = "";
                }

                
                // FB 1926
                if (node.SelectSingleNode("//conference/confInfo/isReminder") != null)
                {
                    chkReminder.Checked = true;
                    if (node.SelectSingleNode("//conference/confInfo/isReminder").InnerText.Equals("0"))
                        chkReminder.Checked = false;
                }
                
                //FB 2620
                lstVMR.ClearSelection();
                if (node.SelectSingleNode("//conference/confInfo/isVMR") != null)
                    if (lstVMR.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/isVMR").InnerText) != null)
                        lstVMR.SelectedValue = node.SelectSingleNode("//conference/confInfo/isVMR").InnerText;
                
                //FB 2717 Start

                if (node.SelectSingleNode("//conference/confInfo/CloudConferencing") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/CloudConferencing").InnerText.Equals("1")) //FB 2448
                    {
                        chkCloudConferencing.Checked = true;
                    }
                    else
                        chkCloudConferencing.Checked = false;
                }
                //FB 2717 End

                // FB 2693 Starts
                if (node.SelectSingleNode("//conference/confInfo/isPCconference") != null)
                {
                    chkPCConf.Checked = true;
                    if (node.SelectSingleNode("//conference/confInfo/isPCconference").InnerText.Equals("0"))
                    {
                        chkPCConf.Checked = false;
                        CheckPCVendor();
                    }

                    if (node.SelectSingleNode("//conference/confInfo/isPCconference").InnerText.Equals("1"))
                    {
                        tblPcConf.Style.Add("display", "block");
                        if (node.SelectSingleNode("//conference/confInfo/pcVendorId") != null)
                        {
                            int nodeValue = Int32.Parse(node.SelectSingleNode("//conference/confInfo/pcVendorId").InnerText);
                            switch (nodeValue)
                            {
                                case 1:
                                    if (Session["EnableBlueJeans"].ToString().Equals("1"))
                                        rdBJ.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                case 2:
                                    if (Session["EnableJabber"].ToString().Equals("1"))
                                        rdJB.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                case 3:
                                    if (Session["EnableLync"].ToString().Equals("1"))
                                        rdLync.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                case 4:
                                    if (Session["EnableVidtel"].ToString().Equals("1"))
                                        rdVidtel.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                default:
                                    rdBJ.Checked = true;
                                    break;
                            }
                        }
                    }
                }
                // FB 2693 Ends

                //FB 2376
                /*if (node.SelectSingleNode("//conference/confInfo/isVMR") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/isVMR").InnerText.Equals("1")) //FB 2448
                    {
                        chkVMR.Checked = true;
                        ShowHideAVforVMR(null, null);
                    }
                    else
                        chkVMR.Checked = false;
                }*/
                /** FB 2376 **/
                if (node.SelectSingleNode("//advAVParam/internalBridge") != null)
                {
                    txtintbridge.Text = node.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                    hdnintbridge.Value = node.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                }

                if (node.SelectSingleNode("//advAVParam/externalBridge") != null)
                {
                    txtextbridge.Text = node.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                    hdnextbridge.Value = node.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                }
                //FB 2501 starts
                txtApprover7.Text = node.SelectSingleNode("//conference/confInfo/lastModifiedByName").InnerText;
                hdnApprover7.Text = node.SelectSingleNode("//conference/confInfo/lastModifiedById").InnerText;
                hdnRequestorMail.Text = node.SelectSingleNode("//conference/confInfo/lastModifiedByEmail").InnerText;
                hdnApproverMail.Text = node.SelectSingleNode("//conference/confInfo/hostEmail").InnerText;
                //FB 2501 ends

                /** FB 2376 **/
                // Code added for the Bug # 74- mpujari
                txtApprover4.Text = node.SelectSingleNode("//conference/confInfo/hostName").InnerText;
                hdnApprover4.Text = node.SelectSingleNode("//conference/confInfo/hostId").InnerText;

                //Code changed by offshore fro FB Issue 1123 -- start
                string confType = node.SelectSingleNode("//conference/confInfo/createBy").InnerText;
                //string confType = node.SelectSingleNode("//conference/confInfo/confType").InnerText;
                //Code changed by offshore fro FB Issue 1123 -- end
                //FB 2501 starts
                if(node.SelectSingleNode("//conference/confInfo/StartMode").InnerText!=null)
                    lstStartMode.SelectedValue = node.SelectSingleNode("//conference/confInfo/StartMode").InnerText;
                //FB 2501 ends

                //FB 2595 - Starts //FB 2993 Starts
                drpNtwkClsfxtn.ClearSelection();
                if (node.SelectSingleNode("//conference/confInfo/Secured") != null)
                    if (drpNtwkClsfxtn.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/Secured").InnerText) != null)
                        drpNtwkClsfxtn.SelectedValue = node.SelectSingleNode("//conference/confInfo/Secured").InnerText;
                //FB 2595 - Ends //FB 2993 Ends
                //FB 2870 Start
                if (node.SelectSingleNode("//conference/confInfo/EnableNumericID") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/EnableNumericID").InnerText.Equals("1"))
                        ChkEnableNumericID.Checked = true;
                    else
                        ChkEnableNumericID.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/CTNumericID") != null)
                    txtNumeridID.Text = node.SelectSingleNode("//conference/confInfo/CTNumericID").InnerText;

                if (!ChkEnableNumericID.Checked)
                    txtNumeridID.Text = "";

                //FB 2870 End
                //FB 2998
                if (node.SelectSingleNode("//conference/confInfo/McuSetupTime") != null)
                    txtMCUConnect.Text = node.SelectSingleNode("//conference/confInfo/McuSetupTime").InnerText;
                
                if (node.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime") != null)
                    txtMCUDisConnect.Text = node.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime").InnerText;

                //Code added by offshore fro FB Issue 1123 -- start
                if (Request.QueryString["t"] != null)
                {
                    if (Request.QueryString["t"].ToString().Equals("t")) // if the conference is from a template then we need to generate current date and time like new conference
                    {

                        if (isCloudEnabled == 1 && chkCloudConferencing.Checked)  //FB 2645 //FB 2717-Doubt
                        {
                            confType = "3";
                            lstVMR.SelectedValue = "3"; 
                            lstVMR.Enabled = false;
                        }
                        else
                        {
                            confType = node.SelectSingleNode("//conference/confInfo/confType").InnerText;
                            lstVMR.SelectedValue = node.SelectSingleNode("//conference/confInfo/isVMR").InnerText; // FB 2620
                        }

                        try
                        {
                            if (Session["confid"] != null) //Added for Template List
                                lstTemplates.Items.FindByValue(Session["confid"].ToString()).Selected = true;
                            else if (Session["defaultConfTemp"].ToString() != "0") //FB 1719 //FB 1746
                                lstTemplates.Items.FindByValue(Session["defaultConfTemp"].ToString()).Selected = true;
                            else
                                lstTemplates.Items[0].Selected = true;
                        }
                        catch (Exception e)
                        { log.Trace(e.Message); }
                    }
                }
                //Code added by offshore fro FB Issue 1123 -- end
                //Code Modified For MOJ Phase2 - Start
                ////if (confType.Equals("")) //FB 1205
                ////    confType = Application["DefaultConferenceType"].ToString();//FB 1205
                if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    if (confType.Equals("")) //FB 1205
                        confType = DefaultConferenceType;//FB 1205//Organization Module Fixes
                }
                else
                    confType = "7"; // 7- Room Conference
                //Code Modified For MOJ Phase2 - End
                lstConferenceType.ClearSelection();

                //Code Added for FB 1321 - start
                if (lstConferenceType.Items.FindByValue(confType) != null)
                    lstConferenceType.Items.FindByValue(confType).Selected = true;
                else
                {
                    DisplayDialog("Conference type has been changed as the existing conference type is no longer available");
                    lstConferenceType.SelectedValue = DefaultConferenceType;//Organization Module Fixes
                }
                //Response.Write(obj.Transfer(node.SelectSingleNode("//conference/confInfo").OuterXml));
                //lstConferenceType.Items.FindByValue(confType).Selected = true;
                //Code Added for FB 1321 - end

                //FB 2501
                if (lstConferenceType.SelectedValue.Equals("4") || lstConferenceType.SelectedValue.Equals("7") || lstVMR.SelectedIndex > 0 || lstConferenceType.SelectedValue.Equals("8")) //FB 2694 //FB 2620
                {
                      trStartMode.Attributes.Add("style", "display:None");//FB 2641 
                      trStartMode1.Attributes.Add("style", "display:None");//FB 2717

                }
                else
                {
                    //FB 2641  start                    
                    if (EnableStartMode == 1)
                    {
                        trStartMode.Attributes.Add("style", "display:");
                        trStartMode1.Attributes.Add("style", "display:");//FB 2717
                    }
                    else
                    {
                        trStartMode.Attributes.Add("style", "display:none");
                        trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                        //FB 2641 End
                    }
                }

                XmlNodeList nodes = node.SelectNodes("//conference/confInfo/timezones/timezone");
                int length = nodes.Count;
                /* Code Modified For FB 1453 - Start */
                //int index = 0;
                //lstConferenceTZ.Items.Clear();
                //while (index < length)
                //{
                //    lstConferenceTZ.Items.Add(new ListItem(nodes.Item(index).SelectSingleNode("timezoneName").InnerText, nodes.Item(index).SelectSingleNode("timezoneID").InnerText));
                //    index++;
                //}
                String selTZ = "-1";
                lstConferenceTZ.ClearSelection();
                obj.GetTimezones(lstConferenceTZ, ref selTZ);
                /* Code Modified For FB 1453 - End */

                String selST = "-1";//FB 2219
                DrpServiceType.ClearSelection();
                obj.BindServiceType(DrpServiceType);

                if (node.SelectSingleNode("//conference/confInfo/publicConf").InnerText.Equals("1"))
                    chkPublic.Checked = true;
                else
                    chkPublic.Checked = false;
                if (node.SelectSingleNode("//conference/confInfo/dynamicInvite").InnerText.Equals("1"))
                    chkOpenForRegistration.Checked = true;
                else
                    chkOpenForRegistration.Checked = false;
                lstConferenceTZ.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/timeZone").InnerText).Selected = true;
                DrpServiceType.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/ServiceType").InnerText).Selected = true;//FB 2219
                LoadCommonValues(node);
                 
                /* *** code added for buffer zone *** -- Start */

                if (node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText != "")
                    {
                        setupDur = node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText;
                    }
                }
                
                if (node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText != "")
                    {
                        tearDownDur = node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText;
                    }
                }

                Double.TryParse(setupDur, out setupDuration);
                Double.TryParse(tearDownDur, out teardownDuration);

                //FB 1716 - Start
                double actualDur;
                if (recurring.Equals("1"))
                    Double.TryParse(node.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText, out actualDur);
                else
                    Double.TryParse(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText, out actualDur);
                //FB 2634
                //hdnDuration.Value = (actualDur - setupDuration - teardownDuration) +
                //    "&" + setupDuration + "&" + teardownDuration;

                hdnDuration.Value = (actualDur - setupDuration - teardownDuration).ToString();
                //FB 1716 - End

                //FB 2634
                //Merging Recurrence
                //if (enableBufferZone == "1" && (setupDur != "0" || tearDownDur != "0"))
                //{
                //    //NONRecurringConferenceDiv9.Attributes.Add("Style", "Display:Block;");
                //    chkEnableBuffer.Checked = true;
                //}

                /* *** code added for buffer zone *** -- End */
               
                if (recurring.Equals("1")) //in case of editing a recurring conference
                {
                    string recurstr = node.SelectSingleNode("//conference/confInfo/appointmentTime").InnerXml;
                    recurstr += node.SelectSingleNode("//conference/confInfo/recurrencePattern").InnerXml;
                    if (node.SelectNodes("//conference/confInfo/recurrenceRange").Count != 0)
                        recurstr += node.SelectSingleNode("//conference/confInfo/recurrenceRange").InnerXml;
                    recurstr = "<recurstr>" + recurstr + "</recurstr>";
                    //Response.Write(obj.Transfer(recurstr));
                    string tzstr = "<TimeZone>" + xmlDOC.SelectSingleNode("//conference/confInfo/timezones").InnerXml + "</TimeZone>";
                    string rst = obj.AssembleRecur(recurstr, tzstr);
                    string[] rst_array = rst.Split('|');
                    string recur = rst_array[0];
                  
                    /* *** Code added by Offshore for fb Issue 1073 DateFormat -Start **** */

                    string recDtString = "";
                    string tempRec = "";

                    String[] recDateArr = recur.Split('#');

                    recur = "";
                    if (recDateArr.Length > 0)
                    {
                        tempRec = recDateArr[recDateArr.Length - 1];
                        if (tempRec != "")
                        {
                            String[] dtsArr = tempRec.Split('&');

                            if (dtsArr.Length > 0)
                            {
                                for (int lp = 0; lp < dtsArr.Length; lp++)
                                {
                                    if (dtsArr[lp].IndexOf("/") > 0)
                                    {
                                        dtsArr[lp] = myVRMNet.NETFunctions.GetFormattedDate(dtsArr[lp]);
                                    }
                                    if (recDtString == "")
                                        recDtString = dtsArr[lp];
                                    else
                                        recDtString += "&" + dtsArr[lp];
                                }
                            }
                        }
                        for (int lp = 0; lp < recDateArr.Length - 1; lp++)
                        {
                            if (recur == "")
                                recur = recDateArr[lp];
                            else
                                recur += "#" + recDateArr[lp];
                        }
                        recur += "#" + recDtString;
                    }
                    /* *** Code added by Offshore for fb Issue 1073 DateFormat - End **** */

                    Recur.Value = recur;
                   

                    string SelectedTimeZoneName = rst_array[1];

                    /* *** code added\changed for buffer zone *** -- Start */

                    //Code changed by Offshore for FB Issue 1073 -- Start

                    //if (node.SelectNodes("recurrenceRange").Count > 0)
                    //{
                    //    confStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(node.SelectSingleNode("//conference/confInfo/recurrenceRange/startDate").InnerText);
                    //}
                    ////confEndDate.Text = DateTime.Today.ToString("MM/hh/yyyy");
                    //confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today);
                    ////Code added by Offshore for FB Issue 1073 -- End
                    //confStartDate.Text = confEndDate.Text;
                    //confStartTime.Text = DateTime.Parse("08:00 AM").ToString(tformat);
                    //confEndTime.Text = DateTime.Parse("05:00 PM").ToString(tformat);

                    DateTime startDateTime = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());


                    if (Request.QueryString["sd"] != null)
                        startDateTime = DateTime.Parse(Request.QueryString["sd"].ToString() + " " + Request.QueryString["st"].ToString());
                    else
                    {
                        if (startDateTime.Minute < 45)
                            startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                        else
                            startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);
                    }

                    confStartDate.Text = startDateTime.ToString(format);
                    confStartTime.Text = startDateTime.ToString(tformat);
                    DateTime endDateTime = startDateTime.AddMinutes(60);

                    confEndDate.Text = endDateTime.ToString(format);
                    confEndTime.Text = endDateTime.ToString(tformat);
                   
                    string startHour="0", startMin="0", startSet="AM";
                    double duration = 0;

                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText != "")
                        {
                            startHour = node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText;
                        }
                    }
                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText != "")
                        {
                            startMin = node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText;
                        }
                    }
                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText != "")
                        {
                            startSet = node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText;
                        }
                    }
                   
                    string durationMin = node.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText; //buffer zone
                    txtModifyType.Text = "1";

                    Double.TryParse(durationMin, out duration);

					//FB 2634
                    //SetupDate.Text = confStartDate.Text;
                    //TearDownDate.Text = confEndDate.Text;
                    
                    if(startHour.Trim() == "")
                        startHour = "0";
                    if (startMin.Trim() == "")
                        startMin = "0";
                    if (startSet.Trim() == "")
                        startSet = "AM";

                    DateTime setupTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                    setupTime = setupTime.AddMinutes(setupDuration);
                    string sTime = setupTime.ToString(tformat);
                    //SetupTime.Text = confStartTime.Text;
                    confStartTime.Text = sTime; //FB 2634
                    confStartTime.SelectedValue = sTime;

                    DateTime endTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                    endTime = endTime.AddMinutes(duration);
                    endTime = endTime.AddMinutes(-teardownDuration);
                    string tTime = endTime.ToString(tformat);
                    //TeardownTime.Text = confEndTime.Text;
                    confEndTime.Text = tTime; //FB 2634
                    confEndTime.SelectedValue = tTime;

                    SetupDuration.Text = setupDuration.ToString();
                    TearDownDuration.Text = teardownDuration.ToString();

                    hdnBufferStr.Value = setupDuration.ToString() + "&" + teardownDuration.ToString(); //FB 2634

                    hdnSetupTime.Value = setupDur;
                    hdnTeardownTime.Value = tearDownDur;

                    /* *** code added for buffer zone *** -- End */

                    //FB 1830 Email Edit - start
                    if (!xConfInfo.ContainsKey("setupdate"))
                        xConfInfo.Add("setupdate", myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + sTime); //without buffer
                    if (!xConfInfo.ContainsKey("teardate"))
                        xConfInfo.Add("teardate", myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + tTime);
                    //FB 1830 Email Edit - end

                }
                else
                {
                    int syear = 0, smonth = 0, sday = 0, sHour = 0, sMin = 0;
                    string sSet = "AM";
                    DateTime startDateTime = DateTime.Now; //FB Case 963 Starts here
                    if (Request.QueryString["t"] != null)
                        if (Request.QueryString["t"].ToString().Equals("t")) // if the conference is from a template then we need to generate current date and time like new conference
                        {
                            //FB 2027 - Start
                            //startDateTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString(tformat));
                            ///* *** Code added for FB 1425 QA Bug -Start *** */
                            obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());// FB 1735
                            //if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                            startDateTime = Convert.ToDateTime(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                            ///* *** Code added for FB 1425 QA Bug -End *** */
                            //FB 2027 - End

                            if (startDateTime.Minute < 45)
                                startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                            else
                                startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);

                            syear = startDateTime.Year;
                            smonth = startDateTime.Month;
                            sday = startDateTime.Day;
                            sHour = startDateTime.Hour;
                            sMin = startDateTime.Minute;
                            sSet = startDateTime.ToString("tt").ToUpper();


                        }
                        else // if it is an old conference or if we clone a conference, then we will take the conference information returned by COM
                        {
                            // FB 1469 - Allowing the old conference to be cloned ... start
                            string confstatus="0";

                            if (node.SelectSingleNode("//conference/confInfo/Status") != null)
                                confstatus = node.SelectSingleNode("//conference/confInfo/Status").InnerText;
                            if (confstatus == "7")
                            {
                                startDateTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString(tformat));
                                if (startDateTime.Minute < 45)
                                    startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                                else
                                    startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);

                                syear = startDateTime.Year;
                                smonth = startDateTime.Month;
                                sday = startDateTime.Day;
                                sHour = startDateTime.Hour;
                                sMin = startDateTime.Minute;
                                sSet = startDateTime.ToString("tt").ToUpper();
                            }   // FB 1469 - Allowing the old conference to be cloned ... end
                            else
                            {
                                //FB 1774 - Start
                                DateTime startDate = DateTime.MinValue;                                
                                startDate = Convert.ToDateTime(node.SelectSingleNode("/conference/confInfo/startDate").InnerText);

                                syear = startDate.Year; //Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[2]);
                                smonth = startDate.Month; //Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[0]);
                                sday = startDate.Day; //Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[1]);
                                //FB 1774 - End
                                sHour = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startHour").InnerText);
                                sMin = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startMin").InnerText);
                                sSet = node.SelectSingleNode("//conference/confInfo/startSet").InnerText;
                                startDateTime = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sHour + ":" + sMin + " " + sSet);

                             
                            }
                        }
                       

                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //confStartDate.Text = startDateTime.ToString("MM/dd/yyyy"); // sDate.ToString("MM") + "/" + sDate.ToString("dd") + "/" + sDate.ToString("yyyy");
                    confStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(startDateTime);
                    //Code changed by Offshore for FB Issue 1073 -- End
                    confStartTime.Text = startDateTime.ToString(tformat);
                    TimeSpan durationMin = new TimeSpan(0, Convert.ToInt32(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText), 0); // * 1000000000 * 60);
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //confEndDate.Text = DateTime.Parse(confStartDate.Text + " " + confStartTime.Text).Add(durationMin).ToString("MM/dd/yyyy");

                    confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(startDateTime.Add(durationMin));
                    //Code changed by Offshore for FB Issue 1073 -- End
                    //Response.Write(Convert.ToInt32(node.SelectSingleNode("/conference/confInfo/durationMin").InnerText));
                    // Code added for MOJ Phase 2 QA Bug START
                    if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                        confEndTime.Text = (startDateTime.AddMinutes(15)).ToString(tformat);
                    else
                        // Code added for MOJ Phase 2 QA Bug END
                        //Code added by Offshore for FB Issue 1073 -- Start
                    confEndTime.Text = (startDateTime.AddMinutes(Convert.ToInt32(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText))).ToString(tformat);
                    
                    //code added/changed for buffer zone --Start
                    string durMin = node.SelectSingleNode("//conference/confInfo/durationMin").InnerText;
                    double duration = 0;

                    Double.TryParse(durMin, out duration);

                    confEndTime.Text = startDateTime.AddMinutes(duration).ToString(tformat);

                    setupStartDateTime = startDateTime.AddMinutes(setupDuration);
                    //FB 2634
                    //SetupDate.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime);
                    //SetupTime.Text = setupStartDateTime.ToString(tformat);
                    SetupDuration.Text = setupDuration.ToString();
                    confStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime);
                    confStartTime.Text = setupStartDateTime.ToString(tformat);
                    TearDownDuration.Text = teardownDuration.ToString();

                    DateTime endDateTime = startDateTime.AddMinutes(duration);
                    teardownStartDateTime = endDateTime.AddMinutes(-teardownDuration);
					//FB 2634
                    confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(teardownStartDateTime);
                    confEndTime.Text = teardownStartDateTime.ToString(tformat);

                    //TearDownDate.Text = myVRMNet.NETFunctions.GetFormattedDate(teardownStartDateTime);
                    //TeardownTime.Text = teardownStartDateTime.ToString(tformat);
                    //code added for buffer zone --End

                    txtModifyType.Text = "0";
                    CalculateDuration();
                    //FB Case 963 Ends here

                    //FB 1830 Email Edit - start //FB 2634
                    if (!xConfInfo.ContainsKey("setupdate"))
                        xConfInfo.Add("setupdate", myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //without buffer //FB 2588
                    if (!xConfInfo.ContainsKey("teardate"))
                        xConfInfo.Add("teardate", myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                    //FB 1830 Email Edit - end
                }
                //Response.Write("<br>here" + durationMin);
                nodes = node.SelectNodes("//conference/confInfo/partys/party");
                //Response.Write(pLength);

                getParty(nodes, isClone);//FB 2550
                //Custom attribute fixes - start
                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl)
                        || client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.NGC)
                        || client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.LHRIC))
                {
                    nodes = node.SelectNodes("//conference/confInfo/CustomAttributesList/CustomAttribute");
                    if (nodes.Count > 0)
                    {
                        if (client.ToString().ToUpper().Equals("LHRIC"))
                            GetCustomAttributesLHRIC(nodes);
                        else
                            GetCustomAttributes(nodes);
                    }
                    else
                        if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl)
                            || client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.NGC)
                            || client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.LHRIC))
                        {
                            errLabel.Text = obj.GetTranslatedText("Some Additional Comments fields are missing. Please contact your VRM Administrator.");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                }
                //Custom attribute fixes - end
                isToogle = false; //FB 2506
                nodes = node.SelectNodes("//conference/confInfo/ConfMessageList/ConfMessage"); //FB 2486
                if (nodes.Count > 0)
                    GetConfMessages(nodes);

                //FB 2506 Starts
                if (isToogle)
                {
                    toggleText.Attributes.Add("Style", "Display:block");
                    displayText.InnerText = obj.GetTranslatedText("Less");
                }
                else
                {
                    toggleText.Attributes.Add("Style", "Display:none");
                    displayText.InnerText = obj.GetTranslatedText("More");
                }
                //FB 2506 Ends

                string advAVParam = node.SelectSingleNode("//conference/confInfo/advAVParam").OuterXml;
                //Response.Write(obj.Transfer(advAVParam));
                //Response.End();
                Session.Add("AdvAVParam", advAVParam);
                //btnAVSendReminder.Visible = true;
                //btnCATSendReminder.Visible = true;
                //btnHKSendReminder.Visible = true;
                UpdateAdvAVSettings(new Object(), new EventArgs()); // Fogbugz case 125
                // FB 2570 Starts
                if (Request.QueryString["t"].ToString().Equals("t")) //FB Case 1029
                {
                    lblConfID.Text = "new";
                    lblConfHeader.Text = obj.GetTranslatedText("New");//FB 1830 - Translation
                }
                if ((Request.QueryString["t"].ToString().Equals("o"))) //FB Case 1029
                {
                    lblConfHeader.Text = obj.GetTranslatedText("New");//FB 1830 - Translation
                }
                // FB 2570 Ends
                String confStatus = xmlDOC.SelectSingleNode("//conference/confInfo/Status").InnerText;
                //Response.Write(Application["Client"].ToString().ToUpper());
                txtTimeCheck.Text = "0";
                if (confStatus.Equals("7") && client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl) && Request.QueryString["t"].ToString().Equals(""))
                    DisableAllControls(Page.Controls[1]);
                nodes = xmlDOC.SelectNodes("//conference/confInfo/fileUpload/file");
                GetUploadedFiles(nodes);
                // fogbugz case 153, 975 Saima
                foreach (DataGridItem dgi in dgRooms.Items)
                    EnableControls((CheckBox)dgi.FindControl("chkUseDefault"), new EventArgs());

                RefreshRoom(null, null);//FB 1600
                SelectTree(null, null);//FB 1600

                //FB 1830 Email Edit - start
                Session.Remove("XCONFINFO");
                Session.Add("XCONFINFO", xConfInfo);
                //FB 1830 Email Edit - end

                //FB 2426 Start
                XmlNodeList profNodes = null;
                DataRow dr = null;
                String address="",connType = "", Password = "", LineRate = "";
                int addType = 0, IsDefault = 0;
                nodes = node.SelectNodes("//conference/confInfo/GuestLocationList/ConfGuestRooms/ConfGuestRoom");
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (i == 0)
                    {
                        CreateDtColumnNames();
                        onflyGrid = obj.LoadDataTable(null, colNames);

                        if (!onflyGrid.Columns.Contains("RowUID"))
                            onflyGrid.Columns.Add("RowUID");
                    }
                    dr = onflyGrid.NewRow();
                    dr["RowUID"] = onflyGrid.Rows.Count;

                    if (nodes[i].SelectSingleNode("RoomID") != null)
                        dr["RoomID"] = nodes[i].SelectSingleNode("RoomID").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("GuestRoomName") != null)
                        dr["RoomName"] = nodes[i].SelectSingleNode("GuestRoomName").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactName") != null)
                        dr["ContactName"] = nodes[i].SelectSingleNode("ContactName").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactEmail") != null)
                        dr["ContactEmail"] = nodes[i].SelectSingleNode("ContactEmail").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactPhoneNo") != null)
                        dr["ContactPhoneNo"] = nodes[i].SelectSingleNode("ContactPhoneNo").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("RoomAddress") != null)
                        dr["ContactAddress"] = nodes[i].SelectSingleNode("RoomAddress").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("State") != null)
                        dr["State"] = nodes[i].SelectSingleNode("State").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("City") != null)
                        dr["City"] = nodes[i].SelectSingleNode("City").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ZipCode") != null)
                        dr["ZIP"] = nodes[i].SelectSingleNode("ZipCode").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("Country") != null)
                        dr["Country"] = nodes[i].SelectSingleNode("Country").InnerText.Trim();

                    profNodes = nodes[i].SelectNodes("Profiles/Profile");
                    for (int p = 0; p < profNodes.Count; p++)
                    {
                        if(profNodes[p].SelectSingleNode("AddressType") == null)
                            continue;

                        if (profNodes[p].SelectSingleNode("Address") != null)
                            address = profNodes[p].SelectSingleNode("Address").InnerText.Trim();

                        if (profNodes[p].SelectSingleNode("Password") != null)
                            Password = profNodes[p].SelectSingleNode("Password").InnerText.Trim();


                        if (profNodes[p].SelectSingleNode("MaxLineRate") != null)
                            LineRate = profNodes[p].SelectSingleNode("MaxLineRate").InnerText.Trim();

                        if (profNodes[p].SelectSingleNode("ConnectionType") != null)
                            connType = profNodes[p].SelectSingleNode("ConnectionType").InnerText.Trim();

                        if (profNodes[p].SelectSingleNode("isDefault") != null)
                            Int32.TryParse(profNodes[p].SelectSingleNode("isDefault").InnerText.Trim(), out IsDefault);

                        if (profNodes[p].SelectSingleNode("AddressType") != null)
                            Int32.TryParse(profNodes[p].SelectSingleNode("AddressType").InnerText.Trim(), out addType);

                        
                        switch (connType.ToString())
                        {
                            case ns_MyVRMNet.vrmConnectionTypes.DialIn:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Dial-in to MCU");
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.DialOut:
                            case ns_MyVRMNet.vrmConnectionTypes.DialOutOld:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Dial-out from MCU");
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.Direct:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Direct to MCU");
                                break;
                            default:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Dial-in to MCU");
                                break;
                        }

                        switch (addType)
                        {
                            case 1:
                                dr["IPAddressType"] = "1";
                                dr["IPAddress"] = address;
                                dr["IPPassword"] = Password;
                                dr["IPconfirmPassword"] = Password;
                                dr["IPMaxLineRate"] = LineRate;
                                dr["IPConnectionType"] = connType;
                                dr["IsIPDefault"] = IsDefault;
                                if (IsDefault == 1)
                                {
                                    dr["DefaultAddressType"] = obj.GetTranslatedText("IP Address");
                                    dr["DefaultAddress"] = address;
                                }
                                break;
                            case 4:
                                dr["ISDNAddressType"] = "4";
                                dr["ISDNAddress"] = address;
                                dr["ISDNPassword"] = Password;
                                dr["ISDNconfirmPassword"] = Password;
                                dr["ISDNMaxLineRate"] = LineRate;
                                dr["ISDNConnectionType"] = connType;
                                dr["IsISDNDefault"] = IsDefault;
                                if (IsDefault == 1)
                                {
                                    dr["DefaultAddressType"] = obj.GetTranslatedText("ISDN Address");
                                    dr["DefaultAddress"] = address;
                                }
                                break;
                            case 6:
                                dr["SIPAddressType"] = "6";
                                dr["SIPAddress"] = address;
                                dr["SIPPassword"] = Password;
                                dr["SIPconfirmPassword"] = Password;
                                dr["SIPMaxLineRate"] = LineRate;
                                dr["SIPConnectionType"] = connType;
                                dr["IsSIPDefault"] = IsDefault;
                                if (IsDefault == 1)
                                {
                                    dr["DefaultAddressType"] = obj.GetTranslatedText("E164/SIP Address");
                                    dr["DefaultAddress"] = address;
                                }
                                break;
                        }
                            
                    }
                    onflyGrid.Rows.Add(dr);
                }
                BindOptionData();
                //dgOnflyGuestRoomlist.Visible = true;
                //FB 2426 end
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindDataOld: " + ex.StackTrace);
            }
        }
        //FB 2274 
        private void CrossSilo()
        {
            try
            {
                string xml = "";
                int multiOrgId = 11,EnableSmartP2P = 0; //FB 2430
                string multisiloOrgID = Session["organizationID"].ToString();
                if (Session["multisiloOrganizationID"] != null)
                    multisiloOrgID = Session["multisiloOrganizationID"].ToString();
                Int32.TryParse(multisiloOrgID, out multiOrgId);
               
                XmlDocument xmldoc = null;
                hdnCrossSetupTime.Value = "-1"; //FB 2398
                hdnCrossTearDownTime.Value = "-1"; //FB 2398
                hdnCrossEnableSmartP2P.Value = "-1"; //FB 2430

                if (multiOrgId >= 11)//FB 2274
                {
                    xml = obj.CallMyVRMServer("GetAllOrgSettings", "<GetAllOrgSettings><organizationID>" + multisiloOrgID + "</organizationID></GetAllOrgSettings>", HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    if (xml.IndexOf("<error>") <= 0)
                    {
                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(xml);

                        hdnCrossrecurEnable.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRecurringConference").InnerText;
                        hdnCrossdynInvite.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDynamicInvite").InnerText;
                        hdnCrossroomModule.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFacilites").InnerText;
                        hdnCrossfoodModule.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCatering").InnerText;
                        hdnCrosshkModule.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableHouseKeeping").InnerText;
                        hdnCrossisVIP.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/isVIP").InnerText;
                        hdnCrossEnableRoomServiceType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomServiceType").InnerText;
                        hdnCrossisSpecialRecur.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/isSpecialRecur").InnerText;
                        hdnCrossConferenceCode.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/ConferenceCode").InnerText;
                        hdnCrossLeaderPin.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/LeaderPin").InnerText;
                        hdnCrossAdvAvParams.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/AdvAvParams").InnerText;
                        hdnCrossEnableBufferZone.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBufferZone").InnerText;
                        hdnCrossEnableEntity.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCustomOption").InnerText;
                        hdnCrossAudioParams.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/AudioParams").InnerText;
                        hdnCrossdefaultPublic.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferencesAsPublic").InnerText;
                        hdnCrossP2PEnable.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableP2PConference").InnerText;
                        hdnCrossEnableRoomConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomConference").InnerText;
                        hdnCrossEnableAudioVideoConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioVideoConference").InnerText;
                        hdnCrossDefaultConferenceType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferenceType").InnerText;
                        hdnCrossEnableAudioOnlyConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioOnlyConference").InnerText;
                        //hdnCrossenableAV.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/AudioParams").InnerText;
                        hdnCrossDefaultConferenceType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferenceType").InnerText;
                        hdnCrossisMultiLingual.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/isMultiLingual").InnerText;
                        hdnCrossroomExpandLevel.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/RoomTreeExpandLevel").InnerText;
                        hdnCrossEnableImmConf.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableImmConf").InnerText;
                        hdnCrossEnablePublicConf.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePublicConference").InnerText;
                        hdnCrossEnableConfPassword.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableConferencePassword").InnerText;
                        hdnCrossEnableRoomParam.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomParam").InnerText;
						hdnCrossEnableSurvey.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSurvey").InnerText;
                        //hdnCrossEnablePC.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePC").InnerText;//FB 2347T
                        hdnCrossSetupTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/SetupTime").InnerText;//FB 2398
                        hdnCrossTearDownTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/TearDownTime").InnerText;//FB 2398
						hdnCrossEnableAudioBridges.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioBridges").InnerText;//FB 2443
                        hdnTxtMsg.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUEnchancedLimit").InnerText;//FB 2486
                        hdnCrossMeetGreetBufferTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MeetandGreetBuffer").InnerText;//FB 2398
						if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSmartP2P") != null) //FB 2430
                            hdnCrossEnableSmartP2P.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSmartP2P").InnerText;
                        //FB 2641 start
                        if(xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLinerate")!=null)
                            hdnCrossEnableLinerate.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLinerate").InnerText;
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableStartMode") != null)
                            hdnCrossEnableStartMode.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableStartMode").InnerText; 
                        //FB 2641 End
                        hdnEnableNumericID.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableNumericID").InnerText;//fb 2870
                        hdnEnableProfileSelection.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableProfileSelection").InnerText;//FB 2947
                        hdnMCUSetupTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/McuSetupTime").InnerText;//FB 2998
                        hdnMCUTeardownTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTeardonwnTime").InnerText;//FB 2998
                        hdnMCUConnectDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUSetupDisplay").InnerText;//FB 2998
                        hdnMCUDisconnectDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTearDisplay").InnerText;//FB 2998
						hdnNetworkSwitching.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/NetworkSwitching").InnerText;//FB 2993
                    }
                }

                //FB 2998
                if (hdnMCUConnectDisplay.Value != "")
                    mcuSetupDisplay = hdnMCUConnectDisplay.Value;

                if (hdnMCUDisconnectDisplay.Value != "")
                    mcuTearDisplay = hdnMCUDisconnectDisplay.Value;

                if (hdnCrossDefaultConferenceType != null && hdnCrossDefaultConferenceType.Value != "")
                    DefaultConferenceType = hdnCrossDefaultConferenceType.Value;
                else if (Session["DefaultConferenceType"] != null)
                    DefaultConferenceType = Session["DefaultConferenceType"].ToString();

                if (hdnCrossroomModule != null && hdnCrossroomModule.Value != "")
                    roomModule = hdnCrossroomModule.Value;
                else if (Session["roomModule"] != null)
                    roomModule = Session["roomModule"].ToString();

                if (hdnCrosshkModule != null && hdnCrosshkModule.Value != "")
                    hkModule = hdnCrosshkModule.Value;
                else if (Session["hkModule"] != null)
                    hkModule = Session["hkModule"].ToString();

                if (hdnCrossfoodModule != null && hdnCrossfoodModule.Value != "")
                    foodModule = hdnCrossfoodModule.Value;
                else if (Session["foodModule"] != null)
                    foodModule = Session["foodModule"].ToString();

                if (hdnCrossP2PEnable != null && hdnCrossP2PEnable.Value != "")
                    P2PEnable = hdnCrossP2PEnable.Value;
                else if (Session["P2PEnable"] != null)
                    P2PEnable = Session["P2PEnable"].ToString();

                if (hdnCrossEnableRoomConfType != null && hdnCrossEnableRoomConfType.Value != "")
                    EnableRoomConfType = hdnCrossEnableRoomConfType.Value;
                else if (Session["EnableRoomConfType"] != null)
                    EnableRoomConfType = Session["EnableRoomConfType"].ToString();

                if (hdnCrossEnableAudioVideoConfType != null && hdnCrossEnableAudioVideoConfType.Value != "")
                    EnableAudioVideoConfType = hdnCrossEnableAudioVideoConfType.Value;
                else if (Session["EnableAudioVideoConfType"] != null)
                    EnableAudioVideoConfType = Session["EnableAudioVideoConfType"].ToString();

                if (hdnCrossEnableAudioOnlyConfType != null && hdnCrossEnableAudioOnlyConfType.Value != "")
                    EnableAudioOnlyConfType = hdnCrossEnableAudioOnlyConfType.Value;
                else if (Session["EnableAudioOnlyConfType"] != null)
                    EnableAudioOnlyConfType = Session["EnableAudioOnlyConfType"].ToString();

                if (hdnCrossisVIP != null && hdnCrossisVIP.Value != "")
                    EnableIsVip = hdnCrossisVIP.Value;
                else if (Session["isVIP"] != null)
                    EnableIsVip = Session["isVIP"].ToString();

                if (hdnCrossEnableRoomServiceType != null && hdnCrossEnableRoomServiceType.Value != "")
                    EnableServiceType = hdnCrossEnableRoomServiceType.Value;
                else if (Session["EnableRoomServiceType"] != null)
                    EnableServiceType = Session["EnableRoomServiceType"].ToString();

                if (hdnCrossEnablePublicConf != null && hdnCrossEnablePublicConf.Value != "")
                    EnablePublicConference = hdnCrossEnablePublicConf.Value;
                else if (Session["EnablePublicConf"] != null)
                    EnablePublicConference = Session["EnablePublicConf"].ToString();

                //FB 2451 - Start
                if (hdnCrossdefaultPublic != null && hdnCrossdefaultPublic.Value != "")
                    defaultPublic = hdnCrossdefaultPublic.Value;
                else if (Session["defaultpublic"] != null)
                    defaultPublic = Session["defaultpublic"].ToString();
                //FB 2451 - End 

                if (hdnCrossEnableConfPassword != null && hdnCrossEnableConfPassword.Value != "")
                    EnableConferencePassword = hdnCrossEnableConfPassword.Value;
                else if (Session["EnableConfPassword"] != null)
                    EnableConferencePassword = Session["EnableConfPassword"].ToString();

                //FB 2446 - Start
                if (hdnCrossdynInvite != null && hdnCrossdynInvite.Value != "") 
                    Enableopenforregistration = hdnCrossdynInvite.Value;
                else if (Session["dynamicInviteEnabled"] != null)
                    Enableopenforregistration = Session["dynamicInviteEnabled"].ToString();
                //FB 2446 - End

                if (hdnCrossEnableRoomParam != null && hdnCrossEnableRoomParam.Value != "")
                    EnableRoomParam = hdnCrossEnableRoomParam.Value;
                else if (Session["EnableRoomParam"] != null)
                    EnableRoomParam = Session["EnableRoomParam"].ToString();

                if (hdnCrossEnableBufferZone != null && hdnCrossEnableBufferZone.Value != "")
                    enableBufferZone = hdnCrossEnableBufferZone.Value;
                else
                    enableBufferZone = Session["EnableBufferZone"].ToString();

				if (hdnCrossEnableSurvey != null && hdnCrossEnableSurvey.Value != "")//FB 2348 For Settings2PartyNet
                    if (Session["EnableSurveySilo"] == null)
                        Session.Add("EnableSurveySilo", hdnCrossEnableSurvey.Value);
                    else
                    {
                        EnableSurvey = hdnCrossEnableSurvey.Value;
                        Session["EnableSurveySilo"] = hdnCrossEnableSurvey.Value;
                    }
                //else if (Session["PCModule"] != null)
                //    EnableSurvey = Session["PCModule"].ToString();

                ////FB 2347T
                //if (hdnCrossEnablePC != null && hdnCrossEnablePC.Value != "")//FB 2348 For Settings2PartyNet
                //    if (Session["PCModuleSilo"] == null)
                //        Session.Add("PCModuleSilo", hdnCrossEnablePC.Value);
                //    else
                //    {
                //        EnablePC = hdnCrossEnablePC.Value;
                //        Session["PCModuleSilo"] = hdnCrossEnablePC.Value;
                //    }
                //else if (Session["PCModule"] != null)
                //    EnablePC = Session["PCModule"].ToString();

                Int32.TryParse(hdnCrossSetupTime.Value, out OrgSetupTime);  //FB 2398 start
                if ( OrgSetupTime < 0 && Session["OrgSetupTime"] != null)
                    Int32.TryParse(Session["OrgSetupTime"].ToString(), out OrgSetupTime);

                Int32.TryParse(hdnCrossTearDownTime.Value, out OrgTearDownTime);
                if (OrgTearDownTime < 0 && Session["OrgTearDownTime"] != null)
                    Int32.TryParse(Session["OrgTearDownTime"].ToString(), out OrgTearDownTime);  //FB 2398 end
				Int32.TryParse(hdnCrossEnableSmartP2P.Value, out EnableSmartP2P);  //FB 2430
                if (EnableSmartP2P < 0 && Session["EnableSmartP2P"] != null)
                {
                    Int32.TryParse(Session["EnableSmartP2P"].ToString(), out EnableSmartP2P);
                    hdnCrossEnableSmartP2P.Value = EnableSmartP2P.ToString();
                }
                int.TryParse(hdnCrossMeetGreetBufferTime.Value, out OrgMeetGrettBuffer);
                if (OrgMeetGrettBuffer < 0 && Session["MeetandGreetBuffer"] != null) //FB 2632
                {
                    int.TryParse(Session["MeetandGreetBuffer"].ToString(), out OrgMeetGrettBuffer);
                    hdnCrossMeetGreetBufferTime.Value = OrgMeetGrettBuffer.ToString();
                }
                
                // FB 2641 start
				if (hdnCrossEnableLinerate != null && hdnCrossEnableLinerate.Value != "")
                    int.TryParse(hdnCrossEnableLinerate.Value, out EnableLinerate);
                else if (Session["EnableLinerate"] != null)
                    int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);

                if (hdnCrossEnableStartMode != null && hdnCrossEnableStartMode.Value != "")
                    int.TryParse(hdnCrossEnableStartMode.Value, out EnableStartMode);
                else if (Session["EnableStartMode"] != null)
                    int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);
                // FB 2641 End
                if (hdnNetworkSwitching != null && hdnNetworkSwitching.Value != "") //FB 2993
                    int.TryParse(hdnNetworkSwitching.Value, out NetworkSwitching);
                else if (Session["NetworkSwitching"] != null)
                    int.TryParse(Session["NetworkSwitching"].ToString(), out NetworkSwitching);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("CrossSilo: " + ex.StackTrace);
            }
        }

        protected void DisableAllControls(Control ctrl)
        {
            try
            {
                //Response.Write("<br>" + ctrl.ID);
                foreach (Control ctl in ctrl.Controls)
                {
                    //Response.Write("<br>" + ctl.ID);
                    if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl))
                    {    //Response.Write(ctl.ID);
                        if (ctl is TextBox && ctl.ID.IndexOf("txtCA") < 0)
                        {
                            //    Response.Write("in if");
                            ((TextBox)ctl).Enabled = false;
                        }
                        if (ctl is CheckBox && ctl.ID.IndexOf("txtCA") < 0)
                            ((CheckBox)ctl).Enabled = false;
                    }
                    else if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.NGC))
                        if (ctl is TextBox && !ctl.ID.Equals("txtCANGC1"))
                            ((TextBox)ctl).Enabled = false;
                    if (ctl is Button && !ctl.ID.Equals("btnConfSubmit"))
                        ((Button)ctl).Enabled = false;
                    if (ctl is HtmlInputFile)
                        ((HtmlInputFile)ctl).Disabled = true;
                    if (ctl is HtmlInputButton)
                        ((HtmlInputButton)ctl).Disabled = true;
                    if (ctl is DropDownList)
                        ((DropDownList)ctl).Enabled = false;
                    if (ctl is ListBox)
                        ((ListBox)ctl).Enabled = false;
                    if (ctl is TreeView)
                        ((TreeView)ctl).Enabled = false;
                    if (ctl is CheckBox && ctl.ID.IndexOf("chkCA1") < 0)
                        ((CheckBox)ctl).Enabled = false;
                    if (ctl is RadioButtonList)
                        ((RadioButtonList)ctl).Enabled = false;
                    if (ctl is MetaBuilders.WebControls.ComboBox)
                        ((MetaBuilders.WebControls.ComboBox)ctl).Enabled = false;

                    DisableAllControls(ctl);
                }
                txtTimeCheck.Text = "1";
                Wizard1.ActiveViewIndex = Wizard1.Views.Count - 2;
                TopMenu.Items[TopMenu.Items.Count - 1].Selected = true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
        protected void GetUploadedFiles(XmlNodeList nodes)
        {
            try
            {
                int i = 0;
                foreach (XmlNode node in nodes)
                    if (!node.InnerText.Equals(""))
                    {
                        i++;
                        String fPath = node.InnerText;
                        if (node.InnerText != "")
                        {
							//FB 1830
                            String fileName = getUploadFilePath(node.InnerText);

                            fPath = fPath.Replace("\\", "/");
                            int startIndex = fPath.IndexOf("/en/");//FB 1830
                            int len = fPath.Length - 1;
                            //Response.Write(fPath + " : " + startIndex + " : " + len);
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830

                            /*
                             * FB 2154
                             * 
                             * if (language == "en" || fPath.IndexOf((@"\en\")) > 0)
                            {
                                fPath = fPath.Replace("\\", "/");
                                int startIndex = fPath.IndexOf("/en/");//FB 1830
                                int len = fPath.Length - 1;
                                //Response.Write(fPath + " : " + startIndex + " : " + len);
                                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830
                            }
                            else
                                fPath = "../Image/" + fileName;*/

                            switch (i)
                            {
                                case 1:
                                    lblUpload1.Text = "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>";
                                    hdnUpload1.Text = node.InnerText;
                                    btnRemove1.Visible = true;
                                    FileUpload1.Visible = false;
                                    lblUpload1.Visible = true;
                                    break;
                                case 2:
                                    lblUpload2.Text = "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>";
                                    hdnUpload2.Text = node.InnerText;
                                    btnRemove2.Visible = true;
                                    FileUpload2.Visible = false;
                                    lblUpload2.Visible = true;
                                    break;
                                case 3:
                                    lblUpload3.Text = "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>";
                                    hdnUpload3.Text = node.InnerText;
                                    btnRemove3.Visible = true;
                                    FileUpload3.Visible = false;
                                    lblUpload3.Visible = true;
                                    break;
                            }
                        }
                    }

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            try
            {
                //
                // CODEGEN: This call is required by the ASP.NET Web Form Designer.
                //
                //Response.Write("<br>OnInit: " + DateTime.Now.ToString("hh:mm ss"));
                InitializeComponent();
                InitializeUIComponent();
                base.OnInit(e);
                //Response.Write("<br>OnInit Exit: " + DateTime.Now.ToString("hh:mm ss"));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.Page_Load);

        }

        private void InitializeUIComponent()
        {

        }
        #endregion
        
        #region GetAVWorkOrders

        protected void GetAVWorkOrders()
        {
            try
            {    //Code added for Wo bug
                String inXML = objInXML.SearchConferenceWorkOrders("", GetConfIDforWO(lblConfID.Text), "", "", "", "", "", "", "", "1", "", "1", "1","-1");
                String outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", "and"); //FB 2164
                //Response.Write(obj.Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                LoadDataGridMain(nodes, "1", AVMainGrid);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("GetAVWorkOrders: " + ex.Message);

            }

        }

        #endregion

        #region GetCATWorkOrders

        protected void GetCATWorkOrders()
        {
            try
            {   //Code added fro WO bug reccurence
                String inXML = "<GetProviderWorkorderDetails>" + obj.OrgXMLElement() + "<ConfID>" + GetConfIDforWO(lblConfID.Text) + "</ConfID><WorkorderID></WorkorderID><Type>2</Type></GetProviderWorkorderDetails>";//Organization Module Fixes
                string outXML = obj.CallMyVRMServer("GetProviderWorkorderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetProviderWorkorderDetails inxml: " + inXML);
                log.Trace("GetProviderWorkorderDetails outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(outXML);
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetProviderWorkorderDetails/Workorders/Workorder");
                LoadProviderWorkorders(nodes, "2", CATMainGrid);
            }
            catch (Exception ex)
            {
                log.Trace("GetCATWorkOrders: " + ex.Message);
            }
        }

        #endregion

        #region LoadCateringWorkorders

        protected void LoadCateringWorkorders()
        {
            try
            {
                log.Trace("in LoadCateringWorkorders");
                SyncRoomSelection();
                if (treeRoomSelection.CheckedNodes.Count.Equals(0))
                {
                    lblCATWOInstructions.Visible = true;
                    lblCATWOInstructions.Text = obj.GetTranslatedText("Please select at least one room from 'Select Rooms' tab in order to create a work order.");//FB 1830 - Traslation
                    btnAddNewCAT.Visible = false;
                }
                else
                {
                    SetInstructionsCAT(new object(), new EventArgs());
                    btnAddNewCAT.Visible = true;
                    btnAddNewCAT.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2214
                    //lblCATWOInstructions.Visible = false;
                    //if (CATMainGrid.Items.Count.Equals(0))
                    //{
                    //    AddBlankWorkorder();
                    //}
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region LoadProviderWorkorders

        protected void LoadProviderWorkorders(XmlNodeList nodes, String tpe, DataGrid dgMain)
        {
            try
            {
                XmlTextReader xtr;
                DataSet dsMain = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    dsMain.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                //DataView dv;
                DataTable dt = new DataTable();
                if (dsMain.Tables.Count > 0)
                {
                    //dv = new DataView(ds.Tables[0]);
                    dt = dsMain.Tables[0]; // dv.Table;
                    if (!dt.Columns.Contains("ServiceName")) dt.Columns.Add("ServiceName");
                    if (!dt.Columns.Contains("strMenus")) dt.Columns.Add("strMenus");
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["SelectedService"].ToString().Equals("0"))
                            dr["SelectedService"] = "1";
                        dr["ServiceName"] = lstServices.Items.FindByValue(dr["SelectedService"].ToString()).Text;
                        //Code added for FB Issue 1073 -- Start
                        if (dt.Columns.Contains("DeliverByDate"))
                        {
                            if (myVRMNet.NETFunctions.GetDefaultDate(dr["DeliverByDate"].ToString()) == "")
                                dr["DeliverByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["DeliverByDate"].ToString(), format);
                            else
                                dr["DeliverByDate"] = myVRMNet.NETFunctions.GetDefaultDate(dr["DeliverByDate"].ToString());//FB 2281
                        }
                        //Code added for FB Issue 1073 -- End
                        // FB 1686
                        if (dt.Columns.Contains("DeliverbyTime"))
                        {
                            if (myVRMNet.NETFunctions.GetFormattedTime(dr["DeliverbyTime"].ToString(), Session["timeFormat"].ToString()) == "")
                                dr["DeliverbyTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["DeliverbyTime"].ToString(), tformat);
                            else
                                dr["DeliverbyTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["DeliverbyTime"].ToString(), Session["timeFormat"].ToString());
                        }

                        if (dt.Columns.Contains("Price"))
                        {
                            //FB 1830
                            tmpVal = 0;
                            decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                            dr["Price"] = tmpVal.ToString("n", cInfo);
                            //dr["Price"] = (dr["Price"].ToString() == "") ? "0.00" : Convert.ToDecimal(dr["Price"].ToString()).ToString("0.00"); //FB 1686
                        }
                    }
                    dgMain.DataSource = dt;
                    dgMain.DataBind();

                    foreach (XmlNode node in nodes)
                    {
                        foreach (DataGridItem dgi in dgMain.Items)
                        {
                            if (node.SelectSingleNode("ID").InnerText.Equals(dgi.Cells[0].Text))
                            {
                                DataSet dsMenu = new DataSet();
                                DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                                XmlNodeList subNodes = node.SelectNodes("MenuList/Menu");
                                Label strMenus = (Label)dgi.FindControl("lblCateringMenus");
                                strMenus.Text = "";
                                foreach (XmlNode subNode in subNodes)
                                {
                                    xtr = new XmlTextReader(subNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                    dsMenu.ReadXml(xtr, XmlReadMode.InferSchema);
                                    strMenus.Text += subNode.SelectSingleNode("ID").InnerText + ":" + subNode.SelectSingleNode("Name").InnerText + ":" + subNode.SelectSingleNode("Quantity").InnerText + ";";
                                }
                                if (dsMenu.Tables.Count > 0)
                                {
                                    dgCateringMenus.DataSource = dsMenu;
                                    dgCateringMenus.DataBind();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region GetHKWorkOrders

        protected void GetHKWorkOrders()
        {
            try
            {
                String inXML = objInXML.SearchConferenceWorkOrders("", GetConfIDforWO(lblConfID.Text), "", "", "", "", "", "", "", "3", "", "1", "1","-1"); //FB 1114
                String outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace("&", " and "); //FB 2164
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("/WorkOrderList/WorkOrder");
                LoadDataGridMain(nodes, "3", HKMainGrid);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("GetHKWorkOrders: " + ex.Message);

            }

        }

        #endregion

        #region PreSelectRooms

        protected void PreSelectRooms(XmlNodeList nodes)
        {


            XmlNodeList selnodes = nodes.Item(0).SelectNodes("selected/level1ID");
            int selLength = selnodes.Count;
            //treeRoomSelection.Attributes.Add("onclick", "javascript:GetRooms()");

            if (selLength >= 1)
                for (int n = 0; n < selLength; n++)
                    selRooms += selnodes.Item(n).InnerText + ", ";
            if (Request.QueryString["rms"] != null)
                selRooms += Request.QueryString["rms"].ToString();
            selectedloc.Value = selRooms.Trim(',');
            

        }

        #endregion

        #region GetLocationList

        protected void GetLocationList(XmlNodeList nodes)
        {
            try
            {
                //FB 2274 
                if (hdnCrossroomExpandLevel != null && hdnCrossroomExpandLevel.Value != "")
                    roomExpand = hdnCrossroomExpandLevel.Value;
                else
                    roomExpand = Session["roomExpandLevel"].ToString();
                ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");
                lstRooms.Items.Clear();
                //lstCATRooms.Items.Clear();
                lstHKRooms.Items.Clear();
                lstRooms.Items.Add(li);
                //lstCATRooms.Items.Add(li);
                lstHKRooms.Items.Add(li);
                int nodes2Count = 0;
                int length = nodes.Count;
                for (int i = 0; i < length; i++)
                {
                    //Response.Write(i);
                    //                    Response.End();
                    TreeNode tn3 = new TreeNode(nodes.Item(i).SelectSingleNode("level3Name").InnerText, nodes.Item(i).SelectSingleNode("level3ID").InnerText);
                    tn3.SelectAction = TreeNodeSelectAction.None;
                    treeRoomSelection.Nodes[0].ChildNodes.Add(tn3);
                    XmlNodeList nodes2 = nodes.Item(i).SelectNodes("level2List/level2");
                    int length2 = nodes2.Count;

                    tn3.Expanded = false;

                    if (roomExpand != "")//Organization Module Fixes //FB 2274
                    {
                        if (!roomExpand.ToLower().Equals("list"))//Organization Module Fixes //FB 2274
                        {
                            if (Int32.Parse(roomExpand) >= 2)//Organization Module Fixes //FB 2274
                                tn3.Expanded = true;
                        }
                    }
                                                
                    for (int j = 0; j < length2; j++)
                    {
                        TreeNode tn2 = new TreeNode(nodes2.Item(j).SelectSingleNode("level2Name").InnerText, nodes2.Item(j).SelectSingleNode("level2ID").InnerText);

                        tn2.SelectAction = TreeNodeSelectAction.None;
                        treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Add(tn2);
                        XmlNodeList nodes1 = nodes2.Item(j).SelectNodes("level1List/level1");
                        int length1 = nodes1.Count;
                        tn2.Expanded = true; // fogbugz case 277
                        nodes2Count = 0;
                        for (int k = 0; k < length1; k++)
                        {
                            TreeNode tn = new TreeNode(nodes1.Item(k).SelectSingleNode("level1Name").InnerText, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                            tn.ToolTip = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                            tn.Value = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                            //Response.Write("<br>" + tn.Value);
                            //tnc.Add(tn2);
                            treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Add(tn);
                            tn.NavigateUrl = @"javascript:chkresource('" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "');";
                            string l1Name = "<a href='#'  title='" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "'  onclick='javascript:chkresource(\"" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "\");'>" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "</a>";
                            li = new ListItem(l1Name, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                            //li.Attributes.Add("onclick", "javascript:chkresource('" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "');");
                            lstRoomSelection.Font.Size = FontUnit.Smaller;
                            lstRoomSelection.ForeColor = System.Drawing.Color.ForestGreen;
                            lstRoomSelection.Items.Add(li);
                            if (selRooms.IndexOf(" " + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + ",") >= 0)
                            {
                                nodes2Count++;
                                tn.Checked = true;
                                li = new ListItem(tn.Text, tn.Value);
                                lstRooms.Items.Add(li);
                                //lstCATRooms.Items.Add(li);
                                lstHKRooms.Items.Add(li);

                                if (nodes1.Item(k).SelectSingleNode("IsVMR") != null && hdnSelectVMRRoom.Value.Trim() == "") //FB 2448
                                    if (nodes1.Item(k).SelectSingleNode("IsVMR").InnerText.Trim() == "1")
                                    {
                                        if (hdnSelectVMRRoom.Value.Trim() != "")
                                            hdnSelectVMRRoom.Value = ",";

                                        hdnSelectVMRRoom.Value = nodes1.Item(k).SelectSingleNode("level1ID").InnerText.Trim();
                                    }
                            }
                        }

                        /* Fogbugz case 156 to check the middle tier if all rooms are selected */
                        if (nodes1.Count.Equals(nodes2Count))
                            tn2.Checked = true; // fogbugz case 277

                        tn2.Expanded = false;
                        if (roomExpand != "")//Organization Module Fixes //FB 2274
                        {
                            if (!roomExpand.ToLower().Equals("list"))//Organization Module Fixes //FB 2274
                            {
                                if (Int32.Parse(roomExpand) >= 3)//Organization Module Fixes //FB 2274
                                    tn2.Expanded = true;
                            }
                        }
                    }
                }
                //FB Case 1056 - Saima starts here 
                for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                    for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                        if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                        {
                            ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                            lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                            log.Trace(i + " : " + lstRoomSelection.Items[i].Text.Substring(lstRoomSelection.Items[i].Text.IndexOf(">")) + " : " + lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[j].Value = liTemp.Value;
                            lstRoomSelection.Items[j].Text = liTemp.Text;
                        }
                //FB Case 1056 - Saima ends here 

                //FB 1149 --Start
                foreach (ListItem listItem in lstRoomSelection.Items)
                {
                    for (int r = 0; r < selRooms.Split(',').Length - 1; r++)
                    {

                        if (listItem.Value.Equals(selRooms.Split(',')[r].Trim()))
                        {
                            listItem.Selected = true;
                        }
                    }
                }
                //FB 1149 --End

                //fogbugz case 466: Saima starts here
                if (Session["RoomListView"].ToString().ToUpper().Equals("LIST"))
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
                //fogbugz case 466: Saima ends here
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region GetOldRoom

        protected void GetOldRoom()
        {
            lstRooms.Items.Clear();
            lstHKRooms.Items.Clear();
            //lstCATRooms.Items.Clear();
            ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");
            lstRooms.Items.Add(li);
            //lstCATRooms.Items.Add(li);
            lstHKRooms.Items.Add(li);
            for (int i = 0; i < treeRoomSelection.Nodes[0].ChildNodes.Count; i++)
                for (int j = 0; j < treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Count; j++)
                    foreach (TreeNode tn in treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes)
                    {
                        if (tn.Checked == true)
                        {
                            li = new ListItem(tn.Text, tn.Value);
                            lstRooms.Items.Add(li);
                            //lstCATRooms.Items.Add(li);
                            lstHKRooms.Items.Add(li);
                        }
                    }
            if (lstRooms.Items.Count <= 1)
            {
                lstRooms.Items.Clear();
                lstRooms.Items.Add("No room selected");
                //lstCATRooms.Items.Add("No room selected");
                lstHKRooms.Items.Add("No room selected");
            }
        }

        #endregion

        #region A_btnAddNew_Click

        public void A_btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dStart, dEnd;//FB 2634

                btnAddNewAV.Enabled = false;
                btnAddNewAV.Attributes.Add("Class", "btndisable"); //FB 2664
                txtWorkOrderID.Text = "new";
                AVItemsTable.Visible = true;
                AVMainGrid.Visible = false;
                btnSubmit.Text = obj.GetTranslatedText("Create");//FB 1830 - Traslation
                lblNewEditAV.Text = obj.GetTranslatedText("Create New Work Order");//FB 1830 - Traslation

                //FB 2634 - starts
                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text)); 

                Double setupDuration = Double.MinValue;//FB 2634-K
                Double tearDuration = Double.MinValue;

                if (!chkEnableBuffer.Checked && enableBufferZone == "1")//FB 2398
                {
                    Double.TryParse(SetupDuration.Text, out setupDuration);
                    Double.TryParse(TearDownDuration.Text, out tearDuration);
                    dStart = dStart.AddMinutes(-setupDuration);
                    dEnd = dEnd.AddMinutes(tearDuration);
                }

                txtStartByDate.Text = dStart.ToString(format);
                startByTime.Text = myVRMNet.NETFunctions.GetFormattedTime(dStart.ToString("HH:mm"), Session["timeFormat"].ToString());

                txtCompletedBy.Text = dEnd.ToString(format);
                completedByTime.Text = myVRMNet.NETFunctions.GetFormattedTime(dEnd.ToString("HH:mm"), Session["timeFormat"].ToString());

                //txtStartByDate.Text = confStartDate.Text;
                //startByTime.Text = confStartTime.Text;

                //FB 2634 - End

                btnSubmit.Enabled = false;
                if (lstRooms.Items.Count > 0)
                    lstRooms.SelectedIndex = 0;
                lstAVSet.Items.Clear();
                for (int i = 0; i < itemsGrid.Items.Count; i++)
                    itemsGrid.Items[i].Cells.Clear();
                itemsGrid.Visible = false;
                txtWorkOrderName.Text = ConferenceName.Text + "_AV";
                txtApprover1.Text = "";
                hdnApprover1.Text = "";
                //txtCompletedBy.Text = confEndDate.Text;//FB 2634
                //completedByTime.Text = confEndTime.Text;
                chkNotify.Checked = true;
                Session["AVEditColumn"] = "-1";
                lblAVWOInstructions.Text = obj.GetTranslatedText("Please select a Room first from the drop down below.") + System.Environment.NewLine + obj.GetTranslatedText("If there are no rooms in the list, click on 'Select Rooms' option available on your left.") + System.Environment.NewLine + obj.GetTranslatedText("Click 'Cancel' before you proceed to another step.");//FB 1830 - Translation
                lstTimezones.ClearSelection();
                lstTimezones.Items.FindByValue(lstConferenceTZ.SelectedValue).Selected = true;
                lstDeliveryType.ClearSelection();
                if (lstRooms.Items.Count > 1)
                {
                    lstRooms.ClearSelection();
                    lstRooms.Items[1].Selected = true;
                    GetRoomSets(lstRooms.SelectedValue);
                    if (lstAVSet.Items.Count > 1)
                    {
                        lstAVSet.ClearSelection();
                        lstAVSet.Items[1].Selected = true;
                        GetAVSetItems(lstAVSet.SelectedValue);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region C_btnAddNew_Click

        public void C_btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                AddBlankWorkorder();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region H_btnAddNew_Click

        public void H_btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                //Response.Write("hre in add");
                DateTime dStart, dEnd;//FB 2634

                btnAddNewHK.Enabled = false;
                btnAddNewHK.Attributes.Add("Class", "btndisable"); //FB 2664
                HKItemsTable.Visible = true;
                btnHKSubmit.Text = obj.GetTranslatedText("Create");//FB 1830 - Traslation
                txtHKWorkOrderID.Text = "new";
                lblNewEditHK.Text = obj.GetTranslatedText("Create New Work Order");//FB 1830 - Traslation
                btnHKSubmit.Enabled = false;
                if (lstHKRooms.Items.Count > 0)
                    lstHKRooms.SelectedIndex = 0;
                lstHKSet.Items.Clear();
                for (int i = 0; i < itemsGridHK.Items.Count; i++)
                    itemsGridHK.Items[i].Cells.Clear();
                itemsGridHK.Visible = false;
                txtHKWorkOrderName.Text = ConferenceName.Text + "_HK";
                txtApprover3.Text = "";
                hdnApprover3.Text = "";


                //FB 2634 - starts
                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                Double setupDuration = Double.MinValue;//FB 2634-K
                Double tearDuration = Double.MinValue;

                if (!chkEnableBuffer.Checked && enableBufferZone == "1")//FB 2398
                {
                    Double.TryParse(SetupDuration.Text, out setupDuration);
                    Double.TryParse(TearDownDuration.Text, out tearDuration);
                    dStart = dStart.AddMinutes(-setupDuration);
                    dEnd = dEnd.AddMinutes(tearDuration);
                }

                txtHKStartByDate.Text = dStart.ToString(format);
                startByTimeHK.Text = myVRMNet.NETFunctions.GetFormattedTime(dStart.ToString("HH:mm"), Session["timeFormat"].ToString());
                //txtHKCompletedBy.Text = confEndDate.Text;
                //completedByTimeHK.Text = confEndTime.Text;


                txtHKCompletedBy.Text = dEnd.ToString(format);
                completedByTimeHK.Text = myVRMNet.NETFunctions.GetFormattedTime(dEnd.ToString("HH:mm"), Session["timeFormat"].ToString());
                //txthkstartbydate.text = confstartdate.text;
                //startbytimehk.text = confstarttime.text;

                //FB 2634 - End
                
                chkHKNotify.Checked = true;
                lstHKTimezone.ClearSelection();
                lstHKTimezone.Items.FindByValue(lstConferenceTZ.SelectedValue).Selected = true;
                lblHKWOInstructions.Text = obj.GetTranslatedText("Please select a Room first from the drop down below.") + System.Environment.NewLine + obj.GetTranslatedText("If there are no rooms in the list, click on 'Select Rooms' option available on your left.") + System.Environment.NewLine + obj.GetTranslatedText("Click 'Cancel' before you proceed to another step.");//FB 1830 - Translation
                if (lstHKRooms.Items.Count > 1)
                {
                    lstHKRooms.ClearSelection();
                    lstHKRooms.Items[1].Selected = true;
                    //code added for FB:1087 
                    GetRoomLayouts(lstHKRooms.SelectedValue);
                    GetRoomHKSets(lstHKRooms.SelectedValue);
                    if (lstHKSet.Items.Count > 1)
                    {
                        lstHKSet.ClearSelection();
                        lstHKSet.Items[1].Selected = true;
                        GetHKSetItems(lstHKSet.SelectedValue);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region LoadDataGrid

        private void LoadDataGrid(XmlNodeList nodes, string Type)
        {
            try
            {

                XmlTextReader xtr;
                dsInv = new DataSet();//Code added for WO bug
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    dsInv.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                DataView dv = new DataView();
                DataTable dt = new DataTable();

                if (dsInv.Tables.Count > 0)
                {
                    dt = dsInv.Tables[0];
                    //if (!dt.Columns.Contains("Type")) dt.Columns.Add("Type");
                    //foreach (DataRow dr in dt.Rows)
                    //    dr["Type"] = txtType.Text;
                    dsInv.Tables[0].TableName = "Items"; //Item_Id
                    if (Type.Equals("1"))
                    {
                        dsInv.Tables[1].TableName = "Charges"; // Item_Id, Charges_Id
                        dsInv.Tables[2].TableName = "Charge"; // Charges_Id 
                    }
                    if (!dt.Columns.Contains("UID"))
                        dt.Columns.Add("UID");
                    if (!dt.Columns.Contains("QuantityRequested")) dt.Columns.Add("QuantityRequested");
                    if (!dt.Columns.Contains("DeliveryType")) dt.Columns.Add("DeliveryType");
                    if (!dt.Columns.Contains("DeliveryCost")) dt.Columns.Add("DeliveryCost");
                    if (!dt.Columns.Contains("ServiceCharge")) dt.Columns.Add("ServiceCharge");

                    Int32 realtimecnt = 0;

                    foreach (DataRow dr in dt.Rows)
                    {
                        realtimecnt = 0;

                        if(dr["Quantity"].ToString().Trim() != "")
                            realtimecnt = Convert.ToInt32(dr["Quantity"].ToString());
                        dr["Quantity"] = realtimecnt - checkrealtimecount(dr["ID"].ToString());

                        if (dr["QuantityRequested"].ToString().Equals(""))
                            dr["QuantityRequested"] = "0"; //WO Issues - Allow quantity to be zero

                        // FB 1686 - Start
                        if (!dr["DeliveryType"].ToString().Equals("") || !dr["DeliveryType"].ToString().Equals("0") || !dr["DeliveryType"].ToString().Equals("-1"))
                            dr["DeliveryType"] = lstDeliveryType.Items.FindByValue(dr["DeliveryType"].ToString());
                        else
                            dr["DeliveryType"] = "-1";
                        //FB 1830 - starts
                        tmpVal = 0;
                        //decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                        decimal.TryParse(dr["ServiceCharge"].ToString(), NumberStyles.Any, cInfo, out tmpVal);
                        dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);
                        tmpVal = 0;
                       // if (dr["ServiceCharge"].ToString().Equals(""))
                       //     dr["ServiceCharge"] = "0.00";
                       // else
                       //     dr["ServiceCharge"] = Decimal.Parse(dr["ServiceCharge"].ToString()).ToString("0.00");
                        //decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                        decimal.TryParse(dr["DeliveryCost"].ToString(), NumberStyles.Any, cInfo, out tmpVal);
                        dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);
                        tmpVal = 0;
                        //if (dr["DeliveryCost"].ToString().Equals(""))
                        //    dr["DeliveryCost"] = "0.00";
                        //else
                        //    dr["DeliveryCost"] = Decimal.Parse(dr["DeliveryCost"].ToString()).ToString("0.00");

                        decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                        dr["Price"] = tmpVal.ToString("n", cInfo);
                        tmpVal = 0;
                        //if (dr["Price"].ToString().Equals(""))
                       //     dr["Price"] = Decimal.Parse("0.00").ToString("n", cInfo);
                       // else
                       //     dr["Price"] = Decimal.Parse(dr["Price"].ToString()).ToString("n", cInfo);
                        //  FB 1686 - End
                        //FB 1830 - Ends
                        if (dr["UID"].ToString().Trim().Equals(""))
                            dr["UID"] = "0";
                        //Response.Write("<br>" + dr["DeliveryType"].ToString());
                    }
                }
                //                Response.Write(dt.Rows.Count);
                //lblInstructions.Text = "Enter requested quantity for corresponding items.";
                itemsGrid.Visible = true;
                btnSubmit.Enabled = true;
                itemsGrid.DataSource = dsInv;
                itemsGrid.DataBind();
                Session.Add("DS", dsInv);
                ChangeDeliveryType(lstDeliveryType, new EventArgs());
                if (Type.ToString().Equals("1"))
                {
                    itemsGrid.DataSource = dt;
                    itemsGrid.DataBind();
                    itemsGrid.Visible = true;
                }
                else if (Type.ToString().Equals("2"))
                {
                    //itemsGridCAT.DataSource = dt;
                    //itemsGridCAT.DataBind();
                    //itemsGridCAT.Visible = true;
                }
                else if (Type.ToString().Equals("3"))
                {
                    itemsGridHK.DataSource = dt;
                    itemsGridHK.DataBind();
                    itemsGridHK.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region ChangeDeliveryType

        protected void ChangeDeliveryType(Object sender, EventArgs e)
        {
            try
            {
                dsInv = (DataSet)Session["DS"];
                if (!lstDeliveryType.SelectedValue.Equals("-1"))
                {
                    foreach (DataGridItem dgi in itemsGrid.Items)
                    {
                        if (lstDeliveryType.SelectedValue.Equals("0"))
                        {
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Enabled = true;
                        }
                        else
                        {
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Items.FindByValue(lstDeliveryType.SelectedValue).Selected = true;
                            ChangeDeliveryTypeItem((DropDownList)dgi.FindControl("lstDeliveryTypeItem"), e);
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Enabled = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region ChangeDeliveryTypeItem

        protected void ChangeDeliveryTypeItem(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    //Response.Write(lstTemp.ClientID + " : " + ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClientID);
                    if (lstTemp.ClientID.Equals(((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClientID))
                    {
                        //Response.Write("in ChangeDeliveryTypeItem");
                        DropDownList lstTempItem = (DropDownList)dgi.FindControl("lstDeliveryCost");
                        lstTempItem.ClearSelection();
                        lstTempItem.Items.FindByValue(lstTemp.SelectedValue).Selected = true;
                        Label lblTemp = (Label)dgi.FindControl("lblDeliveryCost");
                        lblTemp.Text = lstTempItem.SelectedItem.Text;
                        //FB 1830
                        tmpVal = 0;
                        decimal.TryParse(lblTemp.Text, out tmpVal);
                        lblTemp.Text = tmpVal.ToString("n", cInfo);

                        lstTempItem = (DropDownList)dgi.FindControl("lstServiceCharge");
                        lstTempItem.ClearSelection();
                        lstTempItem.Items.FindByValue(lstTemp.SelectedValue).Selected = true;
                        lblTemp = (Label)dgi.FindControl("lblServiceCharge");
                        lblTemp.Text = lstTempItem.SelectedItem.Text;
                        //FB 1830
                        tmpVal = 0;
                        decimal.TryParse(lblTemp.Text, out tmpVal);
                        lblTemp.Text = tmpVal.ToString("n", cInfo);

                        UpdateTotalAV(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region SetDeliveryAttributes

        protected void SetDeliveryAttributes(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType.Equals(ListItemType.Item)) || (e.Item.ItemType.Equals(ListItemType.AlternatingItem)))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    string imCont = "";
                    string finame = "";
                    string fullPath = "";
                    byte[] imgArray = null;

                    if (row["ImageName"] != null)
                        finame = row["ImageName"].ToString().Trim();

                    if (row["Image"] != null)
                        imCont = row["Image"].ToString().Trim();

                    string pathName = "resource";

                    fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/image/" + pathName + "/" + finame;//FB 1830

                    if (imCont != "")
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imCont);

                        if (File.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame))
                            File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame);

                        WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame, ref imgArray);

                        Image imgContrl = (Image)e.Item.FindControl("imgItem");
                        imgContrl.ImageUrl = fullPath;
                    }


                    String ItemID = e.Item.Cells[0].Text;
                    //Response.Write("<br>ItemID: " + ItemID);
                    DataView charges = new DataView();
                    DataView charge = new DataView();
                    charges = dsInv.Tables["Charges"].DefaultView;
                    charge = dsInv.Tables["Charge"].DefaultView;
                    charges.RowFilter = "Item_Id='" + Int32.Parse(ItemID) + "'";
                    charge.RowFilter = "Charges_Id='" + e.Item.ItemIndex + "' AND NOT(DeliveryTypeID ='0')";
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).Items.Clear();
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).DataSource = charge;
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).DataBind();
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).Items.Insert(0, (new ListItem("0.0", "-1")));
                    ((DropDownList)e.Item.FindControl("lstServiceCharge")).DataSource = charge;
                    ((DropDownList)e.Item.FindControl("lstServiceCharge")).DataBind();
                    ((DropDownList)e.Item.FindControl("lstServiceCharge")).Items.Insert(0, (new ListItem("0.0", "-1")));
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region LoadDataGridMain

        private void LoadDataGridMain(XmlNodeList nodes, string Type, DataGrid dgMainGrid)
        {
            try
            {
                XmlTextReader xtr;
                DataSet dsMain = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    dsMain.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                //DataView dv;
                DataTable dt;
                if (dsMain.Tables.Count > 0)
                {
                    //dv = new DataView(ds.Tables[0]);
                    dt = dsMain.Tables[0]; // dv.Table;
                }
                else
                {
                    //dv = new DataView();
                    dt = new DataTable();
                }


                if (dt.Columns.Contains("AssignedToName").Equals(false))
                {
                    dt.Columns.Add("AssignedToName");
                }
                if (dt.Columns.Contains("RoomLayout").Equals(false))
                {
                    dt.Columns.Add("RoomLayout");
                }
                if (dt.Columns.Contains("ReqQuantity").Equals(false))
                {
                    dt.Columns.Add("ReqQuantity");
                }
                if (!dt.Columns.Contains("RowUID"))  //FB 498 work order fixes
                {
                    dt.Columns.Add("RowUID");
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["RowUID"] = i;   //FB 498

                    dt.Rows[i]["ReqQuantity"] = GetWorkOrderDetails(dt.Rows[i]["ID"].ToString(), Type);
                    if (dt.Rows[i]["Status"].Equals("0"))
                        dt.Rows[i]["Status"] = "Pending";
                    else
                        dt.Rows[i]["Status"] = "Completed";
                    if (Request.QueryString["t"].ToString().Trim().ToUpper().Equals("O"))
                        dt.Rows[i]["ID"] = "new";
                    //Code added by Off shore for FB Issue 1073

                    if (dt.Columns.Contains("StartByDate"))
                        dt.Rows[i]["StartByDate"] = DateTime.Parse(dt.Rows[i]["StartByDate"].ToString()).ToString(format);

                    if (dt.Columns.Contains("CompletedByDate"))
                        dt.Rows[i]["CompletedByDate"] = DateTime.Parse(dt.Rows[i]["CompletedByDate"].ToString()).ToString(format);

                    //FB 1686 & WO Bug Fix
                    if (dt.Columns.Contains("StartByTime"))
                        dt.Rows[i]["StartByTime"] = DateTime.Parse(dt.Rows[i]["StartByTime"].ToString()).ToString(tformat);

                    if (dt.Columns.Contains("CompletedByTime"))
                        dt.Rows[i]["CompletedByTime"] = DateTime.Parse(dt.Rows[i]["CompletedByTime"].ToString()).ToString(tformat);
                    //FB 1830 - Start
                    decimal.TryParse(dt.Rows[i]["ServiceCharge"].ToString(), out tmpVal);
                    dt.Rows[i]["ServiceCharge"] = tmpVal.ToString("n", cInfo);
                    tmpVal = 0;
                    //if (dt.Columns.Contains("ServiceCharge"))
                    //    dt.Rows[i]["ServiceCharge"] = Decimal.Parse(dt.Rows[i]["ServiceCharge"].ToString()).ToString("0.00");
                    decimal.TryParse(dt.Rows[i]["DeliveryCost"].ToString(), out tmpVal);
                    dt.Rows[i]["DeliveryCost"] = tmpVal.ToString("n", cInfo);
                    tmpVal = 0;
                    //if (dt.Columns.Contains("DeliveryCost"))
                    //    dt.Rows[i]["DeliveryCost"] = Decimal.Parse(dt.Rows[i]["DeliveryCost"].ToString()).ToString("0.00");
                    decimal.TryParse(dt.Rows[i]["TotalCost"].ToString(), out tmpVal);
                    dt.Rows[i]["TotalCost"] = tmpVal.ToString("n", cInfo);
                    tmpVal = 0;
                    //if (dt.Columns.Contains("TotalCost"))
                    //    dt.Rows[i]["TotalCost"] = Decimal.Parse(dt.Rows[i]["TotalCost"].ToString()).ToString("0.00");
                    //FB 1830 - End
                    //Code added by Off shore for FB Issue 1073
                }
                if (Type.Equals("1"))
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        Session.Add("AVMainGridDS", dt); // FB 2050
                    }
                    else
                    {
                        ViewState.Add("AVMainGridDS", dt); // FB 2050
                    }
                //else if (Type.Equals("2"))
                //    Session.Add("CATMainGridDS", dt);
                else if (Type.Equals("3"))
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        Session.Add("HKMainGridDS", dt); // FB 2050
                    }
                    else
                    {
                        ViewState.Add("HKMainGridDS", dt); // FB 2050
                    }

                dgMainGrid.DataSource = dt;
                dgMainGrid.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        #endregion

        #region GetWorkOrderDetails

        protected String GetWorkOrderDetails(String woID, String type)
        {
            try
            {
                String reqQ = "";
                String inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><ConfID>" + lblConfID.Text + "</ConfID><WorkorderID>" + woID + "</WorkorderID></login>";//Organization Module Fixes
                String outxml = obj.CallMyVRMServer("GetWorkOrderDetails", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outxml));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outxml);
                XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrder/ItemList/Item");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        //FB 1830
                        if (type == "1")
                        {
                            //Response.Write(Request.QueryString["t"].ToString().Trim().ToUpper());
                            if (Request.QueryString["t"].ToString().Trim().ToUpper().Equals("O"))
                                reqQ += node.SelectSingleNode("ID").InnerText + "�0�" + node.SelectSingleNode("QuantityRequested").InnerText + "�";
                            else
                                reqQ += node.SelectSingleNode("ID").InnerText + "�" + node.SelectSingleNode("UID").InnerText + "�" + node.SelectSingleNode("QuantityRequested").InnerText + "�";
                            reqQ += node.SelectSingleNode("DeliveryCost").InnerText + "�" + node.SelectSingleNode("ServiceCharge").InnerText + "�" + node.SelectSingleNode("DeliveryType").InnerText + ";";
                        }
                        else
                        {
                            if (Request.QueryString["t"].ToString().Trim().ToUpper().Equals("O"))
                                reqQ += node.SelectSingleNode("ID").InnerText + ",0," + node.SelectSingleNode("QuantityRequested").InnerText + ",";
                            else
                                reqQ += node.SelectSingleNode("ID").InnerText + "," + node.SelectSingleNode("UID").InnerText + "," + node.SelectSingleNode("QuantityRequested").InnerText + ",";
                            reqQ += node.SelectSingleNode("DeliveryCost").InnerText + "," + node.SelectSingleNode("ServiceCharge").InnerText + "," + node.SelectSingleNode("DeliveryType").InnerText + ";";
                        }
                    }
                }
                return reqQ;
            }

            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return "";
            }

        }

        #endregion

        #region AVMainGrid_Edit

        protected void AVMainGrid_Edit(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("AVEditColumn", e.Item.ItemIndex);

                DataTable myTable;
                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                {
                    myTable = (DataTable)Session["AVMainGridDS"]; // FB 2050
                }
                else
                {
                    myTable = (DataTable)ViewState["AVMainGridDS"]; // FB 2050
                }

                DataRowCollection myRows = myTable.Rows;
                DataRow dr = myTable.Rows[e.Item.ItemIndex];
                btnAddNewAV.Enabled = false;
                btnAddNewAV.Attributes.Add("Class", "btndisable"); //FB 2664
                AVItemsTable.Visible = true;
                AVMainGrid.Visible = false;
                btnSubmit.Text = obj.GetTranslatedText("Edit");//FB 1830 - Traslation
                txtWorkOrderID.Text = dr["ID"].ToString();
                txtWorkOrderName.Text = dr["Name"].ToString();
                lstRooms.ClearSelection();
                try
                {
                    lstRooms.Items.FindByValue(dr["RoomID"].ToString()).Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace + " : " + ex.Message);
                    //Added for FB 1428 START
                    if (client.ToString().ToUpper() == "MOJ")//Added for MOJ
                        errLabel.Text = "Either you are not entitled to use the selected room or room no longer belongs to the hearing.";
                    else
                        //Added for FB 1428 End
                        errLabel.Text = obj.GetTranslatedText("Either you are not entitled to use the selected room or room no longer belongs to the conference.");//FB 1830 - Traslation
                    errLabel.Visible = true;
                }
                GetRoomSets(dr["RoomID"].ToString());
                lstAVSet.ClearSelection();
                try
                {
                    lstAVSet.Items.FindByValue(dr["SetID"].ToString()).Selected = true;
                    //selAVSet_SelectedIndexChanged(null,null);//Code added for WO bug
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace + " : " + ex.Message);
                    errLabel.Text = obj.GetTranslatedText("This audiovisual inventories no longer belongs to selected room.");//FB 1830 - Traslation  FB 2570
                    errLabel.Visible = true;
                }
                lstStatus.ClearSelection();
                lstStatus.Items.FindByText(dr["Status"].ToString()).Selected = true;
                if (dr["Notify"].Equals("1"))
                    chkNotify.Checked = true;
                else
                    chkNotify.Checked = false;
                /* Case 358 */
                //FB 1830
                decimal.TryParse(dr["ServiceCharge"].ToString(), NumberStyles.Any, cInfo, out tmpVal);
                //FB 1830
                txtServiceCharges.Text = tmpVal.ToString("g",cInfo);
                //txtServiceCharges.Text = dr["ServiceCharge"].ToString();
                decimal.TryParse(dr["DeliveryCost"].ToString(), NumberStyles.Any, cInfo, out tmpVal);
                txtDeliveryCost.Text = tmpVal.ToString("g",cInfo);
                //txtDeliveryCost.Text = dr["DeliveryCost"].ToString();
                /* Case 358 Ends here */
                txtComments.Text = dr["Comments"].ToString();
                //Code Changed FB Issue 1073 - Start
                txtStartByDate.Text = dr["StartByDate"].ToString(); 
                startByTime.Text = myVRMNet.NETFunctions.GetFormattedTime(dr["StartByTime"].ToString(),Session["timeFormat"].ToString());
                completedByTime.Text = myVRMNet.NETFunctions.GetFormattedTime(dr["CompletedByTime"].ToString(), Session["timeFormat"].ToString());
                txtCompletedBy.Text = dr["CompletedByDate"].ToString();
                //Code Changed FB Issue 1073 - End
                lstDeliveryType.ClearSelection();
                lstDeliveryType.Items.FindByValue(dr["DeliveryType"].ToString()).Selected = true;
                lblAVWOInstructions.Text = obj.GetTranslatedText("Click Update to re-calculate total OR click Edit to update this Workorder.");//FB 1830 - Translation

                GetAVSetItems(dr["SetID"].ToString());
                //Response.Write(dr["AssignedToName"].ToString());
                hdnApprover1.Text = dr["AssignedToID"].ToString();
                dr["AssignedToName"] = obj.GetMyVRMUserName(hdnApprover1.Text);
                txtApprover1.Text = dr["AssignedToName"].ToString();
                //1,Table,1;2,Chair,0;3,Plasma TV,0; 
                //Code added for WO bug - FB 322
                for (int i = 0; i < dr["ReqQuantity"].ToString().Split(';').Length; i++)
                {    //FB 1830 - Starts (� - Alt 147)
                    foreach (DataGridItem dgi in itemsGrid.Items)
                    {
                        if (dr["ReqQuantity"].ToString().Split(';')[i].Split('�').Length > 7)
                        {
                            if (dgi.Cells[1].Text.Equals(dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[7]))
                            {
                                ((TextBox)dgi.FindControl("txtReqQuantity")).Text = dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[2];
                                dgi.Cells[dgi.Cells.Count - 1].Text = dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[1];
                                ((RangeValidator)dgi.FindControl("validateQuantityRange")).MaximumValue = dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[6];
                                dgi.Cells[6].Text = dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[6];
                                ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Items.FindByValue(dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[5]).Selected = true;
                                ChangeDeliveryTypeItem(((DropDownList)dgi.FindControl("lstDeliveryTypeItem")), new EventArgs());
                            }
                        }
                        else
                        {
                            if (dgi.Cells[0].Text.Equals(dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[0]))
                            {
                                ((TextBox)dgi.FindControl("txtReqQuantity")).Text = dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[2];
                                dgi.Cells[dgi.Cells.Count - 1].Text = dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[1];
                                ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Items.FindByValue(dr["ReqQuantity"].ToString().Split(';')[i].Split('�')[5]).Selected = true;
                                ChangeDeliveryTypeItem(((DropDownList)dgi.FindControl("lstDeliveryTypeItem")), new EventArgs());
                            } //FB 1830 - End
                        }
                    }
                }
                UpdateTotalAV(new object(), new EventArgs());
                lstTimezones.ClearSelection();
                lstTimezones.Items.FindByValue(lstConferenceTZ.SelectedValue).Selected = true;
                AVMainGrid.Visible = false;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("AVMainGrid_Edit" + ex.Message + " : " + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = (ex.Message + " : " + ex.StackTrace);
            }
        }

        #endregion

        #region AVMainGrid_Update

        protected void AVMainGrid_Update(object sender, DataGridCommandEventArgs e)
        {
            //Response.Write("here");
            btnAddNewAV.Enabled = true;
            btnAddNewAV.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
            AVItemsTable.Visible = true;
            btnSubmit.Text = obj.GetTranslatedText("Update");//FB 1830 - Translation
        }

        #endregion

        #region AVMainGrid_Cancel

        protected void AVMainGrid_Cancel(object sender, DataGridCommandEventArgs e)
        {
            //Response.Write("here");
            btnAddNewAV.Enabled = true;
            btnAddNewAV.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
            AVItemsTable.Visible = true;
            btnSubmit.Text = obj.GetTranslatedText("Cancel");//FB 1830 - Translation
        }

        #endregion

        #region CreateWorkorderTable

        protected DataTable CreateWorkorderTable()
        {
            try
            {
                DataTable dt = new DataTable();
                if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
                if (!dt.Columns.Contains("Name")) dt.Columns.Add("Name");
                if (!dt.Columns.Contains("RoomId")) dt.Columns.Add("RoomId");
                if (!dt.Columns.Contains("SelectedService")) dt.Columns.Add("SelectedService");
                if (!dt.Columns.Contains("ServiceName")) dt.Columns.Add("ServiceName");
                if (!dt.Columns.Contains("RoomName")) dt.Columns.Add("RoomName");
                if (!dt.Columns.Contains("strMenus")) dt.Columns.Add("strMenus");
                if (!dt.Columns.Contains("Comments")) dt.Columns.Add("Comments");
                if (!dt.Columns.Contains("Price")) dt.Columns.Add("Price");
                if (!dt.Columns.Contains("DeliverByDate")) dt.Columns.Add("DeliverByDate");
                if (!dt.Columns.Contains("DeliverByTime")) dt.Columns.Add("DeliverByTime");
                return dt;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return null;
            }
        }

        #endregion

        #region AddBlankWorkorder

        protected void AddBlankWorkorder()
        {
            try
            {
                //lblCATWOInstructions.Visible = false;
                DataTable dt = new DataTable();
                dt = CreateWorkorderTable();

                DataRow dr = dt.NewRow();
                dr["ID"] = "new";
                dr["Name"] = ConferenceName.Text + "_CAT_" + (CATMainGrid.Items.Count + 1);
                SyncRoomSelection();
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    if (tn.Depth.Equals(3))
                    {
                        dr["RoomId"] = tn.Value;
                        dr["RoomName"] = tn.Text;
                    }
                dr["SelectedService"] = "1";
                dr["ServiceName"] = "";
                
				//FB 1830
                tmpVal = 0;
                decimal.TryParse("0.00", out tmpVal);
                dr["Price"] = tmpVal.ToString("n", cInfo);
                //dr["Price"] = "0.00";
                
				dr["Comments"] = "";
                dr["strMenus"] = "";

                //FB 2634 - starts
                DateTime dStart, dEnd;//FB 2634
                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                Double setupDuration = Double.MinValue;//FB 2634-K
                Double tearDuration = Double.MinValue;

                if (!chkEnableBuffer.Checked && enableBufferZone == "1")//FB 2398
                {
                    Double.TryParse(SetupDuration.Text, out setupDuration);
                    Double.TryParse(TearDownDuration.Text, out tearDuration);
                    dStart = dStart.AddMinutes(-setupDuration);
                    dEnd = dEnd.AddMinutes(tearDuration);
                }

                //dr["DeliverByDate"] = confStartDate.Text;
                //dr["DeliverByTime"] = confStartTime.Text;
                dr["DeliverByDate"] = dStart.ToString(format);
                dr["DeliverByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dStart.ToString("HH:mm"), Session["timeFormat"].ToString());

                //FB 2634 - End

                string DateTIme, Date, Time; //FB 2588

                dt.Rows.Add(dr);

                foreach (DataGridItem dgi in CATMainGrid.Items)
                {
                    dr = dt.NewRow();

                    DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                    Date = DateTIme.Split(' ')[0];
                    if (Session["timeFormat"].ToString().Equals("2"))//FB 2281
                        Time = DateTIme.Split(' ')[1];
                    else
                        Time = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(DateTIme)).ToString(tformat);

                    dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                    dr["Name"] = dgi.Cells[1].Text;
                    dr["RoomId"] = dgi.Cells[2].Text;
                    dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                    dr["SelectedService"] = dgi.Cells[3].Text;
                    dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;
                    dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                    dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;
                    dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                    //Code changed by offshore for FB Issue 1073 -- start
                    //dr["DeliverByDate"] = DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("MM/dd/yyyy");

                    dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format); //FB 2588
                    dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(tformat);

                    //dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((Label)dgi.FindControl("lblDeliverByDateTime")).Text)).ToString(format);
                    //dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((Label)dgi.FindControl("lblDeliverByDateTime")).Text)).ToString(tformat);
                    //Code changed by offshore for FB Issue 1073 -- end
                    dt.Rows.Add(dr);
                }
                CATMainGrid.DataSource = dt;
                CATMainGrid.DataBind();
                EditWorkorder(new object(), new DataGridCommandEventArgs(CATMainGrid.Items[0], (LinkButton)CATMainGrid.Items[0].FindControl("btnEdit"), new CommandEventArgs("Edit", "2")));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region DeleteWorkorder

        protected void DeleteWorkorder(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string DateTIme, Date, Time; //FB 2588
                if (CATMainGrid.EditItemIndex.Equals(-1))
                {
                    //code added for FB 1128
                    if (CATMainGrid.Items[e.Item.ItemIndex].Cells[0].Text != "new")
                        DeleteWorkOrder(CATMainGrid.Items[e.Item.ItemIndex].Cells[0].Text);

                    btnAddNewCAT.Visible = true;
                    DataTable dt = CreateWorkorderTable();
                    //preserve all orkorders other than the one which is requested for deletion
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        if (!dgi.ItemIndex.Equals(e.Item.ItemIndex))
                        {
                            DataRow dr = dt.NewRow();

                            DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                            Date = DateTIme.Split(' ')[0];
                            if (Session["timeFormat"].ToString().Equals("2"))//FB 2281
                                Time = DateTIme.Split(' ')[1];
                            else
                                Time = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(DateTIme)).ToString(tformat);

                            dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                            dr["Name"] = dgi.Cells[1].Text;
                            dr["RoomId"] = dgi.Cells[2].Text;
                            dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                            dr["SelectedService"] = dgi.Cells[3].Text;
                            dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;
                            dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;
                            dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                            dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                            //Code changed by offshore for FB Issue 1073 -- start
                            //dr["DeliverByDate"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("MM/dd/yyyy");
                            //dr["DeliverByTime"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("hh:mm tt");
                            
                            //dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text)).ToString(format);
                            //dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text)).ToString(tformat);
                            //Code changed by offshore for FB Issue 1073 -- end
                            dt.Rows.Add(dr);
                        }
                    }
                    CATMainGrid.DataSource = dt;
                    CATMainGrid.DataBind();
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        String strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        DataTable dtMenus = new DataTable();
                        if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                        if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                        if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                        for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                        {
                            DataRow drMenu = dtMenus.NewRow();
                            drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                            drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                            drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                            dtMenus.Rows.Add(drMenu);
                        }
                        DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        dgCateringMenus.DataSource = dtMenus;
                        dgCateringMenus.DataBind();
                    }
                    if (dt.Rows.Count.Equals(0))
                    {
                        CATMainGrid.Visible = false;
                        btnAddNewCAT.Visible = true;
                        lblCATWOInstructions.Visible = true;
                        lblCATWOInstructions.Text = obj.GetTranslatedText("To create a new work order, please click on the button below.");//FB 1830 - Traslation
                    }
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Please save or cancel current workorder prior to editing or deleting existing workorders.");//FB 1830 - Traslation
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        #endregion

        #region LoadCateringServices

        protected void LoadCateringServices(Object sender, EventArgs e)
        {
            DropDownList lstServices = (DropDownList)sender;
            obj.GetCateringServices(lstServices);
        }

        #endregion

        #region LoadRooms

        protected void LoadRooms(Object sender, EventArgs e)
        {
            DropDownList lstRooms = (DropDownList)sender;
            foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                if (tn.Depth.Equals(3))
                    lstRooms.Items.Add(new ListItem(tn.Text, tn.Value));
        }

        #endregion

        #region EditWorkorder

        protected void EditWorkorder(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                if (CATMainGrid.EditItemIndex.Equals(-1))
                {
                    string DateTIme,Date,Time; //FB 2588
                   

                    btnAddNewCAT.Visible = false;
                    DataTable dt = CreateWorkorderTable();
                    log.Trace("in EditWorkorder" + e.Item.ItemIndex);
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        DataRow dr = dt.NewRow();

                        DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                        Date = DateTIme.Split(' ')[0];
                        if (Session["timeFormat"].ToString().Equals("2"))//FB 2281
                            Time = DateTIme.Split(' ')[1];
                        else
                            Time = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(DateTIme)).ToString(tformat);

                        dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                        dr["Name"] = dgi.Cells[1].Text;
                        dr["RoomId"] = dgi.Cells[2].Text;
                        dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                        dr["SelectedService"] = dgi.Cells[3].Text;
                        dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;
                        dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;
                        dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                        dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        //Code Changed by OffShore FOr FB Issue 1073 -- Start

                        dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format); //FB 2588
                        dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(tformat);

                        //dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate((((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text))).ToString(tformat);
                        //dr["DeliverByDate"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("MM/dd/yyyy");
                        //dr["DeliverByTime"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("hh:mm tt");
                        //Code Changed by OffShore FOr FB Issue 1073 -- End
                        dt.Rows.Add(dr);
                    }
                    CATMainGrid.EditItemIndex = e.Item.ItemIndex;
                    CATMainGrid.DataSource = dt;
                    CATMainGrid.DataBind();
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        // FB 1492 Work order fixes - Start
                        RegularExpressionValidator1 = (RegularExpressionValidator)dgi.FindControl("RegularExpressionValidator1");
                        MetaBuilders.WebControls.ComboBox lstDeliverByTime = ((MetaBuilders.WebControls.ComboBox)dgi.FindControl("lstDeliverByTime"));
                        if (lstDeliverByTime != null)//FB 2181
                        {
                            lstDeliverByTime.Items.Clear();
                            obj.BindTimeToListBox(lstDeliverByTime, true, true);
                            if (Session["timeFormat"] != null)
                                if (Session["timeFormat"].ToString().Equals("0"))
                                {
                                    RegularExpressionValidator1.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    RegularExpressionValidator1.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                                }
                                else if (Session["timeFormat"].ToString().Equals("2"))
                                {
                                    RegularExpressionValidator1.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                                    RegularExpressionValidator1.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                                }
                        }

                        // FB 1492 Work order fixes - End

                        if (!dgi.ItemIndex.Equals(e.Item.ItemIndex))
                        {
                            String strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                            DataTable dtMenus = new DataTable();
                            if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                            if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                            if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                            for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                            {
                                DataRow drMenu = dtMenus.NewRow();
                                drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                                drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                                drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                                dtMenus.Rows.Add(drMenu);
                            }
                            DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                            dgCateringMenus.DataSource = dtMenus;
                            dgCateringMenus.DataBind();
                        }
                    }
                    CATMainGrid.Visible = true;
                    //Added for FB 1428 START
                    if (client.ToString().ToUpper() == "MOJ")//Added for MOJ
                        lblCATWOInstructions.Text = "Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this hearing.";
                    else
                        //Added for FB 1428 End
                    lblCATWOInstructions.Text = obj.GetTranslatedText("Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this conference.");//FB 1830 - Translation
                    UpdateMenus(e.Item.FindControl("lstRooms"), new EventArgs());
                    log.Trace("finished EditWorkorder");
                    Session["CATMainGridDS"] = dt;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Please save or cancel current workorder prior to editing or deleting existing workorders.");//FB 1830 - Translation
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        #endregion

        #region UpdateMenus

        protected void UpdateMenus(Object sender, EventArgs e)
        {
            try
            {
                log.Trace("CATMainGrid.EditItemIndex: " + CATMainGrid.EditItemIndex);
                DropDownList lstRooms = (DropDownList)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lstRooms");
                DropDownList lstServices = (DropDownList)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lstServices");
                String inXML = objInXML.SearchProviderMenus(Session["userID"].ToString(), lstRooms.SelectedValue, lstServices.SelectedValue);
                log.Trace("SearchProviderMenus Inxml: " + inXML);
                if (inXML != "")
                {
                    String outXML = obj.CallMyVRMServer("SearchProviderMenus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    //outXML = "<SearchProviderMenus><Menu><ID>1</ID><Name>Menu 1</Name><Price>10.00</Price></Menu><Menu><ID>2</ID><Name>Menu 2</Name><Price>10.00</Price></Menu><Menu><ID>3</ID><Name>Menu 3</Name><Price>10.00</Price></Menu></SearchProviderMenus>";
                    log.Trace("SearchProviderMenus Outxml: " + outXML);
                    if (outXML.IndexOf("<error>") >= 0) //in case of error
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//SearchProviderMenus/Menu");
                        XmlTextReader xtr;
                        DataSet dsMenus = new DataSet();
                        foreach (XmlNode node in nodes)
                        {
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            dsMenus.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                        DataTable dtMenu = new DataTable();
                        DataGrid dgMenus = (DataGrid)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("dgCateringMenus");
                        if (dsMenus.Tables.Count > 0)
                        {
                            dtMenu = dsMenus.Tables[0];
                            if (!dtMenu.Columns.Contains("Quantity")) dtMenu.Columns.Add("Quantity");
                            log.Trace(((DataGrid)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("dgCateringMenus")).ClientID);
                            dgMenus.DataSource = dtMenu;
                            dgMenus.DataBind();
                            ((Label)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lblNoMenus")).Visible = false;
                            ((LinkButton)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("btnUpdate")).Visible = true;
                            //Added for FB 1428 START
                            if (client.ToString().ToUpper() == "MOJ")//Added for MOJ
                                lblCATWOInstructions.Text = "Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this hearing.";
                            else
                                //Added for FB 1428 End
                                lblCATWOInstructions.Text = obj.GetTranslatedText("Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this conference.");//FB 1830 - Translation
                            String strMenus = ((Label)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lblCateringMenus")).Text;
                            foreach (DataGridItem dgiMenu in dgMenus.Items)
                            {
                                for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                                {
                                    if (dgiMenu.Cells[0].Text.Trim().Equals(strMenus.Split(';')[i].Split(':')[0]))
                                    {
                                        ((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked = true;
                                        ((TextBox)dgiMenu.FindControl("txtQuantity")).Text = strMenus.Split(';')[i].Split(':')[2];
                                    }
                                }
                                //FB 2181 Start
                                if (((TextBox)dgiMenu.FindControl("txtQuantity")).Text != "")
                                {
                                    ((LinkButton)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("btnUpdate")).Text = "Update";
                                }
                                //FB 2181 End
                                foreach (XmlNode node in nodes)
                                {
                                    if (node.SelectSingleNode("ID").InnerText.Equals(dgiMenu.Cells[0].Text))
                                    {
                                        DataGrid dgMenuItems = (DataGrid)dgiMenu.FindControl("dgMenuItems");
                                        XmlNodeList subNodes = node.SelectNodes("ItemsList/Item");
                                        DataSet dsItem = new DataSet();
                                        foreach (XmlNode subNode in subNodes)
                                        {
                                            xtr = new XmlTextReader(subNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                            dsItem.ReadXml(xtr, XmlReadMode.InferSchema);
                                        }
                                        if (dsItem.Tables.Count > 0)
                                        {
                                            dgMenuItems.DataSource = dsItem;
                                            dgMenuItems.DataBind();
                                            dgMenuItems.Attributes.Add("style", "display:none");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            dgMenus.DataSource = dtMenu;
                            dgMenus.DataBind();
                            ((LinkButton)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("btnUpdate")).Visible = false;
                            ((Label)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lblNoMenus")).Visible = true;
                            //Added for FB 1428 START
                            if (client.ToString().ToUpper() == "MOJ")//Added for MOJ
                                lblCATWOInstructions.Text = "There are no menus associated with the selected Service Type for this location. Please select a new Service Type for this location OR click Cancel to return to the catering work orders summary page for this hearing.";
                            else
                                //Added for FB 1428 End
                                lblCATWOInstructions.Text = obj.GetTranslatedText("There are no menus associated with the selected Service Type for this location. Please select a new Service Type for this location OR click Cancel to return to the catering work orders summary page for this conference.");//FB 1830 - Translation
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("UpdateMenus: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region UpdateWorkorder

        protected void UpdateWorkorder(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string DateTIme, Date, Time; //FB 2588
                DataTable dt = CreateWorkorderTable();
                log.Trace("In UpdateWorkorder");
                Boolean flagUpdate = true;
                foreach (DataGridItem dgi in CATMainGrid.Items)
                {
                    DataRow dr = dt.NewRow();
                    if (dgi.ItemIndex.Equals(CATMainGrid.EditItemIndex)) // if updating edited item 
                    {
                        dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                        dr["Name"] = dgi.Cells[1].Text;
                        dr["RoomId"] = ((DropDownList)dgi.FindControl("lstRooms")).SelectedValue;
                        dr["RoomName"] = ((DropDownList)dgi.FindControl("lstRooms")).SelectedItem.Text;
                        dr["SelectedService"] = ((DropDownList)dgi.FindControl("lstServices")).SelectedValue;
                        dr["ServiceName"] = ((DropDownList)dgi.FindControl("lstServices")).SelectedItem.Text;
                        dr["strMenus"] = "";
                        Double price = Double.Parse("0.00");
                        dr["Price"] = "0.00";
                        dr["Comments"] = ((TextBox)dgi.FindControl("txtComments")).Text;
                        DataGrid dgMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        Boolean flagMenu = true;
                        Boolean flagQuantity = true;
                        foreach (DataGridItem dgiMenu in dgMenus.Items)
                        {
                            flagMenu = true;
                            flagQuantity = true;
                            //WO Bug Fix
                            if (((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked)
                            {
                                if (((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Equals(""))
                                {
                                    flagMenu = false;
                                    break;
                                }
                                //Onsite dont want the 0 value validations
                                //else if (Convert.ToInt32(((TextBox)dgiMenu.FindControl("txtQuantity")).Text) <= 0)
                                //{
                                //    flagMenu = false;
                                //    break;
                                //}

                                dr["strMenus"] += dgiMenu.Cells[0].Text + ":" + ((Label)dgiMenu.FindControl("txtMenuName")).Text + ":" + ((TextBox)dgiMenu.FindControl("txtQuantity")).Text + ";";
                                price += Double.Parse(dgiMenu.Cells[1].Text) * Int32.Parse(((TextBox)dgiMenu.FindControl("txtQuantity")).Text);

                            }
                            else
                            {
                                if (((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked && ((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Equals(""))
                                // || ((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Trim().Equals("0")))  //Onsite dont want the 0 value validations
                                {
                                    flagMenu = false;
                                    break;
                                }
                                if (!((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked && !((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Equals(""))
                                {
                                    flagQuantity = false;
                                    break;
                                }
                            }
                        }
                        errLabel.Visible = true;
                        if (flagMenu.Equals(false))
                        {
                            errLabel.Text = obj.GetTranslatedText("At least one menu should have a requested quantity > 0."); //Code added for WO bug //FB 1830 - Translation
                            flagUpdate = false;
                            break;
                        }
                        if (flagQuantity.Equals(false))
                        {
                            errLabel.Text = obj.GetTranslatedText("Please select the corresponding menu for a valid quantity.");//FB 1830 - Translation
                            flagUpdate = false;
                            break;
                        }
                        if (dr["strMenus"].ToString().Equals(""))
                        {
                            errLabel.Text = obj.GetTranslatedText("Please associate at least one menu with each work order.");//FB 1830 - Translation
                            flagUpdate = false;
                            break;
                        }
                        //dr["Price"] = price.ToString("00.00"); //FB 1686
                        
						//FB 1830
                        tmpVal = 0;
                        decimal.TryParse(price.ToString(), out tmpVal);
                        dr["Price"] = tmpVal.ToString("n", cInfo);

                        //dr["Price"] = price.ToString("0.00"); //FB 1686
                        //Code Changed by OffShore FOr FB Issue 1073 -- Start
                        //dr["DeliverByDate"] = ((TextBox)dgi.FindControl("txtDeliverByDate")).Text;
                        dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((TextBox)dgi.FindControl("txtDeliverByDate")).Text)).ToString(format);
                        //Code Changed by OffShore FOr FB Issue 1073 -- End
                        dr["DeliverByTime"] = ((MetaBuilders.WebControls.ComboBox)dgi.FindControl("lstDeliverByTime")).Text;
                    }
                    else
                    {
                        DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                        Date = DateTIme.Split(' ')[0];
                        if (Session["timeFormat"].ToString().Equals("2"))//FB 2281
                            Time = DateTIme.Split(' ')[1];
                        else
                            Time = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(DateTIme)).ToString(tformat);

                        dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                        dr["Name"] = dgi.Cells[0].Text;
                        dr["RoomId"] = dgi.Cells[2].Text;
                        dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                        dr["SelectedService"] = dgi.Cells[3].Text;
                        dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;
                        dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;
                        dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                        DataGrid dgMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        foreach (DataGridItem dgiMenu in dgMenus.Items)
                        {
                            //if (((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked)
                            dr["strMenus"] = dgiMenu.Cells[0].Text + ":" + dgiMenu.Cells[1].Text + ":" + dgiMenu.Cells[2].Text + ";";
                        }
                        //Code Changed by OffShore FOr FB Issue 1073 -- Start
                        //dr["DeliverByDate"] = DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("MM/dd/yyyy");
                        //dr["DeliverByTime"] = DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("hh:mm tt");

                        dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format); //FB 2588
                        dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(tformat);

                        //dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((Label)dgi.FindControl("lblDeliverByDateTime")).Text)).ToString(format);
                        //dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((Label)dgi.FindControl("lblDeliverByDateTime")).Text)).ToString(tformat);
                        //Code Changed by OffShore FOr FB Issue 1073 -- End
                    }
                    dt.Rows.Add(dr);
                }
                if (flagUpdate)
                {
                    CATMainGrid.EditItemIndex = -1;
                    CATMainGrid.DataSource = dt;
                    CATMainGrid.DataBind();
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        String strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        DataTable dtMenus = new DataTable();
                        if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                        if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                        if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                        for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                        {
                            DataRow drMenu = dtMenus.NewRow();
                            drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                            drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                            drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                            dtMenus.Rows.Add(drMenu);
                        }
                        DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        dgCateringMenus.DataSource = dtMenus;
                        dgCateringMenus.DataBind();
                    }
                    btnAddNewCAT.Visible = true;
                    lblCATWOInstructions.Text = obj.GetTranslatedText("To create a new catering work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.");//FB 1830 - Translation FB 2570
                }
            }
            catch (Exception ex)
            {
                log.Trace("UpdateWorkorder: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region CancelChanges

        protected void CancelChanges(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                CATMainGrid.EditItemIndex = -1;
                CATMainGrid.DataSource = (DataTable)Session["CATMainGridDS"];
                CATMainGrid.DataBind();
                btnAddNewCAT.Visible = true;
                lblCATWOInstructions.Text = obj.GetTranslatedText("To create a new work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.");//FB 1830 - Translation
                String strMenus = ((Label)CATMainGrid.Items[e.Item.ItemIndex].FindControl("lblCateringMenus")).Text;
                if (strMenus.Trim().Equals(""))
                    DeleteWorkorder(sender, e);
                else
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        log.Trace("dgi.cells[0].text: " + dgi.Cells[0].Text);
                        DataTable dtMenus = new DataTable();
                        log.Trace("strMenus: " + strMenus);
                        if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                        if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                        if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                        for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                        {
                            DataRow drMenu = dtMenus.NewRow();
                            drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                            drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                            drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                            dtMenus.Rows.Add(drMenu);
                        }
                        DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        dgCateringMenus.DataSource = dtMenus;
                        dgCateringMenus.DataBind();
                        btnAddNewCAT.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                log.Trace("CancelChanges: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region HKMainGrid_Edit

        protected void HKMainGrid_Edit(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("HKEditColumn", e.Item.ItemIndex);

                DataTable myTable;
                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                {
                    myTable = (DataTable)Session["HKMainGridDS"];
                }
                else
                {
                    myTable = (DataTable)ViewState["HKMainGridDS"];
                }

                DataRowCollection myRows = myTable.Rows;
                DataRow dr = myTable.Rows[e.Item.ItemIndex];
                btnAddNewHK.Enabled = false;
                btnAddNewHK.Attributes.Add("Class", "btndisable"); //FB 2664
                HKItemsTable.Visible = true;
                HKMainGrid.Visible = false;
                btnHKSubmit.Text = obj.GetTranslatedText("Edit");//FB 1830 - Traslation
                btnHKSubmit.Enabled = true;
                lblNewEditHK.Text = obj.GetTranslatedText("Edit Work Order");//FB 1830 - Traslation
                txtHKWorkOrderID.Text = dr["ID"].ToString();
                txtHKWorkOrderName.Text = dr["Name"].ToString();
                lstHKRooms.ClearSelection();
                lstHKSet.ClearSelection();
                ListItem tempLI = new ListItem(dr["RoomName"].ToString(), dr["RoomID"].ToString());
                if (lstHKRooms.Items.Contains(tempLI))
                    lstHKRooms.Items.FindByValue(dr["RoomID"].ToString()).Selected = true;
                GetRoomHKSets(dr["RoomID"].ToString());
                lstHKSet.ClearSelection();
                lstHKSet.Items.FindByValue(dr["SetID"].ToString()).Selected = true;
                lstHKStatus.ClearSelection();
                lstHKStatus.Items.FindByText(dr["Status"].ToString()).Selected = true;
                if (dr["Notify"].Equals("1"))
                    chkHKNotify.Checked = true;
                else
                    chkHKNotify.Checked = false;
                txtHKComments.Text = dr["Comments"].ToString();
                completedByTimeHK.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(dr["CompletedByTime"].ToString())).ToString(tformat);
                startByTimeHK.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(dr["StartByTime"].ToString())).ToString(tformat);
                //Code changed for FB Issue 1073 
                txtHKCompletedBy.Text = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dr["CompletedByDate"].ToString())).ToString(format);
                txtHKStartByDate.Text = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dr["StartByDate"].ToString())).ToString(format);
                //Code changed for FB Issue 1073
                GetHKSetItems(dr["SetID"].ToString());
                hdnApprover3.Text = dr["AssignedToID"].ToString();
                dr["AssignedToName"] = obj.GetMyVRMUserName(hdnApprover3.Text);
                txtApprover3.Text = dr["AssignedToName"].ToString();
                lstRoomLayout.Items.Clear();
                GetRoomLayouts(dr["RoomID"].ToString());  // WO Bug Flx

                lstHKTimezone.ClearSelection();
                lstHKTimezone.Items.FindByValue(dr["Timezone"].ToString()).Selected = true;
                for (int i = 0; i < dr["ReqQuantity"].ToString().Split(';').Length; i++)
                    foreach (DataGridItem dgi in itemsGridHK.Items)
                        if (dgi.Cells[0].Text.Equals(dr["ReqQuantity"].ToString().Split(';')[i].Split(',')[0]))
                        {
                            ((TextBox)dgi.FindControl("txtReqQuantity")).Text = dr["ReqQuantity"].ToString().Split(';')[i].Split(',')[2];
                            dgi.Cells[dgi.Cells.Count - 1].Text = dr["ReqQuantity"].ToString().Split(';')[i].Split(',')[1];
                        }

                UpdateTotalHK(new object(), new EventArgs());
                
                if (dr["RoomLayout"].ToString() != "")
                    lstRoomLayout.Items.FindByText(dr["RoomLayout"].ToString()).Selected = true;
                lblHKWOInstructions.Text = obj.GetTranslatedText("Modify requested quantity and click EDIT to edit this work order.");//FB 1830 - Traslation
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        #endregion

        #region HKMainGrid_Update

        protected void HKMainGrid_Update(object sender, DataGridCommandEventArgs e)
        {
            //Response.Write("here");
            btnAddNewHK.Enabled = false;
            btnAddNewHK.Attributes.Add("Class", "btndisable"); //FB 2664
            HKItemsTable.Visible = true;
            btnHKSubmit.Text = obj.GetTranslatedText("Update");//FB 1830 - Translation
        }

        #endregion

        #region HKMainGrid_Cancel

        protected void HKMainGrid_Cancel(object sender, DataGridCommandEventArgs e)
        {
            //Response.Write("here");
            btnAddNewHK.Enabled = true;
            btnAddNewHK.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
            HKItemsTable.Visible = false;
            btnHKSubmit.Text = obj.GetTranslatedText("Cancel");//FB 1830 - Translation
        }

        #endregion

        #region selHKRooms_SelectedIndexChanged

        protected void selHKRooms_SelectedIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                hdnApprover3.Text = "";
                txtApprover3.Text = "";

                lstHKSet.Items.Clear();
                for (int i = 0; i < itemsGridHK.Items.Count; i++)
                    itemsGridHK.Items[i].Cells.Clear();
                if (lstHKRooms.SelectedIndex > 0)
                {
                    GetRoomHKSets(lstHKRooms.SelectedValue);
                    GetRoomLayouts(lstHKRooms.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("HKselRoomIndexChanged: " + ex.Message);
            }
        }

        #endregion

        #region GetRoomLayouts

        protected void GetRoomLayouts(string roomID)
        {
            try
            {
                string inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + roomID + "</roomID></login>";//Organization Module Fixes
                //string outxml = obj.CallCOM("GetOldRoom", inxml, Application["COM_ConfigPath"].ToString());
                string outxml = obj.CallMyVRMServer("GetOldRoom", inxml, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetOldRoom)
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outxml);
                string roomLayouts = xmldoc.SelectSingleNode("//room/roomImage").InnerText.Trim();
                //Response.Write(roomLayouts);
                lstRoomLayout.Items.Clear();
                lstRoomLayout.Enabled = true;
                ListItem li = new ListItem(obj.GetTranslatedText("Please select one..."), "-1", true);
                lstRoomLayout.Items.Add(li);
                if (roomLayouts != "")
                {
                    for (int i = 0; i < roomLayouts.Split(',').Length; i++)
                    {
                        li = new ListItem(roomLayouts.Split(',')[i].ToString().Trim(), i.ToString());
                        lstRoomLayout.Items.Add(li);
                    }
                }
                else
                {
                    lstRoomLayout.Items.Clear();
                    li = new ListItem(obj.GetTranslatedText("No room layouts defined"), "-2", true);
                    lstRoomLayout.Enabled = false;
                    lstRoomLayout.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetRoomLayouts:Error in getting Room Layouts." + ex.Message);//FB 1881
                errLabel.Visible = true;
                //errLabel.Text = "Error in getting Room Layouts. Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
            }
        }

        #endregion

        #region selHKSet_SelectedIndexChanged

        protected void selHKSet_SelectedIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                hdnApprover3.Text = "";
                txtApprover3.Text = "";
                for (int i = 0; i < itemsGridHK.Items.Count; i++)
                    itemsGridHK.Items[i].Cells.Clear();
                if (lstHKSet.SelectedIndex > 0)
                    GetHKSetItems(lstHKSet.SelectedValue);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("selHKSetIndexChanged: " + ex.StackTrace);
            }
        }

        #endregion

        #region selRooms_SelectedIndexChanged

        protected void selRooms_SelectedIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                lstAVSet.Items.Clear();
                hdnApprover1.Text = "";
                txtApprover1.Text = "";
                for (int i = 0; i < itemsGrid.Items.Count; i++)
                    itemsGrid.Items[i].Cells.Clear();
                if (lstRooms.SelectedIndex > 0)
                    GetRoomSets(lstRooms.SelectedValue);
                lblAVWOInstructions.Text = "Select a Set from the Audiovisual Inventories list." + System.Environment.NewLine + "If there are no items in the list, then there are no sets associated with this room. " + System.Environment.NewLine; // FB 2570
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("selRoomIndexChanged: " + ex.Message);
            }
        }

        #endregion

        //FB 1470
        #region GetEntityDescription

        protected string GetEntityDescription(string eID, string eCode)
        {
            string entityDesc = ""; ;
            string entityCode;
            try
            {

                String inXML = "";
                string selectedEID;
                string selectedECode;
                selectedEID = eID;
                selectedECode = eCode;
                inXML += "<GetEntityCodeDescription>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <CustomAttributeID>1</CustomAttributeID>";
                inXML += "  <OptionType>6</OptionType>";
                inXML += "  <OptionID>" + selectedEID + "</OptionID>";
                inXML += "  <Caption>" + selectedECode + "</Caption>";
                inXML += "</GetEntityCodeDescription>";

                String outXML = obj.CallMyVRMServer("GetCustomAttributeDescription", inXML, Application["MyVRMServer_ConfigPath"].ToString());


                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);
                    XmlNode node;
                    node = xd.SelectSingleNode("//EntityCode/DisplayCaption");
                    entityCode = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//EntityCode/HelpText");
                    entityDesc = node.InnerXml.Trim();
                    /* lblEntityDesc.Text = entityDesc.ToString();
                     tblEntityDesc.Visible = true;
                     lblEntityDesc.Visible = true;*/

                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }


            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                //errLabel.Text = ex.StackTrace;
                log.Trace("GetEntityDescription" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();

            }
            return entityDesc;
        }

        #endregion

        #region selAVSet_SelectedIndexChanged

        protected void selAVSet_SelectedIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < itemsGrid.Items.Count; i++)
                    itemsGrid.Items[i].Cells.Clear();
                hdnApprover1.Text = "";
                txtApprover1.Text = "";
                if (lstAVSet.SelectedIndex > 0)
                    GetAVSetItems(lstAVSet.SelectedValue);
                else
                {
                    foreach (DataGridItem dgi in itemsGrid.Items)
                        dgi.Cells.Clear();
                    itemsGrid.Visible = false;
                    btnSubmit.Enabled = false;  //FB 1379 Work order fixes
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("selSetIndexChanged: " + ex.StackTrace);
            }
        }

        #endregion

        #region GetRoomSets

        protected void GetRoomSets(string roomID)
        {
            XmlDocument xmldoc = new XmlDocument();
            ListItem li = new ListItem();
            string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID><roomID>" + roomID + "</roomID><Type>1</Type></login>";//Organization Module Fixes
            try
            {
                //myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();

                string outXML = obj.CallMyVRMServer("GetRoomSets", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                xmldoc.LoadXml(outXML);
                lstAVSet.Items.Clear();
                XmlNodeList nodes = xmldoc.SelectNodes("/SetList/Set");
                hdnApprover1.Text = "";
                txtApprover1.Text = "";

                int length = nodes.Count;
                lstAVSet.Items.Add("Select one...");
                //Response.Write("here");
                for (int i = 0; i < length; i++)
                {
                    if (nodes[i].SelectSingleNode("Type").InnerText.Equals("1"))
                    {
                        li = new ListItem(nodes[i].SelectSingleNode("Name").InnerText, nodes[i].SelectSingleNode("ID").InnerText);
                        lstAVSet.Items.Add(li);
                        //Response.Write("here");
                    }
                }
                //Code added for WO bug - 1379
                reqFieldApproverAV.Enabled = true;
                btnSubmit.Enabled = true;
                lstDeliveryType.Enabled = true;
                RequiredFieldValidator9.Enabled = true;

                //FB 1379 work order fixes - starts
                if (lstAVSet.Items.Count > 1)
                {
                    lstAVSet.ClearSelection();
                    lstAVSet.Items[1].Selected = true;
                    GetAVSetItems(lstAVSet.SelectedValue);
                }
                else
                {
                    if (lstAVSet.Items.Count.Equals(1))
                    {
                        lstAVSet.Items.Clear();
                        lstAVSet.Items.Add("No Sets");

                        //Code added for WO bug - 1379
                        reqFieldApproverAV.Enabled = false;
                        btnSubmit.Enabled = false;
                        lstDeliveryType.Enabled = false;
                        RequiredFieldValidator9.Enabled = false;
                    }

                    lstAVSet.SelectedIndex = 0;
                    AVMainGrid.Visible = false;
                }
                //FB 1379 work order fixes - ends
                
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region GetAVSetItems

        protected void GetAVSetItems(string groupID)
        {
            try
            {
                String pre = "";
                foreach (DataGridItem dgi in AVMainGrid.Items)
                {
                    string tb = "";
                    tb = dgi.Cells[18].Text.Trim(' '); //dgi.FindControl("txtAVQuantity");
                    DateTime dtStartP = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[4].Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[5].Text)); //FB 2588
                    DateTime dtEndP = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[6].Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[7].Text));
                    //Code changed by offshore for FB Issue 1073 -- start
                    //DateTime dtStart = DateTime.Parse(txtStartByDate.Text + " " + startByTime.Text);
                    //DateTime dtEnd = DateTime.Parse(txtCompletedBy.Text + " " + completedByTime.Text);
                    DateTime dtStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtStartByDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(startByTime.Text));
                    DateTime dtEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtCompletedBy.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(completedByTime.Text));
                    //Code changed by offshore for FB Issue 1073 -- end
                    //Response.Write(dtStart.ToLongTimeString() + " : " + dtStartP.ToLongTimeString() + " : " + dtEnd.ToLongTimeString() + " : " + dtEndP.ToLongTimeString());
                    //Response.Write(dgi.ItemIndex + " : " + Session["AVEditColumn"]);
                    if (lblConfID.Text == "new") // code added for Fb 1327 start
                    {
                        if ((tb.Trim() != "" || tb.Trim() != " ") && !dgi.ItemIndex.Equals(Session["AVEditColumn"]))
                        {
                            //if (CheckDateOverlap(dtStart, dtEnd, dtStartP, dtEndP))
                            {
                                for (int i = 0; i < tb.Split(';').Length - 1; i++)//FB 1830 (� -Alt 147)
                                {
                                    String iID = tb.Split(';')[i].Split('�')[0];
                                    String iQuantity = tb.Split(';')[i].Split('�')[2];
                                    String UID = tb.Split(';')[i].Split('�')[1];
                                    pre += "              <Item>";
                                    pre += "                  <ID>" + iID + "</ID>";
                                    pre += "                  <UID>" + UID + "</UID>";
                                    pre += "                  <Quantity>" + iQuantity + "</Quantity>";
                                    pre += "              </Item>";
                                }
                            }
                        }
                    }  //FB 1327 end
                }

                //FB 2634 - starts
                DateTime dStart, dEnd;//FB 2634
                String DateFrom, DateTo, TimeFrom, TimeTo = "";

                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                Double setupDuration = Double.MinValue;//FB 2634-K
                Double tearDuration = Double.MinValue;

                if (!chkEnableBuffer.Checked && enableBufferZone == "1")//FB 2398
                {
                    Double.TryParse(SetupDuration.Text, out setupDuration);
                    Double.TryParse(TearDownDuration.Text, out tearDuration);
                    dStart = dStart.AddMinutes(-setupDuration);
                    dEnd = dEnd.AddMinutes(tearDuration);
                }

                DateFrom = dStart.ToString(format);
                TimeFrom = myVRMNet.NETFunctions.GetFormattedTime(dStart.ToString("HH:mm"), Session["timeFormat"].ToString());

                DateTo = dEnd.ToString(format);
                TimeTo = myVRMNet.NETFunctions.GetFormattedTime(dEnd.ToString("HH:mm"), Session["timeFormat"].ToString());

                String inXML = objInXML.GetInventoryDetails(groupID, DateFrom, DateTo, TimeFrom, TimeTo, lstConferenceTZ.SelectedValue, "1", pre, "");
                //String inXML = objInXML.GetInventoryDetails(groupID, confStartDate.Text, confEndDate.Text, confStartTime.Text, confEndTime.Text, lstConferenceTZ.SelectedValue, "1", pre, "");

                if (!Session["AVEditColumn"].ToString().Equals("-1") && !AVMainGrid.Items[Int32.Parse(Session["AVEditColumn"].ToString())].Cells[0].Text.Equals("new"))
                    inXML = objInXML.GetInventoryDetails(groupID, DateFrom, DateTo, TimeFrom, TimeTo, lstConferenceTZ.SelectedValue, "1", pre, AVMainGrid.Items[Int32.Parse(Session["AVEditColumn"].ToString())].Cells[0].Text.ToString());
                    //inXML = objInXML.GetInventoryDetails(groupID, confStartDate.Text, confEndDate.Text, confStartTime.Text, confEndTime.Text, lstConferenceTZ.SelectedValue, "1", pre, AVMainGrid.Items[Int32.Parse(Session["AVEditColumn"].ToString())].Cells[0].Text.ToString());

                //FB 2634 - End
                
                String outXML = obj.CallMyVRMServer("GetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//Inventory/ItemList/Item");
                    //Response.Write(nodes.Count);
                    int sublength = nodes.Count;
                    if (nodes.Count > 0)
                        LoadDataGrid(nodes, "1");
                    btnSubmit.Enabled = true;

                    hdnApprover1.Text = xmldoc.SelectSingleNode("//Inventory/Admin/ID").InnerText;
                    txtApprover1.Text = obj.GetMyVRMUserName(hdnApprover1.Text);
                    //Response.Write("txtApprover1_2");
                    lblAVWOInstructions.Text = obj.GetTranslatedText("Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder."); //WO Issue //FB 1830 - Translation
                    AVMainGrid.Visible = false;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region CheckDateOverlap

        protected bool CheckDateOverlap(DateTime dtStart, DateTime dtEnd, DateTime dtStartP, DateTime dtEndP)
        {
            try
            {
                //Response.Write(dtStart.ToLongTimeString() + " : " + dtStartP.ToLongTimeString() + " : " + dtEnd.ToLongTimeString() + " : " + dtEndP.ToLongTimeString());
                if ((dtStart >= dtStartP && dtEnd <= dtEndP) || (dtEnd <= dtEndP && dtEnd >= dtStartP))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        #region GetRoomCATSets

        protected void GetRoomCATSets(string roomID)
        {
            //XmlDocument xmldoc = new XmlDocument();
            //ListItem li = new ListItem();
            //string inXML = "<login><userID>" + Session["userID"] + "</userID><roomID>" + roomID + "</roomID><Type>2</Type></login>";
            try
            {
                //    //                myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
                //    hdnApprover2.Text = "";
                //    txtApprover2.Text = "";

                //    string outXML = obj.CallMyVRMServer("GetRoomSets", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //    xmldoc.LoadXml(outXML);
                //    lstAVSet.Items.Clear();
                //    XmlNodeList nodes = xmldoc.SelectNodes("/SetList/Set");

                //    int length = nodes.Count;
                //    lstCATSet.Items.Add("Select one...");
                //    for (int i = 0; i < length; i++)
                //    {
                //        if (nodes[i].SelectSingleNode("Type").InnerText.Equals("2"))
                //        {
                //            li = new ListItem(nodes[i].SelectSingleNode("Name").InnerText, nodes[i].SelectSingleNode("ID").InnerText);
                //            lstCATSet.Items.Add(li);
                //        }
                //    }
                //    if (lstCATSet.Items.Count.Equals(1))
                //    {
                //        lstCATSet.Items.Clear();
                //        lstCATSet.Items.Add("No Menus");
                //    }
                //    lstCATSet.SelectedIndex = 0;
                //    CATMainGrid.Visible = false;
                //    lblCATWOInstructions.Text = "Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder."; //WO Issue
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region GetCATSetItems

        protected void GetCATSetItems(string groupID)
        {
            try
            {
                //hdnApprover2.Text = "";
                //txtApprover2.Text = "";

                //String inXML = objInXML.GetInventoryDetails(groupID, txtCATStartByDate.Text, txtCATCompletedBy.Text, startByTimeCAT.Text, completedByTimeCAT.Text, lstCATTimezone.SelectedValue, "2", "", "");
                //String outXML = obj.CallMyVRMServer("GetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                ////Response.Write(obj.Transfer(outXML));
                //if (outXML.IndexOf("<error>") >= 0)
                //{
                //    errLabel.Visible = true;
                //    errLabel.Text = obj.ShowErrorMessage(outXML);
                //}
                //else
                //{
                //    XmlDocument xmldoc = new XmlDocument();
                //    xmldoc.LoadXml(outXML);
                //    XmlNodeList nodes = xmldoc.SelectNodes("//Inventory/ItemList/Item");
                //    int sublength = nodes.Count;
                //    if (nodes.Count > 0)
                //        LoadDataGrid(nodes, "2");
                //    hdnApprover2.Text = xmldoc.SelectSingleNode("//Inventory/Admin/ID").InnerText;
                //    txtApprover2.Text = obj.GetMyVRMUserName(hdnApprover2.Text);
                //    btnCATSubmit.Enabled = true;
                //}
                //CATMainGrid.Visible = false;
                //lblCATWOInstructions.Text = "Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder."; //WO Issue
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region GetRoomHKSets

        protected void GetRoomHKSets(string roomID)
        {
            XmlDocument xmldoc = new XmlDocument();
            ListItem li = new ListItem();
            string inXML = "<login>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "<userID>" + Session["userID"] + "</userID><roomID>" + roomID + "</roomID><Type>3</Type></login>";
            try
            {
                //myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();

                string outXML = obj.CallMyVRMServer("GetRoomSets", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                xmldoc.LoadXml(outXML);
                lstHKSet.Items.Clear();
                XmlNodeList nodes = xmldoc.SelectNodes("/SetList/Set");
                //Response.Write(obj.Transfer(outXML));
                int length = nodes.Count;
                lstHKSet.Items.Add("Select one..."); 
                for (int i = 0; i < length; i++)
                {
                    if (nodes[i].SelectSingleNode("Type").InnerText.Equals("3"))
                    {
                        li = new ListItem(nodes[i].SelectSingleNode("Name").InnerText, nodes[i].SelectSingleNode("ID").InnerText);
                        lstHKSet.Items.Add(li);
                    }
                }
                lblHKWOInstructions.Text = obj.GetTranslatedText("Please select a Facility Services now.");//FB 1830 - Translation //FB 2570
                if (lstHKSet.Items.Count.Equals(1))
                {
                    lstHKSet.Items.Clear();
                    lstHKSet.Items.Add("No Groups");
                    lblHKWOInstructions.Text = obj.GetTranslatedText("There are no Facility Services available for this room. Please select another room.");//FB 1830 - Translation //FB 2570
                }
                lstHKSet.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region GetHKSetItems

        protected void GetHKSetItems(string groupID)
        {
            try
            {
                String inXML = objInXML.GetInventoryDetails(groupID, txtHKStartByDate.Text, txtHKCompletedBy.Text, startByTimeHK.Text, completedByTimeHK.Text, lstHKTimezone.SelectedValue, "3", "", "");
                String outXML = obj.CallMyVRMServer("GetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("/Inventory/ItemList/Item");
                    int sublength = nodes.Count;
                    if (nodes.Count > 0)
                        LoadDataGrid(nodes, "3");
                    //                hdnApprover3.Text = xmldoc.SelectSingleNode("//Inventory/Admin/ID").InnerText;
                    hdnApprover3.Text = xmldoc.SelectSingleNode("//Inventory/Admin/ID").InnerText;
                    txtApprover3.Text = obj.GetMyVRMUserName(hdnApprover3.Text);
                    btnHKSubmit.Enabled = true;
                    lblHKWOInstructions.Text = obj.GetTranslatedText("Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder."); //WO Issue //FB 1830 - Translation
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region btnCancel_Click

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            AVItemsTable.Visible = false;
            btnAddNewAV.Enabled = true;
            btnAddNewAV.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
            AVMainGrid.Visible = true;
            SetInstructionsAV(sender, e);
        }

        #endregion

        #region btnSubmit_Click

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //Code changed by offshore for FB Issue 1073 -- start
                if (CheckWorkorderStartEndTime(myVRMNet.NETFunctions.GetDefaultDate(txtStartByDate.Text), myVRMNet.NETFunctions.ChangeTimeFormat(startByTime.Text), myVRMNet.NETFunctions.GetDefaultDate(txtCompletedBy.Text), myVRMNet.NETFunctions.ChangeTimeFormat(completedByTime.Text)))
                //Code changed by offshore for FB Issue 1073 -- end
                {
                    DataTable myTable;
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        myTable = (DataTable)Session["AVMainGridDS"]; // FB 2050
                    }
                    else
                    {
                        myTable = (DataTable)ViewState["AVMainGridDS"]; // FB 2050
                    }

                    if (AVMainGrid.Items.Count > 0)
                    {
                        if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                        {
                            myTable = (DataTable)Session["AVMainGridDS"]; // FB 2050
                        }
                        else
                        {
                            myTable = (DataTable)ViewState["AVMainGridDS"]; // FB 2050
                        }
                    }
                    else
                        myTable = new DataTable();

                    if (!myTable.Columns.Contains("ID")) myTable.Columns.Add("ID");
                    if (!myTable.Columns.Contains("UID")) myTable.Columns.Add("UID");
                    if (!myTable.Columns.Contains("Name")) myTable.Columns.Add("Name");
                    if (!myTable.Columns.Contains("AssignedToName")) myTable.Columns.Add("AssignedToName");
                    if (!myTable.Columns.Contains("AssignedToID")) myTable.Columns.Add("AssignedToID");
                    if (!myTable.Columns.Contains("CompletedByDate")) myTable.Columns.Add("CompletedByDate");
                    if (!myTable.Columns.Contains("CompletedByTime")) myTable.Columns.Add("CompletedByTime");
                    if (!myTable.Columns.Contains("RoomName")) myTable.Columns.Add("RoomName");
                    if (!myTable.Columns.Contains("RoomID")) myTable.Columns.Add("RoomID");
                    if (!myTable.Columns.Contains("SetID")) myTable.Columns.Add("SetID");
                    if (!myTable.Columns.Contains("Comments")) myTable.Columns.Add("Comments");
                    if (!myTable.Columns.Contains("Status")) myTable.Columns.Add("Status");
                    if (!myTable.Columns.Contains("Notify")) myTable.Columns.Add("Notify");
                    if (!myTable.Columns.Contains("Reminder")) myTable.Columns.Add("Reminder");
                    if (!myTable.Columns.Contains("ReqQuantity")) myTable.Columns.Add("ReqQuantity");
                    if (!myTable.Columns.Contains("Timezone")) myTable.Columns.Add("Timezone");
                    if (!myTable.Columns.Contains("Description")) myTable.Columns.Add("Description");
                    if (!myTable.Columns.Contains("DeliveryCost")) myTable.Columns.Add("DeliveryCost");
                    if (!myTable.Columns.Contains("DeliveryType")) myTable.Columns.Add("DeliveryType");
                    if (!myTable.Columns.Contains("ServiceCharge")) myTable.Columns.Add("ServiceCharge");
                    if (!myTable.Columns.Contains("TotalCost")) myTable.Columns.Add("TotalCost");
                    if (!myTable.Columns.Contains("StartByDate")) myTable.Columns.Add("StartByDate");
                    if (!myTable.Columns.Contains("StartByTime")) myTable.Columns.Add("StartByTime");
                    if (!myTable.Columns.Contains("RoomLayout")) myTable.Columns.Add("RoomLayout");
                    if (!myTable.Columns.Contains("Timezone")) myTable.Columns.Add("Timezone");
                    if (!myTable.Columns.Contains("RowUID")) myTable.Columns.Add("RowUID"); //FB 498 work order fixes

                    DataRow dr = myTable.NewRow();

                    dr["Name"] = txtWorkOrderName.Text;
                    dr["AssignedToName"] = txtApprover1.Text;
                    dr["AssignedToID"] = hdnApprover1.Text;
                    //Code changed by offshore for FB Issue 1073 --start
                    //dr["CompletedByDate"] = txtCompletedBy.Text;
                    dr["CompletedByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtCompletedBy.Text)).ToString(format);
                    //Code changed by offshore for FB Issue 1073 --end
                    dr["CompletedByTime"] = completedByTime.Text;// myVRMNet.NETFunctions.ChangeTimeFormat();
                    //Code changed by offshore for FB Issue 1073 --start
                    //dr["StartByDate"] = txtStartByDate.Text;
                    dr["StartByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtStartByDate.Text)).ToString(format);
                    //Code changed by offshore for FB Issue 1073 --end
                    dr["StartByTime"] = startByTime.Text; // myVRMNet.NETFunctions.ChangeTimeFormat(startByTime.Text);
                    dr["RoomName"] = lstRooms.SelectedItem;
                    dr["RoomID"] = lstRooms.SelectedValue;
                    dr["SetID"] = lstAVSet.SelectedValue;
                    dr["Comments"] = txtComments.Text;
                    dr["Status"] = lstStatus.SelectedItem;
                    dr["Timezone"] = lstTimezones.SelectedValue;
                    //dr["DeliveryCost"] = Double.Parse(txtDeliveryCost.Text).ToString("00.00");
                    //dr["ServiceCharge"] = Double.Parse(txtServiceCharges.Text).ToString("00.00");
                    //FB 1830
                    String tempString = txtDeliveryCost.Text;
                    if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        tempString = tempString.Replace(",", ".");
                    tmpVal = decimal.Parse(Double.Parse(tempString).ToString("0.00"));
                    dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);                    
                    
                    tempString = txtServiceCharges.Text;
                    if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        tempString = tempString.Replace(",", ".");
                    tmpVal = decimal.Parse(Double.Parse(tempString).ToString("0.00"));                    
                    dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);

                    dr["DeliveryType"] = lstDeliveryType.SelectedValue;
                    UpdateTotalAV(new object(), new EventArgs());
                    DataGridItem dgFooter = (DataGridItem)itemsGrid.Controls[0].Controls[itemsGrid.Controls[0].Controls.Count - 1];
                    Label lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
                    //dr["TotalCost"] = Double.Parse(lblTemp.Text).ToString("00.00");
                    //FB 1830-Starts
                    //dr["TotalCost"] = Double.Parse(lblTemp.Text).ToString("0.00"); //FB 1686
                    tmpVal = 0;
                    decimal.TryParse(lblTemp.Text, NumberStyles.Any, cInfo, out tmpVal);
                    dr["TotalCost"] = tmpVal.ToString("n", cInfo);
                    //FB 1830 - End
                    if (chkNotify.Checked)
                        dr["Notify"] = "1";
                    else
                        dr["Notify"] = "0";
                    //if (chkReminder.Checked)
                    dr["Reminder"] = "1";
                    //else
                    //    dr["Reminder"] = "0";

                    //FB 498 work order fixes - start
                    if (btnSubmit.Text.Equals(obj.GetTranslatedText("Create")))
                        dr["RowUID"] = myTable.Rows.Count;
                    else
                        dr["RowUID"] = Session["AVEditColumn"].ToString();
                    //FB 498 work order fixes - end

                    //if (CheckQuantity(itemsGrid, "txtReqQuantity"))
                    {
                        if (btnSubmit.Text.Equals(obj.GetTranslatedText("Create")))
                        {
                            dr["ID"] = "new";
                            myTable.Rows.Add(dr);
                            //Response.Write(tb.Text);
                            //Response.End();
                        }
                        else
                        {
                            dr["ID"] = txtWorkOrderID.Text;
                            myTable.Rows.RemoveAt(Convert.ToInt32(Session["AVEditColumn"]));
                            myTable.Rows.InsertAt(dr, Convert.ToInt32(Session["AVEditColumn"]));
                        }
                        string tb = "";
                        foreach (DataGridItem dgi in itemsGrid.Items)
                        {
                            TextBox temp = new TextBox();
                            temp = (TextBox)dgi.FindControl("txtReqQuantity");
                            String lblDC = ((Label)dgi.FindControl("lblDeliveryCost")).Text;
                            String lblSC = ((Label)dgi.FindControl("lblServiceCharge")).Text;
                            String lblDT = ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).SelectedValue;
                            //tb += dgi.Cells[0].Text.Trim() + "," + dgi.Cells[dgi.Cells.Count - 1].Text.Trim() + "," + temp.Text.Trim() + "," + lblDC + "," + lblSC + "," + lblDT + "," + dgi.Cells[6].Text.Trim() + "," + dgi.Cells[1].Text.Trim() + ";";  //Code added fro WO bug FB 322
                            tb += dgi.Cells[0].Text.Trim() + "�" + dgi.Cells[dgi.Cells.Count - 1].Text.Trim() + "�" + temp.Text.Trim() + "�" + lblDC + "�" + lblSC + "�" + lblDT + "�" + dgi.Cells[6].Text.Trim() + "�" + dgi.Cells[1].Text.Trim() + ";";  //Code added fro WO bug FB 322//FB 1830 (�- Alt 147)
                        }
                        dr["ReqQuantity"] = tb;

                        if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                        {
                            Session["AVMainGridDS"] = myTable;
                        }
                        else
                        {
                            ViewState["AVMainGridDS"] = myTable;
                        }

                        AVMainGrid.DataSource = myTable;
                        AVMainGrid.DataBind();
                        AVItemsTable.Visible = false;
                        AVMainGrid.Visible = true;
                        btnAddNewAV.Enabled = true;
                        btnAddNewAV.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                        lblAVWOInstructions.Text = obj.GetTranslatedText("To add a new work order, click the button below.");//FB 1830 - Translation
                        errLabel.Text = "";
                        errLabel.Visible = false;
                    }
                    //else
                    //{
                    //    errLabel.Text = "At least one item should have a requested quantity > 0";
                    //    errLabel.Visible = true;
                    //}
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Please check the start and end Date/time for this workorder."); //FB 1863//FB 1830 - Translation
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region btnCATCancel_Click

        protected void btnCATCancel_Click(object sender, EventArgs e)
        {
            //Response.Write("herein cat");
            //CATItemsTable.Visible = false;
            //btnAddNewCAT.Enabled = true;
            //CATMainGrid.Visible = true;
            //SetInstructionsCAT(sender, e);
        }

        #endregion

        #region btnCATSubmit_Click

        protected void btnCATSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //if (CheckWorkorderStartEndTime(txtCATStartByDate.Text, startByTimeCAT.Text, txtCATCompletedBy.Text, completedByTimeCAT.Text))
                //{
                //    DataTable myTable;
                //    if (Session["CATMainGridDS"] == null)
                //    {
                //        //Response.Write("in cat sbmit");
                //        myTable = new DataTable();
                //    }
                //    else
                //        myTable = (DataTable)Session["CATMainGridDS"];
                //    if (!myTable.Columns.Contains("ID")) myTable.Columns.Add("ID");
                //    if (!myTable.Columns.Contains("Name")) myTable.Columns.Add("Name");
                //    if (!myTable.Columns.Contains("AssignedToName")) myTable.Columns.Add("AssignedToName");
                //    if (!myTable.Columns.Contains("AssignedToID")) myTable.Columns.Add("AssignedToID");
                //    if (!myTable.Columns.Contains("CompletedByDate")) myTable.Columns.Add("CompletedByDate");
                //    if (!myTable.Columns.Contains("CompletedByTime")) myTable.Columns.Add("CompletedByTime");
                //    if (!myTable.Columns.Contains("RoomName")) myTable.Columns.Add("RoomName");
                //    if (!myTable.Columns.Contains("RoomID")) myTable.Columns.Add("RoomID");
                //    if (!myTable.Columns.Contains("SetID")) myTable.Columns.Add("SetID");
                //    if (!myTable.Columns.Contains("Comments")) myTable.Columns.Add("Comments");
                //    if (!myTable.Columns.Contains("Status")) myTable.Columns.Add("Status");
                //    if (!myTable.Columns.Contains("Notify")) myTable.Columns.Add("Notify");
                //    if (!myTable.Columns.Contains("Reminder")) myTable.Columns.Add("Reminder");
                //    if (!myTable.Columns.Contains("ReqQuantity")) myTable.Columns.Add("ReqQuantity");
                //    if (!myTable.Columns.Contains("DeliveryCost")) myTable.Columns.Add("DeliveryCost");
                //    if (!myTable.Columns.Contains("DeliveryType")) myTable.Columns.Add("DeliveryType");
                //    if (!myTable.Columns.Contains("ServiceCharge")) myTable.Columns.Add("ServiceCharge");
                //    if (!myTable.Columns.Contains("TotalCost")) myTable.Columns.Add("TotalCost");
                //    if (!myTable.Columns.Contains("StartByDate")) myTable.Columns.Add("StartByDate");
                //    if (!myTable.Columns.Contains("StartByTime")) myTable.Columns.Add("StartByTime");
                //    if (!myTable.Columns.Contains("RoomLayout")) myTable.Columns.Add("RoomLayout");
                //    if (!myTable.Columns.Contains("Timezone")) myTable.Columns.Add("Timezone");

                //    DataRowCollection myRows = myTable.Rows;
                //    DataRow dr = myTable.NewRow();
                //    dr["Name"] = txtCATWorkOrderName.Text;
                //    dr["AssignedToName"] = txtApprover2.Text;
                //    dr["AssignedToID"] = hdnApprover2.Text;
                //    dr["CompletedByDate"] = txtCATCompletedBy.Text;
                //    dr["CompletedByTime"] = completedByTimeCAT.Text;
                //    dr["RoomName"] = lstCATRooms.SelectedItem;
                //    dr["RoomID"] = lstCATRooms.SelectedValue;
                //    dr["SetID"] = lstCATSet.SelectedValue;
                //    dr["Comments"] = txtCATComments.Text;
                //    dr["Status"] = lstCATStatus.SelectedItem;
                //    dr["StartByDate"] = txtCATStartByDate.Text;
                //    dr["StartByTime"] = startByTimeCAT.Text;
                //    dr["ID"] = txtCATWorkOrderID.Text;
                //    dr["DeliveryType"] = "1";
                //    dr["DeliveryCost"] = "0.00";
                //    dr["ServiceCharge"] = "0.00";
                //    dr["Timezone"] = lstCATTimezone.SelectedValue;
                //    UpdateTotalCAT(new object(), new EventArgs());
                //    DataGridItem dgFooter = (DataGridItem)itemsGridCAT.Controls[0].Controls[itemsGridCAT.Controls[0].Controls.Count - 1];
                //    Label lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
                //    dr["TotalCost"] = lblTemp.Text;
                //    if (chkCATNotify.Checked)
                //        dr["Notify"] = "1";
                //    else
                //        dr["Notify"] = "0";
                //    //if (chkCATReminder.Checked)
                //    dr["Reminder"] = "1";
                //    //else
                //    //    dr["Reminder"] = "0";
                //    if (CheckQuantity(itemsGridCAT, "txtReqQuantity"))
                //    {
                //        if (btnCATSubmit.Text.Equals("Create"))
                //        {
                //            //dr["ID"] = "new";
                //            myRows.Add(dr);
                //        }
                //        else
                //        {
                //            //dr["ID"] = txtCATWorkOrderID.Text;
                //            myRows.RemoveAt(Convert.ToInt32(Session["CATEditColumn"]));
                //            myRows.InsertAt(dr, Convert.ToInt32(Session["CATEditColumn"]));
                //        }
                //        string tb = "";
                //        foreach (DataGridItem dgi in itemsGridCAT.Items)
                //        {
                //            TextBox temp = new TextBox();
                //            temp = (TextBox)dgi.FindControl("txtReqQuantity");
                //            tb += dgi.Cells[0].Text.Trim() + "," + dgi.Cells[dgi.Cells.Count - 1].Text.Trim() + "," + temp.Text.Trim() + ";";
                //        }
                //        dr["ReqQuantity"] = tb;
                //        Session["CATMainGridDS"] = myTable;
                //        CATMainGrid.DataSource = myTable;
                //        CATMainGrid.DataBind();
                //        CATItemsTable.Visible = false;
                //        btnAddNewCAT.Enabled = true;
                //        lblCATWOInstructions.Text = "To add a new work order, click the button below.";
                //        CATMainGrid.Visible = true;
                //        errLabel.Visible = false;
                //    }
                //    else
                //    {
                //        errLabel.Text = "At least one item should have a requested quantity > 0";
                //        errLabel.Visible = true;
                //    }
                //}
                //else
                //{
                //    errLabel.Text = "Please check the Start and End time of this workorder.";
                //    errLabel.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace); // "Catering Submit Error";
            }
        }

        #endregion

        #region btnHKCancel_Click

        protected void btnHKCancel_Click(object sender, EventArgs e)
        {
            //            Response.Write("here in hk cancel");
            btnAddNewHK.Enabled = true;
            btnAddNewHK.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
            HKMainGrid.Visible = true;
            HKItemsTable.Visible = false;
            SetInstructionsHK(sender, e);
        }

        #endregion

        #region btnHKSubmit_Click

        protected void btnHKSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                
                DataTable myTable;

                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                {
                    if (Session["HKMainGridDS"] == null)
                    {
                        //Response.Write("in cat sbmit");
                        myTable = new DataTable();
                    }
                    else
                        myTable = (DataTable)Session["HKMainGridDS"];
                }
                else
                {
                    if (ViewState["HKMainGridDS"] == null)
                    {
                        //Response.Write("in cat sbmit");
                        myTable = new DataTable();
                    }
                    else
                        myTable = (DataTable)ViewState["HKMainGridDS"];
                }

                DataRowCollection myRows = myTable.Rows;
                if (!myTable.Columns.Contains("ID")) myTable.Columns.Add("ID");
                if (!myTable.Columns.Contains("Name")) myTable.Columns.Add("Name");
                if (!myTable.Columns.Contains("AssignedToName")) myTable.Columns.Add("AssignedToName");
                if (!myTable.Columns.Contains("AssignedToID")) myTable.Columns.Add("AssignedToID");
                if (!myTable.Columns.Contains("CompletedByDate")) myTable.Columns.Add("CompletedByDate");
                if (!myTable.Columns.Contains("CompletedByTime")) myTable.Columns.Add("CompletedByTime");
                if (!myTable.Columns.Contains("RoomName")) myTable.Columns.Add("RoomName");
                if (!myTable.Columns.Contains("RoomID")) myTable.Columns.Add("RoomID");
                if (!myTable.Columns.Contains("SetID")) myTable.Columns.Add("SetID");
                if (!myTable.Columns.Contains("Comments")) myTable.Columns.Add("Comments");
                if (!myTable.Columns.Contains("Status")) myTable.Columns.Add("Status");
                if (!myTable.Columns.Contains("Notify")) myTable.Columns.Add("Notify");
                if (!myTable.Columns.Contains("Reminder")) myTable.Columns.Add("Reminder");
                if (!myTable.Columns.Contains("ReqQuantity")) myTable.Columns.Add("ReqQuantity");
                if (!myTable.Columns.Contains("DeliveryCost")) myTable.Columns.Add("DeliveryCost");
                if (!myTable.Columns.Contains("DeliveryType")) myTable.Columns.Add("DeliveryType");
                if (!myTable.Columns.Contains("ServiceCharge")) myTable.Columns.Add("ServiceCharge");
                if (!myTable.Columns.Contains("TotalCost")) myTable.Columns.Add("TotalCost");
                if (!myTable.Columns.Contains("StartByDate")) myTable.Columns.Add("StartByDate");
                if (!myTable.Columns.Contains("StartByTime")) myTable.Columns.Add("StartByTime");
                if (!myTable.Columns.Contains("RoomLayout")) myTable.Columns.Add("RoomLayout");
                if (!myTable.Columns.Contains("Timezone")) myTable.Columns.Add("Timezone");

                DataRow dr = myTable.NewRow();

                dr["ID"] = "new";
                dr["Name"] = txtHKWorkOrderName.Text;
                dr["AssignedToName"] = txtApprover3.Text;
                dr["AssignedToID"] = hdnApprover3.Text;
                dr["CompletedByDate"] = txtHKCompletedBy.Text;
                dr["CompletedByTime"] = completedByTimeHK.Text;
                dr["StartByDate"] = txtHKStartByDate.Text;
                dr["StartByTime"] = startByTimeHK.Text;
                dr["RoomName"] = lstHKRooms.SelectedItem;
                dr["RoomID"] = lstHKRooms.SelectedValue;
                dr["SetID"] = lstHKSet.SelectedValue;
                dr["Comments"] = txtHKComments.Text;
                dr["Status"] = lstHKStatus.SelectedItem;
                dr["RoomLayout"] = lstRoomLayout.SelectedItem;
                dr["DeliveryType"] = "1";
                //FB 1830
                tmpVal = 0;
                decimal.TryParse("0.00", out tmpVal);
                dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);
                dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);
                //dr["DeliveryCost"] = "0.00";
                //dr["ServiceCharge"] = "0.00";
                dr["Timezone"] = lstHKTimezone.SelectedValue;
                UpdateTotalAV(new object(), new EventArgs());
                DataGridItem dgFooter = (DataGridItem)itemsGridHK.Controls[0].Controls[itemsGridHK.Controls[0].Controls.Count - 1];
                Label lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
                dr["TotalCost"] = lblTemp.Text;
                if (chkHKNotify.Checked)
                    dr["Notify"] = "1";
                else
                    dr["Notify"] = "0";
                //if (chkHKReminder.Checked)
                //    dr["Reminder"] = "1";
                //else
                //    dr["Reminder"] = "0";
                //if (CheckQuantity(itemsGridHK, "txtReqQuantity"))
                {
                    if (btnHKSubmit.Text.Equals(obj.GetTranslatedText("Create")))
                    {
                        dr["ID"] = "new";
                        myRows.Add(dr);
                    }
                    else
                    {
                        dr["ID"] = txtHKWorkOrderID.Text;
                        myRows.RemoveAt(Convert.ToInt32(Session["HKEditColumn"]));
                        myRows.InsertAt(dr, Convert.ToInt32(Session["HKEditColumn"]));
                    }
                    string tb = "";
                    foreach (DataGridItem dgi in itemsGridHK.Items)
                    {
                        TextBox temp = new TextBox();
                        temp = (TextBox)dgi.FindControl("txtReqQuantity");
                        tb += dgi.Cells[0].Text.Trim() + "," + dgi.Cells[dgi.Cells.Count - 1].Text.Trim() + "," + temp.Text.Trim() + ";";
                    }
                    dr["ReqQuantity"] = tb;

                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        Session["HKMainGridDS"] = myTable;
                    }
                    else
                    {
                        ViewState["HKMainGridDS"] = myTable;
                    }

                    HKMainGrid.DataSource = myTable;
                    HKMainGrid.DataBind();
                    HKMainGrid.Visible = true;
                    HKItemsTable.Visible = false;
                    btnAddNewHK.Enabled = true;
                    btnAddNewHK.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                    lblHKWOInstructions.Text = obj.GetTranslatedText("To add a new work order, click the button below.");//FB 1830 - Translation
                    errLabel.Text = "";
                }
                //else
                //{
                //    errLabel.Text = "At least one item should have a requested quantity > 0";
                //    errLabel.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + ":" + ex.Message); // "Catering Submit Error";
            }
        }

        #endregion

        #region CheckWorkorderStartEndTime

        protected bool CheckWorkorderStartEndTime(String dStart, String tStart, String dEnd, String tEnd)
        {
            try
            {
                DateTime woStart = DateTime.Parse(dStart + " " + tStart);
                DateTime woEnd = DateTime.Parse(dEnd + " " + tEnd);
                TimeSpan ts = woEnd - woStart;
                //Response.Write(ts.TotalMinutes);
                if (ts.TotalMinutes <= 0)  //FB 1863 
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
                return false;
            }
        }

        #endregion

        #region RefreshRoom

        protected void RefreshRoom(object sender, EventArgs e)
        {
            try
            {
                DateTime dStart, dEnd;
                string RoomVMR = "0";//FB 2448
                int cloudConf = 0; //FB 2717


                    string inxml = "<conferenceTime>";
                    inxml += obj.OrgXMLElement();
                    //inxml += "<userID>" + Session["userID"].ToString() + "</userID>";
                    inxml += "<userID>11</userID>";
                    //Uncommented for FB 1796    
                    inxml += "<confID>" + lblConfID.Text + "</confID>";
                    inxml += "<confType>" + lstConferenceType.SelectedValue.ToString() + "</confType>";//FB 2334
                    //inxml += "<confID>11</confID>";

                    /* if (chkStartNow.Checked)
                     {
                         inxml += "<immediate>1</immediate>";
                         int durationMin = Convert.ToInt32(lstDuration.Text.Split(':')[0]) * 60 + Convert.ToInt32(lstDuration.Text.Split(':')[1]);
                         inxml += "<durationMin>" + durationMin + "</durationMin>";
                     }
                     else
                     {*/
                    inxml += "<immediate>0</immediate>";

                    inxml += "<recurring>0</recurring>";

                    
                    //dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + confStartTime.Text); //new DateTime(Convert.ToInt16(confStartDate.Text.Split('/')[2]), Convert.ToInt16(confStartDate.Text.Split('/')[0]), Convert.ToInt16(confStartDate.Text.Split('/')[1]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                    //dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                    
                    dStart = DateTime.Parse("01/01/1990 12:00:00 AM");
                    dEnd = DateTime.Parse("01/01/1990 12:00:00 AM");                    
                    
                    //Code changed by Offshore for FB Issue 1073 -- end

                    TimeSpan ts = dEnd.Subtract(dStart);
                    //Code changed by Offshore for FB Issue 1073 -- start
                    //FB 1796
                    //inxml += "		<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + "</startDate>";
                    inxml += "		<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(dStart.ToShortDateString()) + "</startDate>";
                    //Code changed by Offshore for FB Issue 1073 -- end
                    inxml += "		<startHour>" + dStart.Hour + "</startHour>";
                    inxml += "		<startMin>" + dStart.Minute + "</startMin>";
                    inxml += "		<startSet>" + dStart.ToString("tt") + "</startSet>";
                    inxml += "		<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>";
                    inxml += "		<ServiceType>" + DrpServiceType.SelectedValue + "</ServiceType>";//FB 2219
                    if (selectedloc.Value != "" && selectedloc.Value.Trim().EndsWith(","))
                        selectedloc.Value = selectedloc.Value.Substring(0, (selectedloc.Value.Trim().Length));
                    inxml += "      <selected>" + selectedloc.Value + "</selected>";

                    //Response.Write(ts.Days + " " + ts.Hours + " " + ts.Minutes);
                    Double durationMin = ts.TotalMinutes; // ts.Days * 24 * 60 + ts.Hours * 60 + ts.Minutes;
                    inxml += "		<durationMin>" + durationMin.ToString() + "</durationMin>";
					//if (chkVMR.Checked) //FB 2448
                    if (lstVMR.SelectedIndex > 0) //FB 2448 // FB 2620
                        RoomVMR = "1";

                    if (isCloudEnabled == 1 && lstVMR.SelectedIndex > 0 && chkCloudConferencing.Checked) //FB 2717
                        cloudConf = 1;
                    
                    inxml += "</conferenceTime>";
                    //string outxml = obj.CallCOM("GetAvailableRoom", inxml, Application["COM_Configpath"].ToString());
                    string outxml = obj.CallMyVRMServer("GetAvailableRoom", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outxml.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outxml);
                        XmlNodeList nodes = xmldoc.DocumentElement.SelectNodes("//locationList/level3List/level3");
                        lstRoomSelection.Items.Clear();
                        treeRoomSelection.Nodes.Clear();
                        TreeNode tn = new TreeNode("All");
                        tn.Expanded = true;
                        treeRoomSelection.Nodes.Add(tn);
                        GetLocationList(nodes);
                    }
                    else
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.ShowErrorMessage(outxml);
                    }
                    /*int countLeaf = 0;
                    int countMid = 0;
                    int countTop = 0;
                    if (!Application["roomExpandLevel"].ToString().Equals("list"))
                    {
                        foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                        {
                            countMid = 0;
                            if (Application["roomExpandLevel"] != null)//Location Issues
                            {
                                if (Application["roomExpandLevel"].ToString() != "")
                                {
                                    if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 2)
                                        tnTop.Expanded = true;
                                }
                            }
                            else
                                tnTop.Expanded = false;

                            foreach (TreeNode tnMid in tnTop.ChildNodes)
                            {
                                if (Application["roomExpandLevel"] != null)//Location Issues
                                {
                                    if (Application["roomExpandLevel"].ToString() != "")
                                    {
                                        if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 3)
                                            tnMid.Expanded = true; // fogbugz case 277
                                    }
                                }
                                else
                                    tnMid.Expanded = false;
                                countLeaf = 0;
                                foreach (TreeNode tn in tnMid.ChildNodes)
                                {
                                    //for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length - 1; i++)//Code changed for Calendar
                                    for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                                        if(tn.Depth.Equals(3) && tn.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                                        {
                                            tn.Checked = true;
                                            countLeaf++;
                                        }
                                }
                                if (countLeaf.Equals(tnMid.ChildNodes.Count))
                                {
                                    tnMid.Checked = true;
                                    countMid++;
                                }
                            }
                            if (countMid.Equals(tnTop.ChildNodes.Count))
                            {
                                tnTop.Checked = true;
                                countTop++;
                            }
                        }
                        if (countTop.Equals(treeRoomSelection.Nodes[0].ChildNodes.Count))
                            treeRoomSelection.Nodes[0].Checked = true;
                    }
                    else
                    {
                        rdSelView.Items.FindByValue("2").Selected = true;
                        rdSelView.SelectedIndex = 1;
                        rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                    }
                    //Code added for Search Room Error - start
                    if (lstRoomSelection != null)
                    {
                        if (lstRoomSelection.Items.Count == 0)
                        {
                            rdSelView.Enabled = false;
                            pnlListView.Visible = false;
                            pnlLevelView.Visible = false;
                            btnCompare.Disabled = true;
                            pnlNoData.Visible = true;
                            openCalendar.Enabled = false;
                            GetAvailableRoom.Enabled = false;
                            MeetingPlanner.Enabled = false;
                        }
                        else if (lstRoomSelection.Items.Count > 0)//FB 1481 - Start
                        {
                            rdSelView.Enabled = true;
                            rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                            btnCompare.Disabled = false;
                            pnlNoData.Visible = false;
                            openCalendar.Enabled = true;
                            GetAvailableRoom.Enabled = true;
                            MeetingPlanner.Enabled = true;
                        }//FB 1481 - End
                    }
                    //Code added for Search Room Error - end*/

                    if (chkRecurrence.Checked) //FB 1587
                    {
                        if(StartDate.Text.Trim() != "")
                        {
                            confStartDate.Text = obj.ControlConformityCheck(StartDate.Text.Trim()); // ZD 100263

                            DateTime.TryParse(StartDate.Text.Trim(), out dStart);
                            confEndDate.Text = obj.ControlConformityCheck(StartDate.Text.Trim()); // ZD 100263
                        }
                    }
                    string immediate = "0"; //FB 2534
                    if (chkStartNow.Checked)
                    {
                        int durationMins = Convert.ToInt32(lstDuration.Text.Split(':')[0]) * 60 + Convert.ToInt32(lstDuration.Text.Split(':')[1]);
                        dStart = DateTime.Now;
                        dEnd = dStart.AddMinutes(Convert.ToDouble(durationMins));
                        immediate = "1"; //FB 2534

                    }
                    else
                    {
                        dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //new DateTime(Convert.ToInt16(confStartDate.Text.Split('/')[2]), Convert.ToInt16(confStartDate.Text.Split('/')[0]), Convert.ToInt16(confStartDate.Text.Split('/')[1]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                        dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text)); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);

                        Double setupDuration = Double.MinValue;//FB 2634
                        Double tearDuration = Double.MinValue;

                        if (!chkEnableBuffer.Checked && enableBufferZone == "1")//FB 2398
                        {
                            Double.TryParse(SetupDuration.Text, out setupDuration);
                            Double.TryParse(TearDownDuration.Text, out tearDuration);
                            dStart = dStart.AddMinutes(-setupDuration);
                            dEnd = dEnd.AddMinutes(tearDuration);
                        }
                    }

                    String strSrc = "RoomSearch.aspx?rmsframe=" + selectedloc.Value + "&confID=" + lblConfID.Text + "&stDate=" + dStart.ToString() + "&enDate=" + dEnd.ToString() + "&tzone=" + lstConferenceTZ.SelectedValue + "&serType=" + DrpServiceType.SelectedValue + "&isVMR=" + RoomVMR + "&CloudConf=" + cloudConf + "&immediate=" + immediate + "&frm=frmConferenceSetUp&ConfType=" + lstConferenceType.SelectedValue.ToString();//FB 2219 2448 , FB 2534 //FB 2637 //FB 2694 //FB 2717

                   if(lstConferenceType.SelectedValue == "7") //FB 2334
                    strSrc += "&dedVid=";

                   if (lstConferenceType.SelectedValue == "4") //FB 2400
                       strSrc += "&isTel=";
                    
                    RoomFrame.Attributes.Add("src", strSrc);

            }
            catch (Exception ex)
            {
                log.Trace("RefreshRoom:Error in getting Available Room(s)." + ex.Message); //FB 1881
                //errLabel.Text = "Error in getting Available Room(s). Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        #endregion

        //code commentted for FB 1319 - Start

        #region rdSelView_SelectedIndexChanged

        protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selRooms = "";
            // code changed for FB 1319 -- start
            Int32 cnt = 0;
            Int32 mCnt = 0;
            Int32 tCnt = 0;
            if (rdSelView.SelectedValue.Equals("2"))
            {
                pnlListView.Visible = true;
                pnlLevelView.Visible = false;
                lstRoomSelection.ClearSelection();
                HtmlInputCheckBox selectAll = (HtmlInputCheckBox)FindControl("selectAllCheckBox");

                if (selectedloc.Value.Trim() != "")
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        //for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length - 1; i++)//Code changed for Calendar
                        for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                            if (lstItem.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                            {
                                lstItem.Selected = true;
                                cnt = cnt + 1;
                            }
                    }

                    if (selectAll != null)
                    {
                        if (cnt == lstRoomSelection.Items.Count)
                            selectAll.Checked = true;
                        else
                            selectAll.Checked = false;
                    }
                }
                else
                {
                    if (selectAll != null)
                        selectAll.Checked = false;
                }
            }
            else
            {
                pnlLevelView.Visible = true;
                pnlListView.Visible = false;

                foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                {
                    tCnt = 0;
                    foreach (TreeNode tnMid in tnTop.ChildNodes)
                    {
                        mCnt = 0;
                        foreach (TreeNode tn in tnMid.ChildNodes)
                        {
                            tn.Checked = false;
                            if (selectedloc.Value.Trim() != "")
                            {
                                //for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length - 1; i++)//Code changed for Calendar
                                for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                                    if (tn.Depth.Equals(3) && tn.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                                    {
                                        tn.Checked = true;
                                        mCnt++;
                                    }
                            }
                        }

                        if (mCnt == tnMid.ChildNodes.Count)
                        {
                            tnMid.Checked = true;
                            tCnt++;
                        }
                        else
                            tnMid.Checked = false;
                    }

                    if (tCnt == tnTop.ChildNodes.Count)
                    {
                        tnTop.Checked = true;
                        treeRoomSelection.Nodes[0].Checked = true;
                    }
                    else
                    {
                        tnTop.Checked = false;
                        treeRoomSelection.Nodes[0].Checked = false;
                    }
                }

            }
            // code changed for FB 1319 -- end
        }

        #endregion

        //code commentted for FB 1319 - end

        #region treeRoomSelection_TreeNodeCheckChanged


        protected void treeRoomSelection_TreeNodeCheckChanged(object sender, EventArgs e)
        {

            foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
            {
                if (tnTop.Parent.Checked.Equals(true)) tnTop.Checked = true;
                foreach (TreeNode tnMid in tnTop.ChildNodes)
                {
                    if (tnMid.Parent.Checked.Equals(true)) tnMid.Checked = true;
                    foreach (TreeNode tn in tnMid.ChildNodes)
                    {
                        if (tn.Parent.Checked.Equals(true)) tn.Checked = true;
                    }
                }
            }
            ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");
            lstRooms.Items.Clear();
            //lstCATRooms.Items.Clear();
            lstHKRooms.Items.Clear();
            lstRooms.Items.Add(li);
            //lstCATRooms.Items.Add(li);
            lstHKRooms.Items.Add(li);
            if (treeRoomSelection.CheckedNodes.Count > 0)
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    if (tn.Depth.Equals(3))
                    {
                        selRooms += ", " + tn.Value;
                        li = new ListItem(tn.Text, tn.Value);
                        lstRooms.Items.Add(li);
                        //lstCATRooms.Items.Add(li);
                        lstHKRooms.Items.Add(li);

                    }

        }

        #endregion

        #region treeRoomSelection_SelectedNodeChanged

        protected void treeRoomSelection_SelectedNodeChanged(object sender, EventArgs e)
        {
            errLabel.Visible = true;
            errLabel.Text = "";
            //            errLabel.Text = "saima2: " + e.ToString();
        }

        #endregion

        #region LoadPreview

        protected void LoadPreview()
        {
            string DateTIme, Date, Time; //FB 2281
			 //FB 2430 start
            hdnSmartP2PTotalEps.Value = "0";
            if (hdnCrossEnableSmartP2P.Value == "1" && lstConferenceType.SelectedValue != "4" && 
                lstConferenceType.SelectedValue != "7" && dgOnflyGuestRoomlist.Items.Count == 0)
            {
                int TotalEndpoint = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length + dgRooms.Items.Count;
                    for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++)
                    {
                        if (!txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1"))
                            TotalEndpoint--;
                    }
               
                hdnSmartP2PTotalEps.Value = TotalEndpoint.ToString();
            }
            //FB 2430 end
            plblConfName.Text = obj.ControlConformityCheck(ConferenceName.Text); // ZD 100263
			//FB 2634
            /* *** CODE ADDED FOR Buffer Zone *** --Start */
            //SetupTime.Text = SetupDateTime.Text;
            //TeardownTime.Text = TearDownDateTime.Text;
            /* *** CODE ADDED FOR Buffer Zone *** --End */

            if (!chkStartNow.Checked)
            {
                //FB 1911
                if (Recur.Value == "" && RecurSpec.Value == "")
                {
                    plblConfStartDateTime.Text = obj.ControlConformityCheck(confStartDate.Text + " " + confStartTime.Text); // ZD 100263
                    plblConfEndDateTime.Text = obj.ControlConformityCheck(confEndDate.Text + " " + confEndTime.Text); // ZD 100263
                    /* *** code added for buffer zone -- start */
					//FB 2634
                    //if (enableBufferZone == "1" && chkEnableBuffer.Checked) //FB 2398
                    //{
                    //    //lblSetupDateTime.Text = "Attendee Show-up/Setup Completed";
                    //    //lblTeardownDateTime.Text = "Attendee Leave/Teardown Started";
                    //    lblSetupDateTime.Text = obj.GetTranslatedText("Conference Start");//FB 1830 - Translation
                    //    lblTeardownDateTime.Text = obj.GetTranslatedText("Conference End");//FB 1830 - Translation

                    //    PSndDate.InnerHtml = obj.GetTranslatedText("Pre Conference Start");//FB 1830 - Translation
                    //    PEndDate.InnerHtml = obj.GetTranslatedText("Post Conference End");//FB 1830 - Translation
                    //    //FB 2634
                    //    //plblSetupDTime.Text = SetupDate.Text + " " + SetupTime.Text;
                    //    //plblTeardownDTime.Text = TearDownDate.Text + " " + TeardownTime.Text;
                    //}

                    //FB 2634,
                    if (enableBufferZone == "1")// && chkEnableBuffer.Checked) //FB 2398 //FB 2634
                    {
                        PSndDate.InnerHtml = obj.GetTranslatedText("Conference Start");
                        PEndDate.InnerHtml = obj.GetTranslatedText("Conference End");

                        lblSetupDateTime.Text = obj.GetTranslatedText("Set-up (Minutes)");
                        lblTeardownDateTime.Text = obj.GetTranslatedText("Tear Down (Minutes)");

                        SetupDuration.Text = obj.ControlConformityCheck(SetupDuration.Text); // ZD 100263
                        plblSetupDTime.Text = SetupDuration.Text; //SetupDate.Text + " " + SetupTime.Text;
                        TearDownDuration.Text = obj.ControlConformityCheck(TearDownDuration.Text); // ZD 100263
                        plblTeardownDTime.Text = TearDownDuration.Text; //TearDownDate.Text + " " + TeardownTime.Text; 
                    }
                        
                    /* *** code added for buffer zone -- end */
                    plblConfRecurrance.Text = obj.GetTranslatedText("No Recurrence");//FB 1830 - Translation
                }
                else
                {

                    /* *** code added for buffer zone *** -- Start */
                    if (enableBufferZone == "1")
                    {
                        //lblSetupDateTime.Text = "Attendee Show-up/Setup Completed Time";
                        //lblTeardownDateTime.Text = "Attendee Leave/Teardown Started Time";
                        //FB 2634
                        PSndDate.InnerHtml = obj.GetTranslatedText("Conference Start");//FB 1830 - Translation
                        PEndDate.InnerHtml = obj.GetTranslatedText("Conference End");//FB 1830 - Translation

                        lblSetupDateTime.Text = obj.GetTranslatedText("Set-up (Minutes)");//FB 1830 - Translation
                        lblTeardownDateTime.Text = obj.GetTranslatedText("Tear Down (Minutes)");//FB 1830 - Translation

                        String[] bufferStr = null;
                        //FB 2634
                        plblSetupDTime.Text = obj.ControlConformityCheck(SetupDuration.Text); //SetupTime.Text.Trim(); // ZD 100263
                        plblTeardownDTime.Text = obj.ControlConformityCheck(TearDownDuration.Text); //TeardownTime.Text.Trim(); // ZD 100263

                        if (hdnBufferStr.Value != "")
                        {
                            bufferStr = hdnBufferStr.Value.Split('&');

                            if (bufferStr.Length > 0)
                            {
                                plblSetupDTime.Text = bufferStr[0];
                                plblTeardownDTime.Text = bufferStr[1];
                            }
                            else
                            {
								//FB 2634	
                                plblSetupDTime.Text = obj.ControlConformityCheck(SetupDuration.Text); //SetupTime.Text.Trim(); // ZD 100263
                                plblTeardownDTime.Text = obj.ControlConformityCheck(TearDownDuration.Text); //TeardownTime.Text.Trim(); // ZD 100263
                            }
                        }
                        plblSetupDTime.Text = plblSetupDTime.Text.Trim() + obj.GetTranslatedText(" (Applicable for all instances)");
                        plblTeardownDTime.Text = plblTeardownDTime.Text + obj.GetTranslatedText(" (Applicable for all instances)");
                    }

                    /* *** code added for buffer zone *** -- End */

                    plblConfStartDateTime.Text = obj.GetTranslatedText("See Recurring Pattern");//FB 1830 - Translation
                    plblConfEndDateTime.Text = obj.GetTranslatedText("See Recurring Pattern");//FB 1830 - Translation

                    /* *** Recurrence Fixes -- Edit dirty Instances Start *** */
                    if (Session["DirtyText"] != null)
                    {
                        if (Session["DirtyText"].ToString() != "")
                            RecurringText.Value = Session["DirtyText"].ToString();
                    }
                    /* *** Recurrence Fixes -- Edit dirty Instances End *** */

                    plblConfRecurrance.Text = RecurringText.Value;
                }
            }
            else
            {
                //Added for FB 1428 START
                if (client.ToString().ToUpper() == "MOJ")//Added for MOJ
                {
                    plblConfStartDateTime.Text = "Immediate Hearing";
                    trRec.Attributes.Add("style", "display:none");
                }
                else
                    //Added for FB 1428 End
                plblConfStartDateTime.Text = obj.GetTranslatedText("Immediate Conference");//FB 1830 - Translation
                plblConfEndDateTime.Text = obj.GetTranslatedText("Duration: ") + obj.ControlConformityCheck(lstDuration.Text) + "(hh:mm)";//FB 1830 - Translation ZD 100263
                plblConfRecurrance.Text = RecurringText.Value;
               
            }
            // code added for FB 1116 - Start
            txtApprover4.Text = obj.ControlConformityCheck(txtApprover4.Text); // ZD 100263
            plblHostName.Text = txtApprover4.Text;
            if (hdnApproverMail.Text != "")
                plblHostName.Text += " (" + hdnApproverMail.Text + ")";
            // code added for FB 1116 - End
            
            //FB 2501 Starts
            if (lstStartMode.Text != "")
                plblStartMode.Text = lstStartMode.SelectedItem.ToString();
            //FB 2501 Ends
            
            //FB 2446 - Start
            //FB 2501 starts
            txtApprover7.Text = obj.ControlConformityCheck(txtApprover7.Text); // ZD 100263
            plblRequestorName.Text = txtApprover7.Text;
            if (hdnRequestorMail.Text != "")
                plblRequestorName.Text += " (" + hdnRequestorMail.Text + ")";
            //FB 2501 ends
            if (hdnCrossrecurEnable != null && hdnCrossrecurEnable.Value != "") 
                Enablerecurrence = hdnCrossrecurEnable.Value;
            else if (Session["enableRecurrance"] != null)
                Enablerecurrence = Session["enableRecurrance"].ToString();
            if (Enablerecurrence != null)
                if (Enablerecurrence == "0" && chkRecurrence.Checked == false)
                    trRec.Visible = false;
                else
                    trRec.Visible = true;
            //FB 2446 - End

            if (lstConferenceType.SelectedValue.Equals("7"))//FB 1865
                    confPassword.Value = "";
            confPassword.Value = obj.ControlConformityCheck(confPassword.Value); // ZD 100263
            plblPassword.Text = confPassword.Value; 
            plblConfType.Text = lstConferenceType.SelectedItem.ToString();
            ConferenceDescription.Text = obj.ControlConformityCheck(ConferenceDescription.Text); // ZD 100263
            plblConfDescription.Text = ConferenceDescription.Text; //FB 2236 
            plblAVWorkOrders.Text = AVMainGrid.Items.Count.ToString(); // +" orders";
            plblCateringWorkOrders.Text = CATMainGrid.Items.Count.ToString();// +" orders";
            plblHouseKeepingWorkOrders.Text = HKMainGrid.Items.Count.ToString();// +" orders";
            if (txtPartysInfo.Text == "")
            {
                plblPartys.Text = obj.GetTranslatedText("None");//FB 1830 - Translation
                lblAudioBridge.Text = obj.GetTranslatedText("None"); //FB 2359
            }
            else
            {
                //FB 1888 start
                string[] partys = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries);
                //string[] partys = txtPartysInfo.Text.Split(';');
                lblAudioBridge.Text = "";//FB 2359
                plblPartys.Text = "";
                String parttext = "";
                DataGridItem gridCol = null;  //FB 2359

                for (int i = 0; i < partys.Length; i++)
                {
                    string[] temp = partys[i].Split(ExclamDelim,StringSplitOptions.RemoveEmptyEntries);
                    //FB 2359 start
                    
                    if (hdnAudioBridges.Value.IndexOf(temp[3]) >= 0 || temp[2].Trim() == "" || temp[2].Trim() == "&nbsp;")
                    {
                        lblAudioBridge.Text += temp[1].Replace("++", ",") + temp[2].Replace("++", ",");
                        for (int j = 0; j < dgUsers.Items.Count; j++)
                        {
                          gridCol = dgUsers.Items[j];
                          if (((TextBox)gridCol.FindControl("lblUserID")).Text.Trim() == temp[0].Trim() && temp[4].Trim() == "1")
                          {
                              if (((TextBox)gridCol.FindControl("txtConfCode")).Text.Trim() != "")
                                  lblAudioBridge.Text += "> Code: " + ((TextBox)gridCol.FindControl("txtConfCode")).Text;
                              if (((TextBox)gridCol.FindControl("txtleaderPin")).Text.Trim() != "")
                                  lblAudioBridge.Text += " > Pin: " + ((TextBox)gridCol.FindControl("txtleaderPin")).Text;

                              break;
                          }
                        }

                        lblAudioBridge.Text += "<br />";
                    }
                    else if (temp.Length > 2)
                    {
                        plblPartys.Text += "<a href='mailto:" + temp[3];
                        plblPartys.Text += "' target='_blank'>" + obj.ControlConformityCheck(temp[1].Replace("++", ",")); //FB 1640 // ZD 100263
                        plblPartys.Text += " " + obj.ControlConformityCheck(temp[2].Replace("++", ",")) + "</a>, "; // ZD 100263
                    }
                    //FB 1888 end
                }
                if (lblAudioBridge.Text.Length > 2)
                    lblAudioBridge.Text = lblAudioBridge.Text.Substring(0, lblAudioBridge.Text.Length - 2);
                else
                    lblAudioBridge.Text = obj.GetTranslatedText("None");

                if (plblPartys.Text.Length > 2)
                    plblPartys.Text = plblPartys.Text.Substring(0, plblPartys.Text.Length - 2);
                else
                    plblPartys.Text = obj.GetTranslatedText("None");

                //FB 2359 end
            }
            plblLocation.Text = " ";
            SyncRoomSelection();
            plblGuestLocation.Text = " ";//FB 2426
            //FB 2595 Start
            lblNetworkState.Text = "";
            if (drpNtwkClsfxtn.SelectedValue == "1")
                lblNetworkState.Text = obj.GetTranslatedText("NATO Secret");
            else
                lblNetworkState.Text = obj.GetTranslatedText("NATO Unclassified");

            if(hdnNetworkSwitching.Value == "2")
                trPreviewNetworkState.Attributes.Add("Style", "display:;");
            else if (hdnNetworkSwitching.Value == "2")
                trPreviewNetworkState.Attributes.Add("Style", "display:none;");
            //FB 2595 End
            //Response.Write(selRooms);
            ArrayList RoomIDs = new ArrayList(selRooms.Split(',').Length - 1);
            IEnumerator enumRoomIDs = RoomIDs.GetEnumerator();
            enumRoomIDs.Reset();
            while (enumRoomIDs.MoveNext())
            {
                //Response.Write(enumRoomIDs.ToString());
                //if (enumRoomIDs.Equals((String)dgi.Cells[7].Text))
                //    flag = true;
            }
            bool flag = true;

            //FB 2281 - Start
            int setuptime = 0, teardowntime = 0;
            int.TryParse(SetupDuration.Text, out setuptime);
            int.TryParse(TearDownDuration.Text, out teardowntime);
            
            DateTime confstart = DateTime.Now;
            DateTime confEnd = DateTime.Now;
            DateTime date1 = DateTime.Now;
            DateTime date2 = DateTime.Now;
            int result, result2;
            confstart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text.Trim())).AddMinutes(-setuptime);
            confEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text.Trim())).AddMinutes(teardowntime);                        
            
            foreach (DataGridItem dgi in AVMainGrid.Items)
            {
                //Response.Write(RoomIDs.Contains((String)dgi.Cells[7].Text));
                flag = false;
                //Code changed by offshore for FB ISsue 1073 -- start
                //if (DateTime.Parse(dgi.Cells[4].Text.Trim() + " " + dgi.Cells[5].Text.Trim()).Equals(DateTime.Parse(confStartDate.Text.Trim() + " " + confStartTime.Text.Trim())) && DateTime.Parse(dgi.Cells[6].Text.Trim() + " " + dgi.Cells[7].Text.Trim()).Equals(DateTime.Parse(confEndDate.Text.Trim() + " " + confEndTime.Text.Trim())))
                //FB 2181 - Start
                //if (DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[4].Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[5].Text.Trim())) <= (confstart)
                //    && DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[6].Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[7].Text.Trim())) <= (confEnd))                
                //        flag = true;
                date1 = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[4].Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[5].Text.Trim()));
                date2 = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[6].Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[7].Text.Trim()));
                result = DateTime.Compare(date1, confstart);
                result2 = DateTime.Compare(date2, confEnd);
                if (result < 0 || result2 > 0)
                    flag = false;                    
                else
                    flag = true;                   
                //FB 2181 - End
                //Code changed by offshore for FB ISsue 1073 -- end
            }
            plblAVInstructions.Text = ""; //fogbugz case 402
            //FB 2181 - Start
            if (flag == false)
                plblAVInstructions.Text = obj.GetTranslatedText(" (Work orders start/end date/time do not match with conference start/end date/time.)");//FB 1830 - Translation
            flag = true;
            //FB 2181 - End
            foreach (DataGridItem dgi in CATMainGrid.Items)
            {

                DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                Date = DateTIme.Split(' ')[0];
                if (Session["timeFormat"].ToString().Equals("2"))
                    Time = DateTIme.Split(' ')[1];
                else
                {
                    if(myVRMNet.NETFunctions.GetDefaultDate(DateTIme) == "")
                        Time = (myVRMNet.NETFunctions.GetFormattedTime(DateTIme, format));
                    else
                        Time = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(DateTIme)).ToString(tformat);
                }
                flag = false;
                //Code changed by offshore for FB ISsue 1073 -- start
                date1 = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time));
                result = DateTime.Compare(date1, confstart);
                if (result < 0)
                    flag = false;
                else
                    flag = true;    
                //FB 2181 - End
            }
            plblCATInstructions.Text = ""; //fogbugz case 402
            //FB 2181 - Start
            if (flag == false)
                plblCATInstructions.Text = obj.GetTranslatedText(" (Work orders deliver by date/time do not match with conference date/time.)");//FB 1830 - Translation
            //FB 2181 - End
            flag = true;
            foreach (DataGridItem dgi in HKMainGrid.Items)
            {
                //Response.Write(RoomIDs.Contains((String)dgi.Cells[7].Text));
                flag = false;
                //Code changed by offshore for FB ISsue 1073 -- start                
                //FB 2181 - Start
                date1 = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[4].Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[5].Text.Trim()));
                date2 = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[6].Text.Trim()) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(dgi.Cells[7].Text.Trim()));
                result = DateTime.Compare(date1, confstart);
                result2 = DateTime.Compare(date2, confEnd);
                if (result < 0 || result2 > 0)
                    flag = false;
                else
                    flag = true;                   
                //FB 2181 - End
                //Code changed by offshore for FB ISsue 1073 -- end
            }
            plblHKInstructions.Text = ""; //fogbugz case 402
            //FB 2181 - Start
            if (flag == false)
                plblHKInstructions.Text = obj.GetTranslatedText(" (Work orders start/end date/time do not match with conference start/end date/time.)");//FB 1830 - Translation
            //FB 2181 - End
            //FB 2281 - End
            if (treeRoomSelection.CheckedNodes.Count.Equals(0))
                plblLocation.Text = obj.GetTranslatedText("None");//FB 1830 - Translation
            else
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    if (tn.Depth.Equals(3))
                        plblLocation.Text += tn.Parent.Parent.Text + " > " + tn.Parent.Text + " > " + "<a href='#' onclick='javascript:chkresource(\"" + tn.Value + "\");'>" + tn.Text + "</a><br>";
                }
            //FB 2426 Start
            if (!lstConferenceType.SelectedValue.Equals("4") && !lstConferenceType.SelectedValue.Equals("7") && !(lstVMR.SelectedIndex > 0) && !lstConferenceType.SelectedValue.Equals("8") && !(chkPCConf.Checked)) //FB 2448 //FB 2694 //FB 2620 //FB 2819
            {
                if (dgOnflyGuestRoomlist.Items.Count != 0)
                {
                    foreach (DataGridItem item in dgOnflyGuestRoomlist.Items)
                    {
                        plblGuestLocation.Text += Session["OnflyTopTierName"].ToString() + " > " + Session["OnflyMiddleTierName"].ToString() + " > " + item.Cells[2].Text.ToString() + "<br>";
                    }
                }
                else
                {
                    plblGuestLocation.Text = obj.GetTranslatedText("None");
                }
            }
            else
            {
                trPreviewGuestLocation.Attributes.Add("style", "display:none");
            }
            //FB 2426 End

            //Custom Attribute Fixes
            //if (enableEntity == "1")
            //{
                plblCustomOption.Text = "";
                
                if (CAObj == null)
                    CAObj = new myVRMNet.CustomAttributes();
                
                //FB 2501 VNOC starts
                plblCustomOption.Text = CAObj.CustomAttributeValues(custControlIDs, tblCustomAttribute);
                //FB 2501 VNOC end
            //}

            plblPublic.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
            plblOpenForRegistration.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
            if (!chkOpenForRegistration.Checked)
                plblOpenForRegistration.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
            if (!chkPublic.Checked)
            {
                plblPublic.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                plblOpenForRegistration.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
            }
            plblICAL.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
            if (chkICAL.Checked)
                plblICAL.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
            //Wizard1.ActiveStep.StepType = WizardStepType.Complete;

            //FB 1830 Email Edit - start
            hdnemailalert.Value = "";
            if (isEditMode == "1")
            {
                if (CheckForUserInput())
                {
                    hdnemailalert.Value = "2"; //alert user for email notification on edit
                }
            }
            //FB 1830 Email Edit - end

            //FB 2376
            plblConfVMR.Text = obj.GetTranslatedText("No");
			//if (chkVMR.Checked)
            if (lstVMR.SelectedIndex > 0) // FB 2620
                plblConfVMR.Text = obj.GetTranslatedText("Yes");

            //FB 2632 //FB 2670 START
            string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
            string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
            string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
            string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();
            
            if(EnableOnsiteAV == "1")
            plblOnsiteAV.InnerText = obj.GetTranslatedText("No");
            if (chkOnSiteAVSupport.Checked)
                plblOnsiteAV.InnerText = obj.GetTranslatedText("Yes");
            
            if(EnableMeetandGreet =="1")
            plblMeetandGreet.InnerText = obj.GetTranslatedText("No");
            if (chkMeetandGreet.Checked)
                plblMeetandGreet.InnerText = obj.GetTranslatedText("Yes");
            
            if(EnableConciergeMonitoring =="1")
            plblConciergeMonitoring.InnerText = obj.GetTranslatedText("No");
            if (chkConciergeMonitoring.Checked)
                plblConciergeMonitoring.InnerText = obj.GetTranslatedText("Yes");

            if (EnableDedicatedVNOC == "1")
            {
                if (txtVNOCOperator.Text != "")
                {
                    plblDedicatedVNOC.InnerText = txtVNOCOperator.Text.Replace("\r\n", ",").TrimEnd(','); 
                }
                else
                    plblDedicatedVNOC.InnerText = obj.GetTranslatedText("No");
            }


            //FB 2670 END
			//FB 2694 Start
            if (lstConferenceType.SelectedValue.Equals("8"))
            {
                lblConfName.Attributes.Add("style", "visibility:hidden");
                lblConfDesc.Attributes.Add("style", "display:none");
                trPwd.Attributes.Add("style", "display:none");
                trPreviewNetworkState.Attributes.Add("style", "display:none");
                traudiobrdige.Attributes.Add("style", "display:none");
                trPreviewGuestLocation.Attributes.Add("style", "display:none");
                trPublicOpen.Attributes.Add("style", "display:none");
                trAutParticipant.Attributes.Add("Style", "display:none");
                trVMRcall.Attributes.Add("Style","display:none");
                trparticipant.Attributes.Add("Style", "display:none");
                trPLBLHKWO.Attributes.Add("Style", "display:none");
                trSendIcalAttachment.Attributes.Add("Style", "display:none");
                TrCTNumericID.Attributes.Add("style", "display:none");//FB 2870
                //FB 3007 START
                tdConcierge.Attributes.Add("style", "display:none");
                tdOnsiteAV.Attributes.Add("style", "display:none");
                tdMeetandGreet.Attributes.Add("style", "display:none");
                tdConciergeMonitoring.Attributes.Add("style", "display:none");
                tdDedicatedVNOC.Attributes.Add("style", "display:none");
                //FB 3007 END
            }
            //FB 2694 End
            //FB 2998
            trMCUConnect.Visible = false;
            trMCUDisconnect.Visible = false;
            if ((lstConferenceType.SelectedValue == "2" || lstConferenceType.SelectedValue == "6") && !chkStartNow.Checked
                 && enableBufferZone == "1" && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
            {
                lblPMCUConnect.Text = txtMCUConnect.Text;
                lblPMCUDisconnect.Text = txtMCUDisConnect.Text;

                if (mcuSetupDisplay == "1")
                    trMCUConnect.Visible = true;
                if (mcuTearDisplay == "1")
                    trMCUDisconnect.Visible = true;
            }
            
        }

        #endregion

        #region SyncRoomSelection

        protected void SyncRoomSelection()
        {
            selRooms = "";
            try
            {
                foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                    foreach (TreeNode tnMid in tnTop.ChildNodes)
                        foreach (TreeNode tn in tnMid.ChildNodes)
                        {
                            tn.Checked = false;

                            if (selectedloc.Value != "")
                            {
                                foreach (String s in selectedloc.Value.Split(','))
                                    if ((tn.Value.Equals(s.Trim())))
                                    {
                                        tn.Checked = true;
                                        selRooms += tn.Value + ",";

                                    }
                            }
                        }

                /*if (rdSelView.SelectedIndex.Equals(1))//RollBack
                {
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                tn.Checked = false;
                                foreach (ListItem lstItem in lstRoomSelection.Items)
                                    if ((lstItem.Selected == true) && (tn.Value.Equals(lstItem.Value)))
                                    {
                                        tn.Checked = true;
                                        selRooms += tn.Value + ",";

                                    }
                            }
                }
                else //FB 1149 --start
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                        foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                        {
                            if (tn.Value.Equals(lstItem.Value) && (tn.Depth.Equals(3)))
                            {
                                lstItem.Selected = true;
                                selRooms += lstItem.Value + ", ";
                            }
                        }
                //} fb 1149 --end*/
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region openCalendar_Click

        protected void openCalendar_Click(object sender, EventArgs e)
        {
            selRooms = ", ";
            foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                selRooms += tn.Value + ",";
            //Code changed by offshore for FB 1073 -- start
            //string tmpstr = selRooms + "d=" + confStartDate.Text;
            string tmpstr = selRooms + "d=" + myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text);
            //Code changed by offshore for FB 1073 -- end
            //openCalendar.OnClientClick = @"javascript:goToCal('" + selRooms + "')";
        }

        #endregion

        #region AVMainGrid_DeleteCommand

        protected void AVMainGrid_DeleteCommand(object sender, DataGridCommandEventArgs e)
        {
            DataTable DSTemp; // = new DataView();

            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
            {
                DSTemp = (DataTable)(Session["AVMainGridDS"]); // FB 2050
            }
            else
            {
                DSTemp = (DataTable)(ViewState["AVMainGridDS"]); // FB 2050
            }

            /* *** FB 498 work order fixes - start *** */
            if (e.Item.Cells[0].Text == "new")
            {
                if (DSTemp != null)
                {
                    string filterExp = "RowUID='" + e.Item.ItemIndex.ToString() + "'";
                    DataRow[] drArr = DSTemp.Select(filterExp);
                    foreach (DataRow dr in drArr)
                    {
                        DSTemp.Rows.Remove(dr);
                    }
                    int lpcount = 0;
                    foreach (DataRow dr in DSTemp.Rows)
                    {
                        dr["RowUID"] = lpcount;
                        lpcount++;
                    }
                }
            }
            else     //On Edit
            {
                if (DeleteWorkOrder(e.Item.Cells[0].Text))
                {
                    if (DSTemp != null)
                    {
                        string filterExp = "ID='" + e.Item.Cells[0].Text + "'";
                        DataRow[] drArr = DSTemp.Select(filterExp);
                        foreach (DataRow dr in drArr)
                        {
                            DSTemp.Rows.Remove(dr);
                        }
                    }
                }
            }
            // Commented out for FB 498
            //if (!DSTemp.Rows[e.Item.ItemIndex]["ID"].ToString().Equals("new"))
            //{
            //    if (DeleteWorkOrder(DSTemp.Rows[e.Item.ItemIndex]["ID"].ToString()))
            //        DSTemp.Rows[e.Item.ItemIndex].Delete();
            //}
            //else
            //    DSTemp.Rows[e.Item.ItemIndex].Delete();

            /* *** FB 498 work order fixes - end *** */

            AVMainGrid.DataSource = DSTemp;
            AVMainGrid.DataBind();

            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
            {
                Session.Add("AVMainGridDS", (DataTable)AVMainGrid.DataSource);
            }
            else
            {
                ViewState.Add("AVMainGridDS", (DataTable)AVMainGrid.DataSource);
            }

            if (DSTemp.Rows.Count.Equals(0))
                AVMainGrid.Visible = false;
        }

        #endregion

        #region HKMainGrid_DeleteCommand

        protected void HKMainGrid_DeleteCommand(object sender, DataGridCommandEventArgs e)
        {
            DataTable DSTemp; // = new DataView();

            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
            {
                DSTemp = (DataTable)(Session["HKMainGridDS"]);
            }
            else
            {
                DSTemp = (DataTable)(ViewState["HKMainGridDS"]);
            }

            if (!DSTemp.Rows[e.Item.ItemIndex]["ID"].ToString().Equals("new"))
            {
                if (DeleteWorkOrder(DSTemp.Rows[e.Item.ItemIndex]["ID"].ToString()))
                    DSTemp.Rows[e.Item.ItemIndex].Delete();
            }
            else
                DSTemp.Rows[e.Item.ItemIndex].Delete();
            //FB 2181 Start
            if (DSTemp.Rows.Count.Equals(0)) 
                HKMainGrid.Visible = false;
            //FB 2181 End
            HKMainGrid.DataSource = DSTemp;
            HKMainGrid.DataBind();

            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
            {
                Session.Add("HKMainGridDS", (DataTable)HKMainGrid.DataSource);
            }
            else
            {
                ViewState.Add("HKMainGridDS", (DataTable)HKMainGrid.DataSource);
            }
        }

        #endregion

        #region DeleteWorkOrder

        protected bool DeleteWorkOrder(string woID)
        {
            try
            {
                string inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><ConferenceID>" + lblConfID.Text + "</ConferenceID><WorkorderID>" + woID + "</WorkorderID></login>";//Organization Module Fixes
                string outxml = obj.CallMyVRMServer("DeleteWorkOrder", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                    errLabel.Visible = true;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace("DeleteWorkOrder:Error deleteting work order." + ex.Message);//FB 1881
                //errLabel.Text = "Error deleteting work order.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        #region SetConference

        protected void SetConference(object sender, EventArgs e)
        {
            //Response.Write(" in setconf:");
            /*
             * string strScript = "<script>";
                        strScript += "SetConference();";
                        strScript += "</script>";
                        RegisterClientScriptBlock("addItems", strScript);
            */
            //FB 1221
            //if (!lstConferenceType.SelectedValue.Equals("7") && Request.QueryString["t"].Equals("t")

            /* *** Code added for FB 1425 * ***/

            CheckTime(null, null);//Code added for Fb 1728
            // FB 2692 Starts
            if (isCheckTimeError == true)
            {
                Wizard1.ActiveViewIndex = 0;
                TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
                return;
            }
            // FB 2692 Ends
            /* *** code added for buffer zone -- start *** */
			//FB 2634
            //SetupTime.Text = SetupDateTime.Text;
            //TeardownTime.Text = TearDownDateTime.Text;



            if ((!(lstConferenceType.SelectedValue.Equals("7") || (lstConferenceType.SelectedValue.Equals("8"))) && Request.QueryString["t"].Equals("t")) || (enableAV.Equals("0") && !(lstVMR.SelectedIndex >0) && !(chkPCConf.Checked)))//For AV Switch //FB 2448 //FB 2694 //FB 2819
            {
                UpdateAdvAVSettings(new object(), new EventArgs());
            }
            //FB 1221
            //if (!lstConferenceType.SelectedValue.Equals("7") && hasVisited.Text.Equals("0")) For Av Switch
            if (!(lstConferenceType.SelectedValue.Equals("7") || lstConferenceType.SelectedValue.Equals("8")) && hasVisited.Text.Equals("0") && enableAV.Equals("1") && !(lstVMR.SelectedIndex > 0) && !(chkPCConf.Checked))// For Av Switch //FB 2448 //FB 2694 //FB 2620 //FB 2819
                errLabel.Text = obj.GetTranslatedText("Please visit the Advanced Audio/Video Settings tab");//FB 1830 - Translation
            else
                if (!SetConference())
                {
                    //errLabel.Text += "\nError in SetConference Command."; Case FB 731
                    errLabel.Visible = true;
                }


            
        }

        #endregion

        #region GenerateICAL

        protected String GenerateICAL()
        {
            if (!chkICAL.Enabled || !chkICAL.Checked)
                return "";
            
            String recurPattern = "";
            if (!Recur.Value.Trim().Equals(""))
            {
                recurPattern = GetICALInfo(Recur.Value, ref confStartDate, ref confStartTime, ref confEndDate, ref confEndTime);
               
                if (recurPattern.Equals(""))
                    return "";
            }
            //BEGIN:VCALENDAR' + CHAR(13)  + CHAR(10) + 'VERSION:1.0' + CHAR(13)  + CHAR(10) + 'METHOD:PUBLISH' + CHAR(13)  + CHAR(10) + 'BEGIN:VEVENT' + CHAR(13)  + CHAR(10) + 'ORGANIZER:MAILTO:' + CHAR(13)  + CHAR(10) + 'DTSTART:20071019T164500Z' + CHAR(13)  + CHAR(10) + 'DTEND:20071019T170000Z' + CHAR(13)  + CHAR(10) + 'Location;ENCODING=QUOTED-PRINTABLE:' + CHAR(13)  + CHAR(10) + 'TRANSP:OPAQUE' + CHAR(13)  + CHAR(10) + 'SEQUENCE:0' + CHAR(13)  + CHAR(10) + 'UID:20071019T164500Z' + CHAR(13)  + CHAR(10) + 'DTSTAMP:20071012T144421Z' + CHAR(13)  + CHAR(10) + 'DESCRIPTION:' + CHAR(13)  + CHAR(10) + 'SUMMARY;ENCODING=QUOTED-PRINTABLE:test display layout' + CHAR(13)  + CHAR(10) + 'PRIORITY:3' + CHAR(13)  + CHAR(10) + 'CLASS:PUBLIC' + CHAR(13)  + CHAR(10) + 'BEGIN:VALARM' + CHAR(13)  + CHAR(10) + 'TRIGGER:PT15M' + CHAR(13)  + CHAR(10) + 'ACTION:DISPLAY' + CHAR(13)  + CHAR(10) + 'DESCRIPTION:Reminder' + CHAR(13)  + CHAR(10) + 'END:VALARM' + CHAR(13)  + CHAR(10) + 'END:VEVENT' + CHAR(13)  + CHAR(10) + 'END:VCALENDAR
            //Modified By Amit Yaduwanshi on 11-08-2008 Issue #435
            String strICAL = "BEGIN:VCALENDAR";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "VERSION:2.0";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "METHOD:PUBLISH";//' + CHAR(13) + CHAR(10) + '";
            //Response.Write(confStartDate.Text + " : " + confStartTime.Text);
            if (Recur.Value != "")
            {
                strICAL += "\r\n";
                strICAL += "BEGIN:VTIMEZONE";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "TZID:Eastern Time (US & Canada)";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "BEGIN:STANDARD";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "DTSTART:16011104T020000";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "TZOFFSETFROM:-0400";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "TZOFFSETTO:-0500";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "END:STANDARD";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "BEGIN:DAYLIGHT";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "DTSTART:16010311T020000";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "TZOFFSETFROM:-0500";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "TZOFFSETTO:-0400";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "END:DAYLIGHT";//' + CHAR(13) + CHAR(10) + '";
                strICAL += "\r\n";
                strICAL += "END:VTIMEZONE";//' + CHAR(13) + CHAR(10) + '";

            }
            strICAL += "\r\n";
            strICAL += "BEGIN:VEVENT";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "ORGANIZER:MAILTO:" + Session["userEmail"].ToString() + "";//' + CHAR(13) + CHAR(10) + '";

            /* *** code added/commented for buffer zone *** -- Start */
            
            //Code changed by offshore for FB ISsue 1073 -- start
            //  String inXML = "<ConvertToGMT><DateTime>" + myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + confStartTime.Text + "</DateTime><TimeZone>" + lstConferenceTZ.SelectedValue + "</TimeZone></ConvertToGMT>";
            //Code changed by offshore for FB ISsue 1073 -- end

            DateTime setupDTime = DateTime.MinValue;
            DateTime teardownDTime = DateTime.MinValue;
            DateTime confSDate = DateTime.MinValue;
            DateTime confEDate = DateTime.MinValue;
            double setupMin = 0;
            double tearDownMin = 0;
            String setupDur = "0";
            String tearDur = "0";

            if (!Recur.Value.Trim().Equals(""))
            {

                if (confStartDate.Text != "" && confStartTime.Text != "")
                {
                    confSDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)));
                }

                if (confEndDate.Text != "" && confEndTime.Text != "")
                {
                    confEDate = Convert.ToDateTime(confEndDate.Text + " " +  myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                }

                setupDur = hdnSetupTime.Value;
                tearDur = hdnTeardownTime.Value;

                Double.TryParse(setupDur, out setupMin);
                Double.TryParse(tearDur, out tearDownMin);

                if (confSDate != DateTime.MinValue)
                    setupDTime = confSDate.AddMinutes(setupMin);

                if (confEDate != DateTime.MinValue)
                    teardownDTime = confEDate.AddMinutes(-tearDownMin);

            }
            else
            {
                //FB 2634
                confSDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)));
                if (SetupDuration.Text != "")
                    setupDTime = confSDate.AddMinutes(-Int32.Parse(SetupDuration.Text));

                confEDate = Convert.ToDateTime(confEndDate.Text + " " +  myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                if (TearDownDuration.Text != "")
                    teardownDTime = confEDate.AddMinutes(Int32.Parse(TearDownDuration.Text));

                //if (SetupDate.Text != "" && SetupTime.Text != "")
                //{
                //    setupDTime = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text + " " + SetupTime.Text));
                //}

                //if (TearDownDate.Text != "" && TeardownTime.Text != "")
                //{
                //    teardownDTime = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text + " " + TeardownTime.Text));
                //}
            }


            String inXML = "<ConvertToGMT>" + obj.OrgXMLElement() + "<DateTime>" + setupDTime + "</DateTime><TimeZone>" + lstConferenceTZ.SelectedValue + "</TimeZone></ConvertToGMT>";//Organization Module Fixes
            /* *** code added/commented for buffer zone *** -- End */

            String outXML = obj.CallMyVRMServer("ConvertToGMT", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            String sDateTime = xmldoc.SelectSingleNode("//ConvertToGMT/DateTime").InnerText;
            strICAL += "\r\n";
            strICAL += "DTSTART;TZID=\"Eastern Time (US & Canada):\"" + DateTime.Parse(sDateTime).ToString("yyyyMMdd") + "T" + DateTime.Parse(sDateTime).ToString("HH:mm") + "00Z";//' + CHAR(13) + CHAR(10) + '";
            //Code changed by offshore for FB ISsue 1073 -- start
            inXML = "<ConvertToGMT>" + obj.OrgXMLElement() + "<DateTime>" + teardownDTime + "</DateTime><TimeZone>" + lstConferenceTZ.SelectedValue + "</TimeZone></ConvertToGMT>";  //buffer zone//Organization Module Fixes
            //Code changed by offshore for FB ISsue 1073 -- end
         
            outXML = obj.CallMyVRMServer("ConvertToGMT", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            xmldoc.LoadXml(outXML);
            String eDateTime = xmldoc.SelectSingleNode("//ConvertToGMT/DateTime").InnerText;
            strICAL += "\r\n";
            strICAL += "DTEND;TZID=\"Eastern Time (US & Canada):\"" + DateTime.Parse(eDateTime).ToString("yyyyMMdd") + "T" + DateTime.Parse(eDateTime).ToString("HH:mm") + "00Z";//' + CHAR(13) + CHAR(10) + '";
            String strLoc = "";
            foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
            {
                if (tn.Depth.Equals(3))
                    strLoc += tn.Text + ", ";
            }
            if (strLoc.Length > 0)
                strLoc = strLoc.Substring(0, strLoc.Length - 2);
            strICAL += "\r\n";
            strICAL += "Location;ENCODING=QUOTED-PRINTABLE:" + strLoc + "";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "TRANSP:OPAQUE";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "SEQUENCE:0";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            //Code changed by offshore for FB Issue 1073 -- start
            //strICAL += "UID:" + DateTime.Parse(confStartDate.Text).ToString("yyyyMMdd") + "T" + DateTime.Parse(confStartTime.Text).ToString("HH:mm") + "00Z";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "UID:" + DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text)).ToString("yyyyMMdd") + "T" + DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)).ToString("HH:mm") + "00Z";//' + CHAR(13) + CHAR(10) + '";
            //Code changed by offshore for FB Issue 1073 -- end
            strICAL += "\r\n";
            strICAL += "DTSTAMP:" + DateTime.Now.ToString("yyyyMMdd") + "T" + DateTime.Now.ToString("HH:mm") + "00Z";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "DESCRIPTION:" + ConferenceDescription.Text + "";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "SUMMARY;ENCODING=QUOTED-PRINTABLE:" + ConferenceName.Text + "";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "PRIORITY:3";//' + CHAR(13) + CHAR(10) + '";
            if (recurPattern != "")
            {
                strICAL += "\r\n";
                strICAL += "RRULE:" + recurPattern + "";//' + CHAR(13) + CHAR(10) + '";
            }
            strICAL += "\r\n";
            strICAL += "CLASS:PUBLIC";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "BEGIN:VALARM";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "TRIGGER:PT15M";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "ACTION:DISPLAY";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "DESCRIPTION:Reminder";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "END:VALARM";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "END:VEVENT";//' + CHAR(13) + CHAR(10) + '";
            strICAL += "\r\n";
            strICAL += "END:VCALENDAR";
            strICAL = strICAL.Replace("& ", "&amp; ");
            //Response.Write(strICAL.Replace("' + CHAR(13) + CHAR(10) + '", "<br>"));
            //Response.End();
            return strICAL;
        }

        #endregion

        #region GetICALInfo

        public string GetICALInfo(string instr, ref TextBox startDate, ref MetaBuilders.WebControls.ComboBox startTime, ref TextBox endDate, ref MetaBuilders.WebControls.ComboBox endTime) //, ref DateTime startDate, ref DateTime endDate)
        {
            string recurpattern = "";
            string[] recurArray = instr.Split('#');
            string[] recurArray0 = recurArray[0].Split('&');
            string[] recurArray1 = recurArray[1].Split('&');
            string[] recurArray2 = recurArray[2].Split('&');
            startDate.Text = recurArray2[0];
            startTime.Text = recurArray0[1] + ":" + recurArray0[2] + " " + recurArray0[3];
            //Code Changed FB Issue 1073 - Start
            //endDate.Text = DateTime.Parse(startDate.Text + " " + startTime.Text).AddMinutes(Int32.Parse(recurArray0[4])).ToString("MM/dd/yyyy");
            //endTime.Text = DateTime.Parse(startDate.Text + " " + startTime.Text).AddMinutes(Int32.Parse(recurArray0[4])).ToString("HH:mm tt");
            endDate.Text = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(startDate.Text) + " " + startTime.Text).AddMinutes(Int32.Parse(recurArray0[4])).ToString("MM/dd/yyyy");
            endTime.Text = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(startDate.Text) + " " + startTime.Text).AddMinutes(Int32.Parse(recurArray0[4])).ToString(tformat);
            //Code Changed FB Issue 1073 - End
            if (recurArray1[0].Equals("5"))
                recurpattern = "";
            else
            {
                switch (recurArray1[0])
                {
                    case "1":
                        recurpattern += "FREQ=DAILY;";
                        recurpattern += "INTERVAL=" + recurArray1[2] + ";";
                        break;
                    case "2":
                        recurpattern += "FREQ=DAILY;";
                        recurpattern += "INTERVAL=" + recurArray1[3] + ";";
                        break;
                    case "3":
                        recurpattern += "FREQ=MONTHLY;";
                        switch (recurArray1[5])
                        {
                            case "1":
                                recurpattern += "INTERVAL=" + recurArray1[7] + ";" + "BYMONTHDAY=" + recurArray1[6] + ";";
                                break;
                            case "2":
                                recurpattern += "INTERVAL=" + recurArray1[10] + ";" + "BYDAY=" + recurArray1[9] + ";";
                                break;
                            //                        default:
                            //                            HttpContext.Current.Response.Write("An error had occured in Monthly recurring pattern");
                        }
                        break;
                    case "4":
                        recurpattern += "FREQ=YEARLY;";
                        switch (recurArray1[11])
                        {
                            case "1":
                                recurpattern += "INTERVAL=1;BYMONTHDAY=" + recurArray1[13] + ";BYMONTH=" + recurArray1[12] + ";";
                                break;
                            case "2":
                                recurpattern += "INTERVAL=1;BYDAY=" + getMonthWeekDay(recurArray1[15]) + ";BYMONTH=" + recurArray1[16] + ";BYSETPOS=" + getMonthWeekSet(recurArray1[14]) + ";";
                                break;
                        }
                        break;
                }
                switch (recurArray2[1])
                {
                    case "1":
                    case "2":
                        recurpattern += "COUNT=" + recurArray2[2] + ";";
                        break;
                    case "3":
                        recurpattern += "UNTIL=" + Convert.ToDateTime(recurArray2[3]).ToString("yyyyMMdd") + "T000000Z" + ";";
                        break;
                }
            }
            return recurpattern;
        }

        #endregion

        #region getWeekDay

        public String getWeekDay(String weekdayno)
        {
            switch (weekdayno)
            {
                case "1":
                    return "SU";
                case "2":
                    return "MO";
                case "3":
                    return "TU";
                case "4":
                    return "WE";
                case "5":
                    return "TH";
                case "6":
                    return "FR";
                case "7":
                    return "SA";
            }
            return "";
        }

        #endregion

        #region getMonthWeekSet

        public String getMonthWeekSet(String monthweeksetno)
        {
            switch (monthweeksetno)
            {
                case "1":
                    return "1";
                case "2":
                    return "2";
                case "3":
                    return "3";
                case "4":
                    return "4";
                case "5":
                    return "-1";
            }
            return "";
        }

        #endregion

        #region getMonthWeekDay

        public String getMonthWeekDay(String monthweekdayno)
        {
            switch (monthweekdayno)
            {
                case "1":
                    return "SU,MO,TU,WE,TH,FR,SA";
                case "2":
                    return "MO,TU,WE,TH,FR";
                case "3":
                    return "SU,SA";
                default:
                    int t = Int32.Parse(monthweekdayno) - 3;
                    return getWeekDay(t.ToString());
            }
            //            return "";
        }

        #endregion

        #region RemoveFile

        protected void RemoveFile(Object sender, CommandEventArgs e)
        {
            try
            {
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                Button btnTemp = new Button();
                switch (e.CommandArgument.ToString())
                {
                    case "1":
                        lblTemp = hdnUpload1;
                        inTemp = FileUpload1;
                        lblFname = lblUpload1;
                        lblHdnName = hdnUpload1;
                        btnTemp = btnRemove1;
                        break;
                    case "2":
                        lblTemp = hdnUpload2;
                        inTemp = FileUpload2;
                        lblFname = lblUpload2;
                        lblHdnName = hdnUpload2;
                        btnTemp = btnRemove2;
                        break;
                    case "3":
                        lblTemp = hdnUpload3;
                        inTemp = FileUpload3;
                        lblFname = lblUpload3;
                        lblHdnName = hdnUpload3;
                        btnTemp = btnRemove3;
                        break;
                }
                //Response.Write("hdnUpload" + e.CommandArgument.ToString());
                if (lblTemp.Text != "")
                {
                    //Response.Write(e.CommandArgument + " : " + lblTemp.Text);
                    FileInfo fi = new FileInfo(lblTemp.Text);
                    if (fi.Exists)
                        fi.Delete();
                    inTemp.Visible = true;
                    inTemp.Disabled = false;
                    lblFname.Visible = false;
                    lblFname.Text = "";
                    lblHdnName.Visible = false;
                    lblHdnName.Text = "";
                    btnTemp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region UploadFiles

        protected void UploadFiles(Object sender, EventArgs e)
        {
            String finalPath = "";
            
            try
            {
                //Response.Write((FileUpload1.Value.LastIndexOf("\\") + 1) + " : " + (FileUpload1.Value.Length - 1));
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData;
                String errMsg = "";
                //FB 1830
                String commonPath = "";                 
                if (language == "en")
                    commonPath = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload";
                else
                   // commonPath = HttpContext.Current.Request.MapPath("..").ToString() + "\\image";
                    commonPath = HttpContext.Current.Request.MapPath("..").ToString() + "\\en\\upload";//FB 2154

                //ZD 100263
                string filextn = "";
                string[] strExtension = null;
                bool filecontains = false;
                string uploadFileExtn = "";
                string[] strSplitExtension = null;//ZD 101052
                int EnableFileWhiteList = 0;
                if (Session["EnableFileWhiteList"] != null && !string.IsNullOrEmpty(Session["EnableFileWhiteList"].ToString()))
                {
                    int.TryParse(Session["EnableFileWhiteList"].ToString(), out EnableFileWhiteList);
                }

                if (Session["FileWhiteList"] != null && !string.IsNullOrEmpty(Session["FileWhiteList"].ToString()))
                {
                    uploadFileExtn = Session["FileWhiteList"].ToString();
                }
                if (!FileUpload1.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload1.Value);
                    //Response.Write(HttpContext.Current.Request.MapPath(".").ToString() + "\\" + fName);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');
                        
                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                        
                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload1.PostedFile;
                    nFileLen = myFile.ContentLength;
                    //Response.Write(nFileLen.ToString());
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 10000000)
                    {
                        //FileUpload1 = fName;  
                        //FB 1830
                        //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                        finalPath = obj.AvailableFilename(commonPath, fName);//FB 2153
                        WriteToFile(finalPath,ref myData);//(commonPath + "\\" + fName, ref myData); FB 2153
                        
                        FileUpload1.Disabled = true;
                        lblUpload1.Text = fName;

                        //FB 1830
                         //hdnUpload1.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                        hdnUpload1.Text = finalPath;//commonPath + "\\" + fName; FB 2153
                        
                        FileUpload1.Visible = false;
                        lblUpload1.Visible = true;
                        btnRemove1.Visible = true;
                    }
                    else
                        errMsg += obj.GetTranslatedText("Attachment 1 is greater than 10MB. File has not been uploaded.");//FB 2272
                }
                if (!FileUpload2.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload2.Value);

                    //ZD 100263 Starts
                    //ZD 101052 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
                        strExtension = uploadFileExtn.Split(';');

                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }

                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload2.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (FileUpload2.Value.Equals(FileUpload1.Value))
                        errMsg += obj.GetTranslatedText("Attachment 2 has already been uploaded.");//FB 2272
                    else
                    {
                        if (nFileLen <= 10000000)
                        {
                            //FileUpload1 = fName;
                            //FB 1830
                            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            finalPath = obj.AvailableFilename(commonPath, fName);//FB 2153
                            //WriteToFile(commonPath + "\\" + fName, ref myData); FB 2153
                            WriteToFile(finalPath, ref myData);//(commonPath + "\\" + fName, ref myData); FB 2153
                            FileUpload2.Disabled = true;
                            lblUpload2.Text = fName;
                            //hdnUpload2.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                            hdnUpload2.Text = finalPath; //commonPath + "\\" + fName; FB 2153
                            FileUpload2.Visible = false;
                            lblUpload2.Visible = true;
                            btnRemove2.Visible = true;
                        }
                        else
                            errMsg += obj.GetTranslatedText("Attachment 3 is greater than 10MB. File has not been uploaded.");//FB 2272
                    }
                }
                if (!FileUpload3.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload3.Value);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');

                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }

                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload3.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (FileUpload3.Value.Equals(FileUpload1.Value) || FileUpload3.Value.Equals(FileUpload2.Value))
                        errMsg += obj.GetTranslatedText("Attachment 3 has already been uploaded.");//FB 2272
                    else
                    {
                        if (nFileLen <= 10000000)
                        {
                            //FileUpload1 = fName;
                            //FB 1830
                            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            finalPath = obj.AvailableFilename(commonPath, fName);//FB 2153
                            //WriteToFile(commonPath + "\\" + fName, ref myData); FB 2153
                            WriteToFile(finalPath, ref myData);//(commonPath + "\\" + fName, ref myData); FB 2153
                            FileUpload3.Disabled = true;
                            //FB 1830
                            //hdnUpload3.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                            hdnUpload3.Text = finalPath; //commonPath + "\\" + fName; FB 2153
                            lblUpload3.Text = fName;
                            lblUpload3.Visible = true;
                            FileUpload3.Visible = false;
                            btnRemove3.Visible = true;
                        }
                        else
                            errMsg += obj.GetTranslatedText("Attachment 3 is greater than 10MB. File has not been uploaded.");//FB 2272
                    }
                }
                errLabel.Text = errMsg;
                errLabel.Visible = true;
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region WriteToFile

        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region SetConference

        protected bool SetConference()
        {
            //            myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();

            String confVMR = "0";//FB 2376
            int v = 0; //FB 2670

            try
            {
                int maxDuration = 24;
                if (Application["MaxConferenceDurationInHours"] != null)
                    if (!Application["MaxConferenceDurationInHours"].ToString().Trim().Equals(""))
                        maxDuration = Int32.Parse(Application["MaxConferenceDurationInHours"].ToString().Trim());

                if (!ValidateEndpoints() && !lstConferenceType.SelectedValue.Equals("7") && !(lstVMR.SelectedIndex >0) && !lstConferenceType.SelectedValue.Equals("8") && !(chkPCConf.Checked))//FB 2448 //FB 2694 //FB 2620 //FB 2819
                    return false;

                for(int i =0; i < dgRooms.Items.Count; i++) //FB 2400
                {
                    if (((Label)dgRooms.Items[i].FindControl("lblIsTelepresence")).Text == "1" && lstConferenceType.SelectedValue == "4")
                    {
                        errLabel.Text = obj.GetTranslatedText("Point to Point Conference should not have Telepresence endpoint.. Telepresence Room Name:")
                                      + ((Label)dgRooms.Items[i].FindControl("lblRoomName")).Text;
                        errLabel.Visible = true;
                        return false;
                    }
                }

                 //buffer zone Start
                if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
                {
                    if (Recur.Value.ToString().Equals("") && chkStartNow.Checked == false)
                    {
                        if (!ValidateConferenceDur())
                            return false;
                    }
                }
                //buffer zone end

                String roomNames = "";
                if (!CheckWorkorderRooms(ref roomNames))
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("Workorders can only be created with rooms selected for this conference. ");//FB 1830 - Translation
                    errLabel.Text += "<br>" + obj.GetTranslatedText("Modify work order to conform to your room changes. Work orders and Room names are as follows:") + roomNames;//FB 1830 - Translation
                    return false;
                }

                string inxml = "";
                inxml += "<conference>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                //FB 2544 - Requestor starts ... modified during FB 2540
                inxml += "<requestorID>" + hdnApprover7.Text + "</requestorID>";
                inxml += "  <userID>" + Session["userid"].ToString() + "</userID>";
                //FB 2544 - Requestor starts ... modified during FB 2540
                inxml += "  <confInfo>";
                //if (chkStartNow.Checked && lblConfID.Text.IndexOf(",")>=0)
                //    inxml += "  <confID>" + lblConfID.Text.Split(',')[0] + "</confID>";
                //else
                if (Request.QueryString["t"].ToString().Equals("o") && flagClone.Equals(false)) //FB Case 1029 Revathi
                {
                    inxml += "      <confID>new</confID>";
                    flagClone = true;
                }
                else
                    inxml += "      <confID>" + lblConfID.Text + "</confID>";
                inxml += "      <confName>" + ConferenceName.Text + "</confName>";
                // Code added for the Bug # 74- mpujari
                inxml += "      <confHost>" + hdnApprover4.Text + "</confHost>";
                inxml += "      <confOrigin>0</confOrigin>";
                inxml += "      <timeCheck>" + txtTimeCheck.Text + "</timeCheck>";
                if (lstConferenceType.SelectedValue.Equals("7"))//FB 1865
                    confPassword.Value = "";
                inxml += "      <confPassword>" + confPassword.Value + "</confPassword>";

                /*if (lstVMR.SelectedIndex > 0) //FB 2376 // FB 2620                                    
				{
					confVMR = "1"; //FB 2620
                    lstStartMode.SelectedValue = "0"; // 0 - Automatic; 1 - Manual //FB 2501
                }*/

                if (lstConferenceType.SelectedValue.Equals("4") || lstConferenceType.SelectedValue.Equals("7") || lstConferenceType.SelectedValue.Equals("8") || lstVMR.SelectedIndex > 0 || chkPCConf.Checked )//FB 2694 //FB 2620 //FB 2819
                {
                    lstStartMode.SelectedValue = "0"; // 0 - Automatic; 1 - Manual //FB 2501
                }

                inxml += "<isVMR>" + lstVMR.SelectedIndex + "</isVMR>"; //FB 2376 //FB 2620

                inxml += "      <IcalID>" + hdnicalID.Value + "</IcalID>"; //2457 exchange round trip

				//FB 2717 Start
                if (chkCloudConferencing.Checked)
                    inxml += "<CloudConferencing>1</CloudConferencing>";
                else
                    inxml += "<CloudConferencing>0</CloudConferencing>";

                if (lstConferenceType.SelectedValue.Equals("4") || lstConferenceType.SelectedValue.Equals("7") || lstVMR.SelectedIndex > 0 || chkCloudConferencing.Checked || chkPCConf.Checked)  // FB 2819
                    inxml += "<StartMode>0</StartMode>"; // FB 2717
                else
	                inxml += "<StartMode>" + lstStartMode.SelectedValue + "</StartMode>"; // FB 2501
                //FB 2717 End
                // FB 2693 Starts
                if (chkPCConf.Checked)
                {
                    inxml += "<isPCconference>1</isPCconference>";

                    string value = "1";
                    if (rdBJ.Checked)
                        value = "1";
                    else if (rdJB.Checked)
                        value = "2";
                    else if (rdLync.Checked)
                        value = "3";
                    else if (rdVidtel.Checked)
                        value = "4";

                    inxml += "<pcVendorId>" + value + "</pcVendorId>";

                }
                else
                {
                    inxml += "<isPCconference>0</isPCconference>";
                    inxml += "<pcVendorId>0</pcVendorId>";
                }
                // FB 2693 Ends

                //FB 2609 Start //FB 2632
                if (hdnCrossMeetGreetBufferTime.Value != null) 
                {
                    if (!hdnCrossMeetGreetBufferTime.Value.Equals(""))
                    {
                        inxml += "<MeetandGreetBuffer>" + hdnCrossMeetGreetBufferTime.Value + "</MeetandGreetBuffer>";
                    }
                }
                //FB 2609 End
                //FB 2398 start
                obj.OrgSetupTime = OrgSetupTime;
                obj.OrgTearDownTime = OrgTearDownTime;
                obj.isBufferChecked = chkEnableBuffer.Checked;
                Int32.TryParse(enableBufferZone, out obj.EnableBufferZone);
                //FB 2398 end
				//FB 2595 Start //FB 2993 Starts
               if(hdnNetworkSwitching.Value == "2") 
                    inxml += "<Secured>" + drpNtwkClsfxtn.SelectedValue + "</Secured>";
               else 
                    inxml += "<Secured>0</Secured>";
                //FB 2595 Ends //FB 2993 Ends

                //FB 2870 Start
                if (ChkEnableNumericID.Checked)
                {
                    inxml += "<EnableNumericID>1</EnableNumericID>";
                    inxml += "<CTNumericID>" + txtNumeridID.Text + "</CTNumericID>";
                }
                else
                {
                    inxml += "<EnableNumericID>0</EnableNumericID>";
                    inxml += "<CTNumericID></CTNumericID>";
                }
                
                //FB 2870 End
                //FB 2998
                if (txtMCUConnect.Text.Trim() != "" && (lstConferenceType.SelectedValue == "2" || lstConferenceType.SelectedValue == "6")  && !chkStartNow.Checked 
                    && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                    inxml += "<McuSetupTime>" + txtMCUConnect.Text + "</McuSetupTime>";
                else
                    inxml += "<McuSetupTime>0</McuSetupTime>";

                if (txtMCUDisConnect.Text.Trim() != "" && (lstConferenceType.SelectedValue == "2" || lstConferenceType.SelectedValue == "6") && !chkStartNow.Checked && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                    inxml += "<MCUTeardonwnTime>" + txtMCUDisConnect.Text + "</MCUTeardonwnTime>";
                else
                    inxml += "<MCUTeardonwnTime>0</MCUTeardonwnTime>";

                //FB 2634
                Int32 setupDuration = Int32.MinValue;
                Int32 tearDuration = Int32.MinValue;

                //Response.Write(Recur.Value);
                //FB 1911
                if (Recur.Value.ToString().Equals("") && RecurSpec.Value.ToString().Equals(""))
                {
                    int durationMin;
                    if (chkStartNow.Checked)
                    {
                        inxml += "      <immediate>1</immediate>";
                        inxml += "      <recurring>0</recurring>";
                        inxml += "      <recurringText></recurringText>";
                        inxml += "		<startDate></startDate>";
                        inxml += "		<startHour></startHour>";
                        inxml += "		<startMin></startMin>";
                        inxml += "		<startSet></startSet>";
                        inxml += "		<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>";
                        //Response.Write(lstDuration.Text);
                        durationMin = Convert.ToInt32(lstDuration.Text.Split(':')[0]) * 60 + Convert.ToInt32(lstDuration.Text.Split(':')[1]);
                         
                        //code added for buffer zone -- Start

                         inxml += "		<setupDuration>0</setupDuration>";
                         inxml += "		<teardownDuration>0</teardownDuration>";
                         inxml += "		<setupDateTime></setupDateTime>";
                         inxml += "		<teardownDateTime></teardownDateTime>";
                        //code added for buffer zone -- End
                    }
                    else
                    {
                        inxml += "      <immediate>0</immediate>";
                        int sHour = 0;

                        if (Session["timeFormat"] != null)
                        {
                            if (Session["timeFormat"].ToString().Equals("1"))
                            {
                                sHour = Convert.ToInt16(myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text).Split(' ')[0].Split(':')[0]);

                                if ((myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text).Split(' ')[1] == "PM") && (sHour != 12))
                                    sHour += 12;
                                if ((myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text).Split(' ')[1] == "AM") && (sHour == 12))
                                    sHour -= 12;
                            }
                        }
                        //Code changed by offshore for Issue 1073 -- start
                        //DateTime dStart = DateTime.Parse(confStartDate.Text + " " + confStartTime.Text);//new DateTime(Convert.ToInt16(confStartDate.Text.Split('/')[2]), Convert.ToInt16(confStartDate.Text.Split('/')[0]), Convert.ToInt16(confStartDate.Text.Split('/')[1]), sHour, Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                        DateTime dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));//new DateTime(Convert.ToInt16(confStartDate.Text.Split('/')[2]), Convert.ToInt16(confStartDate.Text.Split('/')[0]), Convert.ToInt16(confStartDate.Text.Split('/')[1]), sHour, Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                        //Code changed by offshore for Issue 1073 -- End

                        if (Session["timeFormat"] != null)
                        {
                            if (Session["timeFormat"].ToString().Equals("1"))
                            {
                                sHour = Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]);
                                if ((confEndTime.Text.Split(' ')[1] == "PM") && (sHour != 12))
                                    sHour += 12;
                                if ((confEndTime.Text.Split(' ')[1] == "AM") && (sHour == 12))
                                    sHour -= 12;
                            }
                        }
                        
                        //Code changed by offshore for Issue 1073 -- start
                        //DateTime dEnd = DateTime.Parse(confEndDate.Text + " " + confEndTime.Text); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), sHour, Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                        DateTime dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " +  myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text)); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), sHour, Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                        //Code changed by offshore for Issue 1073 -- end                        
                        TimeSpan ts = dEnd.Subtract(dStart);
                        inxml += "      <recurring>0</recurring>";
                        inxml += "      <recurringText></recurringText>";
                        //Code changed by offshore for Issue 1073 -- start
                        //inxml += "		<startDate>" + confStartDate.Text + "</startDate>";
                        //Code changed by offshore for Issue 1073 -- end


                        //inxml += "		<startHour>" + confStartTime.Text.Split(' ')[0].Split(':')[0] + "</startHour>";
                        //inxml += "		<startMin>" + confStartTime.Text.Split(' ')[0].Split(':')[1] + "</startMin>";
                        //inxml += "		<startSet>" + confStartTime.Text.Split(' ')[1] + "</startSet>";

                        durationMin = Int32.Parse(ts.TotalMinutes.ToString()); //ts.Days * 24 + ts.Hours * 60 + ts.Minutes;

                        //FB 2398 start
                        DateTime buffStart = dStart;
                        //if (!chkEnableBuffer.Checked && enableBufferZone == "1")
                        //{
                        //    buffStart = buffStart.AddMinutes(-OrgSetupTime);
                        //    durationMin += OrgTearDownTime + OrgSetupTime;
                        //}

                            if (enableBufferZone == "1")
                            {
                                Int32.TryParse(SetupDuration.Text, out setupDuration);
                                Int32.TryParse(TearDownDuration.Text, out tearDuration);
                            }
                            else
                            {
                                setupDuration = 0;
                                tearDuration = 0;
                            }

                        buffStart = buffStart.AddMinutes(-setupDuration);
                        durationMin += tearDuration + setupDuration;

                        inxml += "		<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(buffStart.ToString(format)) + "</startDate>";
                        inxml += "		<startHour>" + buffStart.ToString("hh") + "</startHour>";
                        inxml += "		<startMin>" + buffStart.ToString("mm") + "</startMin>";
                        inxml += "		<startSet>" + buffStart.ToString("tt") + "</startSet>";
                        //FB 2398 end

                        inxml += "		<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>";
						//FB 2634
                        /* *** code added for buffer zone *** -- Start */
                        DateTime sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                        DateTime tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " +  myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                        sDateTime = sDateTime.AddSeconds(45);
                        tDateTime = tDateTime.AddSeconds(45);

                        if (enableBufferZone == "0") //FB 2398 - ( || chkEnableBuffer.Checked == false) //Changed for bug bufferzone
                        {
                            sDateTime = dStart;
                            tDateTime = dEnd;

                        }

                        inxml += "		<setupDuration></setupDuration>";
                        inxml += "		<teardownDuration></teardownDuration>";
                        inxml += "		<setupDateTime>" + sDateTime + "</setupDateTime>";
                        inxml += "		<teardownDateTime>" + tDateTime + "</teardownDateTime>";
                        /* *** code added for buffer zone *** -- End */
                    }
                    inxml += "      <createBy>" + lstConferenceType.SelectedValue.ToString() + "</createBy>";
                    //Response.Write(durationMin);
                    
                    if (durationMin >= 15 && durationMin <= (maxDuration * 60))
                        inxml += "		<durationMin>" + durationMin.ToString() + "</durationMin>";
                    else
                    {
                        //FB 2634
                        if(durationMin < 15)
                            errLabel.Text = ns_MyVRMNet.ErrorList.InvalidDuration;

                        if(durationMin > (maxDuration * 60))
                            errLabel.Text = ns_MyVRMNet.ErrorList.ExceedDuration;
                        
                        errLabel.Visible = true;
                        Wizard1.ActiveViewIndex = 0;
                        TopMenu.Items[0].Selected = true;//FB 2889
                        return false;
                    }
                }
                else
                {
                    inxml += "      <immediate>0</immediate>";
                    inxml += "      <createBy>" + lstConferenceType.SelectedValue.ToString() + "</createBy>";
                    /* *** code added for buffer zone *** -- Start */

                    int sDur = 0;
                    int tDur = 0;

                    if (hdnSetupTime.Value == "" || hdnSetupTime.Value == null)
                        hdnSetupTime.Value = "0";

                    if (hdnTeardownTime.Value == "" || hdnTeardownTime.Value == null)
                        hdnTeardownTime.Value = "0";

                    Int32.TryParse(hdnSetupTime.Value, out sDur);
                    Int32.TryParse(hdnTeardownTime.Value, out tDur);

                    if (enableBufferZone == "0")//Changed for bug bufferzone //FB 2398
                    {
                        sDur = 0;
                        tDur = 0;

                    }
                    else //if (!chkEnableBuffer.Checked) //FB 2634
                    {
                        Int32.TryParse(SetupDuration.Text, out sDur);
                        Int32.TryParse(TearDownDuration.Text, out tDur);
                        //sDur = OrgSetupTime;
                        //tDur = OrgTearDownTime;
                    }

                    inxml += "		<setupDuration>" + sDur + "</setupDuration>";
                    inxml += "		<teardownDuration>" + tDur + "</teardownDuration>";
                    inxml += "		<setupDateTime></setupDateTime>";
                    inxml += "		<teardownDateTime></teardownDateTime>";

                    string bufferxml = "		<setupDuration>" + sDur + "</setupDuration>";
                    bufferxml += "		<teardownDuration>" + tDur + "</teardownDuration>";

                    /* *** code added for buffer zone *** -- End */
                    
                    inxml += "      <recurring>1</recurring>";
                    inxml += "      <recurringText>" + RecurringText.Value + "</recurringText>";

                    //FB 1911
                    // Need to added a hidden which is to be set when special rec is set
                    //if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Mayo) && hdnSpecRec.Value == "1" && isEditMode == "0")
                    if (Session["isSpecialRecur"].ToString().Equals("1") && hdnSpecRec.Value == "1" && isEditMode == "0") //FB 2052
                    {
                        String recXML = "";
                        String strInXml = "";
                        strInXml = obj.AppendSpecialRecur(RecurSpec.Value, bufferxml, lstConferenceTZ.SelectedValue, ref recXML);
                        //FB 2027 - Start
                        if (strInXml.IndexOf("<error>") >= 0)
                        {
                            XmlDocument xDoc = new XmlDocument();
                            xDoc.LoadXml(strInXml);
                            errLabel.Text = xDoc.SelectSingleNode("error").InnerText;
                            errLabel.Visible = true;
                            return false;
                        }
                        else
                            inxml += strInXml; //FB 2052
                        //FB 2027 - End
                    }
                    else
                        /* *** Recurence Fixes - Edit Recurring conference with dirty instances - Start *** */
                        inxml += obj.AppendRecur(Recur.Value, bufferxml, lstConferenceTZ.SelectedValue); //buffer zone & timezone issue
                    /* *** Recurence Fixes - Edit Recurring conference with dirty instances - End *** */
                }

                inxml += "		<description>" + utilObj.ReplaceInXMLSpecialCharacters(ConferenceDescription.Text) + "</description>";
                inxml += "		<locationList>";
                inxml += "          <selected>";
                if (treeRoomSelection.Nodes.Count > 0) //FB 2998
                {
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                    {
                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                        {
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                if (tn.Checked.Equals(true))
                                    inxml += "<level1ID>" + tn.Value + "</level1ID>";
                            }
                        }
                    }
                }
                inxml += "          </selected>";
                inxml += "      </locationList>";
                //FB 2426 Start
                string Roomid = "";
                inxml += "		<ConfGuestRooms>";
                if (!lstConferenceType.SelectedValue.Equals("4") && !lstConferenceType.SelectedValue.Equals("7") && !lstConferenceType.SelectedValue.Equals("8")) //FB 2694
                {
                    foreach (DataGridItem item in dgOnflyGuestRoomlist.Items)
                    {
                        inxml += "<ConfGuestRoom>";
                        inxml += "<LoginUserID>" + Session["userID"].ToString() + "</LoginUserID>";
                        inxml += obj.OrgXMLElement();
                        inxml += "<GuestRoomUID>" + item.Cells[0].Text.ToString() + "</GuestRoomUID>";
                        Roomid = item.Cells[1].Text.ToString().Replace("&nbsp;", "");
                        if (Roomid == "")
                            Roomid = "new";
                        inxml += "<GuestRoomID>" + Roomid + "</GuestRoomID>";
                        inxml += "<GuestRoomName>" + item.Cells[2].Text.ToString() + "</GuestRoomName>";
                        inxml += "<ContactName>" + item.Cells[3].Text.ToString() + "</ContactName>";
                        inxml += "<ContactEmail>" + item.Cells[4].Text.ToString() + "</ContactEmail>";
                        inxml += "<ContactPhoneNo>" + item.Cells[5].Text.ToString() + "</ContactPhoneNo>";
                        inxml += "<RoomAddress>" + item.Cells[6].Text.ToString() + "</RoomAddress>";
                        inxml += "<State>" + item.Cells[7].Text.ToString() + "</State>";
                        inxml += "<City>" + item.Cells[8].Text.ToString() + "</City>";//To be done 6
                        inxml += "<ZipCode>" + item.Cells[9].Text.ToString() + "</ZipCode>";
                        inxml += "<Country>" + item.Cells[10].Text.ToString() + "</Country>";
                        inxml += "<Tier1>" + Session["OnflyTopTierID"].ToString() + "</Tier1>";
                        inxml += "<Tier2>" + Session["OnflyMiddleTierID"].ToString() + "</Tier2>";
                        inxml += "<Profiles>";
                        int inc = 0;
                        int j = 0;
                        for (int i = 0; i < 3; i++)
                        {
                            item.Cells[12 + inc].Text = item.Cells[12 + inc].Text.ToString().Replace("&nbsp;", "");
                            if (item.Cells[12 + inc].Text.ToString().Trim() != "")
                            {
                                j = j + 1;
                                inxml += "<Profile>";
                                inxml += "<EndpointName>" + item.Cells[2].Text.ToString() + "</EndpointName>";
                                inxml += "<ProfileName>" + item.Cells[2].Text.ToString() + "_" + j + "</ProfileName>";
                                inxml += "<AddressType>" + item.Cells[11 + inc].Text.ToString() + "</AddressType>";
                                inxml += "<Address>" + item.Cells[12 + inc].Text.ToString() + "</Address>";
                                inxml += "<Password>" + item.Cells[13 + inc].Text.ToString() + "</Password>";
                                inxml += "<confirmPassword>" + item.Cells[14 + inc].Text.ToString() + "</confirmPassword>";
                                inxml += "<MaxLineRate>" + item.Cells[15 + inc].Text.ToString() + "</MaxLineRate>";
                                inxml += "<ConnectionType>" + item.Cells[16 + inc].Text.ToString() + "</ConnectionType>";
                                inxml += "<isDefault>" + item.Cells[17 + inc].Text.ToString() + "</isDefault>";
                                inxml += "</Profile>";
                                inc += 7;
                            }
                            else
                            {
                                inc += 7;
                            }
                        }
                        inxml += "</Profiles>";
                        inxml += "</ConfGuestRoom>";
                    }
                }
                inxml += "</ConfGuestRooms>";
                //FB 2426 End
                if (chkPublic.Checked)
                {
                    inxml += "      <publicConf>1</publicConf>";
                    if (chkOpenForRegistration.Checked)
                        inxml += "      <dynamicInvite>1</dynamicInvite>";
                    else
                        inxml += "      <dynamicInvite>0</dynamicInvite>";
                }
                else
                {
                    //Code Modified FOr MOJ - Phase2 - Start
                    if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                        inxml += "      <publicConf>1</publicConf>";
                    else
                        inxml += "      <publicConf>0</publicConf>";
                    //Code Modified FOr MOJ - Phase2 - End
                    inxml += "      <dynamicInvite>0</dynamicInvite>";
                }
                inxml += "		<ServiceType>" + DrpServiceType.SelectedValue + "</ServiceType>";//FB 2219

                //FB 2632 - Starts
                inxml += "<ConciergeSupport>";

                if (chkOnSiteAVSupport.Checked)
                    inxml += "<OnSiteAVSupport>1</OnSiteAVSupport>";
                else
                    inxml += "<OnSiteAVSupport>0</OnSiteAVSupport>";

                if (chkMeetandGreet.Checked)
                    inxml += "<MeetandGreet>1</MeetandGreet>";
                else
                    inxml += "<MeetandGreet>0</MeetandGreet>";

                if (chkConciergeMonitoring.Checked)
                    inxml += "<ConciergeMonitoring>1</ConciergeMonitoring>";
                else
                    inxml += "<ConciergeMonitoring>0</ConciergeMonitoring>";

                //FB 2670 Starts
                if (hdnVNOCOperator.Text.Trim() != "" && txtVNOCOperator.Text.Trim() != "")
                {
                    string[] VNOCIDs = hdnVNOCOperator.Text.Split(',');
                    string[] VNOCName = txtVNOCOperator.Text.Split('\n');
                    inxml += "<DedicatedVNOCOperator>1</DedicatedVNOCOperator>";
                    inxml += "<VNOCAssignAdminID>" + Session["userID"].ToString() + "</VNOCAssignAdminID>"; 
                    inxml += "<ConfVNOCOperators>";
                    for (v = 0; v < VNOCIDs.Length; v++)
                        if (VNOCIDs[v] != "")
                            inxml += "<VNOCOperatorID>" + VNOCIDs[v] + "</VNOCOperatorID>";
                    inxml += "</ConfVNOCOperators>";
                } 
                else
                {
                    if (chkDedicatedVNOCOperator.Checked)
                    {
                        errLabel.Text = obj.GetTranslatedText("Please select Dedicated VNOC Operator.");
                        errLabel.Visible = true;
                        return false;
                    }
                    inxml += "<DedicatedVNOCOperator>0</DedicatedVNOCOperator>";
                    inxml += "<VNOCAssignAdminID>0</VNOCAssignAdminID>";
                    inxml += "<ConfVNOCOperators></ConfVNOCOperators>";
                }
                inxml += "</ConciergeSupport>";
                //FB 2632 - End //FB 2670 Ends
                
                inxml += "      <advAVParam>";
                inxml += "          <maxAudioPart>" + obj.ControlConformityCheck(txtMaxAudioPorts.Text) + "</maxAudioPart>"; // ZD 100263
                inxml += "          <maxVideoPart>" + obj.ControlConformityCheck(txtMaxVideoPorts.Text) + "</maxVideoPart>"; // ZD 100263
                inxml += "          <restrictProtocol>" + lstRestrictNWAccess.SelectedValue + "</restrictProtocol>";
                inxml += "          <restrictAV>" + lstRestrictUsage.SelectedValue + "</restrictAV>";
                inxml += "          <videoLayout>" + txtSelectedImage.Text + "</videoLayout>";
                if (lstConferenceType.SelectedValue.Equals("4"))//Code added for disney 
                    inxml += "          <maxLineRateID>" + DrpDwnLstRate.SelectedValue + "</maxLineRateID>";
                else
                    inxml += "          <maxLineRateID>" + lstLineRate.SelectedValue + "</maxLineRateID>";

                inxml += "          <audioCodec>" + lstAudioCodecs.SelectedValue + "</audioCodec>";
                inxml += "          <videoCodec>" + lstVideoCodecs.SelectedValue + "</videoCodec>";
                if (chkDualStreamMode.Checked)
                    inxml += "          <dualStream>1</dualStream>";
                else
                    inxml += "          <dualStream>0</dualStream>";
                if (chkConfOnPort.Checked)
                    inxml += "          <confOnPort>1</confOnPort>";
                else
                    inxml += "          <confOnPort>0</confOnPort>";
                log.Trace("chkEncryption.Checked: " + chkEncryption.Checked);
                if (chkEncryption.Checked.Equals(true))
                    inxml += "          <encryption>1</encryption>";
                else
                    inxml += "          <encryption>0</encryption>";
                if (chkLectureMode.Checked)
                    inxml += "          <lectureMode>1</lectureMode>";
                else
                    inxml += "          <lectureMode>0</lectureMode>";
                inxml += "          <VideoMode>" + lstVideoMode.SelectedValue + "</VideoMode>";
                if (chkSingleDialin.Checked)
                    inxml += "      <SingleDialin>1</SingleDialin>";
                else
                    inxml += "      <SingleDialin>0</SingleDialin>";
                inxml += "      <internalBridge>"+ hdnintbridge.Value +"</internalBridge>"; //FB 2376
                inxml += "      <externalBridge>" + hdnextbridge.Value + "</externalBridge>";
                //FB 2501 FECC Starts//FB 2571
				// if (!chkVMR.Checked && chkFECC.Checked)
                if (!(lstVMR.SelectedIndex >0) && chkFECC.Checked) // FB 2620
                    inxml += "      <FECCMode>1</FECCMode>";
                else 
                    inxml += "      <FECCMode>0</FECCMode>";
               
                //FB 2441 Starts
                if (chkSendMail.Checked)
                    inxml += "<PolycomSendMail>1</PolycomSendMail>";
                else
                    inxml += "<PolycomSendMail>0</PolycomSendMail>";
                //inxml += "<PolycomTemplate>" + txt_polycomTemplate.Text + "</PolycomTemplate>"; // Commented for ZD 100298
                //FB 2441 Ends
                inxml += "    </advAVParam>";

                string[] partysary = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries); //FB 1888
                int partynum = partysary.Length;
                //Response.Write(partynum);
                //Response.End();

                //FB 1830 Email Edit - start
                int partyid = 0;
                string sendemail = "1";
                GetXConfParams();
                //FB 1830 Email Edit - end

                inxml += "<partys>";//NUll CHeck
                for (int i = 0; i < partynum ; i++) //FB 1888
                {
                    string[] partyary = partysary[i].Split(ExclamDelim, StringSplitOptions.None); //FB 1888 //FB 2376
                    int partyInvite = -1;
                    int partyAudVid = -1;
                    if (partyary[4].Equals("1"))
                        partyInvite = 1;
                    if (partyary[5].Equals("1"))
                        partyInvite = 2;
                    if (partyary[6].Equals("1"))
                        partyInvite = 0;
                    if (partyary[18] != null)
                    {
                        if (partyary[18].Equals("1")) //FB 2376
                            partyInvite = 4;
                    }
                    string partyNotify = partyary[7];
                    if (partyary[8].Equals("1"))
                        partyAudVid = 2; //video FB 1744
                    //if (partyary[9].Equals("1"))//FB 1760 - Commented
                    else //FB 1760
                        partyAudVid = 1; //audio FB 1744

                    string survey = partyary[16]; //FB 2348 
                    //FB 2347
                    if (partyary[17].Equals("1"))
                        partyInvite = 3;
                    //FB 2347

                    //FB 2347
                    if (partyInvite == -1)
                        partyInvite = 0;
                    //FB 2347
                    
                    String UserID = partyary[0].ToString();
                    if (UserID.Trim().IndexOf("new") >= 0)
                        UserID = "new";

                    //FB 1830 Email Edit - start
                    if (isEditMode == "1")
                    {
                        if (hdnemailalert.Value == "0") //Dont notify all participants on edit
                        {
                            partyid = 0;
                            int.TryParse(UserID, out partyid);
                            sendemail = "0";
                            if (xPartys != null)
                            {
                                if (!xPartys.Contains(partyid)) //Notify new participants alone
                                    sendemail = "1";
                            }
                        }
                    }

                    inxml += "<party>";
                    inxml += "<partyID>" + UserID + "</partyID>";
                    inxml += "<partyFirstName>" + obj.ControlConformityCheck(partyary[1].ToString().Replace("++", ",")) + "</partyFirstName>"; //FB 1640 // ZD 100263
                    inxml += "<partyLastName>" + obj.ControlConformityCheck(partyary[2].ToString().Replace("++", ",")) + "</partyLastName>"; // ZD 100263
                    inxml += "<partyEmail>" + partyary[3].ToString() + "</partyEmail>";
                    inxml += "<partyInvite>" + partyInvite + "</partyInvite>";
                    inxml += "<partyNotify>" + partyNotify + "</partyNotify>";
                    inxml += "<partyAudVid>" + partyAudVid + "</partyAudVid>";
                    inxml += "<notifyOnEdit>" + sendemail + "</notifyOnEdit>"; //FB 1830 Email Edit
                    inxml += "<survey>" + survey + "</survey>"; //FB 2348
                    inxml += "<partyPublicVMR>" + partyary[19].ToString() + "</partyPublicVMR>"; //FB 2550
                    inxml += "</party>";
                    //FB 1830 Email Edit - end
                }
                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl)) // figbugz case 349
                {
                    inxml += "          <party>";
                    inxml += "              <partyID>" + hdnApprover4.Text + "</partyID>";
                    inxml += "              <partyFirstName>" + txtApprover4.Text.Split(' ')[0] + "</partyFirstName>";
                    inxml += "              <partyLastName>" + txtApprover4.Text.Split(' ')[0] + "</partyLastName>";
                    inxml += "              <partyEmail></partyEmail>";
                    inxml += "              <partyInvite>1</partyInvite>";
                    inxml += "              <partyNotify>1</partyNotify>";
                    inxml += "              <partyAudVid>1</partyAudVid>"; //FB 1744 - Party media type is set as None earlier for this client & now its Audio 
                    inxml += "              <notifyOnEdit>1</notifyOnEdit>"; //FB 1830 Email Edit
                    inxml += "              <partyPublicVMR>0</partyPublicVMR>"; //FB 2550
                    inxml += "          </party>";

                }

                inxml += "</partys>";//NUll CHeck
                //Response.Write(lblConfID.Text.IndexOf(","));
                if (Recur.Value.Trim() != "")
                    txtModifyType.Text = "1";
                else
                    txtModifyType.Text = "0";
                inxml += "      <ModifyType>" + txtModifyType.Text + "</ModifyType>";
                inxml += "      <fileUpload>";
                inxml += "          <file>" + hdnUpload1.Text + "</file>";
                inxml += "          <file>" + hdnUpload2.Text + "</file>";
                inxml += "          <file>" + hdnUpload3.Text + "</file>";
                inxml += "      </fileUpload>";

                inxml += "  <CustomAttributesList>";

                //code added for custom attribute fixes - start
                //if (enableEntity == "1")
                //{
                    FillCustomAttributeTable();
                    if (CAObj == null)
                        CAObj = new myVRMNet.CustomAttributes();

                    //Corrected FB 2377 codes during FB 2501 - Starts
                    string custAtt = "";
                    custAtt = CAObj.CustomAttributeInxml(custControlIDs, tblCustomAttribute);
                    

                    if (custAtt.IndexOf("<error>") >= 0)
                    {
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.LoadXml(custAtt);
                        errLabel.Text = xDoc.SelectSingleNode("error").InnerText;
                        errLabel.Visible = true;
                        //FB 2501 Starts
                        btnNext.Visible = true;
                        btnPrev.Visible = true;
                        //FB 2501 Ends
                        foreach (MenuItem mi in TopMenu.Items)
                            if (mi.Value.Trim() == "7") //Text.ToLower().IndexOf("additional") >= 0) //FB JAPAN
                            {
                                Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                                TopMenu.FindItem(mi.Value).Selected = true;
                                //UpdateAdvAVSettings(new object(), new EventArgs());
                            }
                        isCOMError = true;
                        return false;
                    }
                    else
                    {
                        inxml += custAtt;
                    }
                //}
                //code added for custom attribute fixes - end

				//FB 1750
                //if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.NGC))
                //{
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>1</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    //inxml += "          <OptionValue>" + txtCANGC1.Text + "</OptionValue>";  //Commented in aspx Page
                //    inxml += "      </CustomAttribute>";
                //}
                //if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.LHRIC))
                //{
                //    for (int i = 12; i <= 19; i++)
                //    {
                //        inxml += "      <CustomAttribute>";
                //        inxml += "          <CustomAttributeID>" + i + "</CustomAttributeID>";
                //        TextBox txtTemp = new TextBox();
                //        DropDownList lstTemp;
                //        if (tblLHRICCustomAttributes.FindControl("ctrlCA" + i).ToString().ToUpper().IndexOf("TEXTBOX") >= 0)
                //        {
                //            inxml += "          <OptionID>1</OptionID>";
                //            inxml += "          <Type>4</Type>";
                //            txtTemp = (TextBox)tblLHRICCustomAttributes.FindControl("ctrlCA" + i);
                //            inxml += "          <OptionValue>" + txtTemp.Text + "</OptionValue>";
                //        }
                //        if (tblLHRICCustomAttributes.FindControl("ctrlCA" + i).ToString().ToUpper().IndexOf("DROPDOWNLIST") >= 0)
                //        {
                //            lstTemp = (DropDownList)tblLHRICCustomAttributes.FindControl("ctrlCA" + i);
                //            inxml += "          <OptionID>" + lstTemp.SelectedValue + "</OptionID>";
                //            inxml += "          <Type>6</Type>";
                //            inxml += "          <OptionValue>1</OptionValue>";
                //        }
                //        inxml += "      </CustomAttribute>";
                //    }
                //}
                //if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl))
                //{
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>1</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA1.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>2</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA2.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>3</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA3.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>4</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA4.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>5</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA5.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>6</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA6.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>7</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA7.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>8</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA8.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>9</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA9.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>10</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>4</Type>";
                //    inxml += "          <OptionValue>" + txtCA10.Text + "</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //    inxml += "      <CustomAttribute>";
                //    inxml += "          <CustomAttributeID>11</CustomAttributeID>";
                //    inxml += "          <OptionID>1</OptionID>";
                //    inxml += "          <Type>2</Type>";
                //    if (chkCA1.Checked)
                //        inxml += "          <OptionValue>1</OptionValue>";
                //    else
                //        inxml += "          <OptionValue>0</OptionValue>";
                //    inxml += "      </CustomAttribute>";
                //}
                inxml += "  </CustomAttributesList>";
                inxml += "  <ICALAttachment>" + GenerateICAL() + "</ICALAttachment>";

                //2457 exchange round trip Starts
                if (Application["Exchange"].ToString().ToUpper() == "YES")
                    inxml += "<isExchange>1</isExchange>";
                else
                    inxml += "<isExchange>0</isExchange>";
                //2457 exchange round trip Ends

				//FB 2659 Starts
                if (enableCloudInstallation == 1)
                    inxml += "<overBookConf>" + hdnOverBookConf.Value + "</overBookConf>";
                else
                    inxml += "<overBookConf>0</overBookConf>";
                //FB 2659 End
                //FB 2441 Starts
                inxml += "  <MCUs>";
                foreach (DataGridItem dgi in dgRooms.Items)
                {                
                    inxml += "<BridgeID>" + ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue + "</BridgeID>";
                }
                foreach (DataGridItem dgi in dgUsers.Items)
                {
                    
                    if (((HtmlTable)dgi.FindControl("tbMCUandConn")).Style.Value == "display:none;" && dgRooms.Items.Count > 0)
                    {
                        ((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue = "2";
                        inxml += "<BridgeID>" + ((DropDownList)dgRooms.Items[0].Cells[0].FindControl("lstBridges")).SelectedValue + "</BridgeID>";
                    }
                    else
                        inxml += "<BridgeID>" + ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue + "</BridgeID>";
                    
                }
                inxml += "</MCUs>";
                //FB 2441 Ends
                inxml += "</confInfo>";
                inxml += "</conference>";
                log.Trace(inxml);
                //Response.Write(btnConfSubmit.Text);
                //Response.Write(btnConfSubmit.Text.IndexOf("Custom"));
                //Response.End();

                //if (btnConfSubmit.Text.IndexOf("Custom") >= 0)
                if (btnConfSubmit.Text.IndexOf(obj.GetTranslatedText("Customized")) >= 0) //FB JAPAN
                    inxml = ChangeXML(inxml);
                if (inxml.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(inxml);
                    errLabel.Visible = true;
                    return false;
                }
                //Response.Write(obj.Transfer(inxml));
                //Response.End();
                //FB 2027 SetConference start
                //String outxml = obj.CallCOM("SetConference", inxml, Application["COM_ConfigPath"].ToString());
                inxml = inxml.Replace("&nbsp;", " "); //FB 2388
                String outxml = obj.CallCommand("SetConference", inxml);
                //FB 2027 SetConference end
                //Response.Write("<hr>" + obj.Transfer(outxml));
                //Response.End();
                log.Trace("SetConference OutXML: " + outxml);
                if (outxml.IndexOf("<error>") >= 0)
                {
                    isCOMError = true;  //custom attribute fixes
                    errLabel.Text = obj.ShowErrorMessageConferenceSetup(outxml);//FB 1426

                    if (enableCloudInstallation == 1 && errLabel.Text.Contains("698")) //FB 2659
                    {
                        hdnOverBookConf.Value = "1";
                    }
                    errLabel.Visible = true;
                    btnConfSubmit.Visible = true;
                    string errLevel = obj.GetErrorLevel(outxml);
                    if (errLevel.Trim().Equals("C"))
                    {
                        AddInstanceInfo(inxml, outxml); //FB 2027 SetConference
                    }
                    return false;
                }
                else
                {
                    isCOMError = false;  //custom attribute fixes
                    /* *** Recurrence Fixes Editing the dirty instances - start **** */
                    Session["DirtyDT"] = null;
                    Session.Remove("DirtyDT");
                    Session.Remove("IsDirty");
                    Session.Remove("DirtyText");
                    Session.Remove("IsInstanceEdit");
                    /* *** Recurrence Fixes Editing the dirty instances - end **** */

                    Session.Add("outxml", outxml);
                    XmlDocument xmlout = new XmlDocument();
                    xmlout.LoadXml(outxml);
                    String confID = xmlout.SelectSingleNode("//setConference/conferences/conference/confID").InnerText;
                    Session["confID"] = confID;
                    lblConfID.Text = confID;
                    if (SetWorkOrders() && SetEndpoints())
                    {//Response.Redirect("Setsessionoutxml.aspx?tp=Confirm.asp");
                        //Session["CATMainGridDS"] = null;

                        if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                        {
                            Session["HKMainGridDS"] = null;
                            Session["AVMainGridDS"] = null; // FB 2050
                        }
                        else
                        {
                            ViewState["HKMainGridDS"] = null;
                            ViewState["AVMainGridDS"] = null; // FB 2050
                        }

                        Session["CalendarMonthly"] = null;//FB 1850


                        //FB 2363 - Start
                        if (Application["External"].ToString() != "")
                        {
                            String inEXML = "";
                            inEXML = "<SetExternalScheduling>";
                            inEXML += "<confID>" + confID + "</confID>";
                            inEXML += "</SetExternalScheduling>";

                            String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                        }
                        //FB 2363 - End
                        //FB 2426 Start

                        String eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["EptXmlPath"].ToString();
                        if (File.Exists(eptxmlPath))
                            File.Delete(eptxmlPath);
                        String roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();
                        if (File.Exists(roomxmlPath))
                        {
                            if (obj.WaitForFile(roomxmlPath))
                                File.Delete(roomxmlPath);
                        }

                        //FB 2426 End

                        Response.Redirect("ManageConference.aspx?confirm=1&t=", true);
                    }
                    else
                        btnConfSubmit.Visible = true;
                    //Response.Redirect("confirmNET.aspx?c=" + lstConferenceType.SelectedValue + "&f=n");
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error occurred in SetConference. Please try later.\n" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();//FB 2659 //ZD 100263
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        /* FB Issue 1192 Customized Instances Handling on conflict .. codes modified & FB 1426 - start */
        #region ChangeXML
        protected string ChangeXML(string inxml)
        {
            String recurringXPath = "";
            String customPatternXml = "";
            string timeZone = "";
            //FB 2218
            XmlNode defunctNode = null;
            string defunctPattern = "defunctPattern";
            string bufferxml = "		<setupDuration>0</setupDuration>";
            bufferxml += "		<teardownDuration>0</teardownDuration>";
            //FB 2218
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inxml);

                XmlNode recurNode = null;

                recurringXPath = "//conference/confInfo/recurrencePattern";

                customPatternXml += "<recurType>5</recurType>";
                customPatternXml += "<startDates></startDates>";

                recurNode = xmldoc.SelectSingleNode(recurringXPath);
                if (recurNode != null)
                {
                    //FB 2218

                    if (xmldoc.SelectSingleNode("//conference/confInfo/setupDuration") != null && xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration")!= null)                    
                    {
                        bufferxml = "		<setupDuration>" + xmldoc.SelectSingleNode("//conference/confInfo/setupDuration").InnerText + "</setupDuration>";
                        bufferxml += "		<teardownDuration>" + xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration").InnerText + "</teardownDuration>";
                    }

                    defunctNode = xmldoc.CreateElement(defunctPattern);
                    defunctNode.InnerXml = obj.AppendRecur(Recur.Value, bufferxml, lstConferenceTZ.SelectedValue);
                    //FB 2218

                    recurNode.InnerXml = "";
                    recurNode.InnerXml = customPatternXml;

                    recurNode = xmldoc.SelectSingleNode("//conference/confInfo/recurrencePattern/startDates");
                }

                customPatternXml = "";
                XmlNode node;
                string appointXml = "";
                node = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/timeZone");
                timeZone = node.InnerXml;

                node = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime");
                string cusXML = "";
                cusXML += "<customInstance>1</customInstance>";
                cusXML += "<instances>";

                CheckBox del;
                MetaBuilders.WebControls.ComboBox tb;
                DateTime conflictSD, conflictED;
                int cntDelete = 0;
                foreach (DataGridItem dgi in dgConflict.Items)
                {
                    del = (CheckBox)dgi.FindControl("chkConflictDelete");
                    if (!del.Checked)
                    {
                        string confStDate = myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[0].Text);
                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictStartTime");
                        confStDate = confStDate + " " + myVRMNet.NETFunctions.ChangeTimeFormat(tb.Text); //FB 2588

                        if (!DateTime.TryParse(confStDate, out conflictSD))
                            throw new Exception(obj.GetTranslatedText("Invalid conflict date"));

                        //code added/changed for buffer zone --Start

                        string confEDDate = conflictSD.ToShortDateString();
                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                        confEDDate = confEDDate + " " + myVRMNet.NETFunctions.ChangeTimeFormat(tb.Text); //FB 2588

                        if(!DateTime.TryParse(confEDDate, out conflictED))
                            throw new Exception(obj.GetTranslatedText("Invalid conflict date"));

                        if (conflictED <= conflictSD) 
                        {
                            conflictED = conflictSD.AddDays(1);
                            confEDDate = conflictED.ToShortDateString() + " " + myVRMNet.NETFunctions.ChangeTimeFormat(tb.Text); //FB 2588

                            if (!DateTime.TryParse(confEDDate, out conflictED))
                                throw new Exception(obj.GetTranslatedText("Invalid conflict date"));
                        }

                        DateTime conflictSetupDate, conflictTearDate;
                        string setupDate = "";
                        string teardownDate = "";
                        
                        if (dgi.Cells[0].Text != "")
                            setupDate = myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[0].Text);

                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictSetupTime");
                        setupDate = setupDate + " " + myVRMNet.NETFunctions.ChangeTimeFormat(tb.Text); //FB 2588

                        DateTime.TryParse(setupDate, out conflictSetupDate);

                        if (conflictSetupDate < conflictSD)
                        {
                            setupDate = conflictED.ToShortDateString() + " " + myVRMNet.NETFunctions.ChangeTimeFormat(tb.Text); //FB 2588
                            DateTime.TryParse(setupDate, out conflictSetupDate);
                        }

                        teardownDate = conflictED.ToShortDateString();

                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictTeardownTime");
                        teardownDate = teardownDate + " " + myVRMNet.NETFunctions.ChangeTimeFormat(tb.Text); //FB 2588
                        DateTime.TryParse(teardownDate, out conflictTearDate);

                        if (conflictED < conflictTearDate)
                        {
                            teardownDate = conflictSetupDate.ToShortDateString() + " " + myVRMNet.NETFunctions.ChangeTimeFormat(tb.Text); //FB 2588
                            DateTime.TryParse(teardownDate, out conflictTearDate);
                        }

                        TimeSpan conflictDur = conflictED.Subtract(conflictSD);
                        TimeSpan setupDuration = conflictSetupDate.Subtract(conflictSD);
                        TimeSpan tearDuration = conflictED.Subtract(conflictTearDate);
                        TimeSpan bufferDur = conflictTearDate.Subtract(conflictSetupDate);

                        if (conflictDur.TotalMinutes <= 0)
                            throw new Exception(obj.GetTranslatedText("Please check the start and end time for the conference."));

                        if (conflictDur.TotalMinutes > (24*60))
                            throw new Exception(obj.GetTranslatedText("Invalid Duration. Conference duration should be maximum of 24 hours."));

                        if (enableBufferZone == "1")   //buffer zone
                        {
                            if (conflictSD > conflictSetupDate)
                                throw new Exception(obj.GetTranslatedText("Invalid Setup Time"));

                            if (conflictTearDate > conflictED)
                                throw new Exception(obj.GetTranslatedText("Invalid Teardown Time"));

                            if (conflictSetupDate >= conflictTearDate)
                                throw new Exception(obj.GetTranslatedText("Invalid Setup/Teardown Time"));

                            if (bufferDur.TotalMinutes < 15)
                                throw new Exception(obj.GetTranslatedText("Invalid Duration. Conference duration should be minimum of 15 mins."));
                        }
                        //code added/changed for buffer zone --End

                        if (cntDelete == 0)
                        {
                            appointXml += "<appointmentTime>";
                            appointXml += "   <timeZone>" + timeZone + "</timeZone>";
                            appointXml += "   <startHour>" + conflictSD.ToString("hh") + "</startHour>";
                            appointXml += "   <startMin>" + conflictSD.Minute + "</startMin>";
                            appointXml += "   <startSet>" + conflictSD.ToString("tt") + "</startSet>";
                            appointXml += "   <durationMin>" + conflictDur.TotalMinutes + "</durationMin>";
                            appointXml += "	  <setupDuration>" + setupDuration.TotalMinutes + "</setupDuration>";  //buffer zone
                            appointXml += "	  <teardownDuration>" + tearDuration.TotalMinutes + "</teardownDuration>"; //buffer zone
                            appointXml += "</appointmentTime>";

                            cusXML += appointXml;
                        }
                        cntDelete++;

                        recurNode.InnerXml += "<startDate>" + conflictSD.ToString("MM/dd/yyyy") + "</startDate>";

                        if (customPatternXml == "")
                            customPatternXml = conflictSD.ToString("MM/dd/yyyy");
                        else
                            customPatternXml += ", " + conflictSD.ToString("MM/dd/yyyy");

                        cusXML += "<instance>";
                        cusXML += " <startDate>" + conflictSD.ToString("MM/dd/yyyy") + "</startDate>";
                        cusXML += " <startHour>" + conflictSD.ToString("hh") + "</startHour>";
                        cusXML += " <startMin>" + conflictSD.Minute + "</startMin>";
                        cusXML += " <startSet>" + conflictSD.ToString("tt") + "</startSet>";
                        cusXML += " <durationMin>" + conflictDur.TotalMinutes + "</durationMin>";
                        cusXML += "	<setupDuration>" + setupDuration.TotalMinutes + "</setupDuration>"; //buffer zone
                        cusXML += "	<teardownDuration>" + tearDuration.TotalMinutes + "</teardownDuration>";  //buffer zone
                        cusXML += "</instance>";
                    }
                }
                cusXML += "</instances>";
                node.InnerXml = cusXML;

                recurNode = xmldoc.SelectSingleNode("//conference/confInfo/recurringText");
                if (recurNode != null)
                {
                    recurNode.InnerXml = "Custom Date Selection: " + customPatternXml;
                    xmldoc.DocumentElement.InsertBefore(defunctNode, null);//FB 2218
                }




                if (cntDelete < 2)
                    return "<error><message>" + obj.GetErrorMessage(432) + "</message></error>";//FB 1881
                //return "<error><message>A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.</message><level>E</level><errorCode></errorCode></error>";
                else
                    return xmldoc.InnerXml;
                //XmlNode cusNode = 
                //parentNode.AppendChild(cusNode);
                //                Response.Write("<hr>" + obj.Transfer(xmldoc.InnerXml));
                //                Response.End();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
                return "<error><message>" + obj.ShowSystemMessage() +"</message></error>"; //ZD 100263
            }
        }
        #endregion
        /* FB Issue 1192 Customized Instances Handling on conflict .. codes modified & FB 1426  - end */

        #region EnableControls

        protected void EnableControls(Object sender, EventArgs e)
        {
            try
            {
                CheckBox chkTemp = (CheckBox)sender;
                foreach (DataGridItem dgi in dgRooms.Items)
                    if (((CheckBox)dgi.FindControl("chkUseDefault")).Equals(chkTemp))
                    {
                        ((DropDownList)dgi.FindControl("lstProfiles")).Enabled = !chkTemp.Checked;
                        if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB Case 770 Saima//FB 2839
                        {
                            ((DropDownList)dgi.FindControl("lstBridges")).Enabled = !chkTemp.Checked;
                            if (EnableProfileSelection != null)
                            {
                                if (EnableProfileSelection == "1")
                                {
                                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Enabled = !chkTemp.Checked;
                                }
                            }
                        }
                        //Code Added for FB 1422- Start
                        if (lstConferenceType.SelectedValue.Equals("4"))
                        {
                            ((DropDownList)dgi.FindControl("lstTelnet")).Enabled = !chkTemp.Checked;
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;
                        }
                        //Code Added for FB 1422- End
                        IsValidBridge(Convert.ToString(((DropDownList)dgi.FindControl("lstBridges")).SelectedValue)); //FB 3052
                        if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;
                    }
                String strScript = "<script>CheckFiles();</script>";
                if (!IsClientScriptBlockRegistered("checkFiles"))
                    RegisterClientScriptBlock("checkFiles", strScript);

                //    PreserveUsersState();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region ValidateRoomSelection

        protected bool ValidateRoomSelection()
        {
            try
            {
                bool flag = true;
                log.Trace("In ValidateRoomSelection");
                int treeCount = 0;
                if (lstConferenceType.SelectedValue.Equals("7"))
                    return flag;
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    if (tn.Depth.Equals(3))
                        treeCount++;
                log.Trace("tree: " + treeCount + ", dgRooms: " + dgRooms.Items.Count);

                if (treeCount.Equals(0))
                {
                    dgRooms.DataSource = null;
                    dgRooms.DataBind();
                    return true;
                }

                //if (!treeCount.Equals(dgRooms.Items.Count)) For AV Switch
                if ( (!treeCount.Equals(dgRooms.Items.Count) && !(lstVMR.SelectedIndex >0) ) && enableAV.Equals("1")) //FB 2448 //FB 2620
                {
                    flag = false;
                }
                else
                {
                    SyncRoomSelection();
                    //code added for FB 1221 -- start
                    Int32 cnt = 0;
                    if (bridgeIds == null)   //FB 1462
                        bridgeIds = new ArrayList();    //FB 1462
                    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    {
                        flag = false;
                        foreach (DataGridItem dgi in dgRooms.Items)
                        {
                            if (tn.Depth.Equals(3))//Code added for 1253
                            {
                                if (tn.Value.Equals(dgi.Cells[0].Text))
                                {
                                    cnt = cnt + 1;
                                    /* *** FB 1462 - start *** */
                                    DropDownList mcuList = new DropDownList();
                                    mcuList = dgi.FindControl("lstBridges") as DropDownList;
                                    if (mcuList.SelectedValue != "")
                                    {
                                        if (!bridgeIds.Contains(mcuList.SelectedValue))
                                            bridgeIds.Add(mcuList.SelectedValue);
                                    }
                                    /* *** FB 1462 - end *** */
                                }
                            }
                            //flag = true;
                        }
                    }

                    if (dgRooms.Items.Count == cnt)
                        flag = true;
                    //code added for FB 1221 -- end
                }
                return flag;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        #endregion

        /* *** FB 1462 - start *** */

        #region ValidateBridgeAdmin
        /// <summary>
        /// Method to validate the MCU admin status
        /// </summary>
        /// <returns></returns>
        private bool ValidateBridgeAdmin()
        {
            bool adminStatus = true;
            string inXML = "";

            if (bridgeIds == null)
                return true;

            if (bridgeIds.Count <= 0)
                return true;

            inXML = "<Bridges>"; 
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            for (int i = 0; i < bridgeIds.Count; i++)
            {
                inXML += "<ID>" + bridgeIds[i] + "</ID>";
            }
            inXML += "</Bridges>";

            String outXML = obj.CallMyVRMServer("FetchBridgeInfo", inXML, Application["MyVRMServer_Configpath"].ToString());
            XmlDocument xd = new XmlDocument();
            errLabel.Text = "";
            if (outXML != "")
            {
                xd.LoadXml(outXML);
                XmlNodeList nodes = xd.SelectNodes("bridges/bridge");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("userstate") != null)
                        {
                            if (node.SelectSingleNode("userstate").InnerText == "I")
                            {
                                if (errLabel.Text == "")
                                    errLabel.Text += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() + obj.GetTranslatedText("is In-Active. Please make sure the user is Active.");
                                else
                                    errLabel.Text += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() +  obj.GetTranslatedText("is In-Active. Please make sure the user is Active.");

                                adminStatus = false;
                            }
                            else if (node.SelectSingleNode("userstate").InnerText == "D")
                            {
                                if (errLabel.Text == "")
                                    errLabel.Text += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ")" + obj.GetErrorMessage(430);//FB 1881
                                    //errLabel.Text += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User has been Deleted";
                                else
                                    errLabel.Text += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ")" + obj.GetErrorMessage(430);//FB 1881
                                    //errLabel.Text += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User has been Deleted";

                                adminStatus = false;
                            }
                        }
                    }
                }
            }
            return adminStatus;
        }
        #endregion
        /* *** FB 1462 - end *** */

        #region ValidateEndpoints

        protected bool ValidateEndpoints()
        {
            try
            {
                //if (!Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.NGC))
                //    return true;

                String errMsg = "";

                if (enableAV.Equals("1"))// FOr AV Switch
                {
                    if (!ValidateRoomSelection() && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//FB 2694
                    {

                        foreach (MenuItem mi in TopMenu.Items)
                            //if (mi.Text.ToLower().IndexOf("video") >= 0)
                            if (mi.Value == "3" && !chkPCConf.Checked) //Text.ToLower().IndexOf("audio") >= 0)//FB 1985 //FB JAPAN //FB 3032
                            {
                                Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                                TopMenu.FindItem(mi.Value).Selected = true;
                                UpdateAdvAVSettings(new object(), new EventArgs());
                            }
                        errLabel.Text = obj.GetTranslatedText("Please confirm settings below. If changes are required, please edit the fields accordingly OR contact technical support ") + HttpContext.Current.Application["contactName"].ToString() + " @ " + HttpContext.Current.Application["contactPhone"].ToString() + obj.GetTranslatedText(" for assistance.<br>Once settings are confirmed please click Preview tab and resubmit the conference.");//FB 1830 - Translation
                        return false;



                    }

                    if (!ValidateUserSelection() && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//FB 2694
                    {

                        foreach (MenuItem mi in TopMenu.Items)
                            //if (mi.Text.ToLower().IndexOf("video") >= 0)
                            if (mi.Value == "3") //if (mi.Text.ToLower().IndexOf("audio") >= 0)//FB 1985 //FB JAPAN
                            {
                                Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                                TopMenu.FindItem(mi.Value).Selected = true;
                                UpdateAdvAVSettings(new object(), new EventArgs());
                            }
                        errLabel.Text = obj.GetTranslatedText("Please confirm settings below. If changes are required, please edit the fields accordingly OR contact technical support ") + HttpContext.Current.Application["contactName"].ToString() + " @ " + HttpContext.Current.Application["contactPhone"].ToString() + obj.GetTranslatedText(" for assistance.<br>Once settings are confirmed please click Preview tab and resubmit the conference.");//FB 1830 - Translation
                        return false;

                    }

                    if (!ValidateIPISDNAddress(ref errMsg) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//FB 2694
                    {

                        foreach (MenuItem mi in TopMenu.Items)
                            //if (mi.Text.ToLower().IndexOf("video") >= 0)
                            if (mi.Value == "3") //if (mi.Text.ToLower().IndexOf("audio") >= 0)//FB 1985 //FB JAPAN
                            {
                                Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                                TopMenu.FindItem(mi.Value).Selected = true;
                                UpdateAdvAVSettings(new object(), new EventArgs());
                            }
                        errLabel.Text += errMsg;
                        return false;

                    }

                    // Code Added For FB 1422 - Start  - Code block position changed from the top - FB 1468              
                    if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                    {
                        //FB 2430 start
                        Int32 var = dgRooms.Items.Count; // +dgUsers.Items.Count;

                        for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++)
                        {
                            if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1"))
                                var++;
                        }
                        //FB 2430 end

                        foreach (MenuItem mi in TopMenu.Items)
                            if (var != 2)//FB 2390
                            {
                                //if (mi.Text.ToLower().IndexOf("video") >= 0)
                                if (mi.Value == "3") //if (mi.Text.ToLower().IndexOf("audio") >= 0) //FB 1985 //FB JAPAN
                                {
                                    errLabel.Visible = true;
                                    Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                                    TopMenu.FindItem(mi.Value).Selected = true;
                                    UpdateAdvAVSettings(new object(), new EventArgs());
                                    errLabel.Text = obj.GetTranslatedText("Only two Endpoints can be selected for Point-To-Point Conference.");//FB 1830 - Translation
                                    return false;
                                }
                            }

                    }
                    //Code Added For FB 1422 -End

                    errMsg = "";
                    if (!ValidateMCUSelection(ref errMsg) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking)) //FB Case 770 Saima //FB 2694
                    {
                        if (enableAV.Equals("1"))// FOr AV Switch
                        {
                            foreach (MenuItem mi in TopMenu.Items)
                                //if (mi.Text.ToLower().IndexOf("video") >= 0)
                                if (mi.Value == "3") //if (mi.Text.ToLower().IndexOf("audio") >= 0)//FB 1985 //FB JAPAN
                                {
                                    Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                                    TopMenu.FindItem(mi.Value).Selected = true;
                                    UpdateAdvAVSettings(new object(), new EventArgs());
                                }
                        }

                        /* *** FB 1462 Reduntant code - Start *** */
                        //if (enableAV.Equals("1"))// FOr AV Switch
                        //{
                        //    foreach (MenuItem mi in TopMenu.Items)
                        //        if (mi.Text.ToLower().IndexOf("video") >= 0)
                        //        {
                        //            Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                        //            TopMenu.FindItem(mi.Value).Selected = true;
                        //            UpdateAdvAVSettings(new object(), new EventArgs());
                        //        }
                        //}
                        /* *** FB 1462 Reduntant code - End *** */

                        errLabel.Text += errMsg;
                        return false;
                    }
                    /* *** FB 1462 - start *** */
                    errLabel.Text = "";
                    if ((!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) && (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly)) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//FB 2694
                    {
                        if (!ValidateBridgeAdmin())
                        {
                            foreach (MenuItem mi in TopMenu.Items)
                                //if (mi.Text.ToLower().IndexOf("video") >= 0)
                                if (mi.Value == "3") //if (mi.Text.ToLower().IndexOf("audio") >= 0) //FB 1985 //FB JAPAN
                                {
                                    Wizard1.ActiveViewIndex = Int32.Parse(mi.Value);
                                    TopMenu.FindItem(mi.Value).Selected = true;
                                    UpdateAdvAVSettings(new object(), new EventArgs());
                                }
                            return false;
                        }
                    }
                    /* *** FB 1462 - end *** */
                }   //FB 1462
                return true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        #endregion

        #region ValidateMCUSelection

        protected bool ValidateMCUSelection(ref String errMsg)
        {
            try
            {
                errMsg = "";
                SyncRoomSelection();
                //Response.Write("Count: " + treeRoomSelection.CheckedNodes.Count);
                //if (treeRoomSelection.CheckedNodes.Count.Equals(0)) // fogbugz case 187
                //    return true;
                if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB Case 770 Saima
                    foreach (DataGridItem dgi in dgRooms.Items)
                    {
                        DropDownList lstBridges = (DropDownList)dgi.FindControl("lstBridges");
                        DropDownList lstProfiles = (DropDownList)dgi.FindControl("lstProfiles");
                        Label lblEndpointName = (Label)dgi.FindControl("lblEndpointName");
                        //Response.Write(lstProfiles.Items.Count);
                        if (lblEndpointName.Text.Trim().Equals("") || (lstProfiles.SelectedValue.Trim().Equals("") && lstProfiles.Items.Count.Equals(0)) || lblEndpointName.Text.Trim().Contains("No Endpoint(s) associated with this room")) //FB 1182 //FB JAPANDoubt
                            errMsg += "<br>" + obj.GetTranslatedText("An audio or video conference cannot be created with room ") + ((Label)dgi.FindControl("lblRoomName")).Text + obj.GetTranslatedText(" since there is no endpoint associated with it"); //FB JAPAN
                        if ((lstProfiles.SelectedValue.Equals("-1") && lstProfiles.Items.Count > 1))
                            errMsg += "<br>" + obj.GetTranslatedText("Please select a Profile for room: ") + ((Label)dgi.FindControl("lblRoomName")).Text;
                        if (lstBridges.SelectedValue.Equals("-1") && lstBridges.SelectedItem.Text.ToLower().IndexOf("auto") < 0) //FB JAPAN
                        	errMsg += "<br>" + obj.GetTranslatedText("Please select MCU for room: ") + ((Label)dgi.FindControl("lblRoomName")).Text;
                        //Response.Write("'" + lstBridges.SelectedValue + "'");
                    }

                //Response.End();
                if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB Case 770 Saima
                {
                    foreach (DataGridItem dgi in dgUsers.Items)
                    {
                        DropDownList lstBridges = (DropDownList)dgi.FindControl("lstBridges");
                        DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                        if (lstBridges.SelectedValue.Equals("-1") && lstBridges.SelectedItem.Text.Equals("No Items...")) //FB 2164
                        {
                            errMsg = "<br>" + obj.GetTranslatedText("Please select MCU for user:") + ((Label)dgi.FindControl("lblUserName")).Text; //FB JAPAN
                        }
                        if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                            if (lstBridges.SelectedValue.Equals("-1"))
                            {
                                errMsg = "<br>" + obj.GetTranslatedText("Please select MCU for user:") + ((Label)dgi.FindControl("lblUserName")).Text; //FB JAPAN//FB 2272
                            }//Response.Write("'" + lstBridges.SelectedValue + "'");
                            else if (!IsValidBridge(lstBridges.SelectedValue))
                            {
                                errMsg = "<br>" + obj.GetTranslatedText("Selected bridge for user ") + ((Label)dgi.FindControl("lblUserName")).Text + obj.GetTranslatedText(" does not support MPI calls."); //FB JAPAN
                            }
                    }
                }

                // fogbugz case 113. moved this check outside foreach loop instead of inside
                if (errMsg.Trim().Equals(""))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        #endregion

        #region IsValidBridge

        protected bool IsValidBridge(String BridgeID)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeID>" + BridgeID + "</bridgeID>";
                inXML += "</login>";

                String outXML = obj.CallMyVRMServer("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    bridgeType = Int32.Parse(xmldoc.SelectSingleNode("//bridge/bridgeType").InnerText); //FB 2839
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridge/bridgeTypes/type");
                                       
                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceServiceID") != null)
                        txtConfServiceID = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceServiceID").InnerText;

                    //Response.Write(bridgeType + " : " + nodes.Count + " : " + nodes[bridgeType - 1].SelectSingleNode("interfaceType").InnerText);
                    if (nodes[bridgeType - 1].SelectSingleNode("interfaceType").InnerText.Equals(ns_MyVRMNet.vrmMCUInterfaceType.Polycom))
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }

        #endregion

        #region ValidateIPISDNAddress

        protected bool ValidateIPISDNAddress(ref String errMsg)
        {
            try
            {
				//FB 3012 Starts
                string patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                Regex checkIP = new Regex(patternIP);
                string patternISDN = @"^[0-9]+$";
                Regex checkISDN = new Regex(patternISDN);

                foreach (DataGridItem dgi in dgUsers.Items)
                {
                    DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                    DropDownList lstConnectionType = (DropDownList)dgi.FindControl("lstConnectionType");
                    TextBox txtAddress = (TextBox)dgi.FindControl("txtAddress");
                    TextBox txtConfCode = (TextBox)dgi.FindControl("txtConfCode");
                    TextBox txtleaderPin = (TextBox)dgi.FindControl("txtleaderPin");

                    DropDownList lstProtocol = (DropDownList)dgi.FindControl("lstProtocol");
                    string pattern = "";
                    if (((DropDownList)dgi.FindControl("lstConnection")).SelectedValue == "2") //Code changed for FB 1744
                        pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                    else
                        pattern = @"^[0-9'.]+$";

                    Regex check = new Regex(pattern);                    
                    if (lstConnectionType.SelectedValue.Equals("3"))
                    {
                        if ((lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP) && (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN) || lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct)))
                            || ((lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN) && (!lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN) || lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct)))))
                        {
                            errMsg += "<br>" + obj.GetTranslatedText("Invalid Protocol, Connection Type and Address Type selected for user: ") + ((Label)dgi.FindControl("lblUserName")).Text;
                        }
                    }
                    if (lstConnectionType.SelectedValue.Equals("2"))
                    {
                        //fogbugz case 427
                        //{
                        //    if (!(lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI) && lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) && lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                        //        && (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI) || lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct)))
                        //    {
                        //        errMsg += "<br>"+obj.GetTranslatedText("Invalid Protocol, Connection Type and Address Type selected for user: ") + ((Label)dgi.FindControl("lblUserName")).Text;
                        //    }
                        //}
                        switch (lstAddressType.SelectedValue)
                        {
                            case ns_MyVRMNet.vrmAddressType.IP:
                                {                                    
                                    if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP))
                                    {
                                        if (txtAddress.Text == "")
                                            errMsg += obj.GetTranslatedText("Invalid IP Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                        else if (!checkIP.IsMatch(txtAddress.Text.Trim(), 0))
                                            errMsg += "<br>" + obj.GetTranslatedText("Invalid IP Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                    }
                                    else if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                                    {
                                        errMsg += obj.GetTranslatedText("Protocol and Address type doesn't match");
                                    }
                                    break;
                                }
                            case ns_MyVRMNet.vrmAddressType.ISDN:
                                {                                    
                                    if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                                    {
                                        if (txtAddress.Text == "")
                                            errMsg += obj.GetTranslatedText("Invalid ISDN Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                        else if (!checkISDN.IsMatch(txtAddress.Text.Trim(), 0))
                                        {
                                            errMsg += "<br>" + obj.GetTranslatedText("Invalid ISDN Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                        }
                                    }
                                    else
                                    {
                                        errMsg += obj.GetTranslatedText("Protocol and Address type doesn't match");
                                    }
                                    break;
                                }

                                // Commemnted because Validation should be kept only for IP and ISDN -- FB 3012.

                            //case ns_MyVRMNet.vrmAddressType.MPI:
                            //    {
                            //        String pattern = "\\w+";
                            //        Regex check = new Regex(pattern);
                            //        if (!check.IsMatch(txtAddress.Text.Trim(), 0))
                            //        {
                            //            errMsg += "Invalid MPI Address for user: " + ((Label)dgi.FindControl("lblUserName")).Text;
                            //        }
                            //        errMsg += "<br>" + obj.GetTranslatedText("MPI Connection must be established through the Endpoint profile. Please contact your VRM administrator.");
                            //    }
                                
                                // Commemnted because Validation should be kept only for IP and ISDN -- FB 3012.
                        }
                    }
                    if (((DropDownList)dgi.FindControl("lstConnection")).SelectedValue == "1") //Code changed for FB 1744
                    {
                            if (txtConfCode.Text.Trim() != "")
                            {
                                if (!check.IsMatch(txtConfCode.Text.Trim(), 0))
                                {
                                    errMsg += "<br>"+obj.GetTranslatedText("Invalid Conference Code for user")+" : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                } 
                            }
                            if (txtleaderPin.Text.Trim() != "")
                            {
                                if (!check.IsMatch(txtleaderPin.Text.Trim(), 0))
                                {
                                    errMsg += "<br>"+obj.GetTranslatedText("Invalid Leader Pin for user")+": " + ((Label)dgi.FindControl("lblUserName")).Text;
                                }
                            }

                     }
                    //FB Case 933 - Saima check if selected protocol and connection type for all users matches with network restriction specified for the conference.
                     if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN) && lstRestrictNWAccess.SelectedValue.Equals("1"))
                     {
                        errMsg += "<br>"+obj.GetTranslatedText("Invalid Network Protocol selected for user")+": " + ((Label)dgi.FindControl("lblUserName")).Text + ". "+ obj.GetTranslatedText("Network access is restricted to IP only.");
                     }
                    //if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    //{
                    //    //String pattern = "\\w+";
                    //    //Regex check = new Regex(pattern);
                    //    //if (!check.IsMatch(txtAddress.Text.Trim(), 0))
                    //    //{
                    //    //errMsg += "Invalid MPI Address for user: " + ((Label)dgi.FindControl("lblUserName")).Text;
                    //    //}
                    //    errMsg += "<br>"+obj.GetTranslatedText("MPI Connection must be established through the Endpoint profile. Please contact your VRM administrator.");
                    //}
                 }
                //FB Case 933 - Saima check if selected profile for the room matches with network restriction specified for the conference.

                //FB 3012 Ends
                foreach (DataGridItem dgi in dgRooms.Items)
                {
                    if (((DropDownList)dgi.FindControl("lstProfileType")).Items[((DropDownList)dgi.FindControl("lstProfiles")).SelectedIndex].Text.Equals(ns_MyVRMNet.vrmAddressType.ISDN)
                        && lstRestrictNWAccess.SelectedValue.Equals("1"))
                        errMsg += "<br>"+obj.GetTranslatedText("Invalid Endpoint Profile selected for room")+": " + ((Label)dgi.FindControl("lblRoomName")).Text + "."+obj.GetTranslatedText("Network access is restricted to IP only.");
                }
                if (errMsg.Trim().Equals(""))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                return false;
            }
        }

        #endregion

        #region ValidateUserSelection

        protected bool ValidateUserSelection()
        {
            try
            {
                log.Trace("In ValidateUserSelection");
                bool flag = true;
                int cnt = 0;

                if (bridgeIds == null)  //FB 1462
                    bridgeIds = new ArrayList();    //FB 1462

                for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++) //FB 1888 start
                {
                    log.Trace("Is External: " + txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4]);
                    if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1"))
                    {
                        cnt++;
                        flag = false;
                        log.Trace("UserID: " + txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0]);
                        foreach (DataGridItem dgi in dgUsers.Items)
                        {
                            String UserID = dgi.Cells[0].Text.ToString();
                            if (UserID.Trim().IndexOf("new") >= 0)
                                UserID = "new";
                            log.Trace("Grid UserID : UserID | " + UserID + " : " + txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim());
                            if (UserID.Equals(txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim())) //FB 1888 end
                                flag = true;
                        }
                        if (flag.Equals(false)) return false;

                    }
                }
                if (cnt == 0) return true; //fogbugz case 407
                //Response.Write(flag + " : " + cnt + " : " + dgUsers.Items.Count + " : " + dgUsers.Visible);
                //if (dgUsers.Visible.Equals(true))
                {
                    //if (!cnt.Equals(dgUsers.Items.Count)) return false;
                    foreach (DataGridItem dgi in dgUsers.Items)
                    {
                        flag = false;
                        String UserID = dgi.Cells[0].Text.ToString();
                        if (UserID.Trim().IndexOf("new") >= 0)
                            UserID = "new";
                        log.Trace("UserID: " + UserID);
                        for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length ; i++) //FB 1888
                        {
                            if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1") && txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim().Equals(UserID)) //FB 1888
                                flag = true;
                        }
                        if (flag.Equals(false)) return false;

                        /* *** FB 1462 - start *** */
                        DropDownList mcuList = new DropDownList();
                        mcuList = dgi.FindControl("lstBridges") as DropDownList;
                        if (mcuList.SelectedValue != "")
                        {
                            if (!bridgeIds.Contains(mcuList.SelectedValue))
                                bridgeIds.Add(mcuList.SelectedValue);
                        }
                        /* *** FB 1462 - end *** */
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        #endregion

        #region SetEndpoints

        protected bool SetEndpoints()
        {
            String isvip = "0";
            String isvipXml = "";
            String assistanceXML = "";
            String ReminderXML = "";//FB 1926
            StringBuilder ConfMsgxml = new StringBuilder(); //FB 2486
            
            try
            {
                //if (((dgRooms.Items.Count <= 0) && (dgUsers.Items.Count <= 0)) || (hasVisited.Text.Equals("0")))
                //    return true;
                String inXML = String.Empty;
                // FB 1864
                if (chkisVIP.Checked)
                    isvip = "1";

                isvipXml = " <isVIP>" + isvip + "</isVIP>";

                if (chkisDedicatedEngineer.Checked)
                    assistanceXML = "      <isDedicatedEngineer>1</isDedicatedEngineer>";
                else
                    assistanceXML = "      <isDedicatedEngineer>0</isDedicatedEngineer>";
                if (chkisLiveAssitant.Checked)
                    assistanceXML += "      <isLiveAssitant>1</isLiveAssitant>";
                else
                    assistanceXML += "      <isLiveAssitant>0</isLiveAssitant>";
                // FB 1864

                //FB 2870 Start
                if (ChkEnableNumericID.Checked)
                {
                    assistanceXML += "<EnableNumericID>1</EnableNumericID>";
                    assistanceXML += "<CTNumericID>" + txtNumeridID.Text + "</CTNumericID>";
                }
                else
                {
                    assistanceXML += "<EnableNumericID>0</EnableNumericID>";
                    assistanceXML += "<CTNumericID></CTNumericID>";
                }
                //FB 2870 End

                //FB 1926
                if (chkReminder.Checked)
                    ReminderXML += "      <isReminder>1</isReminder>";
                else
                    ReminderXML += "      <isReminder>0</isReminder>";

                //FB 2486 Starts
                #region ConfMessageList
                ConfMsgxml.Append("<ConfMessageList>");
                if (chkmsg1.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>1</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg1.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration1.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg2.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>2</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg2.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration2.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg3.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>3</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg3.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration3.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg4.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>4</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg4.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration4.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg5.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>5</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg5.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration5.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg6.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>6</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg6.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration6.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg7.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>7</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg7.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration7.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg8.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>8</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg8.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration8.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }
                if (chkmsg9.Checked)
                {
                    ConfMsgxml.Append("<ConfMessage>");
                    ConfMsgxml.Append("<controlID>9</controlID>");
                    ConfMsgxml.Append("<textMessage>" + drpdownconfmsg9.SelectedItem.Text.Trim() + "</textMessage>");
                    ConfMsgxml.Append("<msgduartion>" + drpdownmsgduration9.SelectedItem.Text.Trim() + "</msgduartion>");
                    ConfMsgxml.Append("</ConfMessage>");
                }

                ConfMsgxml.Append("</ConfMessageList>");
                #endregion
                //FB 2486 Ends

                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly))
                {
                    inXML = "";
                    inXML += "<SetAdvancedAVSettings>";

                    //FB 2457
                    if (Application["Exchange"].ToString().ToUpper() == "YES")
                        inXML += "<isExchange>1</isExchange>";
                    else
                        inXML += "<isExchange>0</isExchange>";

                    inXML += "<editFromWeb>1</editFromWeb>";//FB 2235
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += " <language>" + language + "</language>"; //FB 1830
                    inXML += isvipXml;// FB 1864
                    inXML += ReminderXML;// FB 1926
                    inXML += ConfMsgxml.ToString(); //FB 2486
                    inXML += "  <AVParams>";
                    inXML += assistanceXML;// FB 1864
                    if (chkSingleDialin.Checked)
                        inXML += "      <SingleDialin>1</SingleDialin>";
                    else
                        inXML += "      <SingleDialin>0</SingleDialin>";
                    inXML += "  </AVParams>";
                    inXML += "  <Endpoints>";
                    inXML += "  </Endpoints>";
                    inXML += "</SetAdvancedAVSettings>";
                }
                else
                {
                    //PreserveUsersState(); //Code Commented for FB 1688
                    XmlDocument xmldoc = new XmlDocument();
                    //Response.Write(obj.Transfer(Session["outxml"].ToString()));
                    xmldoc.LoadXml(Session["outxml"].ToString());
                    XmlNodeList nodes = xmldoc.SelectNodes("//setConference/conferences/conference/invited/party");

                    //FB 2426 start
                    string guestRoomStr = "";
                    XmlNode GuestRoom = xmldoc.SelectSingleNode("//setConference/conferences/conference/GuestRooms");
                    if (GuestRoom != null)
                    {
                        guestRoomStr = GuestRoom.InnerXml.Trim();
                    }
                    //FB 2426 end

                    inXML = "";
                    inXML += "<SetAdvancedAVSettings>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes

                    //FB 2457
                    if (Application["Exchange"].ToString().ToUpper() == "YES")
                        inXML += "<isExchange>1</isExchange>";
                    else
                        inXML += "<isExchange>0</isExchange>";

                    inXML += "<editFromWeb>1</editFromWeb>";//FB 2235
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                    inXML += " <language>" + language + "</language>"; //FB 1830
                    inXML += isvipXml;// FB 1864
                    inXML += ReminderXML;// FB 1926
                    inXML += ConfMsgxml.ToString(); //FB 2486
                    inXML += "  <AVParams>";
                    inXML += assistanceXML;// FB 1864
                    if (chkSingleDialin.Checked)
                        inXML += "      <SingleDialin>1</SingleDialin>";
                    else
                        inXML += "      <SingleDialin>0</SingleDialin>";
                    inXML += "  </AVParams>";
                    inXML += "  <Endpoints>";
                    //Response.Write(dgRooms.Visible);
                    //if (dgRooms.Visible.Equals(true))
                    foreach (DataGridItem dgi in dgRooms.Items)
                    {
                        inXML += "    <Endpoint>";
                        inXML += "          <Type>R</Type>";
                        inXML += "          <ID>" + dgi.Cells[0].Text.Trim() + "</ID>";
                        String temp = "0";
                        if (((CheckBox)dgi.FindControl("chkUseDefault")).Checked)
                            temp = "1";
                        inXML += "          <UseDefault>" + temp + "</UseDefault>";
                        if (chkLectureMode.Checked && lstEndpoints.SelectedIndex.Equals(dgi.ItemIndex + dgUsers.Items.Count + 1))
                            inXML += "          <IsLecturer>1</IsLecturer>";
                        else
                            inXML += "          <IsLecturer>0</IsLecturer>";
                        inXML += "          <EndpointID>" + ((Label)dgi.FindControl("lblEndpointID")).Text + "</EndpointID>";
                        inXML += "          <ProfileID>" + ((DropDownList)dgi.FindControl("lstProfiles")).SelectedValue + "</ProfileID>";
                        inXML += "          <BridgeID>" + ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue + "</BridgeID>";
                        inXML += "          <BridgeProfileID>" + ((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedValue + "</BridgeProfileID>";//FB 2839
                        inXML += "          <AddressType></AddressType>";
                        inXML += "          <Address></Address>";
                        inXML += "          <VideoEquipment></VideoEquipment>";
                        inXML += "          <connectionType></connectionType>";
                        inXML += "          <Bandwidth></Bandwidth>";
                        inXML += "          <IsOutside></IsOutside>";
                        inXML += "          <GateKeeeperAddress></GateKeeeperAddress>";//ZD 100132
                        inXML += "          <DefaultProtocol></DefaultProtocol>";
                        inXML += "          <Connection>2</Connection>"; //Code changed for FB 1744
                        inXML += "          <URL></URL>";
                        inXML += "          <ExchangeID></ExchangeID>"; //Cisco Telepresence fix
                        //API Port Starts..
                        int rmEpt = 23;
                        Int32.TryParse(((Label)dgi.FindControl("lblApiportno")).Text.Trim(),out rmEpt);
                        if (rmEpt <= 0)
                            rmEpt = 23;

                        inXML += "<APIPortNo>"+ rmEpt +"</APIPortNo>";
                        //API Port Ends..
                        //FB 1422 - Start  
                        if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                            inXML += "<Connect2>" + ((DropDownList)dgi.FindControl("lstTelnet")).SelectedValue + "</Connect2>";
                        else
                            inXML += "<Connect2>-1</Connect2>";
                        //FB 1422 - End
                        inXML += "    </Endpoint>";
                    }
                    inXML += guestRoomStr; //FB 2426

                    //Response.Write(dgUsers.Visible);
                    //  if (dgUsers.Visible.Equals(true))
                    foreach (DataGridItem dgi in dgUsers.Items)
                    {
                        //Response.Write("<br>cell: " + dgi.Cells[0].Text.Trim());
                        String epID = "";
                        if (dgi.Cells[0].Text.Trim().IndexOf("new") >= 0)
                            foreach (XmlNode node in nodes)
                            {
                                //Response.Write("<br>" + ((Label)dgi.FindControl("lblUserName")).Text + " : " + node.SelectSingleNode("partyEmail").InnerText.Trim());
                                if (((Label)dgi.FindControl("lblUserName")).Text.Trim().ToUpper().IndexOf(node.SelectSingleNode("partyEmail").InnerText.Trim().ToUpper()) >= 0)
                                {
                                    //Response.Write("in if");
                                    epID = node.SelectSingleNode("partyID").InnerText.Trim();
                                }
                            }
                        else
                            epID = dgi.Cells[0].Text;
                        //Response.Write("<br>" + epID);
                        //Response.End();
                        //foreach (DataGridItem dgi in dgUsers.Items)
                        {
                            flag = false;
                            String UserID = dgi.Cells[0].Text.ToString();
                            if (UserID.Trim().IndexOf("new") >= 0)
                                UserID = "new";
                            log.Trace("UserID: " + UserID);
                            for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length ; i++) //FB 1888
                            {
                                if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1") && txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim().Equals(UserID)) //FB 1888
                                    flag = true;
                            }//fogbugz case 407
                            if (flag.Equals(true))
                            {

                                inXML += "    <Endpoint>";
                                inXML += "          <Type>U</Type>";
                                inXML += "          <ID>" + epID + "</ID>";
                                String temp = "0";
                                if (((CheckBox)dgi.FindControl("chkUseDefault")).Checked)
                                    temp = "1";
                                inXML += "          <UseDefault>" + temp + "</UseDefault>";

                                if (chkLectureMode.Checked && (lstEndpoints.SelectedIndex).Equals(dgi.ItemIndex + 1))
                                    inXML += "      <IsLecturer>1</IsLecturer>";
                                else
                                    inXML += "      <IsLecturer>0</IsLecturer>";
                                inXML += "          <EndpointID></EndpointID>";
                                inXML += "          <ProfileID></ProfileID>";
								//FB 2359 Start
                                if (((HtmlTable)dgi.FindControl("tbMCUandConn")).Style.Value == "display:none;" && dgRooms.Items.Count > 0)
                                {
                                    ((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue = "2";
                                    inXML += "          <BridgeID>" + ((DropDownList)dgRooms.Items[0].Cells[0].FindControl("lstBridges")).SelectedValue + "</BridgeID>";
                                }
                                else
                                    inXML += "          <BridgeID>" + ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue + "</BridgeID>";
								//FB 2359 End
                                inXML += "          <BridgeProfileID>" + ((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedValue + "</BridgeProfileID>";//FB 2839

                                inXML += "          <AddressType>" + ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue + "</AddressType>";
                                /****** Code addedd for audio addon ***** */
                                if (((DropDownList)dgi.FindControl("lstConnection")).SelectedValue == "1" && lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo))   //Audio Add On //Code changed for FB 1744
                                {
                                    TextBox confCode = (TextBox)dgi.FindControl("txtConfCode");
                                    TextBox leaderPin = (TextBox)dgi.FindControl("txtleaderPin");
                                    TextBox txtaddress = (TextBox)dgi.FindControl("txtAddress");
                                    String add = "";
                                    if (confCode.Text.Trim() != "" || leaderPin.Text.Trim() != "")
                                        add = txtaddress.Text.Trim() + "D" + confCode.Text + "+" + leaderPin.Text;
                                    else
                                        add = txtaddress.Text.Trim();

                                    inXML += "         <Address>" + add + "</Address>";
                                }
                                else
                                {
                                    inXML += "         <Address>" + obj.ControlConformityCheck(((TextBox)dgi.FindControl("txtAddress")).Text.Trim()) + "</Address>"; // ZD 100263
                                }
                                /****** Code addedd for audio addon ***** */
                                inXML += "          <VideoEquipment>" + ((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedValue + "</VideoEquipment>";
                                inXML += "          <connectionType>" + ((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue + "</connectionType>";
                                inXML += "          <Bandwidth>" + ((DropDownList)dgi.FindControl("lstLineRate")).SelectedValue + "</Bandwidth>";
                                if (((CheckBox)dgi.FindControl("chkIsOutside")).Checked)
                                    inXML += "          <IsOutside>1</IsOutside>";
                                else
                                    inXML += "          <IsOutside>0</IsOutside>";

                                if (((DropDownList)dgi.FindControl("lstProtocol")).SelectedValue == "-1")//FB 1736
                                    ((DropDownList)dgi.FindControl("lstProtocol")).SelectedValue = lstRestrictNWAccess.SelectedValue;

                                inXML += "          <DefaultProtocol>" + ((DropDownList)dgi.FindControl("lstProtocol")).SelectedValue + "</DefaultProtocol>";
                                inXML += "          <Connection>" + ((DropDownList)dgi.FindControl("lstConnection")).SelectedValue + "</Connection>";
                                inXML += "          <URL>" + obj.ControlConformityCheck(((TextBox)dgi.FindControl("txtEndpointURL")).Text) + "</URL>"; // ZD 100263
                                inXML += "          <ExchangeID>" + obj.ControlConformityCheck(((TextBox)dgi.FindControl("txtExchangeID")).Text) + "</ExchangeID>"; //Cisco Telepresence fix ZD 100263
                                //API Port Starts..
                                if(((TextBox)dgi.FindControl("txtApiportno")).Text != "")
                                inXML += "          <APIPortNo>" + obj.ControlConformityCheck(((TextBox)dgi.FindControl("txtApiportno")).Text) + "</APIPortNo>"; // ZD 100263
                                else
                                inXML += "          <APIPortNo>23</APIPortNo>";
                                //API Port Ends..
                                //FB 1422 - Start  
                                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                                    inXML += "<Connect2>" + ((DropDownList)dgi.FindControl("lstTelnetUsers")).SelectedValue + "</Connect2>";
                                else
                                    inXML += "<Connect2>-1</Connect2>";
                                //FB 1422 - End
                                inXML += "    </Endpoint>";
                            }
                        }
                    }
                    inXML += "  </Endpoints>";
                    inXML += "</SetAdvancedAVSettings>";
                }
                log.Trace("SetAdvancedAVSettings: " + inXML);

                String outXML = obj.CallMyVRMServer("SetAdvancedAVSettings", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        #region SetWorkOrders

        protected bool SetWorkOrders()
        {
            try
            {
                //FB 2274 Starts
                if (hdnCrossroomModule != null && hdnCrossroomModule.Value != "")
                    roomModule = hdnCrossroomModule.Value;
                else if (Session["roomModule"] != null)
                    roomModule = Session["roomModule"].ToString();

                if (hdnCrosshkModule != null && hdnCrosshkModule.Value != "")
                    hkModule = hdnCrosshkModule.Value;
                else if (Session["hkModule"] != null)
                    hkModule = Session["hkModule"].ToString();

                if (hdnCrossfoodModule != null && hdnCrossfoodModule.Value != "")
                    foodModule = hdnCrossfoodModule.Value;
                else if (Session["foodModule"] != null)
                    foodModule = Session["foodModule"].ToString();
                //FB 2274 Ends
                if (foodModule.Equals("0") && roomModule.Equals("0") && hkModule.Equals("0")) //organization module FB 1725 //FB 2274
                    return true;
                string inXML = "";
                inXML += "<SetConferenceWorkOrders>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "      <user>";
                inXML += "          <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "      </user>";
                inXML += "  <confInfo>";
                inXML += "      <IsRecur>" + ((Recur.Value.ToString() != "") ? "1" : Recur.Value.ToString()) +"</IsRecur>";//Code added for wo bug reccurence
                if (Request.QueryString["t"] != null)
                    inXML += "      <IsRecurEdit>" + ((Request.QueryString["t"].ToString() == "") ? "1" : "") + "</IsRecurEdit>";//Code added for wo bug reccurence
                inXML += "      <ConfID>" + lblConfID.Text + "</ConfID>";
                inXML += "      <WorkOrderList>";
                foreach (DataGridItem item in AVMainGrid.Items)
                {
                    if (item.Cells[0].Text != "")
                    {
                        inXML += "          <WorkOrder>";
                        inXML += "              <Type>1</Type>";
                        //inXML += "              <UID>" + item.Cells[0].Text.ToString() + "</UID>";
                        inXML += "              <ID>" + item.Cells[0].Text.ToString() + "</ID>";
                        inXML += "              <Name>" + item.Cells[1].Text.ToString() + "</Name>";
                        inXML += "              <RoomID>" + item.Cells[9].Text.ToString() + "</RoomID>";
                        inXML += "              <RoomLayout></RoomLayout>";
                        inXML += "              <SetID>" + item.Cells[17].Text.ToString() + "</SetID>";
                        inXML += "              <AdminID>" + item.Cells[2].Text.ToString() + "</AdminID>";
                        //Code change by Offshore for fb Issue 1073 -Start
                        //inXML += "              <StartByDate>" + item.Cells[4].Text.ToString() + "</StartByDate>";
                        inXML += "              <StartByDate>" + DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(item.Cells[4].Text.ToString())).ToString("MM/dd/yyyy") + "</StartByDate>";
                        //Code Changed by offshore for fb issue 1073 - end
                        inXML += "              <StartByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(item.Cells[5].Text).ToString() + "</StartByTime>"; //FB 2588
                        //Code change by Offshore for fb Issue 1073 -Start
                        //inXML += "              <CompletedByDate>" + item.Cells[6].Text.ToString() + "</CompletedByDate>";
                        inXML += "              <CompletedByDate>" + DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(item.Cells[6].Text.ToString())).ToString("MM/dd/yyyy") + "</CompletedByDate>";
                        //Code Changed by offshore for fb issue 1073 - end
                        inXML += "              <Timezone>" + item.Cells[21].Text + "</Timezone>";
                        inXML += "              <CompletedByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(item.Cells[7].Text).ToString() + "</CompletedByTime>";
                        if (item.Cells[8].Text.IndexOf("Pending") >= 0)
                            inXML += "              <Status>0</Status>";
                        else
                            inXML += "              <Status>1</Status>";
                        inXML += "              <DeliveryType>" + item.Cells[20].Text.ToString() + "</DeliveryType>";
                        //FB 1830 - Starts
                        String tempString = "";
                        tempString = item.Cells[14].Text.ToString();
                        if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        {
                            decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                            tempString = tmpVal.ToString();
                        }
                        inXML += "              <DeliveryCost>" + tempString + "</DeliveryCost>";

                        tempString = item.Cells[13].Text.ToString();
                        if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        {
                            decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                            tempString = tmpVal.ToString();
                        }
                        inXML += "              <ServiceCharge>" + tempString + "</ServiceCharge>";
                        inXML += "              <Comments>" + item.Cells[11].Text.Trim().ToString() + "</Comments>";
                        inXML += "              <Description>" + item.Cells[20].Text.Trim().ToString() + "</Description>";
                        
                        tempString = item.Cells[15].Text.ToString();
                        if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        {
                            decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                            tempString = tmpVal.ToString();
                        }
                       
						inXML += "              <TotalCost>" + tempString + "</TotalCost>";
						//FB 1830 - End
                        inXML += "              <Notify>" + item.Cells[19].Text.ToString() + "</Notify>";
                        inXML += "              <Reminder>1</Reminder>";
                        inXML += "              <ItemList>";
                        string tb = ""; // new Label();
                        tb = item.Cells[18].Text; //.FindControl("txtAVQuantity");
                        string iID;
                        string iQuantity;
                        string uID;
                        //                    Response.Write(tb);
                        //                    Response.End();
                        if (tb != "")
                            for (int i = 0; i < tb.Split(';').Length - 1; i++)
                            {   //FB 1830 - Starts
                                uID = tb.Split(';')[i].Split('�')[1]; //(�- Alt 147)
                                iID = tb.Split(';')[i].Split('�')[0];
                                //Response.Write("<br>" + tb.Split(';')[i]);
                                iQuantity = tb.Split(';')[i].Split('�')[2];
                                if (uID.Trim().Equals(""))
                                    uID = "0";
                                inXML += "<Item>";
                                inXML += "  <UID>" + uID + "</UID>";
                                inXML += "  <ID>" + iID + "</ID>";
                                inXML += "  <Quantity>" + iQuantity + "</Quantity>";
                              
                                tempString = tb.Split(';')[i].Split('�')[4];
                                if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                                {
                                    decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                                    tempString = tmpVal.ToString();
                                }
                                inXML += "  <ServiceCharge>" + tempString + "</ServiceCharge>";
                                inXML += "  <DeliveryType>" + tb.Split(';')[i].Split('�')[5] + "</DeliveryType>";
                                
                                tempString = tb.Split(';')[i].Split('�')[3];
                                if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                                {
                                    decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                                    tempString = tmpVal.ToString();
                                }
                                inXML += "  <DeliveryCost>" + tempString + "</DeliveryCost>";
                                //FB 1830 - End
								inXML += "</Item>";
                            }
                        inXML += "              </ItemList>";
                        inXML += "          </WorkOrder>";
                    }
                }
                //foreach (DataGridItem item in CATMainGrid.Items)
                //{
                //    if (item.Cells[0].Text != "")
                //    {
                //        inXML += "          <WorkOrder>";
                //        inXML += "              <Type>2</Type>";
                //        //inXML += "              <UID>" + item.Cells[0].Text.ToString() + "</UID>";
                //        inXML += "              <ID>" + item.Cells[0].Text.ToString() + "</ID>";
                //        inXML += "              <Name>" + item.Cells[1].Text.ToString() + "</Name>";
                //        inXML += "              <RoomID>" + item.Cells[9].Text.ToString() + "</RoomID>";
                //        inXML += "              <RoomLayout></RoomLayout>";
                //        inXML += "              <StartByDate>" + item.Cells[4].Text.ToString() + "</StartByDate>";
                //        inXML += "              <StartByTime>" + item.Cells[5].Text.ToString() + "</StartByTime>";
                //        inXML += "              <CompletedByDate>" + item.Cells[6].Text.ToString() + "</CompletedByDate>";
                //        inXML += "              <Timezone>" + lstConferenceTZ.SelectedValue + "</Timezone>";
                //        inXML += "              <CompletedByTime>" + item.Cells[7].Text.ToString() + "</CompletedByTime>";
                //        if (item.Cells[8].Text.IndexOf("Pending") >= 0)
                //            inXML += "              <Status>0</Status>";
                //        else
                //            inXML += "              <Status>1</Status>";
                //        inXML += "              <DeliveryType>1</DeliveryType>";
                //        inXML += "              <DeliveryCost>0</DeliveryCost>";
                //        inXML += "              <ServiceCharge>0</ServiceCharge>";
                //        inXML += "              <Description></Description>";
                //        inXML += "              <TotalCost>" + item.Cells[12].Text.ToString() + "</TotalCost>";
                //        inXML += "              <SetID>" + item.Cells[14].Text.ToString() + "</SetID>";
                //        inXML += "              <AdminID>" + item.Cells[2].Text.ToString() + "</AdminID>";
                //        if (item.Cells[8].Text.ToString().IndexOf("Pending") >= 0)
                //            inXML += "              <Status>0</Status>";
                //        else
                //            inXML += "              <Status>1</Status>";
                //        inXML += "              <Comments></Comments>";
                //        inXML += "              <Notify>" + item.Cells[16].Text.ToString() + "</Notify>";
                //        inXML += "              <Reminder>1</Reminder>";
                //        inXML += "          <ItemList>";
                //        string tb = ""; // new Label();
                //        tb = item.Cells[15].Text; //.FindControl("txtReqQuantity");
                //        string iID;
                //        string iQuantity;
                //        string uID;
                //        for (int i = 0; i < tb.Split(';').Length - 1; i++)
                //        {
                //            uID = tb.Split(';')[i].Split(',')[1];
                //            iID = tb.Split(';')[i].Split(',')[0];
                //            iQuantity = tb.Split(';')[i].Split(',')[2];
                //            if (uID.Trim().Equals(""))
                //                uID = "0";
                //            inXML += "              <Item>";
                //            inXML += "                  <UID>" + uID + "</UID>";
                //            inXML += "                  <ID>" + iID + "</ID>";
                //            inXML += "                  <Quantity>" + iQuantity + "</Quantity>";
                //            inXML += "  <ServiceCharge>0</ServiceCharge>";
                //            inXML += "  <DeliveryType>1</DeliveryType>";
                //            inXML += "  <DeliveryCost>0</DeliveryCost>";
                //            inXML += "              </Item>";
                //        }
                //        inXML += "              </ItemList>";
                //        inXML += "          </WorkOrder>";
                //    }
                //}
                if (CheckHKWorkOrderForRoomID(HKDefaultRoomID.Text, HKDefaultSetID.Text) && (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.Wustl)))
                {
                    AddDefaultHKWorkOrder(HKDefaultRoomID.Text, HKDefaultSetID.Text, HKDefaultQuantity.Text);
                }
                foreach (DataGridItem item in HKMainGrid.Items)
                {
                    if (item.Cells[0].Text != "")
                    {
                        inXML += "          <WorkOrder>";
                        inXML += "              <Type>3</Type>";
                        //inXML += "              <UID>" + item.Cells[0].Text.ToString() + "</UID>";
                        inXML += "              <ID>" + item.Cells[0].Text.ToString() + "</ID>";
                        inXML += "              <Name>" + item.Cells[1].Text.ToString() + "</Name>";
                        inXML += "              <RoomID>" + item.Cells[9].Text.ToString() + "</RoomID>";
                        if (item.Cells[17].Text != "Please select one...") // FB 2079
                            inXML += "              <RoomLayout>" + item.Cells[17].Text + "</RoomLayout>"; //FB 2011 //FB 2079
                        else
                            inXML += "              <RoomLayout></RoomLayout>"; //FB 2011
                        //Code change by Offshore for fb Issue 1073 -Start
                        // inXML += "              <StartByDate>" + item.Cells[4].Text.ToString() + "</StartByDate>";
                        inXML += "              <StartByDate>" + DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(item.Cells[4].Text.ToString())).ToString("MM/dd/yyyy") + "</StartByDate>";
                        //Code Changed by offshore for fb issue 1073 - end
                        inXML += "              <StartByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(item.Cells[5].Text).ToString() + "</StartByTime>"; //FB 2588
                        //Code change by Offshore for fb Issue 1073 -Start 
                        //inXML += "              <CompletedByDate>" + item.Cells[6].Text.ToString() + "</CompletedByDate>";
                        inXML += "              <CompletedByDate>" + DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(item.Cells[6].Text.ToString())).ToString("MM/dd/yyyy") + "</CompletedByDate>";
                        //Code Changed by offshore for fb issue 1073 - end
                        inXML += "              <Timezone>" + lstConferenceTZ.SelectedValue + "</Timezone>";
                        inXML += "              <CompletedByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(item.Cells[7].Text).ToString() + "</CompletedByTime>"; //FB 2588
                        if (item.Cells[8].Text.IndexOf("Pending") >= 0)
                            inXML += "              <Status>0</Status>";
                        else
                            inXML += "              <Status>1</Status>";
                        inXML += "              <DeliveryType>1</DeliveryType>";
                        inXML += "              <DeliveryCost>0</DeliveryCost>";
                        inXML += "              <ServiceCharge>0</ServiceCharge>";
                        inXML += "              <Description></Description>";
                        //FB 1830
                        String tempString = item.Cells[12].Text.ToString();
                        if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        {
                            decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                            tempString = tmpVal.ToString();
                        }

                        inXML += "              <TotalCost>" + tempString + "</TotalCost>";
                        inXML += "              <SetID>" + item.Cells[14].Text.ToString() + "</SetID>";
                        inXML += "              <AdminID>" + item.Cells[2].Text.ToString() + "</AdminID>";
                        if (item.Cells[8].Text.ToString().IndexOf("Pending") >= 0)
                            inXML += "              <Status>0</Status>";
                        else
                            inXML += "              <Status>1</Status>";
                        inXML += "              <Comments></Comments>";
                        inXML += "              <Notify>" + item.Cells[16].Text.ToString() + "</Notify>";
                        inXML += "              <Reminder>1</Reminder>";
                        inXML += "          <ItemList>";

                        string tb = ""; // new Label();
                        tb = item.Cells[15].Text; //.FindControl("txtReqQuantity");
                        string iID;
                        string iQuantity;
                        string uID;
                        for (int i = 0; i < tb.Split(';').Length - 1; i++)
                        {
                            uID = tb.Split(';')[i].Split(',')[1];
                            iID = tb.Split(';')[i].Split(',')[0];
                            iQuantity = tb.Split(';')[i].Split(',')[2];
                            if (uID.Trim().Equals(""))
                                uID = "0";
                            inXML += "              <Item>";
                            inXML += "                  <UID>" + uID + "</UID>";
                            inXML += "                  <ID>" + iID + "</ID>";
                            inXML += "                  <Quantity>" + iQuantity + "</Quantity>";
                            inXML += "  <ServiceCharge>0</ServiceCharge>";
                            inXML += "  <DeliveryType>1</DeliveryType>";
                            inXML += "  <DeliveryCost>0</DeliveryCost>";
                            inXML += "              </Item>";
                        }
                        inXML += "              </ItemList>";
                        inXML += "          </WorkOrder>";
                    }
                }

                inXML += "      </WorkOrderList>";

                inXML += "  </confInfo>";
                inXML += "</SetConferenceWorkOrders>";
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                string outXML = obj.CallMyVRMServer("SetConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //"<error><level></level><message>user forced error</message></error>"; 
                //Response.Write("<br>" + obj.Transfer(outXML));

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    return false;
                }
                else
                {
                    inXML = GenerateProviderWorkorderInXML();
                    outXML = obj.CallMyVRMServer("SetProviderWorkorderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        return false;
                    }
                    else
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        return true;
                    }
                }

                //return true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("SubmitWO" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "SubmitWO: " + ex.StackTrace;
                return false;
            }
        }

        #endregion

        #region GenerateProviderWorkorderInXML

        protected String GenerateProviderWorkorderInXML()
        {
            try
            {
                string DateTIme, Date, Time;
                
                String inXML = "";
                inXML += "<SetProviderWorkorderDetails>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <Type>2</Type>";
                inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                inXML += "  <Workorders>";
                foreach (DataGridItem dgi in CATMainGrid.Items)
                {
                    DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                    Date = DateTIme.Split(' ')[0];
                    if (Session["timeFormat"].ToString().Equals("2"))//FB 2281
                        Time = DateTIme.Split(' ')[1];
                    else
                        Time = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(DateTIme)).ToString(tformat);

                    inXML += "    <Workorder>";
                    inXML += "      <ID>" + dgi.Cells[0].Text + "</ID>";
                    inXML += "      <Name>" + ConferenceName.Text + "_CAT_" + (dgi.ItemIndex + 1) + "</Name>";
                    inXML += "      <CateringService>" + dgi.Cells[3].Text + "</CateringService>";
                    inXML += "      <RoomID>" + dgi.Cells[2].Text + "</RoomID>";
                    //Code changed by offshore for FB Issue 1073 -- start
                    //inXML += "      <DeliverByDate>" + DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("MM/dd/yyyy") + "</DeliverByDate>";
                    //inXML += "      <DeliverByTime>" + DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("hh:mm tt") + "</DeliverByTime>";
                    inXML += "      <DeliverByDate>" + myVRMNet.NETFunctions.GetDefaultDate(Date) + "</DeliverByDate>"; //FB 2588 //FB 2281
                    inXML += "      <DeliverByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(Time) + "</DeliverByTime>";
                    //Code changed by offshore for FB Issue 1073 -- end
					//FB 1830
                    if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        String tempString = ((Label)dgi.FindControl("lblPrice")).Text;
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();

                        inXML += "      <Price>" + tempString + "</Price>";
                    }
                    else
                        inXML += "      <Price>" + ((Label)dgi.FindControl("lblPrice")).Text + "</Price>";

                    inXML += "      <Comments>" + ((Label)dgi.FindControl("lblComments")).Text + "</Comments>";
                    inXML += "      <MenuList>";
                    DataGrid dgMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                    foreach (DataGridItem dgiMenu in dgMenus.Items)
                    {
                        inXML += "        <Menu>";
                        inXML += "          <ID>" + dgiMenu.Cells[0].Text + "</ID>";
                        inXML += "          <Quantity>" + dgiMenu.Cells[2].Text + "</Quantity>";
                        inXML += "        </Menu>";
                    }
                    inXML += "      </MenuList>";
                    inXML += "    </Workorder>";
                }
                inXML += "  </Workorders>";
                inXML += "</SetProviderWorkorderDetails>";
                log.Trace("SetProviderWorkorders: " + inXML);
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }

        #endregion

        #region CheckWorkorderRooms

        protected bool CheckWorkorderRooms(ref String roomsName)
        {
            try
            {
                SyncRoomSelection();
                Boolean flagAV = true;
                Boolean flagCAT = true;
                Boolean flagHK = true;

                if (AVMainGrid.Items.Count.Equals(0) && CATMainGrid.Items.Count.Equals(0) && HKMainGrid.Items.Count.Equals(0))
                    flag = true;

                foreach (DataGridItem dgi in AVMainGrid.Items)
                {
                    flagAV = false;
                    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    {
                        if (tn.Depth.Equals(3))
                            if (dgi.Cells[9].Text.Equals(tn.Value))
                                flagAV = true;
                    }
                    if (flagAV.Equals(false))
                        roomsName += "<br><u>" + dgi.Cells[1].Text + "</u> <u>" + dgi.Cells[10].Text + "</u>, ";
                }
                foreach (DataGridItem dgi in CATMainGrid.Items)
                {
                    flagCAT = false;
                    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    {
                        //Response.Write(tn.Value + " : " + dgi.Cells[2].Text);
                        if (tn.Depth.Equals(3))
                            if (dgi.Cells[2].Text.Equals(tn.Value))
                                flagCAT = true;
                    }
                    //Response.Write(flagCAT);
                    if (flagCAT.Equals(false))
                        roomsName += "<br><u>" + dgi.Cells[1].Text + "</u> <u>" + dgi.Cells[10].Text + "</u>, ";
                }
                foreach (DataGridItem dgi in HKMainGrid.Items)
                {
                    flagHK = false;
                    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    {
                        if (tn.Depth.Equals(3))
                            if (dgi.Cells[9].Text.Equals(tn.Value))
                                flagHK = true;
                    }
                    if (flagHK.Equals(false))
                        roomsName += "<br><u>" + dgi.Cells[1].Text + "</u> <u>" + dgi.Cells[10].Text + "</u>, ";
                }
                if (roomsName.Length > 0)
                    roomsName = roomsName.Substring(0, roomsName.Length - 2);

                if (flagAV.Equals(false) || flagCAT.Equals(false) || flagHK.Equals(false))
                    flag = false;
                else
                    flag = true;
                //Response.Write(flagAV + " : " + flagCAT + " : " + flagHK + " : " + flag);

                return flag;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        #endregion

        #region CheckHKWorkOrderForRoomID

        protected bool CheckHKWorkOrderForRoomID(String HKRoomID, String HKSetID)
        {
            try
            {
                if (selectedloc.Value.IndexOf(HKRoomID + ",") >= 0)
                {
                    foreach (DataGridItem dgi in HKMainGrid.Items)
                        if ((dgi.Cells[7].Equals(HKRoomID)) && (dgi.Cells[8].Text.Equals(HKSetID)))
                            return false;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        #region AddDefaultHKWorkOrder

        protected void AddDefaultHKWorkOrder(String HKRoomID, String HKSetID, String HKQuantity)
        {
            DataTable dt = (DataTable)HKMainGrid.DataSource;
            if (dt == null)
                dt = new DataTable();

            if (dt.Columns.Contains("ID").Equals(false))
            {
                dt.Columns.Add("ID");
            }
            if (dt.Columns.Contains("Name").Equals(false))
            {
                dt.Columns.Add("Name");
            }
            if (dt.Columns.Contains("RoomLayout").Equals(false))
            {
                dt.Columns.Add("RoomLayout");
            }
            if (dt.Columns.Contains("Name").Equals(false))
            {
                dt.Columns.Add("Name");
            }
            if (dt.Columns.Contains("AssignedToName").Equals(false))
            {
                dt.Columns.Add("AssignedToName");
            }
            if (dt.Columns.Contains("AssignedToID").Equals(false))
            {
                dt.Columns.Add("AssignedToID");
            }
            if (dt.Columns.Contains("CompletedByDate").Equals(false))
            {
                dt.Columns.Add("CompletedByDate");
            }
            if (dt.Columns.Contains("CompletedByTime").Equals(false))
            {
                dt.Columns.Add("CompletedByTime");
            }
            if (dt.Columns.Contains("Status").Equals(false))
            {
                dt.Columns.Add("Status");
            }
            if (dt.Columns.Contains("RoomID").Equals(false))
            {
                dt.Columns.Add("RoomID");
            }
            if (dt.Columns.Contains("SetID").Equals(false))
            {
                dt.Columns.Add("SetID");
            }
            if (dt.Columns.Contains("RoomName").Equals(false))
            {
                dt.Columns.Add("RoomName");
            }
            if (dt.Columns.Contains("Comments").Equals(false))
            {
                dt.Columns.Add("Comments");
            }
            if (dt.Columns.Contains("Notify").Equals(false))
            {
                dt.Columns.Add("Notify");
            }
            if (dt.Columns.Contains("Reminder").Equals(false))
            {
                dt.Columns.Add("Reminder");
            }
            if (dt.Columns.Contains("ReqQuantity").Equals(false))
            {
                dt.Columns.Add("ReqQuantity");
            }
            DataRow dr = dt.NewRow();
            dr["ID"] = "new";
            dr["Name"] = ConferenceName.Text + "_HK_Default";

            string inXML = "<login><userID>" + Session["userID"].ToString() + "</userID><ID>" + HKSetID + "</ID>";
            inXML += "  <Type>3</Type>";

            int durationMin = 15;
            //Code changed by Offshore for FB Issue 1073 -- Start
            //inXML += "  <ConfDate>" + confStartDate.Text + "</ConfDate>";
            inXML += "  <ConfDate>" + myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + "</ConfDate>";
            //Code changed by Offshore for FB Issue 1073 -- end
            inXML += "  <ConfTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text) + "</ConfTime>";
            inXML += "  <ConfDuration>" + durationMin.ToString() + "</ConfDuration>";
            inXML += "  <ConfTimezone></ConfTimezone>";
            inXML += "</login>";
            //Response.Write(obj.Transfer(inXML));
            //Response.End();                

            string outXML = obj.CallMyVRMServer("GetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            XmlDocument xmldoc = new XmlDocument();
            //Response.Write(obj.Transfer(outXML));
            xmldoc.LoadXml(outXML);
            dr["AssignedToName"] = "";
            dr["AssignedToID"] = xmldoc.SelectSingleNode("//Inventory/Admin/ID").InnerText;
            //Code changed by Offshore for FB Issue 1073 -- Start
            //dr["CompletedByDate"] = confStartDate.Text;
            dr["CompletedByDate"] = myVRMNet.NETFunctions.GetFormattedDate(confStartDate.Text);
            //Code changed by Offshore for FB Issue 1073 -- end
            dr["CompletedByTime"] = myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text);
            dr["Status"] = "Pending";
            dr["RoomID"] = HKRoomID;
            dr["SetID"] = HKSetID;
            dr["RoomName"] = "Default Room";
            dr["Comments"] = "Default WorkOrder.";
            dr["Notify"] = "1";
            dr["Reminder"] = "0";
            dr["RoomLayout"] = "";
            string tb = "";
            XmlNodeList nodes = xmldoc.SelectNodes("//Inventory/ItemList/Item");
            foreach (XmlNode node in nodes)
            {
                tb += node.SelectSingleNode("ID").InnerText + "," + HKQuantity + ";";
            }
            //            if (tb.Length > 0)
            //                tb = tb.Substring(0, tb.Length - 1);
            dr["ReqQuantity"] = tb;
            dt.Rows.Add(dr);
            //Response.Write("TB: " + tb);
            //Response.Write("<br>dtRows: " + dt.Rows.Count);
            HKMainGrid.DataSource = dt;
            HKMainGrid.DataBind();
            //Response.Write("<br>Grid: " + HKMainGrid.Items.Count);
        }
        
        #endregion

        #region AddInstanceInfo
        //MEthod changed for - FB 2027 SetConference
        protected void AddInstanceInfo(string inXMLSetConference, string outxml)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inXMLSetConference);

                //code added for buffer zone --Start
                string setupDur = "0";
                string tearDur = "0";
                double setupDuration = double.MinValue;
                double teardownDuration = double.MinValue;
                if (xmldoc.SelectSingleNode("//conference/confInfo/setupDuration") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/setupDuration").InnerText != "")
                    {
                        setupDur = xmldoc.SelectSingleNode("//conference/confInfo/setupDuration").InnerText;
                    }
                }
                if (xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration").InnerText != "")
                    {
                        tearDur = xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration").InnerText;
                    }
                }

                Double.TryParse(setupDur, out setupDuration);
                Double.TryParse(tearDur, out teardownDuration);

                //code added for buffer zone --End

                #region Commented Area
                //string inxml = "";

                //inxml += "<getRecurDateList>";
                //inxml += obj.OrgXMLElement();//Organization Module Fixes
                //inxml += "<userID>" + Session["userid"].ToString() + "</userID>";
                //inxml += "<confID>" + xmldoc.SelectSingleNode("//conference/confInfo/confID").InnerXml + "</confID>";
                //inxml += "<appointmentTime>" + xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime").InnerXml + "</appointmentTime>";
                //inxml += "<recurrencePattern>" + xmldoc.SelectSingleNode("//conference/confInfo/recurrencePattern").InnerXml;
                //inxml += "</recurrencePattern>";

                //inxml += "<rooms>";
                //string tz = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/timeZone").InnerText;
                //XmlNodeList nodes = xmldoc.SelectNodes("//conference/confInfo/locationList/selected/level1ID");
                //for (int i = 0; i < nodes.Count; i++)
                //    inxml += "  <roomID>" + nodes[i].InnerText + "</roomID>";
                //inxml += "</rooms>";
                //inxml += "</getRecurDateList>";

                //string outxml = obj.CallCOM("GetRecurDateList", inxml, Application["COM_configPath"].ToString());

                //if (outxml.IndexOf("<error>") >= 0)
                //{
                //    errLabel.Text = obj.ShowErrorMessage(outxml);
                //    errLabel.Visible = true;
                //}
                //else
                //{
                #endregion

                xmldoc = null;
                xmldoc = new XmlDocument(); //FB 2027 SetConference
                xmldoc.LoadXml(outxml);
                XmlNodeList nodes = xmldoc.SelectNodes("//error/dateList/dateTime"); //FB 2027 SetConference
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                //Response.Write(nodes.length);
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                    dv = new DataView(ds.Tables[0]);
                else
                    dv = new DataView();
                dt = dv.Table;

                //Code added fro FB 1073 - Start

                if (!dt.Columns.Contains("formatDate"))
                    dt.Columns.Add("formatDate");

                //Code added for FB 1073 - End

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    switch (dt.Rows[i]["conflict"].ToString())
                    {
                        case "0":
                            dt.Rows[i]["conflict"] = obj.GetTranslatedText("No conflict");
                            break;
                        case "1":
                            dt.Rows[i]["conflict"] = obj.GetTranslatedText("Room(s) not available.");
                            break;
                        case "2":
                            dt.Rows[i]["conflict"] = obj.GetTranslatedText("Outside office hours.");
                            break;
                        default:
                            dt.Rows[i]["conflict"] = "";
                            break;
                    }

                    //Code added fro FB 1073 - Start

                    dt.Rows[i]["formatDate"] = myVRMNet.NETFunctions.GetFormattedDate(dt.Rows[i]["startDate"].ToString());
                    //Code added fro FB 1073 - End
                }
                dgConflict.Visible = true;
                tblConflict.Visible = true;
                dgConflict.DataSource = dv;
                dgConflict.DataBind();

                //code added/changed for buffer zone -- Start

                MetaBuilders.WebControls.ComboBox mb = new MetaBuilders.WebControls.ComboBox();
                MetaBuilders.WebControls.ComboBox mb1 = new MetaBuilders.WebControls.ComboBox();
                MetaBuilders.WebControls.ComboBox mb2 = new MetaBuilders.WebControls.ComboBox();
                MetaBuilders.WebControls.ComboBox mb3 = new MetaBuilders.WebControls.ComboBox();
                Button btnConflict = new Button();
                //DateTime cStartTime, cEndTime;

                foreach (DataGridItem dgi in dgConflict.Items)
                {
                    mb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictStartTime");
                    mb.Items.Clear();
                    obj.BindTimeToListBox(mb, true, true);
                    int sHour = Convert.ToInt16(dgi.Cells[7].Text);
                    int sMin = Convert.ToInt16(dgi.Cells[8].Text);
                    DateTime cStartDate = Convert.ToDateTime(dgi.Cells[11].Text + " " + sHour + ":" + sMin + " " + dgi.Cells[9].Text);
                    /*                        cStartTime = new DateTime(cStartDate.Year, cStartDate.Month, cStartDate.Day, sHour, sMin, 0);
                                            if ((sHour != 12) && (dgi.Cells[7].Text.ToUpper().Equals("PM")))
                                                sHour += 12;
                                            if ((sHour == 12) && (dgi.Cells[7].Text.ToUpper().Equals("AM")))
                                                sHour = 0;
                     * */
                    //Response.Write(cStartDate);
                    mb.Text = cStartDate.ToString(tformat);
                    sMin = Convert.ToInt16(dgi.Cells[10].Text);
                    DateTime cEndDate = cStartDate.AddMinutes(sMin); //new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, sHour, sMin, 0);
                    mb1 = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                    mb2 = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictSetupTime");
                    mb3 = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictTeardownTime");


                    DateTime setupTime = cStartDate.AddMinutes(setupDuration);
                    DateTime teardownTime = cEndDate.AddMinutes(-teardownDuration);

                    mb2.Items.Clear();
                    mb3.Items.Clear();
                    obj.BindTimeToListBox(mb2, true, true);
                    obj.BindTimeToListBox(mb3, true, true);
                    mb2.Text = setupTime.ToString(tformat);
                    mb3.Text = teardownTime.ToString(tformat);
                    //code added/changed for buffer zone -- End

                    mb1.Items.Clear();

                    obj.BindTimeToListBox(mb1, true, true);
                    mb1.Text = cEndDate.ToString(tformat);
                    btnConflict = (Button)dgi.FindControl("btnViewConflict");
                    btnConflict.Text = obj.GetTranslatedText("View Conflict"); //FB 2272
                    btnConflict.Attributes.Add("style", "width:100pt");//FB 2890
                    btnConflict.UseSubmitBehavior = false;
                    btnConflict.CssClass = "altMedium0BlueButtonFormat";//FB 2889
                    //btnConflict.OnClientClick = "javascript:viewconflict('" + dgi.ItemIndex + "','" + mb.Text /*cStartTime.ToString("hh:mm tt")*/ + "','" + dgi.Cells[9].Text + "','" + dgi.Cells[8].Text + "','" + lstConferenceTZ.SelectedValue + "','" + ConferenceName.Text + "','" + lblConfID.Text + "')";
                    btnConflict.OnClientClick = "javascript:return roomconflict('" + dgi.Cells[0].Text + "');"; //FB 1154 //FB 2027 SetConference

                    /* *** Recurrence Fixes to disable the View Conflict button on no Conflict - Start *** */

                    if (dgi.Cells[1].Text == "No conflict") //FB JAPAN
                    {
                        btnConflict.Visible = false;
                    }
                    else
                    {
                        if (!dgi.Cells[1].Text.Contains("Outside")) //FB 2027 SetConference
                            btnConflict.Visible = true;
                        else
                            btnConflict.Visible = false;
                    }

                    /* *** Recurrence Fixes to disable the View Conflict button on no Conflict - End *** */
                }

                //} //FB 2027 Setconference
                //btnConfSubmit.Text = "Set Customized Instances"; //FB 2272
                btnConfSubmit.Text = obj.GetTranslatedText("Set Customized Instances"); //FB 2272
                dgConflict.Focus();
                flag = false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);//"Error reading individual conflict for this conference. Please contact your VRM Administrator.";
                errLabel.Visible = true;
            }
        }

        #endregion

        #region SetInstructionsAV

        protected void SetInstructionsAV(object sender, EventArgs e)
        {
            SyncRoomSelection();
            //FB 2181 - Start
            XmlDocument xmldoc = new XmlDocument();
            int selRoomsCount = 0;
            Boolean isSetAvailable = false;
            if (AVMainGrid.Items.Count <= 0)
            {
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    selRoomsCount++;
                    string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID><roomID>" + tn.Value + "</roomID><Type>1</Type></login>";//Organization Module Fixes
                    string outXML = obj.CallMyVRMServer("GetRoomSets", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("/SetList/Set");
                    if (nodes.Count <= 0)
                        selRoomsCount = -1;
                    else
                        isSetAvailable = true;
                    //if (tn.Depth.Equals(3))
                    //    selRoomsCount++;
                }
                if (isSetAvailable == true)
                {
                    btnAddNewAV.Visible = true;
                    if (AVMainGrid.Items.Count <= 0)
                    {
                        lblAVWOInstructions.Text = obj.GetTranslatedText("There are no Audiovisual work orders associated with this conference.") + "<br>" + obj.GetTranslatedText("To create a new work order, click the button below.");//FB 1830 - Translation // FB 2570
                        AVMainGrid.Visible = false;
                        btnAddNewAV.Enabled = true;
                        btnAddNewAV.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                    }
                    else
                    {
                        lblAVWOInstructions.Text = obj.GetTranslatedText("To add a new work order, click the button below. OR click EDIT to edit an existing work order.");//FB 1830 - Translation
                        AVMainGrid.Visible = true;
                        btnAddNewAV.Enabled = true;
                        btnAddNewAV.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                    }
                }
                else if (selRoomsCount == -1 && isSetAvailable == false)
                {
                    lblAVWOInstructions.Text = obj.GetTranslatedText("There are no Audiovisual work orders associated with the selected Room"); // FB 2570
                    AVMainGrid.Visible = false;
                    btnAddNewAV.Enabled = false;
                    btnAddNewAV.Attributes.Add("Class", "btndisable"); //FB 2664
                }
                else if (selRoomsCount == 0)
                {
                    lblAVWOInstructions.Text = obj.GetTranslatedText("Please select at least one room from 'Select Rooms' tab in order to create a work order.");//FB 1830 - Translation
                    AVMainGrid.Visible = false;
                    btnAddNewAV.Enabled = false;
                    btnAddNewAV.Attributes.Add("Class", "btndisable"); //FB 2664
                }
            }
            else
            {
                AVMainGrid.Visible = true;
                btnAddNewAV.Enabled = true;
                btnAddNewAV.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                lblAVWOInstructions.Text = obj.GetTranslatedText("To add a new work order, click the button below. OR click EDIT to edit an existing work order.");//FB 1830 - Translation
            }
            //FB 2181 - End
        }
        
        #endregion

        #region SetInstructionsCAT

        protected void SetInstructionsCAT(object sender, EventArgs e)
        {
            //Method Changed for FB 2181 - Start
            SyncRoomSelection();
            XmlDocument xmldoc = new XmlDocument();
            int selRoomsCount = 0;
            Boolean isSetAvailable = false;
            if (CATMainGrid.Items.Count <= 0)
            {
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    selRoomsCount++;
                    string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID><roomID>" + tn.Value + "</roomID><Type>2</Type></login>";//Organization Module Fixes
                    string outXML = obj.CallMyVRMServer("GetRoomSets", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("/SetList/Set");
                    if (nodes.Count <= 0)
                        selRoomsCount = -1;
                    else
                        isSetAvailable = true;
                }
                if (isSetAvailable == true)
                {
                    lblCATWOInstructions.Visible = true;
                    if (CATMainGrid.Items.Count <= 0)
                    {
                        lblCATWOInstructions.Text = obj.GetTranslatedText("There are no Catering work orders associated with this conference.") + "<br>" + obj.GetTranslatedText("To create a new work order, click the button below.");//FB 1830 - Translation
                        CATMainGrid.Visible = false;
                        btnAddNewCAT.Enabled = true;
                        btnAddNewCAT.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                    }
                    else
                    {
                        lblCATWOInstructions.Text = obj.GetTranslatedText("To add a new work order, click the button below. OR click EDIT to edit an existing work order.");//FB 1830 - Translation
                        CATMainGrid.Visible = true;
                        btnAddNewCAT.Enabled = true;
                        btnAddNewCAT.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                    }
                }
                else if (selRoomsCount == -1 && isSetAvailable == false)
                {
                    lblCATWOInstructions.Text = obj.GetTranslatedText("There are no Catering work orders associated with the selected Room");
                    CATMainGrid.Visible = false;
                    btnAddNewCAT.Enabled = false;
                    btnAddNewCAT.Attributes.Add("Class", "btndisable"); //FB 2664
                }
                else if (selRoomsCount == 0)
                {
                    lblCATWOInstructions.Text = obj.GetTranslatedText("Please select at least one room from 'Select Rooms' tab in order to create a work order.");//FB 1830 - Translation
                    CATMainGrid.Visible = false;
                    btnAddNewCAT.Enabled = false;
                    btnAddNewCAT.Attributes.Add("Class", "btndisable"); //FB 2664
                    
                }
            }
            //if (CATMainGrid.Items.Count <= 0)
            //{
            //    CATMainGrid.Visible = false;
            //    lblCATWOInstructions.Visible = true;
            //    lblCATWOInstructions.Text = obj.GetTranslatedText("There are no Catering work orders associated with this conference.")+ "<br>" + obj.GetTranslatedText("To create a new work order, click the Add New Work Order button.");//FB 1830 - Translation
            //}
            //else
            //{
            //    CATMainGrid.Visible = true;
            //    lblCATWOInstructions.Visible = true;
            //    lblCATWOInstructions.Text = obj.GetTranslatedText("To add a new work order, click the Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.");//FB 1830 - Translation
            //}
            //Method Changed for FB 2181 - End
        }
        
        #endregion

        #region SetInstructionsHK

        protected void SetInstructionsHK(object sender, EventArgs e)
        {
            SyncRoomSelection();
            //FB 2181 - Start
            XmlDocument xmldoc = new XmlDocument();
            int selRoomsCount = 0;
            Boolean isSetAvailable = false;
            btnAddNewHK.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2214
            if (HKMainGrid.Items.Count <= 0)
            {
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    selRoomsCount++;
                    string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID><roomID>" + tn.Value + "</roomID><Type>3</Type></login>";//Organization Module Fixes
                    string outXML = obj.CallMyVRMServer("GetRoomSets", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("/SetList/Set");
                    if (nodes.Count <= 0)
                        selRoomsCount = -1;
                    else
                        isSetAvailable = true;
                    //if (tn.Depth.Equals(3))
                    //    selRoomsCount++;
                }
                if (isSetAvailable == true)
                {
                    btnAddNewHK.Visible = true;
                    if (HKMainGrid.Items.Count <= 0)
                    {
                        lblHKWOInstructions.Text = obj.GetTranslatedText("There are no Facility work orders associated with this conference.") + "<br>" + obj.GetTranslatedText("To create a new work order, click the button below.");//FB 1830 - Translation FB 2570
                        HKMainGrid.Visible = false;
                        btnAddNewHK.Enabled = true;
                        btnAddNewHK.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                    }
                    else
                    {
                        lblHKWOInstructions.Text = obj.GetTranslatedText("To add a new work order, click the button below. OR click EDIT to edit an existing work order.");//FB 1830 - Translation
                        HKMainGrid.Visible = true;
                        btnAddNewHK.Enabled = true;
                        btnAddNewHK.Attributes.Add("Class", "altLongBlueButtonFormat"); //FB 2664
                    }
                }
                else if (selRoomsCount == -1 && isSetAvailable == false)
                {
                    lblHKWOInstructions.Text = obj.GetTranslatedText("There are no Facility work orders associated with the Selected Room"); //FB 2570
                    HKMainGrid.Visible = false;
                    btnAddNewHK.Enabled = false;
                    btnAddNewHK.Attributes.Add("Class", "btndisable"); //FB 2664
                }
                else if (selRoomsCount == 0)
                {
                    lblHKWOInstructions.Text = obj.GetTranslatedText("Please select at least one room from 'Select Rooms' tab in order to create a work order.");//FB 1830 - Translation
                    HKMainGrid.Visible = false;
                    btnAddNewHK.Enabled = false;
                    btnAddNewHK.Attributes.Add("Class", "btndisable"); //FB 2664

                }
            }
            //FB 2181 - End
        }
        
        #endregion

        #region ShowHidePasswords

        protected void ShowHidePasswords(object sender, EventArgs e)
        {
            try
            {
                //FB 1985 - Start
                if (client.ToUpper() == "DISNEY")
                {
                    if (lstConferenceType.SelectedValue == "2")
                    {
                        //lblAudioNote.Visible = true;
                        //lblAudioNote.Text = "Note: to add an audiobridge to your videoconference, 1) complete the information on this page, ";//FB 2023 start
                        //lblAudioNote.Text += "2) under the Select Participants tab, choose address book and select Domestic Audio Add-on (for example) ";
                        imgAudioNote.Visible = true;
                        imgAudioNote.ToolTip = "Note: to add an audio bridge to your conference, 1) complete the information on this page, 2) under the Select Participants tab, "
                                             + "Choose Add Audio Add-On Bridge button and select the desired audio bridge";
                        imgParNote.ToolTip = obj.GetTranslatedText(imgAudioNote.ToolTip);
                        //lblParNote.Visible = true;
                        //lblParNote.Text = "Note: to complete request to add an audio bridge to your videoconference, choose address book and select Domestic";
                        //lblParNote.Text += " Audio Add-on (for example), providing Conference Code and Leader Pin on the Audio Settings tab";
                        imgParNote.Visible = true;
                        imgParNote.ToolTip = "Note: to complete request to add an audio bridge to your conference, "
                                           + "choose Add Audio Add-On Bridge button and select the desired audio bridge";
                        imgParNote.ToolTip = obj.GetTranslatedText(imgParNote.ToolTip);
                    }
                    else
                    {
                        //lblAudioNote.Visible = false;
                        //lblParNote.Visible = false;
                        imgAudioNote.Visible = false;
                        imgParNote.Visible = false;
                    }
                    TopMenu.Items[3].Text = "<div align='center' style='width:123'><b>Audio</b><br><b>Settings</b></div>";
                    if (enableAV == "0")
                        TopMenu.Items[3].Text = "";
                }
				//FB 2998
                if ((mcuSetupDisplay == "1" || mcuTearDisplay == "1") && (lstConferenceType.SelectedValue == "2" || lstConferenceType.SelectedValue == "6")
                    && enableBufferZone == "1" && !chkStartNow.Checked && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                {
                    MCUConnectRow.Attributes.Add("style", "Display:");
                }
                else
                {
                    MCUConnectRow.Attributes.Add("style", "Display:None");
                    MCUConnectDisplayRow.Attributes.Add("style", "Display:None");
                    chkMCUConnect.Checked = false;
                }

                //FB 1985 - End 
                CheckPasswordFields();
            }
            catch (Exception ex)
            {
                log.Trace(ex.ToString());
            }

        }
        
        #endregion

        #region ExpandTree

        protected void ExpandTree(object sender, EventArgs e)
        {
            if (lblConfID.Text.ToLower().Equals("new") || Request.QueryString["t"].ToString().ToLower().Equals("o") || Request.QueryString["t"].ToString().Trim().Equals(""))//FB 1481
                RefreshRoom(new Object(), new EventArgs());
            if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))// Code added for FB 1425 QA Bug
                MeetingPlanner.Attributes.Add("style", "display:none");// Code added for FB 1425 QA Bug
        }
        
        #endregion

        #region CheckPasswordFields

        protected void CheckPasswordFields()
        {
            CreateBy.Value = lstConferenceType.SelectedValue.ToString();
            //Code Modified For MOJ - Phase 2
            if (lstConferenceType.SelectedValue.Equals("7") || client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
            {
                pnlPassword1.Visible = false;
                pnlPassword2.Visible = false;
                trPwd.Visible = false; //FB 2446

                //if (enableAV.Equals("0")) //For AV Switch
                //{
                    if (this.SelectAudioVisual != null)
                    {
                        TopMenu.Items[3].Text = "";
                        //Wizard1.Views.Remove(this.SelectAudioVisual);
                    }
                    //foreach(WizardStep ws in Wizard1.WizardSteps)
                    //int j = 0;
                    //for (int i = 0; i < Wizard1.WizardSteps.Count; i++)
                    //{
                    //    if (!Wizard1.WizardSteps[i].Title.Equals(""))
                    //    {
                    //        j = j + 1;
                    //        Wizard1.WizardSteps[i].Title = j + ". " + Wizard1.WizardSteps[i].Title.Split('.')[1].Trim();
                    //    }
                    //    else
                    //        j = j;
                    //}
                //}

                    // chkVMR.Checked = false; // FB 2620
                    trVMR.Attributes.Add("style", "display:none;");
                    isVMR.Value = "0";
                    trStartMode.Attributes.Add("style", "display:None");//FB 2501 //FB 2641 
                    trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                    if (hdnConferenceName.Value != "")//FB 2694
                    {
                        ConferenceName.Text = "";
                        hdnConferenceName.Value = "";
                    }
            }
            else if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking)) //FB 2694 Starts 
            {
                lstVMR.SelectedIndex = 0;//FB 2717
                trConfDesc.Visible = false;
                trConfTitle.Visible = false;
                trConfTemp.Visible = false;
                if (ConferenceName.Text != "HotDesking Reservation") //FB 2823 //FB 2986
                    selectedloc.Value = "";
                ConferenceName.Text = "HotDesking Reservation"; 
                pnlPassword1.Visible = false;
                pnlPassword2.Visible = false;
                trStartMode.Style.Add("display", "none");
                trStartMode1.Visible = false;
                trPublic.Visible = false;                
                trServType.Attributes.Add("style", "display:none");
                DrpServiceType.ClearSelection();//FB 2962
                CloudConfRow.Attributes.Add("style", "display:none");
                trVIP.Attributes.Add("style", "display:none");
                trVMR.Attributes.Add("Style", "display:none");//FB 2717
                traudiobrdige.Visible = false;
                trPublicOpen.Attributes.Add("style", "display:none");
                TrCTNumericID.Attributes.Add("style", "display:none");//FB 2870
                trCTSNumericID.Attributes.Add("style", "display:none");// FB 2870
                ShowHideAVforVMR(null, null);
                hdnConferenceName.Value = obj.ControlConformityCheck(ConferenceName.Text); // ZD 100263
                //FB 2694 Ends
            } 
            else
            {
                //Code Modified For MOJ - Phase2 - Start
                if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    trPwd.Visible = false; //FB 2446
                    pnlPassword1.Visible = false;
                    pnlPassword2.Visible = false;
                    //FB 2377
                    if (hdnCrossEnableConfPassword != null && hdnCrossEnableConfPassword.Value != "")
                        EnableConferencePassword = hdnCrossEnableConfPassword.Value;
                    else if (Session["EnableConfPassword"] != null)
                        EnableConferencePassword = Session["EnableConfPassword"].ToString();
                    if (Session["EnableConfPassword"] != null) //FB 2359
                        if (EnableConferencePassword == "1")  //FB 2274
                        {
                            pnlPassword1.Visible = true;
                            pnlPassword2.Visible = true;
                            trPwd.Visible = true; //FB 2446
                        }
                }
                else
                {
                    pnlPassword1.Visible = false;
                    pnlPassword2.Visible = false;
                    trPwd.Visible = false; //FB 2446       
                }
                //Code Modified For MOJ - Phase 2 -End
                if (lstConferenceType.SelectedValue.ToString() != ns_MyVRMNet.vrmConfType.HotDesking)//FB 2823
                {
                    if (hdnConferenceName.Value != "")
                    {
                        ConferenceName.Text = "";
                        hdnConferenceName.Value = "";
                        selectedloc.Value = ""; 
                    }
                }
                if (enableAV.Equals("1")) //For AV Switch
                {
                    WizardStep wsTemp = new WizardStep();
                    //wsTemp.ID = "selectAdvAVSettings";
                    //wsTemp.Title = "4. Audio/Video Settings";
                    //this.Wizard1.WizardSteps.AddAt(3, wsTemp);
                    if (this.SelectAudioVisual != null)
                    {
                        if (client.ToUpper() == "DISNEY") //FB 1985
                            TopMenu.Items[3].Text = "<div align='center' style='width:123'><b>Audio</b><br><b>Settings</b></div>";
                        else
                            TopMenu.Items[3].Text = "<div align='center' style='width:123'onclick='javascript:return SubmitRecurrence();'>" + obj.GetTranslatedText("Audio/Video") + "<br />" + obj.GetTranslatedText("Settings") + "</div>";//FB 1737 //FB 1830 - Translation
                            
                            
                        //this.selectAdvAVSettings.ID = "selectAdvAVSettings";
                        //this.selectAdvAVSettings.Title = ". Audio/Video Settings";
                        //int j = 0;
                        //for (int i = 0; i < Wizard1.WizardSteps.Count; i++)
                        //{
                        //    if (!Wizard1.WizardSteps[i].Title.Equals(""))
                        //    {
                        //        j = j + 1;
                        //        Wizard1.WizardSteps[i].Title = j + ". " + Wizard1.WizardSteps[i].Title.Split('.')[1].Trim();
                        //    }
                        //    else
                        //        j = j;
                        //}
                    }
                    //Code Added For FB 1422 - Hide Commonsettings part in AV Tab only for Point-to-Point Conference - Start
                    if (lstConferenceType.SelectedValue.Equals("4"))
                    {
                        trAVCommonSettings.Attributes.Add("style", "display:none");
                        //FB 2641  start
                            if (EnableLinerate == 1)
                                trp2pLinerate.Attributes.Add("style", "display:block");
                            else
                                trp2pLinerate.Attributes.Add("style", "display:none");
                        //FB 2641 End
                    }
                    else
                    {
                        trp2pLinerate.Attributes.Add("style", "display:none");
                        trAVCommonSettings.Attributes.Add("style", "display:block");
                    }
                    //Code Added For FB 1422 - End

                    /* *** Code added for Audio-addon *** */
                    if (enableAdvAvParams == "0")
                        trAVCommonSettings.Attributes.Add("style", "display:none");
                    /* *** Code added for Audio-addon *** */
                   
                    //FB 2620
                    if (lstVMR.SelectedIndex > 0 && !lstConferenceType.SelectedValue.Equals("4")) //FB 2376
                        trVMR.Attributes.Add("style", "display:");//FB 2717

                    if (lstConferenceType.SelectedValue.Equals("4"))
                    {
                        // chkVMR.Checked = false; FB 2620
                        lstVMR.SelectedValue = "0";
                        trVMR.Attributes.Add("style", "display:None");//FB 2717
                        isVMR.Value = "0";
                    }
                }
                //FB 2501
				// if (lstConferenceType.SelectedValue.Equals("4") || chkVMR.Checked)
                if (lstConferenceType.SelectedValue.Equals("4") || lstVMR.SelectedIndex > 0)// FB 2620
                {
                    trStartMode.Attributes.Add("style", "display:None");//FB 2641 
                    //trStartMode1.Visible = false;
                    trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                }
                else
                {
                    //FB 2641  start
                    if (EnableStartMode == 1)
                    {
                        trStartMode.Attributes.Add("style", "display:");
                        trStartMode1.Attributes.Add("style", "display:");//FB 2717
                    }
                    else
                    {
                        trStartMode.Attributes.Add("style", "display:none");
                        trStartMode1.Attributes.Add("style", "display:None");//FB 2717
                    }
                     //FB 2641 End
                }

                ShowHideAVforVMR(null, null); //FB 2448
            }
        }
        
        #endregion

        #region RecalculateSteps

        protected void RecalculateSteps(Object sender, EventArgs e)
        {
            try
            {
                //Response.Write(this.Wizard1.WizardSteps[Wizard1.ActiveStepIndex + 1].Title); // + " : " + 
                //if (this.Wizard1.WizardSteps[Wizard1.ActiveStepIndex].Title.Equals(""))
                //    this.Wizard1.ActiveStepIndex += 1;

                //if (this.selectAdvAVSettings.Title.Equals(""))
                //{
                //    this.selectAdvAVSettings.ID = "";
                //    this.selectAdvAVSettings.Title = "";
                //}
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
        
        #endregion

        #region CheckQuantity

        protected bool CheckQuantity(DataGrid dg, string txtQuan)
        {
            //WO Bug Fix
            Boolean isLessQty = false;
            try
            {
                foreach (DataGridItem dgi in dg.Items)
                {
                    TextBox temp = new TextBox();
                    temp = (TextBox)dgi.FindControl(txtQuan);
                    if (Convert.ToInt32(temp.Text) > 0)
                        isLessQty = true;
                    else
                    {
                        isLessQty = false;
                        break;
                    }
                }
                return isLessQty;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        #region SendReminderToHost

        protected void SendReminderToHost(Object sender, EventArgs e)
        {
            string woID = txtWorkOrderID.Text;
            //Response.Write(sender.ToString() + " : " + e.ToString());
            try
            {
                string inxml = "<login>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "<userID>" + Session["userID"].ToString() + "</userID>";
                inxml += "<WorkOrderID>" + woID + "</WorkOrderID>";
                inxml += "</login>";
                //Response.Write(obj.Transfer(inxml));
                string outxml = obj.CallMyVRMServer("SendWorkOrderReminder", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outxml.IndexOf("<error>") < 0)
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                else
                    errLabel.Text = obj.ShowErrorMessage(outxml);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
        
        #endregion

        #region UpdateTotalAV

        protected void UpdateTotalAV(Object sender, EventArgs e)
        {
            try
            {
                if (itemsGrid.Visible)
                {
                    Double totalCost = 0;
                    foreach (DataGridItem dgi in itemsGrid.Items)
                    {
                        TextBox txtTemp = new TextBox();
                        txtTemp = (TextBox)dgi.FindControl("txtReqQuantity");
                        int reqQuantity = 0;
                        try
                        {
                            reqQuantity = Convert.ToInt32(txtTemp.Text);
                        }
                        catch (Exception exx)
                        {
                            log.Trace(exx.Message);
                            reqQuantity = 0;
                        }
						//FB 1830
                        if (!((Label)dgi.FindControl("lblPrice")).Text.Trim().Equals("") || !((Label)dgi.FindControl("lblServiceCharge")).Text.Trim().Equals("") || !((Label)dgi.FindControl("lblDeliveryCost")).Text.Trim().Equals(""))
                        {
                            
                            if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)                            
                            {
                                tmpVal = 0;
                                decimal.TryParse(((Label)dgi.FindControl("lblPrice")).Text, NumberStyles.Any, cInfo, out tmpVal);
                                String strPrice = tmpVal.ToString();
                                decimal.TryParse(((Label)dgi.FindControl("lblServiceCharge")).Text, NumberStyles.Any, cInfo, out tmpVal);
                                String strServiceCharge = tmpVal.ToString();
                                decimal.TryParse(((Label)dgi.FindControl("lblDeliveryCost")).Text, NumberStyles.Any, cInfo, out tmpVal);
                                String strDeliveryCost = tmpVal.ToString();

                                totalCost += (Convert.ToDouble(strPrice) + Convert.ToDouble(strServiceCharge) + Convert.ToDouble(strDeliveryCost)) * reqQuantity;
                            }
                            else
                                totalCost += (Convert.ToDouble(((Label)dgi.FindControl("lblPrice")).Text) + Convert.ToDouble(((Label)dgi.FindControl("lblServiceCharge")).Text) + Convert.ToDouble(((Label)dgi.FindControl("lblDeliveryCost")).Text)) * reqQuantity;
                        }
                    }

                    //FB 1830
                    if (txtServiceCharges.Text.Trim().Equals(""))
                        txtServiceCharges.Text = (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound) ? "0,00" : "0.00";  // FB 1686 

                    if (txtDeliveryCost.Text.Trim().Equals(""))
                        txtDeliveryCost.Text = (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound) ? "0,00" : "0.00"; // FB 1686

                    String tempString = txtServiceCharges.Text;
                    if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();
                    }
                    totalCost += Convert.ToDouble(tempString);

                    tempString = txtDeliveryCost.Text;
                    if (cFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();
                    }
                    totalCost += Convert.ToDouble(tempString);

                    Label lblTemp = new Label();
                    DataGridItem dgFooter = (DataGridItem)itemsGrid.Controls[0].Controls[itemsGrid.Controls[0].Controls.Count - 1];
                    lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
                    //lblTemp.Text = Double.Parse(totalCost.ToString()).ToString("00.00");
                    lblTemp.Text = Double.Parse(totalCost.ToString()).ToString("0.00"); //FB 1686
                    //FB 1830
                    tmpVal = 0;
                    decimal.TryParse(lblTemp.Text, out tmpVal);
                    lblTemp.Text = tmpVal.ToString("n", cInfo);

                    AVMainGrid.Visible = false;
                    lblAVWOInstructions.Text = obj.GetTranslatedText("Click Edit/Create button to save these changes.");//FB 1830 - Translation
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
        //protected void UpdateTotalCAT(Object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (itemsGridCAT.Visible)
        //        {
        //            Double totalCost = 0;
        //            foreach (DataGridItem dgi in itemsGridCAT.Items)
        //            {
        //                TextBox txtTemp = new TextBox();
        //                txtTemp = (TextBox)dgi.FindControl("txtReqQuantity");
        //                int reqQuantity = 0;
        //                try
        //                {
        //                    reqQuantity = Convert.ToInt32(txtTemp.Text);
        //                }
        //                catch (Exception exx)
        //                {
        //                    reqQuantity = 0;
        //                }
        //                if (!dgi.Cells[6].Text.Trim().Equals(""))
        //                    totalCost += Convert.ToDouble(dgi.Cells[6].Text) * reqQuantity;
        //            }
        //            Label lblTemp = new Label();
        //            DataGridItem dgFooter = (DataGridItem)itemsGridCAT.Controls[0].Controls[itemsGridCAT.Controls[0].Controls.Count - 1];
        //            lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
        //            lblTemp.Text = Double.Parse(totalCost.ToString()).ToString("00.00");
        //            CATMainGrid.Visible = false;
        //            lblCATWOInstructions.Text = "Click Edit/Create button to save these changes.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace(ex.StackTrace);
        //        errLabel.Visible = true;
        //    }
        //}
        #endregion

        #region UpdateTotalHK

        protected void UpdateTotalHK(Object sender, EventArgs e)
        {
            try
            {
                //if (itemsGridHK.Visible)//FB 1830
                //{
                    Double totalCost = 0;
                    foreach (DataGridItem dgi in itemsGridHK.Items)
                    {
                        TextBox txtTemp = new TextBox();
                        txtTemp = (TextBox)dgi.FindControl("txtReqQuantity");
                        int reqQuantity = 0;
                        try
                        {
                            reqQuantity = Convert.ToInt32(txtTemp.Text);
                        }
                        catch (Exception exx)
                        {
                            log.Trace(exx.Message);
                            reqQuantity = 0;
                        }
                        if (!dgi.Cells[5].Text.Trim().Equals(""))//FB 1830
                        {
                            
                            if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)
                            {
                                decimal.TryParse(dgi.Cells[5].Text, NumberStyles.Any, cInfo, out tmpVal);
                                totalCost += Convert.ToDouble(tmpVal) * reqQuantity;
                            }
                            else
                                totalCost += Convert.ToDouble(dgi.Cells[5].Text) * reqQuantity;
                        }
                    }
                    Label lblTemp = new Label();
                    DataGridItem dgFooter = (DataGridItem)itemsGridHK.Controls[0].Controls[itemsGridHK.Controls[0].Controls.Count - 1];
                    lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
                    //lblTemp.Text = Double.Parse(totalCost.ToString()).ToString("00.00");
                    lblTemp.Text = Double.Parse(totalCost.ToString()).ToString("0.00"); // FB 1686
                    //FB 1830
                    tmpVal = 0;
                    decimal.TryParse(lblTemp.Text, out tmpVal);
                    lblTemp.Text = tmpVal.ToString("n", cInfo);

                    lblHKWOInstructions.Text = obj.GetTranslatedText("Click Edit/Create button to save these changes.");//FB 1830 - Translation
                //}//FB 1830
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
        
        #endregion

        #region GetVideoLayouts

        protected void GetVideoLayouts()
        {
            //FB 2524 Starts
            //if (txtSelectedImage.Text.Trim() == "")
            //{
            //    txtSelectedImage.Text = "01";
            //    if (client.ToUpper() == "DISNEY") 
            //        txtSelectedImage.Text = "02";
            //}
            //FB 2524 Ends

            ImagesPath.Attributes.Add("style", "display:none");
            txtSelectedImage.Attributes.Add("style", "display:none");
            ImageFiles.Attributes.Add("style", "display:none");
            ImageFilesBT.Attributes.Add("style", "display:none");
            ImagesPath.Text = "image/displaylayout/";
            if (client.ToString().ToUpper().Equals("BTBOCES"))
                ImagesPath.Text += "BTBoces/";
            ImageFiles.Text = "";
            //Response.Write(Server.MapPath(ImagesPath.Text));

            foreach (string file in Directory.GetFiles(Server.MapPath(ImagesPath.Text)))
            {
                if (file.ToLower().LastIndexOf(".gif") > 1)
                    ImageFiles.Text += file.ToLower().Replace(Server.MapPath(ImagesPath.Text).ToLower(), "").Replace(".gif", "") + ":";
            }
            if (client.ToString().Trim().ToUpper().Equals("PSU")) //FB Case 524 Saima
            {
                ImageFiles.Text = "01:02:03:05:16:";
                lstRestrictNWAccess.ClearSelection();
                //chkLectureMode.Checked = true;
                lstRestrictNWAccess.Items.FindByValue("2").Selected = true;
                chkDualStreamMode.Checked = true;
                chkEncryption.Checked = false;
                //Remove conference line rate defaults to 768 Kbps
                // Code added for FB Issue 1229
                if (Request.QueryString["t"].ToString() == "n" && chkPolycomSpecific.Checked == false)
                {
                    lstLineRate.ClearSelection();
                    lstLineRate.Items.FindByValue("768").Selected = true;
                }
                //lstLineRate.ClearSelection();
                // maneesh 08/19/2008 FB 735
                //lstLineRate.Items.FindByValue("768").Selected = true;
                lstRestrictNWAccess.Enabled = false;
                lstRestrictUsage.Enabled = false;
                lstVideoCodecs.Enabled = false;
                lstAudioCodecs.Enabled = false;
                // lstLineRate.Enabled = false;
                chkConfOnPort.Enabled = false;
                chkEncryption.Enabled = false;
                chkSingleDialin.Enabled = false;
            }
        }
        
        #endregion

        #region UpdateAdvAVSettings

        protected void UpdateAdvAVSettings(Object sender, EventArgs e)
        {
            try
            {
				//if (chkVMR.Checked) //FB 2448
                if (lstVMR.SelectedIndex > 0 || chkPCConf.Checked) //FB 2448 // FB 2620 //FB 2819
                    return;

                log.Trace("in UpdateAdvAVSettings");
                //btnPrev.Visible = true; FB 2516
                btnNext.Visible = true;
                if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly)) //FB Issue 1229
                {
                    //Response.Write("ConfID" + lblConfID.Text);
                   
                    lstRestrictUsage.ClearSelection();
                    if (lstConferenceType.SelectedValue.Equals("2") || lstConferenceType.SelectedValue.Equals("4"))
                        lstRestrictUsage.Items.FindByValue("2").Selected = true; //FB 1744
                    if (lstConferenceType.SelectedValue.Equals("6"))
                        lstRestrictUsage.Items.FindByValue("1").Selected = true; //FB 1744
                    
                    if (hasVisited.Text.Equals("") || hasVisited.Text.Equals("0"))//FB 1721
                    {
                        lstVideoCodecs.ClearSelection();
                        if (lstVideoCodecs.Items.Count <= 0)
                            obj.BindVideoCodecs(lstVideoCodecs);
                        lstVideoCodecs.Items.FindByValue("0").Selected = true;

                        lstAudioCodecs.ClearSelection();
                        if (lstAudioCodecs.Items.Count <= 0)
                            obj.BindAudioCodecs(lstAudioCodecs);
                        lstAudioCodecs.Items.FindByValue("0").Selected = true;
                        //FB 1985 - Starts
                        if (client.ToUpper() == "DISNEY")
                        {
                            lstRestrictNWAccess.ClearSelection();
                            lstRestrictNWAccess.Items.FindByValue("3").Selected = true;
                        }
                        //FB 1985 - End
                    }
                    if (hasVisited.Text.Equals("") || hasVisited.Text.Equals("0"))//FB Issue 1229
                    {
                        //FB 1985 - Starts
                        //FB 2429 - Starts
                        lstLineRate.ClearSelection();
                        if (lstLineRate.Items.Count <= 0)
                            obj.BindLineRate(lstLineRate);
                        if (lstLineRate.Items.FindByValue(OrgLineRate.ToString()) != null) // FB 2516
                            lstLineRate.Items.FindByValue(OrgLineRate.ToString()).Selected = true;

                        DrpDwnLstRate.ClearSelection();
                        if (DrpDwnLstRate.Items.Count <= 0)
                            obj.BindLineRate(DrpDwnLstRate);
                        if (DrpDwnLstRate.Items.FindByValue(OrgLineRate.ToString()) != null)  //FB 2516
                            DrpDwnLstRate.Items.FindByValue(OrgLineRate.ToString()).Selected = true;

                        if (OrgLineRate <= 0)
                        {
                            lstLineRate.ClearSelection();
                            lstLineRate.Items.FindByValue("384").Selected = true;

                            DrpDwnLstRate.ClearSelection();
                            DrpDwnLstRate.Items.FindByValue("384").Selected = true;
                        }
                        //FB 2429 - End
                        //FB 1985 - End
                    }
                    lstVideoMode.ClearSelection();
                    if (lstVideoMode.Items.Count <= 0)
                        obj.BindVideoMode(lstVideoMode);
                    //lstVideoMode.Items.FindByValue("3").Selected = true;

                    GetVideoLayouts();
                    log.Trace("hasVisited: " + hasVisited.Text);
                    if ((Session["AdvAVParam"] != null)) // && hasVisited.Text.Equals("") && hasVisited.Text.Equals("0"))
                        GetAudioVideoSettings(Session["AdvAVParam"].ToString());

                    lstEndpoints.Items.Clear();
                    lstEndpoints.Items.Add(new ListItem(obj.GetTranslatedText("Auto select..."), "0"));
                    DisplayUserEndpoints();
                    DisplayRoomEndpoints();
                    //Response.Write("----");
                    hasVisited.Text = "1";
                    if (lstEndpoints.Items.Count.Equals(1))
                    {
                        lstEndpoints.Items.Clear();
                        lstEndpoints.Items.Add(new ListItem(obj.GetTranslatedText("No Endpoints selected"), "0"));
                        chkLectureMode.Checked = false;
                    }
                    else
                    {
                        lstEndpoints.ClearSelection();
                        try
                        {
                            lstEndpoints.Items.FindByValue(txtLecturer.Value).Selected = true;
                        }
                        catch (Exception e1)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }

        #endregion

        // FB Case 712 - Firefox does not preserve the viewstate for users datagrid under advanced av settings page
        // this function will  restore the values entered by user which were saved before postback using javascript function
        // called as CheckFiles() > last section.

        //#region PreserveUsersState

        //protected void PreserveUsersState1()
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        dt = CreateDataTable(dt);

        //        String txtUsers = txtdgUsers.Value;
        //        String strUsers = "";
        //        int rows = (txtUsers.Split(';').Length - 1) / 2;
        //        for (int i = 0; i < rows; i++)
        //        {
        //            DataRow dr = dt.NewRow();
        //            dr["ID"] = txtUsers.Split(';')[i].Split(':')[0];
        //            for (int j = 0; j < txtPartysInfo.Text.Split(';').Length - 1; j++)
        //            {
        //                if (txtPartysInfo.Text.Split(';')[j].Split(',')[4].Equals("1") && txtPartysInfo.Text.Split(';')[j].Split(',')[0].Equals(dr["ID"].ToString()))
        //                    strUsers = txtPartysInfo.Text.Split(';')[j];
        //            }

        //            dr["Name"] = "<a href=\"mailto:" + strUsers.Split(',')[3] + "\">" + strUsers.Split(',')[1] + " " + strUsers.Split(',')[2] + "</a>";

        //            //FB 1468 start
        //            if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
        //                dr["Connect2"] = txtUsers.Split(';')[rows + i].Split(':')[0];   
        //            else
        //                dr["BridgeID"] = txtUsers.Split(';')[rows + i].Split(':')[0];
        //            //FB 1468 end

        //            dr["Bandwidth"] = txtUsers.Split(';')[rows + i].Split(':')[1];
        //            dr["connectionType"] = txtUsers.Split(';')[rows + i].Split(':')[2];
        //            dr["DefaultProtocol"] = txtUsers.Split(';')[rows + i].Split(':')[3];
        //            dr["AddressType"] = txtUsers.Split(';')[rows + i].Split(':')[4];
        //            dr["VideoEquipment"] = txtUsers.Split(';')[rows + i].Split(':')[5];
        //            dr["Connection"] = txtUsers.Split(';')[rows + i].Split(':')[6];

        //            dr["Address"] = txtUsers.Split(';')[i].Split(':')[2].Trim();
        //            dr["URL"] = txtUsers.Split(';')[i].Split(':')[3];
        //            dr["ExchangeID"] = txtUsers.Split(';')[i].Split(':')[3]; //Cisco
        //            dr["APIPortNo"] = txtUsers.Split(';')[i].Split(':')[4]; //API Port...
        //            dr["IsOutside"] = txtUsers.Split(';')[i].Split(':')[1];
        //            dt.Rows.Add(dr);
        //        }
        //        //Response.Write(dt.Rows.Count);
        //        dgUsers.DataSource = dt;
        //        dgUsers.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace("Preserve: " + ex.Message + " : " + ex.StackTrace);
        //    }
        //}

        //#endregion

        #region PreserveUsersState
        //Modified during API Port NO..
        protected void PreserveUsersState()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = CreateDataTable(dt);

                String txtUsers = txtdgUsers.Value;
                log.Trace("txtUsers = " + txtUsers);
                String strUsers = "";
                int rows = (txtUsers.Split(';').Length - 1) / 2;

                string[] extusers = txtUsers.Split(';');
                if (extusers.Length > 0)
                {
                    for (int i = 0; i < extusers.Length - 1; i++)
                    {
                        DataRow dr = dt.NewRow();

                        string[] usrParams = extusers[i].Split(':');
                        if (usrParams.Length > 0)
                        {
                            dr["IsOutside"] = usrParams[0].ToString();
                            dr["Address"] = usrParams[1].ToString();
                            dr["ID"] = usrParams[2].ToString();
                            dr["URL"] = usrParams[3].ToString();
                            dr["ExchangeID"] = usrParams[4].ToString(); //Cisco
                            dr["APIPortNo"] = usrParams[5].ToString();
                            //FB 1468 start
                            if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                                dr["Connect2"] = usrParams[6].ToString();
                            else
                                dr["BridgeID"] = usrParams[6].ToString();
                            //FB 1468 end

                            dr["Bandwidth"] = usrParams[7].ToString();
                            dr["connectionType"] = usrParams[8].ToString();
                            dr["DefaultProtocol"] = usrParams[9].ToString();
                            dr["AddressType"] = usrParams[10].ToString();
                            dr["VideoEquipment"] = usrParams[11].ToString();
                            dr["Connection"] = usrParams[12].ToString();
                            /* ***** Code addedd for audio addon ***** */
                            dr["ConfCode"] = usrParams[13].ToString();
                            dr["LPin"] = usrParams[14].ToString();
                            dr["BridgeProfileID"] = usrParams[15].ToString(); //FB 2839
                            /* ***** Code addedd for audio addon ***** */
                            for (int j = 0; j < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length ; j++) //FB 1888 start
                            {
                                if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[j].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Equals("1") && txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[j].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Equals(dr["ID"].ToString()))
                                    strUsers = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[j];
                            }
                            try //FB 2839
                            {

                                dr["Name"] = "<a href=\"mailto:" + strUsers.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[3] + "\">" + strUsers.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[1] + " " + strUsers.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[2] + "</a>"; //FB 1888 end
                            }
                            catch (Exception ex)
                            { log.Trace(ex.Message); }
                            //else
                            //    dr["Name"] = "";

                            dt.Rows.Add(dr);
                        }
                    }
                }

                //Response.Write(dt.Rows.Count);
                dgUsers.DataSource = dt;
                dgUsers.DataBind();
                hdnextusrcnt.Value = "";
                hdnextusrcnt.Value = dgUsers.Items.Count.ToString();

            }
            catch (Exception ex)
            {
                log.Trace("Preserve: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region DisplayUserEndpoints

        protected void DisplayUserEndpoints()//preserve endpoints
        {
            try
            {
                DataTable dt = new DataTable();
                dt = CreateDataTable(dt);
                //Response.Write(lblConfID.Text + " : " + dgUsers.Items.Count);
                if (!lblConfID.Text.ToLower().Equals("new"))
                {
                    //Response.Write("in if");
                    dt = GetAdvancedAVSettings(dt, "U");
                }
                PreserveUsersState(); // Preload the datagrid with users input and rest works the same way it always does
                UsersChanged(ref dt);
                //if (UsersChanged(ref dt))
                //{
                    //Response.Write("rows: " + dt.Rows.Count);
                    if (dt.Rows.Count.Equals(0))
                    {
                        lblNoUsers.Visible = true;
                        dgUsers.DataSource = null;
                        dgUsers.Visible = false;
                    }
                    else
                    {
                        //Response.Write("in else: " + dt.Rows[1]["connectionType"].ToString());
                        dgUsers.Visible = true;
                        lblNoUsers.Visible = false;
                        //Code Added For FB 1422 - Start 
                        Int32 cnt = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            //Response.Write(dr["Name"]);
                            if (dr["BridgeId"].ToString().Equals("0")) //FB Case 1016 - Check for bad data
                                dr["BridgeId"] = "-1";
                            if (dr["Name"].ToString().IndexOf("'>") > 0)
                            {
                                lstEndpoints.Items.Add(new ListItem(dr["Name"].ToString().Substring(dr["Name"].ToString().IndexOf("'>", 0) + 2, dr["Name"].ToString().IndexOf("</", 0) - dr["Name"].ToString().IndexOf("'>", 0) - 2), "U_" + dr["ID"].ToString()));
                                txtLecturer.Value = "U_" + dr["ID"].ToString(); //FB 1721
                            }
                            else
                            {
                                lstEndpoints.Items.Add(new ListItem(dr["Name"].ToString(), "U_" + dr["ID"].ToString()));
                                txtLecturer.Value = "U_" + dr["ID"].ToString(); //FB 1721
                            }
                            //Code Added For FB 1422 - Start 
                            if (dr["Connect2"].ToString() == "")
                            {
                                if (cnt == 0)
                                    dr["Connect2"] = "1";
                                else
                                    dr["Connect2"] = "0";
                            }
                            /****** Code addedd for audio addon ***** */
                            mcuName = dr["BridgeID"].ToString();
                            String address = dr["Address"].ToString();
                            String inXML = "";
                            String outXML = "";
                            if (address != "" && mcuName != "")
                            {
                                String[] spiltAdd = SpiltAddress(address, mcuName);
                               
                                if(spiltAdd != null)
                                {
                                    dr["Address"] = spiltAdd[0];
                                    if(spiltAdd.Length > 2)
                                    {
                                        
                                        if((spiltAdd[1] =="" && spiltAdd[2] == "") && !address.Contains("+"))
                                        {
                                                inXML += "<EndpointDetails>";
                                                inXML += obj.OrgXMLElement();//Organization Module Fixes
                                                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                                                inXML += "  <EndpointID>" + dr["EndpointID"].ToString() + "</EndpointID>";
                                                inXML += "</EndpointDetails>";
                                                outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                                                if (outXML.IndexOf("<Error>") < 0)
                                                {
                                                    XmlDocument xmldoc = new XmlDocument();
                                                    xmldoc.LoadXml(outXML);
                                                    //XmlNodeList nodesEP = xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                                                    dr["ConfCode"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode").InnerText;
                                                    dr["LPin"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin").InnerText;

                                                }
                                        }
                                        else
                                        {
                                             dr["confCode"] = spiltAdd[1];
                                             dr["LPin"] = spiltAdd[2];   
                                        }
                                    }
                                    else if (dr["EndpointID"].ToString() != "" && dr["EndpointID"].ToString() != "0")
                                    {
                                                inXML += "<EndpointDetails>";
                                                inXML += obj.OrgXMLElement();//Organization Module Fixes
                                                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                                                inXML += "  <EndpointID>" + dr["EndpointID"].ToString() + "</EndpointID>";
                                                inXML += "</EndpointDetails>";
                                                outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                                                if (outXML.IndexOf("<Error>") < 0)
                                                {
                                                    XmlDocument xmldoc = new XmlDocument();
                                                    xmldoc.LoadXml(outXML);
                                                    //XmlNodeList nodesEP = xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                                                    dr["ConfCode"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode").InnerText;
                                                    dr["LPin"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin").InnerText;

                                                }
                                    }

                                }
                            /****** Code addedd for audio addon ***** */
                        }
                          cnt++; //FB 1736
                          //Code Added For FB 1422 - End 
                    }
                    //FB 1736 start
                    dgUsers.DataSource = dt;
                    dgUsers.DataBind();
                    hdnextusrcnt.Value = ""; //Added during API Port NO
                    hdnextusrcnt.Value = dgUsers.Items.Count.ToString(); //Added during API Port NO
                    //FB 2839 - Start
                    foreach (DataGridItem dgi in dgUsers.Items)
                    {
                        
                        if (((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Count > 1)
                        {
                            try
                            {
                                ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.FindByValue(((Label)dgi.FindControl("lstMCUProfSelected")).Text).Selected = true;

                            }
                            catch (Exception ex)
                            { log.Trace(ex.Message); }
                        }
                        else
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Add("No items");

                        if (((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedItem.Text.Equals("No items"))
                        {
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;
                            ((HtmlTableCell)dgi.FindControl("tdusrMCUprofile")).Visible = false;
                        }
                        else
                        {
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = true;
                            ((HtmlTableCell)dgi.FindControl("tdusrMCUprofile")).Visible = true;
                        }

                        IsValidBridge(Convert.ToString(((DropDownList)dgi.FindControl("lstBridges")).SelectedValue)); //FB 3052
                        if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                        {
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;
                            ((HtmlTableCell)dgi.FindControl("tdusrMCUprofile")).Visible = false;
                        }
                    }
                    //FB 2839 - End
                    if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB Case 770 Saima
                        foreach (DataGridItem dgi in dgUsers.Items)
                            ((DropDownList)dgi.FindControl("lstBridges")).Enabled = false;

                    //FB 1736 end
                }
            }
            catch (Exception ex)
            {
                log.Trace("Display: " + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region UsersChanged

        protected bool UsersChanged(ref DataTable dt)
        {
            try
            {
                bool flagUser = true;
                String connectn = "";
                //13,Intercall,Audio,linda.athmer@1dsney.com,1,0,0,1,1,0,0,0,,0,0,0,;
                log.Trace("txtPartysInfo.Text = " + txtPartysInfo.Text);
                for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++) //FB 1888 start
                {
                    if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1"))
                    {
                        flagUser = false;
                        //foreach (DataGridItem dgi in dgRooms.Items)
                        //Response.Write("<br>dt.Rows.Count:" + dt.Rows.Count);
                        foreach (DataRow dr in dt.Rows)
                        {
                            //Response.Write("<br>" + dr["ID"].ToString() + " : " + txtPartysInfo.Text.Split(';')[i].Split(',')[0].Trim() + " : " + txtPartysInfo.Text.Split(';')[i].Split(',')[4].Trim());
                            if (dr["ID"].ToString().Equals(txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim()) && (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1")))
                                flagUser = true;
                        }
                        //Response.Write(flagUser);

                        if (flagUser.Equals(false))
                        {
                            //Response.Write("in if");
                            String UserID = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                            if (UserID.Equals("new"))
                                UserID += "_" + i;

                            //FB 1678 - code changes starts

                            String videocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim();
                            String audiocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[9].Trim();

                            connectn = "1"; //FB 1744
                            if (videocnn == "1") //FB 1678
                            {
                                connectn = "2"; //AudioVideo //Code changed for FB 1744
                            }
                            else
                            {
                                //Code commented during FB 1744
                                //if (audiocnn == "1")
                                //    connectn = "2"; //Audio only                          
                                //else
                                //    connectn = "1"; //None

                                connectn = "1"; //Audio only                          
                            }
                            //FB 1678 - code changes ends

                            if (UserID.Equals("new"))
                                UserID += "_" + i;
                            dt.Rows.Add(AddUserEndpoint(UserID, dt, connectn));
                            //flagUser = true;
                        }
                    }
                }
                //
                //foreach (DataRow dr in dt.Rows)
                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    DataRow dr = dt.Rows[r];
                    //                    Response.Write(dt.Rows.Count);
                    flagUser = false;
                    for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++)
                    {
                        String UserID = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                        if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim().Equals("new"))
                            UserID += "_" + i;

                        //FB 1678 - code changes starts

                        String videocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim();
                        String audiocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[9].Trim();
                        //FB 1888 end

                        //if (vidcnn == "0") //FB 1678   changed the variable to 0
                        //    connectn = "2"; //Audio only
                        //else if (vidcnn == "1")  //Code changed for FB 1678 changed the variable to 1
                        //    connectn = "3"; //AudioVideo
                        //else
                        //    connectn = "1"; //None

                        connectn = "1"; //FB 1744 
                        if (videocnn == "1")
                        {
                            connectn = "2"; //AudioVideo //Code changed for FB 1744
                        }
                        else
                        {
                            //Commented for FB 1744 
                            //if (audiocnn == "1")
                            //    connectn = "2"; //Audio only                             
                            //else
                            //    connectn = "1"; //None

                            connectn = "1"; //Audio only  //FB 1744                            
                        }
                        //FB 1678 - code changes ends

                        if (dr["ID"].ToString().Equals(UserID) && (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1"))) //FB 1888
                        {
                            dr["Connection"] = connectn; //FB 1678
                            flagUser = true;
                        }
                    }
                    //Response.Write(flagUser);
                    if (flagUser.Equals(false))
                    {
                        dt.Rows.RemoveAt(r);
                        r--; //Code added for FB 1678

                        //                        Response.Write("----here" + r);
                    }
                    else
                    {
                        //Response.Write("Items: " + dgUsers.Items.Count);
                        foreach (DataGridItem dgi in dgUsers.Items)
                        {
                            if (dgi.Cells[0].Text.Equals(dr["ID"].ToString()))
                            {

                                dr["Bandwidth"] = ((DropDownList)dgi.FindControl("lstLineRate")).SelectedValue;
                                dr["BridgeID"] = ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue;
                                dr["connectionType"] = ((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue;
                                dr["DefaultProtocol"] = ((DropDownList)dgi.FindControl("lstProtocol")).SelectedValue;
                                dr["AddressType"] = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue;
                                dr["VideoEquipment"] = ((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedValue;
                                //Code changed for FB 1678
                                //dr["Connection"] = ((DropDownList)dgi.FindControl("lstConnection")).SelectedValue;
                                dr["Address"] = ((TextBox)dgi.FindControl("txtAddress")).Text.Trim();
                                dr["URL"] = ((TextBox)dgi.FindControl("txtEndpointURL")).Text;
                                dr["ExchangeID"] = ((TextBox)dgi.FindControl("txtExchangeID")).Text; //Cisco
                                dr["APIPortNo"] = ((TextBox)dgi.FindControl("txtApiportno")).Text;//API Port...
                                dr["IsOutside"] = "0";
                                if (((CheckBox)dgi.FindControl("chkIsOutside")).Checked)
                                    dr["IsOutside"] = "1";
                                //Code Added For FB 1422 - Start
                                dr["Connect2"] = ((DropDownList)dgi.FindControl("lstTelnetUsers")).SelectedValue;
                                //Code Added For FB 1422 - End
                                dr["ConfCode"] = ((TextBox)dgi.FindControl("txtConfCode")).Text.Trim();
                                dr["LPin"] = ((TextBox)dgi.FindControl("txtleaderPin")).Text.Trim();

                                if (dr["Connection"].ToString() == "1" && lstConferenceType.SelectedValue == "2")//code added for Audio addon //Code changed for FB 1744
                                {
                                    //Code changed for FB 1678
                                    dr["Address"] = ((TextBox)dgi.FindControl("txtAddress")).Text.Trim() + "D" + dr["ConfCode"].ToString().Trim() + "+" + dr["LPin"].ToString().Trim();
                                    
                                }
                                else
                                    dr["Address"] = ((TextBox)dgi.FindControl("txtAddress")).Text.Trim();

                                dr["BridgeProfileID"] = ((Label)dgi.FindControl("lstMCUProfSelected")).Text.Trim();
                                //dr["BridgeProfileID"] = ((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedValue;
                                //dt.Rows.Add(dr);  //Code changed for FB 1678
                            }
                        }
                    }
                }
                //Response.Write(flagUser + " : " + txtPartysInfo.Text + " : " + dt.Rows.Count);
                return flagUser;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        #endregion

        #region GetGuestID
        protected void GetGuestID(String strUser)
        {
            String inXML = "";
            inXML += "<saveUser>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <login>" + Session["userID"].ToString() + "</login>";
            inXML += "  <user>";
            inXML += "      <userName>";
            inXML += "          <firstName>" + "</firstName>";
            inXML += "          <lastName>" + "</lastName>";
            inXML += "      </userName>";
            inXML += "  </user>";
            inXML += "</saveUser>";
            String outXML = obj.CallMyVRMServer("SetGuest", inXML, Application["MyVRMServer_ConfigPath"].ToString());

        }
         #endregion

        #region DisplayRoomEndpoints
        protected void DisplayRoomEndpoints()
        {
            try
            {
                log.Trace("in DisplayRoomEndpoint");
                //Response.Write("in Rooms");
                SyncRoomSelection();
                DataTable dt = new DataTable();
                dt = CreateDataTable(dt);
                if (!lblConfID.Text.ToLower().Equals("new"))
                    dt = GetAdvancedAVSettings(dt, "R");
                //Response.Write("before if");
                if (RoomsChanged(ref dt))
                {
                    //Response.Write("in if" + dt.Rows.Count);
                    if (dt.Rows.Count.Equals(0))
                    {
                        lblNoRooms.Visible = true;
                        dgRooms.DataSource = null;
                        dgRooms.DataBind();  // FB 1691
                        dgRooms.Visible = false;
                    }
                    else
                    {
                        //Response.Write("in if");
                        dgRooms.Visible = true;
                        lblNoRooms.Visible = false;
                        dgRooms.DataSource = dt;
                        dgRooms.DataBind();
                        try
                        {
                            foreach (DataRow dr in dt.Rows)
                                if (dr["Name"].ToString().IndexOf("'>") > 0)
                                {
                                    lstEndpoints.Items.Add(new ListItem(dr["Name"].ToString().Substring(dr["Name"].ToString().IndexOf("'>", 0) + 2, dr["Name"].ToString().IndexOf("</", 0) - dr["Name"].ToString().IndexOf("'>", 0) - 2), "R_" + dr["ID"].ToString()));
                                    if (txtLecturer.Value == "") //FB 1721
                                    txtLecturer.Value = "R_" + dr["ID"].ToString(); //FB 1721
                                }
                                else
                                {
                                    lstEndpoints.Items.Add(new ListItem(dr["Name"].ToString(), "R_" + dr["ID"].ToString()));
                                    if (txtLecturer.Value == "") //FB 1721
                                    txtLecturer.Value = "R_" + dr["ID"].ToString(); //FB 1721
                                }
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message);
                        }
                    }
                    //errLabel.Text = "";   //FB 1462
                    errLabel.Visible = true;
                    foreach (DataGridItem dgi in dgRooms.Items)
                    {

                        if (((Label)dgi.FindControl("lblEndpointName")).Text.Trim().Equals("") || ((Label)dgi.FindControl("lblEndpointName")).Text.Trim().Contains("No Endpoint(s) associated with this room"))//FB 1182
                        {//FB 1182
                            //if (!errLabel.Text.Trim().Contains(((Label)dgi.FindControl("lblRoomName")).Text) && (TopMenu.Items[Wizard1.ActiveViewIndex].Text.IndexOf("Audio/Video") > 0))//FB 1182 
                            if (!errLabel.Text.Trim().Contains(((Label)dgi.FindControl("lblRoomName")).Text) && TopMenu.SelectedValue == "3") //(TopMenu.Items[Wizard1.ActiveViewIndex].Text.IndexOf("Audio") > 0))//FB 1182 //FB 1985 //FB JAPAN
                                errLabel.Text += "<br>Room " + ((Label)dgi.FindControl("lblRoomName")).Text + " does not have any Endpoint associated with it. An audio or video Conference cannot be created with this room.";
                        }//FB 1182
                        //if (Request.QueryString["t"].ToString().Trim().Equals(""))
                        //    ((CheckBox)dgi.FindControl("chkUseDefault")).Checked = false;

                        ((DropDownList)dgi.FindControl("lstProfiles")).ClearSelection();
                        if (((DropDownList)dgi.FindControl("lstProfiles")).Items.Count > 0)
                        {
                            try
                            {
                                ((DropDownList)dgi.FindControl("lstProfiles")).Items.FindByValue(((Label)dgi.FindControl("lblDefaultProfileID")).Text).Selected = true;
                                if (((DropDownList)dgi.FindControl("lstProfileType")).Items[((DropDownList)dgi.FindControl("lstProfiles")).SelectedIndex].Text.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                                    ((CheckBox)dgi.FindControl("chkUseDefault")).Enabled = false;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                        }
                        ((DropDownList)dgi.FindControl("lstBridges")).ClearSelection();
                        if (((DropDownList)dgi.FindControl("lstBridges")).Items.Count > 1)
                        {
                            try
                            {
                                ((DropDownList)dgi.FindControl("lstBridges")).Items.FindByValue(((Label)dgi.FindControl("lblBridgeID")).Text).Selected = true;
                            }
                            catch (Exception ex)
                            { log.Trace(ex.Message); }
                        }
                        //FB 2839 - Start
                        ((DropDownList)dgi.FindControl("lstMCUProfile")).ClearSelection();
                        if (((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Count > 1)
                        {
                            try
                            {
                                ((DropDownList)dgi.FindControl("lstMCUProfile")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.FindByValue(((Label)dgi.FindControl("lstMCUProfSelected")).Text).Selected = true;
                            }
                            catch (Exception ex)
                            { log.Trace(ex.Message); }
                        }
                        else
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Add("No items");

                        IsValidBridge(Convert.ToString(((DropDownList)dgi.FindControl("lstBridges")).SelectedValue)); //FB 3052
                        if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;
                        //FB 2839 - End

                        if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB Case 770 Saima
                            ((DropDownList)dgi.FindControl("lstBridges")).Enabled = false;
                    }
                }
                // fogbugz case 153, 975 Saima
                bool isProfileSelection = false;
                foreach (DataGridItem dgi in dgRooms.Items)
                {
                    EnableControls((CheckBox)dgi.FindControl("chkUseDefault"), new EventArgs());
                    //FB 3052 Starts
                      if (((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedItem.Text.Equals("No items"))
                        {
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;
                        }
                        else
                        {
                            isProfileSelection = true;
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = true;
                        }
                      if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                          ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;
                    //FB 3052 Ends
                }// Code Added For FB 1422 -For P2P Conf  - Start
                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                {
                    foreach (DataGridItem dgi in dgRooms.Items)
                    {
                        ((DropDownList)dgi.FindControl("lstMCUProfile")).Visible = false;

                        ((DropDownList)dgi.FindControl("lstTelnet")).ClearSelection();

                        if (((Label)dgi.FindControl("lblRoomTelnet")).Text != "")
                        {
                            /*on Edit Conf , show the value from Database as selected in caller/callee Dropdown*/
                            if (((Label)dgi.FindControl("lblRoomTelnet")).Text != "-1")
                            {
                                ((DropDownList)dgi.FindControl("lstTelnet")).Items.FindByValue(((Label)dgi.FindControl("lblRoomTelnet")).Text).Selected = true;
                            }
                        }
                        else
                        {
                            /*on New Conf Creation, Only the first  caller/callee Dropdown should have the value as caller selected and rest of it as callee.*/
                            if (cntTelnetRoom == 0)
                            {
                                ((DropDownList)dgi.FindControl("lstTelnet")).Items.FindByValue("1").Selected = true;
                                cntTelnetRoom = cntTelnetRoom + 1;
                            }
                            else
                            {
                                ((DropDownList)dgi.FindControl("lstTelnet")).Items.FindByValue("0").Selected = true;
                            }
                        }

                    }

                }
                // Code Added For FB 1422 - End
                if (dgRooms.Controls.Count > 0)
                {
                    Table childtable = dgRooms.Controls[0] as Table;
                    Label chk = childtable.Rows[0].FindControl("LblMCUProf") as Label;
                    if (lstConferenceType.SelectedValue.Equals("4"))
                        chk.Visible = false;
                    else
                        chk.Visible = isProfileSelection;

                    if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                        chk.Visible = false;
                }
                //FB 3052 Ends
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
         #endregion

        #region RoomsChanged
        protected bool RoomsChanged(ref DataTable dt)
        {
            try
            {
                bool flagRoom = true;
                //Response.Write(dt.Rows.Count);
                //foreach (DataGridItem dgi in dgRooms.Items)
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    if (tn.Depth.Equals(3))
                    {
                        //Response.Write("in if");
                        flagRoom = false;
                        //foreach (DataGridItem dgi in dgRooms.Items)
                        foreach (DataRow dr in dt.Rows)
                            if (dr["ID"].ToString().Equals(tn.Value))
                            {
                                flagRoom = true;
                                if (ProfileID.Count > 0) //ZD 100298
                                {
                                    int.TryParse(dr["BridgeID"].ToString(), out ConfBridge);
                                    if (ProfileID.ContainsKey(ConfBridge))
                                        dr["BridgeProfileID"] = ProfileID[ConfBridge].ToString();
                                }
                            }
                        if (flagRoom.Equals(false))
                            dt.Rows.Add(AddRoomEndpoint(tn.Value, dt));
                        //FB 2599 Start
                        if (isCloudEnabled == 1 && chkCloudConferencing.Checked) //FB 2717-Doubt
                        {
                            if (dt.Rows[dt.Rows.Count - 1]["isVMR"].ToString() == "1")//Vidyo 
                                dt.Rows.Remove(dt.Rows[dt.Rows.Count - 1]);
                        }
                        //FB 2599 End
                    }
                    foreach (DataRow dr in dt.Rows)
                    {
                        flagRoom = false;
                        foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                            if (tn.Depth.Equals(3))
                            {
                                if (dr["ID"].ToString().Equals(tn.Value))
                                    flagRoom = true;
                                if (!dt.Columns.Contains("IsVMR"))//FB 2448
                                    dt.Columns.Add("IsVMR");
                                if (dr["IsVMR"].ToString().Equals(1)) 
                                    dt.Rows.Count.Equals(0);
                            }
                        if (flagRoom.Equals(false))
                            dt.Rows.Remove(dr);
                        else
                        {
                            foreach (DataGridItem dgi in dgRooms.Items)
                            {
                                if (dgi.Cells[0].Text.Equals(dr["ID"].ToString()))
                                {
                                    dr["ProfileID"] = ((DropDownList)dgi.FindControl("lstProfiles")).SelectedValue;
                                    dr["BridgeID"] = ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue;
                                    dr["BridgeProfileID"] = ((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedValue; //FB 2839
                                }
                            }
                        }
                    }
                
                //Response.Write(dt.Rows.Count + " : " + flagRoom);
                return flagRoom;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }
         #endregion

        #region InUsers
        protected bool InUsers(String userid, String tpe)
        {
            if (tpe.ToUpper().Equals("U"))
                for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length ; i++) //FB 1888
                {
                    if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim().Equals(userid)) //FB 1888
                    {
                        return true;
                    }
                }
            if (tpe.ToUpper().Equals("R"))
            {
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    if (tn.Depth.Equals(3) && tn.Value.Equals(userid))
                        return true;
                }
            }
            return false;
        }
        #endregion

        #region GetAdvancedAVSettings
        protected DataTable GetAdvancedAVSettings(DataTable dt, String tpe)
        {
            try
            {
                String inXML = "<GetAdvancedAVSettings><UserID>" + Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<ConfID>" + lblConfID.Text + "</ConfID></GetAdvancedAVSettings>";//Organization Module Fixes
                String outXML = obj.CallMyVRMServer("GetAdvancedAVSettings", inXML, Application["MyVRMServer_Configpath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //outXML = "<GetAdvancedAVSettings><UserID>11</UserID>  <ConfID>9</ConfID>  <Endpoints>    <Endpoint>      <Type>R</Type><Name>Room1</Name><EndpointName>EP1</EndpointName>      <ID>2</ID>      <UseDefault>0</UseDefault>      <EndpointID>2</EndpointID>      <ProfileID>2</ProfileID>      <BridgeID>1</BridgeID>      <AddressType></AddressType>      <Address></Address>      <VideoEquipment></VideoEquipment>      <connectionType></connectionType>      <Bandwidth></Bandwidth>      <IsOutside></IsOutside>      <DefaultProtocol></DefaultProtocol>      <URL></URL>    </Endpoint>    <Endpoint>      <Type>U</Type>      <ID>13</ID><Name>Room1</Name><EndpointName>EP1</EndpointName>      <UseDefault>0</UseDefault>      <EndpointID></EndpointID>      <ProfileID></ProfileID>      <BridgeID>1</BridgeID>      <AddressType>1</AddressType>      <Address>12.12.12.12</Address>      <VideoEquipment>4</VideoEquipment>      <connectionType>1</connectionType>      <Bandwidth>384</Bandwidth>      <IsOutside>0</IsOutside>      <DefaultProtocol>1</DefaultProtocol>      <URL>12121212</URL>    </Endpoint>    <Endpoint>      <Type>U</Type>      <ID>15</ID><Name>Room1</Name><EndpointName>EP1</EndpointName>      <UseDefault>0</UseDefault>      <EndpointID></EndpointID>      <ProfileID></ProfileID>      <BridgeID>1</BridgeID>      <AddressType>1</AddressType>      <Address>13.13.13.13</Address>      <VideoEquipment>5</VideoEquipment>      <connectionType>2</connectionType>      <Bandwidth>384</Bandwidth>      <IsOutside>0</IsOutside>      <DefaultProtocol>1</DefaultProtocol>      <URL></URL>    </Endpoint>  </Endpoints></GetAdvancedAVSettings>";
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetAdvancedAVSettings/Endpoints/Endpoint");
                    
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    foreach (XmlNode node in nodes)
                    {
                        //ZD 100298 Start
                        int profiled = 0, bridgeid = 0;
                        int.TryParse(node.SelectSingleNode("BridgeProfileID").InnerText.Trim(), out profiled);
                        int.TryParse(node.SelectSingleNode("BridgeID").InnerText.Trim(), out bridgeid);
                        if (bridgeid > 0)
                        {
                            if (!ProfileID.ContainsKey(bridgeid))
                                ProfileID.Add(bridgeid, profiled);
                        }
                        //ZD 100298 End

                        if (node.SelectSingleNode("Type").InnerText.Trim().ToUpper().Equals(tpe) && InUsers(node.SelectSingleNode("ID").InnerText.Trim(), tpe))
                        {
                            //Response.Write("in if");
                            //FB 2400 start
                            String adds = "";
                            nodes = xmldoc.SelectNodes("//GetAdvancedAVSettings/Endpoints/Endpoint/MultiCodec");
                            foreach (XmlNode node1 in nodes)
                            {
                                adds = "";
                                XmlNodeList nodess = node1.SelectNodes("Address");
                                Int32 j = 0;
                                foreach (XmlNode snode in nodess)
                                {
                                    if (j > 0)
                                        adds += "�";

                                    adds += snode.InnerText;
                                    j = j + 1;
                                }
                                node1.InnerText = adds;
                            }
                            //FB 2400 End
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                            if (node.SelectSingleNode("IsLecturer").InnerText.Trim().Equals("1"))
                                txtLecturer.Value = tpe + "_" + node.SelectSingleNode("ID").InnerText;
                        }
                    }
                    if (ds.Tables.Count > 0)
                        dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["VideoEquipment"].ToString().Equals("0"))
                            dr["VideoEquipment"] = "-1";
                        if (dr["AddressType"].ToString().Equals("0"))
                            dr["AddressType"] = "-1";
                        
                        //Code Added for FB 1688 Starts for remove ++ character in user end point
                        if (dr["Name"] != null)
                        {
                            if (dr["Name"].ToString().IndexOf("++") >= 0)
                            {
                                dr["Name"] = dr["Name"].ToString().Replace("++", " "); ;
                            }
                        }
                        //Code Added for FB 1688 End
                    }
                }
                else
                {
                    log.Trace(tpe + " : " + obj.ShowErrorMessage(outXML));
                }
                //Coded Added for FB 1707 Start
                int i = 0;
                for (int j = 0; j <= dt.Columns.Count - 1; j++)
                {
                    if (dt.Columns[j].Caption.ToUpper().ToString() == "APIPORTNO")
                    {
                        i++;
                        if (i == 2)
                        {
                            dt.Columns.Remove("ApiPortno");
                            break;
                        }
                    }

                }
                //Coded Added for FB 1707 End
				/****** Code addedd for audio addon ***** */
                if (!dt.Columns.Contains("ConfCode"))
                    dt.Columns.Add("ConfCode");
                if (!dt.Columns.Contains("LPin"))
                    dt.Columns.Add("LPin");
                /****** Code addedd for audio addon ***** */
                if (!dt.Columns.Contains("isTelePresence")) //FB 2400
                    dt.Columns.Add("isTelePresence");
                if (!dt.Columns.Contains("BridgeProfileID")) //FB 2839
                    dt.Columns.Add("BridgeProfileID");
                return dt;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
                return null;
            }
        }
         #endregion

        #region CheckChange
        protected bool CheckChange(ArrayList RoomIDs, DataGrid dg) // not in use function
        {
            try
            {
                ArrayList dgIDs = new ArrayList(dgRooms.Items.Count);
                foreach (DataGridItem dgi in dg.Items)
                    dgIDs.Add(Int32.Parse(dgi.Cells[0].Text));

                dgIDs.Sort();
                RoomIDs.Sort();
                bool flag = false;
                if (!dgIDs.Count.Equals(RoomIDs.Count))
                    return false;
                for (int i = 0; i < dgIDs.Count; i++)
                {
                    flag = false;
                    for (int j = 0; j < RoomIDs.Count; j++)
                    {
                        if (dgIDs[i].Equals(RoomIDs[j]))
                            flag = true;
                    }
                }
                //Response.Write(flag);
                return flag;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                return false;
            }
        }
         #endregion

        #region AddRoomEndpoint
        protected DataRow AddRoomEndpoint(String RoomID, DataTable dt)
        {
            try
            {
                log.Trace("In AddRoomEndpoint");
                DataRow dr = dt.NewRow();
                //Response.Write("1");
                dr["ID"] = RoomID;
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + RoomID + "</roomID></login>");//Organization Module Fixes
                //String outXML = obj.CallCOM("GetOldRoom", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetOldRoom", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetOldRoom) 
                log.Trace("GetOldRoom outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                String VMR = xmldoc.SelectSingleNode("//room/IsVMR").InnerText.Trim();
                if (VMR == "0")//FB 2448
                {
                    if (!dt.Columns.Contains("Name"))
                        dt.Columns.Add("Name");
                    if (!dt.Columns.Contains("EndpointName"))
                        dt.Columns.Add("EndpointName");
                    dr["Name"] = xmldoc.SelectSingleNode("//room/roomName").InnerText.Trim();
                    dr["Name"] = "<a href='#' onclick='javascript:chkresource(\"" + dr["ID"].ToString() + "\")'>" + dr["Name"] + "</a>";
                    dr["EndpointID"] = xmldoc.SelectSingleNode("//room/endpoint").InnerText.Trim();
                    log.Trace("EndpointID:  " + dr["EndpointID"].ToString());
                    if (dr["EndpointID"].ToString().Equals("") || dr["EndpointID"].ToString().Equals("0") || dr["EndpointID"].ToString().Equals("-1"))
                        dr["EndpointName"] = "No Endpoint(s) associated with this room.";
                    else
                    {
                        //FB Case 546 Saima
                        //XmlNodeList nodes = xmldoc.SelectNodes("//room/endpoints/endpoint");
                        //foreach (XmlNode node in nodes)
                        //    if (dr["EndpointID"].Equals(node.SelectSingleNode("ID").InnerText.Trim()))
                        //    {
                        //        dr["EndpointName"] = node.SelectSingleNode("name").InnerText;
                        //        dr["EndpointName"] = "<a href='#' onclick='javascript:viewendpoint(\"" + dr["EndpointID"].ToString() + "\")'>" + dr["EndpointName"].ToString() + "</a>";
                        //        break;
                        //    }
                        inXML = new StringBuilder();
                        inXML.Append("<EndpointDetails>");
                        inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                        inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                        inXML.Append("<EndpointID>" + dr["EndpointID"].ToString() + "</EndpointID>");
                        inXML.Append("</EndpointDetails>");
                        outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                        
                        log.Trace("GetEndpointDetails outxml: " + outXML);
                        XmlDocument xmlEP = new XmlDocument();
                        xmlEP.LoadXml(outXML);
                        XmlNodeList nodesEP = xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                        dr["ProfileID"] = "0";
                        if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Name") != null)
                            dr["EndpointName"] = xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText; //FB Case 546 Saima
                        if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ApiPortno") != null)
                            dr["APIPortNo"] = xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ApiPortno").InnerText;//API Port...

                        foreach (XmlNode node in nodesEP)
                        {
                            //FB 2400 start
                            dr["isTelePresence"] = "0";
                            if (node.SelectSingleNode("isTelePresence") != null)
                                if (node.SelectSingleNode("isTelePresence").InnerText.Trim() != "")
                                    dr["isTelePresence"] = node.SelectSingleNode("isTelePresence").InnerText.Trim();
                            //FB 2400 end
                            dr["EndpointName"] = xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText; //FB Case 546 Saima

                            if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Equals(node.SelectSingleNode("ProfileID").InnerText))
                            {
                                dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                if (dr["BridgeID"].Equals("0"))
                                    dr["BridgeID"] = "-1";
                                else
                                    dr["BridgeProfileID"] = node.SelectSingleNode("BridgeProfileID").InnerText; //FB 2839
                                if (ProfileID.Count > 0) //ZD 100298
                                {
                                    int.TryParse(dr["BridgeID"].ToString(), out ConfBridge);
                                    if (ProfileID.ContainsKey(ConfBridge))
                                        dr["BridgeProfileID"] = ProfileID[ConfBridge].ToString();
                                }

                                dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                break;
                            }
                        }
                    }
                }
                if (!dt.Columns.Contains("IsVMR"))
                    dt.Columns.Add("IsVMR");
                dr["IsVMR"] = VMR;
                //Response.Write(dr["ProfileID"] + " : " + dr["EndpointName"]);
                //Response.End();
                return dr;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                DataRow dr = dt.NewRow();
                return dr;
            }
        }
         #endregion

        #region AddUserEndpoint
        protected DataRow AddUserEndpoint(String UserID, DataTable dt, String connection)//Code added for audio addon
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ID"] = UserID;
                String userType = "";   //Guest is not display in Audio Setting Tab - end

                if (UserID.IndexOf("new") < 0)
                {
                    String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID>" + UserID + "</userID></user></login>";//Organization Module Fixes
                    //FB 2027 Start
                    String outXML = obj.CallMyVRMServer("GetOldUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    //String outXML = obj.CallCOM("GetOldUser", inXML, Application["COM_ConfigPath"].ToString());
                    //FB 2027 End
                    //Response.Write(obj.Transfer(inXML));
                    //Guest is not display in Audio Setting Tab - end - Start
                    if (outXML.IndexOf("<userName>") < 0)
                    {
                        userType = "G";
                        inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + UserID + "</userID><alphabet></alphabet><pageNo>0</pageNo><sortBy>1</sortBy><EntityType>2</EntityType><userType>G</userType></login>";
                        outXML = obj.CallMyVRMServer("GetUserList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //outXML = obj.CallCOM("GetOldGuest", inXML, Application["COM_ConfigPath"].ToString());
                    }
                    else
                        userType = "";
                    //Guest is not display in Audio Setting Tab - end - End

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    if (!dt.Columns.Contains("Name"))
                        dt.Columns.Add("Name");
                    //Response.Write("1");
                    //Guest is not display in Audio Setting Tab - end - Start

                    if (userType == "G")
                    {
                        dr["ID"] = UserID;
                        dr["Name"] = xmldoc.SelectSingleNode("//users/user/firstName").InnerText.Trim() + " " + xmldoc.SelectSingleNode("//users/user/lastName").InnerText.Trim();
                        dr["BridgeID"] = "-1";
                        dr["VideoEquipment"] = "-1";
                        dr["AddressType"] = xmldoc.SelectSingleNode("//users/user/addresstype").InnerText; //FB 3041
                        dr["Address"] = xmldoc.SelectSingleNode("//users/user/address").InnerText; //FB 3041
                        dr["VideoEquipment"] = "-1";
                        dr["Bandwidth"] = "384";
                        dr["connectionType"] = xmldoc.SelectSingleNode("//users/user/connectiontype").InnerText; //FB 3041
                        dr["Connection"] = "2"; //Code changed for FB 1744
                        dr["IsOutside"] = xmldoc.SelectSingleNode("//users/user/outsidenetwork").InnerText; //FB 3041
                        dr["URL"] = "";
                        dr["DefaultProtocol"] = xmldoc.SelectSingleNode("//users/user/protocol").InnerText; //FB 3041

                    }
                    else if (xmldoc.SelectNodes("//oldUser/userName").Count > 0) //Guest is not display in Audio Setting Tab - end - end
                    {
                        dr["Name"] = xmldoc.SelectSingleNode("//oldUser/userName/firstName").InnerText.Trim() + " " + xmldoc.SelectSingleNode("//oldUser/userName/lastName").InnerText.Trim();
                        dr["Name"] = "<a href='mailto:" + xmldoc.SelectSingleNode("//oldUser/userEmail").InnerText.Trim() + "'>" + dr["Name"] + "</a>";
                        dr["BridgeID"] = xmldoc.SelectSingleNode("//oldUser/bridgeID").InnerText.Trim();
                        //Response.Write(dr["BridgeID"]);
                        if (dr["BridgeID"].Equals("") || dr["BridgeID"].Equals("0"))
                            dr["BridgeID"] = "-1";
                        dr["VideoEquipment"] = xmldoc.SelectSingleNode("//oldUser/videoEquipmentID").InnerText.Trim();
                        if (dr["VideoEquipment"].Equals("0"))
                            dr["VideoEquipment"] = "-1";
                        dr["AddressType"] = xmldoc.SelectSingleNode("//oldUser/addressTypeID").InnerText.Trim();
                        if (dr["AddressType"].Equals("0"))
                            dr["AddressType"] = "1";
                        dr["Address"] = xmldoc.SelectSingleNode("//oldUser/IPISDNAddress").InnerText.Trim();
                        dr["Bandwidth"] = xmldoc.SelectSingleNode("//oldUser/lineRateID").InnerText.Trim();
                        if (dr["Bandwidth"].Equals("-1") || dr["Bandwidth"].Equals("0"))
                            dr["Bandwidth"] = "384";
                        dr["ConnectionType"] = xmldoc.SelectSingleNode("//oldUser/connectionType").InnerText.Trim();
                        if (dr["ConnectionType"].Equals("-1") || dr["connectionType"].Equals("0"))
                            dr["ConnectionType"] = "1";
                        //Response.Write(dr["connectionType"]);
                        dr["Connection"] = connection;//"3";//Code added for audio addon
                        dr["IsOutside"] = xmldoc.SelectSingleNode("//oldUser/isOutside").InnerText.Trim();

                        dr["URL"] = xmldoc.SelectSingleNode("//oldUser/URL").InnerText.Trim();//During API Port...

                        dr["ExchangeID"] = xmldoc.SelectSingleNode("//oldUser/ExchangeID").InnerText.Trim(); //Cisco Fix

                        dr["APIPortNo"] = xmldoc.SelectSingleNode("//oldUser/APIPortNo").InnerText.Trim();//API Port...
                        dr["ConfCode"] = xmldoc.SelectSingleNode("//oldUser/conferenceCode").InnerText.Trim();//FB 1642 Audio Add On...
                        dr["LPin"] = xmldoc.SelectSingleNode("//oldUser/leaderPin").InnerText.Trim();//FB 1642 Audio Add On...   
                        if (!dt.Columns.Contains("BridgeProfileID")) //FB 2839
                            dt.Columns.Add("BridgeProfileID");
                        dr["BridgeProfileID"] = "";//FB 2839
                        if (xmldoc.SelectSingleNode("//oldUser/videoProtocol").InnerText.ToUpper().Equals("IP"))
                            dr["DefaultProtocol"] = "1";
                        else
                            dr["DefaultProtocol"] = "2";
                        //                Response.Write("<br>" + dr["BridgeID"].ToString());
                        String epID = xmldoc.SelectSingleNode("//oldUser/EndpointID").InnerText;
                        if (!epID.Trim().Equals("") || !epID.Trim().Equals("0") || !epID.Trim().Equals("-1"))
                        {
                            inXML = "";
                            inXML += "<EndpointDetails>";
                            inXML += obj.OrgXMLElement();//Organization Module Fixes
                            inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                            inXML += "  <EndpointID>" + epID + "</EndpointID>";
                            inXML += "</EndpointDetails>";
                            outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            log.Trace("GetEndpointDetails outxml: " + outXML);
                            if (outXML.IndexOf("<error>") < 0)
                            {
                                xmldoc = new XmlDocument();
                                xmldoc.LoadXml(outXML);
                                try
                                {
                                    dr["BridgeID"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Bridge").InnerText;
                                    dr["VideoEquipment"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/VideoEquipment").InnerText;
                                    if (dr["VideoEquipment"].ToString().Equals("0")) //FB 1769
                                        dr["VideoEquipment"] = "-1";
                                    dr["AddressType"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/AddressType").InnerText;
                                    if (dr["AddressType"].ToString().Equals("0")) //FB 1769
                                        dr["AddressType"] = "-1";
                                    dr["Address"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Address").InnerText.Trim();
                                    dr["Bandwidth"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/LineRate").InnerText;
                                    dr["ConnectionType"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ConnectionType").InnerText;
                                    dr["IsOutside"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/IsOutside").InnerText;
                                    dr["URL"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/URL").InnerText;
                                    dr["ExchangeID"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ExchangeID").InnerText; //Cisco Fix
                                    dr["DefaultProtocol"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/DefaultProtocol").InnerText;
                                    dr["APIPortNo"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ApiPortno").InnerText;//API Port...
                                    if (dr["connection"].ToString() == "1") //Code changed for FB 1744
                                    {
                                        if(enableConferenceCode == "1")
                                            dr["ConfCode"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode").InnerText;//FB-1642 Audio Add On...
                                        else
                                            dr["ConfCode"] = "";
                                        if(enableLeaderPin == "1")
                                            dr["LPin"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin").InnerText;//FB-1642 Audio Add On...
                                        else
                                            dr["LPin"] = "";

                                    }
                                    else
                                    {
                                        dr["ConfCode"] = "";
                                        dr["LPin"] = "";
                                    }
                                    dr["BridgeProfileID"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/BridgeProfileID").InnerText;//FB 2839
                                    if (ProfileID.Count > 0) //ZD 100298
                                    {
                                        int.TryParse(dr["BridgeID"].ToString(), out ConfBridge);
                                        if (ProfileID.ContainsKey(ConfBridge))
                                            dr["BridgeProfileID"] = ProfileID[ConfBridge].ToString();
                                    }
                                }
                                catch (Exception ex1) { log.Trace(ex1.Message); }
                            }
                        }

                        /****** Code addedd for audio addon ***** */

                        String txtUsers = txtdgUsers.Value;

                        int rows = txtUsers.Split(';').Length -1 ;

                        //if (rows <= 0)
                        //{
                        //   dr["ConfCode"] = "";
                        //   dr["LPin"] = "";
                        //}
                        if (rows > 0)
                        {

                            for (int i = 0; i < rows; i++)
                            {

                                if (UserID == txtUsers.Split(';')[i].Split(':')[2])
                                {
                                    dr["ConfCode"] = txtUsers.Split(';')[i].Split(':')[13];
                                    dr["LPin"] = txtUsers.Split(';')[i].Split(':')[14];
                                }
                            }
                        }

                        /****** Code addedd for audio addon ***** */


                    }
                    else
                    {
                        inXML = "<GetConferenceEndpoint>";
                        inXML += obj.OrgXMLElement();//Organization Module Fixes
                        inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                        inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                        inXML += "  <EndpointID>" + UserID + "</EndpointID>";
                        inXML += "  <Type>U</Type>";
                        inXML += "</GetConferenceEndpoint>";
                        //Response.Write(obj.Transfer(inXML));
                        outXML = obj.CallMyVRMServer("GetConferenceEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            //Response.Write(obj.Transfer(outXML));
                            try
                            {
                                dr["ID"] = UserID;
                                dr["Name"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Name").InnerText;
                                dr["BridgeID"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/BridgeID").InnerText;
                                dr["VideoEquipment"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/VideoEquipment").InnerText;
                                if (dr["VideoEquipment"].ToString().Equals("0")) //FB 1769
                                    dr["VideoEquipment"] = "-1";
                                dr["AddressType"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/AddressType").InnerText;
                                if (dr["AddressType"].ToString().Equals("0")) //FB 1769
                                    dr["AddressType"] = "-1";
                                dr["Address"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Address").InnerText.Trim();
                                dr["Bandwidth"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Bandwidth").InnerText;
                                dr["connectionType"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/connectionType").InnerText;
                                dr["Connection"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Connection").InnerText;
                                dr["IsOutside"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/IsOutside").InnerText;
                                dr["URL"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/URL").InnerText;
                                dr["ExchangeID"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/ExchangeID").InnerText; //Cisco Fix
                                dr["DefaultProtocol"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/DefaultProtocol").InnerText;
                                dr["APIPortNo"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/APIPortNo").InnerText;//API Port...
                            }
                            catch (Exception ex)
                            {
                                dr["Name"] = "";
                                log.Trace(ex.Message);
                            }
                        }
                        else
                        {
                            dr["ID"] = UserID;
                            dr["Name"] = "User not found.";
                            dr["BridgeID"] = "-1";
                            dr["VideoEquipment"] = "-1";
                            dr["AddressType"] = "-1";
                            dr["Address"] = "";
                            dr["VideoEquipment"] = "-1";
                            dr["Bandwidth"] = "384";
                            dr["connectionType"] = "1";
                            dr["Connection"] = "2";//Code changed for FB 1744
                            dr["IsOutside"] = "0";
                            dr["URL"] = "";
                            dr["ExchangeID"] = ""; //Cisco Fix
                            dr["DefaultProtocol"] = "1";
                            dr["APIPortNo"] = "";//API Port...
                        }
                    }
                }
                else
                {
                    String strUser = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[Int32.Parse(UserID.Split('_')[1])]; // UserID.Split(';')[1]; //FB 1888
                    dr["ID"] = UserID;
                    dr["Name"] = "<a href='mailto:" + strUser.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[3] + "'>" + strUser.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[1] + " " + strUser.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[2] + "</a>"; //FB 1888
                    dr["BridgeID"] = "-1";
                    dr["VideoEquipment"] = "-1";
                    dr["AddressType"] = "-1";
                    dr["Address"] = "";
                    dr["VideoEquipment"] = "-1";
                    dr["Bandwidth"] = "384";
                    dr["connectionType"] = "1";
                    dr["Connection"] = "2";//Code changed for FB 1744
                    dr["IsOutside"] = "0";
                    dr["URL"] = "";
                    dr["ExchangeID"] = ""; //Cisco Fix
                    dr["DefaultProtocol"] = "1";
                    dr["APIPortNo"] = "";//API Port...
                }
                return dr;
            }
            catch (Exception ex)
            {
                log.Trace("AddUserEndpoint: " + ex.StackTrace + " : " + ex.Message);
                return null;
            }
        }
         #endregion

        #region CreateDataTable
        protected DataTable CreateDataTable(DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("ID"))
                    dt.Columns.Add("ID");
                if (!dt.Columns.Contains("Type"))
                    dt.Columns.Add("Type");
                if (!dt.Columns.Contains("Name"))
                    dt.Columns.Add("Name");
                if (!dt.Columns.Contains("EndpointID"))
                    dt.Columns.Add("EndpointID");
                if (!dt.Columns.Contains("EndpointName"))/***/
                    dt.Columns.Add("EndpointName");
                if (!dt.Columns.Contains("ProfileID"))
                    dt.Columns.Add("ProfileID");
                if (!dt.Columns.Contains("Bandwidth"))
                    dt.Columns.Add("Bandwidth");
                if (!dt.Columns.Contains("connectionType"))
                    dt.Columns.Add("connectionType");
                if (!dt.Columns.Contains("DefaultProtocol"))
                    dt.Columns.Add("DefaultProtocol");
                if (!dt.Columns.Contains("AddressType"))
                    dt.Columns.Add("AddressType");
                if (!dt.Columns.Contains("Address"))
                    dt.Columns.Add("Address");
                if (!dt.Columns.Contains("VideoEquipment"))
                    dt.Columns.Add("VideoEquipment");
                if (!dt.Columns.Contains("BridgeID"))
                    dt.Columns.Add("BridgeID");
                if (!dt.Columns.Contains("Connection"))
                    dt.Columns.Add("Connection");
                if (!dt.Columns.Contains("URL"))
                    dt.Columns.Add("URL");
                if (!dt.Columns.Contains("ExchangeID")) //Cisco
                    dt.Columns.Add("ExchangeID");
                if (!dt.Columns.Contains("IsOutside"))
                    dt.Columns.Add("IsOutside");
                //Code Added for FB 1422 -Start
                if (!dt.Columns.Contains("Connect2"))
                    dt.Columns.Add("Connect2");
                //Code Added for FB 1422 -End
                //API Port Starts...
                if (!dt.Columns.Contains("APIPortNo")) 
                    dt.Columns.Add("APIPortNo");
                //API Port Ends...
                /****** Code addedd for audio addon ***** */
                if (!dt.Columns.Contains("ConfCode"))
                    dt.Columns.Add("ConfCode");
                if (!dt.Columns.Contains("LPin"))
                    dt.Columns.Add("LPin");
                /****** Code addedd for audio addon ***** */
                if (!dt.Columns.Contains("isTelePresence")) //FB 2400
                    dt.Columns.Add("isTelePresence");
                if (!dt.Columns.Contains("BridgeProfileID")) //FB 2839
                    dt.Columns.Add("BridgeProfileID");

                return dt;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
                return null;
            }
        }
         #endregion

        #region InitializeLists
        protected void InitializeLists(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))// || (e.Item.ItemType.Equals(ListItemType.Footer)))
                {
                    log.Trace("'" + ((Label)e.Item.FindControl("lblEndpointID")).Text + "'");
                    if (((Label)e.Item.FindControl("lblEndpointID")).Text.Trim() != "" && ((Label)e.Item.FindControl("lblEndpointID")).Text.Trim() != "0")
                    {
                        BindEndpointDetails((DropDownList)e.Item.FindControl("lstProfiles"), ((Label)e.Item.FindControl("lblEndpointID")).Text, (DropDownList)e.Item.FindControl("lstProfileType"), "AddressType");
                        BindEndpointDetails((DropDownList)e.Item.FindControl("lstProfiles"), ((Label)e.Item.FindControl("lblEndpointID")).Text, (DropDownList)e.Item.FindControl("lstProfileBridge"), "Bridge"); //FB Case 198: Saima
                    }
                    obj.BindBridges((DropDownList)e.Item.FindControl("lstBridges"));

                    obj.BindProfileDetails((DropDownList)e.Item.FindControl("lstMCUProfile"), ((Label)e.Item.FindControl("lblBridgeID")).Text);//FB 2839

                    //Code Added For FB 1422 - Point-to-Point Conference change - Start
                    if (lstConferenceType.SelectedValue.Equals("4"))
                    {
                        ((DropDownList)e.Item.FindControl("lstBridges")).Visible = false;
                        ((DropDownList)e.Item.FindControl("lstTelnet")).Visible = true;
                        ((DropDownList)e.Item.FindControl("lstMCUProfile")).Visible = false;

                    }
                    //Code Added For FB 1422 - Start   
                    IsValidBridge(Convert.ToString(((DropDownList)e.Item.FindControl("lstBridges"))));//FB 3052
                    if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                        ((DropDownList)e.Item.FindControl("lstMCUProfile")).Visible = false;
                   
                }
               
                //Code Added For FB 1422 - Point-to-Point Conference change - Start
                if (e.Item.ItemType.Equals(ListItemType.Header))
                {
                    IsValidBridge(Convert.ToString(((DropDownList)e.Item.FindControl("lstBridges"))));//FB 3052
                    if (lstConferenceType.SelectedValue.Equals("4"))
                    {
                        ((Label)e.Item.FindControl("LblEP")).Text = obj.GetTranslatedText("Select Caller/Callee");//FB 1830 - Translation
                        ((Label)e.Item.FindControl("LblMCUProf")).Visible = false; //FB 2839
                    }
                }
                //Code Added For FB 1422 - Point-to-Point Conference change - End
                
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
         #endregion

        #region InitializeUsers
        protected void InitializeUsers(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))// || (e.Item.ItemType.Equals(ListItemType.Footer)))
                {
                    DataRowView dr = e.Item.DataItem as DataRowView;    //FB 1468 

                    /* *** Code added for Audio-addon *** */
                    HtmlTableRow audioParams1 = (HtmlTableRow)e.Item.FindControl("AudioParams1");
                    HtmlTableRow audioParams2 = (HtmlTableRow)e.Item.FindControl("AudioParams2");
                    HtmlTableRow audioParams3 = (HtmlTableRow)e.Item.FindControl("AudioParams3");
                    DropDownList drpConnection = (DropDownList)e.Item.FindControl("lstConnection");
                    HtmlTable tbMCUandConn = (HtmlTable)e.Item.FindControl("tbMCUandConn"); //FB 2359
                    TextBox txtconfcode = (TextBox)e.Item.FindControl("txtConfCode");
                    Label lblconfcode = (Label)e.Item.FindControl("LblConfCode");
                    TextBox txtleaderpin = (TextBox)e.Item.FindControl("txtleaderPin");
                    Label lblleaderpin = (Label)e.Item.FindControl("LblLeaderpin");
                    /* RequiredFieldValidator required1 = (RequiredFieldValidator)e.Item.FindControl("reqLineRate");
                    RequiredFieldValidator required2 = (RequiredFieldValidator)e.Item.FindControl("reqAddressType");
                    RequiredFieldValidator required3 = (RequiredFieldValidator)e.Item.FindControl("reqEquipment");
                    RequiredFieldValidator required4 = (RequiredFieldValidator)e.Item.FindControl("reqConnectionType");  */
                     
                    txtconfcode.Attributes.Add("style", "display:none;");
                    lblconfcode.Attributes.Add("style", "display:none;");
                    txtleaderpin.Attributes.Add("style", "display:none;");
                    lblleaderpin.Attributes.Add("style", "display:none;");
                     
                    /* *** Code added for Audio-addon *** */

                    obj.BindBridges((DropDownList)e.Item.FindControl("lstBridges"));
                    obj.BindLineRate((DropDownList)e.Item.FindControl("lstLineRate"));
                    obj.BindAddressType((DropDownList)e.Item.FindControl("lstAddressType"));
                    obj.BindVideoEquipment((DropDownList)e.Item.FindControl("lstVideoEquipment"));
                    obj.BindMediaTypes((DropDownList)e.Item.FindControl("lstConnection")); 
                    obj.BindDialingOptions((DropDownList)e.Item.FindControl("lstConnectionType"));
                    obj.BindVideoProtocols((DropDownList)e.Item.FindControl("lstProtocol"));

                    if (EnableProfileSelection != null)
                    {
                        if (EnableProfileSelection == "0")
                        {
                            ((DropDownList)e.Item.FindControl("lstMCUProfile")).Enabled = false;
                        }
                    }
                    // Code modified for FB 1468 start
                    //Code Added For FB 1422 - Point-to-Point Conference change - Start
                    if (lstConferenceType.SelectedValue.Equals("4"))
                    {
                        ((DropDownList)e.Item.FindControl("lstBridges")).Visible = false;
                        ((DropDownList)e.Item.FindControl("lstMCUProfile")).Visible = false;
                        DropDownList lstTelnet = new DropDownList();
                        lstTelnet = e.Item.FindControl("lstTelnetUsers") as DropDownList;
                        lstTelnet.Visible = true;

                        if (dr != null)
                        {
                            if (dr["Connect2"].ToString() == "-1")
                                dr["Connect2"] = "1";

                            lstTelnet.SelectedValue = dr["Connect2"].ToString();
                        }
                        ((Label)e.Item.FindControl("LblEPUsers")).Text = obj.GetTranslatedText("Select Caller/Callee");//FB 1830 - Translation
                    }   //Code Added For FB 1422 - End  
                    else
                    {
                        DropDownList lstMCU = new DropDownList();
                        DropDownList lstMCUProfiles = new DropDownList();
                        lstMCU = e.Item.FindControl("lstBridges") as DropDownList;
                        lstMCUProfiles = e.Item.FindControl("lstMCUProfile") as DropDownList;
                        lstMCU.Visible = true;
                        lstMCUProfiles.Visible = true;

                        if (dr != null)
                        {
                            if (dr["BridgeID"].ToString() != "")
                                lstMCU.SelectedValue = dr["BridgeID"].ToString();

                            obj.BindProfileDetails((DropDownList)e.Item.FindControl("lstMCUProfile"), lstMCU.SelectedValue);
                            
                            if (lstMCUProfiles.Items.Count > 0)
                            {
                                if (!lblConfID.Text.ToLower().Equals("new"))
                                {
                                    if (dr["BridgeProfileID"].ToString() != "" && dr["BridgeProfileID"].ToString() != "0") //FB 2839
                                    {
                                        lstMCUProfiles.ClearSelection();
                                        lstMCUProfiles.SelectedValue = dr["BridgeProfileID"].ToString();
                                    }
                                }
                            }
                            IsValidBridge(Convert.ToString((lstMCU.SelectedValue)));//FB 3052
                            if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                            {
                                lstMCUProfiles.Visible = false;
                                ((HtmlTableCell)e.Item.FindControl("tdusrMCUprofile")).Visible = false;
                            }

                        }
                        ((DropDownList)e.Item.FindControl("lstTelnetUsers")).Visible = false;
                        ((Label)e.Item.FindControl("LblEPUsers")).Text = obj.GetTranslatedText("MCU");//FB 1830 - Translation
                    }
                    if (lstConferenceType.SelectedValue.Equals("4"))
                        ((HtmlTableCell)e.Item.FindControl("tdusrMCUprofile")).Visible = false;
                    // Code modified for FB 1468 end
                    if (dr != null)
                    {
                        if (dr["connection"].ToString().Equals("1") && lstConferenceType.SelectedValue == "2")//code added for Audio addon //Code changed for FB 1744
                        {
                            if (enableAudioParams == "0")
                            {
                                audioParams1.Attributes.Add("style", "display:none;");
                                audioParams2.Attributes.Add("style", "display:none;");
                                audioParams3.Attributes.Add("style", "display:none;");

                                if (client.ToUpper() == "DISNEY") //FB 2359
                                    tbMCUandConn.Attributes.Add("style", "display:none;");

                                ((DropDownList)e.Item.FindControl("lstAddressType")).SelectedValue = "3";
                                lstRestrictNWAccess.SelectedValue = "3";
                                ((DropDownList)e.Item.FindControl("lstProtocol")).SelectedValue = "2";
                                ((DropDownList)e.Item.FindControl("lstLineRate")).SelectedValue = OrgLineRate.ToString(); //FB 2429
                                if (OrgLineRate <= 0)
                                    ((DropDownList)e.Item.FindControl("lstLineRate")).SelectedValue = "384"; //FB 2429
                                ((DropDownList)e.Item.FindControl("lstConnectionType")).SelectedValue = "2";
                            }

                            if (enableConferenceCode == "1")
                            {
                                txtconfcode.Attributes.Add("style", "display:block;");
                                lblconfcode.Attributes.Add("style", "display:block;");
                            }

                            if (enableLeaderPin == "1")
                            {
                                txtleaderpin.Attributes.Add("style", "display:block;");
                                lblleaderpin.Attributes.Add("style", "display:block;");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
         #endregion

        #region BindEndpointDetails
        public void BindEndpointDetails(DropDownList sender, String EPID, DropDownList lstProfileType, String col2) //FB Case 198: Saima added col2
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                StringBuilder inXML = new StringBuilder();
                if (!EPID.Equals(""))
                {
                    inXML.Append("<EndpointDetails>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>");
                    inXML.Append("<EndpointID>" + EPID + "</EndpointID>");
                    inXML.Append("</EndpointDetails>");

                    log.Trace("Endpoint Details InXML " + inXML.ToString());
                    String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("Endpoint Details OutXML " + outXML);
                    xmldoc.LoadXml(outXML);
                }
                XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                if (nodes.Count > 0)
                {
                    obj.LoadList(sender, nodes, "ProfileID", "ProfileName");
                    //Response.Write(col2);
                    obj.LoadList(lstProfileType, nodes, "ProfileID", col2); //FB Case 198: Saima
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
         #endregion

        #region getUploadFilePath
        protected string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            try
            {

                if (fpn.Equals(""))
                    fPath = "";
                else
                {
                    char[] splitter = { '\\' };
                    string[] fa = fpn.Split(splitter[0]);
                    if (fa.Length.Equals(0))
                        fPath = "";
                    else
                        fPath = fa[fa.Length - 1];
                }

                
            }
            catch (Exception ex)
            { }

            return fPath;
        }
         #endregion

        #region GetTimezones
        protected void GetTimezones(Object sender, EventArgs e)
        {
            try
            {
                String selTZ = lstConferenceTZ.SelectedValue;
                DropDownList lstTemp = (DropDownList)sender;
                lstTemp.Items.Clear();
                lstTemp.ClearSelection();
                obj.GetTimezones(lstTemp, ref selTZ);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
         #endregion

        #region LoadDeliveryTypes
        protected void LoadDeliveryTypes(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                obj.GetDeliveryTypes(lstTemp);
                if (lstTemp.ID.IndexOf("Item") >= 0)
                    lstTemp.Items.RemoveAt(lstTemp.Items.Count - 2);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
         #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Workorder?") + "')"); //FB japnese
                    DropDownList lstServices = (DropDownList)e.Item.FindControl("lstServices");
                    //LoadCateringServices(lstServices, new EventArgs());
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
         #endregion

        #region SetCalendar

        protected void SetCalendar(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.EditItem))
                {
                    Image imgCalendar = (Image)e.Item.FindControl("imgCalendar");
                    //Response.Write(imgCalendar.ClientID);
                    //Code changed by offshore for FB Issue 1073 -- start
                    imgCalendar.Attributes.Add("onclick", "javascript:showCalendar('" + ((TextBox)e.Item.FindControl("txtDeliverByDate")).ClientID + "', '" + ((Image)e.Item.FindControl("imgCalendar")).ClientID + "', 1, '" + format + "');");
                    //Code changed by offshore for FB Issue 1073 -- end
                    //imgCalendar.Attributes.Add("onclick", "javascript:alert('" + ((TextBox)e.Item.FindControl("txtDeliverByDate")).ClientID + "');");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
        /* Fogbugz case 162 for previous button */
         #endregion

        #region SubtractActiveIndex
        protected void SubtractActiveIndex(Object sender, EventArgs e)
        {
            try
            {
                //if (Wizard1.WizardSteps[Wizard1.ActiveStepIndex - 1].Title.Trim().Equals(""))
                //    this.Wizard1.ActiveStepIndex = this.Wizard1.ActiveStepIndex - 2;
                //else
                //    this.Wizard1.ActiveStepIndex = this.Wizard1.ActiveStepIndex - 1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
         #endregion

        #region SideBarButtonClickValidation
        protected void SideBarButtonClickValidation(Object sender, EventArgs e)
        {
            try
            {
                //Response.Write(Wizard1.ActiveStepIndex + " : " + Wizard1.WizardSteps.Count + " : " + dgConflict.Items.Count + " : " + dgConflict.Visible);

                // FB Case 718: Saima hiding the conflict datagrid when navigating away from preview screen after it is bbeen populated.
                if (Wizard1.ActiveViewIndex.Equals(Wizard1.Views.Count - 1) && (dgConflict.Items.Count > 0) && (dgConflict.Visible.Equals(true)))
                {
                    dgConflict.DataSource = null;
                    dgConflict.DataBind();
                    dgConflict.Visible = false;
                    tblConflict.Visible = false;
                    btnConfSubmit.Text = obj.GetTranslatedText("Submit Conference");//FB 1830 - Translation
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
         #endregion

        #region confStartTime_TextChanged
        protected void confStartTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                log.Trace("In confStartTime_TextChanged");
                SetEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("Error confStartTime_TextChanged: " + ex.StackTrace);
            }
        }
         #endregion

        /* *** -- Method Added for Buffer Zone  -- *** Start */
       
        #region SetSetupTearDownTime
        public void SetSetupTearDownTime(object sender, EventArgs e)
        {
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            try
            {
				//FB 2634
                //SetupDateTime.Text = SetupTime.Text;
                //TearDownDateTime.Text = TeardownTime.Text;

                //FB 1865
                if (txtPartysInfo.Text != "")
                    hdnParty.Value = txtPartysInfo.Text.Trim();

                //CheckTime(sender, e);//Code added for  timezone issue FB 2692

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return;
            }
        }
        #endregion

        // FB 2692 Starts
        //#region SetSetupTearDownTime
        //public void SetBufferTimeOnActiate(object sender, EventArgs e)
        //{
        //    DateTime startDate = DateTime.MinValue;
        //    DateTime endDate = DateTime.MinValue;
        //    try
        //    {
        //        //FB 2634
        //        //SetupTime.Text = SetupDateTime.Text;
        //        //TeardownTime.Text = TearDownDateTime.Text;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace(ex.StackTrace + " : " + ex.Message);
        //        return;
        //    }
        //}
        //#endregion
        // FB 2692 Ends

        #region Validate Conferencing Duration
        public bool ValidateConferenceDur()
        {
            try
            {
                //FB 2634
                DateTime startDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                DateTime endDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                TimeSpan confduration = endDate.Subtract(startDate);
                if (confduration.TotalMinutes < 15)
                {
                    errLabel.Text = obj.GetTranslatedText("Invalid Duration.Conference duration should be minimum of 15 mins.");//FB 1830 - Translation
                    return false;
                }

                return true;

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;

            }
        }
        #endregion


        /* *** --Method Added for Buffer Zone -- *** End */

        #region confEndTime_SelectedIndexChanged
        protected void confStartTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                log.Trace("In confStartTime_SelectedIndexChanged");
                SetEndTime();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
         #endregion

        #region confEndTime_SelectedIndexChanged
        private void SetEndTime()
        {
            try
            {
                if (Recur.Value.Trim().Equals(""))
                {
                    log.Trace("In SetEndTime");
                    //Code changed by offshore for FB Issue 1073 -- start
                    //DateTime t = Convert.ToDateTime( confStartDate.Text + " " + confStartTime.Text);
                    //DateTime et = Convert.ToDateTime(confEndDate.Text + " " + confEndTime.Text);
                    DateTime t = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                    DateTime et = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " +  myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                    //Code changed by offshore for FB Issue 1073 -- end
                    if (et <= t)
                    {
                        t = t.AddHours(1.00);
                        if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ)) // Code added for MOJ Phase 2 QA Bug
                            t = t.AddMinutes(15);
                        //Code changed by offshore for FB Issue 1073 -- start
                        confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(t);
                        //Code changed by offshore for FB Issue 1073 -- End
                        confEndTime.Text = t.ToString(tformat);
                    }
                    CalculateDuration();
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error confStartTime_TextChanged: " + ex.Message + " : " + ex.StackTrace);
                DisplayDialog("Invalid Start Date/Time.");
                //DateTime tReset = Convert.ToDateTime(confEndDate.Text + " " + confEndTime.Text);
                //tReset = tReset.Subtract(new TimeSpan(1, 0, 0));
                //confStartDate.Text = tReset.ToString("MM/dd/yyyy");
                //confStartTime.Text = tReset.ToString("hh:mm tt");
                //confStartTime.Focus();
            }
        }
         #endregion

        #region confEndTime_SelectedIndexChanged
        private void StartLessThanEndTime()
        {
            try
            {
                log.Trace("In StartLessThenEndTime");
                //Code changed by Offshore for FB Issue 1073 -- Start
                //DateTime tStart = Convert.ToDateTime(confStartDate.Text + " " + confStartTime.Text);
                //DateTime tEnd = Convert.ToDateTime(confEndDate.Text + " " + confEndTime.Text);
                DateTime tStart = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                DateTime tEnd = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " +  myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                //Code changed by Offshore for FB Issue 1073 -- ENd
                log.Trace(tStart.ToLongDateString() + " : " + tEnd.ToLongDateString());
                if (tStart >= tEnd)
                {
                    log.Trace("in if");
                    DisplayDialog("End time will be changed because it should be greater than start time.");
                    ResetEndTime();
                    confEndTime.Focus();
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error StartLessThrenEndTime " + ex.StackTrace + " : " + ex.Message);
                DisplayDialog("Invalid Date/Time.");
                ResetEndTime();
                confEndTime.Focus();
            }

        }
         #endregion

        #region confEndTime_SelectedIndexChanged
        private void ResetEndTime()
        {
            try
            {
                log.Trace("In ResetEndTime");
                DateTime tReset = Convert.ToDateTime(confStartDate.Text + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                tReset = tReset.AddHours(1.00);
                //Code changed by Offshore for FB Issue 1073 -- Start
                //confEndDate.Text = tReset.ToString("MM/dd/yyyy");
                confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(tReset);
                //Code changed by Offshore for FB Issue 1073 -- End
                confEndTime.Text = tReset.ToString(tformat);
                CalculateDuration();
            }
            catch (Exception ex)
            {
                log.Trace("ResetEndTime: " + ex.StackTrace + " : " + ex.Message);
                DisplayDialog("Invalid Date/Time.");
            }

        }
         #endregion

        #region confEndTime_SelectedIndexChanged

        protected void confEndTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                StartLessThanEndTime();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region confEndTime_TextChanged

        protected void confEndTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                StartLessThanEndTime();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion
                
        #region DisplayDialog
        protected void DisplayDialog(string strDisplayMessage)
        {
            try
            {
                string script = "<Script language='javascript'>";
                script += "callalert('" + strDisplayMessage + "')";
                script += "</";
                script += "script>";
                Literal literal = new Literal();
                literal.Text = script;
                Page.FindControl("frmSettings2").Controls.Add(literal);
            }
            catch (Exception ex)
            {
                log.Trace("DisplayDialog: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region CalculateDuration
        public void CalculateDuration()
        {
            try
            {
                log.Trace("In CalculateDuration");
                //Code changed by Offshore for FB Issue 1073 -- Start
                //TimeSpan durationMin = DateTime.Parse(confEndDate.Text + " " + confEndTime.Text).Subtract(DateTime.Parse(confStartDate.Text + " " + confStartTime.Text));
                //TimeSpan durationMin = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text).Subtract(DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + confStartTime.Text));
                TimeSpan durationMin = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text)).Subtract(DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)));
                //Code changed by Offshore for FB Issue 1073 -- End
                log.Trace(durationMin.ToString());
                lblConfDuration.Text = "";
                if (durationMin.TotalMinutes < 0)
                    lblConfDuration.Text = obj.GetTranslatedText("Invalid duration");//FB 1830 - Traslation
                if (Math.Floor(durationMin.TotalDays) > 0)
                    lblConfDuration.Text = Math.Floor(durationMin.TotalDays) + " days ";
                if (Math.Floor(durationMin.TotalHours) > 0)
                    lblConfDuration.Text += durationMin.Hours + " hrs ";
                if (durationMin.Minutes > 0)
                    lblConfDuration.Text += durationMin.Minutes + " mins";
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                lblConfDuration.Text = obj.GetTranslatedText("Invalid duration");//FB 1830 - Translation
            }
        }

        #endregion

        #region CalculateDuration
        public void CalculateDuration(Object server, EventArgs e)
        {
            CalculateDuration();
        }
        
        #endregion

        #region GetCustomAttributesLHRIC
        public void GetCustomAttributesLHRIC(XmlNodeList nodes)
        {
            try
            {
                for (int i = 11; i < 19; i++)
                {
                    ((Label)tblLHRICCustomAttributes.FindControl("lblCA" + (i + 1))).Text = nodes[i].SelectSingleNode("Title").InnerText;
                    if (nodes[i].SelectSingleNode("Type").InnerText.Equals("4"))
                    {
                        TextBox txtTemp = (TextBox)tblLHRICCustomAttributes.FindControl("ctrlCA" + (i + 1));
                        if (nodes[i].SelectSingleNode("OptionList/Option/DisplayValue") != null)
                            txtTemp.Text = nodes[i].SelectSingleNode("OptionList/Option/DisplayValue").InnerText;
                    }
                    if (nodes[i].SelectSingleNode("Type").InnerText.Equals("6"))
                    {
                        DropDownList lstTemp = (DropDownList)tblLHRICCustomAttributes.FindControl("ctrlCA" + (i + 1));
                        foreach (XmlNode node in nodes[i].SelectNodes("OptionList/Option"))
                        {
                            ListItem li = new ListItem(node.SelectSingleNode("DisplayCaption").InnerText, node.SelectSingleNode("OptionID").InnerText);
                            lstTemp.Items.Add(li);
                            if (node.SelectSingleNode("Selected").InnerText.Equals("1")) //Custom Attribute Fixes
                            {
                                lstTemp.ClearSelection();
                                li.Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetCustomAttributesLHRIC" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Visible = true;
            }
        }
        
        #endregion

        //FB 2486 Starts
        #region GetConfMessages
        public void GetConfMessages(XmlNodeList nodes)
        {
            try
            {
                string controlID = "0", duration = "0", textMessage = "0";
                int msgCount = nodes.Count;
                
                for (int i = 0; i < msgCount; i++)
                {
                    controlID = "0";
                    duration = "0";
                    textMessage = "0";
                    if (nodes[i].SelectSingleNode("controlID").InnerText.Trim() != null)
                        controlID = nodes[i].SelectSingleNode("controlID").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("textMessage").InnerText.Trim() != "")
                        textMessage = nodes[i].SelectSingleNode("textMessage").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("msgduartion").InnerText.Trim() != null)
                        duration = nodes[i].SelectSingleNode("msgduartion").InnerText.Trim();

                    if (controlID == "1")
                    {
                        chkmsg1.Checked = true;

                        drpdownconfmsg1.ClearSelection();
                        if (drpdownconfmsg1.Items.FindByText(textMessage) != null)
                            drpdownconfmsg1.Items.FindByText(textMessage).Selected = true;
                        
                        drpdownmsgduration1.ClearSelection();
                        if (drpdownmsgduration1.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration1.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration1.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "2")
                    {
                        chkmsg2.Checked = true;

                        drpdownconfmsg2.ClearSelection();
                        if (drpdownconfmsg2.Items.FindByText(textMessage) != null)
                            drpdownconfmsg2.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration2.ClearSelection();
                        if (drpdownmsgduration2.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration2.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration2.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "3")
                    {
                        chkmsg3.Checked = true;

                        drpdownconfmsg3.ClearSelection();
                        if (drpdownconfmsg3.Items.FindByText(textMessage) != null)
                            drpdownconfmsg3.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration3.ClearSelection();
                        if (drpdownmsgduration3.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration3.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration3.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "4")
                    {
                        isToogle = true; //FB 2506
                        chkmsg4.Checked = true;

                        drpdownconfmsg4.ClearSelection();
                        if (drpdownconfmsg4.Items.FindByText(textMessage) != null)
                            drpdownconfmsg4.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration4.ClearSelection();
                        if (drpdownmsgduration4.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration4.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration4.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "5")
                    {
                        isToogle = true; //FB 2506
                        chkmsg5.Checked = true;

                        drpdownconfmsg5.ClearSelection();
                        if (drpdownconfmsg5.Items.FindByText(textMessage) != null)
                            drpdownconfmsg5.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration5.ClearSelection();
                        if (drpdownmsgduration5.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration5.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration5.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "6")
                    {
                        isToogle = true; //FB 2506
                        chkmsg6.Checked = true;

                        drpdownconfmsg6.ClearSelection();
                        if (drpdownconfmsg6.Items.FindByText(textMessage) != null)
                            drpdownconfmsg6.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration6.ClearSelection();
                        if (drpdownmsgduration6.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration6.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration6.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "7")
                    {
                        isToogle = true; //FB 2506
                        chkmsg7.Checked = true;

                        drpdownconfmsg7.ClearSelection();
                        if (drpdownconfmsg7.Items.FindByText(textMessage) != null)
                            drpdownconfmsg7.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration7.ClearSelection();
                        if (drpdownmsgduration7.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration7.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration7.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "8")
                    {
                        isToogle = true; //FB 2506
                        chkmsg8.Checked = true;

                        drpdownconfmsg8.ClearSelection();
                        if (drpdownconfmsg8.Items.FindByText(textMessage) != null)
                            drpdownconfmsg8.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration8.ClearSelection();
                        if (drpdownmsgduration8.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration8.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration8.Attributes["PreValue"] = duration;
                        }
                    }
                    else if (controlID == "9")
                    {
                        isToogle = true; //FB 2506
                        chkmsg9.Checked = true;

                        drpdownconfmsg9.ClearSelection();
                        if (drpdownconfmsg9.Items.FindByText(textMessage) != null)
                            drpdownconfmsg9.Items.FindByText(textMessage).Selected = true;

                        drpdownmsgduration9.ClearSelection();
                        if (drpdownmsgduration9.Items.FindByText(duration) != null)
                        {
                            drpdownmsgduration9.Items.FindByText(duration).Selected = true;
                            drpdownmsgduration9.Attributes["PreValue"] = duration;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetConfMessages" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Visible = true;
            }
        }

        #endregion
        //FB 2486 Ends

        #region ChangeDefaultBridge
        //FB Case 198: Saima starts here
        protected void ChangeDefaultBridge(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                DataGridItem dgi = (DataGridItem)lstTemp.Parent.Parent;
                DropDownList lstProfileBridge = (DropDownList)dgi.FindControl("lstProfileBridge");
                //Response.Write("in defaultbridge" + lstProfileBridge.Items.Count);
                DropDownList lstBridges = (DropDownList)dgi.FindControl("lstBridges");
                try
                {
                    //Response.Write("profile: " + lstTemp.SelectedIndex + " : " + lstProfileBridge.Items[lstTemp.SelectedIndex].Text);
                    lstBridges.ClearSelection();
                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Clear();
                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Add("No items");
                    lstBridges.Items.FindByValue(lstProfileBridge.Items[lstTemp.SelectedIndex].Text).Selected = true;

                    ChangeEPBridgeProfile(dgi); //FB 2947
                }
                catch (Exception ex1)
                {
                    log.Trace(ex1.StackTrace + " : " + ex1.Message);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        //FB Case 198: Saima ends here
        #endregion
		//FB 2839 - Start
        #region ChangeBridgeProfile
        protected void ChangeBridgeProfile(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                DataGridItem dgi;
                dgi = (DataGridItem)lstTemp.Parent.Parent;

                ChangeEPBridgeProfile(dgi); //FB 2947
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }        
        #endregion

        #region ChangeConfProfile
        protected void ChangeConfProfile(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                DataGridItem dgi = (DataGridItem)lstTemp.Parent.Parent;
                ChangeEPConfProfile(dgi); //FB 2947
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region ChangeUserConfProfile
        protected void ChangeUserConfProfile(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                DataGridItem dgi = (DataGridItem)lstTemp.Parent.Parent.Parent.Parent.Parent;
                ChangeEPConfProfile(dgi); //FB 2947
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region ChangeUserBridgeProfile
        protected void ChangeUserBridgeProfile(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                DataGridItem dgi;
                dgi = (DataGridItem)lstTemp.Parent.Parent.Parent.Parent.Parent;
                ChangeEPBridgeProfile(dgi); //FB 2947
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //FB 2839 - End

        //FB 2947 Starts
        #region ChangeEPBridgeProfile
        protected void ChangeEPBridgeProfile(DataGridItem dgi)
        {
            //int ConfProfileID = 0, DefConfSerID = 0, ChangedSerID = -1;
            //Hashtable ProfileID = new Hashtable(); //ZD 100298
            try
            {
                IsValidBridge(Convert.ToString(((DropDownList)dgi.FindControl("lstBridges")).SelectedValue));
                if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                {
                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Clear();
                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Add(obj.GetTranslatedText("No items"));
                }
                else
                {
                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Clear();
                    obj.BindProfileDetails((DropDownList)dgi.FindControl("lstMCUProfile"), Convert.ToString(((DropDownList)dgi.FindControl("lstBridges")).SelectedValue));
                    if (txtConfServiceID == "0") //ZD 100298
                        txtConfServiceID = "-1";

                    if (((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Count > 1)
                    {
                        try
                        {
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.FindByValue(txtConfServiceID).Selected = true;
                            int.TryParse(txtConfServiceID, out DefConfSerID);
                        }
                        catch (Exception ex)
                        { log.Trace(ex.Message); }

                        int i = 0;
                        foreach (DataGridItem dgirooms in dgUsers.Items) //FB 2947 Starts
                        {

                            if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                                i++;
                        }

                        foreach (DataGridItem dgirooms in dgRooms.Items)
                        {
                            if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                            {
                                i++;
                            }
                        }

                        if (i == 1)
                        {
                            if (!ProfileID.ContainsKey((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)))
                                ProfileID.Add((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex), DefConfSerID); //ZD 100298
                            else
                                //int.TryParse(ProfileID[(((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedIndex)].ToString(), out DefConfSerID);
                                ProfileID[(((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)] = DefConfSerID;
                        }



                        foreach (DataGridItem dgirooms in dgUsers.Items) //FB 2947 Starts
                        {

                            if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                            {
                                //ConfProfileID = (((DropDownList)dgirooms.FindControl("lstMCUProfile")).SelectedValue);
                                int.TryParse((((DropDownList)dgirooms.FindControl("lstMCUProfile")).SelectedValue), out ConfProfileID);
                                if ((ConfProfileID != ChangedSerID) && (ConfProfileID != DefConfSerID))
                                {
                                    ChangedSerID = ConfProfileID;
                                    if (!ProfileID.ContainsKey((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)))
                                        ProfileID.Add((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex), ChangedSerID); //ZD 100298
                                }
                                else
                                {
                                    ChangedSerID = ConfProfileID;
                                    if (!ProfileID.ContainsKey((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)))
                                        ProfileID.Add((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex), ChangedSerID); //ZD 100298
                                    else
                                        ProfileID[(((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)] = ChangedSerID;
                                }

                            }
                        }
                        foreach (DataGridItem dgirooms in dgRooms.Items)
                        {
                            if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                            {
                                //ConfProfileID = (((DropDownList)dgirooms.FindControl("lstMCUProfile")).SelectedIndex);
                                int.TryParse((((DropDownList)dgirooms.FindControl("lstMCUProfile")).SelectedValue), out ConfProfileID);
                                if ((ConfProfileID != ChangedSerID) && (ConfProfileID != DefConfSerID))
                                {
                                    if (!ProfileID.ContainsValue(ConfProfileID))
                                    {
                                        int.TryParse((((DropDownList)dgirooms.FindControl("lstMCUProfile")).SelectedValue), out ConfProfileID);
                                        ChangedSerID = ConfProfileID;
                                        if (!ProfileID.ContainsKey((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)))
                                            ProfileID.Add((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex), ChangedSerID); //ZD 100298
                                        break;
                                    }
                                    else
                                        ChangedSerID = ConfProfileID;
                                }
                                else
                                {

                                    ChangedSerID = ConfProfileID;
                                    if (!ProfileID.ContainsKey((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)))
                                        ProfileID.Add((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex), ChangedSerID); //ZD 100298
                                    else
                                        ProfileID[(((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)] = ChangedSerID;

                                }
                            }
                        }
                        //} //FB 2947 Ends
                        if (ChangedSerID == 0) //ZD 100298
                            ChangedSerID = -1;
                        Session["HashProfileIDs"] = ProfileID; //ZD 100298
                        //if (ProfileID.ContainsKey(ConfBridge))
                        //    dr["BridgeProfileID"] = ProfileID[ConfBridge].ToString();
                        foreach (DataGridItem dgirooms in dgRooms.Items)
                        {
                            if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                            {


                                if (ChangedSerID > 0)
                                {
                                    ((DropDownList)dgi.FindControl("lstMCUProfile")).ClearSelection();
                                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.FindByValue(ChangedSerID.ToString()).Selected = true; //FB 2947
                                }
                                else
                                {
                                    ((DropDownList)dgirooms.FindControl("lstMCUProfile")).ClearSelection();
                                    ((DropDownList)dgirooms.FindControl("lstMCUProfile")).Items.FindByValue("-1").Selected = true;
                                }
                                //break; //FB 2947
                            }
                        }
                        foreach (DataGridItem dgirooms in dgUsers.Items)
                        {
                            if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                            {
                                //ConfProfileID = (((DropDownList)dgirooms.FindControl("lstMCUProfile")).SelectedIndex);
                                int.TryParse((((DropDownList)dgirooms.FindControl("lstMCUProfile")).SelectedValue), out ConfProfileID);
                                if (ConfProfileID > 0)
                                {
                                    ((DropDownList)dgi.FindControl("lstMCUProfile")).ClearSelection();
                                    ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.FindByValue(ChangedSerID.ToString()).Selected = true;
                                }
                                //break; //FB 2947
                            }
                        }
                    }
                    else
                    {
                        ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Clear();
                        ((DropDownList)dgi.FindControl("lstMCUProfile")).Items.Add(obj.GetTranslatedText("No items"));
                    }
                }

                //FB 3052 Starts
                bool isProfileSelection = false;
                foreach (DataGridItem dgiRooms in dgRooms.Items)
                {
                    IsValidBridge(Convert.ToString(((DropDownList)dgiRooms.FindControl("lstBridges")).SelectedValue));
                    if (((DropDownList)dgiRooms.FindControl("lstMCUProfile")).SelectedItem.Text.Equals("No items"))
                    {
                        ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Visible = false;
                    }
                    else
                    {
                        if ((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM)) //ZD 100298
                            isProfileSelection = true;
                        ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Visible = true;
                    }

                    if (lstConferenceType.SelectedValue.Equals("4"))
                        ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Visible = false;

                    if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                        ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Visible = false;
                }
                foreach (DataGridItem dgiRooms in dgUsers.Items)
                {
                    IsValidBridge(Convert.ToString(((DropDownList)dgiRooms.FindControl("lstBridges")).SelectedValue));
                    if (((DropDownList)dgiRooms.FindControl("lstMCUProfile")).SelectedItem != null)
                    {
                        if (((DropDownList)dgiRooms.FindControl("lstMCUProfile")).SelectedItem.Text.Equals("No items"))
                        {
                            ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Visible = false;
                            ((HtmlTableCell)dgiRooms.FindControl("tdusrMCUprofile")).Visible = false;
                        }
                        else
                        {
                            ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Visible = true;
                            ((HtmlTableCell)dgiRooms.FindControl("tdusrMCUprofile")).Visible = true;
                        }
                        if (EnableProfileSelection != null)
                        {
                            if (EnableProfileSelection == "0")
                            {
                                ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Enabled = false;
                            }
                        }
                        if (!((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM))) //ZD 100298
                        {
                            ((DropDownList)dgiRooms.FindControl("lstMCUProfile")).Visible = false;
                            ((HtmlTableCell)dgiRooms.FindControl("tdusrMCUprofile")).Visible = false;
                        }
                    }
                }
                if (dgRooms.Controls.Count > 0)
                {
                    Table childtable = dgRooms.Controls[0] as Table;
                    Label chk = childtable.Rows[0].FindControl("LblMCUProf") as Label;
                    if (lstConferenceType.SelectedValue.Equals("4"))
                        chk.Visible = false;
                    else
                        chk.Visible = isProfileSelection;

                }
                //FB 3052 Ends
            }
            catch (Exception ex1)
            {
                log.Trace(ex1.StackTrace + " : " + ex1.Message);
            }

        }
        #endregion
        #region ChangeEPConfProfile
        protected void ChangeEPConfProfile(DataGridItem dgi)
        {
            int ConfProfileID = 0;
            try
            {
                int.TryParse(((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedValue, out ConfProfileID); //ZD 100298
                //if (ConfProfileID > 0) //ZD 100298
                //{
                int.TryParse(((DropDownList)dgi.FindControl("lstMCUProfile")).SelectedValue, out ConfProfileID); //ZD 100298
                foreach (DataGridItem dgirooms in dgRooms.Items)
                {
                    if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                    {
                        ((DropDownList)dgirooms.FindControl("lstMCUProfile")).ClearSelection();
                        ((DropDownList)dgirooms.FindControl("lstMCUProfile")).Items.FindByValue(ConfProfileID.ToString()).Selected = true;

                        if (!ProfileID.ContainsKey((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)))//ZD 100298
                            ProfileID.Add((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex), ConfProfileID);
                        else
                            ProfileID[(((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)] = ConfProfileID;
                    }
                }
                foreach (DataGridItem dgirooms in dgUsers.Items)
                {
                    if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                    {
                        ((DropDownList)dgirooms.FindControl("lstMCUProfile")).ClearSelection();
                        ((DropDownList)dgirooms.FindControl("lstMCUProfile")).Items.FindByValue(ConfProfileID.ToString()).Selected = true;
                    }
                    if (!ProfileID.ContainsKey((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)))//ZD 100298
                        ProfileID.Add((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex), ConfProfileID);
                    else
                        ProfileID[(((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex)] = ConfProfileID;
                }
                Session["HashProfileIDs"] = ProfileID; //ZD 100298
                //}
                //else
                //{
                //    foreach (DataGridItem dgirooms in dgRooms.Items)
                //    {
                //        if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                //        {
                //            ((DropDownList)dgirooms.FindControl("lstMCUProfile")).ClearSelection();
                //            ((DropDownList)dgirooms.FindControl("lstMCUProfile")).Items.FindByValue("-1").Selected = true;
                //        }
                //    }
                //    foreach (DataGridItem dgirooms in dgUsers.Items)
                //    {
                //        if ((((DropDownList)dgi.FindControl("lstBridges")).SelectedIndex) == (((DropDownList)dgirooms.FindControl("lstBridges")).SelectedIndex))
                //        {
                //            ((DropDownList)dgirooms.FindControl("lstMCUProfile")).ClearSelection();
                //            ((DropDownList)dgirooms.FindControl("lstMCUProfile")).Items.FindByValue("-1").Selected = true;
                //        }
                //    }
                //}
            }

            catch (Exception ex1)
            {
                log.Trace(ex1.StackTrace + " : " + ex1.Message);
            }
        }
        #endregion
        //FB 2947 Ends
        

        #region MoveNext

        protected void MoveNext(Object sender, EventArgs e)
        {
            try
            {
                Wizard1.ActiveViewIndex += 1;
                TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
				
				//Merging Recurrence
            	hdnValue.Value = Wizard1.ActiveViewIndex.ToString();
               
				while (TopMenu.Items[Wizard1.ActiveViewIndex].Text.Trim().Equals(""))
                {
                    Wizard1.ActiveViewIndex += 1;
                    TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
                }
                if (Wizard1.ActiveViewIndex == 6 ) //TopMenu.Items[Wizard1.ActiveViewIndex].Text.IndexOf("Cater") > 0) //FB JAPAN
                {
                    LoadCateringWorkorders();
                }
                //if (TopMenu.Items[Wizard1.ActiveViewIndex].Text.IndexOf("Audio/Video") > 0)
                if (Wizard1.ActiveViewIndex == 3 ) //TopMenu.Items[Wizard1.ActiveViewIndex].Text.IndexOf("Audio") > 0)//FB 1985 //FB JAPAN
                {
                    UpdateAdvAVSettings(new object(), new EventArgs());
                }

                if (Wizard1.ActiveViewIndex.Equals(Wizard1.Views.Count - 1))
                {
                    btnPrev.Visible = false;
                    btnNext.Visible = false;
                    LoadPreview();
                }
                else
                {
                    btnNext.Visible = true;
                    btnPrev.Visible = true;
                }

                //FB 1865
                if (hdnParty.Value != "" && txtPartysInfo.Text.Trim() == "")
                    txtPartysInfo.Text = hdnParty.Value;

               
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region MoveBack

        protected void MoveBack(Object sender, EventArgs e)
        {
            try
            {
                Wizard1.ActiveViewIndex -= 1;

                //Merging Recurrence
                if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    hdnValue.Value = Convert.ToString(Wizard1.ActiveViewIndex - 1);
                else
                    hdnValue.Value = Wizard1.ActiveViewIndex.ToString();
				//FB 2694
                //if (Wizard1.ActiveViewIndex.Equals(0))
                //{
                //    btnPrev.Visible = false;
                //}
                TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
                while (TopMenu.Items[Wizard1.ActiveViewIndex].Text.Trim().Equals(""))
                {
                    Wizard1.ActiveViewIndex -= 1;
                    TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
                }

                //FB 1865
                if (txtPartysInfo.Text != "")
                    hdnParty.Value = txtPartysInfo.Text.Trim();
                
                if(Wizard1.ActiveViewIndex ==3) //FB 2533
                    UpdateAdvAVSettings(new Object(), new EventArgs());

                //FB 2694 - Starts
                if (Wizard1.ActiveViewIndex.Equals(0))
                {
                    btnPrev.Visible = false;
					this.RegisterStartupScript("initial", "<script>fnEnableBuffer();openRecur()</script>");//FB 2694
	
	                this.RegisterStartupScript("initial", "<script>fnEnableBuffer();fnShow()</script>");
                }
                //FB 2694 - End

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        /* FB Issue for Customized Instances Handling on conflict and Edit Dirty Instances - start */

        #region Hide recur button
        /// <summary>
        /// To hide the recur button on instance edit
        /// </summary>
        protected void HideRecurButton()
        {
            try
            {
                isInstanceEdit = "";
                if (Session["IsInstanceEdit"] != null)
                {
                    if (Session["IsInstanceEdit"].ToString() != "")
                        isInstanceEdit = Session["IsInstanceEdit"].ToString();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        /* FB Issue for Customized Instances Handling on conflict and Edit Dirty Instances - end */

        /* ** Code added for FB 1426  ** */

        #region InitializeConflict
        protected void InitializeConflict(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    if (Session["timeFormat"] != null)
                    {
                        if (Session["timeFormat"].ToString() == "0")
                        {
                            RegularExpressionValidator valid = (RegularExpressionValidator)e.Item.FindControl("RegConflictTime");
                            if (valid != null)
                            {
                                valid.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                valid.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");

                            }
                            RegularExpressionValidator validend = (RegularExpressionValidator)e.Item.FindControl("RegConflictEndTime");
                            if (validend != null)
                            {
                                validend.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                validend.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");

                            }

                            //code added for buffer zone --Start
                            if (enableBufferZone == "1")
                            {
                                RegularExpressionValidator validsetup = (RegularExpressionValidator)e.Item.FindControl("RegConflictTeardownTime");
                                if (validsetup != null)
                                {
                                    validsetup.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    validsetup.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");

                                }
                                RegularExpressionValidator validtear = (RegularExpressionValidator)e.Item.FindControl("RegConflictSetupTime");
                                if (validtear != null)
                                {
                                    validtear.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    validtear.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");

                                }
                            }

                            //code added for buffer zone --end
                        }
                        else if (Session["timeFormat"].ToString() == "2")
                        {
                            RegularExpressionValidator valid = (RegularExpressionValidator)e.Item.FindControl("RegConflictTime");
                            if (valid != null)
                            {
                                valid.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                                valid.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");

                            }
                            RegularExpressionValidator validend = (RegularExpressionValidator)e.Item.FindControl("RegConflictEndTime");
                            if (validend != null)
                            {
                                validend.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                                validend.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");

                            }

                            //code added for buffer zone --Start
                            if (enableBufferZone == "1")
                            {
                                RegularExpressionValidator validsetup = (RegularExpressionValidator)e.Item.FindControl("RegConflictTeardownTime");
                                if (validsetup != null)
                                {
                                    validsetup.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                                    validsetup.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");

                                }
                                RegularExpressionValidator validtear = (RegularExpressionValidator)e.Item.FindControl("RegConflictSetupTime");
                                if (validtear != null)
                                {
                                    validtear.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                                    validtear.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");

                                }
                            }

                            //code added for buffer zone --end
                        }
                    }

                    //code added for buffer zone --Start
                    TemplateColumn lblSetup = (TemplateColumn)dgConflict.Columns[3];  //buffer zone
                    TemplateColumn lblTear = (TemplateColumn)dgConflict.Columns[4];

                    MetaBuilders.WebControls.ComboBox setup = (MetaBuilders.WebControls.ComboBox)e.Item.FindControl("conflictSetupTime");
                    MetaBuilders.WebControls.ComboBox tear = (MetaBuilders.WebControls.ComboBox)e.Item.FindControl("conflictTeardownTime");

                    if (enableBufferZone == "0")
                    {
                        setup.Visible = false;
                        tear.Visible = false;
                        lblSetup.Visible = false;
                        lblTear.Visible = false;
                    }
                    //code added for buffer zone --End

                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        /* ** Code added for FB 1426  ** */

        /* *** Custom Attribute Fixes - start *** */
        #region FillCustomAttributeTable
        
        private void FillCustomAttributeTable()
        {
            XmlDocument xmlDOC = new XmlDocument();
            xmlDOC.LoadXml(Session["outxml"].ToString());
            XmlNode node = (XmlNode)xmlDOC.DocumentElement;
            XmlNodeList nodes = node.SelectNodes("//conference/confInfo/CustomAttributesList/CustomAttribute");
            if (nodes.Count > 0)
            {
                if (CAObj == null)
                    CAObj = new myVRMNet.CustomAttributes();
                //FB 2632 - Starts
                custControlIDs = CAObj.CreateCustomAttributes(nodes, tblCustomAttribute, true); //FB 2501 - Fixed during this case for Vnoc //FB 2632
                //FB 2632 - End

                tblCustomAttribute.Width = Unit.Percentage(90);
            }
            else
            {
                TableCell tCol = new TableCell();
                tCol.Text = "<br/> <br/>" + obj.GetTranslatedText("No Custom Options found.");//FB 1830 - Translation
                tCol.HorizontalAlign = HorizontalAlign.Center;
                TableRow tRow = new TableRow();
                tRow.Cells.Add(tCol);
                tRow.Visible = true;
                tblCustomAttribute.Rows.Add(tRow);
            }
        }
        #endregion
        /* *** Custom Attribute Fixes - end *** */

        #region Select Tree

        protected void SelectTree(object sender, EventArgs e)
        {
            selRooms = "";
            try
            {
                foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                    foreach (TreeNode tnMid in tnTop.ChildNodes)
                        foreach (TreeNode tn in tnMid.ChildNodes)
                        {
                            tn.Checked = false;

                            if (selectedloc.Value != "")
                            {
                                foreach (String s in selectedloc.Value.Split(','))
                                    if ((tn.Value.Equals(s.Trim())))
                                    {
                                        tn.Checked = true;
                                        selRooms += tn.Value + ",";

                                    }
                            }
                        }

                ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");
                lstRooms.Items.Clear();
                lstHKRooms.Items.Clear();
                lstRooms.Items.Add(li);
                lstHKRooms.Items.Add(li);

                if (treeRoomSelection.CheckedNodes.Count > 0)
                    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                        if (tn.Depth.Equals(3))
                        {
                            li = new ListItem(tn.Text, tn.Value);
                            lstRooms.Items.Add(li);
                            lstHKRooms.Items.Add(li);

                        }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        //Added for Template List
        #region UpdateTemplates 
        protected void UpdateTemplates(Object sender, EventArgs e)
        {
            try
            {
                if (!lstTemplates.SelectedValue.Equals("-1"))
                {
                    Session.Remove("confid");
                    Session.Add("confid", lstTemplates.SelectedValue);
                    //FB 1765 start
                    Session.Remove("confTempID");
                    Session.Add("confTempID", lstTemplates.SelectedValue);
                    //FB 1765 end
                    Response.Redirect("ConferenceSetup.aspx?t=t");
                }
                else
                {
                    Response.Redirect("ConferenceSetup.aspx?t=n&op=1");
                }

            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace("UpdateTemplates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        /****Recur Wo bug****/
        #region Get ConfID for WO
        private String GetConfIDforWO(string confID)
        {
            String WOConfID = "";
            try
            {
                WOConfID = confID;

                String[] conf = WOConfID.Split(',');

                if(conf.Length <= 1)
                    WOConfID = conf[0] + ",1" ;
                else
                {

                    if(conf[1] == "0" || conf[1].Trim() == "")
                        WOConfID = conf[0] + ",1";

                }

            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }
            return WOConfID;
        }
        #endregion

        #region CheckWOCount Real Time
        private int checkrealtimecount(string Iid)
        {
            int realtimecnt = 0;
            try
            {
                if (lblConfID.Text != "new") // code added for WO bug
                {
                    foreach (DataGridItem item in AVMainGrid.Items)
                    {

                        if (item.Cells[0].Text != "")
                        {
                            if (!item.ItemIndex.Equals(Session["AVEditColumn"]))
                            {
                                string tb = "";
                                tb = item.Cells[18].Text;
                                string iID;
                                string iQuantity;
                                string uID;
                                if (tb != "")
                                {
                                    for (int i = 0; i < tb.Split(';').Length - 1; i++)//FB 1830 (�- Alt 147)
                                    {
                                        uID = tb.Split(';')[i].Split('�')[1];
                                        iID = tb.Split(';')[i].Split('�')[0];
                                        iQuantity = tb.Split(';')[i].Split('�')[2];
                                        if (uID.Trim().Equals(""))
                                            uID = "0";
                                        if (uID == "0")
                                        {
                                            if (iID == Iid)
                                            {
                                                if (iQuantity.Trim() != "")
                                                    realtimecnt = realtimecnt + Convert.ToInt32(iQuantity);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }

            return realtimecnt;
        }
#endregion


        #region MethodToSplitAddress
        /// <summary>
        /// MethodToSplitAddress
        /// </summary>
        /// <param name="address"></param>
        /// <param name="mcuName"></param>
        /// <returns></returns>
        protected String[] SpiltAddress(String address, String mcuName)
        {
            String[] add = null;
            try
            {
                //if (mcuName.ToUpper().Contains("CODIAN"))
                //{
                //    address = address.Replace(",,,,,,,,", "+");
                //    address = address.Replace(",#,,,,,*,,,,,", "+");
                //    address = address.Replace(",#,,,,,1", "+");
                //}
                //else if (mcuName.ToUpper().Contains("MGC") || mcuName.ToUpper().Contains("POLYCOM"))
                //{
                //    address = address.Replace("pppppppp", "+");
                //    address = address.Replace("p#ppppp*ppppp", "+");
                //    address = address.Replace("p#ppppp1", "+");
                //}
                add = new String[3];
                if (address.Contains("D") && address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    if (address.IndexOf('+') > 0)
                    {
                        add[1] = address.Split('D')[1].Split('+')[0]; //Conference Code
                        add[2] = address.Split('D')[1].Split('+')[1]; // Leader Pin
                    }
                    else
                    {
                        add[1] = address.Split('D')[1]; //Conference Code
                        add[2] = ""; // Leader Pin
                    }
                }
                else if (address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    add[1] = address.Split('D')[1]; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                else if (!address.Contains("D") && !address.Contains("+"))
                {
                    add = new String[1];
                    add[0] = address;//Address
                    //add[1] = ""; //Conference Code
                    //add[2] = ""; // Leader Pin
                }
                return add;

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                throw ex;
            }
        }

        #endregion

        #region BindUsers
        protected void BindUsers(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView dr = e.Item.DataItem as DataRowView;

                    HtmlTableRow audioParams1 = (HtmlTableRow)e.Item.FindControl("AudioParams1");
                    HtmlTableRow audioParams2 = (HtmlTableRow)e.Item.FindControl("AudioParams2");
                    HtmlTableRow audioParams3 = (HtmlTableRow)e.Item.FindControl("AudioParams3");
                    DropDownList drpConnection = (DropDownList)e.Item.FindControl("lstConnection");
                    HtmlTable tbMCUandConn = (HtmlTable)e.Item.FindControl("tbMCUandConn"); //FB 2359
                    TextBox txtconfcode = (TextBox)e.Item.FindControl("txtConfCode");
                    Label lblconfcode = (Label)e.Item.FindControl("LblConfCode");
                    TextBox txtleaderpin = (TextBox)e.Item.FindControl("txtleaderPin");
                    Label lblleaderpin = (Label)e.Item.FindControl("LblLeaderpin");

                    txtconfcode.Attributes.Add("style", "display:none;");
                    lblconfcode.Attributes.Add("style", "display:none;");
                    txtleaderpin.Attributes.Add("style", "display:none;");
                    lblleaderpin.Attributes.Add("style", "display:none;");

                    if (dr["VideoEquipment"].ToString().Equals("0")) //FB 1769
                        dr["VideoEquipment"] = "-1";

                    if (dr["AddressType"].ToString().Equals("0"))   //FB 1769
                        dr["AddressType"] = "-1";

                    if (dr["connection"].ToString().Equals("1") && lstConferenceType.SelectedValue == "2")//code added for Audio addon //Code changed for FB 1744
                    {
                        if (enableAudioParams == "0")
                        {
                            audioParams1.Attributes.Add("style", "display:none;");
                            audioParams2.Attributes.Add("style", "display:none;");
                            audioParams3.Attributes.Add("style", "display:none;");
                            
                            if (client.ToUpper() == "DISNEY") //FB 2359
                                tbMCUandConn.Attributes.Add("style", "display:none;"); //FB 2359

                            ((DropDownList)e.Item.FindControl("lstAddressType")).SelectedValue = "1";
                            ((DropDownList)e.Item.FindControl("lstProtocol")).SelectedValue = "1";
                            ((DropDownList)e.Item.FindControl("lstLineRate")).SelectedValue = OrgLineRate.ToString(); //FB 2429
                            if (OrgLineRate <= 0)
                                ((DropDownList)e.Item.FindControl("lstLineRate")).SelectedValue = "384"; //Disney Fix
                            ((DropDownList)e.Item.FindControl("lstConnectionType")).SelectedValue = "2";
                        }

                        if (enableConferenceCode == "1")
                        {
                            txtconfcode.Attributes.Add("style", "display:block;");
                            lblconfcode.Attributes.Add("style", "display:block;");
                        }
                        else
                        {
                           txtconfcode.Text = "";  //FB 1734
                        }

                        if (enableLeaderPin == "1")
                        {
                            txtleaderpin.Attributes.Add("style", "display:block;");
                            lblleaderpin.Attributes.Add("style", "display:block;");
                        }
                        else
                        {
                            txtleaderpin.Text = ""; //FB 1734
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        /*** FB 1728 ****/
        #region CheckTime

        protected void CheckTime(object sender, EventArgs e)
        {
            String result = "false";
            String passwordResult = "";// FB 1865
            String tmzone = "26";
            String timezonedisplay = "";
            String confid = "";// FB 1865
            String skipCheck = "0";
            /** FB 2440 **/
            DateTime strtDateTime = DateTime.MinValue;
            DateTime sDateTime = DateTime.MinValue;
            DateTime tDateTime = DateTime.MinValue;
            DateTime dEnd = DateTime.MinValue; 
            Int32 iSDur = -1, iTDur = -1,iMCUResult = 0;
            String sMcuResult = "-1";
            /** FB 2440 **/

            try
            {
                //string date = ((Recur.Value == "") ? confStartDate.Text : StartDate.Text);
                //FB 1911 - Start
                string date = "";
                date = ((Recur.Value == "") ? confStartDate.Text : StartDate.Text);

                if (RecurSpec.Value != "")
                    date = RecurSpec.Value.Split('#')[2].Split('&')[0];
                //FB 1911 - End

                timezonedisplay = timeZone;//((Request.QueryString["t"].ToString().Equals("o") && flagClone.Equals(false)) ? timeZone : "0");

                if (chkRecurrence.Checked || chkStartNow.Checked || !RecurSpec.Value.ToString().Equals(""))
                    skipCheck = "1";


                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                confid = lblConfID.Text; //FB 1865

                if (Request.QueryString["t"] != null)//FB 1865
                {

                    if (Request.QueryString["t"].ToString().Equals("o"))
                        confid = "new";
                }

                /** FB 2440 **/
                if (chkEnableBuffer.Checked)
                {
                    if (Recur.Value.Trim() != "")
                    {
                        if (hdnSetupTime.Value == "" || hdnSetupTime.Value == null)
                            hdnSetupTime.Value = "0";

                        if (hdnTeardownTime.Value == "" || hdnTeardownTime.Value == null)
                            hdnTeardownTime.Value = "0";

                        Int32.TryParse(hdnSetupTime.Value, out iSDur);
                        iTDur = 16;
                    }
                    else
                    {
						//FB 2634
                        //sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text);
                        //tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text) + " " + TeardownTime.Text);
                        //strtDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(date) + " " + confStartTime.Text);
                        //dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text); 
                        //iSDur =(int) sDateTime.Subtract(strtDateTime).TotalMinutes;
                        //iTDur = (int)dEnd.Subtract(tDateTime).TotalMinutes;
                        iSDur = Int32.Parse(SetupDuration.Text);
                        iTDur = Int32.Parse(TearDownDuration.Text);
                    }
                }
                /** FB 2440 **/


                string resXML;
                string inputXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><hostID>" + hdnApprover4.Text + "</hostID><systemDate>" + Session["systemDate"].ToString() + "</systemDate><systemTime>" + Session["systemTime"].ToString() + "</systemTime><confDate>" + myVRMNet.NETFunctions.GetDefaultDate(date) + "</confDate><confTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text) + "</confTime><timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone><timezoneDisplay>" + timezonedisplay + "</timezoneDisplay><password>" + confPassword.Value + "</password><confID>" + confid + "</confID><skipCheck>" + skipCheck + "</skipCheck><mcuSetup>" + iSDur.ToString() + "</mcuSetup><mcuTeardonw>" + iTDur.ToString() + "</mcuTeardonw></login>";//FB 1865 FB 2440 //FB 2588

                resXML = obj.CallMyVRMServer("Isconferenceschedulable", inputXML, Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(resXML);


                if (timeZone == "0")
                {
                    if (xmlDOC.SelectSingleNode("/login/hosttimezone") != null)
                        tmzone = xmlDOC.SelectSingleNode("/login/hosttimezone").InnerText;

                    if (lstConferenceTZ.Items.FindByValue(tmzone) != null)// && (Request.QueryString["t"].ToString().Equals("o") && flagClone.Equals(false)))
                    {
                        lstConferenceTZ.ClearSelection();
                        lstConferenceTZ.Items.FindByValue(tmzone).Selected = true;
                    }

                }
                if (xmlDOC.SelectSingleNode("/login/result") != null)
                    result = xmlDOC.SelectSingleNode("/login/result").InnerText;

                if (xmlDOC.SelectSingleNode("/login/passwordCheck") != null)
                    passwordResult = xmlDOC.SelectSingleNode("/login/passwordCheck").InnerText;
                /** FB 2440 **/
                if (xmlDOC.SelectSingleNode("/login/mcuresult") != null)
                    sMcuResult = xmlDOC.SelectSingleNode("/login/mcuresult").InnerText;

                Int32.TryParse(sMcuResult.Trim(),out iMCUResult);
                /** FB 2440 **/                

                if (result.ToUpper().Equals("FALSE"))
                {
                    btnNext.Visible = true;

                    String tmezne = "";

                    if (timeZone == "0")
                        tmezne = " host timezone.(" + lstConferenceTZ.SelectedItem.Text + ")";
                    else
                        tmezne = " selected conference timezone.";



                    errLabel.Visible = true;                    
                    errLabel.Text = obj.GetTranslatedText("Invalid Start Date or Time. It should be greater than current time in") + tmezne;//FB 1830 - Translation

                    /** FB 2440 **/
                    if (iMCUResult == 1)
                        errLabel.Text = obj.GetTranslatedText("Setup time cannot be less than the MCU pre start time.");
                    else if(iMCUResult == 2)
                        errLabel.Text = obj.GetTranslatedText("Teardown time cannot be less than the MCU pre end time.");
                    /** FB 2440 **/


					//FB 1716
                    this.RegisterStartupScript("initial", "<script>fnEnableBuffer();fnShow()</script>");

                    //Wizard1.ActiveViewIndex = 0; FB 2692

                    //TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true; FB 2692
                    isCheckTimeError = true; // FB 2692


                }
                // FB 1865
                if (passwordResult == "0" && lstConferenceType.SelectedValue != "7")
                {
                    btnNext.Visible = true;

                    
                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("Please check the password. It must be unique");//FB 1830 - Translation

                    //FB 1716
                    this.RegisterStartupScript("initial", "<script>fnEnableBuffer();openRecur()</script>");
                    //this.RegisterStartupScript("initial", "<script>fnEnableBuffer();fnShow()</script>");

                    //Wizard1.ActiveViewIndex = 0; FB 2692

                    //TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true; FB 2692
                    isCheckTimeError = true; // FB 2692
                }
                // FB 1865
                //string[] tmpstrs = hdnConceirgeSupp.Value.Split(',');
                //Commented for FB 2359
                //FB 2341 start
                //int tmpint = ((hdnConceirgeSupp.Value.Trim()).Equals("")) ? -1 : tmpstrs.Length;
                //for (int i = 0; i < tmpint; i++)
                //    ChklstConcSupport.Items[Convert.ToInt16(tmpstrs[i]) - 1].Selected = true;
                ////FB 2341 end
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        //Method added for FB 1830 Email Edit - start
        #region GetXConfParams
        /// <summary>
        /// GetXConfParams
        /// </summary>
        /// <returns></returns>
        private void GetXConfParams()
        {
            xPartys = new List<int>();
            try
            {
                string xconfpartys = "";
                if (Session["XCONFINFO"] != null)
                {
                    xConfInfo = (StringDictionary)Session["XCONFINFO"];
                    if (xConfInfo != null)
                    {
                        if (xConfInfo["partys"] != null)
                            xconfpartys = xConfInfo["partys"];

                        if (xConfInfo["password"] != null)
                            xconfpassword = xConfInfo["password"];

                        if (xConfInfo["setupdate"] != null)
                            DateTime.TryParse(xConfInfo["setupdate"], out xconfsetup);

                        if (xConfInfo["teardate"] != null)
                            DateTime.TryParse(xConfInfo["teardate"], out xconftear);

                        if (xconfpartys != "")
                        {
                            string[] xcfpartys = xconfpartys.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries); //FB 1888 //Partys before edit
                            int xpartid = 0;
                            for (int p = 0; p < xcfpartys.Length; p++)
                            {
                                int.TryParse(xcfpartys[p], out xpartid);
                                if (xpartid > 0)
                                {
                                    if (!xPartys.Contains(xpartid))
                                        xPartys.Add(xpartid);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Trace(e.Message);
            }
        }
        #endregion

        #region CheckForUserInput
        /// <summary>
        /// CheckForUserInput
        /// </summary>
        /// <returns></returns>
        private bool CheckForUserInput()
        {
            try
            {
                if (txtPartysInfo.Text.Trim() == "")
                    return false; //No participants

                GetXConfParams();
                //FB 2634
                DateTime sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                DateTime tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                //DateTime sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text);
                //DateTime tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text) + " " + TeardownTime.Text);
                
                if (confPassword.Value != xconfpassword || xconfsetup != sDateTime || xconftear != tDateTime)
                    return false;  //send email to all partys without checking


                string[] partysary = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries); //FB 1888

                int partynum = partysary.Length;

                if ((partynum - 1) == 0)
                    return false; //No participants

                //int usrid = 0;
                //for (int i = 0; i < partynum - 1; i++)
                //{
                //    string[] partyary = partysary[i].Split(',');
                //    usrid = 0;
                //    int.TryParse(partyary[0].Trim(), out usrid);
                //    if (usrid < 0)
                //        usrid = 0;

                //    if (!xPartys.Contains(usrid))
                //    {
                //        isNewPartysAdded = true;
                //        break;
                //    }
                //}
                //if (isNewPartysAdded)
                //{
                //    emailAlertMes = "New participant(s) are added into conference. Do you want to notify all participant(s)?";
                //    return true; //Get user input for sending emails. if he says NO to all partys
                //    //then send mail only to newly added party
                //}
                //if ((partynum - 1) != xPartys.Count && isNewPartysAdded == false) //party removed - get user input for sending mails
                //{
                //    emailAlertMes = "Participant(s) has been removed from the conference. Do you want to notify all participant(s)?";
                //    return true;
                //}
                //emailAlertMes = "Do you want to notify all participant(s)?";
                return true;
            }
            catch (Exception e)
            {
                log.Trace(e.Message);
                return false;
            }
        }
        #endregion
        //Method added for FB 1830 Email Edit - end

        //FB 2274
        #region CrossSiloSession
        /// <summary>
        /// CrossSiloSession
        /// </summary>
        private void CrossSiloSession()
        {
            if (hdnCrossConferenceCode.Value != null && hdnCrossConferenceCode.Value != "")
                enableConferenceCode = hdnCrossConferenceCode.Value;
            else if (Session["ConferenceCode"] != null)
                enableConferenceCode = Session["ConferenceCode"].ToString();

            if (hdnCrossLeaderPin.Value != null && hdnCrossLeaderPin.Value != "")
                enableLeaderPin = hdnCrossLeaderPin.Value;
            else if (Session["LeaderPin"] != null)
                enableLeaderPin = Session["LeaderPin"].ToString();

            if (hdnCrossAdvAvParams.Value != null && hdnCrossAdvAvParams.Value != "")
                enableAdvAvParams = hdnCrossAdvAvParams.Value;
            else if (Session["AdvAvParams"] != null)
                enableAdvAvParams = Session["AdvAvParams"].ToString();

            if (hdnCrossAudioParams.Value != null && hdnCrossAudioParams.Value != "")
                enableAudioParams = hdnCrossAudioParams.Value;
            else if (Session["AudioParams"] != null)
                enableAudioParams = Session["AudioParams"].ToString();

            if (hdnCrossisMultiLingual != null && hdnCrossisMultiLingual.Value != "")
                isMulti = hdnCrossisMultiLingual.Value;
            else if (Session["isMultiLingual"] != null)
                isMulti = Session["isMultiLingual"].ToString();

            if (hdnCrossroomExpandLevel != null && hdnCrossroomExpandLevel.Value != "")
                roomExpand = hdnCrossroomExpandLevel.Value;
            else
                roomExpand = Session["roomExpandLevel"].ToString();

            if (enableAdvAvParams == "0")
                trAVCommonSettings.Attributes.Add("style", "display:none");

            if (hdnCrossEnableBufferZone != null && hdnCrossEnableBufferZone.Value != "")
                enableBufferZone = hdnCrossEnableBufferZone.Value;
            else
                enableBufferZone = Session["EnableBufferZone"].ToString();

           

            if (hdnCrossEnableEntity != null && hdnCrossEnableEntity.Value != "")
                enableEntity = hdnCrossEnableEntity.Value;

            //FB 2451 - Start
            //if (hdnCrossdefaultPublic != null && hdnCrossdefaultPublic.Value != "")
            //{
            //    if (hdnCrossdefaultPublic.Value.Equals("1"))
            //    {
            //        chkPublic.Checked = true;
            //    }
            //}
            //else if (Session["defaultPublic"].ToString().Equals("1"))
            //    chkPublic.Checked = true;

            if (hdnCrossdefaultPublic != null && hdnCrossdefaultPublic.Value != "")
                defaultPublic = hdnCrossdefaultPublic.Value;
            else if (Session["defaultpublic"] != null)
                defaultPublic = Session["defaultpublic"].ToString();                
            if ((defaultPublic == "1" && chkPublic.Checked == true) || (defaultPublic == "0" && chkPublic.Checked == true))
                chkPublic.Checked = true;
            else if ((defaultPublic == "1" && chkPublic.Checked == false) || (defaultPublic == "0" && chkPublic.Checked == false))
                chkPublic.Checked = false;
            //FB 2451 - End

            if (hdnCrossSetupTime.Value.Trim() != "" && hdnCrossSetupTime.Value.Trim() != "-1") //FB 2398
                Int32.TryParse(hdnCrossSetupTime.Value, out OrgSetupTime);
            else if (Session["OrgSetupTime"] != null)
            {
                if (Session["OrgSetupTime"].ToString() != null)
                    Int32.TryParse(Session["OrgSetupTime"].ToString(), out OrgSetupTime);
            }

            if (hdnCrossTearDownTime.Value.Trim() != "" && hdnCrossTearDownTime.Value.Trim() != "-1") //FB 2398
                Int32.TryParse(hdnCrossTearDownTime.Value.Trim(), out OrgTearDownTime);
            else if (Session["OrgTearDownTime"] != null)
            {
                if (Session["OrgTearDownTime"].ToString() != "")
                    Int32.TryParse(Session["OrgTearDownTime"].ToString(), out OrgTearDownTime);
            }

            if (hdnNetworkSwitching != null && hdnNetworkSwitching.Value != "") //FB 2993
                int.TryParse(hdnNetworkSwitching.Value, out NetworkSwitching);
            else if (Session["NetworkSwitching"] != null)
                int.TryParse(Session["NetworkSwitching"].ToString(), out NetworkSwitching);

            //FB 2641 start

            if (hdnCrossEnableLinerate != null && hdnCrossEnableLinerate.Value != "")
                int.TryParse(hdnCrossEnableLinerate.Value, out EnableLinerate);
            else if (Session["EnableLinerate"] != null)
                int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);

            if (hdnCrossEnableStartMode != null && hdnCrossEnableStartMode.Value != "")
                int.TryParse(hdnCrossEnableStartMode.Value, out EnableStartMode);
            else if (Session["EnableStartMode"] != null)
                int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);

            // FB 2641 End

            //FB 2694 - Starts
            if (hdnCrossisVIP != null && hdnCrossisVIP.Value != "")
                EnableIsVip = hdnCrossisVIP.Value;
            else if (Session["isVIP"] != null)
                EnableIsVip = Session["isVIP"].ToString();

            if (hdnCrossEnableRoomServiceType != null && hdnCrossEnableRoomServiceType.Value != "")
                EnableServiceType = hdnCrossEnableRoomServiceType.Value;
            else if (Session["EnableRoomServiceType"] != null)
                EnableServiceType = Session["EnableRoomServiceType"].ToString();
            //FB 2694 - End
		
         }
        #endregion

        //FB 2426 - Start

        #region Video Guest Location Submit
        /// <summary>
        /// Video Guest Location Submit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnGuestLocationSubmit(object sender, EventArgs e)
        {
            DataRow dr = null;
            onflyGrid = (DataTable)Session["OnlyFlyRoom"];
            if (onflyGrid == null || onflyGrid.Rows.Count.Equals(0))
            {
                CreateDtColumnNames();                  
                onflyGrid = obj.LoadDataTable(null, colNames);
                
                if (!onflyGrid.Columns.Contains("RowUID"))
                    onflyGrid.Columns.Add("RowUID");
            }
            
            for (int i = 0; i < onflyGrid.Rows.Count; i++)
            {
                if (onflyGrid.Rows[i]["RowUID"].ToString().Trim() != hdnGuestloc.Value.Trim()
                    && onflyGrid.Rows[i]["RoomName"].ToString().Trim() == txtsiteName.Text.Trim())
                {
                    errLabel.Text = obj.GetTranslatedText("Room Name has already been used");
                    errLabel.Visible = true;
                    return;
                }
            }

            dr = onflyGrid.NewRow();
            dr["RowUID"] = onflyGrid.Rows.Count;
            dr["RoomID"] = hdnGuestRoomID.Value;
            dr["RoomName"] = obj.ControlConformityCheck(txtsiteName.Text.Trim()); // ZD 100263
            dr["ContactName"] = obj.ControlConformityCheck(txtApprover5.Text.Trim()); // ZD 100263
            dr["ContactEmail"] = obj.ControlConformityCheck(txtEmailId.Text.Trim()); // ZD 100263
            dr["ContactPhoneNo"] = obj.ControlConformityCheck(txtPhone.Text.Trim()); // ZD 100263
            dr["ContactAddress"] = obj.ControlConformityCheck(txtAddress.Text.Trim()); // ZD 100263
            dr["State"] = obj.ControlConformityCheck(txtState.Text.Trim()); // ZD 100263
            dr["City"] = obj.ControlConformityCheck(txtCity.Text.Trim()); // ZD 100263
            dr["ZIP"] = obj.ControlConformityCheck(txtZipcode.Text.Trim()); // ZD 100263
            dr["Country"] = lstCountries.SelectedValue.ToString();
            dr["IPAddressType"] = "1";
            dr["IPAddress"] = obj.ControlConformityCheck(txtIPAddress.Text.Trim()); // ZD 100263
            dr["IPPassword"] = txtIPPassword.Text.Trim();
            dr["IPconfirmPassword"] = txtIPconfirmPassword.Text.Trim();
            dr["IPMaxLineRate"] = lstIPlinerate.SelectedValue.ToString();
            dr["IPConnectionType"] = lstIPConnectionType.SelectedValue.ToString();
            if (radioIsDefault.Checked)
            {
                dr["IsIPDefault"] = "1";
                dr["DefaultAddressType"] = obj.GetTranslatedText("IP Address");
                dr["DefaultAddress"] = obj.ControlConformityCheck(txtIPAddress.Text.Trim()); // ZD 100263
                dr["DefaultConnetionType"] = lstIPConnectionType.SelectedItem.Text.Trim();
            }
            else
                dr["IsIPDefault"] = "0";

            dr["SIPAddressType"] = "6";
            dr["SIPAddress"] = obj.ControlConformityCheck(txtSIPAddress.Text.Trim()); // ZD 100263
            dr["SIPPassword"] = txtSIPPassword.Text.Trim();
            dr["SIPconfirmPassword"] = txtSIPconfirmPassword.Text.Trim(); 
            dr["SIPMaxLineRate"] = lstSIPlinerate.SelectedValue.ToString();
            dr["SIPConnectionType"] = lstSIPConnectionType.SelectedValue.ToString();
            if (radioIsDefault2.Checked)
            {
                dr["IsSIPDefault"] = "1";
                dr["DefaultAddressType"] = obj.GetTranslatedText("E164/SIP Address");
                dr["DefaultAddress"] = obj.ControlConformityCheck(txtSIPAddress.Text.Trim()); // ZD 100263
                dr["DefaultConnetionType"] = lstSIPConnectionType.SelectedItem.Text.Trim();
            }
            else
                dr["IsSIPDefault"] = "0";
            dr["ISDNAddressType"] = "4";
            dr["ISDNAddress"] = obj.ControlConformityCheck(txtISDNAddress.Text.Trim()); // ZD 100263
            dr["ISDNPassword"] = txtISDNPassword.Text.Trim();
            dr["ISDNconfirmPassword"] = txtISDNconfirmPassword.Text.Trim(); 
            dr["ISDNMaxLineRate"] = lstISDNlinerate.SelectedValue.ToString();
            dr["ISDNConnectionType"] = lstISDNConnectionType.SelectedValue.ToString();
            if (radioIsDefault3.Checked)
            {
                dr["IsISDNDefault"] = "1";
                dr["DefaultAddressType"] = obj.GetTranslatedText("ISDN Address");
                dr["DefaultAddress"] = obj.ControlConformityCheck(txtISDNAddress.Text.Trim()); // ZD 100263
                dr["DefaultConnetionType"] = lstISDNConnectionType.SelectedItem.Text.Trim();
            }
            else
                dr["IsISDNDefault"] = "0";

            if (hdnGuestRoom.Value != "")
            {
                if (Convert.ToInt32(hdnGuestRoom.Value) == -1)
                {
                    onflyGrid.Rows.Add(dr);
                }
                else
                {
                    onflyGrid.Rows.RemoveAt(Convert.ToInt32(hdnGuestRoom.Value));
                    onflyGrid.Rows.InsertAt(dr, Convert.ToInt32(hdnGuestRoom.Value));
                }
            }
            BindOptionData();
            dgOnflyGuestRoomlist.Visible = true;
        }
        #endregion video guest location submit

        #region video guest location Cancel
        /// <summary>
        /// video guest location Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnGuestLocationCancel(object sender, EventArgs e)
        {
            try
            {
                guestLocationPopup.Hide();
            }
            catch (Exception ex)
            {
                log.Trace("fnGuestLocationCancel" + ex.Message);
            }
        }
        #endregion video guest location Cancel

        #region Bind Option Data
        /// <summary>
        /// Bind Option Data
        /// </summary>
        private void BindOptionData() 
        {
            try
            {
                if (onflyGrid == null)
                    onflyGrid = new DataTable();

                if (Session["OnlyFlyRoom"] == null)
                    Session.Add("OnlyFlyRoom", onflyGrid);
                else
                    Session["OnlyFlyRoom"] = onflyGrid;

                dgOnflyGuestRoomlist.DataSource = onflyGrid;
                dgOnflyGuestRoomlist.DataBind();

                if (onflyGrid.Rows.Count.Equals(0))
                    dgOnflyGuestRoomlist.Visible = false;
                else
                    dgOnflyGuestRoomlist.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("BindOptionData" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region dgOnflyGuestRoomlist_Edit
        /// <summary>
        /// dgOnflyGuestRoomlist_Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgOnflyGuestRoomlist_Edit(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                int n = e.Item.ItemIndex;
                hdnGuestRoom.Value = n.ToString();

                if (Session["OnlyFlyRoom"] != null)
                    onflyGrid = (DataTable)Session["OnlyFlyRoom"];

                DataRow dr = onflyGrid.Rows[n];
                hdnGuestloc.Value = dr["RowUID"].ToString();
                hdnGuestRoomID.Value = dr["RoomID"].ToString();
                txtsiteName.Text = dr["RoomName"].ToString();
                txtApprover5.Text = dr["ContactName"].ToString();
                txtEmailId.Text = dr["ContactEmail"].ToString();
                txtPhone.Text = dr["ContactPhoneNo"].ToString();
                txtAddress.Text = dr["ContactAddress"].ToString();
                if (dr["State"].ToString().Trim() == "55")
                    dr["State"] = "AB";
                else if (dr["State"].ToString().Trim() == "68")
                    dr["State"] = "AGS";
                else
                    dr["State"] = "NY"; 
                txtState.Text = dr["State"].ToString();
                txtCity.Text = dr["City"].ToString();
                txtZipcode.Text = dr["ZIP"].ToString();
                lstCountries.ClearSelection();
                lstCountries.SelectedValue = dr["Country"].ToString();
                txtIPAddress.Text = dr["IPAddress"].ToString();
                txtIPPassword.Text = dr["IPPassword"].ToString();
                txtIPconfirmPassword.Text = dr["IPconfirmPassword"].ToString();
                lstIPlinerate.ClearSelection();
                if (dr["IPMaxLineRate"].ToString().Trim() == "")
                    dr["IPMaxLineRate"] = -1;
                lstIPlinerate.SelectedValue = dr["IPMaxLineRate"].ToString();
                lstIPConnectionType.ClearSelection();
                if (dr["IPConnectionType"].ToString().Trim() == "")
                    dr["IPConnectionType"] = -1;
                lstIPConnectionType.SelectedValue = dr["IPConnectionType"].ToString();
                if (dr["IsIPDefault"].ToString() == "1")
                    radioIsDefault.Checked = true;
                else
                    radioIsDefault.Checked = false;
                txtISDNAddress.Text = dr["ISDNAddress"].ToString();
                txtISDNPassword.Text = dr["ISDNPassword"].ToString();
                txtISDNconfirmPassword.Text = dr["ISDNconfirmPassword"].ToString();
                lstISDNlinerate.ClearSelection();
                if (dr["ISDNMaxLineRate"].ToString().Trim() == "")
                    dr["ISDNMaxLineRate"] = -1;
                lstISDNlinerate.SelectedValue = dr["ISDNMaxLineRate"].ToString();
                lstISDNConnectionType.ClearSelection();
                if (dr["ISDNConnectionType"].ToString().Trim() == "")
                    dr["ISDNConnectionType"] = -1;
                lstISDNConnectionType.SelectedValue = dr["ISDNConnectionType"].ToString();
                if (dr["IsISDNDefault"].ToString() == "1")
                    radioIsDefault3.Checked = true;
                else
                    radioIsDefault3.Checked = false;
                txtSIPAddress.Text = dr["SIPAddress"].ToString();
                txtSIPPassword.Text = dr["SIPPassword"].ToString();
                txtSIPconfirmPassword.Text = dr["SIPconfirmPassword"].ToString();
                lstSIPlinerate.ClearSelection();
                if (dr["SIPMaxLineRate"].ToString().Trim() == "")
                    dr["SIPMaxLineRate"] = -1;
                lstSIPlinerate.SelectedValue = dr["SIPMaxLineRate"].ToString();
                lstSIPConnectionType.ClearSelection();
                if (dr["SIPConnectionType"].ToString().Trim() == "")
                    dr["SIPConnectionType"] = -1;
                lstSIPConnectionType.SelectedValue = dr["SIPConnectionType"].ToString();
                if (dr["IsSIPDefault"].ToString() == "1")
                    radioIsDefault2.Checked = true;
                else
                    radioIsDefault2.Checked = false;
                btnGuestLocationSubmit.Text = obj.GetTranslatedText("Update");
                guestLocationPopup.Show();
            }
            catch (Exception ex)
            {
                log.Trace("dgOnflyGuestRoomlist_Edit" + ex.Message);
            }
        }

        #endregion

        #region dgOnflyGuestRoomlist_DeleteCommand 
        /// <summary>
        /// dgOnflyGuestRoomlist_DeleteCommand 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgOnflyGuestRoomlist_DeleteCommand(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                onflyGrid = (DataTable)Session["OnlyFlyRoom"];
                int i = e.Item.ItemIndex;
                onflyGrid.Rows[i].Delete();
                BindOptionData();
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.Message;
                //ZD 100263
                log.Trace("dgOnflyGuestRoomlist_DeleteCommand" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region Create Column Names
        /// <summary>
        /// Create Column Names
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("RoomID");
            colNames.Add("RoomName");
            colNames.Add("ContactName");
            colNames.Add("ContactEmail");
            colNames.Add("ContactPhoneNo");
            colNames.Add("ContactAddress");
            colNames.Add("State");
            colNames.Add("City");
            colNames.Add("ZIP");
            colNames.Add("Country");
            colNames.Add("IPAddressType");
            colNames.Add("IPAddress");
            colNames.Add("IPPassword");
            colNames.Add("IPconfirmPassword");
            colNames.Add("IPMaxLineRate");
            colNames.Add("IPConnectionType");
            colNames.Add("IsIPDefault");
            colNames.Add("SIPAddressType");
            colNames.Add("SIPAddress");
            colNames.Add("SIPPassword");
            colNames.Add("SIPconfirmPassword");
            colNames.Add("SIPMaxLineRate");
            colNames.Add("SIPConnectionType");
            colNames.Add("IsSIPDefault");
            colNames.Add("ISDNAddressType");
            colNames.Add("ISDNAddress");
            colNames.Add("ISDNPassword");
            colNames.Add("ISDNconfirmPassword");
            colNames.Add("ISDNMaxLineRate");
            colNames.Add("ISDNConnectionType");
            colNames.Add("IsISDNDefault");
            colNames.Add("DefaultAddressType");
            colNames.Add("DefaultAddress");
            colNames.Add("DefaultConnetionType");

        }
        #endregion
        //FB 2448 
        #region ShowHideAVforVMR

        protected void ShowHideAVforVMR(object sender, EventArgs e)
        {
            try
            {
                if (hdnCrosshkModule != null && hdnCrosshkModule.Value != "")
                    hkModule = hdnCrosshkModule.Value;
                else if (Session["hkModule"] != null)
                    hkModule = Session["hkModule"].ToString();

                if (lstVMR.SelectedIndex > 0 || chkPCConf.Checked || enableAV == "0" || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) 
                    || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking) || (isCloudEnabled == 1 && chkCloudConferencing.Checked)) // FB 2620 //FB 2819
                {
                    TopMenu.Items[3].Text = "";
                    
                    //FB 2694 Starts
                    if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking)) 
                    {
                        TopMenu.Items[1].Text = "";
                        TopMenu.Items[3].Text = "";
                        TopMenu.Items[6].Text = "";
                    }
                    else
                    {
                        if (enablePar.Equals("1"))
                            TopMenu.Items[1].Text = "<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>" + obj.GetTranslatedText("Select") + "<br />" + obj.GetTranslatedText("Participants") + "</div>";//FB 2835
                        if (hkModule.Equals("1"))
                            TopMenu.Items[6].Text = "<div align='center' style='width:123'onclick='javascript:return SubmitRecurrence();'>" + obj.GetTranslatedText("Select") + "<br />" + obj.GetTranslatedText("Facility") + "</div>";//FB 2835
                    }
                    //FB 2694 End
                    dgRooms.DataSource = null;//FB 2606
                    dgRooms.DataBind();

                    dgUsers.DataSource = null;
                    dgUsers.DataBind();
                }
                else
                {
                    WizardStep wsTemp = new WizardStep();
                    if (this.SelectAudioVisual != null)
                    {
                        if (client.ToUpper() == "DISNEY") //FB 1985
                            TopMenu.Items[3].Text = "<div align='center' style='width:123'><b>Audio</b><br><b>Settings</b></div>";
                        else
                            TopMenu.Items[3].Text = "<div align='center' style='width:123'onclick='javascript:return SubmitRecurrence();'>" + obj.GetTranslatedText("Audio/Video") + "<br />" + obj.GetTranslatedText("Settings") + "</div>";//FB 1737 //FB 1830 - Translation

                    }
                    if (enablePar.Equals("1"))
                        TopMenu.Items[1].Text = "<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>" + obj.GetTranslatedText("Select") + "<br />" + obj.GetTranslatedText("Participants") + "</div>";//FB 2694
                    if (hkModule.Equals("1"))
                        TopMenu.Items[6].Text = "<div align='center' style='width:123'onclick='javascript:return SubmitRecurrence();'>" + obj.GetTranslatedText("Select") + "<br />" + obj.GetTranslatedText("Facility") + "</div>";//FB 2694
                }

                //FB 2819 Starts
                trVMRcall.Attributes.Add("Style", "display:");
                if (lstVMR.SelectedIndex > 0)
                {
                    trPCConf.Attributes.Add("Style", "display:none");
                    trStartMode.Attributes.Add("Style", "display:none");

                }
                else if (chkPCConf.Checked)
                {
                    trVMR.Attributes.Add("Style", "display:none");
                    trStartMode.Attributes.Add("Style", "display:none");
                    trStartMode1.Attributes.Add("style", "display:none");
                    trVMRcall.Attributes.Add("Style", "display:none");
                }
                //FB 2819 Ends
            }
            catch (Exception ex)
            {
                log.Trace(ex.ToString());
            }

        }

        #endregion

        //FB 2426 - End

		//FB 2659 - Starts
        #region CheckSeatsAvailability
        /// <summary>
        /// CheckSeatsAvailability
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckSeatsAvailability(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                DateTime dStart, dEnd;
                int setupDuration = 0, tearDuration = 0;

                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                if (!chkEnableBuffer.Checked && enableBufferZone == "1")
                {
                    int.TryParse(SetupDuration.Text, out setupDuration);
                    int.TryParse(TearDownDuration.Text, out tearDuration);
                    dStart = dStart.AddMinutes(-setupDuration);
                    dEnd = dEnd.AddMinutes(tearDuration);
                }

                if (dEnd.Subtract(dStart).TotalMinutes < 15)
                {
                    errLabel.Text = obj.GetTranslatedText("Invalid Duration.Conference duration should be minimum of 15 mins.");//FB 1830 - Translation
                    errLabel.Visible = true;
                    return;
                }

                inXML.Append("<TDGetSlotAvailableSeats>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                if (lblConfID.Text == "new")
                    inXML.Append("<editConfId></editConfId>");
                else
                    inXML.Append("<editConfId>" + lblConfID.Text + "</editConfId>");

                inXML.Append("<startDate>" + dStart + "</startDate>");
                inXML.Append("<endDate>" + dEnd + "</endDate>");
                inXML.Append("<timezone>" + lstConferenceTZ.SelectedValue + "</timezone>");
                inXML.Append("</TDGetSlotAvailableSeats>");

                string outXML = obj.CallMyVRMServer("TDGetSlotAvailableSeats", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(outXML);

                int totalOrgSeats = 0;
                if (xd.SelectSingleNode("//TDGetSlotAvailableSeats/TotalOrgSeats") != null)
                    int.TryParse(xd.SelectSingleNode("//TDGetSlotAvailableSeats/TotalOrgSeats").InnerText.Trim(), out  totalOrgSeats);

                if (totalOrgSeats <= 0) //FB 3062
                {
                    errLabel.Text = obj.GetTranslatedText("Insufficient Seats. Please Contact Your VRM Administrator.");
                    errLabel.Visible = true;
                }
                else
                {
                    XmlNodeList nodes = xd.SelectNodes("//TDGetSlotAvailableSeats/availableSeatsList/availableSeats");

                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    if (nodes != null)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            xtr = new XmlTextReader(nodes[i].OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                    }
                    DataTable dt = new DataTable();

                    HtmlTableRow trRow;
                    HtmlTableCell tdCell;
                    string tdConfDateId = "";
                    DateTime startDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;

                    tblSeatsAvailability.BorderColor = "Black";

                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                        for (int i = 0; i < 2; i++)
                        {
                            trRow = new HtmlTableRow();
                            if (i == 0)
                            {
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    tdCell = new HtmlTableCell();
                                    tdCell.NoWrap = false;
                                    tdCell.Align = "left";
                                    tdCell.ID = "tdConfDate" + j;

                                    startDate = Convert.ToDateTime(dt.Rows[j]["startDate"].ToString());
                                    endDate = Convert.ToDateTime(dt.Rows[j]["endDate"].ToString());

                                    tdCell.InnerHtml = "Start Date/Time: " + startDate.ToString(format) + " " + startDate.ToString(tformat);
                                    tdCell.InnerHtml += "<br>End Date/Time: " + endDate.ToString(format) + " " + endDate.ToString(tformat);
                                    tdCell.InnerHtml += "<br>Available Seats: " + dt.Rows[j]["seatsNumber"].ToString();
                                    trRow.Height = "100px";
                                    trRow.Cells.Add(tdCell);
                                }
                            }
                            else
                            {
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    tdCell = new HtmlTableCell();
                                    tdConfDateId = "tdConfDate" + j;
                                    tdCell.Style.Add("cursor", "pointer");

                                    if (dt.Rows[j]["seatsNumber"].ToString() == totalOrgSeats.ToString())
                                        tdCell.BgColor = "#65FF65";//Green-Totally Free.
                                    else
                                        tdCell.BgColor = "#F8F075";//Yellow-Partially Free.

                                    tdCell.Attributes.Add("onclick", "javascript:SelectOneDefault('" + tdConfDateId + "');");
                                    trRow.Height = "100px";
                                    trRow.Cells.Add(tdCell);
                                }
                            }
                            tblSeatsAvailability.Rows.Add(trRow);
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "seats", "fnShowSeats();", true);
                }
            }
            catch (Exception ex)
            {
                log.Trace("CheckSeatsAvailability: " + ex.Message);
                //ZD 100263
                //errLabel.Text = ex.Message;
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion
		//FB 2659 - End

        //FB 2693 Start
        #region FetchPCDetails
        /// <summary>
        /// fnFetchPCDetails
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="PcType"></param>
        /// <returns></returns>
        [WebMethod]
        public static string fnFetchPCDetails(string userID, string pctype)
        {
            myVRMNet.NETFunctions obj;
            obj = new myVRMNet.NETFunctions();
            ns_Logger.Logger log;
            log = new ns_Logger.Logger();
            string res = "";
            StringBuilder inXML = new StringBuilder();
            String outXML = "";
            int PCId = 0;
            string htmlcontent = "";
            string Description = "", SkypeURL = "", VCDialinIP = "", VCMeetingID = "", VCDialinSIP = "", VCDialinH323 = "", VCPin = "", PCDialinNum = "";
            string PCDialinFreeNum = "", PCMeetingID = "", PCPin = "", Intructions = "";
            try
            {
                inXML.Append("<FetchPCDetails>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + userID + "</userID>");
                inXML.Append("<PCType>" + pctype + "</PCType>");
                inXML.Append("</FetchPCDetails>");
                outXML = obj.CallMyVRMServer("FetchSelectedPCDetails", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_Configpath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                {
                    htmlcontent = "";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//PCDetails/PCDetail");
                    if (nodes.Count > 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].SelectSingleNode("PCId") != null)
                                int.TryParse(nodes[i].SelectSingleNode("PCId").InnerText.Trim(), out PCId);
                            if (nodes[i].SelectSingleNode("Description") != null)
                                Description = nodes[i].SelectSingleNode("Description").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("SkypeURL") != null)
                                SkypeURL = nodes[i].SelectSingleNode("SkypeURL").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinIP") != null)
                                VCDialinIP = nodes[i].SelectSingleNode("VCDialinIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCMeetingID") != null)
                                VCMeetingID = nodes[i].SelectSingleNode("VCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinNum") != null)
                                PCDialinNum = nodes[i].SelectSingleNode("PCDialinNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinFreeNum") != null)
                                PCDialinFreeNum = nodes[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCMeetingID") != null)
                                PCMeetingID = nodes[i].SelectSingleNode("PCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("Intructions") != null)
                                Intructions = nodes[i].SelectSingleNode("Intructions").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinSIP") != null)
                                VCDialinSIP = nodes[i].SelectSingleNode("VCDialinSIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinH323") != null)
                                VCDialinH323 = nodes[i].SelectSingleNode("VCDialinH323").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCPin") != null)
                                VCPin = nodes[i].SelectSingleNode("VCPin").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCPin") != null)
                                PCPin = nodes[i].SelectSingleNode("PCPin").InnerText.Trim();
                            switch (PCId)
                            {
                                case ns_MyVRMNet.vrmPC.BlueJeans:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in IP: " + VCDialinIP + "<br />Meeting ID: " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />Meeting ID: " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />First time joining a Blue Jeans Video Meeting?<br />For detailed instructions: " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Blue Jeans Network 2012</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Jabber:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in IP: " + VCDialinIP + "<br />Meeting ID: " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />Meeting ID: " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />First time joining a Jabber Video Meeting?<br />For detailed instructions: " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Jabber Network 2012</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Lync:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in IP: " + VCDialinIP + "<br />Meeting ID: " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />Meeting ID: " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />First time joining a Lync Video Meeting?<br />For detailed instructions: " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Lync Network 2012</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Vidtel:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in SIP: " + VCDialinSIP + "<br />Dial-in H.323: " + VCDialinH323 + "<br />PIN: " + VCPin + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />PIN: " + PCPin + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    //htmlcontent += "<br /><br />"; //FB 2723
                                    //htmlcontent += "***********************************************************";
                                    //htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    //htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Vidtel Network 2012</td></tr></table>";
                                    break;
                                default:

                                    break;

                            }
                        }
                    }
                    else
                    {
                        htmlcontent = "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='3' height='20px'></td></tr>";
                        htmlcontent += "<tr><td colspan='3' align='center'><b style='font-family:Verdana;' >" + obj.GetTranslatedText("No Information") + "</b><br><br><br><br></td></tr>";
                    }
                    res = htmlcontent.ToString();
                    //res = "text";
                }
            }
            catch (Exception ex)
            {
                log.Trace("PacketDetails" + ex.Message);
            }
            return res;
        }
        #endregion

        private void CheckPCVendor()
        {
            try
            {
                if (Session["EnableBlueJeans"] != null)
                {
                    if (Session["EnableBlueJeans"].ToString().Equals("1"))
                        PCVendorType = 1;
                }
                if (Session["EnableJabber"] != null)
                {
                    if (Session["EnableJabber"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 2;
                    }
                }
                if (Session["EnableLync"] != null)
                {
                    if (Session["EnableLync"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 3;
                    }
                }
                if (Session["EnableVidtel"] != null)
                {
                    if (Session["EnableVidtel"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 4;
                    }
                }
                switch (PCVendorType)
                {
                    case ns_MyVRMNet.vrmPC.BlueJeans:
                        rdBJ.Checked = true;
                        break;
                    case ns_MyVRMNet.vrmPC.Jabber:
                        rdJB.Checked = true;
                        break;
                    case ns_MyVRMNet.vrmPC.Lync:
                        rdLync.Checked = true;
                        break;
                    case ns_MyVRMNet.vrmPC.Vidtel:
                        rdVidtel.Checked = true;
                        break;
                    default:
                        rdBJ.Checked = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Trace("PacketDetails" + ex.Message);
            }
        }
        //FB 2693 End
    }
}
