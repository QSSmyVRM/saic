/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using myVRMNet;
using System.Xml; //FB 2450

public partial class en_ConfirmTemplate : System.Web.UI.Page
{
    # region Private & Protected Members
    private String xmlstr = "";
	//FB 2450
    //private msxml4_Net.IXMLDOMNodeList nodes = null;
    //private msxml4_Net.IXMLDOMNodeList subnotes = null;
    //public msxml4_Net.DOMDocument40Class XmlDoc = null;

    private XmlNodeList nodes = null;
    private XmlNodeList subnotes = null;
    public XmlDocument XmlDoc = null;

    protected System.Web.UI.HtmlControls.HtmlInputButton BtnEdit;
    protected System.Web.UI.HtmlControls.HtmlGenericControl divError;
    protected System.Web.UI.HtmlControls.HtmlGenericControl TempOK;
    protected String userName = "";
    protected String templateID = "";
    protected String templateName = "";
    protected String templatePublic = "";
    protected String templateDescription = "";
    protected String templateOwner = "";
    protected String confName = "";
    protected String confPassword = "";
    protected Int32 durationMin = 0;
    protected String description = "";
    protected String locations = "";
    protected Int32 length = 0;
    protected String locationName = "";
    protected String publicConf = "";
    protected String invited = "";
    protected Int32 sublength = 0;
    protected String pFN = "";
    protected String pLN = "";
    protected String pE = "";
    protected String invitee = "";
    protected String cc = "";
    protected Int32 dhour = 0;
    protected Int32 dmin = 0;
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    MyVRMNet.Util utilObj = new MyVRMNet.Util(); //FB 2236
    protected System.Web.UI.WebControls.Label errLabel;
    # endregion

    # region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("confirmtemplate.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
      
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            
            if (Session["outXML"] != null)
            {
                if (Session["outXML"].ToString() != "")
                    xmlstr = Session["outXML"].ToString();
            }
            if (!IsPostBack)
                GetConfirmTemplateValues();

             if (xmlstr.IndexOf("<error>") > 0)
                 Response.Write(obj.ShowErrorMessage(xmlstr));
           
            
           
        }
        catch (Exception ex)
        {
            BtnEdit.Disabled = true;
            //errLabel.Text = "ASP Runtime Error in Confirm Template.";
            log.Trace("Page_Load: ASP Runtime Error in Confirm Template." + ex.Message);//FB 1881
            errLabel.Text  = obj.ShowSystemMessage();//FB 1881
            
        }
    }
    # endregion

    # region GetConfirmTemplateValues
    private void GetConfirmTemplateValues()
    {
		//FB 2450
        //msxml4_Net.IXMLDOMNode node = null;
        //msxml4_Net.IXMLDOMNode subnode = null;
        //XmlDoc = new msxml4_Net.DOMDocument40Class();
       XmlNode node = null;
       XmlNode subnode = null;
        XmlDoc = new XmlDocument();
        try
        {
            //FB 2450
            //XmlDoc.async = false;
            XmlDoc.LoadXml(Session["outXML"].ToString());
            node = XmlDoc.SelectSingleNode("//template/userInfo");    //XmlDoc.documentElement.selectSingleNode("userInfo")
            userName = node.SelectSingleNode("userName").InnerText;
            node = XmlDoc.SelectSingleNode("//template/templateInfo");
            templateID = node.SelectSingleNode("ID").InnerText;
            templateName = node.SelectSingleNode("name").InnerText;
            templatePublic = node.SelectSingleNode("public").InnerText;
            templateDescription = node.SelectSingleNode("description").InnerText;
            templateOwner = node.SelectSingleNode("owner/firstName").InnerText + " " + node.SelectSingleNode("owner/lastName").InnerText;

            node = XmlDoc.SelectSingleNode("//template/confInfo");
            confName = node.SelectSingleNode("confName").InnerText;
            confPassword = node.SelectSingleNode("confPassword").InnerText;
            durationMin = Convert.ToInt32(node.SelectSingleNode("durationMin").InnerText);
            description = utilObj.ReplaceOutXMLSpecialCharacters(node.SelectSingleNode("description").InnerText, 2);//FB 2236	
            locations = "";
            TempOK.Attributes.Add("style", "display:block");
            divError.Attributes.Add("style", "display:none");
            nodes = node.SelectNodes("mainLocation/location");
            length = nodes.Count;
            for (Int32 j = 0; j < length; j++)
            {
                subnode = nodes[j]; // nodes.nextNode();
                locationName = subnode.SelectSingleNode("locationName").InnerText;
                 locations = locations + locationName + "<br>";
            }

            publicConf = node.SelectSingleNode("publicConf").InnerText;
            invited = "";
            subnotes = node.SelectNodes("invited/party");
            sublength = subnotes.Count;
            //removed "-1"
            for (Int32 j = 0; j < sublength ; j++)
            {
                subnode = subnotes[j];//subnotes.nextNode();
                pFN = subnode.SelectSingleNode("partyFirstName").InnerText;
                pLN = subnode.SelectSingleNode("partyLastName").InnerText;
                pE = subnode.SelectSingleNode("partyEmail").InnerText;

              invited = invited + "<tr><td height=20><a href='mailto:" + pE + "'>" + pFN + " " + pLN + "</a> (" + obj.GetTranslatedText("External Attendee") + ")</td></tr>";
            }

            invitee = "";
            subnotes = node.SelectNodes("invitee/party");
            sublength = subnotes.Count;
                for(Int32 j = 0;j< sublength ;j++)
                {
                    subnode = subnotes[j]; //subnotes.nextNode();
                    pFN = subnode.SelectSingleNode("partyFirstName").InnerText;
                    pLN = subnode.SelectSingleNode("partyLastName").InnerText;
                    pE = subnode.SelectSingleNode("partyEmail").InnerText;
                    invitee = invitee + "<tr><td height=20><a href='mailto:" + pE + "'>" + pFN + " " + pLN + "</a> (" + obj.GetTranslatedText("Room Attendee") + ")</td></tr>";
                }

                cc = "";
                 subnotes = node.SelectNodes("cc/party");
                    sublength = subnotes.Count;
                    for(Int32 j = 0 ;j< sublength ;j++)
                    {
                        subnode = subnotes[j]; //subnotes.nextNode();
                        pFN = subnode.SelectSingleNode("partyFirstName").InnerText;
                        pLN = subnode.SelectSingleNode("partyLastName").InnerText;
                        pE = subnode.SelectSingleNode("partyEmail").InnerText;

                        cc = cc + "<tr><td height=20><a href='mailto:" + pE + "'>" + pFN + " " + pLN + "</a> (" + obj.GetTranslatedText("cc") + ")</td></tr>";
                    }

                   

        }
        catch (Exception ex)
        {
           // BtnEdit.Disabled = true;
            log.Trace("Confirm Template" + ex.ToString());
          TempOK.Attributes.Add("style", "display:none");
          divError.Attributes.Add("style", "display:block");
           // errLabel.Text = "ASP Runtime Error in Confirm Template.";

        }

    }
    # endregion

}
