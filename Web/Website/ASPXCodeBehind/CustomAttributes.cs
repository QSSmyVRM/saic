/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Web.Security;
using System.Web.Util;

namespace myVRMNet
{
    /// <summary>
    /// This is a generic class which has custom attributes related codes that are used across the website.
    /// Created by: Valli.M.
    /// TextBox-4, Combo-6, MultiCombo-5, Radio-3, CheckBox-2, URL -7, RadioButtonsList -8, Multiline
    /// textBox - 10
    /// </summary>
    public class CustomAttributes
    {
        #region Data Members
        /// <summary>
        /// Data Members
        /// </summary>
        private string controlIDString = "";
        private XmlNode CANode = null;
        string customAttId = "";
        string isRequired = "0";
        string isEnableDisplay = "1";   //status-0 Enable; status=1 Disable
        bool isValidationRequired = false;
        string optionType = "";

        public ns_Logger.Logger log;

        //FB 1779 - Start
        XmlNodeList hostNodes = null;
        XmlNodeList specialNodes = null;
        myVRMNet.NETFunctions obj;//FB 1830 - Traslation
        //FB 1779 - End
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public CustomAttributes()
        {
            log = new ns_Logger.Logger();
            obj = new NETFunctions();
        }
        #endregion

        #region Display Custom Options
        /// <summary>
        /// DisplayCustomOptions
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        public Table DisplayCustomOptions(XmlNodeList nodes)
        {
            Table tblCA = new Table();
            TableRow tRow;
            bool displaySt = false;
            int CreateType = 0; //FB 2377
            string Description = "";
            try
            {
                tblCA.Width = Unit.Percentage(90);
                //FB 2508 - Start
                //tblCA.CellPadding = 1;
                //tblCA.CellSpacing = 1;
                tblCA.Style.Add("border-collapse", "collapse");

                optionType = "";
                if (nodes != null)
                {
                    if (nodes.Count > 0)
                    {
                        string isSelected = "";
                        string CALabel = "";
                        string selectedValue = "";
                        string includedInEmail = "";
                        string displayValue = "";
                        TableCell tCol = null;

                        //Values display table
                        tRow = new TableRow();
                        tRow.BorderColor = System.Drawing.Color.Black;
                        tRow.BorderWidth = Unit.Pixel(1);
                        tCol = new TableCell();
                        
                        tCol.BorderColor = System.Drawing.Color.Black;
                        tCol.BorderWidth = Unit.Pixel(1);
                        tCol.Text = obj.GetTranslatedText("Title");//FB 1830 - Translation
                        tCol.CssClass = "tableHeader";
                        tCol.HorizontalAlign = HorizontalAlign.Center;
                        tRow.Cells.Add(tCol);
                        tCol = null;

                        tCol = new TableCell();
                        tCol.BorderColor = System.Drawing.Color.Black;
                        tCol.BorderWidth = Unit.Pixel(1);
                        tCol.Text = obj.GetTranslatedText("Custom Option Value");//FB 1830 - Translation
                        tCol.CssClass = "tableHeader";
                        tCol.HorizontalAlign = HorizontalAlign.Center;
                        tRow.Cells.Add(tCol);
                        tCol = null;

                        tCol = new TableCell();
                        tCol.BorderColor = System.Drawing.Color.Black;
                        tCol.BorderWidth = Unit.Pixel(1);
                        tCol.Text = obj.GetTranslatedText("Include in Email");//FB 1830 - Translation
                        tCol.CssClass = "tableHeader";
                        tCol.HorizontalAlign = HorizontalAlign.Center;
                        tRow.Cells.Add(tCol);
                        tCol = null;
                        tblCA.Rows.Add(tRow);

                        foreach (XmlNode node in nodes)
                        {
                            optionType = "";
                            customAttId = "";
                            isEnableDisplay = "";
                            CALabel = "";
                            displayValue = "";
                            includedInEmail = "";

                            if (node.SelectSingleNode("Type") != null)
                                optionType = node.SelectSingleNode("Type").InnerText;

                            if (node.SelectSingleNode("CustomAttributeID") != null)
                                customAttId = node.SelectSingleNode("CustomAttributeID").InnerText;

                            if (customAttId == "")
                                continue;

                            if (node.SelectSingleNode("Status") != null)
                                isEnableDisplay = node.SelectSingleNode("Status").InnerText;

                            if (isEnableDisplay == "")
                                isEnableDisplay = "1"; //Disable

                            if (isEnableDisplay == "1")
                                continue;

                            if (node.SelectSingleNode("Selected") != null)
                                isSelected = node.SelectSingleNode("Selected").InnerText;

                            if (isSelected == "")
                                isSelected = "0"; //Not linked to the conference

                            if (isSelected == "0")
                                continue;

                            if (node.SelectSingleNode("Title") != null)
                                CALabel = node.SelectSingleNode("Title").InnerText.Trim();

                            if (node.SelectSingleNode("SelectedValue") != null)
                                selectedValue = node.SelectSingleNode("SelectedValue").InnerText.Trim();

                            if (node.SelectSingleNode("IncludeInEmail") != null)
                                includedInEmail = node.SelectSingleNode("IncludeInEmail").InnerText.Trim();

                            if (includedInEmail == "1")
                                includedInEmail = "Yes";
                            else
                                includedInEmail = "No";

                            //FB 2377 - Starts
                            if (node.SelectSingleNode("Description") != null)
                                Description = node.SelectSingleNode("Description").InnerText;

                            if (node.SelectSingleNode("CreateType") != null)
                                Int32.TryParse(node.SelectSingleNode("CreateType").InnerText, out CreateType);

                            displayValue = "";
                            switch (optionType)
                            {
                                case "2"://FB 2377
                                    {
                                        if (selectedValue == "1")
                                            displayValue = "Yes";
                                        else
                                            displayValue = "";
                                        break;
                                    }
                                case "3":
                                    {
                                        if (selectedValue == "1")
                                            displayValue = "Yes";
                                        else
                                            displayValue = "No";
                                        break;
                                    }
                                case "4":
                                case "10": //FB 1718
                                    {
                                        displayValue = selectedValue;
                                        displayValue = displayValue.Replace("\r\n", "<br>");
                                        displayValue = displayValue.Replace("\n", "<br>");
                                        break;
                                    }
                                case "5":
                                case "6":
                                case "8": //FB 1718
                                    {
                                        displayValue = "";
                                        XmlNodeList optNodes = node.SelectNodes("OptionList/Option");
                                        if (optNodes != null)
                                        {
                                            foreach (XmlNode optNode in optNodes)
                                            {
                                                if (optNode.SelectSingleNode("Selected") != null)
                                                {
                                                    if (optNode.SelectSingleNode("Selected").InnerText == "1")
                                                    {
                                                        if (optNode.SelectSingleNode("DisplayCaption") != null)
                                                        {
                                                            if (displayValue == "")
                                                            {
                                                                displayValue = optNode.SelectSingleNode("DisplayCaption").InnerText.Trim();
                                                            }
                                                            else
                                                            {
                                                                displayValue = displayValue + "," + optNode.SelectSingleNode("DisplayCaption").InnerText.Trim();

                                                            }
                                                        }

                                                    }

                                                }
                                            }
                                        }
                                        break;
                                    }
                                case "7":
                                    {
                                        if (selectedValue != "")
                                        {
                                            selectedValue = selectedValue.ToLower().Replace("https", "");
                                            selectedValue = selectedValue.ToLower().Replace("http", "");
                                            selectedValue = selectedValue.ToLower().Replace("://", "");
                                            displayValue = "<a href=http://" + selectedValue + " target='_blank'>" + selectedValue + "</a>";
                                        }
                                        break;
                                    }
                            }
                            if (displayValue != "" && CALabel != "")
                            {
                                displaySt = true;

                                tRow = new TableRow();
                                tRow.BorderColor = System.Drawing.Color.Black;
                                tRow.BorderWidth = Unit.Pixel(1);

                                tCol = new TableCell();
                                tCol.BorderColor = System.Drawing.Color.Black;
                                tCol.BorderWidth = Unit.Pixel(1);
                                tCol.Text = CALabel;
                                tCol.CssClass = "tableBody";
                                tCol.HorizontalAlign = HorizontalAlign.Left;
                                tRow.Cells.Add(tCol);
                                tCol = null;

                                tCol = new TableCell();
                                tCol.BorderColor = System.Drawing.Color.Black;
                                tCol.BorderWidth = Unit.Pixel(1);
                                tCol.Text = displayValue;
                                tCol.CssClass = "tableBody";
                                tCol.HorizontalAlign = HorizontalAlign.Left;
                                tRow.Cells.Add(tCol);
                                tCol = null;

                                tCol = new TableCell();
                                tCol.BorderColor = System.Drawing.Color.Black;
                                tCol.BorderWidth = Unit.Pixel(1);
                                tCol.Text = includedInEmail;
                                tCol.CssClass = "tableBody";
                                tCol.HorizontalAlign = HorizontalAlign.Left;
                                tRow.Cells.Add(tCol);
                                tCol = null;
                                tblCA.Rows.Add(tRow);
                                //FB 2508 - End
                            }
                        }
                    }
                }
                if (!displaySt)
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.Trace("DisplayCustomOptions" + ex.Message);
            }
            return tblCA;
        }
        #endregion

        //FB 2592 - Starts
        #region AdditonalOptions
        /// <summary>
        /// AdditonalOptions
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="tblCA"></param>
        /// <param name="isValidationReq"></param>
        /// <returns></returns>
        public string AdditonalOptions(XmlNodeList nodes, Table tblCA, Boolean isValidationReq)
        {
            TableRow tRow;
            TableCell caCol;
            int tdCount = 0;
            try
            {
                optionType = "";
                controlIDString = "";
                isValidationRequired = isValidationReq;

                if (tblCA == null)
                    tblCA = new Table();
                else
                    tblCA.Controls.Clear();

                if (nodes == null || (nodes != null && nodes.Count <= 0))
                {
                    TableCell tCol = new TableCell();
                    tCol.Text = "<br/> <br/>" + obj.GetTranslatedText("No Custom Options found.");
                    tCol.HorizontalAlign = HorizontalAlign.Center;
                    tRow = new TableRow();
                    tRow.Cells.Add(tCol);
                    tRow.Visible = true;
                    tblCA.Rows.Add(tRow);
                }
                else
                {
                    controlIDString = "";
                    tRow = new TableRow();
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("Type") != null)
                            optionType = node.SelectSingleNode("Type").InnerText;

                        CANode = null;
                        CANode = node;
                        customAttId = "";
                        if (node.SelectSingleNode("CustomAttributeID") != null)
                            customAttId = node.SelectSingleNode("CustomAttributeID").InnerText;

                        if (customAttId == "")
                            continue;

                        if (node.SelectSingleNode("Status") != null)
                            isEnableDisplay = node.SelectSingleNode("Status").InnerText;

                        if (isEnableDisplay == "")
                            isEnableDisplay = "1"; //Disable

                        if (isEnableDisplay == "1")
                            continue;

                        if (node.SelectSingleNode("Mandatory") != null)
                            isRequired = node.SelectSingleNode("Mandatory").InnerText;

                        if (isRequired == "")
                            isRequired = "0"; //False

                        switch (optionType)
                        {
                            case "2":
                                {
                                    caCol = CreateLabel();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateCheckBox();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                            case "3":
                                {
                                    caCol = CreateLabel();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateRadioButton();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                            case "4":
                                {
                                    caCol = CreateLabel();
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateTextBox();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                            case "5":
                                {
                                    caCol = CreateLabel();
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateMultiSelectCombo();
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                            case "6":
                                {
                                    caCol = CreateLabel();
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateCombo();
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                            case "7":
                                {
                                    caCol = CreateLabel();
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateURLTextBox();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                            case "8":
                                {
                                    caCol = CreateLabel();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateRadioList();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                            case "10":
                                {
                                    caCol = CreateLabel();
                                    caCol.Visible = true;
                                    caCol.Width = Unit.Percentage(32);
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    tRow.Cells.Add(caCol);
                                    caCol = null;

                                    caCol = CreateMultilineText();
                                    caCol.VerticalAlign = VerticalAlign.Middle;
                                    caCol.Visible = true;
                                    //caCol.Width = Unit.Percentage(25);
                                    tRow.Cells.Add(caCol);
                                    caCol = null;
                                    tdCount++;
                                    break;
                                }
                        }

                        tRow.Visible = true;
                        tblCA.Rows.Add(tRow);

                        tRow = null;
                        tRow = new TableRow();
                        tdCount = 0;
                    }
                }

            }
            catch (Exception ex)
            {
                log.Trace("AdditonalOptions:" + ex.Message);
            }
            return controlIDString;
        }
        #endregion
        //FB 2592 - End

        #region Build Custom Attributes
        /// <summary>
        ///  Build Custom Attributes
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="tblCA"></param>
        /// <returns></returns>
        public string CreateCustomAttributes(XmlNodeList nodes, Table tblCA, Boolean isValidationReq) //FB 2501 Vnoc - corrected during this case //FB 2632
        {
            TableRow tRow;
            TableCell caCol;
            int tdCount = 0;
            bool isSingleTD = false;
            int lp = 1;
            string Description = "";//FB 2377
            int CreateType = 0;
            try
            {
                optionType = "";
                controlIDString = "";
                isValidationRequired = isValidationReq;

                if (tblCA == null)
                    tblCA = new Table();
                else
                    tblCA.Controls.Clear();

                if (nodes != null)
                {
                    if (nodes.Count <= 0)
                    {
                        TableCell tCol = new TableCell();
                        tCol.Text = "<br/> <br/>" + obj.GetTranslatedText("No Custom Options found.");//FB 1830 - Translation
                        tCol.HorizontalAlign = HorizontalAlign.Center;
                        tRow = new TableRow();
                        tRow.Cells.Add(tCol);
                        tRow.Visible = true;
                        tblCA.Rows.Add(tRow);
                    }
                    else
                    {
                        controlIDString = "";
                        tRow = new TableRow();
                        foreach (XmlNode node in nodes)
                        {
                            if (lp == nodes.Count)
                                isSingleTD = true;

                            lp++;
                            if (node.SelectSingleNode("Type") != null)
                                optionType = node.SelectSingleNode("Type").InnerText;

                            CANode = null;
                            CANode = node;
                            customAttId = "";
                            if (node.SelectSingleNode("CustomAttributeID") != null)
                                customAttId = node.SelectSingleNode("CustomAttributeID").InnerText;

                            if (customAttId == "")
                                continue;

                            if (node.SelectSingleNode("Status") != null)
                                isEnableDisplay = node.SelectSingleNode("Status").InnerText;

                            if (isEnableDisplay == "")
                                isEnableDisplay = "1"; //Disable

                            if (isEnableDisplay == "1")
                                continue;

                            if (node.SelectSingleNode("Mandatory") != null)
                                isRequired = node.SelectSingleNode("Mandatory").InnerText;
                            //FB 2377 - Starts
                            if (node.SelectSingleNode("Description") != null)
                                Description = node.SelectSingleNode("Description").InnerText;

                            if (node.SelectSingleNode("CreateType") != null)
                                Int32.TryParse(node.SelectSingleNode("CreateType").InnerText, out CreateType);

                            if (isRequired == "")
                                isRequired = "0"; //False

                            switch (optionType)
                            {
                                case "2":
                                    {
                                        caCol = CreateLabel();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateCheckBox();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(35);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }//FB 2377 - End
                                case "3":
                                    {
                                        caCol = CreateLabel();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateRadioButton();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(35);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }
                                case "4":
                                    {
                                        caCol = CreateLabel();
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateTextBox();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(25);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }
                                case "5":
                                    {
                                        caCol = CreateLabel();
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateMultiSelectCombo();
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(25);
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }
                                case "6":
                                    {
                                        caCol = CreateLabel();
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateCombo();
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(25);
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }
                                case "7":
                                    {
                                        caCol = CreateLabel();
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateURLTextBox();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(25);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }
                                case "8": //FB 1718
                                    {
                                        caCol = CreateLabel();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateRadioList();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(35);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }
                                case "10": //FB 1718
                                    {
                                        caCol = CreateLabel();
                                        caCol.Visible = true;
                                        if (tdCount == 1)
                                            caCol.Width = Unit.Percentage(20);
                                        else
                                            caCol.Width = Unit.Percentage(15);
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        tRow.Cells.Add(caCol);
                                        caCol = null;

                                        caCol = CreateMultilineText();
                                        caCol.VerticalAlign = VerticalAlign.Middle;
                                        caCol.Visible = true;
                                        caCol.Width = Unit.Percentage(25);
                                        tRow.Cells.Add(caCol);
                                        caCol = null;
                                        tdCount++;
                                        break;
                                    }
                            }
                            //Corrected FB 2377 during FB 2501 Vnoc ... END

                            if (tdCount == 2)
                            {
                                tRow.Visible = true;
                                tblCA.Rows.Add(tRow);

                                tRow = null;
                                tRow = new TableRow();
                                tdCount = 0;
                            }
                            else
                            {
                                tRow.Visible = true;
                                if (isSingleTD)
                                    tblCA.Rows.Add(tRow);
                            }
                        }
                    }
                }
                else
                {
                    TableCell tCol = new TableCell();
                    tCol.Text = "<br/> <br/>" + obj.GetTranslatedText("No Custom Options found.");//FB 1830 - Translation
                    tCol.HorizontalAlign = HorizontalAlign.Center;
                    tRow = new TableRow();
                    tRow.Cells.Add(tCol);
                    tRow.Visible = true;
                    tblCA.Rows.Add(tRow);
                }
            }
            catch (Exception ex)
            {
                log.Trace("CreateCustomAttributes" + ex.Message);
            }
            return controlIDString;
        }
        #endregion

        #region Create Control Labels

        private TableCell CreateLabel()
        {
            TableCell tCol = new TableCell();
            try
            {
                string CALabel = CANode.SelectSingleNode("Title").InnerText;

                if (CALabel == "")
                    CALabel = "Custom Attribute" + customAttId;

                tCol.HorizontalAlign = HorizontalAlign.Left;
                tCol.ID = "td_" + customAttId;
                tCol.CssClass = "blackblodtext";

                string reqText = "";
                if (isRequired == "1" && isValidationRequired)
                {
                    reqText = "<span id='span1' class='reqfldstarText'>*</span>";
                }
                tCol.Text = CALabel + ":" + reqText;
            }
            catch (Exception ex)
            {
                log.Trace("CreateLabel" + ex.Message);
            }
            return tCol;
        }
        #endregion

        #region Display Custom Values

        private TableCell DisplayCustomValues()
        {
            TableCell tCol = new TableCell();
            tCol.HorizontalAlign = HorizontalAlign.Left;

            string attriValue = "";
            string optionType = "";
            try
            {
                if (CANode.SelectSingleNode("Type") != null)
                    optionType = CANode.SelectSingleNode("Type").InnerText;

                switch (optionType)
                {
                    /*case "2":
                        {
                            if (CANode.SelectSingleNode("SelectedValue") != null)
                            {
                                if (CANode.SelectSingleNode("SelectedValue").InnerText != "")
                                {
                                    if (CANode.SelectSingleNode("SelectedValue").InnerText == "1")
                                    {
                                        tCol.Text = obj.GetTranslatedText("Yes");
                                    }
                                    else if (CANode.SelectSingleNode("SelectedValue").InnerText == "")
                                    {
                                        tCol.Text = obj.GetTranslatedText("No");
                                    }
                                }
                            }
                        }
                    */
                    case "3":
                        {
                            if (CANode.SelectSingleNode("SelectedValue") != null)
                            {
                                if (CANode.SelectSingleNode("SelectedValue").InnerText != "")
                                {
                                    if (CANode.SelectSingleNode("SelectedValue").InnerText == "1")
                                    {
                                        tCol.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                                    }
                                    else if (CANode.SelectSingleNode("SelectedValue").InnerText == "0")
                                    {
                                        tCol.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                                    }
                                }
                            }
                            break;
                        }
                    case "4":
                    case "10": //FB 1718
                        {
                            if (CANode.SelectSingleNode("SelectedValue") != null)
                            {
                                if (CANode.SelectSingleNode("SelectedValue").InnerText != "")
                                {
                                    tCol.Text = CANode.SelectSingleNode("SelectedValue").InnerText.ToString();
                                }
                            }
                            break;
                        }
                    case "7":
                        {
                            if (CANode.SelectSingleNode("SelectedValue") != null)
                            {
                                string tempVal = CANode.SelectSingleNode("SelectedValue").InnerText.ToString().Trim();
                                if (tempVal != "")
                                {
                                    tempVal = tempVal.ToLower().Replace("https", "");
                                    tempVal = tempVal.ToLower().Replace("http", "");
                                    tempVal = tempVal.ToLower().Replace("://", "");
                                    tempVal = "<a href=http://" + tempVal + " target='_blank'>" + tempVal + "</a>";
                                    tCol.Text = tempVal;
                                }
                            }
                            break;
                        }
                    case "5":
                    case "6":
                    case "8": //FB 1718
                        {
                            attriValue = "";
                            XmlNodeList optNodes = CANode.SelectNodes("OptionList/Option");
                            if (optNodes != null)
                            {
                                foreach (XmlNode optNode in optNodes)
                                {
                                    if (optNode.SelectSingleNode("Selected") != null)
                                    {
                                        if (optNode.SelectSingleNode("Selected").InnerText == "1")
                                        {
                                            if (optNode.SelectSingleNode("DisplayCaption") != null)
                                            {
                                                if (attriValue == "")
                                                {
                                                    attriValue = optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                }
                                                else
                                                {
                                                    attriValue = attriValue + "," + optNode.SelectSingleNode("DisplayCaption").InnerText;

                                                }
                                            }

                                        }

                                    }
                                }
                            }

                            tCol.Text = attriValue;

                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCol;
        }
        #endregion

        #region Create Combo
        /// <summary>
        /// Create Combo
        /// </summary>
        /// <param name="CANode"></param>
        private TableCell CreateCombo()
        {
            TableCell tCell = null;
            XmlNodeList optionList = null;
            string controlUID = "";
            try
            {
                if (CANode != null)
                {
                    controlUID = "DrpCA_6_" + customAttId;

                    if (controlIDString == "")
                        controlIDString = controlUID;
                    else
                        controlIDString += "?" + controlUID;


                    DropDownList lstTemp = new DropDownList();
                    lstTemp.ID = controlUID;
                    lstTemp.CssClass = "altText";
                    lstTemp.EnableViewState = true;

                    //FB 2045 - Starts
                    //lstTemp.Items.Add(new ListItem("Please select...", ""));

                    optionList = CANode.SelectNodes("OptionList/Option");
                    if (optionList != null)
                    {
                        if (optionList.Count != 0)
                        {
                            string optText = "";
                            string optID = "";
                            lstTemp.Items.Add(new ListItem(obj.GetTranslatedText("Please select..."), ""));
                            foreach (XmlNode xnode in optionList)
                            {
                                optID = xnode.SelectSingleNode("OptionID").InnerText;
                                optText = xnode.SelectSingleNode("DisplayCaption").InnerText;

                                ListItem li = new ListItem(optText, optID);
                                lstTemp.Items.Add(li);
                                if (xnode.SelectSingleNode("Selected").InnerText.Equals("1"))
                                {
                                    lstTemp.ClearSelection();
                                    li.Selected = true;
                                }
                            }
                        }
                        else
                            lstTemp.Items.Add(new ListItem("No Items...", ""));
                        //FB 2045 - End

                    }

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.Controls.Add(lstTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create Multi-Select Combo

        private TableCell CreateMultiSelectCombo()
        {
            TableCell tCell = null;
            XmlNodeList optionList = null;
            string controlUID = "";
            try
            {
                if (CANode != null)
                {
                    controlUID = "LstCA_5_" + customAttId;

                    if (controlIDString == "")
                        controlIDString = controlUID;

                    else
                        controlIDString += "?" + controlUID;


                    ListBox lstTemp = new ListBox();
                    lstTemp.ID = controlUID;
                    lstTemp.CssClass = "altText";
                    lstTemp.EnableViewState = true;
                    lstTemp.SelectionMode = ListSelectionMode.Multiple;
                    //lstTemp.Items.Add(new ListItem("Please select...", "-1"));

                    optionList = CANode.SelectNodes("OptionList/Option");
                    if (optionList != null)
                    {
                        string optText = "";
                        string optID = "";
                        foreach (XmlNode xnode in optionList)
                        {
                            optID = xnode.SelectSingleNode("OptionID").InnerText;
                            optText = xnode.SelectSingleNode("DisplayCaption").InnerText;

                            ListItem li = new ListItem(optText, optID);
                            lstTemp.Items.Add(li);
                            if (xnode.SelectSingleNode("Selected").InnerText.Equals("1"))
                            {
                                li.Selected = true;
                            }
                        }
                    }

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.Controls.Add(lstTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create Check Box

        private TableCell CreateCheckBox()
        {
            TableCell tCell = null;
            string controlUID = "";
            string optionSelSt = "";
            try
            {
                if (CANode != null)
                {
                    controlUID = "ChkCA_2_" + customAttId;

                    optionSelSt = CANode.SelectSingleNode("SelectedValue").InnerText;
                    if (optionSelSt == "")
                        optionSelSt = "0";

                    if (controlIDString == "")
                    {
                        controlIDString = controlUID;
                    }
                    else
                    {
                        controlIDString += "?" + controlUID;
                    }

                    CheckBox chkTemp = new CheckBox();
                    chkTemp.ID = controlUID;
                    chkTemp.EnableViewState = true;

                    if (optionSelSt == "1")
                        chkTemp.Checked = true;
                    else
                        chkTemp.Checked = false;

                    chkTemp.CssClass = "blackxxxstext";
                    chkTemp.TextAlign = TextAlign.Right;

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.VerticalAlign = VerticalAlign.Middle;
                    tCell.Controls.Add(chkTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create Radio Buttons

        private TableCell CreateRadioButton()
        {
            TableCell tCell = null;
            string controlUID = "";
            string controlUID2 = "";
            string optionSelSt = "";
            try
            {
                if (CANode != null)
                {
                    controlUID = "OptCAYes_3_" + customAttId;
                    controlUID2 = "OptCANo_3_" + customAttId;

                    optionSelSt = CANode.SelectSingleNode("SelectedValue").InnerText;
                    if (optionSelSt == "")
                        optionSelSt = "0";

                    if (controlIDString == "")
                    {
                        controlIDString = controlUID;
                    }
                    else
                    {
                        controlIDString += "?" + controlUID;
                    }

                    RadioButton optTemp = new RadioButton();
                    optTemp.ID = controlUID;
                    optTemp.EnableViewState = true;
                    optTemp.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation

                    if (optionSelSt == "1")
                        optTemp.Checked = true;

                    optTemp.CssClass = "blackxxxstext";
                    optTemp.TextAlign = TextAlign.Right;
                    optTemp.GroupName = "CAGroup_" + customAttId;

                    RadioButton optTemp2 = new RadioButton();
                    optTemp2.ID = controlUID2;
                    optTemp2.EnableViewState = true;
                    optTemp2.Text = obj.GetTranslatedText("No");//FB 1830 - Translation

                    if (optionSelSt == "0")
                        optTemp2.Checked = true;

                    optTemp2.CssClass = "blackxxxstext";
                    optTemp2.TextAlign = TextAlign.Right;
                    optTemp2.GroupName = "CAGroup_" + customAttId;

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.VerticalAlign = VerticalAlign.Middle;
                    tCell.Controls.Add(optTemp);
                    tCell.Controls.Add(optTemp2);

                    //if (isRequired == "1" && isValidationRequired)
                    //{
                    //    RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                    //    lstReqVal.ID = "ReqValCA_" + customAttId;
                    //    lstReqVal.ControlToValidate = controlUID;
                    //    lstReqVal.Display = ValidatorDisplay.Dynamic;
                    //    lstReqVal.ErrorMessage = "Required";
                    //    tCell.Controls.Add(lstReqVal);
                    //}
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create Text Box

        private TableCell CreateTextBox()
        {
            TableCell tCell = null;
            string controlUID = "";
            string txtSelValue = "";
            try
            {
                if (CANode != null)
                {
                    customAttId = CANode.SelectSingleNode("CustomAttributeID").InnerText;
                    controlUID = "TxtCA_4_" + customAttId;

                    txtSelValue = CANode.SelectSingleNode("SelectedValue").InnerText;

                    if (controlIDString == "")
                        controlIDString = controlUID;
                    else
                        controlIDString += "?" + controlUID;


                    TextBox txtTemp = new TextBox();
                    txtTemp.ID = controlUID;
                    txtTemp.CssClass = "altText";
                    txtTemp.EnableViewState = true;
                    txtTemp.Text = txtSelValue;
                    txtTemp.Width = Unit.Pixel(150);

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.Controls.Add(txtTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }

                    // ZD 100263 Starts
                    if (HttpContext.Current.Session["roomCascadingControl"] != null && HttpContext.Current.Session["roomCascadingControl"].Equals("1"))
                    {
                        RegularExpressionValidator expMultiRangeval = new RegularExpressionValidator();
                        expMultiRangeval.ID = "ValCA2_" + customAttId;
                        expMultiRangeval.ControlToValidate = controlUID;
                        expMultiRangeval.Display = ValidatorDisplay.Dynamic;
                        expMultiRangeval.ErrorMessage = "<br>" + obj.GetTranslatedText("& < and > are invalid characters.");
                        expMultiRangeval.ValidationExpression = "^[^<>&]*$";
                        tCell.Controls.Add(expMultiRangeval);
                    }
                    // ZD 100263 Ends
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create URL Text Box

        private TableCell CreateURLTextBox()
        {
            TableCell tCell = null;
            string controlUID = "";
            string txtSelValue = "";
            try
            {
                if (CANode != null)
                {
                    customAttId = CANode.SelectSingleNode("CustomAttributeID").InnerText;
                    controlUID = "TxtURLCA_7_" + customAttId;

                    txtSelValue = CANode.SelectSingleNode("SelectedValue").InnerText;

                    if (controlIDString == "")
                        controlIDString = controlUID;
                    else
                        controlIDString += "?" + controlUID;


                    TextBox txtTemp = new TextBox();
                    txtTemp.ID = controlUID;
                    txtTemp.CssClass = "altText";
                    txtTemp.EnableViewState = true;
                    txtTemp.Text = txtSelValue;
                    txtTemp.Width = Unit.Pixel(245);

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.Controls.Add(txtTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create Multiline Text

        private TableCell CreateMultilineText()
        {
            TableCell tCell = null;
            string controlUID = "";
            string txtSelValue = "";
            try
            {
                if (CANode != null)
                {
                    customAttId = CANode.SelectSingleNode("CustomAttributeID").InnerText;
                    controlUID = "TxtCA_10_" + customAttId;

                    txtSelValue = CANode.SelectSingleNode("SelectedValue").InnerText;

                    if (controlIDString == "")
                        controlIDString = controlUID;
                    else
                        controlIDString += "?" + controlUID;


                    TextBox txtTemp = new TextBox();
                    txtTemp.ID = controlUID;
                    txtTemp.CssClass = "altText";
                    txtTemp.EnableViewState = true;
                    txtTemp.Text = txtSelValue;
                    txtTemp.Width = Unit.Pixel(150);
                    txtTemp.TextMode = TextBoxMode.MultiLine;
                    txtTemp.Width = Unit.Pixel(250);
                    txtTemp.Height = Unit.Pixel(40);

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.Controls.Add(txtTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }
                    //FB 2508
                    RegularExpressionValidator expMultiRangeval = new RegularExpressionValidator();
                    expMultiRangeval.ID = "ValCA_" + customAttId;
                    expMultiRangeval.ControlToValidate = controlUID;
                    expMultiRangeval.Display = ValidatorDisplay.Dynamic;
                    expMultiRangeval.ErrorMessage = "<br>" + obj.GetTranslatedText("Maximum limit is 4000 characters.");
                    expMultiRangeval.ValidationExpression = "^[\\s\\S]{0,4000}$";
                    tCell.Controls.Add(expMultiRangeval);

                    // ZD 100263 Starts
                    if (HttpContext.Current.Session["roomCascadingControl"] != null && HttpContext.Current.Session["roomCascadingControl"].Equals("1"))
                    {
                        expMultiRangeval = new RegularExpressionValidator();
                        expMultiRangeval.ID = "ValCA2_" + customAttId;
                        expMultiRangeval.ControlToValidate = controlUID;
                        expMultiRangeval.Display = ValidatorDisplay.Dynamic;
                        expMultiRangeval.ErrorMessage = "<br>" + obj.GetTranslatedText("& < and > are invalid characters.");
                        expMultiRangeval.ValidationExpression = "^[^<>&]*$";
                        tCell.Controls.Add(expMultiRangeval);
                    }
                    // ZD 100263 Ends
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region CustomAttributeInxml
        /// <summary>
        /// CustomAttributeInxml
        /// </summary>
        /// <param name="customIDs"></param>
        /// <param name="customTable"></param>
        /// <returns></returns>
        public string CustomAttributeInxml(string customIDs, Table customTable) //FB 2632
        {
            string inxml = "";
            string optionId = "";
            string optionType = "";
            string optionValue = "";
            string reqValName = "";
            bool isReqNotSel = false;
            isValidationRequired = false;
            customAttId = "";
            //DrpCA_6_1?TxtCA_4_2?LstCA_5_4?OptCAYes_3_5?OptCANo_3_5
            try
            {

                if (customIDs != null && customTable != null)
                {
                    if (customIDs != "")
                    {
                        String[] custControls = customIDs.Split('?');

                        if (custControls.Length > 0)
                        {
                            foreach (string custControlName in custControls)
                            {
                                String[] attributeDt = custControlName.Split('_');
                                if (attributeDt.Length > 0)
                                {
                                    optionType = "";
                                    optionType = attributeDt[1];
                                    customAttId = attributeDt[2];

                                    reqValName = "ReqValCA_" + customAttId;
                                    RequiredFieldValidator reqV = (RequiredFieldValidator)customTable.FindControl(reqValName);
                                    if (reqV != null)
                                        isValidationRequired = true;
                                    else
                                        isValidationRequired = false;

                                    switch (optionType)
                                    {
                                        case "6":
                                            {
                                                DropDownList drpTemp = (DropDownList)customTable.FindControl(custControlName);
                                                optionId = drpTemp.SelectedValue;
                                                optionValue = drpTemp.SelectedItem.Text;

                                                if (optionId == "")
                                                    optionValue = "";

                                                if (isValidationRequired)
                                                {
                                                    if (optionValue == "")
                                                        isReqNotSel = true;
                                                }
                                                break;
                                            }
                                        case "5":
                                            {
                                                ListBox lstTemp = (ListBox)customTable.FindControl(custControlName);
                                                foreach (ListItem li in lstTemp.Items)
                                                {
                                                    if (li.Selected && li.Value != "")
                                                    {
                                                        optionValue = li.Text;
                                                        inxml += "<CustomAttribute>";
                                                        inxml += "<CustomAttributeID>" + customAttId + "</CustomAttributeID>";
                                                        inxml += "<OptionID>" + li.Value + "</OptionID>";
                                                        inxml += "<Type>" + optionType + "</Type>";
                                                        inxml += "<OptionValue>" + li.Text + "</OptionValue>";
                                                        inxml += "</CustomAttribute>";
                                                    }
                                                }
                                                if (isValidationRequired)
                                                {
                                                    if (optionValue == "")
                                                        isReqNotSel = true;
                                                }
                                                break;
                                            }
                                        case "4":
                                        case "7":
                                        case "10": //FB 1718
                                            {
                                                TextBox txtTemp = (TextBox)customTable.FindControl(custControlName);
                                                optionId = "-1";
                                                optionValue = txtTemp.Text.Trim();

                                                if (optionValue.Contains("&") || optionValue.Contains("<") || optionValue.Contains(">")) //FB 2607
                                                {
                                                    optionValue = optionValue.Replace("&", " ");
                                                    optionValue = optionValue.Replace("<", " ");
                                                    optionValue = optionValue.Replace(">", " ");
                                                }

                                                if (isValidationRequired)
                                                {
                                                    if (optionValue == "")
                                                        isReqNotSel = true;
                                                }
                                                break;
                                            }
                                        case "8": //FB 1718
                                            {
                                                RadioButtonList rdList = (RadioButtonList)customTable.FindControl(custControlName);
                                                optionValue = "";
                                                if (rdList != null)
                                                {
                                                    optionId = rdList.SelectedValue;

                                                    if (rdList.SelectedItem != null)
                                                    {
                                                        optionValue = rdList.SelectedItem.Text;
                                                    }

                                                    if (optionId == "")
                                                        optionValue = "";

                                                    if (isValidationRequired)
                                                    {
                                                        if (optionValue == "")
                                                            isReqNotSel = true;
                                                    }
                                                }
                                                break;
                                            }
                                        case "2"://FB 2377
                                            {
                                                CheckBox chkTemp = (CheckBox)customTable.FindControl(custControlName);
                                                optionId = "-1";

                                                if (chkTemp.Checked)
                                                {
                                                    optionValue = "1";
                                                }
                                                else
                                                {
                                                    optionValue = "";//Set empty then olny its not saved in Conf_CustomAttr_D for uncheck.
                                                }
                                                break;
                                            }
                                        case "3":
                                            {
                                                RadioButton optTemp = (RadioButton)customTable.FindControl(custControlName);
                                                optionId = "-1";
                                                if (optTemp.Checked)
                                                {
                                                    optionValue = "1";
                                                }
                                                else
                                                {
                                                    optionValue = "0";
                                                }
                                                break;
                                            }
                                    }
                                    if (optionValue != "" && optionType != "5")
                                    {
                                        inxml += "<CustomAttribute>";
                                        inxml += "<CustomAttributeID>" + customAttId + "</CustomAttributeID>";
                                        inxml += "<OptionID>" + optionId + "</OptionID>";
                                        inxml += "<Type>" + optionType + "</Type>";
                                        inxml += "<OptionValue>" + optionValue + "</OptionValue>";
                                        inxml += "</CustomAttribute>";
                                    }
                                    optionId = "";
                                    optionValue = "";
                                }
                            }
                        }
                    }
                }
                if (isReqNotSel)    //FB 2632 start
                {
                    inxml = "<error>" + obj.GetTranslatedText("Please set the values for the mandatory custom options.") + "</error>";
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return inxml;
        }
        #endregion

        #region Custom Attribute Selected Details
        /// <summary>
        /// Custom Attribute Selected Details
        /// </summary>
        /// <param name="customIDs"></param>
        /// <param name="customTable"></param>
        /// <returns></returns>
        public string CustomAttributeValues(string customIDs, Table customTable) //FB 2632
        {
            string selectedValues = "";
            string optionId = "";
            string optionType = "";
            string optionValue = "";
            customAttId = "";
            string custLabel = "";
            string tdId = "";

            try
            {

                if (customIDs != null && customTable != null) //Corrected during FB 2501 VNOC
                {
                    if (customIDs != "")
                    {
                        String[] custControls = customIDs.Split('?');

                        if (custControls.Length > 0)
                        {
                            foreach (string custControlName in custControls)
                            {
                                String[] attributeDt = custControlName.Split('_');
                                if (attributeDt.Length > 0)
                                {
                                    optionType = "";
                                    optionType = attributeDt[1];
                                    customAttId = attributeDt[2];

                                    tdId = "td_" + customAttId;

                                    TableCell tempTd = (TableCell)customTable.FindControl(tdId);
                                    custLabel = tempTd.Text;
                                    custLabel = custLabel.Replace(":", "");
                                    custLabel = custLabel.Replace("*", "");
                                    switch (optionType)
                                    {
                                        case "6":
                                            {
                                                DropDownList drpTemp = (DropDownList)customTable.FindControl(custControlName);
                                                optionId = drpTemp.SelectedValue;
                                                optionValue = drpTemp.SelectedItem.Text;

                                                if (optionId == "")
                                                    optionValue = "";
                                                break;
                                            }
                                        case "5":
                                            {
                                                ListBox lstTemp = (ListBox)customTable.FindControl(custControlName);
                                                int vcnt = 1;
                                                foreach (ListItem li in lstTemp.Items)
                                                {
                                                    if (li.Selected && li.Value != "")
                                                    {
                                                        optionValue = li.Text;

                                                        if (vcnt == 1)
                                                            selectedValues += custLabel + " > " + optionValue;
                                                        else
                                                            selectedValues += ", " + optionValue;

                                                        vcnt++;
                                                    }
                                                }

                                                if (vcnt > 1)
                                                    selectedValues += "<br/>";

                                                break;
                                            }
                                        case "4":
                                            {
                                                TextBox txtTemp = (TextBox)customTable.FindControl(custControlName);
                                                optionId = "-1";
                                                optionValue = txtTemp.Text.Trim();
                                                break;
                                            }
                                        case "10": //FB 1718
                                            {
                                                TextBox txtTemp = (TextBox)customTable.FindControl(custControlName);
                                                optionId = "-1";
                                                optionValue = obj.ControlConformityCheck(txtTemp.Text.Trim()); // ZD 100263
                                                optionValue = optionValue.Replace("\r\n", "<br>");
                                                optionValue = optionValue.Replace("\n", "<br>");
                                                optionValue = "<br>" + optionValue;

                                                selectedValues += custLabel + ":" + optionValue + "<br />";
                                                break;
                                            }
                                        case "7":
                                            {
                                                TextBox txtTemp = (TextBox)customTable.FindControl(custControlName);
                                                optionId = "-1";
                                                optionValue = txtTemp.Text.Trim();
                                                if (optionValue != "")
                                                {
                                                    optionValue = optionValue.ToLower().Replace("https", "");
                                                    optionValue = optionValue.ToLower().Replace("http", "");
                                                    optionValue = optionValue.ToLower().Replace("://", "");

                                                    selectedValues += custLabel + " > <a href=http://" + optionValue + " target='_blank'>" + optionValue + "</a><br />";
                                                }
                                                break;
                                            }
                                        case "2"://FB 2377
                                            {
                                                CheckBox chkTemp = (CheckBox)customTable.FindControl(custControlName);
                                                optionId = "-1";
                                                if (chkTemp.Checked)
                                                {
                                                    optionValue = "Yes";
                                                }
                                                else
                                                {
                                                    optionValue = "";//Set empty then only its not displayed in Load Preview(ConferenceSetup) 
                                                }
                                                break;
                                            }
                                        case "3":
                                            {
                                                RadioButton optTemp = (RadioButton)customTable.FindControl(custControlName);
                                                optionId = "-1";
                                                if (optTemp.Checked)
                                                {
                                                    optionValue = "Yes";
                                                }
                                                else
                                                {
                                                    optionValue = "No";
                                                }
                                                break;
                                            }
                                        case "8": //FB 1718
                                            {
                                                RadioButtonList rdlTemp = (RadioButtonList)customTable.FindControl(custControlName);
                                                optionId = "";
                                                optionValue = "";

                                                if (rdlTemp != null)
                                                {
                                                    optionId = rdlTemp.SelectedValue;

                                                    if (rdlTemp.SelectedItem != null)
                                                        optionValue = rdlTemp.SelectedItem.Text;

                                                    if (optionId == "")
                                                        optionValue = "";
                                                }
                                                break;
                                            }
                                    }
                                    if (optionValue != "" && optionType != "5" && optionType != "7" && optionType != "10") //FB 1718
                                    {
                                        selectedValues += custLabel + " > " + optionValue + "<br />";
                                    }
                                    optionId = "";
                                    optionValue = "";
                                    custLabel = "";
                                }
                            }
                        }
                    }
                }
                if (selectedValues == "")
                {
                    selectedValues = "N/A";
                }


            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return selectedValues;
        }
        #endregion
        /* Added for FB 1718 - start */
        #region Create RadioList
        /// <summary>
        /// CreateRadioList
        /// </summary>
        /// <returns></returns>
        private TableCell CreateRadioList()
        {
            TableCell tCell = null;
            string controlUID = "";
            string controlText = "";
            try
            {
                if (CANode != null)
                {
                    controlText = CANode.SelectSingleNode("Title").InnerText.Trim();

                    if (controlText == "")
                        controlText = "Custom Attribute" + customAttId;

                    controlUID = "RadioList_8_" + customAttId;

                    if (controlIDString == "")
                        controlIDString = controlUID;
                    else
                        controlIDString += "?" + controlUID;

                    RadioButtonList rdList = new RadioButtonList();
                    rdList.ID = controlUID;
                    rdList.CssClass = "blackxxxstext";
                    rdList.EnableViewState = true;

                    XmlNodeList optionList = CANode.SelectNodes("OptionList/Option");
                    if (optionList != null)
                    {
                        string optText = "";
                        string optID = "";
                        foreach (XmlNode xnode in optionList)
                        {
                            optID = xnode.SelectSingleNode("OptionID").InnerText;
                            optText = xnode.SelectSingleNode("DisplayCaption").InnerText;

                            ListItem li = new ListItem(optText, optID);
                            rdList.Items.Add(li);
                            if (xnode.SelectSingleNode("Selected").InnerText.Equals("1"))
                            {
                                rdList.ClearSelection();
                                li.Selected = true;
                            }
                        }
                    }
                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.VerticalAlign = VerticalAlign.Middle;
                    tCell.Controls.Add(rdList);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion
        /* Added for FB 1718 - end */

        //FB 1779 - Express Conference Specifics - Start

        #region Create Express Attributes
        /// <summary>
        /// CreateExpressAttributes
        /// </summary>
        /// <param name="customAttrs"></param>
        /// <param name="tblHost"></param>
        /// <param name="tblSpecial"></param>
        /// <param name="tblCustomAttribute"></param>
        /// <param name="isValidationReq"></param>
        /// <param name="hdnHostIDs"></param>
        /// <returns></returns>
        public string CreateExpressAttributes(string customAttrs, Table tblHost, Table tblSpecial, Table tblCustomAttribute, Boolean isValidationReq, ref HtmlInputHidden hdnHostIDs) //FB 2632 //FB 2592
        {
            try
            {
                optionType = "";
                controlIDString = "";
                string hostIds = "";

                isValidationRequired = isValidationReq; //FB 2592

                if (tblHost == null)
                {
                    tblHost = new Table();
                }
                else
                    tblHost.Controls.Clear();

                if (tblSpecial == null)
                    tblSpecial = new Table();
                else
                    tblSpecial.Controls.Clear();

                XmlDocument xdoc = new XmlDocument();
                if (customAttrs != "")
                {
                    xdoc.LoadXml(customAttrs);
                }
                hostNodes = xdoc.SelectNodes("descendant::CustomAttribute[Description='Host']");
                hostIds = DisplayHostInfo(ref tblHost);
                hdnHostIDs.Value = "";
                hdnHostIDs.Value = hostIds;

                specialNodes = xdoc.SelectNodes("descendant::CustomAttribute[Description='Special Instructions']");
                DisplaySpecialInfo(ref tblSpecial);
                //FB 2592 - Starts-Doubt
                XmlNodeList nodes = xdoc.SelectNodes("//CustomAttributesList/CustomAttribute");
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (nodes[i].SelectSingleNode("Title").InnerText.Trim().Equals("Special Instructions") || nodes[i].SelectSingleNode("Title").InnerText.Trim().Equals("Work") || nodes[i].SelectSingleNode("Title").InnerText.Trim().Equals("Cell"))
                        nodes[i].RemoveAll();
                }

                string customIds = controlIDString;
                AdditonalOptions(nodes, tblCustomAttribute, true);
                controlIDString += "?" + customIds;
                //FB 2592 - End
            }
            catch (Exception ex)
            {
                log.Trace("CreateExpressAttributes:" + ex.Message);
            }
            return controlIDString;
        }
        #endregion

        #region DisplayHostInfo

        private string DisplayHostInfo(ref Table tblHost)
        {
            TableRow tRow = null;
            TableCell tCell = null;
            string hostAttrIds = "";
            try
            {
                if (hostNodes != null)
                {
                    if (hostNodes.Count > 0)
                    {
                        foreach (XmlNode node in hostNodes)
                        {
                            tRow = new TableRow();

                            if (node.SelectSingleNode("Type") != null)
                                optionType = node.SelectSingleNode("Type").InnerText;

                            CANode = null;
                            CANode = node;

                            if (node.SelectSingleNode("CustomAttributeID") != null)
                                customAttId = node.SelectSingleNode("CustomAttributeID").InnerText;

                            if (customAttId == "")
                                continue;

                            customAttId = CANode.SelectSingleNode("CustomAttributeID").InnerText;

                            if (hostAttrIds == "")
                                hostAttrIds = "TxtCA_4_" + customAttId;
                            else
                                hostAttrIds = hostAttrIds + "?" + "TxtCA_4_" + customAttId;

                            if (node.SelectSingleNode("Status") != null)
                                isEnableDisplay = node.SelectSingleNode("Status").InnerText;

                            if (isEnableDisplay == "")
                                isEnableDisplay = "1"; //Disable

                            if (isEnableDisplay == "1")
                                continue;

                            if (node.SelectSingleNode("Mandatory") != null)
                                isRequired = node.SelectSingleNode("Mandatory").InnerText;

                            if (isRequired == "")
                                isRequired = "0";   //False

                            tCell = CreateExpressLabel();
                            tCell.Visible = true;
                            tCell.VerticalAlign = VerticalAlign.Middle;
                            tCell.HorizontalAlign = HorizontalAlign.Left;//FB 2592
                            tCell.Width = Unit.Percentage(32);
                            tRow.Cells.Add(tCell);
                            tCell = null;
                            
                            tCell = CreateExpressTextBox();
                            tCell.HorizontalAlign = HorizontalAlign.Left;
                            tCell.VerticalAlign = VerticalAlign.Middle;
                            tCell.Visible = true;
                            tRow.Cells.Add(tCell);
                            tCell = null;

                            tblHost.Rows.Add(tRow);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return hostAttrIds;
        }
        #endregion

        #region DisplaySpecialInfo

        private void DisplaySpecialInfo(ref Table tblSpecial)
        {
            TableRow tRow = null;
            TableCell tCell = null;
            try
            {
                if (specialNodes != null)
                {
                    if (specialNodes.Count > 0)
                    {
                        foreach (XmlNode node in specialNodes)
                        {
                            if (node.SelectSingleNode("Type") != null)
                                optionType = node.SelectSingleNode("Type").InnerText;

                            CANode = null;
                            CANode = node;

                            string controlText = CANode.SelectSingleNode("Title").InnerText.Trim();

                            if (node.SelectSingleNode("CustomAttributeID") != null)
                                customAttId = node.SelectSingleNode("CustomAttributeID").InnerText;

                            if (customAttId == "")
                                continue;

                            if (node.SelectSingleNode("Status") != null)
                                isEnableDisplay = node.SelectSingleNode("Status").InnerText;

                            if (isEnableDisplay == "")
                                isEnableDisplay = "1"; //Disable

                            if (isEnableDisplay == "1")
                                continue;

                            if (node.SelectSingleNode("Mandatory") != null)
                                isRequired = node.SelectSingleNode("Mandatory").InnerText;

                            if (isRequired == "")
                                isRequired = "0";   //False

                            tRow = new TableRow();
                            tCell = CreateExpressLabel();
                            tCell.Visible = true;
                            tCell.VerticalAlign = VerticalAlign.Middle;
                            tCell.HorizontalAlign = HorizontalAlign.Left;
                            tCell.Text = tCell.Text.Trim();
                            tRow.Cells.Add(tCell);
                            tblSpecial.Rows.Add(tRow);
                            tRow = null;
                            tCell = null;

                            //if (controlText.IndexOf("Type Instructions Here") >= 0)
                            //{
                            tRow = new TableRow();
                            tCell = CreateExpressMultilineText();
                            tCell.HorizontalAlign = HorizontalAlign.Left;
                            tCell.VerticalAlign = VerticalAlign.Middle;
                            tCell.Visible = true;
                            tRow.Cells.Add(tCell);
                            tblSpecial.Rows.Add(tRow);
                            tRow = null;
                            tCell = null;
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //FB 2592 - Starts
        #region ExpressAttributesInxml
        /// <summary>
        /// ExpressAttributesInxml
        /// </summary>
        /// <param name="customIDs"></param>
        /// <param name="tblHost"></param>
        /// <param name="tblSpecial"></param>
        /// <param name="customTable"></param>
        /// <returns></returns>
        public string ExpressAttributesInxml(string customIDs, Table tblHost, Table tblSpecial, Table customTable) //FB 2501 //FB 2632
        {
            string inxml = "";
            string optionId = "";
            string optionType = "";
            string optionValue = "";
            string reqValName = "";
            isValidationRequired = false;
            bool isReqNotSel = false;
            customAttId = "";
            try
            {
                if (customIDs != null && customIDs != "" && tblHost != null && tblSpecial != null && customTable != null)
                {
                    String[] custControls = customIDs.Split('?');

                    if (custControls.Length > 0)
                    {
                        //FB 2501 Starts
                        foreach (string custControlName in custControls)
                        {
                            String[] attributeDt = custControlName.Split('_');
                            if (attributeDt.Length > 0)
                            {
                                optionType = "";
                                optionType = attributeDt[1];
                                customAttId = attributeDt[2];

                                reqValName = "ReqValCA_" + customAttId; //FB 2592
                                RequiredFieldValidator reqV = (RequiredFieldValidator)customTable.FindControl(reqValName);
                                if (reqV != null)
                                    isValidationRequired = true;
                                else
                                    isValidationRequired = false;

                                switch (optionType)
                                {
                                    case "4":
                                    case "10":
                                    case "7": //FB 2592
                                        {
                                            Object tempObj = null;
                                            tempObj = tblHost.FindControl(custControlName);

                                            if (tempObj == null)
                                                tempObj = tblSpecial.FindControl(custControlName);

                                            if (tempObj == null)
                                                tempObj = customTable.FindControl(custControlName);

                                            if (tempObj != null)
                                            {
                                                TextBox txtTemp = (TextBox)tempObj;
                                                optionId = "-1";
                                                optionValue = txtTemp.Text.Trim();
                                            }

                                            //FB 2592 - Starts
                                            if (optionValue.Contains("&") || optionValue.Contains("<") || optionValue.Contains(">")) 
                                            {
                                                optionValue = optionValue.Replace("&", " ");
                                                optionValue = optionValue.Replace("<", " ");
                                                optionValue = optionValue.Replace(">", " ");
                                            }

                                            if (isValidationRequired)
                                            {
                                                if (optionValue == "")
                                                    isReqNotSel = true;
                                            }
                                            //FB 2592 - Starts
                                            break;
                                        }
                                    case "8": //FB 2592 - Starts
                                        {
                                            RadioButtonList rdList = (RadioButtonList)customTable.FindControl(custControlName);
                                            optionValue = "";
                                            if (rdList != null)
                                            {
                                                optionId = rdList.SelectedValue;

                                                if (rdList.SelectedItem != null)
                                                {
                                                    optionValue = rdList.SelectedItem.Text;
                                                }

                                                if (optionId == "")
                                                    optionValue = "";

                                                if (isValidationRequired)
                                                {
                                                    if (optionValue == "")
                                                        isReqNotSel = true;
                                                }
                                            }
                                            break;
                                        }
                                    case "2":
                                        {
                                            CheckBox chkTemp = (CheckBox)customTable.FindControl(custControlName);
                                            optionId = "-1";

                                            if (chkTemp.Checked)
                                            {
                                                optionValue = "1";
                                            }
                                            else
                                            {
                                                optionValue = "";
                                            }
                                            break;
                                        }
                                    case "3":
                                        {
                                            RadioButton optTemp = (RadioButton)customTable.FindControl(custControlName);
                                            optionId = "-1";
                                            if (optTemp.Checked)
                                            {
                                                optionValue = "1";
                                            }
                                            else
                                            {
                                                optionValue = "0";
                                            }
                                            break;
                                        }
                                    case "6":
                                        {
                                            DropDownList drpTemp = (DropDownList)customTable.FindControl(custControlName);
                                            optionId = drpTemp.SelectedValue;
                                            optionValue = drpTemp.SelectedItem.Text;

                                            if (optionId == "")
                                                optionValue = "";

                                            if (isValidationRequired)
                                            {
                                                if (optionValue == "")
                                                    isReqNotSel = true;
                                            }
                                            break;
                                        }
                                    case "5":
                                        {
                                            ListBox lstTemp = (ListBox)customTable.FindControl(custControlName);
                                            foreach (ListItem li in lstTemp.Items)
                                            {
                                                if (li.Selected && li.Value != "")
                                                {
                                                    optionValue = li.Text;
                                                    inxml += "<CustomAttribute>";
                                                    inxml += "<CustomAttributeID>" + customAttId + "</CustomAttributeID>";
                                                    inxml += "<OptionID>" + li.Value + "</OptionID>";
                                                    inxml += "<Type>" + optionType + "</Type>";
                                                    inxml += "<OptionValue>" + li.Text + "</OptionValue>";
                                                    inxml += "</CustomAttribute>";
                                                }
                                            }
                                            if (isValidationRequired)
                                            {
                                                if (optionValue == "")
                                                    isReqNotSel = true;
                                            }
                                            break;
                                        }
                                    //FB 2592 - End
                                }
                                if (optionValue != "")
                                {
                                    inxml += "<CustomAttribute>";
                                    inxml += "<CustomAttributeID>" + customAttId + "</CustomAttributeID>";
                                    inxml += "<OptionID>" + optionId + "</OptionID>";
                                    inxml += "<Type>" + optionType + "</Type>";
                                    inxml += "<OptionValue>" + optionValue + "</OptionValue>";
                                    inxml += "</CustomAttribute>";
                                }
                                optionId = "";
                                optionValue = "";
                            }
                        }
                    }
                }
                if (isReqNotSel)//FB 2592
                {
                    inxml = "<error>" + obj.GetTranslatedText("Please set the values for the mandatory custom options.") + "</error>";
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return inxml;
        }
        #endregion
        //FB 2592 - End

        #region Create Express Text Box

        private TableCell CreateExpressTextBox()
        {
            TableCell tCell = null;
            string controlUID = "";
            string txtSelValue = "";
            try
            {
                if (CANode != null)
                {
                    customAttId = CANode.SelectSingleNode("CustomAttributeID").InnerText;
                    controlUID = "TxtCA_4_" + customAttId;

                    txtSelValue = CANode.SelectSingleNode("SelectedValue").InnerText;

                    if (controlIDString == "")
                        controlIDString = controlUID;
                    else
                        controlIDString += "?" + controlUID;


                    TextBox txtTemp = new TextBox();
                    txtTemp.ID = controlUID;
                    txtTemp.CssClass = "altText";
                    txtTemp.EnableViewState = true;
                    txtTemp.Text = txtSelValue;
                    txtTemp.Width = Unit.Pixel(150);//FB 2592 - 100px

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.Controls.Add(txtTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        lstReqVal.Attributes.Add("style", "font-weight: normal");//FB 2592
                        tCell.Controls.Add(lstReqVal);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create Express Multiline Text

        private TableCell CreateExpressMultilineText()
        {
            TableCell tCell = null;
            string controlUID = "";
            string txtSelValue = "";
            try
            {
                if (CANode != null)
                {
                    customAttId = CANode.SelectSingleNode("CustomAttributeID").InnerText;
                    controlUID = "TxtCA_10_" + customAttId;

                    txtSelValue = CANode.SelectSingleNode("SelectedValue").InnerText;

                    if (controlIDString == "")
                        controlIDString = controlUID;
                    else
                        controlIDString += "?" + controlUID;


                    TextBox txtTemp = new TextBox();
                    txtTemp.ID = controlUID;
                    txtTemp.CssClass = "altText";
                    txtTemp.EnableViewState = true;
                    txtTemp.Text = txtSelValue;
                    txtTemp.TextMode = TextBoxMode.MultiLine;
                    txtTemp.Width = Unit.Pixel(310);   //FB 2341
                    txtTemp.Height = Unit.Pixel(80);

                    tCell = new TableCell();
                    tCell.HorizontalAlign = HorizontalAlign.Left;
                    tCell.Controls.Add(txtTemp);

                    if (isRequired == "1" && isValidationRequired)
                    {
                        RequiredFieldValidator lstReqVal = new RequiredFieldValidator();
                        lstReqVal.ID = "ReqValCA_" + customAttId;
                        lstReqVal.ControlToValidate = controlUID;
                        lstReqVal.Display = ValidatorDisplay.Dynamic;
                        lstReqVal.ErrorMessage = obj.GetTranslatedText("Required");
                        tCell.Controls.Add(lstReqVal);
                    }                 

                    //FB 2508
                    RegularExpressionValidator expMultiRangeval = new RegularExpressionValidator();
                    expMultiRangeval.ID = "ValCA_" + customAttId;
                    expMultiRangeval.ControlToValidate = controlUID;
                    expMultiRangeval.Display = ValidatorDisplay.Dynamic;
                    expMultiRangeval.ErrorMessage = "<br>" + obj.GetTranslatedText("Maximum limit is 4000 characters.");
                    expMultiRangeval.ValidationExpression = "^[\\s\\S]{0,4000}$";
                    tCell.Controls.Add(expMultiRangeval);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCell;
        }
        #endregion

        #region Create Control Labels for Express Interface

        private TableCell CreateExpressLabel()
        {
            TableCell tCol = new TableCell();
            try
            {
                string CALabel = CANode.SelectSingleNode("Title").InnerText.Trim();

                if (CALabel.ToLower() == "special instructions")
                    CALabel = obj.GetTranslatedText("Type Instructions Here");

                if (CALabel == "")
                    CALabel = "Custom Attribute" + customAttId;

                tCol.HorizontalAlign = HorizontalAlign.Left;//FB 2592
                tCol.ID = "td_" + customAttId;
                tCol.CssClass = "blackblodtext";

                string reqText = "";
                if (isRequired == "1" && isValidationRequired)
                {
                    reqText = "<span id='span1' class='reqfldstarText'>*</span>";
                }
                tCol.Text = CALabel + reqText;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return tCol;
        }
        #endregion
        //FB 1779 - Express Conference Specifics - End
    }
}
