﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Text;

namespace ns_DefaultLicense
{
    public partial class en_DefaultLicense : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        private int currRooms = 0;
        private int currVRooms = 0;
        private int currNVRooms = 0;
        private int currMCU = 0;
        private int currUsers = 0;
        private int currEUsers = 0;
        private int currDUsers = 0;
        private int currMUsers = 0;
        private int currEndpoint = 0;
        private int currMCUEnchanced = 0;
        private int currVMRRooms = 0;
        private int currExtRooms = 0;
        private int currGstPerUser = 0;
        private int enableFacility = 0;
        private int enableCatering = 0;
        private int enableHK = 0;
        private int enableAPI = 0;
        private int enablePC = 0;
        private int currVCHotRooms = 0, currROHotRooms;
        private int currPCUsers = 0;
        private int enableBJ = 0, enableJabber = 0, enableLync = 0, enableVidtel = 0;
        private int enableCloud = 0;
        private int enablePublicRoom = 0;
        private int enableadvancedreport = 0;

        #region Protected Data Members
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveExUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveDomUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMobUsers; 
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMCU;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMCUEnchanced;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveVRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveNVRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveEpts;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveFacility;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveCat;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveHK;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveAPI;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanExternalRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanBJ;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanJabber;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanLync;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanVidtel;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActivePCUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanGuRoomUser;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanVMRRooms;

        protected System.Web.UI.WebControls.ImageButton img_Rooms;
        protected System.Web.UI.WebControls.ImageButton img_Modules;
        protected System.Web.UI.WebControls.ImageButton img_MCUs;
        protected System.Web.UI.WebControls.ImageButton img_Users;

        protected System.Web.UI.HtmlControls.HtmlTable tblRooms;
        protected System.Web.UI.HtmlControls.HtmlTable tblMCUs;
        protected System.Web.UI.HtmlControls.HtmlTable tblusers;
        protected System.Web.UI.HtmlControls.HtmlTable tblModules;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPCUser;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBJ;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnJabber;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLync;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVidtel;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCU;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUEncha;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnNVRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMUsers; 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndPoint;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFacility;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCatering;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHK;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAPI;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnExtRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVCHotdesking;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnROHotdesking;

        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveVC;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveRO;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanAdvReport;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAdvReport;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVMR;

        protected System.Web.UI.WebControls.CheckBox ChkFacility;
        protected System.Web.UI.WebControls.CheckBox ChkCatering;
        protected System.Web.UI.WebControls.CheckBox ChkHK;
        protected System.Web.UI.WebControls.CheckBox ChkAPI;
        protected System.Web.UI.WebControls.CheckBox chkBlueJeans;
        protected System.Web.UI.WebControls.CheckBox chkJabber;
        protected System.Web.UI.WebControls.CheckBox chkLync;
        protected System.Web.UI.WebControls.CheckBox chkVidtel;
        protected System.Web.UI.WebControls.CheckBox ChkAdvReport;

        protected System.Web.UI.WebControls.TextBox TxtMCU;
        protected System.Web.UI.WebControls.TextBox TxtUsers;
        protected System.Web.UI.WebControls.TextBox TxtExUsers;
        protected System.Web.UI.WebControls.TextBox TxtDomUsers;
        protected System.Web.UI.WebControls.TextBox TxtMobUsers;
        protected System.Web.UI.WebControls.TextBox TxtRooms;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox TxtEndPoint;
        protected System.Web.UI.WebControls.TextBox TxtVRooms;
        protected System.Web.UI.WebControls.TextBox TxtNVRooms;
        protected System.Web.UI.WebControls.TextBox TxtExtRooms;
        protected System.Web.UI.WebControls.TextBox TxtGstPerUser;
        protected System.Web.UI.WebControls.TextBox TxtMCUEncha;
        protected System.Web.UI.WebControls.TextBox TxtVMR;
        protected System.Web.UI.WebControls.TextBox txtPCUser;
        protected System.Web.UI.WebControls.TextBox txtVCHotRooms;
        protected System.Web.UI.WebControls.TextBox txtROHotRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGstPerUser;
        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("defaultlicense.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();

            img_Rooms.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblRooms.ClientID + "', false);return false;");
            img_Modules.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblModules.ClientID + "', false);return false;");
            img_MCUs.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblMCUs.ClientID + "', false);return false;");
            img_Users.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblusers.ClientID + "', false);return false;");

            if (!IsPostBack)
            {
                BindData();
            }
            SpanActiveRooms.InnerText = hdnRooms.Value;
            SpanActiveRooms.Style.Add("color", "#1F8CC0");
            SpanActiveVRooms.InnerText = hdnVRooms.Value;
            SpanActiveNVRooms.InnerText = hdnNVRooms.Value;
            SpanActiveMCU.InnerText = hdnMCU.Value;
            SpanActiveUsers.InnerText = hdnUsers.Value;
            SpanActiveExUsers.InnerText = hdnEUsers.Value;
            SpanActiveDomUsers.InnerText = hdnDUsers.Value;
            SpanActiveMobUsers.InnerText = hdnMUsers.Value;
            SpanActiveEpts.InnerText = hdnEndPoint.Value;
            SpanExternalRooms.InnerText = hdnExtRooms.Value;
            SpanActiveFacility.InnerText = hdnFacility.Value;
            SpanActiveCat.InnerText = hdnCatering.Value;
            SpanActiveHK.InnerText = hdnHK.Value;
            SpanActiveAPI.InnerText = hdnAPI.Value;
            SpanActiveMCUEnchanced.InnerText = hdnMCUEncha.Value;
            SpanVMRRooms.InnerText = hdnVMR.Value;
            SpanBJ.InnerText = hdnBJ.Value;
            SpanJabber.InnerText = hdnJabber.Value;
            SpanLync.InnerText = hdnLync.Value;
            SpanVidtel.InnerText = hdnVidtel.Value;
            SpanActivePCUsers.InnerText = hdnPCUser.Value;
            SpanGuRoomUser.InnerText = obj.GetTranslatedText("N/A");
            SpanActiveVC.InnerText = hdnVCHotdesking.Value;
            SpanActiveRO.InnerText = hdnROHotdesking.Value;
            SpanAdvReport.InnerText = hdnAdvReport.Value;

            if (hdnFacility.Value == "0")
            {
                ChkFacility.Enabled = false;
                ChkFacility.Checked = false;
            }
            if (hdnCatering.Value == "0")
            {
                ChkCatering.Enabled = false;
                ChkCatering.Checked = false;
            }
            if (hdnHK.Value == "0")
            {
                ChkHK.Enabled = false;
                ChkHK.Enabled = false;
            }
            if (hdnAPI.Value == "0")
            {
                ChkAPI.Enabled = false;
                ChkAPI.Checked = false;
            }
            if (hdnBJ.Value == "0")
            {
                chkBlueJeans.Enabled = false;
                chkBlueJeans.Checked = false;
            }
            if (hdnJabber.Value == "0")
            {
                chkJabber.Enabled = false;
                chkJabber.Checked = false;
            }
            if (hdnLync.Value == "0")
            {
                chkLync.Enabled = false;
                chkLync.Checked = false;
            }
            if (hdnVidtel.Value == "0")
            {
                chkVidtel.Enabled = false;
                chkVidtel.Checked = false;
            }
            if (hdnAdvReport.Value == "0")
            {
                ChkAdvReport.Enabled = false;
                ChkAdvReport.Checked = false;
            }
        }

        #region SetDefaultLicense
        /// <summary>
        /// SetDefaultLicense
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetDefaultLicense(object sender, EventArgs e)
        {
            try
            {
                if (!validatelicense())
                {
                    return;
                }

                enableFacility = 0;
                enableCatering = 0;
                enableHK = 0;
                enableAPI = 0;
                enablePC = 0;
                enableBJ = 0; enableJabber = 0; enableLync = 0; enableVidtel = 0;

                if (ChkFacility.Checked == true)
                    enableFacility = 1;
                if (ChkCatering.Checked == true)
                    enableCatering = 1;
                if (ChkHK.Checked == true)
                    enableHK = 1;
                if (ChkAPI.Checked == true)
                    enableAPI = 1;

                if (chkBlueJeans.Checked == true)
                    enableBJ = 1;
                if (chkJabber.Checked == true)
                    enableJabber = 1;
                if (chkLync.Checked == true)
                    enableLync = 1;
                if (chkVidtel.Checked == true)
                    enableVidtel = 1;
                if (ChkAdvReport.Checked == true)
                    enableadvancedreport = 1;




                StringBuilder inXML = new StringBuilder();
                inXML.Append("<SetDefaultLicense>");
                inXML.Append("<LastModifiedUser>" + Session["userID"].ToString() + "</LastModifiedUser>");//FB 3007
                inXML.Append("<roomlimit>" + TxtRooms.Text + "</roomlimit>");
                inXML.Append("<videoroomlimit>" + TxtVRooms.Text + "</videoroomlimit>");
                inXML.Append("<nonvideoroomlimit>" + TxtNVRooms.Text + "</nonvideoroomlimit>");
                inXML.Append("<userlimit>" + TxtUsers.Text + "</userlimit>");
                inXML.Append("<exchangeusers>" + TxtExUsers.Text + "</exchangeusers>");
                inXML.Append("<dominousers>" + TxtDomUsers.Text + "</dominousers>");
                inXML.Append("<mobileusers>" + TxtMobUsers.Text + "</mobileusers>"); //FB 1979
                inXML.Append("<mculimit>" + TxtMCU.Text + "</mculimit>");
                inXML.Append("<endpointlimit>" + TxtEndPoint.Text + "</endpointlimit>");
                inXML.Append("<mcuenchancedlimit>" + TxtMCUEncha.Text + "</mcuenchancedlimit>");//FB 2486
                inXML.Append("<vmrroomlimit>" + TxtVMR.Text + "</vmrroomlimit>");//FB 2586

                //FB 2426 Start
                inXML.Append("<GuestRooms>" + TxtExtRooms.Text + "</GuestRooms>");
                inXML.Append("<GuestRoomPerUser>" + TxtGstPerUser.Text + "</GuestRoomPerUser>");
                Session["GuestRooms"] = TxtExtRooms.Text;
                //FB 2426 End
                inXML.Append("<enablefacilities>" + enableFacility + "</enablefacilities>");
                inXML.Append("<enablecatering>" + enableCatering + "</enablecatering>");
                inXML.Append("<enablehousekeeping>" + enableHK + "</enablehousekeeping>");
                inXML.Append("<enableAPI>" + enableAPI + "</enableAPI>");

                //inXML += "<enablePC>" + enablePC + "</enablePC>"; //FB 2347 //FB 2693
                //inXML.Append("<enableCloud>" + enableCloud + "</enableCloud>"); //FB 2262 - J  //FB 2599
                //inXML.Append("<EnablePublicRoom>" + enablePublicRoom + "</EnablePublicRoom>"); //FB 2594

                //FB 2693 Starts
                inXML.Append("<pcusers>" + txtPCUser.Text + "</pcusers>");
                inXML.Append("<enableBlueJeans>" + enableBJ + "</enableBlueJeans>");
                inXML.Append("<enableJabber>" + enableJabber + "</enableJabber>");
                inXML.Append("<enableLync>" + enableLync + "</enableLync>");
                inXML.Append("<enableVidtel>" + enableVidtel + "</enableVidtel>");
                //FB 2693 Ends
                //FB 2694 Starts
                inXML.Append("<VCHotRooms>" + txtVCHotRooms.Text + "</VCHotRooms>");
                inXML.Append("<ROHotRooms>" + txtROHotRooms.Text + "</ROHotRooms>");
                inXML.Append("<AdvancedReports>" + enableadvancedreport + "</AdvancedReports>");

                inXML.Append("</SetDefaultLicense>");

                string outXML = obj.CallMyVRMServer("SetDefaultLicense", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    //Response.Redirect("SuperAdministrator.aspx");
                    Response.Redirect("SuperAdministrator.aspx");
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("SetDefaultLicense: " + ex.Message);
            }
        }
        #endregion

        #region ValidateLicense

        private Boolean validatelicense()
        {
            Int32 actMcus = 0, actUsers = 0, actEUsers = 0, actDUsers = 0, actMUsers = 0; //FB 1979
            Int32 actVRms = 0, actNVRms = 0, actEndpts = 0, actExtRooms = 0, actGstPerUser = 0; //FB 2426
            Int32 actMCUEnchanced = 0;
            Int32 actVMRRooms = 0;//FB 2586

            int actPCUsers = 0; //FB 2693
            int actVCHotRooms = 0, actROHotRooms = 0; //FB 2694
            try
            {
                Int32.TryParse(TxtMCU.Text, out actMcus);
                Int32.TryParse(TxtUsers.Text, out actUsers);
                Int32.TryParse(TxtExUsers.Text, out actEUsers);
                Int32.TryParse(TxtDomUsers.Text, out actDUsers);
                Int32.TryParse(TxtMobUsers.Text, out actMUsers); //FB 1979
                Int32.TryParse(TxtVRooms.Text, out actVRms);
                Int32.TryParse(TxtNVRooms.Text, out actNVRms);
                Int32.TryParse(TxtEndPoint.Text, out actEndpts);
                Int32.TryParse(TxtMCUEncha.Text, out actMCUEnchanced);//FB 2486
                Int32.TryParse(TxtExtRooms.Text, out actExtRooms);
                Int32.TryParse(TxtGstPerUser.Text, out actGstPerUser);
                Int32.TryParse(TxtVMR.Text, out actVMRRooms);//FB 2586
                Int32.TryParse(txtVCHotRooms.Text, out actVCHotRooms);//FB 2694
                Int32.TryParse(txtROHotRooms.Text, out actROHotRooms);//FB 2694
                Int32.TryParse(txtPCUser.Text, out actPCUsers);

                Int32.TryParse(hdnUsers.Value, out currUsers);
                Int32.TryParse(hdnEUsers.Value, out currEUsers);
                Int32.TryParse(hdnDUsers.Value, out currDUsers);
                Int32.TryParse(hdnMUsers.Value, out currMUsers); //FB 1979
                Int32.TryParse(hdnMCU.Value, out currMCU);
                Int32.TryParse(hdnVRooms.Value, out currVRooms);
                Int32.TryParse(hdnNVRooms.Value, out currNVRooms);
                Int32.TryParse(hdnEndPoint.Value, out currEndpoint);
                Int32.TryParse(hdnMCUEncha.Value, out currMCUEnchanced);//FB 2486
                Int32.TryParse(hdnExtRooms.Value, out currExtRooms);
                Int32.TryParse(hdnGstPerUser.Value, out currGstPerUser);
                Int32.TryParse(hdnVMR.Value, out currVMRRooms);//FB 2586
                int.TryParse(hdnPCUser.Value, out currPCUsers);
                //int.TryParse(hdnPCUser.Value, out currPCUsers);
                //FB 2693 Ends
                Int32.TryParse(hdnVCHotdesking.Value, out currVCHotRooms);//FB 2694
                Int32.TryParse(hdnROHotdesking.Value, out currROHotRooms);//FB 2694


                //FB 1881 start                
                if (actVRms > currVRooms)
                    throw new Exception(obj.GetErrorMessage(455)); //"Video rooms limit exceeds VRM license."

                if (actNVRms > currNVRooms)
                    throw new Exception(obj.GetErrorMessage(456)); //"Non-Video rooms limit exceeds VRM license.");
                //FB 2694 Starts
                if (actVCHotRooms > currVCHotRooms)
                    throw new Exception(obj.GetErrorMessage(693)); //"VC Hotdesking rooms limit exceeds VRM license.");

                if (actROHotRooms > currROHotRooms)
                    throw new Exception(obj.GetErrorMessage(694)); //"RO Hotdesking rooms limit exceeds VRM license.");
                //FB 2694 End
                if (actMcus > currMCU)
                    throw new Exception(obj.GetErrorMessage(457)); //"MCU limit exceeds VRM license.");

                if (actMCUEnchanced > currMCUEnchanced)
                    throw new Exception(obj.GetErrorMessage(624)); //"MCU Enchanced limit exceeds VRM license.");

                if (actMCUEnchanced > actMcus)//FB 2486 
                    throw new Exception(obj.GetErrorMessage(626)); //"MCU Enchanced limit exceeds active Standard MCU.");

                if (actEndpts > currEndpoint)
                    throw new Exception(obj.GetErrorMessage(458)); //"Endpoints limit exceeds VRM license.");

                if (actUsers > currUsers)
                    throw new Exception(obj.GetErrorMessage(459)); //"User limit exceeds VRM license.");

                if (actEUsers > currEUsers)
                    throw new Exception(obj.GetErrorMessage(460)); //"Exchange user limit exceeds VRM license.");

                if (actDUsers > currDUsers)
                    throw new Exception(obj.GetErrorMessage(461)); //"Domino user limit exceeds VRM license.");

                if (actMUsers > currMUsers) //FB 1979
                    throw new Exception(obj.GetErrorMessage(526)); //"Mobile user limit exceeds VRM license.");

                //FB 2693 Starts
                if (actPCUsers > currPCUsers)
                    throw new Exception(obj.GetErrorMessage(682));  //PC user limit exceeds VRM license.
                //FB 26963 Ends

                if(actVMRRooms > currVMRRooms)
                    throw new Exception(obj.GetErrorMessage(655)); //"VMR rooms limit exceeds VRM license.");

                if ((actEUsers + actDUsers + actMUsers + actPCUsers) > actUsers) //FB 1979 //FB 2693
                    throw new Exception(obj.GetErrorMessage(462)); //"Active users limit should be inclusive of both domino and exchange users.");

                //FB 2426 Start
                if (actExtRooms > currExtRooms)
                    throw new Exception(obj.GetErrorMessage(618)); //"Guest Room limit exceeds VRM license.");

                if (actGstPerUser > actExtRooms)
                    throw new Exception(obj.GetErrorMessage(619)); //"Guest Room limit per user exceeds Guest Room limit.");
                //FB 2426 End limit
            }
            catch (Exception ex)
            {
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
                errLabel.Visible = true;
                log.Trace("SetOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
            return true;
        }

        #endregion

        #region BindData
        /// <summary>
        /// BindData
        /// </summary>
        protected void BindData()
        {
            try
            {
                string inXML = "<GetDefaultLicense><userid>" + Session["userID"].ToString() + "</userid></GetDefaultLicense>";

                string outXML = obj.CallMyVRMServer("GetDefaultLicense", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    TxtRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/Rooms").InnerText;
                    TxtVRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/VideoRooms").InnerText;
                    TxtNVRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/NonVideoRooms").InnerText;
                    TxtUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/Users").InnerText;
                    TxtExUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/ExchangeUsers").InnerText;
                    TxtDomUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/DominoUsers").InnerText;
                    TxtMobUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/MobileUsers").InnerText; //FB 1979
                    TxtMCU.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/MCU").InnerText;
                    TxtEndPoint.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/EndPoints").InnerText;
                    TxtMCUEncha.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/MCUEnchanced").InnerText;//FB 2486
                    //FB 2426 Start
                    TxtExtRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/GuestRooms").InnerText;
                    TxtGstPerUser.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/GuestRoomPerUser").InnerText;
                    //FB 2426 End
                    TxtVMR.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/VMRRooms").InnerText;//FB 2586
                    string enabAV = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableFacilites").InnerText;
                    string enabCat = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableCatering").InnerText;
                    string enabHK = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableHouseKeeping").InnerText;
                    string enabAPI = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableAPIs").InnerText;
                    //string enabPC = xmldoc.SelectSingleNode("//OrganizationProfile/EnablePC").InnerText; //FB 2347 //FB 2693

                    //if (xmldoc.SelectSingleNode("//GetDefaultLicense/EnableCloud") != null) //FB 2599 feb 22
                    //    Int32.TryParse(xmldoc.SelectSingleNode("//GetDefaultLicense/EnableCloud").InnerText, out enableCloud); //FB 2262  
                    //FB 2594 Starts
                    //if (xmldoc.SelectSingleNode("//GetDefaultLicense/EnablePublicRoom") != null)
                    //    Int32.TryParse(xmldoc.SelectSingleNode("//GetDefaultLicense/EnablePublicRoom").InnerText, out enablePublicRoom);
                    //FB 2594 Ends

                    //FB 2693 Starts
                    if (xmldoc.SelectSingleNode("//GetDefaultLicense/PCUsers") != null)
                        txtPCUser.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/PCUsers").InnerText;

                    //if (xmldoc.SelectSingleNode("//GetDefaultLicense/EnableBlueJeans") != null)
                    //int.TryParse(xmldoc.SelectSingleNode("//GetDefaultLicense/EnableBlueJeans").InnerText.Trim(), out enableBJ);

                    string enableBJ = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableBlueJeans").InnerText;

                    //if (xmldoc.SelectSingleNode("//GetDefaultLicense/EnableJabber") != null)
                    //int.TryParse(xmldoc.SelectSingleNode("//GetDefaultLicense/EnableJabber").InnerText.Trim(), out enableJabber);

                    string enableJabber = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableJabber").InnerText;

                    //if (xmldoc.SelectSingleNode("//GetDefaultLicense/EnableLync") != null)
                    //int.TryParse(xmldoc.SelectSingleNode("//GetDefaultLicense/EnableLync").InnerText.Trim(), out enableLync);

                    string enableLync = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableLync").InnerText;

                    //if (xmldoc.SelectSingleNode("//GetDefaultLicense/EnableVidtel") != null)
                    //int.TryParse(xmldoc.SelectSingleNode("//GetDefaultLicense/EnableVidtel").InnerText.Trim(), out enableVidtel);

                    string enableVidtel = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableVidtel").InnerText;

                    //FB 2693 Ends
                    //FB 2694 Starts
                    if (xmldoc.SelectSingleNode("//GetDefaultLicense/VCHotRooms") != null)
                        txtVCHotRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/VCHotRooms").InnerText.Trim();

                    if (xmldoc.SelectSingleNode("//GetDefaultLicense/ROHotRooms") != null)
                        txtROHotRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/ROHotRooms").InnerText.Trim();

                    string enableadvancedreport = xmldoc.SelectSingleNode("//GetDefaultLicense/AdvancedReports").InnerText;

                    if (enabAV == "1")
                        ChkFacility.Checked = true;
                    if (enabCat == "1")
                        ChkCatering.Checked = true;
                    if (enabHK == "1")
                        ChkHK.Checked = true;
                    if (enabAPI == "1")
                        ChkAPI.Checked = true;
                    //if (enabPC == "1")
                    //    ChkPC.Checked = true;

                    if (enableBJ == "1")
                        chkBlueJeans.Checked = true;
                    if (enableJabber == "1")
                        chkJabber.Checked = true;
                    if (enableLync == "1")
                        chkLync.Checked = true;
                    if (enableLync == "1")
                        chkVidtel.Checked = true;
                    if (enableadvancedreport == "1")
                        ChkAdvReport.Checked = true;


                }

                //Int32.TryParse(TxtRooms.Text.Trim(), out currRooms);
                //Int32.TryParse(TxtVRooms.Text.Trim(), out currVRooms);
                //Int32.TryParse(TxtNVRooms.Text.Trim(), out currNVRooms);
                //Int32.TryParse(TxtMCU.Text.Trim(), out currMCU);
                //Int32.TryParse(TxtUsers.Text.Trim(), out currUsers);
                //Int32.TryParse(TxtExUsers.Text.Trim(), out currEUsers);
                //Int32.TryParse(TxtDomUsers.Text.Trim(), out currDUsers);
                //Int32.TryParse(TxtMobUsers.Text.Trim(), out currMUsers); //FB 1979
                //Int32.TryParse(TxtEndPoint.Text.Trim(), out currEndpoint);
                //Int32.TryParse(TxtMCUEncha.Text.Trim(), out currMCUEnchanced);//FB 2486
                //Int32.TryParse(TxtExtRooms.Text.Trim(), out currExtRooms);
                //Int32.TryParse(TxtGstPerUser.Text.Trim(), out currGstPerUser);
                //Int32.TryParse(TxtVMR.Text.Trim(), out currVMRRooms);//FB 2586
                //Int32.TryParse(enabAV, out enableFacility);
                //Int32.TryParse(enabCat, out enableCatering);
                //Int32.TryParse(enabHK, out enableHK);
                //Int32.TryParse(enabAPI, out enableAPI);
                //Int32.TryParse(enabPC, out enablePC); //FB 2347

                obj.GetSysLicenseInfo();


                hdnRooms.Value = obj.remainingRooms.ToString();
                hdnVRooms.Value = obj.remainingVRooms.ToString();
                hdnNVRooms.Value = obj.remainingNVRooms.ToString();
                hdnMCU.Value = obj.remainingMCUs.ToString();
                hdnUsers.Value = obj.remainingUsers.ToString();
                hdnEUsers.Value = obj.remExchangeUsers.ToString();
                hdnDUsers.Value = obj.remDominoUsers.ToString();
                hdnMUsers.Value = obj.remMobileUsers.ToString();
                hdnEndPoint.Value = obj.remainingEndPoints.ToString();
                hdnMCUEncha.Value = obj.remainingEnchancedMCUs.ToString();
                hdnExtRooms.Value = obj.remainingExtRooms.ToString();
                hdnGstPerUser.Value = obj.remainingGstRoomPerUser.ToString();
                hdnVMR.Value = obj.remainingVMRRooms.ToString();
                hdnFacility.Value = obj.remainingFacilities.ToString();
                hdnCatering.Value = obj.remainingCatering.ToString();
                hdnHK.Value = obj.remainingHouseKeeping.ToString();
                hdnAPI.Value = obj.remainingAPI.ToString();
                hdnVCHotdesking.Value = obj.remainingVCHotRooms.ToString();
                hdnROHotdesking.Value = obj.remainingROHotRooms.ToString();
                hdnPCUser.Value = obj.remainingPCUsers.ToString();
                hdnBJ.Value = obj.remainingBJ.ToString();
                hdnJabber.Value = obj.remainingJabber.ToString();
                hdnLync.Value = obj.remainingLync.ToString();
                hdnVidtel.Value = obj.remainingVidtel.ToString();
                hdnAdvReport.Value = obj.remainingAdvReport.ToString();


            }
            catch (Exception ex)
            {
                log.Trace("GetDefaultLicense BindData: " + ex.Message);
            }
        }
        #endregion
    }
}