/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Department
{
    public partial class Department : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtMultiDepartment;
        protected System.Web.UI.WebControls.TextBox txtNewDepartmentName;
        protected System.Web.UI.WebControls.Table tblNoDepartments;
        protected System.Web.UI.WebControls.DataGrid dgDepartments;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNew;  //FB 2670
        protected System.Web.UI.HtmlControls.HtmlTableRow trNew1;
        protected System.Web.UI.WebControls.Button btnCreateNewDepartment;//FB 2670
        public Department()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("managedepartment.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                
                //reqDepartmentName.Enabled = true;
                //                DateTime startTime = DateTime.Now;
                //                Response.Write(("<br>Start time:" + startTime.ToString()));
                lblHeader.Text = obj.GetTranslatedText("Manage Departments");//FB 1830 - Translation
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    trNew.Visible = false;
                    //trNew1.Visible = false;
                    
                    // btnCreateNewDepartment.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //btnCreateNewDepartment.Attributes.Add("Class", "btndisable");// FB 2796
                    btnCreateNewDepartment.Visible = false;//ZD 100263
                }
                 // FB 2796 start
                else
                    btnCreateNewDepartment.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                 // FB 2796 End

                if (!IsPostBack)
                    BindData();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("Page_Load" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "PageLoad: " + ex.StackTrace;
            }

        }

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetManageDepartment", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    txtMultiDepartment.Text = xmldoc.SelectSingleNode("//getManageDepartment/multiDepartment").InnerText;
                    XmlNodeList nodes = xmldoc.SelectNodes("//getManageDepartment/departments/department");
                    LoadDepartmentGrid(nodes);
                    if (nodes.Count > 0)
                    {
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgDepartments.Controls[0].Controls[dgDepartments.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text = nodes.Count.ToString();
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible=true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }

        protected void LoadDepartmentGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    Session.Add("DepartmentDS", dt);
                    dgDepartments.DataSource = dt;
                    dgDepartments.DataBind();
                }
                else
                    tblNoDepartments.Visible = true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadDepartmentGrid" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        #endregion
        protected void DeleteDepartment(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <edit></edit>";
                inXML += "  <delete>";
                inXML += "      <departmentID>" + e.Item.Cells[0].Text + "</departmentID>";
                inXML += "  </delete>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("UpdateManageDepartment", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(outXML.IndexOf("<error>"));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageDepartment.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteDepartment" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void EditDepartment(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = false;
                DataTable dtTemp = (DataTable)Session["DepartmentDS"];
                dgDepartments.EditItemIndex = e.Item.ItemIndex;
                Session.Add("DepartmentID", e.Item.Cells[0].Text);
                //Response.Write(dtTemp.Rows.Count);
                dgDepartments.DataSource = dtTemp;
                dgDepartments.DataBind();
                //foreach (DataGridItem dgi in dgDepartments.Items)
                //    if (dgi.ItemIndex.Equals(e.Item.ItemIndex))
                //    {
                //        LinkButton btnTemp = (LinkButton)dgi.FindControl("btnUpdate");
                //        btnTemp.Visible = true;
                //        btnTemp = (LinkButton)dgi.FindControl("btnDelete");
                //        btnTemp.Visible = false;
                //        btnTemp = (LinkButton)dgi.FindControl("btnCancel");
                //        btnTemp.Visible = true;
                //        btnTemp = (LinkButton)dgi.FindControl("btnEdit");
                //        btnTemp.Visible = false;
                //    }//Response.Write(":in edit");
                Label lblTemp = new Label();
                DataGridItem dgFooter = (DataGridItem)dgDepartments.Controls[0].Controls[dgDepartments.Controls[0].Controls.Count - 1];
                lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                lblTemp.Text = dgDepartments.Items.Count.ToString();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("EditDepartment" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void CancelDepartment(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Response.Redirect("ManageDepartment.aspx");
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CancelDepartment" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void UpdateDepartment(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                TextBox tbTemp = (TextBox)e.Item.FindControl("txtDepartmentName");
                String outXML = UpdateManageDepartment(tbTemp);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageDepartment.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("UpdateDepartment" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected String UpdateManageDepartment(TextBox tbTemp)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <delete></delete>";
                inXML += "  <edit>";
                inXML += "      <departmentID>" + Session["DepartmentID"].ToString() + "</departmentID>";
                inXML += "      <multiDepartment>" + txtMultiDepartment.Text + "</multiDepartment>";
                inXML += "      <departmentName>" + tbTemp.Text + "</departmentName>";
                inXML += "      <approvers>";
                inXML += "          <approver>";
                inXML += "              <ID></ID>";
                inXML += "              <firstName></firstName>";
                inXML += "              <lastName></lastName>";
                inXML += "          </approver>";
                inXML += "          <approver>";
                inXML += "              <ID></ID>";
                inXML += "              <firstName></firstName>";
                inXML += "              <lastName></lastName>";
                inXML += "          </approver>";
                inXML += "          <approver>";
                inXML += "              <ID></ID>";
                inXML += "              <firstName></firstName>";
                inXML += "              <lastName></lastName>";
                inXML += "          </approver>";
                inXML += "      </approvers>";
                inXML += "  </edit>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("UpdateManageDepartment", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.Write(outXML.IndexOf("<error>"));
                //Response.End();
                return outXML;
            }
            catch (Exception ex)
            {
                log.Trace("UpdateManageDepartment:Error in Setting Department" + ex.Message);//FB 1881
                //return "Error in Setting Department";
                return obj.ShowSystemMessage();//FB 1881
            }
        }

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    //FB 2670
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this department?") + "')");//FB japnese

                    if (Session["admin"].ToString() == "3")
                    {
                        
                        //btnTemp.Attributes.Remove("onClick");
                        //btnTemp.Style.Add("cursor", "default");
                        //btnTemp.ForeColor = System.Drawing.Color.Gray;

                        
                        //btnEdit.Attributes.Remove("onClick");
                        //btnEdit.Style.Add("cursor", "default");
                        //btnEdit.ForeColor = System.Drawing.Color.Gray;
                        btnTemp.Visible = false;//ZD 100263
                        btnEdit.Visible = false;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindRowsDeleteMessage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void CreateNewDepartment(Object sender, EventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = true;
                Session.Add("DepartmentID", "new");
                String outXML = UpdateManageDepartment(txtNewDepartmentName);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageDepartment.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CreateNewDepartment" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
    }
}
