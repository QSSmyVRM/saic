/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxMenu;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts;
using DevExpress.Utils;
                

/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

public partial class ESEventReport : System.Web.UI.Page
{
    #region protected Members
    
    protected System.Web.UI.HtmlControls.HtmlTableRow rptImgRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow trDetails;
    protected System.Web.UI.HtmlControls.HtmlGenericControl MainDiv;
    protected System.Web.UI.WebControls.Label lblHeading;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRequestID;

    protected DevExpress.Web.ASPxPopupControl.ASPxPopupControl ASPxPopupControl1;
    protected DevExpress.Web.ASPxGridView.ASPxGridView MainGrid;
    protected DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter gridExport;
    protected DevExpress.Web.ASPxEditors.ASPxDateEdit startDateedit;
    protected DevExpress.Web.ASPxEditors.ASPxDateEdit endDateedit;
    protected DevExpress.Web.ASPxEditors.ASPxCheckBox chkNew;
    protected DevExpress.Web.ASPxEditors.ASPxCheckBox chkInProgress;
    protected DevExpress.Web.ASPxEditors.ASPxCheckBox chkCompleted;
    protected DevExpress.Web.ASPxEditors.ASPxCheckBox chkError;
    protected DevExpress.Web.ASPxEditors.ASPxCheckBox chkCancel;
    
    #endregion

    #region protected enums

    protected enum MainMenuSelection
    {
        Conference = 1, Host, Participant, SheduledConference, ConfType, ConfBehavior, ConfTime, Duration
    }

    #endregion

    #region protected data members 

    private myVRMNet.NETFunctions obj;
    private ns_Logger.Logger log;

    protected Int32 orgId = 11;
    protected XmlDocument xmlDoc = null;
    protected DataSet ds = null;
    protected DataTable rptTable = new DataTable();
    protected DataTable participantTable = new DataTable();
    protected String format = "dd/MM/yyyy";
    protected String tformat = "hh:mm tt";
    protected String tmzone = "";
    protected String organizationID = "";
    protected Int32 usrID = 11;

    #endregion

    #region Constructor 
    public ESEventReport()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
        //imageUtilObj = new myVRMNet.ImageUtil(); 
    }

    #endregion

    #region Page_init
    /// <summary>
    /// Page_init
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_init(object sender, EventArgs e)
    {
        try
        {
            //Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

            if (!IsPostBack)
                Session["ReportXML"] = null;

            if (Session["organizationID"] != null)
                Int32.TryParse(Session["organizationID"].ToString(), out orgId);

            //String strList = "11:1:Sales;11:2:Engineering;11:3:larry;12:4:New Dept";
            //hdnDepartmentList.Value = strList;
            
            if (Session["timezoneID"] != null)
                tmzone = Session["timezoneID"].ToString();
            else
                tmzone = "26";

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            if (Session["timeFormat"] != null)
            {
                if (Session["timeFormat"].ToString() != "")
                    if (Session["timeFormat"].ToString() == "0")
                        tformat = "HH:mm";
            }

            startDateedit.DisplayFormatString = format;
            startDateedit.EditFormatString = format;

            endDateedit.DisplayFormatString = format;
            endDateedit.EditFormatString = format;

            //if (Session["hdnhdnSubmitValue"] != null && Session["hdnhdnSubmitValue"].ToString() != "" && hdnSubmitValue.Value == "")
            //    hdnSubmitValue.Value = Session["hdnhdnSubmitValue"].ToString();

            if (Session["ReportXML"] != null) // && hdnOkValue.Value == "")
                btnOk_Click(null, null);

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_init" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            //errLabel.Text = ex.Message;
        }
    }

    #endregion

    #region Methods Executed on Page Load 

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("eseventreport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            if (Session["userID"] != null)
            {
                if (Session["userID"].ToString() != "")
                    Int32.TryParse(Session["userID"].ToString(), out usrID);
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            if (Session["timeFormat"] != null)
            {
                if (Session["timeFormat"].ToString() != "")
                    if (Session["timeFormat"].ToString() == "0")
                        tformat = "HH:mm";
            }

            if (hdnValue.Value == "1")
            {
                Session["ReportXML"] = null;
                hdnValue.Value = "";
            }

            if (!IsPostBack)
            {
                TimeSpan sevenDays = new TimeSpan(7, 0, 0, 0);

                startDateedit.DisplayFormatString = format + " " + tformat;
                startDateedit.EditFormatString = format + " " + tformat;

                endDateedit.DisplayFormatString = format + " " + tformat;
                endDateedit.EditFormatString = format + " " + tformat;

                startDateedit.EditFormat = EditFormat.Custom;
                startDateedit.EditFormatString = format;
                startDateedit.Date = DateTime.Now.Subtract(sevenDays);
                endDateedit.Date = DateTime.Now;
                endDateedit.Date = new DateTime(endDateedit.Date.Year, endDateedit.Date.Month, endDateedit.Date.Day, 23, 00, 00);

                Session["ReportXML"] = null;

                BindData();
            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_Load" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            //errLabel.Text = ex.Message;
        }
    }

    #endregion

    #region BindData 

    private void BindData()
    {
        try
        {
            btnOk_Click(null, null);
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BindData: " + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            //errLabel.Text = "BindData: " + ex.StackTrace;
        }
    }

    #endregion


    protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        int newPageSize;
        if (e != null)
        {
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            grid.SettingsPager.PageSize = newPageSize;
        }
    }


    #region MainGrid_HtmlRowCreated

    protected void MainGrid_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

            GridViewDataColumn colR = (GridViewDataColumn)MainGrid.Columns["RequestID"];
            if (colR != null)
            {
                String rCellText = MainGrid.GetRowValues(e.VisibleIndex, "RequestID").ToString();
                e.Row.Attributes.Add("RequestID", rCellText);
            }

            String strStatus = "";
            for (Int32 c = 1; c < e.Row.Cells.Count; c++)
            {
                var cname = MainGrid.Columns[c].Caption;

                var colValue = MainGrid.GetRowValues(e.VisibleIndex, cname).ToString();

                if (cname.ToLower().Trim() == "status")
                    strStatus = colValue.ToLower();

                if (MainGrid.Columns[c].Visible == true)
                    e.Row.Cells[c].Text = ProcessLineBreaks(colValue);
            }

            GridViewCommandColumn colR1 = (GridViewCommandColumn)MainGrid.Columns["checkCol"];
            if (colR1 != null)
            {
                if (strStatus == "error" || strStatus == "cancel")
                    e.Row.Cells[colR1.Index].Enabled = true;
                else
                    e.Row.Cells[colR1.Index].Enabled = false;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            //ZD 100263
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = "MainGrid_HtmlRowCreated: " + ex.Message;
            log.Trace(ex.StackTrace + " MainGrid_HtmlRowCreated: " + ex.Message);
        }
    }

    #endregion

    protected void ASPxGridView1_CustomButtonInitialize(object sender,ASPxGridViewCustomButtonEventArgs e)
    {
        if (e.ButtonID == "btnCustom" && e.VisibleIndex % 2 != 0)
            e.Visible = DefaultBoolean.False;
    }


    #region btnOk_Click 
    protected void btnOk_Click(object sender, EventArgs e)
    {
        StringBuilder inXml = new StringBuilder();
        String outXML = "";
        try
        {   

            GenerateInXML(ref inXml);

            if ((Session["ReportXML"] == null) && inXml.ToString() != "")
            {
                outXML = obj.CallMyVRMServer("GetUsageReports", inXml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                Session["ReportXML"] = outXML;
            }
            else if (Session["ReportXML"] != null && Session["ReportXML"].ToString() != "")
                outXML = Session["ReportXML"].ToString();

            xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(outXML);
            ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(xmlDoc));
            MainGrid.Columns.Clear();            

            if (ds.Tables.Count > 0)
            {
                CreateTable();
                CreateGridColumns();

                try
                {
                    MainGrid.DataSource = rptTable;
                    MainGrid.DataBind();
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace + " : " + ex.Message);
                }
            }
            else
            {
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText("No Records");
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            //ZD 100263
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = ex.Message;
            log.Trace(ex.Message);
        }
    }

    #endregion

    #region GenerateInXML 

    private void GenerateInXML(ref StringBuilder inXml)
    {
        try
        {
            String fromDate = myVRMNet.NETFunctions.GetDefaultDate(startDateedit.Text);
            String toDate = myVRMNet.NETFunctions.GetDefaultDate(endDateedit.Text);
            String strSelect = "D";

            if (chkNew.Checked)
                strSelect += ",'N'";
            if (chkInProgress.Checked)
                strSelect += ",'I'";
            if (chkCompleted.Checked)
                strSelect += ",'C'";
            if (chkError.Checked)
                strSelect += ",'E'";
            if (chkCancel.Checked)  //FB 2363
                strSelect += ",'X'";

            if (strSelect.IndexOf(',') > 0)
                strSelect = strSelect.Replace("D,", "");
            else
                strSelect = "";

            inXml.Append("<GetUsageReports>");
            inXml.Append(obj.OrgXMLElement());
            inXml.Append("<DateFrom>" + fromDate + "</DateFrom>");
            inXml.Append("<DateTo>" + toDate + "</DateTo>");
            inXml.Append("<DateFormat>" + format + "</DateFormat>");
            inXml.Append("<UserID>" + usrID + "</UserID>");
            inXml.Append("<ReportType>ES</ReportType>");
            inXml.Append("<InputType>" + strSelect + "</InputType>");
            inXml.Append("</GetUsageReports>");

        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            //ZD 100263
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = ex.Message;
            log.Trace(ex.Message);
        }
    }

    #endregion

    #region CreateTable 

    private void CreateTable()
    {
        try
        {
            String colName = "";
            rptTable = new DataTable();
            participantTable = new DataTable();

            for (Int32 t1 = 0; t1 < ds.Tables.Count; t1++)
            {
                DataTable colTable = ds.Tables[t1];

                for (Int32 i = 0; i < colTable.Columns.Count; i++)
                {
                    colName = "";
                    colName = colTable.Columns[i].ToString().ToLower();

                    if (i == 1 || i == 6 || colName == "tzone") //FB 2363K (i== 4 -> i == 6)
                        continue;

                    if (i == 0)
                        rptTable.Columns.Add(obj.GetTranslatedText("Task Type <br/> Trans Id"), typeof(String));
                    else if (i == 5) //FB 2363K (i== 3 -> i == 5)
                        rptTable.Columns.Add(obj.GetTranslatedText("Issued By <br/> Issued On"), typeof(String));
                    else
                        rptTable.Columns.Add(obj.GetTranslatedText(colTable.Columns[i].ToString()), typeof(String));//FB 2695
                }
            }
            
            for (Int32 t = 0; t < ds.Tables.Count; t++)
            {
                DataTable detailsTable = ds.Tables[t];

                for (Int32 i = 0; i < detailsTable.Rows.Count; i++)
                {
                    DataRow dataRow = null;
                    dataRow = rptTable.NewRow();

                    if (detailsTable.Rows[0][1].ToString() == "")
                        break;

                    Int32 rCnt = 0;
                    rCnt = detailsTable.Columns.Count;

                    Int32 c = 0;
                    for (int j = 0; j < rCnt; j++)
                    {
                        colName = "";
                        colName = obj.GetTranslatedText(detailsTable.Columns[j].ColumnName.ToLower());//FB 2695

                        if (j == 1 || j == 6 || colName == "tzone")//FB 2363K (i== 4 -> i == 6)
                            continue;

                        if (j == 0)
                            dataRow[c] = detailsTable.Rows[i][colName].ToString() + "\\r" + detailsTable.Rows[i]["Trans Id"].ToString();
                        else if (j == 5) //FB 2363K (i== 3 -> i == 5)
                            dataRow[c] = detailsTable.Rows[i][colName].ToString() + "\\r"
                                + Convert.ToDateTime(detailsTable.Rows[i]["issued on"].ToString()).ToString("ddd MMM dd yyyy HH:mm:ss 'GMT'z") + "\\r" + " (" + detailsTable.Rows[i]["tzone"].ToString() + ")";
                        else if (colName == obj.GetTranslatedText("status date"))//FB 2695
                            dataRow[c] = Convert.ToDateTime(detailsTable.Rows[i][colName].ToString()).ToString("ddd MMM dd yyyy HH:mm:ss 'GMT'z") + "\\r" + "(" + detailsTable.Rows[i]["tzone"].ToString() + ")";
                        else if (colName == obj.GetTranslatedText("status"))
                            dataRow[c] = GetStatus(detailsTable.Rows[i]["status"].ToString());//FB 2695
                        else if (colName == obj.GetTranslatedText("Conference ID"))
                            dataRow[c] = detailsTable.Rows[i]["Conference ID"].ToString();//FB 2695
                        else
                            dataRow[c] = detailsTable.Rows[i][colName].ToString();
                           
                        c = c + 1;
                    }

                    rptTable.Rows.Add(dataRow);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateTable " + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region GetStatus

    private string GetStatus(string status)
    {
        String text = "";
        switch (status)
        {
            case "N":
                text = "New";
                break;
            case "I":
                text = "In Progress";
                break;
            case "C":
                text = "Completed";
                break;
            case "E":
                text = "Error";
                break;
            case "X": //FB 2363
                text = "Cancel";
                break;
        }

        return text;
    }

    #endregion

    #region ProcessLineBreaks

    private string ProcessLineBreaks(string text)
    {
        if (text.Contains("\\r"))
        {
            text = text.Replace("\\r", "<br/>");
        }

        if (text.Contains("\\n"))
        {
            text = text.Replace("\\n", "<br/>");
        }

        return text;
    }

    #endregion

    #region CreateGridColumns

    private void CreateGridColumns()
    {
        try
        {
            GridViewDataColumn dataColumn = new GridViewDataColumn();
            MainGrid.Columns.Clear();
            for (Int32 i = 0; i < rptTable.Columns.Count; i++)
            {
                String colName = "";
                colName = obj.GetTranslatedText(rptTable.Columns[i].ColumnName); //FB 2272

                if (i == 0)
                {
                    GridViewCommandColumn viewColumn = new GridViewCommandColumn();
                    viewColumn.ShowSelectCheckbox = true;
                    viewColumn.Name = "checkCol";                    
                    MainGrid.Columns.Add(viewColumn);
                }

                dataColumn = new GridViewDataColumn();
                dataColumn.Caption = colName;
                dataColumn.FieldName = colName;
                if (colName.ToLower() == "requestid")
                    dataColumn.Visible = false;
                //dataColumn.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
                MainGrid.Columns.Add(dataColumn);
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateGridColumns" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region Retry_Click
    protected void Retry_Click(object sender, EventArgs e)
    {
        StringBuilder inXml = new StringBuilder();
        try
        {
            if (hdnRequestID.Value != "")
            {
                inXml.Append("<TriggerEvent>");
                inXml.Append(obj.OrgXMLElement());
                inXml.Append("<UserID>" + usrID + "</UserID>");
                inXml.Append("<RequestID>" + hdnRequestID.Value + "</RequestID>");
                inXml.Append("<TriggerFrom>" + Session["userName"].ToString() + "</TriggerFrom>");
                inXml.Append("</TriggerEvent>");

                String outXML = obj.CallCommand("TriggerEvent", inXml.ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowSuccessMessage();
                    Session["ReportXML"] = null;
                }
            }
            btnOk_Click(null, null);
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            //ZD 100263
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = ex.Message;
            log.Trace(ex.Message);
        }
    }

    #endregion

    //FB 2363
    #region Cancel_Click
    /// <summary>
    /// Cancel_Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Cancel_Click(object sender, EventArgs e)
    {
        StringBuilder inXML = new StringBuilder();

        try
        {
            if (hdnRequestID.Value.Trim() != "")
            {
                inXML.Append("<UpdateCancelEvents>");
                //inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userName>" + Session["userName"] + "</userName>");
                inXML.Append("<RequestId>" + hdnRequestID.Value + "</RequestId>");
                inXML.Append("</UpdateCancelEvents>");

                String outXML = obj.CallMyVRMServer("UpdateCancelEvents", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.ToString().IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text = obj.ShowSuccessMessage();
                    errLabel.Visible = true;
                    Session["ReportXML"] = null;
                }
            }
            btnOk_Click(null, null);
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            //ZD 100263
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = ex.Message;
            log.Trace(ex.Message);
        }
    }
    #endregion
}