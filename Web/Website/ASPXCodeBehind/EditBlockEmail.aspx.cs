/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace ns_MyVRM
{
    public partial class EditBlockEmail : System.Web.UI.Page
    {
        #region protected Members
        protected System.Web.UI.WebControls.TextBox txtEmailSubject;
        protected System.Web.UI.WebControls.TextBox txtFrom;
        protected System.Web.UI.WebControls.TextBox txtTo;
        protected System.Web.UI.WebControls.Button cancel;
        protected System.Web.UI.WebControls.Button submit;
        protected System.Web.UI.WebControls.Label errLabel;
        protected DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor dxHTMLEditor;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailContID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailLangID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailLangName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCreateType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPlaceHolders;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailMode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnuserid;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnImg; //FB 1923
        
        protected ns_Logger.Logger log = null;
        protected myVRMNet.NETFunctions obj = null;

        ArrayList colNames = null;
        protected String uuid = "";
        Int32 orgId = 11; //FB 1923
        MyVRMNet.Util utilObj;//FB 2236

        #endregion

        #region EditBlockEmail 
        public EditBlockEmail()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util(); //FB 2236
        }
        #endregion

        #region Page_Load 
        protected void Page_Load(object sender, EventArgs e)
        {
            string confChkArg = string.Empty;
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());
                if (Request.QueryString["tp"] != null)
                    confChkArg = Request.QueryString["tp"].ToString();
                obj.AccessConformityCheck("editinventory.aspx?tp=" + confChkArg);
                // ZD 100263 Ends

                errLabel.Text = "";

                if (Request.QueryString["uuid"] != null)
                    if (Request.QueryString["uuid"].ToString() != "")
                        uuid = Request.QueryString["uuid"].ToString();

                if (!IsPostBack)
                {
                    if (Request.QueryString["tp"] != null)
                    {
                        if (Request.QueryString["tp"].ToLower().Equals("au"))
                        {
                            if (Session["UserToEdit"] != null)
                                hdnuserid.Value = Session["UserToEdit"].ToString();
                        }
                    }

                    BindData();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.Message;
            }

        }
        #endregion

        #region BindData 
        protected void BindData()
        {
            String message = "";
            try
            {
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<uuid>"+ uuid +"</uuid>";
                inXML += "</login>";

                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                String outXML = obj.CallMyVRMServer("GetUserEmails", inXML, Application["MyVRMServer_ConfigPath"].ToString());                
                if (outXML.IndexOf("<error>") < 0)
                {
                    message = GetStringInBetween("<Message>", "</Message>", outXML, false, false);
                    message = utilObj.ReplaceOutXMLSpecialCharacters(message, 1); //FB 2236
                    outXML = outXML.Replace(message, "");

                    //FB 1923 - Start
                    if (Session["organizationID"] != null)
                    {
                        if (Session["organizationID"].ToString() != "")
                            Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                    }

                    String maillogo = "Org_" + orgId.ToString() + "mail_logo.gif";
                    hdnImg.Value = HttpContext.Current.Request.ApplicationPath + "//image//Maillogo//" + maillogo;
                    message = message.Replace(maillogo, hdnImg.Value);
                    //FB 1923 - End

                    XmlDocument xmldoc = new XmlDocument();
                    outXML = outXML.Replace("&", "&amp;");
                    xmldoc.LoadXml(outXML);

                    CreateDtColumnNames();
                    XmlNodeList nodes = null;
                    DataTable dtable = obj.LoadDataTable(nodes, colNames);
                    nodes = xmldoc.SelectNodes("//Emails/Email");                    
                    foreach (XmlNode node in nodes)
                    {                           
                        txtFrom.Text  = node.SelectSingleNode("//Email/emailFrom").InnerText;
                        txtTo.Text = node.SelectSingleNode("//Email/emailTo").InnerText;
                        txtEmailSubject.Text = node.SelectSingleNode("//Email/Subject").InnerText;
                        dxHTMLEditor.Html = message;                     
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("EditBlockEmail: " + ex.Message);
            }
        }
        #endregion

        #region SetBlockEmail 
        /// <summary>
        /// SetBlockEmail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetBlockEmail(Object sender, EventArgs e)
        {
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<SetBlockEmail>");
                inXML.Append("<userid>" + Session["userID"].ToString() + "</userid>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<emailFrom>" + txtFrom.Text + "</emailFrom>");
                inXML.Append("<emailTo>" + txtTo.Text + "</emailTo>");
                inXML.Append("<Subject>" + txtEmailSubject.Text + "</Subject>");

                //FB 1923 - Start
                String emailMsg = "";
                String imgSrc = "";                
                if (Session["organizationID"] != null)
                {
                    if (Session["organizationID"].ToString() != "")
                        Int32.TryParse(Session["organizationID"].ToString(), out orgId);                
                }

                emailMsg = dxHTMLEditor.Html;
                int iIndexOfBegin = emailMsg.IndexOf("src=");
                if (iIndexOfBegin != -1)
                {
                    imgSrc = GetStringInBetween("src=", "/>", dxHTMLEditor.Html, false, false);                    
                    String maillogo = "Org_" + orgId.ToString() + "mail_logo.gif";
                    emailMsg = emailMsg.Replace(imgSrc, "\"" + maillogo + "\"");
                }
                //FB 1923 - End

                inXML.Append("<Message>" + emailMsg + "</Message>");
                inXML.Append("<uuid>" + uuid + "</uuid>");
                inXML.Append("</SetBlockEmail>");

                log.Trace("SetBlockEmail: inXML -" + inXML.ToString());
                String outXML = obj.CallMyVRMServer("SetBlockEmail", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("ViewBlockedMails.aspx?m=1&tp=" + Request.QueryString["tp"].ToString());
            }
            catch (Exception ex)
            {
                log.Trace("EditBlockEmail: : " + ex.Message);
            }
        }
        #endregion

        #region Create Column Names 
        /// <summary>
        /// CreateDtColumnNames
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("UUID");
            colNames.Add("From");
            colNames.Add("To");
            colNames.Add("Subject");
            colNames.Add("Message");
        }
        #endregion

        #region Redirect To TargetPage 
        /// <summary>
        /// RedirectToTargetPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RedirectToTargetPage(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ViewBlockedMails.aspx?tp=" + Request.QueryString["tp"].ToString());
            }
            catch (Exception ex)
            {
                log.Trace("EditBlockEmail:" + ex.Message);
            }
        }
        #endregion

        #region GetString Between

        public string GetStringInBetween(string strBegin, string strEnd, string strSource, bool includeBegin, bool includeEnd)
        {
            string result = "";
            try
            {
                int iIndexOfBegin = strSource.IndexOf(strBegin);
                if (iIndexOfBegin != -1)
                {
                    if (includeBegin)
                        iIndexOfBegin -= strBegin.Length;
                    strSource = strSource.Substring(iIndexOfBegin + strBegin.Length);
                    int iEnd = strSource.IndexOf(strEnd);
                    if (iEnd != -1)
                    {
                        if (includeEnd)
                            iEnd += strEnd.Length;
                        result = strSource.Substring(0, iEnd);

                    }
                }
                else
                    result = strSource;
            }
            catch (Exception ex)
            {
                result = "";
            }
            return result;
        }

        #endregion

    }
}