/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Globalization;//FB 1830
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_EditConferenceOrder
{
    public partial class EditConferenceOrder : System.Web.UI.Page
    {
        #region Protected Data Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.Label lblType;
        protected System.Web.UI.WebControls.Button btnAddNewAV;
        protected System.Web.UI.WebControls.Button btnCancel;
        protected System.Web.UI.WebControls.Button btnSendReminder;
        protected System.Web.UI.WebControls.Button btnCancelMain;
        protected System.Web.UI.WebControls.Button btnSubmitMain;
        protected System.Web.UI.WebControls.DataGrid itemsGrid;
        protected System.Web.UI.WebControls.DataGrid CATMainGrid;
        protected System.Web.UI.WebControls.Label lblInstructions;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.DataGrid AVMainGrid;
        protected System.Web.UI.WebControls.Label lblConfID;
        protected System.Web.UI.WebControls.Table AVItemsTable;
        protected System.Web.UI.WebControls.TableCell tblRoomLayout;
        protected System.Web.UI.WebControls.TextBox txtWorkOrderID;
        protected System.Web.UI.WebControls.TextBox txtWorkOrderName;
        protected System.Web.UI.WebControls.DropDownList lstRooms;
        protected System.Web.UI.WebControls.DropDownList lstAVSet;
        protected System.Web.UI.WebControls.DropDownList lstRoomLayout;
        protected System.Web.UI.WebControls.DropDownList lstStatus;
        protected System.Web.UI.WebControls.DropDownList lstTimezones;
        protected System.Web.UI.WebControls.DropDownList lstDeliveryType;
        protected System.Web.UI.WebControls.DropDownList lstServices;
        protected System.Web.UI.WebControls.CheckBox chkReminder;
        protected System.Web.UI.WebControls.CheckBox chkNotify;
        protected System.Web.UI.WebControls.TextBox txtComments;
        protected System.Web.UI.WebControls.TextBox txtCompletedBy;
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.TextBox txtApprover1;
        protected System.Web.UI.WebControls.TextBox txtStartByDate;
        protected System.Web.UI.WebControls.TextBox txtDeliveryCost;
        protected System.Web.UI.WebControls.TextBox txtServiceCharges;
        protected System.Web.UI.WebControls.Label lblConfName;
        protected System.Web.UI.WebControls.Label lblConfTime;
        protected System.Web.UI.WebControls.Label lblConfDate;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        protected System.Web.UI.WebControls.Label lblPublic;
        protected System.Web.UI.WebControls.Label lblRegistration;
        protected System.Web.UI.WebControls.Label lblFiles;
        protected System.Web.UI.WebControls.Label lblConfHost;
        protected System.Web.UI.WebControls.Label lblLocation;
        protected System.Web.UI.WebControls.Label lblConfTimezone;
        protected System.Web.UI.WebControls.Label lblNoWorkrders;
        protected System.Web.UI.WebControls.TextBox cmd;
        protected MetaBuilders.WebControls.ComboBox completedByTime;
        protected MetaBuilders.WebControls.ComboBox startByTime;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDelivery;
        protected System.Web.UI.HtmlControls.HtmlTableRow trService;
        protected System.Web.UI.HtmlControls.HtmlTable tblConferenceDetails;
        protected System.Web.UI.WebControls.Label lblDescription; // FB 1888


        protected System.Web.UI.WebControls.RegularExpressionValidator regTime; //WO bug
        protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDuration; //FB 2011
        protected System.Web.UI.HtmlControls.HtmlTableCell lblTZ; //FB 2588
        

        #endregion

        #region Private Data Members

        string selRoomID;
        string selAVSetID;
        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        ns_Logger.Logger log;
        MyVRMNet.Util utilObj; //FB 2236
        DataSet ds;

        myVRMNet.ImageUtil imageUtilObj; //Image Project
        byte[] imgArray = null;
        
        //Code added by Offshore for FB Issue 1073 -- Start
        protected String format = "";
        //Code added by Offshore for FB Issue 1073 -- End
        
        // WO Bug Fix
        protected String tformat = "hh:mm tt";
        
		//FB 1830
		protected String language = "";        
        protected string currencyFormat = "";
        CultureInfo cInfo = null;
        decimal tmpVal = 0;
        #endregion

        public EditConferenceOrder()
        {
            obj = new myVRMNet.NETFunctions();
            selRoomID = String.Empty;
            selAVSetID = String.Empty;
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger();
            ds = new DataSet();
            imageUtilObj = new myVRMNet.ImageUtil(); //Image Project
            utilObj = new MyVRMNet.Util(); //FB 2236
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = "";
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("editconferenceorder.aspx", Request.Url.AbsoluteUri.ToLower());
                // ZD 100263 Ends

                //FB 1830 - Start
                if (Session["CurrencyFormat"] == null)
                    Session["CurrencyFormat"] = ns_MyVRMNet.vrmCurrencyFormat.dollar;

                currencyFormat = Session["CurrencyFormat"].ToString();

                cInfo = new CultureInfo(Session["NumberFormat"].ToString());
				//FB 1830 - End

                errLabel.Text = "";
                errLabel.Visible = false;

                if (Session["timeFormat"] != null)   //WO BUg
                {
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        regTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        RegularExpressionValidator2.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        RegularExpressionValidator2.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        
                    }
                    else if (Session["timeFormat"].ToString().Equals("2")) //FB 2588
                    {
                        regTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        regTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                        RegularExpressionValidator2.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        RegularExpressionValidator2.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");

                    }
                }

                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                //Code added by Offshore for FB Issue 1073 -- End
                
                //WO Bug Fix
               //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 Ends
                
				//FB 1830
				if(Session["language"] == null)
					Session["language"] = "en";				
                
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();                

                if (!IsPostBack)
                {
                    //WO Bug Fix
                    startByTime.Items.Clear();
                    completedByTime.Items.Clear();
                    obj.BindTimeToListBox(startByTime, true, true);
                    obj.BindTimeToListBox(completedByTime, true, true);

                    //Response.Write("here");
                    Session["DS"] = null;
                    if (Request.QueryString["t"] != null)
                        txtType.Text = Request.QueryString["t"].ToString();
                    if (txtType.Text.Equals("1"))
                        lblType.Text = obj.GetTranslatedText("Audiovisual ");//FB 1830 - Translation FB 2570
                    if (txtType.Text.Equals("2"))
                        lblType.Text = obj.GetTranslatedText("Catering ");//FB 1830 - Translation
                    if (txtType.Text.Equals("3"))
                        lblType.Text = obj.GetTranslatedText("Facility ");//FB 1830 - Translation //FB 2570

                    if (!Request.QueryString["id"].ToString().ToUpper().Equals("PH") && !Request.QueryString["id"].ToString().ToUpper().Equals("11,1"))
                    {
                        DisplayConferenceDetails();
                        if (txtType.Text.Equals("2")) //in case of catering workorder
                        {
                            //Response.Write(txtType.Text);
                            String inXML = "<GetProviderWorkorderDetails>" + obj.OrgXMLElement() + "<ConfID>" + lblConfID.Text + "</ConfID><WorkorderID></WorkorderID><Type>2</Type></GetProviderWorkorderDetails>";//Organization Module Fixes
                            log.Trace(inXML);
                            String outXML = obj.CallMyVRMServer("GetProviderWorkorderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            if (outXML.IndexOf("<error>") >= 0)
                            {
                                errLabel.Text = obj.ShowErrorMessage(outXML);
                                errLabel.Visible = true;
                            }
                            else
                                GetProviderWorkorderDetails(outXML);
                            if (Request.QueryString["woid"] != null)
                                if (Request.QueryString["id"] != "11,1")
                                    foreach (DataGridItem dgi in CATMainGrid.Items)
                                    {
                                        if (Request.QueryString["woid"].ToString().Equals(dgi.Cells[0].Text))
                                        {
                                            EditWorkorder(new object(), new DataGridCommandEventArgs(dgi, CATMainGrid, new CommandEventArgs("Edit", new object())));
                                            break;
                                        }
                                    }
                        }
                        else
                        {
                            String inXML = objInXML.SearchConferenceWorkOrders("", Request.QueryString["id"].ToString(), "", "", "", "", "", "", "", txtType.Text, "", "1", "1","-1");//FB 1114
                            String outXML = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            outXML = outXML.Replace("&", "and"); //FB 2164
                            //Response.Write(obj.Transfer(inXML));
                            if (outXML.IndexOf("<error>") < 0)
                            {
                                BindData(outXML);
                            }
                            else
                            {
                                errLabel.Visible = true;
                                errLabel.Text = obj.ShowErrorMessage(outXML);
                            }
                            if (Request.QueryString["woid"] != null)
                                if (Request.QueryString["id"] != "11,1")
                                {
                                    if (!txtType.Text.Equals("2"))
                                        foreach (DataGridItem dgi in AVMainGrid.Items)
                                        {
                                            if (Request.QueryString["woid"].ToString().Equals(dgi.Cells[0].Text))
                                            {
                                                if (!txtType.Text.Equals("1"))
                                                {
                                                    trDelivery.Visible = false;
                                                    trService.Visible = false;
                                                }
                                                DataGridCommandEventArgs ea = new DataGridCommandEventArgs(dgi, AVMainGrid, new CommandEventArgs("Edit", new object())); // DataGridItemEventArgs(dgi);
                                                AVMainGrid_Edit(new object(), ea);
                                            }
                                        }
                                }
                        }                        
                        //else
                        //{
                        //    CreatePhantomConferenceWorkorder();
                        //}
                    }
                    else
                    {
                        tblConferenceDetails.Visible = false;
                        lblInstructions.Visible = false;
                        btnAddNewAV.Visible = false;
                        CreatePhantomConferenceWorkorder();
                    }
                    if (Request.QueryString["m"] != null)
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    }
                }
                if (txtType.Text.Equals("1"))
                    lblType.Text = obj.GetTranslatedText("Audiovisual");//FB 1830 - Translation FB 2570
                if (txtType.Text.Equals("2"))
                    lblType.Text = obj.GetTranslatedText("Catering");//FB 1830 - Translation
                if (txtType.Text.Equals("3"))
                    lblType.Text = obj.GetTranslatedText("Facility");//FB 1830 - Translation//FB 2570

                //                btnAddNewAV.Visible = true;

                if(Session["admin"].ToString() == "0")//ZD 100263
                    btnCancelMain.Visible = false;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("Page_Load" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "PageLoad: " + ex.StackTrace;
            }

        }

        protected void GetProviderWorkorderDetails(String outXML)
        {
            try
            {
                log.Trace("in GetProviderWorkorderDetails outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetProviderWorkorderDetails/Workorders/Workorder");
                if (nodes.Count > 0)
                {
                    XmlTextReader xtr;
                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    if (ds.Tables.Count > 0)
                    {
                        if (!ds.Tables[0].Columns.Contains("ServiceName")) ds.Tables[0].Columns.Add("ServiceName");
                        if (!ds.Tables[0].Columns.Contains("strMenus")) ds.Tables[0].Columns.Add("strMenus");

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (dr["SelectedService"].ToString().Equals("0"))
                                dr["SelectedService"] = "1";
                            dr["ServiceName"] = lstServices.Items.FindByValue(dr["SelectedService"].ToString()).Text;
                            //Code added by Offshore for FB Issue 1073 -- Start
                            dr["DeliverByDate"] = DateTime.Parse(dr["DeliverByDate"].ToString()).ToString(format);
                            //Code added by Offshore for FB Issue 1073 -- End
                            
                            //FB 1830
                            if (ds.Tables[0].Columns.Contains("Price"))
                            {                                
                                tmpVal = 0;
                                decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                                dr["Price"] = tmpVal.ToString("n", cInfo);
                                //dr["Price"] = (dr["Price"].ToString() == "") ? "0.00" : Convert.ToDecimal(dr["Price"].ToString()).ToString("0.00"); //FB 1686
                            }
                        }
                        CATMainGrid.DataSource = ds.Tables[0];
                        CATMainGrid.DataBind();
                        foreach (XmlNode node in nodes)
                        {
                            foreach (DataGridItem dgi in CATMainGrid.Items)
                            {
                                if (node.SelectSingleNode("ID").InnerText.Equals(dgi.Cells[0].Text))
                                {
                                    ds = new DataSet();
                                    DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                                    XmlNodeList subNodes = node.SelectNodes("MenuList/Menu");
                                    Label strMenus = (Label)dgi.FindControl("lblCateringMenus");
                                    strMenus.Text = "";
                                    foreach (XmlNode subNode in subNodes)
                                    {
                                        xtr = new XmlTextReader(subNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                                        strMenus.Text += subNode.SelectSingleNode("ID").InnerText + ":" + subNode.SelectSingleNode("Name").InnerText + ":" + subNode.SelectSingleNode("Quantity").InnerText + ";";
                                    }

                                    if (ds.Tables.Count > 0)
                                    {
                                        dgCateringMenus.DataSource = ds;
                                        dgCateringMenus.DataBind();
                                    }
                                }
                            }
                        }
                    }
                }
                btnCancel.Visible = false;
                btnSubmit.Visible = false;
            }
            catch (Exception ex)
            {
                log.Trace("GetProviderWorkorderDetails: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void CreatePhantomConferenceWorkorder()
        {
            try
            {
                if (txtType.Text.Equals("2"))
                {

                }
                else
                {
                    if (!txtType.Text.Equals("1"))
                    {
                        trDelivery.Visible = false;
                        trService.Visible = false;
                    }
                    lblConfID.Text = "11,1";
                    if (Request.QueryString["woid"] != null)
                        txtWorkOrderID.Text = Request.QueryString["woid"].ToString();
                    txtWorkOrderName.Text = obj.GetTranslatedText("No Conference-AV");//FB 1830 - Translation
                    AVItemsTable.Visible = true;
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //txtStartByDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    //txtCompletedBy.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    txtStartByDate.Text = DateTime.Now.ToString(format);
                    txtCompletedBy.Text = DateTime.Now.ToString(format);
                    //Code changed by Offshore for FB Issue 1073 -- End
                    //WO Bug Fix
                    //startByTime.Text = DateTime.Now.ToString("hh:mm tt");
                    //completedByTime.Text = DateTime.Now.Add(new TimeSpan(1, 0, 0)).ToString("hh:mm tt");

                    startByTime.Text = DateTime.Now.ToString(tformat);
                    completedByTime.Text = DateTime.Now.Add(new TimeSpan(1, 0, 0)).ToString(tformat);

                    lstRooms.Items.Add(new ListItem("Phantom Room", "11"));
                    lstRooms.SelectedIndex = 1;
                    lstRooms.Enabled = false;
                    GetRoomAVSets(lstRooms.SelectedValue);
                    tblRoomLayout.Visible = false;
                    btnSubmit.Text = obj.GetTranslatedText("Create");//FB 1830 - Translation
                    if (Request.QueryString["woid"] != null)
                        LoadWorkorderDetails();
                    else
                        if (lstAVSet.Items.Count > 1)
                        {
                            lstAVSet.ClearSelection();
                            lstAVSet.Items[1].Selected = true;
                            GetAVSetItems(lstAVSet.SelectedValue);
                        }
                    if (lstRooms.Items.Count.Equals(1) && lstAVSet.Items.Count <= 1)//FB 1951
                        btnSubmit.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CreatePhantomConferenceWorkorder" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Visible = true;
            }
        }

        protected void LoadWorkorderDetails()
        {
            try
            {
                String inXML = objInXML.GetWorkOrderDetails(txtWorkOrderID.Text, lblConfID.Text);
                String outXML = obj.CallMyVRMServer("GetWorkOrderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = "<WorkOrder><ID>10</ID><Name>No Conference-A/V</Name><Type>1</Type><Set><ID>1</ID><Name>Test Inventory 1</Name></Set><Room><ID>11</ID><Name>Phantom Room</Name><Layout></Layout></Room><AssignedTo><ID>12</ID></AssignedTo><StartByDate>12/10/2007</StartByDate><StartByTime>2:38 PM</StartByTime><CompletedByDate>12/10/2007</CompletedByDate><CompletedByTime>3:38 PM</CompletedByTime><Status>0</Status><Comments></Comments><Description></Description><ServiceCharge>1.99</ServiceCharge><DeliveryType>1</DeliveryType><DeliveryCost>1.99</DeliveryCost><Deleted>1.99</Deleted><Notify>0</Notify><Timezone>26</Timezone><Reminder>0</Reminder><ItemList><Item><ID>1</ID> <UID>28</UID> <QuantityRequested>1</QuantityRequested> <Name>Table</Name> <Price>99.99</Price><SerialNumber></SerialNumber> <Image>image/resource/table.jpg</Image><Portable>1</Portable><Comments>Round Table for 8</Comments><Description></Description><ServiceCharge>1.99</ServiceCharge><DeliveryType>1.99</DeliveryType><DeliveryCost>1.99</DeliveryCost><Quantity>10</Quantity> </Item><Item><ID>2</ID> <UID>29</UID> <QuantityRequested>1</QuantityRequested> <Name>Chair</Name> <Price>49.99</Price><SerialNumber></SerialNumber> <Image>image/resource/chair.jpg</Image><Portable>1</Portable><Comments></Comments><Description></Description><ServiceCharge>1.99</ServiceCharge><DeliveryType>1.99</DeliveryType><DeliveryCost>1.99</DeliveryCost><Quantity>80</Quantity> </Item><Item><ID>3</ID> <UID>30</UID> <QuantityRequested>1</QuantityRequested> <Name>Plasma TV</Name> <Price>2999.99</Price><SerialNumber></SerialNumber> <Image>image/resource/projector.jpg</Image><Portable>1</Portable><Comments></Comments><Description></Description><ServiceCharge>1.99</ServiceCharge><DeliveryType>1.99</DeliveryType><DeliveryCost>1.99</DeliveryCost><Quantity>2</Quantity> </Item></ItemList></WorkOrder>";
                if (outXML.IndexOf("<error>") <= 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    lstRooms.ClearSelection();
                    try
                    {
                        lstRooms.Items.FindByValue(xmldoc.SelectSingleNode("//WorkOrder/RoomID").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        errLabel.Text = obj.GetTranslatedText("Room No longer belongs to this conference");//FB 1830 - Translation
                        errLabel.Visible = true;
                        if (lstRooms.Items.Count.Equals(1))
                            btnSubmit.Enabled = false;
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    if (txtType.Text.Equals("3")) //FB 2011
                        GetRoomLayouts(lstRooms.SelectedValue);

                    GetRoomAVSets(lstRooms.SelectedValue);
                    lstAVSet.ClearSelection();
                    try
                    {
                        lstAVSet.Items.FindByValue(xmldoc.SelectSingleNode("//WorkOrder/SetID").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        errLabel.Text = obj.GetTranslatedText("Inventory Set has been changed and no longer belongs to the selected room.");//FB 1830 - Translation
                        errLabel.Visible = true;
                        if (lstAVSet.Items.Count.Equals(1))
                            btnSubmit.Enabled = false;
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //// case #259 ///////////////////////////////
                    if (xmldoc.SelectSingleNode("//WorkOrder") != null)
                    {
                        txtWorkOrderID.Text = xmldoc.SelectSingleNode("//WorkOrder/ID").InnerText;
                        txtWorkOrderName.Text = xmldoc.SelectSingleNode("//WorkOrder/Name").InnerText;
                        hdnApprover1.Text = xmldoc.SelectSingleNode("//WorkOrder/AssignedToID").InnerText;
                        txtApprover1.Text = xmldoc.SelectSingleNode("//WorkOrder/AssignedToName").InnerText;
                        //Code added by Offshore for FB Issue 1073 -- Start
                        //txtStartByDate.Text = xmldoc.SelectSingleNode("//WorkOrder/StartByDate").InnerText;
                        txtStartByDate.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//WorkOrder/StartByDate").InnerText);
                        //Code added by Offshore for FB Issue 1073 -- End
                        //Response.Write("1");
                        //startByTime.Text = DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/StartByTime").InnerText).ToString("hh:mm tt");
                        //completedByTime.Text = DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/CompletedByTime").InnerText).ToString("hh:mm tt");
                        //WO bug Fix
                        startByTime.Text = DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/StartByTime").InnerText).ToString(tformat);
                        completedByTime.Text = DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/CompletedByTime").InnerText).ToString(tformat);

                        //Code added by Offshore for FB Issue 1073 -- Start
                        //txtCompletedBy.Text = xmldoc.SelectSingleNode("//WorkOrder/CompletedByDate").InnerText;
                        txtCompletedBy.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//WorkOrder/CompletedByDate").InnerText);
                        //Code added by Offshore for FB Issue 1073 -- End
                        //txtDeliveryCost.Text = xmldoc.SelectSingleNode("//WorkOrder/DeliveryCost").InnerText;
                        txtDeliveryCost.Text = Decimal.Parse(xmldoc.SelectSingleNode("//WorkOrder/DeliveryCost").InnerText).ToString("0.00"); // FB 1686
                        //FB 1830
                        decimal.TryParse(txtDeliveryCost.Text.Trim(), out tmpVal);
                        txtDeliveryCost.Text = tmpVal.ToString("g", cInfo);
                        tmpVal = 0;
                        //txtServiceCharges.Text = xmldoc.SelectSingleNode("//WorkOrder/ServiceCharge").InnerText;
                        txtServiceCharges.Text = Decimal.Parse(xmldoc.SelectSingleNode("//WorkOrder/ServiceCharge").InnerText).ToString("0.00"); // FB 1686
                        //FB 1830
                        decimal.TryParse(txtServiceCharges.Text.Trim(), out tmpVal);
                        txtServiceCharges.Text = tmpVal.ToString("g", cInfo);
                        tmpVal = 0;

                        lstDeliveryType.ClearSelection();
                        lstDeliveryType.Items.FindByValue(xmldoc.SelectSingleNode("//WorkOrder/DeliveryType").InnerText).Selected = true;
                        txtComments.Text = xmldoc.SelectSingleNode("//WorkOrder/Comments").InnerText;
                        lstStatus.ClearSelection();
                        lstStatus.Items.FindByValue(xmldoc.SelectSingleNode("//WorkOrder/Status").InnerText).Selected = true;
                        if (xmldoc.SelectSingleNode("//WorkOrder/Notify").InnerText.Trim().Equals("1"))
                            chkNotify.Checked = true;
                        XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrder/ItemList/Item");
                        LoadDataGrid(nodes, txtType.Text);
                        lstTimezones.ClearSelection();
                        lstTimezones.Items.FindByValue(xmldoc.SelectSingleNode("//WorkOrder/Timezone").InnerText).Selected = true;

                        if (xmldoc.SelectSingleNode("//WorkOrder/RoomLayout").InnerText != "")
                            lstRoomLayout.SelectedIndex = lstRoomLayout.Items.IndexOf(lstRoomLayout.Items.FindByText(xmldoc.SelectSingleNode("//WorkOrder/RoomLayout").InnerText));//FB 2011
                    }
                    //// case #259 ///////////////////////////////
                    if (!txtType.Text.Equals("3"))
                        tblRoomLayout.Visible = false;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadWorkorderDetails" + ex.Message + " : " + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.Message + " : " + ex.StackTrace;
            }
        }
        protected void GetTimezones(Object sender, EventArgs e)
        {
            try
            {
                String selTZ = "";
                obj.GetTimezones(lstTimezones, ref selTZ);
                lstTimezones.SelectedValue = selTZ;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetTimezones" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
            }
        }
        private void LoadDataGrid(XmlNodeList nodes, string Type)
        {
            try
            {

                XmlTextReader xtr;
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                DataView dv = new DataView();
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    //if (!dt.Columns.Contains("Type")) dt.Columns.Add("Type");
                    //foreach (DataRow dr in dt.Rows)
                    //    dr["Type"] = txtType.Text;
                    ds.Tables[0].TableName = "Items"; //Item_Id
                    if (txtType.Text.Equals("11"))
                    {
                        ds.Tables[1].TableName = "Charges"; // Item_Id, Charges_Id
                        ds.Tables[2].TableName = "Charge"; // Charges_Id 
                    }
                    if (!dt.Columns.Contains("UID"))
                        dt.Columns.Add("UID");
                    if (!dt.Columns.Contains("QuantityRequested")) dt.Columns.Add("QuantityRequested");
                    if (!dt.Columns.Contains("DeliveryType")) dt.Columns.Add("DeliveryType");
                    if (!dt.Columns.Contains("DeliveryCost")) dt.Columns.Add("DeliveryCost");
                    if (!dt.Columns.Contains("ServiceCharge")) dt.Columns.Add("ServiceCharge");

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["QuantityRequested"].ToString().Equals(""))
                            dr["QuantityRequested"] = "1";
                        if (!dr["DeliveryType"].ToString().Equals("") || !dr["DeliveryType"].ToString().Equals("0") || !dr["DeliveryType"].ToString().Equals("-1"))
                            dr["DeliveryType"] = lstDeliveryType.Items.FindByValue(dr["DeliveryType"].ToString());
                        else
                            dr["DeliveryType"] = "-1";
                        
						//FB 1830 - Start
                        tmpVal = 0;
                        decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                        dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);

                        tmpVal = 0;
                        decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                        dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);

                        tmpVal = 0;
                        decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                        dr["Price"] = tmpVal.ToString("n", cInfo);

                        //FB 1686 - Start
                        //if (dr["ServiceCharge"].ToString().Equals(""))
                        //    dr["ServiceCharge"] = "0.00";
                        //else
                        //    dr["ServiceCharge"] = Decimal.Parse(dr["ServiceCharge"].ToString()).ToString("0.00");

                        //if (dr["DeliveryCost"].ToString().Equals(""))
                        //    dr["DeliveryCost"] = "0.00";
                        //else
                        //    dr["DeliveryCost"] = Decimal.Parse(dr["DeliveryCost"].ToString()).ToString("0.00");

                        //if (dr["Price"].ToString().Equals(""))
                        //    dr["Price"] = "0.00";
                        //else
                        //    dr["Price"] = Decimal.Parse(dr["Price"].ToString()).ToString("0.00");
                        // WO bug Fix - End
						//FB 1830 - End

                        if (dr["UID"].ToString().Trim().Equals(""))
                            dr["UID"] = "0";
                        //Response.Write("<br>" + dr["DeliveryType"].ToString());
                    }
                }
                //                Response.Write(dt.Rows.Count);
                lblInstructions.Text = obj.GetTranslatedText("Enter requested quantity for corresponding items.");//FB 1830 - Translation
                itemsGrid.Visible = true;
                btnSubmit.Enabled = true;
                itemsGrid.DataSource = ds;
                itemsGrid.DataBind();
                Session.Add("DS", ds);
                ChangeDeliveryType(lstDeliveryType, new EventArgs());
                if (!txtType.Text.Equals("1"))
                {
                    foreach (DataGridItem dgi in itemsGrid.Items)
                    {
                        RangeValidator rv = new RangeValidator();
                        rv = (RangeValidator)dgi.FindControl("validateQuantityRange");
                        rv.Enabled = false;
                    }
                    //}
                    //else
                    //{
                    itemsGrid.Columns[2].Visible = false;
                    itemsGrid.Columns[6].Visible = false;
                    itemsGrid.Columns[7].Visible = false;
                    itemsGrid.Columns[8].Visible = false;
                    itemsGrid.Columns[9].Visible = false;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadDataGrid" + ex.Message + " : " + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.Message + " : " + ex.StackTrace;
            }
        }

        private void LoadDataGridMain(XmlNodeList nodes, string Type)
        {
            try
            {
                //Response.Write("in LoadDataGridMain:Type= " + Type + nodes.Count);
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    //Response.Write(node.Name);
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                }


                if (dt.Columns.Contains("AssignedToName").Equals(false))
                {
                    dt.Columns.Add("AssignedToName");
                }
                if (dt.Columns.Contains("RoomLayout").Equals(false))
                {
                    dt.Columns.Add("RoomLayout");
                }
                foreach (DataRow dr in dt.Rows)
                {
                    //Response.Write(dt.Rows[i]["Status"].ToString());
                    if (dr["Status"].Equals("0"))
                        dr["Status"] = "Pending";
                    else
                        dr["Status"] = "Completed";
                    dr["DeliveryType"] = lstDeliveryType.Items.FindByValue(dr["DeliveryType"].ToString());
                    //Code added by Offshore for FB Issue 1073 -- Start
                    dr["StartByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["StartByDate"].ToString());
                    dr["CompletedByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["CompletedByDate"].ToString());
                    //Code added by Offshore for FB Issue 1073 -- End

                    if (dt.Columns.Contains("StartByTime")) //Code added fro WO bug
                        dr["StartByTime"] = DateTime.Parse(dr["StartByTime"].ToString()).ToString(tformat);

                    if (dt.Columns.Contains("CompletedByTime"))
                        dr["CompletedByTime"] = DateTime.Parse(dr["CompletedByTime"].ToString()).ToString(tformat);

                    //FB 1830 - Start
                    tmpVal = 0;
                    if (dt.Columns.Contains("ServiceCharge"))
                    {
                        decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                        dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);
                    }

                    tmpVal = 0;
                    if (dt.Columns.Contains("DeliveryCost"))
                    {
                        decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                        dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);
                    }

                    tmpVal = 0;
                    if (dt.Columns.Contains("Price"))
                    {
                        decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                        dr["Price"] = tmpVal.ToString("n", cInfo);
                    }

                    //if (dt.Columns.Contains("ServiceCharge"))
                    //    dr["ServiceCharge"] = Decimal.Parse(dr["ServiceCharge"].ToString()).ToString("0.00");

                    //if (dt.Columns.Contains("DeliveryCost"))
                    //    dr["DeliveryCost"] = Decimal.Parse(dr["DeliveryCost"].ToString()).ToString("0.00");

                    //if (dt.Columns.Contains("Price"))
                    //    dr["Price"] = (dr["Price"].ToString() == "") ? "0.00" : Convert.ToDecimal(dr["Price"].ToString()).ToString("0.00"); //FB 1686
					//FB 1830 - End                
				}
                Session["AVMainGridDS"] = dt;
                AVMainGrid.DataSource = dv;
                AVMainGrid.DataBind();

                if (dt.Rows.Count <= 0)
                    lblInstructions.Text = obj.GetTranslatedText("There are no work orders currently associated with this conference.") +"\r\n" + lblInstructions.Text;
            }

            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadDataGridMain" + ex.Message + " : " + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.Message + " : " + ex.StackTrace;
            }
        }
        protected void AVMainGrid_DeleteCommand(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                DeleteWorkOrder(e.Item.ItemIndex);
                AVMainGrid.Items[e.Item.ItemIndex].Cells.Clear();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "Error in Delete Item. Please contact your VRM Administrator.";
                log.Trace("AVMainGrid_DeleteCommand: Error in Delete Item." + ex.Message);//FB 1881
                errLabel.Text  = obj.ShowSystemMessage(); //FB 1881
            }
        }

        public void DeleteWorkOrder(int woPos)
        {
            //Response.Write(woPos);
            //Response.Write (AVMainGrid.Items[woPos].Cells[0].Text);
            //Response.End();
            try
            {
                String inXML = "";
                if (!txtType.Text.Equals("2"))
                {
                    inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><ConferenceID>" + lblConfID.Text + "</ConferenceID><WorkorderID>" + AVMainGrid.Items[woPos].Cells[0].Text + "</WorkorderID></login>";//Organization Module Fixes
                }
                else
                    inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><ConferenceID>" + lblConfID.Text + "</ConferenceID><WorkorderID>" + CATMainGrid.Items[woPos].Cells[0].Text + "</WorkorderID></login>";//Organization Module Fixes
                log.Trace("DeleteWorkorder inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("DeleteWorkOrder outXML: " + outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    Response.Redirect("EditConferenceOrder.aspx?id=" + lblConfID.Text + "&m=1&t=" + txtType.Text);
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "Error in DeleteWorkOrder. Please contact your VRM Administrator.";
                log.Trace("DeleteWorkOrder:" + ex.Message);//FB 1881
                errLabel.Text  = obj.ShowSystemMessage(); //FB 1881
            }
        }


        protected void AVMainGrid_Edit(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                lblType.Text = "Edit " + lblType.Text;
                Session.Add("EditColumn", e.Item.ItemIndex);
                DataTable myTable;
                if (Session["AVMainGridDS"] != null)
                {
                    //Response.Write("in if");
                    myTable = (DataTable)Session["AVMainGridDS"];
                }
                else
                {
                    myTable = new DataTable();
                    myTable.Columns.Add("ID");
                    myTable.Columns.Add("Name");
                    myTable.Columns.Add("AssignedToName");
                    myTable.Columns.Add("AssignedToID");
                    myTable.Columns.Add("StartByDate");
                    myTable.Columns.Add("StartByTime");
                    myTable.Columns.Add("CompletedByDate");
                    myTable.Columns.Add("CompletedByTime");
                    myTable.Columns.Add("RoomName");
                    myTable.Columns.Add("RoomID");
                    myTable.Columns.Add("Comments");
                    myTable.Columns.Add("Status");
                    myTable.Columns.Add("Notify");
                    myTable.Columns.Add("Reminder");
                    myTable.Columns.Add("RoomLayout");
                    myTable.Columns.Add("Timezone");
                    myTable.Columns.Add("DeliveryCost");
                    myTable.Columns.Add("ServiceCharge");
                    myTable.Columns.Add("TotalCost");
                    myTable.Columns.Add("Description");
                }
                DataRowCollection myRows = myTable.Rows;
                DataRow dr = myTable.Rows[e.Item.ItemIndex];
                btnAddNewAV.Enabled = false;
                AVItemsTable.Visible = true;
                AVMainGrid.Visible = false;
                itemsGrid.Visible = false;
                btnSubmit.Text = obj.GetTranslatedText("Edit");//FB 1830 - Translation
                txtWorkOrderID.Text = e.Item.Cells[0].Text;
                errLabel.Text = "";
                LoadWorkorderDetails();

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("AVMainGrid_Edit" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;
            }
        }

        protected void AVMainGrid_Update(object sender, DataGridCommandEventArgs e)
        {
            //Response.Write("here");
            btnAddNewAV.Enabled = false;
            AVItemsTable.Visible = true;
            btnSubmit.Text = obj.GetTranslatedText("Update");//FB 1830 - Translation
        }
        protected void AVMainGrid_Cancel(object sender, DataGridCommandEventArgs e)
        {
            //Response.Write("here");
            btnAddNewAV.Enabled = true;
            btnSubmit.Text = obj.GetTranslatedText("Cancel");//FB 1830 - Translation
        }
        private void DisplayConferenceDetails()
        {
            try
            {
                string inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<selectType>7</selectType><selectID>" + Request.QueryString["id"].ToString() + "</selectID></login>";//Organization Module Fixes
                //Response.Write(obj.Transfer(inXML));
                string outXML;
                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    //Response.Write(obj.Transfer(outXML));
                    xmldoc.LoadXml(outXML);
                    lblConfName.Text = xmldoc.SelectSingleNode("/conference/confInfo/confName").InnerText;
                    lblConfID.Text = xmldoc.SelectSingleNode("/conference/confInfo/confID").InnerText;
                    lblDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("/conference/confInfo/description").InnerText,2); // FB 1888, FB 2236
                    string recurring = xmldoc.SelectSingleNode("/conference/confInfo/recurring").InnerText;
                    if (recurring.Equals("1"))
                    {
                        string recurstr = xmldoc.SelectSingleNode("/conference/confInfo/appointmentTime").InnerXml;
                        recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrencePattern").InnerXml;
                        if (xmldoc.SelectNodes("/conference/confInfo/recurrenceRange").Count != 0)
                            recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange").InnerXml;
                        recurstr = "<recurstr>" + recurstr + "</recurstr>";
                        //Response.Write(obj.Transfer(recurstr));
                        string tzstr = "<TimeZone>" + xmldoc.SelectSingleNode("/conference/confInfo/timezones").InnerXml + "</TimeZone>";
                        string rst = obj.AssembleRecur(recurstr, tzstr);
                        string[] rst_array = rst.Split('|');
                        string recur = rst_array[0];
                        lblConfTime.Text = recur;
                        string SelectedTimeZoneName = rst_array[1];
                        if (xmldoc.SelectNodes("recurrenceRange").Count != 0)
                            //Code changed by Offshore for FB Issue 1073 -- Start
                            lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange/startDate").InnerText);
                        //lblConfDate.Text = xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange/startDate").InnerText;
                        //Code changed by Offshore for FB Issue 1073 -- End
                    }
                    else
                    {
                        //FB 1774 Start
                        DateTime startDate = DateTime.MinValue;                       
                        startDate = Convert.ToDateTime(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText);
                        int syear = startDate.Year; // Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[2]);
                        int smonth = startDate.Month; //Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[0]);
                        int sday = startDate.Day; //Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[1]);
                        //FB 1774 - End
                        int sHour = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startHour").InnerText);
                        int sMin = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startMin").InnerText);
                        string sSet = xmldoc.SelectSingleNode("/conference/confInfo/startSet").InnerText;
                        if ((sSet.ToUpper().Equals("PM")) && (sHour != 12))
                            sHour += 12;
                        DateTime sDate = new DateTime(syear, smonth, sday, sHour, sMin, 0);
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        //lblConfDate.Text = sDate.ToString("MM/dd/yyyy"); //.Split(' ')[0];
                        lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(sDate); //.Split(' ')[0];
                        //Code changed by Offshore for FB Issue 1073 -- End
                        lblConfTime.Text = sDate.ToString(tformat);//.Split(' ')[1].Split(':')[0] + ":" + sDate.ToString().Split(' ')[1].Split(':')[1]) + " " + sDate.ToString().Split(' ')[2]; // Convert.ToUInt16(sHour.ToString(), 10) + ":" + Convert.ToUInt16(sMin.ToString(), 10) + " " + sSet;//FB 1906
                        int dur = Convert.ToInt32(xmldoc.SelectSingleNode("/conference/confInfo/durationMin").InnerText);
                        lblConfDuration.Text = (dur / 60) + " hour(s) " + (dur % 60) + " min(s)";
                        hdnDuration.Value = dur.ToString(); //FB 2011
                    }
                    if (xmldoc.SelectSingleNode("/conference/confInfo/publicConf").InnerText.Equals("0"))
                        lblPublic.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                    else
                    {
                        lblPublic.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                        if (xmldoc.SelectSingleNode("/conference/confInfo/dynamicInvite").InnerText.Equals("1"))
                            lblRegistration.Text = obj.GetTranslatedText(" (Open for Registration)");//FB 1830 - Translation
                    }
                    string tzID = xmldoc.SelectSingleNode("/conference/confInfo/timeZone").InnerText;
                    string tzName = "";
                    XmlNodeList nodes = xmldoc.SelectNodes("/conference/confInfo/timezones/timezone");
                    int length = nodes.Count;
                    for (int i = 0; i < length; i++)
                        if (nodes[i].SelectSingleNode("timezoneID").InnerText.Equals(tzID))
                            tzName = nodes[i].SelectSingleNode("timezoneName").InnerText;
                    lblConfTimezone.Text = tzName;

                    if (Session["timezoneDisplay"].ToString() == "0") //FB 2588
                    {
                        lblConfTimezone.Text = "";
                        lblTZ.Visible = false;
                        lstTimezones.Visible = false;
                    }

                    nodes = xmldoc.SelectNodes("/conference/confInfo/fileUpload/file");
                    lblFiles.Text = "";
                    foreach (XmlNode node in nodes)
                        if (!node.InnerText.Equals(""))
                        {   //FB 1830
                            String fileName = getUploadFilePath(node.InnerText);
                            String fPath = node.InnerText;
                            int startIndex = fPath.IndexOf(@"\en\");
                            if ((language == "en" || fPath.IndexOf(@"\en\") > 0) && startIndex > 0)
                            {
                                fPath = fPath.Replace("\\", "/");
                                int len = fPath.Length - 1;                                
                                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830
                            }
                            else
                                fPath = "../Image/" + fileName;

                            lblFiles.Text += "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>, ";
                        }
                    //if (lblFiles.Text.Length > 0)
                    //    lblFiles.Text = lblFiles.Text.Substring(0, lblFiles.Text.Length - 2);
                    lblConfHost.Text = "<a href='mailto:" + xmldoc.SelectSingleNode("conference/confInfo/hostEmail").InnerText + "'>" + xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText + "</a>";
                    nodes = xmldoc.SelectNodes("/conference/confInfo/locationList/selected/level1ID");
                    string selRooms = ", ";
                    Int32[] selectedRooms = new Int32[nodes.Count]; // = { 0 }; // = new Int32();
                    int index = 0;
                    foreach (XmlNode selNode in nodes)
                    {
                        selRooms += selNode.InnerText + ", ";
                        selectedRooms[index] = new Int32();
                        selectedRooms[index] = Convert.ToInt32(selNode.InnerText);
                        index++;
                    }

                    lstRooms.Items.Clear();
                    lstRooms.Items.Add(new ListItem("Please select ...", "-1"));
                    lblLocation.Text = "";
                    nodes = xmldoc.SelectNodes("//conference/confInfo/locationList/level3List/level3");
                    foreach (XmlNode node in nodes)
                    {
                        XmlNodeList nodes2 = node.SelectNodes("level2List/level2");
                        foreach (XmlNode node2 in nodes2)
                        {
                            XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                            foreach (XmlNode node1 in nodes1)
                            {
                                //Response.Write("<br>before" + node1.SelectSingleNode("level1ID").InnerText);
                                if (IsRoomSelected(Convert.ToInt32(node1.SelectSingleNode("level1ID").InnerText), selectedRooms))
                                {
                                    lstRooms.Items.Add(new ListItem(node1.SelectSingleNode("level1Name").InnerText, node1.SelectSingleNode("level1ID").InnerText));
                                    //Response.Write("<br>after" + node1.SelectSingleNode("level1ID").InnerText + "<br>");
                                    lblLocation.Text += "<font class='blackblodtext'>" + node.SelectSingleNode("level3Name").InnerText + "</font>" + "> ";
                                    lblLocation.Text += "<font class='blackblodtext'>" + node2.SelectSingleNode("level2Name").InnerText + "</font>" + " > ";
                                    lblLocation.Text += "<a href='#' onclick='javascript:chkresource(\"" + node1.SelectSingleNode("level1ID").InnerText + "\")'>" + node1.SelectSingleNode("level1Name").InnerText + "</a><br>";
                                }
                            }
                        }
                    }
                    if (lblLocation.Text.Length.Equals(0))
                        lblLocation.Text = "<font class='blackblodtext'>" + obj.GetTranslatedText("No Locations") + "</font>";//FB 1830 - Translation
                    if (lstRooms.Items.Count.Equals(0))
                        lstRooms.Items.Add(new ListItem("No Rooms Selected", "0"));
                    // Response.Write(lstRooms.Items.Count);
                }
                else
                {
                    //Response.Write("here");
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Details" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "Details: " + ex.Message;
                //Response.Write(ex.Message);
            }
        }
        protected bool IsRoomSelected(Int32 level1ID, Int32[] roomID)
        {
            try
            {
                //Response.Write(roomID.Length);
                for (int i = 0; i < roomID.Length; i++)
                {
                    //Response.Write("<br>RoomID[" + i + "]: " + roomID[i]);
                    if (roomID[i].Equals(level1ID))
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("IsRoomSelected" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
                return false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            AVItemsTable.Visible = false;
            AVMainGrid.Visible = true;
            btnAddNewAV.Enabled = true;
            if (Request.QueryString["id"].ToString().ToLower().Equals("ph"))
                Response.Redirect("ConferenceOrders.aspx?t=" + txtType.Text);
        }

        public void A_btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtType.Text.Equals("2"))
                {
                    btnAddNewAV.Enabled = false;
                    btnSubmit.Enabled = false;
                    AVItemsTable.Visible = true;
                    itemsGrid.Visible = false;
                    btnSubmit.Text = obj.GetTranslatedText("Create");//FB 1830 - Translation
                    btnSubmit.Enabled = false;
                    lblNoWorkrders.Visible = false;
                    //btnSendReminder.Visible = false;
                    AVMainGrid.Visible = false;
                    lstAVSet.Items.Clear();
                    lstRooms.SelectedIndex = 0;
                    switch (txtType.Text)
                    {
                        case "1":
                            //txtWorkOrderName.Text = lblConfName.Text + "-A/V";
                            txtWorkOrderName.Text = lblConfName.Text + "-AV";  // WO Bug Fix
                            tblRoomLayout.Visible = false;
                            break;
                        case "2":
                            txtWorkOrderName.Text = lblConfName.Text + "-CAT";
                            tblRoomLayout.Visible = false;
                            break;
                        case "3":
                            txtWorkOrderName.Text = lblConfName.Text + "-H/K";
                            tblRoomLayout.Visible = true;
                            break;
                    }
                    txtWorkOrderID.Text = "new";
                    txtCompletedBy.Text = lblConfDate.Text;
                    //Response.Write(lblConfTime.Text);

                    // WO Bug Fix
                    //completedByTime.Text = DateTime.Parse(lblConfTime.Text).ToString("hh:mm tt"); //.Split(' ')[0] + " " + lblConfTime.Text.Split(' ')[1];
                    //startByTime.Text = DateTime.Parse(lblConfTime.Text).Subtract(new TimeSpan(1, 0, 0)).ToString("hh:mm tt");
                    
                    //FB 2011 - Start
                    //completedByTime.Text = DateTime.Parse(lblConfTime.Text).ToString(tformat); 
                    //startByTime.Text = DateTime.Parse(lblConfTime.Text).Subtract(new TimeSpan(1, 0, 0)).ToString(tformat);

                    startByTime.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(lblConfTime.Text)).ToString(tformat); //FB 2588
                    Int32 duration = 60;

                    if(hdnDuration.Value != "")
                        duration = Convert.ToInt32(hdnDuration.Value);

                    completedByTime.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(lblConfTime.Text)).Add(new TimeSpan((duration / 60), (duration % 60), 0)).ToString(tformat); //FB 2588
                    //FB 2011 - End

                    txtStartByDate.Text = lblConfDate.Text;
                    //lblInstructions.Text = "Please select a room first in order to create a work order";
                    if (lstRooms.Items.Count > 1)
                    {
                        lstRooms.ClearSelection();
                        lstRooms.Items[1].Selected = true;
                        GetRoomAVSets(lstRooms.SelectedValue);
                        if (lstAVSet.Items.Count > 1)
                        {
                            lstAVSet.ClearSelection();
                            lstAVSet.Items[1].Selected = true;
                            GetAVSetItems(lstAVSet.SelectedValue);
                        }
                    }

                    if (txtType.Text.Equals("3"))
                        GetRoomLayouts(lstRooms.SelectedValue);
                }
                else
                    AddBlankWorkorder();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("A_btnAddNew_Click" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace; // "Error in Adding New Work Order. Please contact your VRM Administrator.";
            }
        }

        private void BindData(String outXML)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                //Response.Write(x
                string confID = Request.QueryString["id"].ToString();//xmldoc.SelectSingleNode("/WorkOrderList/ConfID").InnerText;

                XmlNodeList nodes = xmldoc.SelectNodes("WorkOrderList/WorkOrder");
                //Response.Write(obj.Transfer(xmldoc.InnerXml));
                lblNoWorkrders.Visible = false;
                if (nodes.Count > 0)
                    LoadDataGridMain(nodes, txtType.Text);
                else
                    lblNoWorkrders.Visible = true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }



        protected void selRooms_SelectedIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                lstAVSet.Items.Clear();
                lstRoomLayout.Items.Clear();
                if (cmd.Text.Equals("2"))
                {
                    lstRooms.ClearSelection();
                    lstRooms.Items.FindByValue(selRoomID).Selected = true;
                    GetRoomAVSets(lstRooms.SelectedValue);
                }
                else
                {
                    if (lstRooms.SelectedIndex > 0)
                    {
                        if (txtType.Text.Equals("3"))
                            GetRoomLayouts(lstRooms.SelectedValue);
                        GetRoomAVSets(lstRooms.SelectedValue);
                        lblInstructions.Text = obj.GetTranslatedText("Select a Set from the Drop down list.");//FB 1830 - Translation
                    }
                    else
                        itemsGrid.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("selRoomIndexChanged" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "selRoomIndexChanged: " + ex.StackTrace;
            }
        }

        protected void GetRoomLayouts(string roomID)
        {
            try
            {
                string inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + roomID + "</roomID></login>";//Organization Module Fixes
                //string outxml = obj.CallCOM("GetOldRoom", inxml, Application["COM_ConfigPath"].ToString());
                string outxml = obj.CallMyVRMServer("GetOldRoom", inxml, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetOldRoom)
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outxml);
                string roomLayouts = xmldoc.SelectSingleNode("//room/roomImage").InnerText.Trim();
                //Response.Write(roomLayouts);
                lstRoomLayout.Items.Clear();
                lstRoomLayout.Enabled = true;
                ListItem li = new ListItem("Please select one...", "-1", true);
                lstRoomLayout.Items.Add(li);
                if (roomLayouts != "" && roomLayouts != "0") //FB 2011
                {
                    for (int i = 0; i < roomLayouts.Split(',').Length; i++)
                    {
                        li = new ListItem(roomLayouts.Split(',')[i].ToString().Trim(), i.ToString());
                        lstRoomLayout.Items.Add(li);
                    }
                }
                else
                {
                    lstRoomLayout.Items.Clear();
                    li = new ListItem("No room layouts defined", "-2", true);
                    lstRoomLayout.Enabled = false;
                    lstRoomLayout.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "Error in getting Room Layouts. Please contact your VRM Administrator.";
                log.Trace("GetRoomLayouts:" + ex.Message);//FB 1881
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
            }
        }

        protected void selAVSet_SelectedIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                if (cmd.Text.Equals("2"))
                {
                    lstAVSet.ClearSelection();
                    lstAVSet.Items.FindByValue(selAVSetID).Selected = true;
                    GetAVSetItems(lstAVSet.SelectedValue);
                }
                else
                {
                    if (lstAVSet.SelectedIndex > 0)
                        GetAVSetItems(lstAVSet.SelectedValue);
                    else
                        itemsGrid.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("selAVSetIndexChanged" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "selAVSetIndexChanged: " + ex.StackTrace;
            }
        }

        protected void GetRoomAVSets(string roomID)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                ListItem li = new ListItem();
                string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID><roomID>" + roomID + "</roomID><Type>" + txtType.Text + "</Type></login>";
                string outXML = obj.CallMyVRMServer("GetRoomSets", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                xmldoc.LoadXml(outXML);
                lstAVSet.Items.Clear();
                hdnApprover1.Text = "";
                txtApprover1.Text = "";
                XmlNodeList nodes = xmldoc.SelectNodes("/SetList/Set");
                int length = nodes.Count;
                //                Response.Write("Length " + length);
                if (length <= 0)
                    lstAVSet.Items.Add(new ListItem("No Sets", "-1"));
                else
                {
                    lstAVSet.Items.Add(new ListItem("Select one...", "-1"));
                    for (int i = 0; i < length; i++)
                    {
                        li = new ListItem(nodes[i].SelectSingleNode("Name").InnerText, nodes[i].SelectSingleNode("ID").InnerText);
                        lstAVSet.Items.Add(li);
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetRoomAVSets" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "GetRoomAVSets: " + ex.StackTrace;
            }
        }

        protected void GetAVSetItems(string groupID)
        {
            try
            {
                String inXML = objInXML.GetInventoryDetails(groupID, txtStartByDate.Text, txtCompletedBy.Text, startByTime.Text, completedByTime.Text, lstTimezones.SelectedValue, txtType.Text, "", ""); 
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("GetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList subnodes = xmldoc.SelectNodes("//Inventory/ItemList/Item");
                    //Response.End();
                    if (subnodes.Count > 0)
                        LoadDataGrid(subnodes, txtType.Text);
                    hdnApprover1.Text = xmldoc.SelectSingleNode("//Inventory/Admin/ID").InnerText;
                    txtApprover1.Text = obj.GetMyVRMUserName(hdnApprover1.Text);
                    btnSubmit.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "Error in getting Items. Please contact your VRM Administrator.";
                log.Trace("GetAVSetItems:" + ex.Message);//FB 1881
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
            }

        }

        protected bool SubmitWorkOrder()
        {
            try
            {
                if (!CheckQuantity())
                {
                    errLabel.Text = obj.GetTranslatedText("At least one item should have a requested quantity greater than 0.");//FB 1830 - Translation
                    errLabel.Visible = true;
                    return false;
                }
                if (!CheckDates())
                {
                    errLabel.Text = obj.GetTranslatedText("Please check Start and Completed Date/Time for this workorder.");//FB 1830 - Translation
                    errLabel.Visible = true;
                    return false;
                }
                //Response.Write("in if");
                //FB 2181 - Start
                //string inXML = "";
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<SetConferenceWorkOrders>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<user>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</user>");
                inXML.Append("<confInfo>");
                inXML.Append("<ConfID>" + lblConfID.Text + "</ConfID>");
                inXML.Append("<WorkOrderList>");
                inXML.Append("<WorkOrder>");
                inXML.Append("<Type>" + txtType.Text + "</Type>");
                inXML.Append("<ID>" + txtWorkOrderID.Text + "</ID>");
                inXML.Append("<Name>" + txtWorkOrderName.Text + "</Name>");
                inXML.Append("<RoomID>" + lstRooms.SelectedValue.ToString() + "</RoomID>");
                //Response.Write(lstRoomLayout.SelectedValue);
                if (lstRoomLayout.Items.Count > 0)
                {
                    if ((txtType.Text == "3") && (lstRoomLayout.Enabled) && (Convert.ToInt16(lstRoomLayout.SelectedValue) >= 0))
                        inXML.Append("<RoomLayout>" + lstRoomLayout.SelectedItem.ToString() + "</RoomLayout>");
                    else
                        inXML.Append("<RoomLayout></RoomLayout>");
                }
                else
                    inXML.Append("<RoomLayout></RoomLayout>");
                inXML.Append("<SetID>" + lstAVSet.SelectedValue.ToString() + "</SetID>");
                inXML.Append("<AdminID>" + hdnApprover1.Text + "</AdminID>");
                //Code changed by offshore for FB Issue 1073 -- start
                //inXML += "              <StartByDate>" + txtStartByDate.Text + "</StartByDate>";
                inXML.Append("<StartByDate>" + myVRMNet.NETFunctions.GetDefaultDate(txtStartByDate.Text) + "</StartByDate>");
                //Code changed by offshore for FB Issue 1073 -- end
                inXML.Append("<StartByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(startByTime.Text) + "</StartByTime>"); //FB 2588
                //Code changed by offshore for FB Issue 1073 -- start
                //inXML += "              <CompletedByDate>" + txtCompletedBy.Text + "</CompletedByDate>";
                inXML.Append("<CompletedByDate>" + myVRMNet.NETFunctions.GetDefaultDate(txtCompletedBy.Text) + "</CompletedByDate>");
                //Code changed by offshore for FB Issue 1073 -- end
                inXML.Append("<Timezone>" + lstTimezones.SelectedValue + "</Timezone>");
                inXML.Append("<CompletedByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(completedByTime.Text) + "</CompletedByTime>"); //FB 2588 
                //inXML += "              <CompletedBy>" + txtCompletedBy.Text + completedByTime.Text + "</CompletedBy>";
                inXML.Append("<Status>" + lstStatus.SelectedValue.ToString() + "</Status>");
                if (txtType.Text.Equals("1"))
                {
                    inXML.Append("<DeliveryType>" + lstDeliveryType.SelectedValue + "</DeliveryType>");
                    //FB 1830
                    String tempString = txtDeliveryCost.Text;
                    if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();
                    }

                    inXML.Append("<DeliveryCost>" + tempString + "</DeliveryCost>");

                    tempString = txtServiceCharges.Text;
                    if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();
                    }

                    inXML.Append("<ServiceCharge>" + tempString+ "</ServiceCharge>");
                }
                else
                {
                    inXML.Append("<DeliveryType>1</DeliveryType>");
                    inXML.Append("<DeliveryCost>0</DeliveryCost>");
                    inXML.Append("<ServiceCharge>0</ServiceCharge>");
                }
                inXML.Append("<Comments>" + txtComments.Text + "</Comments>");
                inXML.Append("<Description>" + lblDescription.Text + "</Description>"); // FB 1888
                UpdateTotal(new object(), new EventArgs());
                DataGridItem dgFooter = (DataGridItem)itemsGrid.Controls[0].Controls[itemsGrid.Controls[0].Controls.Count - 1];
                String txtTemp = ((Label)dgFooter.FindControl("lblTotalQuantity")).Text;
                //FB 1830
                if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)
                {
                    decimal.TryParse(txtTemp, NumberStyles.Any, cInfo, out tmpVal);
                    inXML.Append("<TotalCost>" + tmpVal.ToString() + "</TotalCost>");
                }
                else
                    inXML.Append("<TotalCost>" + txtTemp.Replace("$", "") + "</TotalCost>");

                if (chkNotify.Checked.Equals(true))
                    inXML.Append("<Notify>1</Notify>");
                else
                    inXML.Append("<Notify>0</Notify>");
                //if (chkReminder.Checked.Equals(true))
                //    inXML += "          <Reminder>1</Reminder>";
                //else
                inXML.Append("<Reminder>0</Reminder>");
                inXML.Append("<ItemList>");
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    TextBox tb = new TextBox();
                    tb = (TextBox)dgi.FindControl("txtReqQuantity");
                    if (tb.Text == "")
                        tb.Text = "0";
                    inXML.Append("<Item>");
                    //if (txtWorkOrderID.Text.Trim().ToLower().Equals("new"))
                    //    inXML += "              <UID>0</UID>";
                    //else
                    inXML.Append("<UID>" + dgi.Cells[dgi.Cells.Count - 1].Text + "</UID>");
                    inXML.Append("<ID>" + dgi.Cells[0].Text + "</ID>");
                    inXML.Append("<Quantity>" + tb.Text + "</Quantity>");
                    //FB 1830
                    if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(((Label)dgi.FindControl("lblServiceCharge")).Text, NumberStyles.Any, cInfo, out tmpVal);
                        inXML.Append("<ServiceCharge>" + tmpVal.ToString() + "</ServiceCharge>");
                    }
                    else
                        inXML.Append("<ServiceCharge>" + ((Label)dgi.FindControl("lblServiceCharge")).Text + "</ServiceCharge>");

                    inXML.Append("<DeliveryType>" + ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).SelectedValue + "</DeliveryType>");

                    //FB 1830
                    if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(((Label)dgi.FindControl("lblDeliveryCost")).Text, NumberStyles.Any, cInfo, out tmpVal);
                        inXML.Append("<DeliveryCost>" + tmpVal.ToString() + "</DeliveryCost>");
                    }
                    else
                        inXML.Append("<DeliveryCost>" + ((Label)dgi.FindControl("lblDeliveryCost")).Text + "</DeliveryCost>");

                    inXML.Append("</Item>");
                }
                inXML.Append("</ItemList>");
                inXML.Append("</WorkOrder>");
                inXML.Append("</WorkOrderList>");
                inXML.Append("</confInfo>");
                inXML.Append("</SetConferenceWorkOrders>");
                //Response.Write(obj.Transfer(inXML));
                string outXML = obj.CallMyVRMServer("SetConferenceWorkOrders", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write("<br>" + obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    return false;
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    Session["AVMainGridDS"] = null;
                    return true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SubmitWO" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "SubmitWO: " + ex.StackTrace;
                return false;
            }
        }

        protected void SetProviderWorkorderDetails()
        {
            try
            {
                //FB 2181 - Start
                StringBuilder inXML = new StringBuilder();
                inXML.Append(GenerateProviderWorkorderInXML());
                String outXML = obj.CallMyVRMServer("SetProviderWorkorderDetails", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    Response.Redirect("EditConferenceOrder.aspx?id=" + lblConfID.Text + "&m=1&t=" + txtType.Text);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        protected String GenerateProviderWorkorderInXML()
        {
            try
            {
                //FB 2181 - Start
                //String inXML = "";
                string DateTIme, Date, Time;
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<SetProviderWorkorderDetails>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<Type>2</Type>");
                //inXML.Append("<confInfo>"); //during FB 2304
                inXML.Append("<ConfID>" + lblConfID.Text + "</ConfID>");
                inXML.Append("<Workorders>");
                foreach (DataGridItem dgi in CATMainGrid.Items)
                {
                    DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                    Date = DateTIme.Split(' ')[0];
                    Time = DateTIme.Split(' ')[1];

                    inXML.Append("<Workorder>");
                    inXML.Append("<ID>" + dgi.Cells[0].Text + "</ID>");
                    inXML.Append("<Name>" + lblConfName.Text + "_CAT_" + (dgi.ItemIndex + 1) + "</Name>");
                    inXML.Append("<CateringService>" + dgi.Cells[3].Text + "</CateringService>");
                    inXML.Append("<RoomID>" + dgi.Cells[2].Text + "</RoomID>");
                    inXML.Append("<RoomName>" + ((Label)dgi.FindControl("lblRoomName")).Text + "</RoomName>");
                    //Code added for FB Issue 1073
                    //inXML += "      <DeliverByDate>" + DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("MM/dd/yyyy") + "</DeliverByDate>";
                    //inXML += "      <DeliverByTime>" + DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("hh:mm tt") + "</DeliverByTime>";
                    inXML.Append("<DeliverByDate>" + DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format) + "</DeliverByDate>"); //FB 2588
                    inXML.Append("<DeliverByTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(Time) + "</DeliverByTime>");
                    //Code added for FB Issue 1073

                    if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)//FB 1830
                    {
                        decimal.TryParse(((Label)dgi.FindControl("lblPrice")).Text, NumberStyles.Any, cInfo, out tmpVal);
                        inXML.Append("<Price>" + tmpVal.ToString() + "</Price>");
                    }
                    else
                        inXML.Append("<Price>" + ((Label)dgi.FindControl("lblPrice")).Text + "</Price>");

                    inXML.Append("<Comments>" + ((Label)dgi.FindControl("lblComments")).Text + "</Comments>");
                    inXML.Append("<MenuList>");
                    DataGrid dgMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                    foreach (DataGridItem dgiMenu in dgMenus.Items)
                    {
                        inXML.Append("<Menu>");
                        inXML.Append("<ID>" + dgiMenu.Cells[0].Text + "</ID>");
                        inXML.Append("<Quantity>" + dgiMenu.Cells[2].Text + "</Quantity>");
                        inXML.Append("</Menu>");
                    }
                    inXML.Append("</MenuList>");
                    inXML.Append("</Workorder>");
                }
                inXML.Append("</Workorders>");
                //inXML.Append("</confInfo>"); //during FB 2304
                inXML.Append("</SetProviderWorkorderDetails>");
                log.Trace("SetProviderWorkorders: " + inXML.ToString());
                return inXML.ToString();
                //FB 2181 - End
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtType.Text.Equals("2"))
                {
                    SetProviderWorkorderDetails();
                }
                else
                {
                    DataTable myTable = (DataTable)Session["AVMainGridDS"];
                    DataRowCollection myRows;
                    if (AVMainGrid.Items.Count > 0)
                    {
                        myTable = (DataTable)Session["AVMainGridDS"];
                    }
                    else
                    {
                        myTable = new DataTable();
                        myTable.Columns.Add("ID");
                        myTable.Columns.Add("Name");
                        myTable.Columns.Add("AssignedToName");
                        myTable.Columns.Add("AssignedToID");
                        myTable.Columns.Add("StartByDate");
                        myTable.Columns.Add("StartByTime");
                        myTable.Columns.Add("CompletedByDate");
                        myTable.Columns.Add("CompletedByTime");
                        myTable.Columns.Add("RoomName");
                        myTable.Columns.Add("RoomID");
                        myTable.Columns.Add("Comments");
                        myTable.Columns.Add("Status");
                        myTable.Columns.Add("Notify");
                        myTable.Columns.Add("Reminder");
                        myTable.Columns.Add("RoomLayout");
                        myTable.Columns.Add("Timezone");
                        myTable.Columns.Add("DeliveryCost");
                        myTable.Columns.Add("ServiceCharge");
                        myTable.Columns.Add("TotalCost");
                        myTable.Columns.Add("Description");
                        myTable.Columns.Add("DeliveryType");
                    }
                    myRows = myTable.Rows;
                    DataRow dr = myTable.NewRow();

                    dr["Name"] = txtWorkOrderName.Text;
                    dr["AssignedToName"] = txtApprover1.Text;
                    dr["AssignedToID"] = hdnApprover1.Text;
                    //Code changed by offshore for FB Issue 1073
                    //dr["StartByDate"] = txtStartByDate.Text;
                    //dr["CompletedByDate"] = txtCompletedBy.Text;
                    dr["StartByDate"] = myVRMNet.NETFunctions.GetDefaultDate(txtStartByDate.Text);
                    dr["CompletedByDate"] = myVRMNet.NETFunctions.GetDefaultDate(txtCompletedBy.Text);
                    //Code changed by offshore for FB Issue 1073 -- End
                    dr["StartByTime"] = myVRMNet.NETFunctions.ChangeTimeFormat(startByTime.Text); //FB 2588
                    dr["CompletedByTime"] = myVRMNet.NETFunctions.ChangeTimeFormat(completedByTime.Text);
                    dr["RoomName"] = lstRooms.SelectedItem;
                    dr["RoomID"] = lstRooms.SelectedValue;
                    dr["Comments"] = txtComments.Text;
                    dr["Status"] = lstStatus.SelectedItem;
                    dr["RoomLayout"] = lstRoomLayout.SelectedItem;
                    dr["Timezone"] = lstTimezones.SelectedValue;
                    //FB 1830
                    String tempString = txtDeliveryCost.Text;
                    if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();
                    }
                    dr["DeliveryCost"] = tempString;
                    tempString = txtServiceCharges.Text;
                    if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();
                    }
                    dr["ServiceCharge"] = tempString;
                    UpdateTotal(new object(), new EventArgs());
                    DataGridItem dgFooter = (DataGridItem)itemsGrid.Controls[0].Controls[itemsGrid.Controls[0].Controls.Count - 1];
                    Label lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
                    dr["TotalCost"] = lblTemp.Text;
                    //FB 1830
                    if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(dr["TotalCost"].ToString(), NumberStyles.Any, cInfo, out tmpVal);
                        dr["TotalCost"] = tmpVal.ToString();
                    }
                    if (chkNotify.Checked)
                        dr["Notify"] = "1";
                    else
                        dr["Notify"] = "0";
                    if (SubmitWorkOrder())
                    {
                        if (Request.QueryString["id"].ToString().ToUpper().Equals("PH") || Request.QueryString["id"].ToString().ToUpper().Equals("11,1"))
                            Response.Redirect("ConferenceOrders.aspx?t=" + txtType.Text + "&m=1");
                        else
                            Response.Redirect("EditConferenceOrder.aspx?id=" + lblConfID.Text + "&m=1&t=" + txtType.Text);
                    }
                }
            }
            catch (System.Threading.ThreadAbortException) { } // WO BUg Fix
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("btnSubmit_Click" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;
            }
        }
        protected void btnSubmitMain_Click(object sender, EventArgs e)
        {
            //WO Bug Fix
            try
            {
               // if (!txtType.Text.Equals("2"))
                    if (SubmitWorkOrder())
                    {
                        if (Request.QueryString["id"].ToString().ToUpper().Equals("PH") || Request.QueryString["id"].ToString().ToUpper().Equals("11,1"))
                            Response.Redirect("ConferenceOrders.aspx?t=" + txtType.Text + "&m=1");
                        else
                            Response.Redirect("EditConferenceOrder.aspx?id=" + lblConfID.Text + "&m=1&t=" + txtType.Text);
                    }
                    else
                        SetProviderWorkorderDetails();
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("btnSubmitMain_Click" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;
            }
        }
        //ZD 100170 - Start 
        protected void btnCancelMain_Click(object sender, EventArgs e)
        {            
            try
            {
                Response.Redirect("ConferenceOrders.aspx?t=" + txtType.Text);
             
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("btnCancelMain_Click" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;
            }
        }
        //ZD 100170 - End 

        protected bool CheckQuantity()
        {
            try
            {
                if (!txtType.Text.Equals("3"))
                {
                    TextBox tb;
                    foreach (DataGridItem dgi in itemsGrid.Items)
                    {
                        tb = new TextBox();
                        //Response.Write("<br>Index: " + dgi.ItemIndex);
                        tb = (TextBox)dgi.FindControl("txtReqQuantity");
                        //Response.Write(" Text: " + tb.Text);
                        //if (Convert.ToInt32(tb.Text) > 0) //during FB 2304
                          if (Convert.ToInt32(tb.Text) >= 0) 
                            return true;
                    }
                    return false;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("In CheckQuantity:" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "In CheckQuantity: " + ex.StackTrace;
                errLabel.Visible = true;
                return false;
            }
        }
        protected bool CheckDates()
        {
            try
            {   //Code changed by offshore for FB Issue 1073 -- Start
                //DateTime dtStart = DateTime.Parse(txtStartByDate.Text + " " + startByTime.Text);
                //DateTime dtEnd = DateTime.Parse(txtCompletedBy.Text + " " + completedByTime.Text
                DateTime dtStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtStartByDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(startByTime.Text)); //FB 2588
                DateTime dtEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtCompletedBy.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(completedByTime.Text)); //FB 2588
                //Code changed by offShore for FB Issue 1073 -- End
                if (dtEnd <= dtStart)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CheckDates" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
                return false;
            }
        }
        protected void SendReminderToHost(Object sender, EventArgs e)
        {
            try
            {
                string inxml = "<login>";
                inxml += "<userID>" + Session["userID"].ToString() + "</userID>";
                inxml += "<WorkOrderID>" + txtWorkOrderID.Text + "</WorkOrderID>";
                inxml += "</login>";
                //Response.Write(obj.Transfer(inxml));
                string outxml = obj.CallMyVRMServer("SendWorkOrderReminder", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outxml.IndexOf("<error>") < 0)
                    errLabel.Text = obj.GetTranslatedText("Operation Successful! Reminder email has been sent to the person-in-charge of the work order.");//FB 1830 - Translation
                else
                    errLabel.Text = obj.ShowErrorMessage(outxml);
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SendReminderToHost" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void UpdateTotal(Object sender, EventArgs e)
        {
            try
            {
                Double totalCost = 0;
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    TextBox txtTemp = new TextBox();
                    txtTemp = (TextBox)dgi.FindControl("txtReqQuantity");
                    int reqQuantity = Convert.ToInt32(txtTemp.Text);
                    //Response.Write("<br>Price: " + ((Label)dgi.FindControl("lblPrice")).Text);
                    //Response.Write("<br>Service Charge: " + ((Label)dgi.FindControl("lblServiceCharge")).Text);
                    //Response.Write("<br>Delivery Cost: " + ((Label)dgi.FindControl("lblDelivery Cost")).Text);
                    //Response.Write("<br>Price: " + ((Label)dgi.FindControl("lblPrice")).Text);
                    //FB 1830
                    if (Session["CurrencyFormat"].ToString() == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        tmpVal = 0;
                        decimal.TryParse(((Label)dgi.FindControl("lblPrice")).Text, NumberStyles.Any, cInfo, out tmpVal);
                        String strPrice = tmpVal.ToString();
                        decimal.TryParse(((Label)dgi.FindControl("lblServiceCharge")).Text, NumberStyles.Any, cInfo, out tmpVal);
                        String strServiceCharge = tmpVal.ToString();
                        decimal.TryParse(((Label)dgi.FindControl("lblDeliveryCost")).Text, NumberStyles.Any, cInfo, out tmpVal);
                        String strDeliveryCost = tmpVal.ToString();
                       
                        totalCost += (Convert.ToDouble(strPrice) + Convert.ToDouble(strServiceCharge) + Convert.ToDouble(strDeliveryCost)) * reqQuantity;
                    }
                    else
                        totalCost += (Convert.ToDouble(((Label)dgi.FindControl("lblPrice")).Text) + Convert.ToDouble(((Label)dgi.FindControl("lblServiceCharge")).Text) + Convert.ToDouble(((Label)dgi.FindControl("lblDeliveryCost")).Text)) * reqQuantity;
                }
                //FB 1830
                if (txtServiceCharges.Text.Trim().Equals(""))
                    txtServiceCharges.Text = (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)? "0,00": "0.00";  // FB 1686 
                    
                if (txtDeliveryCost.Text.Trim().Equals(""))
                    txtDeliveryCost.Text = (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound) ? "0,00" : "0.00"; // FB 1686

                String tempString = txtServiceCharges.Text;
                if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                {
                    decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                    tempString = tmpVal.ToString();
                }
                totalCost += Convert.ToDouble(tempString);

                tempString = txtDeliveryCost.Text;
                if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                {
                    decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                    tempString = tmpVal.ToString();
                }
                totalCost += Convert.ToDouble(tempString);

                Label lblTemp = new Label();
                DataGridItem dgFooter = (DataGridItem)itemsGrid.Controls[0].Controls[itemsGrid.Controls[0].Controls.Count - 1];
                lblTemp = (Label)dgFooter.FindControl("lblTotalQuantity");
                //lblTemp.Text = Double.Parse(totalCost.ToString()).ToString("00.00");
                lblTemp.Text = Double.Parse(totalCost.ToString()).ToString("0.00"); //FB 1686
                
				//FB 1830 
				tmpVal = 0;
                decimal.TryParse(lblTemp.Text, out tmpVal);
                lblTemp.Text = tmpVal.ToString("n", cInfo);

                lblInstructions.Text = "";
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("UpdateTotal" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void LoadDeliveryTypes(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                obj.GetDeliveryTypes(lstTemp);
                if (lstTemp.ID.IndexOf("Item") >= 0)
                    lstTemp.Items.RemoveAt(lstTemp.Items.Count - 2);
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadDeliveryTypes" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                log.Trace(e.Item.ItemType.ToString());
                if (!e.Item.ItemType.Equals(ListItemType.EditItem) && (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem)))
                {
                    log.Trace("in if");
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Workorder?") + "')) {DataLoading(1); } else { return false; }");//FB 2011, FB japnese
                    btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                    btnTemp.Attributes.Add("onclick", "DataLoading(1);");
                }
                if (e.Item.ItemType.Equals(ListItemType.EditItem) && !txtType.Text.Equals("2"))
                {
                    DropDownList lstTempRooms = (DropDownList)e.Item.FindControl("lstRooms");
                }
            }
            catch (Exception ex)
            {
                log.Trace("\r\n" + DateTime.Now + " : " + ex.StackTrace + " || " + ex.Message);
            }
        }
        protected void SetCalendar(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.EditItem))
                {
                    System.Web.UI.WebControls.Image imgCalendar = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgCalendar");
                    //Response.Write(imgCalendar.ClientID);
                    //Code Changed By Offshore For FB Issue 1073 -- Start
                    // imgCalendar.Attributes.Add("onclick", "javascript:showCalendar('" + ((TextBox)e.Item.FindControl("txtDeliverByDate")).ClientID + "', '" + ((Image)e.Item.FindControl("imgCalendar")).ClientID + "', 1, '%m/%d/%Y');");
                    imgCalendar.Attributes.Add("onclick", "javascript:showCalendar('" + ((TextBox)e.Item.FindControl("txtDeliverByDate")).ClientID + "', '" + ((System.Web.UI.WebControls.Image)e.Item.FindControl("imgCalendar")).ClientID + "', 1, '" + format + "');");
                    //Code changed by Offshore for FB Issue 1073 -- End
                    //imgCalendar.Attributes.Add("onclick", "javascript:alert('" + ((TextBox)e.Item.FindControl("txtDeliverByDate")).ClientID + "');");
                    //imgCalendar.Attributes.Add("onclick", "javascript:alert('" + ((TextBox)e.Item.FindControl("txtDeliverByDate")).ClientID + "');");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }

        protected void ChangeDeliveryType(Object sender, EventArgs e)
        {
            try
            {
                ds = (DataSet)Session["DS"];
                if (!lstDeliveryType.SelectedValue.Equals("-1"))
                {
                    foreach (DataGridItem dgi in itemsGrid.Items)
                    {
                        if (lstDeliveryType.SelectedValue.Equals("0"))
                        {
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Enabled = true;
                        }
                        else
                        {
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Items.FindByValue(lstDeliveryType.SelectedValue).Selected = true;
                            ChangeDeliveryTypeItem((DropDownList)dgi.FindControl("lstDeliveryTypeItem"), e);
                            ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).Enabled = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void ChangeDeliveryTypeItem(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    //Response.Write(lstTemp.ClientID + " : " + ((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClientID);
                    if (lstTemp.ClientID.Equals(((DropDownList)dgi.FindControl("lstDeliveryTypeItem")).ClientID))
                    {
                        //Response.Write("in ChangeDeliveryTypeItem");
                        DropDownList lstTempItem = (DropDownList)dgi.FindControl("lstDeliveryCost");
                        lstTempItem.ClearSelection();
                        lstTempItem.Items.FindByValue(lstTemp.SelectedValue).Selected = true;
                        Label lblTemp = (Label)dgi.FindControl("lblDeliveryCost");
                        lblTemp.Text = lstTempItem.SelectedItem.Text;
                        //FB 1830
                        tmpVal = 0;
                        decimal.TryParse(lblTemp.Text, out tmpVal);
                        lblTemp.Text = tmpVal.ToString("n", cInfo);

                        lstTempItem = (DropDownList)dgi.FindControl("lstServiceCharge");
                        lstTempItem.ClearSelection();
                        lstTempItem.Items.FindByValue(lstTemp.SelectedValue).Selected = true;
                        lblTemp = (Label)dgi.FindControl("lblServiceCharge");
                        lblTemp.Text = lstTempItem.SelectedItem.Text;
                        //FB 1830
                        tmpVal = 0;
                        decimal.TryParse(lblTemp.Text, out tmpVal);
                        lblTemp.Text = tmpVal.ToString("n", cInfo);

                        UpdateTotal(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void SetDeliveryAttributes(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                //Image Project
                if ((e.Item.ItemType.Equals(ListItemType.Item)) || (e.Item.ItemType.Equals(ListItemType.AlternatingItem)))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    string imCont = "";
                    string finame = "";
                    string fullPath = "";

                    if (row["ImageName"] != null)
                        finame = row["ImageName"].ToString().Trim();

                    if (row["Image"] != null)
                        imCont = row["Image"].ToString().Trim();

                    string pathName = "resource";

                    if (txtType.Text.Trim().Equals("2"))
                        pathName = "food";

                    if (txtType.Text.Trim().Equals("3"))
                        pathName = "Housekeeping";

                    fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/image/" + pathName + "/" + finame;//FB 1830

                    if (imCont != "")
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imCont);

                        if (File.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame))
                            File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame);

                        WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame, ref imgArray);

                        Image imgContrl = (Image)e.Item.FindControl("imgItem");
                        imgContrl.ImageUrl = fullPath;

                        //myVRMWebControls.ImageControl imgCtrl = (myVRMWebControls.ImageControl)e.Item.FindControl("imgItem");

                        //MemoryStream ms2 = new MemoryStream(imgArray, 0, imgArray.Length);
                        //ms2.Write(imgArray, 0, imgArray.Length);

                        //if (fileExtn.ToLower() == "gif")
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        //else if ((fileExtn.ToLower() == "jpg") || (fileExtn.ToLower() == "jpeg"))
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        //imgCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        //imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms2);
                        ////imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms2);

                        //imCont = null;
                        //imgCtrl.Visible = true;
                    }
                }

                if ((txtType.Text.Equals("1")) && ((e.Item.ItemType.Equals(ListItemType.Item)) || (e.Item.ItemType.Equals(ListItemType.AlternatingItem))))
                {
                    String ItemID = e.Item.Cells[0].Text;
                    //Response.Write("<br>ItemID: " + ItemID);
                    DataView charges = new DataView();
                    DataView charge = new DataView();
                    charges = ds.Tables["Charges"].DefaultView;
                    charge = ds.Tables["Charge"].DefaultView;
                    charges.RowFilter = "Item_Id='" + Int32.Parse(ItemID) + "'";
                    charge.RowFilter = "Charges_Id='" + e.Item.ItemIndex + "' AND NOT(DeliveryTypeID ='0')";
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).Items.Clear();
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).DataSource = charge;
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).DataBind();
                    ((DropDownList)e.Item.FindControl("lstDeliveryCost")).Items.Insert(0, (new ListItem("0.0", "-1")));
                    ((DropDownList)e.Item.FindControl("lstServiceCharge")).DataSource = charge;
                    ((DropDownList)e.Item.FindControl("lstServiceCharge")).DataBind();
                    ((DropDownList)e.Item.FindControl("lstServiceCharge")).Items.Insert(0, (new ListItem("0.0", "-1")));
                }

                
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("WriteToFile" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }
        
        protected void DeleteWorkorder(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string DateTIme, Date, Time;
                log.Trace("AVMainGrid.EditItemIndex Delete: " + AVMainGrid.EditItemIndex);
                if (CATMainGrid.EditItemIndex.Equals(-1))
                {
                    if (!e.Item.Cells[0].Text.Equals("") && !e.Item.Cells[0].Text.Equals("new"))   // WO Bug Fixes
                        DeleteWorkOrder(e.Item.ItemIndex);
                    else
                    {
                        btnAddNewAV.Visible = true;
                        DataTable dt = CreateWorkorderTable();
                        foreach (DataGridItem dgi in CATMainGrid.Items)
                        {
                            if (!dgi.ItemIndex.Equals(e.Item.ItemIndex))
                            {
                                DataRow dr = dt.NewRow();

                                DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                                Date = DateTIme.Split(' ')[0];
                                Time = DateTIme.Split(' ')[1];

                                dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                                dr["Name"] = dgi.Cells[1].Text;
                                dr["RoomId"] = dgi.Cells[2].Text;
                                dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                                dr["SelectedService"] = dgi.Cells[3].Text;
                                dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;
                                dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;
                                dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                                dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                                //Code changed by Offshore for FB Issue 1073 -- Start
                                //dr["DeliverByDate"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("MM/dd/yyyy");
                                dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format); //FB 2588
                                dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(tformat);
                                //Code changed by Offshore for FB Issue 1073 -- End
                                //dr["DeliverByTime"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("hh:mm tt");
                                dt.Rows.Add(dr);
                            }
                        }
                        CATMainGrid.DataSource = dt;
                        CATMainGrid.DataBind();
                        foreach (DataGridItem dgi in CATMainGrid.Items)
                        {
                            String strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                            DataTable dtMenus = new DataTable();
                            if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                            if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                            if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                            for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                            {
                                DataRow drMenu = dtMenus.NewRow();
                                drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                                drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                                drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                                dtMenus.Rows.Add(drMenu);
                            }
                            DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                            dgCateringMenus.DataSource = dtMenus;
                            dgCateringMenus.DataBind();
                        }
                        if (dt.Rows.Count.Equals(0))
                        {
                            CATMainGrid.Visible = false;
                            btnAddNewAV.Visible = true;
                            lblInstructions.Visible = true;
                            lblInstructions.Text = obj.GetTranslatedText("To create a new work order, please click on the button below.");//FB 1830 - Translation
                        }
                    }
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Please save/update or cancel current workorder prior to editing or deleting existing workorders.");//FB 1830 - Translation
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        protected void LoadCateringServices(Object sender, EventArgs e)
        {
            DropDownList lstServices = (DropDownList)sender;
            if (lstServices.Items.Count.Equals(0))
                obj.GetCateringServices(lstServices);
        }

        protected void LoadRooms(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstSender = (DropDownList)sender;

                //WO Bug Fixing 
                //lstSender.Items.Clear();
                if (lstSender.Items.Count == 0)
                {
                    foreach (ListItem li in lstRooms.Items)
                        if (!li.Value.Equals("-1"))
                            lstSender.Items.Add(new ListItem(li.Text, li.Value));
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void EditWorkorder(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string DateTIme, Date, Time; //FB 2588
                log.Trace("AVMainGrid.EditItemIndex Edit: " + AVMainGrid.EditItemIndex);
                if (CATMainGrid.EditItemIndex.Equals(-1))
                {
                    btnAddNewAV.Visible = false;
                    btnCancelMain.Visible = false;
                    btnSubmitMain.Visible = false;
                    DataTable dt = CreateWorkorderTable();
                    log.Trace("in EditWorkorder" + e.Item.ItemIndex);
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        DataRow dr = dt.NewRow();

                        DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                        Date = DateTIme.Split(' ')[0];
                        Time = DateTIme.Split(' ')[1];

                        dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                        dr["Name"] = dgi.Cells[1].Text;
                        dr["RoomId"] = dgi.Cells[2].Text;
                        //Response.Write(dr["RoomId"].ToString());
                        dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                        dr["SelectedService"] = dgi.Cells[3].Text;
                        dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                        //Response.Write(dgi.Cells[3].Text);
                        dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;
                        dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;
                        dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        //dr["DeliverByDate"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("MM/dd/yyyy");
                        //dr["DeliverByTime"] = DateTime.Parse(((Label)(dgi.FindControl("lblDeliverByDateTime"))).Text).ToString("hh:mm tt");
                        dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format); //FB 2588
                        dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(tformat);//FB 1906
                        //Code changed by Offshore for FB Issue 1073 -- End
                        dt.Rows.Add(dr);
                    }
                    CATMainGrid.EditItemIndex = e.Item.ItemIndex;
                    CATMainGrid.DataSource = dt;
                    CATMainGrid.DataBind();
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        // FB 2181 Work order fixes - Start

                        RegularExpressionValidator RegularExpressionValidator12 = (RegularExpressionValidator)dgi.FindControl("RegularExpressionValidator12");
                        MetaBuilders.WebControls.ComboBox lstDeliverByTime = ((MetaBuilders.WebControls.ComboBox)dgi.FindControl("lstDeliverByTime"));
                        if (lstDeliverByTime != null)
                        {
                            lstDeliverByTime.Items.Clear();
                            obj.BindTimeToListBox(lstDeliverByTime, true, true);
                            if (Session["timeFormat"] != null)
                                if (Session["timeFormat"].ToString().Equals("0"))
                                {
                                    RegularExpressionValidator12.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    RegularExpressionValidator12.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                                }
                                else if (Session["timeFormat"].ToString().Equals("2")) //FB 2588
                                {
                                    RegularExpressionValidator12.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                                    RegularExpressionValidator12.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                                }
                        }

                        // FB 2181 Work order fixes - End
                        if (!dgi.ItemIndex.Equals(e.Item.ItemIndex))
                        {
                            String strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                            DataTable dtMenus = new DataTable();
                            if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                            if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                            if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                            for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                            {
                                DataRow drMenu = dtMenus.NewRow();
                                drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                                drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                                drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                                dtMenus.Rows.Add(drMenu);
                            }
                            DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                            dgCateringMenus.DataSource = dtMenus;
                            dgCateringMenus.DataBind();
                        }
                    }
                    CATMainGrid.Visible = true;
                    lblInstructions.Text = obj.GetTranslatedText("Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this conference.");//FB 1830 - Translation
                    UpdateMenus(e.Item.FindControl("lstRooms"), new EventArgs());
                    log.Trace("finished EditWorkorder");
                    Session["CATMainGridDS"] = dt;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Please save/update or cancel current workorder prior to editing or deleting existing workorders.");//FB 1830 - Translation
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        protected void UpdateMenus(Object sender, EventArgs e)
        {
            try
            {
                log.Trace("CATMainGrid.EditItemIndex: " + CATMainGrid.EditItemIndex);
                DropDownList lstSender = (DropDownList)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lstRooms");
                DropDownList lstServices = (DropDownList)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lstServices");
                String inXML = objInXML.SearchProviderMenus(Session["userID"].ToString(), lstSender.SelectedValue, lstServices.SelectedValue);
                log.Trace("SearchProviderMenus Inxml: " + inXML);
                if (inXML != "")
                {
                    String outXML = obj.CallMyVRMServer("SearchProviderMenus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("SearchProviderMenus Outxml: " + outXML);
                    if (outXML.IndexOf("<error>") >= 0) //in case of error
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//SearchProviderMenus/Menu");
                        XmlTextReader xtr;
                        DataSet dsMenus = new DataSet();
                        foreach (XmlNode node in nodes)
                        {
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            dsMenus.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                        DataTable dtMenu = new DataTable();
                        DataGrid dgMenus = (DataGrid)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("dgCateringMenus");
                        if (dsMenus.Tables.Count > 0)
                        {
                            dtMenu = dsMenus.Tables[0];
                            if (!dtMenu.Columns.Contains("Quantity")) dtMenu.Columns.Add("Quantity");
                            log.Trace(((DataGrid)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("dgCateringMenus")).ClientID);
                            dgMenus.DataSource = dtMenu;
                            dgMenus.DataBind();
                            ((Label)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lblNoMenus")).Visible = false;
                            ((LinkButton)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("btnUpdate")).Visible = true;
                            lblInstructions.Text = obj.GetTranslatedText("Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this conference.");//FB 1830 - Translation
                            String strMenus = ((Label)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lblCateringMenus")).Text;
                            foreach (DataGridItem dgiMenu in dgMenus.Items)
                            {
                                for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                                {
                                    if (dgiMenu.Cells[0].Text.Trim().Equals(strMenus.Split(';')[i].Split(':')[0]))
                                    {
                                        ((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked = true;
                                        ((TextBox)dgiMenu.FindControl("txtQuantity")).Text = strMenus.Split(';')[i].Split(':')[2];
                                    }
                                }
                                foreach (XmlNode node in nodes)
                                {
                                    if (node.SelectSingleNode("ID").InnerText.Equals(dgiMenu.Cells[0].Text))
                                    {
                                        DataGrid dgMenuItems = (DataGrid)dgiMenu.FindControl("dgMenuItems");
                                        XmlNodeList subNodes = node.SelectNodes("ItemsList/Item");
                                        DataSet dsItem = new DataSet();
                                        foreach (XmlNode subNode in subNodes)
                                        {
                                            xtr = new XmlTextReader(subNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                            dsItem.ReadXml(xtr, XmlReadMode.InferSchema);
                                        }
                                        if (dsItem.Tables.Count > 0)
                                        {
                                            dgMenuItems.DataSource = dsItem;
                                            dgMenuItems.DataBind();
                                            dgMenuItems.Attributes.Add("style", "display:none");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            dgMenus.DataSource = dtMenu;
                            dgMenus.DataBind();
                            ((LinkButton)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("btnUpdate")).Visible = false;
                            ((Label)CATMainGrid.Items[CATMainGrid.EditItemIndex].FindControl("lblNoMenus")).Visible = true;
                            lblInstructions.Text = obj.GetTranslatedText("There are no menus associated with the selected Service Type for this location. Please select a new Service Type for this location OR click Cancel to return to the catering work orders summary page for this conference.");//FB 1830 - Translation
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("UpdateMenus: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void UpdateWorkorder(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string DateTIme, Date, Time; //FB 2588
                Boolean flagUpdate = true;
                DataTable dt = CreateWorkorderTable();
                log.Trace("In UpdateWorkorder");
                foreach (DataGridItem dgi in CATMainGrid.Items)
                {
                    DataRow dr = dt.NewRow();
                    if (dgi.ItemIndex.Equals(CATMainGrid.EditItemIndex)) // if updating edited item 
                    {
                        dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                        dr["Name"] = dgi.Cells[1].Text;
                        dr["RoomId"] = ((DropDownList)dgi.FindControl("lstRooms")).SelectedValue;
                        dr["RoomName"] = ((DropDownList)dgi.FindControl("lstRooms")).SelectedItem.Text;
                        dr["SelectedService"] = ((DropDownList)dgi.FindControl("lstServices")).SelectedValue;
                        dr["ServiceName"] = ((DropDownList)dgi.FindControl("lstServices")).SelectedItem.Text;
                        dr["Comments"] = ((TextBox)dgi.FindControl("txtComments")).Text;
                        dr["strMenus"] = "";
                        Double price = Double.Parse("0.00");
                        dr["Price"] = "0.00";
                        DataGrid dgMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        Boolean flagMenu = true;
                        Boolean flagQuantity = true;
                        foreach (DataGridItem dgiMenu in dgMenus.Items)
                        {
                            flagMenu = true;
                            flagQuantity = true;
                            //WO Bug Fix
                            if (((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked)
                            {
                                if (((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Equals(""))
                                {
                                    flagMenu = false;
                                    break;
                                }
                                else if (Convert.ToInt32(((TextBox)dgiMenu.FindControl("txtQuantity")).Text) <= 0)
                                {
                                    flagMenu = false;
                                    break;
                                }

                                dr["strMenus"] += dgiMenu.Cells[0].Text + ":" + ((Label)dgiMenu.FindControl("txtMenuName")).Text + ":" + ((TextBox)dgiMenu.FindControl("txtQuantity")).Text + ";";
                                price += Double.Parse(dgiMenu.Cells[1].Text) * Int32.Parse(((TextBox)dgiMenu.FindControl("txtQuantity")).Text);
                            }
                            else
                            {
                                if (((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked && (((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Equals("") 
                                    || ((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Trim().Equals("0")))
                                {
                                    flagMenu = false;
                                    break;
                                }
                                if (!((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked && !((TextBox)dgiMenu.FindControl("txtQuantity")).Text.Equals(""))
                                {
                                    flagQuantity = false;
                                    break;
                                }
                            }
                        }
                        errLabel.Visible = true;
                        if (flagMenu.Equals(false))
                        {
                            errLabel.Text = obj.GetTranslatedText("At least one menu should have a requested quantity > 0.");//Code added for WO //FB 1830 - Translation
                            flagUpdate = false;
                            break;
                        }
                        if (flagQuantity.Equals(false))
                        {
                            errLabel.Text = obj.GetTranslatedText("Please select the corresponding menu for a valid quantity.");//FB 1830 - Translation
                            flagUpdate = false;
                            break;
                        }
                        if (dr["strMenus"].ToString().Equals(""))
                        {
                            errLabel.Text = obj.GetTranslatedText("Please associate at least one menu with each work order.");//FB 1830 - Translation
                            flagUpdate = false;
                            break;
                        }
                        //dr["Price"] = price.ToString("00.00");
                        //FB 1830
                        tmpVal = 0;
                        decimal.TryParse(price.ToString(), out tmpVal);
                        dr["Price"] = tmpVal.ToString("n", cInfo);
                        dr["DeliverByDate"] = ((TextBox)dgi.FindControl("txtDeliverByDate")).Text;
                        dr["DeliverByTime"] = ((MetaBuilders.WebControls.ComboBox)dgi.FindControl("lstDeliverByTime")).Text;
                    }
                    else
                    {
                        DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                        Date = DateTIme.Split(' ')[0];
                        Time = DateTIme.Split(' ')[1];

                        dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                        dr["Name"] = dgi.Cells[0].Text;
                        dr["RoomId"] = dgi.Cells[2].Text;
                        dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                        dr["SelectedService"] = dgi.Cells[3].Text;
                        dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;
                        dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                        dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;
                        DataGrid dgMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        dr["strMenus"] = "";
                        foreach (DataGridItem dgiMenu in dgMenus.Items)
                        {
                            //if (((CheckBox)dgiMenu.FindControl("chkSelectedMenu")).Checked)
                            dr["strMenus"] += dgiMenu.Cells[0].Text + ":" + dgiMenu.Cells[1].Text + ":" + dgiMenu.Cells[2].Text + ";";
                        }
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        //dr["DeliverByDate"] = DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("MM/dd/yyyy");
                        //dr["DeliverByTime"] = DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("hh:mm tt");
                        dr["DeliverByDate"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format); //FB 2588
                        //dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(((Label)dgi.FindControl("lblDeliverByDateTime")).Text)).ToString("hh:mm tt");
                        dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(tformat); 
                        //Code changed by Offshore for FB Issue 1073 -- End
                    }
                    dt.Rows.Add(dr);
                }
                if (flagUpdate)
                {
                    CATMainGrid.EditItemIndex = -1;
                    CATMainGrid.DataSource = dt;
                    CATMainGrid.DataBind();
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        String strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        DataTable dtMenus = new DataTable();
                        if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                        if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                        if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                        for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                        {
                            DataRow drMenu = dtMenus.NewRow();
                            drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                            drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                            drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                            dtMenus.Rows.Add(drMenu);
                        }
                        DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        dgCateringMenus.DataSource = dtMenus;
                        dgCateringMenus.DataBind();
                    }
                    btnAddNewAV.Visible = true;
                    btnCancelMain.Visible = true;
                    btnSubmitMain.Visible = true;
                    lblInstructions.Text = obj.GetTranslatedText("To create a new work order, click on Add New Work Order button.");//FB 1830 - Translation
                }
            }
            catch (Exception ex)
            {
                log.Trace("UpdateWorkorder: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected DataTable CreateWorkorderTable()
        {
            try
            {
                DataTable dt = new DataTable();
                if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
                if (!dt.Columns.Contains("Name")) dt.Columns.Add("Name");
                if (!dt.Columns.Contains("RoomId")) dt.Columns.Add("RoomId");
                if (!dt.Columns.Contains("SelectedService")) dt.Columns.Add("SelectedService");
                if (!dt.Columns.Contains("ServiceName")) dt.Columns.Add("ServiceName");
                if (!dt.Columns.Contains("RoomName")) dt.Columns.Add("RoomName");
                if (!dt.Columns.Contains("strMenus")) dt.Columns.Add("strMenus");
                if (!dt.Columns.Contains("Price")) dt.Columns.Add("Price");
                if (!dt.Columns.Contains("Comments")) dt.Columns.Add("Comments");
                if (!dt.Columns.Contains("DeliverByDate")) dt.Columns.Add("DeliverByDate");
                if (!dt.Columns.Contains("DeliverByTime")) dt.Columns.Add("DeliverByTime");
                return dt;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return null;
            }
        }
        protected void AddBlankWorkorder()
        {
            try
            {
                string DateTIme, Date, Time; //FB 2588
                AVItemsTable.Visible = false;
                DataTable dt = new DataTable();
                dt = CreateWorkorderTable();
                DataRow dr = dt.NewRow();
                foreach (DataGridItem dgi in CATMainGrid.Items)
                {
                    dr = dt.NewRow();

                    DateTIme = ((Label)dgi.FindControl("lblDeliverByDateTime")).Text; //FB 2588
                    Date = DateTIme.Split(' ')[0];
                    Time = DateTIme.Split(' ')[1];

                    dr["ID"] = ((Label)dgi.FindControl("lblID")).Text;
                    dr["Name"] = dgi.Cells[1].Text;
                    dr["RoomId"] = dgi.Cells[2].Text;
                    dr["RoomName"] = ((Label)dgi.FindControl("lblRoomName")).Text;
                    dr["SelectedService"] = dgi.Cells[3].Text;
                    dr["ServiceName"] = ((Label)dgi.FindControl("lblServiceName")).Text;                                       
                    dr["Price"] = ((Label)dgi.FindControl("lblPrice")).Text;

                    dr["Comments"] = ((Label)dgi.FindControl("lblComments")).Text;
                    dr["strMenus"] = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //dr["DeliverByDate"] = DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("MM/dd/yyyy");
                    dr["DeliverByDate"] =  DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(format); //FB 2588
                    //Code changed by Offshore for FB Issue 1073 -- End
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //dr["DeliverByTime"] = DateTime.Parse(((Label)dgi.FindControl("lblDeliverByDateTime")).Text).ToString("hh:mm tt");
                    dr["DeliverByTime"] = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time)).ToString(tformat); //FB 2588
                    //Code changed by Offshore for FB Issue 1073 -- end
                    dt.Rows.Add(dr);
                }
                dr = dt.NewRow();
                dr["ID"] = "new";
                dr["Name"] = lblConfName.Text + "_CAT_" + (CATMainGrid.Items.Count + 1);
                dr["RoomId"] = lstRooms.Items[1].Value;
                dr["RoomName"] = lstRooms.Items[1].Text;
                dr["Comments"] = "";
                dr["SelectedService"] = "1";
                dr["ServiceName"] = "";
                //FB 1830
                Decimal price = 0;
                dr["Price"] = price.ToString("n", cInfo); 
                dr["strMenus"] = "";
                dr["DeliverByDate"] = lblConfDate.Text;
                dr["DeliverByTime"] = lblConfTime.Text;
                dt.Rows.Add(dr);

                CATMainGrid.DataSource = dt;
                CATMainGrid.DataBind();
                EditWorkorder(new object(), new DataGridCommandEventArgs(CATMainGrid.Items[CATMainGrid.Items.Count - 1], (LinkButton)CATMainGrid.Items[CATMainGrid.Items.Count - 1].FindControl("btnEdit"), new CommandEventArgs("Edit", "2")));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void CancelChanges(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                CATMainGrid.EditItemIndex = -1;
                CATMainGrid.DataSource = (DataTable)Session["CATMainGridDS"];
                CATMainGrid.DataBind();
                btnAddNewAV.Visible = true;
                btnCancelMain.Visible = true;
                btnSubmitMain.Visible = true;
                lblInstructions.Text = obj.GetTranslatedText("To create a new work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.");//FB 1830 - Translation
                String strMenus = ((Label)CATMainGrid.Items[e.Item.ItemIndex].FindControl("lblCateringMenus")).Text;
                if (strMenus.Trim().Equals(""))
                    DeleteWorkorder(sender, e);
                else
                    foreach (DataGridItem dgi in CATMainGrid.Items)
                    {
                        strMenus = ((Label)dgi.FindControl("lblCateringMenus")).Text;
                        log.Trace("dgi.cells[0].text: " + dgi.Cells[0].Text);
                        DataTable dtMenus = new DataTable();
                        log.Trace("strMenus: " + strMenus);
                        if (!dtMenus.Columns.Contains("ID")) dtMenus.Columns.Add("ID");
                        if (!dtMenus.Columns.Contains("Name")) dtMenus.Columns.Add("Name");
                        if (!dtMenus.Columns.Contains("Quantity")) dtMenus.Columns.Add("Quantity");
                        for (int i = 0; i < strMenus.Split(';').Length - 1; i++)
                        {
                            DataRow drMenu = dtMenus.NewRow();
                            drMenu["ID"] = strMenus.Split(';')[i].Split(':')[0];
                            drMenu["Name"] = strMenus.Split(';')[i].Split(':')[1];
                            drMenu["Quantity"] = strMenus.Split(';')[i].Split(':')[2];
                            dtMenus.Rows.Add(drMenu);
                        }
                        DataGrid dgCateringMenus = (DataGrid)dgi.FindControl("dgCateringMenus");
                        dgCateringMenus.DataSource = dtMenus;
                        dgCateringMenus.DataBind();
                        btnAddNewAV.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                log.Trace("CancelChanges: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        //FB 1830
        protected string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '\\' };
                string[] fa = fpn.Split(splitter[0]);
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }
    }
}