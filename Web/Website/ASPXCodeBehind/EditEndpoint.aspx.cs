/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;

/// <summary>
/// Summary description for Edit Endpoints
/// </summary>
/// 
namespace ns_EditEndpoint
{
    public partial class EditEndpoint : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblNoEndpoints;
        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;

        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtEndpointID;
        protected System.Web.UI.WebControls.DataGrid dgEndpointList;
        protected System.Web.UI.WebControls.DataGrid dgProfiles;
        protected System.Web.UI.WebControls.Table tblPage;
        
        protected CustomValidator cvSubmit;

        protected System.Web.UI.WebControls.Button btnAddNewProfile;
        protected System.Web.UI.WebControls.Button btnSubmit; //FB 2670

        protected System.Web.UI.HtmlControls.HtmlInputHidden isMarkedDeleted;
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        private String DrpValue = "";  //Endpoint Search Fix
        public EditEndpoint()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("editendpoint.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                Session["profCnt"] = "";
                //code added for endpoint search -- Start
                if (Request.QueryString["EpID"] != null)
                    if (Request.QueryString["EpID"].ToString() != "")
                    {
                        String enpointID = Request.QueryString["EpID"].ToString();
                        Session.Remove("EndpointID");
                        Session.Add("EndpointID", enpointID);
                    }

                if (Request.QueryString["DrpValue"] != null)
                    if (Request.QueryString["DrpValue"].ToString().Trim() != "")
                    {
                        DrpValue = Request.QueryString["DrpValue"].ToString().Trim();
                    }
                //code added for endpoint search -- End
 

                //errLabel.Visible = false; //FB 2400
                //Response.Write("here");
                //Response.Write(Session["userEmail"].ToString());
                if (!IsPostBack)
                {
                    BindData();
                }
                BindprofileAddresseslist(); //FB 2400

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //btnAddNewProfile.ForeColor = System.Drawing.Color.Gray;// FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnAddNewProfile.Attributes.Add("Class", "btndisable");// FB 2796
                    lblHeader.Text = obj.GetTranslatedText("View Endpoint Details");
                    //ZD 100263
                    btnSubmit.Visible = false;
                    btnAddNewProfile.Visible = false;
                }
                // FB 2796 Start
                else
                {
                    btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    btnAddNewProfile.Attributes.Add("Class", "altLongBlueButtonFormat");
                }
                // FB 2796 End
            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.StackTrace + " : " + ex.Message);
            }

        }

        protected void InitializeLists(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))// || (e.Item.ItemType.Equals(ListItemType.Footer)))
                {
                    ((DropDownList)e.Item.FindControl("lstAddressType")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    ((DropDownList)e.Item.FindControl("lstMCUAddressType")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    //((DropDownList)e.Item.FindControl("lstConnectionType")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    ((DropDownList)e.Item.FindControl("lstVideoProtocol")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    //                    Response.Write("1");
                    obj.BindBridges((DropDownList)e.Item.FindControl("lstBridges"));
                    obj.BindVideoEquipment((DropDownList)e.Item.FindControl("lstVideoEquipment"));
                    obj.BindLineRate((DropDownList)e.Item.FindControl("lstLineRate"));
                    obj.BindAddressType((DropDownList)e.Item.FindControl("lstAddressType"));
                    obj.BindAddressType((DropDownList)e.Item.FindControl("lstMCUAddressType"));
                    obj.BindVideoProtocols((DropDownList)e.Item.FindControl("lstVideoProtocol"));
                    obj.BindDialingOptions((DropDownList)e.Item.FindControl("lstConnectionType"));
                    ((Label)e.Item.FindControl("lblProfileCount")).Text = (e.Item.ItemIndex + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                log.Trace("InitializeLists: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void BindData()
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                if (Session["EndpointID"].ToString().ToLower().Equals("new"))
                {
                    lblHeader.Text = obj.GetTranslatedText("Create New Endpoint"); //FB 1830 - Translation
                    txtEndpointID.Text = Session["EndpointID"].ToString();
                    txtEndpointName.Text = "";
                    //String DetailsXml = "<Profile><ProfileID>new</ProfileID><ProfileName>New Profile</ProfileName><EncryptionPreferred>0</EncryptionPreferred><AddressType>1</AddressType><Password></Password><Address></Address><URL></URL><IsOutside>0</IsOutside><VideoEquipment>1</VideoEquipment><LineRate>384</LineRate><Bridge>-1</Bridge><DefaultProtocol>-1</DefaultProtocol><ConnectionType>1</ConnectionType><MCUAddress></MCUAddress><MCUAddressType>-1</MCUAddressType></Profile>";
                    xmldoc = new XmlDocument();
                    XmlNodeList nodes = xmldoc.SelectNodes("//Profile");
                    LoadProfiles(nodes, "new");
                }
                else
                {
                    String inXML = "";
                    inXML += "<EndpointDetails>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <EndpointID>" + Session["EndpointID"].ToString() + "</EndpointID>";
                    inXML += "  <EntityType></EntityType>";
                    inXML += "</EndpointDetails>";
                    String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    outXML = outXML.Replace(" % ", "&lt;br/&gt;"); //FB 1886
                    //Response.Write(obj.Transfer(outXML));
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        if (Session["ProfileID"] != null)
                            if (Session["ProfileID"].ToString().ToLower().Equals("new"))
                            {
                                String newProfileXml = "<Profile><ProfileID>new</ProfileID><ProfileName>New Profile</ProfileName><EncryptionPreferred>0</EncryptionPreferred><AddressType>1</AddressType><Password></Password><Address></Address><URL></URL><IsOutside>0</IsOutside><VideoEquipment>1</VideoEquipment><LineRate>768</LineRate><Bridge>-1</Bridge><DefaultProtocol>1</DefaultProtocol><ConnectionType>2</ConnectionType><MCUAddress></MCUAddress><MCUAddressType>-1</MCUAddressType></Profile>";//FB 2272 //FB 2565
                                outXML = outXML.Substring(0, outXML.IndexOf("</Profiles></Endpoint></EndpointDetails>"));
                                outXML += newProfileXml;
                                outXML += "</Profiles></Endpoint></EndpointDetails>";
                            }

                        //Response.Write(obj.Transfer(outXML));
                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                        txtEndpointID.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/ID").InnerText;
                        txtEndpointName.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText;
                        LoadProfiles(nodes, xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText);
                        //Response.Write(nodes.Count);
                    }
                    //ZD 100263_Nov11 Start
                    else if (outXML.IndexOf("<error>-1</error>") == 0)
                        Response.Redirect("ShowError.aspx");
                    //ZD 100263_Nov11 End
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                }
                //Response.Write(xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile").Count);
                if (xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile").Count >= 5)
                    btnAddNewProfile.Visible = false;
            }
            catch (Exception ex)
            {
                log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void LoadProfiles(XmlNodeList nodes, String defaultProfile)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                String adds = ""; //FB 2400 start
                XmlNodeList multicode = null;

                foreach (XmlNode node in nodes)
                {
                    adds = "";//FB 2602
                    if (node.SelectNodes("//MultiCodec/Address") != null)
                    {
                        multicode = node.SelectNodes("MultiCodec/Address");//FB 2602
                        for (int i = 0; i < multicode.Count; i++)
                        {
                            if (multicode[i] != null)
                                if (multicode[i].InnerText.Trim() != "")
                                {
                                    if (i > 0)
                                        adds += "~";
                                    
                                    adds += multicode[i].InnerText;
                                }
                        }
                        
                        if (multicode.Count > 0)
                            node.SelectSingleNode("MultiCodec").InnerText = adds;
                    }

                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                //FB 2400 end

                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    //Response.Write(ds.Tables[0].Columns[0].ColumnName);
                    dt = ds.Tables[0];
                    if (!dt.Columns.Contains("DefaultProfile"))
                        dt.Columns.Add("DefaultProfile");
                    if (!dt.Columns.Contains("isTelePresence"))//FB 2400 start
                        dt.Columns.Add("isTelePresence");
                    if (!dt.Columns.Contains("RearSecCameraAddress"))
                        dt.Columns.Add("RearSecCameraAddress");
                    if (!dt.Columns.Contains("MultiCodec"))//FB 2400 end
                        dt.Columns.Add("MultiCodec");
                    if (!dt.Columns.Contains("GateKeeeperAddress"))//ZD 100132
                        dt.Columns.Add("GateKeeeperAddress");
                    
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["ProfileID"].ToString().Equals(defaultProfile))
                            dr["DefaultProfile"] = "1";
                        else
                            dr["DefaultProfile"] = "0";
                        if (dr["MCUAddressType"].ToString().Equals("0"))
                            dr["MCUAddressType"] = "-1";
                        if (dr["DefaultProtocol"].ToString().Equals("0"))
                            dr["DefaultProtocol"] = "-1";
                        if (dr["ConnectionType"].ToString().Equals("0"))
                            dr["ConnectionType"] = "2";
                        if (dr["isTelePresence"].ToString().Equals("1")) //FB 2400
                            dr["Address"] = "";
                        //dr["Password"] = ns_MyVRMNet.vrmPassword.MCUProfile; //FB 3054
                    }
                }
                else
                {
                    dt.Columns.Add("ProfileID");
                    dt.Columns.Add("ProfileName");
                    dt.Columns.Add("EncryptionPreferred");
                    dt.Columns.Add("AddressType");
                    dt.Columns.Add("Password");
                    dt.Columns.Add("Address");
                    dt.Columns.Add("URL");
                    dt.Columns.Add("IsOutside");
                    dt.Columns.Add("ConnectionType");
                    dt.Columns.Add("VideoEquipment");
                    dt.Columns.Add("LineRate");
                    dt.Columns.Add("Bridge");
                    dt.Columns.Add("DefaultProfile");
                    dt.Columns.Add("DefaultProtocol");
                    dt.Columns.Add("MCUAddress");
                    dt.Columns.Add("MCUAddressType");
                    dt.Columns.Add("ExchangeID"); //Cisco Telepresence fix
                    dt.Columns.Add("IsCalendarInvite"); //Cisco ICAL FB 1602
                    //Code Added For FB1422 - Start
                    dt.Columns.Add("TelnetAPI");
                    //Code Added For FB1422 - End
                   
                    dt.Columns.Add("ApiPortno");//Api Port...
                    dt.Columns.Add("isTelePresence"); //FB 2400 start
                    dt.Columns.Add("RearSecCameraAddress");
                    dt.Columns.Add("MultiCodec");  //FB 2400 end
                    dt.Columns.Add("Secured");//FB 2595
                    dt.Columns.Add("NetworkURL");//FB 2595
                    dt.Columns.Add("Securedport");//FB 2595
                    dt.Columns.Add("GateKeeeperAddress");//ZD 100132
                    DataRow dr = dt.NewRow();
                    AddNewRow(dr);
                    dr["DefaultProfile"] = "1";
                    dt.Rows.Add(dr);
                }
                dgProfiles.DataSource = dt;
                dgProfiles.DataBind();
                foreach (DataGridItem dgi in dgProfiles.Items)
                {
                    if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                        AllowCreatingNew(dgi, false);
                }

                //FB 1901
                foreach (DataGridItem dgi in dgProfiles.Items)
                {
                    if (Session["isAssignedMCU"] != null)
                        if (Session["isAssignedMCU"].ToString() == "1")
                        {
                            if (((RequiredFieldValidator)dgi.FindControl("reqBridges")) != null)
                                ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = true;

                            //FB 2093
                            if (((RequiredFieldValidator)dgi.FindControl("reqPrefDial")) != null)
                                ((RequiredFieldValidator)dgi.FindControl("reqPrefDial")).Enabled = true;
                        }

                    //FB 2595 - Starts
                    CheckBox chkTempSecured = (CheckBox)(dgi.FindControl("chkSecured"));
                    if (Session["SecureSwitch"] != null)
                    {
                        if (Session["SecureSwitch"].ToString() == "0")
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:hidden");
                            chkTempSecured.Visible = false;
                            chkTempSecured.Checked = false;
                        }
                        else
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:visible");
                            chkTempSecured.Visible = true;
                        }
                    }
                    
                    if (chkTempSecured.Checked)
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:visible");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style","visibility:visible");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:hidden");
                    }
                    //FB 2595 - End
                    //FB 2993 Start
                    if (Session["EnableNetworkFeatures"].ToString() != null)
                    {
                        if (Session["EnableNetworkFeatures"].ToString().Equals("1"))
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:visible");
                            chkTempSecured.Visible = true;
                        }
                        else
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:hidden");
                            chkTempSecured.Visible = false;
                            chkTempSecured.Checked = false;
                            ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:hidden");
                            ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:hidden");
                        }
                    }
                    //FB 2993 End

                    //ZD 100132 START
                    CheckBox chkTemp = (CheckBox)(dgi.FindControl("chkIsOutside"));
                    if (chkTemp.Checked)
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");
                        
                    }
                    //ZD 100132 - End
                }

                ViewState.Add("dgDataSource", dt);
            }
            catch (Exception ex)
            {
                log.Trace("LoadProfiles: " + ex.StackTrace + " : " + ex.Message);
            }

        }
        protected void AddNewRow(DataRow dr)
        {
            try
            {
                dr["ProfileID"] = "new";
                dr["ProfileName"] = "New Profile";
                dr["EncryptionPreferred"] = "";
                dr["AddressType"] = "1";
                dr["Password"] = "";
                dr["Address"] = "";
                dr["URL"] = "";
                dr["IsOutside"] = "0";
                dr["ConnectionType"] = "2"; //FB 2565
                dr["VideoEquipment"] = "1";
                dr["DefaultProtocol"] = "1";
                dr["LineRate"] = "768"; //FB 2565
                dr["Bridge"] = "-1";
                dr["MCUAddress"] = "";
                dr["MCUAddressType"] = "-1";
                //Code Added For FB1422
                dr["TelnetAPI"] = "";         
                dr["IsCalendarInvite"] = "";  //Cisco ICAL FB 1602
                dr["Secured"] = "";  //FB 2595
                dr["NetworkURL"]="";//FB 2595
                dr["Securedport"]="";//FB 2595
            }
            catch (Exception ex)
            {
                log.Trace("AddNewRow: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void SubmitEndpoint(Object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    String inXML = GenerateInxml();
                    //Response.Write(obj.Transfer(inXML));
                    //Response.End();
                    if (inXML == "error") //Cisco ICAL FB 1602//FB 2601
                        return;

                    String outXML = obj.CallMyVRMServer("SetEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    //String outXML = "<success>1</success>";
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        GetAllEndPoints(); //FB 2361
                        Response.Redirect("EndpointList.aspx?t=&m=1&Drpvalue=" + DrpValue);
                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("SubmitEndpoint: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected String GenerateInxml()
        {
            try
            {
                int isTelePresence = 0; //FB 2400
                TextBox txtapiTemp = null;
				int Telepresencecount = 0;//FB 2602
                TextBox txtSecuredTemp = null;//FB 2595
                System.Web.UI.HtmlControls.HtmlInputHidden lblPWChange = null; //FB 3054
                String inXML = "<SetEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "      <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "      <EndpointName>" + txtEndpointName.Text + "</EndpointName>";
                inXML += "      <EntityType></EntityType>";
                inXML += "      <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "      <Profiles>";
                foreach (DataGridItem dgi in dgProfiles.Items)
                {
                    TextBox txtTemp = (TextBox)(dgi.FindControl("txtProfileID"));
                    inXML += "        <Profile>";
                    inXML += "          <ProfileID>" + txtTemp.Text + "</ProfileID>";
                    txtTemp = (TextBox)(dgi.FindControl("txtProfileName"));
                    inXML += "          <ProfileName>" + txtTemp.Text + "</ProfileName>";
                    CheckBox chkTemp = (CheckBox)dgi.FindControl("chkDelete");
                    if (chkTemp.Checked)
                        inXML += "          <Deleted>1</Deleted>";
                    else
                        inXML += "          <Deleted>0</Deleted>";
                    RadioButton rdTemp = (RadioButton)(dgi.FindControl("rdDefault"));
                    if (rdTemp.Checked)
                        inXML += "          <Default>1</Default>";
                    else
                        inXML += "          <Default>0</Default>";
                    chkTemp = (CheckBox)(dgi.FindControl("chkEncryptionPreferred"));
                    if (chkTemp.Checked)
                        inXML += "          <EncryptionPreferred>1</EncryptionPreferred>";
                    else
                        inXML += "          <EncryptionPreferred>0</EncryptionPreferred>";

                    DropDownList lstTemp = (DropDownList)(dgi.FindControl("lstAddressType"));
                    inXML += "          <AddressType>" + lstTemp.SelectedValue + "</AddressType>";
                    txtTemp = (TextBox)dgi.FindControl("txtProfilePassword");
                    lblPWChange = (System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("lbltxtCompare"); //FB 3054
                    if (lblPWChange.Value == "true")
                        inXML += "          <Password>" + Password(txtTemp.Text) + "</Password>"; //FB 3054
                    
                    txtTemp = (TextBox)dgi.FindControl("txtURL");
                    inXML += "          <URL>" + txtTemp.Text + "</URL>";
                    chkTemp = (CheckBox)(dgi.FindControl("chkIsOutside"));
                    if (chkTemp.Checked)
                        inXML += "          <IsOutside>1</IsOutside>";
                    else
                        inXML += "          <IsOutside>0</IsOutside>";
                    
                    //ZD 100132 START
                    if (chkTemp.Checked)
                    {
                        inXML += "<GateKeeeperAddress>" + ((TextBox)(dgi.FindControl("txtGateKeeeperAddress"))).Text + "</GateKeeeperAddress>";
                    }
                    else
                    {
                        inXML += "<GateKeeeperAddress></GateKeeeperAddress>";
                    }
                    //ZD 100132 END
                    
                    lstTemp = (DropDownList)dgi.FindControl("lstConnectionType");
                    inXML += "          <ConnectionType>" + lstTemp.SelectedValue + "</ConnectionType>";
                    lstTemp = (DropDownList)dgi.FindControl("lstVideoEquipment");
                    inXML += "          <VideoEquipment>" + lstTemp.SelectedValue + "</VideoEquipment>";
                    lstTemp = (DropDownList)dgi.FindControl("lstLineRate");
                    inXML += "          <LineRate>" + lstTemp.SelectedValue + "</LineRate>";
                    lstTemp = (DropDownList)dgi.FindControl("lstVideoProtocol");
                    inXML += "          <DefaultProtocol>" + lstTemp.SelectedValue + "</DefaultProtocol>";
                    lstTemp = (DropDownList)dgi.FindControl("lstBridges");
                    inXML += "          <Bridge>" + lstTemp.SelectedValue + "</Bridge>";
                    txtTemp = (TextBox)dgi.FindControl("txtMCUAddress");
                    inXML += "          <MCUAddress>" + txtTemp.Text + "</MCUAddress>";
                    lstTemp = (DropDownList)dgi.FindControl("lstMCUAddressType");
                    inXML += "          <MCUAddressType>" + lstTemp.SelectedValue + "</MCUAddressType>";
                    //Code Added For FB1422-Start
                    chkTemp = (CheckBox)(dgi.FindControl("chkP2PSupport"));
                    if (chkTemp.Checked)
                        inXML += "          <TelnetAPI>1</TelnetAPI>";
                    else
                        inXML += "          <TelnetAPI>0</TelnetAPI>";
                    //Code Added For FB1422-Start

                    txtTemp = (TextBox)dgi.FindControl("txtExchangeID"); //Cisco Telepresence fix
                    inXML += "          <ExchangeID>" + txtTemp.Text + "</ExchangeID>";

                    //Cisco ICAL FB 1602 -- Start
                    CheckBox chkCalendarInvite = (CheckBox)(dgi.FindControl("chkIsCalderInvite"));   
                    if (chkCalendarInvite.Checked)
                        inXML += "          <IsCalendarInvite>1</IsCalendarInvite>"; 
                    else
                        inXML += "          <IsCalendarInvite>0</IsCalendarInvite>";

                    TextBox txtTemp1 = (TextBox)(dgi.FindControl("txtProfileName"));
                    //API Port starts...
                   
                    if(dgi.FindControl("txtApiport") != null)
                    txtapiTemp = (TextBox)(dgi.FindControl("txtApiport"));
                    
                    if(txtapiTemp.Text!= "")
                        inXML += "              <ApiPortno>" + txtapiTemp.Text + "</ApiPortno>";
                    else
                        inXML += "              <ApiPortno>" + "23" + "</ApiPortno>";
                    //API Port ends...
                    if (chkCalendarInvite.Checked)
                    {
                        if (txtTemp.Text.Trim() == "")
                        {
                            String bret = "error";
                            errLabel.Text = obj.GetTranslatedText("Please enter the ExchangeID for the profile name ") + "'" + txtTemp1.Text + "'";//FB 1830 - Translation //FB 2601
                            errLabel.Visible = true;
                            return bret;
                        }
                    }
                    //Cisco ICAL FB 1602 -- End

                    //FB 2400 start
                    txtTemp = (TextBox)dgi.FindControl("txtAddress");
                    isTelePresence = 0;
                    if (((CheckBox)(dgi.FindControl("chkTelepresence"))).Checked)
                        isTelePresence = 1;

                    if (isTelePresence == 1)//FB 2602
                        Telepresencecount++;
                        
                    String[] multiCodec = ((System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("hdnprofileAddresses")).Value.Split('~');//ZD 100263
                    inXML += "<MultiCodec>";
                    if (isTelePresence == 1)
                    {
                        for (int i = 0; i < multiCodec.Length; i++)
                        {
                            if (multiCodec[i].Trim() != "")
                                inXML += "<Address>" + multiCodec[i].Trim().Replace('~', ' ') + "</Address>";

                            if (i == 0)
                                txtTemp.Text = multiCodec[i].Trim().Replace('~', ' ');
                        }
                    }
                    inXML += "</MultiCodec>";

                    inXML += "<Address>" + txtTemp.Text + "</Address>";
                    inXML += "<RearSecCameraAddress>" + ((TextBox)(dgi.FindControl("txtRearSecCamAdd"))).Text + "</RearSecCameraAddress>";
                    inXML += "<isTelePresence>" + isTelePresence + "</isTelePresence>"; //FB 2400 end
                    //FB 2595 - Starts
                    CheckBox chkTempSecured = (CheckBox)(dgi.FindControl("chkSecured"));
                    if (chkTempSecured.Checked)
                        inXML += "<Secured>1</Secured>";
                    else
                        inXML += "<Secured>0</Secured>";

                    if (dgi.FindControl("txtSecuredport") != null)
                    {
                        txtSecuredTemp = (TextBox)(dgi.FindControl("txtSecuredport"));
                        inXML += "<Securedport>" + txtSecuredTemp.Text + "</Securedport>";
                    }

                    txtTemp = (TextBox)dgi.FindControl("txtNetworkURL");
                    inXML += "<NetworkURL>" + txtTemp.Text + "</NetworkURL>";

                    //FB 2595 - End
                    inXML += "</Profile>";
                }
                if (Telepresencecount > 0)//FB 2602
                {
                    if (dgProfiles.Items.Count != Telepresencecount)
                    {
                        errLabel.Text = obj.GetTranslatedText("Please check the telepresence status of all profiles");
                        errLabel.Visible = true;
                        return "error";
                    }
                }                
                inXML += "      </Profiles>";
                inXML += "<EM7EndpointStatus>1</EM7EndpointStatus>"; // FB 2501 EM7 1- Active 0-inActive
                inXML += "<Eptcurrentstatus>0</Eptcurrentstatus>"; // FB 2616 EM7 0- Healthy 
                inXML += "    </SetEndpoint>";
                //Response.Write(obj.Transfer(inXML));
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("GenerateInxml: " + ex.StackTrace + " : " + ex.Message);
                return "<error></error>";
            }
        }
        protected void AddNewProfile(Object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                    if (dgProfiles.Items.Count < 5)
                    {
                        Session["profCnt"] = dgProfiles.Items.Count + 2; // ZD 100263
                        String outXML = ""; //<success>1</success>";
                        String inXML = GenerateInxml();
                        if (inXML == "error")
                        {
                            errLabel.Visible = true;
                            return;
                        }
						else //FB 2601
                        {
                            errLabel.Visible = false;
                        }
                        outXML = obj.CallMyVRMServer("SetEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            GetAllEndPoints(); //FB 2361

                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);

                            Session["EndpointID"] = xmldoc.SelectSingleNode("//SetEndpoint/EndpointID").InnerText;
                            if (Session["ProfileID"] == null)
                                Session.Add("ProfileID", "new");
                            else
                                Session["ProfileID"] = "new";
                            BindData();
                            BindprofileAddresseslist(); //FB 2400
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                    }
                    else
                    {
                        errLabel.Text = obj.GetTranslatedText("At most 5 profiles can be created.");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                log.Trace("AddNewProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void CancelEndpoint(Object sender, EventArgs e)
        {
            Response.Redirect("EndpointList.aspx?t=&Drpvalue="+DrpValue);
        }
        //Fogbugz case 427
        protected void ValidateTypes(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                DataGridItem dgi = (DataGridItem)lstTemp.Parent.Parent;
                ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = false;
                //((RegularExpressionValidator)dgi.FindControl("regAddress")).Enabled = true;
                ((RegularExpressionValidator)dgi.FindControl("regMPI")).Enabled = false;
                if (lstTemp.ID.IndexOf("AddressType") > 0)
                {
                    switch (lstTemp.SelectedValue)
                    {
                        case ns_MyVRMNet.vrmAddressType.MPI:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = true;
                            ((RegularExpressionValidator)dgi.FindControl("regAddress")).Enabled = false;
                            ((RegularExpressionValidator)dgi.FindControl("regMPI")).Enabled = true;
                            isMarkedDeleted.Value = "1";
                            AllowCreatingNew(dgi, false);
                            break;
                        case ns_MyVRMNet.vrmAddressType.ISDN:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            if (((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                            {
                                ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                            }
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.ISDN).Selected = true;
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            AllowCreatingNew(dgi, true);
                            isMarkedDeleted.Value = "0";
                            break;
                        case ns_MyVRMNet.vrmAddressType.IP:
                            if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN))
                            {
                                ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.IP).Selected = true;
                            }
                            if (((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                            {
                                ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                            }
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.IP).Selected = true;
                            if (((DropDownList)dgi.FindControl("lstMCUAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || ((DropDownList)dgi.FindControl("lstMCUAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN))
                            {
                                ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.IP).Selected = true;
                            }
                            AllowCreatingNew(dgi, true);
                            isMarkedDeleted.Value = "0";
                            break;
                    }
                }
                if (lstTemp.ID.IndexOf("Protocol") > 0)
                {
                    switch (lstTemp.SelectedValue)
                    {
                        case ns_MyVRMNet.vrmVideoProtocol.MPI:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = true;
                            AllowCreatingNew(dgi, false);
                            isMarkedDeleted.Value = "1";
                            break;
                        case ns_MyVRMNet.vrmVideoProtocol.ISDN:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            if (((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                            {
                                ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                            }
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            AllowCreatingNew(dgi, true);
                            isMarkedDeleted.Value = "0";
                            break;
                    }
                }

                //if (lstTemp.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) && lstTemp.ID.IndexOf("AddressType") > 0)
                //{
                //    ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                //    ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                //    ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                //    ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                //}
            }
            catch (Exception ex)
            {
                log.Trace("Validatetypes: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void AllowCreatingNew(DataGridItem dgiCurrent, bool isEnabled)
        {
            try
            {
                btnAddNewProfile.Visible = isEnabled;
                isMarkedDeleted.Value = "0";
                if (dgProfiles.Items.Count > 1)
                    foreach (DataGridItem dgi in dgProfiles.Items)
                    {
                        if (!dgi.Equals(dgiCurrent))
                        {
                            ((CheckBox)dgi.FindControl("chkDelete")).Enabled = isEnabled;
                            ((CheckBox)dgi.FindControl("chkDelete")).Checked = !isEnabled;
                            ((RadioButton)dgi.FindControl("rdDefault")).Enabled = isEnabled;
                            //FB Case 734 changes start here: Saima
                            if (!((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                            {
                                //((RadioButton)dgi.FindControl("rdDefault")).Checked = isEnabled; //Commented for FB 1845
                                ((RequiredFieldValidator)dgi.FindControl("reqAddressType")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqName")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqAddress")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqVideoEquipment")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqLineRate")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = isEnabled;
                                isMarkedDeleted.Value = "1";
                            }
                            //FB Case 734 changes end here: Saima
                        }
                        else
                        {
                            //if (!isEnabled) //Commented for FB 1845
                              //  ((RadioButton)dgi.FindControl("rdDefault")).Checked = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqAddressType")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqName")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqAddress")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqVideoEquipment")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqLineRate")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = !isEnabled;
                        }
                    }
            }
            catch (Exception ex)
            {
                log.Trace("AllowCreatingNew: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void ValidateInput(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                cvSubmit.Text = "";
				//FB 3012 Starts
                bool valid = true;
                e.IsValid = true;
                string patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}\:?(\d{4})?$";
                Regex checkIP = new Regex(patternIP);
                String patternISDN = @"^[0-9]+$";
                Regex checkISDN = new Regex(patternISDN);

                foreach (DataGridItem dgi in dgProfiles.Items) //FB 2400
                {
                    DropDownList lstVideoProtocol = (DropDownList)dgi.FindControl("lstVideoProtocol");
                    DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                    RequiredFieldValidator reqAdd = ((RequiredFieldValidator)dgi.FindControl("reqAddress"));
                    TextBox txtAdd = ((TextBox)dgi.FindControl("txtAddress"));
                    String[] IPToValidate = ((System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("hdnprofileAddresses")).Value.Split('~');//ZD 100263

                    reqAdd.Enabled = false;
                    reqAdd.ControlToValidate = "lstProfileAddress";
                    if ((dgProfiles.Items.Count > 1) || (dgProfiles.Items.Count == 1 && !((CheckBox)(dgi.FindControl("chkTelepresence"))).Checked))
                        reqAdd.ControlToValidate = "txtAddress";

                    if(!((CheckBox)(dgi.FindControl("chkTelepresence"))).Checked)
                        IPToValidate = new String[] { txtAdd.Text };

                    if (IPToValidate.Length == 1 && IPToValidate[0].Trim() == "") 
                    {
                        reqAdd.Enabled = true;
                        valid = false;
                    }
                    else if (!((CheckBox)(dgi.FindControl("chkDelete"))).Checked) //FB 2044
                    {
                        for (int i = 0; i < IPToValidate.Length; i++)
                        {                            
                            switch (lstAddressType.SelectedValue)
                            {
                                case ns_MyVRMNet.vrmAddressType.IP:
                                    {
                                        if (lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP))
                                        {
                                            
                                            if (IPToValidate[i] == "")
                                                valid = false;
                                            else
                                                valid = checkIP.IsMatch(IPToValidate[i], 0);
                                            if (!valid)
                                            {
                                                ((TextBox)dgi.FindControl("txtAddress")).Focus();
                                                cvSubmit.Text = obj.GetTranslatedText("Invalid IP Address:") + IPToValidate[i];//FB 1830 - Translation
                                            }
                                        }
                                        else
                                        {
                                            valid = false;
                                            cvSubmit.Text = obj.GetTranslatedText("Default Protocol does not match with selected Address Type.");
                                        }
                                        break;
                                    }

                                case ns_MyVRMNet.vrmAddressType.ISDN:
                                    {
                                        if (lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                                        {                                           
                                            if (IPToValidate[i] == "")
                                                valid = false;
                                            else
                                                valid = checkISDN.IsMatch(IPToValidate[i], 0);
                                            if (!valid)
                                            {
                                                ((TextBox)dgi.FindControl("txtAddress")).Focus();
                                                cvSubmit.Text = obj.GetTranslatedText("Invalid ISDN Address:") + IPToValidate[i];//FB 1830 - Translation
                                            }
                                        }
                                        else
                                        {
                                            valid = false;
                                            cvSubmit.Text = obj.GetTranslatedText("Default Protocol does not match with selected Address Type.");
                                        }
                                        break;
                                    }
                            }                              
                                
                            String errMsg = "";
                            if (!AreMPITypesValid(dgi, ref errMsg))  //Fogbugz case 427
                            {
                                valid = false;
                                cvSubmit.Text = errMsg;
                            }
                            e.IsValid = valid;
                            if (valid.Equals(false))
                                break;

                            //FB 3012 Ends          
                        }
                    }
                    //FB 2703 - Start
                    CheckBox chkTempSecured = (CheckBox)(dgi.FindControl("chkSecured"));
                    if (chkTempSecured.Checked)
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:visible");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:hidden");
                    }
                    //FB 2703 - End

                    //ZD 100132 START
                    CheckBox chkTemp = (CheckBox)(dgi.FindControl("chkIsOutside"));
                    if (chkTemp.Checked)
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");

                    }
                    //ZD 100132 - End
                    
                    //FB 2044 - Starts
                    if (((CheckBox)(dgi.FindControl("chkDelete"))).Checked)
                    {
                        reqAdd.Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqName")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regProfileName")).Enabled = false;

                        ((RegularExpressionValidator)dgi.FindControl("regnumPassword1")).Enabled = false;
                        ((CompareValidator)dgi.FindControl("cmpPass1")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regnumPassword2")).Enabled = false;
                        ((CompareValidator)dgi.FindControl("cmpPass2")).Enabled = false;

                        ((RegularExpressionValidator)dgi.FindControl("regAddress")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regMPI")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqAddressType")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqVideoEquipment")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqLineRate")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqPrefDial")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegMCUAddress")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegURL")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegExchangeID")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegApiport")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regRearSecCamAdd")).Enabled = false;
                        valid = true;
                    }
                    //FB 2044 - End
                    
                    if (valid.Equals(false))
                        break;
                    //Response.End();
                }
            }
            catch (Exception ex)
            {
                log.Trace("ValidateInput: " + ex.StackTrace + " : " + ex.Message);
                e.IsValid = false;
            }
        }
        //Fogbugz case 427
        protected bool AreMPITypesValid(DataGridItem dgi, ref String errMsg)
        {
            try
            {
                DropDownList lstVideoProtocol = (DropDownList)dgi.FindControl("lstVideoProtocol");
                DropDownList lstConnectionType = (DropDownList)dgi.FindControl("lstConnectionType");
                DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                DropDownList lstMCUAddressType = (DropDownList)dgi.FindControl("lstMCUAddressType");
                DropDownList lstMCU = (DropDownList)dgi.FindControl("lstBridges");

                TextBox txtAddress = (TextBox)dgi.FindControl("txtAddress");

                if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    if (!lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    {
                        errMsg = "Invalid MCU Address Type, Connection Type or Protocol Selected for profile #: " + (dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    if (!lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    {
                        errMsg = "Invalid Address Type, Connection Type or Protocol Selected for profile #: " + (dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                    if (!lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    {
                        errMsg = "Invalid MCU Address Type, Address Type or Protocol Selected for profile #: " + (dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    if (!lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    {
                        errMsg = "Invalid Address Types, Connection Type or MCU Address Type Selected for profile #: " + (dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) && !obj.IsValidMCUForMPI(lstMCU.SelectedValue, ref errMsg))
                {
                    errMsg = ns_MyVRMNet.ErrorList.InvalidMPIBridge;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace("TypesAreValid: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        //FB 2361
        #region GetAllEndPoints

        private void GetAllEndPoints()
        {
            try
            {
                String eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["EptXmlPath"].ToString();//FB 1830
                //FB 2594 Starts
                string isPublicEP = "0";
                if (Session["EnablePublicRooms"].ToString() == "1")
                    isPublicEP = "1";
                //FB 2594 Ends
                String inXML = "";
                inXML += "<EndpointDetails>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <PublicEndpoint>" + isPublicEP + "</PublicEndpoint>"; //FB 2594
                inXML += "</EndpointDetails>";

                if (File.Exists(eptxmlPath))
                    File.Delete(eptxmlPath);

                String outXML1 = obj.CallMyVRMServer("GetAllEndpoints", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML1.IndexOf("<error>") < 0)
                {
                    XmlDocument endPoints = new XmlDocument();
                    endPoints.LoadXml(outXML1);
                    endPoints.Save(eptxmlPath);
                    endPoints = null;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "GetAllEndPoints error: " + ex.Message;
                log.Trace(ex.StackTrace + " GetAllEndPoints error : " + ex.Message);
            }
        }

        #endregion

        //FB 2400
        #region BindprofileAddresseslist
        /// <summary>
        /// BindprofileAddresseslist
        /// </summary>
        private void BindprofileAddresseslist()
        {
            String[] ProfileAdds = null;

            if (dgProfiles.Items.Count == 1)
            {
                //FB 2602
                //if (((CheckBox)(dgProfiles.Items[0].FindControl("chkTelepresence"))).Checked)
                //    btnAddNewProfile.Enabled = false;
                //else
                    btnAddNewProfile.Enabled = true;
            }

            DataGridItem dgi = null;
            for (int p = 0; p < dgProfiles.Items.Count; p++)
            {
                dgi = dgProfiles.Items[p];
                System.Web.UI.HtmlControls.HtmlInputHidden hdnprofileAddresses
                       = (System.Web.UI.HtmlControls.HtmlInputHidden)(dgi.FindControl("hdnprofileAddresses"));
                ListBox lstProfileAddress = (ListBox)(dgi.FindControl("lstProfileAddress"));
                lstProfileAddress.Items.Clear();


               if (!((CheckBox)(dgProfiles.Items[p].FindControl("chkTelepresence"))).Checked) //Telepresence will be available if endpoint has only one profile
               {
                    //FB 2602
                    //if (dgProfiles.Items.Count > 1)
                        //((CheckBox)(dgProfiles.Items[p].FindControl("chkTelepresence"))).Enabled = false;
                    
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Enabled = false;
                    ((ListBox)(dgProfiles.Items[p].FindControl("lstProfileAddress"))).Enabled = false;
                    ((System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("hdnprofileAddresses")).Value = "";
                }
                else
                {
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Enabled = true;
                    ((ListBox)(dgProfiles.Items[p].FindControl("lstProfileAddress"))).Enabled = true;
                }
               ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Attributes.Add("OnClick", "javascript:return AddRemoveList('add',this)");                     

                if (hdnprofileAddresses != null && hdnprofileAddresses.Value.Trim() != "")
                {
                    ProfileAdds = hdnprofileAddresses.Value.Split('~');//ZD 100263
                    for (int i = 0; i < ProfileAdds.Length; i++)
                    {
                        if (ProfileAdds[i].Trim() != "")
                        {
                            ListItem item = new ListItem();
                            item.Text = ProfileAdds[i];
                            item.Attributes.Add("title", ProfileAdds[i]);
                            lstProfileAddress.Items.Add(item);
                        }
                    }
                }

            }
        }
        #endregion

        protected string Password(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }
    }
}