﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;

namespace ns_EditEntityOption
{
    public partial class EditEntityOption : System.Web.UI.Page
    {
        #region Protected Data Members
        protected System.Web.UI.WebControls.Label lblHeader; //FB 2670
        protected System.Web.UI.WebControls.TextBox txtEntityName;
        protected System.Web.UI.WebControls.TextBox txtEntityDesc;
        protected System.Web.UI.WebControls.TextBox txtEntityID;

        protected System.Web.UI.WebControls.TextBox txtOptionName;
        protected System.Web.UI.WebControls.TextBox txtOptionDesc;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblEntityOption;
        protected System.Web.UI.WebControls.DataGrid dgLangOptionlist;
        protected System.Web.UI.WebControls.ImageButton imgLangsOption;
        protected System.Web.UI.HtmlControls.HtmlTableRow LangsOptionRow;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnUpdate;
        protected System.Web.UI.HtmlControls.HtmlTableRow trLang;

        DataTable optionsTable = null;
        DataTable optionLanTable = null;
        ArrayList colNames = null;
        DataTable dtable = new DataTable();
        Label lblLangID = new Label();
        Label lblLangName = new Label();
        protected bool defaultLang = true;
        String languageId = "";
        String EntityOptionId = "";

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        #endregion

        #region EditEntityOption

        public EditEntityOption()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("editentityoption.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            if (Session["languageID"] != null)
            {
                if (Session["languageID"].ToString() != "")
                    languageId = Session["languageID"].ToString();
            }

            if (Request.QueryString["OptionID"] != null)
            {
                if (Request.QueryString["OptionID"].ToString().Trim() != "")
                {
                    EntityOptionId = Request.QueryString["OptionID"].ToString().Trim();
                }
            }

            imgLangsOption.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + LangsOptionRow.ClientID + "', false);return false;");
            if (!IsPostBack)
            {
                GetLanguageList();

                if (EntityOptionId != "")
                {
                    if (Session["dtLanguage"] != null)
                        optionLanTable = (DataTable)Session["dtLanguage"];

                    dgLangOptionlist.DataSource = optionLanTable;
                    dgLangOptionlist.DataBind();

                    trLang.Visible = false;
                    btnSubmit.Visible = false;
                    btnUpdate.Visible = true;
                }
                else
                {
                    btnUpdate.Visible = false;
                }
            }

            //FB 2670
            if (Session["admin"].ToString() == "3")
            {
                
                //btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796

                
                //btnUpdate.ForeColor = System.Drawing.Color.Gray;// FB 2796
                //btnUpdate.Attributes.Add("Class", "btndisable");// FB 2796
                lblHeader.Text = obj.GetTranslatedText("View Entity Code Details");//FB 2670
                //ZD 100263
                btnSubmit.Visible = false;
                btnUpdate.Visible = false;

            }
            // FB 2796 start
            else
            {
                btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                btnUpdate.Attributes.Add("Class", "altMedium0BlueButtonFormat");
            }
            // FB 2796 End

        }
        #endregion

        #region CreateNewEntityCode
        /// <summary>
        /// CreateNewEntityCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void CreateNewEntityCode(object sender, EventArgs e)
        {
            try
            {
                TextBox txtOptionName = txtEntityName;
                TextBox txtOptionDesc = txtEntityDesc;
                TextBox txtOptionID = txtEntityID;

                defaultLang = true;
                DataRow dr = null;
                int displayValue = 0;
                CreateDtColumnNames();
                optionsTable = obj.LoadDataTable(null, colNames);

                if (!optionsTable.Columns.Contains("RowUID"))
                    optionsTable.Columns.Add("RowUID");
                int rowID = optionsTable.Rows.Count;

                dtable = optionsTable;
                if (dgLangOptionlist.Items.Count > 0) //ZD 100334
                {
                    for (int i = 0; i < dgLangOptionlist.Items.Count; i++)
                    {
                        if (defaultLang)
                        {
                            lblLangID.Text = "1";
                            lblLangName.Text = obj.GetTranslatedText("English");//ZD 100288
                            i--;
                        }
                        else
                        {
                            txtOptionName = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtOptionName"));
                            txtOptionDesc = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtOptionDesc"));
                            txtOptionID = txtEntityID;
                            lblLangID = ((Label)dgLangOptionlist.Items[i].FindControl("lblLangID"));
                            lblLangName = ((Label)dgLangOptionlist.Items[i].FindControl("lblLangName"));
                            if (txtOptionName.Text.Trim() == "") //Default to option name if not given
                                txtOptionName = txtEntityName;
                            if (txtOptionDesc.Text.Trim() == "") //Default to option Descript if not given
                                txtOptionDesc = txtEntityDesc;
                        }

                        DataTable optionTable = new DataTable();
                        if (Session["dtEntityCode"] != null)
                            optionTable = (DataTable)Session["dtEntityCode"];

                        for (int k = 0; k < optionTable.Rows.Count; k++)
                        {
                            if (optionTable.Rows[k]["ID"].ToString().Equals(lblLangID.Text.Trim()) && !optionTable.Rows[k]["OptionID"].ToString().Equals(txtEntityID.Text.Trim()))
                            {
                                if (optionTable.Rows[k]["DisplayCaption"].ToString().ToLower().Trim().Equals(txtOptionName.Text.ToLower().Trim()))
                                {
                                    errLabel.Text = obj.GetTranslatedText("Option Name already exists in the list for Language") + " " + lblLangName.Text.Trim();
                                    errLabel.Visible = true;
                                    optionsTable = dtable;
                                    return;
                                }
                            }
                        }

                        rowID = optionsTable.Rows.Count;
                        dr = optionsTable.NewRow();
                        dr["ID"] = lblLangID.Text.Trim();
                        dr["name"] = lblLangName.Text.Trim();
                        dr["DisplayCaption"] = txtOptionName.Text.Trim();
                        dr["HelpText"] = txtOptionDesc.Text.Trim();
                        dr["OptionID"] = txtOptionID.Text.Trim();

                        string filterExp = "ID= '" + languageId + "'";
                        DataRow[] drArr = null;
                        if (optionTable.Rows.Count > 0)
                            drArr = optionTable.Select(filterExp);

                        if (defaultLang)
                        {
                            if (drArr != null)
                                if (drArr.Length > 0)
                                    displayValue = drArr.Length;
                            defaultLang = false;
                            displayValue++;
                        }

                        dr["DisplayValue"] = displayValue;
                        dr["RowUID"] = rowID + 1;
                        optionsTable.Rows.Add(dr);
                    }
                }
                else if (dgLangOptionlist.Items.Count == 0) //ZD 100334
                {
                    rowID = optionsTable.Rows.Count;
                    dr = optionsTable.NewRow();
                    dr["ID"] = languageId.ToString();
                    dr["name"] = lblLangName.Text.Trim();
                    dr["DisplayCaption"] = txtOptionName.Text.Trim();
                    dr["HelpText"] = txtOptionDesc.Text.Trim();
                    dr["OptionID"] = "new";
                    dr["DisplayValue"] = "1";
                    dr["RowUID"] = rowID + 1;
                    optionsTable.Rows.Add(dr);
                }

                if (Session["dtLanguages"] != null)
                {
                    dgLangOptionlist.DataSource = (DataTable)Session["dtLanguages"];
                    dgLangOptionlist.DataBind();
                }

                Session["dtEntityCode"] = optionsTable;
                txtEntityName.Text = "";
                txtEntityDesc.Text = "";
                txtEntityID.Text = "new";

                SetEntityCode();

            }
            catch (Exception ex)
            {
                log.Trace("CreateNewEntityCode" + ex.Message);
            }
        }
        #endregion

        #region CreateDtColumnName
        /// <summary>
        /// CreateDtColumnNames
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("ID");
            colNames.Add("name");
            colNames.Add("DisplayCaption");
            colNames.Add("HelpText");
            colNames.Add("OptionID");
            colNames.Add("DisplayValue");
        }
        #endregion

        #region SetEntityCode
        /// <summary>
        /// SetEntityCode
        /// </summary>
        protected void SetEntityCode()
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<SetEntityCode>");
                inXML.Append("<userId>" + Session["userID"].ToString() + "</userId>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<optionType>6</optionType>");//6- DropDown-List
                if (Session["dtEntityCode"] != null)
                    optionsTable = (DataTable)Session["dtEntityCode"];
                if (optionsTable != null)
                {
                    inXML.Append("<OptionList>");
                    foreach (DataRow drr in optionsTable.Rows)
                    {
                        inXML.Append("<Option>");
                        inXML.Append("<OptionID>" + drr["OptionID"].ToString() + "</OptionID>");
                        inXML.Append("<OptionValue>" + drr["DisplayValue"].ToString() + " </OptionValue>");
                        inXML.Append("<OptionCaption>" + drr["DisplayCaption"].ToString() + "</OptionCaption>");
                        inXML.Append("<HelpText>" + drr["HelpText"].ToString() + "</HelpText>");
                        inXML.Append("<LangID>" + drr["ID"].ToString() + "</LangID>");
                        inXML.Append("</Option>");
                    }
                    inXML.Append("</OptionList>");
                }
                inXML.Append("</SetEntityCode>");

                String outXML = obj.CallMyVRMServer("SetEntityCode", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.ToString().IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("ManageEntityCode.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("SetEntityCode" + ex.Message);
            }
        }

        #endregion

        #region UpdateEntityCode
        /// <summary>
        /// UpdateEntityCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateEntityCode(Object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;

                optionLanTable = new DataTable();

                CreateDtColumnNames();
                optionLanTable = obj.LoadDataTable(null, colNames);

                if (!optionLanTable.Columns.Contains("RowUID"))
                    optionLanTable.Columns.Add("RowUID");
                String defaultOptName = "", defaultOptDesc = "";
                for (int i = 0; i < dgLangOptionlist.Items.Count; i++)
                {
                    txtOptionName = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtOptionName"));
                    txtOptionDesc = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtOptionDesc"));
                    lblLangID = ((Label)dgLangOptionlist.Items[i].FindControl("lblLangID"));
                    lblLangName = ((Label)dgLangOptionlist.Items[i].FindControl("lblLangName"));
                    if (lblLangName.Text.Trim() == "English")
                    {
                        defaultOptName = txtOptionName.Text.Trim();
                        if (defaultOptName == "")
                        {
                            errLabel.Text = obj.GetTranslatedText("Please add option Name to Language English.");
                            errLabel.Visible = true;
                            optionsTable = dtable;
                            return;
                        }
                        defaultOptDesc = txtOptionDesc.Text.Trim();
                    }
                    if (txtOptionName.Text.Trim() == "") //Default to option name if not given
                        txtOptionName.Text = defaultOptName;
                    if (txtOptionDesc.Text.Trim() == "") //Default to option Descript if not given
                        txtOptionDesc.Text = defaultOptDesc;
                    if (Session["dtEntityCode"] != null)
                        optionsTable = (DataTable)Session["dtEntityCode"];

                    dr = optionLanTable.NewRow();

                    for (int k = 0; k < optionsTable.Rows.Count; k++)
                    {
                        if (optionsTable.Rows[k]["ID"].ToString().Equals(lblLangID.Text.Trim()) && !optionsTable.Rows[k]["OptionID"].ToString().Equals(EntityOptionId.Trim()))
                        {
                            if (optionsTable.Rows[k]["DisplayCaption"].ToString().ToLower().Trim().Equals(txtOptionName.Text.ToLower().Trim()))
                            {
                                errLabel.Text = obj.GetTranslatedText("Option Name already exists in the list for Language  ") + lblLangName.Text.Trim();
                                errLabel.Visible = true;
                                optionsTable = dtable;
                                return;
                            }
                        }
                        if (optionsTable.Rows[k]["OptionID"].ToString().Equals(EntityOptionId.Trim()))
                        {
                            dr["RowUID"] = optionsTable.Rows[k]["RowUID"].ToString();
                            dr["DisplayValue"] = optionsTable.Rows[k]["DisplayValue"].ToString();

                        }
                    }

                    dr["ID"] = lblLangID.Text.Trim();
                    dr["name"] = lblLangName.Text.Trim();
                    dr["DisplayCaption"] = txtOptionName.Text.Trim();
                    dr["HelpText"] = txtOptionDesc.Text.Trim();
                    dr["OptionID"] = EntityOptionId.Trim();
                    optionLanTable.Rows.Add(dr);
                }

                StringBuilder inXML = new StringBuilder();
                inXML.Append("<UpdateEntityCode>");
                inXML.Append("<userId>" + Session["userID"].ToString() + "</userId>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<optionType>6</optionType>");//6- DropDown-List
                inXML.Append("<OptionList>");
                foreach (DataRow drw in optionLanTable.Rows)
                {
                    if (drw["OptionID"].ToString() == EntityOptionId.Trim())
                    {
                        inXML.Append("<Option>");
                        inXML.Append("<OptionID>" + drw["OptionID"].ToString() + "</OptionID>");
                        inXML.Append("<OptionValue>" + drw["DisplayValue"].ToString() + " </OptionValue>");
                        inXML.Append("<OptionCaption>" + drw["DisplayCaption"].ToString() + "</OptionCaption>");
                        inXML.Append("<HelpText>" + drw["HelpText"].ToString() + "</HelpText>");
                        inXML.Append("<LangID>" + drw["ID"].ToString() + "</LangID>");
                        inXML.Append("</Option>");
                    }
                }
                inXML.Append("</OptionList>");
                inXML.Append("</UpdateEntityCode>");

                String outXML = obj.CallMyVRMServer("UpdateEntityCode", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.ToString().IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("ManageEntityCode.aspx");

                txtEntityName.Text = "";
                txtEntityDesc.Text = "";
                txtEntityID.Text = "new";
            }
            catch (Exception ex)
            {
                log.Trace("UpdateEntityCode" + ex.Message);
            }
        }
        #endregion

        #region GetLanguageList
        /// <summary>
        /// GetLanguageList
        /// </summary>
        protected void GetLanguageList()
        {
            try
            {
                String userID = "11,", inXML = "", outXML = "";
                dtable = new DataTable();
                XmlDocument xmldoc = new XmlDocument();

                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                inXML = "<GetLanguages><UserID>" + userID + "</UserID>" + obj.OrgXMLElement() + "</GetLanguages>";
                outXML = obj.CallMyVRMServer("GetLanguages", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLanguages/Language");
                    if (nodes.Count > 0)
                    {
                        dtable = obj.LoadDataTable(nodes, null);
                        if (!dtable.Columns.Contains("Title"))
                            dtable.Columns.Add("Title");

                        for (int i = 0; i < dtable.Rows.Count; i++)
                        {
                            DataRow dr = dtable.Rows[i];
                            if (dr["ID"].ToString() == "1")
                                dtable.Rows.Remove(dr);
                        }
                        if (Session["dtLanguages"] == null)
                            Session.Add("dtLanguages", dtable);
                        else
                            Session["dtLanguages"] = dtable;
                    }
                    dgLangOptionlist.DataSource = dtable;

                    if (!dtable.Columns.Contains("ID"))
                        dtable.Columns.Add("ID");
                    if (!dtable.Columns.Contains("name"))
                        dtable.Columns.Add("name");
                    if (!dtable.Columns.Contains("DisplayCaption"))
                        dtable.Columns.Add("DisplayCaption");
                    if (!dtable.Columns.Contains("HelpText"))
                        dtable.Columns.Add("HelpText");
                    if (!dtable.Columns.Contains("OptionID"))
                        dtable.Columns.Add("OptionID");
                    if (!dtable.Columns.Contains("DisplayValue"))
                        dtable.Columns.Add("DisplayValue");

                    dgLangOptionlist.DataBind();
                }
                else
                    errLabel.Text = obj.ShowErrorMessage(outXML);

            }
            catch (Exception ex)
            {
                log.Trace("GetLanguageList" + ex.Message);
            }
        }
        #endregion

        #region btnCancel_Click
        /// <summary>
        /// btnCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageEntityCode.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("btnCancel_Click" + ex.Message);
            }
        }
        #endregion

        // ZD 100288 start
        public string fnGetTranslatedText(string par)
        {
            return obj.GetTranslatedText(par);
        }
        // ZD 100288 End
    }
}
