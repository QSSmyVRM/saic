/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_HolidayDetails
{
    public partial class EditHolidayDetails : System.Web.UI.Page
    {
        #region Protected Data Members

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtHolidayName;
        protected System.Web.UI.WebControls.TextBox txtbgcolor;
        protected System.Web.UI.WebControls.ListBox CustomDate;
        protected System.Web.UI.WebControls.CheckBox chkBlocked;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTypeID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDateList;
        protected System.Web.UI.WebControls.Button btnSubmit; //FB 2670
        
        
        #endregion

        #region private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        protected String typeID = "";
        protected String format = "MM/dd/yyyy";
        protected Int32 CustomSelectedLimit = 100;

        #endregion

        #region EditHolidayDetails

        public EditHolidayDetails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region Page Load 

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("editholidaydetails.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

              
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!"); //FB 2272
                        errLabel.Visible = true;
                    }

                if (Request.QueryString["TypeID"] != null)
                {
                    if (Request.QueryString["TypeID"].ToString().Trim() != "")
                    {
                        typeID = Request.QueryString["TypeID"].ToString().Trim();
                        hdnTypeID.Value = typeID;
                    }
                }
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                if(typeID != "")
                    lblHeader.Text = obj.GetTranslatedText("Edit Day Color Details"); //FB 2272
                else
                    lblHeader.Text = obj.GetTranslatedText("Create New Day Color"); //FB 2272

                if (!IsPostBack)
                {
                    //CustomDate.Items.Clear();
                    BindData();
                }

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray; //FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    lblHeader.Text = obj.GetTranslatedText("View Color Day Details");
                    btnSubmit.Visible = false;//ZD 100263
                }
                else
                    btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;
            }
        }
        #endregion

        #region BindData 

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<GetOrgHolidays>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                if (hdnTypeID.Value != "")
                    inXML += "  <typeID>" + hdnTypeID.Value + "</typeID>";
                else
                    inXML += "  <typeID>new</typeID>";
                inXML += "</GetOrgHolidays>";

                String outXML = obj.CallMyVRMServer("GetOrgHolidays", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    if(xmldoc.SelectSingleNode("//SystemHolidays/HolidayDescription") != null)
                        txtHolidayName.Text = xmldoc.SelectSingleNode("//SystemHolidays/HolidayDescription").InnerText;
                    if (xmldoc.SelectSingleNode("//SystemHolidays/Color") != null)
                        txtbgcolor.Text = xmldoc.SelectSingleNode("//SystemHolidays/Color").InnerText;

                    XmlNodeList nodes = xmldoc.SelectNodes("//SystemHolidays/Holidays/Holiday");
                    if (nodes != null && nodes.Count > 0)
                    {
                        for (Int32 n = 0; n < nodes.Count; n++)
                            CustomDate.Items.Add(myVRMNet.NETFunctions.GetFormattedDate(nodes[n].SelectSingleNode("Date").InnerText));
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }

        #endregion

        #region SetOrgHolidays 
        protected void SetOrgHolidays(Object sender, EventArgs e)
        {
            String outxml = "";
            StringBuilder outXML = new StringBuilder();
            String[] strDateList = null;
            try
            {
                outXML.Append("<SetOrgHolidays>");
                outXML.Append(obj.OrgXMLElement());
                outXML.Append("<SystemHolidays>");
                outXML.Append("<HolidayName>" + txtHolidayName.Text + "</HolidayName>");
                outXML.Append("<Color>" + txtbgcolor.Text + "</Color>");
                if (hdnTypeID.Value != "")
                    outXML.Append("<HolidayType>" + hdnTypeID.Value + "</HolidayType>");
                else
                    outXML.Append("<HolidayType></HolidayType>");
                
                if (hdnDateList.Value != "")
                {
                    strDateList = hdnDateList.Value.Split(',');

                    outXML.Append("<Holidays>");
                    if (strDateList != null)
                    {
                        for (int dts = 0; dts < strDateList.Length; dts++)
                        {
                            outXML.Append("<Holiday>");
                            outXML.Append("<Date>" + myVRMNet.NETFunctions.GetDefaultDate(strDateList[dts]) + "</Date>");
                            outXML.Append("<HolidayType></HolidayType>");
                            outXML.Append("</Holiday>");
                        }
                    }
                    outXML.Append("</Holidays>");
                }
                outXML.Append("</SystemHolidays>");
                outXML.Append("</SetOrgHolidays>");

                outxml = obj.CallCommand("SetOrgHolidays", outXML.ToString());

                if (outxml.IndexOf("<error>") < 0)
                {
                    MyVRMNet.LoginManagement objLogin = new MyVRMNet.LoginManagement();
                    objLogin.LoadOrgHolidays();
                    Response.Redirect("HolidayDetails.aspx?m=1");
                }
                else
                {
                    hdnDateList.Value = "";
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                    if (CustomDate.Items.Count <= 0 && strDateList != null)
                    {
                      for (int dts = 0; dts < strDateList.Length ; dts++)
                        CustomDate.Items.Add(myVRMNet.NETFunctions.GetFormattedDate(strDateList[dts]));
                    }
                    return;
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SetOrgHolidays" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion
    }
}
