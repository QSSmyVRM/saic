/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using System.Text.RegularExpressions;
using System.Text;
/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_EditUserTemplates
{
    public partial class UserTemplates : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.TextBox txtTemplateName;
        protected System.Web.UI.WebControls.TextBox txtInitialTime;
        protected System.Web.UI.WebControls.TextBox txtAccountExpiry;
        protected System.Web.UI.WebControls.TextBox txtIPISDNAddress;
        protected System.Web.UI.WebControls.DropDownList lstTimeZone;
        protected System.Web.UI.WebControls.DropDownList lstUserRole;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;
        protected System.Web.UI.WebControls.DropDownList lstMCU;
        protected System.Web.UI.WebControls.DropDownList lstGroup;
        protected System.Web.UI.WebControls.DropDownList lstProtocol;
        protected System.Web.UI.WebControls.DropDownList lstDepartment;
        protected System.Web.UI.WebControls.DropDownList lstEquipment;
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstLanguage;
        protected System.Web.UI.WebControls.CheckBox chkOutsideNetwork;
        protected System.Web.UI.WebControls.CheckBox chkEmailNotification;
        protected System.Web.UI.WebControls.RadioButtonList lstRoomSelection;
        protected System.Web.UI.WebControls.RadioButtonList rdSelView;
        protected System.Web.UI.WebControls.TreeView treeRoomSelection;
        protected System.Web.UI.WebControls.Panel pnlListView;
        protected System.Web.UI.WebControls.Panel pnlLevelView;
        //protected System.Web.UI.WebControls.RangeValidator rangeExpiryDate; FB issue 1493
        protected System.Web.UI.WebControls.CustomValidator cusVal1;
        protected System.Web.UI.WebControls.Button btnSubmit; //FB 481 Saima
        protected System.Web.UI.WebControls.Button btnSubmitNew; //FB 481 Saima
        //Code added by Offshore for FB Issue 1073
        protected String format = "MM/dd/yyyy"; //Changed for FB issue 1493
        protected string licenseDate = ""; //Added for FB issue 1493
        protected System.Web.UI.WebControls.Panel pnlNoData; //Added For Location Issues
        protected System.Web.UI.HtmlControls.HtmlTableRow TzTR;//Code added for FB 1425 QA Bug
        protected System.Web.UI.HtmlControls.HtmlTableRow trAudvid;//Code added for FB 1425 QA Bug 
        protected System.Web.UI.HtmlControls.HtmlTableCell trTbAudvid;//Code added for FB 1425 QA Bug 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdntzone;//Code added for FB 1425 QA Bug 
        /**** code added for room search  ****/
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;
        protected System.Web.UI.WebControls.Button benReset;//ZD 100263

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        public UserTemplates()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        private void Page_Init(Object sender, System.EventArgs e)
        {
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("editusertemplate.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                obj = new myVRMNet.NETFunctions();
                

                errLabel.Text = "";
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                //Code added by Offshore for FB Issue 1073 -- End
                
                if (!IsPostBack)
                {
                    if (Session["UTID"].ToString().Equals("new"))
                        lblHeader.Text = obj.GetTranslatedText("Create New User Template");//FB 1830 - Translation
                    else if(Session["admin"].ToString() == "3")
                        lblHeader.Text = obj.GetTranslatedText("View User Template Details");//FB 2670
                    else
                        lblHeader.Text = obj.GetTranslatedText("Edit User Template");//FB 1830 - Translation
                    BindData();
                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                }

                btnSubmit.Enabled = true;

                /*Added for Location Issues-- Start
                if (treeRoomSelection.Nodes.Count == 0 && lstRoomSelection.Items.Count == 0)
                {
                    rdSelView.Enabled = false;
                    pnlNoData.Visible = true;
                    pnlListView.Visible = false;
                    pnlLevelView.Visible = false;
                    btnSubmit.Enabled = false;
                }
                //Added for Location Issues-- End*/
				/* *** Code added for FB 1425 QA Bug -Start *** */

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    TzTR.Visible = false;
                    trAudvid.Visible = false;
                    trTbAudvid.Visible = false;

                    if (hdntzone.Value == "")
                    {
                        lstTimeZone.SelectedValue = "31";
                        //String timezne = obj.GetSystemTimeZone(Application["COM_ConfigPath"].ToString());
                        String timezne = obj.GetSystemTimeZone(Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                        if (timezne != "")
                        {
                            lstTimeZone.ClearSelection();
                            lstTimeZone.SelectedValue = timezne;
                        }
                    }
                }

                /* *** Code added for FB 1425 QA Bug -End *** */


                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    // btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2976
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796

                    
                    // btnSubmitNew.ForeColor = System.Drawing.Color.Gray;// FB 2976
                    //btnSubmitNew.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnSubmit.Visible = false;
                    btnSubmitNew.Visible = false;
                    benReset.Visible = false;

                }
                //FB 2796 start
                else
                {
                    btnSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");
                    btnSubmitNew.Attributes.Add("Class", "altLongBlueButtonFormat");
                }
                //FB 2796 End
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }

        protected void BindData()
        {
            try
            {
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<userTemplateID>" + Session["UTID"].ToString() + "</userTemplateID>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetUserTemplateDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    obj.BindDialingOptions(lstConnectionType);
                    obj.BindVideoProtocols(lstProtocol);
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    //Changed for FB issue 1493 - Start
                    
                    //rangeExpiryDate.MinimumValue = DateTime.Today.ToShortDateString();
                    //Code added by Offshore for FB Issue 1073 -- Start
                    //rangeExpiryDate.ErrorMessage = "Invalid Date<br>Maximum date allowed is the<br>site license expiry date (" + DateTime.Parse(xmldoc.SelectSingleNode("//UserTemplate/licenseExpiry").InnerText).ToShortDateString() + ")";
                    //rangeExpiryDate.ErrorMessage = "Invalid Date<br>Maximum date allowed is the<br>site license expiry date (" + myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//UserTemplate/licenseExpiry").InnerText) + ")";
                    //Code added by Offshore for FB Issue 1073 -- End
                    //rangeExpiryDate.MaximumValue = DateTime.Parse(xmldoc.SelectSingleNode("//UserTemplate/licenseExpiry").InnerText).ToShortDateString();
                   
                    licenseDate = xmldoc.SelectSingleNode("//UserTemplate/licenseExpiry").InnerText;
                    if (!xmldoc.SelectSingleNode("//UserTemplate/accountExpiry").InnerText.Trim().Equals("01/01/0001"))
                    {
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        txtAccountExpiry.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//UserTemplate/accountExpiry").InnerText);
                        //Code changed by Offshore for FB Issue 1073 -- End
                        //txtAccountExpiry.Text = xmldoc.SelectSingleNode("//UserTemplate/accountExpiry").InnerText;
                    }
                    else
                        txtAccountExpiry.Text = "";
                  
                    
                    //Changed for FB issue 1493 - End

                    txtTemplateName.Text = xmldoc.SelectSingleNode("//UserTemplate/templateName").InnerText;
                    txtInitialTime.Text = xmldoc.SelectSingleNode("//UserTemplate/initialTime").InnerText;
                    txtIPISDNAddress.Text = xmldoc.SelectSingleNode("//UserTemplate/IPISDNAddress").InnerText;
                    XmlNodeList nodes = xmldoc.SelectNodes("//UserTemplate/timezones/timezone");
                    String selTZ = "-1";
                    lstTimeZone.ClearSelection();
                    obj.GetTimezones(lstTimeZone, ref selTZ);
                    //obj.GetSystemDateTime(Application["COM_ConfigPath"].ToString());//Login Management
                    obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    lstTimeZone.ClearSelection();
                    if (!xmldoc.SelectSingleNode("//UserTemplate/timezone").InnerText.Equals("0"))
                        lstTimeZone.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/timezone").InnerText).Selected = true;
                    else
                        lstTimeZone.Items.FindByValue(Session["systemTimezoneID"].ToString()).Selected = true;
                    nodes = xmldoc.SelectNodes("//UserTemplate/roles/role");
                    LoadUserRoles(nodes);
                    if (!xmldoc.SelectSingleNode("//UserTemplate/roleID").InnerText.Equals("0"))
                        lstUserRole.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/roleID").InnerText).Selected = true;
                    //GetLocationList(xmldoc.SelectSingleNode("//UserTemplate/locationList/selected").InnerText);Code added for room search
                    myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                    locstrname.Value = ntfuncs.RoomDetailsString(xmldoc.SelectSingleNode("//UserTemplate/locationList/selected").InnerText);
                    selectedloc.Value = xmldoc.SelectSingleNode("//UserTemplate/locationList/selected").InnerText;
                    BindRoomToList();
                    ntfuncs = null;
                    nodes = xmldoc.SelectNodes("//UserTemplate/lineRate/rate");
                    LoadLineRate(nodes);
                    string LineRateID = xmldoc.SelectSingleNode("//UserTemplate/lineRateID").InnerText;
                    if (LineRateID.Equals("0"))
                        LineRateID = "384";
                    lstLineRate.Items.FindByValue(LineRateID).Selected = true;
                    nodes = xmldoc.SelectNodes("//UserTemplate/bridges/bridge");
                    LoadBridges(nodes);
                    if (!xmldoc.SelectSingleNode("//UserTemplate/bridgeID").InnerText.Equals("0"))
                        lstMCU.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/bridgeID").InnerText).Selected = true;
                    nodes = xmldoc.SelectNodes("//UserTemplate/languages/language");
                    //LoadLanguage(nodes);
                    obj.GetLanguages(lstLanguage);//FB 1830
                    if (Session["UTID"].ToString().Equals("new"))
                        lstLanguage.Items.FindByValue("1").Selected = true;
                    if (!xmldoc.SelectSingleNode("//UserTemplate/languageID").InnerText.Equals("0"))
                        lstLanguage.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/languageID").InnerText).Selected = true;
                    if (!xmldoc.SelectSingleNode("//UserTemplate/videoProtocol").InnerText.Equals("0"))
                        lstProtocol.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/videoProtocol").InnerText).Selected = true;
                    //Response.Write(xmldoc.SelectSingleNode("//UserTemplate/connectionType").InnerText);
                    if (!xmldoc.SelectSingleNode("//UserTemplate/connectionType").InnerText.Equals("") && !xmldoc.SelectSingleNode("//UserTemplate/connectionType").InnerText.Equals("-1"))
                    {
                        if(lstConnectionType.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/connectionType").InnerText) != null)
                            lstConnectionType.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/connectionType").InnerText).Selected = true;
                    }
                    if (!xmldoc.SelectSingleNode("//UserTemplate/departmentID").InnerText.Equals("0"))
                        lstDepartment.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/departmentID").InnerText).Selected = true;
                    if (!xmldoc.SelectSingleNode("//UserTemplate/EquipmentID").InnerText.Equals("0"))
                        lstEquipment.Items.FindByValue(xmldoc.SelectSingleNode("//UserTemplate/EquipmentID").InnerText).Selected = true;
                    if (xmldoc.SelectSingleNode("//UserTemplate/OutsideNetwork").InnerText.Equals("1"))
                        chkOutsideNetwork.Checked = true;
                    if (xmldoc.SelectSingleNode("//UserTemplate/EmailNotification").InnerText.Equals("1"))
                        chkEmailNotification.Checked = true;
                    if (!Session["UTID"].ToString().Equals("new"))//Code added for  FB 1425 QA Bug
                        hdntzone.Value = "1";//Code added for  FB 1425 QA Bug
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void GetLocationList(String selectedLoc)
        {
            try
            {
                String inXML = "<GetLocationsList>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></GetLocationsList>";//Organization Module Fixes
                String outXML = obj.CallMyVRMServer("GetLocationsList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//level3List/level3");
                TreeNode tnTop;

                if (nodes.Count > 0)
                {
                    tnTop = new TreeNode("All Rooms", "0");
                    tnTop.SelectAction = TreeNodeSelectAction.None;
                    foreach (XmlNode node3 in nodes)
                    {
                        //Response.Write("<br>" + node3.SelectSingleNode("level3Name").InnerText + " : " + node3.SelectNodes("level2List/level2/level1List/level1").Count);
                        if (node3.SelectNodes("level2List/level2/level1List/level1").Count > 0)
                        {
                            TreeNode tn3 = new TreeNode(node3.SelectSingleNode("level3Name").InnerText, node3.SelectSingleNode("level3ID").InnerText);
                            if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                                if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                                {
                                    if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                                    {
                                        if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 2)
                                            tn3.Expanded = true;
                                    }
                                }
                                else
                                    tn3.Expanded = false;
                            tnTop.ChildNodes.Add(tn3);
                            XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");
                            tn3.SelectAction = TreeNodeSelectAction.None;
                            foreach (XmlNode node2 in nodes2)
                            {
                                TreeNode tn2 = new TreeNode(node2.SelectSingleNode("level2Name").InnerText, node2.SelectSingleNode("level2ID").InnerText);
                                tn2.SelectAction = TreeNodeSelectAction.None;
                                if (node2.SelectNodes("level1List/level1").Count > 0)
                                {
                                    tn3.ChildNodes.Add(tn2);
                                    XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                    foreach (XmlNode node1 in nodes1)
                                    {
                                        if (Convert.ToInt32(node1.SelectSingleNode("disabled").InnerText) != 1) //code added for Fb 1443
                                        {
                                            TreeNode tn1 = new TreeNode(node1.SelectSingleNode("level1Name").InnerText, node1.SelectSingleNode("level1ID").InnerText);
                                            tn1.ToolTip = node1.SelectSingleNode("level1ID").InnerText;
                                            tn1.NavigateUrl = @"javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');";
                                            if (node1.SelectSingleNode("level1ID").InnerText.Equals(selectedLoc))
                                                tn1.Checked = true;
                                            tn2.ChildNodes.Add(tn1);
                                            ListItem li = new ListItem("<a href=\"javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');\">" + node1.SelectSingleNode("level1Name").InnerText + "</a>", node1.SelectSingleNode("level1ID").InnerText);
                                            lstRoomSelection.Items.Add(li);
                                        }
                                    }
                                }
                                if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                                    if (Session["roomExpandLevel"] != null)//Location Issues//Organization Module Fixes
                                    {
                                        if (Session["roomExpandLevel"].ToString() != "")
                                        {
                                            if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 3)//Organization Module Fixes
                                                tn2.Expanded = true;
                                        }
                                    }
                                    else
                                        tn2.Expanded = false;
                            }
                        }
                    }
                    treeRoomSelection.Nodes.Add(tnTop);
                }
                else
                    tnTop = new TreeNode("You have no rooms available", "-1");
                //FB Case 1056 - Saima starts here 
                for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                    for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                        if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                        {
                            ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                            lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                            log.Trace(i + " : " + lstRoomSelection.Items[i].Text.Substring(lstRoomSelection.Items[i].Text.IndexOf(">")) + " : " + lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[j].Value = liTemp.Value;
                            lstRoomSelection.Items[j].Text = liTemp.Text;
                        }
                //FB Case 1056 - Saima ends here 

                //fogbugz case 466: Saima starts here
                if (Session["roomExpandLevel"].ToString().ToUpper().Equals("LIST"))//Organization Module Fixes
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
                //fogbugz case 466: Saima ends here
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void ValidateIPAddress(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                String patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                String patternISDN = @"[1-9]\d{3,21}"; //[1-9][0-9]\d{3,9}
                String patternMPI = @"^[0-9A-Za-z]+"; //[1-9][0-9]\d{3,9}
                Regex checkIP = new Regex(patternIP);
                Regex checkISDN = new Regex(patternISDN);
                Regex checkMPI = new Regex(patternMPI);
                bool valid = true;
                if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP))
                {
                    valid = checkIP.IsMatch(txtIPISDNAddress.Text, 0);
                    cusVal1.Text = obj.GetTranslatedText("Invalid IP Address");//FB 1830 - Translation
                }
                else if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                {
                    valid = checkISDN.IsMatch(txtIPISDNAddress.Text, 0);
                    cusVal1.Text = obj.GetTranslatedText("Invalid ISDN Address");//FB 1830 - Translation
                }
                else if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                {
                    valid = checkISDN.IsMatch(txtIPISDNAddress.Text, 0);
                    cusVal1.Text = obj.GetTranslatedText("Invalid MPI Address");//FB 1830 - Translation
                    if (lstMCU.SelectedValue.Equals("-1"))
                    {
                        valid = false;
                        cusVal1.Text = obj.GetTranslatedText("MCU is mandatory for MPI protocol");//FB 1830 - Translation
                    }
                }
                if (lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) && (!lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI) || lstMCU.SelectedValue.Equals("-1")))
                {
                    valid = false;
                    cusVal1.Text = obj.GetTranslatedText("Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI protocol.");//FB 1830 - Translation
                }
                if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI) && !lstMCU.SelectedValue.Equals("-1"))
                    if (!IsValidBridge(lstMCU.SelectedValue))
                    {
                        cusVal1.Text = obj.GetTranslatedText("Selected bridge for this template does not support MPI calls.");//FB 1830 - Translation
                        valid = false;
                    }

                //Response.Write(valid);
                //Response.End();
                e.IsValid = valid;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected bool IsValidBridge(String BridgeID)
        {
            try
            {
                Session["multisiloOrganizationID"] = null; //FB 2274
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeID>" + BridgeID + "</bridgeID>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    int bridgeType = Int32.Parse(xmldoc.SelectSingleNode("//bridge/bridgeType").InnerText);
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridge/bridgeTypes/type");
                    //Response.Write(bridgeType + " : " + nodes.Count + " : " + nodes[bridgeType - 1].SelectSingleNode("interfaceType").InnerText);
                    if (nodes[bridgeType - 1].SelectSingleNode("interfaceType").InnerText.Equals(ns_MyVRMNet.vrmMCUInterfaceType.Polycom))
                        return true;
                    //Response.End();
                }
                return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }

        protected void SubmitNew(Object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                    if (CreateNewTemplate())
                    {
                        Session.Add("UTID", "new");
                        Response.Redirect("EditUserTemplate.aspx?m=1");
                    }
            }
            catch (Exception ex)
            {
                log.Trace("SubmitNew:Error creating user template." + ex.Message);//FB 1881
                //errLabel.Text = "Error creating user template. Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        protected void SubmitOnly(Object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                    if (CreateNewTemplate())
                        Response.Redirect("ManageUserTemplatesList.aspx?m=1");
            }
            catch (Exception ex)
            {
                log.Trace("SubmitOnly:Error creating user template." + ex.Message);//FB 1881
                //errLabel.Text = "Error creating user template. Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        protected Boolean CreateNewTemplate()
        {
            try
            {
                String inXML = "";
                inXML += "<SetUserTemplate>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <login>" + Session["userID"].ToString() + "</login>";
                inXML += "  <UserTemplate>";
                inXML += "      <UserTemplateID>" + Session["UTID"].ToString() + "</UserTemplateID>";
                inXML += "      <TemplateName>" + txtTemplateName.Text + "</TemplateName>";
                inXML += "      <InitialTime>" + txtInitialTime.Text + "</InitialTime>";
                inXML += "      <VideoProtocol>" + lstProtocol.SelectedValue + "</VideoProtocol>";
                inXML += "      <IPISDNAddress>" + txtIPISDNAddress.Text + "</IPISDNAddress>";
                inXML += "      <ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                inXML += "      <RoleID>" + lstUserRole.SelectedValue + "</RoleID>";
                inXML += "      <Location>";
                //foreach (TreeNode tnSel in treeRoomSelection.CheckedNodes) code changed for room search
                inXML += selectedloc.Value; //tnSel.Value.Trim();
                inXML += "</Location>";
                inXML += "      <LanguageID>" + lstLanguage.SelectedValue + "</LanguageID>"; //FB 1830
                inXML += "      <LineRateID>" + lstLineRate.SelectedValue + "</LineRateID>";
                inXML += "      <BridgeID>" + lstMCU.SelectedValue + "</BridgeID>";
                inXML += "      <DepartmentID>" + lstDepartment.SelectedValue + "</DepartmentID>";
                inXML += "      <GroupID>" + lstGroup.SelectedValue + "</GroupID>";
                //Code changed by Offshore for FB Issue 1073 -- Start
                inXML += "      <ExpirationDate>" + myVRMNet.NETFunctions.GetDefaultDate(txtAccountExpiry.Text) + "</ExpirationDate>";
                //Code changed by Offshore for FB Issue 1073 -- Start
                inXML += "      <TimeZoneID>" + lstTimeZone.SelectedValue + "</TimeZoneID>";
                inXML += "      <EquipmentID>" + lstEquipment.SelectedValue + "</EquipmentID>";
                if (chkEmailNotification.Checked)
                    inXML += "      <EmailNotification>1</EmailNotification>";
                else
                    inXML += "      <EmailNotification>0</EmailNotification>";
                if (chkOutsideNetwork.Checked)
                    inXML += "      <OutsideNetwork>1</OutsideNetwork>";
                else
                    inXML += "      <OutsideNetwork>0</OutsideNetwork>";
                inXML += "  </UserTemplate>";
                inXML += "</SetUserTemplate>";
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                String outXML = obj.CallMyVRMServer("SetUserTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    btnSubmit.Visible = true; //FB 481 Saima
                    btnSubmitNew.Visible = true; //FB 481 Saima
                    return false;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                log.Trace("CreateNewTemplate:Error creating user template." + ex.Message);//FB 1881
                //errLabel.Text = "Error creating user template. Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
                return false;
            }
        }

        protected void LoadTimeZones(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstTimeZone.DataSource = dt;
                    lstTimeZone.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadLanguage(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstLanguage.DataSource = dt;
                    lstLanguage.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadLineRate(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstLineRate.DataSource = dt;
                    lstLineRate.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadBridges(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstMCU.DataSource = dt;
                    lstMCU.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void LoadUserRoles(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstUserRole.DataSource = dt;
                    lstUserRole.DataBind();
                    /* *** Code added for FB 1425 QA Bug -Start *** */

                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        lstUserRole.Items.FindByValue("2").Text = "Hearing Administrator";
                       
                    }

                    if (Session["UsrCrossAccess"] != null)
                    {
                        if (Session["UsrCrossAccess"].ToString() != "1" || Session["organizationID"].ToString() != "11")
                        {
                            if (lstUserRole.Items.FindByText("Site Administrator") != null)
                                lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Site Administrator"));
                        }
                    }
                    /* *** Code added for FB 1425 QA Bug -End *** */
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        protected void GetGroups(Object sender, EventArgs e)
        {
            try
            {
                Session["multisiloOrganizationID"] = null; //FB 2274
                //FB 2027 - Starts
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><groupID></groupID><sortBy>1</sortBy></login>";
                //String outXML = obj.CallCOM("GetGroup", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetGroup", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//groups/group");
                GetGroupDetails(nodes);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void GetGroupDetails(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstGroup.DataSource = dt;
                    lstGroup.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }

        protected void LoadEquipment(Object sender, EventArgs e)
        {
            try
            {
                //FB 2027 start
                StringBuilder inXML = new StringBuilder();
                //String inXML = "";
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</login>");
                String outXML = obj.CallMyVRMServer("GetUserPreference", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = obj.CallCOM("GetUserPreference", inXML, Application["COM_ConfigPath"].ToString());
                //FB 2027 end
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//userPreference/videoEquipment/equipment");
                    LoadEquipmentDetails(nodes);
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadEquipmentDetails(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    lstEquipment.Items.Clear();
                    DataTable dt = ds.Tables[0];
                    lstEquipment.DataSource = dt;
                    lstEquipment.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadDepartments(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetManageDepartment", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//getManageDepartment/departments/department");
                    LoadDepartmentDetails(nodes);
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadDepartmentDetails(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                lstDepartment.Items.Clear();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    lstDepartment.Items.Clear();
                    DataTable dt = ds.Tables[0];
                    lstDepartment.DataSource = dt;
                    lstDepartment.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdSelView.SelectedIndex.Equals(1))
                {
                    pnlListView.Visible = true;
                    pnlLevelView.Visible = false;
                    lstRoomSelection.ClearSelection();
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                            if (tn.Value.Equals(lstItem.Value) && (tn.Depth.Equals(3)))
                                lstItem.Selected = true;
                    }
                }
                else
                {
                    pnlLevelView.Visible = true;
                    pnlListView.Visible = false;
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                tn.Checked = false;
                                foreach (ListItem lstItem in lstRoomSelection.Items)
                                    if ((lstItem.Selected == true) && (tn.Value.Equals(lstItem.Value)))
                                        tn.Checked = true;
                            }
                }
            }
            catch (Exception ex)
            {
                log.Trace("rdSelView_SelectedIndexChanged:Error changing view." + ex.Message);//FB 1881
                //errLabel.Text = "Error changing view. Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        protected void ResetTemplate(Object sender, EventArgs e)
        {
            Response.Redirect("EditUserTemplate.aspx"); //BindData(); //fogbugz case 229
        }


        #region Bind to Rooms

        public void BindRoomToList()
        {
            String[] locsName = null;
            try
            {
                if (locstrname.Value != "")
                {

                    locsName = locstrname.Value.Split('+');
                    RoomList.Items.Clear();
                    foreach (String s in locsName)
                    {
                        if (s != "")
                        {

                            if (s.Split('|').Length > 1)
                                RoomList.Items.Add(new ListItem(s.Split('|')[1], s.Split('|')[0]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(obj.ShowSystemMessage());//ZD 100263
            }
        }
        #endregion

    }



}