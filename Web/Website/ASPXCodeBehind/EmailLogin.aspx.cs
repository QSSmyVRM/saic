/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_EmailLogin : System.Web.UI.Page
{
        
    #region Protected Data members
    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.WebControls.Label LblMessage;
    protected System.Web.UI.WebControls.TextBox TxtEmail;
    protected System.Web.UI.WebControls.Button BtnSubmit;
    protected System.Web.UI.HtmlControls.HtmlForm frmEmailLogin;
    #endregion

    #region Private Data Members

    private String inXML = "";
    private ns_Logger.Logger log = null;
    private myVRMNet.NETFunctions obj;


    public en_EmailLogin()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
        }

    #endregion    

    #region Page Load
    /// <summary>
    /// Page Load Event Handler 
    /// </summary>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("emaillogin.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            //obj = new myVRMNet.NETFunctions();
            

            this.IntializeUIResources();
            this.CheckUserSession();

            LblMessage.Visible = false;
            LblMessage.Text = "";

            if (Session["maillogin"] != null)//ZD 100263
            {
				//ZD 100170 - Start
                if (Session["mailExtn"] == null)
                    Session["mailExtn"] = "";
                if (Session["mailExtn"].ToString().Equals(""))
                {
                    String strThkYou = "You will receive the password, If you enter the correct email address.";
                    LblMessage.Visible = true;
                    LblMessage.Text = strThkYou;
                    LblMessage.Attributes.Add("Style", "Color:Green");
                }
                else if (Session["maillogin"].ToString().Equals("s")) //ZD 100170 - End //ZD 100263
                {
                    //String strThkYou = "Thank you. Your request was successful. Your password will be sent to the email account.";//ZD 100263
                    String strThkYou = "You will receive the password, If you enter the correct email address.";//ZD 100263
                    LblMessage.Visible = true;
                    LblMessage.Text = strThkYou;
                    LblMessage.Attributes.Add("Style", "Color:Green");
                }
                else if (Session["maillogin"].ToString().Equals("e")) //ZD 100263
                {
                    //String strSorry = "Sorry, there is no record of this email. Please check the email address you have entered and try again.";//ZD 100263
                    String strSorry = "You will receive the password, If you enter the correct email address.";//ZD 100263
                    LblMessage.Visible = true;
                    LblMessage.Attributes.Add("Style", "Color:Green");//ZD 100263
                    LblMessage.Text = strSorry;
                }
            }

        }
        catch (Exception ex)
        {
            log = new ns_Logger.Logger();
            LblError.Visible = true;
            //ZD 100263
            log.Trace("Page_Load" + ex.Message);
            LblError.Text = obj.ShowSystemMessage();
            //LblError.Text = "PageLoad: " + ex.Message;
            //log.Trace(ex.StackTrace + " : " + ex.Message);
            log = null;
        }
    }
    #endregion    

    #region IntializeUIResources
    /// <summary>
    /// Intialize all Event Handlers used in this Screen
    /// </summary>

    private void IntializeUIResources()
    {
        BtnSubmit.Click += new EventHandler(BtnSubmit_Click);
    }

    #endregion    

    #region CheckUserSession
    // <summary>
    /// Checks the User Session and sets application path.
    /// </summary>

    private void CheckUserSession()
    {
        if (Session["userID"] == null)
        {
            Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
            Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
        }
    }

    #endregion    

    #region Event Handlers Methods
    /// <summary>
    ///  Event Handlers Method - To Check email Id  with the myvrm account details
    /// </summary>

    private void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            BuildInXML();
            GetOutXML(inXML);
        }
        catch (System.Threading.ThreadAbortException)
        {

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Submitting" + ex.Message);
            LblError.Text = obj.ShowSystemMessage();
            //LblError.Text = "Submitting: " + ex.Message;
            LblError.Visible = true;
        }

    }

    #endregion    

    #region User Defined Methods
    /// <summary>
    /// User Defined Methods
    /// </summary>
    
    #region BuildInXML
    /// <summary>
    /// User Defined Method - Building InPut XML,which sends to COM
    /// </summary>

    private void BuildInXML()
    {
        inXML += "<login>";
        if (Session["organizationID"] == null) //Organization Module Fixes
            Session["organizationID"] = "11";
        inXML += obj.OrgXMLElement();//Organization Module Fixes
        inXML += "<emailLoginInfo>";
        inXML += "<email>" + TxtEmail.Text.ToString() + "</email>";
        inXML += "</emailLoginInfo>";
        inXML += "</login>";
    }

    #endregion

    #region GetOutXML
    /// <summary>
    /// User Defined Method - Retrieving OutPut XML  from COM
    /// </summary>

    private void GetOutXML(String inXML)
    {
        String cmd = "RequestPassword";   //FB 1830
        String outXML = obj.CallCommand(cmd, inXML);

        if (outXML.IndexOf("<error>") < 0)
        {
            Session["outXML"] = "";
            Session["maillogin"] ="s";//ZD 100263
            Response.Redirect("EmailLogin.aspx");
        }
        else if (outXML.IndexOf("<error>-1</error>") >= 0)
        {
            Session["outXML"] = "";
            Session["maillogin"] = "e";//ZD 100263
            Response.Redirect("EmailLogin.aspx");
        }
        //obj = null;
    }
    #endregion

    #endregion
}
