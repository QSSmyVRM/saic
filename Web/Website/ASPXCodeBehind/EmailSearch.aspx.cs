/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

    public partial class en_EmailSearch : System.Web.UI.Page
    {

        #region Data Members
        
        protected String sfname = "";
        protected String slname = "";
        protected String sgname = "";
        protected String slogname = "";
        protected String frm = "";
        protected Int32 pn = Int32.MinValue;
        protected String fn = "";
        StringBuilder inXML = new StringBuilder();//FB 2027

        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log;


        protected System.Web.UI.HtmlControls.HtmlInputText LoginName;
        //FB 1888
        protected System.Web.UI.WebControls.TextBox FirstName;
        protected System.Web.UI.WebControls.TextBox LastName;


        #endregion


        #region

        public en_EmailSearch()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion


        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("emailsearch.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                SetQueryStrings();        
            }
            catch (Exception ex)
            {
                log.Trace( "Email Search Page Load:" + ex.StackTrace + ex.Message);
            }
        }
        #endregion

        #region SetQueryStrings
        /// <summary>
        /// GetQueryStrings
        /// </summary>
        private void SetQueryStrings()
        {

            try
            {

                if (Request.Form["FirstName"] == "")
                {
                    if (Request.QueryString["fname"] != null)
                        sfname = Request.QueryString["fname"].ToString();
                }
                else
                {
                    sfname = Request.Form["FirstName"];
                }
                if (Request.Form["LastName"] == "")
                {
                    if (Request.QueryString["lname"] != null)
                        slname = Request.QueryString["lname"];
                }
                else
                {
                    slname = Request.Form["LastName"];
                }
                if (Request.Form["LoginName"] == "")
                {
                    if (Request.QueryString["gname"] != null)
                        slogname = Request.QueryString["gname"];
                }
                else
                {
                    slogname = Request.Form["LoginName"];
                }
                
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                // log trace
                log.Trace(ex.StackTrace + ex.Message);

            }
        }

        #endregion

        #region BindData
        /// <summary>
        /// GetQueryStrings
        /// </summary>
        private void BindData()
        {
            String outXML = "";
            try
            {
                //FB 2027 Starts
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<name>");
                inXML.Append("<firstName>" + sfname + "</firstName>");
                inXML.Append("<lastName>" + slname + "</lastName>");
                inXML.Append("<userName>" + slogname + "</userName>");
                inXML.Append("</name>");

                inXML.Append("<pageNo>");
                if (Request.QueryString["pn"] != null)
                {
                    if (Request.QueryString["pn"] == "")
                    {
                        pn = 1;
                    }
                    else
                    {
                        pn = Int32.Parse(Request.QueryString["pn"].ToString());
                    }
                }
                inXML.Append(pn + "</pageNo>");

                inXML.Append("</login>");

                if (Request.Form["t"] != null)
                {
                    if (Request.Form["t"].ToString() == "g" || Request.QueryString["t"].ToString() == "g")
                        outXML = obj.CallMyVRMServer("RetrieveGuest", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                    else
                        outXML = obj.CallMyVRMServer("RetrieveUsers", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                }
                //FB 2027 Ends
                //if (Request.QueryString["frm"] != null)
                //{
                //    if (Request.QueryString["frm"] == "")
                //    {
                //        frm = Request.Form["frm"].ToString();
                //    }
                //}
                //else
                //{
                //    frm = Request.QueryString["frm"].ToString();
                //}
                if (Request.Form["fn"] == ",")
                {
                    fn = "";
                }
                else
                {
                    fn = Request.Form["fn"].ToString();
                }
                

                //Code Modified For Address LookUp - changed emaillist2.aspx, emaillist2main.aspx to emaillist2Main.aspx - FB Issue 412- Start
                if (Convert.ToInt32(Request.QueryString["pn"]) > 0)
                {
                    //Response.Redirect("emaillist2.aspx?t=" + Request.Form["t"].ToString() + "&frm=" + frm + "&srch=y&fname=" + sfname + "&lname=" + slname + "&gname=" + slogname + "&fn=" + Request.Form["fn"].ToString() + "&n=" + Request.Form["n"].ToString());
                    Response.Redirect("emaillist2.aspx?t=" + Request.QueryString["t"].ToString() + "&frm=" + frm + "&srch=y&fname=" + sfname + "&lname=" + slname + "&gname=" + slogname + "&fn=" + Request.QueryString["fn"].ToString() + "&n=" + Request.Form["n"].ToString());  //FB 1627
                }
                else
                {

                    //Response.Redirect("emaillist2main.aspx?t=" + Request.Form["t"] + "&frm=" + frm + "&srch=y&fname=" + sfname + "&lname=" + slname + "&gname=" + slogname + "&fn=" + Request.Form["fn"] + "&n=" + Request.Form["n"].ToString());
                    Response.Redirect("emaillist2main.aspx?t=" + Request.QueryString["t"].ToString() + "&frm=" + Request.QueryString["frm"].ToString() + "&srch=y&fname=" + sfname + "&lname=" + slname + "&gname=" + slogname + "&fn=" + fn + "&n=" + Request.Form["n"].ToString()); //FB 1627
                }

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                // log trace
                log.Trace(ex.StackTrace + ex.Message);

            }
        }

        #endregion

        #region SearchUsers
        protected void SearchUsers(Object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("Submit User" + ex.StackTrace + ex.Message);
            }
        }
        #endregion

    }
