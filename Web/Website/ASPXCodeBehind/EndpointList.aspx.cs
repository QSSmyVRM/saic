/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_EndpointList
{
    public partial class EndpointList : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblSubHeader1;
        protected System.Web.UI.WebControls.Label lblNoEndpoints;
        protected System.Web.UI.WebControls.Label lblAddSelection;

        protected System.Web.UI.HtmlControls.HtmlTableRow trNew;

        protected System.Web.UI.WebControls.RadioButton rdNewEndpoint;

        protected System.Web.UI.WebControls.Button btnSubmit;

        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstVideoProtocol;

        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtEndpointID;
        protected System.Web.UI.WebControls.DataGrid dgEndpointList;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.HtmlControls.HtmlControl EndpointFrame;//Code added for Endpoint Search
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        private String DrpValue = "";  //Endpoint Search Fix

        protected int Cloud = 0; //FB 2262 //FB 2599

        public EndpointList()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("endpointlist.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
               
                //Code added for Endpoint Search -- Start
                String strSrc = "";
                if (Request.QueryString["t"].ToUpper().Equals("TC"))
                {
                    strSrc = "EndpointSearch.aspx?t=TC&DrpValue=" + DrpValue;
                }
                else
                {
                    strSrc = "EndpointSearch.aspx?DrpValue=" + DrpValue;
                }

                EndpointFrame.Attributes.Add("src", strSrc);
                Session.Remove("EndpointXML");

                if (Request.QueryString["DrpValue"] != null)
                    if (Request.QueryString["DrpValue"].ToString().Trim() != "")
                    {
                        DrpValue = Request.QueryString["DrpValue"].ToString().Trim();
                    }

                //Code added for Endpoint Search -- End
                //FB 2599 Start //FB 2717 Vidyo Integration Start
                /*if (Session["Cloud"] != null) //FB 2262
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }


                if (Cloud == 1) //FB 2262
                {
                    btnSubmit.Visible = false;
                }*/
				//FB 2717 Vidyo Integration End
                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray;// FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnSubmit.Visible = false;
                }
                // FB 2796 Start
                else
                    btnSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796
                // FB 2796 End

                //FB 2599 End
                errLabel.Visible = false;
                //Response.Write("here");
                //Response.Write(Session["userEmail"].ToString());
                if (Request.QueryString["t"].ToUpper().Equals("TC"))
                {
                    dgEndpointList.Columns[1].Visible = true;
                    dgEndpointList.Columns[dgEndpointList.Columns.Count - 1].Visible = false;
                    dgEndpointList.Columns[dgEndpointList.Columns.Count - 4].Visible = true;
                    //lblAddSelection.Visible = true;
                    //trNew.Visible = false;     //Code comented for endpoint search
                    //btnSubmit.Visible = false;
                }
                if (!IsPostBack)
                {
                    obj.BindAddressType(lstAddressType);
                    obj.BindBridges(lstBridges);
                    obj.BindVideoEquipment(lstVideoEquipment);
                    obj.BindLineRate(lstLineRate);
                    obj.BindVideoProtocols(lstVideoProtocol);
                    obj.BindDialingOptions(lstConnectionType); 
                    BindData();
                }
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "PageLoad: " + ex.StackTrace;
            }
        }

        protected void BindData()
        {
            try
            {
                int pageNo = 1;
                string endPointName = "";
                string endPointType = "";
                string qString = Request.QueryString.ToString();
                qString = "";
                if (Request.QueryString["pageNo"] != null)
                    pageNo = Int32.Parse(Request.QueryString["pageNo"].ToString());
                if ((txtEndpointName.Text != "") || (!lstAddressType.SelectedValue.Equals("-1")))
                    pageNo = 1;

                //This code is added to fix the bug no 445
                if (Request.QueryString["endPointName"] != null)
                {
                  endPointName = Request.QueryString["endPointName"].ToString();
                }
                if(txtEndpointName.Text != "")
                    endPointName = txtEndpointName.Text;

                if (Request.QueryString["endPointType"] != null)
                {
                    endPointType = Request.QueryString["endPointType"].ToString();
                }
                if (!lstAddressType.SelectedValue.Equals("-1"))
                    endPointType = lstAddressType.Text;
                else
                    endPointType = "";
                //end
                //FB 2594 Starts
                string isPublicEP = "0";
                if (Session["EnablePublicRooms"].ToString() == "1")
                    isPublicEP = "1";
                //FB 2594 Ends
               String inXML = "";
               
                   inXML += "<SearchEndpoint>";
                   inXML += obj.OrgXMLElement();//Organization Module Fixes
                   inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                   //inXML += "  <EndpointName>" + txtEndpointName.Text + "</EndpointName>";
                   //This code is added to fix the bug no 445
                   inXML += "  <EndpointName>" + endPointName + "</EndpointName>";
                  // if (!lstAddressType.SelectedValue.Equals("-1"))
                    //   inXML += "  <EndpointType>" + lstAddressType.Text + "</EndpointType>";
                   //else
                     //  inXML += "  <EndpointType></EndpointType>";
                   //This code is added to fix the bug no 445
                   inXML += "  <EndpointType>" + endPointType + "</EndpointType>";
                   inXML += "  <PublicEndpoint>" + isPublicEP + "</PublicEndpoint>"; //FB 2594
                   inXML += "  <PageNo>" + pageNo + "</PageNo>";
                   inXML += "</SearchEndpoint>";
               
                String outXML = obj.CallMyVRMServer("SearchEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SearchEndpoint/Endpoints/Endpoint");
                    
                    if (nodes.Count > 0)
                    {
                        LoadEndpointList(nodes);
                        foreach (DataGridItem dgi in dgEndpointList.Items)
                        {
                            
                            ((LinkButton)dgi.FindControl("btnViewBridgeDetails")).Attributes.Add("onclick", "javascript:ViewBridgeDetails('" + ((Label)dgi.FindControl("lblBridgeID")).Text + "');return false;");
                        }
                        lblNoEndpoints.Visible = false;
                        DataGridItem dgFooter = (DataGridItem)dgEndpointList.Controls[0].Controls[dgEndpointList.Controls[0].Controls.Count - 1];
                        RadioButton rdNewEndpoint = (RadioButton)dgFooter.FindControl("rdNewEndpoint");
                        
                        if (!Request.QueryString["t"].ToString().ToUpper().Equals("TC"))
                            rdNewEndpoint.Visible = false;
                        Label lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        Label lblTempLic = (Label)dgFooter.FindControl("lblRemaining"); //Added for License Modification

                        if (xmldoc.SelectNodes("//SearchEndpoint/Endpoints/Paging").Count > 0)
                        {
                            lblTemp.Text = xmldoc.SelectSingleNode("//SearchEndpoint/Endpoints/Paging/TotalRecords").InnerText;
                            //if (xmldoc.SelectSingleNode("//SearchEndpoint/Endpoints/Paging/licensesRemain").InnerText != "")//Added for License Modification
                            //lblTempLic.Text = xmldoc.SelectSingleNode("//SearchEndpoint/Endpoints/Paging/licensesRemain").InnerText;//Added for License Modification
                            if(lblTemp.Text != "" && Session["EndPoints"] != null)
                                lblTempLic.Text = (Int32.Parse(Session["EndPoints"].ToString()) - Int32.Parse(lblTemp.Text)).ToString(); //Added for License Modification
                            int totalPages = Int32.Parse(xmldoc.SelectSingleNode("//SearchEndpoint/Endpoints/Paging/TotalPages").InnerText);
                            pageNo = Int32.Parse(xmldoc.SelectSingleNode("//SearchEndpoint/Endpoints/Paging/PageNo").InnerText);
                           
                            qString = qString.Replace("t=" + Request.QueryString["t"], "");
                            if (Request.QueryString["pageNo"] != null)
                            {
                                qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                                
                            }
                            //This code is added to fix the bug no 445                       
                            if (endPointName != "")
                                qString = qString + "&endPointName=" + endPointName; 
                            if (endPointType != "")
                                 qString = qString + "&endPointType=" + lstAddressType.Text;
                            //end
                                                      
                            if (totalPages > 1)
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "EndpointList.aspx?t=" + Request.QueryString["t"] + qString);
                            else
                                tblPage.Visible = false;
                        }
                    }
                    else
                    {
                        tblPage.Visible = false;
                        lblNoEndpoints.Visible = true;
                        dgEndpointList.DataSource = null;
                        dgEndpointList.DataBind();
                        //Response.Write("here");
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Bind Data" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }

        protected void LoadEndpointList(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    //Response.Write(ds.Tables[0].Columns[0].ColumnName);
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (!dt.Columns.Contains("ID"))
                        dt.Columns.Add("ID");
                    if (!dt.Columns.Contains("ProfileID"))
                        dt.Columns.Add("ProfileID");
                    if (!dt.Columns.Contains("EndpointName"))
                        dt.Columns.Add("EndpointName");
                    if (!dt.Columns.Contains("RoomName")) //FB 1886,1552
                        dt.Columns.Add("RoomName");
                    if (!dt.Columns.Contains("DefaultProfileName"))
                        dt.Columns.Add("DefaultProfileName");
                    if (!dt.Columns.Contains("VideoEquipment"))
                        dt.Columns.Add("VideoEquipment");
                    if (!dt.Columns.Contains("AddressType"))
                        dt.Columns.Add("AddressType");
                    if (!dt.Columns.Contains("Address"))
                        dt.Columns.Add("Address");
                    if (!dt.Columns.Contains("IsOutside"))
                        dt.Columns.Add("IsOutside");
                    if (!dt.Columns.Contains("ConnectionType"))
                        dt.Columns.Add("ConnectionType");
                    if (!dt.Columns.Contains("LineRate"))
                        dt.Columns.Add("LineRate");
                    if (!dt.Columns.Contains("Bridge"))
                        dt.Columns.Add("Bridge");
                    if (!dt.Columns.Contains("BridgeName"))
                        dt.Columns.Add("BridgeName");
                    if (!dt.Columns.Contains("TotalProfiles"))
                        dt.Columns.Add("TotalProfiles");
                    if (!dt.Columns.Contains("DefaultProtocol"))
                        dt.Columns.Add("DefaultProtocol");
                    if (!dt.Columns.Contains("ProfilesXML"))
                        dt.Columns.Add("ProfilesXML");
                    if (!dt.Columns.Contains("GateKeeeperAddress"))//ZD 100132
                        dt.Columns.Add("GateKeeeperAddress");
                    foreach (DataRow dr in dt.Rows)
                    {
                        UpdateDataRow(dr);
                    }
                    dv.Sort = SortField;
                    if (!SortAscending)
                    {
                        // append "DESC" to the sort field name in order to 
                        // sort descending
                        dv.Sort += " DESC";
                    }

                    dgEndpointList.DataSource = dv;
                    dgEndpointList.DataBind();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadList" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "LoadList: " + ex.StackTrace;
            }
        }
        string SortField
        {

            get
            {
                object o = ViewState["SortField"];
                if (o == null)
                {
                    return String.Empty;
                }
                return (string)o;
            }

            set
            {
                if (value == SortField)
                {
                    //if ascending change to descending or vice versa.
                    SortAscending = !SortAscending;
                }
                ViewState["SortField"] = value;
            }
        }

        // using ViewState for SortAscending property
        bool SortAscending
        {

            get
            {
                object o = ViewState["SortAscending"];
                if (o == null)
                {
                    return true;
                }
                return (bool)o;
            }

            set
            {
                ViewState["SortAscending"] = value;
            }
        }
        protected void SortGrid(Object src, DataGridSortCommandEventArgs e)
        {

            dgEndpointList.CurrentPageIndex = 0;
            // get the requested sorting field name and set it to SortField
            SortField = e.SortExpression;
            BindData();
        }

        protected void UpdateDataRow(DataRow dr)
        {
            try
            {
                //Response.Write(dr["EndpointID"].ToString());
                String inXML = "";
                inXML += "<EndpointDetails>";
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndpointID>" + dr["ID"].ToString() + "</EndpointID>";
                inXML += "</EndpointDetails>";
                String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = outXML.Replace(" % ", "&lt;br/&gt;");//FB 1886
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    dr["ProfilesXML"] = outXML;
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                    dr["EndpointName"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText;
                    dr["RoomName"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/RoomName").InnerText; //FB 1886,1552
                    dr["TotalProfiles"] = nodes.Count;
                    dr["ProfileID"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText;
                    foreach (XmlNode node in nodes)
                    {
                        if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Equals(node.SelectSingleNode("ProfileID").InnerText))
                        {
                            //Response.Write("<br>" +node.SelectSingleNode("VideoEquipment").InnerText);
                            try
                            {
                                dr["VideoEquipment"] = lstVideoEquipment.Items.FindByValue(node.SelectSingleNode("VideoEquipment").InnerText.Trim()).Text;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            try
                            {
                                String connType = node.SelectSingleNode("ConnectionType").InnerText.Trim();
                                if (connType.Equals("0"))
                                    connType = "2";
                                dr["ConnectionType"] = lstConnectionType.Items.FindByValue(connType).Text;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            try
                            {
                                if (!node.SelectSingleNode("LineRate").InnerText.Equals("-1"))
                                    dr["LineRate"] = lstLineRate.Items.FindByValue(node.SelectSingleNode("LineRate").InnerText.Trim()).Text;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            try
                            {
                                dr["AddressType"] = lstAddressType.Items.FindByValue(node.SelectSingleNode("AddressType").InnerText.Trim()).Text;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            try
                            {
                                dr["Bridge"] = node.SelectSingleNode("Bridge").InnerText;
                                if (!node.SelectSingleNode("Bridge").InnerText.Equals("-1"))
                                    dr["BridgeName"] = lstBridges.Items.FindByValue(node.SelectSingleNode("Bridge").InnerText.Trim()).Text;
                            }
                            catch (Exception ex)
                            {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            try
                            {
                                if (!node.SelectSingleNode("DefaultProtocol").InnerText.Equals("-1"))
                                    dr["DefaultProtocol"] = lstVideoProtocol.Items.FindByValue(node.SelectSingleNode("DefaultProtocol").InnerText.Trim()).Text;
                            }
                            catch (Exception ex) {
                                log.Trace(ex.Message + " : " + ex.StackTrace);
                            }
                            dr["Address"] = node.SelectSingleNode("Address").InnerText.Trim();
                            if (node.SelectSingleNode("IsOutside").InnerText.Trim().Equals("1"))
                                dr["IsOutside"] = "Yes";
                            else
                                dr["IsOutside"] = "No";
                            dr["DefaultProfileName"] = node.SelectSingleNode("ProfileName").InnerText.Trim();
                        }
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("UpdateRow" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "UpdateRow: " + ex.StackTrace + " : " + ex.Message;
            }
        }
        protected void EditEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("EndpointID", e.Item.Cells[0].Text);
                if (Session["ProfileID"] != null)
                    Session["ProfileID"] = "";
                Response.Redirect("EditEndpoint.aspx?t=");
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("EditEndpoint" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void DeleteEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<DeleteEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndPointID>" + e.Item.Cells[0].Text + "</EndPointID>";
                inXML += "</DeleteEndpoint>";
                String outXML = obj.CallMyVRMServer("DeleteEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    BindData();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteEndpoint" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void SearchEndpoint(Object sender, EventArgs e)
        {
            try
            {
                lblSubHeader1.Text = obj.GetTranslatedText("Search Results");//FB 1830 - Translation
                //Response.Redirect("EndpointList.aspx?m=1");
                BindData();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SearchEndpoint" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void CreateNewEndpoint(Object sender, EventArgs e)
        {
            try
            {
                if (Session["EndpointID"] == null)
                    Session.Add("EndpointID", "new");
                else
                    Session["EndpointID"] = "new";
                if (Session["ProfileID"] != null)
                    Session["ProfileID"] = "";
                if (Request.QueryString["t"].ToUpper().Equals("TC"))
                {
                    // // DataGridItem dgFooter = (DataGridItem)dgEndpointList.Controls[0].Controls[dgEndpointList.Controls[0].Controls.Count - 1];
                    // // RadioButton rdNewEndpoint = (RadioButton)dgFooter.FindControl("rdNewEndpoint");
                    // // Button btnSubmit2 = (Button)dgFooter.FindControl("btnSubmitNew");
                    ////  btnSubmit2.Visible = true;
                    //  rdNewEndpoint.Visible = true;
                    // // if (rdNewEndpoint.Checked)
                    //      txtEndpointID.Text = "new";
                    //  else
                    //  {
                    //      foreach (DataGridItem dgi in dgEndpointList.Items)
                    //      {
                    //          if (((RadioButton)dgi.FindControl("rdSelectEndpoint")).Checked)
                    //          {
                    //              txtEndpointID.Text = dgi.Cells[0].Text;
                    //              Session.Add("ProfileID", ((DropDownList)dgi.FindControl("lstProfiles")).SelectedValue);
                    //          }
                    //      }
                    //  }
                    //  Session["EndpointID"] = txtEndpointID.Text;
                    //Session.Add("EndpointID", "new");
                    txtEndpointID.Text = "new";
                    Response.Redirect("AddTerminalEndpoint.aspx?t=TC&epid=" + txtEndpointID.Text + "&cid=" + Session["ConfID"] + "&tpe=U");
                }
                else
                {
                    Response.Redirect("EditEndpoint.aspx?t=");
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CreateNewEndpoint" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this endpoint?") + "')");//FB japnese
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindRowsDeleteMessage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void LoadProfiles(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(((TextBox)e.Item.FindControl("txtProfilesXML")).Text);
                    String adds = "";//FB 2602
                    XmlNodeList multicode = null;
                    XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                    foreach (XmlNode node in nodes)
                    {
                        //FB 2602 - Start
                        adds = "";
                        if (node.SelectNodes("//MultiCodec/Address") != null)
                        {
                            multicode = node.SelectNodes("MultiCodec/Address");
                            for (int i = 0; i < multicode.Count; i++)
                            {
                                if (multicode[i] != null)
                                    if (multicode[i].InnerText.Trim() != "")
                                    {
                                        if (i > 0)
                                            adds += "�";

                                        adds += multicode[i].InnerText;
                                    }
                            }
                            if (multicode.Count > 0)
                                node.SelectSingleNode("MultiCodec").InnerText = adds;
                        }
                        //FB 2602 - End
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    DataTable dt = new DataTable();
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                        ((DropDownList)e.Item.FindControl("lstProfiles")).DataSource = dt;
                        ((DropDownList)e.Item.FindControl("lstProfiles")).DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadProfiles" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
    }
}