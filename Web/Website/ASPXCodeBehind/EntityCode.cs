/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace ns_EntityCode
{
    public partial class EntityCode : System.Web.UI.Page
    {
        #region protected Data Members 

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtEntityID;
        protected System.Web.UI.WebControls.TextBox txtEntityName;
        protected System.Web.UI.WebControls.TextBox txtEntityDesc;
        protected System.Web.UI.WebControls.Button btnAddEntityCode;
        protected System.Web.UI.WebControls.DataGrid dgEntityCode;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;

        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        ns_Logger.Logger log = null;//ZD 100263
        #endregion

        #region EntityCode 

        public EntityCode()
        {
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger(); //ZD 100263
        }

        #endregion

        #region Methods Executed on Page Load 

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("entitycode.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                

                if (!IsPostBack)
                {

                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].Equals("1"))
                        {
                            errLabel.Visible = true;
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        }
                    string inXML = "<GetEntityCode>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID><CustomAttributeID>1</CustomAttributeID><OptionType>6</OptionType></GetEntityCode>";//Organization Module Fixes
                   /* string outXML="<EntityCode><OptionList><Option><CustomAttributeID>1</CustomAttributeID><Type>6</Type><OptionID>0</OptionID><DisplayValue>0</DisplayValue><DisplayCaption>textbox1</DisplayCaption><HelpText>This is Textbox1</HelpText></Option><Option><CustomAttributeID>1</CustomAttributeID><Type>6</Type><OptionID>1</OptionID> <DisplayValue>0</DisplayValue><DisplayCaption>Test</DisplayCaption><HelpText>This is Test</HelpText></Option><Option><CustomAttributeID>1</CustomAttributeID>";
                   outXML = outXML + "<Type>6</Type><OptionID>2</OptionID><DisplayValue>0</DisplayValue><DisplayCaption>Test 1</DisplayCaption><HelpText>This is Test1</HelpText></Option></OptionList></EntityCode>";*/

                    string outXML = obj.CallMyVRMServer("GetCustomAttributes", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                    BindData(outXML);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad " + ex.StackTrace;
            }

        }

        #endregion

        #region BindData 

        private void BindData(string outXML)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlNodeList nodes = xmldoc.SelectNodes("//OptionList/Option");
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (Session["dtEntityCode"] == null)
                        Session.Add("dtEntityCode", dt);
                    else
                        Session["dtEntityCode"] = dt;
                    
                    hdnValue.Value = dt.Rows.Count.ToString();

                    dgEntityCode.DataSource = dt;
                    dgEntityCode.DataBind();
                }
            }

            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region EditItem 

        protected void EditItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtEntityID.Text = e.Item.Cells[0].Text;
                if (ViewState["EntityID"] == null)
                    ViewState.Add("EntityID", e.Item.Cells[0].Text);
                else
                    ViewState["EntityID"] = e.Item.Cells[0].Text;

                dgEntityCode.EditItemIndex = e.Item.ItemIndex;

                dgEntityCode.DataSource = Session["dtEntityCode"];
                dgEntityCode.DataBind();
                btnAddEntityCode.Enabled = false;

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("EditItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }

        #endregion

        #region UpdateItem 
        protected void UpdateItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtEntityName.Text = ((TextBox)e.Item.FindControl("txtEntityName")).Text.Trim();
                txtEntityDesc.Text = ((TextBox)e.Item.FindControl("txtEntityDesc")).Text.Trim();
                if (txtEntityDesc.Text.Length > 35)
                {
                    errLabel.Text = obj.GetTranslatedText("Billing Code description should be within 35 characters"); //Entity Code Changed //FB 1830 - Translation
                    errLabel.Visible = true;
                    txtEntityName.Text = "";    //Code added for Entity Code Changes
                    txtEntityDesc.Text = "";    //Code added for Entity Code Changes
                    return;
                }
                UpdateEntityCode(sender, (EventArgs)e);
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("UpdateItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }
        
        #endregion 

        #region CancelItem 
        protected void CancelItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtEntityID.Text = "new";
                dgEntityCode.EditItemIndex = -1;
                dgEntityCode.DataSource = Session["dtEntityCode"];
                dgEntityCode.DataBind();
                btnAddEntityCode.Enabled = true;
                errLabel.Visible = false;   //Code added for Entity Code Changes
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CancelItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }
        #endregion

        #region DeleteItem 
        protected void DeleteItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<DeleteEntityCode>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
		        inXML += "  <CustomAttributeID>1</CustomAttributeID>";
		        inXML += "  <OptionType>6</OptionType>";
                inXML += "  <OptionID>" + e.Item.Cells[0].Text + "</OptionID>";
		        inXML += "  <Caption>" + e.Item.Cells[1].Text + "</Caption>";
                inXML += "</DeleteEntityCode>";
                
                String outXML = obj.CallMyVRMServer("DeleteCustomAttribute", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = "<success>1</success>";
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    Response.Redirect("EntityCode.aspx?m=1");
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }
        #endregion

        #region BindRowsDeleteMessage 
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Billing Code?") + "')");//FB japnese
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindRowsDeleteMessage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region UpdateEntityCode 
        protected void UpdateEntityCode(Object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridItem dg1 in dgEntityCode.Items)
                {
                    if ((Label)dg1.FindControl("lblEntityName") != null)
                    {
                        if (((Label)dg1.FindControl("lblEntityName")).Text.ToLower().Trim() == txtEntityName.Text.ToLower().Trim())
                        {
                            errLabel.Text = obj.GetTranslatedText("Billing Code already exists in the list"); //Entity Code Changed //FB 1830 - Translation
                            errLabel.Visible = true;
                            txtEntityName.Text = "";    //Code added for Entity Code Changes
                            txtEntityDesc.Text = "";    //Code added for Entity Code Changes
                            return;
                        }
                        //Code added for Entity Code Changes - Start
                        if (txtEntityName.Text.Length > 35)
                        {
                            errLabel.Text = obj.GetTranslatedText("Billing Code Name should be within 35 characters");//FB 1830 - Translation
                            errLabel.Visible = true;
                            txtEntityName.Text = "";    //Code added for Entity Code Changes
                            txtEntityDesc.Text = "";    //Code added for Entity Code Changes
                            return;
                        }
                        //Code added for Entity Code Changes - End
                    }
                }

                String inXML = "";
                inXML += "<SetEntityCode>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <CustomAttributeID>1</CustomAttributeID>";
                inXML += "  <DeptID>0</DeptID>";
                inXML += "  <OptionType>6</OptionType>";
                inXML += "  <OptionID>" + txtEntityID.Text + "</OptionID>";
                inXML += "  <OptionValue>0</OptionValue>";
                inXML += "  <Caption>" + txtEntityName.Text.Trim() + "</Caption>";
                inXML += "  <HelpText>" + txtEntityDesc.Text.Trim() + "</HelpText>";
                inXML += "</SetEntityCode>";

                String outXML = obj.CallMyVRMServer("SetCustomAttribute", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("EntityCode.aspx?m=1");
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }


            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("UpdateEntityCode" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region AddEntityCode 
        protected void AddEntityCode(Object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridItem dg1 in dgEntityCode.Items)
                {
                    if (((Label)dg1.FindControl("lblEntityName")).Text.ToLower().Trim() == txtEntityName.Text.ToLower().Trim())
                    {
                        errLabel.Text = obj.GetTranslatedText("Billing Code already exists in the list"); //Entity Code Changed //FB 1830 - Translation
                        errLabel.Visible = true;
                        return;
                    }
                    //Code added for Entity Code Changes - Start
                    if (txtEntityName.Text.Length > 35)
                    {
                        errLabel.Text = obj.GetTranslatedText("Billing Code Name should be within 35 characters");//FB 1830 - Translation
                        errLabel.Visible = true;
                        return;
                    }
                    //Code added for Entity Code Changes - End
                }
                if (txtEntityDesc.Text.Length > 35)
                {
                    errLabel.Text = obj.GetTranslatedText("Billing Code description should be within 35 characters"); //Entity Code Changed //FB 1830 - Translation
                    errLabel.Visible = true;
                    return;
                }
                String inXML = "";
                inXML += "<SetEntityCode>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <CustomAttributeID>1</CustomAttributeID>";
                inXML += "  <DeptID>0</DeptID>";
                inXML += "  <OptionType>6</OptionType>";
                inXML += "  <OptionID>" + txtEntityID.Text + "</OptionID>";
                inXML += "  <OptionValue>0</OptionValue>";
                inXML += "  <Caption>" + txtEntityName.Text.Trim() + "</Caption>";
                inXML += "  <HelpText>" + txtEntityDesc.Text.Trim() + "</HelpText>";
                inXML += "</SetEntityCode>";

                String outXML = obj.CallMyVRMServer("SetCustomAttribute", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("EntityCode.aspx?m=1");
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }


            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("AddEntityCode" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion
    }
   
}
