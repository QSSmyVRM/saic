/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using System.Xml;
using System.Drawing;
using System.IO;

using System.Management;
using System.Management.Instrumentation;
using MetaBuilders.WebControls;
using System.ServiceProcess;
using Microsoft.Win32;
using System.Text;


public partial class en_EventLog : System.Web.UI.Page
{
    #region protected member 

    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Label lblUpdateOn;
    protected System.Web.UI.WebControls.DataGrid LogGrid;
    protected System.Web.UI.WebControls.DataGrid myVRMLogGrid;
    protected System.Web.UI.WebControls.Table SystemInfoTable;
    protected System.Web.UI.WebControls.Button PurgeNow;
    protected System.Web.UI.WebControls.Button PurgeNowInDiv;
    protected System.Web.UI.WebControls.Button PurgeRegular;
    protected System.Web.UI.WebControls.RadioButton Daily;
    protected System.Web.UI.WebControls.RadioButton Weekly;
    protected System.Web.UI.WebControls.Label lblText;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSeviceValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailServiceStatus;// FB 1838
    protected System.Web.UI.WebControls.RadioButton Monthly;
    protected System.Web.UI.WebControls.RadioButton RemoveType;
    protected System.Web.UI.WebControls.RadioButton RNow;
    protected System.Web.UI.WebControls.ImageButton Start;
    protected System.Web.UI.WebControls.ImageButton Pause;
    protected System.Web.UI.WebControls.ImageButton Stop;
    protected System.Web.UI.WebControls.Label ServiceStatus;
    protected System.Web.UI.WebControls.Label EmailServiceStatus; // FB 1838
    protected System.Web.UI.WebControls.RadioButton DCompletedConf;
    protected System.Web.UI.WebControls.RadioButton DAllConf;
    protected System.Web.UI.WebControls.RadioButton DAllData;
    protected System.Web.UI.HtmlControls.HtmlTableRow PScheduleRow1;
    protected System.Web.UI.HtmlControls.HtmlTableRow PScheduleRow2;
    protected System.Web.UI.HtmlControls.HtmlTableRow PScheduleRow3;
    protected System.Web.UI.HtmlControls.HtmlTableRow PScheduleRow4;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPurgeData;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPurgeType;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPass;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDisplayValue;
    protected AjaxControlToolkit.TabContainer DiagnosticsTabs;
    protected System.Web.UI.WebControls.Table SQLServerInfoTable;    
    protected System.Web.UI.WebControls.Button ApExport;
    protected System.Web.UI.WebControls.Button ApExportAll;
    protected System.Web.UI.WebControls.Button AlExport;
    protected System.Web.UI.WebControls.Button AlExportAll;
    protected System.Web.UI.WebControls.Table MyVRMassemblyTable;
    protected System.Web.UI.WebControls.Table ComponentTable;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnExportValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnActiveIndex;
    protected System.Web.UI.WebControls.TextBox txtPwd;
    
    protected System.Web.UI.WebControls.Label lblMessage;
    protected System.Web.UI.WebControls.Label lblMyVRMVersion;
    protected System.Web.UI.WebControls.Label lblASPILVersion;
    protected System.Web.UI.WebControls.Label lblDatabaseVersion;
    protected System.Web.UI.WebControls.Label lblRTCVersion;
    protected System.Web.UI.WebControls.DropDownList moduleType;
    protected System.Web.UI.WebControls.TextBox txtErrorCode;
    protected System.Web.UI.WebControls.TextBox startDate;
    protected System.Web.UI.WebControls.TextBox endDate;
    protected System.Web.UI.WebControls.HiddenField startTime;
    protected System.Web.UI.WebControls.HiddenField endTime;
    protected System.Web.UI.WebControls.CheckBoxList Severity;
    protected System.Web.UI.WebControls.TextBox ErrorMessage;
    protected System.Web.UI.WebControls.Table tblLogPref;
    protected RequiredFieldValidator startDateRequired;
    protected RequiredFieldValidator endDateRequired;
    protected CompareValidator startDateComp;
    protected RegularExpressionValidator startDateRegEx;
    protected RequiredFieldValidator startTimeReq;
    protected RegularExpressionValidator startTimeRegEx;
    protected RequiredFieldValidator endTimeReq;
    protected RegularExpressionValidator endTimeRegEx;
    protected CustomValidator validateSeverity;
    protected ComboBox startTimeCombo;
    protected ComboBox endTimeCombo;
    protected TextBox logDate;// Added for Log export
    protected RadioButtonList Servicesnames;// Added for Log export

    protected System.Web.UI.WebControls.Label ReminderServiceStatus; //FB 1926
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnReminderServiceStatus; //FB 1926
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdntformat; //FB 2588
    
    ManagementObjectSearcher objOS;
    ManagementObjectSearcher objCS;

    public String email_Log = "";
    public String RTC_Log = "";

    #endregion

    #region Private Data Members 

    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;

    String tformat = "hh:mm tt";
    protected String format = "";
    protected String clientName = "";

    private string[] moduleName;
    private string[] logLevel;
    private string[] logLife;
    private string getLogPrefInXml = "";

    private DropDownList[] lstLogLevel;
    private DropDownList[] lstLogLife;
    private Button delButton;
    private bool isApplicationLog = false;
    private bool ismyVRMLog = false;
    
    #endregion

    #region en_EventLog 

    public en_EventLog()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
    }

    #endregion

    #region Page Load Event Handler 

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("eventlog.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            

            if (Application["Client"] == null)
                Application["Client"] = "";

            clientName = Application["Client"].ToString().ToUpper();

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

            if (Session["timeFormat"].ToString() == "2") //FB 2588
                tformat = "HHmmZ";

            hdntformat.Value = DateTime.Parse("12:00 AM").ToString(tformat); //FB 2588

            startTimeCombo.Items.Clear();
            endTimeCombo.Items.Clear();
            obj.BindTimeToListBox(startTimeCombo, false, false);
            obj.BindTimeToListBox(endTimeCombo, false, false);

            if (tformat.Equals("HH:mm"))
            {
                startTimeRegEx.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                startTimeRegEx.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                endTimeRegEx.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                endTimeRegEx.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
            }
            else if (tformat.Equals("HHmmZ")) //FB 2588
            {
                startTimeRegEx.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                startTimeRegEx.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                endTimeRegEx.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                endTimeRegEx.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
            }

            startTimeCombo.Text = DateTime.Parse("01:00 AM").ToString(tformat);
            endTimeCombo.Text = DateTime.Parse("11:00 PM").ToString(tformat);

            //FB 2670 Start
            if (Session["admin"].ToString() == "3")
            {
                
                //PurgeRegular.ForeColor = System.Drawing.Color.Gray;
                //PurgeRegular.Attributes.Add("Class", "btndisable");// FB 2796
                //ZD 100263
                PurgeRegular.Visible = false;

            }
            else
                PurgeRegular.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796
            //FB 2670 End

            BindSystemInfo();

            BindSQLServerInfoTable(null,null);

            GetFileVersion();            

            BindEventLogs();

            if (!IsPostBack)
            {
                if (Request.QueryString["S"] != null)
                {
                    hdnDisplayValue.Value = Request.QueryString["S"].ToString();
                }

                BindGrid();

                BindmyVRMGrid();

                BindPurgeDetails();

                SetServiceControls();
                
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    }

                GetSiteDiagnostics();
            }
            else
            {
                if (Request.QueryString["d"] != null)
                {
                    deleteLog(Request.QueryString["m"], Request.QueryString["d"]);
                }
            }

            DCompletedConf.Text = ((clientName == "MOJ")? DCompletedConf.Text.Replace("Conferences","Hearings"):DCompletedConf.Text);
            DAllConf.Text = ((clientName == "MOJ")? DAllConf.Text.Replace("Conferences","Hearings"):DAllConf.Text);
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_Load" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            //errLabel.Text = "PageLoad: " + ex.StackTrace;
        }
    }
   
    #endregion

    #region Web Form Designer generated code 

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeUIComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    
    private void InitializeUIComponent()
    {
        LogGrid.PageIndexChanged += new DataGridPageChangedEventHandler(LogGrid_PageIndexChanged);
        LogGrid.ItemDataBound += new DataGridItemEventHandler(LogGrid_ItemDataBound);
        LogGrid.ItemCreated += new DataGridItemEventHandler(LogGrid_ItemCreated);
        myVRMLogGrid.PageIndexChanged += new DataGridPageChangedEventHandler(myVRMLogGrid_PageIndexChanged);
        myVRMLogGrid.ItemDataBound += new DataGridItemEventHandler(myVRMLogGrid_ItemDataBound);
        myVRMLogGrid.ItemCreated += new DataGridItemEventHandler(myVRMLogGrid_ItemCreated);
        //PurgeNow.Click += new EventHandler(PurgeNow_Click);        
        PurgeNowInDiv.Click += new EventHandler(PurgeNow_Click);
        PurgeRegular.Click += new EventHandler(PurgeRegular_Click);
        Start.Click += new ImageClickEventHandler(SetServiceDetails);
        Pause.Click += new ImageClickEventHandler(SetServiceDetails);
        Stop.Click += new ImageClickEventHandler(SetServiceDetails);
        ApExport.Click +=new EventHandler(ApExport_Click);
        ApExportAll.Click += new EventHandler(ApExport_Click);
        AlExport.Click += new EventHandler(ApExport_Click);
        AlExportAll.Click += new EventHandler(ApExport_Click);
    }

    #endregion

    #region BindEventLogs 

    private void BindEventLogs()
    {
        try
        {
            string inXML = "<getLogPreferences>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "<userID>" + Session["userID"] + "</userID>";
            inXML += "</getLogPreferences>";

            getLogPrefInXml = inXML;

            string returnXML = obj.CallMyVRMServer("GetLogPreferences", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
            extractData(returnXML);
            
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region myVRMLogGrid_ItemCreated 

    void myVRMLogGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (!ismyVRMLog)
            {
                if (e.Item.ItemType == ListItemType.Footer && myVRMLogGrid.Items.Count == 0)
                {
                    myVRMLogGrid.ShowFooter = true;
                    myVRMLogGrid.PagerStyle.Visible = false;
                    Label label = new Label();
                    label.Text = obj.GetTranslatedText("No Logs Found.");//FB 1830 - Translation
                    label.CssClass = "lblError";
                    e.Item.Cells.Clear();
                    TableCell cell = new TableCell();
                    cell.ColumnSpan = myVRMLogGrid.Columns.Count;
                    cell.Controls.Add(label);
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    e.Item.Cells.Add(cell);

                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region  myVRMLogGrid_ItemDataBound 

    void myVRMLogGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                EventLogEntry entry = e.Item.DataItem as EventLogEntry;
                String strImage = "";

                if (entry != null)
                {
                    switch (entry.EntryType)
                    {
                        case EventLogEntryType.Warning:
                            strImage = "Image/warning.png";
                            break;
                        case EventLogEntryType.Error:
                            strImage = "Image/error.png";
                            break;
                        default:
                            strImage = "Image/info.png";
                            break;
                    }

                    e.Item.Cells[0].Text = "<img src=" + strImage + @">  " + entry.EntryType;
                    e.Item.Cells[1].Text = ((clientName == "MOJ") ? entry.Message.ToString().Replace("Conferences", "Hearings").Replace("Conference", "Hearing") : entry.Message.ToString());
                    e.Item.Cells[2].Text = Convert.ToDateTime(entry.TimeGenerated.ToString()).ToString(format + " " + tformat);
                    e.Item.Cells[3].Text = entry.Source;

                    ismyVRMLog = true;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }
    #endregion

    #region  myVRMLogGrid_PageIndexChanged 

    void myVRMLogGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            myVRMLogGrid.CurrentPageIndex = e.NewPageIndex;
            BindmyVRMGrid();
            BindSystemInfo();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }
    #endregion

    #region Bind Grid 

    private void BindGrid()
    {
        try
        {
            EventLog aLog = new EventLog();
            aLog.Log = "Application";
            aLog.MachineName = ".";
            
            LogGrid.DataSource = aLog.Entries;
            LogGrid.DataBind();

            if (isApplicationLog)
                LogGrid.ShowFooter = false;
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }

    #endregion

    #region Bind myVRM Grid 

    private void BindmyVRMGrid()
    {
        try
        {
            EventLog aLog = new EventLog();
            aLog.Log = "myVRM";
            aLog.MachineName = ".";

            myVRMLogGrid.DataSource = aLog.Entries;
            myVRMLogGrid.DataBind();

            if (ismyVRMLog)
                myVRMLogGrid.ShowFooter = false;
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region LogGrid_ItemCreated Event Handler 

    void LogGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (!isApplicationLog)
            {
                if (e.Item.ItemType == ListItemType.Footer && LogGrid.Items.Count == 0)
                {
                    LogGrid.ShowFooter = true;                    
                    Label label = new Label();
                    label.Text = obj.GetTranslatedText("No Logs Found.");//FB 1830 - Translation
                    label.CssClass = "lblError";
                    e.Item.Cells.Clear();
                    TableCell cell = new TableCell();
                    cell.ColumnSpan = LogGrid.Columns.Count;
                    cell.Controls.Add(label);
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    e.Item.Cells.Add(cell);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region LogGrid ItemDataBound 
     
    void LogGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                EventLogEntry entry = e.Item.DataItem as EventLogEntry;
                String strImage = "";

                if (entry != null)
                {
                    switch (entry.EntryType)
                    {
                        case EventLogEntryType.Warning:
                            strImage = "Image/warning.png";
                            break;
                        case EventLogEntryType.Error:
                            strImage = "Image/error.png";
                            break;
                        default:
                            strImage = "Image/info.png";
                            break;
                    }

                    e.Item.Cells[0].Text = "<img src=" + strImage + @">  " + entry.EntryType;
                    e.Item.Cells[1].Text = ((clientName == "MOJ") ? entry.Message.ToString().Replace("Conferences", "Hearings").Replace("Conference", "Hearing").Replace("\t", "").Replace("\r\n", " ").Replace("\n", " ") : 
                        entry.Message.ToString().Replace("\t", "").Replace("\r", " ").Replace("\n", " ").Replace("_"," _").Replace("-"," -").Replace("."," ."));
                    e.Item.Cells[2].Text = Convert.ToDateTime(entry.TimeGenerated.ToString()).ToString(format + " " + tformat);
                    e.Item.Cells[3].Text = entry.Source;

                    isApplicationLog = true;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }
    #endregion

    #region  LogGrid_PageIndexChanged 

    void LogGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            LogGrid.CurrentPageIndex = e.NewPageIndex;
            BindGrid();
            BindSystemInfo();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region BindSystemInfo 

    private void BindSystemInfo()
    {
        ManagementObject objMgmt;
        try
        {
            objOS = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem");
            objCS = new ManagementObjectSearcher("SELECT * FROM Win32_ComputerSystem");

            foreach (ManagementObject objmgmt in objOS.Get())
            {
                BindRows("<b>"+obj.GetTranslatedText("Computer Name")+"</b>", objmgmt["csname"].ToString(), SystemInfoTable);
                BindRows("<b>"+obj.GetTranslatedText("Windows Directory")+"</b>", objmgmt["windowsdirectory"].ToString(), SystemInfoTable);
                BindRows("<b>"+obj.GetTranslatedText("OS Name")+"</b>", objmgmt["name"].ToString().Split('|')[0], SystemInfoTable);
                BindRows("<b>"+obj.GetTranslatedText("OS Version")+"</b>", objmgmt["version"].ToString(), SystemInfoTable);
                BindRows("<b>" + obj.GetTranslatedText(".Net Version") + "</b>", System.Environment.Version.ToString(), SystemInfoTable);
            }

            foreach (ManagementObject objmgmt in objCS.Get())
            {
                BindRows("<b>"+obj.GetTranslatedText("Computer Manufacturer")+"</b>", objmgmt["manufacturer"].ToString(), SystemInfoTable);
                BindRows("<b>"+obj.GetTranslatedText("Computer Model")+"</b>", objmgmt["model"].ToString(), SystemInfoTable);
                BindRows("<b>"+obj.GetTranslatedText("System Type")+"</b>", objmgmt["systemtype"].ToString(), SystemInfoTable);
                BindRows("<b>" + obj.GetTranslatedText("Total Physical Memory") + "</b>", objmgmt["totalphysicalmemory"].ToString() + " B", SystemInfoTable);
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }
    
    #endregion

    #region BindRows 

    private void BindRows(string lblName, string strValue,Table table)
    {
        TableRow row = null;
        TableCell cell = null;

        try
        {
            row = new TableRow();
            row.CssClass = "tableBody";

            cell = new TableCell();
            cell.Text = lblName;
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = strValue;
            row.Cells.Add(cell);
            table.Rows.Add(row);
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }
    
    #endregion

    #region BindPurgeDetails

    private void BindPurgeDetails()
    {
        String confName = "";
        String lbltext = "";
        String timeofDay = DateTime.Parse("12:00 AM").ToString(tformat);
        String emailServiceStatus = "";
        String reminderServiceStatus = "";//FB 1926
        try
        {
            XmlNodeList nodes = GetPurgeDetails();

            foreach (XmlNode node in nodes)
            {
                String purgeType = node.SelectSingleNode("PurgeType").InnerText.Trim();

                String serviceStatus = node.SelectSingleNode("ServiceStatus").InnerText.Trim();
                String purgeData = node.SelectSingleNode("PurgeData").InnerText.Trim();
                String updateon = node.SelectSingleNode("UpdatedDate").InnerText;
                if (!updateon.Equals(""))
                    updateon = Convert.ToDateTime(updateon).ToString(format + " " + tformat);
                else
                    updateon = "N/A";

                if (purgeType == "" || purgeType == "0")
                    updateon = "N/A";
                lblUpdateOn.Text = updateon.Trim(); // FB 1725
                if (purgeData != "0")
                {
                    hdnPurgeData.Value = purgeData;  
                    switch (purgeData)
                    {
                        case "1":
                            DCompletedConf.Checked = true;
                            break;
                        case "2":
                            DAllConf.Checked = true;
                            break;
                        case "3":
                            DAllData.Checked = true;
                            break;
                    }
                }

               

                ServiceController vrmServiceManager = new ServiceController("myVRMService");
                serviceStatus = "4";

                try
                {
                    if (vrmServiceManager.Status.Equals(ServiceControllerStatus.Running))
                        serviceStatus = "1";
                    else if (vrmServiceManager.Status.Equals(ServiceControllerStatus.Paused))
                        serviceStatus = "2";
                    else if (vrmServiceManager.Status.Equals(ServiceControllerStatus.Stopped) || vrmServiceManager.Status.Equals(ServiceControllerStatus.StopPending))
                        serviceStatus = "3";
                }
                catch
                {
                    serviceStatus = "4";
                }

                hdnSeviceValue.Value = serviceStatus;

                //FB 1838

                ServiceController vrmEmailServiceManager = new ServiceController("myVRMEmailService");
                emailServiceStatus = "4";

                try
                {
                    if (vrmEmailServiceManager.Status.Equals(ServiceControllerStatus.Running))
                        emailServiceStatus = "1";
                    else if (vrmEmailServiceManager.Status.Equals(ServiceControllerStatus.Paused))
                        emailServiceStatus = "2";
                    else if (vrmEmailServiceManager.Status.Equals(ServiceControllerStatus.Stopped) || vrmEmailServiceManager.Status.Equals(ServiceControllerStatus.StopPending))
                        emailServiceStatus = "3";
                }
                catch
                {
                    emailServiceStatus = "4";
                }

                hdnEmailServiceStatus.Value = emailServiceStatus;

                //FB 1838

                //FB 1926
                ServiceController vrmReminderServiceManager = new ServiceController("myVRMReminderService");
                reminderServiceStatus = "4";

                try
                {
                    if (vrmReminderServiceManager.Status.Equals(ServiceControllerStatus.Running))
                        reminderServiceStatus = "1";
                    else if (vrmReminderServiceManager.Status.Equals(ServiceControllerStatus.Paused))
                        reminderServiceStatus = "2";
                    else if (vrmReminderServiceManager.Status.Equals(ServiceControllerStatus.Stopped) || vrmReminderServiceManager.Status.Equals(ServiceControllerStatus.StopPending))
                        reminderServiceStatus = "3";
                }
                catch
                {
                    reminderServiceStatus = "4";
                }

                hdnReminderServiceStatus.Value = reminderServiceStatus;

                if (clientName == "MOJ")
                    confName = "Hearings";
                else
                    confName = "Conferences";

                hdnPurgeType.Value = purgeType;
                switch (purgeType)
                {
                    case "1":
                        Daily.Checked = true;
                        lbltext = obj.GetTranslatedText("at") + " " + timeofDay + " " + obj.GetTranslatedText("automatically on daily basis."); //FB 2588
                        break;
                    case "2":
                        Weekly.Checked = true;
                        lbltext = obj.GetTranslatedText("every Friday at") + timeofDay + obj.GetTranslatedText("automatically.");
                        break;
                    case "3":
                        Monthly.Checked = true;
                        lbltext = obj.GetTranslatedText("every Month End at") + timeofDay + obj.GetTranslatedText("automatically.");
                        break;
                    case "4":
                    case "":
                    case "0":
                        RemoveType.Checked = true;
                        lbltext = "";
                        break;
                    case "5":
                        RNow.Checked = true;
                        lbltext = "";
                        break;
                }

                if (lbltext != "")
                    lblText.Text = String.Format(obj.GetTranslatedText("Purge Completed {0} {1}"), confName, lbltext);
                else
                    lblText.Text = "";

            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }
    #endregion

    #region Purge Now - Delete Conf 

    public void PurgeNow_Click(object sender, EventArgs e)
    {
        String inXML = "";
        String outXML = "";
        try
        {
            inXML = "<login>" + obj.OrgXMLElement() + "<emailID>" + Session["LoginUserName"].ToString() + "</emailID><userPassword>" + hdnPass.Value + "</userPassword><homeURL></homeURL></login>";//Organization Module Fixes
            outXML = obj.CallMyVRMServer("GetHome", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
            {
                errLabel.Text = obj.ShowErrorMessage(outXML).Replace("login or ","");
                errLabel.Visible = true;
            }
            else
            {
                //// Now 
                inXML = "";
                inXML += "<DeletePastConference>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <PurgeData>" + hdnPurgeData.Value + "</PurgeData>";
                inXML += "  <Path>" + Application["MyVRMServer_ConfigPath"].ToString() + "</Path>";
                inXML += "</DeletePastConference>";

                if (hdnPurgeData.Value == "1" || hdnPurgeData.Value == "2")
                    outXML = obj.CallMyVRMServer("DeletePastConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                else if (hdnPurgeData.Value == "3")
                {
                    Session.Remove("RoomsXML");  //Room search and endpoint search Delete all data's fix
                    Session.Remove("DtDisp");
                    Cache.Remove("RoomsXML" + Session["organizationID"]);
                    Session.Remove("EndpointXML");
                    Cache.Remove("EndpointXML"); 

                    outXML = obj.CallMyVRMServer("DeleteAllData", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                }

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    String purgeType = "";

                    if (Daily.Checked)
                        purgeType = "1";
                    else if (Weekly.Checked)
                        purgeType = "2";
                    else if (Monthly.Checked)
                        purgeType = "3";
                    else if (RemoveType.Checked)
                        purgeType = "4";
                    else if (RNow.Checked)
                        purgeType = "5";

                    inXML = "";
                    inXML += "<PurgeDetails>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <PurgeType>" + purgeType + "</PurgeType>";
                    inXML += "  <UpdatedDate>" + DateTime.Now + "</UpdatedDate>";
                    inXML += "</PurgeDetails>";

                    outXML = obj.CallMyVRMServer("SetPurgeDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!.");//FB 1830 - Translation
                        BindPurgeDetails();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }

    #endregion

    #region Set Purge Type 

    void PurgeRegular_Click(object sender, EventArgs e)
    {
        try
        {
            String purgeType = "";

            if (Daily.Checked)
                purgeType = "1";
            else if (Weekly.Checked)
                purgeType = "2";
            else if (Monthly.Checked)
                purgeType = "3";
            else if (RemoveType.Checked)
                purgeType = "4";
            else if (RNow.Checked)
                purgeType = "5";

            String inXML = "";
            inXML += "<PurgeDetails>";
            inXML += obj.OrgXMLElement();//Organization Module
            inXML += "  <PurgeData>" + hdnPurgeData.Value + "</PurgeData>";
            inXML += "  <PurgeType>" + purgeType + "</PurgeType>";
            inXML += "  <UpdatedDate>" + DateTime.Now + "</UpdatedDate>";
            inXML += "</PurgeDetails>";

            SetPurgeDetails(inXML);

        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }

    #endregion 

    #region SetPurgeDetails 

    private void SetPurgeDetails(String inXML)
    {
        try
        {
            String outXML = obj.CallMyVRMServer("SetPurgeDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
            }
            else
            {
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                BindPurgeDetails();
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }

    #endregion

    #region GetPurgeDetails 

    public XmlNodeList GetPurgeDetails()
    {
        XmlNodeList nodes = null;
        try
        {
            string inXML = "<GetPurgeDetails>"+ obj.OrgXMLElement() +"</GetPurgeDetails>";
            string outXML = obj.CallMyVRMServer("GetPurgeDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);

            nodes = xmldoc.SelectNodes("//PurgeDetails");

        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
        return nodes;
    }
    #endregion

    #region SetServiceControls 

    private void SetServiceControls()
    {
        // 0 - Stop , 1- Start , 2 - Pause
        try
        {
           
            Start.Attributes.Add("Style", "Display:None");
            Pause.Attributes.Add("Style", "Display:None");
            Stop.Attributes.Add("Style", "Display:None");

            switch (hdnSeviceValue.Value)
            {
                case "0":
                    ServiceStatus.Text = obj.GetTranslatedText("Not Started");//FB 1830 - Translation
                    ServiceStatus.ForeColor = Color.Red;
                    //Start.Attributes.Add("Style", "Display:Block");
                    break;
                case "1":
                    ServiceStatus.Text = obj.GetTranslatedText("Running");//FB 1830 - Translation
                    //Pause.Attributes.Add("Style", "Display:Block");
                    //Stop.Attributes.Add("Style", "Display:Block");
                    ServiceStatus.ForeColor = Color.Green;
                    break;
                case "2":
                    ServiceStatus.Text = obj.GetTranslatedText("Paused");//FB 1830 - Translation
                    ServiceStatus.ForeColor = Color.Red;
                    //Start.Attributes.Add("Style", "Display:Block");
                    //Stop.Attributes.Add("Style", "Display:Block");
                    break;
                case "3":
                    ServiceStatus.Text = obj.GetTranslatedText("Stopped");//FB 1830 - Translation
                    ServiceStatus.ForeColor = Color.Red;
                    // Start.Attributes.Add("Style", "Display:Block");
                    break;
                case "4":
                    ServiceStatus.ForeColor = Color.Red;
                    ServiceStatus.Text = obj.GetTranslatedText("VRM Service is not installed");//FB 1830 - Translation
                    break;
            }

            switch (hdnEmailServiceStatus.Value)// FB 1838
            {
                case "0":
                    EmailServiceStatus.Text = obj.GetTranslatedText("Not Started");//FB 1830 - Translation
                    EmailServiceStatus.ForeColor = Color.Red;
                    //Start.Attributes.Add("Style", "Display:Block");
                    break;
                case "1":
                    EmailServiceStatus.Text = obj.GetTranslatedText("Running");//FB 1830 - Translation
                    //Pause.Attributes.Add("Style", "Display:Block");
                    //Stop.Attributes.Add("Style", "Display:Block");
                    EmailServiceStatus.ForeColor = Color.Green;
                    break;
                case "2":
                    EmailServiceStatus.Text = obj.GetTranslatedText("Paused");//FB 1830 - Translation
                    EmailServiceStatus.ForeColor = Color.Red;
                    //Start.Attributes.Add("Style", "Display:Block");
                    //Stop.Attributes.Add("Style", "Display:Block");
                    break;
                case "3":
                    EmailServiceStatus.Text = obj.GetTranslatedText("Stopped");//FB 1830 - Translation
                    EmailServiceStatus.ForeColor = Color.Red;
                    // Start.Attributes.Add("Style", "Display:Block");
                    break;
                case "4":
                    EmailServiceStatus.ForeColor = Color.Red;
                    EmailServiceStatus.Text = obj.GetTranslatedText("VRM Service is not installed");//FB 1830 - Translation
                    break;
            }

            switch (hdnReminderServiceStatus.Value)// FB 1926
            {
                case "0":
                    ReminderServiceStatus.Text = obj.GetTranslatedText("Not Started");//FB 1830 - Translation
                    ReminderServiceStatus.ForeColor = Color.Red;
                    //Start.Attributes.Add("Style", "Display:Block");
                    break;
                case "1":
                    ReminderServiceStatus.Text = obj.GetTranslatedText("Running");//FB 1830 - Translation
                    //Pause.Attributes.Add("Style", "Display:Block");
                    //Stop.Attributes.Add("Style", "Display:Block");
                    ReminderServiceStatus.ForeColor = Color.Green;
                    break;
                case "2":
                    ReminderServiceStatus.Text = obj.GetTranslatedText("Paused");//FB 1830 - Translation
                    ReminderServiceStatus.ForeColor = Color.Red;
                    //Start.Attributes.Add("Style", "Display:Block");
                    //Stop.Attributes.Add("Style", "Display:Block");
                    break;
                case "3":
                    ReminderServiceStatus.Text = obj.GetTranslatedText("Stopped");//FB 1830 - Translation
                    ReminderServiceStatus.ForeColor = Color.Red;
                    // Start.Attributes.Add("Style", "Display:Block");
                    break;
                case "4":
                    ReminderServiceStatus.ForeColor = Color.Red;
                    ReminderServiceStatus.Text = obj.GetTranslatedText("VRM Service is not installed");//FB 1830 - Translation
                    break;
            }

        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region SetServiceDetails 

    private void SetServiceDetails(object sender, ImageClickEventArgs e)
    {
        try
        {
            String inXML = "";
            inXML += "<PurgeDetails>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <ServiceStatus>" + hdnSeviceValue.Value + "</ServiceStatus>";
            inXML += "</PurgeDetails>";

            SetPurgeDetails(inXML);

            SetServiceControls();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);;
        }
    }

    #endregion

    //This Method call from RTC_RepeaterOps.aspx

    #region PurgeConferenceRegular 

    public void PurgeConferenceRegular()
    {
        Int32 dayofWeek = Int32.MinValue;
        Boolean isPurgeDate = false;
        String purgeType = "";
        String inXML = "";
        String outXML = "";
        String configPath = "";
        String serviceStatus = "";
        try
        {
            if (Application == null)
            {
                configPath = @"C:\VRMSchemas_v1.8.3\";
            }
            else
                configPath = Application["MyVRMServer_ConfigPath"].ToString();

            inXML = "<GetPurgeDetails><organizationID>11</organizationID></GetPurgeDetails>"; //Service Manager Error - Default organization
            outXML = obj.CallMyVRMServer("GetPurgeDetails", inXML, configPath);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);

            XmlNodeList nodes = xmldoc.SelectNodes("//PurgeDetails");

            foreach (XmlNode node in nodes)
            {
                purgeType = node.SelectSingleNode("PurgeType").InnerText.Trim();

                serviceStatus = node.SelectSingleNode("ServiceStatus").InnerText.Trim();

                if (node.SelectSingleNode("DayofWeek").InnerText.Trim() != "")
                {
                    dayofWeek = Convert.ToInt32(node.SelectSingleNode("DayofWeek").InnerText.Trim());
                }
            }

            if (purgeType == "1")
            {
                if (DateTime.Now.ToShortTimeString() == "12:00 AM")
                    isPurgeDate = true;
            }
            else if (purgeType == "2")
            {
                if ((Int32)DateTime.Now.DayOfWeek == dayofWeek && DateTime.Now.ToShortTimeString() == "12:00 AM")
                    isPurgeDate = true;
            }
            else if (purgeType == "3")
            {
                DateTime dtTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                dtTo = dtTo.AddMonths(1);
                dtTo = dtTo.AddDays(-(dtTo.Day));

                if (DateTime.Today == dtTo && DateTime.Now.ToShortTimeString() == "12:00 AM")
                    isPurgeDate = true;
            }

            if (isPurgeDate)
            {
                inXML = "";
                inXML += "<DeletePastConference>";
                inXML += "<organizationID>11</organizationID>";//Organization Module Fixes - Service Manager Issue
                inXML += "  <PurgeData>1</PurgeData>";
                inXML += "  <Path>" + configPath + "</Path>";
                inXML += "</DeletePastConference>";

                outXML = obj.CallMyVRMServer("DeletePastConference", inXML, configPath);

                inXML = "";
                inXML += "<PurgeDetails>";
                inXML += "<organizationID>11</organizationID>";//Organization Module Fixes  - Service Manager Issue
                inXML += "  <PurgeType>" + purgeType + "</PurgeType>";
                inXML += "  <UpdatedDate>" + DateTime.Now + "</UpdatedDate>";
                inXML += "</PurgeDetails>";

                outXML = obj.CallMyVRMServer("SetPurgeDetails", inXML, configPath);
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region BindSQLServerInfoTable 

    protected void BindSQLServerInfoTable(Object sender, EventArgs e)
    {
        XmlNodeList nodes = null;
        TableRow tr = null;
        TableCell tc = null;

        try
        {
            string inXML = "";

            inXML = "";
            inXML += "<ServerInfo>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <Path>" + Application["MyVRMServer_ConfigPath"].ToString() + "</Path>";
            inXML += "</ServerInfo>";

            string outXML = obj.CallMyVRMServer("GetSQLServerInfo", inXML, Application["MyVRMServer_ConfigPath"].ToString());

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);

            nodes = xmldoc.SelectNodes("//ServerInformation/ServerInfo");

            foreach (XmlNode node in nodes)
            {
                tr = new TableRow();
                tr.CssClass = "tableBody";
                tc = new TableCell();
                if(node.SelectSingleNode("Item").InnerText.IndexOf("[") > 0)
                    tc.Text = "<b>" + obj.GetTranslatedText(node.SelectSingleNode("Item").InnerText.Replace(" ","&nbsp;")) +"</b>";
                else
                    tc.Text = "<b>" + obj.GetTranslatedText(node.SelectSingleNode("Item").InnerText) + "</b>";

                tr.Cells.Add(tc);

                tc = new TableCell();
                tc.Text = node.SelectSingleNode("Description").InnerText.Trim().Replace("amp;","&");
                tr.Cells.Add(tc);

                SQLServerInfoTable.Rows.Add(tr);
            }

        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region ApExport_Click 

    void ApExport_Click(object sender, EventArgs e)
    {
        String DestDirectory = "";
        DataSet ds = null;
        Hashtable logList = null;
        String redURL = "";
        try
        {
            DestDirectory = Server.MapPath(".."); //FB 1830
            DestDirectory += "/" + Session["Language"].ToString(); //FB 1830
            String dFile = "EvenlLog.xls";

            DataTable eventTable = new DataTable();
            DataColumn colItem = null;

            colItem = new DataColumn(obj.GetTranslatedText("Type"), typeof(System.String));
            eventTable.Columns.Add(colItem);
            colItem = new DataColumn(obj.GetTranslatedText("Description"), typeof(System.String));
            eventTable.Columns.Add(colItem);
            colItem = new DataColumn(obj.GetTranslatedText("EventDate"), typeof(System.String));
            eventTable.Columns.Add(colItem);
            colItem = new DataColumn(obj.GetTranslatedText("Source"), typeof(System.String));
            eventTable.Columns.Add(colItem);

            EventLog aLog = new EventLog();
            DataRow newRow;
            String logName = "";
            logList = new Hashtable();
            ds = new DataSet();
            if (hdnExportValue.Value != "")
            {
                switch (hdnExportValue.Value)
                {
                    case "1":
                        logName = "Application";
                        logList.Add(logName, "Application Log");
                        eventTable.TableName = logName;
                        BindLogData(logName, eventTable);
                        ds.Tables.Add(eventTable);

                        DataTable logTable = new DataTable();
                        logTable = eventTable.Clone();
                        logName = "myVRM";
                        logList.Add(logName, "myVRM Alerts Log");
                        logTable.TableName = logName;
                        BindLogData(logName, logTable);
                        ds.Tables.Add(logTable);
                        break;
                    case "2":
                        logName = "Application";
                        logList.Add(logName, "Application Log");
                        BindLogData(logName, eventTable);
                        eventTable.TableName = logName;
                        ds.Tables.Add(eventTable);
                        break;
                    case "3":
                        logName = "myVRM";
                        logList.Add(logName, "myVRM Alerts Log");
                        BindLogData(logName, eventTable);
                        eventTable.TableName = logName;
                        ds.Tables.Add(eventTable);
                        break;
                }

                //FB 1750 - Changed Excel Util reference 
                myVRM.ExcelXmlWriter.ExcelUtil exlUtil = new myVRM.ExcelXmlWriter.ExcelUtil();
                exlUtil.CreateXLFile(logList, DestDirectory + "\\" + dFile, ds);

                String[] pathAry = Request.ServerVariables["URL"].Split('/');
                redURL = "http://" + Request.ServerVariables["http_host"];

                for (Int32 i = 0; i < pathAry.Length - 1; i++)
                {
                    if (pathAry[i].Trim() != "")
                        redURL += "/" + pathAry[i].Trim();
                }

                redURL = redURL + "/"+ dFile;
                Response.Redirect(redURL);
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region BindLogData 

    private void BindLogData(String logName, DataTable eventTable)
    {
        EventLog aLog = new EventLog();
        DataRow newRow;
        aLog.Log = logName;
        aLog.MachineName = ".";

        try //FB 1830
        {
            foreach (EventLogEntry entry in aLog.Entries)
            {
                newRow = eventTable.NewRow();
                newRow["Type"] = entry.EntryType.ToString();
                newRow["Description"] = ((clientName == "MOJ") ? entry.Message.ToString().Replace("Conferences", "Hearings").Replace("Conference", "Hearing") : entry.Message.ToString());
                newRow["EventDate"] = entry.TimeGenerated.ToString(format + " " + tformat);
                newRow["source"] = entry.Source.ToString();
                eventTable.Rows.Add(newRow);
            }
        }
        catch{}
    }

    #endregion

    #region GetFileVersion 

    public void GetFileVersion()
    {
        String path = "";
        String strPath = "";
        TableRow tr = null;
        TableCell tc = null;
        try
        {
            Hashtable assemblyList = new Hashtable();
            assemblyList.Add("myVRMPresentation.dll", "Front-End");
            assemblyList.Add("ASPIL.dll", "Business Facade layer");
            assemblyList.Add("vrmBusinessLayer.dll", "Business Logic Layer");
            assemblyList.Add("vrmDataLayer.dll", "Data Access Layer");
            assemblyList.Add("VRMRTC.dll", "Hardware Interaction layer");

            ArrayList componentsList = new ArrayList();
            componentsList.Add("myVRM.ExcelXmlWriter.dll");
            componentsList.Add("NHibernate.dll");
            componentsList.Add("Castle.DynamicProxy.dll");
            componentsList.Add("DayPilot.dll");
            componentsList.Add("Dart.Telnet.dll");
            componentsList.Add("AjaxControlToolkit.dll");

            path = Server.MapPath("..") + "\\bin";

            tr = new TableRow();
            tc = new TableCell();
            tr.CssClass = "tableHeader";
            tc.Text = obj.GetTranslatedText("DLL Name");//FB 1830 - Translation
            tr.Cells.Add(tc);

            tc = new TableCell();
            tc.Text = obj.GetTranslatedText("Version Number");//FB 1830 - Translation
            tr.Cells.Add(tc);

            tc = new TableCell();
            tc.Text = obj.GetTranslatedText("Last Updated Date");//FB 1830 - Translation
            tr.Cells.Add(tc);
            MyVRMassemblyTable.Rows.Add(tr);

            foreach (String key in assemblyList.Keys)
            {
                strPath = "";
                strPath = path + "\\" + key;

                FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(strPath);
                DateTime modification = File.GetLastWriteTime(strPath);
                //DateTime creation = File.GetCreationTime(@"E:\MyVrmFiles\UIBR2\Website\bin\v1.8.3.dll");
                tr = new TableRow();
                tc = new TableCell();
                tr.CssClass = "tableBody";
                tc.Text = assemblyList[key].ToString();
                tr.Cells.Add(tc);

                tc = new TableCell();
                tc.Text = myFileVersionInfo.FileVersion;
                tr.Cells.Add(tc);

                tc = new TableCell();
                tc.Text =  modification.ToString(format + " " + tformat);
                tr.Cells.Add(tc);
                MyVRMassemblyTable.Rows.Add(tr);
            }

            tr = new TableRow();
            tc = new TableCell();
            tr.CssClass = "tableHeader";
            tc.Text = obj.GetTranslatedText("DLL Name");//FB 1830 - Translation
            tr.Cells.Add(tc);

            tc = new TableCell();
            tc.Text = obj.GetTranslatedText("Version Number");//FB 1830 - Translation
            tr.Cells.Add(tc);

            tc = new TableCell();
            tc.Text = obj.GetTranslatedText("Last Updated Date");//FB 1830 - Translation
            tr.Cells.Add(tc);
            ComponentTable.Rows.Add(tr);

            for (Int32 i = 0; i < componentsList.Count; i++)
            {
                strPath = "";
                strPath = path + "\\" + componentsList[i].ToString();

                FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(strPath);
                DateTime modification = File.GetLastWriteTime(strPath);
                //DateTime creation = File.GetCreationTime(@"E:\MyVrmFiles\UIBR2\Website\bin\v1.8.3.dll");
                tr = new TableRow();
                tc = new TableCell();
                tr.CssClass = "tableBody";
                tc.Text = componentsList[i].ToString();
                tr.Cells.Add(tc);

                tc = new TableCell();
                tc.Text = myFileVersionInfo.FileVersion;
                tr.Cells.Add(tc);

                tc = new TableCell();
                tc.Text = modification.ToString(format + " " + tformat);
                tr.Cells.Add(tc);
                ComponentTable.Rows.Add(tr);
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region searchLog 
    
    private void searchLog()
    {
        try
        {
            string severityOption = "";
            string fromDate, toDate = "";
            DateTime dtFrom = new DateTime();
            //FB 2027 Start
            DateTime dtTo = new DateTime();
            StringBuilder inXML = new StringBuilder();
            inXML.Append("<searchLog>");
            inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
            inXML.Append("<module>");
            inXML.Append("<moduleName>" + moduleType.SelectedValue + "</moduleName>");
            inXML.Append("<moduleSystemID></moduleSystemID>");
            inXML.Append("<moduleErrorCode>" + txtErrorCode.Text + "</moduleErrorCode>");
            inXML.Append("</module>");

            fromDate = myVRMNet.NETFunctions.GetDefaultDate(startDate.Text);
            toDate = myVRMNet.NETFunctions.GetDefaultDate(endDate.Text);

            dtFrom = Convert.ToDateTime(myVRMNet.NETFunctions.ChangeTimeFormat(startTimeCombo.Text)); //FB 2588
            dtTo = Convert.ToDateTime(myVRMNet.NETFunctions.ChangeTimeFormat(endTimeCombo.Text)); //FB 2588

            inXML.Append("<from>" + fromDate + " " + dtFrom.ToString("HH:mm:ss tt") + "</from>");
            inXML.Append("<to>" + toDate + " " + dtTo.ToString("HH:mm:ss tt") + "</to>");

            for (int i = 0; i < Severity.Items.Count; i++)
            {
                if (Severity.Items[i].Selected == true)
                {
                    severityOption += Severity.Items[i].Value;
                    severityOption += ",";
                }
            }

            severityOption = severityOption.Remove(severityOption.LastIndexOf(","));
            Severity.CssClass = "blackblodtext";

            inXML.Append("<severity>" + severityOption + "</severity>");
            inXML.Append("<file></file>");
            inXML.Append("<function></function>");
            inXML.Append("<line></line>");
            inXML.Append("<message>" + ErrorMessage.Text + "</message>");
            inXML.Append("<pageNo>1</pageNo>");
            inXML.Append("</searchLog>");
            //log.Trace("SearchLog: " + inXML);
            string returnXML = obj.CallMyVRMServer("SearchLog", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
            //FB 2027 End
            log.Trace("SearchLog: " + returnXML);

            if (returnXML.IndexOf("<error>") < 0)
            {
                Session.Add("searchParam", inXML);
                Session.Add("searchResult", returnXML);
                Response.Redirect("logList.aspx", true);
            }

        }
        catch (Exception ex)
        {
            log.Trace("searchLog: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    
    #endregion

    #region deleteLog 

    protected void deleteLog(string moduleName, string deleteAll)
    {
        string inXML = "<login>";
        inXML += obj.OrgXMLElement();//Organization Module Fixes
        inXML += "<userID>" + Session["userID"] + "</userID>";

        if (deleteAll == "0")
        {
            inXML += "<moduleName>" + moduleName + "</moduleName>";
            inXML += "<deleteAll>0</deleteAll>";
        }
        else
        {
            inXML += "<moduleName></moduleName>";
            inXML += "<deleteAll>" + deleteAll + "</deleteAll>";
        }

        inXML += "</login>";

        string returnXML = obj.CallMyVRMServer("DeleteModuleLog", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(DeleteModuleLog)

        if (returnXML.IndexOf("<error>") < 0)
        {
            string logPrefXML = "<getLogPreferences>";
            logPrefXML += obj.OrgXMLElement();//Organization Module Fixes
            logPrefXML += "<userID>" + Session["userID"] + "</userID>";
            logPrefXML += "</getLogPreferences>";

            returnXML = obj.CallMyVRMServer("GetLogPreferences", logPrefXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
            if (returnXML.IndexOf("<error>") < 0)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
            }
            Response.Redirect("EventLog.aspx?m=1");
        }
        else
        {
            //myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
            errLabel.Text = obj.ShowErrorMessage(returnXML.ToString());
            errLabel.Visible = true;

            string logPrefXML = "<getLogPreferences>";
            logPrefXML += obj.OrgXMLElement();//Organization Module Fixes
            logPrefXML += "<userID>" + Session["userID"] + "</userID>";
            logPrefXML += "</getLogPreferences>";

            returnXML = obj.CallMyVRMServer("GetLogPreferences", logPrefXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
        }
    }
    
    #endregion

    #region updateLogPref 

    private void updateLogPref()
    {
        int ctr = 0;
        string inXML = "<setLogPreferences>";
        inXML += obj.OrgXMLElement();//Organization Module Fixes

        XmlDocument logXML = new XmlDocument();

        logXML.LoadXml(Session["outXML"].ToString());

        XmlNodeList moduleList = logXML.SelectNodes("//getLogPreferences/module");

        foreach (XmlNode node in moduleList)
        {
            inXML += "<module>";
            inXML += "<moduleName>" + node.SelectSingleNode("moduleName").InnerText + "</moduleName>";
            inXML += "<logLevel>" + lstLogLevel[ctr].SelectedValue + "</logLevel>";
            inXML += "<logLife>" + lstLogLife[ctr].SelectedValue + "</logLife>";

            inXML += "</module>";

            ctr++;
        }

        inXML += "</setLogPreferences>";
        string returnXML = obj.CallMyVRMServer("SetLogPreferences", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027

        if (returnXML.IndexOf("<error>") < 0)
        {
            Response.Redirect("EventLog.aspx?m=1");
        }

    }
    
    #endregion

    #region CallCOM 

    private string CallCOM(string command, string inXML)
    {
        String outXML = obj.CallCOM(command, inXML, Application["COM_ConfigPath"].ToString());
        Session.Add("outXML", outXML);

        return outXML;
    }
    
    #endregion

    #region extractData 

    private void extractData(string resultXML)
    {
        int ctr = 0;
        int recordCount = 0;

        tblLogPref.BorderWidth = 0;

        //creates the static column header
        TableRow tr_header = new TableRow();
        tr_header.CssClass = "tableHeader";
        TableCell td_header = new TableCell();
        td_header.CssClass = "tableHeader";
        td_header.Font.Bold = true;
        td_header.Font.Size = 8;
        td_header.HorizontalAlign = HorizontalAlign.Center;
        td_header.Text = obj.GetTranslatedText("Module Type");//FB 1830 - Translation
        tr_header.Cells.Add(td_header);

        TableCell td_space = new TableCell();
        td_space.CssClass = "tableHeader";
        td_space.Width = Unit.Percentage(4);
        td_space.Text = " ";
        tr_header.Cells.Add(td_space);

        td_header = new TableCell();
        td_header.CssClass = "tableHeader";
        td_header.Font.Bold = true;
        td_header.Font.Size = 8;
        td_header.HorizontalAlign = HorizontalAlign.Center;
        td_header.Text = obj.GetTranslatedText("Event level to be logged");//FB 1830 - Translation

        tr_header.Cells.Add(td_header);

        td_space = new TableCell();
        td_space.CssClass = "tableHeader";
        tr_header.Cells.Add(td_space);

        td_header = new TableCell();
        td_header.CssClass = "tableHeader";
        td_header.Font.Bold = true;
        td_header.Font.Size = 8;
        td_header.HorizontalAlign = HorizontalAlign.Center;
        td_header.Text = obj.GetTranslatedText("Overwrite events older than");//FB 1830 - Translation

        tr_header.Cells.Add(td_header);

        td_space = new TableCell();
        td_space.CssClass = "tableHeader";
        tr_header.Cells.Add(td_space);

        td_header = new TableCell();
        td_header.CssClass = "tableHeader";
        td_header.Font.Bold = true;
        td_header.Font.Size = 8;
        td_header.HorizontalAlign = HorizontalAlign.Center;
        td_header.Text = obj.GetTranslatedText("Delete Log");//FB 1830 - Translation

        tr_header.Cells.Add(td_header);

        //add the static column header
        tblLogPref.Rows.Add(tr_header);

        //read from the return XML 
        XmlDocument logPrefXML = new XmlDocument();

        logPrefXML.LoadXml(resultXML);

        XmlNodeList logPrefNL = logPrefXML.SelectNodes("//getLogPreferences/module");
        recordCount = logPrefNL.Count;

        moduleName = new string[recordCount];
        logLevel = new string[recordCount];
        logLife = new string[recordCount];

        lstLogLevel = new DropDownList[recordCount];
        lstLogLife = new DropDownList[recordCount];

        if (!IsPostBack)
        {
            Severity.Items.Add(new ListItem(obj.GetTranslatedText("System"), "0"));//FB2272
            Severity.Items.Add(new ListItem(obj.GetTranslatedText("User"), "1"));
            Severity.Items.Add(new ListItem(obj.GetTranslatedText("Warning"), "2"));
            Severity.Items.Add(new ListItem(obj.GetTranslatedText("Information"), "3"));
            Severity.Items.Add(new ListItem(obj.GetTranslatedText("Debug"), "9"));
            Severity.Items[0].Selected = true;
        }

        if (!IsPostBack)
            moduleType.Items.Add(new ListItem("Any", ""));

        foreach (XmlNode node in logPrefNL)
        {
            moduleName[ctr] = node.SelectSingleNode("moduleName").InnerText;
            logLevel[ctr] = node.SelectSingleNode("logLevel").InnerText;
            logLife[ctr] = node.SelectSingleNode("logLife").InnerText;
            //creates the two drop down list for log level and log life

            lstLogLevel[ctr] = new DropDownList();
            lstLogLevel[ctr].Items.Add(new ListItem(obj.GetTranslatedText("Sytem Errors"), "0"));
            lstLogLevel[ctr].Items.Add(new ListItem(obj.GetTranslatedText("System and User Errors"), "1"));
            lstLogLevel[ctr].Items.Add(new ListItem(obj.GetTranslatedText("Warnings (and all above)"), "2"));
            lstLogLevel[ctr].Items.Add(new ListItem(obj.GetTranslatedText("Information (and all above)"), "3"));
            lstLogLevel[ctr].Items.Add(new ListItem(obj.GetTranslatedText("Debug (and all above)"), "9"));
            lstLogLevel[ctr].CssClass = "altSelectFormat";
            //lstLogLevel[ctr].ID = logName + "Level" + rowCount.ToString();

            //creates the row dynamically for each module
            lstLogLife[ctr] = new DropDownList();
            lstLogLife[ctr].Items.Add(new ListItem(obj.GetTranslatedText("Off"), "0"));
            lstLogLife[ctr].Items.Add(new ListItem(obj.GetTranslatedText("1 day"), "1"));
            lstLogLife[ctr].Items.Add(new ListItem(obj.GetTranslatedText("3 days"), "3"));
            lstLogLife[ctr].Items.Add(new ListItem(obj.GetTranslatedText("7 days"), "7"));
            lstLogLife[ctr].CssClass = "altSelectFormat";
            //lstLogLife[ctr].ID = logName + "Life" + rowCount.ToString();

            constructTable(ctr, moduleName[ctr], logLevel[ctr], logLife[ctr]);

            if (!IsPostBack)
            {
                moduleType.Items.Add(new ListItem(moduleName[ctr], moduleName[ctr]));
            }
            ctr++;
        }

        Button delAllButton = new Button();
        delAllButton.Text = obj.GetTranslatedText("Delete All");//FB 1830 - Translation
        delAllButton.CssClass = "altMedium0BlueButtonFormat";
        delAllButton.ID = "DeleteAll";
        delAllButton.OnClientClick = "deleteLog(\"\", 1)";

        TableRow tr = new TableRow();
        TableCell td = new TableCell();
        td.ColumnSpan = 6;
        td.RowSpan = 2;
        td.HorizontalAlign = HorizontalAlign.Center;
        td.Text = "&nbsp;";
        tr.HorizontalAlign = HorizontalAlign.Center;
        tr.Cells.Add(td);

        td = new TableCell();
        td.Controls.Add(delAllButton);

        tr.Cells.Add(td);

        tblLogPref.Rows.Add(tr);
        if (!IsPostBack)
        {
            TimeSpan sevenDays = new TimeSpan(7, 0, 0, 0);
            startDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now.Subtract(sevenDays));
            endDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now);
        }
    }

    #endregion
     
    #region constructTable 

    private void constructTable(int rowCount, string logName, string logLevel, string logLife)
    {
        TableRow tr = new TableRow();

        TableCell td = new TableCell();
        td.CssClass = "tableBody"; 
        td.Text = logName;
        td.HorizontalAlign = HorizontalAlign.Center;
        tr.Cells.Add(td);

        TableCell cellSpace = new TableCell();
        cellSpace.Width = Unit.Percentage(2);
        cellSpace.Text = " ";
        tr.Cells.Add(cellSpace);

        td = new TableCell();
        td.HorizontalAlign = HorizontalAlign.Center;

        //set the selected value
        foreach (ListItem li in lstLogLevel[rowCount].Items)
        {
            if (logLevel == li.Value)
            {
                li.Selected = true;
            }
        }

        //add the Log Level dropdown list to the cell
        td.Controls.Add(lstLogLevel[rowCount]);

        tr.Cells.Add(td);

        cellSpace = new TableCell();
        tr.Cells.Add(cellSpace);

        td = new TableCell();
        td.HorizontalAlign = HorizontalAlign.Center;

        //set the selected value
        foreach (ListItem li in lstLogLife[rowCount].Items)
        {
            if (logLife == li.Value)
            {
                li.Selected = true;
            }
        }

        //add the Log Life dropdown list to the cell
        td.Controls.Add(lstLogLife[rowCount]);

        tr.Cells.Add(td);

        cellSpace = new TableCell();
        tr.Cells.Add(cellSpace);

        td = new TableCell();

        delButton = new Button();
        delButton.Text = obj.GetTranslatedText("Delete");//FB 1830 - Translation
        delButton.CssClass = "altMedium0BlueButtonFormat";
        delButton.ID = logName;
        delButton.OnClientClick = "deleteLog(\"" + logName + "\", 0)";

        td.HorizontalAlign = HorizontalAlign.Center;
        td.Controls.Add(delButton);

        tr.Cells.Add(td);

        tblLogPref.Rows.Add(tr);

    }

    #endregion

    #region btnSubmit_Click 

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        updateLogPref();
    }

    #endregion

    #region searchSubmit_Click 

    protected void searchSubmit_Click(object sender, EventArgs e)
    {
        if (validateSeverity.IsValid)
        {
            searchLog();
        }
    }

    #endregion

    #region validateSeverity_ServerValidate 

    protected void validateSeverity_ServerValidate(object source, ServerValidateEventArgs args)
    {
        int checkSel = 0;
        foreach (ListItem severityEvent in Severity.Items)
        {

            if (severityEvent.Selected == true)
            {

                checkSel += 1;
            }
        }

        if (checkSel > 0)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }

    }

    #endregion

    #region logRetSubmit_Click 

    protected void logRetSubmit_Click(object sender, EventArgs e)
    {
        string logFilePath = "";
        string logFileName = "";
        string strScript = "";
        DateTime lgDate = DateTime.Now;
        StringBuilder sb = null;
        StringWriter sw = null;
        try
        {

            RegistryKey hklm = null;

            if(logDate.Text == "")
                throw new Exception("Please select a date.");

            lgDate = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(logDate.Text));

            if (Servicesnames.SelectedValue == "0")
            {
                hklm = Registry.LocalMachine;
                hklm = hklm.OpenSubKey(@"System\CurrentControlSet\Services\myVRMEmailService");
                if (hklm == null) //FB 1830 - Translation
                {
                    throw new Exception("EmailService is not installed.");

                }
                email_Log = Path.GetDirectoryName(hklm.GetValue("ImagePath").ToString());

                logFileName = "EmailLog_" + lgDate.Year.ToString() + lgDate.Month.ToString() + lgDate.Day.ToString() + ".log";
                logFilePath = email_Log + "\\VRMSchemas\\EmailLogs\\" + logFileName;

            }
            else
            {
                hklm = Registry.LocalMachine;
                hklm = hklm.OpenSubKey(@"System\CurrentControlSet\Services\myVRMService");
                if (hklm == null) //FB 1830 - Translation
                {
                    throw new Exception("Service is not installed.");

                }
                RTC_Log = Path.GetDirectoryName(hklm.GetValue("ImagePath").ToString());

                logFileName = "VRMRTCLog_" + lgDate.Year.ToString() + lgDate.Month.ToString() + lgDate.Day.ToString() + ".log";
                logFilePath = RTC_Log + "\\VRMSchemas\\RTCLogs\\" + logFileName;

            }

            if (File.Exists(logFilePath))
            {



                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename="+ logFileName);
                Response.Charset = "";

                sb = new StringBuilder(File.ReadAllText(logFilePath));
                sw = new StringWriter(sb);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                sw.Close();
                sw.Dispose();
                sb = null;

                //strScript = "<script language=JavaScript>";
                //strScript += "window.open('" + logFilePath + "','','width=600,height=400,resizable=yes,scrollbars=yes,status=no');";
                //strScript += "</script>";

                //if (!Page.IsStartupScriptRegistered("openLog"))
                //{
                //    Page.RegisterStartupScript("openLog", strScript);
                //}

            }
            else
                throw new Exception("Sorry the required log file is not found.");


        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("logRetSubmit_Click" + ex.Message);
            errLabel.Text = ex.Message.ToString();
            errLabel.Visible = true;
            //errLabel.Text = obj.ShowSystemMessage();
        }
        finally
        {
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
            if(sb != null)
                sb = null;
 
        }
    }
    #endregion

    #region  GetSiteDiagnostics 
    protected void GetSiteDiagnostics()
    {
        try
        {
            //    String inXML = "";
            //    inXML += "<SiteDiagnostics>";
            //    inXML += "  <UserID>11</UserID>";
            //    inXML += "</SiteDiagnostics>";
            //    //String outXML = com.CallMyVRMServer("GetSiteDiagnostics", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            //    String outXML = "";
            //    outXML += "<SiteDiagnostics>";
            //    outXML += "     <Database>123</Database>";
            //    outXML += "     <ASPIL>123</ASPIL>";
            //    outXML += "     <MYVRM>123</MYVRM>";
            //    outXML += "     <RTC>123</RTC>";
            //    outXML += "</SiteDiagnostics>";
            //    if (outXML.IndexOf("<error>") >= 0)
            //    {
            //        lblMessage.Text = com.ShowErrorMessage(outXML);
            //        lblMessage.Visible = true;
            //    }
            //    else
            //    {
            //        XmlDocument xmldoc = new XmlDocument();
            //        xmldoc.LoadXml(outXML);
            //        lblDatabaseVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/Database").InnerText;
            //        lblASPILVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/ASPIL").InnerText;
            //        //lblMyVRMVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/MYVRM").InnerText;
            //        String filePath = "../bin/v1.8.3.dll";
            //        FileVersionInfo fleVersion;

            //        if (File.Exists(filePath))
            //        {
            //            fleVersion = FileVersionInfo.GetVersionInfo(filePath);
            //            lblMyVRMVersion.Text = fleVersion.FileMajorPart + "." + fleVersion.FileMinorPart;
            //        }
            //        else
            //            lblMessage.Text = "Error Reading file";
            //        lblRTCVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/RTC").InnerText;
            //    }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("GetSiteDiagnostics" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            //errLabel.Text = ex.StackTrace;
        }
    }

    #endregion

}
