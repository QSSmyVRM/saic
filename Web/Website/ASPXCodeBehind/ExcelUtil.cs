/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;
using myVRM.ExcelXmlWriter;

/// <summary>
/// Summary description for ExcelUtil
/// </summary>

public class ExcelUtil
{
    #region public constructors

    public ExcelUtil()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #endregion

    #region private data members

    private WorksheetStyle boldTextStyle;
    private WorksheetStyle boldHeaderTextStyle;
    private WorksheetStyle numberStyle ;
    private WorksheetStyle DefaultStyle;
    private WorksheetStyle RemarksStyle;
    #endregion

    #region CreateXLFile
    /// <summary>
    /// This uses ExcelXmlWriter assembly to generate an excel
    /// If the input dataset is not null and the count = 3, Create an excel workBook
    /// In that, it will first bind the summary data/sumamry  in the 
    /// 1 Sheet. Summary data is available in ds(1).
    /// Then for each EntityCode in  arrEntities it will get the details for that entitycode from
    /// the details table (ds[0]) and generate details/summary part in the separate sheets for each entity
    /// Save the excelfile using DestFile path
    /// </summary>
    /// <param name="arrEntities">List  of all/single entitycode(s) that has been selected</param>
    /// <param name="DestFile">Path + fileName where the generated excel has to be saved</param>
    /// <param name="ds">Has 3 tables.ds[0] - Contains Details data,ds[1] - Contains Summary data,
    ///     ds[2] contains summary data that has to be displayed at the bottom of each sheet </param>

    public void CreateXLFile(ArrayList arrEntities, String DestFile, System.Data.DataSet ds)
    {
        Workbook book = null;
        Worksheet sheet = null;
        String sheetName = "";
        DataTable detailsTable = null;
        DataTable summartyDetailsTable = null;
        DataTable summaryTable = null;
        DataRow[] filteredRows = null;
        try
        {
            if (ds != null)
            {
                if (ds.Tables.Count == 3)
                {
                    detailsTable = ds.Tables[0];
                    summaryTable = ds.Tables[1];
                    summartyDetailsTable = ds.Tables[2];
                    

                    book = new Workbook();
                    BuildStyle(ref book);

                    //To bind Summary Sheet in a book
                    sheetName = "Summary";
                    sheet = book.Worksheets.Add(sheetName);
                    if (summaryTable != null)
                    {
                        filteredRows = summaryTable.Select("");
                        if (filteredRows != null)
                        {
                            if (filteredRows.Length > 0)
                            {
                                DumpData(filteredRows, ref sheet);

                                DumpSummaryforSummarySheet(summaryTable, summartyDetailsTable, ref sheet);
                            }

                        }
                    }

                    sheet = null;

                    //To bind sheets for each Entity Code in a book

                    foreach (String entityCode in arrEntities)
                    {
                        if (entityCode == "")
                            sheetName = "Need Entity Code";
                        else
                            sheetName = entityCode;
                        sheet = book.Worksheets.Add(sheetName);

                        filteredRows = detailsTable.Select("[Entity Code]='" + entityCode + "'");
                        if (filteredRows != null)
                        {
                            if (filteredRows.Length > 0)
                            {
                                DumpData(filteredRows, ref sheet);
                                filteredRows = null;
                                filteredRows = summartyDetailsTable.Select("[Entity Code]='" + entityCode + "'");
                                if (filteredRows != null)
                                {
                                    if (filteredRows.Length > 0)
                                    {
                                        DumpSummaryforCodeSheets(filteredRows[0], ref sheet, entityCode);
                                    }
                                }
                            }
                        }

                        sheet = null;
                        
                    }
                }

                //To save the book
                if (book != null)
                    book.Save(DestFile);

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region CreateXLFile 

    public void CreateXLFile(Hashtable logList, String DestFile, System.Data.DataSet ds)
    {
        Workbook book = null;
        Worksheet sheet = null;
        String sheetName = "";
        DataRow[] filteredRows = null;
        try
        {
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    book = new Workbook();

                    BuildStyle(ref book);

                    if (logList != null)
                    {
                        Int32 i = 0;
                        foreach(String key in logList.Keys)
                        {   
                            sheetName = "";
                            DataTable eventTable = ds.Tables[key];
                            sheetName = logList[key].ToString();
                            sheet = book.Worksheets.Add(sheetName);

                            filteredRows = eventTable.Select("");
                            if (filteredRows != null)
                            {
                                if (filteredRows.Length > 0)
                                {
                                    DumpData(filteredRows, ref sheet);
                                }
                            }

                            i++;
                        }
                    }

                    //To save the book
                    if (book != null)
                        book.Save(DestFile);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region DumpData
    /// <summary>
    /// 
    /// </summary>
    /// <param name="filteredRows"></param>
    /// <param name="doFillHeader"></param>
    /// <param name="sheet"></param>

    private void DumpData(DataRow[] filteredRows, ref Worksheet sheet)
    {
        WorksheetRow row;
        WorksheetColumn wkSheetCol;
        WorksheetColumnCollection wsColColl;
        if (filteredRows != null)
        {
            DataColumnCollection colCollection = filteredRows[0].Table.Columns;
            row = sheet.Table.Rows.Add();
            row.AutoFitHeight = true;

            //Header
            for (int iCol = 0; iCol < colCollection.Count; iCol++)
            {
                WorksheetCell cell = new WorksheetCell(colCollection[iCol].ColumnName, "boldHeaderTextStyle");
                row.Cells.Add(cell);
            }
            sheet.Options.SplitHorizontal = 1;
            sheet.Options.FreezePanes = true;
            sheet.Options.TopRowBottomPane = 1;

            //Data
            foreach (DataRow dr in filteredRows)
            {
                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;
                for (int iCol = 0; iCol < colCollection.Count; iCol++)
                {
                    if(dr[iCol].GetType().ToString() == "System.Int32")
                        row.Cells.Add(dr[iCol].ToString(),DataType.Number,"NumberStyle");
                    else 
                        row.Cells.Add(dr[iCol].ToString(),DataType.String,"DefaultStyle");
                   
                }
            }
            wsColColl = sheet.Table.Columns;
            for (int iCol = 1; iCol < colCollection.Count; iCol++)
            {
                
                wkSheetCol = wsColColl.Add(120);
                wkSheetCol.Index = iCol;
                wkSheetCol.AutoFitWidth = true;
            }
            
        }
    }

    #endregion


    #region DumpSummaryforCodes
    /// <summary>
    /// 
    /// </summary>
    /// <param name="dr"></param>
    /// <param name="sheet"></param>
    /// <param name="entity"></param>
    /// <param name="dt"></param>
    /// <param name="codeType"></param>

    private void DumpSummaryforCodeSheets(DataRow dr, ref Worksheet sheet, String entity)
    {
        WorksheetRow row;
        try
        {
            ArrayList summaryList = GetSummaryArray("Detail");

            //sheet.Table.DefaultColumnWidth = 20;
            for (int i = 0; i < 3; i++)
                row = sheet.Table.Rows.Add();

            row = sheet.Table.Rows.Add();
            row.AutoFitHeight = true;

            for (int i = 0; i < 2; i++)
                AddEmptyCell(ref row);

            //To add the First row of the Summary Details of an entity/Summary Sheets which contains VTC Totals Minutes
            //and Report Title 
            row.Cells.Add(new WorksheetCell("VTC Totals", "BoldTextStyle"));
            row.Cells.Add(dr["Minutes"].ToString(), DataType.Number, "NumberStyle");
            row.Cells.Add(new WorksheetCell("Minutes", "BoldTextStyle"));

            row.Cells.Add(new WorksheetCell((String.Format("Report Title : {0}", entity)), "BoldTextStyle"));

            //To add the second row of the Summary Details of an entity/Summary Sheets which contains VTC Totals Hours
            row = sheet.Table.Rows.Add();
            row.AutoFitHeight = true;

            for (int i = 0; i < 3; i++)
                AddEmptyCell(ref row);

            row.Cells.Add(dr["Hours"].ToString(),DataType.Number,"NumberStyle");

            row.Cells.Add(new WorksheetCell("Hours", "boldHeaderTextStyle"));


            row = sheet.Table.Rows.Add();

            //To add the subsequent rows of the Summary Details of an entity/Summary Sheets which gets
            // the caption from ArrayList
            foreach (String str in summaryList)
            {
                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;

                for (int i = 0; i < 4; i++)
                    AddEmptyCell(ref row);

                row.Cells.Add(new WorksheetCell(str, "BoldTextStyle"));


                row.Cells.Add(dr[str].ToString(),DataType.Number,"NumberStyle");

                if (summaryList.IndexOf(str) > 2)
                    row.Cells.Add("Hours");

            }

            row = sheet.Table.Rows.Add();
            row.AutoFitHeight = true;

            for (int i = 0; i < 4; i++)
                AddEmptyCell(ref row);

            row.Cells.Add("Internal Meetings");
            row.Cells.Add("0", DataType.Number, "NumberStyle");
            row.Cells.Add("Meetings");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region DumpSummaryforSummarySheet
    /// <summary>
    /// For VTC Totals, display sum the columns of all the rows in the summaryTable and append the row to the ref sheet
    /// For other totals,get the total from summaryDetailsTable for each param and append the rows to the ref sheet
    /// </summary>
    /// <param name="summaryTable"></param>
    /// <param name="summaryDetailsTable"></param>
    /// <param name="sheet"></param>

    private void DumpSummaryforSummarySheet(DataTable summaryTable, DataTable summaryDetailsTable, ref Worksheet sheet)
    {
        WorksheetRow row = null;
        try
        {
            //To display VTC Totals Row
            if (summaryTable != null)
            {
                for (int i = 0; i < 3; i++)
                    row = sheet.Table.Rows.Add();

                foreach (DataColumn col in summaryTable.Columns)
                {
                    //To write "VTC Totals" instead of Entity_code in the 1st position
                    if (col.Ordinal == 0)
                        row.Cells.Add(new WorksheetCell("VTC Totals", "BoldTextStyle"));
                    else
                        row.Cells.Add(summaryTable.Compute("Sum([" + col.ColumnName + "])", "").ToString(), DataType.Number, "NumberStyle");

                }

            }
            //To display Other Summary Rows which gets from Summary
            ArrayList paramList = GetSummaryArray("Summary");
            if (paramList != null)
            {
                foreach (String str in paramList)
                {
                    row = sheet.Table.Rows.Add();
                    //row.Cells.Add(str, DataType.String, "boldTextStyle");

                    row.Cells.Add(new WorksheetCell(str, "BoldTextStyle"));
                    if (summaryDetailsTable.Columns.Contains(str))
                        row.Cells.Add(summaryDetailsTable.Compute("Sum([" + str + "])", "").ToString(), DataType.Number, "NumberStyle");
                    //row.Cells.Add(summaryDetailsTable.Compute("Sum([" + str + "])", "").ToString());
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    #endregion

    #region BuildStyle
    /// <summary>
    /// Builds the style for excel rows/cells that has been used to format the sheet(s).
    /// </summary>
    private void BuildStyle(ref Workbook book)
    {
        try
        {

            //To assign the bold effect/left alignment for Summary Texts
            boldTextStyle = book.Styles.Add("BoldTextStyle");
            boldTextStyle.Font.Bold = true;
            
            boldHeaderTextStyle = book.Styles.Add("boldHeaderTextStyle");
            boldHeaderTextStyle.Font.Bold = true;
            boldHeaderTextStyle.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            
            //To assign the right alignment for Summary Number values
            numberStyle = book.Styles.Add("NumberStyle");
            numberStyle.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            
            DefaultStyle = book.Styles.Add("DefaultStyle");
            DefaultStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            
            RemarksStyle = book.Styles.Add("RemarksStyle");
            RemarksStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            RemarksStyle.Alignment.WrapText = true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region AddEmptyCell

    private void AddEmptyCell(ref WorksheetRow row)
    {
        row.Cells.Add("");
    }

    #endregion

    #region GetSummaryArray

    private ArrayList GetSummaryArray(String sheetType)
    {
        ArrayList aList = new ArrayList();

        aList.Add("Pt-Pt Meetings");
        aList.Add("Multi-Pt Meetings");
        aList.Add("VTC Meeting Totals");
        if (sheetType == "Summary")
        {
            aList.Add("Internal Meetings");
            aList.Add("LiveBoard Meetings");
        }
        else
        {
            aList.Add("ISDN Outbound Totals");
            aList.Add("Inbound ISDN Totals");
            aList.Add("ViIP Totals");
            aList.Add("MCI Totals");
        }
        return aList;

    }

    #endregion

    
}



