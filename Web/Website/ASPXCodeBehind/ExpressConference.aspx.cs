/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.Services;

namespace en_ExpressConference
{
    public partial class ExpressConference : System.Web.UI.Page
    {
        #region Private Data Members
        Boolean isSetCustom = false;

        protected System.Web.UI.ScriptManager ConfScriptManager;

        protected System.Web.UI.WebControls.TextBox hdnApprover4;
        protected System.Web.UI.WebControls.TextBox hdnApprover7; //FB 2501
        protected System.Web.UI.WebControls.TextBox hdnApproverMail;//FB 2501
        protected System.Web.UI.WebControls.TextBox hdnRequestorMail;//FB 2501 
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurringText;
        protected System.Web.UI.WebControls.DataGrid dgConflict;
        protected System.Web.UI.WebControls.Table tblConflict;
        protected System.Web.UI.WebControls.TextBox txtApprover4;
        protected System.Web.UI.WebControls.TextBox txtApprover7; //FB 2501
        protected System.Web.UI.WebControls.TextBox confEndDate;
        protected System.Web.UI.WebControls.TextBox confStartDate;
        protected System.Web.UI.WebControls.TextBox txtModifyType;
        protected System.Web.UI.WebControls.TextBox ConferenceName;
        protected System.Web.UI.WebControls.TextBox ConferenceDescription;
        protected System.Web.UI.WebControls.TextBox txtPartysInfo;
        protected System.Web.UI.WebControls.TextBox txtUsersStr;
        protected System.Web.UI.WebControls.TextBox txtTimeCheck;
        protected System.Web.UI.WebControls.Button btnUploadFiles;
        protected AjaxControlToolkit.ModalPopupExtender ModalPopupExtender1;
        protected AjaxControlToolkit.ModalPopupExtender RoomPopUp;
        protected AjaxControlToolkit.ModalPopupExtender SubModalPopupExtender;
        protected System.Web.UI.WebControls.Label showConfMsg;
        protected System.Web.UI.UpdatePanel UpdatePanel1;
        protected System.Web.UI.UpdatePanel UpdatePanelRooms;
        protected System.Web.UI.WebControls.Button btnConfSubmit;
        protected System.Web.UI.WebControls.Button btnDummy; //FB 1830 Email Edit
        protected System.Web.UI.WebControls.Label aFileUp;
        protected System.Web.UI.WebControls.Label plblGuestLocation;//FB 2426
        protected System.Web.UI.WebControls.TextBox RecurDurationhr;
        protected System.Web.UI.WebControls.TextBox RecurDurationmi;
        protected System.Web.UI.WebControls.TextBox EndText;
        protected System.Web.UI.WebControls.RadioButtonList RecurType;
        protected System.Web.UI.WebControls.RadioButton DEveryDay;
        protected System.Web.UI.WebControls.TextBox DayGap;
        protected System.Web.UI.WebControls.RadioButton DWeekDay;
        protected System.Web.UI.WebControls.Panel Daily;
        protected System.Web.UI.WebControls.TextBox WeekGap;
        protected System.Web.UI.WebControls.CheckBoxList WeekDay;
        protected System.Web.UI.WebControls.Panel Weekly;
        protected System.Web.UI.WebControls.RadioButton MEveryMthR1;
        protected System.Web.UI.WebControls.TextBox MonthDayNo;
        protected System.Web.UI.WebControls.TextBox MonthGap1;
        protected System.Web.UI.WebControls.RadioButton MEveryMthR2;
        protected System.Web.UI.WebControls.DropDownList MonthWeekDayNo;
        protected System.Web.UI.WebControls.DropDownList MonthWeekDay;
        protected System.Web.UI.WebControls.TextBox MonthGap2;
        protected System.Web.UI.WebControls.Panel Monthly;
        protected System.Web.UI.WebControls.RadioButton YEveryYr1;
        protected System.Web.UI.WebControls.DropDownList YearMonth1;
        protected System.Web.UI.WebControls.TextBox YearMonthDay;
        protected System.Web.UI.WebControls.RadioButton YEveryYr2;
        protected System.Web.UI.WebControls.DropDownList YearMonthWeekDayNo;
        protected System.Web.UI.WebControls.DropDownList YearMonthWeekDay;
        protected System.Web.UI.WebControls.DropDownList YearMonth2;
        protected System.Web.UI.WebControls.Panel Yearly;
        protected System.Web.UI.WebControls.ListBox CustomDate;
        protected System.Web.UI.WebControls.Button btnsortDates;
        protected System.Web.UI.WebControls.Panel Custom;
        protected System.Web.UI.WebControls.TextBox StartDate;
        protected System.Web.UI.WebControls.RadioButton EndType;
        protected System.Web.UI.WebControls.RadioButton REndAfter;
        protected System.Web.UI.WebControls.TextBox Occurrence;
        protected System.Web.UI.WebControls.RadioButton REndBy;
        protected System.Web.UI.WebControls.TextBox EndDate;
        protected System.Web.UI.WebControls.Button Cancel;
        protected System.Web.UI.WebControls.Button Reset;
        protected System.Web.UI.WebControls.Button RecurSubmit;
        protected System.Web.UI.WebControls.Button RecurSubmit1;
        protected System.Web.UI.HtmlControls.HtmlTableRow RangeRow;
        protected System.Web.UI.WebControls.DropDownList RecurSetuphr;
        protected System.Web.UI.WebControls.DropDownList RecurSetupmi;
        protected System.Web.UI.WebControls.DropDownList RecurSetupap;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownhr;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownmi;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownap;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRecurValue;

        protected MetaBuilders.WebControls.ComboBox confStartTime;
        protected MetaBuilders.WebControls.ComboBox confEndTime;
        protected MetaBuilders.WebControls.ComboBox lstDuration;
        protected MetaBuilders.WebControls.ComboBox startByTime;
        protected MetaBuilders.WebControls.ComboBox completedByTime;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetupTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBufferStr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTeardownTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAVParamState;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnextusrcnt;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnconftype;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Recur;
        protected System.Web.UI.HtmlControls.HtmlInputHidden confPassword;
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurFlag;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.WebControls.Button btnCompare;
        protected System.Web.UI.WebControls.Panel pnlNoData;

        protected System.Web.UI.WebControls.Label plblSetupDTime;
        protected System.Web.UI.WebControls.Label plblTeardownDTime;
        protected System.Web.UI.WebControls.Label lblTeardownDateTime;
        protected System.Web.UI.WebControls.Label lblSetupDateTime;
        protected System.Web.UI.WebControls.Label lblConfHeader;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        //FB 2634
        //protected System.Web.UI.WebControls.TextBox SetupDate;
        //protected System.Web.UI.WebControls.TextBox TearDownDate;
        //protected MetaBuilders.WebControls.ComboBox SetupTime;
        //protected MetaBuilders.WebControls.ComboBox TeardownTime;
        //protected System.Web.UI.WebControls.TextBox SetupDateTime;
        //protected System.Web.UI.WebControls.TextBox TearDownDateTime;
        //protected System.Web.UI.WebControls.RegularExpressionValidator regTearDownStartTime;
        //protected System.Web.UI.WebControls.RegularExpressionValidator regSetupStartTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqStartTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqStartDate;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqEndTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqEndDate;
        protected System.Web.UI.WebControls.RegularExpressionValidator regStartTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEndTime;
        protected System.Web.UI.WebControls.Table tblHost;
        protected System.Web.UI.WebControls.Table tblAudioServices;
        protected System.Web.UI.WebControls.Table tblSpecial;
        protected System.Web.UI.WebControls.Label lblExpHead; // FB 2570
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblConfID;
        protected System.Web.UI.WebControls.Label lblUpload1;
        protected System.Web.UI.WebControls.Label hdnUpload1;
        protected System.Web.UI.WebControls.Button btnRemove1;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload1;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtHasCalendar;
        protected System.Web.UI.WebControls.Label lblUpload2;
        protected System.Web.UI.WebControls.Label hdnUpload2;
        protected System.Web.UI.WebControls.Button btnRemove2;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload3;
        protected System.Web.UI.WebControls.Label lblUpload3;
        protected System.Web.UI.WebControls.Label hdnUpload3;
        protected System.Web.UI.WebControls.Button btnRemove3;
        protected System.Web.UI.WebControls.DropDownList lstConferenceTZ;
        protected System.Web.UI.WebControls.ListBox RoomList; //FB 2367
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;//FB 2367
        protected System.Web.UI.HtmlControls.HtmlGenericControl ifrmPartylist;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConfCode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLeaderPin;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblConfcode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblLeaderPin;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRegConfCode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRegLeaderPin;
        protected System.Web.UI.HtmlControls.HtmlTableRow trsplinst; //FB 2349
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUProfile;//FB 3063
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        protected String client = "";
        protected String enableEntity = "";
        protected string enableaudiobridge = "0";//FB 2443

        protected String isInstanceEdit = "";
        protected String isCustomEdit = "";
        protected String enableBufferZone = "";
        protected string partysInfo;
        public string strConfStartTime;
        public string strConfEndTime;
        protected Int32 CustomSelectedLimit = 100;
        protected String timeZone = "0";

        MyVRMNet.Util utilObj = null; //FB 2236
        protected ns_Logger.Logger log = null;
        private myVRMNet.NETFunctions obj = null;
        protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;
        protected System.Web.UI.WebControls.TreeView treeRoomSelection;
        protected static string selRooms;

        protected System.Web.UI.WebControls.Panel pnlLevelView;
        protected System.Web.UI.WebControls.Panel pnlListView;
        protected System.Web.UI.WebControls.RadioButtonList rdSelView;

        protected System.Web.UI.WebControls.Button btnRoomSelect;

        protected System.Web.UI.WebControls.Label lblFileList;
        protected System.Web.UI.WebControls.RadioButtonList RdListAudioPartys;
        protected System.Web.UI.WebControls.TextBox txtNoLines;
        protected System.Web.UI.WebControls.TextBox txtAudioDialNo;
        protected System.Web.UI.WebControls.TextBox txtConfCode;
        protected System.Web.UI.WebControls.TextBox txtLeaderPin;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBridgeId;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAudioInsIDs;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHostIDs;
        protected System.Web.UI.WebControls.RadioButtonList lstAudioParty;//FB 2341(d)
        protected System.Web.UI.WebControls.Label lblConfCode;
        protected System.Web.UI.WebControls.Label lblLeaderPin;
        protected String enableConferenceCode = "0";
        protected String enableLeaderPin = "0";
        private String strRoomName = "";
        private String strRoomIds = "";
        myVRMNet.CustomAttributes CAObj = null;
        private string custControlIDs = "";
        private bool isFormSubmit = false;
        private string audioDialString = "";
        private Hashtable usrEndpoints = null;
        private string isCOMError = "";
        int audioPartyId = 0;
        protected String language = "";//FB 1830
		 //FB 1716   
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDuration;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChange;        

        StringDictionary xConfInfo; //FB 1830 Email Edit - start
        string xconfpassword = "";
        DateTime xconfsetup;
        DateTime xconftear;
        //public string emailAlertMes = "";
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnemailalert;
        public string isEditMode = "";
        List<int> xPartys;  //FB 1830 Email Edit - end
		String[] pipeDelim = { "||" }; //FB 1888
        String[] ExclamDelim = { "!!" };//FB 1888
        String[] DoublePlusDelim = { "++" };//FB 2367
        //FB 1911
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurSpec;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSpecRec;
        protected System.Web.UI.HtmlControls.HtmlControl divConcSupport;//FB 2341
        protected System.Web.UI.HtmlControls.HtmlControl divAudioConf;
        //protected CheckBoxList ChklstConcSupport;//FB 2377
        protected System.Web.UI.WebControls.CheckBox chkStartNow;//FB 2341
		//FB 2620 Starts
        //protected System.Web.UI.WebControls.CheckBox chkVMR;//FB 2376
		protected System.Web.UI.WebControls.DropDownList lstVMR;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden isVMR;//FB 2376  
		//FB 2620 End
        protected System.Web.UI.HtmlControls.HtmlTableRow trVMR;//FB 2376
        protected System.Web.UI.WebControls.Label plblConfVMR; //FB 2376
        protected System.Web.UI.WebControls.TextBox txtintbridge;//FB 2376
        protected System.Web.UI.WebControls.TextBox txtextbridge;//FB 2376
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnintbridge;//FB 2376 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnextbridge;//FB 2376 
        protected System.Web.UI.WebControls.DropDownList lstLineRate; //FB 2394
        protected System.Web.UI.WebControls.DropDownList lstStartMode; //FB 2501
        protected Int32 OrgLineRate = 0; //FB 2429
        protected Int32 OrgSetupTime = 0, OrgTearDownTime = 0, EnableBufferZone = 0; //FB 2398
        protected System.Web.UI.HtmlControls.HtmlTable tblAudioser; //FB 2443
        // FB 2426 Starts
        protected System.Web.UI.WebControls.Button btnGuestLocation;
        protected System.Web.UI.WebControls.Image Image6;
        protected AjaxControlToolkit.ModalPopupExtender guestLocationPopup;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPreviewGuestLocation;
        protected System.Web.UI.WebControls.DropDownList lstIPlinerate;
        protected System.Web.UI.WebControls.DropDownList lstSIPlinerate;
        protected System.Web.UI.WebControls.DropDownList lstISDNlinerate;
        protected System.Web.UI.WebControls.DropDownList lstIPVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstSIPVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstISDNVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstIPConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstSIPConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstISDNConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstCountries;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestRoomID;
        
        protected System.Web.UI.WebControls.TextBox txtsiteName;
        protected System.Web.UI.WebControls.TextBox txtApprover5;
        protected System.Web.UI.WebControls.TextBox txtEmailId;
        protected System.Web.UI.WebControls.TextBox txtPhone;
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.TextBox txtState;
        protected System.Web.UI.WebControls.TextBox txtCity;
        protected System.Web.UI.WebControls.TextBox txtZipcode;
        protected System.Web.UI.WebControls.TextBox txtIPAddress;
        protected System.Web.UI.WebControls.TextBox txtIPPassword;
        protected System.Web.UI.WebControls.TextBox txtIPconfirmPassword;
        protected System.Web.UI.WebControls.TextBox txtISDNAddress;
        protected System.Web.UI.WebControls.TextBox txtISDNPassword;
        protected System.Web.UI.WebControls.TextBox txtISDNconfirmPassword;
        protected System.Web.UI.WebControls.TextBox txtSIPAddress;
        protected System.Web.UI.WebControls.TextBox txtSIPPassword;
        protected System.Web.UI.WebControls.TextBox txtSIPconfirmPassword;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestRoom;
        protected System.Web.UI.WebControls.DataGrid dgOnflyGuestRoomlist;
        protected System.Web.UI.WebControls.Button btnGuestLocationSubmit;
        protected System.Web.UI.HtmlControls.HtmlTableRow OnFlyRowGuestRoom;
        protected System.Web.UI.WebControls.RadioButton radioIsDefault;
        protected System.Web.UI.WebControls.RadioButton radioIsDefault2;
        protected System.Web.UI.WebControls.RadioButton radioIsDefault3;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestloc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetStartNow;//FB 1825
        DataTable onflyGrid = null;
        ArrayList colNames = null;
        protected System.Web.UI.WebControls.CheckBox chkPublic;//FB 2226
        protected System.Web.UI.WebControls.CheckBox chkOpenForRegistration;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPublic;

        // FB 2426 Ends

        //FB 2501 Starts        
        protected System.Web.UI.WebControls.ImageButton delConfVNOC;
        protected System.Web.UI.HtmlControls.HtmlImage imgVNOC; //FB 2608
        protected System.Web.UI.HtmlControls.HtmlImage imgdeleteVNOC; //FB 2608
        protected System.Web.UI.WebControls.TextBox ConferencePassword;
        protected System.Web.UI.WebControls.TextBox ConferencePassword2;
        protected System.Web.UI.WebControls.Panel pnlPassword1;
        protected System.Web.UI.WebControls.Panel pnlPassword2;
        public string EnableConferencePassword;
        public bool isVNOC = false;
        
		//FB 2501 Ends
        //FB 2670 START
        protected System.Web.UI.WebControls.TextBox txtVNOCOperator;
        protected System.Web.UI.WebControls.TextBox hdnVNOCOperator;
        //FB 2670 END
        protected int EnableVNOCselection = 1;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnSiteAVSupport; //FB 2632
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOCOperator;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDedicatedVNOC;

        protected System.Web.UI.WebControls.CheckBox chkFECC; // FB 2583 FECC For Express
        protected System.Web.UI.HtmlControls.HtmlTableCell tdFECC;//FB 2583 FECC For Express
        //protected System.Web.UI.HtmlControls.HtmlTableRow trFECC;//FB 2583 FECC For Express
        string EnableFECC = "";//FB 2543
        protected System.Web.UI.HtmlControls.HtmlTableRow trNetworkState;//FB 2595
        //FB 2634
        protected System.Web.UI.WebControls.TextBox SetupDuration;
        protected System.Web.UI.WebControls.TextBox TearDownDuration;
        protected System.Web.UI.HtmlControls.HtmlTableRow SetupRow;
        protected System.Web.UI.WebControls.Table tblCustomAttribute;   //FB 2592
        protected System.Web.UI.WebControls.Table tblMCUProfile;//FB 2839
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCusID; //FB 2592
        //FB 2670 START
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConciergeMonitoring;
        //protected System.Web.UI.HtmlControls.HtmlTableCell tdDedicatedVNOC;
        //FB 2670 END
		// FB 2641 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trLineRate; 
        protected System.Web.UI.HtmlControls.HtmlTableRow trStartMode;
        protected int EnableLinerate = 0;
        protected int EnableStartMode = 0;
        // FB 2641 End
		 //FB 2693 Starts
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPCConf;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdBJ;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdJB;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdLync;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdVidtel;
        protected System.Web.UI.HtmlControls.HtmlTable tblPcConf;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdBJ;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdJB;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLy;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVid;
        int PCVendorType = 0;
        //FB 2693 Ends
		//FB 2717 Starts
        protected int isCloudEnabled = 0;
        protected System.Web.UI.WebControls.CheckBox chkCloudConferencing;
		protected System.Web.UI.HtmlControls.HtmlTableRow trCloudConferencing;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMcuProfileSelected;//FB 2839
		//FB 2717 End
		//FB 2659 - Starts
        protected int enableCloudInstallation = 0;
        public System.Web.UI.HtmlControls.HtmlTable tblSeatsAvailability;
        protected System.Web.UI.HtmlControls.HtmlTableRow trseatavailable;
        //FB 2659 - End
        //FB 2870 Start
        public string EnableNumericID;
        protected System.Web.UI.WebControls.CheckBox ChkEnableNumericID;
        protected System.Web.UI.WebControls.TextBox txtNumeridID;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrCTNumericID;
        //FB 2870 End

        //FB 2998
        protected System.Web.UI.HtmlControls.HtmlTableRow MCUConnectDisplayRow;
        protected System.Web.UI.HtmlControls.HtmlTableCell ConnectCell;
        protected System.Web.UI.HtmlControls.HtmlTableCell DisconnectCell;
        protected System.Web.UI.WebControls.TextBox txtMCUConnect;
        protected System.Web.UI.WebControls.TextBox txtMCUDisConnect;
        protected System.Web.UI.WebControls.ImageButton imgSetup;
        protected System.Web.UI.WebControls.ImageButton imgMCUConnect;
        protected System.Web.UI.WebControls.ImageButton imgMCUDisconnect;
        protected System.Web.UI.WebControls.Label lblMCUConnect;
        public String mcuSetupDisplay = "";
        public String mcuTearDisplay = "";
		//FB 2993 Starts
        protected System.Web.UI.WebControls.DropDownList drpNtwkClsfxtn;
        //public System.Web.UI.HtmlControls.HtmlInputHidden hdnNetworkSwitching;
        protected int NetworkSwitching = 0;
        //FB 2993 Ends

        protected System.Web.UI.HtmlControls.HtmlTableRow trAdvancedSettings; //ZD 100166
        protected System.Web.UI.HtmlControls.HtmlTableRow trPCConf; //ZD 100166
        protected System.Web.UI.WebControls.RegularExpressionValidator regConfDisc; // ZD 100263

        #endregion

        #region Public Constructor

        public ExpressConference()
        {
            utilObj = new MyVRMNet.Util(); //FB 2236
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            selRooms = ", ";
        }
        #endregion          

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("expressconference.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                Session.Remove("Confbridge"); //ZD 100298
                if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].ToString().Equals("1"))
                {
                    if (Request.QueryString["t"] == null)
                    {
                        log.Trace("Page: ExpressConference.aspx.cs, Function: Page_Load, Reason: Querystring t is null");
                        Response.Redirect("ShowError.aspx");
                    }
                    regConfDisc.Enabled = true;
                }
                
                // ZD 100263 Ends
                //ZD 100335 start
                if (Request.Cookies["hdnscreenres"] != null)
                    Session["hdnscreenresoul"] = Request.Cookies["hdnscreenres"].Value;
                //ZD 100335 End
                if (Session["language"] == null)//FB 1830
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                // FB 2570 Starts

                if (Request.QueryString["t"].ToString() == "")//FB 1830 Email Edit
                {
                    isEditMode = "1";
                    lblExpHead.Text = "Edit Conference";
                }
                else
                {
                    isEditMode = "0";
                    lblExpHead.Text = "Express New Conference";
                }
                // FB 2570 Ends
                if (Application["roomExpandLevel"] == null)
                    Application.Add("roomExpandLevel", "1");

                if (Application["Client"] == null)//FB 2341
                    Application["Client"] = "";

                client = Application["Client"].ToString();
                //if (client.ToUpper() == "DISNEY") //FB 2359
                //{
                //    divConcSupport.Attributes.Add("style", "display:block");//FB 2341
                //    divAudioConf.Attributes.Add("style", "display:block");
                //}
                //else
                //{
                //    divConcSupport.Attributes.Add("style", "display:none");//FB 2341
                //    divAudioConf.Attributes.Add("style", "display:none");
                //}


                // FB 2608 Start
                if (Session["EnableVNOCselection"] != null)
                {
                    if (Session["EnableVNOCselection"].ToString() == "0" || Session["EnableDedicatedVNOC"].ToString() == "0")//FB 2670)
                    {
                        EnableVNOCselection = 0;
                        txtVNOCOperator.Visible = false;//FB 2670
                        imgVNOC.Visible = false;
                        imgdeleteVNOC.Visible = false;
                        tdDedicatedVNOC.Visible = false;
                    }
                    else
                    {
                        EnableVNOCselection = 1;
                        txtVNOCOperator.Visible = true;//FB 2670
                        imgVNOC.Visible = true;
                        imgdeleteVNOC.Visible = true;
                        tdDedicatedVNOC.Visible = true;
                    }
                }
                // FB 2608 End

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                strConfStartTime = confStartTime.Text;
                strConfEndTime = confEndTime.Text;
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

                if (Application["EnableBufferZone"] == null)
                {
                    Application["EnableBufferZone"] = "1";
                }

                if (Application["EnableAudioAddOn"] == null)  
                {
                    Application["EnableAudioAddOn"] = "1";
                }

                if (Application["EnableEntity"] == null)
                    Application["EnableEntity"] = "1";

                enableEntity = Application["EnableEntity"].ToString();
                enableBufferZone = Application["EnableBufferZone"].ToString();
                
                if(Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                Session["FormatDateType"] = ((Session["FormatDateType"] == null) ? "1" : Session["FormatDateType"]);
                Session["timeZoneDisplay"] = ((Session["timeZoneDisplay"] == null) ? "1" : Session["timeZoneDisplay"]);
                Application["interval"] = ((Application["interval"] == null) ? "30" : "30"); //Providea
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";

                if (Session["ConferenceCode"] != null)
                    enableConferenceCode = Session["ConferenceCode"].ToString();
                if (Session["LeaderPin"] != null)
                    enableLeaderPin = Session["LeaderPin"].ToString();


                //Modified for FB 1982 - Start
                if (enableConferenceCode == "0")
                {
                    tdlblConfcode.Attributes.Add("style", "display:none");
                    tdConfCode.Attributes.Add("style", "display:none");
                    tdRegConfCode.Attributes.Add("style", "display:none");
                }

                if (enableLeaderPin == "0")
                {
                    tdlblLeaderPin.Attributes.Add("style", "display:none");
                    tdLeaderPin.Attributes.Add("style", "display:none");
                    tdRegLeaderPin.Attributes.Add("style", "display:none");
                }
                //FB 2443 - Start
                tblAudioser.Visible = false;
                if (Session["EnableAudioBridges"].ToString() == "1")
                    tblAudioser.Visible = true;
                //FB 2443 - End
                //Modified for FB 1982 - End

                //if (lstAudioParty.SelectedValue.Equals("-1"))//FB 2341
                //{
                //    txtAudioDialNo.Enabled = false;
                //    txtConfCode.Enabled = false;
                //    txtLeaderPin.Enabled = false;
                //}

                if (Session["EnableBufferZone"] != null) //FB 2398
                    if (Session["EnableBufferZone"].ToString() != "")
                        Int32.TryParse(Session["EnableBufferZone"].ToString(), out EnableBufferZone);

                //FB 2998 - Start
                if (Session["MCUSetupDisplay"] != null)
                    mcuSetupDisplay = Session["MCUSetupDisplay"].ToString();

                if (Session["MCUTearDisplay"] != null)
                    mcuTearDisplay = Session["MCUTearDisplay"].ToString();

                if ((mcuSetupDisplay == "1" || mcuTearDisplay == "1") && EnableBufferZone == 1)
                {
                    MCUConnectDisplayRow.Visible = true;

                    if (mcuSetupDisplay == "1" && mcuTearDisplay == "1")
                        lblMCUConnect.Text = "MCU Connect / Disconnect";
                    else if (mcuSetupDisplay == "1")
                        lblMCUConnect.Text = "MCU Connect";
                    else if (mcuTearDisplay == "1")
                        lblMCUConnect.Text = "MCU Disconnect";
                }
                else
                    MCUConnectDisplayRow.Visible = false;
                //FB 2998 - End

                if (Session["OrgSetupTime"] != null) //FB 2398
                    if (Session["OrgSetupTime"].ToString() != "")
                        Int32.TryParse(Session["OrgSetupTime"].ToString(), out OrgSetupTime);

                if (Session["OrgTearDownTime"] != null)//FB 2398
                    if (Session["OrgTearDownTime"].ToString() != "")
                        Int32.TryParse(Session["OrgTearDownTime"].ToString(), out OrgTearDownTime);

                if (Session["OrgLineRate"] != null)//FB 2429
                {
                    if (!Session["OrgLineRate"].ToString().Equals(""))
                        Int32.TryParse(Session["OrgLineRate"].ToString(), out OrgLineRate);
                }

                //FB 2693 Starts
                if (Session["EnableBlueJeans"] != null)
                {
                    if (Session["EnableBlueJeans"].ToString().Equals("0"))
                        tdBJ.Attributes.Add("style", "display:none");
                }
                if (Session["EnableJabber"] != null)
                {
                    if (Session["EnableJabber"].ToString().Equals("0"))
                        tdJB.Attributes.Add("style", "display:none");
                }
                if (Session["EnableLync"] != null)
                {
                    if (Session["EnableLync"].ToString().Equals("0"))
                        tdLy.Attributes.Add("style", "display:none");
                }
                if (Session["EnableVidtel"] != null)
                {
                    if (Session["EnableVidtel"].ToString().Equals("0"))
                        tdVid.Attributes.Add("style", "display:none");
                }
                //FB 2693 Ends

                //FB 2583 Start

                string DefaultFECC = Session["DefaultFECC"].ToString();
                EnableFECC = Session["EnableFECC"].ToString();

                if (EnableFECC == "1") //YES//FB 2592
                {
                    tdFECC.Attributes.Add("Style", "display:inline;"); //FB 2595
                    chkFECC.Visible = true;
                }
                else
                {
                    tdFECC.Attributes.Add("Style", "display:none;"); //FB 2595
                    chkFECC.Visible = false;
                }
                //FB 2583 End
                //FB 2641 start
                if (Session["EnableStartMode"] != null)
                    if (!string.IsNullOrEmpty(Session["EnableStartMode"].ToString()))
                        int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);

                if (Session["EnableLinerate"] != null)
                if (!string.IsNullOrEmpty(Session["EnableLinerate"].ToString()))
                    int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);
                
                if (EnableLinerate == 1)
                {
                    trLineRate.Attributes.Add("Style", "Display:;");
                    
                }
                else
                {
                    trLineRate.Attributes.Add("Style", "Display:None;");
                   
                }
                
                if (EnableStartMode == 1)
                {
                    trStartMode.Attributes.Add("Style", "Display:;");
                }
                else
                {
                    trStartMode.Attributes.Add("Style", "Display:None;");
                }

                //FB 2641 End

                //FB 2670 START
                string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();

                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" || EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "1" && EnableVNOCselection == 0)
                    divConcSupport.Attributes.Add("style", "display:None");
                else
                    divConcSupport.Attributes.Add("style", "display:Block");

                if (EnableOnsiteAV == "1")
                    tdOnSiteAVSupport.Attributes.Add("Style", "Display:Block;");
                else
                    tdOnSiteAVSupport.Attributes.Add("Style", "Display:None;");
                
                if (EnableMeetandGreet == "1")
                    tdMeetandGreet.Attributes.Add("Style", "Display:Block;");
                else
                    tdMeetandGreet.Attributes.Add("Style", "Display:None;");
                
                if (EnableConciergeMonitoring == "1")
                    tdConciergeMonitoring.Attributes.Add("Style", "Display:Block;");
                else
                    tdConciergeMonitoring.Attributes.Add("Style", "Display:None;");
                
                if (EnableDedicatedVNOC == "1")
                   tdDedicatedVNOC.Attributes.Add("Style", "Display:Block;");
                else
                   tdDedicatedVNOC.Attributes.Add("Style", "Display:None;");
                //FB 2670 END

				//FB 2501 - Starts
                if (Session["EnableConfPassword"] != null)
                    EnableConferencePassword = Session["EnableConfPassword"].ToString();

                if (EnableConferencePassword == "1")  //FB 2274
                {
                    pnlPassword1.Visible = true;
                    pnlPassword2.Visible = true;
                }
                else
                {
                    pnlPassword1.Visible = false;
                    pnlPassword2.Visible = false;
                }

                ConferencePassword.Attributes.Add("value", confPassword.Value);
                ConferencePassword2.Attributes.Add("value", confPassword.Value);
				//FB 2501 - End

				//FB 2659 - Starts
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);

                //ZD 100166 Starts
                if (enableCloudInstallation == 1)
                {
                    SetupDuration.Text = "0";
                    TearDownDuration.Text = "0";
                    lstVMR.ClearSelection();
                    lstVMR.SelectedValue = "0";
                    trseatavailable.Visible = true;
                    trAdvancedSettings.Attributes.Add("style", "display:none");
                    trPCConf.Attributes.Add("style", "display:none");
                    trVMR.Attributes.Add("style", "display:none");
                }
                else
                {
                    trseatavailable.Visible = false;
                    trAdvancedSettings.Attributes.Add("style", "display:");
                }
                //ZD 100166 End
                //FB 2659 - End

                if (Session["Cloud"] != null) //FB 2717
                    if (Session["Cloud"].ToString().Trim() == "1")
                        isCloudEnabled = 1;
                
                if (isCloudEnabled == 1)
                    trCloudConferencing.Attributes.Add("style", "display:");
                else
                    trCloudConferencing.Attributes.Add("style", "display:none");
                
                //if (isCloudEnabled == 0)//Vidyo_J
                //    trVMR.Visible = false;
                //else
                //    trVMR.Visible = true;
                //FB 2993 Starts
                if (Session["NetworkSwitching"] != null)
                {
                    int.TryParse(Session["NetworkSwitching"].ToString(), out NetworkSwitching);

                    if (NetworkSwitching == 2)
                        trNetworkState.Visible = true;
                    else if (NetworkSwitching == 3)
                    {
                        trNetworkState.Visible = false;
                        drpNtwkClsfxtn.ClearSelection();
                        drpNtwkClsfxtn.SelectedValue = "0";
                    }
                    else
                    {
                        trNetworkState.Visible = false;
                        drpNtwkClsfxtn.ClearSelection();
                        drpNtwkClsfxtn.SelectedValue = "1";
                    }

                }
                //FB 2993 Ends
                if (!IsPostBack)
                {
                    //FB 2426 Start
                    obj.BindLineRate(lstIPlinerate);
                    obj.BindLineRate(lstSIPlinerate);
                    obj.BindLineRate(lstISDNlinerate);
                    obj.BindVideoEquipment(lstIPVideoEquipment);
                    obj.BindVideoEquipment(lstSIPVideoEquipment);
                    obj.BindVideoEquipment(lstISDNVideoEquipment);
                    obj.BindDialingOptions(lstIPConnectionType);
                    obj.BindDialingOptions(lstSIPConnectionType);
                    obj.BindDialingOptions(lstISDNConnectionType);
                    obj.GetCountryCodes(lstCountries);
                    lstCountries.Items.FindByValue("225").Selected = true;
                    txtApprover5.Attributes.Add("readonly", "");
                    txtEmailId.Attributes.Add("readonly", "");
                    Session["OnlyFlyRoom"] = null;
                    if (Session["GuestRooms"].ToString() == "0")
                    {
                        OnFlyRowGuestRoom.Attributes.Add("style", "display:none");
                        btnGuestLocation.Attributes.Add("style", "display:none");
                        Image6.Attributes.Add("style", "display:none");
                    }
                    //FB 2426 End
                    //FB 2226 - Start
                    if (Session["EnablePublicConf"] != null)
                    {
                        if (Session["EnablePublicConf"].ToString() == "1")
                            trPublic.Visible = true;
                        else
                            trPublic.Visible = false;
                    }
                    if (Session["defaultPublic"] != null)
                    {
                        if (Session["defaultPublic"].ToString() == "1")
                            chkPublic.Checked = true;
                        else
                            chkPublic.Checked = false;
                    }
                    //FB 2226 - End
                    txtVNOCOperator.Attributes.Add("readonly", ""); //FB 2501//FB 2670

                   //FB 2870 Start
                   if (Session["EnableNumericID"] != null)
                        EnableNumericID = Session["EnableNumericID"].ToString();

                   if (EnableNumericID == "1")
                   {
                       TrCTNumericID.Visible = true;
                   }
                   else
                   {
                       TrCTNumericID.Visible = false;
                       ChkEnableNumericID.Checked = false;
                   }

                   HtmlGenericControl MyDiv = (HtmlGenericControl)Page.FindControl("DivNumeridID");

                   if (ChkEnableNumericID.Checked)
                       MyDiv.Attributes.Add("style", "display:block");
                   else
                       MyDiv.Attributes.Add("style", "display:none");

                    //FB 2870 End
                    isFormSubmit = false;

                    Session.Remove("IsInstanceEdit");
                    //FB 2501 - Start
					//lstDuration.Text = "01:00";
                    if (Session["DefaultConfDuration"] != null)
                    {
                        int ConfDuration = Convert.ToInt32(Session["DefaultConfDuration"]);
                        int ConfHours = ConfDuration / 60;
                        String hrs = (ConfHours < 10) ? "0" + ConfHours.ToString() : ConfHours.ToString();
                        int ConfMinutes = ConfDuration % 60;
                        String Mins = (ConfMinutes < 10) ? "0" + ConfMinutes.ToString() : ConfMinutes.ToString();
                        lstDuration.Text = hrs + ":" + Mins;
                    }
                    //FB 2501 - End   
                    
                    if (Session["hasCalendar"] != null)
                        txtHasCalendar.Value = Session["hasCalendar"].ToString();

                    chkStartNow.Attributes.Add("onclick", "javascript:ChangeImmediate('S')"); //FB 2341
                    confEndTime.Attributes.Add("onblur", "javascript:CheckDuration()");
                    chkPublic.Attributes.Add("onclick", "javascript:ChangePublic()");//FB 2226
                    ChkEnableNumericID.Attributes.Add("onclick", "javascript:ChangeNumeric()");//FB 2870
                    //confStartDate.Attributes.Add("onblur", "javascript:ChangeEndDate()");
                    // confStartDate.Attributes.Add("onblur", "javascript:ChangeEndDate(0)");//FB 1715
                    //confEndDate.Attributes.Add("onblur", "javascript:ChangeStartDate()");
                    //confStartTime.Attributes.Add("onblur", "javascript:ChangeEndDate()");
                    // confStartTime.Attributes.Add("onblur", "javascript:return document.getElementById('hdnChange').value='ST';formatTime('confStartTime_Text');ChangeEndDate(0)"); //FB 1715, FB 1716
                    //SetupDate.Attributes.Add("onblur", "javascript:ChangeEndDate()");
                    //SetupDate.Attributes.Add("onblur", "javascript:ChangeEndDate(0)"); //FB 1715
                    //TearDownDate.Attributes.Add("onblur", "javascript:ChangeStartDate()"); 
                    //confEndTime.Attributes.Add("onblur", "javascript:ChangeStartDate()");
                    //confEndTime.Attributes.Add("onblur", "javascript:return document.getElementById('hdnChange').value='ET';formatTime('confEndTime_Text');ChangeStartDate(0)"); //FB 1715, FB 1716
                    lstAudioParty.Attributes.Add("onchange", "javascript:SelectAudioParty()");
                    ConferencePassword.Attributes.Add("onchange", "javascript:SavePassword()");//FB 2501
					//FB 2634
                    //SetupTime.Items.Clear();
                    //TeardownTime.Items.Clear();
                    //obj.BindTimeToListBox(SetupTime, true, true);
                    //obj.BindTimeToListBox(TeardownTime, true, true);

                    lstLineRate.ClearSelection(); //FB 2394  start
                    obj.BindLineRate(lstLineRate);

                    //FB 2429 - Starts
                    lstLineRate.SelectedValue = OrgLineRate.ToString();
                    if (OrgLineRate <= 0)
                        lstLineRate.SelectedValue = "384";
                    //FB 2501 starts
                    if (Session["StartMode"].ToString() != null)
                        lstStartMode.SelectedValue = Session["StartMode"].ToString();
                    //FB 2501 Ends
                    if (Session["isExpressUser"] != null)
                    {
                        if (Session["isExpressUser"].ToString() == "1")
                        {
                            if (Session["isExpressManage"] != null && Session["isExpressUserAdv"] != null)
                            {
                                if (Session["isExpressManage"].ToString() == "1" || Session["isExpressUserAdv"].ToString() == "1")
                                    lstLineRate.Enabled = true;
                                else
                                    lstLineRate.Enabled = false;
                            }
                        }
                    }

                    //FB 2998
                    txtMCUConnect.Text = Session["McuSetupTime"].ToString();
                    txtMCUDisConnect.Text = Session["MCUTeardonwnTime"].ToString();
                    imgSetup.ToolTip = obj.GetTranslatedText("The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting.");
                    imgMCUConnect.ToolTip = obj.GetTranslatedText("The number of minutes that the MCU is to connect the endpoints, prior to the arrival of the participants into the conference.");
                    imgMCUDisconnect.ToolTip = obj.GetTranslatedText("The number of minutes that the MCU is to disconnect the endpoints, prior to the departure of the participants from the conference.");

                    /*if (client.ToUpper() == "DISNEY")
                        lstLineRate.SelectedValue = "2048";
                    else
                        lstLineRate.SelectedValue = "384"; //FB 2394 end*/
                    //FB 2429 - End
                    //FB 2583 Start
                    if (DefaultFECC == "1" && EnableFECC != "2") //DD2
                        chkFECC.Checked = true;
                    else
                        chkFECC.Checked = false;
                    //FB 2583 End

                    String inXML = "";
                    String outXML;
                    confStartTime.Items.Clear();
                    confEndTime.Items.Clear();
                    obj.BindTimeToListBox(confStartTime, true, true);
                    obj.BindTimeToListBox(confEndTime, true, true);
                    if (Session["timeFormat"] != null)
                        if (Session["timeFormat"].ToString().Equals("0"))
                        {
							//FB 2634
                            if (regEndTime != null && regStartTime != null)
                            {
                                regEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                                regStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                regStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            }
							//FB 2634	
                            //regSetupStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            //regSetupStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            //regTearDownStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            //regTearDownStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        }
                        else if (Session["timeFormat"].ToString().Equals("2"))//FB 2588
                        {
                            regEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                            regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                            regStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                            regStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                        }
                    BindRecurrenceDefault();
                    
                    switch (Request.QueryString["t"].ToString())
                    {
                        case "n": // for new conference
                            {
                                FetchAudioParticipants("n");
                                //FB 2501 starts
                                txtApprover7.Text = Session["userName"].ToString();
                                hdnApprover7.Text = Session["userId"].ToString();
                                hdnRequestorMail.Text = Session["userEmail"].ToString();
                                hdnApproverMail.Text = Session["userEmail"].ToString();
                                //FB 2501 ends
                                txtApprover4.Text = Session["userName"].ToString();
                                hdnApprover4.Text = Session["userId"].ToString();

                                inXML = "<login>"+ obj.OrgXMLElement()+ "<userID>" + Session["userID"] + "</userID></login>";
                                outXML = obj.CallMyVRMServer("GetNewConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataNew();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                break;
                            }
                        case "o": //if you want to clone a conference
                            {
                                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><selectType>1</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataOld();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                break;
                            }
                        case "t": //if you want to create a conference from a template
                            {
                                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><selectType>2</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataOld();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                break;
                            }
                        case "": // for edit conference
                            {
                                inXML = "<login>"+ obj.OrgXMLElement()+ "<userID>" + Session["userID"].ToString() + "</userID><selectType>1</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                outXML = outXML.Replace("& ", "&amp; ");
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                {
                                    BindDataOld();

                                    FetchAudioParticipants("e");
                                }
                                else
                                {
                                    errLabel.Visible = true;
                                }
                                
                                //FB 1825 Start
                                if (!(Session["admin"].ToString().Trim() == "2"))
                                {

                                    hdnSetStartNow.Value = "hide";
                                }
                                //FB 1825 End
                                break;
                            }
                    }

                    if (Request.QueryString["op"] != null)//FB 2341
                    {
                        if (Request.QueryString["op"].ToString().Equals("2"))
                            chkStartNow.Checked = true;
                    }
                }
                else
                {
                    isFormSubmit = true;
                    //ZD 101052 Starts - Files are saved in upload path tiwce.
                    //if (Request.Params.Get("__EVENTTARGET").ToString().IndexOf("btnUploadFiles") >= 0)
                    //{
                    //    UploadFiles(new Object(), new EventArgs());
                    //}
                    //ZD 101052 End

                    //FB 2634
                    if ((RecurFlag.Value.Equals(1)) || (chkStartNow.Checked)) //FB 2341
                    {
                        reqStartTime.Enabled = false;
                        regStartTime.Enabled = false;
                        reqEndTime.Enabled = false;
                        //reqEndTime.Enabled = false;
                    }
                    
                }

                //FB 2599 Start
                if (isCloudEnabled == 1 && chkCloudConferencing.Checked) //FB 2262-S
                {
                    lstVMR.SelectedValue = "3";
                    lstVMR.Enabled = false;
                    OnFlyRowGuestRoom.Attributes.Add("style", "display:none");
                    btnGuestLocation.Attributes.Add("style", "display:none");
                    Image6.Attributes.Add("style", "display:none");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "changeVMR", "changeVMR();", true);
                    //chkVMR.Attributes.Add("onclick", "javascript:changeVMR()");
                }
                else
                {
                    lstVMR.Enabled = true;//Vidyo_J
                    OnFlyRowGuestRoom.Attributes.Add("style", "display:");
                    btnGuestLocation.Attributes.Add("style", "display:");
                    Image6.Attributes.Add("style", "display:");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "changeVMR", "changeVMR();", true);
                    //chkVMR.Attributes.Add("onclick", "javascript:changeVMR()");
                }
                //FB 2599 End


                HideRecurButton();
				//FB 2592
                FillCustomAttributeTable();

                hdnMcuProfileSelected.Value = "";//FB 2839
                FillMCUProfile();//FB 2839

                if (!IsPostBack)
                {
                     FillRequestorPhoneNo();
                }
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

            }   
            catch (Exception ex)
            {
                log.Trace("Simple Conference :Page_load" + ex.Message);

            }
        }
        #endregion

        #region Add Rooms
        //FB 2367
        protected void btnRoomSelect_Click(Object sender, EventArgs e)
        {
            try
            {
                SyncRoomSelection();
                RoomList.Items.Clear();
                RoomList.Enabled = true;
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    if (tn.Depth == 3)
                    {
                        if (strRoomIds == "")
                            strRoomIds = tn.Value;
                        else
                            strRoomIds = strRoomIds + "," + tn.Value;

                        if (strRoomName == "")
                            strRoomName = tn.Text;
                        else
                            strRoomName = strRoomName + "$" + tn.Text;
                    }
                }
                String[] strArrRms;
                strArrRms = strRoomName.Split('$');
                String[] strArrRmsIds;
                strArrRmsIds = strRoomIds.Split(',');

                if (strArrRms != null && strArrRmsIds != null)
                {
                    if (strArrRms.Length == strArrRmsIds.Length)
                    {
                        for (Int32 i = 0; i < strArrRms.Length; i++)
                        {
                            ListItem liRms = new ListItem();
                            liRms.Value = strArrRmsIds[i].ToString();
                            liRms.Text = strArrRms[i].ToString();
                            RoomList.Items.Add(liRms);
                        }
                    }
                }
                if (RoomList.Items.Count > 0)
                    RoomList.Enabled = true;
                else
                    RoomList.Enabled = true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Add Rooms : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "Add Rooms : " + ex.Message;
            }
        }
        #endregion

        #region FillRequestorPhoneNo
        private void FillRequestorPhoneNo()
        {
            string workNo = "";
            string cellNo = "";
            try
            {
                if (Session["workPhone"] != null)
                    workNo = Session["workPhone"].ToString();

                if (Session["cellPhone"] != null)
                    cellNo = Session["cellPhone"].ToString();

                string[] hostIDArr = hdnHostIDs.Value.Trim().Split('?');
                if (hostIDArr != null)
                {
                    if (hostIDArr.Length > 1)
                    {
                        TextBox txtWork = (TextBox)this.FindControl(hostIDArr[0]);
                        if (txtWork.Text.Trim() == "")
                            txtWork.Text = workNo;

                        TextBox txtCell = (TextBox)this.FindControl(hostIDArr[1]);
                        if (txtCell.Text.Trim() == "")
                            txtCell.Text = cellNo;
                    }
                }
            }
            catch
            { }
        }
        #endregion

        #region CalculateDuration
        public void CalculateDuration(Object server, EventArgs e)
        {
            CalculateDuration();
        }

        #endregion

        #region BindDataNew
        /// <summary>
        /// This method will be used on new conference Creation
        /// </summary>
        private void BindDataNew()
        {
            XmlNode usrNode = null;//FB 2376 
            try
            {
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                XmlNodeList nodes = node.SelectNodes("//conference/confInfo/timezones/timezone");
                lblConfID.Text = "new";
                int length = nodes.Count;
                //obj.GetSystemDateTime(Application["COM_ConfigPath"].ToString());// FB 1735
                obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());// FB 2027


                //FB 2717 Starts
                lstVMR.ClearSelection();
                lstVMR.SelectedIndex = 0; //FB 2780
                //FB 2717 End

                //FB 2634
                if (EnableBufferZone == 1)
                {
                    SetupDuration.Text = OrgSetupTime.ToString();
                    TearDownDuration.Text = OrgTearDownTime.ToString();
                }
                else
                {
                    SetupDuration.Text = "0";
                    TearDownDuration.Text = "0";
                }
                DateTime startDateTime = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                if (Request.QueryString["sd"] != null)
                    startDateTime = DateTime.Parse(Request.QueryString["sd"].ToString() + " " + Request.QueryString["st"].ToString());
                else
                {
                    if (startDateTime.Minute < 45)
                        startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                    else
                        startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);
                }

                //FB 2634
                if (EnableBufferZone == 1)
                    startDateTime = startDateTime.AddMinutes(Double.Parse(OrgSetupTime.ToString()));
                confStartDate.Text = startDateTime.ToString(format);
                confStartTime.Text = startDateTime.ToString(tformat);
                //FB 2364
                //FB 2398 Start
                //if (EnableBufferZone == 1) 
                //{
                //    startDateTime =  startDateTime.AddMinutes(OrgSetupTime);
                //}
                //FB 2398 End
                //FB 2501 - Start
                DateTime endDateTime = DateTime.Now;
                String ConfDuration = Session["DefaultConfDuration"].ToString();
                if (ConfDuration != null)
                    endDateTime = startDateTime.AddMinutes(Int32.Parse(ConfDuration));

                //DateTime endDateTime = startDateTime.AddMinutes(60);
                //if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                //    endDateTime = startDateTime.AddMinutes(Int32.Parse(ConfDuration));
                //FB 2501 - End

                confEndDate.Text = endDateTime.ToString(format);
                confEndTime.Text = endDateTime.ToString(tformat);
                confStartTime.SelectedValue = confStartTime.Text;
                confEndTime.SelectedValue = confEndTime.Text;
                CalculateDuration();
                //FB 2634
                //SetupDate.Text = startDateTime.ToString(format);
                //SetupTime.Text = startDateTime.ToString(tformat);
                //TearDownDate.Text = endDateTime.ToString(format);
                //TeardownTime.Text = endDateTime.ToString(tformat);
                //SetupTime.SelectedValue = confStartTime.Text;
                //TeardownTime.SelectedValue = confEndTime.Text;
                
                String selTZ = "-1";
                lstConferenceTZ.ClearSelection();
                obj.GetTimezones(lstConferenceTZ, ref selTZ);
                if (node.SelectSingleNode("/conference/confInfo/timeZone") != null)
                    lstConferenceTZ.Items.FindByValue(node.SelectSingleNode("/conference/confInfo/timeZone").InnerText).Selected = true;
                txtModifyType.Text = "0";
                LoadCommonValues(node);
               // RefreshRoom(null, null);
                /** FB 2376 **/
                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/internalBridge");
                if (usrNode != null)
                {
                    txtintbridge.Text = usrNode.InnerText;
                    hdnintbridge.Value = usrNode.InnerText;
                }

                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/externalBridge");
                if (usrNode != null)
                {
                    txtextbridge.Text = usrNode.InnerText;
                    hdnextbridge.Value = usrNode.InnerText;
                }
                /** FB 2376 **/
                CheckPCVendor();//FB 2693 
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindDataNew: " + ex.Message);
            }
        }
        #endregion

        #region BindDataOld
        /// <summary>
        /// To get Created Conferences in Edit/Clone Mode
        /// </summary>

        private void BindDataOld()
        {
            try
            {
                xConfInfo = new StringDictionary(); //FB 1830 Email Edit
                lblConfHeader.Text = obj.GetTranslatedText("Edit");//FB 1830 - Translation
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                string recurring = "";
                XmlNodeList ConfVNOCnodes; //FB 2670

                DateTime setupStartDateTime = DateTime.MinValue;
                DateTime teardownStartDateTime = DateTime.MinValue;
                Double setupDuration = Double.MinValue;
                Double teardownDuration = Double.MinValue;
                string setupDur = "0";
                string tearDownDur = "0";
                //string[] tmpstrs;//FB 2341//FB 2377
                //string tmpstr = "";
                //int i, tmpint;
               
                if (!Request.QueryString["t"].ToString().Equals("t"))
                {
                    recurring = node.SelectSingleNode("//conference/confInfo/recurring").InnerText;
                    lblConfID.Text = node.SelectSingleNode("//conference/confInfo/confID").InnerText;
                }

                //FB 2550 - Starts
                bool isClone = false;
                if (Request.QueryString["t"].ToString().Equals("o"))
                {
                    isClone = true;
                }
                //FB 2550 - End

                if (!Request.QueryString["t"].ToString().Equals("t") && !Request.QueryString["t"].ToString().Equals("o"))
                {
                    if (node.SelectSingleNode("//conference/isinstanceedit") != null)
                    {
                        if (node.SelectSingleNode("//conference/isinstanceedit").InnerText == "1")
                        {
                            isInstanceEdit = "Y";

                            Session.Remove("IsInstanceEdit");
                            Session.Add("IsInstanceEdit", isInstanceEdit);
                        }
                    }
                }

                if (recurring.Equals("1")) //in case of editing a recurring conference
                {
                    XmlNode usrnode = xmlDOC.SelectSingleNode("conference/userInfo");
                    if (usrnode != null)
                        usrnode.InnerXml += "<userId>" + Session["userID"].ToString() + "</userId>";

                    string recOutxml = obj.CallMyVRMServer("GetIfDirtyorPast", xmlDOC.InnerXml, Application["MyVRMServer_ConfigPath"].ToString());
                    if (recOutxml != "")
                    {
                        if (recOutxml.IndexOf("<error>") < 0)
                        {
                            xmlDOC.LoadXml(recOutxml);
                            node = null;
                            node = (XmlNode)xmlDOC.DocumentElement;
                        }
                    }
                }
                ConferenceName.Text = node.SelectSingleNode("//conference/confInfo/confName").InnerText;
                ConferenceDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters(node.SelectSingleNode("//conference/confInfo/description").InnerText.Replace("N/A", ""),1); //FB 2236_s
                //FB 2501 starts
                txtApprover7.Text = node.SelectSingleNode("//conference/confInfo/lastModifiedByName").InnerText;
                hdnApprover7.Text = node.SelectSingleNode("//conference/confInfo/lastModifiedById").InnerText;
                //FB 2501 ends
                txtApprover4.Text = node.SelectSingleNode("//conference/confInfo/hostName").InnerText;
                hdnApprover4.Text = node.SelectSingleNode("//conference/confInfo/hostId").InnerText;
				//FB 2501 - Starts
                xconfpassword = node.SelectSingleNode("//conference/confInfo/confPassword").InnerText.Trim();
                if (!xConfInfo.ContainsKey("password"))
                    xConfInfo.Add("password", xconfpassword);

                ConferencePassword.Attributes.Add("value", xconfpassword);
                ConferencePassword2.Attributes.Add("value", xconfpassword);
                confPassword.Value = xconfpassword;  //FB 2991
				//FB 2501 - End
                string confType = node.SelectSingleNode("//conference/confInfo/createBy").InnerText;
                if (Request.QueryString["t"] != null)
                {
                    if (Request.QueryString["t"].ToString().Equals("t")) // if the conference is from a template then we need to generate current date and time like new conference
                        confType = node.SelectSingleNode("//conference/confInfo/confType").InnerText;
                }
                if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    if (confType.Equals("")) 
                        confType = Application["DefaultConferenceType"].ToString();
                }
                else
                    confType = "7"; // 7- Room Conference

                hdnconftype.Value = confType; 
                XmlNodeList nodes = node.SelectNodes("//conference/confInfo/timezones/timezone");
                int length = nodes.Count;
                String selTZ = "-1";
                lstConferenceTZ.ClearSelection();
                obj.GetTimezones(lstConferenceTZ, ref selTZ);

                lstConferenceTZ.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/timeZone").InnerText).Selected = true;
                LoadCommonValues(node);

                if (node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText != "")
                    {
                        setupDur = node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText;
                    }
                }

                if (node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText != "")
                    {
                        tearDownDur = node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText;
                    }
                }
                Double.TryParse(setupDur, out setupDuration);
                Double.TryParse(tearDownDur, out teardownDuration);
                
                //FB 2634
                SetupDuration.Text = Math.Abs(setupDuration).ToString();
                TearDownDuration.Text = Math.Abs(teardownDuration).ToString();

                //FB 1716 - Start
                double actualDur;
                if (recurring.Equals("1"))
                    Double.TryParse(node.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText, out actualDur);
                else
                    Double.TryParse(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText, out actualDur);

                //FB 2634
                //hdnDuration.Value = (actualDur - setupDuration - teardownDuration) +
                //    "&" + setupDuration + "&" + teardownDuration;

                hdnDuration.Value = (actualDur - setupDuration - teardownDuration).ToString();

                //FB 1716 - End

                if (EnableBufferZone == 1 && (setupDur != "0" || tearDownDur != "0"))
                {
                    //chkEnableBuffer.Checked = true; // New Design Code Review Needed
                }
                if (recurring.Equals("1")) //in case of editing a recurring conference
                {
                    string recurstr = node.SelectSingleNode("//conference/confInfo/appointmentTime").InnerXml;
                    recurstr += node.SelectSingleNode("//conference/confInfo/recurrencePattern").InnerXml;
                    if (node.SelectNodes("//conference/confInfo/recurrenceRange").Count != 0)
                        recurstr += node.SelectSingleNode("//conference/confInfo/recurrenceRange").InnerXml;
                    recurstr = "<recurstr>" + recurstr + "</recurstr>";
                    string tzstr = "<TimeZone>" + xmlDOC.SelectSingleNode("//conference/confInfo/timezones").InnerXml + "</TimeZone>";
                    string rst = obj.AssembleRecur(recurstr, tzstr);
                    string[] rst_array = rst.Split('|');
                    string recur = rst_array[0];

                    string recDtString = "";
                    string tempRec = "";
                    String[] recDateArr = recur.Split('#');
                    recur = "";
                    if (recDateArr.Length > 0)
                    {
                        tempRec = recDateArr[recDateArr.Length - 1];
                        if (tempRec != "")
                        {
                            String[] dtsArr = tempRec.Split('&');
                            if (dtsArr.Length > 0)
                            {
                                for (int lp = 0; lp < dtsArr.Length; lp++)
                                {
                                    if (dtsArr[lp].IndexOf("/") > 0)
                                    {
                                        dtsArr[lp] = myVRMNet.NETFunctions.GetFormattedDate(dtsArr[lp]);
                                    }
                                    if (recDtString == "")
                                        recDtString = dtsArr[lp];
                                    else
                                        recDtString += "&" + dtsArr[lp];
                                }
                            }
                        }
                        for (int lp = 0; lp < recDateArr.Length - 1; lp++)
                        {
                            if (recur == "")
                                recur = recDateArr[lp];
                            else
                                recur += "#" + recDateArr[lp];
                        }
                        recur += "#" + recDtString;
                    }
                    Recur.Value = recur;
                    string SelectedTimeZoneName = rst_array[1];
                    DateTime startDateTime = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());

                    if (Request.QueryString["sd"] != null)
                        startDateTime = DateTime.Parse(Request.QueryString["sd"].ToString() + " " + Request.QueryString["st"].ToString());
                    else
                    {
                        if (startDateTime.Minute < 45)
                            startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                        else
                            startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);
                    }
                    confStartDate.Text = startDateTime.ToString(format);
                    confStartTime.Text = startDateTime.ToString(tformat);
                    DateTime endDateTime = startDateTime.AddMinutes(60);

                    confEndDate.Text = endDateTime.ToString(format);
                    confEndTime.Text = endDateTime.ToString(tformat);
                    string startHour = "0", startMin = "0", startSet = "AM";
                    double duration = 0;

                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText != "")
                        {
                            startHour = node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText;
                        }
                    }
                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText != "")
                        {
                            startMin = node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText;
                        }
                    }
                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText != "")
                        {
                            startSet = node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText;
                        }
                    }

                    string durationMin = node.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText; 
                    txtModifyType.Text = "1";
                    Double.TryParse(durationMin, out duration);
                    //SetupDate.Text = confStartDate.Text; //FB 2634
                    //FB 2266 - Starts
                    //endDateTime = startDateTime.AddMinutes(duration);
                    //confEndDate.Text = endDateTime.ToString(format);
                    //confEndTime.Text = endDateTime.ToString(tformat);
                    //FB 2266 - Starts
                    //TearDownDate.Text = confEndDate.Text; //FB 2634

                    if (startHour.Trim() == "")
                        startHour = "0";
                    if (startMin.Trim() == "")
                        startMin = "0";
                    if (startSet.Trim() == "")
                        startSet = "AM";

                    DateTime setupTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                    setupTime = setupTime.AddMinutes(setupDuration);
                    string sTime = setupTime.ToString(tformat);
                    confStartTime.Text = sTime; //FB 2634
                    confStartTime.SelectedValue = confStartTime.Text;

                    DateTime endTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                    endTime = endTime.AddMinutes(duration);
                    endTime = endTime.AddMinutes(-teardownDuration);
                    string tTime = endTime.ToString(tformat);
                    //TeardownTime.Text = confEndTime.Text; FB 2634
                    confEndTime.Text = tTime; //FB 2634
                    confEndTime.SelectedValue = confEndTime.Text;

                    hdnBufferStr.Value = sTime + "&" + tTime;
                    hdnSetupTime.Value = setupDur;
                    hdnTeardownTime.Value = tearDownDur;

                    //FB 2634
                    //FB 1830 Email Edit - start
                    if (!xConfInfo.ContainsKey("setupdate"))
                        xConfInfo.Add("setupdate", myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + sTime); //without buffer
                    if (!xConfInfo.ContainsKey("teardate"))
                        xConfInfo.Add("teardate", myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + tTime);
                    //FB 1830 Email Edit - end
                }
                else
                {
                    int syear = 0, smonth = 0, sday = 0, sHour = 0, sMin = 0;
                    string sSet = "AM";
                    DateTime startDateTime = DateTime.Now; 
                    if (Request.QueryString["t"] != null)
                        if (Request.QueryString["t"].ToString().Equals("t")) // if the conference is from a template then we need to generate current date and time like new conference
                        {
                            startDateTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString(tformat));
                            if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                                startDateTime = Convert.ToDateTime(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                            if (startDateTime.Minute < 45)
                                startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                            else
                                startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);

                            syear = startDateTime.Year;
                            smonth = startDateTime.Month;
                            sday = startDateTime.Day;
                            sHour = startDateTime.Hour;
                            sMin = startDateTime.Minute;
                            sSet = startDateTime.ToString("tt").ToUpper();
                        }
                        else // if it is an old conference or if we clone a conference, then we will take the conference information returned by COM
                        {
                            string confstatus = "0";
                            if (node.SelectSingleNode("//conference/confInfo/Status") != null)
                                confstatus = node.SelectSingleNode("//conference/confInfo/Status").InnerText;
                            if (confstatus == "7")
                            {
                                startDateTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString(tformat));
                                if (startDateTime.Minute < 45)
                                    startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                                else
                                    startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);

                                syear = startDateTime.Year;
                                smonth = startDateTime.Month;
                                sday = startDateTime.Day;
                                sHour = startDateTime.Hour;
                                sMin = startDateTime.Minute;
                                sSet = startDateTime.ToString("tt").ToUpper();
                            }   
                            else
                            {
                                syear = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[2]);
                                smonth = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[0]);
                                sday = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[1]);
                                sHour = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startHour").InnerText);
                                sMin = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startMin").InnerText);
                                sSet = node.SelectSingleNode("//conference/confInfo/startSet").InnerText;
                                startDateTime = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sHour + ":" + sMin + " " + sSet);
                            }
                        }
                    confStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(startDateTime);
                    confStartTime.Text = startDateTime.ToString(tformat);
                    TimeSpan durationMin = new TimeSpan(0, Convert.ToInt32(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText), 0); // * 1000000000 * 60);
                    confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(startDateTime.Add(durationMin));
                    
                    if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                        confEndTime.Text = (startDateTime.AddMinutes(15)).ToString(tformat);
                    else
                        confEndTime.Text = (startDateTime.AddMinutes(Convert.ToInt32(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText))).ToString(tformat);

                    string durMin = node.SelectSingleNode("//conference/confInfo/durationMin").InnerText;
                    double duration = 0;
                    Double.TryParse(durMin, out duration);

                    confEndTime.Text = startDateTime.AddMinutes(duration).ToString(tformat);
                    setupStartDateTime = startDateTime.AddMinutes(setupDuration);
					//FB 2634
                    //SetupDate.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime);
                    //SetupTime.Text = setupStartDateTime.ToString(tformat);
                    SetupDuration.Text = setupDuration.ToString();
                    confStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime);
                    confStartTime.Text = setupStartDateTime.ToString(tformat);
                    TearDownDuration.Text = teardownDuration.ToString();

                    DateTime endDateTime = startDateTime.AddMinutes(duration);
                    teardownStartDateTime = endDateTime.AddMinutes(-teardownDuration);

                    confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(teardownStartDateTime);
                    confEndTime.Text = teardownStartDateTime.ToString(tformat);
                    
                    txtModifyType.Text = "0";
                    CalculateDuration();

                    //FB 2634
                    //FB 1830 Email Edit - start
                    if (!xConfInfo.ContainsKey("setupdate"))
                        xConfInfo.Add("setupdate", myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //without buffer//FB 2588
                    if (!xConfInfo.ContainsKey("teardate"))
                        xConfInfo.Add("teardate", myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                    //FB 1830 Email Edit - end
                }
                nodes = node.SelectNodes("//conference/confInfo/partys/party");

                getParty(nodes, isClone);//FB 2550
                string advAVParam = node.SelectSingleNode("//conference/confInfo/advAVParam").OuterXml;
                Session.Add("AdvAVParam", advAVParam);

                //FB 2377 - Starts
                /*tmpstrs = ((tmpstr = node.SelectSingleNode("//conference/confInfo/ConceirgeSupport").InnerText)).Split(',');//FB 2341
                tmpint = ((tmpstr.Trim()).Equals("")) ? -1 : tmpstrs.Length;
                for (i = 0; i < tmpint; i++)
                    ChklstConcSupport.Items[Convert.ToInt16(tmpstrs[i]) - 1].Selected = true;*/
                //FB 2377 - End

                //FB 2226 - Start
                if (node.SelectSingleNode("//conference/confInfo/publicConf") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/publicConf").InnerText.Equals("1"))
                        chkPublic.Checked = true;
                    else
                        chkPublic.Checked = false;
                }
                if (node.SelectSingleNode("//conference/confInfo/dynamicInvite") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/dynamicInvite").InnerText.Equals("1"))
                        chkOpenForRegistration.Checked = true;
                    else
                        chkOpenForRegistration.Checked = false;
                }
                //FB 2226 - End

                // FB 2620 Starts

                //FB 2376
                //if (node.SelectSingleNode("//conference/confInfo/isVMR") != null)
                //{
                //    if (node.SelectSingleNode("//conference/confInfo/isVMR").InnerText.Equals("1"))
                //        chkVMR.Checked = true;
                //    else
                //        chkVMR.Checked = false;
                //}
                lstVMR.ClearSelection();
                if (node.SelectSingleNode("//conference/confInfo/isVMR") != null)
                    if (lstVMR.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/isVMR").InnerText) != null)
                        lstVMR.SelectedValue = node.SelectSingleNode("//conference/confInfo/isVMR").InnerText.Trim();
                // FB 2620 Ends
                
                //FB 2717 Start
                if (node.SelectSingleNode("//conference/confInfo/CloudConferencing") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/CloudConferencing").InnerText.Equals("1")) //FB 2448
                    {
                        chkCloudConferencing.Checked = true;
                    }
                    else
                        chkCloudConferencing.Checked = false;
                }
                //FB 2717 End

                if (EnableFECC == "1") //YES//FB 2543//FB 2592
                    //if (!chkVMR.Checked)
                    if (lstVMR.SelectedIndex > 0) //FB 2620
                    {
                        tdFECC.Attributes.Add("Style", "display:none;"); //FB 2595
                        chkFECC.Visible = false;
                    }
                    else
                    {
                        tdFECC.Attributes.Add("Style", "display:inline;"); //FB 2595
                        chkFECC.Visible = true;
                    }
                /** FB 2376 **/
                if (node.SelectSingleNode("//advAVParam/internalBridge") != null)
                {
                    txtintbridge.Text = node.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                    hdnintbridge.Value = node.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                }

                if (node.SelectSingleNode("//advAVParam/externalBridge") != null)
                {
                    txtextbridge.Text = node.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                    hdnextbridge.Value = node.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                }

                /** FB 2376 **/

                // FB 2583 FECC for EXPRESS Starts
                if (node.SelectSingleNode("//advAVParam/FECCMode").InnerText.Equals("1"))
                    chkFECC.Checked = true;
                else
                    chkFECC.Checked = false;
                // FB 2583 FECC for EXPRESS Ends

                //FB 2632 Starts
                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/OnSiteAVSupport") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/OnSiteAVSupport").InnerText.Equals("1"))
                        chkOnSiteAVSupport.Checked = true;
                    else
                        chkOnSiteAVSupport.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/MeetandGreet") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/MeetandGreet").InnerText.Equals("1"))
                        chkMeetandGreet.Checked = true;
                    else
                        chkMeetandGreet.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/ConciergeMonitoring") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/ConciergeMonitoring").InnerText.Equals("1"))
                        chkConciergeMonitoring.Checked = true;
                    else
                        chkConciergeMonitoring.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator").InnerText.Equals("1"))
                        chkDedicatedVNOCOperator.Checked = true;
                    else
                        chkDedicatedVNOCOperator.Checked = false;
                }
                //FB 2632 Ends

                //FB 2501 Starts FB 2670 START
                
                ConfVNOCnodes = node.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperatorID");
                if (ConfVNOCnodes.Count > 0)
                {
                    hdnVNOCOperator.Text = "";
                    for (int i = 0; i < ConfVNOCnodes.Count; i++)
                    {
                        if (ConfVNOCnodes[i].InnerText != null)
                        {
                            if (hdnVNOCOperator.Text == "")
                                hdnVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                            else
                                hdnVNOCOperator.Text += "," + ConfVNOCnodes[i].InnerText.Trim();
                        }
                    }
                }
                ConfVNOCnodes = null;
                ConfVNOCnodes = node.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperator");
                if (ConfVNOCnodes.Count > 0)
                {
                    txtVNOCOperator.Text = "";
                    for (int i = 0; i < ConfVNOCnodes.Count; i++)
                    {
                        {
                            if (ConfVNOCnodes[i].InnerText != null)
                            {
                                if (hdnVNOCOperator.Text == "")
                                    txtVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                                else
                                    txtVNOCOperator.Text += "\n" + ConfVNOCnodes[i].InnerText.Trim();
                            }
                        }
                    }
                }
                if (txtVNOCOperator.Text != "")
                    chkDedicatedVNOCOperator.Checked = true;
                //FB 2501 Ends FB 2670 END
                //FB 2608 - Starts
                if (isClone && EnableVNOCselection == 0)
                {
                    txtVNOCOperator.Text = "";//FB 2670
                }
                //FB 2608 - End
				// FB 2693 Starts
                if (node.SelectSingleNode("//conference/confInfo/isPCconference") != null)
                {
                    chkPCConf.Checked = true;
                    if (node.SelectSingleNode("//conference/confInfo/isPCconference").InnerText.Equals("0"))
                    {
                        chkPCConf.Checked = false;
                        CheckPCVendor();
                    }

                    if (node.SelectSingleNode("//conference/confInfo/isPCconference").InnerText.Equals("1"))
                    {
                        tblPcConf.Style.Add("display", "block");
                        if (node.SelectSingleNode("//conference/confInfo/pcVendorId") != null)
                        {
                            int nodeValue = Int32.Parse(node.SelectSingleNode("//conference/confInfo/pcVendorId").InnerText);
                            switch (nodeValue)
                            {
                                case 1:
                                    if (Session["EnableBlueJeans"].ToString().Equals("1"))
                                        rdBJ.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                case 2:
                                    if (Session["EnableJabber"].ToString().Equals("1"))
                                        rdJB.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                case 3:
                                    if (Session["EnableLync"].ToString().Equals("1"))
                                        rdLync.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                case 4:
                                    if (Session["EnableVidtel"].ToString().Equals("1"))
                                        rdVidtel.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                default:
                                    rdBJ.Checked = true;
                                    break;
                            }
                        }
                    }
                }
                // FB 2693 Ends
                

                if ((Request.QueryString["t"].ToString().Equals("t"))) 
                {
                    lblConfID.Text = "new";
                    lblConfHeader.Text = obj.GetTranslatedText("Create New");//FB 1830 - Translation
                }
                if ((Request.QueryString["t"].ToString().Equals("o"))) 
                {
                    lblConfHeader.Text = obj.GetTranslatedText("Create New");//FB 1830 - Translation
                }
                String confStatus = xmlDOC.SelectSingleNode("//conference/confInfo/Status").InnerText;
                txtTimeCheck.Text = "0";
                nodes = xmlDOC.SelectNodes("//conference/confInfo/fileUpload/file");
                GetUploadedFiles(nodes);
               // RefreshRoom(null, null);
                btnRoomSelect_Click(null, null);
                ifrmPartylist.Attributes["src"] = "settings2partyNET.aspx?wintype=ifr";

                if (Request.QueryString["t"].ToString() == "")
                {
                    GetAdvancedAVSettings();
                }
                //FB 1830 Email Edit - start
                Session.Remove("XCONFINFO");
                Session.Add("XCONFINFO", xConfInfo);
                //FB 1830 Email Edit - end
                //FB 2426 Start
                XmlNodeList profNodes = null;
                String address = "", connType = "", Password = "", LineRate = "";
                int addType = 0, IsDefault = 0;
                nodes = node.SelectNodes("//conference/confInfo/GuestLocationList/ConfGuestRooms/ConfGuestRoom");
                for (int i = 0; i < nodes.Count; i++)
                {
                    DataRow dr = null;
                    CreateDtColumnNames();
                    onflyGrid = obj.LoadDataTable(null, colNames);

                    if (!onflyGrid.Columns.Contains("RowUID"))
                        onflyGrid.Columns.Add("RowUID");

                    dr = onflyGrid.NewRow();
                    dr["RowUID"] = onflyGrid.Rows.Count;

                    if (nodes[i].SelectSingleNode("RoomID") != null)
                        dr["RoomID"] = nodes[i].SelectSingleNode("RoomID").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("GuestRoomName") != null)
                        dr["RoomName"] = nodes[i].SelectSingleNode("GuestRoomName").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactName") != null)
                        dr["ContactName"] = nodes[i].SelectSingleNode("ContactName").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactEmail") != null)
                        dr["ContactEmail"] = nodes[i].SelectSingleNode("ContactEmail").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactPhoneNo") != null)
                        dr["ContactPhoneNo"] = nodes[i].SelectSingleNode("ContactPhoneNo").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("RoomAddress") != null)
                        dr["ContactAddress"] = nodes[i].SelectSingleNode("RoomAddress").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("State") != null)
                        dr["State"] = nodes[i].SelectSingleNode("State").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("City") != null)
                        dr["City"] = nodes[i].SelectSingleNode("City").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ZipCode") != null)
                        dr["ZIP"] = nodes[i].SelectSingleNode("ZipCode").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("Country") != null)
                        dr["Country"] = nodes[i].SelectSingleNode("Country").InnerText.Trim();

                    profNodes = nodes[i].SelectNodes("//Profiles/Profile");
                    for (int p = 0; p < profNodes.Count; p++)
                    {
                        if (profNodes[p].SelectSingleNode("AddressType") == null)
                            continue;

                        if (profNodes[p].SelectSingleNode("Address") != null)
                            address = profNodes[p].SelectSingleNode("Address").InnerText.Trim();

                        if (profNodes[p].SelectSingleNode("Password") != null)
                            Password = profNodes[p].SelectSingleNode("Password").InnerText.Trim();


                        if (profNodes[p].SelectSingleNode("MaxLineRate") != null)
                            LineRate = profNodes[p].SelectSingleNode("MaxLineRate").InnerText.Trim();

                        if (profNodes[p].SelectSingleNode("ConnectionType") != null)
                            connType = profNodes[p].SelectSingleNode("ConnectionType").InnerText.Trim();

                        if (profNodes[p].SelectSingleNode("isDefault") != null)
                            Int32.TryParse(profNodes[p].SelectSingleNode("isDefault").InnerText.Trim(), out IsDefault);

                        if (profNodes[p].SelectSingleNode("AddressType") != null)
                            Int32.TryParse(profNodes[p].SelectSingleNode("AddressType").InnerText.Trim(), out addType);


                        switch (connType.ToString())
                        {
                            case ns_MyVRMNet.vrmConnectionTypes.DialIn:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Dial-in to MCU");
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.DialOut:
                            case ns_MyVRMNet.vrmConnectionTypes.DialOutOld:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Dial-out from MCU");
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.Direct:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Direct to MCU");
                                break;
                            default:
                                dr["DefaultConnetionType"] = obj.GetTranslatedText("Dial-in to MCU");
                                break;
                        }

                        switch (addType)
                        {
                            case 1:
                                dr["IPAddressType"] = "1";
                                dr["IPAddress"] = address;
                                dr["IPPassword"] = Password;
                                dr["IPconfirmPassword"] = Password;
                                dr["IPMaxLineRate"] = LineRate;
                                dr["IPConnectionType"] = connType;
                                dr["IsIPDefault"] = IsDefault;
                                if (IsDefault == 1)
                                {
                                    dr["DefaultAddressType"] = obj.GetTranslatedText("IP Address");
                                    dr["DefaultAddress"] = address;
                                }
                                break;
                            case 4:
                                dr["ISDNAddressType"] = "4";
                                dr["ISDNAddress"] = address;
                                dr["ISDNPassword"] = Password;
                                dr["ISDNconfirmPassword"] = Password;
                                dr["ISDNMaxLineRate"] = LineRate;
                                dr["ISDNConnectionType"] = connType;
                                dr["IsISDNDefault"] = IsDefault;
                                if (IsDefault == 1)
                                {
                                    dr["DefaultAddressType"] = obj.GetTranslatedText("ISDN Address");
                                    dr["DefaultAddress"] = address;
                                }
                                break;
                            case 6:
                                dr["SIPAddressType"] = "6";
                                dr["SIPAddress"] = address;
                                dr["SIPPassword"] = Password;
                                dr["SIPconfirmPassword"] = Password;
                                dr["SIPMaxLineRate"] = LineRate;
                                dr["SIPConnectionType"] = connType;
                                dr["IsSIPDefault"] = IsDefault;
                                if (IsDefault == 1)
                                {
                                    dr["DefaultAddressType"] = obj.GetTranslatedText("E164/SIP Address");
                                    dr["DefaultAddress"] = address;
                                }
                                break;
                        }

                    }
                    onflyGrid.Rows.Add(dr);
                }
                BindOptionData();
                dgOnflyGuestRoomlist.Visible = true;
                //FB 2426 end
                if (node.SelectSingleNode("//advAVParam/maxLineRateID") != null) //FB 2394
                    if (node.SelectSingleNode("//advAVParam/maxLineRateID").InnerText.Trim() != "")
                    {
                        lstLineRate.ClearSelection();
                        lstLineRate.SelectedValue = node.SelectSingleNode("//advAVParam/maxLineRateID").InnerText;
                    }
                //FB 2501 starts
                if (node.SelectSingleNode("//conference/confInfo/StartMode").InnerText != null)
                    lstStartMode.SelectedValue = node.SelectSingleNode("//conference/confInfo/StartMode").InnerText;
                //FB 2501 Ends

                //FB 2595 - Starts //FB 2993 Starts
                drpNtwkClsfxtn.ClearSelection();
                if (node.SelectSingleNode("//conference/confInfo/Secured") != null)
                    if (drpNtwkClsfxtn.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/Secured").InnerText) != null)
                        drpNtwkClsfxtn.SelectedValue = node.SelectSingleNode("//conference/confInfo/Secured").InnerText;
                //FB 2595 End //FB 2993 Ends
                //FB 2870 Start
                if (node.SelectSingleNode("//conference/confInfo/EnableNumericID") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/EnableNumericID").InnerText.Equals("1"))
                        ChkEnableNumericID.Checked = true;
                    else
                        ChkEnableNumericID.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/CTNumericID") != null)
                    txtNumeridID.Text = node.SelectSingleNode("//conference/confInfo/CTNumericID").InnerText;

                HtmlGenericControl MyDiv = (HtmlGenericControl)Page.FindControl("DivNumeridID");

                if (ChkEnableNumericID.Checked)
                    MyDiv.Attributes.Add("style", "display:block");
                else
                    MyDiv.Attributes.Add("style", "display:none");

                //FB 2870 End

                //FB 2998
                if (node.SelectSingleNode("//conference/confInfo/McuSetupTime") != null)
                    txtMCUConnect.Text = node.SelectSingleNode("//conference/confInfo/McuSetupTime").InnerText;

                if (mcuSetupDisplay == "0")
                    txtMCUConnect.Text = Session["McuSetupTime"].ToString();


                if (node.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime") != null)
                    txtMCUDisConnect.Text = node.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime").InnerText;

                if (mcuTearDisplay == "0")
                    txtMCUDisConnect.Text = Session["MCUTeardonwnTime"].ToString();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindDataOld: " + ex.Message);
            }
        }
        #endregion

        #region BindRecurrenceDefault

        private void BindRecurrenceDefault()
        {
            try
            {

                if (!IsPostBack)
                {
                    Int32 r = 0;
                    foreach (myVRMNet.NETFunctions.RecurringPattern myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.RecurringPattern)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272 - Starts
                        RecurType.Items.Insert(r++, li);
                    }

                    r = 0;
                    foreach (myVRMNet.NETFunctions.WeekDays myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.WeekDays)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        WeekDay.Items.Insert(r++, li);
                    }

                    //WeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.WeekDays));
                    //WeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DayCount myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DayCount)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        MonthWeekDayNo.Items.Insert(r++, li);
                    }
                    //MonthWeekDayNo.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DayCount));
                    //MonthWeekDayNo.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DaysType myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DaysType)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        MonthWeekDay.Items.Insert(r++, li);
                    }
                    //MonthWeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DaysType));
                    //MonthWeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DayCount myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DayCount)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonthWeekDayNo.Items.Insert(r++, li);
                    }
                    //YearMonthWeekDayNo.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DayCount));
                    //YearMonthWeekDayNo.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DaysType myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DaysType)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonthWeekDay.Items.Insert(r++, li);
                    }
                    //YearMonthWeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DaysType));
                    //YearMonthWeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.MonthNames myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.MonthNames)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonth1.Items.Insert(r++, li);
                    }
                    //YearMonth1.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                    //YearMonth1.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.MonthNames myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.MonthNames)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonth2.Items.Insert(r++, li);
                    }
                    //YearMonth2.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                    //YearMonth2.DataBind();
                    //FB 2272 - End
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindRecurrenceDefault : " + ex.Message);
            }
        }

        #endregion

        #region Hide recur button
        /// <summary>
        /// To hide the recur button on instance edit
        /// </summary>
        protected void HideRecurButton()
        {
            try
            {
                isInstanceEdit = "";
                if (Session["IsInstanceEdit"] != null)
                {
                    if (Session["IsInstanceEdit"].ToString() != "")
                        isInstanceEdit = Session["IsInstanceEdit"].ToString();
                }
            }
            catch (Exception ex)
            {
                log.Trace("Hide recur button : " + ex.Message);
            }
        }

        #endregion        

        #region SetEndTime
        private void SetEndTime()
        {
            try
            {
                //FB 2634
                //if (Recur.Value.Trim().Equals(""))
                //{
                //    log.Trace("In SetEndTime");
                //    DateTime t = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text); 
                //    DateTime et = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);
                //    if (et <= t)
                //    {
                //        t = t.AddHours(1.00);
                //        if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ)) 
                //            t = t.AddMinutes(15);
                //        confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(t);
                //        confEndTime.Text = t.ToString(tformat);
                //    }
                //    CalculateDuration();
                //}
        }
            catch (Exception ex)
            {
                log.Trace("Error SetEndTime: " + ex.Message);
                DisplayDialog("Invalid Start Date/Time.");
            }
        }
        #endregion

        #region confStartTime_TextChanged
        protected void confStartTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                log.Trace("In confStartTime_TextChanged");
                SetEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("Error confStartTime_TextChanged: " + ex.Message);
            }
        }
        #endregion

        #region confStartTime_SelectedIndexChanged
        protected void confStartTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                log.Trace("In confStartTime_SelectedIndexChanged");
                SetEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("Error confStartTime_SelectedIndexChanged : " + ex.Message);
            }
        }
        #endregion

        #region confEndTime_SelectedIndexChanged

        protected void confEndTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                StartLessThanEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("confEndTime_SelectedIndexChanged : " + ex.Message);
            }
        }

        #endregion
        
        #region confEndTime_TextChanged

        protected void confEndTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                StartLessThanEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("confEndTime_TextChanged : " + ex.Message);
            }
        }

        #endregion

        #region StartLessThanEndTime
        private void StartLessThanEndTime()
        {
            try
            {
                log.Trace("In StartLessThenEndTime");
                //FB 2634
                //DateTime tStart = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text); //FB 2634
                //DateTime tEnd = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);
                //log.Trace(tStart.ToLongDateString() + " : " + tEnd.ToLongDateString());
                //if (tStart >= tEnd)
                //{
                //    log.Trace("in if");
                //    DisplayDialog("End time will be changed because it should be greater than start time.");
                //    ResetEndTime();
                //    confEndTime.Focus();
                //}
            }
            catch (Exception ex)
            {
                log.Trace("Error StartLessThrenEndTime : " + ex.Message);
                DisplayDialog("Invalid Date/Time.");
                ResetEndTime();
                confEndTime.Focus();
            }
        }
        #endregion

        #region ResetEndTime
        private void ResetEndTime()
        {
            try
            {
                log.Trace("In ResetEndTime");
                DateTime tReset = Convert.ToDateTime(confStartDate.Text + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //FB 2588
                tReset = tReset.AddHours(1.00);
                confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(tReset);
                confEndTime.Text = tReset.ToString(tformat);
                CalculateDuration();
            }
            catch (Exception ex)
            {
                log.Trace("ResetEndTime: : " + ex.Message);
                DisplayDialog("Invalid Date/Time.");
            }
        }
        #endregion

        #region CalculateDuration
        /// <summary>
        /// To calculate Conference Duration
        /// </summary>
        public void CalculateDuration()
        {
            try
            {
                log.Trace("In CalculateDuration");
                TimeSpan durationMin = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text).Subtract(DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text))); //FB 2588
                log.Trace(durationMin.ToString());
                lblConfDuration.Text = "";
                if (durationMin.TotalMinutes < 0)
                    lblConfDuration.Text = obj.GetTranslatedText("Invalid duration");//FB 1830 - Translation
                
                if (Math.Floor(durationMin.TotalDays) > 0)
                    lblConfDuration.Text = Math.Floor(durationMin.TotalDays) + " days ";
                if (Math.Floor(durationMin.TotalHours) > 0)
                    lblConfDuration.Text += durationMin.Hours + " hrs ";
                if (durationMin.Minutes > 0)
                    lblConfDuration.Text += durationMin.Minutes + " mins";
            }
            catch (Exception ex)
            {
                log.Trace("CalculateDuration : " + ex.Message);
                lblConfDuration.Text = obj.GetTranslatedText("Invalid duration");//FB 1830 - Translation
            }
        }

        #endregion      
        
        #region  Get Party details

        private void getParty(XmlNodeList nodes, bool isClone) //FB 2550
        {
            int pLength = nodes.Count;
            partysInfo = "";
            string confPartys = ""; //FB 1830 Email Edit
            for (int i = 0; i < pLength; i++)
            {
                //FB 2550
                if (isClone && nodes[i].SelectSingleNode("partyPublicVMR").InnerText.Equals("1"))
                    continue;

                //FB 1830 Email Edit - start
                if (confPartys == "")
                    confPartys = nodes[i].SelectSingleNode("partyID").InnerText.Trim();
                else
                    confPartys += "!!" + nodes[i].SelectSingleNode("partyID").InnerText.Trim(); //FB 1888 start
                //FB 1830 Email Edit - end

                if ((nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("1")) && (nodes[i].SelectSingleNode("partyAudVid").InnerText.Equals("1")))
                {
                    Int32.TryParse(nodes[i].SelectSingleNode("partyID").InnerText.Trim(), out audioPartyId);
                    hdnAudioInsIDs.Value = audioPartyId.ToString();
                    continue;
                }

                partysInfo += nodes[i].SelectSingleNode("partyID").InnerText + "!!";
                partysInfo += nodes[i].SelectSingleNode("partyFirstName").InnerText + "!!"; //.Replace(","," ")
                partysInfo += nodes[i].SelectSingleNode("partyLastName").InnerText + "!!";
                partysInfo += nodes[i].SelectSingleNode("partyEmail").InnerText + "!!";

                if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("0")) //CC
                    partysInfo += "0!!0!!1!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("1")) //External Attendee
                    partysInfo += "1!!0!!0!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("2")) //Room attendee
                    partysInfo += "0!!1!!0!!";
                else
                    partysInfo += "0!!0!!0!!";
                if (nodes[i].SelectSingleNode("partyNotify").InnerText.Equals("1"))
                    partysInfo += "1!!";
                else
                    partysInfo += "0!!";
                if (nodes[i].SelectSingleNode("partyAudVid").InnerText.Equals("2")) //AudioVideo
                    partysInfo += "1!!0!!";
                else if (nodes[i].SelectSingleNode("partyAudVid").InnerText.Equals("1")) //Audio only
                    partysInfo += "0!!1!!";
                else
                    partysInfo += "0!!0!!"; //None
                if (nodes[i].SelectNodes("partyAddressType").Count > 0)
                {
                    partysInfo += nodes[i].SelectSingleNode("partyProtocol").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyConnectionType").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyAddress").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyAddressType").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyIsOutside").InnerText; //FB 2550
                }
                else
                    partysInfo += "!!!!-3!!!!"; //FB 1888 end //FB 2550

                //FB 2550 Starts - Survey -2!!pcparty!!vmrparty!!publicparty
                if (nodes[i].SelectSingleNode("partyPublicVMR").InnerText.Equals("1"))
                    partysInfo += "!!0!!0!!0!!0!!1||";
                else
                    partysInfo += "!!0!!0!!0!!0!!0||";
                //FB 2550 Ends
                txtPartysInfo.Text = partysInfo;
            }
            if (!xConfInfo.ContainsKey("partys"))   //FB 1830 Email Edit
                xConfInfo.Add("partys", confPartys.Trim());
        }
        #endregion
                
        #region GetUploadedFiles

        protected void GetUploadedFiles(XmlNodeList nodes)
        {
            try
            {
                int i = 0;
                foreach (XmlNode node in nodes)
                    if (!node.InnerText.Equals(""))
                    {
                        i++;
                        String fPath = node.InnerText;
                        if (node.InnerText != "")
                        {
                            fPath = fPath.Replace("\\", "/");
                            int startIndex = fPath.IndexOf("/" + language + "/");//FB 1830
                            int len = fPath.Length - 1;
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + fPath.Substring(startIndex + 3);//FB 1830
                            switch (i)
                            {
                                case 1:
                                    lblUpload1.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    hdnUpload1.Text = node.InnerText;
                                    btnRemove1.Visible = true;
                                    FileUpload1.Visible = false;
                                    lblUpload1.Visible = true;
                                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                                    break;
                                case 2:
                                    lblUpload2.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    hdnUpload2.Text = node.InnerText;
                                    btnRemove2.Visible = true;
                                    FileUpload2.Visible = false;
                                    lblUpload2.Visible = true;
                                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                                    break;
                                case 3:
                                    lblUpload3.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    hdnUpload3.Text = node.InnerText;
                                    btnRemove3.Visible = true;
                                    FileUpload3.Visible = false;
                                    lblUpload3.Visible = true;
                                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                                    break;
                            }
                        }
                    }
                GetFileList();
            }
            catch (Exception ex)
            {
                log.Trace("GetUploadedFiles : " + ex.Message);
            }
        }

        #endregion

        #region getUploadFilePath
        protected string getUploadFilePath(string fpn)
        {
            String fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '\\' };
                String[] fa = fpn.Split('\\');
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }
        #endregion

        #region RemoveFile

        protected void RemoveFile(Object sender, CommandEventArgs e)
        {
            try
            {
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                Button btnTemp = new Button();
                switch (e.CommandArgument.ToString())
                {
                    case "1":
                        lblTemp = hdnUpload1;
                        inTemp = FileUpload1;
                        lblFname = lblUpload1;
                        lblHdnName = hdnUpload1;
                        btnTemp = btnRemove1;
                        aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                        break;
                    case "2":
                        lblTemp = hdnUpload2;
                        inTemp = FileUpload2;
                        lblFname = lblUpload2;
                        lblHdnName = hdnUpload2;
                        btnTemp = btnRemove2;
                        aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                        break;
                    case "3":
                        lblTemp = hdnUpload3;
                        inTemp = FileUpload3;
                        lblFname = lblUpload3;
                        lblHdnName = hdnUpload3;
                        btnTemp = btnRemove3;
                        aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                        break;
                }
                if (lblTemp.Text != "")
                {
                    FileInfo fi = new FileInfo(lblTemp.Text);
                    if (fi.Exists)
                        fi.Delete();
                    inTemp.Visible = true;
                    inTemp.Disabled = false;
                    lblFname.Visible = false;
                    lblFname.Text = "";
                    lblHdnName.Visible = false;
                    lblHdnName.Text = "";
                    btnTemp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("RemoveFile : " + ex.Message);
            }
        }

        #endregion

        #region UploadFiles

        protected void UploadFiles(Object sender, EventArgs e)
        {
            try
            {
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData;
                String errMsg = "";

                //ZD 100263
                string filextn = "";
                string[] strExtension = null;
                string[] strSplitExtension = null;//ZD 101052
                bool filecontains = false;
                string uploadFileExtn = "";
                int EnableFileWhiteList = 0;
                if (Session["EnableFileWhiteList"] != null && !string.IsNullOrEmpty(Session["EnableFileWhiteList"].ToString()))
                {
                    int.TryParse(Session["EnableFileWhiteList"].ToString(), out EnableFileWhiteList);
                }

                if (Session["FileWhiteList"] != null && !string.IsNullOrEmpty(Session["FileWhiteList"].ToString()))
                {
                    uploadFileExtn = Session["FileWhiteList"].ToString();
                }

                if (!FileUpload1.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload1.Value);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');
                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }

                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload1.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 10000000)
                    {
                        WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                        FileUpload1.Disabled = true;
                        lblUpload1.Text = fName;
                        hdnUpload1.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                        FileUpload1.Visible = false;
                        lblUpload1.Visible = true;
                        btnRemove1.Visible = true;
                        aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                    }
                    else
                        errMsg += "Attachment 1 is greater than 10MB. File has not been uploaded.";
                }

                if (!FileUpload2.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload2.Value);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');
                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                        
                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload2.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (FileUpload2.Value.Equals(FileUpload1.Value))
                        errMsg += "Attachment 2 has already been uploaded.";
                    else
                    {
                        if (nFileLen <= 10000000)
                        {
                            WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            FileUpload2.Disabled = true;
                            lblUpload2.Text = fName;
                            hdnUpload2.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                            FileUpload2.Visible = false;
                            lblUpload2.Visible = true;
                            btnRemove2.Visible = true;
                            aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                        }
                        else
                            errMsg += "Attachment 3 is greater than 10MB. File has not been uploaded.";
                    }
                }

                if (!FileUpload3.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload3.Value);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');

                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                        
                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload3.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (FileUpload3.Value.Equals(FileUpload1.Value) || FileUpload3.Value.Equals(FileUpload2.Value))
                        errMsg += "Attachment 3 has already been uploaded.";
                    else
                    {
                        if (nFileLen <= 10000000)
                        {
                            WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            FileUpload3.Disabled = true;
                            hdnUpload3.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                            lblUpload3.Text = fName;
                            lblUpload3.Visible = true;
                            FileUpload3.Visible = false;
                            btnRemove3.Visible = true;
                            aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                        }
                        else
                            errMsg += "Attachment 3 is greater than 10MB. File has not been uploaded.";
                    }
                }
                if (FileUpload1.Value.Equals("") && FileUpload2.Value.Equals("") && FileUpload3.Value.Equals(""))
                  {
                      if (lblFileList.Text.Trim() == "")
                          aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                  }
                
                GetFileList();
                
                if(lblFileList.Text.Trim() != "")
                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files. : " + ex.Message);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region WriteToFile

        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace("WriteToFile : " + ex.Message);
            }
        }

        #endregion

        #region DisplayDialog
        protected void DisplayDialog(string strDisplayMessage)
        {
            try
            {
                string script = "<Script language='javascript'>";
                script += "callalert('" + strDisplayMessage + "')";
                script += "</script>";
                Literal literal = new Literal();
                literal.Text = script;
                Page.FindControl("frmSettings2").Controls.Add(literal);
            }
            catch (Exception ex)
            {
                log.Trace("DisplayDialog: " + ex.Message);
            }
        }

        #endregion        

        #region rdSelView_SelectedIndexChanged

        protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selRooms = "";
            Int32 cnt = 0;
            Int32 mCnt = 0;
            Int32 tCnt = 0;
            if (rdSelView.SelectedValue.Equals("2"))
            {
                pnlListView.Visible = true;
                pnlLevelView.Visible = false;
                lstRoomSelection.ClearSelection();
                HtmlInputCheckBox selectAll = (HtmlInputCheckBox)FindControl("selectAllCheckBox");

                if (selectedloc.Value.Trim() != "")
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                            if (lstItem.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                            {
                                lstItem.Selected = true;
                                cnt = cnt + 1;
                            }
                    }

                    if (selectAll != null)
                    {
                        if (cnt == lstRoomSelection.Items.Count)
                            selectAll.Checked = true;
                        else
                            selectAll.Checked = false;
                    }
                }
                else
                {
                    if (selectAll != null)
                        selectAll.Checked = false;
                }
            }
            else
            {
                pnlLevelView.Visible = true;
                pnlListView.Visible = false;

                foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                {
                    tCnt = 0;
                    foreach (TreeNode tnMid in tnTop.ChildNodes)
                    {
                        mCnt = 0;
                        foreach (TreeNode tn in tnMid.ChildNodes)
                        {
                            tn.Checked = false;
                            if (selectedloc.Value.Trim() != "")
                            {
                                for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                                    if (tn.Depth.Equals(3) && tn.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                                    {
                                        tn.Checked = true;
                                        mCnt++;
                                    }
                            }
                        }
                        if (mCnt == tnMid.ChildNodes.Count)
                        {
                            tnMid.Checked = true;
                            tCnt++;
                        }
                        else
                            tnMid.Checked = false;
                    }

                    if (tCnt == tnTop.ChildNodes.Count)
                    {
                        tnTop.Checked = true;
                        treeRoomSelection.Nodes[0].Checked = true;
                    }
                    else
                    {
                        tnTop.Checked = false;
                        treeRoomSelection.Nodes[0].Checked = false;
                    }
                }

            }
        }

        #endregion

        #region LoadCommonValues

        public void LoadCommonValues(XmlNode node)
        {
            XmlNodeList nodes = null;
            lstRoomSelection.Items.Clear();
            treeRoomSelection.Nodes.Clear();
            TreeNode tn = new TreeNode("All");
            tn.Expanded = true;
            treeRoomSelection.Nodes.Add(tn);
            nodes = node.SelectNodes("/conference/confInfo/locationList");
            PreSelectRooms(nodes);
            nodes = node.SelectNodes("/conference/confInfo/locationList/level3List/level3");
            GetLocationList(nodes);

            if (nodes.Count == 0)
            {
                rdSelView.Enabled = false;
                pnlListView.Visible = false;
                pnlLevelView.Visible = false;
                btnCompare.Enabled = false;
                pnlNoData.Visible = false;
            }
        }
        #endregion

        #region PreSelectRooms

        protected void PreSelectRooms(XmlNodeList nodes)
        {
            XmlNodeList selnodes = nodes.Item(0).SelectNodes("selected/level1ID");
            int selLength = selnodes.Count;

            if (selLength >= 1)
                for (int n = 0; n < selLength; n++)
                    selRooms += selnodes.Item(n).InnerText + ", ";
            if (Request.QueryString["rms"] != null)
                selRooms += Request.QueryString["rms"].ToString();
            selectedloc.Value = selRooms.Trim(',');
        }

        #endregion

        #region GetLocationList

        protected void GetLocationList(XmlNodeList nodes)
        {
            try
            {
                ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");
                int nodes2Count = 0;
                int length = nodes.Count;
                for (int i = 0; i < length; i++)
                {
                    TreeNode tn3 = new TreeNode(nodes.Item(i).SelectSingleNode("level3Name").InnerText, nodes.Item(i).SelectSingleNode("level3ID").InnerText);
                    tn3.SelectAction = TreeNodeSelectAction.None;
                    treeRoomSelection.Nodes[0].ChildNodes.Add(tn3);
                    XmlNodeList nodes2 = nodes.Item(i).SelectNodes("level2List/level2");
                    tn3.Expanded = true;
                    int length2 = nodes2.Count;
                    if (!Application["roomExpandLevel"].ToString().ToLower().Equals("list"))
                        if (Application["roomExpandLevel"] != null)
                        {
                            if (Application["roomExpandLevel"].ToString() != "")
                            {
                                if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 2)
                                    tn3.Expanded = true;
                            }
                        }
                        else
                            tn3.Expanded = false;
                    for (int j = 0; j < length2; j++)
                    {
                        TreeNode tn2 = new TreeNode(nodes2.Item(j).SelectSingleNode("level2Name").InnerText, nodes2.Item(j).SelectSingleNode("level2ID").InnerText);

                        tn2.SelectAction = TreeNodeSelectAction.None;
                        treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Add(tn2);
                        XmlNodeList nodes1 = nodes2.Item(j).SelectNodes("level1List/level1");
                        int length1 = nodes1.Count;
                        tn2.Expanded = true; 
                        nodes2Count = 0;
                        for (int k = 0; k < length1; k++)
                        {
                            TreeNode tn = new TreeNode(nodes1.Item(k).SelectSingleNode("level1Name").InnerText, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                            tn.ToolTip = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                            tn.Value = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                            treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Add(tn);
                            tn.NavigateUrl = @"javascript:chkresource('" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "');";
                            string l1Name = "<a href='#'  title='" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "'  onclick='javascript:chkresource(\"" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "\");'>" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "</a>";
                            li = new ListItem(l1Name, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                            lstRoomSelection.Font.Size = FontUnit.Smaller;
                            lstRoomSelection.ForeColor = System.Drawing.Color.ForestGreen;
                            lstRoomSelection.Items.Add(li);
                            if (selRooms.IndexOf(" " + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + ",") >= 0)
                            {
                                nodes2Count++;
                                tn.Checked = true;
                                li = new ListItem(tn.Text, tn.Value);
                            }
                        }

                        if (nodes1.Count.Equals(nodes2Count))
                            tn2.Checked = true; 
                        if (!Application["roomExpandLevel"].ToString().ToLower().Equals("list"))
                            if (Application["roomExpandLevel"] != null)
                            {
                                if (Application["roomExpandLevel"].ToString() != "")
                                {
                                    if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 3)
                                        tn2.Expanded = true;
                                }
                            }
                            else
                                tn2.Expanded = false;
                    }
                }
                for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                    for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                        if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                        {
                            ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                            lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                            log.Trace(i + " : " + lstRoomSelection.Items[i].Text.Substring(lstRoomSelection.Items[i].Text.IndexOf(">")) + " : " + lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[j].Value = liTemp.Value;
                            lstRoomSelection.Items[j].Text = liTemp.Text;
                        }
                foreach (ListItem listItem in lstRoomSelection.Items)
                {
                    for (int r = 0; r < selRooms.Split(',').Length - 1; r++)
                    {

                        if (listItem.Value.Equals(selRooms.Split(',')[r].Trim()))
                        {
                            listItem.Selected = true;
                        }
                    }
                }
                if (Application["RoomListView"].ToString().ToUpper().Equals("LIST"))
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message);
            }
        }

        #endregion

        #region treeRoomSelection_TreeNodeCheckChanged


        protected void treeRoomSelection_TreeNodeCheckChanged(object sender, EventArgs e)
        {
            foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
            {
                if (tnTop.Parent.Checked.Equals(true)) tnTop.Checked = true;
                foreach (TreeNode tnMid in tnTop.ChildNodes)
                {
                    if (tnMid.Parent.Checked.Equals(true)) tnMid.Checked = true;
                    foreach (TreeNode tn in tnMid.ChildNodes)
                    {
                        if (tn.Parent.Checked.Equals(true)) tn.Checked = true;
                    }
                }
            }
            ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");//FB 2272
            if (treeRoomSelection.CheckedNodes.Count > 0)
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    if (tn.Depth.Equals(3))
                    {
                        selRooms += ", " + tn.Value;
                        li = new ListItem(tn.Text, tn.Value);
                    }
        }

        #endregion

        #region treeRoomSelection_SelectedNodeChanged

        protected void treeRoomSelection_SelectedNodeChanged(object sender, EventArgs e)
        {
            errLabel.Visible = true;
            errLabel.Text = "";
        }

        #endregion

        #region FillCustomAttributeTable

        private void FillCustomAttributeTable()
        {
            XmlDocument xmlDOC = new XmlDocument();
            xmlDOC.LoadXml(Session["outxml"].ToString());
            XmlNode node = (XmlNode)xmlDOC.DocumentElement;

            XmlNode node1 = node.SelectSingleNode("//conference/confInfo/CustomAttributesList");
            if (node1 != null)
            {
                string caxml = node1.OuterXml;
                if (CAObj == null)
                    CAObj = new myVRMNet.CustomAttributes();

                //FB 2592
                //custControlIDs = CAObj.CreateExpressAttributes(caxml, tblHost, tblWeb, tblSpecial, isFormSubmit, ref hdnHostIDs);
                custControlIDs = CAObj.CreateExpressAttributes(caxml, tblHost, tblSpecial, tblCustomAttribute, true, ref hdnHostIDs);
                
                hdnCusID.Value = custControlIDs;//FB 2592
                //FB 2349 Start
                if (tblSpecial.Rows.Count <= 0)
                    trsplinst.Attributes.Add("Style", "Display:None");
                //FB 2349 End
            }
        }
        #endregion
		
		//FB 2839
        #region FillMCUProfile
        /// <summary>
        /// FillMCUProfile
        /// </summary>
        private void FillMCUProfile()
        {
            bool RMXbrid = false; //FB 3063
            try
            {
                String inXML = "<GetBridges>" + obj.OrgXMLElement() + "<UserID>" + Session["userID"].ToString() + "</UserID></GetBridges>";//Organization Module Fixes
                String outXML = obj.CallMyVRMServer("GetBridges", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNode node;
                StringBuilder inXML1 = new StringBuilder();
                XmlNodeList nodes = xmldoc.SelectNodes("//Bridges/Bridge");
                int bridgetypeid = 0, MCUProfID = 0;
                String BridgeName = "", BridgeID = "", ConfServiceID = ""; ;
                for (int row = 0; row < nodes.Count; row++)
                {
                    node = nodes[row];
                    if (node.SelectSingleNode("BridgeType") != null)
                        Int32.TryParse(node.SelectSingleNode("BridgeType").InnerText, out bridgetypeid);

                    //ZD 100298 Start
                    if (bridgetypeid == 8 || bridgetypeid == 13)
                        RMXbrid = true; //FB 3063
                    else
                        continue;
                    //ZD 100298 End

                    if (node.SelectSingleNode("BridgeName") != null)
                        BridgeName = node.SelectSingleNode("BridgeName").InnerText;

                    if (node.SelectSingleNode("BridgeID") != null)
                        BridgeID = node.SelectSingleNode("BridgeID").InnerText;

                    if (node.SelectSingleNode("ConferenceServiceID") != null)
                        ConfServiceID = node.SelectSingleNode("ConferenceServiceID").InnerText;
                    if (ConfServiceID == "0") //ZD 100298
                        ConfServiceID = "-1";

                    TableRow rw = new TableRow();

                    TableCell cell = new TableCell();
                    cell.Width = Unit.Percentage(32);
                    Label text = new Label();
                    text.CssClass = "blackblodtext";
                    text.Text = BridgeName;
                    cell.Controls.Add(text);
                    TableCell cell1 = new TableCell();
                    inXML1 = new StringBuilder();
                    inXML1.Append("<GetMCUProfiles>");
                    inXML1.Append("<UserID>" + Session["userID"].ToString() + "</UserID>" + "<OrganizationID>" + Session["OrganizationID"].ToString() + "</OrganizationID>");
                    inXML1.Append("<MCUId>" + BridgeID + "</MCUId>");
                    inXML1.Append("</GetMCUProfiles>");
                    log.Trace("MCU Profile Details InXML " + inXML1.ToString());
                    string outXML1 = obj.CallMyVRMServer("GetMCUProfiles", inXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument xmldoc1 = new XmlDocument();
                    xmldoc1.LoadXml(outXML1);
                    XmlNodeList nodes1 = xmldoc1.SelectNodes("//GetMCUProfiles/Profile");
                    XmlNode node1;

                    DropDownList lstMCUProfiles = new DropDownList();
                    lstMCUProfiles.CssClass = "altText";
                    lstMCUProfiles.ID = "DrpMCU_" + row;
                    lstMCUProfiles.Width = Unit.Percentage(55); //ZD 100298

                    ListItem li = new ListItem();

                    if (nodes1.Count > 0)
                    {
                        for (int i = 0; i < nodes1.Count; i++)
                        {
                            if (i == 0)
                            {
                                li = new ListItem(obj.GetTranslatedText("None"), "-1"); //ZD 100298
                                lstMCUProfiles.Items.Add(li);
                            }
                            node1 = nodes1[i];
                            li = new ListItem(node1.SelectSingleNode("Name").InnerText, node1.SelectSingleNode("Id").InnerText);
                            lstMCUProfiles.Items.Add(li);
                        }
                        RMXbrid = true; //FB 3052
                        cell1.Controls.Add(lstMCUProfiles);
                        rw.Cells.Add(cell);
                        rw.Cells.Add(cell1);

                        tblMCUProfile.Controls.Add(rw);
                    }
                    else
                    {
                        li = new ListItem(obj.GetTranslatedText("None"), "-1", true); //ZD 100298
                        lstMCUProfiles.Items.Add(li);
                        lstMCUProfiles.Visible = false; //FB 3052
                    }

                    if (Session["EnableProfileSelection"] != null)
                    {
                        
                            string nd1 = "";
                            if (Session["Confbridge"] != null)
                            {
                                nd1 = Session["Confbridge"].ToString();
                                XmlDocument xmldoc2 = new XmlDocument();
                                xmldoc2.LoadXml(nd1);

                                XmlNodeList endpointNodes = xmldoc2.SelectNodes("descendant::GetAdvancedAVSettings/Endpoints/Endpoint[Type='R']");
                                if (endpointNodes != null)
                                {
                                    foreach (XmlNode nd in endpointNodes)
                                    {
                                        if (BridgeID == nd.SelectSingleNode("BridgeID").InnerText.Trim())
                                            ConfServiceID = nd.SelectSingleNode("BridgeProfileID").InnerText.Trim();
                                    }
                                }
                            }
                            if (Session["EnableProfileSelection"].ToString() == "1") //FB 3052
                                lstMCUProfiles.Enabled = true; 
                            else
                                lstMCUProfiles.Enabled = false;
                    }
                    if (ConfServiceID == "0") //ZD 100298
                        ConfServiceID = "-1";
                    int.TryParse(ConfServiceID, out MCUProfID);
                    //  if (MCUProfID > 0)
                    {
                        lstMCUProfiles.Items.FindByValue(ConfServiceID).Selected = true;

                        hdnMcuProfileSelected.Value += BridgeID + "?" + lstMCUProfiles.ID + "$";
                    }
                }
                //FB 3063 Start
                if (RMXbrid)
                    trMCUProfile.Visible = true;
                else
                    trMCUProfile.Visible = false;
                //FB 3063 End
            }
            catch (Exception ex)
            {
                log.Trace("FillMCUProfile : " + ex.Message);
            }
        }
        
        #endregion

        #region SetConference

        //FB 1830 Email Edit - start
        protected void EmailDecision(object sender, EventArgs e)
        {
            try
            {
                //FB 2634
                //if (ValidateConferenceDur() && CheckTime()) //Code added for Time Zone
                //if (CheckTime()) //Code added for Time Zone
                //      NotificationAlert();

                //FB 2634
                if (ValidateConferenceDur() && CheckTime()) //Code added for Time Zone
                    NotificationAlert();
            }
            catch (Exception ex)
            {
                log.Trace("EmailDecision : " + ex.Message);
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
                
        private void NotificationAlert()
        {
            try
            {
                hdnemailalert.Value = "";
                if (isEditMode == "1")
                {
                    if (CheckForUserInput())
                        hdnemailalert.Value = "2";
                }
                String respStr = "<script>fnemailalert();</script>";
                ClientScript.RegisterStartupScript(GetType(), "EmailScript", respStr);
            }
            catch (Exception e)
            {
                log.Trace("NotificationAlert : " + e.Message);
            }
        }
        //FB 1830 Email Edit - end

        protected void SetConference(object sender, EventArgs e)
        {
            try
            {
                //if (ValidateConferenceDur()) //Commented during FB 1830 Email Edit
                //if (ValidateConferenceDur() && CheckTime()) //Code added for Time Zone
                    SetConference();
            }
            catch (Exception ex)
            {
                log.Trace("SetConference : " + ex.Message);
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }

        protected void SetConferenceCustom(object sender, EventArgs e)
        {
            try
            {
                isSetCustom = true;
                SetConference();
            }
            catch (Exception ex)
            {
                log.Trace("SetConferenceCustom : " + ex.Message);
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }

        protected bool SetConference()
        {
            DataTable dtRooms = new DataTable();
            String confVMR = "0";//FB 2376
            int v = 0; //FB 2670
            try
            {
                CreateDataTable(dtRooms);

                int maxDuration = 24;
                if (Application["MaxConferenceDurationInHours"] != null)
                    if (!Application["MaxConferenceDurationInHours"].ToString().Trim().Equals(""))
                        maxDuration = Int32.Parse(Application["MaxConferenceDurationInHours"].ToString().Trim());

                GetXConfParams(); //FB 1830 Email Edit

                //FB 2367 start
                String[] roomsSelcted = locstrname.Value.Split(DoublePlusDelim, StringSplitOptions.RemoveEmptyEntries);
                ListItem item = null;
                RoomList.Items.Clear();
                for (int i = 0; i < roomsSelcted.Length; i++)
                {
                    if (roomsSelcted[i].Trim() == "")
                        continue;
                   
                    item = new ListItem();
                    item.Value = roomsSelcted[i].Split('|')[0];
                    item.Text = roomsSelcted[i].Split('|')[1];
                    RoomList.Items.Add(item);
                }
                //FB 2367 end

                if (lstLineRate.SelectedValue.Trim() == "-1") 
                    lstLineRate.SelectedValue = "384";

                StringBuilder inxml = new StringBuilder();
                inxml.Append("<conference>");
                inxml.Append(obj.OrgXMLElement());
                //FB 2544 - Requestor starts ... modified during FB 2540
                inxml.Append("<requestorID>" + hdnApprover7.Text + "</requestorID>");
                inxml.Append("<userID>" + Session["userid"].ToString() + "</userID>");
                //FB 2544 - Requestor ends ... modified during FB 2540
                inxml.Append("<confInfo>");
                if (Request.QueryString["t"].ToString().Equals("o"))
                {
                    inxml.Append("<confID>new</confID>");
                }
                else
                    inxml.Append("<confID>" + lblConfID.Text + "</confID>");
                
                inxml.Append(" <confName>" + ConferenceName.Text + "</confName>");
                inxml.Append("<confHost>" + hdnApprover4.Text + "</confHost>");
                inxml.Append("<confOrigin>0</confOrigin>");
                inxml.Append("<timeCheck>" + txtTimeCheck.Text + "</timeCheck>");
                //inxml.Append("<confPassword></confPassword>");
                inxml.Append("<confPassword>" + confPassword.Value + "</confPassword>");//FB 2501

                //if (chkVMR.Checked) //FB 2376
                if (lstVMR.SelectedIndex > 0) //FB 2620
                {
                    // confVMR = "1"; //FB 2620
                    lstStartMode.SelectedValue = "0";//FB 2501
                }
                // Fb 2620 Start
                inxml.Append("<isVMR>" + lstVMR.SelectedIndex + "</isVMR>");
                //inxml.Append("<isVMR>" + confVMR + "</isVMR>");//FB 2376

                // Fb 2620 Ends
                //FB 2717 Start
                if (chkCloudConferencing.Checked)
                    inxml.Append("<CloudConferencing>1</CloudConferencing>");
                else
                    inxml.Append("<CloudConferencing>0</CloudConferencing>");
                //FB 2717 End

                // FB 2693 Starts
                if (chkPCConf.Checked)
                {
                    inxml.Append("<isPCconference>1</isPCconference>");

                    string value = "1";
                    if (rdBJ.Checked)
                        value = "1";
                    else if (rdJB.Checked)
                        value = "2";
                    else if (rdLync.Checked)
                        value = "3";
                    else if (rdVidtel.Checked)
                        value = "4";

                    inxml.Append("<pcVendorId>" + value + "</pcVendorId>");

                }
                else
                {
                    inxml.Append("<isPCconference>0</isPCconference>");
                    inxml.Append("<pcVendorId>0</pcVendorId>");
                }
                // FB 2693 Ends

                int durationMin;
                //FB 2634
                Int32 setupDuration = Int32.MinValue;
                Int32 tearDuration = Int32.MinValue;

                obj.isBufferChecked = true; //FB 2398
                inxml.Append("<StartMode>" + lstStartMode.SelectedValue + "</StartMode>"); //FB 2501
                //FB 2609 Start
                if (Session["MeetandGreetBuffer"] != null)
                {
                    if (!Session["MeetandGreetBuffer"].ToString().Equals(""))
                    {
                        inxml.Append("<MeetandGreetBuffer>" + Session["MeetandGreetBuffer"].ToString() + "</MeetandGreetBuffer>");
                    }
                }
                //FB 2609 End
                //FB 2595 Start //FB 2993 Starts
                if (NetworkSwitching == 2)
                   inxml.Append("<Secured>" + drpNtwkClsfxtn.SelectedValue + "</Secured>");
                else
                     inxml.Append("<Secured>0</Secured>");
                //FB 2595 Ends //FB 2993 Ends
                //FB 2870 Start
                if (ChkEnableNumericID.Checked)
                {
                    inxml.Append("<EnableNumericID>1</EnableNumericID>");
                    inxml.Append("<CTNumericID>" + txtNumeridID.Text + "</CTNumericID>");
                }
                else
                {
                    inxml.Append("<EnableNumericID>0</EnableNumericID>");
                    inxml.Append("<CTNumericID></CTNumericID>");
                }
                //FB 2870 End
                //FB 2998
                if (EnableBufferZone == 1 && txtMCUConnect.Text.Trim() != "" && !chkStartNow.Checked && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                    inxml.Append("<McuSetupTime>" + txtMCUConnect.Text + "</McuSetupTime>");
                else
                    inxml.Append("<McuSetupTime>0</McuSetupTime>");

                if (EnableBufferZone == 1 && txtMCUDisConnect.Text.Trim() != "" && !chkStartNow.Checked && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                    inxml.Append("<MCUTeardonwnTime>" + txtMCUDisConnect.Text + "</MCUTeardonwnTime>");
                else
                    inxml.Append("<MCUTeardonwnTime>0</MCUTeardonwnTime>");

                //inxml.Append("<immediate>0</immediate>");//FB 2341
                //FB 1911
                if (chkStartNow.Checked ||(Recur.Value == "" && RecurSpec.Value == "")) //FB 2341
                {
                    //FB 2341 - Start
                    if (chkStartNow.Checked)
                    {
                        inxml.Append("<immediate>1</immediate>");
                        inxml.Append("<recurring>0</recurring>");
                        inxml.Append("<recurringText></recurringText>");
                        inxml.Append("<startDate></startDate>");
                        inxml.Append("<startHour></startHour>");
                        inxml.Append("<startMin></startMin>");
                        inxml.Append("<startSet></startSet>");
                        inxml.Append("<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>");
                        //Response.Write(lstDuration.Text);
                        durationMin = Convert.ToInt32(lstDuration.Text.Split(':')[0]) * 60 + Convert.ToInt32(lstDuration.Text.Split(':')[1]);

                        //code added for buffer zone -- Start
                        inxml.Append("<setupDuration>0</setupDuration>");
                        inxml.Append("<teardownDuration>0</teardownDuration>");
                        inxml.Append("<setupDateTime></setupDateTime>");
                        inxml.Append("<teardownDateTime></teardownDateTime>");

                        //code added for buffer zone -- End
                    }
                    //FB 2341 - Ends
                    else
                    {
                        inxml.Append("<immediate>0</immediate>");//FB 2341

                        int sHour = 0;
                        if (Session["timeFormat"] != null)
                        {
                            if (Session["timeFormat"].ToString().Equals("1"))
                            {
                                sHour = Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[0]);

                                if ((confStartTime.Text.Split(' ')[1] == "PM") && (sHour != 12))
                                    sHour += 12;
                                if ((confStartTime.Text.Split(' ')[1] == "AM") && (sHour == 12))
                                    sHour -= 12;
                            }
                        }
                        DateTime dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //FB 2588

                        if (Session["timeFormat"] != null)
                        {
                            if (Session["timeFormat"].ToString().Equals("1"))
                            {
                                sHour = Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]);
                                if ((confEndTime.Text.Split(' ')[1] == "PM") && (sHour != 12))
                                    sHour += 12;
                                if ((confEndTime.Text.Split(' ')[1] == "AM") && (sHour == 12))
                                    sHour -= 12;
                            }
                        }

                        DateTime dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text)); //FB 2634 //FB 2588
                        //FB 2398 Start
                        //if (EnableBufferZone == 1)
                        //{
                        //    dEnd = dEnd.AddMinutes(OrgTearDownTime);
                        //}
                        //FB 2398 End
                        TimeSpan ts = dEnd.Subtract(dStart);
                        durationMin = Int32.Parse(ts.TotalMinutes.ToString());
						//FB 2634
                        if (EnableBufferZone == 1)
                        {
                            Int32.TryParse(SetupDuration.Text, out setupDuration);
                            Int32.TryParse(TearDownDuration.Text, out tearDuration);
                        }
                        else
                        {
                            setupDuration = 0;
                            tearDuration = 0;
                        }

                        dStart = dStart.AddMinutes(-setupDuration);
                        durationMin += tearDuration + setupDuration;

                        inxml.Append("<recurring>0</recurring>");
                        inxml.Append("<recurringText></recurringText>");
                        inxml.Append("<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(dStart.ToString(format)) + "</startDate>");
                        inxml.Append("<startHour>" + dStart.ToString("hh") + "</startHour>");
                        inxml.Append("<startMin>" + dStart.ToString("mm") + "</startMin>");
                        inxml.Append("<startSet>" + dStart.ToString("tt") + "</startSet>");

                        inxml.Append("<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>");

                        //FB 2634
                        DateTime sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //FB 2588
                        DateTime tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                        sDateTime = sDateTime.AddSeconds(45);
                        tDateTime = tDateTime.AddSeconds(45);

                        if (EnableBufferZone == 0)
                        {
                            sDateTime = dStart;
                            tDateTime = dEnd;
                        }

                        inxml.Append("<setupDuration></setupDuration>");
                        inxml.Append("<teardownDuration></teardownDuration>");
                        inxml.Append("<setupDateTime>" + sDateTime + "</setupDateTime>");
                        inxml.Append("<teardownDateTime>" + tDateTime + "</teardownDateTime>");
                    }
                    inxml.Append("<createBy>2</createBy>");

                    if (durationMin >= 15 && durationMin <= (maxDuration * 60))
                        inxml.Append("<durationMin>" + durationMin.ToString() + "</durationMin>");
                    else
                    {
                        //FB 2634
                        if (durationMin < 15)
                            errLabel.Text = ns_MyVRMNet.ErrorList.InvalidDuration;

                        if (durationMin > (maxDuration * 60))
                            errLabel.Text = ns_MyVRMNet.ErrorList.ExceedDuration;

                        errLabel.Visible = true;
                        return false;
                    }
                }
                else
                {
                    inxml.Append("<immediate>0</immediate>");//FB 2341
                    inxml.Append("<createBy>2</createBy>");
					
                    int sDur = 0;
                    int tDur = 0;

                    if (hdnSetupTime.Value == "" || hdnSetupTime.Value == null)
                        hdnSetupTime.Value = "0";

                    if (hdnTeardownTime.Value == "" || hdnTeardownTime.Value == null)
                        hdnTeardownTime.Value = "0";

                    Int32.TryParse(hdnSetupTime.Value, out sDur);
                    Int32.TryParse(hdnTeardownTime.Value, out tDur);

                    if (EnableBufferZone == 0)
                    {
                        sDur = 0;
                        tDur = 0;

                    }
                    else
                    {
                        Int32.TryParse(SetupDuration.Text, out sDur);
                        Int32.TryParse(TearDownDuration.Text, out tDur);
                    }

                    inxml.Append("<setupDuration>" + sDur + "</setupDuration>");
                    inxml.Append("<teardownDuration>" + tDur + "</teardownDuration>");
                    inxml.Append("<setupDateTime></setupDateTime>");
                    inxml.Append("<teardownDateTime></teardownDateTime>");

                    string bufferxml = "<setupDuration>" + sDur + "</setupDuration>";
                    bufferxml += "<teardownDuration>" + tDur + "</teardownDuration>";
                    inxml.Append("<recurring>1</recurring>");
                    inxml.Append("<recurringText>" + RecurringText.Value + "</recurringText>");

                    //FB 1911
                    // Need to added a hidden which is to be set when special rec is set
                    if (Session["isSpecialRecur"].ToString().Equals("1") && hdnSpecRec.Value == "1" && isEditMode == "0") //FB 2052
                    {
                        String recXML = "";
                        String strInXml = "";
                        strInXml = obj.AppendSpecialRecur(RecurSpec.Value, bufferxml, lstConferenceTZ.SelectedValue, ref recXML);
                        //FB 2027 - Start
                        if (strInXml.ToString().IndexOf("<error>") >= 0)
                        {
                            XmlDocument xDoc = new XmlDocument();
                            xDoc.LoadXml(strInXml);
                            errLabel.Text = xDoc.SelectSingleNode("error").InnerText;
                            errLabel.Visible = true;
                            return false;
                        }
                        else
                            inxml.Append(strInXml);
                        //FB 2027 - End
                    }
                    else
                        inxml.Append(obj.AppendRecur(Recur.Value, bufferxml, lstConferenceTZ.SelectedValue));
                }
                inxml.Append("<description>" + utilObj.ReplaceInXMLSpecialCharacters(ConferenceDescription.Text) + "</description>"); //FB 2236_s
                inxml.Append("<locationList>");
                inxml.Append("<selected>");

                //FB 2367 Start
                //foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                //{
                //    foreach (TreeNode tnMid in tnTop.ChildNodes)
                //    {
                //        foreach (TreeNode tn in tnMid.ChildNodes)
                //        {
                //            if (tn.Checked.Equals(true))
                //            {
                //                inxml.Append("<level1ID>" + tn.Value + "</level1ID>");
                //                dtRooms.Rows.Add(AddRoomEndpoint(tn.Value, dtRooms));

                //            }
                //        }
                //    }
                //}
                String[] locs = selectedloc.Value.Split(',');
                for (int i = 0; i < locs.Length; i++)
                {
                    if (locs[i].Trim() == "")
                        continue;

                    inxml.Append("<level1ID>" + locs[i] + "</level1ID>");
                    //if (!chkVMR.Checked) //FB 2448
                    if (!(lstVMR.SelectedIndex > 0)) //FB 2620
                        dtRooms.Rows.Add(AddRoomEndpoint(locs[i], dtRooms));
                }
                //FB 2367 end
                inxml.Append("</selected>");
                inxml.Append("</locationList>");
                //FB 2426 Start
                string Roomid = "";
                inxml.Append( "		<ConfGuestRooms>");
                foreach (DataGridItem item1 in dgOnflyGuestRoomlist.Items)
                {
                    inxml.Append("<ConfGuestRoom>");
                    inxml.Append("<LoginUserID>" + Session["userID"].ToString() + "</LoginUserID>");
                    inxml.Append(obj.OrgXMLElement());
                    inxml.Append("<GuestRoomUID>" + item1.Cells[0].Text.ToString() + "</GuestRoomUID>");
                    Roomid = item1.Cells[1].Text.ToString().Replace("&nbsp;", "");
                    if (Roomid == "")
                        Roomid = "new";
                    inxml.Append("<GuestRoomID>" + Roomid + "</GuestRoomID>");
                    inxml.Append("<GuestRoomName>" + item1.Cells[2].Text.ToString() + "</GuestRoomName>");
                    inxml.Append("<ContactName>" + item1.Cells[3].Text.ToString() + "</ContactName>");
                    inxml.Append("<ContactEmail>" + item1.Cells[4].Text.ToString() + "</ContactEmail>");
                    inxml.Append("<ContactPhoneNo>" + item1.Cells[5].Text.ToString() + "</ContactPhoneNo>");
                    inxml.Append("<RoomAddress>" + item1.Cells[6].Text.ToString() + "</RoomAddress>");
                    inxml.Append("<State>" + item1.Cells[7].Text.ToString() + "</State>");
                    inxml.Append("<City>" + item1.Cells[8].Text.ToString() + "</City>");//To be done 6
                    inxml.Append("<ZipCode>" + item1.Cells[9].Text.ToString() + "</ZipCode>");
                    inxml.Append("<Country>" + item1.Cells[10].Text.ToString() + "</Country>");
                    inxml.Append("<Tier1>" + Session["OnflyTopTierID"].ToString() + "</Tier1>");
                    inxml.Append("<Tier2>" + Session["OnflyMiddleTierID"].ToString() + "</Tier2>");
                    inxml.Append("<Profiles>");
                    int inc = 0;
                    int j = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        item1.Cells[12 + inc].Text = item1.Cells[12 + inc].Text.ToString().Replace("&nbsp;", "");
                        if (item1.Cells[12 + inc].Text.ToString().Trim() != "")
                        {
                            j = j + 1;
                            inxml.Append("<Profile>");
                            inxml.Append("<EndpointName>" + item1.Cells[2].Text.ToString() + "</EndpointName>");
                            inxml.Append("<ProfileName>" + item1.Cells[2].Text.ToString() + "_" + j + "</ProfileName>");
                            inxml.Append("<AddressType>" + item1.Cells[11 + inc].Text.ToString() + "</AddressType>");
                            inxml.Append("<Address>" + item1.Cells[12 + inc].Text.ToString() + "</Address>");
                            inxml.Append("<Password>" + item1.Cells[13 + inc].Text.ToString() + "</Password>");
                            inxml.Append("<confirmPassword>" + item1.Cells[14 + inc].Text.ToString() + "</confirmPassword>");
                            inxml.Append("<MaxLineRate>" + item1.Cells[15 + inc].Text.ToString() + "</MaxLineRate>");
                            inxml.Append("<ConnectionType>" + item1.Cells[16 + inc].Text.ToString() + "</ConnectionType>");
                            inxml.Append("<isDefault>" + item1.Cells[17 + inc].Text.ToString() + "</isDefault>");
                            inxml.Append("</Profile>");
                            inc += 7;
                        }
                        else
                        {
                            inc += 7;
                        }
                    }
                    inxml.Append("</Profiles>");
                    inxml.Append("</ConfGuestRoom>");
                }
                inxml.Append("</ConfGuestRooms>");
                //FB 2426 End

                //FB 2226 - Start
                if (chkPublic.Checked)
                {
                    inxml.Append("<publicConf>1</publicConf>");
                    if (chkOpenForRegistration.Checked)
                        inxml.Append("<dynamicInvite>1</dynamicInvite>");
                    else
                        inxml.Append("<dynamicInvite>0</dynamicInvite>");
                }
                else
                {
                    inxml.Append("<publicConf>0</publicConf>");
                    inxml.Append("<dynamicInvite>0</dynamicInvite>");
                }
               
                //FB 2223 Starts
                //if (Session["defaultPublic"].ToString().Equals("1"))
                //    inxml.Append("<publicConf>1</publicConf>");
                //else
                //    inxml.Append("<publicConf>0</publicConf>");
                //FB 2223 ends
                //FB 2226 - End
                inxml.Append("<dynamicInvite>0</dynamicInvite>");
                inxml.Append("<advAVParam> ");
                inxml.Append("<maxAudioPart></maxAudioPart>");
                inxml.Append("<maxVideoPart></maxVideoPart>");
                inxml.Append("<restrictProtocol>3</restrictProtocol>"); //Restrict Network access to IP,ISDN,SIP
                inxml.Append("<restrictAV>2</restrictAV>"); //Restrict Usage default to AudioVideo
                //FB 2524 Starts
                //if (client.ToUpper() == "DISNEY") 
                //    inxml.Append("<videoLayout>02</videoLayout>");
                //else
                    inxml.Append("<videoLayout>01</videoLayout>");
                //FB 2424 Ends
                inxml.Append("<maxLineRateID>" + lstLineRate.SelectedValue + "</maxLineRateID>"); //FB 2394
                inxml.Append("<audioCodec>0</audioCodec>");
                inxml.Append("<videoCodec>0</videoCodec>");
                inxml.Append("<dualStream>1</dualStream>");
                inxml.Append("<confOnPort>0</confOnPort>");
                inxml.Append("<encryption>0</encryption>");
                inxml.Append("<lectureMode>0</lectureMode>");
                inxml.Append("<VideoMode>3</VideoMode>");
                inxml.Append("<SingleDialin>0</SingleDialin>");
                inxml.Append("<internalBridge>" + hdnintbridge.Value + "</internalBridge>"); //FB 2376
                inxml.Append("<externalBridge>" + hdnextbridge.Value + "</externalBridge>");
                //inxml.Append("<VNOCOperator>" + hdnApprover6.Text + "</VNOCOperator>"); //FB 2501
                //FB 2583 FECC Starts//FB 2571
                   //if (!chkVMR.Checked && chkFECC.Checked)
                if (!(lstVMR.SelectedIndex > 0) && chkFECC.Checked)//FB 2620
                        inxml.Append("<FECCMode>1</FECCMode>");
                    else
                        inxml.Append("<FECCMode>0</FECCMode>");
                
                // FB 2583 FECC Ends
                inxml.Append("</advAVParam>");

                //FB 2377 - Starts
                /*String tmpStr = "";//FB 2341
                foreach (ListItem li in ChklstConcSupport.Items)
                    if (li.Selected)
                        tmpStr += li.Value + ",";
                if (tmpStr.Length > 0)
                    tmpStr = tmpStr.Substring(0, tmpStr.Length - 1);
                inxml.Append("<ConceirgeSupport>" + tmpStr + "</ConceirgeSupport>");*/
                //FB 2377 - End
                
                int partyInvite = 2; // Room Attendee
                int partyAudVid = 2; // Media type  - Video 

                string[] partysary = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries); //FB 1888
                int partynum = partysary.Length;
                //FB 1830 Email Edit - start
                int partyid = 0;
                string sendemail = "1";
                //FB 1830 Email Edit - end
                inxml.Append("<partys>");//NUll CHeck
                for (int i = 0; i < partynum ; i++) //FB 1888
                {
                    string[] partyary = partysary[i].Split(ExclamDelim, StringSplitOptions.None); //FB 1888 //FB 2550
                    inxml.Append("<party>");
                    String UserID = partyary[0].ToString();
                    if (UserID.Trim().IndexOf("new") >= 0)
                        UserID = "new";

                    //FB 1830 Email Edit - start
                    if (isEditMode == "1")
                    {
                        if (hdnemailalert.Value == "0") //Dont notify all participants on edit
                        {
                            partyid = 0;
                            int.TryParse(UserID, out partyid);
                            sendemail = "0";
                            if (xPartys != null)
                            {
                                if (!xPartys.Contains(partyid)) //Notify new participants alone
                                    sendemail = "1";
                            }
                        }
                    }
                    //FB 1830 Email Edit - end
                    inxml.Append("<partyID>" + UserID + "</partyID>");
                    inxml.Append("<partyFirstName>" + partyary[1].ToString() + "</partyFirstName>");
                    inxml.Append("<partyLastName>" + partyary[2].ToString() + "</partyLastName>");
                    inxml.Append("<partyEmail>" + partyary[3].ToString() + "</partyEmail>");
                    inxml.Append("<partyInvite>" + partyInvite + "</partyInvite>");
                    inxml.Append("<partyNotify>1</partyNotify>");
                    inxml.Append("<partyAudVid>" + partyAudVid + "</partyAudVid>");
                    inxml.Append("<notifyOnEdit>" + sendemail + "</notifyOnEdit>"); //FB 1830 Email Edit
                    inxml.Append("<partyPublicVMR>" + partyary[19].ToString() + "</partyPublicVMR>"); //FB 2550
                    inxml.Append("</party>");
                }

                audioDialString = "";
                audioPartyId = 0;                

                if(lstAudioParty.SelectedValue != "-1" && lstAudioParty.SelectedValue != "")
                {
                    string[] audioIns = lstAudioParty.SelectedValue.Split('|');

                    if (audioIns.Length > 4)
                    {
                        Int32.TryParse(audioIns[0], out audioPartyId);

                        if (audioPartyId > 0)
                        {
                            //FB 1830 Email Edit - start
                            if (isEditMode == "1")
                            {
                                if (hdnemailalert.Value == "0") //Dont notify all participants on edit
                                {
                                    sendemail = "0";
                                    if (xPartys != null)
                                    {
                                        if (!xPartys.Contains(audioPartyId)) //Notify new participants alone
                                            sendemail = "1";
                                    }
                                }
                            }
                            //FB 1830 Email Edit - end
                            audioDialString = txtAudioDialNo.Text.Trim() + "D" + txtConfCode.Text.Trim() + "+" + txtLeaderPin.Text.Trim();

                            string [] partyName = lstAudioParty.SelectedItem.Text.Split(' ');
                            string lastName = "";
                            if (partyName.Length > 1)
                                lastName = partyName[1];
                            
                            partyInvite = 1; // Default to External Attendee
                            partyAudVid = 1; // Default to Media type Audio

                            inxml.Append("<party>");
                            inxml.Append("<partyID>" + audioPartyId + "</partyID>");
                            inxml.Append("<partyFirstName>" + partyName[0] + "</partyFirstName>");
                            inxml.Append("<partyLastName>" + lastName + "</partyLastName>");
                            inxml.Append("<partyEmail>" + audioIns[1] + "</partyEmail>");
                            inxml.Append("<partyInvite>" + partyInvite + "</partyInvite>");
                            inxml.Append("<partyNotify>1</partyNotify>");
                            inxml.Append("<partyAudVid>" + partyAudVid + "</partyAudVid>");
                            inxml.Append("<notifyOnEdit>" + sendemail + "</notifyOnEdit>"); //FB 1830 Email Edit
                            inxml.Append("</party>");
                        }
                    }
                }
                inxml.Append("</partys>");//NUll CHeck
                               
                if (Recur.Value.Trim() != "")
                    txtModifyType.Text = "1";
                else
                    txtModifyType.Text = "0";

                inxml.Append("<ModifyType>" + txtModifyType.Text + "</ModifyType>");
                inxml.Append("<fileUpload>");
                inxml.Append("<file>" + hdnUpload1.Text + "</file>");
                inxml.Append("<file>" + hdnUpload2.Text + "</file>");
                inxml.Append("<file>" + hdnUpload3.Text + "</file>");
                inxml.Append("</fileUpload>");
                
                //FB 2632 - Starts
                inxml.Append("<ConciergeSupport>");

                if (chkOnSiteAVSupport.Checked)
                    inxml.Append("<OnSiteAVSupport>1</OnSiteAVSupport>");
                else
                    inxml.Append("<OnSiteAVSupport>0</OnSiteAVSupport>");

                if (chkMeetandGreet.Checked)
                    inxml.Append("<MeetandGreet>1</MeetandGreet>");
                else
                    inxml.Append("<MeetandGreet>0</MeetandGreet>");
                
                if (chkConciergeMonitoring.Checked)
                    inxml.Append("<ConciergeMonitoring>1</ConciergeMonitoring>");
                else
                    inxml.Append("<ConciergeMonitoring>0</ConciergeMonitoring>");
                
                //FB 2501 VNOC Starts //FB 2670 START
                //int vnocStatus = 0; //Vnoc operator not assigned
                if (hdnVNOCOperator.Text.Trim() != "" && txtVNOCOperator.Text.Trim() != "")
                {
                    string[] VNOCIDs = hdnVNOCOperator.Text.Split(',');
                    string[] VNOCName = txtVNOCOperator.Text.Split('\n');
                    inxml.Append("<DedicatedVNOCOperator>1</DedicatedVNOCOperator>");
                    inxml.Append("<VNOCAssignAdminID>" + Session["userID"].ToString() + "</VNOCAssignAdminID>");
                    inxml.Append("<ConfVNOCOperators>");
                    for (v = 0; v < VNOCIDs.Length; v++)
                        if (VNOCIDs[v] != "")
                            inxml.Append("<VNOCOperatorID>" + VNOCIDs[v] + "</VNOCOperatorID>");
                    inxml.Append("</ConfVNOCOperators>");
                }
                else
                {
                    if (chkDedicatedVNOCOperator.Checked)
                    {
                        errLabel.Text = obj.GetTranslatedText("Please select Dedicated VNOC Operator.");
                        errLabel.Visible = true;
                        return false;
                    }
                    inxml.Append("<DedicatedVNOCOperator>0</DedicatedVNOCOperator>");
                    inxml.Append("<VNOCOperatorID></VNOCOperatorID>");
                    inxml.Append("<ConfVNOCOperators></ConfVNOCOperators>");
                }
                inxml.Append("</ConciergeSupport>");
                //FB 2632 - End //FB 2670 END

                inxml.Append("<CustomAttributesList>");

                if (CAObj == null)
                    CAObj = new myVRMNet.CustomAttributes();

                string custAtt = "";//FB 2377 //FB 2592
                //custAtt = CAObj.ExpressAttributesInxml(custControlIDs, tblHost, tblWeb, tblSpecial);
                custAtt = CAObj.ExpressAttributesInxml(custControlIDs, tblHost, tblSpecial, tblCustomAttribute); //FB 2501 //FB 2632

                if (custAtt.IndexOf("<error>") >= 0)
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(custAtt);
                    errLabel.Text = xDoc.SelectSingleNode("error").InnerText;
                    errLabel.Visible = true;
                    return false;
                }
                else
                {
                    inxml.Append(custAtt);
                }
                inxml.Append("</CustomAttributesList>");

                inxml.Append("<ICALAttachment></ICALAttachment>");
                //FB 2441 II Starts
                inxml.Append("<MCUs>");
                for (int i = 0; i < dtRooms.Rows.Count; i++)
                {
                    inxml.Append("<BridgeID>" + dtRooms.Rows[i]["BridgeID"].ToString().Trim() + "</BridgeID>");
                }
                inxml.Append("</MCUs>");
                //FB 2441 II Ends
                inxml.Append("</confInfo>");
                inxml.Append("</conference>");

                if (inxml.ToString().IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(inxml.ToString());
                    errLabel.Visible = true;
                    return false;
                }
                string confInxml = inxml.ToString();
                if (isSetCustom)
                    confInxml = ChangeXML(confInxml);

                log.Trace("Express Conference INXML- " + confInxml);

                //FB 2027 SetConference start
                //String outxml = obj.CallCOM("SetConference", confInxml, Application["COM_ConfigPath"].ToString());
                //FB 2164 Starts
                String outxml = ""; 
                if (confInxml.IndexOf("<error>") < 0)
                    outxml = obj.CallCommand("SetConference", confInxml);
                else
                    return false;
                //FB 2164 Ends
                
                //FB 2027 SetConference end
                
                log.Trace("Express Conference OutXML- " + outxml);
                //Session.Add("outxml", outxml);//FB 2592
                if (outxml.IndexOf("<error>") < 0)
                {
                    Session.Add("outxml", outxml); //FB 2592
                    XmlDocument xmlout = new XmlDocument();
                    xmlout.LoadXml(outxml);
                    String confID = xmlout.SelectSingleNode("//setConference/conferences/conference/confID").InnerText;
                    Session["confID"] = confID;
                    lblConfID.Text = confID;
                    if (SetEndpoints(dtRooms)) 
                    {
                        string inDisXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><selectType>7</selectType><selectID>" + Session["ConfID"].ToString() + "</selectID></login>";
                        
                        log.Trace("SimpleConference GetOldConference InXML- " + inDisXML);
                        string outDisXML = obj.CallMyVRMServer("GetOldConference", inDisXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                        
                        log.Trace("SimpleConference GetOldConference OutXML- " + outDisXML);
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outDisXML);

                        if (outDisXML.IndexOf("<error>") < 0)
                        {
                            outDisXML = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/Status").InnerText);

                            switch (outDisXML)
                            {
                                case ns_MyVRMNet.vrmConfStatus.Scheduled:
                                    showConfMsg.Text = obj.GetTranslatedText("Congratulations! Your conference has been scheduled successfully. Email notifications have been sent to participants.");//FB 1830 - Translation
                                    break;
                                case ns_MyVRMNet.vrmConfStatus.Pending:
                                    showConfMsg.Text = obj.GetTranslatedText("Congratulations! Your conference has been submitted successfully and is currently in pending status awaiting administrative approval.");//FB 1830 - Translation
                                    break;
                               default: //FB 2341
                                    showConfMsg.Text = obj.GetTranslatedText("Congratulations! Your conference has been scheduled successfully.");//FB 1830 - Translation
                                    break;
                            }
                            SubModalPopupExtender.Show();
                            Session["outxml"] = null;
                            Session["CalendarMonthly"] = null;//FB 1850

                            //FB 2363 - Start
                            if (Application["External"].ToString() != "")
                            {
                                String inEXML = "";
                                inEXML = "<SetExternalScheduling>";
                                inEXML += "<confID>" + confID + "</confID>";
                                inEXML += "</SetExternalScheduling>";

                                String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                            }
                            //FB 2363 - End
                            //FB 2426 Start

                            String eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["EptXmlPath"].ToString();
                            if (File.Exists(eptxmlPath))
                                File.Delete(eptxmlPath);
                            String roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();
                            if (File.Exists(roomxmlPath))
                            {
                                if (obj.WaitForFile(roomxmlPath))
                                    File.Delete(roomxmlPath);
                            }

                            //FB 2426 End
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outxml);
                            errLabel.Visible = true;
                            return false;
                        }
                    }
                    return true;
                }
                else
                {
                    string errLevel = obj.GetErrorLevel(outxml);
                    if (errLevel.Equals("C"))
                    {
                        //AddInstanceInfo(inxml.ToString()); //FB 2027 SetConference
                        AddInstanceInfo(inxml.ToString(), outxml); //FB 2027 SetConference
                    }
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                    errLabel.Visible = true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error occurred in SetConference. Please try later.\n" + ex.Message);
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
                return false;
            }
        }

        #endregion

        #region SetEndpoints
        /// <summary>
        /// SetEndpoints
        /// </summary>
        /// <param name="dtrms"></param>
        /// <returns></returns>
        protected bool SetEndpoints(DataTable dtrms)
        {
            string bridgeId = "-1";
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(Session["outxml"].ToString());
                XmlNodeList nodes = xmldoc.SelectNodes("//setConference/conferences/conference/invited/party");
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<SetAdvancedAVSettings>");
                inXML.Append("<editFromWeb>1</editFromWeb>");//FB 2235
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<ConfID>" + lblConfID.Text + "</ConfID>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<AVParams>");
                inXML.Append("<SingleDialin>0</SingleDialin>");
                //FB 2870 Start
                if (ChkEnableNumericID.Checked)
                {
                    inXML.Append("<EnableNumericID>1</EnableNumericID>");
                    inXML.Append("<CTNumericID>" + txtNumeridID.Text + "</CTNumericID>");
                }
                else
                {
                    inXML.Append("<EnableNumericID>0</EnableNumericID>");
                    inXML.Append("<CTNumericID></CTNumericID>");
                }
                //FB 2870 End
                inXML.Append("</AVParams>");
                inXML.Append("<Endpoints>");
                if (!(chkPCConf.Checked || lstVMR.SelectedIndex > 0)) //FB 2819 //FB 2833 - NO ENDPOINTS WILL BE FORMED FOR PC & VMR CONFERENCE
                {
                    //FB 2426 start
                    string guestRoomStr = "";
                    XmlNode GuestRoom = xmldoc.SelectSingleNode("//setConference/conferences/conference/GuestRooms");
                    if (GuestRoom != null)
                    {
                        guestRoomStr = GuestRoom.InnerXml.Trim();
                    }
                    //FB 2426 end

                    //FB 2839
                    Hashtable mculidt = new Hashtable();

                    string optionId = "", optionValue = "", BID = "";
                    String[] ProfileList = hdnMcuProfileSelected.Value.Split('$');
                    string bridgeProfileID = "";
                    if (ProfileList.Length > 0)
                    {
                        for (int s = 0; s < ProfileList.Length - 1; s++)
                        {
                            string BridgeValue = ProfileList[s];
                            String[] attributeDt = BridgeValue.Split('?');
                            if (attributeDt.Length > 0)
                            {
                                bridgeId = attributeDt[0];
                                bridgeProfileID = attributeDt[1];
                                DropDownList drpTemp = (DropDownList)tblMCUProfile.FindControl(bridgeProfileID);
                                if (drpTemp != null) //FB 3052
                                {
                                    optionId = drpTemp.SelectedValue;
                                    optionValue = drpTemp.SelectedItem.Text;
                                }
                                if (optionId == "")
                                    optionValue = "";

                                if (!mculidt.Contains(bridgeId))
                                    mculidt.Add(bridgeId, optionId);

                            }
                        }
                    }
                    //FB 2839 End

                    //Room Endpoints
                    foreach (DataRow dr in dtrms.Rows)
                    {
                        inXML.Append("<Endpoint>");
                        inXML.Append("<Type>R</Type>");
                        inXML.Append("<ID>" + dr["ID"].ToString() + "</ID>");
                        inXML.Append("<UseDefault>1</UseDefault>");
                        inXML.Append("<IsLecturer>0</IsLecturer>");
                        inXML.Append("<EndpointID>" + dr["EndpointID"].ToString() + "</EndpointID>");
                        inXML.Append("<ProfileID>" + dr["ProfileID"].ToString() + "</ProfileID>");
                        inXML.Append("<BridgeID>" + dr["BridgeID"].ToString() + "</BridgeID>");

                        //FB 2839 Start
                        BID = dr["BridgeID"].ToString();
                        IDictionaryEnumerator iEnum = mculidt.GetEnumerator();
                        while (iEnum.MoveNext())
                        {
                            if (BID == iEnum.Key.ToString())
                            {
                                optionId = iEnum.Value.ToString();
                                break;
                            }
                            else
                                optionId = "";
                        }
                        //FB 2839 End

                        inXML.Append("<BridgeProfileID>" + optionId + "</BridgeProfileID>");
                        inXML.Append("<AddressType></AddressType>");
                        inXML.Append("<Address></Address>");
                        inXML.Append("<VideoEquipment></VideoEquipment>");
                        inXML.Append("<connectionType></connectionType>");
                        inXML.Append("<Bandwidth></Bandwidth>");
                        inXML.Append("<IsOutside></IsOutside>");
                        inXML.Append("<DefaultProtocol></DefaultProtocol>");
                        inXML.Append("<Connection>2</Connection>"); //Default to AudioVideo
                        inXML.Append("<URL></URL>");
                        inXML.Append("<Connect2>-1</Connect2>");
                        inXML.Append("<APIPortNo>23</APIPortNo>");
                        inXML.Append("<ExchangeID></ExchangeID>");
                        inXML.Append("</Endpoint>");
                    }
                    inXML.Append(guestRoomStr); //FB 2426
                    //User Endpoints
                    if (audioDialString.Trim().Length > 2)
                    {
                        if (Session["ConfEndPts"] != null)
                            usrEndpoints = (Hashtable)Session["ConfEndPts"];

                        if (audioPartyId > 0)
                        {
                            bridgeId = "-1";
                            if (usrEndpoints != null)
                            {
                                if (usrEndpoints.Contains(audioPartyId))
                                {
                                    bridgeId = usrEndpoints[audioPartyId].ToString().Trim();
                                }
                            }

                            inXML.Append("<Endpoint>");
                            inXML.Append("<Type>U</Type>");
                            inXML.Append("<ID>" + audioPartyId + "</ID>");
                            inXML.Append("<UseDefault>1</UseDefault>"); //Default Profile
                            inXML.Append("<IsLecturer>0</IsLecturer>");
                            inXML.Append("<EndpointID></EndpointID>");
                            inXML.Append("<ProfileID></ProfileID>");
                            inXML.Append("<AddressType>4</AddressType>"); //Set as ISDN
                            inXML.Append("<Address>" + audioDialString + "</Address>");
                            inXML.Append("<BridgeID>" + bridgeId + "</BridgeID>");
                            inXML.Append("<Connection>1</Connection>"); //Default to Audio
                            inXML.Append("<VideoEquipment>-1</VideoEquipment>");
                            inXML.Append("<connectionType>2</connectionType>"); //Default to Dial-out
                            inXML.Append("<Bandwidth>" + lstLineRate.SelectedValue + "</Bandwidth>"); //FB 2394
                            inXML.Append("<IsOutside>1</IsOutside>");
                            inXML.Append("<DefaultProtocol>2</DefaultProtocol>"); //Default to ISDN protocol
                            inXML.Append("<URL></URL>");
                            inXML.Append("<Connect2>-1</Connect2>");
                            inXML.Append("<APIPortNo>23</APIPortNo>");
                            inXML.Append("<ExchangeID></ExchangeID>");
                            inXML.Append("</Endpoint>");
                        }
                    }
                }
                inXML.Append("</Endpoints>");
                inXML.Append("</SetAdvancedAVSettings>");

                String outXML = obj.CallMyVRMServer("SetAdvancedAVSettings", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        #region CreateDataTable
        protected DataTable CreateDataTable(DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("ID"))
                    dt.Columns.Add("ID");
                if (!dt.Columns.Contains("Type"))
                    dt.Columns.Add("Type");
                if (!dt.Columns.Contains("Name"))
                    dt.Columns.Add("Name");
                if (!dt.Columns.Contains("EndpointID"))
                    dt.Columns.Add("EndpointID");
                if (!dt.Columns.Contains("EndpointName"))/***/
                    dt.Columns.Add("EndpointName");
                if (!dt.Columns.Contains("ProfileID"))
                    dt.Columns.Add("ProfileID");
                if (!dt.Columns.Contains("Bandwidth"))
                    dt.Columns.Add("Bandwidth");
                if (!dt.Columns.Contains("connectionType"))
                    dt.Columns.Add("connectionType");
                if (!dt.Columns.Contains("DefaultProtocol"))
                    dt.Columns.Add("DefaultProtocol");
                if (!dt.Columns.Contains("AddressType"))
                    dt.Columns.Add("AddressType");
                if (!dt.Columns.Contains("Address"))
                    dt.Columns.Add("Address");
                if (!dt.Columns.Contains("VideoEquipment"))
                    dt.Columns.Add("VideoEquipment");
                if (!dt.Columns.Contains("BridgeID"))
                    dt.Columns.Add("BridgeID");
                if (!dt.Columns.Contains("Connection"))
                    dt.Columns.Add("Connection");
                if (!dt.Columns.Contains("URL"))
                    dt.Columns.Add("URL");
                if (!dt.Columns.Contains("IsOutside"))
                    dt.Columns.Add("IsOutside");
                if (!dt.Columns.Contains("Connect2"))
                    dt.Columns.Add("Connect2");
                if (!dt.Columns.Contains("DropMCU"))
                    dt.Columns.Add("DropMCU");
                if (!dt.Columns.Contains("ConfCode"))
                    dt.Columns.Add("ConfCode");
                if (!dt.Columns.Contains("LPin"))
                    dt.Columns.Add("LPin");

                return dt;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
                return null;
            }
        }
        #endregion

        #region AddRoomEndpoint 
        protected DataRow AddRoomEndpoint(String RoomID, DataTable dt)
        {
            DataRow dr = dt.NewRow();
            try
            {
                log.Trace("In AddRoomEndpoint");

                dr["ID"] = RoomID;
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + RoomID + "</roomID></login>";
                //String outXML = obj.CallCOM("GetOldRoom", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetOldRoom", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetOldRoom)
                log.Trace("GetOldRoom outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                if (!dt.Columns.Contains("Name"))
                    dt.Columns.Add("Name");
                if (!dt.Columns.Contains("EndpointName"))
                    dt.Columns.Add("EndpointName");
                dr["Name"] = xmldoc.SelectSingleNode("//room/roomName").InnerText.Trim();
                dr["Name"] = "<a href='#' onclick='javascript:chkresource(\"" + dr["ID"].ToString() + "\")'>" + dr["Name"] + "</a>";
                dr["EndpointID"] = xmldoc.SelectSingleNode("//room/endpoint").InnerText.Trim();
                if (dr["EndpointID"].ToString().Equals("") || dr["EndpointID"].ToString().Equals("0") || dr["EndpointID"].ToString().Equals("-1"))
                {
                    dr["EndpointName"] = obj.GetTranslatedText("No Endpoint(s) associated with this room.");
                    throw new Exception(obj.GetTranslatedText("No Endpoint(s) associated with room: ") + dr["Name"].ToString());
                }
                else
                {
                    inXML = "";
                    inXML += "<EndpointDetails>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <EndpointID>" + dr["EndpointID"].ToString() + "</EndpointID>";
                    inXML += "</EndpointDetails>";
                    outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("GetEndpointDetails outxml: " + outXML);
                    XmlDocument xmlEP = new XmlDocument();
                    xmlEP.LoadXml(outXML);
                    if (xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile").Count > 0)
                    {
                        XmlNodeList nodesEP = xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                        dr["ProfileID"] = "0";
                        dr["EndpointName"] = xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText; 
                        foreach (XmlNode node in nodesEP)
                        {
                            if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Equals(node.SelectSingleNode("ProfileID").InnerText))
                            {
                                dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                if (dr["BridgeID"].Equals("0"))
                                    dr["BridgeID"] = "-1";
                                dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                break;
                            }
                        }
                    }
                    else
                    {
                        dr["EndpointID"] = "0";
                        dr["EndpointName"] = obj.GetTranslatedText("No Endpoint(s) associated with this room.");
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dr;
        }
        #endregion

        #region AddInstanceInfo
        //MEthod changed for - FB 2027 SetConference
        protected void AddInstanceInfo(string inXMLSetConference, string outxml)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inXMLSetConference);

                #region Commented during FB 2027 SetConference
                //StringBuilder inxml = new StringBuilder();

                //inxml.Append("<getRecurDateList>");
                //inxml.Append(obj.OrgXMLElement());//Organization Module Fixes
                //inxml.Append("<userID>" + Session["userid"].ToString() + "</userID>");
                //inxml.Append("<confID>" + xmldoc.SelectSingleNode("//conference/confInfo/confID").InnerXml + "</confID>");
                //inxml.Append("<appointmentTime>" + xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime").InnerXml + "</appointmentTime>");
                //inxml.Append("<recurrencePattern>" + xmldoc.SelectSingleNode("//conference/confInfo/recurrencePattern").InnerXml);
                //inxml.Append("</recurrencePattern>");

                //inxml.Append("<rooms>");
                //XmlNodeList nodes = xmldoc.SelectNodes("//conference/confInfo/locationList/selected/level1ID");
                //for (int i = 0; i < nodes.Count; i++)
                //    inxml.Append("<roomID>" + nodes[i].InnerText + "</roomID>");
                //inxml.Append("</rooms>");
                //inxml.Append("</getRecurDateList>");

                //string outxml = obj.CallCOM("GetRecurDateList", inxml.ToString(), Application["COM_configPath"].ToString());
                #endregion

                string setupDur = "0";
                string tearDur = "0";
                double setupDuration = double.MinValue;
                double teardownDuration = double.MinValue;
                if (xmldoc.SelectSingleNode("//conference/confInfo/setupDuration") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/setupDuration").InnerText != "")
                    {
                        setupDur = xmldoc.SelectSingleNode("//conference/confInfo/setupDuration").InnerText;
                    }
                }
                if (xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration").InnerText != "")
                    {
                        tearDur = xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration").InnerText;
                    }
                }

                Double.TryParse(setupDur, out setupDuration);
                Double.TryParse(tearDur, out teardownDuration);

                #region Commented during FB 2027 SetConference
                //if (outxml.IndexOf("<error>") >= 0)
                //{
                //    errLabel.Text = obj.ShowErrorMessage(outxml);
                //    errLabel.Visible = true;
                //}
                //else
                //{
                #endregion

                xmldoc = null;
                xmldoc = new XmlDocument(); //FB 2027 SetConference
                xmldoc.LoadXml(outxml);
                XmlNodeList nodes = xmldoc.SelectNodes("//error/dateList/dateTime");  //FB 2027 SetConference
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                    dv = new DataView(ds.Tables[0]);
                else
                    dv = new DataView();
                dt = dv.Table;

                if (!dt.Columns.Contains("formatDate"))
                    dt.Columns.Add("formatDate");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    switch (dt.Rows[i]["conflict"].ToString())
                    {
                        case "0":
                            dt.Rows[i]["conflict"] = "No conflict";
                            break;
                        case "1":
                            dt.Rows[i]["conflict"] = "Room(s) not available.";
                            break;
                        case "2":
                            dt.Rows[i]["conflict"] = "Outside office hours.";
                            break;
                        default:
                            dt.Rows[i]["conflict"] = "";
                            break;
                    }
                    dt.Rows[i]["formatDate"] = myVRMNet.NETFunctions.GetFormattedDate(dt.Rows[i]["startDate"].ToString());
                }
                dgConflict.Visible = true;
                tblConflict.Attributes.Add("style", "display:block");
                btnConfSubmit.Visible = false;
                dgConflict.DataSource = dv;
                dgConflict.DataBind();

                MetaBuilders.WebControls.ComboBox mb = new MetaBuilders.WebControls.ComboBox();
                MetaBuilders.WebControls.ComboBox mb1 = new MetaBuilders.WebControls.ComboBox();
                MetaBuilders.WebControls.ComboBox mb2 = new MetaBuilders.WebControls.ComboBox();
                Button btnConflict = new Button();

                foreach (DataGridItem dgi in dgConflict.Items)
                {
                    mb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictStartTime");
                    mb.Items.Clear();
                    obj.BindTimeToListBox(mb, true, true);
                    int sHour = Convert.ToInt16(dgi.Cells[7].Text);
                    int sMin = Convert.ToInt16(dgi.Cells[8].Text);
                    DateTime cStartDate = Convert.ToDateTime(dgi.Cells[11].Text + " " + sHour + ":" + sMin + " " + dgi.Cells[9].Text);

                    mb.Text = cStartDate.ToString(tformat);
                    sMin = Convert.ToInt16(dgi.Cells[10].Text);
                    DateTime cEndDate = cStartDate.AddMinutes(sMin); //new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, sHour, sMin, 0);
                    mb1 = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                    mb2 = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictSetupTime");

                    DateTime setupTime = cStartDate.AddMinutes(setupDuration);
                    DateTime teardownTime = cEndDate.AddMinutes(-teardownDuration);
                    mb2.Items.Clear();
                    obj.BindTimeToListBox(mb2, true, true);
                    mb2.Text = setupTime.ToString(tformat);

                    mb1.Items.Clear();

                    obj.BindTimeToListBox(mb1, true, true);
                    mb1.Text = cEndDate.ToString(tformat);
                    btnConflict = (Button)dgi.FindControl("btnViewConflict");
                    btnConflict.Text = obj.GetTranslatedText("View Conflict"); //FB 2272
                    btnConflict.UseSubmitBehavior = false;
                    btnConflict.CssClass = "altMedium0BlueButtonFormat";//FB 3030
                    btnConflict.Attributes.Add("Style", "Width:100pt");//FB 3030
                    btnConflict.OnClientClick = "javascript:return roomconflict('" + dgi.Cells[0].Text + "')"; //FB 2027 SetConference

                    if (dgi.Cells[1].Text == "No conflict")
                    {
                        btnConflict.Visible = false;
                    }
                    else
                    {
                        //FB 2027 SetConference ... start
                        if (!dgi.Cells[1].Text.Contains("Outside"))
                            btnConflict.Visible = true;
                        else
                            btnConflict.Visible = false;
                        //FB 2027 SetConference ... end
                    }
                }
                dgConflict.Focus();
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);//"Error reading individual conflict for this conference. Please contact your VRM Administrator.";
                errLabel.Visible = true;
            }
        }

        #endregion

        #region InitializeConflict
        protected void InitializeConflict(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    if (Session["timeFormat"] != null)
                    {
                        if (Session["timeFormat"].ToString() == "0")
                        {
                            RegularExpressionValidator valid = (RegularExpressionValidator)e.Item.FindControl("RegConflictTime");
                            if (valid != null)
                            {
                                valid.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                valid.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            }
                            RegularExpressionValidator validend = (RegularExpressionValidator)e.Item.FindControl("RegConflictEndTime");
                            if (validend != null)
                            {
                                validend.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                validend.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            }

                            if (EnableBufferZone == 1)
                            {
                                RegularExpressionValidator validsetup = (RegularExpressionValidator)e.Item.FindControl("RegConflictTeardownTime");
                                if (validsetup != null)
                                {
                                    validsetup.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    validsetup.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");

                                }
                                RegularExpressionValidator validtear = (RegularExpressionValidator)e.Item.FindControl("RegConflictSetupTime");
                                if (validtear != null)
                                {
                                    validtear.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    validtear.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");

                                }
                            }
                        }
                    }

                    TemplateColumn lblSetup = (TemplateColumn)dgConflict.Columns[3]; 
                    TemplateColumn lblTear = (TemplateColumn)dgConflict.Columns[4];
                    MetaBuilders.WebControls.ComboBox setup = (MetaBuilders.WebControls.ComboBox)e.Item.FindControl("conflictSetupTime");
                    MetaBuilders.WebControls.ComboBox tear = (MetaBuilders.WebControls.ComboBox)e.Item.FindControl("conflictTeardownTime");

                    if (EnableBufferZone == 0)
                    {
                        setup.Visible = false;
                        
                        lblSetup.Visible = false;
                        
                    }
                    tear.Visible = false;
                    lblTear.Visible = false;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region ChangeXML
        protected string ChangeXML(string inxml)
        {
            String recurringXPath = "";
            String customPatternXml = "";
            string timeZone = "";
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inxml);

                XmlNode recurNode = null;

                recurringXPath = "//conference/confInfo/recurrencePattern";

                customPatternXml += "<recurType>5</recurType>";
                customPatternXml += "<startDates></startDates>";

                recurNode = xmldoc.SelectSingleNode(recurringXPath);
                if (recurNode != null)
                {
                    recurNode.InnerXml = "";
                    recurNode.InnerXml = customPatternXml;

                    recurNode = xmldoc.SelectSingleNode("//conference/confInfo/recurrencePattern/startDates");
                }

                customPatternXml = "";
                XmlNode node;
                StringBuilder appointXml = new StringBuilder();
                node = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/timeZone");
                timeZone = node.InnerXml;

                node = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime");
                StringBuilder cusXML = new StringBuilder();
                cusXML.Append("<customInstance>1</customInstance>");
                cusXML.Append("<instances>");

                CheckBox del;
                MetaBuilders.WebControls.ComboBox tb;
                DateTime conflictSD, conflictED;
                int cntDelete = 0;
                foreach (DataGridItem dgi in dgConflict.Items)
                {
                    del = (CheckBox)dgi.FindControl("chkConflictDelete");
                    if (!del.Checked)
                    {
                        string confStDate = myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[0].Text);
                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictStartTime");
                        confStDate = confStDate + " " + tb.Text;

                        if (!DateTime.TryParse(confStDate, out conflictSD))
                            throw new Exception("Invalid conflict date");

                        string confEDDate = conflictSD.ToShortDateString();
                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                        confEDDate = confEDDate + " " + tb.Text;

                        if (!DateTime.TryParse(confEDDate, out conflictED))
                            throw new Exception("Invalid conflict date");

                        if (conflictED <= conflictSD)
                        {
                            conflictED = conflictSD.AddDays(1);
                            confEDDate = conflictED.ToShortDateString() + " " + tb.Text;

                            if (!DateTime.TryParse(confEDDate, out conflictED))
                                throw new Exception("Invalid conflict date");
                        }

                        DateTime conflictSetupDate, conflictTearDate;
                        string setupDate = "";
                        string teardownDate = "";

                        if (dgi.Cells[0].Text != "")
                            setupDate = myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[0].Text);

                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictSetupTime");
                        setupDate = setupDate + " " + tb.Text;

                        DateTime.TryParse(setupDate, out conflictSetupDate);

                        if (conflictSetupDate < conflictSD)
                        {
                            setupDate = conflictED.ToShortDateString() + " " + tb.Text;
                            DateTime.TryParse(setupDate, out conflictSetupDate);
                        }

                        teardownDate = conflictED.ToShortDateString();

                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                        teardownDate = teardownDate + " " + tb.Text;
                        DateTime.TryParse(teardownDate, out conflictTearDate);

                        if (conflictED < conflictTearDate)
                        {
                            teardownDate = conflictSetupDate.ToShortDateString() + " " + tb.Text;
                            DateTime.TryParse(teardownDate, out conflictTearDate);
                        }

                        TimeSpan conflictDur = conflictED.Subtract(conflictSD);
                        TimeSpan setupDuration = conflictSetupDate.Subtract(conflictSD);
                        TimeSpan tearDuration = conflictED.Subtract(conflictTearDate);
                        TimeSpan bufferDur = conflictTearDate.Subtract(conflictSetupDate);

                        if (conflictDur.TotalMinutes <= 0)
                            throw new Exception("Please check the start and end time for the conference.");

                        if (conflictDur.TotalMinutes > (24 * 60))
                            throw new Exception("Invalid Duration. Conference duration should be maximum of 24 hours.");

                        if (EnableBufferZone == 1)   
                        {
                            if (conflictSD > conflictSetupDate)
                                throw new Exception("Invalid Setup Time");

                            if (conflictTearDate > conflictED)
                                throw new Exception("Invalid Teardown Time");

                            if (conflictSetupDate >= conflictTearDate)
                                throw new Exception("Invalid Setup/Teardown Time");

                            if (bufferDur.TotalMinutes < 15)
                                throw new Exception("Invalid Duration. Conference duration should be minimum of 15 mins.");
                        }

                        if (cntDelete == 0)
                        {
                            appointXml.Append("<appointmentTime>");
                            appointXml.Append("<timeZone>" + timeZone + "</timeZone>");
                            appointXml.Append("<startHour>" + conflictSD.ToString("hh") + "</startHour>");
                            appointXml.Append("<startMin>" + conflictSD.Minute + "</startMin>");
                            appointXml.Append("<startSet>" + conflictSD.ToString("tt") + "</startSet>");
                            appointXml.Append("<durationMin>" + conflictDur.TotalMinutes + "</durationMin>");
                            appointXml.Append("<setupDuration>" + setupDuration.TotalMinutes + "</setupDuration>");  
                            appointXml.Append("<teardownDuration>" + tearDuration.TotalMinutes + "</teardownDuration>"); 
                            appointXml.Append("</appointmentTime>");

                            cusXML.Append(appointXml.ToString());
                        }
                        cntDelete++;

                        recurNode.InnerXml += "<startDate>" + conflictSD.ToString("MM/dd/yyyy") + "</startDate>";

                        if (customPatternXml == "")
                            customPatternXml = conflictSD.ToString("MM/dd/yyyy");
                        else
                            customPatternXml += ", " + conflictSD.ToString("MM/dd/yyyy");

                        cusXML.Append("<instance>");
                        cusXML.Append("<startDate>" + conflictSD.ToString("MM/dd/yyyy") + "</startDate>");
                        cusXML.Append("<startHour>" + conflictSD.ToString("hh") + "</startHour>");
                        cusXML.Append("<startMin>" + conflictSD.Minute + "</startMin>");
                        cusXML.Append("<startSet>" + conflictSD.ToString("tt") + "</startSet>");
                        cusXML.Append("<durationMin>" + conflictDur.TotalMinutes + "</durationMin>");
                        cusXML.Append("<setupDuration>" + setupDuration.TotalMinutes + "</setupDuration>"); 
                        cusXML.Append("<teardownDuration>" + tearDuration.TotalMinutes + "</teardownDuration>");  
                        cusXML.Append("</instance>");
                    }
                }
                cusXML.Append("</instances>");
                node.InnerXml = cusXML.ToString();

                recurNode = xmldoc.SelectSingleNode("//conference/confInfo/recurringText");
                if (recurNode != null)
                    recurNode.InnerXml = "Custom Date Selection: " + customPatternXml;


                if (cntDelete < 2)
                    throw new Exception (obj.GetErrorMessage(432));//FB 1881 //FB 2164
                //return "<error><message>A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.</message><level>E</level><errorCode></errorCode></error>";
                else
                    return xmldoc.InnerXml;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
				//errLabel.Text = ex.Message; //FB 2164
                return "<error><message>" + obj.ShowSystemMessage() + "</message></error>";
            }
        }
        #endregion

        #region RefreshRoom

        protected void RefreshRoom(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inxml = new StringBuilder();
                inxml.Append("<conferenceTime>");
                inxml.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inxml.Append("<confID>" + lblConfID.Text + "</confID>");
                inxml.Append(obj.OrgXMLElement());
                //FB 2023
                if(chkStartNow.Checked)
                    inxml.Append("<immediate>1</immediate>");
                else
                    inxml.Append("<immediate>0</immediate>");

                inxml.Append("<recurring>0</recurring>");
                DateTime dStart, dEnd;
				//FB 2634
                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //new DateTime(Convert.ToInt16(confStartDate.Text.Split('/')[2]), Convert.ToInt16(confStartDate.Text.Split('/')[0]), Convert.ToInt16(confStartDate.Text.Split('/')[1]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local); //FB 2588
                //dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text) + " " + TeardownTime.Text); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text)); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);

                TimeSpan ts = dEnd.Subtract(dStart);
                inxml.Append("<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + "</startDate>");
                inxml.Append("<startHour>" + dStart.Hour + "</startHour>");
                inxml.Append("<startMin>" + dStart.Minute + "</startMin>");
                inxml.Append("<startSet>" + dStart.ToString("tt") + "</startSet>");
                inxml.Append("<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>");
                Double durationMin = ts.TotalMinutes; // ts.Days * 24 * 60 + ts.Hours * 60 + ts.Minutes;
                inxml.Append("<durationMin>" + durationMin.ToString() + "</durationMin>");
                //inxml.Append("<availableOnly>0</availableOnly>");

                inxml.Append("</conferenceTime>");
                //string outxml = obj.CallCOM("GetAvailableRoom", inxml.ToString(), Application["COM_Configpath"].ToString());
                string outxml = obj.CallMyVRMServer("GetAvailableRoom", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetAvailableRoom outXML : " + outxml);
                if (outxml.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    XmlNodeList nodes = xmldoc.DocumentElement.SelectNodes("//locationList/level3List/level3");
                    lstRoomSelection.Items.Clear();
                    treeRoomSelection.Nodes.Clear();
                    TreeNode tn = new TreeNode("All");
                    tn.Expanded = true;
                    treeRoomSelection.Nodes.Add(tn);
                    GetLocationList(nodes);
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                }
                int countLeaf = 0;
                int countMid = 0;
                int countTop = 0;
                if (!Application["roomExpandLevel"].ToString().Equals("list"))
                {
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                    {
                        countMid = 0;
                        if (Application["roomExpandLevel"] != null)
                        {
                            if (Application["roomExpandLevel"].ToString() != "")
                            {
                                if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 2)
                                    tnTop.Expanded = true;
                            }
                        }
                        else
                            tnTop.Expanded = false;

                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                        {
                            if (Application["roomExpandLevel"] != null)
                            {
                                if (Application["roomExpandLevel"].ToString() != "")
                                {
                                    if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 3)
                                        tnMid.Expanded = true;
                                }
                            }
                            else
                                tnMid.Expanded = false;
                            countLeaf = 0;
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                                    if (tn.Depth.Equals(3) && tn.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                                    {
                                        tn.Checked = true;
                                        countLeaf++;
                                    }
                            }
                            if (countLeaf.Equals(tnMid.ChildNodes.Count))
                            {
                                tnMid.Checked = true;
                                countMid++;
                            }
                        }
                        if (countMid.Equals(tnTop.ChildNodes.Count))
                        {
                            tnTop.Checked = true;
                            countTop++;
                        }
                    }
                    if (countTop.Equals(treeRoomSelection.Nodes[0].ChildNodes.Count))
                        treeRoomSelection.Nodes[0].Checked = true;
                }
                else
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
                if (lstRoomSelection != null)
                {
                    if (lstRoomSelection.Items.Count == 0)
                    {
                        rdSelView.Enabled = false;
                        pnlListView.Visible = false;
                        pnlLevelView.Visible = false;
                        btnCompare.Enabled = false;
                        pnlNoData.Visible = true;
                    }
                    else if (lstRoomSelection.Items.Count > 0)
                    {
                        rdSelView.Enabled = true;
                        rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                        btnCompare.Enabled = true;
                        pnlNoData.Visible = false;

                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("RefreshRoom : " + ex.Message);
                //errLabel.Text = "Error in getting Available Room(s). Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        #endregion

        #region SyncRoomSelection

        protected void SyncRoomSelection()
        {
            try
            {
                if (rdSelView.SelectedIndex.Equals(1))
                {
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                tn.Checked = false;
                                foreach (ListItem lstItem in lstRoomSelection.Items)
                                    if ((lstItem.Selected == true) && (tn.Value.Equals(lstItem.Value)))
                                    {
                                        tn.Checked = true;
                                        selRooms += tn.Value + ",";
                                    }
                            }
                }
                else 
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                        foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                        {
                            if (tn.Value.Equals(lstItem.Value) && (tn.Depth.Equals(3)))
                            {
                                lstItem.Selected = true;
                                selRooms += lstItem.Value + ", ";
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region GetAdvancedAVSettings
        /// <summary>
        /// GetAdvancedAVSettings
        /// </summary>
        /// <returns></returns>
        private void GetAdvancedAVSettings()
        {
            string bridgeID = "-1";
            int usrId = 0;
            string epaddress = "";
            try
            {
                usrEndpoints = new Hashtable();
                String inXML = "<GetAdvancedAVSettings><UserID>" + Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<ConfID>" + lblConfID.Text + "</ConfID></GetAdvancedAVSettings>";
                String outXML = obj.CallMyVRMServer("GetAdvancedAVSettings", inXML, Application["MyVRMServer_Configpath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetAdvancedAVSettings/Endpoints/Endpoint");

                    //FB 2839 Start
                    Session.Remove("Confbridge");
                    Session.Add("Confbridge", outXML);
                    //FB 2839 End

                    XmlNodeList endpointNodes = xmldoc.SelectNodes("descendant::GetAdvancedAVSettings/Endpoints/Endpoint[Type='U']");
                    if (endpointNodes != null)
                    {
                        foreach (XmlNode nd in endpointNodes)
                        {
                            bridgeID = nd.SelectSingleNode("BridgeID").InnerText.Trim();
                            Int32.TryParse(nd.SelectSingleNode("ID").InnerText.Trim(), out usrId);
                            epaddress = nd.SelectSingleNode("Address").InnerText.Trim();
                            if (usrId > 0)
                            {
                                if (!usrEndpoints.Contains(usrId))
                                {
                                    usrEndpoints.Add(usrId, bridgeID);
                                }
                            }
                        }

                        if (epaddress != "")
                        {
                            String[] spiltAdd = SpiltAddress(epaddress);

                            if (spiltAdd != null)
                            {
                                if (spiltAdd.Length > 2)
                                {
                                    txtAudioDialNo.Text = spiltAdd[0];
                                    txtConfCode.Text = spiltAdd[1];
                                    txtLeaderPin.Text = spiltAdd[2];
                                }
                            }
                        }
                    }
                }
                else
                {
                    log.Trace(obj.ShowErrorMessage(outXML));
                }
                if (Session["ConfEndPts"] == null)
                {
                    Session.Add("ConfEndPts", usrEndpoints);
                }
                else
                {
                    Session.Remove("ConfEndPts");
                    Session.Add("ConfEndPts", usrEndpoints);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region MethodToSplitAddress
        /// <summary>
        /// MethodToSplitAddress
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        protected String[] SpiltAddress(String address)
        {
            String[] add = null;
            try
            {
                add = new String[3];
                if (address.Contains("D") && address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    if (address.IndexOf('+') > 0)
                    {
                        add[1] = address.Split('D')[1].Split('+')[0]; //Conference Code
                        add[2] = address.Split('D')[1].Split('+')[1]; // Leader Pin
                    }
                    else
                    {
                        add[1] = address.Split('D')[1]; //Conference Code
                        add[2] = ""; // Leader Pin
                    }
                }
                else if (address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    add[1] = address.Split('D')[1]; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                else if (!address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address;//Address
                    add[1] = ""; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                return add;

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                throw ex;
            }
        }

        #endregion

        #region Get File List

        private void GetFileList()
        {
            try
            {
                lblFileList.Text = "";

                if (lblUpload1.Text.Trim() != "")
                    lblFileList.Text = lblUpload1.Text.Trim() + "; ";

                if (lblUpload2.Text.Trim() != "")
                    lblFileList.Text += lblUpload2.Text.Trim() + "; ";

                if (lblUpload3.Text.Trim() != "")
                    lblFileList.Text += lblUpload3.Text.Trim() + "; ";

            }
            catch (Exception ex)
            {
                log.Trace("GetFileList : " + ex.Message);
            }
        }

        #endregion

        #region Validate Conferencing Duration
        public bool ValidateConferenceDur()
        {
            try
            {
                //FB 2634
                DateTime startDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //FB 2588
                DateTime endDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                TimeSpan confduration = endDate.Subtract(startDate);
                if (confduration.TotalMinutes < 15)
                {
                    errLabel.Text = obj.GetTranslatedText("Invalid Duration.Conference duration should be minimum of 15 mins.");//FB 1830 - Translation
                    errLabel.Visible = true;
                    return false;
                }


                return true;

            }
            catch (Exception ex)
            {
                log.Trace("ValidateConferenceDur : " + ex.Message);
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
                errLabel.Visible = true;
                return false;

            }
        }
        #endregion

        #region  Fetch Audio Participant details

        private void FetchAudioParticipants(string mode)
        {
            String inXML1 = "";
            String outXML1 = "";
            try
            {
                inXML1 = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";
                outXML1 = obj.CallMyVRMServer("GetAudioUserList", inXML1, Application["COM_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();

                if (outXML1.IndexOf("<error>") < 0)
                {
                    xmldoc.LoadXml(outXML1);
                    XmlNodeList nodes = xmldoc.SelectNodes("//users/user");

                    if (nodes != null)
                    {
                        lstAudioParty.Items.Add(new ListItem("None", "-1"));//FB 2341
                        if (lstAudioParty.Items.FindByValue("-1") != null)
                            lstAudioParty.Items.FindByValue("-1").Selected = true;

                    }
                    else
                    {
                        lstAudioParty.Items.Add(new ListItem("No Items...", "-1"));
                    }

                    string valueFld = "";
                    string textFld = "";
                    int adpartyid = -1;

                    XmlNode node = null;
                    for (int i = 0; i < nodes.Count; i++ )
                    {
                        if (i == 2)//FB 2341
                            break;
                            
                        node = nodes[i];
                        Int32.TryParse(node.SelectSingleNode("userID").InnerText.Trim(), out adpartyid);

                        valueFld = node.SelectSingleNode("userID").InnerText.Trim() +
                            "|" + node.SelectSingleNode("userEmail").InnerText.Trim() +
                            "|" + node.SelectSingleNode("audioDialIn").InnerText.Trim() +
                            "|" + node.SelectSingleNode("confCode").InnerText.Trim() +
                            "|" + node.SelectSingleNode("leaderPin").InnerText.Trim();

                        textFld = node.SelectSingleNode("firstName").InnerText.Trim() +
                            " " + node.SelectSingleNode("lastName").InnerText.Trim();

                        lstAudioParty.Items.Add(new ListItem(textFld, valueFld));
                        try
                        {
                            if (adpartyid == audioPartyId)//FB 2341
                                lstAudioParty.SelectedValue = valueFld;
                        }
                        catch (Exception e) { }

                    }
                    if (lstAudioParty.SelectedValue.Equals("-1"))//FB 2341
                    {
                        txtAudioDialNo.Enabled = false;
                        txtConfCode.Enabled = false;
                        txtLeaderPin.Enabled = false;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML1);
                    errLabel.Visible = true;
                }
            }
            catch (Exception e)
            {
                log.Trace("FetchAudioParticipants: " + e.Message);
            }
        }
        #endregion

        #region CheckTime 

        protected Boolean CheckTime()
        {
            String result = "false";
            String passwordResult = "";// FB 1865
            String tmzone = "26";
            String timezonedisplay = "";
            String confid = "";// FB 1865
            bool rtn = true;
            String skipCheck = "0";
            /** FB 2440 **/
            DateTime strtDateTime = DateTime.MinValue;
            DateTime sDateTime = DateTime.MinValue;
            DateTime tDateTime = DateTime.MinValue;
            DateTime dEnd = DateTime.MinValue;
            Int32 iSDur = -1, iTDur = 16, iMCUResult = 0;
            String sMcuResult = "-1";
            /** FB 2440 **/
            try
            {
				//FB 1911 - Start
                string date = "";
                date = ((Recur.Value == "") ? confStartDate.Text : StartDate.Text);
                
                if (RecurSpec.Value != "")
                    date = RecurSpec.Value.Split('#')[2].Split('&')[0];
				//FB 1911 - End

                timezonedisplay = timeZone;//((Request.QueryString["t"].ToString().Equals("o") && flagClone.Equals(false)) ? timeZone : "0");

                if (Recur.Value != "" || chkStartNow.Checked || RecurSpec.Value != "") //FB 2266 //if (Recur.Value != "" && RecurSpec.Value != "") //FB 2546
                    skipCheck = "1";


                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                confid = lblConfID.Text; //FB 1865

                if (Request.QueryString["t"] != null)//FB 1865
                {

                    if (Request.QueryString["t"].ToString().Equals("o"))
                        confid = "new";
                }

                /** FB 2440 **/
                if (EnableBufferZone == 1)
                {
                    //FB 2634
                    if (Recur.Value.Trim() == "")
                    {
                        Int32.TryParse(SetupDuration.Text, out iSDur);
                        Int32.TryParse(TearDownDuration.Text, out iTDur);
                    }
                    else
                    {
                        if (hdnSetupTime.Value == "" || hdnSetupTime.Value == null)
                            hdnSetupTime.Value = "0";

                        if (hdnTeardownTime.Value == "" || hdnTeardownTime.Value == null)
                            hdnTeardownTime.Value = "0";

                        Int32.TryParse(hdnSetupTime.Value, out iSDur);
                        Int32.TryParse(hdnSetupTime.Value, out iTDur);

                        //sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text);
                        //tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text) + " " + TeardownTime.Text);
                        //strtDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(date) + " " + confStartTime.Text);
                        //dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);
                        //iSDur = (int)sDateTime.Subtract(strtDateTime).TotalMinutes;
                    }
                }
                /** FB 2440 **/

                string resXML;
                string inputXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><hostID>" + hdnApprover4.Text + "</hostID><systemDate>" + Session["systemDate"].ToString() + "</systemDate><systemTime>" + Session["systemTime"].ToString() + "</systemTime><confDate>" + myVRMNet.NETFunctions.GetDefaultDate(date) + "</confDate><confTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text) + "</confTime><timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone><timezoneDisplay>" + timezonedisplay + "</timezoneDisplay><password>" + confPassword.Value + "</password><confID>" + confid + "</confID><skipCheck>" + skipCheck + "</skipCheck><mcuSetup>" + iSDur.ToString() + "</mcuSetup><mcuTeardonw>" + iTDur.ToString() + "</mcuTeardonw></login>";//FB 1865 2440

                resXML = obj.CallMyVRMServer("Isconferenceschedulable", inputXML, Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(resXML);


                if (timeZone == "0")
                {
                    if (xmlDOC.SelectSingleNode("/login/hosttimezone") != null)
                        tmzone = xmlDOC.SelectSingleNode("/login/hosttimezone").InnerText;

                    if (lstConferenceTZ.Items.FindByValue(tmzone) != null)// && (Request.QueryString["t"].ToString().Equals("o") && flagClone.Equals(false)))
                    {
                        lstConferenceTZ.ClearSelection();
                        lstConferenceTZ.Items.FindByValue(tmzone).Selected = true;
                    }

                }
                if (xmlDOC.SelectSingleNode("/login/result") != null)
                    result = xmlDOC.SelectSingleNode("/login/result").InnerText;

                if (xmlDOC.SelectSingleNode("/login/passwordCheck") != null)
                    passwordResult = xmlDOC.SelectSingleNode("/login/passwordCheck").InnerText;

                /** FB 2440 **/
                if (xmlDOC.SelectSingleNode("/login/mcuresult") != null)
                    sMcuResult = xmlDOC.SelectSingleNode("/login/mcuresult").InnerText;

                Int32.TryParse(sMcuResult.Trim(), out iMCUResult);
                /** FB 2440 **/  

                if (result.ToUpper().Equals("FALSE"))
                {

                    String tmezne = "";

                    if (timeZone == "0")
                        tmezne = " host timezone.(" + lstConferenceTZ.SelectedItem.Text + ")";
                    else
                        tmezne = " selected conference timezone.";



                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("Invalid Start Date or Time. It should be greater than current time in") + tmezne;//FB 1830 - Translation

                    /** FB 2440 **/
                    if (iMCUResult == 1)
                        errLabel.Text = obj.GetTranslatedText("Setup time cannot be less than the MCU pre start time.");
                    else if (iMCUResult == 2)
                        errLabel.Text = obj.GetTranslatedText("Teardown time cannot be less than the MCU pre end time.");
                    /** FB 2440 **/

                    rtn = false;


                }
                // FB 1865
                else if (passwordResult == "0")
                {
                    

                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("Please check the password. It must be unique");//FB 1830 - Translation

                    rtn = false;
                }
                // FB 1865
                else
                {
                    errLabel.Text = "";
                }

            
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
                
            }

            return rtn;
        }
        #endregion

        //Method added for FB 1830 Email Edit - start
        #region GetXConfParams
        /// <summary>
        /// GetXConfParams
        /// </summary>
        /// <returns></returns>
        private void GetXConfParams()
        {
            xPartys = new List<int>();
            try
            {
                string xconfpartys = "";
                if (Session["XCONFINFO"] != null)
                {
                    xConfInfo = (StringDictionary)Session["XCONFINFO"];
                    if (xConfInfo != null)
                    {
                        if (xConfInfo["partys"] != null)
                            xconfpartys = xConfInfo["partys"];

                        if (xConfInfo["password"] != null)
                            xconfpassword = xConfInfo["password"];

                        if (xConfInfo["setupdate"] != null)
                            DateTime.TryParse(xConfInfo["setupdate"], out xconfsetup);

                        if (xConfInfo["teardate"] != null)
                            DateTime.TryParse(xConfInfo["teardate"], out xconftear);

                        if (xconfpartys != "")
                        {
                            string[] xcfpartys = xconfpartys.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries); //FB 1888 //Partys before edit
                            int xpartid = 0;
                            for (int p = 0; p < xcfpartys.Length; p++)
                            {
                                int.TryParse(xcfpartys[p], out xpartid);
                                if (xpartid > 0)
                                {
                                    if (!xPartys.Contains(xpartid))
                                        xPartys.Add(xpartid);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Trace(e.Message);
            }
        }
        #endregion

        #region CheckForUserInput
        /// <summary>
        /// CheckForUserInput
        /// </summary>
        /// <returns></returns>
        private bool CheckForUserInput()
        {
            bool isNewPartysAdded = false;
            try
            {
                if (txtPartysInfo.Text.Trim() == "")
                    return false; //No participants

                GetXConfParams();
                //FB 2634
                DateTime sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //FB 2588
                DateTime tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                //DateTime sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text);
                //DateTime tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text) + " " + TeardownTime.Text);

                if (confPassword.Value != xconfpassword || xconfsetup != sDateTime || xconftear != tDateTime)
                    return false;  //send email to all partys without checking


                string[] partysary = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries); //FB 1888

                int partynum = partysary.Length;

                if ((partynum) == 0) //FB 1888
                    return false; //No participants

                //int usrid = 0;
                //for (int i = 0; i < partynum - 1; i++)
                //{
                //    string[] partyary = partysary[i].Split(',');
                //    usrid = 0;
                //    int.TryParse(partyary[0].Trim(), out usrid);
                //    if (usrid < 0)
                //        usrid = 0;

                //    if (!xPartys.Contains(usrid))
                //    {
                //        isNewPartysAdded = true;
                //        break;
                //    }
                //}
                //if (isNewPartysAdded)
                //{
                //    emailAlertMes = "New participant(s) are added into conference. Do you want to notify all participant(s)?";
                //    return true; //Get user input for sending emails. if he says NO to all partys
                //    //then send mail only to newly added party
                //}
                //if ((partynum - 1) != xPartys.Count && isNewPartysAdded == false) //party removed - get user input for sending mails
                //{
                //    emailAlertMes = "Participant(s) has been removed from the conference. Do you want to notify all participant(s)?";
                //    return true;
                //}
                //emailAlertMes = "Do you want to notify all participant(s)?";
                return true;
            }
            catch (Exception e)
            {
                log.Trace(e.Message);
                return false;
            }
        }
        #endregion
        //Method added for FB 1830 Email Edit - end

        //FB 2426 - Start

        #region Video Guest Location Submit
        /// <summary>
        /// Video Guest Location Submit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnGuestLocationSubmit(object sender, EventArgs e)
        {
            DataRow dr = null;
            onflyGrid = (DataTable)Session["OnlyFlyRoom"];
            if (onflyGrid == null || onflyGrid.Rows.Count.Equals(0))
            {
                CreateDtColumnNames();
                onflyGrid = obj.LoadDataTable(null, colNames);

                if (!onflyGrid.Columns.Contains("RowUID"))
                    onflyGrid.Columns.Add("RowUID");
            }

            for (int i = 0; i < onflyGrid.Rows.Count; i++)
            {
                if (onflyGrid.Rows[i]["RowUID"].ToString().Trim() != hdnGuestloc.Value.Trim()
                    && onflyGrid.Rows[i]["RoomName"].ToString().Trim() == txtsiteName.Text.Trim())
                {
                    // FB 2525 Starts
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "errMessage", "<script>alert('" + obj.GetTranslatedText("Room Name has already been used") + "');</script>", false);
                    //errLabel.Text = obj.GetTranslatedText("Room Name has already been used");
                    //errLabel.Visible = true; FB 2525 Ends
                    return;
                }
            }

            dr = onflyGrid.NewRow();
            dr["RowUID"] = onflyGrid.Rows.Count;
            dr["RoomID"] = hdnGuestRoomID.Value;
            dr["RoomName"] = txtsiteName.Text.Trim();
            dr["ContactName"] = txtApprover5.Text.Trim();
            dr["ContactEmail"] = txtEmailId.Text.Trim();
            dr["ContactPhoneNo"] = txtPhone.Text.Trim();
            dr["ContactAddress"] = txtAddress.Text.Trim();
            dr["State"] = txtState.Text.Trim();
            dr["City"] = txtCity.Text.Trim();
            dr["ZIP"] = txtZipcode.Text.Trim();
            dr["Country"] = lstCountries.SelectedValue.ToString();
            dr["IPAddressType"] = "1";
            dr["IPAddress"] = txtIPAddress.Text.Trim();
            dr["IPPassword"] = txtIPPassword.Text.Trim();
            dr["IPconfirmPassword"] = txtIPconfirmPassword.Text.Trim();
            dr["IPMaxLineRate"] = lstIPlinerate.SelectedValue.ToString();
            dr["IPConnectionType"] = lstIPConnectionType.SelectedValue.ToString();
            if (radioIsDefault.Checked)
            {
                dr["IsIPDefault"] = "1";
                dr["DefaultAddressType"] = obj.GetTranslatedText("IP Address");
                dr["DefaultAddress"] = txtIPAddress.Text.Trim();
                dr["DefaultConnetionType"] = lstIPConnectionType.SelectedItem.Text.Trim();
            }
            else
                dr["IsIPDefault"] = "0";

            dr["SIPAddressType"] = "6";
            dr["SIPAddress"] = txtSIPAddress.Text.Trim();
            dr["SIPPassword"] = txtSIPPassword.Text.Trim();
            dr["SIPconfirmPassword"] = txtSIPconfirmPassword.Text.Trim();
            dr["SIPMaxLineRate"] = lstSIPlinerate.SelectedValue.ToString();
            dr["SIPConnectionType"] = lstSIPConnectionType.SelectedValue.ToString();
            if (radioIsDefault2.Checked)
            {
                dr["IsSIPDefault"] = "1";
                dr["DefaultAddressType"] = obj.GetTranslatedText("E164/SIP Address");
                dr["DefaultAddress"] = txtSIPAddress.Text.Trim();
                dr["DefaultConnetionType"] = lstSIPConnectionType.SelectedItem.Text.Trim();
            }
            else
                dr["IsSIPDefault"] = "0";
            dr["ISDNAddressType"] = "4";
            dr["ISDNAddress"] = txtISDNAddress.Text.Trim();
            dr["ISDNPassword"] = txtISDNPassword.Text.Trim();
            dr["ISDNconfirmPassword"] = txtISDNconfirmPassword.Text.Trim();
            dr["ISDNMaxLineRate"] = lstISDNlinerate.SelectedValue.ToString();
            dr["ISDNConnectionType"] = lstISDNConnectionType.SelectedValue.ToString();
            if (radioIsDefault3.Checked)
            {
                dr["IsISDNDefault"] = "1";
                dr["DefaultAddressType"] = obj.GetTranslatedText("ISDN Address");
                dr["DefaultAddress"] = txtISDNAddress.Text.Trim();
                dr["DefaultConnetionType"] = lstISDNConnectionType.SelectedItem.Text.Trim();
            }
            else
                dr["IsISDNDefault"] = "0";

            if (hdnGuestRoom.Value != "")
            {
                if (Convert.ToInt32(hdnGuestRoom.Value) == -1)
                {
                    onflyGrid.Rows.Add(dr);
                }
                else
                {
                    onflyGrid.Rows.RemoveAt(Convert.ToInt32(hdnGuestRoom.Value));
                    onflyGrid.Rows.InsertAt(dr, Convert.ToInt32(hdnGuestRoom.Value));
                }
            }
            BindOptionData();
            dgOnflyGuestRoomlist.Visible = true;
        }
        #endregion video guest location submit

        #region video guest location Cancel
        /// <summary>
        /// video guest location Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnGuestLocationCancel(object sender, EventArgs e)
        {
            try
            {
                guestLocationPopup.Hide();
            }
            catch (Exception ex)
            {
                log.Trace("fnGuestLocationCancel" + ex.Message);
            }
        }
        #endregion video guest location Cancel

        #region Bind Option Data
        /// <summary>
        /// Bind Option Data
        /// </summary>
        private void BindOptionData()
        {
            try
            {
                if (onflyGrid == null)
                    onflyGrid = new DataTable();

                if (Session["OnlyFlyRoom"] == null)
                    Session.Add("OnlyFlyRoom", onflyGrid);
                else
                    Session["OnlyFlyRoom"] = onflyGrid;

                dgOnflyGuestRoomlist.DataSource = onflyGrid;
                dgOnflyGuestRoomlist.DataBind();

                if (onflyGrid.Rows.Count.Equals(0))
                    dgOnflyGuestRoomlist.Visible = false;
                else
                    dgOnflyGuestRoomlist.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region dgOnflyGuestRoomlist_Edit
        /// <summary>
        /// dgOnflyGuestRoomlist_Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgOnflyGuestRoomlist_Edit(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                int n = e.Item.ItemIndex;
                hdnGuestRoom.Value = n.ToString();

                if (Session["OnlyFlyRoom"] != null)
                    onflyGrid = (DataTable)Session["OnlyFlyRoom"];

                DataRow dr = onflyGrid.Rows[n];
                hdnGuestloc.Value = dr["RowUID"].ToString();
                hdnGuestRoomID.Value = dr["RoomID"].ToString();
                txtsiteName.Text = dr["RoomName"].ToString();
                txtApprover5.Text = dr["ContactName"].ToString();
                txtEmailId.Text = dr["ContactEmail"].ToString();
                txtPhone.Text = dr["ContactPhoneNo"].ToString();
                txtAddress.Text = dr["ContactAddress"].ToString();
                if (dr["State"].ToString().Trim() == "55")
                    dr["State"] = "AB";
                else if (dr["State"].ToString().Trim() == "68")
                    dr["State"] = "AGS";
                else
                    dr["State"] = "NY";
                txtState.Text = dr["State"].ToString();
                txtCity.Text = dr["City"].ToString();
                txtZipcode.Text = dr["ZIP"].ToString();
                lstCountries.ClearSelection();
                lstCountries.SelectedValue = dr["Country"].ToString();
                txtIPAddress.Text = dr["IPAddress"].ToString();
                txtIPPassword.Text = dr["IPPassword"].ToString();
                txtIPconfirmPassword.Text = dr["IPconfirmPassword"].ToString();
                lstIPlinerate.ClearSelection();
                if (dr["IPMaxLineRate"].ToString().Trim() == "")
                    dr["IPMaxLineRate"] = -1;
                lstIPlinerate.SelectedValue = dr["IPMaxLineRate"].ToString();
                lstIPConnectionType.ClearSelection();
                if (dr["IPConnectionType"].ToString().Trim() == "")
                    dr["IPConnectionType"] = -1;
                lstIPConnectionType.SelectedValue = dr["IPConnectionType"].ToString();
                if (dr["IsIPDefault"].ToString() == "1")
                    radioIsDefault.Checked = true;
                else
                    radioIsDefault.Checked = false;
                txtISDNAddress.Text = dr["ISDNAddress"].ToString();
                txtISDNPassword.Text = dr["ISDNPassword"].ToString();
                txtISDNconfirmPassword.Text = dr["ISDNconfirmPassword"].ToString();
                lstISDNlinerate.ClearSelection();
                if (dr["ISDNMaxLineRate"].ToString().Trim() == "")
                    dr["ISDNMaxLineRate"] = -1;
                lstISDNlinerate.SelectedValue = dr["ISDNMaxLineRate"].ToString();
                lstISDNConnectionType.ClearSelection();
                if (dr["ISDNConnectionType"].ToString().Trim() == "")
                    dr["ISDNConnectionType"] = -1;
                lstISDNConnectionType.SelectedValue = dr["ISDNConnectionType"].ToString();
                if (dr["IsISDNDefault"].ToString() == "1")
                    radioIsDefault3.Checked = true;
                else
                    radioIsDefault3.Checked = false;
                txtSIPAddress.Text = dr["SIPAddress"].ToString();
                txtSIPPassword.Text = dr["SIPPassword"].ToString();
                txtSIPconfirmPassword.Text = dr["SIPconfirmPassword"].ToString();
                lstSIPlinerate.ClearSelection();
                if (dr["SIPMaxLineRate"].ToString().Trim() == "")
                    dr["SIPMaxLineRate"] = -1;
                lstSIPlinerate.SelectedValue = dr["SIPMaxLineRate"].ToString();
                lstSIPConnectionType.ClearSelection();
                if (dr["SIPConnectionType"].ToString().Trim() == "")
                    dr["SIPConnectionType"] = -1;
                lstSIPConnectionType.SelectedValue = dr["SIPConnectionType"].ToString();
                if (dr["IsSIPDefault"].ToString() == "1")
                    radioIsDefault2.Checked = true;
                else
                    radioIsDefault2.Checked = false;
                btnGuestLocationSubmit.Text = obj.GetTranslatedText("Update");
                guestLocationPopup.Show();
            }
            catch (Exception ex)
            {
                log.Trace("dgOnflyGuestRoomlist_Edit" + ex.Message);
            }
        }

        #endregion

        #region dgOnflyGuestRoomlist_DeleteCommand
        /// <summary>
        /// dgOnflyGuestRoomlist_DeleteCommand 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgOnflyGuestRoomlist_DeleteCommand(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                onflyGrid = (DataTable)Session["OnlyFlyRoom"];
                int i = e.Item.ItemIndex;
                onflyGrid.Rows[i].Delete();
                BindOptionData();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("dgOnflyGuestRoomlist_DeleteCommand" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region Create Column Names
        /// <summary>
        /// Create Column Names
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("RoomID");
            colNames.Add("RoomName");
            colNames.Add("ContactName");
            colNames.Add("ContactEmail");
            colNames.Add("ContactPhoneNo");
            colNames.Add("ContactAddress");
            colNames.Add("State");
            colNames.Add("City");
            colNames.Add("ZIP");
            colNames.Add("Country");
            colNames.Add("IPAddressType");
            colNames.Add("IPAddress");
            colNames.Add("IPPassword");
            colNames.Add("IPconfirmPassword");
            colNames.Add("IPMaxLineRate");
            colNames.Add("IPConnectionType");
            colNames.Add("IsIPDefault");
            colNames.Add("SIPAddressType");
            colNames.Add("SIPAddress");
            colNames.Add("SIPPassword");
            colNames.Add("SIPconfirmPassword");
            colNames.Add("SIPMaxLineRate");
            colNames.Add("SIPConnectionType");
            colNames.Add("IsSIPDefault");
            colNames.Add("ISDNAddressType");
            colNames.Add("ISDNAddress");
            colNames.Add("ISDNPassword");
            colNames.Add("ISDNconfirmPassword");
            colNames.Add("ISDNMaxLineRate");
            colNames.Add("ISDNConnectionType");
            colNames.Add("IsISDNDefault");
            colNames.Add("DefaultAddressType");
            colNames.Add("DefaultAddress");
            colNames.Add("DefaultConnetionType");

        }
        #endregion

        //FB 2426 - End
		
		//FB 2659 - Starts
        #region CheckSeatsAvailability
        /// <summary>
        /// CheckSeatsAvailability
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckSeatsAvailability(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                DateTime dStart, dEnd;
                int setupDuration = 0, tearDuration = 0;

                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + confStartTime.Text);
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);

                if (EnableBufferZone == 1)
                {
                    int.TryParse(SetupDuration.Text, out setupDuration);
                    int.TryParse(TearDownDuration.Text, out tearDuration);
                    dStart = dStart.AddMinutes(-setupDuration);
                    dEnd = dEnd.AddMinutes(tearDuration);
                }

                inXML.Append("<TDGetSlotAvailableSeats>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<editConfId>" + lblConfID.Text + "</editConfId>");
                inXML.Append("<startDate>" + dStart + "</startDate>");
                inXML.Append("<endDate>" + dEnd + "</endDate>");
                inXML.Append("<timezone>" + lstConferenceTZ.SelectedValue + "</timezone>");
                inXML.Append("</TDGetSlotAvailableSeats>");

                string outXML = obj.CallMyVRMServer("TDGetSlotAvailableSeats", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(outXML);

                int totalOrgSeats = 0;
                if (xd.SelectSingleNode("//TDGetSlotAvailableSeats/TotalOrgSeats") != null)
                    int.TryParse(xd.SelectSingleNode("//TDGetSlotAvailableSeats/TotalOrgSeats").InnerText.Trim(), out  totalOrgSeats);

                if (totalOrgSeats <= 0)  //FB 3062
                {
                    errLabel.Text = obj.GetTranslatedText("Insufficient Seats. Please Contact Your VRM Administrator.");
                    errLabel.Visible = true;
                }
                else
                {

                    XmlNodeList nodes = xd.SelectNodes("//TDGetSlotAvailableSeats/availableSeatsList/availableSeats");

                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    if (nodes != null)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            xtr = new XmlTextReader(nodes[i].OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                    }
                    DataTable dt = new DataTable();

                    tblSeatsAvailability.BorderColor = "Black";
                    HtmlTableRow trRow;
                    HtmlTableCell tdCell;
                    string tdConfDateId = "";
                    DateTime startDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                        for (int i = 0; i < 2; i++)
                        {
                            trRow = new HtmlTableRow();
                            if (i == 0)
                            {
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    tdCell = new HtmlTableCell();
                                    tdCell.NoWrap = false;
                                    tdCell.Align = "left";
                                    tdCell.ID = "tdConfDate" + j;

                                    startDate = Convert.ToDateTime(dt.Rows[j]["startDate"].ToString());
                                    endDate = Convert.ToDateTime(dt.Rows[j]["endDate"].ToString());

                                    tdCell.InnerHtml = "Start Date/Time: " + startDate.ToString(format) + " " + startDate.ToString(tformat);
                                    tdCell.InnerHtml += "<br>End Date/Time: " + endDate.ToString(format) + " " + endDate.ToString(tformat);
                                    tdCell.InnerHtml += "<br>Available Seats: " + dt.Rows[j]["seatsNumber"].ToString();
                                    trRow.Height = "100px";
                                    trRow.Cells.Add(tdCell);
                                }
                            }
                            else
                            {
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    tdCell = new HtmlTableCell();
                                    tdConfDateId = "tdConfDate" + j;
                                    tdCell.Style.Add("cursor", "pointer");

                                    if (dt.Rows[j]["seatsNumber"].ToString() == totalOrgSeats.ToString())
                                        tdCell.BgColor = "#65FF65";//Green-Totally Free.
                                    else
                                        tdCell.BgColor = "#F8F075";//Yellow-Partially Free.

                                    tdCell.Attributes.Add("onclick", "javascript:SelectOneDefault('" + tdConfDateId + "');");
                                    trRow.Height = "100px";
                                    trRow.Cells.Add(tdCell);
                                }
                            }
                            tblSeatsAvailability.Rows.Add(trRow);
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "seats", "fnShowSeats();", true);
                }
            }
            catch (Exception ex)
            {
                log.Trace("CheckSeatsAvailability: " + ex.Message);
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        #endregion
        //FB 2659 - End


        //FB 2693 Start
        #region FetchPCDetails
        /// <summary>
        /// fnFetchPCDetails
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="PcType"></param>
        /// <returns></returns>
        [WebMethod]
        public static string fnFetchPCDetails(string userID, string pctype)
        {
            myVRMNet.NETFunctions obj;
            obj = new myVRMNet.NETFunctions();
            ns_Logger.Logger log;
            log = new ns_Logger.Logger();
            string res = "";
            StringBuilder inXML = new StringBuilder();
            String outXML = "";
            int PCId = 0;
            string htmlcontent = "";
            string Description = "", SkypeURL = "", VCDialinIP = "", VCMeetingID = "", VCDialinSIP = "", VCDialinH323 = "", VCPin = "", PCDialinNum = "";
            string PCDialinFreeNum = "", PCMeetingID = "", PCPin = "", Intructions = "";
            try
            {
                inXML.Append("<FetchPCDetails>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + userID + "</userID>");
                inXML.Append("<PCType>" + pctype + "</PCType>");
                inXML.Append("</FetchPCDetails>");
                outXML = obj.CallMyVRMServer("FetchSelectedPCDetails", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_Configpath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                {
                    htmlcontent = "";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//PCDetails/PCDetail");
                    if (nodes.Count > 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].SelectSingleNode("PCId") != null)
                                int.TryParse(nodes[i].SelectSingleNode("PCId").InnerText.Trim(), out PCId);
                            if (nodes[i].SelectSingleNode("Description") != null)
                                Description = nodes[i].SelectSingleNode("Description").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("SkypeURL") != null)
                                SkypeURL = nodes[i].SelectSingleNode("SkypeURL").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinIP") != null)
                                VCDialinIP = nodes[i].SelectSingleNode("VCDialinIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCMeetingID") != null)
                                VCMeetingID = nodes[i].SelectSingleNode("VCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinNum") != null)
                                PCDialinNum = nodes[i].SelectSingleNode("PCDialinNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinFreeNum") != null)
                                PCDialinFreeNum = nodes[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCMeetingID") != null)
                                PCMeetingID = nodes[i].SelectSingleNode("PCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("Intructions") != null)
                                Intructions = nodes[i].SelectSingleNode("Intructions").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinSIP") != null)
                                VCDialinSIP = nodes[i].SelectSingleNode("VCDialinSIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinH323") != null)
                                VCDialinH323 = nodes[i].SelectSingleNode("VCDialinH323").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCPin") != null)
                                VCPin = nodes[i].SelectSingleNode("VCPin").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCPin") != null)
                                PCPin = nodes[i].SelectSingleNode("PCPin").InnerText.Trim();
                            switch (PCId)
                            {
                                case ns_MyVRMNet.vrmPC.BlueJeans:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in IP: " + VCDialinIP + "<br />Meeting ID: " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />Meeting ID: " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />First time joining a Blue Jeans Video Meeting?<br />For detailed instructions: " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Blue Jeans Network 2012</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Jabber:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in IP: " + VCDialinIP + "<br />Meeting ID: " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />Meeting ID: " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />First time joining a Jabber Video Meeting?<br />For detailed instructions: " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Jabber Network 2012</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Lync:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in IP: " + VCDialinIP + "<br />Meeting ID: " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />Meeting ID: " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />First time joining a Lync Video Meeting?<br />For detailed instructions: " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Lync Network 2012</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Vidtel:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += "To join or start the meeting,go to: <br />" + Description + "<br />";
                                    htmlcontent += "<br /> Or join directly with the following options:<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Video Conferencing Systems:</td></tr>";
                                    htmlcontent += "<tr><td>Dial-in SIP: " + VCDialinSIP + "<br />Dial-in H.323: " + VCDialinH323 + "<br />PIN: " + VCPin + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Phone Conferencing:<br />Dial in toll number: " + PCDialinNum + "<br />Dial in toll free number: " + PCDialinFreeNum + "<br />PIN: " + PCPin + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />(c) Vidtel Network 2012</td></tr></table>";
                                    break;
                                default:

                                    break;

                            }
                        }
                    }
                    else
                    {
                        htmlcontent = "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='3' height='20px'></td></tr>";
                        htmlcontent += "<tr><td colspan='3' align='center'><b style='font-family:Verdana;' >" + obj.GetTranslatedText("No Information") + "</b><br><br><br><br></td></tr>";
                    }
                    res = htmlcontent.ToString();
                    //res = "text";
                }
            }
            catch (Exception ex)
            {
                log.Trace("PacketDetails" + ex.Message);
            }
            return res;
        }
        #endregion

        private void CheckPCVendor()
        {
            try
            {
                if (Session["EnableBlueJeans"] != null)
                {
                    if (Session["EnableBlueJeans"].ToString().Equals("1"))
                        PCVendorType = 1;
                }
                if (Session["EnableJabber"] != null)
                {
                    if (Session["EnableJabber"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 2;
                    }
                }
                if (Session["EnableLync"] != null)
                {
                    if (Session["EnableLync"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 3;
                    }
                }
                if (Session["EnableVidtel"] != null)
                {
                    if (Session["EnableVidtel"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 4;
                    }
                }
                switch (PCVendorType)
                {
                    case ns_MyVRMNet.vrmPC.BlueJeans:
                        rdBJ.Checked = true;
                        break;
                    case ns_MyVRMNet.vrmPC.Jabber:
                        rdJB.Checked = true;
                        break;
                    case ns_MyVRMNet.vrmPC.Lync:
                        rdLync.Checked = true;
                        break;
                    case ns_MyVRMNet.vrmPC.Vidtel:
                        rdVidtel.Checked = true;
                        break;
                    default:
                        rdBJ.Checked = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Trace("PacketDetails" + ex.Message);
            }
        }
        //FB 2693 End
        
    }
}
