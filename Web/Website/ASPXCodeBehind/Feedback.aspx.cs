/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_Feedback : System.Web.UI.Page
{
    #region Protected Data Members

    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.WebControls.TextBox TxtName;
    protected System.Web.UI.WebControls.TextBox TxtEmail;
    protected System.Web.UI.WebControls.TextBox TxtSubject;
    protected System.Web.UI.WebControls.TextBox TxtComment;
    protected System.Web.UI.WebControls.Button BtnSubmit;
    protected System.Web.UI.HtmlControls.HtmlInputHidden parentpage;

    #endregion

    #region  Variables Declaration

    private myVRMNet.NETFunctions obj = null;
    private ns_Logger.Logger log;
    String errorMessage = "";
    #endregion

    #region Page Load
    /// <summary>
    /// On Page Load,If Session of UserID is null,then assign values to variables COM_ConfigPath and MyVRMServer_ConfigPath
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("Feedback.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            obj = new myVRMNet.NETFunctions();
            

            if (Session["userID"] == null)
            {
                Session.Add("userID", "11");
                Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
                Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
            }
            
            InitializeUIResources();
            BindData();

            
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            //LblError.Text = "PageLoad: " + ex.Message;
            WriteIntoLog(ex.StackTrace);
        }

    }
    #endregion

    #region InitializeUIResources

    private void InitializeUIResources()
    {
        this.BtnSubmit.Attributes.Add("onclick", "javascript:return fnValidate();");
        this.TxtSubject.Attributes.Add("onkeyup", "javascript:chkLimit(this,'15');");
        this.TxtComment.Attributes.Add("onkeyup", "javascript:chkLimit(this,'3');");
        this.TxtEmail.Attributes.Add("onkeyup", "javascript:chkLimit(this,'e');");

    }

    #endregion

    #region BindData
    /// <summary>
    /// Construct inXML. Get the outXML from Com (Command : GetTemplate) and bind it to the controls
    /// Application["COM_ConfigPath"] refers ComConfig.xml file path
    /// </summary>

    private void BindData()
    {
        XmlNode node = null;

        try
        {
            String inXML = BuildInXML();

            //String outXML = obj.CallCOM("GetFeedback", inXML, Application["COM_ConfigPath"].ToString());
            String outXML = obj.CallMyVRMServer("GetFeedback", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027

           
            if (outXML != "")
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(outXML);

                if (xmlDoc == null)
                {
                    if (Session["who_use"].ToString() == "VRM")
                    {
                        //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";
                        //errorMessage = errorMessage + xmlDoc;
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + xmlDoc);
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                    }
                    else
                    {
                        //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                        //Response.Write("<br><br><p align='left'><font size=2><b>" + xmlDoc + "<b></font></p>");
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + xmlDoc);
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                    }
                    xmlDoc = null;
                    Response.End();
                }

                this.TxtName.Text = Session["userName"].ToString();
                this.TxtEmail.Text = xmlDoc.DocumentElement.SelectSingleNode("//userInfo/userEmail").InnerText;
                
            }
            else
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowErrorMessage(outXML);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        
    }

    #endregion

    #region BuildInXML
    /// <summary>
    /// To construct the inputXML for fetching the data
    /// </summary>
    /// <returns></returns>

    private String BuildInXML()
    {
        String inXML = "";
        inXML += "<login>";
        inXML += obj.OrgXMLElement();//Organization Module Fixes
        inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
        inXML += "</login>";
        return inXML;
    }

    #endregion

    #region Submit Click Event
    /// <summary>
    /// Construct Input XML and Pass it to COM.If Success, then display message and redirect the page again.
    /// If Error, then display the error.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SubmitFeedback(Object sender, EventArgs e)
    {
        try
        {
            
            String inXML = "";
            inXML += "<login>";
            inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += " <name>" + TxtName.Text + "</name>";
            inXML += " <email>" + TxtEmail.Text + "</email>";
            inXML += " <subject>" + TxtSubject.Text + "</subject>";
            inXML += " <comment>" + TxtComment.Text + "</comment>";
            //FB 1664 Start
            //inXML += " <parentPage>" + parentpage.Value.ToString() + "</parentPage>";
            inXML += " <parentPage>" + parentpage.Value.ToString().Replace("&", "&amp;") + "</parentPage>";//FB 1664
            //FB 1664 End

            inXML += " <browserAgent>" + Request.ServerVariables["HTTP_USER_AGENT"].ToString() + "</browserAgent>";
            inXML += " <IPAddress>" + Request.ServerVariables["REMOTE_ADDR"].ToString() + "</IPAddress>";
            inXML += " <browserLanguage>" + Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"].ToString() + "</browserLanguage>";
            inXML += " <hostAddress>" + Request.ServerVariables["REMOTE_HOST"].ToString() + "</hostAddress>";

            inXML += "</login>";

            String outXML = obj.CallMyVRMServer("Feedback", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027

            if (outXML.IndexOf("<error>") >= 0)
            {
                LblError.Text = obj.ShowErrorMessage(outXML);
                LblError.Visible = true;               
            }
            else
            {
                Response.Write("<script>alert('" + obj.GetTranslatedText("Thank you for the feedback.") + "');</script>");//FB 2272

                Response.Write("<script language=javascript> window.close();</script>");
            }
           
            obj = null;

        }
        catch (System.Threading.ThreadAbortException) { }
        catch (Exception ex)
        {
            //ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            //LblError.Text = ex.Message;
            LblError.Visible = true;
            WriteIntoLog(ex.StackTrace);
        }
    }

    #endregion

    #region WriteIntoLog

    private void WriteIntoLog(String stackTrace)
    {
        log = new ns_Logger.Logger();
        log.Trace(stackTrace);
        log = null;
    }

    #endregion
}
