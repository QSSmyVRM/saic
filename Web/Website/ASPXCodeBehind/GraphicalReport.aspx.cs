/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;
using ExpertPdf.HtmlToPdf;//Graphical Reports

public partial class en_GraphicalReport : System.Web.UI.Page
{
    # region Protected members  
    
    protected System.Web.UI.HtmlControls.HtmlTableRow trGraph1M;   
    protected System.Web.UI.WebControls.Button btnviewRep;   
    protected System.Web.UI.WebControls.DropDownList DrpLocations;
    protected System.Web.UI.WebControls.DropDownList DrpAllType;
    protected System.Web.UI.HtmlControls.HtmlTableCell tdLoc;
    protected System.Web.UI.WebControls.Label lblCur;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPrtCtrl;
    protected System.Web.UI.WebControls.DropDownList lstReport;
    protected System.Web.UI.WebControls.DropDownList lstConference;
    protected System.Web.UI.WebControls.DropDownList DrpChrtType;
    protected System.Web.UI.WebControls.DropDownList lstBridges;
    protected System.Web.UI.WebControls.TextBox txtWeekDate;
    protected System.Web.UI.WebControls.TextBox txtZipCode;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart1;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart7;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart8;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart9;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart10;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart11;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart12;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart13;
    protected System.Web.UI.DataVisualization.Charting.Chart Chart19;     
    protected System.Web.UI.WebControls.TextBox txtCusDateFrm;
    protected System.Web.UI.WebControls.TextBox txtCusDateTo;
    protected System.Web.UI.WebControls.TextBox txtDate;
    protected System.Web.UI.WebControls.RequiredFieldValidator reqCusFrom;
    protected System.Web.UI.WebControls.RequiredFieldValidator reqCusTo;
    protected System.Web.UI.WebControls.RequiredFieldValidator reqDte;
    protected System.Web.UI.WebControls.Label lblHeading;
    protected System.Web.UI.WebControls.DropDownList lstCountries;
    protected System.Web.UI.WebControls.DropDownList lstStates;
    protected System.Web.UI.WebControls.DropDownList DrpMCU;
    protected System.Web.UI.WebControls.Table tbReports;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart1Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow tbReportsRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart7Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart8Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart9Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart10Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart11Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart12Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart13Row;
    protected System.Web.UI.HtmlControls.HtmlTableRow Chart19Row;
    protected System.Web.UI.HtmlControls.HtmlGenericControl divTab;
    //SQL Reports - Start
    protected AjaxControlToolkit.CollapsiblePanelExtender ParamPanel;
    protected System.Web.UI.WebControls.DropDownList UserReportsList;
    protected System.Web.UI.WebControls.DropDownList UsageReportList;
    protected System.Web.UI.WebControls.DropDownList ReportsList;
    protected System.Web.UI.WebControls.DropDownList ConfScheRptDivList;
    protected System.Web.UI.WebControls.DataGrid ContactListGrid;
    protected System.Web.UI.WebControls.DataGrid DailyScheduleGrid;
    protected System.Web.UI.WebControls.DataGrid PRIAllocationGrid;
    protected System.Web.UI.WebControls.DataGrid ResourceAllocationReportGrid;
    protected System.Web.UI.WebControls.DataGrid AggregateUsageGrid;
    protected System.Web.UI.WebControls.DataGrid UsageByRoomGrid;
    protected System.Web.UI.WebControls.DataGrid CalendarGrid;
    protected System.Web.UI.HtmlControls.HtmlTableRow ContactListRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow DailyScheduleRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow PRIAllocationGRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow ResourceAllocationReportRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow AggregateUsageRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow UsageByRoomRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow CalendarRow;
    protected System.Web.UI.HtmlControls.HtmlGenericControl DetailsDiv;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRowColor;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfNum;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
    protected System.Web.UI.WebControls.Label PtPtCnt;
    protected System.Web.UI.WebControls.Label MultiPtCnt;
    protected System.Web.UI.WebControls.Label VTCMtTotCnt;
    protected System.Web.UI.WebControls.Label GenerateText;
    protected System.Web.UI.WebControls.Table VTCTotal;
    protected System.Web.UI.WebControls.Table VTCDetails;
    protected System.Web.UI.WebControls.TableRow FirstRow;
    protected System.Web.UI.WebControls.TextBox txtStartDate;
    protected System.Web.UI.WebControls.TextBox txtEndDate;
    protected System.Web.UI.WebControls.DropDownList drpTimeZone;
    protected System.Web.UI.WebControls.TextBox txtRooms;
    protected System.Web.UI.WebControls.CheckBox chkSelectall;
    protected System.Web.UI.WebControls.CheckBoxList cblRoom;
    protected MetaBuilders.WebControls.ComboBox CmbStrtTime;
    protected MetaBuilders.WebControls.ComboBox CmbEndTime;
    protected System.Web.UI.HtmlControls.HtmlTableCell LblTZTD;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomIDs;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.ImageButton btnPDF;
    protected System.Web.UI.WebControls.ImageButton btnExcel;
    protected System.Web.UI.WebControls.Label lblDate;
    protected System.Web.UI.HtmlControls.HtmlTableRow trDateDisplay;
    protected System.Web.UI.HtmlControls.HtmlTable SummaryTable;
    protected System.Web.UI.HtmlControls.HtmlTable DetailsHeadingTable;
    protected System.Web.UI.WebControls.ImageButton ImgPrinter;
    protected System.Web.UI.WebControls.TextBox tempText;    //Graphical Reports
    private string savepath = ""; //Graphical Reports
    protected System.Web.UI.ScriptManager DiagnosticsScriptManager;
    protected System.Web.UI.UpdatePanel LocTab;
    protected System.Web.UI.UpdatePanel CountryPan;

    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChartName;

    protected System.Web.UI.WebControls.Button btnAdvRep;//Advance Reports

    
    //SQL Reports - End

    #endregion

    # region Private Members  

    private ns_Logger.Logger log;
    private myVRMNet.NETFunctions obj;

    protected String format = "";
    protected String dFormat = "";
    protected String timeZone = "0";
    protected String tformat = "hh:mm tt";
    protected String rptType = "";
    protected String report = "";
    protected String reportName = "";
    private String CusDateFrm = "";
    private String CusDateTo = "";  
    protected String path = "";
    protected String dtInMonFormat = "MM/dd/yyyy";
    protected String usrID = "";
    //protected Int32[] days = null;
    private DateTime dtStart = DateTime.Now;
    private DateTime startDate = DateTime.Now;
    private DateTime endDate = DateTime.Now;
    private DateTime LastWkEndDate = new DateTime();
    private DateTime LastWkStrtDate = new DateTime();
    private DateTime ThsWkEndDate = new DateTime();
    private DateTime ThsWkStrtDate = new DateTime();
    private DateTime YearToDate = new DateTime();    
    private DayOfWeek day = DateTime.Now.DayOfWeek;
    private DateTimeFormatInfo dtf = null;
    protected Int32 monthNum = Int32.MinValue;

    protected String defaultRoomID = "";
    protected Boolean isItemCreated = false;
    protected Boolean isFillColor = false;
    protected String stDate = "";
    protected DataSet ds = null;
    protected String roomIds = "";
    protected String rowColor = "";
    protected Int32 R = 0;
    protected DataTable table = null;
    protected DataTable table1 = null;
    //FB 1830
    CultureInfo cInfo = null;
    decimal tmpVal = 0;
    protected String colValue = "";
    String language = "en"; //FB 2155


    # endregion

    # region Page Load  
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("GraphicalReport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            path = Application["MyVRMServer_ConfigPath"].ToString();
            savepath = Server.MapPath("image") + @"\Chart1.jpeg";    //Graphical Reports

            if (ConfScheRptDivList.SelectedValue.Equals("1") || ReportsList.SelectedValue.Equals("4"))
            DiagnosticsScriptManager.RegisterAsyncPostBackControl(cblRoom);

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            if (Session["userID"] != null)
            {
                if (Session["userID"].ToString() != "")
                    usrID = Session["userID"].ToString();
            }
            //FB 2155
            if (Session["language"] != null)
            {
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
            }

            if (DrpAllType.SelectedValue != "0")
                report = DrpAllType.SelectedValue;
            //FB 1830
            cInfo = new CultureInfo(Session["NumberFormat"].ToString());

            //SQL Reports start
            if (Session["timeZoneDisplay"] != null)
            {
                if (Session["timeZoneDisplay"].ToString() != "")
                    timeZone = Session["timeZoneDisplay"].ToString();
            }

            //Added for FB 1511 START
            if (Session["EnableEntity"] == null)    //Organization Module Fixes
            {
                Session["EnableEntity"] = "0"; //Organization Module Fixes
            }
            //Added for FB 1511 END

            if (format == "dd/MM/yyyy")
                dFormat = "dmy";
            else
                dFormat = "mdy";

            if (timeZone.Equals("0"))
            {
                drpTimeZone.SelectedValue = "26";

                if (Session["systemTimezoneID"] != null)
                {
                    if (Session["systemTimezoneID"].ToString() != "")
                    {
                        drpTimeZone.SelectedValue = Session["systemTimezoneID"].ToString();
                    }
                }
            }

            obj = ((obj == null) ? new myVRMNet.NETFunctions() : obj);
            CmbStrtTime.Items.Clear();
            CmbEndTime.Items.Clear();
            obj.BindTimeToListBox(CmbStrtTime, false, false);
            obj.BindTimeToListBox(CmbEndTime, false, false);

            if (timeZone.Equals("0"))
            {
                LblTZTD.Attributes.Add("style", "display:none");
            }
            //end SQL Reports

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
            
            lblCur.Text = DateTime.Now.ToString(format + " " + tformat);
            log = new ns_Logger.Logger();

            ChartDisplay();
            //BindCountryState();
			//Advance Graphical Reports
 			if (Session["admin"] != null && Session["organizationID"] != null) //Advance reports
            {
                if (!Session["admin"].ToString().Equals("0") && Session["organizationID"].ToString().Equals("11"))
                {
                    btnAdvRep.Visible = true;
                }
            }
            //FB 1906 - Start
            DateTime reportTime = Convert.ToDateTime("12:00 AM");
            for (int i = 3; i <= 50; i++)
            {
                BoundColumn bndcolumn = PRIAllocationGrid.Columns[i] as BoundColumn;
                bndcolumn.HeaderText = reportTime.ToString(tformat);

                bndcolumn = ResourceAllocationReportGrid.Columns[i + 2] as BoundColumn;
                bndcolumn.HeaderText = reportTime.ToString(tformat);

                reportTime = reportTime.AddMinutes(30);
            }
            //FB 1906 - End

            if (!IsPostBack)
            {
                //FB 1750
                if (Application["Client"].ToString().ToUpper() == "NGC" && ReportsList.Items.IndexOf(ReportsList.Items.FindByValue("5")) == -1)
                    ReportsList.Items.Add(new ListItem("Utilization Reports", "5"));

                BindBridges(lstBridges);

                //SQL Reports Start
                CmbStrtTime.Text = ((CmbStrtTime.SelectedIndex < 0) ? DateTime.Parse("12:00 AM").ToString(tformat) : CmbStrtTime.Text);
                CmbEndTime.Text = ((CmbEndTime.SelectedIndex < 0) ? DateTime.Parse("12:00 AM").ToString(tformat) : CmbEndTime.Text);

                //String inXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetTimezones>";

                String outXML=""; 
                // outXML = obj.CallMyVRMServer("GetTimezones", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>fnAssignDefault()</script>");
                if (defaultRoomID == "")
                    defaultRoomID = "0000";

                BindData(outXML);
                GetLocations("");//FB 1830
                BindData();

                //SQL Reports Start End

                GetReportType();
            }
            Response.Redirect("../" + language + "/ReportDetails.aspx", true); //FB 2155
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_Load" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;            
        }
    }
    # endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeUIComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>

    private void InitializeUIComponent()
    {
        DailyScheduleGrid.ItemDataBound += new DataGridItemEventHandler(DailyScheduleGrid_ItemDataBound);
        DailyScheduleGrid.ItemCreated += new DataGridItemEventHandler(DailyScheduleGrid_ItemCreated);
        DailyScheduleGrid.PageIndexChanged += new DataGridPageChangedEventHandler(DailyScheduleGrid_PageIndexChanged);
        ContactListGrid.ItemDataBound += new DataGridItemEventHandler(ContactListGrid_ItemDataBound);
        ContactListGrid.ItemCreated += new DataGridItemEventHandler(ContactListGrid_ItemCreated);
        ContactListGrid.PageIndexChanged += new DataGridPageChangedEventHandler(ContactListGrid_PageIndexChanged);
        PRIAllocationGrid.ItemCreated += new DataGridItemEventHandler(PRIAllocationGrid_ItemCreated);
        PRIAllocationGrid.ItemDataBound += new DataGridItemEventHandler(PRIAllocationGrid_ItemDataBound);
        PRIAllocationGrid.PageIndexChanged += new DataGridPageChangedEventHandler(PRIAllocationGrid_PageIndexChanged);
        ResourceAllocationReportGrid.ItemCreated += new DataGridItemEventHandler(ResourceAllocationReportGrid_ItemCreated);
        ResourceAllocationReportGrid.ItemDataBound += new DataGridItemEventHandler(ResourceAllocationReportGrid_ItemDataBound);
        ResourceAllocationReportGrid.PageIndexChanged += new DataGridPageChangedEventHandler(ResourceAllocationReportGrid_PageIndexChanged);
        AggregateUsageGrid.ItemCreated += new DataGridItemEventHandler(AggregateUsageGrid_ItemCreated);
        AggregateUsageGrid.ItemDataBound += new DataGridItemEventHandler(AggregateUsageGrid_ItemDataBound);
        UsageByRoomGrid.ItemCreated += new DataGridItemEventHandler(UsageByRoomGrid_ItemCreated);
        UsageByRoomGrid.ItemDataBound += new DataGridItemEventHandler(UsageByRoomGrid_ItemDataBound);
        UsageByRoomGrid.PageIndexChanged += new DataGridPageChangedEventHandler(UsageByRoomGrid_PageIndexChanged);
        CalendarGrid.ItemCreated += new DataGridItemEventHandler(CalendarGrid_ItemCreated);
        CalendarGrid.ItemDataBound += new DataGridItemEventHandler(CalendarGrid_ItemDataBound);
        CalendarGrid.PageIndexChanged += new DataGridPageChangedEventHandler(CalendarGrid_PageIndexChanged);

        chkSelectall.Attributes.Add("onclick", "javascript:fnSelectAll(this,'" + cblRoom.ClientID + "');");
        cblRoom.Attributes.Add("onclick", "javascript:fnDeselectAll(this,'" + chkSelectall + "');");

        btnviewRep.Click += new EventHandler(btnviewRep_Click);
        //ImgPrinter.Click += new ImageClickEventHandler(ImgPrinter_Click);

    }  

    #endregion

    #region BindData  

    private void BindData()
    {
        String strSQL = "";
        String tmzone = "";
        String startTime = "";
        String endTime = "";
        String roomList = "";
        String inXML = "";
        String outXML = "";
        XmlDocument xmlDstDoc = new XmlDocument();
        try
        {
            // GenerateText.Text = DateTime.Now.ToString();
            if (Session["timezoneID"] != null)
                tmzone = Session["timezoneID"].ToString();//drpTimeZone.SelectedValue;
            else
                tmzone = "26";

            //lblDate.Text = "<b>From:</b> " + myVRMNet.NETFunctions.GetFormattedDate(txtStartDate.Text) + " " + CmbStrtTime.Text
            //            + " <b>To:</b> " + myVRMNet.NETFunctions.GetFormattedDate(txtEndDate.Text) + " " + CmbEndTime.Text;
            lblDate.Text = "<b>From:</b> " + txtStartDate.Text + " " + CmbStrtTime.Text
                        + " <b>To:</b> " + txtEndDate.Text + " " + CmbEndTime.Text;//FB 1906

            startTime = myVRMNet.NETFunctions.GetDefaultDate(txtStartDate.Text) + " " + CmbStrtTime.Text;
            endTime = myVRMNet.NETFunctions.GetDefaultDate(txtEndDate.Text) + " " + CmbEndTime.Text;
            
            GetRoomIDs();                       
            roomList = roomIds;

            if (!IsPostBack)
            {
                if (ReportsList.SelectedValue == "")
                    ReportsList.SelectedValue = "1";

                if (ConfScheRptDivList.SelectedValue == "")
                    ConfScheRptDivList.SelectedValue = "1";
            }

            switch (ReportsList.SelectedValue)
            {
                case "1":
                    switch (ConfScheRptDivList.SelectedValue)
                    {
                        case "1":
                            report = "CAL";
                            break;
                        case "2":
                            report = "DS";
                            break;
                        case "3":
                            report = "PRI";
                            break;
                        case "4":
                            report = "RAL";
                            break;
                    }
                    break;
                case "2":
                    report = "CL";
                    break;
                case "3":
                    switch (UsageReportList.SelectedValue)
                    {
                        case "1":
                            report = "AU";
                            break;
                        case "2":
                            report = "UR";
                            break;
                    }
                    break;
            }

            inXML = "<report>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "<configpath>" + Application["MyVRMServer_ConfigPath"].ToString() + "</configpath>";
            inXML += "<starttime>" + startTime + "</starttime>";
            inXML += "<endtime>" + endTime + "</endtime>";
            inXML += "<timezone>" + tmzone + "</timezone>";
            inXML += "<roomslist>" + roomList + "</roomslist>";
            inXML += "<reportType>" + report + "</reportType>";
            inXML += "</report>";

            outXML = obj.CallMyVRMServer("ConferenceReports", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") >= 0)
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
            }
            else
            {
                xmlDstDoc.LoadXml(outXML);
                ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
            }
                     
            HideRows();

            ShowReports();

        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    private void BindData(String outXML)
    {
        string tmpstr = "";
        string sel = "";
        string[] tmpstrs;
        int i, tmpint;
        XmlDocument xmldoc = new XmlDocument();
        XmlNodeList nodelist;
        try
        {

            //xmldoc = new XmlDocument();
            //xmldoc.LoadXml(outXML);

            //log.Trace("GetTimezones: " + outXML);

            //sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;
            //XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");

            //if (nodes.Count > 0)
            //{
            //    obj.LoadList(drpTimeZone, nodes, "timezoneID", "timezoneName");

            //}
            //drpTimeZone.SelectedValue = sel;
            if (dFormat == "dmy")
            {
                txtStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.ToShortDateString());
                txtEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.AddDays(1).ToShortDateString());

                txtCusDateFrm.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.ToShortDateString());
                txtCusDateTo.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.AddDays(1).ToShortDateString());
            }
            else
            {
                txtStartDate.Text = DateTime.Today.ToShortDateString();
                txtEndDate.Text = DateTime.Today.AddDays(1).ToShortDateString();

                txtCusDateFrm.Text = DateTime.Today.ToShortDateString();
                txtCusDateTo.Text = DateTime.Today.AddDays(1).ToShortDateString();
            }

            //GetLocations(""); //Commented for Onchange
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }

    }

    #endregion

    #region BindCountryState

    private void BindCountryState()
    {
        try
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //if (!IsPostBack)
            //{
                if (obj != null)
                {
                    obj.GetCountryCodes(lstCountries);
                    lstCountries.Items.FindByValue("225").Selected = true; // United States
                    obj.GetCountryStates(lstStates, lstCountries.SelectedValue);
                    lstStates.Items.FindByValue("34").Selected = true; // NY
                }
            //}
        }
        catch (Exception ex)
        {
            log.Trace("BindCountryState: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region UpdateStates 
    protected void UpdateStates(Object sender, EventArgs e)
    {
        try
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            lstStates.Items.Clear();
            if (!lstCountries.SelectedValue.Equals("-1"))
                obj.GetCountryStates(lstStates, lstCountries.SelectedValue);

        }
        catch (Exception ex)
        {
            log.Trace("UpdateStates: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    # region ViewReport 

    private void viewReport()
    {
        log = new ns_Logger.Logger();
        try
        {
            String repPath = "";

            GetReportType();
        }
        catch (Exception ex)
        {
            log.Trace("View Report: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    # endregion

    #region GetReportType

    private void GetReportType()
    {
        log = new ns_Logger.Logger();
        try
        {
            switch (lstReport.SelectedValue)
            {
                case "1":
                    rptType = "M";
                    break;
                case "2":
                    rptType = "W";
                    break;
                case "3":
                    rptType = "D";
                    break;
            }
        }
        catch (Exception ex)
        {
            log.Trace("GetReportType: " + ex.StackTrace + " : " + ex.Message);
        }

    }

    #endregion

    #region BindChart

    private void BindChart(DataSet ds)
    {
        String xVal = "";
        Double yVal = 0;
        log = new ns_Logger.Logger();

        try
        {
            if (DrpAllType.SelectedValue == "1" && DrpChrtType.SelectedValue != "0")
            {
                if (lstConference.SelectedValue == "1") //Conference Type All ( Audio, Video, Room, Pt to Pt)
                {
                    switch (lstReport.SelectedValue)
                    {
                        case "1":
                                lblHeading.Text = obj.GetTranslatedText("Yesterday Conference Total Usage Report");//FB 1830 - Translation
                                break;
                        case "2":
                                lblHeading.Text = obj.GetTranslatedText("Last Week Conferences Total Usage Report");//FB 1830 - Translation
                                break;
                        case "3":
                                lblHeading.Text = obj.GetTranslatedText("Last Month Conferences Total Usage Report");//FB 1830 - Translation
                                break;
                        case "4":
                                lblHeading.Text = obj.GetTranslatedText("This Week Conferences Total Usage Report");//FB 1830 - Translation
                                break;
                        case "5":
                                lblHeading.Text = obj.GetTranslatedText("Year To Date Conferences Total Usage Report");//FB 1830 - Translation
                                break;
                        case "6":
                                lblHeading.Text = obj.GetTranslatedText("Custom Date Conferences Total Usage Report");//FB 1830 - Translation
                            break;
                    }

                    if (DrpChrtType.SelectedValue.Equals("1"))
                    {
                        if (ds.Tables.Count > 0)
                        {
                            foreach (DataColumn column in ds.Tables[0].Columns)
                            {
                                if (column.ColumnName == "confTypeCnt1" || column.ColumnName == "confTypeCnt2" || column.ColumnName == "confTypeCnt3" || column.ColumnName == "confTypeCnt4" || column.ColumnName == "M")
                                {
                                    TableRow row = new TableRow();
                                    TableCell cell = new TableCell();
                                    cell.Width = Unit.Percentage(25);
                                    cell.CssClass = "tableHeader";
                                    cell.Font.Bold = true;
                                    row.CssClass = "tableBody";

                                    if (column.ColumnName == "confTypeCnt1")
                                        cell.Controls.Add(new LiteralControl("Audio Conferences"));
                                    if (column.ColumnName == "confTypeCnt2")
                                        cell.Controls.Add(new LiteralControl("Video Conferences"));
                                    if (column.ColumnName == "confTypeCnt3")
                                        cell.Controls.Add(new LiteralControl("Room Conferences"));
                                    if (column.ColumnName == "confTypeCnt4")
                                        cell.Controls.Add(new LiteralControl("Point-to- Point"));
                                    else if (column.ColumnName == "M")
                                    {
                                        if (lstReport.SelectedValue == "3" || lstReport.SelectedValue == "5")
                                            cell.Controls.Add(new LiteralControl("Month(s)"));
                                        else
                                            cell.Controls.Add(new LiteralControl("Day(s)"));

                                    }
                                    //cell.Width = Unit.Pixel(750);

                                    row.Cells.Add(cell);

                                    int pointIndex = 0;
                                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                                    {
                                        TableCell dataCell = new TableCell();
                                        if (column.ColumnName == "M")
                                            dataCell.CssClass = "verticaltextlong";
                                        //FB 1830 Start
                                         if (column.ColumnName.IndexOf("Cnt") > 0)
                                        {
                                            tmpVal = 0;
                                            decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                            colValue = tmpVal.ToString("n0", cInfo);
                                        }
                                        else
                                            colValue = dataRow[column].ToString();
                                        //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                        dataCell.Controls.Add(new LiteralControl(colValue));
                                        //FB 1830 End
                                        //dataCell.Width = Unit.Pixel(750);
                                        dataCell.HorizontalAlign = HorizontalAlign.Center;
                                        row.Cells.Add(dataCell);
                                        ++pointIndex;
                                    }

                                    tbReports.Rows.Add(row);
                                }
                            }

                        }
                        else if (ds.Tables.Count == 0)
                        {
                            TableRow row = new TableRow();

                            TableCell cell = new TableCell();
                            //cell.CssClass = "tableHeader";
                            row.CssClass = "tableBody";

                            cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                            cell.CssClass = "lblError";
                            cell.HorizontalAlign = HorizontalAlign.Center;
                            cell.VerticalAlign = VerticalAlign.Middle;

                            row.Cells.Add(cell);
                            tbReports.Rows.Add(row);
                        }
                        tbReportsRow.Visible = true;
                        tbReports.Visible = true;
                        //hdnValue.Value = "tbReports";
                    }
                    else
                    {
                        if (DrpChrtType.SelectedValue == "2")
                        {
                            Chart1.Series["Series1"].ChartType = SeriesChartType.Column;
                            Chart1.Series["Series2"].ChartType = SeriesChartType.Column;
                            Chart1.Series["Series3"].ChartType = SeriesChartType.Column;
                            Chart1.Series["Series4"].ChartType = SeriesChartType.Column;
                            Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            if (lstReport.SelectedValue == "3" || lstReport.SelectedValue == "5")
                                Chart1.ChartAreas["ChartArea1"].AxisX.Title = "Month";
                            else
                                Chart1.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";

                            if (ds.Tables.Count > 0)
                            {
                                for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    if (ds.Tables[0].Rows[i]["confTypeCnt1"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt1"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }


                                    if (ds.Tables[0].Rows[i]["confTypeCnt2"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt2"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }


                                    if (ds.Tables[0].Rows[i]["confTypeCnt3"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt3"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }


                                    if (ds.Tables[0].Rows[i]["confTypeCnt4"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt4"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }
                                }
                            }

                            Chart1.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                            Chart1.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            Chart1.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                        }

                        else if (DrpChrtType.SelectedValue == "3")
                        {
                            Chart1.Series["Series1"].ChartType = SeriesChartType.Line;
                            Chart1.Series["Series2"].ChartType = SeriesChartType.Line;
                            Chart1.Series["Series3"].ChartType = SeriesChartType.Line;
                            Chart1.Series["Series4"].ChartType = SeriesChartType.Line;


                            Chart1.Series["Series1"].MarkerStyle = MarkerStyle.Diamond;
                            Chart1.Series["Series2"].MarkerStyle = MarkerStyle.Diamond;
                            Chart1.Series["Series3"].MarkerStyle = MarkerStyle.Diamond;
                            Chart1.Series["Series4"].MarkerStyle = MarkerStyle.Diamond;


                            if (ds.Tables.Count > 0)
                            {
                                for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    if (ds.Tables[0].Rows[i]["confTypeCnt1"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt1"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }


                                    if (ds.Tables[0].Rows[i]["confTypeCnt2"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt2"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }


                                    if (ds.Tables[0].Rows[i]["confTypeCnt3"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt3"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }


                                    if (ds.Tables[0].Rows[i]["confTypeCnt4"].ToString() == "0")
                                    {
                                        // ds.Tables[0].Rows[i]["confTypeCnt4"] = null;
                                        ds.Tables[0].Rows[i].AcceptChanges();
                                    }
                                }
                            }


                            Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;


                            if (lstReport.SelectedValue == "3" || lstReport.SelectedValue == "5")
                                Chart1.ChartAreas["ChartArea1"].AxisX.Title = "Month";
                            else
                                Chart1.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                            if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                            {
                                Chart1.Series["Series1"].MarkerSize = 3;
                                Chart1.Series["Series2"].MarkerSize = 3;
                                Chart1.Series["Series3"].MarkerSize = 3;
                                Chart1.Series["Series4"].MarkerSize = 3;
                            }
                            else
                            {
                                Chart1.Series["Series1"].MarkerSize = 10;
                                Chart1.Series["Series2"].MarkerSize = 10;
                                Chart1.Series["Series3"].MarkerSize = 10;
                                Chart1.Series["Series4"].MarkerSize = 10;
                            }

                            Chart1.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                            Chart1.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            Chart1.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                        }
                        if (DrpChrtType.SelectedValue == "4")
                        {
                            Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            Chart1.Series["Series1"].ChartType = SeriesChartType.Pie;
                            Chart1.Series["Series2"].ChartType = SeriesChartType.Pie;
                            Chart1.Series["Series3"].ChartType = SeriesChartType.Pie;
                            Chart1.Series["Series4"].ChartType = SeriesChartType.Pie;
                        }

                        Chart1.BackGradientStyle = GradientStyle.TopBottom;
                        Chart1.BackColor = System.Drawing.Color.Yellow;
                        Chart1.BackSecondaryColor = System.Drawing.Color.Orange;
                        Chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                        Chart1.BorderlineDashStyle = ChartDashStyle.Solid;
                        Chart1.BorderlineWidth = 1;
                        Chart1.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;

                        if (ds.Tables.Count > 0)
                        {
                            Chart1.DataSource = ds.Tables[0];


                            Chart1.Series["Series1"].XValueMember = "M";
                            Chart1.Series["Series2"].XValueMember = "M";
                            Chart1.Series["Series3"].XValueMember = "M";
                            Chart1.Series["Series4"].XValueMember = "M";




                            Chart1.Series["Series1"].YValueMembers = "confTypeCnt1";
                            Chart1.Series["Series2"].YValueMembers = "confTypeCnt2";
                            Chart1.Series["Series3"].YValueMembers = "confTypeCnt3";
                            Chart1.Series["Series4"].YValueMembers = "confTypeCnt4";



                            Chart1.Series["Series1"].Color = System.Drawing.Color.DarkGreen;
                            Chart1.Series["Series2"].Color = System.Drawing.Color.Red;
                            Chart1.Series["Series3"].Color = System.Drawing.Color.Orange;
                            Chart1.Series["Series4"].Color = System.Drawing.Color.Blue;

                            //Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            Chart1.Series["Series1"].LegendText = "Audio Conferences";
                            Chart1.Series["Series2"].LegendText = "Video Conferences";
                            Chart1.Series["Series3"].LegendText = "Room Conferences";
                            Chart1.Series["Series4"].LegendText = "Point-to-Point";
                            Chart1.AlignDataPointsByAxisLabel(PointSortOrder.Ascending);

                            Chart1.Series["Series1"].IsXValueIndexed = false;
                            Chart1.Series["Series2"].IsXValueIndexed = false;
                            Chart1.Series["Series3"].IsXValueIndexed = false;
                            Chart1.Series["Series4"].IsXValueIndexed = false;
                        }

                        else if (ds.Tables.Count == 0)
                        {

                            Chart1.Series["Series1"].Enabled = false;
                            Chart1.Series["Series2"].Enabled = false;
                            Chart1.Series["Series3"].Enabled = false;
                            Chart1.Series["Series4"].Enabled = false;
                            Chart1.Titles.Add(new Title
                            {
                                Text = "No Data Found",
                                Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                              System.Drawing.GraphicsUnit.Point),
                                Position = Chart1.ChartAreas["ChartArea1"].Position,
                                TextStyle = TextStyle.Default,
                                Alignment = System.Drawing.ContentAlignment.MiddleCenter
                            });

                        }
                        Chart1Row.Visible = true;
                        Chart1.Visible = true;
                        Chart1.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                        hdnChartName.Value = "Chart1";
                    }
                }
                if (!lstConference.SelectedValue.Equals("1"))
                {
                    if (lstReport.SelectedValue.Equals("1"))
                    {
                        lblHeading.Text = "Yesterday - " + lstConference.SelectedItem.Text.Replace("Only", "") + " Usage Report";


                        if (DrpChrtType.SelectedValue.Equals("1"))
                        {
                            if (ds.Tables.Count > 0)
                            {
                                foreach (DataColumn column in ds.Tables[0].Columns)
                                {
                                    if (column.ColumnName == "confTypeCnt1" || column.ColumnName == "D")
                                    {
                                        TableRow row = new TableRow();

                                        TableCell cell = new TableCell();

                                        cell.CssClass = "tableHeader";
                                        cell.Font.Bold = true;

                                        row.CssClass = "tableBody";

                                        if (column.ColumnName == "confTypeCnt1")
                                            cell.Controls.Add(new LiteralControl(lstConference.SelectedItem.Text.Replace("Only", "")));
                                        else if (column.ColumnName == "D")
                                            cell.Controls.Add(new LiteralControl("Day(s)"));
                                        //cell.Width = new Unit(100, UnitType.Pixel);

                                        row.Cells.Add(cell);

                                        int pointIndex = 0;
                                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                                        {
                                            TableCell dataCell = new TableCell();
                                            if (column.ColumnName == "D")//Added for Vertical alignment
                                                dataCell.CssClass = "verticaltextlong";
                                            //FB 1830 Start
                                            if (column.ColumnName.IndexOf("Cnt") > 0)
                                            {
                                                tmpVal = 0;
                                                decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                                colValue = tmpVal.ToString("n0", cInfo);
                                            }
                                            else
                                                colValue = dataRow[column].ToString();
                                            //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                            dataCell.Controls.Add(new LiteralControl(colValue));
                                            //FB 1830 end
                                            //dataCell.Width = new Unit(100, UnitType.Pixel);
                                            dataCell.HorizontalAlign = HorizontalAlign.Center;
                                            row.Cells.Add(dataCell);
                                            ++pointIndex;
                                        }

                                        tbReports.Rows.Add(row);
                                    }
                                }

                            }
                            else if (ds.Tables.Count == 0)
                            {
                                TableRow row = new TableRow();

                                TableCell cell = new TableCell();
                                //cell.CssClass = "tableHeader";
                                row.CssClass = "tableBody";

                                cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                                cell.CssClass = "lblError";
                                cell.HorizontalAlign = HorizontalAlign.Center;
                                cell.VerticalAlign = VerticalAlign.Middle;

                                row.Cells.Add(cell);
                                tbReports.Rows.Add(row);
                            }
                            tbReportsRow.Visible = true;
                            tbReports.Visible = true;

                        }
                        else
                        {
                            if (DrpChrtType.SelectedValue == "2")
                            {
                                Chart7.Series["Series1"].ChartType = SeriesChartType.Column;
                                Chart7.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart7.ChartAreas["ChartArea1"].AxisX.Title = "Day";
                                Chart7.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart7.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart7.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }

                            else if (DrpChrtType.SelectedValue == "3")
                            {
                                Chart7.Series["Series1"].ChartType = SeriesChartType.Line;
                                Chart7.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart7.ChartAreas["ChartArea1"].AxisX.Title = "Day";
                                Chart7.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart7.Series["Series1"].MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    Chart7.Series["Series1"].MarkerSize = 3;
                                else
                                    Chart7.Series["Series1"].MarkerSize = 10;
                                Chart7.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart7.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            }
                            Chart7.BackGradientStyle = GradientStyle.TopBottom;
                            Chart7.BackColor = System.Drawing.Color.Yellow;
                            Chart7.BackSecondaryColor = System.Drawing.Color.Orange;
                            Chart7.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                            Chart7.BorderlineDashStyle = ChartDashStyle.Solid;
                            Chart7.BorderlineWidth = 1;
                            Chart7.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;

                            if (ds.Tables.Count > 0)
                            {
                                Chart7.DataSource = ds.Tables[0];


                                if (DrpChrtType.SelectedValue == "4")
                                {
                                    DataTable dt = new DataTable();
                                    if (ds.Tables.Count > 0)
                                        dt = ds.Tables[0];
                                    String lbl1 = "";
                                    Int32 series1 = 0;
                                    Chart7.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                    Chart7.Series["Series1"].ChartType = SeriesChartType.Pie;


                                    Chart7.Series["Series1"]["PieLabelStyle"] = "Inside";

                                    Chart7.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

                                    if (dt.Rows[0]["confTypeCnt1"].ToString() != "")
                                        series1 = Convert.ToInt32(dt.Rows[0]["confTypeCnt1"]);

                                    if (series1 != 0)
                                        lbl1 = series1 + "%";

                                    string[] xValues = { lbl1 };

                                    int[] yValues = { series1 };

                                    Chart7.Series["Series1"].Points.DataBindXY(xValues, yValues);
                                    Chart7.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 15;
                                }
                                else
                                {
                                    Chart7.Series["Series1"].XValueMember = "D";
                                    Chart7.Series["Series1"].XValueType = ChartValueType.DateTime;
                                    Chart7.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                                    Chart7.Series["Series1"].YValueMembers = "confTypeCnt1";

                                    if (dFormat == "dmy")
                                    {
                                        Chart7.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "dd/MM/yy";

                                    }
                                    else
                                    {
                                        Chart7.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MM/dd/yy";
                                    }
                                }


                                if (lstConference.SelectedValue == "6")
                                    Chart7.Series["Series1"].Color = System.Drawing.Color.DarkGreen;
                                if (lstConference.SelectedValue == "2")
                                    Chart7.Series["Series1"].Color = System.Drawing.Color.Red;
                                if (lstConference.SelectedValue == "7")
                                    Chart7.Series["Series1"].Color = System.Drawing.Color.Orange;
                                if (lstConference.SelectedValue == "4")
                                    Chart7.Series["Series1"].Color = System.Drawing.Color.Blue;

                                Chart7.Series["Series1"].LegendText = lstConference.SelectedItem.Text.Replace("Only", "");
                                Chart7.AlignDataPointsByAxisLabel();
                            }

                            if (ds.Tables.Count == 0)
                            {

                                Chart7.Series["Series1"].Enabled = false;
                                Chart7.Titles.Add(new Title
                                {
                                    Text = "No Data Found",
                                    Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                                  System.Drawing.GraphicsUnit.Point),
                                    Position = Chart7.ChartAreas["ChartArea1"].Position,
                                    TextStyle = TextStyle.Default,
                                    Alignment = System.Drawing.ContentAlignment.MiddleCenter
                                });

                            }
                            Chart7Row.Visible = true;
                            Chart7.Visible = true;
                            Chart7.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                            hdnChartName.Value = "Chart7";

                        }


                    }
                    if (lstReport.SelectedValue.Equals("2"))
                    {
                        lblHeading.Text = "Last Week - " + lstConference.SelectedItem.Text.Replace("Only", "") + " Usage Report";

                        if (DrpChrtType.SelectedValue.Equals("1"))
                        {
                            if (ds.Tables.Count > 0)
                            {
                                foreach (DataColumn column in ds.Tables[0].Columns)
                                {
                                    if (column.ColumnName == "confTypeCnt1" || column.ColumnName == "D")
                                    {
                                        TableRow row = new TableRow();

                                        TableCell cell = new TableCell();

                                        cell.CssClass = "tableHeader";
                                        cell.Font.Bold = true;

                                        row.CssClass = "tableBody";

                                        if (column.ColumnName == "confTypeCnt1")
                                            cell.Controls.Add(new LiteralControl(lstConference.SelectedItem.Text.Replace("Only", "")));
                                        else if (column.ColumnName == "D")
                                            cell.Controls.Add(new LiteralControl("Day(s)"));

                                        //cell.Width = new Unit(100, UnitType.Pixel);

                                        row.Cells.Add(cell);

                                        int pointIndex = 0;
                                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                                        {
                                            TableCell dataCell = new TableCell();
                                            if (column.ColumnName == "D")//Added for Vertical alignment
                                                dataCell.CssClass = "verticaltextlong";
                                            //FB 1830 Start
                                            if (column.ColumnName.IndexOf("Cnt") > 0)
                                            {
                                                tmpVal = 0;
                                                decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                                colValue = tmpVal.ToString("n0", cInfo);
                                            }
                                            else
                                                colValue = dataRow[column].ToString();
                                            //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                            dataCell.Controls.Add(new LiteralControl(colValue));
                                            //FB 1830 end
                                            //dataCell.Width = new Unit(100, UnitType.Pixel);
                                            dataCell.HorizontalAlign = HorizontalAlign.Center;
                                            row.Cells.Add(dataCell);
                                            ++pointIndex;
                                        }


                                        tbReports.Rows.Add(row);
                                    }
                                }


                            }
                            else if (ds.Tables.Count == 0)
                            {
                                TableRow row = new TableRow();

                                TableCell cell = new TableCell();
                                //cell.CssClass = "tableHeader";
                                row.CssClass = "tableBody";

                                cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                                cell.CssClass = "lblError";
                                cell.HorizontalAlign = HorizontalAlign.Center;
                                cell.VerticalAlign = VerticalAlign.Middle;

                                row.Cells.Add(cell);
                                tbReports.Rows.Add(row);
                            }
                            tbReportsRow.Visible = true;
                            tbReports.Visible = true;

                        }
                        else
                        {

                            if (DrpChrtType.SelectedValue == "2")
                            {
                                Chart8.Series["Series1"].ChartType = SeriesChartType.Column;
                                Chart8.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart8.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart8.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart8.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart8.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }

                            else if (DrpChrtType.SelectedValue == "3")
                            {
                                Chart8.Series["Series1"].ChartType = SeriesChartType.Line;
                                Chart8.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart8.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart8.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart8.Series["Series1"].MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    Chart8.Series["Series1"].MarkerSize = 3;
                                else
                                    Chart8.Series["Series1"].MarkerSize = 10;
                                Chart8.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart8.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }


                            Chart8.BackGradientStyle = GradientStyle.TopBottom;
                            Chart8.BackColor = System.Drawing.Color.Yellow;
                            Chart8.BackSecondaryColor = System.Drawing.Color.Orange;
                            Chart8.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                            Chart8.BorderlineDashStyle = ChartDashStyle.Solid;
                            Chart8.BorderlineWidth = 1;
                            Chart8.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;

                            if (ds.Tables.Count > 0)
                            //if (ds.Tables[0].Rows.Count > 0)
                            {
                                Chart8.DataSource = ds.Tables[0];

                                if (DrpChrtType.SelectedValue == "4")
                                {
                                    DataTable dt = new DataTable();
                                    if (ds.Tables.Count > 0)
                                        dt = ds.Tables[0];
                                    String lbl1 = "";
                                    Int32 series1 = 0;
                                    Chart8.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                    Chart8.Series["Series1"].ChartType = SeriesChartType.Pie;


                                    Chart8.Series["Series1"]["PieLabelStyle"] = "Inside";

                                    Chart8.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

                                    if (dt.Rows[0]["confTypeCnt1"].ToString() != "")
                                        series1 = Convert.ToInt32(dt.Rows[0]["confTypeCnt1"]);

                                    if (series1 != 0)
                                        lbl1 = series1 + "%";

                                    string[] xValues = { lbl1 };

                                    int[] yValues = { series1 };

                                    Chart8.Series["Series1"].Points.DataBindXY(xValues, yValues);
                                    Chart8.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 15;
                                }
                                else
                                {
                                    Chart8.Series["Series1"].XValueMember = "D";
                                    Chart8.Series["Series1"].XValueType = ChartValueType.DateTime;
                                    Chart8.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                                    Chart8.Series["Series1"].YValueMembers = "confTypeCnt1";

                                    if (dFormat == "dmy")
                                    {
                                        Chart8.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "dd/MM/yy";

                                    }
                                    else
                                    {
                                        Chart8.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MM/dd/yy";
                                    }
                                }

                                if (lstConference.SelectedValue == "6")
                                    Chart8.Series["Series1"].Color = System.Drawing.Color.DarkGreen;
                                if (lstConference.SelectedValue == "2")
                                    Chart8.Series["Series1"].Color = System.Drawing.Color.Red;
                                if (lstConference.SelectedValue == "7")
                                    Chart8.Series["Series1"].Color = System.Drawing.Color.Orange;
                                if (lstConference.SelectedValue == "4")
                                    Chart8.Series["Series1"].Color = System.Drawing.Color.Blue;

                                Chart8.Series["Series1"].LegendText = lstConference.SelectedItem.Text.Replace("Only", "");
                                Chart8.AlignDataPointsByAxisLabel();
                            }


                            if (ds.Tables.Count == 0)
                            {

                                Chart8.Series["Series1"].Enabled = false;
                                Chart8.Titles.Add(new Title
                                {
                                    Text = "No Data Found",
                                    Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                                  System.Drawing.GraphicsUnit.Point),
                                    Position = Chart8.ChartAreas["ChartArea1"].Position,
                                    TextStyle = TextStyle.Default,
                                    Alignment = System.Drawing.ContentAlignment.MiddleCenter
                                });

                            }
                            Chart8Row.Visible = true;
                            Chart8.Visible = true;
                            Chart8.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                            hdnChartName.Value = "Chart8";


                        }
                    }
                    if (lstReport.SelectedValue.Equals("3"))
                    {
                        lblHeading.Text = "Last Month - " + lstConference.SelectedItem.Text.Replace("Only", "") + " Usage Report";


                        if (DrpChrtType.SelectedValue.Equals("1"))
                        {
                            if (ds.Tables.Count > 0)
                            {
                                foreach (DataColumn column in ds.Tables[0].Columns)
                                {
                                    if (column.ColumnName == "confTypeCnt1" || column.ColumnName == "M")
                                    {
                                        TableRow row = new TableRow();

                                        TableCell cell = new TableCell();
                                        cell.CssClass = "tableHeader";
                                        cell.Font.Bold = true;

                                        row.CssClass = "tableBody";

                                        if (column.ColumnName == "confTypeCnt1")
                                            cell.Controls.Add(new LiteralControl(lstConference.SelectedItem.Text.Replace("Only", "")));
                                        else if (column.ColumnName == "M")
                                            cell.Controls.Add(new LiteralControl("Month"));

                                        row.Cells.Add(cell);

                                        int pointIndex = 0;
                                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                                        {
                                            TableCell dataCell = new TableCell();
                                            if (column.ColumnName == "M")//Added for Vertical alignment
                                                dataCell.CssClass = "verticaltextlong";
                                            //FB 1830 Start
                                            if (column.ColumnName.IndexOf("Cnt") > 0)
                                            {
                                                tmpVal = 0;
                                                decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                                colValue = tmpVal.ToString("n0", cInfo);
                                            }
                                            else
                                                colValue = dataRow[column].ToString();
                                            //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                            dataCell.Controls.Add(new LiteralControl(colValue));
                                            //FB 1830 end
                                            //dataCell.Width = new Unit(100, UnitType.Pixel);
                                            dataCell.HorizontalAlign = HorizontalAlign.Center;
                                            row.Cells.Add(dataCell);
                                            ++pointIndex;
                                        }
                                        tbReports.Rows.Add(row);
                                    }
                                }

                            }
                            else if (ds.Tables.Count == 0)
                            {
                                TableRow row = new TableRow();

                                TableCell cell = new TableCell();
                                //cell.CssClass = "tableHeader";
                                row.CssClass = "tableBody";

                                cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                                cell.CssClass = "lblError";
                                cell.HorizontalAlign = HorizontalAlign.Center;
                                cell.VerticalAlign = VerticalAlign.Middle;

                                row.Cells.Add(cell);
                                tbReports.Rows.Add(row);
                            }
                            tbReportsRow.Visible = true;
                            tbReports.Visible = true;

                        }
                        else
                        {
                            if (DrpChrtType.SelectedValue == "2")
                            {
                                Chart9.Series["Series1"].ChartType = SeriesChartType.Column;
                                Chart9.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart9.ChartAreas["ChartArea1"].AxisX.Title = "Month";
                                Chart9.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart9.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart9.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }
                            else if (DrpChrtType.SelectedValue == "3")
                            {
                                Chart9.Series["Series1"].ChartType = SeriesChartType.Line;
                                Chart9.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart9.ChartAreas["ChartArea1"].AxisX.Title = "Month";
                                Chart9.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart9.Series["Series1"].MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    Chart9.Series["Series1"].MarkerSize = 3;
                                else
                                    Chart9.Series["Series1"].MarkerSize = 10;
                                Chart9.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart9.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }

                            Chart9.BackGradientStyle = GradientStyle.TopBottom;
                            Chart9.BackColor = System.Drawing.Color.Yellow;
                            Chart9.BackSecondaryColor = System.Drawing.Color.Orange;
                            Chart9.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                            Chart9.BorderlineDashStyle = ChartDashStyle.Solid;
                            Chart9.BorderlineWidth = 1;
                            Chart9.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;

                            if (ds.Tables.Count > 0)
                            {
                                Chart9.DataSource = ds.Tables[0];

                                if (DrpChrtType.SelectedValue == "4")
                                {
                                    DataTable dt = new DataTable();
                                    if (ds.Tables.Count > 0)
                                        dt = ds.Tables[0];
                                    String lbl1 = "";
                                    Int32 series1 = 0;
                                    Chart9.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                    Chart9.Series["Series1"].ChartType = SeriesChartType.Pie;


                                    Chart9.Series["Series1"]["PieLabelStyle"] = "Inside";

                                    Chart9.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

                                    if (dt.Rows[0]["confTypeCnt1"].ToString() != "")
                                        series1 = Convert.ToInt32(dt.Rows[0]["confTypeCnt1"]);

                                    if (series1 != 0)
                                        lbl1 = series1 + "%";

                                    string[] xValues = { lbl1 };

                                    int[] yValues = { series1 };

                                    Chart9.Series["Series1"].Points.DataBindXY(xValues, yValues);
                                    Chart9.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 15;
                                }
                                else
                                {
                                    Chart9.Series["Series1"].XValueMember = "M";
                                    Chart9.Series["Series1"].XValueType = ChartValueType.DateTime;
                                    Chart9.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                                    Chart9.Series["Series1"].YValueMembers = "confTypeCnt1";

                                    if (dFormat == "dmy")
                                    {
                                        Chart9.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "dd/MM/yy";

                                    }
                                    else
                                    {
                                        Chart9.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MM/dd/yy";
                                    }
                                }

                                if (lstConference.SelectedValue == "6")
                                    Chart9.Series["Series1"].Color = System.Drawing.Color.DarkGreen;
                                if (lstConference.SelectedValue == "2")
                                    Chart9.Series["Series1"].Color = System.Drawing.Color.Red;
                                if (lstConference.SelectedValue == "7")
                                    Chart9.Series["Series1"].Color = System.Drawing.Color.Orange;
                                if (lstConference.SelectedValue == "4")
                                    Chart9.Series["Series1"].Color = System.Drawing.Color.Blue;

                                Chart9.Series["Series1"].LegendText = lstConference.SelectedItem.Text.Replace("Only", "");
                                Chart9.AlignDataPointsByAxisLabel();
                            }

                            if (ds.Tables.Count == 0)
                            {

                                Chart9.Series["Series1"].Enabled = false;
                                Chart9.Titles.Add(new Title
                                {
                                    Text = "No Data Found",
                                    Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                                  System.Drawing.GraphicsUnit.Point),
                                    Position = Chart9.ChartAreas["ChartArea1"].Position,
                                    TextStyle = TextStyle.Default,
                                    Alignment = System.Drawing.ContentAlignment.MiddleCenter
                                });

                            }
                            Chart9Row.Visible = true;
                            Chart9.Visible = true;
                            Chart9.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                            hdnChartName.Value = "Chart9";

                        }
                    }
                    if (lstReport.SelectedValue.Equals("4"))
                    {
                        lblHeading.Text = "This Week - " + lstConference.SelectedItem.Text.Replace("Only", "") + " Usage Report";


                        if (DrpChrtType.SelectedValue.Equals("1"))
                        {
                            if (ds.Tables.Count > 0)
                            {
                                foreach (DataColumn column in ds.Tables[0].Columns)
                                {
                                    if (column.ColumnName == "confTypeCnt1" || column.ColumnName == "D")
                                    {
                                        TableRow row = new TableRow();

                                        TableCell cell = new TableCell();

                                        cell.CssClass = "tableHeader";
                                        cell.Font.Bold = true;

                                        row.CssClass = "tableBody";

                                        if (column.ColumnName == "confTypeCnt1")
                                            cell.Controls.Add(new LiteralControl(lstConference.SelectedItem.Text.Replace("Only", "")));
                                        else if (column.ColumnName == "D")
                                            cell.Controls.Add(new LiteralControl("Day(s)"));
                                        //cell.Width = new Unit(100, UnitType.Pixel);

                                        row.Cells.Add(cell);

                                        int pointIndex = 0;
                                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                                        {
                                            TableCell dataCell = new TableCell();
                                            if (column.ColumnName == "D")//Added for Vertical alignment
                                                dataCell.CssClass = "verticaltextlong";
                                            //FB 1830 Start
                                            if (column.ColumnName.IndexOf("Cnt") > 0)
                                            {
                                                tmpVal = 0;
                                                decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                                colValue = tmpVal.ToString("n0", cInfo);
                                            }
                                            else
                                                colValue = dataRow[column].ToString();
                                            //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                            dataCell.Controls.Add(new LiteralControl(colValue));
                                            //FB 1830 end
                                            //dataCell.Width = new Unit(100, UnitType.Pixel);
                                            dataCell.HorizontalAlign = HorizontalAlign.Center;
                                            row.Cells.Add(dataCell);
                                            ++pointIndex;
                                        }

                                        tbReports.Rows.Add(row);
                                    }
                                }

                            }
                            else if (ds.Tables.Count == 0)
                            {
                                TableRow row = new TableRow();

                                TableCell cell = new TableCell();
                                //cell.CssClass = "tableHeader";
                                row.CssClass = "tableBody";

                                cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                                cell.CssClass = "lblError";
                                cell.HorizontalAlign = HorizontalAlign.Center;
                                cell.VerticalAlign = VerticalAlign.Middle;

                                row.Cells.Add(cell);
                                tbReports.Rows.Add(row);
                            }
                            tbReportsRow.Visible = true;
                            tbReports.Visible = true;
                        }
                        else
                        {
                            if (DrpChrtType.SelectedValue == "2")
                            {
                                Chart10.Series["Series1"].ChartType = SeriesChartType.Column;
                                Chart10.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart10.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart10.ChartAreas["ChartArea1"].AxisY.Title = "Conferences Usage";
                                Chart10.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart10.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            }

                            else if (DrpChrtType.SelectedValue == "3")
                            {
                                Chart10.Series["Series1"].ChartType = SeriesChartType.Line;
                                Chart10.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart10.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart10.ChartAreas["ChartArea1"].AxisY.Title = "Conferences Usage";
                                Chart10.Series["Series1"].MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    Chart10.Series["Series1"].MarkerSize = 3;
                                else
                                    Chart10.Series["Series1"].MarkerSize = 10;
                                Chart10.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart10.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }

                            Chart10.BackGradientStyle = GradientStyle.TopBottom;
                            Chart10.BackColor = System.Drawing.Color.Yellow;
                            Chart10.BackSecondaryColor = System.Drawing.Color.Orange;
                            Chart10.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                            Chart10.BorderlineDashStyle = ChartDashStyle.Solid;
                            Chart10.BorderlineWidth = 1;
                            Chart10.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;

                            if (ds.Tables.Count > 0)
                            {
                                Chart10.DataSource = ds.Tables[0];


                                if (DrpChrtType.SelectedValue == "4")
                                {
                                    DataTable dt = new DataTable();
                                    if (ds.Tables.Count > 0)
                                        dt = ds.Tables[0];
                                    String lbl1 = "";
                                    Int32 series1 = 0;
                                    Chart10.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                    Chart10.Series["Series1"].ChartType = SeriesChartType.Pie;


                                    Chart10.Series["Series1"]["PieLabelStyle"] = "Inside";

                                    Chart10.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

                                    if (dt.Rows[0]["confTypeCnt1"].ToString() != "")
                                        series1 = Convert.ToInt32(dt.Rows[0]["confTypeCnt1"]);

                                    if (series1 != 0)
                                        lbl1 = series1 + "%";

                                    string[] xValues = { lbl1 };

                                    int[] yValues = { series1 };

                                    Chart10.Series["Series1"].Points.DataBindXY(xValues, yValues);
                                    Chart10.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 15;
                                }
                                else
                                {
                                    Chart10.Series["Series1"].XValueMember = "D";
                                    Chart10.Series["Series1"].XValueType = ChartValueType.DateTime;
                                    Chart10.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                                    Chart10.Series["Series1"].YValueMembers = "confTypeCnt1";

                                    if (dFormat == "dmy")
                                    {
                                        Chart10.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "dd/MM/yy";

                                    }
                                    else
                                    {
                                        Chart10.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MM/dd/yy";
                                    }
                                }

                                if (lstConference.SelectedValue == "6")
                                    Chart10.Series["Series1"].Color = System.Drawing.Color.DarkGreen;
                                if (lstConference.SelectedValue == "2")
                                    Chart10.Series["Series1"].Color = System.Drawing.Color.Red;
                                if (lstConference.SelectedValue == "7")
                                    Chart10.Series["Series1"].Color = System.Drawing.Color.Orange;
                                if (lstConference.SelectedValue == "4")
                                    Chart10.Series["Series1"].Color = System.Drawing.Color.Blue;

                                Chart10.Series["Series1"].LegendText = lstConference.SelectedItem.Text.Replace("Only", "");
                                Chart10.AlignDataPointsByAxisLabel();
                            }

                            if (ds.Tables.Count == 0)
                            {

                                Chart10.Series["Series1"].Enabled = false;
                                Chart10.Titles.Add(new Title
                                {
                                    Text = "No Data Found",
                                    Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                                  System.Drawing.GraphicsUnit.Point),
                                    Position = Chart10.ChartAreas["ChartArea1"].Position,
                                    TextStyle = TextStyle.Default,
                                    Alignment = System.Drawing.ContentAlignment.MiddleCenter
                                });

                            }
                            Chart10Row.Visible = true;
                            Chart10.Visible = true;
                            Chart10.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                            hdnChartName.Value = "Chart10";

                        }

                    }
                    if (lstReport.SelectedValue.Equals("5"))
                    {
                        lblHeading.Text = "Year To Date - " + lstConference.SelectedItem.Text.Replace("Only", "") + "  Usage Report";

                        if (DrpChrtType.SelectedValue.Equals("1"))
                        {
                            if (ds.Tables.Count > 0)
                            {
                                foreach (DataColumn column in ds.Tables[0].Columns)
                                {
                                    if (column.ColumnName == "confTypeCnt1" || column.ColumnName == "D")
                                    {
                                        TableRow row = new TableRow();

                                        TableCell cell = new TableCell();
                                        cell.CssClass = "tableHeader";
                                        cell.Font.Bold = true;

                                        row.CssClass = "tableBody";

                                        if (column.ColumnName == "confTypeCnt1")
                                            cell.Controls.Add(new LiteralControl(lstConference.SelectedItem.Text.Replace("Only", "")));
                                        else if (column.ColumnName == "D")
                                            cell.Controls.Add(new LiteralControl("Month(s)"));
                                        //cell.Width = Unit.Pixel(500);
                                        cell.Wrap = false;

                                        row.Cells.Add(cell);

                                        int pointIndex = 0;
                                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                                        {
                                            TableCell dataCell = new TableCell();
                                            if (column.ColumnName == "D")//Added for Vertical alignment
                                                dataCell.CssClass = "verticaltextlong";
                                            //FB 1830 Start
                                            if (column.ColumnName.IndexOf("Cnt") > 0)
                                            {
                                                tmpVal = 0;
                                                decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                                colValue = tmpVal.ToString("n0", cInfo);
                                            }
                                            else
                                                colValue = dataRow[column].ToString();
                                            //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                            dataCell.Controls.Add(new LiteralControl(colValue));
                                            //FB 1830 end
                                            //dataCell.Width = Unit.Pixel(500);
                                            dataCell.Wrap = false;
                                            dataCell.HorizontalAlign = HorizontalAlign.Center;
                                            row.Cells.Add(dataCell);
                                            ++pointIndex;
                                        }

                                        tbReports.Rows.Add(row);
                                    }
                                    //divTab.Attributes.Add("style", "width:50px");
                                }
                            }
                            else if (ds.Tables.Count == 0)
                            {
                                TableRow row = new TableRow();

                                TableCell cell = new TableCell();
                                //cell.CssClass = "tableHeader";
                                row.CssClass = "tableBody";

                                cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                                cell.CssClass = "lblError";
                                cell.HorizontalAlign = HorizontalAlign.Center;
                                cell.VerticalAlign = VerticalAlign.Middle;

                                row.Cells.Add(cell);
                                tbReports.Rows.Add(row);
                            }
                            tbReportsRow.Visible = true;
                            tbReports.Visible = true;

                        }
                        else
                        {
                            if (DrpChrtType.SelectedValue == "2")
                            {
                                Chart11.Series["Series1"].ChartType = SeriesChartType.Column;
                                Chart11.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart11.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart11.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart11.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart11.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }

                            else if (DrpChrtType.SelectedValue == "3")
                            {
                                Chart11.Series["Series1"].ChartType = SeriesChartType.Line;
                                Chart11.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart11.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart11.ChartAreas["ChartArea1"].AxisY.Title = "Conferences";
                                Chart11.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart11.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart11.Series["Series1"].MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    Chart11.Series["Series1"].MarkerSize = 3;
                                else
                                    Chart11.Series["Series1"].MarkerSize = 10;


                            }

                            Chart11.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            Chart11.BackGradientStyle = GradientStyle.TopBottom;
                            Chart11.BackColor = System.Drawing.Color.Yellow;
                            Chart11.BackSecondaryColor = System.Drawing.Color.Orange;
                            Chart11.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                            Chart11.BorderlineDashStyle = ChartDashStyle.Solid;
                            Chart11.BorderlineWidth = 1;
                            Chart11.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;

                            if (ds.Tables.Count > 0)
                            {
                                Chart11.DataSource = ds.Tables[0];


                                if (DrpChrtType.SelectedValue == "4")
                                {
                                    DataTable dt = new DataTable();
                                    if (ds.Tables.Count > 0)
                                        dt = ds.Tables[0];
                                    String lbl1 = "";
                                    Int32 series1 = 0;
                                    Chart11.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                    Chart11.Series["Series1"].ChartType = SeriesChartType.Pie;


                                    Chart11.Series["Series1"]["PieLabelStyle"] = "Inside";

                                    Chart11.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                    //for (Int32 i = 0; i < dt.Rows.Count; i++)
                                    //{

                                    //    if (dt.Rows[i]["confTypeCnt1"].ToString() != "")
                                    //        series1 = Convert.ToInt32(dt.Rows[i]["confTypeCnt1"]);

                                    //    if (series1 != 0)
                                    //        lbl1 = series1 + "%";

                                    //    string[] xValues = { lbl1 };

                                    //    int[] yValues = { series1 };
                                    //    Chart11.Series["Series1"].Points.DataBindXY(xValues, yValues);
                                    //}


                                    if (dt.Rows[0]["confTypeCnt1"].ToString() != "")
                                        series1 = Convert.ToInt32(dt.Rows[0]["confTypeCnt1"]);

                                    if (series1 != 0)
                                        lbl1 = series1 + "%";

                                    string[] xValues = { lbl1 };

                                    int[] yValues = { series1 };

                                    Chart10.Series["Series1"].Points.DataBindXY(xValues, yValues);
                                    Chart11.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 15;
                                }
                                else
                                {
                                    Chart11.Series["Series1"].XValueMember = "D";
                                    Chart11.Series["Series1"].XValueType = ChartValueType.Date;
                                    Chart11.ChartAreas["ChartArea1"].AxisX.IntervalType = DateTimeIntervalType.Auto;
                                    Chart11.Series["Series1"].YValueMembers = "confTypeCnt1";

                                    if (dFormat == "dmy")
                                    {
                                        Chart11.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "dd/MM/yyyy";

                                    }
                                    else
                                    {
                                        Chart11.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MM/dd/yyyy";
                                    }
                                }

                                if (lstConference.SelectedValue == "6")
                                    Chart11.Series["Series1"].Color = System.Drawing.Color.DarkGreen;
                                if (lstConference.SelectedValue == "2")
                                    Chart11.Series["Series1"].Color = System.Drawing.Color.Red;
                                if (lstConference.SelectedValue == "7")
                                    Chart11.Series["Series1"].Color = System.Drawing.Color.Orange;
                                if (lstConference.SelectedValue == "4")
                                    Chart11.Series["Series1"].Color = System.Drawing.Color.Blue;

                                Chart11.Series["Series1"].LegendText = lstConference.SelectedItem.Text.Replace("Only", "");
                                Chart11.AlignDataPointsByAxisLabel();
                            }

                            if (ds.Tables.Count == 0)
                            {

                                Chart11.Series["Series1"].Enabled = false;
                                Chart11.Titles.Add(new Title
                                {
                                    Text = "No Data Found",
                                    Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                                  System.Drawing.GraphicsUnit.Point),
                                    Position = Chart11.ChartAreas["ChartArea1"].Position,
                                    TextStyle = TextStyle.Default,
                                    Alignment = System.Drawing.ContentAlignment.MiddleCenter
                                });

                            }
                            Chart11Row.Visible = true;
                            Chart11.Visible = true;
                            Chart11.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                            hdnChartName.Value = "Chart11";

                        }

                    }
                    if (lstReport.SelectedValue.Equals("6"))
                    {
                        lblHeading.Text = "Custom Date - " + lstConference.SelectedItem.Text.Replace("Only", "") + "  Usage Report";

                        if (DrpChrtType.SelectedValue.Equals("1"))
                        {
                            if (ds.Tables.Count > 0)
                            {
                                foreach (DataColumn column in ds.Tables[0].Columns)
                                {
                                    if (column.ColumnName == "confTypeCnt1" || column.ColumnName == "D")
                                    {
                                        TableRow row = new TableRow();

                                        TableCell cell = new TableCell();
                                        cell.CssClass = "tableHeader";
                                        cell.Font.Bold = true;

                                        row.CssClass = "tableBody";


                                        if (column.ColumnName == "confTypeCnt1")
                                            cell.Controls.Add(new LiteralControl(lstConference.SelectedItem.Text.Replace("Only", "")));
                                        else if (column.ColumnName == "D")
                                            cell.Controls.Add(new LiteralControl("Day(s)"));

                                        row.Cells.Add(cell);

                                        int pointIndex = 0;
                                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                                        {
                                            TableCell dataCell = new TableCell();
                                            if (column.ColumnName == "D")//Added for Vertical alignment
                                                dataCell.CssClass = "verticaltextlong";
                                            //FB 1830 Start
                                            if (column.ColumnName.IndexOf("Cnt") > 0)
                                            {
                                                tmpVal = 0;
                                                decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                                colValue = tmpVal.ToString("n0", cInfo);
                                            }
                                            else
                                                colValue = dataRow[column].ToString();
                                            //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                            dataCell.Controls.Add(new LiteralControl(colValue));
                                            //FB 1830 end
                                            //dataCell.Width = new Unit(150, UnitType.Pixel);
                                            dataCell.HorizontalAlign = HorizontalAlign.Center;
                                            row.Cells.Add(dataCell);
                                            ++pointIndex;
                                        }

                                        tbReports.Rows.Add(row);
                                    }
                                }
                            }
                            else if (ds.Tables.Count == 0)
                            {
                                TableRow row = new TableRow();

                                TableCell cell = new TableCell();
                                //cell.CssClass = "tableHeader";
                                row.CssClass = "tableBody";

                                cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                                cell.CssClass = "lblError";
                                cell.HorizontalAlign = HorizontalAlign.Center;
                                cell.VerticalAlign = VerticalAlign.Middle;

                                row.Cells.Add(cell);
                                tbReports.Rows.Add(row);
                            }
                            tbReportsRow.Visible = true;
                            tbReports.Visible = true;

                        }
                        else
                        {
                            if (DrpChrtType.SelectedValue == "2")
                            {
                                Chart12.Series["Series1"].ChartType = SeriesChartType.Column;
                                Chart12.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart12.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart12.ChartAreas["ChartArea1"].AxisY.Title = "Conferences Usage";
                                Chart12.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart12.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            }

                            else if (DrpChrtType.SelectedValue == "3")
                            {
                                Chart12.Series["Series1"].ChartType = SeriesChartType.Line;
                                Chart12.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                Chart12.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                                Chart12.ChartAreas["ChartArea1"].AxisY.Title = "Conferences Usage";
                                Chart12.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart12.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                                Chart12.Series["Series1"].MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    Chart12.Series["Series1"].MarkerSize = 3;
                                else
                                    Chart12.Series["Series1"].MarkerSize = 10;

                            }

                            Chart12.BackGradientStyle = GradientStyle.TopBottom;
                            Chart12.BackColor = System.Drawing.Color.Yellow;
                            Chart12.BackSecondaryColor = System.Drawing.Color.Orange;
                            Chart12.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                            Chart12.BorderlineDashStyle = ChartDashStyle.Solid;
                            Chart12.BorderlineWidth = 1;
                            Chart12.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;

                            if (ds.Tables.Count > 0)
                            {
                                Chart12.DataSource = ds.Tables[0];


                                if (DrpChrtType.SelectedValue == "4")
                                {
                                    DataTable dt = new DataTable();
                                    if (ds.Tables.Count > 0)
                                        dt = ds.Tables[0];
                                    String lbl1 = "";
                                    Int32 series1 = 0;
                                    Chart12.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                                    Chart12.Series["Series1"].ChartType = SeriesChartType.Pie;


                                    Chart12.Series["Series1"]["PieLabelStyle"] = "Inside";

                                    Chart12.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

                                    if (dt.Rows[0]["confTypeCnt1"].ToString() != "")
                                        series1 = Convert.ToInt32(dt.Rows[0]["confTypeCnt1"]);

                                    if (series1 != 0)
                                        lbl1 = series1 + "%";

                                    string[] xValues = { lbl1 };

                                    int[] yValues = { series1 };

                                    Chart12.Series["Series1"].Points.DataBindXY(xValues, yValues);
                                    Chart12.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 15;
                                }
                                else
                                {
                                    Chart12.Series["Series1"].XValueMember = "D";
                                    Chart12.Series["Series1"].XValueType = ChartValueType.DateTime;
                                    Chart12.ChartAreas["ChartArea1"].AxisX.IntervalType = DateTimeIntervalType.Auto;
                                    Chart12.Series["Series1"].YValueMembers = "confTypeCnt1";

                                    if (dFormat == "dmy")
                                        Chart12.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "dd/MM/yy";
                                    else
                                        Chart12.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MM/dd/yy";
                                }

                                if (lstConference.SelectedValue == "6")
                                    Chart12.Series["Series1"].Color = System.Drawing.Color.DarkGreen;
                                if (lstConference.SelectedValue == "2")
                                    Chart12.Series["Series1"].Color = System.Drawing.Color.Red;
                                if (lstConference.SelectedValue == "7")
                                    Chart12.Series["Series1"].Color = System.Drawing.Color.Orange;
                                if (lstConference.SelectedValue == "4")
                                    Chart12.Series["Series1"].Color = System.Drawing.Color.Blue;

                                Chart12.Series["Series1"].LegendText = lstConference.SelectedItem.Text.Replace("Only", "");
                                Chart12.AlignDataPointsByAxisLabel();
                            }

                            if (ds.Tables.Count == 0)
                            {

                                Chart12.Series["Series1"].Enabled = false;
                                Chart12.Titles.Add(new Title
                                {
                                    Text = "No Data Found",
                                    Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                                  System.Drawing.GraphicsUnit.Point),
                                    Position = Chart12.ChartAreas["ChartArea1"].Position,
                                    TextStyle = TextStyle.Default,
                                    Alignment = System.Drawing.ContentAlignment.MiddleCenter
                                });

                            }
                            Chart12Row.Visible = true;
                            Chart12.Visible = true;
                            Chart12.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                            hdnChartName.Value = "Chart12";

                        }

                    }
                }
            }
            if (DrpAllType.SelectedValue.Equals("2") && DrpChrtType.SelectedValue != "0")
            {
                String strHeading = "";

                if (DrpLocations.SelectedValue == "1")
                    strHeading = "Location(s) Report";
                else
                    strHeading = "- Locations(" + DrpLocations.SelectedItem.Text + ") Report";

                switch (lstReport.SelectedValue)
                {
                    case "1":
                        lblHeading.Text = " Yesterday " + strHeading;
                        break;
                    case "2":
                        lblHeading.Text = "Last Week " + strHeading;
                        break;
                    case "3":
                        lblHeading.Text = "Last Month " + strHeading;
                        break;
                    case "4":
                        lblHeading.Text = "This Week " + strHeading;
                        break;
                    case "5":
                        lblHeading.Text = "Year To Date " + strHeading;
                        break;
                    case "6":
                        lblHeading.Text = "Custom Date " + strHeading;
                        break;
                }

                DataTable dt = new DataTable();

                if (DrpChrtType.SelectedValue.Equals("1"))
                {
                    #region All 
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataColumn column in ds.Tables[0].Columns)
                        {
                            if (column.ColumnName == "D" || column.ColumnName == "RoomName" || column.ColumnName == "cnt")
                            {
                                TableRow row = new TableRow();

                                TableCell cell = new TableCell();

                                cell.CssClass = "tableHeader";
                                cell.Font.Bold = true;

                                row.CssClass = "tableBody";

                                if (column.ColumnName == "D")
                                {
                                    if (lstReport.SelectedValue.Equals("5"))
                                        cell.Controls.Add(new LiteralControl("Month(s)"));
                                    else
                                        cell.Controls.Add(new LiteralControl("Day(s)"));
                                }
                                else if (column.ColumnName == "RoomName")
                                    cell.Controls.Add(new LiteralControl("Location Name"));
                                else if (column.ColumnName == "cnt")
                                    cell.Controls.Add(new LiteralControl("Location Usage"));
                                //cell.Width = new Unit(100, UnitType.Pixel);

                                row.Cells.Add(cell);

                                int pointIndex = 0;
                                foreach (DataRow dataRow in ds.Tables[0].Rows)
                                {
                                    TableCell dataCell = new TableCell();
                                    if (column.ColumnName == "D")//Added for Vertical alignment
                                        dataCell.CssClass = "verticaltextlong";
                                    //FB 1830 Start
                                    if (column.ColumnName.IndexOf("cnt") >= 0)
                                    {
                                        tmpVal = 0;
                                        decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                        colValue = tmpVal.ToString("n0", cInfo);
                                    }
                                    else
                                        colValue = dataRow[column].ToString();
                                    //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                    dataCell.Controls.Add(new LiteralControl(colValue));
                                    //FB 1830 end
                                    //dataCell.Width = new Unit(100, UnitType.Pixel);
                                    dataCell.HorizontalAlign = HorizontalAlign.Center;
                                    row.Cells.Add(dataCell);
                                    ++pointIndex;
                                }

                                tbReports.Rows.Add(row);
                            }
                        }

                    }
                    else if (ds.Tables.Count == 0)
                    {
                        TableRow row = new TableRow();

                        TableCell cell = new TableCell();
                        //cell.CssClass = "tableHeader";
                        row.CssClass = "tableBody";

                        cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                        cell.CssClass = "lblError";
                        cell.HorizontalAlign = HorizontalAlign.Center;
                        cell.VerticalAlign = VerticalAlign.Middle;

                        row.Cells.Add(cell);
                        tbReports.Rows.Add(row);
                    }
                    tbReportsRow.Visible = true;
                    tbReports.Visible = true;

                    #endregion 
                }
                else
                {
                    #region Individul Conference Type
                    if (ds.Tables.Count > 0)
                    {
                        Chart13.DataSource = ds.Tables[0];
                        dt = ds.Tables[0];
                        ////if (lstReport.SelectedValue.Equals("5") || lstReport.SelectedValue.Equals("6") || lstReport.SelectedValue.Equals("2"))
                        ////{

                        Chart13.DataBindCrossTable(ds.Tables[0].DefaultView[0].DataView, "RoomName", "D", "cnt", "");
                        //Chart13.AlignDataPointsByAxisLabel(PointSortOrder.Ascending);
                        Legend ld = new Legend("RoomName");
                        ld.Title = "Locations";
                        Chart13.Series["Series1"].Enabled = false;
                        ld.CustomItems.Add("", dt.Columns["RoomName"].ToString());

                        if (DrpChrtType.SelectedValue == "2")
                        {
                            //Chart13.Series["Series1"].ChartType = SeriesChartType.Column;
                            Chart13.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            Chart13.ChartAreas["ChartArea1"].AxisX.Title = "Locations";
                            Chart13.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            Chart13.ChartAreas["ChartArea1"].AxisY.Title = "Usage Count";
                            Chart13.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);


                            foreach (Series series in Chart13.Series)
                            {
                                series.ChartType = SeriesChartType.Column;
                                series.XValueType = ChartValueType.String;
                                series.IsXValueIndexed = true;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    series.MarkerSize = 3;
                                else
                                    series.MarkerSize = 10;

                            }


                        }

                        else if (DrpChrtType.SelectedValue == "3")
                        {
                            //Chart13.Series["Series1"].ChartType = SeriesChartType.Line;
                            Chart13.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            Chart13.ChartAreas["ChartArea1"].AxisX.Title = " Day(s)";
                            Chart13.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            Chart13.ChartAreas["ChartArea1"].AxisY.Title = "Locations Usage";
                            Chart13.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            foreach (Series series in Chart13.Series)
                            {
                                series.ChartType = SeriesChartType.Line;
                                series.XValueType = ChartValueType.String;
                                series.IsXValueIndexed = true;
                                series.MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    series.MarkerSize = 3;
                                else
                                    series.MarkerSize = 10;

                            }


                        }
                        if (DrpChrtType.SelectedValue == "4")
                        {
                            Chart13.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            Chart13.Series["Series1"].ChartType = SeriesChartType.Pie;
                        }

                        System.Globalization.DateTimeFormatInfo monthName = new System.Globalization.DateTimeFormatInfo();
                    }

                    Chart13Row.Visible = true;
                    Chart13.Visible = true;
                    if (lstReport.SelectedValue == "5")
                        Chart13.AlignDataPointsByAxisLabel();
                    else
                        Chart13.AlignDataPointsByAxisLabel(PointSortOrder.Ascending);
                    Chart13.BackGradientStyle = GradientStyle.TopBottom;
                    Chart13.BackColor = System.Drawing.Color.Yellow;
                    Chart13.BackSecondaryColor = System.Drawing.Color.Orange;
                    Chart13.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                    Chart13.BorderlineDashStyle = ChartDashStyle.Solid;
                    Chart13.BorderlineWidth = 1;
                    Chart13.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;


                    //Chart13.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new System.Drawing.Font("Arial", 5.15F, System.Drawing.FontStyle.Bold);

                    Chart13.ChartAreas["ChartArea1"].AxisX.IsLabelAutoFit = false;
                    Chart13.ChartAreas["ChartArea1"].AxisX.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont | LabelAutoFitStyles.IncreaseFont | LabelAutoFitStyles.WordWrap | LabelAutoFitStyles.StaggeredLabels | LabelAutoFitStyles.LabelsAngleStep30;

                    System.Globalization.DateTimeFormatInfo df = new System.Globalization.DateTimeFormatInfo();
                    //Chart13.Titles["Location"].Text = "Location Usage Report(" + df.GetMonthName(Convert.ToInt32(lstMonth.SelectedValue)) + "-" + lstYear.SelectedValue + ")";
                    if (ds.Tables.Count == 0)
                    {
                        Chart13.Series["Series1"].Enabled = false;
                        Chart13.Titles.Add(new Title
                        {
                            Text = "No Data Found",
                            Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                          System.Drawing.GraphicsUnit.Point),
                            Position = Chart13.ChartAreas["ChartArea1"].Position,
                            TextStyle = TextStyle.Default,
                            Alignment = System.Drawing.ContentAlignment.MiddleCenter
                        });
                    }
                    Chart13.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                    hdnChartName.Value = "Chart13";

                   
                    #endregion
                }
            }
            if (DrpAllType.SelectedValue.Equals("3") && DrpChrtType.SelectedValue != "0")
            {
                if (lstReport.SelectedValue.Equals("1"))
                {
                    if (DrpMCU.SelectedValue == "1")
                        lblHeading.Text = " Yesterday - MCU(" + DrpMCU.SelectedItem.Text + ") Report";
                    else
                    {
                        if (lstBridges.SelectedValue != "-1")
                            lblHeading.Text = " Yesterday - MCU(" + lstBridges.SelectedItem.Text + ") Report";
                    }
                }
                if (lstReport.SelectedValue.Equals("2"))
                {
                    if (DrpMCU.SelectedValue == "1")
                        lblHeading.Text = "Last Week - MCU(" + DrpMCU.SelectedItem.Text + ") Report";
                    else
                    {
                        if (lstBridges.SelectedValue != "-1")
                            lblHeading.Text = "Last Week - MCU(" + lstBridges.SelectedItem.Text + ") Report";
                    }
                }
                if (lstReport.SelectedValue.Equals("3"))
                {
                    if (DrpMCU.SelectedValue == "1")
                        lblHeading.Text = "Last Month - MCU(" + DrpMCU.SelectedItem.Text + ") Report";
                    else
                    {
                        if (lstBridges.SelectedValue != "-1")
                            lblHeading.Text = "Last Month - MCU(" + lstBridges.SelectedItem.Text + ") Report";
                    }

                }
                if (lstReport.SelectedValue.Equals("4"))
                {
                    if (DrpMCU.SelectedValue == "1")
                        lblHeading.Text = "This Week - MCU(" + DrpMCU.SelectedItem.Text + ") Report";
                    else
                    {
                        if (lstBridges.SelectedValue != "-1")
                            lblHeading.Text = "This Week - MCU(" + lstBridges.SelectedItem.Text + ") Report";
                    }
                }
                if (lstReport.SelectedValue.Equals("5"))
                {
                    if (DrpMCU.SelectedValue == "1")
                        lblHeading.Text = "Year To Date - MCU(" + DrpMCU.SelectedItem.Text + ") Report";
                    else
                    {
                        if (lstBridges.SelectedValue != "-1")
                            lblHeading.Text = "Year To Date - MCU(" + lstBridges.SelectedItem.Text + ") Report";
                    }
                }
                if (lstReport.SelectedValue.Equals("6"))
                {
                    if (DrpMCU.SelectedValue == "1")
                        lblHeading.Text = "Custom Date - MCU(" + DrpMCU.SelectedItem.Text + ") Report";
                    else
                    {
                        if (lstBridges.SelectedValue != "-1")
                            lblHeading.Text = "Custom Date - MCU(" + lstBridges.SelectedItem.Text + ") Report";
                    }
                }

                DataTable dt = new DataTable();


                if (DrpChrtType.SelectedValue.Equals("1"))
                {
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataColumn column in ds.Tables[0].Columns)
                        {
                            if (column.ColumnName.ToUpper() == "M2")
                                continue;

                            TableRow row = new TableRow();

                            TableCell cell = new TableCell();

                            cell.CssClass = "tableHeader";
                            cell.Font.Bold = true;

                            row.CssClass = "tableBody";

                            if (column.ColumnName == "BridgeName")
                                cell.Controls.Add(new LiteralControl("MCU Name"));
                            //if (column.ColumnName == "videoprotocoltype")
                            //    cell.Controls.Add(new LiteralControl("Protocol Name"));
                            else if (column.ColumnName == "mcucount")
                                cell.Controls.Add(new LiteralControl("MCU Usage"));
                            else if (column.ColumnName == "d1")
                            {
                                if (lstReport.SelectedValue.Equals("5"))
                                    cell.Controls.Add(new LiteralControl("Month(s)"));
                                else
                                    cell.Controls.Add(new LiteralControl("Day(s)"));
                            }
                            else if (column.ColumnName == "videoprotocoltype")
                                cell.Controls.Add(new LiteralControl("Protocol Type"));
                            //cell.Width = new Unit(100, UnitType.Pixel);

                            row.Cells.Add(cell);

                            int pointIndex = 0;
                            foreach (DataRow dataRow in ds.Tables[0].Rows)
                            {
                                if (dataRow[column].ToString().ToUpper() == "M2")
                                    continue;
                                TableCell dataCell = new TableCell();
                                if (column.ColumnName == "d1")//Added for Vertical alignment
                                    dataCell.CssClass = "verticaltextlong";
                                //FB 1830 Start
                                if (column.ColumnName.IndexOf("count") > 0)
                                {
                                    tmpVal = 0;
                                    decimal.TryParse(dataRow[column].ToString(), out tmpVal);
                                    colValue = tmpVal.ToString("n0", cInfo);
                                }
                                else
                                    colValue = dataRow[column].ToString();
                                //dataCell.Controls.Add(new LiteralControl(dataRow[column].ToString()));
                                dataCell.Controls.Add(new LiteralControl(colValue));
                                //FB 1830 end
                                //dataCell.Width = new Unit(100, UnitType.Pixel);
                                dataCell.HorizontalAlign = HorizontalAlign.Center;
                                row.Cells.Add(dataCell);
                                ++pointIndex;
                            }

                            tbReports.Rows.Add(row);
                        }
                    }
                    else if (ds.Tables.Count == 0)
                    {
                        TableRow row = new TableRow();

                        TableCell cell = new TableCell();
                        //cell.CssClass = "tableHeader";
                        row.CssClass = "tableBody";

                        cell.Text = obj.GetTranslatedText("No Data Found");//FB 1830 - Translation
                        cell.CssClass = "lblError";
                        cell.HorizontalAlign = HorizontalAlign.Center;
                        cell.VerticalAlign = VerticalAlign.Middle;

                        row.Cells.Add(cell);
                        tbReports.Rows.Add(row);
                    }

                    tbReportsRow.Visible = true;
                    tbReports.Visible = true;

                }
                else
                {
                    if (ds.Tables.Count > 0)
                    {
                        Chart19.DataSource = ds.Tables[0];
                        dt = ds.Tables[0];

                        if (DrpMCU.SelectedValue == "1")
                            Chart19.DataBindCrossTable(ds.Tables[0].DefaultView[0].DataView, "BridgeName", "d1", "mcucount", "");
                        else
                            Chart19.DataBindCrossTable(ds.Tables[0].DefaultView[0].DataView, "videoprotocoltype", "d1", "mcucount", "");

                        //Chart19.AlignDataPointsByAxisLabel(PointSortOrder.Ascending);
                        

                        Legend ld = new Legend("BridgeName");
                        ld.Title = "BridgeName";
                        Chart19.Series["Series1"].Enabled = false;
                        if (DrpMCU.SelectedValue == "1")
                            ld.CustomItems.Add("", dt.Columns["BridgeName"].ToString());
                        else
                            ld.CustomItems.Add("", dt.Columns["videoprotocoltype"].ToString());


                        if (DrpChrtType.SelectedValue == "2")
                        {
                            Chart19.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            if (lstReport.SelectedValue == "3" || lstReport.SelectedValue == "5")
                                Chart19.ChartAreas["ChartArea1"].AxisX.Title = "Month";
                            else
                                Chart19.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                            Chart19.ChartAreas["ChartArea1"].AxisY.Title = "MCU Usage";
                            Chart19.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            Chart19.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

                            foreach (Series series in Chart19.Series)
                            {
                                series.ChartType = SeriesChartType.Column;
                                series.XValueType = ChartValueType.String;
                                series.IsXValueIndexed = true;
                                //series.Color = System.Drawing.Color.YellowGreen;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    series.MarkerSize = 3;
                                else
                                    series.MarkerSize = 10;
                                //Chart19.AlignDataPointsByAxisLabel(series.Name);

                            }
                        }

                        else if (DrpChrtType.SelectedValue == "3")
                        {
                            Chart19.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            if (lstReport.SelectedValue == "3" || lstReport.SelectedValue == "5")
                                Chart19.ChartAreas["ChartArea1"].AxisX.Title = "Month";
                            else
                                Chart19.ChartAreas["ChartArea1"].AxisX.Title = "Day(s)";
                            Chart19.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            Chart19.ChartAreas["ChartArea1"].AxisY.Title = "MCU Usage";
                            Chart19.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);
                            foreach (Series series in Chart19.Series)
                            {
                                series.ChartType = SeriesChartType.Line;
                                series.XValueType = ChartValueType.String;
                                series.IsXValueIndexed = true;
                                series.MarkerStyle = MarkerStyle.Diamond;
                                if (lstReport.SelectedValue == "5" || lstReport.SelectedValue == "6")
                                    series.MarkerSize = 3;
                                else
                                    series.MarkerSize = 10;
                                //Chart19.AlignDataPointsByAxisLabel(series.Name);
                            }
                        }
                        if (DrpChrtType.SelectedValue == "4")
                        {
                            Chart19.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;
                            Chart19.Series["Series1"].ChartType = SeriesChartType.Pie;
                        }

                        System.Globalization.DateTimeFormatInfo monthName = new System.Globalization.DateTimeFormatInfo();
                    }
                    Chart19Row.Visible = true;
                    Chart19.Visible = true;
                    if(lstReport.SelectedValue == "5")
                        Chart19.AlignDataPointsByAxisLabel();
                    else
                        Chart19.AlignDataPointsByAxisLabel(PointSortOrder.Ascending);

                    Chart19.BackGradientStyle = GradientStyle.TopBottom;
                    Chart19.BackColor = System.Drawing.Color.Yellow;
                    Chart19.BackSecondaryColor = System.Drawing.Color.Orange;
                    Chart19.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(59)))), ((int)(((byte)(105)))));
                    Chart19.BorderlineDashStyle = ChartDashStyle.Solid;
                    Chart19.BorderlineWidth = 1;
                    Chart19.BorderSkin.SkinStyle = BorderSkinStyle.Sunken;


                    //Chart19.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new System.Drawing.Font("Arial", 5.15F, System.Drawing.FontStyle.Bold);

                    Chart19.ChartAreas["ChartArea1"].AxisX.IsLabelAutoFit = false;
                    Chart19.ChartAreas["ChartArea1"].AxisX.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont | LabelAutoFitStyles.IncreaseFont | LabelAutoFitStyles.WordWrap | LabelAutoFitStyles.StaggeredLabels | LabelAutoFitStyles.LabelsAngleStep30;

                    System.Globalization.DateTimeFormatInfo df = new System.Globalization.DateTimeFormatInfo();

                    if (ds.Tables.Count == 0)
                    {
                        Chart19.Series["Series1"].Enabled = false;
                        Chart19.Titles.Add(new Title
                        {
                            Text = "No Data Found",
                            Font = new System.Drawing.Font("Microsoft Calisto MT", 10,

                                                          System.Drawing.GraphicsUnit.Point),
                            Position = Chart19.ChartAreas["ChartArea1"].Position,
                            TextStyle = TextStyle.Default,
                            Alignment = System.Drawing.ContentAlignment.MiddleCenter
                        });

                    }
                    Chart19.SaveImage(savepath, ChartImageFormat.Jpeg);//Graphical Reports
                    hdnChartName.Value = "Chart19";

                }

            }

        }
        catch (Exception ex)
        {
            log.Trace("Bind Chart: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    # region GetLocations
    /*
    protected void GetLocations(String RoomID)
    {
        try
        {
            obj = new myVRMNet.NETFunctions();
            String inXML = "<GetLocationsList><userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></GetLocationsList>";

            String outXML = obj.CallMyVRMServer("GetLocationsList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            XmlNodeList nodes = xmldoc.SelectNodes("//level3List/level3");
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("roomID");
            DataColumn dc2 = new DataColumn("roomName");
            dc1.DataType = System.Type.GetType("System.Int32");
            dc2.DataType = System.Type.GetType("System.String");
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            DataRow dr = null;
            if (nodes.Count > 0)
            {
                foreach (XmlNode node3 in nodes)
                {

                    if (node3.SelectNodes("level2List/level2/level1List/level1").Count > 0)
                    {

                        XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");

                        foreach (XmlNode node2 in nodes2)
                        {

                            if (node2.SelectNodes("level1List/level1").Count > 0)
                            {
                                XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                foreach (XmlNode node1 in nodes1)
                                {
                                    dr = dt.NewRow();
                                    dr[dc1] = node1.SelectSingleNode("level1ID").InnerText;
                                    if (defaultRoomID == "")
                                        defaultRoomID = node1.SelectSingleNode("level1ID").InnerText;
                                    else
                                        defaultRoomID = defaultRoomID + "," + node1.SelectSingleNode("level1ID").InnerText;
                                    dr[dc2] = node1.SelectSingleNode("level1Name").InnerText;
                                    dt.Rows.Add(dr);
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("roomID");
                dt.Columns.Add("roomName");
                dr = dt.NewRow();
                dr[0] = "-1";
                dr[1] = "No Items...";
                dt.Rows.InsertAt(dr, 0);
            }
            cblRoom.DataValueField = "roomID";
            cblRoom.DataTextField = "roomName";
            cblRoom.DataSource = dt;
            cblRoom.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
     * */
    # endregion

    #region BindBridges
    /// <summary>
    /// BindBridges
    /// </summary>
    /// <param name="sender"></param>

    public void BindBridges(DropDownList sender)
    {
        log = new ns_Logger.Logger();
        try
        {
            String inXML = "<GetBridges>" + obj.OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetBridges>";//Organization Module Fixes
            String outXML = obj.CallMyVRMServer("GetBridges", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            //HttpContext.Current.Response.Write(Transfer(inXML) + "<hr>" + Transfer(outXML));
            //String outXML = "<Bridges><Bridge><BridgeID>1</BridgeID><BridgeName>Expedite</BridgeName></Bridge><Bridge><BridgeID>2</BridgeID><BridgeName>ExpediteV</BridgeName></Bridge><Bridge><BridgeID>3</BridgeID><BridgeName>Umiami</BridgeName></Bridge></Bridges>";
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            XmlNodeList nodes = xmldoc.SelectNodes("//Bridges/Bridge");
            LoadList(sender, nodes, "BridgeID", "BridgeName");
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region LoadList
    /// <summary>
    /// LoadList
    /// </summary>
    /// <param name="lstList"></param>
    /// <param name="nodes"></param>
    /// <param name="col1"></param>
    /// <param name="col2"></param>
    public void LoadList(DropDownList lstList, XmlNodeList nodes, String col1, String col2)
    {
        try
        {
            XmlTextReader xtr;
            DataSet ds = new DataSet();

            foreach (XmlNode node in nodes)
            {
                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                ds.ReadXml(xtr, XmlReadMode.InferSchema);
            }
            DataView dv;
            DataTable dt;

            if (ds.Tables.Count > 0)
            {
                dv = new DataView(ds.Tables[0]);
                dt = dv.Table;
            }
            else
            {
                dv = new DataView();
                dt = new DataTable();
                dt.Columns.Add(col1);
                dt.Columns.Add(col2);
                DataRow dr = dt.NewRow();
                dr[0] = "-1";
                dr[1] = "No Items...";
                dt.Rows.InsertAt(dr, 0);
            }
            lstList.DataSource = dt;
            lstList.DataBind();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
        }

    }
    #endregion

    #region Month and Year
    /// <summary>
    /// Checking Leap Year and getting Month of Days count
    /// </summary>
    /*
    public void GetMonYr()
    {
        Boolean leapyear = false;
        if ((lstMonth.SelectedValue == "2") && (leapyear == false))
        {

            if ((Convert.ToInt32(lstYear.SelectedValue) % 100) == 0)
            {
                if ((Convert.ToInt32(lstYear.SelectedValue) % 400) == 0)
                    leapyear = true;
            }
            else if ((Convert.ToInt32(lstYear.SelectedValue) % 4) == 0)
            {
                leapyear = true;
            }
        }

        days = new Int32[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        if ((lstMonth.SelectedValue == "2") && (leapyear == true))
        {
            days[1] = 29;
        }
        else
            days[1] = 28;
        

    }
    */

    #endregion

    #region Button Event Handlers Method

    private void btnviewRep_Click(object sender, EventArgs e)
    {
        try
        {
            String rptType = ReportsList.SelectedValue;

            HideRows();

            if (rptType == "4")
            {
                trDateDisplay.Attributes.Add("style", "display:None");
                viewReport();
                GetQueries();

                if (DrpChrtType.SelectedValue == "1")
                {
                    hdnValue.Value = "T";
                    Session["reportType"] = hdnValue.Value;
                    Session["titleString"] = lblHeading.Text;
                    //Session["PrintAspTable"] = tbReports;
                    Session.Add("PrintAspTable", tbReports);
                }
            }
            else
            {
                CalendarGrid.AllowPaging = true;

                BindData();

                if (hdnValue.Value != "CL")
                    trDateDisplay.Attributes.Add("style", "display:Block");
                else
                    trDateDisplay.Attributes.Add("style", "display:None");

            }
        }
        catch (Exception ex)
        {
            log.Trace("Button View Report:" + ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    #region GetQueries Method

    private void GetQueries()
    {
        String InXML = "";
        log = new ns_Logger.Logger();
        ns_SqlHelper.SqlHelper ss = new ns_SqlHelper.SqlHelper(path);
        XmlDocument xmlDstDoc = new XmlDocument();
        DataSet ds = new DataSet();
        try
        {
            obj = new myVRMNet.NETFunctions();
            ss.OpenConnection();

            String strSQL = "";
            String outXML = "";


            //Code for 'Last Week' START
            dtStart = DateTime.Today;
            day = dtStart.DayOfWeek;
            int days = day - DayOfWeek.Sunday;
            startDate = dtStart.AddDays(-days);
            LastWkEndDate = startDate.AddDays(-1);
            LastWkStrtDate = LastWkEndDate.AddDays(-6);
            //Code for 'Last Week' End

            //Code  for 'Last Month' Start
            monthNum = (dtStart.Month) - 1;
            //Code  for 'Last Month' End

            //Code for 'This Week' START
            dtStart = DateTime.Today;
            day = dtStart.DayOfWeek;
            int daysThsWk = day - DayOfWeek.Sunday;
            ThsWkStrtDate = dtStart.AddDays(-daysThsWk);
            ThsWkEndDate = startDate.AddDays(6);
            //Code for 'This Week' End


            //Code for 'Year to Date' START
            String sYearToDate = "01/01/";
            YearToDate = Convert.ToDateTime(sYearToDate + dtStart.Year);
            dtStart = DateTime.Today;
            //Code for 'Year to Date' End


            //Code for 'Custom Date' START
            if (txtCusDateFrm.Text != "")
                CusDateFrm = txtCusDateFrm.Text;
            if (txtCusDateTo.Text != "")
                CusDateTo = txtCusDateTo.Text;
            //Code for 'Custom Date' End

            switch (report)
            {
                case "1":
                    {
                        if (lstConference.SelectedValue == "1") //For All Conferences
                        {
                            if (lstReport.SelectedValue == "1")
                            {

                                InXML = "<GetTotalUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday>" + DateTime.Now.AddDays(-1).Date.ToString(dtInMonFormat) + "</Yesterday>";

                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetTotalUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetTotalUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);
                            }
                            if (lstReport.SelectedValue == "2")
                            {

                                InXML = "<GetTotalUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom>" + LastWkStrtDate.ToShortDateString() + "</DateFrom>";
                                InXML += "      <DateTo>" + LastWkEndDate.ToShortDateString() + "</DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetTotalUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetTotalUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }

                            if (lstReport.SelectedValue == "3")
                            {

                                InXML = "<GetTotalUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth> " + monthNum + "</LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetTotalUsageMonth>";

                                outXML = obj.CallMyVRMServer("GetTotalUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                            if (lstReport.SelectedValue == "4")
                            {

                                InXML = "<GetTotalUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom>" + ThsWkStrtDate.ToShortDateString() + "</DateFrom>";
                                InXML += "      <DateTo>" + ThsWkEndDate.ToShortDateString() + "</DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetTotalUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetTotalUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                            if (lstReport.SelectedValue == "5")
                            {

                                InXML = "<GetTotalUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom>" + YearToDate.ToShortDateString() + "</DateFrom>";
                                InXML += "      <DateTo>" + dtStart.ToShortDateString() + "</DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetTotalUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetTotalUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                            if (lstReport.SelectedValue == "6")
                            {

                                InXML = "<GetTotalUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                if (txtCusDateFrm.Text != "")
                                    InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateFrm) + "</DateFrom>";
                                if (txtCusDateTo.Text != "")
                                    InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateTo) + "</DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetTotalUsageMonth>";


                                outXML = obj.CallMyVRMServer("GetTotalUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                        }
                        else //For Audio/Video/Point -To Point Conferences START
                        {
                            if (lstReport.SelectedValue == "1")
                            {

                                InXML = "<GetSpecificUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday>" + DateTime.Now.AddDays(-1).Date.ToString(dtInMonFormat) + "</Yesterday>";

                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <ConferenceType>" + lstConference.SelectedValue + "</ConferenceType>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetSpecificUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetSpecificUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);
                            }
                            if (lstReport.SelectedValue == "2")
                            {
                                InXML = "<GetSpecificUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom>" + LastWkStrtDate.ToShortDateString() + "</DateFrom>";
                                InXML += "      <DateTo>" + LastWkEndDate.ToShortDateString() + "</DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <ConferenceType>" + lstConference.SelectedValue + "</ConferenceType>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetSpecificUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetSpecificUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }

                            if (lstReport.SelectedValue == "3")
                            {

                                InXML = "<GetSpecificUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth> " + monthNum + "</LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <ConferenceType>" + lstConference.SelectedValue + "</ConferenceType>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetSpecificUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetSpecificUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                            if (lstReport.SelectedValue == "4")
                            {

                                InXML = "<GetSpecificUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom>" + ThsWkStrtDate.ToShortDateString() + "</DateFrom>";
                                InXML += "      <DateTo>" + ThsWkEndDate.ToShortDateString() + "</DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <ConferenceType>" + lstConference.SelectedValue + "</ConferenceType>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetSpecificUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetSpecificUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                            if (lstReport.SelectedValue == "5")
                            {

                                InXML = "<GetSpecificUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom>" + YearToDate.ToShortDateString() + "</DateFrom>";
                                InXML += "      <DateTo>" + dtStart.ToShortDateString() + "</DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <CustomDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <ConferenceType>" + lstConference.SelectedValue + "</ConferenceType>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetSpecificUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetSpecificUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                            if (lstReport.SelectedValue == "6")
                            {

                                InXML = "<GetSpecificUsageMonth>";
                                InXML += obj.OrgXMLElement();//Organization Module Fixes
                                InXML += "  <Yesterday></Yesterday>";
                                InXML += "  <LastWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </LastWeek>";
                                InXML += "  <LastMonth></LastMonth>";
                                InXML += "  <ThisWeek>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </ThisWeek>";
                                InXML += "  <YearToDate>";
                                InXML += "      <DateFrom></DateFrom>";
                                InXML += "      <DateTo></DateTo>";
                                InXML += "  </YearToDate>";
                                InXML += "  <ConferenceType>" + lstConference.SelectedValue + "</ConferenceType>";
                                InXML += "  <CustomDate>";
                                if (txtCusDateFrm.Text != "")
                                    InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateFrm) + "</DateFrom>";
                                if (txtCusDateTo.Text != "")
                                    InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateTo) + "</DateTo>";
                                InXML += "  </CustomDate>";
                                InXML += "  <DateFormat>" + format + "</DateFormat>";
                                InXML += "  <UserID>" + usrID + "</UserID>";
                                InXML += "</GetSpecificUsageMonth>";



                                outXML = obj.CallMyVRMServer("GetSpecificUsageMonth", InXML, path);
                                xmlDstDoc.LoadXml(outXML);
                                ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                BindChart(ds);

                            }
                        } //For Audio/Video/Point -To Point Conferences START

                        break;
                    }
                case "2":
                    {
                        if (DrpAllType.SelectedValue.Equals("2"))//For All Locations
                        {
                            if (DrpLocations.SelectedValue.Equals("1"))
                            {
                                // Added for Location Filter
                                GetRoomIDs();
                                String roomList = roomIds;

                                if (lstReport.SelectedValue.Equals("1"))
                                {
                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday>" + DateTime.Today.AddDays(-1).ToShortDateString() + "</Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "<roomslist>" + roomList + "</roomslist>"; // Added for Location Filter
                                    InXML += "</GetLocationsUsage>";

                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);
                                }
                                if (lstReport.SelectedValue == "2")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom>" + LastWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + LastWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "<roomslist>" + roomList + "</roomslist>"; // Added for Location Filter
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }

                                if (lstReport.SelectedValue == "3")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth> " + monthNum + "</LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "<roomslist>" + roomList + "</roomslist>"; // Added for Location Filter
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "4")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom>" + ThsWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + ThsWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "<roomslist>" + roomList + "</roomslist>"; // Added for Location Filter
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "5")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom>" + YearToDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + dtStart.ToShortDateString() + "</DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "<roomslist>" + roomList + "</roomslist>"; // Added for Location Filter
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "6")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    if (txtCusDateFrm.Text != "")
                                        InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateFrm) + "</DateFrom>";
                                    if (txtCusDateTo.Text != "")
                                        InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateTo) + "</DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "<roomslist>" + roomList + "</roomslist>"; // Added for Location Filter
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                            }
                            else if (DrpLocations.SelectedValue.Equals("2")) //For Country/State
                            {
                                if (lstReport.SelectedValue.Equals("1"))
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday>" + DateTime.Today.AddDays(-1).ToShortDateString() + "</Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country>" + lstCountries.SelectedValue + "</Country>";
                                    if (lstStates.SelectedValue != "-1")
                                        InXML += "  <State>" + lstStates.SelectedValue + "</State>";
                                    else
                                        InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);
                                }
                                if (lstReport.SelectedValue == "2")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom>" + LastWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + LastWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country>" + lstCountries.SelectedValue + "</Country>";
                                    if (lstStates.SelectedValue != "-1")
                                        InXML += "  <State>" + lstStates.SelectedValue + "</State>";
                                    else
                                        InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }

                                if (lstReport.SelectedValue == "3")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth> " + monthNum + "</LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country>" + lstCountries.SelectedValue + "</Country>";
                                    if (lstStates.SelectedValue != "-1")
                                        InXML += "  <State>" + lstStates.SelectedValue + "</State>";
                                    else
                                        InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "4")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom>" + ThsWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + ThsWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country>" + lstCountries.SelectedValue + "</Country>";
                                    if (lstStates.SelectedValue != "-1")
                                        InXML += "  <State>" + lstStates.SelectedValue + "</State>";
                                    else
                                        InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "5")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom>" + YearToDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + dtStart.ToShortDateString() + "</DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country>" + lstCountries.SelectedValue + "</Country>";
                                    if (lstStates.SelectedValue != "-1")
                                        InXML += "  <State>" + lstStates.SelectedValue + "</State>";
                                    else
                                        InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "6")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    if (txtCusDateFrm.Text != "")
                                        InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateFrm) + "</DateFrom>";
                                    if (txtCusDateTo.Text != "")
                                        InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateTo) + "</DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country>" + lstCountries.SelectedValue + "</Country>";
                                    if (lstStates.SelectedValue != "-1")
                                        InXML += "  <State>" + lstStates.SelectedValue + "</State>";
                                    else
                                        InXML += "  <State></State>";
                                    InXML += "  <Zipcode></Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";

                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                            }
                            else if (DrpLocations.SelectedValue.Equals("3"))
                            {
                                if (lstReport.SelectedValue.Equals("1"))
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday>" + DateTime.Today.AddDays(-1).ToShortDateString() + "</Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode>" + txtZipCode.Text + "</Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);
                                }
                                if (lstReport.SelectedValue == "2")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom>" + LastWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + LastWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode>" + txtZipCode.Text + "</Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }

                                if (lstReport.SelectedValue == "3")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth> " + monthNum + "</LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode>" + txtZipCode.Text + "</Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "4")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom>" + ThsWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + ThsWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode>" + txtZipCode.Text + "</Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "5")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom>" + YearToDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + dtStart.ToShortDateString() + "</DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode>" + txtZipCode.Text + "</Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "6")
                                {

                                    InXML = "<GetLocationsUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    if (txtCusDateFrm.Text != "")
                                        InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateFrm) + "</DateFrom>";
                                    if (txtCusDateTo.Text != "")
                                        InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateTo) + "</DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <Country></Country>";
                                    InXML += "  <State></State>";
                                    InXML += "  <Zipcode>" + txtZipCode.Text + "</Zipcode>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "</GetLocationsUsage>";



                                    outXML = obj.CallMyVRMServer("GetLocationsUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                            }
                        }

                        break;

                    }
                case "3":
                    {
                        if (DrpAllType.SelectedValue.Equals("3"))
                        {
                            if (DrpMCU.SelectedValue.Equals("1"))
                            {
                                if (lstReport.SelectedValue == "1")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday>" + DateTime.Now.AddDays(-1).Date.ToString(dtInMonFormat) + "</Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID></BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);
                                }
                                if (lstReport.SelectedValue == "2")
                                {
                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom>" + LastWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + LastWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID></BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }

                                if (lstReport.SelectedValue == "3")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth> " + monthNum + "</LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID></BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "4")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom>" + ThsWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + ThsWkEndDate.ToShortDateString() + "</DateTo>";
                                    //InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(ThsWkStrtDate.ToShortDateString()) + "</DateFrom>";
                                    //InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(ThsWkEndDate.ToShortDateString()) + "</DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID></BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "5")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom>" + YearToDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + dtStart.ToShortDateString() + "</DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID></BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "6")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    if (txtCusDateFrm.Text != "")
                                        InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateFrm) + "</DateFrom>";
                                    if (txtCusDateTo.Text != "")
                                        InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateTo) + "</DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID></BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }

                            }
                            else if (DrpMCU.SelectedValue.Equals("2"))
                            {
                                if (lstReport.SelectedValue == "1")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday>" + DateTime.Now.AddDays(-1).Date.ToString(dtInMonFormat) + "</Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID> " + lstBridges.SelectedValue + "</BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);
                                }
                                if (lstReport.SelectedValue == "2")
                                {
                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom>" + LastWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + LastWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID> " + lstBridges.SelectedValue + "</BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }

                                if (lstReport.SelectedValue == "3")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth> " + monthNum + "</LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID> " + lstBridges.SelectedValue + "</BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "4")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom>" + ThsWkStrtDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + ThsWkEndDate.ToShortDateString() + "</DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID> " + lstBridges.SelectedValue + "</BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "5")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom>" + YearToDate.ToShortDateString() + "</DateFrom>";
                                    InXML += "      <DateTo>" + dtStart.ToShortDateString() + "</DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID> " + lstBridges.SelectedValue + "</BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                                if (lstReport.SelectedValue == "6")
                                {

                                    InXML = "<GetMCUUsage>";
                                    InXML += obj.OrgXMLElement();//Organization Module Fixes
                                    InXML += "  <Yesterday></Yesterday>";
                                    InXML += "  <LastWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </LastWeek>";
                                    InXML += "  <LastMonth></LastMonth>";
                                    InXML += "  <ThisWeek>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </ThisWeek>";
                                    InXML += "  <YearToDate>";
                                    InXML += "      <DateFrom></DateFrom>";
                                    InXML += "      <DateTo></DateTo>";
                                    InXML += "  </YearToDate>";
                                    InXML += "  <CustomDate>";
                                    if (txtCusDateFrm.Text != "")
                                        InXML += "      <DateFrom>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateFrm) + "</DateFrom>";
                                    if (txtCusDateTo.Text != "")
                                        InXML += "      <DateTo>" + myVRMNet.NETFunctions.GetDefaultDate(CusDateTo) + "</DateTo>";
                                    InXML += "  </CustomDate>";
                                    InXML += "  <DateFormat>" + format + "</DateFormat>";
                                    InXML += "  <UserID>" + usrID + "</UserID>";
                                    InXML += "  <BridgeID> " + lstBridges.SelectedValue + "</BridgeID>";
                                    InXML += "</GetMCUUsage>";



                                    outXML = obj.CallMyVRMServer("GetMCUUsage", InXML, path);
                                    xmlDstDoc.LoadXml(outXML);
                                    ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                                    BindChart(ds);

                                }
                            }

                        }
                        break;
                    }

            }
            if (Application["Client"].ToString().ToUpper() == "MOJ")
                lblHeading.Text = lblHeading.Text.Replace("Conference", "Hearing");

        }
        catch (Exception ex)
        {
            log.Trace("Error Occured in Binding Query - " + ex.Message);
        }
        finally
        {
            ss.CloseConnection();
            ss = null;
        }
    }

    #endregion

    #region ChartDisplay

    private void ChartDisplay()
    {
        log = new ns_Logger.Logger();

        try
        {
            Chart1Row.Visible = false;
            Chart7Row.Visible = false;
            Chart8Row.Visible = false;
            Chart9Row.Visible = false;
            Chart10Row.Visible = false;
            Chart11Row.Visible = false;
            Chart12Row.Visible = false;
            Chart13Row.Visible = false;
            Chart19Row.Visible = false;
            tbReportsRow.Visible = false;

        }
        catch (Exception ex)
        {
            log.Trace("Error Occured in Chart Display - " + ex.Message);
        }
    }

    #endregion

    //SQL Report Start

    #region DailyScheduleGrid ItemCreated Event Handler

    void DailyScheduleGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (!isItemCreated)
        {
            if (e.Item.ItemType == ListItemType.Footer && DailyScheduleGrid.Items.Count == 0)
            {
                DailyScheduleGrid.ShowFooter = true;
                Label label = new Label();
                label.Text = obj.GetTranslatedText("No Records Found.");//FB 1830 - Translation
                label.CssClass = "lblError";
                e.Item.Cells.Clear();
                TableCell cell = new TableCell();
                cell.ColumnSpan = DailyScheduleGrid.Columns.Count;
                cell.Controls.Add(label);
                cell.HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells.Add(cell);
            }
        }

    }
    #endregion

    #region DailyScheduleGrid ItemDataBound Event Handler

    void DailyScheduleGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                if (row != null)
                {
                    e.Item.Cells[0].Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startDate"]).ToShortDateString());
                    e.Item.Cells[1].Text = Convert.ToDateTime(row["StartDate"]).ToString(tformat);
                    e.Item.Cells[2].Text = Convert.ToDateTime(row["endDate"]).ToString(tformat);
                    e.Item.Cells[3].Text = row["Room"].ToString();
                    e.Item.Cells[4].Text = row["title"].ToString();
                    e.Item.Cells[5].Text = row["lastname"].ToString();
                    e.Item.Cells[6].Text = row["confNum"].ToString();
                    e.Item.Cells[7].Text = row["Remarks"].ToString();

                    if (hdnConfNum.Value != row["confNum"].ToString())
                        isFillColor = false;

                    if (row["type"].ToString() == "Y" && hdnConfNum.Value != row["confNum"].ToString())
                    {
                        if (hdnRowColor.Value == "" || hdnRowColor.Value == "C")
                            hdnRowColor.Value = "Y";
                        else
                            hdnRowColor.Value = "C";

                        isFillColor = true;
                    }

                    if (isFillColor)
                    {
                        if (hdnRowColor.Value == "Y")
                            e.Item.BackColor = Color.Yellow;
                        else
                            e.Item.BackColor = Color.Cyan;
                    }

                    isItemCreated = true;
                    hdnConfNum.Value = row["confNum"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region DailyScheduleGrid PageIndexChanged Event Handler

    void DailyScheduleGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            DailyScheduleGrid.CurrentPageIndex = e.NewPageIndex;
            if (report == "")
                report = "DS";
            BindData();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region ContactListGrid ItemCreated Event Handler

    void ContactListGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (!isItemCreated)
        {
            if (e.Item.ItemType == ListItemType.Footer && ContactListGrid.Items.Count == 0)
            {
                ContactListGrid.ShowFooter = true;
                Label label = new Label();
                label.Text = obj.GetTranslatedText("No Records Found.");//FB 1830 - Translation
                label.CssClass = "lblError";
                e.Item.Cells.Clear();
                TableCell cell = new TableCell();
                cell.ColumnSpan = ContactListGrid.Columns.Count;
                cell.Controls.Add(label);
                cell.HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells.Add(cell);
            }
        }

    }
    #endregion

    #region ContactListGrid ItemDataBound Event Handler

    void ContactListGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                if (row != null)
                {
                    e.Item.Cells[0].Text = row["TopTier"].ToString();
                    e.Item.Cells[1].Text = row["MiddleTier"].ToString();
                    e.Item.Cells[2].Text = row["Room"].ToString();
                    e.Item.Cells[3].Text = row["VoIP"].ToString();
                    e.Item.Cells[4].Text = row["ISDN"].ToString();
                    e.Item.Cells[5].Text = row["firstname"].ToString() + " " + row["lastname"].ToString();
                    e.Item.Cells[6].Text = row["telephone"].ToString();
                    e.Item.Cells[7].Text = row["VTC"].ToString();

                    isItemCreated = true;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }
    #endregion

    #region ContactListGrid PageIndexChanged Event Handler

    void ContactListGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            ContactListGrid.CurrentPageIndex = e.NewPageIndex;
            if (report == "")
                report = "CL";
            BindData();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region PRIAllocationGrid_ItemCreated Event Handler

    void PRIAllocationGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (!isItemCreated)
        {
            if (e.Item.ItemType == ListItemType.Footer && PRIAllocationGrid.Items.Count == 0)
            {
                PRIAllocationGrid.ShowFooter = true;
                Label label = new Label();
                label.Text = obj.GetTranslatedText("No Records Found.");//FB 1830 - Translation
                label.CssClass = "lblError";
                e.Item.Cells.Clear();
                TableCell cell = new TableCell();
                cell.ColumnSpan = PRIAllocationGrid.Columns.Count;
                cell.Controls.Add(label);
                cell.HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells.Add(cell);
            }
        }
    }

    #endregion

    #region PRIAllocationGrid ItemDataBound Event Handler

    void PRIAllocationGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                if (row != null)
                {
                    if (stDate != Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString())
                        e.Item.Cells[0].Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString());
                    e.Item.Cells[1].Text = row["PRI"].ToString();
                    e.Item.Cells[2].Text = row["Title"].ToString();

                    DateTime blockDate = DateTime.Parse("00:00:00");
                    for (Int32 i = 3; i < e.Item.Cells.Count; i++)
                    {
                        e.Item.Cells[i].BackColor = (BlockBetween(blockDate, Convert.ToDateTime(row["startdate"]), Convert.ToDateTime(row["enddate"]))) ? Color.Gold : Color.White;
                        blockDate = blockDate.AddMinutes(30);
                    }

                    stDate = Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString();
                    isItemCreated = true;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region PRIAllocationGrid PageIndexChanged Event Handler

    void PRIAllocationGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            PRIAllocationGrid.CurrentPageIndex = e.NewPageIndex;
            if (report == "")
                report = "PRI";
            BindData();

            foreach (DataGridItem item in this.PRIAllocationGrid.Items)
            {

            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region ResourceAllocationReportGrid_ItemCreated

    void ResourceAllocationReportGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (!isItemCreated)
        {
            if (e.Item.ItemType == ListItemType.Footer && ResourceAllocationReportGrid.Items.Count == 0)
            {
                ResourceAllocationReportGrid.ShowFooter = true;
                Label label = new Label();
                label.Text = obj.GetTranslatedText("No Records Found.");//FB 1830 - Translation
                label.CssClass = "lblError";
                e.Item.Cells.Clear();
                TableCell cell = new TableCell();
                cell.ColumnSpan = ResourceAllocationReportGrid.Columns.Count;
                cell.Controls.Add(label);
                cell.HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells.Add(cell);
            }
        }
    }

    #endregion

    #region ResourceAllocationReportGrid_ItemDataBound

    void ResourceAllocationReportGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                if (row != null)
                {
                    if (stDate != Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString())
                        e.Item.Cells[0].Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString());
                    e.Item.Cells[1].Text = Convert.ToDateTime(row["startdate"].ToString()).ToString(tformat);
                    e.Item.Cells[2].Text = Convert.ToDateTime(row["enddate"].ToString()).ToString(tformat);
                    e.Item.Cells[3].Text = row["Room"].ToString();
                    e.Item.Cells[4].Text = row["Title"].ToString();

                    DateTime blockDate = DateTime.Parse("00:00:00");
                    for (Int32 i = 5; i < e.Item.Cells.Count; i++)
                    {
                        e.Item.Cells[i].BackColor = (BlockBetween(blockDate, Convert.ToDateTime(row["startdate"]), Convert.ToDateTime(row["enddate"]))) ? Color.Gold : Color.White;
                        blockDate = blockDate.AddMinutes(30);
                    }

                    stDate = Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString();

                    isItemCreated = true;
                }
            }

            if(e.Item.ItemType == ListItemType.Footer)
            {
                if (isItemCreated)
                {
                    ResourceAllocationReportGrid.ShowFooter = true;

                    e.Item.Cells[4].Text = obj.GetTranslatedText("Totals:");//FB 1830 - Translation
                    //e.Item.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                    e.Item.Cells[4].Font.Bold = true;

                    foreach (DataGridItem item in ResourceAllocationReportGrid.Items)
                    {
                        for (Int32 n = 5; n < ResourceAllocationReportGrid.Columns.Count; n++)
                        {
                            if (item.Cells[n].BackColor == Color.Gold)
                                e.Item.Cells[n].Text = ((e.Item.Cells[n].Text == "&nbsp;") ? 1 : Convert.ToInt32(e.Item.Cells[n].Text) + 1).ToString();
                            else
                                e.Item.Cells[n].Text = "0";
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region ResourceAllocationReportGrid_PageIndexChanged

    void ResourceAllocationReportGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            ResourceAllocationReportGrid.CurrentPageIndex = e.NewPageIndex;
            if (report == "")
                report = "RAL";
            BindData();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region AggregateUsageGrid_ItemCreated Event Handler

    void AggregateUsageGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (!isItemCreated)
        {
            if (e.Item.ItemType == ListItemType.Footer && AggregateUsageGrid.Items.Count == 0)
            {
                AggregateUsageGrid.ShowFooter = true;
                Label label = new Label();
                label.Text = obj.GetTranslatedText("No Records Found.");//FB 1830 - Translation
                label.CssClass = "lblError";
                e.Item.Cells.Clear();
                TableCell cell = new TableCell();
                cell.ColumnSpan = AggregateUsageGrid.Columns.Count;
                cell.Controls.Add(label);
                cell.HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells.Add(cell);
            }
        }
    }

    #endregion

    #region AggregateUsageGrid_ItemDataBound Event Handler

    void AggregateUsageGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                if (row != null)
                {
                    //FB 1830 - Start
                    tmpVal = 0;
                    decimal.TryParse(row["AudioConf"].ToString(), out tmpVal);
                    e.Item.Cells[0].Text = tmpVal.ToString("n0", cInfo);

                    tmpVal = 0;
                    decimal.TryParse(row["AudioVideoConf"].ToString(), out tmpVal);
                    e.Item.Cells[1].Text = tmpVal.ToString("n0", cInfo);

                    tmpVal = 0;
                    decimal.TryParse(row["PointToPointConf"].ToString(), out tmpVal);
                    e.Item.Cells[2].Text = tmpVal.ToString("n0", cInfo);

                    tmpVal = 0;
                    decimal.TryParse(row["RoomConf"].ToString(), out tmpVal);
                    e.Item.Cells[3].Text = tmpVal.ToString("n0", cInfo);

                    tmpVal = 0;
                    decimal.TryParse(row["TotalConf"].ToString(), out tmpVal);
                    e.Item.Cells[4].Text = tmpVal.ToString("n0", cInfo);

                    tmpVal = 0;
                    decimal.TryParse(row["TotalDuration"].ToString(), out tmpVal);
                    e.Item.Cells[5].Text = tmpVal.ToString("n0", cInfo);
                    //e.Item.Cells[0].Text = row["AudioConf"].ToString();
                    //e.Item.Cells[1].Text = row["AudioVideoConf"].ToString();
                    //e.Item.Cells[2].Text = row["PointToPointConf"].ToString();
                    //e.Item.Cells[3].Text = row["RoomConf"].ToString();
                    //e.Item.Cells[4].Text = row["TotalConf"].ToString();
                    //e.Item.Cells[5].Text = row["TotalDuration"].ToString();
                    //FB 1830 - End
                    
                    if (ds.Tables.Count > 0)
                        e.Item.Cells[6].Text = ds.Tables[1].Rows[0][0].ToString();

                    isItemCreated = true;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region UsageByRoomGrid ItemCreated Event Handler

    void UsageByRoomGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (!isItemCreated)
        {
            if (e.Item.ItemType == ListItemType.Footer && UsageByRoomGrid.Items.Count == 0)
            {
                UsageByRoomGrid.ShowFooter = true;
                Label label = new Label();
                label.Text = obj.GetTranslatedText("No Records Found.");//FB 1830 - Translation
                label.CssClass = "lblError";
                e.Item.Cells.Clear();
                TableCell cell = new TableCell();
                cell.ColumnSpan = UsageByRoomGrid.Columns.Count;
                cell.Controls.Add(label);
                cell.HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells.Add(cell);
            }
        }
    }

    #endregion

    #region UsageByRoomGrid_ItemDataBound Event Handler

    void UsageByRoomGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                if (row != null)
                {
                    e.Item.Cells[0].Text = row["name"].ToString();
                    //FB 1830
                    tmpVal = 0;
                    decimal.TryParse(row["TotalMinutes"].ToString(), out tmpVal);
                    e.Item.Cells[1].Text = tmpVal.ToString("n0", cInfo);
                    //e.Item.Cells[1].Text = row["TotalMinutes"].ToString();

                    isItemCreated = true;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region UsageByRoomGrid PageIndexChanged Event Handler

    void UsageByRoomGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            UsageByRoomGrid.CurrentPageIndex = e.NewPageIndex;
            if (report == "")
                report = "UR";
            BindData();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }

    #endregion

    #region CalendarGrid ItemCreated Event Handler

    void CalendarGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (!isItemCreated)
        {
            if (e.Item.ItemType == ListItemType.Footer && CalendarGrid.Items.Count == 0)
            {
                CalendarGrid.ShowFooter = true;
                Label label = new Label();
                label.Text = obj.GetTranslatedText("No Records Found.");//FB 1830 - Translation
                label.CssClass = "lblError";
                e.Item.Cells.Clear();
                TableCell cell = new TableCell();
                cell.ColumnSpan = CalendarGrid.Columns.Count;
                cell.Controls.Add(label);
                cell.HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells.Add(cell);
            }
        }
    }
    #endregion

    #region CalendarGrid ItemDataBound Event Handler

    void CalendarGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                if (row != null)
                {   
                    e.Item.Cells[0].Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startdate"]).ToShortDateString());
                    e.Item.Cells[1].Text = Convert.ToDateTime(row["startdate"]).ToString(tformat);
                    e.Item.Cells[2].Text = Convert.ToDateTime(row["endDate"]).ToString(tformat);
                    e.Item.Cells[3].Text = row["duration"].ToString();
                    e.Item.Cells[4].Text = row["Room"].ToString();
                    e.Item.Cells[5].Text = row["Title"].ToString();
                    e.Item.Cells[6].Text = row["lastname"].ToString();
                    e.Item.Cells[7].Text = row["firstname"].ToString();
                    e.Item.Cells[8].Text = row["email"].ToString().Replace("@", "@ ");
                    e.Item.Cells[9].Text = row["State"].ToString();
                    e.Item.Cells[10].Text = row["City"].ToString();
                    e.Item.Cells[11].Text = row["ConfNum"].ToString();
                    e.Item.Cells[12].Text = row["meetType"].ToString();
                    e.Item.Cells[13].Text = row["connType"].ToString();
                    e.Item.Cells[14].Text = row["vidProtocol"].ToString();
                    e.Item.Cells[15].Text = row["Status"].ToString();
                    e.Item.Cells[16].Text = row["description"].ToString();

                    isItemCreated = true;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region CalendarGrid PageIndexChanged Event Handler

    void CalendarGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            CalendarGrid.CurrentPageIndex = e.NewPageIndex;
            if (report == "")
                report = "CAL";
            BindData();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace); ;
        }
    }
    #endregion

    #region HideRows

    private void HideRows()
    {
        lblHeading.Text = "";
        ContactListRow.Attributes.Add("Style", "Display:None");
        PRIAllocationGRow.Attributes.Add("Style", "Display:None");
        ResourceAllocationReportRow.Attributes.Add("Style", "Display:None");
        DailyScheduleRow.Attributes.Add("Style", "Display:None");
        AggregateUsageRow.Attributes.Add("Style", "Display:None");
        UsageByRoomRow.Attributes.Add("Style", "Display:None");
        CalendarRow.Attributes.Add("Style", "Display:None");

    }

    #endregion

    #region BlockBetween

    private Boolean BlockBetween(DateTime blockDate, DateTime startT, DateTime endT)
    {
        bool isBlock = false;
        try
        {
            DateTime stDate;
            DateTime etDate;

            stDate = DateTime.Parse(startT.ToShortTimeString());
            etDate = DateTime.Parse(endT.ToShortTimeString());

            if (blockDate == stDate || (blockDate > stDate && blockDate < etDate))
                isBlock = true;
            else
                isBlock = false;

        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
        return isBlock;
    }

    #endregion

    #region ShowReports

    private void ShowReports()
    {
        try
        {
            hdnValue.Value = report;
            DataTable emptyTable = new DataTable();

            Session["reportType"] = hdnValue.Value;

            if (ds.Tables.Count > 0)
                Session["PrintTable"] = ds.Tables[0];
            else
                Session["PrintTable"] = emptyTable;

            switch (report)
            {
                case "CAL":
                    lblHeading.Text = obj.GetTranslatedText("VTC Calendar");//FB 1830 - Translation

                    Session["titleString"] = obj.GetTranslatedText("VTC Calendar");//FB 1830 - Translation

                    CalendarRow.Attributes.Add("Style", "Display:Block");
                    if (ds.Tables.Count > 0)//FB 1830
                    {
                        table = new DataTable();
                        table1 = new DataTable();
                        table = ds.Tables[0];
                        table1 = ds.Tables[1];
                        BindCalendarSummary(ds);
                    }
                    if (ds.Tables.Count > 0)
                    {                           
                        if (ds.Tables.Count == 1)
                        {
                            if (ds.Tables[0].TableName == "Table")
                                table = ds.Tables[0];
                            else if (ds.Tables[0].TableName == "Table1")
                                table1 = ds.Tables[0];
                        }
                        else
                        {
                            // Added for FB 1511 START
                            if (Session["EnableEntity"].ToString() == "1")//Organization Module Fixes
                            if (ds.Tables[0].Columns.Count >= 16)
                            {
                                for (Int32 i = 16; i < ds.Tables[0].Columns.Count; i++)
                                {
                                    BoundColumn column = new BoundColumn();
                                    column.DataField = ds.Tables[0].Columns[i].ColumnName.ToString();
                                    column.HeaderText = ds.Tables[0].Columns[i].ColumnName.ToString();
                                    CalendarGrid.Columns.Add(column);
                                }
                            }
                            // Added for FB 1511 END
                            //table = ds.Tables[0]; //Commented for FB 1830
                            //table1 = ds.Tables[1];
                        }

                        CalendarGrid.DataSource = table;

                        if (isItemCreated)
                            CalendarGrid.ShowFooter = false;
                    }
                    else
                        CalendarGrid.DataSource = emptyTable;
                    CalendarGrid.DataBind();

                    Session["PrintGrid"] = CalendarGrid;
                    //Commented for FB 1830
                    //if (ds.Tables.Count > 0)
                    //    BindCalendarSummary(ds);

                    break;
                case "CL":
                    lblHeading.Text = obj.GetTranslatedText("VTC Contact List");//FB 1830 - Translation
                    Session["titleString"] = obj.GetTranslatedText("VTC Contact List");//FB 1830 - Translation

                    ContactListRow.Attributes.Add("Style", "Display:Block");
                    if (ds.Tables.Count > 0)
                    {
                        ContactListGrid.DataSource = ds.Tables[0];
                    }
                    else
                        ContactListGrid.DataSource = emptyTable;

                    ContactListGrid.DataBind();

                    if (isItemCreated)
                        ContactListGrid.ShowFooter = false;

                    Session["PrintGrid"] = ContactListGrid;
                    break;
                case "PRI":
                    lblHeading.Text = obj.GetTranslatedText("PRI Allocation Report");//FB 1830 - Translation
                    Session["titleString"] = obj.GetTranslatedText("PRI Allocation Report");//FB 1830 - Translation

                    PRIAllocationGRow.Attributes.Add("Style", "Display:Block");
                    if (ds.Tables.Count > 0)
                    {
                        // Added for FB 1511 START
                        if (Session["EnableEntity"].ToString() == "1")//Organization Module Fixes
                        if (ds.Tables[0].Columns.Count >= 5)
                        {
                            for (Int32 i = 5; i < ds.Tables[0].Columns.Count; i++)
                            {
                                BoundColumn column = new BoundColumn();
                                column.DataField = ds.Tables[0].Columns[i].ColumnName.ToString();
                                column.HeaderText = ds.Tables[0].Columns[i].ColumnName.ToString();
                                PRIAllocationGrid.Columns.Add(column);
                            }
                        }
                        // Added for FB 1511 END
                        PRIAllocationGrid.DataSource = ds.Tables[0];
                    }
                    else
                        PRIAllocationGrid.DataSource = emptyTable;
                        
                    PRIAllocationGrid.DataBind();

                    if (isItemCreated)
                        PRIAllocationGrid.ShowFooter = false;

                    Session["PrintGrid"] = PRIAllocationGrid;
                    break;
                case "RAL":
                    lblHeading.Text = obj.GetTranslatedText("VTC Resource Allocation Report");//FB 1830 - Translation
                    Session["titleString"] = obj.GetTranslatedText("VTC Resource Allocation Report");//FB 1830 - Translation

                    ResourceAllocationReportRow.Attributes.Add("Style", "Display:Block");
                    if (ds.Tables.Count > 0)
                    {
                        // Added for FB 1511 START
                        if (Session["EnableEntity"].ToString() == "1")//Organization Module Fixes
                        if (ds.Tables[0].Columns.Count >= 4)
                        {
                            for (Int32 i = 4; i < ds.Tables[0].Columns.Count; i++)
                            {
                                BoundColumn column = new BoundColumn();
                                column.DataField = ds.Tables[0].Columns[i].ColumnName.ToString();
                                column.HeaderText = ds.Tables[0].Columns[i].ColumnName.ToString();
                                ResourceAllocationReportGrid.Columns.Add(column);
                            }
                        }
                        // Added for FB 1511 END
                        ResourceAllocationReportGrid.DataSource = ds.Tables[0];
                    }
                    else
                        ResourceAllocationReportGrid.DataSource = emptyTable;
                    ResourceAllocationReportGrid.DataBind();

                    if (isItemCreated)
                        ResourceAllocationReportGrid.ShowFooter = false;

                    Session["PrintGrid"] = ResourceAllocationReportGrid;
                    break;
                case "DS":
                    lblHeading.Text = obj.GetTranslatedText("VTC Daily Schedule");//FB 1830 - Translation
                    Session["titleString"] = obj.GetTranslatedText("VTC Daily Schedule");//FB 1830 - Translation

                    DailyScheduleRow.Attributes.Add("Style", "Display:Block");
                    if (ds.Tables.Count > 0)
                    {
                       // Added for FB 1511 START
                        if (Session["EnableEntity"].ToString() == "1")//Organization Module Fixes
                        if (ds.Tables[0].Columns.Count >= 10)
                        {
                            for (Int32 i = 10; i < ds.Tables[0].Columns.Count; i++)
                            {
                                BoundColumn column = new BoundColumn();
                                column.DataField = ds.Tables[0].Columns[i].ColumnName.ToString();
                                column.HeaderText = ds.Tables[0].Columns[i].ColumnName.ToString();
                                DailyScheduleGrid.Columns.Add(column);
                            }
                        }
                       // Added for FB 1511 END
                        //Commented for FB 1830
                       // table = ds.Tables[0];
                      //  table1 = ds.Tables[1];
                        DailyScheduleGrid.DataSource = ds.Tables[0];
                    }
                    else
                        DailyScheduleGrid.DataSource = emptyTable;
                    DailyScheduleGrid.DataBind();

                    if (isItemCreated)
                        DailyScheduleGrid.ShowFooter = false;

                    Session["PrintGrid"] = DailyScheduleGrid;
                    break;
                case "AU":
                    lblHeading.Text = obj.GetTranslatedText("Aggregate Usage Report");//FB 1830 - Translation
                    Session["titleString"] = obj.GetTranslatedText("Aggregate Usage Report");//FB 1830 - Translation

                    AggregateUsageRow.Attributes.Add("Style", "Display:Block");
                    if (ds.Tables.Count > 0)
                        AggregateUsageGrid.DataSource = ds.Tables[0];
                    else
                        AggregateUsageGrid.DataSource = emptyTable;
                    AggregateUsageGrid.DataBind();

                    if (isItemCreated)
                        AggregateUsageGrid.ShowFooter = false;

                    Session["PrintGrid"] = AggregateUsageGrid;

                    break;
                case "UR":
                    lblHeading.Text = obj.GetTranslatedText("Usage By Room");//FB 1830 - Translation
                    Session["titleString"] = obj.GetTranslatedText("Usage By Room");//FB 1830 - Translation
                    UsageByRoomRow.Attributes.Add("Style", "Display:Block");
                    if (ds.Tables.Count > 0)
                        UsageByRoomGrid.DataSource = ds.Tables[0];
                    else
                        UsageByRoomGrid.DataSource = emptyTable;
                    UsageByRoomGrid.DataBind();

                    if (isItemCreated)
                        UsageByRoomGrid.ShowFooter = false;

                    Session["PrintGrid"] = UsageByRoomGrid;
                    break;
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region VerifyRenderingInServerForm 

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region BindCalendarSummary 

    private void BindCalendarSummary(DataSet ds)
    {
        TableRow row = null;
        TableCell cell = null;
        Decimal duration = 0;
        Decimal ISDNoutBound = 0;
        Decimal ISDNinBound = 0;
        Decimal IPduration = 0;
        String strValue = "";
        try
        {
            DataTable dt = null;
            if (table1 != null)
                dt = table1;

            DataRow[] filterRows = null;

            #region Bind Summary

            if (dt != null)
            {
                filterRows = dt.Select("conftype=4");
                PtPtCnt.Text = filterRows.Length.ToString();

                filterRows = null;
                filterRows = dt.Select("conftype not in(11,7,4,9)");
                MultiPtCnt.Text = filterRows.Length.ToString();

                filterRows = null;
                filterRows = dt.Select("conftype not in(11,7,9)");
                VTCMtTotCnt.Text = filterRows.Length.ToString();
            }
            #endregion

            DataTable totTable = null;

            if (table != null)
                totTable = table;

            if (totTable != null)
            {
                DetailsHeadingTable.Attributes.Add("Style", "Display:Block");
                VTCTotal.Attributes.Add("Style", "Display:Block");

                #region

                //FB 1830
                cInfo = new CultureInfo(Session["NumberFormat"].ToString());
                Int32 divHeight = 0;
                foreach (DataRow dr in totTable.Rows)
                {
                    row = new TableRow();
                    row.HorizontalAlign = HorizontalAlign.Right;
                    cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(12); row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10); 
                    //FB 1830
                    decimal.TryParse(dr["Duration"].ToString(), out tmpVal);
                    dr["Duration"] = tmpVal.ToString("n0", cInfo);
                   
                    cell.Text = dr["Duration"].ToString();
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    //FB 1830
                    cell.Text = Math.Round(tmpVal / 60, 1).ToString("n0",cInfo); 
                    //cell.Text = Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1).ToString();
                    row.Cells.Add(cell);

                    cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(10); row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    //FB 1830
                    //cell.Text = ((dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Outbound") ? Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1) : 0).ToString();
                    cell.Text = ((dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Outbound") ? Math.Round(tmpVal / 60, 1) : 0).ToString("n0", cInfo); 
                    row.Cells.Add(cell);

                    cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(10); row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    //FB 1830
                    //cell.Text = ((dr["vidProtocol"].ToString() == "IP") ? Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1) : 0).ToString();
                    cell.Text = ((dr["vidProtocol"].ToString() == "IP") ? Math.Round(tmpVal / 60, 1) : 0).ToString("n0", cInfo); 
                    row.Cells.Add(cell);

                    cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(10); row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    //FB 1830
                    //cell.Text = ((dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Inbound") ? Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1) : 0).ToString();
                    cell.Text = ((dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Inbound") ? Math.Round(tmpVal / 60, 1) : 0).ToString("n0", cInfo); 
                    row.Cells.Add(cell);

                    VTCDetails.Rows.Add(row);

                    if (divHeight <= 150)
                        divHeight = divHeight + 10;

                    DetailsDiv.Attributes.Add("Style", "Height:" + divHeight + "px;display:none;overflow-y: auto;overflow-x: hidden;Width:80%");
                    //FB 1830
                    //duration += Convert.ToDecimal(dr["Duration"]);
                    duration += tmpVal;

                    if (dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Outbound")
                        //ISDNoutBound += Convert.ToDecimal(dr["Duration"]);
                        ISDNoutBound += tmpVal;//FB 1830

                    if (dr["vidProtocol"].ToString() == "IP")
                        //IPduration += Convert.ToDecimal(dr["Duration"]);
                        IPduration += tmpVal;//FB 1830

                    if (dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Inbound")
                        //ISDNinBound += Convert.ToDecimal(dr["Duration"]);
                        ISDNinBound += tmpVal;//FB 1830

                    //FB 1830
                    ds.Tables[0].Select()[0]["Duration"] = tmpVal.ToString("n0", cInfo);
                }

                #endregion

                #region Bind Total Row

                cell = new TableCell();
                //duration = Convert.ToDecimal(ds.Tables[0].Compute("sum(Duration)", ""));
                //FB 1830
                cell.Text = duration.ToString("n0", cInfo);
                cell.Width = Unit.Percentage(10);
                FirstRow.Cells.Add(cell);

                cell = new TableCell();
                //FB 1830
                cell.Text = Math.Round(duration / 60, 1).ToString("n0", cInfo);
                cell.Width = Unit.Percentage(10);
                FirstRow.Cells.Add(cell);

                cell = new TableCell();
                //FB 1830
                cell.Text = totTable.Rows.Count.ToString("n0", cInfo);
                cell.Width = Unit.Percentage(10);
                FirstRow.Cells.Add(cell);

                cell = new TableCell();
                cell.Width = Unit.Percentage(10);
                //strValue = totTable.Compute("sum(Duration)", "vidProtocol = 'ISDN' and connType = 'Outbound'").ToString();
                if (strValue != "")
                {
                    //ISDNoutBound = Math.Round(Convert.ToDecimal(strValue) / 60);
                    //FB 1830
                    cell.Text = (Math.Round(ISDNoutBound / 60, 1)).ToString("n0", cInfo);
                }
                else
                    cell.Text = "0";

                FirstRow.Cells.Add(cell);

                cell = new TableCell();
                cell.Width = Unit.Percentage(10);
                //FB 1830
                cell.Text = Math.Round((ISDNoutBound / (duration / 60)) * 100).ToString("n0", cInfo);
                FirstRow.Cells.Add(cell);

                cell = new TableCell();
                cell.Width = Unit.Percentage(10);
                strValue = "";
                // strValue = totTable.Compute("sum(Duration)", "vidProtocol = 'IP'").ToString();
                // IPduration = Math.Round(Convert.ToDecimal(strValue) / 60);
                //FB 1830
                cell.Text = Math.Round(IPduration / 60).ToString("n0", cInfo);
                FirstRow.Cells.Add(cell);

                cell = new TableCell();
                cell.Width = Unit.Percentage(10);
                //FB 1830
                cell.Text = Math.Round((IPduration / (duration / 60)) * 100).ToString("n0", cInfo);
                FirstRow.Cells.Add(cell);

                cell = new TableCell();
                cell.Width = Unit.Percentage(10);
                strValue = "";
                //strValue = totTable.Compute("sum(Duration)", "vidProtocol = 'ISDN' and connType = 'Inbound' ").ToString();
                if (ISDNinBound != 0)
                    //FB 1830
                    cell.Text = Math.Round(ISDNinBound / 60).ToString("n0", cInfo);
                else
                    cell.Text = "0";
                FirstRow.Cells.Add(cell);

                VTCTotal.Rows.Add(FirstRow);
                //FB 1830
                ds.AcceptChanges();
                #endregion
            }
            else
            {
                DetailsHeadingTable.Attributes.Add("Style", "Display:None");
                VTCTotal.Attributes.Add("Style", "Display:None");
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    # region GetLocations 
    protected void GetLocations(String RoomID)
    {
        try
        {
            String inXML = "<GetLocationsList>" + obj.OrgXMLElement() + "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></GetLocationsList>";//Organization Module Fixes
            // String inXML = "<GetLocationsList><userID>11</userID></GetLocationsList>";

            String outXML = obj.CallMyVRMServer("GetLocationsList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            //String outXML = obj.CallMyVRMServer("GetLocationsList", inXML, "C:\\VRMSchemas_v1.8.3\\");

            //Response.Write(obj.Transfer(outXML));
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            XmlNodeList nodes = xmldoc.SelectNodes("//level3List/level3");
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("roomID");
            DataColumn dc2 = new DataColumn("roomName");
            dc1.DataType = System.Type.GetType("System.Int32");
            dc2.DataType = System.Type.GetType("System.String");
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            DataRow dr = null;
            if (nodes.Count > 0)
            {
                foreach (XmlNode node3 in nodes)
                {

                    if (node3.SelectNodes("level2List/level2/level1List/level1").Count > 0)
                    {

                        XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");

                        foreach (XmlNode node2 in nodes2)
                        {

                            if (node2.SelectNodes("level1List/level1").Count > 0)
                            {
                                XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                foreach (XmlNode node1 in nodes1)
                                {
                                    dr = dt.NewRow();
                                    dr[dc1] = node1.SelectSingleNode("level1ID").InnerText;
                                    if (defaultRoomID == "")
                                        defaultRoomID = node1.SelectSingleNode("level1ID").InnerText;
                                    else
                                        defaultRoomID = defaultRoomID + "," + node1.SelectSingleNode("level1ID").InnerText;
                                    dr[dc2] = node1.SelectSingleNode("level1Name").InnerText;
                                    dt.Rows.Add(dr);
                                }
                            }

                        }
                    }
                }

            }
            cblRoom.DataValueField = "roomID";
            cblRoom.DataTextField = "roomName";
            cblRoom.DataSource = dt;
            cblRoom.DataBind();
           
            //Code added for FB 1672 - Starts 
            if (dt.Rows.Count == 0)
            {
                chkSelectall.Checked = false;
                chkSelectall.Enabled = false;
            }
            //Code added for FB 1672 - End 
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);           
        }
    }
    # endregion

    # region GetRoomIDs 
    private String GetRoomIDs()
    {
        Boolean isItemChecked = true;
        String rooms = "";
        
        try
        {
            if (cblRoom.SelectedValue == "")
                isItemChecked = false;
            
            foreach (ListItem item in cblRoom.Items)
            {
                if (!isItemChecked)
                    item.Selected = true;

                if (item.Selected)
                {
                    if (roomIds != "")
                    {
                        roomIds += ",";
                        rooms += ", ";
                    }
                    roomIds += item.Value;
                    rooms += item.Text;
                }

            }

            if (rooms != "")
                txtRooms.Text = rooms;

        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);            
        }
        return roomIds;
    }
    # endregion

    #region ExportExcel 

    protected void ExportExcel(object sender, EventArgs e)
    {
        try
        {

            if (ReportsList.SelectedValue == "4")
                GetQueries();
            else
                BindData();

            // Excel 
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition","attachment;filename=Report.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            trGraph1M.RenderControl(hw);
            Response.Output.Write(sw.ToString());

            sw = new StringWriter();
            hw = new HtmlTextWriter(sw);

            switch (hdnValue.Value)
            {
                case "CAL":
                    CalendarGrid.AllowPaging = false;
                    CalendarGrid.DataBind();
                    CalendarGrid.RenderControl(hw);                    
                    break;
                case "CL":
                    ContactListGrid.AllowPaging = false;
                    ContactListGrid.DataBind();
                    ContactListGrid.RenderControl(hw);
                    break;
                case "PRI":
                    PRIAllocationGrid.AllowPaging = false;
                    PRIAllocationGrid.DataBind();
                    PRIAllocationGrid.RenderControl(hw);
                    break;
                case "RAL":
                    ResourceAllocationReportGrid.AllowPaging = false;
                    ResourceAllocationReportGrid.DataBind();
                    ResourceAllocationReportGrid.RenderControl(hw);
                    break;
                case "DS":
                    DailyScheduleGrid.AllowPaging = false;
                    DailyScheduleGrid.DataBind();
                    DailyScheduleGrid.RenderControl(hw);
                    break;
                case "AU":
                    AggregateUsageGrid.AllowPaging = false;
                    AggregateUsageGrid.DataBind();
                    AggregateUsageGrid.RenderControl(hw);
                    break;
                case "UR":
                    UsageByRoomGrid.AllowPaging = false;
                    UsageByRoomGrid.DataBind();
                    UsageByRoomGrid.RenderControl(hw);
                    break;
                case "T":
                    tbReports.DataBind();
                    tbReports.RenderControl(hw);
                    break;
            }

            ////style to format numbers to string 
            //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            //Response.Write(style);
            Response.Output.Write(sw.ToString());

            if (hdnValue.Value == "CAL")
            {
                sw = new StringWriter();
                hw = new HtmlTextWriter(sw);
                SummaryTable.RenderControl(hw);
                Response.Output.Write(sw.ToString());

                sw = new StringWriter();
                hw = new HtmlTextWriter(sw);
                DetailsHeadingTable.RenderControl(hw);
                Response.Output.Write(sw.ToString());

                sw = new StringWriter();
                hw = new HtmlTextWriter(sw);
                VTCDetails.RenderControl(hw);
                Response.Output.Write(sw.ToString());

                sw = new StringWriter();
                hw = new HtmlTextWriter(sw);
                VTCTotal.RenderControl(hw);
                Response.Output.Write(sw.ToString());
            }
            Response.Flush();
            Response.End(); 
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region ExportPDF 

    protected void ExportPDF(object sender, EventArgs e)
    {
        try
        {
            if (ReportsList.SelectedValue.Equals("4"))
            {
                if (DrpChrtType.SelectedValue.Equals("1"))
                {
                    GetQueries();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter hw = new HtmlTextWriter(sw);

                    lblHeading.RenderControl(hw);

                    switch (hdnValue.Value)
                    {
                        case "T":
                            tbReports.DataBind();
                            tbReports.RenderControl(hw);
                            break;
                    }
                    ////style to format numbers to string 
                    StringReader sr = new StringReader(sw.ToString());

                    iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    htmlparser.Parse(sr);
                    pdfDoc.Close();
                    Response.Write(pdfDoc);
                    Response.End();
                }
                else
                {

                    while (tempText.Text.Equals(""))
                    {
                        Response.Write("here");
                    }
                    String strScript = "<script language=\"javascript\">document.form1.Submit()</script>";
                    RegisterClientScriptBlock("addItems", strScript);
                    PdfConverter pdfConverter = new PdfConverter();
                    pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A5;
                    pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                    pdfConverter.PdfDocumentOptions.ShowHeader = true;
                    pdfConverter.PdfDocumentOptions.ShowFooter = true;
                    pdfConverter.PdfDocumentOptions.LeftMargin = 5;
                    pdfConverter.PdfDocumentOptions.RightMargin = 5;
                    pdfConverter.PdfDocumentOptions.TopMargin = 5;
                    pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                    pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;

                    pdfConverter.PdfDocumentOptions.ShowHeader = false;

                    pdfConverter.PdfFooterOptions.PageNumberText = "";
                    pdfConverter.PdfFooterOptions.ShowPageNumber = false;
                    pdfConverter.PdfFooterOptions.FooterText = "";
                    pdfConverter.LicenseKey = "kGb8Fr0gg2spzZd/SluMYY+Bv/RxuZmz6thATnaDnlkD20HkaCEyR4X+P9QqabXI";

                    string txtHtml = "";
                    if (Application["Client"].ToString().ToUpper() == "MOJ")
                    {
                        txtHtml = tempText.Text;

                    }
                    else
                    {
                        txtHtml = tempText.Text;

                    }

                    //pdfConverter.LicenseKey = "put your serial number here";
                    byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(txtHtml);
                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.Clear();
                    response.AddHeader("Content-Type", "binary/octet-stream");

                    response.AddHeader("Content-Disposition", "attachment; filename=Report.pdf; size=" + downloadBytes.Length.ToString());

                    response.BinaryWrite(downloadBytes);
                    //response.Flush();
                    response.End();
                }
            }
            else
            {
                BindData();

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                lblHeading.RenderControl(hw);                

                switch (hdnValue.Value)
                {
                    case "CAL":
                        CalendarGrid.AllowPaging = false;
                        CalendarGrid.DataBind();
                        CalendarGrid.RenderControl(hw);
                        SummaryTable.RenderControl(hw);
                        DetailsHeadingTable.RenderControl(hw);
                        VTCDetails.RenderControl(hw);
                        VTCTotal.RenderControl(hw);
                        break;
                    case "CL":
                        ContactListGrid.AllowPaging = false;
                        ContactListGrid.DataBind();
                        ContactListGrid.RenderControl(hw);
                        break;
                    case "PRI":
                        PRIAllocationGrid.AllowPaging = false;
                        PRIAllocationGrid.DataBind();
                        PRIAllocationGrid.RenderControl(hw);
                        break;
                    case "RAL":
                        ResourceAllocationReportGrid.AllowPaging = false;
                        ResourceAllocationReportGrid.DataBind();
                        ResourceAllocationReportGrid.RenderControl(hw);
                        break;
                    case "DS":
                        DailyScheduleGrid.AllowPaging = false;
                        DailyScheduleGrid.DataBind();
                        DailyScheduleGrid.RenderControl(hw);
                        break;
                    case "AU":
                        AggregateUsageGrid.AllowPaging = false;
                        AggregateUsageGrid.DataBind();
                        AggregateUsageGrid.RenderControl(hw);
                        break;
                    case "UR":
                        UsageByRoomGrid.AllowPaging = false;
                        UsageByRoomGrid.DataBind();
                        UsageByRoomGrid.RenderControl(hw);
                        break;
                }
                ////style to format numbers to string 
                StringReader sr = new StringReader(sw.ToString());

                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
        }
        catch (System.Threading.ThreadAbortException)
        { }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region ImgPrinter_Click Event Handler

    void ImgPrinter_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            BindData();

            switch (hdnValue.Value)
            {
                case "CAL":
                    Session["titleString"] = "Calendar Report";
                    Session["PrintGrid"] = CalendarGrid;
                    break;
                case "CL":
                    Session["titleString"] = "Contact List";
                    Session["PrintGrid"] = ContactListGrid;
                    break;
                case "PRI":
                    Session["titleString"] = "PRI Allocation Report";
                    Session["PrintGrid"] = PRIAllocationGrid;
                    break;
                case "RAL":
                    Session["titleString"] = "VTC Resource Allocation Report";
                    Session["PrintGrid"] = ResourceAllocationReportGrid;
                    break;
                case "DS":
                    Session["titleString"] = "VTC Daily Schedule";
                    Session["PrintGrid"] = DailyScheduleGrid;
                    break;
                case "AU":
                    Session["titleString"] = "Aggregate Usage Report";
                    Session["PrintGrid"] = AggregateUsageGrid;
                    break;
                case "UR":
                    Session["titleString"] = "Usage By Room";
                    Session["PrintGrid"] = UsageByRoomGrid;
                    break;
                case "T":    //Added for Graphical Reports
                    Session["titleString"] = lblHeading.Text;
                    Session["PrintGrid"] = tbReports;
                    break;

            }

            DataTable emptyTable = null;

            if (ds.Tables.Count > 0)
                Session["PrintTable"] = ds.Tables[0];
            else
                Session["PrintTable"] = emptyTable;

            Session["reportType"] = hdnValue.Value;

            string pageURL = "PrintInterface.aspx";

            pageURL = "<script>window.open('" + pageURL + "')</script>";

            this.RegisterStartupScript("Print", pageURL);

        }
        catch (System.Threading.ThreadAbortException)
        { }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion


    #region DisplayLocations

    protected void DisplayLocations(Object sender, EventArgs e)
    {
        try
        {
            if (ConfScheRptDivList.SelectedValue.Equals("1") || ReportsList.SelectedValue.Equals("4"))
            {
                GetLocations("");
                GetRoomIDs();

                LocTab.Update();
                if (ReportsList.SelectedValue.Equals("4"))
                {
                    BindCountryState();
                    CountryPan.Update();
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("DisplayLocations: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

   
    //SQL Report ENd

}
