/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace DynamicDataGridTemplates
{
    public class DataGridTemplate : ITemplate
    {
        ListItemType templateType;
        string columnName;

        public DataGridTemplate(ListItemType type, string colname)
        {
            templateType = type;
            columnName = colname;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            Literal lc = new Literal();
            switch (templateType)
            {
                case ListItemType.Header:
                    lc.Text = "<B>" + columnName + "</B>";
                    container.Controls.Add(lc);
                    break;
                case ListItemType.Item:
                    lc.Text = "Item " + columnName;
                    container.Controls.Add(lc);
                    break;
                case ListItemType.EditItem:
                    TextBox tb = new TextBox();
                    tb.Text = "";
                    container.Controls.Add(tb);
                    break;
                case ListItemType.Footer:
                    lc.Text = "<I>" + columnName + "</I>";
                    container.Controls.Add(lc);
                    break;
            }
        }
    }
}
