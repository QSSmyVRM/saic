/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxMenu;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts;
using DevExpress.Utils;
                

/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

public partial class HDConferenceList : System.Web.UI.Page
{
    #region protected Members
    
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
    protected DevExpress.Web.ASPxGridView.ASPxGridView MainGrid;
    
    #endregion

    #region protected data members 

    private myVRMNet.NETFunctions obj;
    private ns_Logger.Logger log;

    protected Int32 orgId = 11;
    protected XmlDocument xmlDoc = null;
    protected DataSet ds = null;
    protected DataSet dr = null;
    protected DataTable rptTable = new DataTable();
    protected String format = "dd/MM/yyyy";
    protected String tformat = "hh:mm tt";
    protected String tmzone = "";
    protected String organizationID = "";
    protected Int32 usrID = 11;
    bool isSearchConf = false; //FB 2763
    protected System.Web.UI.WebControls.Button btnCancel; //FB 2763
    #endregion

    #region Constructor 
    public HDConferenceList()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
    }

    #endregion
    
    #region Methods Executed on Page Load 

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("HDConferenceList.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            if (Session["userID"] != null)
            {
                if (Session["userID"].ToString() != "")
                    Int32.TryParse(Session["userID"].ToString(), out usrID);
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            if (Session["timeFormat"] != null)
            {
                if (Session["timeFormat"].ToString() != "")
                    if (Session["timeFormat"].ToString() == "0")
                        tformat = "HH:mm";
            }

            if (Request.QueryString["frm"] != null) //FB 2763
                if (Request.QueryString["frm"].ToString().Equals("1"))
                {
                    isSearchConf = true;
                    btnCancel.Visible = true;
                }

            //if (!IsPostBack)
                BindData();
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_Load" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;            
        }
    }

    #endregion

    #region BindData 

    private void BindData()
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();
            String outXML = obj.CallMyVRMServer("SearchConference", Session["inXML"].ToString(), Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") < 0)
            {
                xmlDoc.LoadXml(outXML);
                ds = new DataSet();
                dr = new DataSet();

                ds.ReadXml(new XmlNodeReader(xmlDoc));

                if (ds.Tables.Count > 0)
                {
                    CreateTable();

                    try
                    {
                        MainGrid.DataSource = rptTable;
                        MainGrid.DataBind();
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.StackTrace + " : " + ex.Message);
                    }
                }
            }
            else
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BindData" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;            
        }
    }

    #endregion

    #region MainGrid_HtmlRowCreated

    protected void MainGrid_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            GridViewDataColumn colDuration = (GridViewDataColumn)MainGrid.Columns["Duration"];

            // ZD 100263 Starts
            if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].ToString().Equals("1"))
                if (colDuration == null)
                {
                    log.Trace("Page: HDConferenceList.aspx.cs, Function: MainGrid_HtmlRowCreated, Reason: GridViewDataColumn colDuration is null");
                    Response.Redirect("ShowError.aspx");
                }
            // ZD 100263 Ends

            colDuration.Width = Unit.Percentage(8);
            GridViewDataColumn colConfirmationNumber = (GridViewDataColumn)MainGrid.Columns["Confirmation <br>Number"];
            colConfirmationNumber.Width = Unit.Percentage(9);
            GridViewDataColumn colReservationDate = (GridViewDataColumn)MainGrid.Columns["Reservation <br>Start Date/Time"];
            colReservationDate.Width = Unit.Percentage(15);
            GridViewDataColumn colRequestor = (GridViewDataColumn)MainGrid.Columns["Requestor"];
            colRequestor.Width = Unit.Percentage(13);
            GridViewDataColumn colHost = (GridViewDataColumn)MainGrid.Columns["Host"];
            colHost.Width = Unit.Percentage(13);
            GridViewDataColumn colRooms = (GridViewDataColumn)MainGrid.Columns["Rooms"];
            colRooms.Width = Unit.Percentage(19);
            GridViewDataColumn colR = (GridViewDataColumn)MainGrid.Columns["Actions"];
            colR.Width = Unit.Percentage(24);

            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
            String colValue = "";
            String isHost = "", status = "";
            GridViewDataColumn colID = (GridViewDataColumn)MainGrid.Columns["ID"];
            if (colID != null)
            {
                colValue = MainGrid.GetRowValues(e.VisibleIndex, MainGrid.Columns["ID"].ToString()).ToString();
                colID.Visible = false;
            }
            
            GridViewDataColumn colIsHost = (GridViewDataColumn)MainGrid.Columns["Ishost"];
            if (colIsHost != null)
            {
                isHost = MainGrid.GetRowValues(e.VisibleIndex, MainGrid.Columns["Ishost"].ToString()).ToString();
                colIsHost.Visible = false;
            }

            GridViewDataColumn colStatus = (GridViewDataColumn)MainGrid.Columns["Status"];
            if (colStatus != null)
            {
                status = MainGrid.GetRowValues(e.VisibleIndex, MainGrid.Columns["Status"].ToString()).ToString();
                colStatus.Visible = false;
            }

            if (colRooms != null)
            {
                Int32 index = colRooms.Index;
                //e.Row.Cells[index].Wrap = false;
                String rmText = MainGrid.GetRowValues(e.VisibleIndex, "Rooms").ToString();
                e.Row.Cells[colRooms.Index].Text = rmText.Replace(",", "<br>");
            }
            
            if (colR != null)
            {
                Int32 index = colR.Index;
                String rCellText = MainGrid.GetRowValues(e.VisibleIndex, "Actions").ToString();
                LinkButton lnkView = new LinkButton();
                lnkView.ID = "View" + e.VisibleIndex + "_" + index;
                lnkView.Attributes.Add("onclick", "javascript:fnOpen('V','" + colValue + "')");
                lnkView.Style.Add("Cursor", "Hand");
                lnkView.Text = "View" + e.KeyValue;
                e.Row.Cells[index].Controls.Add(lnkView);

                if ((Request.QueryString["hf"] != "1") && isHost == "1" || (Int32.Parse(Session["admin"].ToString()) > 0))
                {
                    e.Row.Cells[index].Wrap = false;
                    e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                    if (status != "5")
                    {
                        LinkButton lnkClone = new LinkButton();
                        lnkClone.ID = "Clone" + e.VisibleIndex + "_" + index;
                        lnkClone.Attributes.Add("onclick", "javascript:fnOpen('C','" + colValue + "')");
                        lnkClone.Style.Add("Cursor", "Hand");
                        lnkClone.Text = "Clone";
                        lnkClone.Click += new EventHandler(lnkClone_Click);
                        e.Row.Cells[index].Controls.Add(lnkClone);
                    }

                    if (status != "7" && status != "5" && status != ns_MyVRMNet.vrmConfStatus.Deleted && status != ns_MyVRMNet.vrmConfStatus.Terminated) //TICK  #100037 
                    {
                        e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                        LinkButton lnkDelete = new LinkButton();
                        lnkDelete.ID = "Delete" + e.VisibleIndex + "_" + index;
                        lnkDelete.Attributes.Add("onclick", "javascript:fnOpen('D','" + colValue + "')");
                        lnkDelete.Style.Add("Cursor", "Hand");
                        lnkDelete.Text = "Delete";
                        lnkDelete.Click += new EventHandler(lnkDelete_Click);
                        e.Row.Cells[index].Controls.Add(lnkDelete);

                        e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                        LinkButton lnkEdit = new LinkButton();
                        lnkEdit.ID = "Edit" + e.VisibleIndex + "_" + index;
                        lnkEdit.Attributes.Add("onclick", "javascript:fnOpen('E','" + colValue + "')");
                        lnkEdit.Style.Add("Cursor", "Hand");
                        lnkEdit.Text = "Edit";
                        lnkEdit.Click += new EventHandler(lnkEdit_Click);
                        e.Row.Cells[index].Controls.Add(lnkEdit);
                    }

                    e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                    LinkButton lnkManage = new LinkButton();
                    lnkManage.ID = "Manage" + e.VisibleIndex + "_" + index;
                    lnkManage.Attributes.Add("onclick", "javascript:fnOpen('M','" + colValue + "')");
                    lnkManage.Style.Add("Cursor", "Hand");
                    lnkManage.Text = "Manage";
                    lnkManage.Click += new EventHandler(lnkManage_Click);
                    e.Row.Cells[index].Controls.Add(lnkManage);
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace(ex.StackTrace + " MainGrid_HtmlRowCreated: " + ex.Message);
        }
    }

    void lnkDelete_Click(object sender, EventArgs e)
    {
        DeleteConference(null, null);
    }

    void lnkClone_Click(object sender, EventArgs e)
    {
        Session["ConfID"] = hdnValue.Value;
        Response.Redirect("ConferenceSetup.aspx?t=o");
    }

    void lnkEdit_Click(object sender, EventArgs e)
    {
        Session["ConfID"] = hdnValue.Value;
        Response.Redirect("ConferenceSetup.aspx?t=");
    }

    void lnkManage_Click(object sender, EventArgs e)
    {
        Session["ConfID"] = hdnValue.Value;
        Response.Redirect("ManageConference.aspx?t=");
    }

    #endregion

    #region DeleteConference
    protected void DeleteConference(Object sender, DataGridCommandEventArgs e)
    {
        try
        {
            if (hdnValue.Value == "")
                return;

            String inXML = "";
            inXML += "<login>";
            inXML += obj.OrgXMLElement();
            inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
            inXML += "  <delconference>";
            inXML += "      <conference>";
            inXML += "          <confID>" + hdnValue.Value + "</confID>";
            inXML += "          <reason></reason>";
            inXML += "      </conference>";
            inXML += "  </delconference>";
            inXML += "</login>";
            String outXML = obj.CallMyVRMServer("DeleteConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); 
            log.Trace("DeleteConference - " + outXML);
            if (outXML.IndexOf("<error>") >= 0)
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
                BindData();
            }
            else
            {
                if (Application["External"].ToString() != "")
                {
                    String inEXML = "";
                    inEXML = "<SetExternalScheduling>";
                    inEXML += "<confID>" + hdnValue.Value + "</confID>";
                    inEXML += "</SetExternalScheduling>";

                    String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                }

                String inxml = "<DeleteParticipantICAL>";
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + hdnValue.Value + "</ConfID>";
                inxml += "</DeleteParticipantICAL>";
                obj.CallCommand("DeleteParticipantICAL", inxml);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }

                inxml = "<DeleteCiscoICAL>";
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + hdnValue.Value + "</ConfID>";
                inxml += "</DeleteCiscoICAL>";
                obj.CallCommand("DeleteCiscoICAL", inxml);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }

                Session["CalendarMonthly"] = null;
                inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <ConferenceID>" + hdnValue.Value + "</ConferenceID>";
                inXML += "  <WorkorderID>0</WorkorderID>";
                inXML += "</login>";

                outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    String qString = "m=1&t=" + Request.QueryString["t"].ToString();
                    if (Request.QueryString["pageNo"] != null)
                        qString += "&pageNo=" + Request.QueryString["pageNo"].ToString();
                    Response.Redirect("HDConferenceList.aspx?" + qString);
                }
            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("DeleteConference" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }
    #endregion

    #region CreateTable 

    private void CreateTable()
    {
        try
        {
            rptTable = new DataTable();
            rptTable.Columns.Add("Confirmation <br>Number", typeof(String));
            rptTable.Columns.Add("Reservation <br>Start Date/Time", typeof(String));
            rptTable.Columns.Add("Duration", typeof(String));
            rptTable.Columns.Add("Requestor", typeof(String));
            rptTable.Columns.Add("Host", typeof(String));
            rptTable.Columns.Add("Rooms", typeof(String));
            rptTable.Columns.Add("Actions", typeof(String));

            DataTable detailsTable = null;
            if (ds.Tables.Count > 2)
                detailsTable = ds.Tables[2];

            if (detailsTable != null)
            {
                for (Int32 i = 0; i < detailsTable.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        rptTable.Columns.Add("ID", typeof(String));
                        rptTable.Columns.Add("Ishost", typeof(String));
                        rptTable.Columns.Add("Status", typeof(String));
                    }
                    DataRow dataRow = null;

                    dataRow = rptTable.NewRow();

                    if (detailsTable.Rows[0][1].ToString() == "")
                        break;

                    DateTime startDate;
                    dataRow[0] = detailsTable.Rows[i]["ConferenceUniqueID"].ToString();
                    startDate = DateTime.Parse(detailsTable.Rows[i]["ConferenceDateTime"].ToString());
                    dataRow[1] = startDate.ToString(format) + " " + startDate.ToString(tformat);
                    dataRow[2] = detailsTable.Rows[i]["ConferenceDuration"].ToString() + " Min(s)";
                    dataRow[3] = detailsTable.Rows[i]["ConferenceRequestor"].ToString();
                    dataRow[4] = detailsTable.Rows[i]["ConferenceHost"].ToString();
                    dataRow[5] = detailsTable.Rows[i]["LocationList"].ToString().Replace("~", ", "); //<br />");
                    //dataRow[6] - Actions
                    dataRow[7] = detailsTable.Rows[i]["ConferenceID"].ToString();
                    dataRow[8] = detailsTable.Rows[i]["Ishost"].ToString();
                    dataRow[9] = detailsTable.Rows[i]["ConferenceStatus"].ToString();

                    rptTable.Rows.Add(dataRow);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateTable " + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion
    
    //FB 2763
    #region GoBack
    /// <summary>
    /// GoToLobby
    /// Fogbugz case 158
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GoBack(Object sender, EventArgs e)
    {
        try
        {
            if (Application["loginPage"] != null)
            {
                Response.Redirect(Application["loginPage"].ToString());
            }
            else if (isSearchConf) //FB 2763
                Response.Redirect("SearchConferenceInputParameters.aspx");
           
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message + " : " + ex.StackTrace);
        }
    }
    #endregion
}