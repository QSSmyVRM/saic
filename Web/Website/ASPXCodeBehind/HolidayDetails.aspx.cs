/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Drawing;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_HolidayDetails
{
    public partial class HolidayDetails : System.Web.UI.Page
    {
        #region Protected Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        MyVRMNet.LoginManagement objLogin;

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtMultiDepartment;
        protected System.Web.UI.WebControls.TextBox txtNewDepartmentName;
        protected System.Web.UI.WebControls.Table tblNoDetails;
        protected System.Web.UI.WebControls.DataGrid dgHolidayDetails;
        protected System.Web.UI.WebControls.Button btnManageOrder;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Bridges;

        protected System.Web.UI.WebControls.Button btnCreate;//FB 2670
        
        
        #endregion

        #region HolidayDetails 

        public HolidayDetails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objLogin = new MyVRMNet.LoginManagement();
        }

        #endregion

        #region Page Load 

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("HolidayDetails.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                lblHeader.Text = obj.GetTranslatedText("Manage Day Color"); //FB 2272
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    
                    //btnCreate.ForeColor = System.Drawing.Color.Gray; // FB 2796
                     //btnCreate.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                     btnCreate.Visible = false;
                }
                else
                    btnCreate.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796

                if (!IsPostBack)
                    BindData();

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }
        }
        #endregion

        #region BindData 

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetHolidayType", inXML, Application["MyVRMServer_ConfigPath"].ToString());                
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SystemHolidayType/HolidayTypes/HolidayType");
                    LoadDetailsGrid(nodes);


                    foreach (XmlNode node in nodes)
                    {
                        if(hdnValue.Value == "")
                            hdnValue.Value = node.SelectSingleNode("id").InnerText + "``" + node.SelectSingleNode("HolidayDescription").InnerText + "||";
                        else
                            hdnValue.Value += node.SelectSingleNode("id").InnerText + "``" + node.SelectSingleNode("HolidayDescription").InnerText + "||";
                    }

                    btnManageOrder.Attributes.Add("style", "display:none");

                    if (dgHolidayDetails.Items.Count > 0)
                        btnManageOrder.Attributes.Add("style", "display:block");                    
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible=true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }
        }

        #endregion

        #region LoadDetailsGrid 
        protected void LoadDetailsGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;

                    if (!dt.Columns.Contains("RowUID"))
                        dt.Columns.Add("RowUID");

                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["RowUID"] = (i + 1).ToString();
                    }

                    dgHolidayDetails.DataSource = dt;
                    dgHolidayDetails.DataBind();
                    //if (Session["Holidays"] != null)
                    //    Session["Holidays"] = nodes;
                    //else
                    //    Session.Add("Holidays", nodes);
                }
                else
                    tblNoDetails.Visible = true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadDetailsGrid" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        #endregion

        #region dgHolidayDetails_ItemDataBound
        protected void dgHolidayDetails_ItemDataBound(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    TextBox txtColor = (TextBox)e.Item.FindControl("txtColor");

                    if (txtColor != null)
                    {
                        if (txtColor.Text != "")
                            txtColor.ForeColor = ColorTranslator.FromHtml(txtColor.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("dgHolidayDetails_ItemDataBound" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }
        }
        #endregion

        #region EditDetails
        protected void EditDetails(object sender, DataGridCommandEventArgs e)
        {
            String inXML = "";
            String outXML = "";
            try
            {
                Response.Redirect("EditHolidayDetails.aspx?TypeID=" + e.Item.Cells[0].Text);
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

        #region DeleteHolidayDetails
        protected void DeleteHolidayDetails(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; 
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                inXML += "  <delete>";
                inXML += "      <typeID>" + e.Item.Cells[0].Text + "</typeID>";
                inXML += "  </delete>";
                inXML += "</login>";

                String outXML = obj.CallMyVRMServer("DeleteHolidayDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    objLogin.LoadOrgHolidays();
                    Response.Redirect("HolidayDetails.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteHolidayDetails" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        #endregion

        #region DeleteHolidayTypeMsg 

        protected void DeleteHolidayTypeMsg(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    //FB 2670
                    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                    btnDelete.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Day Color?") + "')"); //FB 2272

                    if (Session["admin"].ToString() == "3")
                    {
                        LinkButton btnEdit = ((LinkButton)e.Item.FindControl("btnEdit"));

                        btnEdit.Text = "View";

                        
                        //btnDelete.Attributes.Remove("onClick");
                        //btnDelete.Style.Add("cursor", "default");
                        //btnDelete.ForeColor = System.Drawing.Color.Gray;
                        //ZD 100263
                        btnDelete.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteHolidayTypeMsg" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        #endregion

        #region ManageTypeOrder
        protected void ManageTypeOrder(Object sender, EventArgs e)
        {
            try
            {
                String DayOrder = Request.Params["Bridges"].ToString();
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <DayOrder>";
                for (int i = 0; i < DayOrder.Split(';').Length - 1; i++)
                {
                    inXML += "      <Day>";
                    inXML += "          <Order>" + (i + 1).ToString() + "</Order>";
                    inXML += "          <DayID>" + DayOrder.Split(';')[i].ToString() + "</DayID>";
                    inXML += "      </Day>";
                }
                inXML += "  </DayOrder>";
                inXML += "</login>";

                String outXML = obj.CallMyVRMServer("SetDayOrderList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    objLogin.LoadOrgHolidays();
                    Response.Redirect("HolidayDetails.aspx?m=1");
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ManageTypeOrder" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        #endregion

        #region CreateNewHolidayDetails 
        protected void CreateNewHolidayDetails(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EditHolidayDetails.aspx");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CreateNewHolidayDetails" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }

        #endregion

    }
}
