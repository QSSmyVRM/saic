﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Web.Security;
using System.Web.Util;

namespace myVRMNet
{
    /// <summary>
    /// This is a generic class to decode and encode image files as byte array.
    /// also converts byte array into xml supporting data string.
    /// Created by: Valli.M.
    /// </summary>
    public class ImageUtil
    {
        #region Data Members
        /// <summary>
        /// Data Members
        /// </summary>

        private string errXML = "";
        private ns_Logger.Logger log;
        private byte[] bytThumb = null;
        
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ImageUtil()
        {
            log = new ns_Logger.Logger();
            errXML = "<error><message>System Error. Please contact your myVRM Administrator and supply this error code: </message><level>E</level><errorCode>444</errorCode></error>";
        }
        #endregion

        #region ConvertImageToBase64
        /// <summary>
        /// Converting an image into base64 format
        /// </summary>
        /// <param name="ImagePath"></param>
        /// <returns></returns>
        public String ConvertImageToBase64(string ImagePath)
        {
            bytThumb = null;
            string imageDataString = "";
            try
            {
                bytThumb = ConvertImageToBinary(ImagePath);
                if (bytThumb != null)
                {
                    imageDataString = ConvertByteArrToBase64(bytThumb);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
            return imageDataString;
        }
        #endregion

        #region ConvertImageToBinary
        /// <summary>
        /// Converting an image into byte array
        /// </summary>
        /// <param name="ImagePath"></param>
        /// <returns></returns>
        public byte[] ConvertImageToBinary(string ImagePath)
        {
            bytThumb = null;
            FileStream fsImage = null;
            try
            {
                if (ImagePath == null)
                    return null;

                if (ImagePath.Trim() == "")
                    return null;

                if (!File.Exists(ImagePath))
                    return null;

                fsImage = new FileStream(ImagePath, FileMode.Open, FileAccess.Read);

                bytThumb = new byte[fsImage.Length];
                fsImage.Position = 0;
                fsImage.Read(bytThumb, 0, (int)fsImage.Length);
                fsImage.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
            finally
            {
                if (fsImage != null)
                {
                    fsImage.Close();
                    fsImage.Dispose();
                    fsImage = null;
                }
            }
            return bytThumb;
        }
        #endregion

        #region ConvertByteArrToBase64
        /// <summary>
        /// Converting the bytearray into base64 string.
        /// </summary>
        /// <param name="imageData"></param>
        /// <returns></returns>
        public String ConvertByteArrToBase64(Byte[] imageData)
        {
            string imageString = "";
            try
            {
                if (imageData == null)
                    return "";

                if (imageData.Length <=0)
                    return "";

                imageString = Convert.ToBase64String(imageData);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
            return imageString;
        }
        #endregion

        #region ConvertBase64ToByteArray
        /// <summary>
        /// Converting base64 string into byte array
        /// </summary>
        /// <param name="ImagePath"></param>
        /// <returns></returns>
        public byte[] ConvertBase64ToByteArray(String imageData)
        {
            bytThumb = null;
            try
            {
                if (imageData == null)
                    return null;

                if (imageData.Trim() == "")
                    return null;

                bytThumb = new Byte[imageData.Length];
                bytThumb = Convert.FromBase64String(imageData);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
            return bytThumb;
        }
        #endregion

        #region WriteToFile
        public void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
                
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
    }
}
