/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;

/// <summary>
/// Summary description for InventoryManagement
/// </summary>
/// 
namespace ns_InventoryManagement
{
    public partial class InventoryManagement : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.Label lblMainLabel;
        protected System.Web.UI.WebControls.Button btnAddNew;
        protected System.Web.UI.WebControls.Label lblCount;
        protected System.Web.UI.WebControls.Label lblNoRecords;
        protected System.Web.UI.WebControls.DataGrid itemsGrid;
        protected System.Web.UI.WebControls.DataGrid dgInventoryList;
        protected System.Web.UI.WebControls.DropDownList lstDeliveryType;
        protected System.Web.UI.WebControls.DropDownList lstCateringServices;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNew;//FB 2670

        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        ns_Logger.Logger log;
        DataSet ds;

        //Image Project...
        String fName;
        byte[] imgArray = null;
        myVRMNet.ImageUtil imageUtilObj = null;
        string fileext;
        protected string currencyFormat = "";//FB 1830
        protected string language = "";//FB 1830
        CultureInfo cInfo = null;
        decimal tmpVal = 0;
        public InventoryManagement()
        {
            // An object for MyVRMServer calls and all other related functions.
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            imageUtilObj = new myVRMNet.ImageUtil(); //Image Project
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = string.Empty;
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());
                if (Request.QueryString["t"] != null)
                    confChkArg = Request.QueryString["t"].ToString();
                obj.AccessConformityCheck("inventorymanagement.aspx?t=" + confChkArg);
                // ZD 100263 Ends

                Session["multisiloOrganizationID"] = null; //FB 2274 
                //FB 1830 - Starts
                if (Session["CurrencyFormat"] == null)
                    Session["CurrencyFormat"] = ns_MyVRMNet.vrmCurrencyFormat.dollar;

                currencyFormat = Session["CurrencyFormat"].ToString();

                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                
                cInfo = new CultureInfo(Session["NumberFormat"].ToString());
				//FB 1830 - End

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    
                    //btnAddNew.ForeColor = System.Drawing.Color.Gray;
                    //btnAddNew.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnAddNew.Visible = false;
                }
                else
                    btnAddNew.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796

                errLabel.Visible = false;
                if (!IsPostBack)
                {
                    Session["DS"] = new DataSet();
                    obj.GetCateringServices(lstCateringServices);
                    obj.GetDeliveryTypes(lstDeliveryType);
                    if (Request.QueryString["t"] != null)
                        txtType.Text = Request.QueryString["t"].ToString();
                    String inXML = "<login><userID>" + Session["userID"] + "</userID>" + obj.OrgXMLElement() + "<Type>" + txtType.Text + "</Type></login>";//Organization Module Fixes
                    // FB 2570 Starts
                    if (txtType.Text.ToString().Equals("1"))
                    {
                        lblMainLabel.Text = obj.GetTranslatedText("Audiovisual Inventories");//FB 1830 - Translation  FB 2570
                        btnAddNew.Text = obj.GetTranslatedText("Create New Audiovisual Inventories");//FB 1830 - Translation //FB 2570
                    }
                    else if (txtType.Text.Equals("2"))
                    {
                        lblMainLabel.Text = obj.GetTranslatedText("Catering Menus");//FB 1830 - Translation  FB 2570
                        btnAddNew.Text = obj.GetTranslatedText("Create New Catering Menus");//FB 1830 - Translation  FB 2570
                    }
                    else if (txtType.Text.Equals("3"))
                    {
                        lblMainLabel.Text = obj.GetTranslatedText("Facility Services");//FB 1830 - Translation  FB 2570
                        btnAddNew.Text = obj.GetTranslatedText("Create New Facility Services");//FB 1830 - Translation  FB 2570
                    }
                    // FB 2570 Ends
                    String outXML = obj.CallMyVRMServer("GetInventoryList", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //"<InventoryList><Type>1</Type><Inventory>    <ID>1</ID>    <Name>Inventory 1</Name>    <Comments>Test Inventory 1</Comments>    <Admin>      <ID>11</ID>      <Name>Administrator Group 1</Name>      <Email>admin1@email.com</Email>    </Admin>    <Notify>1</Notify> <AssignedCostCtr>1</AssignedCostCtr><AllowOverBooking>0</AllowOverBooking><LocationList><selected><level1Name>Room 1</level1Name><level1Name>Room 2</level1Name><level1Name>Room 3</level1Name></selected></LocationList></Inventory><Inventory><ID>2</ID><Name>Inventory 2</Name><Comments>Test Inventory 2</Comments><Admin><ID>11</ID><Name>Administrator Group 2</Name><Email>admin2@email.com</Email></Admin><Notify>0</Notify><AssignedCostCtr>1</AssignedCostCtr><AllowOverBooking>0</AllowOverBooking><LocationList><selected><level1Name>Room 1</level1Name><level1Name>Room 2</level1Name><level1Name>Room 3</level1Name></selected></LocationList></Inventory></InventoryList>"; // 

                    //Response.Write(obj.Transfer(inXML) + "<br>" + obj.Transfer(outXML));
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//InventoryList/Inventory");
                        if (nodes.Count > 0)
                            BindData(nodes);
                        else
                            lblNoRecords.Visible = true;
                    }
                }
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].Equals("1"))
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    }

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }

        }

        private void BindData(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.Auto);
                }
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    UpdateDataTable(ref dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["Notify"].ToString().Equals("1"))
                            dr["Notify"] = obj.GetTranslatedText("Yes");
                        else
                            dr["Notify"] = obj.GetTranslatedText("No");
                        dr["RoomNames"] = "";
                        XmlNodeList selNodes = nodes[i].SelectNodes("locationList/selected");
                        foreach (XmlNode selNode in selNodes)
                            dr["RoomNames"] += selNode.SelectSingleNode("level1Name").InnerText + "<br>";
                        dgInventoryList.DataSource = dt;
                        dgInventoryList.DataBind();

                        foreach (DataGridItem dgi in dgInventoryList.Items)
                        {
                            if (txtType.Text.Equals("2"))
                                ((LinkButton)dgi.FindControl("btnViewDetails")).Text = obj.GetTranslatedText("View Menus");//FB 1830 - Translation
                            
                        }
                        //Response.Write(nodes.Count);
                        DataGridItem dgFooter = (DataGridItem)dgInventoryList.Controls[0].Controls[dgInventoryList.Controls[0].Controls.Count - 1];
                        Label lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text = nodes.Count.ToString();
                    }
                }
            }

            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }
        }
        protected void HideColumns(DataGrid itemsGrid)
        {
            try
            {
                //DataGridItem dgHeader = (DataGridItem)itemsGrid.Controls[0].Controls[0];
                //if (txtType.Text.Equals("3"))
                //{
                //    ((Label)dgHeader.FindControl("lblHName")).Text = "Task/Item";
                //    dgHeader.FindControl("tdHSerialNumber").Visible = false;
                //}
                //dgHeader.FindControl("tdHQuantity").Visible = false;
                //dgHeader.FindControl("tdHPortable").Visible = false;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("HideColumns" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected DataView getDeliveryTypesDataSource(String ItemID)
        {
            try
            {
                DataView charges = new DataView();
                DataView charge = new DataView();
                ds = (DataSet)Session["DS"];
                if (ds.Tables.Count > 1)                
                {
                    //FB 1830
                    DataTable temp = ds.Tables[2].Copy();

                    foreach (DataRow dr in temp.Rows)
                    {   
                        decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                        dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);
                        tmpVal = 0;

                        decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                        dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);
                        tmpVal = 0;  
                      
                        if(dr["DeliveryName"] != null)//FB 2272
                            dr["DeliveryName"] = obj.GetTranslatedText(dr["DeliveryName"].ToString());
                    }
                    
                    charges = ds.Tables["Charges"].DefaultView;
                    //charge = ds.Tables["Charge"].DefaultView;
                    charge = temp.DefaultView;

                    charges.RowFilter = "Item_Id='" + Int32.Parse(ItemID) + "'";
                    charge.RowFilter = "Charges_Id='" + Int32.Parse(charges.Table.DefaultView[0]["Charges_Id"].ToString()) + "' AND NOT(DeliveryTypeID ='0')";
                }
                return charge;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("getDeliveryTypesDataSource" + ex.Message + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
                return null;
            }
        }
        protected void UpdateDataTable(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("ID"))
                    dt.Columns.Add("ID");
                if (!dt.Columns.Contains("Name"))
                    dt.Columns.Add("Name");
                if (!dt.Columns.Contains("AdminID"))
                    dt.Columns.Add("AdminID");
                if (!dt.Columns.Contains("Notify"))
                    dt.Columns.Add("Notify");
                if (!dt.Columns.Contains("RoomNames"))
                    dt.Columns.Add("RoomNames");
                if (!dt.Columns.Contains("Comment"))
                    dt.Columns.Add("Comment");
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("UpdateDataTable" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = false;
            }
        }

        #endregion

        protected void DeleteInventory(Object sender, DataGridCommandEventArgs e)
        {
            string inXML = "", outXML = "";
            try
            {
                inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<Type>" + txtType.Text + "</Type><ID>" + ((Label)e.Item.FindControl("lblInventoryID")).Text + "</ID></login>";//Organization Module Fixes
                outXML = obj.CallMyVRMServer("DeleteInventory", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    Response.Redirect("InventoryManagement.aspx?t=" + Request.QueryString["t"] + "&m=1");
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = "Error in deleting Inventory: " + ex.StackTrace;
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;//ZD 100263
                log.Trace("DeleteInventory:" + ex.StackTrace);//FB 1881
            }
        }
        protected void EditInventory(Object sender, DataGridCommandEventArgs e)
        {
            //////// Bug Fix Case # 309///////////////////////////
            DataTable dtInventory = new DataTable();
            dtInventory.Columns.Add("Name");
            DataRow drInventory;
            foreach (DataGridItem dgi in dgInventoryList.Items)
            {
                if (null != dgi.FindControl("lblInventoryName"))
                {
                    drInventory = dtInventory.NewRow();
                    drInventory["Name"] = ((Label)dgi.FindControl("lblInventoryName")).Text;
                    dtInventory.Rows.Add(drInventory);

                }
            }
            Session["dtInventoryListgrid"] = dtInventory;
            //////// Bug Fix Case # 309///////////////////////////
            Response.Redirect("EditInventory.aspx?id=" + ((Label)e.Item.FindControl("lblInventoryID")).Text + "&t=" + txtType.Text);
        }
        protected void CreateNewInventory(Object sender, EventArgs e)
        {
            Response.Redirect("EditInventory.aspx?id=new&t=" + txtType.Text);
        }


        protected void ViewDetails(Object sender, DataGridCommandEventArgs e)
        {
            Int32 cntRows = 0; //FB 1830
            String gridID = "";//FB 1830
            try
            {
                //Response.Write(DateTime.Now);
                if (txtType.Text.Equals("2")) //Catering Module
                {
                    String inXML = "<GetProviderDetails>" + obj.OrgXMLElement() + "<ID>" + ((Label)e.Item.FindControl("lblInventoryID")).Text + "</ID><Type>" + txtType.Text + "</Type></GetProviderDetails>";//Organization Module Fixes
                    if (((DataGrid)e.Item.FindControl("dgCateringMenus")).Items.Count == 0)
                    {
                        String outXML = obj.CallMyVRMServer("GetProviderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //outXML = "<success>1</success>";
                        if (outXML.IndexOf("<error>") >= 0) //in case of error
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                        else
                        {
                            DataGrid dgCateringMenus = (DataGrid)e.Item.FindControl("dgCateringMenus");
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            //xmldoc.Load("C:\\VRM\\XML\\OutXML\\GetProviderDetails.xml");
                            XmlNodeList nodes = xmldoc.SelectNodes("//GetProviderDetails/MenuList/Menu");
                            XmlTextReader xtr;
                            DataSet ds = new DataSet();
                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                ds.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            if (ds.Tables.Count > 0)
                            {
                                DataTable dtMenu = ds.Tables[0];
                                //FB 1686
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                                    dr["Price"] = tmpVal.ToString("n", cInfo);
                                    tmpVal = 0;
                                }
                                dgCateringMenus.DataSource = dtMenu;
                                dgCateringMenus.DataBind();
                                //foreach (DataTable dt in ds.Tables)
                                //{
                                //    Response.Write("<br>Table Name: " + dt.TableName);
                                //    foreach (DataColumn dc in dt.Columns)
                                //        Response.Write("<br>" + dc.ColumnName);
                                //}
                                ds = new DataSet();
                                foreach (DataGridItem dgMenu in dgCateringMenus.Items)
                                {
                                    foreach (XmlNode node in nodes)
                                    {
                                        if (node.SelectSingleNode("ID").InnerText.Equals(dgMenu.Cells[0].Text))
                                        {
                                            ds = new DataSet();
                                            XmlNodeList subNodes = node.SelectNodes("CateringServices/Service");
                                            foreach (XmlNode subNode in subNodes)
                                            {
                                                xtr = new XmlTextReader(subNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                                ds.ReadXml(xtr, XmlReadMode.InferSchema);
                                            }
                                            if (ds.Tables.Count > 0)
                                            {
                                                if (!ds.Tables[0].Columns.Contains("Name")) ds.Tables[0].Columns.Add("Name");
                                                foreach (DataRow dr in ds.Tables[0].Rows)
                                                    dr["Name"] = lstCateringServices.Items.FindByValue(dr["ID"].ToString());
                                                ((DataGrid)dgMenu.FindControl("dgServices")).DataSource = ds.Tables[0];
                                                ((DataGrid)dgMenu.FindControl("dgServices")).DataBind();
                                            }
                                            subNodes = node.SelectNodes("ItemsList/Item");
                                            ds = new DataSet();
                                            foreach (XmlNode subNode in subNodes)
                                            {
                                                xtr = new XmlTextReader(subNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                                ds.ReadXml(xtr, XmlReadMode.InferSchema);
                                            }
                                            if (ds.Tables.Count > 0)
                                            {
                                                ((DataGrid)dgMenu.FindControl("dgMenuItems")).DataSource = ds.Tables[0];
                                                ((DataGrid)dgMenu.FindControl("dgMenuItems")).DataBind();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else //AV Inv & House Keeping
                {
                    String inXML = objInXML.GetInventoryDetails(((Label)e.Item.FindControl("lblInventoryID")).Text, "", "", "", "", "", txtType.Text, "", "");
                    String outXML;
                    if (((DataGrid)e.Item.FindControl("itemsGrid")).Items.Count == 0)
                    {
                        outXML = obj.CallMyVRMServer("GetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //Response.Write("<br>" + DateTime.Now);
                        //Response.Write(obj.Transfer(outXML));
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                        else
                        {
                            DataGrid dgTemp = (DataGrid)e.Item.FindControl("itemsGrid");
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            XmlNodeList nodes = xmldoc.SelectNodes("//Inventory/ItemList/Item");
                            XmlTextReader xtr;
                            DataSet ds = new DataSet();
                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                ds.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            DataTable dt, dtCharge;
                            if (ds.Tables.Count == 0)
                            {
                                dt = new DataTable();
                                dtCharge = new DataTable();
                            }
                            else
                            {
                                dt = ds.Tables[0];
                                if (!dt.Columns.Contains("Type")) dt.Columns.Add("Type");
                                if (!dt.Columns.Contains("Item_Id")) dt.Columns.Add("Item_Id");
                                
                                //Image Project...
                                if (!dt.Columns.Contains("ImageId")) dt.Columns.Add("ImageId");

                                //FB 1686
                                foreach (DataRow dr in dt.Rows)
                                {
                                    dr["Type"] = txtType.Text;
                                    //dr["Price"] = Decimal.Parse(dr["Price"].ToString()).ToString("0.00");
                                    //FB 1830
                                    decimal.TryParse(dr["Quantity"].ToString(), out tmpVal);
                                    dr["Quantity"] = tmpVal.ToString("n0", cInfo);
                                    tmpVal = 0;

                                    decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                                    dr["Price"] = tmpVal.ToString("n", cInfo);
                                    tmpVal = 0;

                                    decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                                    dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);
                                    tmpVal = 0;

                                    decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                                    dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);
                                    tmpVal = 0;

                                }
                                ds.Tables[0].TableName = "Items"; //Item_Id
                                if (ds.Tables.Count > 1)
                                {
                                    ds.Tables[1].TableName = "Charges"; // Item_Id, Charges_Id
                                    ds.Tables[2].TableName = "Charge"; // Charges_Id 
                                }
                            }

                            if (!txtType.Text.Equals("1"))
                            {
                                HideColumns(dgTemp);
                            }
                            Session.Add("DS", ds);
                            ///////case #274 /////////////////////
                            if (ds.Tables.Count > 0)
                                dgTemp.DataSource = ds.Tables[0];
                            dgTemp.DataBind();
                            dgTemp.Visible = true;
                        }
                    }
                }
                foreach (DataGridItem dgi in dgInventoryList.Items)
                {
                    Label lbl1 = (Label)dgi.FindControl("lblInventoryID");
                    Label lbl2 = (Label)e.Item.FindControl("lblInventoryID");
                    //Response.Write(lbl1.Text);
                    //Response.Write(lbl2.Text);
                    if (txtType.Text.Equals("2"))
                    {
                        if (!lbl1.Text.Equals(lbl2.Text))
                            ((DataGrid)dgi.FindControl("dgCateringMenus")).Visible = false;
                        else
                        {
                            ((DataGrid)dgi.FindControl("dgCateringMenus")).Visible = true;
                            cntRows = ((DataGrid)dgi.FindControl("dgCateringMenus")).Items.Count;
                            gridID = "dgCateringMenus";//FB 1830
                        }
                    }
                    else
                    {
                        if (!lbl1.Text.Equals(lbl2.Text))
                            ((DataGrid)dgi.FindControl("itemsGrid")).Visible = false;
                        else
                        {
                            ((DataGrid)dgi.FindControl("itemsGrid")).Visible = true;
                            cntRows = ((DataGrid)dgi.FindControl("itemsGrid")).Items.Count;
                            gridID = "itemsGrid";//FB 1830
                        }
                    }
                }
                
                ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>fnChangeCurrencyFormat('" + cntRows.ToString()
                    + "','" + gridID + "','" + txtType.Text + "')</script>");//FB 1830
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ViewDetails" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }

        }

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    //FB 2670
                    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDeleteInventory");
                    btnDelete.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this inventory?") + "')");//FB japnese

                    if (Session["admin"].ToString().Equals("3"))
                    {
                        LinkButton btnEdit = ((LinkButton)e.Item.FindControl("btnEditInventory"));
                        
                        //btnEdit.Attributes.Remove("onClick");
                        //btnEdit.Style.Add("cursor", "default");
                        //btnEdit.ForeColor = System.Drawing.Color.Gray;

                        
                        //btnDelete.Attributes.Remove("onClick");
                        //btnDelete.Style.Add("cursor", "default");
                        //btnDelete.ForeColor = System.Drawing.Color.Gray;
                        //ZD 100263
                        btnEdit.Visible = false;
                        btnDelete.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindRowsDeleteMessage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }

        //Image Project..
        protected void BindImages(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    string imCont = "";
                    string pathName = "resource";
                    string fullPath = "";

                    if (row["ImageName"] != null)
                        fName = row["ImageName"].ToString().Trim();

                    if (row["Image"] != null)
                        imCont = row["Image"].ToString().Trim();

                    if (txtType.Text.Trim() == "2")
                        pathName = "food";

                    if (txtType.Text.Trim() == "3")
                        pathName = "Housekeeping";

                    fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/image/" + pathName + "/" + fName;//FB 1830

                    if (imCont != "")
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imCont);

                        if (File.Exists(Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName))
                            File.Delete(Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName);

                        WriteToFile(Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName, ref imgArray);

                        Image imgContrl = (Image)e.Item.FindControl("imgItem");
                        imgContrl.ImageUrl = fullPath;

                        //myVRMWebControls.ImageControl imgCtrl = (myVRMWebControls.ImageControl)e.Item.FindControl("imgItem");

                        //MemoryStream ms = new MemoryStream(imgArray, 0, imgArray.Length);
                        //ms.Write(imgArray, 0, imgArray.Length);

                        //if (fileext == "gif")
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        //else
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        //imgCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        //imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                        //imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms);
                        //imgCtrl.Visible = true;
                        //imCont = null;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindImages" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }

        #region Write File
        /// <summary>
        /// WriteToFile
        /// </summary>
        /// <param name="strPath"></param>
        /// <param name="Buffer"></param>
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("WriteToFile" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }
        #endregion
    }
}