/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Collections; //ZD 100263
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Drawing;//ZD 100175
using System.Drawing.Imaging;//ZD 100175
using System.Drawing.Drawing2D;//ZD 100175


/// <summary>
/// Summary description for InventoryManagement
/// </summary>
/// 
namespace ns_ItemsList
{
    public partial class ItemsList : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.HiddenField selItems;
        protected System.Web.UI.WebControls.TextBox txtItemID;
        protected System.Web.UI.WebControls.TextBox txtItemName;
        protected System.Web.UI.WebControls.FileUpload fleImage;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnAddItem;
        protected System.Web.UI.WebControls.DataGrid dgItems;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSrcID;
        
        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        String fName;
        string fileext;
        String pathName;
        myVRMNet.ImageUtil imageUtilObj = null;
        byte[] imgArray = null;
        protected String language = "";//FB 1830
        ns_Logger.Logger log;//FB 1881

        public ItemsList()
        {
            obj = new myVRMNet.NETFunctions();
            imageUtilObj = new myVRMNet.ImageUtil();
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger();
            fName = "";
            pathName = "resource";
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ItemsList.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if (Session["language"] == null)//FB 1830
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                if (!IsPostBack)
                {
                    //                    fleImage.Attributes.Add("onchange", "return checkFileExtension(this);");
                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].Equals("1"))
                        {
                            errLabel.Visible = true;
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        }
                    string inXML = "<login><userID>" + Session["userID"] + "</userID>" + obj.OrgXMLElement() + "<Type>" + Request.QueryString["type"].ToString() + "</Type></login>";//Organization Module Fixes
                    string outXML = obj.CallMyVRMServer("GetItemsList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    //                    Response.Write(obj.Transfer(inXML));
                    //                    Response.Write(obj.Transfer(outXML));
                    //                    txtType.Value = Request.QueryString["type"].ToString();
                    BindData(outXML);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }

        }
        private void BindData(string outXML)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlNodeList nodes = xmldoc.SelectNodes("//ItemList/Item");
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;

                    if (Session["dtItems"] == null)
                        Session.Add("dtItems", dt);
                    else
                        Session["dtItems"] = dt;
                    
                    dgItems.DataSource = dt;
                    dgItems.DataBind();
                }
            }

            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData: " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }
        }

        #endregion

        #region btnSubmit_Click - adding the inventory item to the inv set

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                selItems.Value = "";
                foreach (DataGridItem dgi in dgItems.Items)
                {
                    if (!Request.QueryString["type"].ToString().Equals("2"))
                    {
                        if (((CheckBox)dgi.FindControl("chkSelectItem")).Checked) //Image Project
                            selItems.Value += dgi.Cells[2].Text.Trim() +":"+ dgi.Cells[3].Text.Trim() + ":" + dgi.Cells[5].Text.Trim() + ":" + dgi.Cells[1].Text.Trim() + ",";
                    }
                    else
                        if (((CheckBox)dgi.FindControl("chkSelectItem")).Checked)//ZD 100175
                            selItems.Value += dgi.Cells[5].Text + ":" + dgi.Cells[0].Text + ":" + dgi.Cells[3].Text + ":" + dgi.Cells[1].Text + ","; //Image Project
                }
                string strScript = "<script language='javascript'>";
                if (!Request.QueryString["type"].ToString().Equals("2"))
                {
                    strScript += "window.opener.document.getElementById('selItems').value+='" + selItems.Value + "';";
                    strScript += "opener.document.frmSubmit.submit();";
                    strScript += "window.close();";
                }
                else
                {
                    //strScript += "alert('" + txtSrcID.Value + "');";
                    //FB Case 853 Saima
                    strScript += "window.opener.document.getElementById('__EVENTTARGET').value = 'popup';";
                    strScript += "window.opener.document.getElementById('" + txtSrcID.Value + "').value += '" + selItems.Value + "';";
                    strScript += "opener.document.frmSubmit.submit();";
                    strScript += "window.close();";
                }
                //strScript += "alert('" + strScript + "');";
                strScript += "</script>";
                RegisterClientScriptBlock("addItems", strScript);
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("btnSubmit_Click" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        
        #endregion

        #region AddCategoryItem
        protected void AddCategoryItem(Object sender, EventArgs e)
        {
			//ZD 100175 start
            HttpPostedFile myFile;
            myFile = fleImage.PostedFile;
            System.Drawing.Image img = null;
            string filename = null;
			//ZD 100175 End
            try
            {
                if (IsValid)
                {
                    if (ConvertImage()) //Image Project
                    {
                        foreach (DataGridItem dg1 in dgItems.Items)
                        {
                            if(dg1.Cells[5].Text.ToLower() == txtItemName.Text.ToLower())
                            {
                                errLabel.Text = obj.GetTranslatedText("Item Name already exists in the list");//FB 1830 - Translation
                                errLabel.Visible = true;
                                return;
                            }
                        }
                        //ZD 100175 start
                        if (fleImage.HasFile)
                        {
                            filename = Path.GetFileName(fleImage.FileName);
                            img = System.Drawing.Image.FromStream(fleImage.PostedFile.InputStream);
                        }
                        double aspectRatio;
                        int aspectWidth, aspectHight;
                        if (img.Width > 200 || img.Height > 200)
                        {

                            if (((double)img.Width / img.Height) > (200.0 / 200.0))
                                aspectRatio = (double)img.Width / 200.0;
                            else
                                aspectRatio = (double)img.Height / 200.0;

                            aspectWidth = (int)Math.Round(img.Width / aspectRatio);
                            aspectHight = (int)Math.Round(img.Height / aspectRatio);

                        }
                        else if (img.Width < 200 || img.Height < 200)
                        {
                            if (((double)img.Width / img.Height) > (200.0 / 200.0))
                                aspectRatio = 200.0 / img.Width;
                            else
                                aspectRatio = 200.0 / img.Height;

                            aspectWidth = (int)Math.Round(img.Width * aspectRatio);
                            aspectHight = (int)Math.Round(img.Height * aspectRatio);
                        }
                        else
                        {
                            aspectWidth = img.Width;
                            aspectHight = img.Height;
                        }

                        Bitmap bmp2 = new Bitmap(200, 200, PixelFormat.Format24bppRgb);
                        bmp2.SetResolution(72, 72);

                        Graphics gfx = Graphics.FromImage(bmp2);
                        gfx.SmoothingMode = SmoothingMode.AntiAlias;
                        gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        gfx.Clear(Color.LightGray);
                        gfx.DrawImage(img, new Rectangle((int)Math.Round((double)(200 - aspectWidth) / 2), (int)Math.Round((double)(200 - aspectHight) / 2), aspectWidth, aspectHight), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                        gfx.Dispose();

                        MemoryStream ms = new MemoryStream();
                        bmp2.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] imgArray = new byte[ms.Length];
                        imgArray = ms.ToArray();
                        //ZD 100175 End
                        String inXML = "";
                        inXML += "<SetCategoryItem>";
                        inXML += obj.OrgXMLElement();//Organization Module Fixes
                        inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inXML += "<ID>" + txtItemID.Text + "</ID>";
                        inXML += "<Type>" + Request.QueryString["type"].ToString() + "</Type>";
                        inXML += "<Name>" + txtItemName.Text + "</Name>";
                        inXML += "<Image>" + Convert.ToBase64String(imgArray).ToString() + "</Image>"; //Image Project
                        inXML += "<ImageName>" + fName + "</ImageName>";

                        inXML += "</SetCategoryItem>";

                        String outXML = obj.CallMyVRMServer("SetCategoryItem", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                        if (outXML.IndexOf("<error>") < 0)
                        {
                            Response.Redirect("ItemsList.aspx?type=" + Request.QueryString["type"].ToString() + "&m=1&srcID=" + Request.QueryString["srcID"].ToString());
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                    }
                    else
                    {
                        if (errLabel.Text.Trim() == "")
                        {
                            //errLabel.Text = "Error Uploading Image. Please check your VRM Administrator.";
                            errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                            //log.Trace("AddCategoryItem:" + ex.Message);//FB 1881
                        }
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("AddCategoryItem" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        #endregion

        #region ConvertImage
        /// <summary>
        /// ConvertImage
        /// </summary>
        /// <returns></returns>
        protected bool ConvertImage()
        {
            try
            {
                HttpPostedFile myFile;
                int nFileLen;
                imgArray = null;
                                
                pathName = "resource";
                
                if (Request.QueryString["type"].ToString().Equals("2"))
                    pathName = "food";
                if (Request.QueryString["type"].ToString().Equals("3"))
                    pathName = "Housekeeping";

                //ZD 100263
                List<string> strExtension = null;
                bool filecontains = false;


                if (!fleImage.FileName.Equals(""))
                {
                    fName = Path.GetFileName(fleImage.FileName);
                    string imgtype = Path.GetExtension(fleImage.FileName);

                    //ZD 100263 Starts
                    strExtension = new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif" };
                    filecontains = strExtension.Contains(imgtype, StringComparer.OrdinalIgnoreCase);
                    if (!filecontains)
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return false;
                    }
                    //ZD 100263 End

                    myFile = fleImage.PostedFile;
                    nFileLen = myFile.ContentLength;
                    //Response.Write(nFileLen.ToString());
                    //imgArray = new byte[nFileLen];
                    //myFile.InputStream.Read(imgArray, 0, imgArray.Length);

                    //if (File.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName))
                    //{
                    //    errLabel.Text = "Image already exists.";
                    //    errLabel.Visible = true;
                    //    return false;
                    //}
                    // ZD 100175 start
                    //if (nFileLen > 5000000) // 5MB // Commented for ZD 100175
                    //if (nFileLen > 102000) // ZD 100175
                    //{
                    //    errLabel.Text = obj.GetTranslatedText("Image size is greater than 100KB. File has not been uploaded.");//FB 1830 - Translation
                    //    errLabel.Visible = true;
                    //    return false;
                    //}
                    //ZD 100175 End
                    
                }
                return true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ConvertImage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
                return false;
            }
        }
        #endregion

        #region ValidateInput
        protected void ValidateInput(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                FileUpload fleTemp = fleImage;
                if (sender.ToString().IndexOf("DataGrid") >= 0)
                    fleTemp = (FileUpload)ViewState["objImage"];
                String pattern = @"^.+\.((jpg)|(gif)|(jpeg)|(bmp))$";
                Regex check = new Regex(pattern);
                bool valid = true;
                if (fleTemp.FileName.Equals(""))
                    valid = false;
                else
                    valid = check.IsMatch(fleImage.FileName.ToLower());

                e.IsValid = valid;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ValidateInput" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
            }
        }
        #endregion

        #region Not Used - Image Project
        protected bool UploadImage()
        {
            try
            {
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData;
                String errMsg = "";
                
                pathName = "resource";
                if (Request.QueryString["type"].ToString().Equals("2"))
                    pathName = "food";

                if (Request.QueryString["type"].ToString().Equals("3"))
                    pathName = "Housekeeping";
                
                if (!fleImage.FileName.Equals(""))
                {
                    fName = Path.GetFileName(fleImage.FileName);
                    //Response.Write(HttpContext.Current.Request.MapPath(".").ToString() + "\\" + fName);
                    myFile = fleImage.PostedFile;
                    //nFileLen = myFile.ContentLength;
                    //Response.Write(nFileLen.ToString());
                    myData = new byte[imgArray.Length];
                    myFile.InputStream.Read(imgArray, 0, imgArray.Length);
                    if (imgArray.Length <= 5000000)
                        WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName, ref myData);
                    else
                        errMsg +=obj.GetTranslatedText("Image size is greater than 5MB. File has not been uploaded.");
                }
                return true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("UploadImage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
                return false;
            }
        }
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {                
                //ZD 100263
                log.Trace("WriteToFile" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }

        

        protected void EditItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtItemID.Text = e.Item.Cells[0].Text;
                if (ViewState["itemID"] == null)
                    ViewState.Add("itemID", e.Item.Cells[0].Text);
                else
                    ViewState["itemID"] = e.Item.Cells[0].Text;
                if (ViewState["objImage"] == null)
                    ViewState.Add("objImage", e.Item.FindControl("fleImage"));
                else
                    ViewState["objImage"] = e.Item.FindControl("fleImage");
                dgItems.EditItemIndex = e.Item.ItemIndex;
                //((FileUpload)e.Item.FindControl("fleImage")).Attributes.Add("value", e.Item.Cells[1].Text);
                dgItems.DataSource = Session["dtItems"];
                dgItems.DataBind();
                btnAddItem.Enabled = false;
                btnSubmit.Enabled = false;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("EditItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
            }
        }
        protected void UpdateItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtItemName.Text = ((TextBox)e.Item.FindControl("txtItemName")).Text;
                fleImage = ((FileUpload)e.Item.FindControl("fleImage"));
                AddCategoryItem(sender, (EventArgs)e);
            }
            catch (Exception ex)
            {                
                //ZD 100263
                log.Trace("UpdateItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        protected void CancelItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtItemID.Text = "new";
                dgItems.EditItemIndex = -1;
                dgItems.DataSource = Session["dtItems"];
                dgItems.DataBind();
                btnAddItem.Enabled = true;
                btnSubmit.Enabled = true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("CancelItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
            }
        }
        #endregion

        #region DeleteItem 
        protected void DeleteItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                fName = "";
                fName = e.Item.Cells[3].Text.Trim();

                String inXML = "";
                inXML += "<DeleteCategoryItem>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <CategoryItemID>" + e.Item.Cells[0].Text + "</CategoryItemID>";
                inXML += "  <ImageId>" + e.Item.Cells[2].Text + "</ImageId>";//FB 2626
                inXML += "</DeleteCategoryItem>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("DeleteCategoryItem", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = "<success>1</success>";
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    if(fName != "")
                        DeleteFile();

                    Response.Redirect("ItemsList.aspx?m=1&type=" + Request.QueryString["type"].ToString() + "&srcID=" + Request.QueryString["srcID"].ToString());
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteItem" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
            }
        }

        private void DeleteFile()
        {
            try
            {
                pathName = "resource";

                if (Request.QueryString["type"].ToString().Equals("2"))
                    pathName = "food";

                if (Request.QueryString["type"].ToString().Equals("3"))
                    pathName = "Housekeeping";

                string fullPath = HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName;

                if (File.Exists(fullPath))
                    File.Delete(fullPath);
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteFile" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Item?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindRowsDeleteMessage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        #endregion

        #region BindImages - on item databound
        /// <summary>
        /// BindImages - on item databound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BindImages(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    string imCont = "";
                    string fullPath = "";
                    fName = "";
                    
                    if (row["ImageName"] != null)
                        fName = row["ImageName"].ToString().Trim();

                    if(row["Image"] != null)
                       imCont = row["Image"].ToString().Trim();

                    pathName = "resource";

                    if (Request.QueryString["type"].ToString().Equals("2"))
                        pathName = "food";

                    if (Request.QueryString["type"].ToString().Equals("3"))
                        pathName = "Housekeeping";

                    fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/image/" + pathName + "/" + fName;//FB 1830
                    
                    if (imCont != "")
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imCont);

                        if (File.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName))
                            File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName);

                        WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\"+ pathName +"\\" + fName, ref imgArray);
                        System.Web.UI.WebControls.Image imgContrl = (System.Web.UI.WebControls.Image)e.Item.FindControl("itemImage");//ZD 100175
                        imgContrl.ImageUrl = fullPath;

                        //myVRMWebControls.ImageControl imgCtrl = (myVRMWebControls.ImageControl)e.Item.FindControl("itemImage");

                        //MemoryStream ms = new MemoryStream(imgArray, 0, imgArray.Length);
                        //ms.Write(imgArray, 0, imgArray.Length);

                        //if (fileext == ns_MyVRMNet.vrmImageFormattype.gif)
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        ////else if ((fileext == ns_MyVRMNet.vrmImageFormattype.jpg) || (fileext == ns_MyVRMNet.vrmImageFormattype.jpeg))
                        //else
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        //imgCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        ////Map2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                        //imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms);
                        
                        //imCont = null;
                        //imgCtrl.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindImages" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        #endregion

        
    }
}
