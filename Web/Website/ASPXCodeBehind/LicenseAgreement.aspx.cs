/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.IO;

public partial class en_LicenseAgreement : System.Web.UI.Page
{
    #region Protected Data Members

    protected System.Web.UI.WebControls.Button BtnReset;
    protected System.Web.UI.WebControls.Button BtnSubmit;
    protected System.Web.UI.HtmlControls.HtmlControl ifrmlocation;

    #endregion

    #region  Variables Declaration

    private myVRMNet.NETFunctions obj = null;
    private ns_Logger.Logger log;   
    String outXML = "";
    String strXML = "";
    //XmlDocument xmldoc = null;
    //String outXMLVal = "";
    //Added for Accept License start
	//FB 2450
    //public msxml4_Net.DOMDocument40Class xmldoc = null;
    public XmlDocument xmldoc = null;
    //msxml4_Net.IXMLDOMNode node = null;
    //Added for Accept License End
    protected String language = "";//FB 1830

    #endregion

    //FB 2337
    #region en_LicenseAgreement
    /// <summary>
    /// en_LicenseAgreement
    /// </summary>
    public en_LicenseAgreement()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();          
    }
    #endregion

    #region Page Load  Event Hander
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            if (Session["roomCascadingControl"] == null)
                Response.Redirect("genlogin.aspx");
            obj.AccessandURLConformityCheck("licenseagreement.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            //FB 1830 - Starts
            if (Session["language"] == null)
                Session["language"] = "en";

            if (Session["language"].ToString() != "")
                language = Session["language"].ToString();
            //FB 1830 - End
            //FB 2337 start
            if (ifrmlocation != null)
                ifrmlocation.Attributes["src"] = "license/license.htm";

            if(!IsPostBack)
             LoadLicenseAgreement();
            
            //FB 2337 end
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion   
   
    #region AcceptLicense

    protected void AcceptLicense(Object sender, EventArgs e)
    {
		//FB 2450
        //Added for Accept License start
        //xmldoc = new msxml4_Net.DOMDocument40Class();
        xmldoc = new XmlDocument();
        //Added for Accept License End
        try
        {
            strXML = BuildXML();

            if (strXML != "")
            {
                //xmldoc = new XmlDocument();
                xmldoc.LoadXml(strXML); //FB 2450
                // FB 2027 Start
                //Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
                outXML = obj.CallMyVRMServer("SetOldUser", strXML, Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = obj.CallCOM("SetOldUser", strXML, Application["COM_ConfigPath"].ToString());
                //FB 2027 Ends
                if (outXML.IndexOf("<error>") < 0)
                {
                    //FB 1779 start
                    if (Session["isExpressUser"] != null)
                    {
                        if (Session["isExpressUser"].ToString() == "1")
                            Response.Redirect("ExpressConference.aspx?t=n");
                    }
                    //FB 1779 end
                    Response.Redirect("SettingSelect2.aspx?c=GH"); //Login Management
                    
				}
                else
                {
                    Session["errMsg"] = outXML;
                    Session["outXML"] = outXML;
                    Response.Redirect("~/en/genlogin.aspx"); //Login Management //FB 1830
                }
            }
           
        }
        catch (Exception ex)
        {
            log.Trace("AcceptLicense" + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region RejectLicense

    protected void RejectLicense(Object sender, EventArgs e)
    {
        try
        {
            Session["errMsg"] = "";
            Session["outXML"] = "";
            Session.Abandon();//Login Management
            Session.RemoveAll();
            Response.Redirect("~/en/genlogin.aspx");//FB 1830
        }
        catch (Exception ex)
        {
            log.Trace("RejectLicense" + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Build XML

    private String BuildXML()
    {
        try
        {
            //FB 2027 Start
            StringBuilder inXML = new StringBuilder();
            //String inXML = "";
            inXML.Append("<login>");
            inXML.Append("<userid>" + Request.QueryString["a"].ToString() + "</userid>");
            inXML.Append("<userPassword>" + Request.QueryString["p"].ToString() + "</userPassword>");
            inXML.Append("<homeURL>" + Request.QueryString["u"].ToString() + "</homeURL>");
            inXML.Append("</login>");            
            return inXML.ToString();
            //FB 2027 Ends

           
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    //FB 2337
    #region LoadLicenseAgreement
    /// <summary>
    /// LoadLicenseAgreement
    /// </summary>
    private void LoadLicenseAgreement()
    {
        try
        {
            if (Request.QueryString["a"] == null)
            {
                log.Trace("User ID Not Found in Query string");
                Response.Redirect("genlogin.aspx");
            }

            String inXML = "", outXML = "";
            inXML = "<GetOrgLicenseAgreement>"
                  + "<userid>" + Request.QueryString["a"].ToString() + "</userid>"
                  + "</GetOrgLicenseAgreement>";
            outXML = obj.CallMyVRMServer("GetOrgLicenseAgreement", inXML, Application["MyVRMServer_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") < 0)
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(outXML);
                
                if (xd.SelectSingleNode("//OrgLicenseAgreement/LicenseAgreement") != null)
                    if (xd.SelectSingleNode("//OrgLicenseAgreement/LicenseAgreement").InnerXml.Trim() != "")
                    {
                        String path = Server.MapPath(".") + "\\license\\licenseAgreement.htm";
                        File.WriteAllText(path, xd.SelectSingleNode("//OrgLicenseAgreement/LicenseAgreement").InnerXml.Trim().Replace("�", "&"));
                        if (ifrmlocation != null)
                         ifrmlocation.Attributes["src"] = "license/licenseAgreement.htm";
                    }
            }

        }
        catch (Exception ex)
        {
            log.Trace("LoadLicenseAgreement" + ex.Message);
        }
    }
    #endregion

}
