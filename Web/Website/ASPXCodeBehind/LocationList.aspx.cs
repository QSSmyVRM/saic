/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System.IO;
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_MyVRM
{
    public partial class LocationList : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.RadioButtonList lstRoomSelection;
        protected System.Web.UI.WebControls.TreeView treeRoomSelection;
        protected System.Web.UI.WebControls.RadioButtonList rdSelView;
        protected System.Web.UI.WebControls.Panel pnlLevelView;
        protected System.Web.UI.WebControls.Panel pnlListView;
        protected System.Web.UI.WebControls.Panel pnlNoData; //Added For Location Issues
        protected System.Web.UI.WebControls.Button btnClose;//Added For Location Issues
        protected System.Web.UI.WebControls.Button btnSubmit;//Added For Location Issues
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        public LocationList()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("LocationList.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263


                if (!IsPostBack)
                    GetLocations(Request.QueryString["roomID"].ToString());
                //Added for Location Issues-- Start
                if (treeRoomSelection.Nodes.Count == 0 && lstRoomSelection.Items.Count == 0)
                {
                    rdSelView.Enabled = false;
                    pnlNoData.Visible = true;
                    pnlListView.Visible = false;
                    pnlLevelView.Visible = false;
                    btnClose.Visible = true;
                    btnSubmit.Visible = false;
                }
                //Added for Location Issues-- End
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }

        protected void GetLocations(String RoomID)
        {
            try
            {
                String inXML = "<GetLocationsList>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></GetLocationsList>";//Organization Module Fixes
                String outXML = obj.CallMyVRMServer("GetLocationsList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//level3List/level3");
                TreeNode tnTop;

                if (nodes.Count > 0)
                {
                    tnTop = new TreeNode("All Rooms", "0");
                    tnTop.SelectAction = TreeNodeSelectAction.None;
                    foreach (XmlNode node3 in nodes)
                    {
                        //Response.Write("<br>" + node3.SelectSingleNode("level3Name").InnerText + " : " + node3.SelectNodes("level2List/level2/level1List/level1").Count);
                        if (node3.SelectNodes("level2List/level2/level1List/level1").Count > 0)
                        {
                            TreeNode tn3 = new TreeNode(node3.SelectSingleNode("level3Name").InnerText, node3.SelectSingleNode("level3ID").InnerText);
                            if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                                if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                                {
                                    if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                                    {
                                        if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 2)//Organization Module Fixes
                                            tn3.Expanded = true;
                                    }
                                }
                                else
                                    tn3.Expanded = false;
                            tnTop.ChildNodes.Add(tn3);
                            XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");
                            tn3.SelectAction = TreeNodeSelectAction.None;
                            foreach (XmlNode node2 in nodes2)
                            {
                                TreeNode tn2 = new TreeNode(node2.SelectSingleNode("level2Name").InnerText, node2.SelectSingleNode("level2ID").InnerText);
                                tn2.SelectAction = TreeNodeSelectAction.None;
                                if (node2.SelectNodes("level1List/level1").Count > 0)
                                {
                                    tn3.ChildNodes.Add(tn2);
                                    XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                    foreach (XmlNode node1 in nodes1)
                                    {
                                        if (Convert.ToInt32(node1.SelectSingleNode("disabled").InnerText) != 1) //fb 1438
                                        {
                                            TreeNode tn1 = new TreeNode(node1.SelectSingleNode("level1Name").InnerText, node1.SelectSingleNode("level1ID").InnerText);
                                            tn1.ToolTip = node1.SelectSingleNode("level1ID").InnerText;
                                            tn1.NavigateUrl = @"javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');";
                                            tn2.ChildNodes.Add(tn1);
                                            ListItem li = new ListItem("<a href=\"javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');\">" + node1.SelectSingleNode("level1Name").InnerText + "</a>", node1.SelectSingleNode("level1ID").InnerText);
                                            lstRoomSelection.Items.Add(li);
                                        }
                                    }
                                }
                                if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                                    if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                                    {
                                        if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                                        {
                                            if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 3)//Organization Module Fixes
                                                tn2.Expanded = true;
                                        }
                                    }
                                    else
                                        tn2.Expanded = false;
                            }
                        }
                    }
                    treeRoomSelection.Nodes.Add(tnTop);
                }
                else
                    tnTop = new TreeNode("You have no rooms available", "-1");
                //FB Case 1056 - Saima starts here 
                for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                    for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                        if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                        {
                            ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                            lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                            log.Trace(i + " : " + lstRoomSelection.Items[i].Text.Substring(lstRoomSelection.Items[i].Text.IndexOf(">")) + " : " + lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[j].Value = liTemp.Value;
                            lstRoomSelection.Items[j].Text = liTemp.Text;
                        }
                //FB Case 1056 - Saima ends here 

                //fogbugz case 466: Saima starts here
                if (Session["roomExpandLevel"].ToString().ToUpper().Equals("LIST"))//Organization Module Fixes
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
                //fogbugz case 466: Saima ends here
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("GetLocations" + ex.StackTrace);
            }
        }
        protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdSelView.SelectedIndex.Equals(1))
                {
                    pnlListView.Visible = true;
                    pnlLevelView.Visible = false;
                    lstRoomSelection.ClearSelection();
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                            if (tn.Value.Equals(lstItem.Value) && (tn.Depth.Equals(3)))
                                lstItem.Selected = true;
                    }
                }
                else
                {
                    pnlLevelView.Visible = true;
                    pnlListView.Visible = false;
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                tn.Checked = false;
                                foreach (ListItem lstItem in lstRoomSelection.Items)
                                    if ((lstItem.Selected == true) && (tn.Value.Equals(lstItem.Value)))
                                        tn.Checked = true;
                            }
                }
            }
            catch (Exception ex)
            {
                log.Trace("rdSelView_SelectedIndexChanged: Error changing view." + ex.Message);
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                //errLabel.Text = "Error changing view. Please contact your VRM Administrator.";
                errLabel.Visible = true;
            }
        }
        protected void SetRoom(Object sender, EventArgs e)
        {
            try
            {
                String selRoomID = "";
                String selRoomName = "";
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    selRoomID = tn.Value;
                    selRoomName = tn.Text;
                }
                String strJavaScript = "<script language='javascript'>";
                strJavaScript += "opener.document.getElementById('hdnLocation').value ='" + selRoomID + "';";
                strJavaScript += "opener.document.getElementById('txtLocation').value ='" + selRoomName + "';";
                strJavaScript += "window.close();";
                strJavaScript += "</script>";
                RegisterClientScriptBlock("SetRoom", strJavaScript);
            }
            catch (Exception ex)
            {                
                //ZD 100263
                errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
    }
}