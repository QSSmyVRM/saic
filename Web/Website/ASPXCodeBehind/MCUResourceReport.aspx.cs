/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Reports
{
    public partial class MCUResourceReport : System.Web.UI.Page
    {
        #region Front-end Controls
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblNoReports;
        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DataGrid dgConferences;
        protected System.Web.UI.WebControls.TextBox txtDateFrom;
        protected System.Web.UI.WebControls.TextBox txtDateTo;
        protected System.Web.UI.WebControls.Button btnGenerateReport; //Added for FB 1428

        #endregion 
        //Code added by Offshore for FB Issue 1073 -- Start
        protected String timeZone = "0";
        String tformat = "hh:mm tt";
        protected String format = "";
        //Code added by Offshore for FB Issue 1073 -- End
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;       
        public MCUResourceReport()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        public void Page_Load(Object sender, EventArgs e)
        {
            try 
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("MCUResourceReport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                /* *** Code added for FB 1428 QA Bug -Start *** */

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    btnGenerateReport.Text = "Search Hearings";
                }
                /* *** Code added for FB 1428 QA Bug -End *** */
                errLabel.Text = "";  //FB 1105
                //Code added by Offshore for FB Issue 1073 -- Start
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                //Code added by Offshore for FB Issue 1073 -- End
                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");


                if (Session["timeFormat"].ToString().Equals("2")) //FB 2588 
                    tformat = "HHmmZ";

                /* **** Code added for FB 1425 *** */
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }
               
                //Code added by Offshore for FB Issue 1073 -- End
                if (!IsPostBack)
                {
                    obj.BindBridges(lstBridges); //Get the list of all bridges in the system and store it in a hidden dropdown
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //txtDateFrom.Text = DateTime.Now.ToString("MM/dd/yyyy"); //Assign default values in date range boxes from today to 7 days 
                    //txtDateTo.Text = DateTime.Now.Add(new TimeSpan(7, 0, 0, 0)).ToString("MM/dd/yyyy");
                    txtDateFrom.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now); //Assign default values in date range boxes from today to 7 days 
                    txtDateTo.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now.AddDays(7));
                    //Code changed by Offshore for FB Issue 1073 -- End 
                }
            }
            catch (Exception ex)
            {

                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Page_Load" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }

            
        }
        public void SearchConference(Object sender, EventArgs e)
        {
            if (ValidateDates()) //first validate the dates if they are within 7 days range
            {
                //FB 2027 McuUsage Start
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confSearch>");
                inXML.Append("<confInfo>");
                inXML.Append("<confDate>");
                inXML.Append("<type>5</type>");
                //Code changed by Offshore for FB Issue 1073 -- Start
                //inXML += "              <from>" + txtDateFrom.Text + "</from>";
                //inXML += "              <to>" + txtDateTo.Text + "</to>";
                inXML.Append("<from>" + myVRMNet.NETFunctions.GetDefaultDate(txtDateFrom.Text) + "</from>");
                inXML.Append("<to>" + myVRMNet.NETFunctions.GetDefaultDate(txtDateTo.Text) + "</to>");
                //Code changed by Offshore for FB Issue 1073 -- End
                inXML.Append("</confDate>");
                inXML.Append("<confName></confName>");
                inXML.Append("<confUniqueID></confUniqueID>");
                inXML.Append("<confHost></confHost>");
                inXML.Append("<pending>0</pending>");
                inXML.Append("<public>0</public>");
                inXML.Append("</confInfo>");
                inXML.Append("<confResource>");
                inXML.Append("<confParticipant></confParticipant>");
                inXML.Append("<confRooms>");
                inXML.Append("<type>1</type>");
                inXML.Append("</confRooms>");
                inXML.Append("</confResource>");
                inXML.Append("</confSearch>");
                inXML.Append("</login>");
                log.Trace("McuUsage: " + inXML.ToString());
                String outXML = obj.CallMyVRMServer("McuUsage", inXML.ToString(), Application["MyVRMServer_Configpath"].ToString());
                //FB 2027 McuUsage End
                log.Trace(outXML);
                if (outXML.IndexOf("<error>") >= 0) //if search returns an error the display it otherwise goto else
                {
                    dgConferences.Visible = false;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//account/conferences/conference");
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    //Response.Write(nodes.length);
                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        if (!dt.Columns.Contains("confTimezone"))
                            dt.Columns.Add("confTimezone");
                        if (!dt.Columns.Contains("confDuration"))
                            dt.Columns.Add("confDuration");
                        foreach (DataRow dr in dt.Rows)
                        {
                            if ((Int32.Parse(dr["duration"].ToString()) / 60) > 0)
                                dr["confDuration"] = Int32.Parse(dr["duration"].ToString()) / 60 + " hrs " + (Int32.Parse(dr["duration"].ToString()) % 60) + " mins";
                            else
                                dr["confDuration"] = (Int32.Parse(dr["duration"].ToString()) % 60) + " mins";
                            String txtTemp = dr["confTime"].ToString();
                            dr["confTime"] = DateTime.Parse((txtTemp.Split(' ')[0] + " " + txtTemp.Split(' ')[1])).ToString(tformat);
                            
                            //Modified for FB 1906 - Start
                            if (tformat == "HH:mm")
                            dr["confTimezone"] = ((timeZone == "1") ? txtTemp.Remove(0, dr["confTime"].ToString().Length + 3).Trim() : "");//FB 1426
                            else
                            dr["confTimezone"] = ((timeZone == "1") ? txtTemp.Remove(0, dr["confTime"].ToString().Length + 1).Trim() : "");//FB 1426
                            //Modified for FB 1906 - End

                            //Code added by Offshore for FB Issue 1073 -- Start
                            dr["confDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["confDate"].ToString());
                            //Code added by Offshore for FB Issue 1073 -- End
                        }
                        dgConferences.DataSource = dt;
                        dgConferences.DataBind(); //all the conferences returned by COM will get bind here to the data grid.
                        foreach (DataGridItem dgi in dgConferences.Items)
                        {
                            ((LinkButton)dgi.FindControl("btnView")).Attributes.Add("onclick", "javascript:viewconf('" + dgi.Cells[0].Text + "');return false;");
                            GetBridgeResources(dgi); // after binding the conferences to the datagrid, loop through all the items and then calculate the resources
                        }
                        lblNoReports.Visible = false;
                    }
                    else
                        lblNoReports.Visible = true; //if there are no conferences found then display the message
                }
            }
            else
            {
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText("Please enter valid date range of at most a week.");//FB 1830 - Translation
            }
        }

        /**************************************************************************
         * This method will calculate all the audio and video resources for 
         * that particular conference for all the bridges in the system. since
         * some conferences can go cascade with multiple bridges.
         * ************************************************************************/
        protected void GetBridgeResources(DataGridItem dgi)
        {
            try
            {
                int[] countV, countA;
                countV = new int[lstBridges.Items.Count];
                countA = new int[lstBridges.Items.Count];
                //This DataTable is created to display Yes/No for sufficient Audio and Video ports for that bridge under a conference.
                DataTable dt = new DataTable();
                dt.Columns.Add("BridgeID");
                dt.Columns.Add("BridgeName");
                dt.Columns.Add("AudioPorts");
                dt.Columns.Add("VideoPorts");
                // GetAdvancedAVSettings gets called to get all the endpoint information for a conference.
                String inXMLConf = "<GetAdvancedAVSettings><UserID>" + Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<ConfID>" + dgi.Cells[0].Text + "</ConfID></GetAdvancedAVSettings>";//Organization Module Fixes
                String outXMLConf = obj.CallMyVRMServer("GetAdvancedAVSettings", inXMLConf, Application["MyVRMServer_Configpath"].ToString());
                //Response.Write(obj.Transfer(outXMLConf)+"<hr>");
                //if ASPIL does not return any error then proceed.
                if (outXMLConf.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXMLConf);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetAdvancedAVSettings/Endpoints/Endpoint");
                    //For all the endpoints, we check if it is a User. Endpoint can either be a user or a room.
                    foreach (XmlNode node in nodes)
                    {
                        //if (node.SelectSingleNode("Type").InnerText.ToString().ToUpper().Equals("U"))
                        {
                            for (int i = 0; i < lstBridges.Items.Count; i++)
                            {
                                //if the bridge ID is same as the one for which we are checking against and the type is Audio/Video (3)
                                if (lstBridges.Items[i].Value.Equals(node.SelectSingleNode("BridgeID").InnerText) && node.SelectSingleNode("Connection").InnerText.Equals("3"))
                                    countV[i]++;
                                //if the bridge ID is same as the one for which we are checking against and the type is Audio-Only (2)
                                if (lstBridges.Items[i].Value.Equals(node.SelectSingleNode("BridgeID").InnerText) && node.SelectSingleNode("Connection").InnerText.Equals("2"))
                                    countA[i]++;
                            }
                        }
                    }
                    // Generate a dropdown box for both audio and video ports used by this conference for all bridges
                    for (int i = 0; i < countV.Length; i++)
                        ((DropDownList)dgi.FindControl("lstBridgeCountV")).Items.Add(new ListItem(countV[i].ToString(), lstBridges.Items[i].Value));
                    for (int i = 0; i < countV.Length; i++)
                        ((DropDownList)dgi.FindControl("lstBridgeCountA")).Items.Add(new ListItem(countA[i].ToString(), lstBridges.Items[i].Value));
                }
                // for all bridges we need to call the command to get the available Audio/Video ports to match with the dropdown that we just created in above steps.
                for(int i=0;i<lstBridges.Items.Count;i++)
                {
                    if (!lstBridges.Items[i].Value.Equals("-1"))
                    {
                        DataRow dr = dt.NewRow();
                        dr["BridgeID"] = lstBridges.Items[i].Value;
                        dr["BridgeName"] = lstBridges.Items[i].Text;
                        String inXML = "";
                        inXML += "<GetMCUAvailableResources>";
                        inXML += obj.OrgXMLElement();//Organization Module Fixes
                        inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                        inXML += "  <BridgeID>" + dr["BridgeID"].ToString() + "</BridgeID>";
                        inXML += "  <ConfID>" + dgi.Cells[0].Text + "</ConfID>";
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        //inXML += "  <StartDate>" + ((Label)dgi.FindControl("lblConfStartDate")).Text + "</StartDate>";
                        inXML += "  <StartDate>" + myVRMNet.NETFunctions.GetDefaultDate(((Label)dgi.FindControl("lblConfStartDate")).Text) + "</StartDate>";
                        //Code changed by Offshore for FB Issue 1073 -- End
                        inXML += "  <StartTime>" + ((Label)dgi.FindControl("lblConfStartTime")).Text + "</StartTime>";
                        inXML += "  <Duration>" + dgi.Cells[1].Text + "</Duration>";
                        inXML += "</GetMCUAvailableResources>";
                        log.Trace("GetMCUAvailableResources: " + inXML);
                        String outXML = obj.CallMyVRMServer("GetMCUAvailableResources", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        log.Trace("GetMCUAvailableResources: " + outXML);
                        //String outXML = "";
                        //outXML += "<MCUResources>";
                        //outXML += "  <AudioVideo>";
                        //outXML += "     <Available>1</Available>";
                        //outXML += "     <Total>10</Total>";
                        //outXML += "  </AudioVideo>";
                        //outXML += "  <AudioOnly>";
                        //outXML += "     <Available>2</Available>";
                        //outXML += "     <Total>10</Total>";
                        //outXML += "  </AudioOnly>";
                        //outXML += "</MCUResources>";
                        
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            XmlDocument xmldoc1 = new XmlDocument();
                            xmldoc1.LoadXml(outXML);
                            // based on available numbers, we display yes or no against each bridge name
                            if ((Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText) < 0))// - Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText)) >= countA[i]) FB 1937
                            {
                                dr["AudioPorts"] = obj.GetTranslatedText("No");
                                if ((Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText) <= -1000))// - Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText)) >= countA[i]) FB 1937
                                {
                                    dr["AudioPorts"] = obj.GetTranslatedText("N/A");
                                }
                            }
                            else
                            {
                                dr["AudioPorts"] = obj.GetTranslatedText("Yes");
                            }

                            if ((Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioVideo/Available").InnerText) < 0))// Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioVideo/Available").InnerText)) >= countV[i]) FB 1937
                            {
                                dr["VideoPorts"] = obj.GetTranslatedText("No");
                            }
                            else
                            {
                                dr["VideoPorts"] = obj.GetTranslatedText("Yes");
                            }
                            //FB 1937 end

                            //if ((Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioVideo/Total").InnerText) - Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioVideo/Available").InnerText)) >= countV[i])
                            //    dr["VideoPorts"] = "Yes";
                            //else
                            //    dr["VideoPorts"] = "No";
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                        dt.Rows.Add(dr);
                    }
                }
                ((DataGrid)dgi.FindControl("dgBridgeResources")).DataSource = dt;
                ((DataGrid)dgi.FindControl("dgBridgeResources")).DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("GetBridgeResources" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        /*************************************************************************
         * This function validates dates entered by user for search conference
         * the dates should not span more than a week and should be valid dates.
         * ***********************************************************************/
        protected bool ValidateDates()
        {
            try
            {
                //Code added by Offshore for FB Issue 1073 -- Start
                //DateTime dateFrom = DateTime.Parse(txtDateFrom.Text);
                //DateTime dateTo = DateTime.Parse(txtDateTo.Text);
                DateTime dateFrom = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtDateFrom.Text));
                DateTime dateTo = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtDateTo.Text));
                //Code added by Offshore for FB Issue 1073 -- Start
                if (dateTo.Subtract(dateFrom).Days > 7 || dateTo.Subtract(dateFrom).Days < 0)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText("Invalid Date."); //FB 2272
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }
    }
}