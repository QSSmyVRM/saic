/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Globalization;
using DevExpress.Web.ASPxEditors;

namespace ns_ManageBatchReport
{
    public partial class ManageBatchReport : System.Web.UI.Page
    {
        #region Protected Data Members

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.HtmlControls.HtmlGenericControl UserReportsCell1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl UserReportsCell2;
        protected System.Web.UI.WebControls.TextBox txtJob;
        protected System.Web.UI.WebControls.TextBox txtEmail;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDateList;
        protected System.Web.UI.HtmlControls.HtmlGenericControl DateRow1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl DateRow2;
        protected System.Web.UI.HtmlControls.HtmlGenericControl DateRowEnd1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl DateRowEnd2;


        protected System.Web.UI.HtmlControls.HtmlTableRow CustomRow;
        //protected System.Web.UI.HtmlControls.HtmlTableRow tdSpace;
        protected System.Web.UI.WebControls.ListBox CustomDate;

        protected DevExpress.Web.ASPxEditors.ASPxComboBox drpJobName; //FB 2410
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstDailyMonthly;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox ReportsList;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox DrpAllType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstUsageReports;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstUserType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstRoomType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstConfRoomRpt;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstStatusType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox ConfScheRptDivList;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstResourseType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstUserReports;

        protected DevExpress.Web.ASPxEditors.ASPxDateEdit startDateedit;
        protected DevExpress.Web.ASPxEditors.ASPxDateEdit endDateedit;
        protected DevExpress.Web.ASPxEditors.ASPxDateEdit dateFrom;
        protected DevExpress.Web.ASPxEditors.ASPxDateEdit dateTo;
        protected DevExpress.Web.ASPxEditors.ASPxRadioButtonList rblFrequency;

        protected System.Web.UI.WebControls.ImageButton delJobName;  //FB 2410
        protected System.Web.UI.HtmlControls.HtmlGenericControl divCustom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCompareDate;

        #endregion

        #region protected data members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        MyVRMNet.LoginManagement objLogin;

        int crossAccess = 0, admin = 0;
        protected String format = "dd/MM/yyyy";
        int usrID = 11;
        DataSet ds = null;
        protected Int32 CustomSelectedLimit = 100;
        Int32 workingHours = 0;
        string strParam = "";
        String pivotInput = "";
        String SelectedYears = "";
        Int32 selectedYear = DateTime.Now.Year, monthweekCount = 0;
        DataSet ds1 = new DataSet();

        #endregion

        #region ManageBatchReport

        public ManageBatchReport()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objLogin = new MyVRMNet.LoginManagement();
        }

        #endregion

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("managebatchreport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                startDateedit.MinDate = DateTime.Today;//FB 2410 
                endDateedit.MinDate = DateTime.Today;//FB 2410 

                hdnCompareDate.Value = System.DateTime.Now.Date.ToShortDateString();
                
               
                lblHeader.Text = obj.GetTranslatedText("Manage Batch Report");
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }

                if (Session["userID"] != null)
                {
                    if (Session["userID"].ToString() != "")
                        Int32.TryParse(Session["userID"].ToString(), out usrID);
                }

                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString() != "")
                        Int32.TryParse(Session["admin"].ToString(), out admin);
                }

                if (Session["UsrCrossAccess"] != null)
                {
                    if (Session["UsrCrossAccess"].ToString() != "")
                        Int32.TryParse(Session["UsrCrossAccess"].ToString(), out crossAccess);
                }

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                if (Session["WorkingHours"] != null)
                {
                    if (Session["WorkingHours"].ToString() != "")
                        Int32.TryParse(Session["WorkingHours"].ToString(), out workingHours);
                }
               
                startDateedit.DisplayFormatString = format;
                startDateedit.EditFormatString = format;

                endDateedit.DisplayFormatString = format;
                endDateedit.EditFormatString = format;

                dateFrom.DisplayFormatString = format;
                dateFrom.EditFormatString = format;

                dateTo.DisplayFormatString = format;
                dateTo.EditFormatString = format;

                //if (format == "dd/MM/yyyy")
                //    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB", true);
                //else
                //    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);
                 
                if (!IsPostBack)
                {

                    delJobName.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this job name?") + "')");
                    if (admin == 2)
                    {
                        ReportsList.Items.Add(obj.GetTranslatedText("Organization Reports"), "6");

                    }
                    BindReports();
                    DisplayControls();
                    rblFrequency.Items[0].Selected = true;
                    delJobName.Visible = false;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Page Load" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region BindReports

        private void BindReports()
        {
            String outXML = "";
            String inXML = "";
            XmlDocument xmlDoc = null;
            try
            {

                String inxml = "<GetAllBatchReports>" + obj.OrgXMLElement();
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "</GetAllBatchReports>";

                outXML = obj.CallMyVRMServer("GetAllBatchReports", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));


                    drpJobName.Items.Clear();

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[1];
                        drpJobName.Items.Add(obj.GetTranslatedText("Please select...."), -1);

                        //    drpJobName.Items.Add(new ListItem("Please Select", "-1")); //fb 2410
                        for (Int32 i = 0; i < dt.Rows.Count; i++)
                            drpJobName.Items.Add(dt.Rows[i]["JobName"].ToString(), dt.Rows[i]["ID"].ToString());
                        //drpJobName.Items.Add(new ListItem(dt.Rows[i]["JobName"].ToString(), dt.Rows[i]["ID"].ToString()));

                        Session["BatchReports"] = dt;
                    }
                    else
                        drpJobName.Items.Add(obj.GetTranslatedText("No Items..."), -1);
                    // drpJobName.Items.Add(new ListItem("No Items", "-1"));
                    drpJobName.SelectedIndex = 0;
                   
                }
                if (drpJobName.SelectedIndex == -1)
                {
                    rblFrequency.Items[0].Selected = true;
                    delJobName.Visible = false;
                    
                                      
                 } //FB 2410
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindReports" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region DisplayControls
        /// <summary>
        /// DisplayControls
        /// </summary>
        protected void DisplayControls()
        {
            try
            {
                string[] mary = Session["sMenuMask"].ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int topMenu = Convert.ToInt32(ccary[1]);
                int adminMenu = Convert.ToInt32(mmary[0].Split('*')[1]);
                int subMenu;
                int orgMenu = Convert.ToInt32(mmary[1].Split('*')[1]);

                UserReportsCell1.Attributes.Add("Style", "display:block");
                UserReportsCell2.Attributes.Add("Style", "display:block");

                if (!Convert.ToBoolean(topMenu & 32))
                {
                    if (ReportsList.Items.FindByValue("5") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("5"));
                }
                else
                {
                    if (lstUsageReports.Items.FindByValue("1") == null)
                        lstUsageReports.Items.Add(obj.GetTranslatedText("Aggregate Usage"), "1");

                    if (DrpAllType.Items.FindByValue("1") == null)
                        DrpAllType.Items.Add(obj.GetTranslatedText("Conference Type"), "1");
                }

                subMenu = Convert.ToInt32(mmary[3].Split('*')[1]);
                if (!Convert.ToBoolean(subMenu & 2))
                {
                    UserReportsCell1.Attributes.Add("Style", "display:none");
                    UserReportsCell2.Attributes.Add("Style", "display:none");
                    if (ReportsList.Items.FindByValue("2") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("2"));
                }
                else
                {
                    if (lstUsageReports.Items.FindByValue("2") == null)
                        lstUsageReports.Items.Add(obj.GetTranslatedText("Usage by Room"), "2");

                    if (DrpAllType.Items.FindByValue("2") == null)
                        DrpAllType.Items.Add(obj.GetTranslatedText("Locations"), "2");


                    //if (lstUsageReports.Items.FindByValue("3") == null)
                    //    lstUsageReports.Items.Add(obj.GetTranslatedText("Weekly Room Usage"), "3");

                    //if (lstUsageReports.Items.FindByValue("4") == null)
                    //    lstUsageReports.Items.Add(obj.GetTranslatedText("Monthly Room Usage"), "4");

                }

                subMenu = Convert.ToInt32(mmary[2].Split('*')[1]);
                if (Convert.ToBoolean(subMenu & 1))
                    if (DrpAllType.Items.FindByValue("3") == null)
                        DrpAllType.Items.Add("MCU", "3");

                if (lstUsageReports.Items.Count == 0)
                {
                    if (ReportsList.Items.FindByValue("3") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("3")); //Usage report
                }
                else if (lstUsageReports.Items.Count > 0)
                    lstUsageReports.SelectedIndex = 0;

                if (DrpAllType.Items.Count == 0)
                {
                    if (ReportsList.Items.FindByValue("4") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("4")); //Graphical report
                }
                else if (DrpAllType.Items.Count > 0)
                    DrpAllType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                log.Trace("" + ex.StackTrace);
            }
        }
        #endregion

        #region DisplayDetailsByID

        protected void DisplayDetailsByID(object sender, EventArgs e)
        {
            DataTable dt = null;
            try
            {
                CustomDate.Items.Clear();
                errLabel.Text = "";
               
                if (drpJobName.SelectedIndex != -1) //FB 2410 .selectedvalue
                {
                    delJobName.Visible = true;
                    delJobName.Attributes.Add("style", "display:block;");

                    if (Session["BatchReports"] != null)
                        dt = (DataTable)Session["BatchReports"];
                    dt.Rows.Add(-1);
                    DataRow[] dr;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        dr = dt.Select("ID='" + drpJobName.Value + "'");

                        ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>fnShowMenu()</script>");

                        ViewReportType(dr[0]["ReportType"].ToString(), dr[0]["InputType"].ToString(), dr[0]["InputValue"].ToString());

                        txtJob.Text = dr[0]["JobName"].ToString();
                        txtEmail.Text = dr[0]["EmailAddress"].ToString();
                        if (dr[0]["FrequencyType"].ToString() != "")
                        {
                            rblFrequency.Value = Convert.ToInt32(dr[0]["FrequencyType"].ToString());
                            rblFrequency.Items[Convert.ToInt32(dr[0]["FrequencyType"].ToString()) - 1].Selected = true;
                        }
                        else
                            rblFrequency.Items[0].Selected = true;//FB 2410
                        if (dr[0]["FrequencyType"].ToString() == "5")
                        {
                            CustomRow.Attributes.Add("style", "display:block;");
                            //tdSpace.Attributes.Add("style", "display:block;");
                            DateRow1.Attributes.Add("style", "display:none;");
                            DateRow2.Attributes.Add("style", "display:none;");
                            DateRowEnd1.Attributes.Add("style", "display:none;");
                            DateRowEnd2.Attributes.Add("style", "display:none;");
                          //  divCustom.Attributes.Add("style", "display:none;");

                        }
                        else
                        {
                            CustomRow.Attributes.Add("style", "display:none;");
                            //tdSpace.Attributes.Add("style", "display:none;");
                            DateRow1.Attributes.Add("style", "display:block;");
                            DateRow2.Attributes.Add("style", "display:block;");
                            DateRowEnd1.Attributes.Add("style", "display:block;");
                            DateRowEnd2.Attributes.Add("style", "display:block;");

                        }

                        if (dr[0]["RptStartDate"].ToString() != "")
                        {
                            if (Convert.ToDateTime(dr[0]["RptStartDate"]) >= DateTime.Now)
                            {
                              //  if (format == "mm/dd/yyyy")
                              //      startDateedit.Date = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(dr[0]["RptStartDate"].ToString()));
                              //  else
                              //     startDateedit.Date = Convert.ToDateTime(dr[0]["RptStartDate"].ToString());
                             startDateedit.Date = Convert.ToDateTime(dr[0]["RptStartDate"].ToString());
                            }
                            else
                                startDateedit.Date = DateTime.Now;

                        }
                        else
                            startDateedit.Text = "";

                        if (dr[0]["RptEndDate"].ToString() != "")
                        {
                            if (Convert.ToDateTime(dr[0]["RptEndDate"]) >= DateTime.Now)
                            {
                                //if (format == "mm/dd/yyyy")
                                //    endDateedit.Date = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(dr[0]["RptEndDate"].ToString()));
                                //else
                                //    endDateedit.Date = Convert.ToDateTime(dr[0]["RptEndDate"].ToString());
                                endDateedit.Date = Convert.ToDateTime(dr[0]["RptEndDate"].ToString());
                            }
                            else
                                endDateedit.Date = DateTime.Now;
                        }
                        else
                            endDateedit.Text = "";
                        if (dr[0]["DateFrom"].ToString() != "")
                        {
                          //  if (format == "mm/dd/yyyy")
                          //      dateFrom.Date = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(dr[0]["DateFrom"].ToString()));
                          //  else
                          //     dateFrom.Date = Convert.ToDateTime(dr[0]["DateFrom"].ToString());
                          dateFrom.Date = Convert.ToDateTime(dr[0]["DateFrom"].ToString());
                        }
                        else
                            dateFrom.Text = "";

                        if (dr[0]["DateTo"].ToString() != "")
                        {
                           // if (format == "mm/dd/yyyy")
                           //     dateTo.Date = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(dr[0]["DateTo"].ToString()));

                           // else
                           //     dateTo.Date = Convert.ToDateTime(dr[0]["DateTo"].ToString());
                           dateTo.Date = Convert.ToDateTime(dr[0]["DateTo"].ToString());
                        }
                        else
                            dateTo.Text = "";

                        String[] strDateList = null;

                        if (dr[0]["ReportDates"].ToString() != "")
                        {
                            strDateList = dr[0]["ReportDates"].ToString().Split(',');

                            for (Int32 n = 0; n < strDateList.Length; n++)
                                CustomDate.Items.Add(myVRMNet.NETFunctions.GetFormattedDate(strDateList[n].ToString()));
                        }
                    }
                    if(drpJobName.SelectedIndex ==0)
                        rblFrequency.Items[0].Selected = true;
                }
                else
                {
                    txtJob.Text = "";
                    txtEmail.Text = "";
                    rblFrequency.Value = "";
                    startDateedit.Text = "";
                    endDateedit.Text = "";
                    dateFrom.Text = "";
                    dateTo.Text = "";
                    CustomDate.Items.Clear();
                    CustomRow.Attributes.Add("style", "display:none;");
                    //tdSpace.Attributes.Add("style", "display:none;");
                    DateRow1.Attributes.Add("style", "display:block;");
                    DateRow2.Attributes.Add("style", "display:block;");
                    DateRowEnd1.Attributes.Add("style", "display:block;");
                    DateRowEnd2.Attributes.Add("style", "display:block;");
                    
                    //FB 2410
                    delJobName.Attributes.Add("style", "display:none;");
                }

                if (drpJobName.Value.Equals("-1")) //FB 2410
                {
                    //delJobName.Visible = false; //FB 2410
                    delJobName.Attributes.Add("style", "display:none;");
                }
                else
                {
                    delJobName.Attributes.Add("style", "display:block;");
                }


            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("DisPlayDetailsById" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region ViewReportType

        private void ViewReportType(String strRptType, String strInput, String strValue)
        {
            switch (strRptType)
            {
                case "CR":
                    ReportsList.Value = "1";
                    ConfScheRptDivList.Value = "1";
                    break;
                case "DS":
                    ReportsList.Value = "1";
                    ConfScheRptDivList.Value = "2";
                    break;
                case "PRI":
                    ReportsList.Value = "1";
                    ConfScheRptDivList.Value = "3";
                    break;
                case "RA":
                    ReportsList.Value = "1";
                    ConfScheRptDivList.Value = "4";
                    break;
                case "CL":
                    ReportsList.Value = "2";
                    lstUserReports.Value = "1";
                    break;
                case "AU":
                    ReportsList.Value = "3";
                    lstUsageReports.Value = "1";
                    break;
                case "UR":
                    ReportsList.Value = "3";
                    lstUsageReports.Value = "2";
                    break;
                case "WUR":
                    ReportsList.Value = "3";
                    lstUsageReports.Value = "3";
                    break;
                case "MUR":
                    ReportsList.Value = "3";
                    lstUsageReports.Value = "4";
                    break;
                case "PERS":
                    ReportsList.Value = "5";
                    switch (strInput)
                    {
                        case "ON":
                            lstStatusType.Value = "2"; break;
                        case "PE":
                            lstStatusType.Value = "3"; break;
                        case "RE":
                            lstStatusType.Value = "4"; break;
                        case "TE":
                            lstStatusType.Value = "5"; break;
                        case "DE":
                            lstStatusType.Value = "6"; break;
                        case "PU":
                            lstStatusType.Value = "7"; break;
                    }
                    break;
                case "USR":
                    ReportsList.Value = "6";
                    lstResourseType.Value = "1";
                    switch (strInput)
                    {
                        case "ALL":
                            lstUserType.Value = "1"; break;
                        case "ACU":
                            lstUserType.Value = "2"; break;
                        case "IA":
                            lstUserType.Value = "3"; break;
                        case "BL":
                            lstUserType.Value = "4"; break;
                    }
                    break;
                case "RR":
                    ReportsList.Value = "6";
                    lstResourseType.Value = "2";
                    switch (strInput)
                    {
                        case "ACR":
                            lstRoomType.Value = "1"; break;
                        case "DE":
                            lstRoomType.Value = "2"; break;
                    }
                    break;
                case "ER":
                    ReportsList.Value = "6";
                    lstResourseType.Value = "3";
                    break;
                case "MR":
                    ReportsList.Value = "6";
                    lstResourseType.Value = "4";
                    break;
                case "DSR":
                    ReportsList.Value = "6";
                    lstResourseType.Value = "5";
                    switch (strInput)
                    {
                        case "RSC":
                            lstConfRoomRpt.Value = "1"; break;
                        case "RAC":
                            lstRoomType.Value = "2"; break;
                        case "RCO":
                            lstRoomType.Value = "3"; break;
                    }
                    break;
            }
        }

        #endregion

        #region ResetPage

        protected void ResetPage(object sender, EventArgs e)
        {
            try
            {
                Session["BatchReports"] = null;
                Response.Redirect("ManageBatchReport.aspx?m=1");
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Resetpage" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region SaveReportConfiguration

        protected void SaveReportConfiguration(object sender, EventArgs e)
        {
            try
            {
               
                String strRptType = "";
                String strInput = "";
                String strValue = "";
                StringBuilder InXML = new StringBuilder();
                String frequencyType = "";
                String outXML = "";
                String fromDate = "";
                String toDate = "";

                ////FB 2410
                //if (format == "dd/MM/yyyy")
                //   Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB", true);
                //else
                //    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);
                ////FB 2410

                GetReportParams(ref strRptType, ref strInput, ref strValue);

                frequencyType = rblFrequency.Value.ToString();

                String dateXML = "";

                GetDates(frequencyType, ref dateXML);

                InXML.Append("<SetBatchReportConfig>");
                //if (orgID != "")
                //    InXML.Append("<organizationID>" + orgID + "</organizationID>");
                //else
                InXML.Append(obj.OrgXMLElement());
                InXML.Append("<ID>" + drpJobName.Value + "</ID>");//FB 2410

                InXML.Append("<JobName>" + txtJob.Text + "</JobName>");
                if (startDateedit.Text != "")
                    InXML.Append("<RptStartDate>" + myVRMNet.NETFunctions.GetDefaultDate(startDateedit.Text) + "</RptStartDate>");
                else
                    InXML.Append("<RptStartDate></RptStartDate>");

                if (endDateedit.Text != "")
                    InXML.Append("<RptEndDate>" + myVRMNet.NETFunctions.GetDefaultDate(endDateedit.Text) + "</RptEndDate>");
                else
                    InXML.Append("<RptEndDate></RptEndDate>");

                InXML.Append("<ReportName>" + ReportsList.Text + "</ReportName>");
                InXML.Append("<UserID>" + usrID + "</UserID>");
                InXML.Append("<ReportType>" + strRptType + "</ReportType>");
                InXML.Append("<InputType>" + strInput + "</InputType>");
                InXML.Append("<InputValue>" + strValue + "</InputValue>");
                
                //FB 2410
                InXML.Append("<DateFrom>" + Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(dateFrom.Text + " " + "12:00 AM")) + "</DateFrom>"); //myVRMNet.NETFunctions.GetDefaultDate(dateFrom.Text)
                InXML.Append("<DateTo>" + Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(dateTo.Text + " " + "11:59 PM")) + "</DateTo>"); //myVRMNet.NETFunctions.GetDefaultDate(dateTo.Text)
                //FB 2410

                InXML.Append("<FrequencyType>" + frequencyType + "</FrequencyType>");
                InXML.Append("<EmailAddress>" + txtEmail.Text + "</EmailAddress>");
                InXML.Append("<WorkingHours>" + workingHours + "</WorkingHours>");

                if (dateXML != "")
                    InXML.Append(dateXML);

                InXML.Append("</SetBatchReportConfig>");

                outXML = obj.CallMyVRMServer("SetBatchReportConfig", InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("ManageBatchReport.aspx?m=1");

            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("SaveReportConfiguration" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region DeleteBatchReportConf   
        protected void DeleteBatchReportConf(object sender, EventArgs e)
        {
            string iJobValue = drpJobName.Value.ToString();

            String inXML = "";
            inXML += "<login>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
            inXML += "  <delBatchReport>";
            inXML += "      <JobID>" + drpJobName.Value + "</JobID>";
            inXML += "  </delBatchReport>";
            inXML += "</login>";
            String outXML = obj.CallMyVRMServer("DeleteBatchReport", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
            log.Trace("DeleteBatchReport - " + outXML);

            if (outXML.IndexOf("<error>") >= 0)
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
            }
            else
                Response.Redirect("ManageBatchReport.aspx?m=1");

           
        }
        #endregion

        #region GetReportParams

        private void GetReportParams(ref String strRptType, ref String strInput, ref String strValue)
        {
            // strRptType --> CONF - Conf Usage , LOC - Locations, MCU - MCU
            // strInput & strValue --> ALL-Conf All, CA-Conf Audio, CV-Conf Video, CR-Conf Room, CP-Conf pt to pt, 
            //RM - Room Id's, CS - Country/State, ZP - Zip, MCU

            switch (ReportsList.Value.ToString())
            {
                case "1":
                    switch (ConfScheRptDivList.Value.ToString())
                    {
                        case "1":
                            strRptType = "CR";
                            strInput = "RM";
                            break;
                        case "2": strRptType = "DS"; break;
                        case "3": strRptType = "PRI"; break;
                        case "4": strRptType = "RA"; break;
                    }
                    break;
                case "2": strRptType = "CL"; break;
                case "3":
                    if (lstUsageReports.Value.ToString() == "1")
                        strRptType = "AU";
                    else if (lstUsageReports.Value.ToString() == "2")
                        strRptType = "UR";
                    else if (lstUsageReports.Value.ToString() == "3")
                        strRptType = "WUR";
                    else if (lstUsageReports.Value.ToString() == "4")
                        strRptType = "MUR";
                    break;
                case "5":
                    strRptType = "PERS";
                    String reportRange = DateTime.Today.ToShortDateString();
                    strValue = lstStatusType.Value.ToString();

                    switch (lstStatusType.Value.ToString())
                    {
                        case "2":
                            strInput = "ON"; break;
                        case "3":
                            strInput = "PE"; break;
                        case "4":
                            strInput = "RE"; break;
                        case "5":
                            strInput = "TE"; break;
                        case "6":
                            strInput = "DE"; break;
                        case "7":
                            strInput = "PU"; break;
                    }

                    DateTime calDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(dateFrom.Text));
                    DateTime dtFrom;
                    DateTime dtTo;
                    if (lstDailyMonthly.Value.ToString() == "1")
                    {
                        dtFrom = new DateTime(calDate.Year, calDate.Month, calDate.Day, 0, 0, 0);
                        dtTo = new DateTime(calDate.Year, calDate.Month, calDate.Day, 23, 59, 59);
                    }
                    else
                    {
                        dtFrom = new DateTime(calDate.Year, calDate.Month, 1, 0, 0, 0);
                        dtTo = new DateTime(calDate.Year, calDate.Month, calDate.AddMonths(1).AddDays(-calDate.Day).Day, 23, 59, 59);
                    }

                    dateFrom.Date = dtFrom;
                    dateTo.Date = dtTo;

                    break;
                case "6":
                case "7":
                    switch (lstResourseType.Value.ToString())
                    {
                        case "1":
                            strRptType = "USR";
                            if (lstUserType.Value.ToString() == "1") strInput = "ALL";
                            else if (lstUserType.Value.ToString() == "2") strInput = "ACU";
                            else if (lstUserType.Value.ToString() == "3") strInput = "IA";
                            else if (lstUserType.Value.ToString() == "4") strInput = "BL";
                            break;
                        case "2":
                            strRptType = "RR";
                            if (lstRoomType.Value.ToString() == "1") strInput = "ACR";
                            else strInput = "DE";
                            break;
                        case "3":
                            strRptType = "ER";
                            break;
                        case "4":
                            strRptType = "MR";
                            break;
                        case "5":
                            strRptType = "DSR";
                            if (lstConfRoomRpt.Value.ToString() == "1") strInput = "RSC";
                            else if (lstConfRoomRpt.Value.ToString() == "2") strInput = "RAC";
                            else if (lstConfRoomRpt.Value.ToString() == "3") strInput = "RCO";
                            break;
                    }
                    break;
                case "8":
                    strRptType = "CD";
                    break;
            }
        }

        #endregion

        #region GetDates

        private void GetDates(String frequencyType, ref String dateXML)
        {
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            myVRMNet.DateUtilities du = new myVRMNet.DateUtilities();

            if (frequencyType != "5")
            {
                startDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(startDateedit.Text)); //myVRMNet.NETFunctions.GetDefaultDate
                endDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(endDateedit.Text)); //myVRMNet.NETFunctions.GetDefaultDate
            }
            dateXML = "<ReportDates>";

            switch (frequencyType)
            {
                case "1":
                    for (DateTime sd = startDate; sd <= endDate; sd = sd.AddDays(1))
                    {
                        if (sd < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                            continue;

                        dateXML += "<ReportDate>" + sd.ToShortDateString() + "</ReportDate>";

                        if (sd.ToShortDateString() == endDate.ToShortDateString())
                            break;
                    }
                    break;
                case "2":
                case "3":
                    DateTime startofWeek = du.GetStartOfWeek(startDate);
                    if (startofWeek.ToShortDateString() != startDate.ToShortDateString() && startofWeek >= DateTime.Now)
                        startDate = startofWeek;

                    int DaysToSubtract = (int)startDate.DayOfWeek;
                    int n = 0;

                    if (frequencyType == "2")
                        n = 7;
                    else if (frequencyType == "3")
                        n = 14;

                    for (DateTime sd = startDate; sd <= endDate; sd = (sd == startDate) ? sd.AddDays(n - DaysToSubtract + 1) : sd.AddDays(n))
                    {
                        dateXML += "<ReportDate>" + sd.ToShortDateString() + "</ReportDate>";

                        if (sd.ToShortDateString() == endDate.ToShortDateString())
                            break;
                    }
                    break;
                case "4":
                    DateTime startofMonth = du.GetStartOfMonth(startDate.Month, startDate.Year);
                    if (startofMonth.ToShortDateString() != startDate.ToShortDateString() && startofMonth >= DateTime.Now)
                        startDate = startofMonth;

                    Int32 m = 0;
                    for (DateTime sd = startDate; sd <= endDate; sd = sd.AddMonths(1))
                    {
                        if (m > 0)
                            sd = du.GetStartOfMonth(sd.Month, sd.Year);

                        dateXML += "<ReportDate>" + sd.ToShortDateString() + "</ReportDate>";

                        if (sd.ToShortDateString() == endDate.ToShortDateString())
                            break;

                        m = m + 1;
                    }

                    break;
                case "5":
                    String[] strDateList = null;
                    if (hdnDateList.Value != "")
                    {
                        strDateList = hdnDateList.Value.Split(',');

                        if (strDateList != null)
                        {
                            for (int dts = 0; dts < strDateList.Length; dts++)
                                dateXML += "<ReportDate>" + myVRMNet.NETFunctions.GetDefaultDate(strDateList[dts]) + "</ReportDate>";
                        }
                    }
                    break;
            }
            dateXML += "</ReportDates>";
        }

        #endregion

    }

}
