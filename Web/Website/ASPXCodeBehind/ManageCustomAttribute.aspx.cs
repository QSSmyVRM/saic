/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections;

namespace ns_ManageCustomAttribute
{
    public partial class ManageCustomAttribute : System.Web.UI.Page
    {
        #region protected Data Members 

        protected System.Web.UI.WebControls.Label lblHeader; //FB 2670
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtEntityID;
        protected System.Web.UI.WebControls.TextBox txtEntityName;
        protected System.Web.UI.WebControls.TextBox txtEntityDesc;
        protected System.Web.UI.WebControls.Button btnAddEntityCode;
        protected System.Web.UI.WebControls.DataGrid dgEntityCode;
        protected System.Web.UI.WebControls.Button BtnAddCustom;
        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;

        protected System.Web.UI.WebControls.TextBox TxtCAName;
        protected System.Web.UI.WebControls.TextBox TxtCADesc;
        protected System.Web.UI.WebControls.CheckBox ChkCARequired;
        protected System.Web.UI.WebControls.CheckBox ChkCAStatus;
        protected System.Web.UI.WebControls.CheckBox ChkCAEmail;
        protected System.Web.UI.WebControls.DropDownList DrpCAType;
        protected System.Web.UI.WebControls.Table tblNoOptions;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCustomAttID;
        
        //FB 1767 start
        protected System.Web.UI.WebControls.CheckBox ChkRoomApp;
        protected System.Web.UI.WebControls.CheckBox ChkMcuApp;
        protected System.Web.UI.WebControls.CheckBox ChkSysApp;
        protected System.Web.UI.WebControls.CheckBox chkScheduler;
        protected System.Web.UI.WebControls.CheckBox chkHost;
        protected System.Web.UI.WebControls.CheckBox chkParty;
        protected System.Web.UI.WebControls.CheckBox chkMcuAdmin;
        protected System.Web.UI.WebControls.CheckBox chkRoomAdmin;
        protected System.Web.UI.WebControls.CheckBox ChkAll;
        //FB 1767 end
        //FB 1970 start
        protected System.Web.UI.WebControls.DataGrid dgTitlelist;
        protected System.Web.UI.WebControls.DataGrid dgLangOptionlist;
        protected System.Web.UI.WebControls.ImageButton imgLangsTitle;
        protected System.Web.UI.WebControls.ImageButton imgLangsOption;
        protected System.Web.UI.HtmlControls.HtmlTableRow otherLangRow;
        protected System.Web.UI.HtmlControls.HtmlTableRow LangsOptionRow;
        //FB 1970 end
        protected System.Web.UI.WebControls.CheckBox ChkCAinCalendar; //FB 2013
        protected System.Web.UI.WebControls.CheckBox chkVNOCOperator; //FB 2501
        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        ns_Logger.Logger log;
        DataTable optionsTable = null;
        String customAttributeId = "";
        String outXML = "";
        ArrayList colNames = null;
        string sessKey = "";
        DataTable dtable = new DataTable(); //FB 1970 start
        Label lblLangID = new Label();
        Label lblLangName = new Label();
        protected bool defaultLang = true; //FB 1970 end
        bool isSytemAtt = false; //FB 2349
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCreateby;//FB 2349
        #endregion

        #region ManageCustomAttribute 

        public ManageCustomAttribute()
        {
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region Methods Executed on Page Load 

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageCustomAttribute.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                errLabel.Text = "";
                sessKey = "OptDt" + Session.SessionID;
                DrpCAType.Attributes.Add("onchange", "JavaScript:return fnOnComboChange()");
                //FB 1970 start
                dgEntityCode.ItemDataBound += new DataGridItemEventHandler(dgEntityCode_ItemDataBound);
                imgLangsTitle.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + otherLangRow.ClientID + "', false);return false;");
                imgLangsOption.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + LangsOptionRow.ClientID + "', false);return false;");
                txtEntityName.Attributes.Add("OnKeyUp", "javascript:maxCharRowShow()");
                txtEntityDesc.Attributes.Add("OnKeyUp", "javascript:maxCharRowShow()");
                //FB 1970 end
                if (Request.QueryString["CustomID"] != null)
                {
                    if (Request.QueryString["CustomID"].ToString().Trim() != "")
                    {
                        customAttributeId = Request.QueryString["CustomID"].ToString().Trim();
                        hdnCustomAttID.Value = customAttributeId;
                    }
                }
                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //BtnAddCustom.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //BtnAddCustom.Attributes.Add("Class", "btndisable");// FB 2796

                    lblHeader.Text = obj.GetTranslatedText("View Custom Option Details");
                    //ZD 100263
                    BtnAddCustom.Visible = false;
                }
                else
                    BtnAddCustom.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796

                if (!IsPostBack)
                {
                    GetLanguageList(); //FB 1970
                    if (customAttributeId != "")
                    {
                        GetOldCustomAttribute();
                    }
                    else
                    {
                        CreateDtColumnNames();
                        optionsTable = obj.LoadDataTable(null, colNames);

                        if (!optionsTable.Columns.Contains("RowUID"))
                            optionsTable.Columns.Add("RowUID");
                    }
                    BindOptionData();
                }
                
                if (errLabel.Text.Trim() != "")
                    errLabel.Visible = true;
               /* else //FB 1767
                    errLabel.Visible = false; */
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Page_load" + ex.Message);//ZD 100263

            }
        }
        #endregion

        #region Get Custom Attribute Details
        /// <summary>
        /// Get Custom Attribute Details
        /// </summary>
        private void GetOldCustomAttribute()
        {
            try
            {
                XmlNodeList nodes = null; //FB 1970
                DataTable dt = new DataTable(); //FB 1970
                string inXML = "<CustomAttribute>" + obj.OrgXMLElement() + "<ID>" + customAttributeId + "</ID></CustomAttribute>";//Organization Module Fixes
                outXML = obj.CallMyVRMServer("GetCustomAttributeByID", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(outXML);
                XmlNode node = null;

                if (outXML.IndexOf("<error>") >= 0)
                {
                    node = xd.SelectSingleNode("error");
                    errLabel.Text = node.InnerText;
                    errLabel.Visible = true;
                    return;
                }
                //FB 2349 - start
                node = xd.SelectSingleNode("//CustomAttribute/CreateType");
                if (node != null)
                {
                    if (node.InnerText.Trim() == "1")
                        isSytemAtt = true;
                }
                if (isSytemAtt)
                {
                    hdnCreateby.Value = "1";
                }
                else
                {
                    hdnCreateby.Value = "0";
                }
                //FB 2349 - end
                node = xd.SelectSingleNode("//CustomAttribute/CustomAttributeID");
                if (node != null)
                    hdnCustomAttID.Value = node.InnerText;

                node = xd.SelectSingleNode("//CustomAttribute/Title");
                if (node != null)
                    TxtCAName.Text = node.InnerText;

                //FB 1970 start
                nodes = xd.SelectNodes("//CustomAttribute/TitleList");
                if (nodes != null)
                    if (nodes.Count > 0)
                        dt = obj.LoadDataTable(nodes, null);
                if (dt.Rows.Count > 0)
                {
                    dgTitlelist.DataSource = dt;
                    dgTitlelist.DataBind();
                }
                else if (Session["dtLanguages"] != null)
                {
                    dgTitlelist.DataSource = (DataTable)Session["dtLanguages"];
                    dgTitlelist.DataBind();
                }
                //FB 1970 end

                string isMandatory = "";
                node = xd.SelectSingleNode("//CustomAttribute/Mandatory");
                if (node != null)
                    isMandatory = node.InnerText;

                if (isMandatory == "1")
                    ChkCARequired.Checked = true;

                node = xd.SelectSingleNode("//CustomAttribute/Description");
                if (node != null)
                    TxtCADesc.Text = node.InnerText;

                string fieldType = "";
                node = xd.SelectSingleNode("//CustomAttribute/Type");
                if (node != null)
                    fieldType = node.InnerText;

                foreach (ListItem lst in DrpCAType.Items)
                {
                    if (lst.Value.Equals(fieldType))
                    {
                        lst.Selected = true;
                        break;
                    }
                }

                string displayStatus = "";
                node = xd.SelectSingleNode("//CustomAttribute/Status");
                if (node != null)
                    displayStatus = node.InnerText;

                if (displayStatus == "0")
                    ChkCAStatus.Checked = true;

                string isIncludeEmail = "";
                node = xd.SelectSingleNode("//CustomAttribute/IncludeInEmail");
                if (node != null)
                    isIncludeEmail = node.InnerText;

                if (isIncludeEmail == "1")
                    ChkCAEmail.Checked = true;

                //FB 1767 start
                string host = "",scheduler="",party="",roomAdmin="",mcuAdmin="",roomApp="",mcuApp="",sysApp="",vnocOperator="";//FB 2501
                ChkAll.Checked = true;
                node = xd.SelectSingleNode("//CustomAttribute/MailToHost");
                if (node != null)
                    host = node.InnerText;

                if (host == "1")
                    chkHost.Checked = true;
                else
                {
                    chkHost.Checked = false;
                    ChkAll.Checked = false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/MailToScheduler");
                if (node != null)
                    scheduler = node.InnerText;

                if (scheduler == "1")
                    chkScheduler.Checked = true;
                else
                {
                    chkScheduler.Checked = false;
                    ChkAll.Checked = false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/MailToParty");
                if (node != null)
                    party = node.InnerText;

                if (party == "1")
                    chkParty.Checked = true;
                else
                {
                    chkParty.Checked = false;
                    ChkAll.Checked = false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/MailToRoomAdmin");
                if (node != null)
                    roomAdmin = node.InnerText;
                
                if (roomAdmin == "1")
                    chkRoomAdmin.Checked = true;
                else
                {
                    chkRoomAdmin.Checked = false;
                    ChkAll.Checked = false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/MailToMCUAdmin");
                if (node != null)
                    mcuAdmin = node.InnerText;
                
                if (mcuAdmin == "1")
                    chkMcuAdmin.Checked = true;
                else
                {
                    chkMcuAdmin.Checked = false;
                    ChkAll.Checked = false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/MailToRoomApp");
                if (node != null)
                    roomApp = node.InnerText;
                
                if (roomApp == "1")
                    ChkRoomApp.Checked = true;
                else
                {
                    ChkRoomApp.Checked = false;
                    ChkAll.Checked = false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/MailToMCUApp");
                if (node != null)
                    mcuApp = node.InnerText;
                
                if (mcuApp == "1")
                    ChkMcuApp.Checked = true;
                else
                {
                    ChkMcuApp.Checked = false;
                    ChkAll.Checked = false;
                }

                node = xd.SelectSingleNode("//CustomAttribute/MailToSysApp");
                if (node != null)
                    sysApp = node.InnerText;
                
                if (sysApp == "1")
                    ChkSysApp.Checked = true;
                else
                {
                    ChkSysApp.Checked = false;
                    ChkAll.Checked = false;
                }
                //FB 1767 end
                //FB 2501 Starts
                node = xd.SelectSingleNode("//CustomAttribute/MailToVNOCOp");
                if (node != null)
                    vnocOperator = node.InnerText;

                if (vnocOperator == "1")
                    chkVNOCOperator.Checked = true;
                else
                {
                    chkVNOCOperator.Checked = false;
                    ChkAll.Checked = false;
                }
                //FB 2501 Ends
                //FB 2013 start
                ChkCAinCalendar.Checked = false;
                node = xd.SelectSingleNode("//CustomAttribute/IncludeInCalendar");
                if (node != null)
                    if(node.InnerText.Trim().Equals("1"))
                        ChkCAinCalendar.Checked = true;
                //FB 2013 end
                XmlNodeList optNodes = xd.SelectNodes("//CustomAttribute/OptionList/Option");
                obj = new myVRMNet.NETFunctions();
                CreateDtColumnNames();

                optionsTable = obj.LoadDataTable(optNodes, colNames);

                if (!optionsTable.Columns.Contains("RowUID"))
                    optionsTable.Columns.Add("RowUID");

                int rowCnt=1;
                foreach (DataRow dr in optionsTable.Rows)
                {
                    dr["RowUID"] = rowCnt++;
                }
                //FB 2349 - start
                if (isSytemAtt)
                {
                    TxtCAName.Enabled = false;
                    dgTitlelist.Enabled = false;
                    TxtCADesc.Enabled = false;
                    DrpCAType.Enabled = false;
                    ChkCAStatus.Enabled = false; //FB 2377
                }
                //FB 2349 - end
            }
            catch (Exception ex)
            {
				log.Trace(ex.Message);
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                //errLabel.Text = "System Error. Please contact VRM Administrator";
            }
        }
        #endregion

        #region Create Column Names

        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("ID"); //FB 1970
            colNames.Add("name"); //FB 1970
            colNames.Add("DisplayCaption");
            colNames.Add("HelpText");
            colNames.Add("OptionID");
            colNames.Add("DisplayValue");
        }
        #endregion

        #region Bind Option Data
        /// <summary>
        /// Bind Option Data
        /// </summary>
        private void BindOptionData()
        {
            try
            {
                if (optionsTable == null)
                    optionsTable = new DataTable();

                if (Session[sessKey] == null)
                    Session.Add(sessKey, optionsTable);
                else
                    Session[sessKey] = optionsTable;

                hdnValue.Value = optionsTable.Rows.Count.ToString();

                dgEntityCode.DataSource = optionsTable;
                dgEntityCode.DataBind();

                if (optionsTable.Rows.Count > 0)
                    tblNoOptions.Visible = false;
                else
                    tblNoOptions.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindoptionData" + ex.Message);//ZD 100263
            }
        }
        #endregion

        #region BindData - Not USed

        private void BindData(string outXML)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlNodeList nodes = xmldoc.SelectNodes("//OptionList/Option");
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (Session["dtEntityCode"] == null)
                        Session.Add("dtEntityCode", dt);
                    else
                        Session["dtEntityCode"] = dt;
                    
                    hdnValue.Value = dt.Rows.Count.ToString();

                    dgEntityCode.DataSource = dt;
                    dgEntityCode.DataBind();
                }
            }

            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindData" + ex.Message);//ZD 100263
            }
        }
        #endregion

        #region EditItem 

        protected void EditItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtEntityID.Text = e.Item.Cells[0].Text;
                if (ViewState["EntityID"] == null)
                    ViewState.Add("EntityID", e.Item.Cells[0].Text);
                else
                    ViewState["EntityID"] = e.Item.Cells[0].Text;

                dgEntityCode.EditItemIndex = e.Item.ItemIndex;

                if(Session[sessKey] != null)
                    optionsTable = (DataTable)Session[sessKey];

                dgEntityCode.DataSource = optionsTable;
                dgEntityCode.DataBind();
                btnAddEntityCode.Enabled = false;

            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("EditItem" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region UpdateItem 
        protected void UpdateItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtEntityName.Text = ((TextBox)e.Item.FindControl("txtEntityName")).Text.Trim();
                txtEntityDesc.Text = ((TextBox)e.Item.FindControl("txtEntityDesc")).Text.Trim();
                lblLangID.Text = ((Label)e.Item.FindControl("lblLangID")).Text.Trim(); //FB 1970
                if (txtEntityDesc.Text.Length > 35)
                {
                    errLabel.Text = obj.GetTranslatedText("Custom option description should be within 35 characters");//FB 1830 - Translation
                    errLabel.Visible = true;
                    txtEntityName.Text = "";
                    txtEntityDesc.Text = "";
                    return;
                }
                UpdateEntityCode(sender, (EventArgs)e);
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("UpdateItem" + ex.Message);//ZD 100263
            }
        }
        
        #endregion 

        #region CancelItem 
        protected void CancelItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtEntityID.Text = "new";
                dgEntityCode.EditItemIndex = -1;
                dgEntityCode.DataSource = (DataTable) Session[sessKey];
                dgEntityCode.DataBind();
                btnAddEntityCode.Enabled = true;
                errLabel.Visible = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("CancelItem" + ex.Message);//ZD 100263
            }
        }
        #endregion

        #region DeleteItem
        /// <summary>
        /// Delete Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string caRowId = "";
                caRowId = e.Item.Cells[2].Text; //FB 1970[DisplayValue]  //e.Item.Cells[0].Text;

                optionsTable = (DataTable)Session[sessKey];
                if (optionsTable != null)
                {
                    string filterExp = "DisplayValue='" + caRowId + "'"; //FB 1970
                    //filterExp = "RowUID='" + caRowId + "'"; //FB 1970
                    DataRow[] drArr = optionsTable.Select(filterExp);
                    foreach (DataRow dr in drArr)
                    {
                        optionsTable.Rows.Remove(dr);
                    }
                    int rowCnt = 1;
                    foreach (DataRow dr in optionsTable.Rows)
                    {
                        dr["RowUID"] = rowCnt++;
                    }
                }
                BindOptionData();
                
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("DeleteItem" + ex.Message);//ZD 100263
            }
        }
        #endregion

        //method changed for FB 1970
        #region BindRowsDeleteMessage - not use
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Option?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Update Option
        protected void UpdateEntityCode(Object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridItem dg1 in dgEntityCode.Items)
                {
                    if ((Label)dg1.FindControl("lblEntityName") != null)
                    {
                        if (((Label)dg1.FindControl("lblLangID")).Text.Trim() == lblLangID.Text.Trim()) //FB 1970
                        {
                            if (((Label)dg1.FindControl("lblEntityName")).Text.ToLower().Trim() == txtEntityName.Text.ToLower().Trim())
                            {
                                errLabel.Text = obj.GetTranslatedText("Custom option title already exists in the list");//FB 1830 - Translation
                                errLabel.Visible = true;
                                txtEntityName.Text = "";
                                txtEntityDesc.Text = "";
                                return;
                            }
                            if (txtEntityName.Text.Length > 35)
                            {
                                errLabel.Text = obj.GetTranslatedText("Custom option name should be within 35 characters");//FB 1830 - Translation
                                errLabel.Visible = true;
                                txtEntityName.Text = "";
                                txtEntityDesc.Text = "";
                                return;
                            }
                        }
                    }
                }

                optionsTable = (DataTable)Session[sessKey];

                if (optionsTable != null)
                {
                    foreach (DataRow dr in optionsTable.Rows)
                    {
                        if (dr["RowUID"].ToString() == txtEntityID.Text)
                        {
                            dr["DisplayCaption"] = txtEntityName.Text.Trim();
                            dr["HelpText"] = txtEntityDesc.Text.Trim();

                            break;
                        }
                    }
                }

                txtEntityID.Text = "new";
                dgEntityCode.EditItemIndex = -1;
                btnAddEntityCode.Enabled = true;
                
                txtEntityName.Text = "";
                txtEntityDesc.Text = "";

                BindOptionData();
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("UpdateEntitycode" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Add Option
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddEntityCode(Object sender, EventArgs e)
        {
            try
            {
                //FB 1970 start
                TextBox txtOptionName = txtEntityName;
                TextBox txtOptionDesc = txtEntityDesc;
                TextBox txtOptionID = txtEntityID;
                //FB 1970 end
                defaultLang = true;
                DataRow dr = null;
                int displayValue = 1;
                optionsTable = (DataTable)Session[sessKey];

                if (optionsTable == null)
                {
                    CreateDtColumnNames();
                    optionsTable = obj.LoadDataTable(null, colNames);
                    
                    if (!optionsTable.Columns.Contains("RowUID"))
                        optionsTable.Columns.Add("RowUID");
                }
                int rowID = optionsTable.Rows.Count;
                //FB 1970 start
                dtable = optionsTable;
                if (dgLangOptionlist.Items.Count > 0) //ZD 100334
                {
                    for (int i = 0; i < dgLangOptionlist.Items.Count; i++)
                    {
                        if (defaultLang)
                        {
                            lblLangID.Text = "1";
                            lblLangName.Text = obj.GetTranslatedText("English");//ZD100288
                            i--;
                        }
                        else
                        {
                            txtOptionName = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtOptionName"));
                            txtOptionDesc = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtOptionDesc"));
                            txtOptionID = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtOptionID"));
                            lblLangID = ((Label)dgLangOptionlist.Items[i].FindControl("lblLangID"));
                            lblLangName = ((Label)dgLangOptionlist.Items[i].FindControl("lblLangName"));
                            if (txtOptionName.Text.Trim() == "") //Default to option name if not given
                                txtOptionName = txtEntityName;
                            if (txtOptionDesc.Text.Trim() == "") //Default to option Descript if not given
                                txtOptionDesc = txtEntityDesc;

                        }
                        for (int j = 0; j < dgEntityCode.Items.Count; j++)
                        {
                            if (((Label)dgEntityCode.Items[j].FindControl("lblLangID")).Text.Trim() == lblLangID.Text.Trim())
                                if (((Label)dgEntityCode.Items[j].FindControl("lblEntityName")).Text.ToLower().Trim() == txtOptionName.Text.ToLower().Trim())
                                {
                                    errLabel.Text = obj.GetTranslatedText("Option name already exists in the list for Language  ") + lblLangName.Text.Trim(); //FB 2272
                                    errLabel.Visible = true;
                                    optionsTable = dtable;
                                    return;
                                }
                        }
                        rowID = optionsTable.Rows.Count;
                        dr = optionsTable.NewRow();
                        dr["ID"] = lblLangID.Text.Trim();
                        dr["name"] = obj.GetTranslatedText(lblLangName.Text.Trim()); //ZD 100288_4Dec2013
                        dr["DisplayCaption"] = txtOptionName.Text.Trim();
                        dr["HelpText"] = txtOptionDesc.Text.Trim();
                        dr["OptionID"] = txtOptionID.Text.Trim();
                        if (defaultLang)
                        {
                            if (dgEntityCode.Items.Count > 0)
                                int.TryParse(optionsTable.Rows[dgEntityCode.Items.Count - 1]["DisplayValue"].ToString(), out displayValue);

                            defaultLang = false;
                            if (rowID != 0)
                                displayValue++;
                        }

                        dr["DisplayValue"] = displayValue;
                        dr["RowUID"] = rowID + 1;
                        optionsTable.Rows.Add(dr);
                    }
                }
                else if (dgLangOptionlist.Items.Count == 0) //ZD 100334
                {
                    rowID = optionsTable.Rows.Count;
                    dr = optionsTable.NewRow();
                    dr["ID"] = Session["languageID"].ToString();
                    dr["name"] = lblLangName.Text.Trim();
                    dr["DisplayCaption"] = txtOptionName.Text.Trim();
                    dr["HelpText"] = txtOptionDesc.Text.Trim();
                    dr["OptionID"] = "new";
                    dr["DisplayValue"] = "1";
                    dr["RowUID"] = rowID + 1;
                    optionsTable.Rows.Add(dr);
                }
                BindOptionData();
                if (Session["dtLanguages"] != null)
                {
                    dgLangOptionlist.DataSource = (DataTable)Session["dtLanguages"];
                    dgLangOptionlist.DataBind();
                }
                //FB 1970 end

                txtEntityName.Text = "";
                txtEntityDesc.Text = "";
                txtEntityID.Text = "";
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("AddEntitiycode" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Set Custom Attribute
        /// <summary>
        /// Set Custom Attribute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetCustomAttribute(Object sender, EventArgs e)
        {
            String inXML = "";
            try
            {
                string mandatory = "";
                string includeEmail = "", CAinCalendar = "0";//FB 2013
                string castatus = "";

                if (ChkCAEmail.Checked)
                    includeEmail = "1";
                else
                    includeEmail = "0";

                if (ChkCAinCalendar.Checked) //FB 2013
                    CAinCalendar = "1";

                if (ChkCARequired.Checked)
                    mandatory = "1";
                else
                    mandatory = "0";

                if (ChkCAStatus.Checked)    //0-Enable; 1-Disable
                    castatus = "0";
                else
                    castatus = "1";

                customAttributeId = hdnCustomAttID.Value.Trim();

                if (customAttributeId == "")
                    customAttributeId = "new";

                string fieldType = "";
                fieldType = DrpCAType.SelectedValue;

                if (fieldType == "")
                    fieldType = "4";

                inXML = "<CustomAttribute>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<CustomAttributeID>" + customAttributeId + "</CustomAttributeID>";
                inXML += "<Title>" + TxtCAName.Text.Trim() + "</Title>";
                //FB 1970 start
                inXML += "<TitleList>";
                inXML += "<Title>" + TxtCAName.Text.Trim() + "</Title>";
                inXML += "<ID>1</ID>";
                inXML += "</TitleList>";
                for (int i = 0; i < dgTitlelist.Items.Count; i++)
                  {
                    String strTitle = ((TextBox)dgTitlelist.Items[i].FindControl("txtTitle")).Text.Trim();
                    if (strTitle == "")
                        strTitle = TxtCAName.Text.Trim();

                    inXML += "<TitleList>";
                    inXML += "<ID>" + ((Label)dgTitlelist.Items[i].FindControl("lblLangID")).Text.Trim() + "</ID>";
                    inXML += "<Title>" + strTitle + "</Title>";
                    inXML += "</TitleList>";
                  }
                //FB 1970 end
                inXML += "<Mandatory>" + mandatory + "</Mandatory>";
                inXML += "<Description>" + TxtCADesc.Text.Trim() + "</Description>";
                inXML += "<Type>" + fieldType + "</Type>";
                inXML += "<Status>" + castatus + "</Status>";
                inXML += "<IncludeEmail>" + includeEmail + "</IncludeEmail>";
                inXML += "<IncludeInCalendar>" + CAinCalendar + "</IncludeInCalendar>"; //FB 2013
                //FB 1767 start
                if (includeEmail == "1" && chkHost.Checked == true)
                    inXML += "<MailToHost>1</MailToHost>";
                else
                    inXML += "<MailToHost>0</MailToHost>";

                if (includeEmail == "1" && chkScheduler.Checked == true)
                    inXML += "<MailToScheduler>1</MailToScheduler>";
                else
                    inXML += "<MailToScheduler>0</MailToScheduler>";

                if (includeEmail == "1" && chkParty.Checked == true)
                    inXML += "<MailToParty>1</MailToParty>";
                else
                    inXML += "<MailToParty>0</MailToParty>";

                if (includeEmail == "1" && chkRoomAdmin.Checked == true)
                    inXML += "<MailToRoomAdmin>1</MailToRoomAdmin>";
                else
                    inXML += "<MailToRoomAdmin>0</MailToRoomAdmin>";

                if (includeEmail == "1" && chkMcuAdmin.Checked == true)
                    inXML += "<MailToMCUAdmin>1</MailToMCUAdmin>";
                else
                    inXML += "<MailToMCUAdmin>0</MailToMCUAdmin>";

                if (includeEmail == "1" && ChkRoomApp.Checked == true)
                    inXML += "<MailToRoomApp>1</MailToRoomApp>";
                else
                    inXML += "<MailToRoomApp>0</MailToRoomApp>";

                if (includeEmail == "1" && ChkMcuApp.Checked == true)
                    inXML += "<MailToMCUApp>1</MailToMCUApp>";
                else
                    inXML += "<MailToMCUApp>0</MailToMCUApp>";

                if (includeEmail == "1" && ChkSysApp.Checked == true)
                    inXML += "<MailToSysApp>1</MailToSysApp>";
                else
                    inXML += "<MailToSysApp>0</MailToSysApp>";

                //FB 1767 end

                //FB 2501 Starts
                if (includeEmail == "1" && chkVNOCOperator.Checked == true)
                    inXML += "<MailToVNOCOp>1</MailToVNOCOp>";
                else
                    inXML += "<MailToVNOCOp>0</MailToVNOCOp>";

                //FB 2501 Ends
                if (fieldType == "5" || fieldType == "6" || fieldType == "8") //FB 1718 
                {
                    optionsTable = (DataTable)Session[sessKey];
                    if (optionsTable != null)
                    {
                        if (optionsTable.Rows.Count <= 0)
                        {
                            errLabel.Text = obj.GetTranslatedText("Please add atleast one option to the custom option.");//FB 1830 - Translation
                            errLabel.Visible = true;
                            return;
                        }

                        inXML += "<OptionList>";
                        foreach (DataRow dr in optionsTable.Rows)
                        {
                            inXML += "<Option>";
                            inXML += "<OptionID>new</OptionID>";
                            inXML += "<OptionValue>" + dr["DisplayValue"].ToString() + "</OptionValue>";
                            inXML += "<OptionCaption>" + dr["DisplayCaption"].ToString() + "</OptionCaption>";
                            inXML += "<HelpText>" + dr["HelpText"].ToString() + "</HelpText>";
                            inXML += "<LangID>" + dr["ID"].ToString() + "</LangID>"; //FB 1970
                            inXML += "</Option>";
                        }
                        inXML += "</OptionList>";
                    }
                    else
                    {
                        errLabel.Text = obj.GetTranslatedText("Please add atleast one option to the custom option.");//FB 1830 - Translation
                        errLabel.Visible = true;
                        return;
                    }
                }
                inXML += "</CustomAttribute>";

                String outXML = obj.CallMyVRMServer("SetCustomAttribute", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    Session["CalendarMonthly"] = null; //FB 2013
                    Session.Remove("dtLanguages"); //FB 1970
                    Response.Redirect("ViewCustomAttributes.aspx?m=1");
                }
                else
                {
                    //FB 1881 start
                    /*XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(outXML);
                    XmlNode nd = null;
                    nd = xdoc.SelectSingleNode("error");

                    if(nd != null)
                        errLabel.Text = nd.InnerText;
                    else*/
                    errLabel.Text = obj.ShowErrorMessage(outXML);//FB 1881
                    //errLabel.Text = "Error 122: Please contact your VRM Administrator";
                    //FB 1881 end
                    errLabel.Visible = true;
                }
            }
            catch (System.Threading.ThreadAbortException)
            {}
            catch (Exception ex)
            {
                log.Trace(ex.Message);
				//errLabel.Text = "Error 122: Please contact your VRM Administrator";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }
        #endregion

        //FB 1970 start
        #region GetLanguageList
        /// <summary>
        /// GetLanguageList
        /// </summary>
        protected void GetLanguageList()
        {
            try
            {
                String userID = "11,", inXML = "", outXML = "";
                dtable = new DataTable();
                XmlDocument xmldoc = new XmlDocument();

                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                inXML = "<GetLanguages><UserID>" + userID + "</UserID>" + obj.OrgXMLElement() + "</GetLanguages>";
                outXML = obj.CallMyVRMServer("GetLanguages", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLanguages/Language");
                    if (nodes.Count > 0)
                    {
                        dtable = obj.LoadDataTable(nodes, null);
                        if (!dtable.Columns.Contains("Title"))
                            dtable.Columns.Add("Title");

                        for (int i = 0; i < dtable.Rows.Count; i++)
                        {
                            DataRow dr = dtable.Rows[i];
                            //ZD 100288_4Dec2013
                            if (dr["Name"].ToString() == "Spanish") 
                                dr["Name"] = "Espa�ol";

                            if (dr["ID"].ToString() == "1")
                            {
                                dtable.Rows.Remove(dr);
                                i--;
                            }
                            //ZD 100288_4Dec2013
                        }
                        if (Session["dtLanguages"] == null)
                            Session.Add("dtLanguages", dtable);
                        else
                            Session["dtLanguages"] = dtable;
                    }
                    dgTitlelist.DataSource = dtable;
                    dgTitlelist.DataBind();
                    dgLangOptionlist.DataSource = dtable;
                    dgLangOptionlist.DataBind();
                }
                else
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    
            }
            catch (Exception ex)
            {
                log.Trace("GetMultiLangCustomAtt" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region dgEntityCode_ItemDataBound
        /// <summary>
        /// dgEntityCode_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgEntityCode_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    String lblLangID = ((Label)e.Item.FindControl("lblLangID")).Text.Trim();
                    //FB 2349 Start
                    if (isSytemAtt || hdnCreateby.Value == "1") 
                    {
                        ((LinkButton)e.Item.FindControl("btnDelete")).Enabled = false;
                        if (TxtCAName.Text.Equals("Entity Code")) //FB 2535
                            ((LinkButton)e.Item.FindControl("btnEdit")).Enabled = false;
                    }
                    else if ((lblLangID != "1" && lblLangID != "") && (!isSytemAtt))//FB 2349 End
                        ((LinkButton)e.Item.FindControl("btnDelete")).Enabled = false;
                    else
                    {
                        LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                        btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Option?") + "')");//ZD 100288
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("dgEntityCode_ItemDataBound" + ex.Message);
            }
        }
        #endregion
        //FB 1970 start

        // ZD 100288 start
        public string fnGetTranslatedText(string par)
        {
            return obj.GetTranslatedText(par);
        }
        // ZD 100288 End
    }
}
