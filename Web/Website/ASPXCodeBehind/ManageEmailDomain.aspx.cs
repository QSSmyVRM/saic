﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace ns_myVRMNet
{
    public partial class en_ManageEmailDomain : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtNewCompanytName;
        protected System.Web.UI.WebControls.TextBox txtNewEmailDomain;
        protected System.Web.UI.WebControls.Table tblNoEmailDomain;
        protected System.Web.UI.WebControls.DataGrid dgEmailDomain;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDomainID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDomainStatus;
        protected System.Web.UI.WebControls.Button btnSaveDomain;

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        String inXML = "",outXML= "";
        XmlDocument xmldoc = null;
        XmlNodeList nodes = null;
        DataTable dtDomain = null;

        public en_ManageEmailDomain()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageEmailDomain.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                lblHeader.Text = obj.GetTranslatedText("Manage Email Domains");
                errLabel.Text = "";

                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }

                if (!IsPostBack)
                {
                    hdnDomainID.Value = "";
                    BindEmailDomains();
                }

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    
                    //btnSaveDomain.ForeColor = System.Drawing.Color.Gray;
                    //btnSaveDomain.Attributes.Add("Class", "btndisable");//FB 2664
                    //ZD 100263
                    btnSaveDomain.Visible = false;
                }
                else
                {
                    btnSaveDomain.Enabled = true;
                    btnSaveDomain.Attributes.Add("Class", "altMedium0BlueButtonFormat"); // FB 2664
                }

            }
            catch(Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }

        #region BindEmailDomains
        /// <summary>
        /// BindEmailDomains
        /// </summary>
        protected void BindEmailDomains()
        {
            try
            {
                tblNoEmailDomain.Visible = false;
                inXML = "<GetOrgEmailDomains>";
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                inXML += "</GetOrgEmailDomains>";
                outXML = obj.CallMyVRMServer("GetOrgEmailDomains", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNode node = (XmlNode)xmldoc.DocumentElement;
                    nodes = xmldoc.SelectNodes("//getOrgEmailDomains/EmailDomains/EmailDomain");
                    if (nodes.Count > 0)
                    {
                        dtDomain = obj.LoadDataTable(nodes, null);
                        dgEmailDomain.DataSource = dtDomain;
                        dgEmailDomain.DataBind();
                        dgEmailDomain.EditItemIndex = -1;
                        Session.Remove("EmailDomains");
                        Session.Add("EmailDomains", dtDomain);
                    }
                    else
                        tblNoEmailDomain.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindEmailDomains" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region BindEmailDomain
        /// <summary>
        /// BindEmailDomain
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BindEmailDomain(Object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
            {
                String isActive = e.Item.Cells[1].Text.Trim();
                LinkButton btnstatus = (LinkButton)e.Item.FindControl("btnStatus");
                LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnEdit.Attributes.Remove("onClick");
                    //btnEdit.Style.Add("cursor", "default");
                    //btnEdit.ForeColor = System.Drawing.Color.Gray;

                    
                    //btnstatus.Attributes.Remove("onClick");
                    //btnstatus.Style.Add("cursor", "default");
                    //btnstatus.ForeColor = System.Drawing.Color.Gray;
                    //ZD 100263
                    btnEdit.Visible = false;
                    btnstatus.Visible = false;
                }

                if (isActive.Trim() == "1")
                    btnstatus.Text = obj.GetTranslatedText("Deactivate"); //FB 2272
                else
                    btnstatus.Text = obj.GetTranslatedText("Activate"); //FB 2272

                btnstatus.Attributes.Add("onclick", "Javascript:return DomainStatus('" + e.Item.Cells[0].Text + "','" + isActive + "');DataLoading(1)");//ZD 100263
            }
        }
        #endregion

        #region Submit
        /// <summary>
        /// Submit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveDomain(object sender, EventArgs e)
        {
            try
            {
                if (hdnDomainID.Value == "")
                    hdnDomainID.Value = "New";
                
                SaveEmailDomain(hdnDomainID.Value);
            }
            catch (Exception ex)
            {
                log.Trace("Submit" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region EditEmailDomain
        /// <summary>
        /// EditEmailDomain
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected void EditEmailDomain(Object Sender, DataGridCommandEventArgs e)
        {
            try
            {
                dtDomain = new DataTable();
                if (Session["EmailDomains"] != null)
                    dtDomain = (DataTable)Session["EmailDomains"];

                dgEmailDomain.EditItemIndex = e.Item.ItemIndex;
                hdnDomainID.Value = e.Item.Cells[0].Text;
                dgEmailDomain.DataSource = dtDomain;
                dgEmailDomain.DataBind();
                btnSaveDomain.Enabled = false;
                btnSaveDomain.Attributes.Add("Class", "btndisable");//FB 2664
            }
            catch (Exception ex)
            {
                log.Trace("EditEmailDomain" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }

        }
        #endregion

        #region CancelEmailDomain
        /// <summary>
        /// CancelEmailDomain
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected void CancelEmailDomain(Object Sender, DataGridCommandEventArgs e)
        {
            try
            {
                dtDomain = new DataTable();
                if(Session["EmailDomains"] != null)
                    dtDomain =  (DataTable)Session["EmailDomains"];
                
                dgEmailDomain.EditItemIndex = -1;
                dgEmailDomain.DataSource = dtDomain;
                dgEmailDomain.DataBind();
                btnSaveDomain.Enabled = true;
                btnSaveDomain.Attributes.Add("Class", "altMedium0BlueButtonFormat");//FB 2664
            }
            catch (Exception ex)
            {
                log.Trace("CancelEmailDomain" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }

        }
        #endregion

        #region UpdateEmailDomain
        /// <summary>
        /// UpdateEmailDomain
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected void UpdateEmailDomain(Object Sender, DataGridCommandEventArgs e)
        {
            try
            {
                txtNewCompanytName = (TextBox)e.Item.FindControl("txtCompanyName");
                txtNewEmailDomain = (TextBox)e.Item.FindControl("txtEmailDomain");
                dgEmailDomain.EditItemIndex = -1;
                SaveEmailDomain(hdnDomainID.Value);
                btnSaveDomain.Enabled = true;
                btnSaveDomain.Attributes.Add("Class", "altMedium0BlueButtonFormat");//FB 2664
            }
            catch (Exception ex)
            {
                log.Trace("UpdateEmailDomain" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }

        }
        #endregion

        #region SaveEmailDomain
        /// <summary>
        /// SaveEmailDomain
        /// </summary>
        /// <param name="DomainID"></param>
        protected void SaveEmailDomain(String DomainID)
        {
            try
            {
                if (DomainID.Trim() == "")
                    return;
                else
                {
                    for (int i = 0; i < dgEmailDomain.Items.Count; i++)
                    {
                        if (dgEmailDomain.Items[i].Cells[0].Text == DomainID)
                        {
                            txtNewCompanytName.Text = ((Label)dgEmailDomain.Items[i].FindControl("lblCompanyName")).Text;
                            txtNewEmailDomain.Text = ((Label)dgEmailDomain.Items[i].FindControl("lblEmailDomain")).Text;
                        }
                    }
                }
                if (hdnDomainStatus.Value == "")
                    hdnDomainStatus.Value = "1";

                inXML += "<SaveEmailDomain>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                inXML += "  <EmailDomain>";
                inXML += "      <DomainID>" + DomainID.ToString() + "</DomainID>";
                inXML += "      <Companyname>" + txtNewCompanytName.Text + "</Companyname>";
                inXML += "      <Emaildomain>" + txtNewEmailDomain.Text + "</Emaildomain>";
                inXML += "      <isActive>" + hdnDomainStatus.Value + "</isActive>";
                inXML += "  </EmailDomain>";
                inXML += "</SaveEmailDomain>";

                outXML = obj.CallMyVRMServer("SaveEmailDomain", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    //Response.Redirect("ManageEmailDomain.aspx?m=1");
                    txtNewCompanytName.Text = "";
                    txtNewEmailDomain.Text = "";
                    hdnDomainID.Value = "";
                    BindEmailDomains();
                    errLabel.Text = obj.ShowSuccessMessage();
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("SaveEmailDomain" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion
    }
}
