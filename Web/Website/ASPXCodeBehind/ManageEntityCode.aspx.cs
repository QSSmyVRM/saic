﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;

namespace ns_ManageEntityCode
{
    public partial class ManageEntityCode : System.Web.UI.Page
    {
        #region Protected Data Members
        protected System.Web.UI.WebControls.TextBox txtEntityName;
        protected System.Web.UI.WebControls.TextBox txtEntityDesc;
        protected System.Web.UI.WebControls.TextBox txtEntityID;
        protected System.Web.UI.WebControls.TextBox txtOptionName;
        protected System.Web.UI.WebControls.TextBox txtOptionDesc;
        protected System.Web.UI.WebControls.TextBox txtOptionID;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblEntityOption;
        protected System.Web.UI.WebControls.Label lblDisplay;

        protected System.Web.UI.WebControls.Table tblNoOptions;
        protected System.Web.UI.HtmlControls.HtmlTableRow trLang;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDisplay;

        protected System.Web.UI.WebControls.DataGrid dgEntityCode;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnUpdate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnOptionId;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLangName;

        DataTable optionsTable = null;
        DataTable optionLanTable = null;
        ArrayList colNames = null;
        DataTable dtable = new DataTable();
        String languageId = "";

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        #endregion

        #region ManageEntityCode

        public ManageEntityCode()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region Methods Executed on Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("ManageEntityCode.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            if (Session["languageID"] != null)
            {
                if (Session["languageID"].ToString() != "")
                    languageId = Session["languageID"].ToString();
            }

            //FB 2670
            if (Session["admin"].ToString().Equals("3"))
            {
                
                //btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                //ZD 100263
                btnSubmit.Visible = false;
            }
            else
                btnSubmit.Attributes.Add("Class", "altShortBlueButtonFormat");// FB 2796

            if (!IsPostBack)
            {
                GetLanguageList();
                GetEntityCode();
            }

            errLabel.Text = "";
            lblDisplay.Text = obj.GetTranslatedText("Entity Codes displayed in ") + hdnLangName.Value + "";
        }

        #endregion

        #region CreateNewEntityCode
        /// <summary>
        /// CreateNewEntityCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void CreateNewEntityCode(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EditEntityOption.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("CreateNewEntityCode" + ex.Message);
            }
        }
        #endregion

        #region GetEntityCode
        /// <summary>
        /// GetEntityCode
        /// </summary>
        private void GetEntityCode()
        {
            try
            {
                String inXML = "";

                inXML += "<GetEntityCode>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "</GetEntityCode>";
                String outXML = obj.CallMyVRMServer("GetEntityCode", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetEntityCode outxml: " + outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);

                    XmlNodeList optNodes = xd.SelectNodes("//GetEntityCode/OptionList/Option");
                    if (optNodes.Count > 0)
                    {
                        optionsTable = obj.LoadDataTable(optNodes, colNames);
                        if (!optionsTable.Columns.Contains("RowUID"))
                            optionsTable.Columns.Add("RowUID");

                        int rowCnt = 1;
                        foreach (DataRow dr in optionsTable.Rows)
                        {
                            dr["RowUID"] = rowCnt++;
                        }

                        String filter = "ID='" + languageId + "'";
                        optionsTable.DefaultView.RowFilter = filter;

                        dgEntityCode.DataSource = optionsTable;
                        dgEntityCode.DataBind();

                        //FB 2670
                        if (Session["admin"].ToString() == "3")
                        {
                            foreach (DataGridItem dgi in dgEntityCode.Items)
                            {
                                LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                                LinkButton btnDelete = ((LinkButton)dgi.FindControl("btnDelete"));

                                btnEdit.Text = "View";

                                
                                //btnDelete.Attributes.Remove("onClick");
                                //btnDelete.Style.Add("cursor", "default");
                                //btnDelete.ForeColor = System.Drawing.Color.Gray;
                                //ZD 100263
                                btnDelete.Visible = false;

                            }
                        }
                        Session.Remove("dtEntityCode");
                        Session.Add("dtEntityCode", optionsTable);
                    }
                    else
                    {
                        Session["dtEntityCode"] = null;
                        tblNoOptions.Visible = true;
                        trDisplay.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetEntityCode" + ex.Message);
            }

        }
        #endregion

        #region GetConfOptionID
        /// <summary>
        /// GetConfOptionID
        /// </summary>
        /// <param name="optionID"></param>
        /// <param name="msg"></param>
        /// <returns></returns>

        protected String GetConfOptionID(String optionID, ref String msg)
        {
            try
            {
                int completedConfcount = 0, futureConfcount = 0;
                StringBuilder inXML = new StringBuilder();

                inXML.Append("<GetConfListByEntityOptID>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<entitycode>");
                inXML.Append("<optionID>" + optionID + "</optionID>");
                inXML.Append("</entitycode>");
                inXML.Append("</GetConfListByEntityOptID>");
                string outXML = obj.CallMyVRMServer("GetConfListByEntityOptID", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);

                    XmlNode node = null;
                    node = xd.SelectSingleNode("//error/conferencelist/completedConfcount");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out completedConfcount);

                    node = xd.SelectSingleNode("//error/conferencelist/futureConfcount");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out futureConfcount);

                    if (completedConfcount > 0)
                        msg = completedConfcount + " " + obj.GetTranslatedText("Completed Conference(s)");//FB 2272

                    if (futureConfcount > 0)
                    {
                        if (msg == "")
                            msg = futureConfcount + " " + obj.GetTranslatedText("Scheduled Conference(s)");
                        else
                        {
                            if (futureConfcount > 1)
                                msg += " " + obj.GetTranslatedText("and") + " " + futureConfcount + " " + obj.GetTranslatedText("Scheduled conference(s)");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetConfOptionID" + ex.Message);
            }
            return msg;

        }
        #endregion

        #region EditEntityCode
        /// <summary>
        /// EditEntityCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void EditEntityCode(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String msg = "", mode = "";
                hdnOptionId.Value = e.Item.Cells[1].Text;

                if (Session["dtEntityCode"] != null)
                    optionsTable = (DataTable)Session["dtEntityCode"];

                optionLanTable = optionsTable.Copy();

                String filter1 = "";
                filter1 = "OptionId = '" + hdnOptionId.Value + "' ";
                optionLanTable.DefaultView.RowFilter = filter1;

                Session.Remove("dtLanguage");
                Session.Add("dtLanguage", optionLanTable);

                GetConfOptionID(hdnOptionId.Value, ref msg);

                if (msg != "" && Session["admin"].ToString() != "3") //FB 2670
                {
                    mode = "E";
                    msg += obj.GetTranslatedText("are linked to the selected entity option and data will be lost.") + " \\n" + obj.GetTranslatedText("Are you sure you want to edit this Entity Code?"); //FB 2272
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "EditConfirm", "<script>FnConfirm('" + msg + "','" + mode + "');</script>", false);
                }
                else
                {
                    Response.Redirect("EditEntityOption.aspx?OptionID=" + hdnOptionId.Value);

                }
            }
            catch (Exception ex)
            {
                log.Trace("EditEntityCode" + ex.Message);
            }
        }

        #endregion

        #region EditConfByOptionId
        /// <summary>
        /// EditConfByOptionId
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditConfByOptionId(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();

                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<entitycode>");
                inXML.Append("<optionID>" + hdnOptionId.Value + "</optionID>");
                inXML.Append("</entitycode>");
                inXML.Append("</login>");
                string outXML = obj.CallMyVRMServer("EditConfEntityOption", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("EditEntityOption.aspx?OptionID=" + hdnOptionId.Value);
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("EditConfByOptionId" + ex.Message);
            }

        }
        #endregion

        #region DeleteConfByOptionId
        /// <summary>
        /// DeleteConfByOptionId
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteConfByOptionId(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();

                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<entitycode>");
                inXML.Append("<optionID>" + hdnOptionId.Value + "</optionID>");
                inXML.Append("</entitycode>");
                inXML.Append("</login>");
                string outXML = obj.CallMyVRMServer("DeleteConfEntityOptionID", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                    Response.Redirect("ManageEntityCode.aspx?m=1");
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("DeleteConfByOptionId" + ex.Message);
            }

        }
        #endregion

        #region DeleteEntityCode
        /// <summary>
        /// DeleteEntityCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteEntityCode(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String msg = "";
                hdnOptionId.Value = e.Item.Cells[1].Text.Trim();

                GetConfOptionID(hdnOptionId.Value, ref msg);

                if (msg != "")
                {
                    String mode = "D";
                    msg += obj.GetTranslatedText("are linked to the selected entity option and data will be lost. \\n Are you sure you want to delete this Entity Code?");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "deleteConfirm", "<script>FnConfirm('" + msg + "','" + mode + "');</script>", false);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "DeleteeConfirm", "<script>FnDeleteConfirm(true);</script>", false);
                }
            }
            catch (Exception ex)
            {
                log.Trace("DeleteEntityCode" + ex.Message);
            }
        }
        #endregion

        #region DeleteConfEntityCode
        /// <summary>
        /// DeleteConfEntityCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void DeleteConfEntityCode(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<DeleteEntityCode>");
                inXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<delete>");
                inXML.Append("<optionId>" + hdnOptionId.Value + "</optionId>");
                inXML.Append("</delete>");
                inXML.Append("</DeleteEntityCode>");

                String outXML = obj.CallMyVRMServer("DeleteEntityCode", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                    Response.Redirect("ManageEntityCode.aspx?m=1");
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }

            }
            catch (Exception ex)
            {
                log.Trace("DeleteConfEntityCode" + ex.Message);
            }

        }

        #endregion

        #region GetLanguageList
        /// <summary>
        /// GetLanguageList
        /// </summary>
        protected void GetLanguageList()
        {
            try
            {
                String userID = "11,", inXML = "", outXML = "";
                dtable = new DataTable();
                XmlDocument xmldoc = new XmlDocument();

                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                inXML = "<GetLanguages><UserID>" + userID + "</UserID>" + obj.OrgXMLElement() + "</GetLanguages>";
                outXML = obj.CallMyVRMServer("GetLanguages", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xd = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNode node = null;
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLanguages/Language");
                    string langName = "", langId = "";
                    foreach (XmlNode Xlnode in nodes)
                    {
                        node = Xlnode.SelectSingleNode("ID");
                        if (node != null)
                            langId = node.InnerText.Trim();

                        node = Xlnode.SelectSingleNode("Name");
                        if (node != null)
                            langName = node.InnerText.Trim();

                        if (langId == languageId)
                            hdnLangName.Value = langName;
                    }
                }
                else
                    errLabel.Text = obj.ShowErrorMessage(outXML);

            }
            catch (Exception ex)
            {
                log.Trace("GetLanguageList" + ex.Message);
            }
        }
        #endregion

        #region btnCancel_Click
        /// <summary>
        /// btnCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("OrganisationSettings.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("btnCancel_Click" + ex.Message);
            }
        }
        #endregion

    }
}
