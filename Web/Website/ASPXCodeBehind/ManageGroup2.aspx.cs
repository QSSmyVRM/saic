/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;


namespace ns_MyVRM
{
    public partial class ManageGroup2: System.Web.UI.Page
    {
        #region Protected Data Members

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox GroupName;
        protected System.Web.UI.WebControls.RequiredFieldValidator ReqFieldGName;
        protected System.Web.UI.WebControls.RegularExpressionValidator regGName;
        protected System.Web.UI.WebControls.RegularExpressionValidator regGrpDescription;
        protected System.Web.UI.WebControls.CheckBox GroupPublic;
        protected System.Web.UI.WebControls.TextBox GroupDescription;
        protected System.Web.UI.WebControls.ListBox Group;
        protected System.Web.UI.WebControls.TextBox UsersStr;
        protected System.Web.UI.WebControls.TextBox PartysInfo;
        protected System.Web.UI.WebControls.Button Managegroup2Submit;

        #endregion

        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        public Boolean searchgroup = false;
        private Int32 groupID = Int32.MinValue;
        private String strpartysInfo = "";

        #endregion

        public ManageGroup2()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #region Page Load Event Handler

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageGroup2.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                errLabel.Text = "";

                if (Request.QueryString["groupID"] != null)
                {
                    if (Request.QueryString["groupID"].ToString() != "")
                    {
                        groupID = Convert.ToInt32(Request.QueryString["groupID"]);
                    }
                }
                
                if (!IsPostBack)
                    BindData();
                
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                errLabel.Visible = true;
                log.Trace("pageLoad" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }

        #endregion

        #region BindData 

        private void BindData()
        {
            String inXML = "";

            if (groupID == Int32.MinValue)
                lblHeader.Text = obj.GetTranslatedText("Create Group");//FB 1830 - Translation
            else
                lblHeader.Text = obj.GetTranslatedText("Edit Group");//FB 1830 - Translation

            //FB 2027 - Starts
            inXML += "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><groupID>" + groupID + "</groupID></login>";
            //String outXML = obj.CallCOM("GetGroup", inXML, Application["COM_ConfigPath"].ToString());
            String outXML = obj.CallMyVRMServer("GetGroup", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            //FB 2027 - End
            //Response.Write(obj.Transfer(outXML));
            //String outXML = "<bridgeInfo><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><bridges><bridge><ID>1</ID><name>Test Bridge</name><interfaceType>3</interfaceType><administrator>VRM Administrator</administrator><exist>1</exist><status>1</status><order></order></bridge></bridges><totalNumber>1</totalNumber><licensesRemain>9</licensesRemain></bridgeInfo>";
            if (outXML.IndexOf("<error>") < 0)
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//groups/group");

                LoadGroupDetailsTable(nodes);
                
                if (groupID != Int32.MinValue)
                    LoadPageGrid(nodes);
            }
            else
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region LoadGroupDetailsTable

        protected void LoadGroupDetailsTable(XmlNodeList nodes)
        {
            string strUser = "";
            try
            {                
                Group.Items.Clear();
                strUser = "";
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (!nodes[i].SelectSingleNode("groupID").InnerText.Equals(groupID.ToString()))
                    {
                        Group.Items.Add(new ListItem(nodes[i].SelectSingleNode("groupName").InnerText, nodes[i].SelectSingleNode("groupID").InnerText));
                        XmlNodeList subnotes = nodes[i].SelectNodes("user");

                        strUser += BindMembers(subnotes);
                        strUser += "``"; // FB 1888
                    }
                }

                if (strUser.Length > 0)
                    UsersStr.Text = strUser.Substring(0, strUser.Length - 2); //FB 1888

                Group.Attributes.Add("onchange", "JavaScript:groupChg();");
                Group.Attributes.Add("onDblClick", "JavaScript:getAGroupDetail('g', this, null)");
            }
            catch (Exception ex)
            {
                log.Trace("LoadGroupDetailTable" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion

        #region LoadPageGrid 

        protected void LoadPageGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                Int32 i = 0;
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("groupID").InnerText.Equals(groupID.ToString()))
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        strpartysInfo = "";
                        XmlNodeList subnotes = node.SelectNodes("user");
                        PartysInfo.Text = BindMembers(subnotes);
                        break;
                    }
                    i++;
                }
            
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["groupName"].ToString() != "")
                            GroupName.Text  = dr["groupName"].ToString();
                        if (dr["description"].ToString() != "")
                            GroupDescription.Text = dr["description"].ToString();
                        if (dr["public"].ToString().Trim().Equals("1"))
                            GroupPublic.Checked = true;
                        else
                            GroupPublic.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadPageGrid" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion

        #region BindMembers 

        private String BindMembers(XmlNodeList subNodes)
        {
            String uStr = "";
            int sublength = subNodes.Count;
            
            for (int j = 0; j < sublength; j++)
            {
                string userID = subNodes[j].SelectSingleNode("userID").InnerText;
                string userFirstName = subNodes[j].SelectSingleNode("userFirstName").InnerText;
                string userLastName = subNodes[j].SelectSingleNode("userLastName").InnerText;
                string userEmail = subNodes[j].SelectSingleNode("userEmail").InnerText;

                // uStr += userID + "," + userFirstName + "," + userLastName + "," + userEmail + ",0,1,0,1,0,1,,,3,,,;"; FB 1888
                uStr += userID + "!!" + userFirstName + "!!" + userLastName + "!!" + userEmail + "!!0!!1!!0!!1!!0!!1!!!!!!3!!!!!!||"; //FB 1888
            }
            
            return uStr;
        }
        #endregion

        #region Submit Button Event Handler

        protected void Managegroup2Submit_Click(object sender, EventArgs e)
        {
            try
            {
                //FB 2027 - Starts
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <userInfo>");
                inXML .Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("  </userInfo>");
                inXML.Append("  <groupInfo>");
                inXML.Append("  <group>");
                String grpID = "new";
                if (groupID != Int32.MinValue)
                    grpID = groupID.ToString();
                inXML.Append("<groupID>" + grpID + "</groupID>");
                inXML.Append("<groupName>" + GroupName.Text + "</groupName>");
                inXML.Append("<ownerID>"+ Session["userID"].ToString() +"</ownerID>");
                inXML.Append("<ownerName>" + Session["userName"].ToString() + "</ownerName>");
                inXML.Append("<description>" + GroupDescription.Text + "</description>");
                String grpPublic = "0";
                if(GroupPublic.Checked) grpPublic = "1";
                inXML.Append("<public>" + grpPublic + "</public>");

                if (PartysInfo.Text.Trim() != "")
                {
                    //FB 1888 Starts
                    String[] delimiterArr = { "||" };
                    String[] delimiterArr1 = { "!!" };
                    //String[] partysInfoSplit = PartysInfo.Text.Split(';');
                    String[] partysInfoSplit = PartysInfo.Text.Split(delimiterArr, StringSplitOptions.RemoveEmptyEntries);
                    //FB 1888 Ends
                    String[] pInfoDetails = null;
                    
                    for (Int32 s = 0; s < partysInfoSplit.Length ; s++) // FB 1888
                    {
                        pInfoDetails = null;
                        //pInfoDetails = partysInfoSplit[s].Split(','); FB 1888
                        pInfoDetails = partysInfoSplit[s].Split(delimiterArr1, StringSplitOptions.RemoveEmptyEntries); //FB 1888
                        pInfoDetails[0] = pInfoDetails[0].Replace("|", ""); //FB 1914
                        inXML.Append("<user>");
                        inXML.Append("<userID>" + pInfoDetails[0] + "</userID>");
                        inXML.Append("<userFirstName>" + pInfoDetails[1] + "</userFirstName>");
                        inXML.Append("<userLastName>" + pInfoDetails[2] + "</userLastName>");
                        inXML.Append("<userEmail>" + pInfoDetails[3] + "</userEmail>");
                        inXML.Append("</user>");
                    }
                }
                inXML.Append("</group>");
                inXML.Append("</groupInfo>");
                inXML.Append("</login>");

                //inXML = "<userInfo><userID>11</userID></userInfo><groupInfo><group><groupID>5</groupID><groupName>Group 1</groupName><ownerID>11</ownerID><ownerName>VRM Administrator</ownerName><description>for test from asp</description><public>1</public><user><userID>300</userID><userFirstName>Saima2</userFirstName><userLastName>Ahmad2</userLastName><userEmail>saima2@myvrm.com</userEmail></user></group></groupInfo>";
                log.Trace("SetGroup inXML: " + inXML.ToString());
                //String outXML = obj.CallCOM("SetGroup", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("SetGroup", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
                //Response.Write(obj.Transfer(inXML));
                log.Trace("SetGroup Outxml: " + outXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                    Response.Redirect("ManageGroup.aspx?m=1");

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }
        #endregion
    }
}
