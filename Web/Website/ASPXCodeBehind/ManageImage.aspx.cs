/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;

namespace ns_ManageImage
{
    public partial class ManageImage : System.Web.UI.Page
    {
        #region Protected Data Members

        protected System.Web.UI.WebControls.Button btnUploadFiles;
        protected System.Web.UI.WebControls.Button btnRemove;
        protected System.Web.UI.HtmlControls.HtmlInputFile file1;
        protected System.Web.UI.HtmlControls.HtmlInputFile file2;
        protected System.Web.UI.HtmlControls.HtmlInputFile file3;
        protected System.Web.UI.WebControls.Label lblUploadFile1;
        protected System.Web.UI.WebControls.Label lblUploadFile2;
        protected System.Web.UI.WebControls.Label lblUploadFile3;
        protected System.Web.UI.HtmlControls.HtmlImage img1;
        protected System.Web.UI.HtmlControls.HtmlImage img2;
        protected System.Web.UI.HtmlControls.HtmlImage img3;
        //protected System.Web.UI.HtmlControls.HtmlImage ImgRoomDIV;
        protected System.Web.UI.HtmlControls.HtmlInputHidden RoomImage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden fileNM;
        protected System.Web.UI.HtmlControls.HtmlInputHidden RoomName;
        protected System.Web.UI.HtmlControls.HtmlGenericControl RmImg1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl RmImg2;
        protected System.Web.UI.HtmlControls.HtmlGenericControl RmImg3;
        protected System.Web.UI.HtmlControls.HtmlTableCell ImgNameDIV;
        protected System.Web.UI.HtmlControls.HtmlTableCell RoomImgDIV;
        protected System.Web.UI.WebControls.Button btnCancel;
        protected String language = "";//FB 1830

        #endregion

        #region Private Data Members
        ns_Logger.Logger log = null;
        myVRMNet.NETFunctions obj;//FB 1830 - Translation
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("ManageImage.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            obj = new myVRMNet.NETFunctions();//FB 1830 - Translation
            //FB 1830 - Starts
            if (Session["language"] == null)
                Session["language"] = "en";

            if (Session["language"].ToString() != "")
                language = Session["language"].ToString();
            //FB 1830 - End

            IntializeUIResources();
        }
        #endregion

        #region Intialize UI Resources

        private void IntializeUIResources()
        {
            this.btnUploadFiles.Click += new EventHandler(btnUploadFiles_Click);
            this.btnRemove.Click += new EventHandler(btnRemove_Click);

        }


        #endregion

        #region Button Event Handlers

        #region Upload Button Event Handlers
        /// <summary>
        /// To Upload Image File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnUploadFiles_Click(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            String fName;
            HttpPostedFile myFile;
            int nFileLen;
            byte[] myData;
            String fPath = "";
            String fChk = "";
            try
            {
                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/image/room/"; //FB 1830
                if (!file1.Value.Equals(""))
                {
                    fName = Path.GetFileName(file1.Value);
                    //fName = fName.Remove(fName.LastIndexOf("."));
                    fName = fName.Remove(fName.IndexOf("."));
                    fName = fName + ".jpg";
                    fChk = HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\room\\" + fName;
                    if (File.Exists(fChk))
                    {
                        lblUploadFile1.Visible = true;
                        lblUploadFile1.Text = obj.GetTranslatedText("File trying to upload already exists");//FB 1830 - Translation
                        lblUploadFile1.CssClass = "lblError";
                    }
                    else
                    {
                        myFile = file1.PostedFile;
                        nFileLen = myFile.ContentLength;
                        myData = new byte[nFileLen];
                        myFile.InputStream.Read(myData, 0, nFileLen);
                        if (nFileLen <= 10000000)
                        {
                            WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\room\\" + fName, ref myData);
                            file1.Disabled = true;
                            fileNM.Value = fName;
                            lblUploadFile1.Text = "<a href='" + fPath + fName + "' target='_blank'>" + fName + "</a>";
                            lblUploadFile1.Text = fName + "Upload Done";

                            file1.Visible = false;
                            lblUploadFile1.Visible = true;
                            img1.Visible = true;
                            RmImg1.Visible = false;
                            //Label lblUP = new Label();
                            //lblUP.Text = "done";
                            //lblUP.SkinID = lblUP.Font.Italic.ToString();
                            lblUploadFile1.Text = "File (" + fName + "):" + " " + "upload ...done";
                            lblUploadFile1.CssClass = "";
                            //Code added for the uploaded images to be update on 'ManageRoomProfile.aspx' Start
                            RoomName.Value = fName.Replace(".jpg", "");
                            //Code added for the uploaded images to be update on 'ManageRoomProfile.aspx' End
                            btnUploadFiles.Visible = false;
                            btnCancel.Text = obj.GetTranslatedText("Close");//FB 1830 - Translation
                        }
                        else
                        {
                            lblUploadFile1.Visible = true;
                            lblUploadFile1.Text = obj.GetTranslatedText("File attachment is greater than 10MB. File has not been uploaded.");//FB 1830 - Translation
                            lblUploadFile1.CssClass = "lblError";
                        }
                        //ImgRoomDIV.Src = fChk;
                        //Response.Write("RoomImgDIV.InnerHTML = <img style='width:400;height:300' src='" + fChk + "' />");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);

            }
        }

        #endregion

        #region Remove Button Event Handlers
        /// <summary>
        /// To Remove Image File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnRemove_Click(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            String delFile = "";
            try
            {
                if (RoomImage.Value != "")
                {
                    delFile = HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\room\\" + RoomImage.Value;
                    if (File.Exists(delFile))
                    {
                        File.Delete(delFile);
                        btnCancel.Text = obj.GetTranslatedText("Close");//FB 1830 - Translation
                        img1.Visible = true;
                        lblUploadFile1.Visible = true;
                        file1.Visible = false;
                        RmImg1.Visible = false;
                        btnUploadFiles.Visible = false;
                        Label lbl = new Label();
                        lbl.Text = obj.GetTranslatedText("deleted");//FB 1830 - Translation
                        lbl.Font.Underline = true;
                        lbl.Font.Italic = true;

                        lbl.CssClass = "underLine";



                        //Font prototype = lbl.Font;
                        //lbl.Font = new Font(prototype, prototype.Style | FontStyle.Underline);

                        lblUploadFile1.Text = "File (" + RoomImage.Value + ")" + lbl.Text + "...";
                        lblUploadFile1.CssClass = "lblError";

                    }
                    RoomImage.Value = "";
                }
            }

            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);

            }
        }

        #endregion

        #endregion

        #region User Defined Methods

        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
    }
}
