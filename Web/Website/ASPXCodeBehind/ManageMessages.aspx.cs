﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace ns_MyVRM
{

    public partial class ManageMessages : System.Web.UI.Page
    {

        #region ProtectDataMember

        protected System.Web.UI.WebControls.Label lblError;
        protected System.Web.UI.WebControls.Label lblTotalMessage;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.TextBox txtTxtMsgID;
        protected System.Web.UI.WebControls.TextBox txtTxtMsg;
        protected System.Web.UI.WebControls.TextBox txtMsg;
        protected System.Web.UI.WebControls.DataGrid dgTxtMsg;
        protected System.Web.UI.WebControls.DataGrid dgLangOptionlist;
        protected System.Web.UI.WebControls.ImageButton imgLangsOption;
        protected System.Web.UI.HtmlControls.HtmlTableRow LangsOptionRow;
        protected System.Web.UI.WebControls.Button btnNewMessage; //FB 2670
        

        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        DataTable dtable = new DataTable(); 

        #endregion
        
        #endregion

        #region ManageMessages
        /// <summary>
        /// ManageMessages
        /// </summary>
        public ManageMessages()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageMessages.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                lblError.Text = "";
                imgLangsOption.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + LangsOptionRow.ClientID + "', false);return false;");
                //txtTxtMsg.Attributes.Add("OnKeyUp", "javascript:maxCharRowShow()");

                if (!IsPostBack)
                {
                    //GetLanguageList();
                    GetAllMessage();
                }

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnNewMessage.ForeColor = System.Drawing.Color.Gray; //FB 2796
                    //btnNewMessage.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnNewMessage.Visible = false;
                }
                else
                    btnNewMessage.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796

            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region GetAllMessage
        /// <summary>
        /// GetAllMessage
        /// </summary>
        private void GetAllMessage()
        {
            try
            {
                string inXML = "", outXML = "";

                inXML = "<login>"
                      + obj.OrgXMLElement()
                      + "  <userID>" + Session["userID"].ToString() + "</userID>"
                      + "</login>";

                outXML = obj.CallMyVRMServer("GetAllMessage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    DataTable dt = new DataTable();

                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetAllMessage/MessageList/Message");

                    if (nodes.Count > 0)
                    {
                        dt = obj.LoadDataTable(nodes, null);

                        dgTxtMsg.DataSource = dt;
                        dgTxtMsg.DataBind();

                        //lblTotalMessage.Text = nodes.Count.ToString();
                    }
                }
                else
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                    lblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                if (dgTxtMsg.CurrentPageIndex >= dgTxtMsg.PageCount)
                {
                    dgTxtMsg.CurrentPageIndex = 0;
                    dgTxtMsg.DataBind();
                }
                else
                {
                    log.Trace("GetAllMessage" + ex.Message);
                    lblError.Text = obj.ShowSystemMessage();
                }
            }
        }
        #endregion

        #region GetLanguageList
        /// <summary>
        /// GetLanguageList
        /// </summary>
        protected void GetLanguageList()
        {
            try
            {
                String userID = "11,", inXML = "", outXML = "";
                dtable = new DataTable();
                XmlDocument xmldoc = new XmlDocument();

                if (Session["userID"] != null)
                    userID = Session["userID"].ToString();

                inXML = "<GetLanguages><UserID>" + userID + "</UserID>" + obj.OrgXMLElement() + "</GetLanguages>";
                outXML = obj.CallMyVRMServer("GetLanguages", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLanguages/Language");
                    if (nodes.Count > 0)
                    {
                        dtable = obj.LoadDataTable(nodes, null);
                        if (!dtable.Columns.Contains("Title"))
                            dtable.Columns.Add("Title");

                        for (int i = 0; i < dtable.Rows.Count; i++)
                        {
                            DataRow dr = dtable.Rows[i];
                            if (dr["ID"].ToString() == "1")
                                dtable.Rows.Remove(dr);
                        }
                        if (Session["dtLanguages"] == null)
                            Session.Add("dtLanguages", dtable);
                        else
                            Session["dtLanguages"] = dtable;
                    }

                    dgLangOptionlist.DataSource = dtable;
                    dgLangOptionlist.DataBind();
                }
                else
                    lblError.Text = obj.ShowErrorMessage(outXML);
            }
            catch (Exception ex)
            {
                log.Trace("GetLanguageList" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;
                    //LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    //if(btnTemp.Enabled == true)
                    //    btnTemp.Attributes.Add("onclick", "javascript:return fnDelete(this.disabled)");

                    //Label lblLangName = (Label)e.Item.FindControl("lblLangName");
                    //if (lblLangName.Text.IndexOf("English") < 0 && lblLangName.Text != "")
                    //{
                    //    lblLangName.Text = "&nbsp;&nbsp;&nbsp;&nbsp;" + lblLangName.Text;

                    //    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                    //    btnDelete.Visible = false;
                    //}
                    //FB 2670
                    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                    LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");

                    if (row["Type"].ToString() == "1" || Session["admin"].ToString() == "3")
                    {
                        
                        //btnEdit.Attributes.Remove("onClick");
                        //btnEdit.Style.Add("cursor", "default");
                        //btnEdit.ForeColor = System.Drawing.Color.Gray;  

                        
                        //btnDelete.Attributes.Remove("onClick");
                        //btnDelete.Style.Add("cursor", "default");
                        //btnDelete.ForeColor = System.Drawing.Color.Gray;
                        //ZD 100263
                        btnEdit.Visible = false;
                        btnDelete.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindRowsDeleteMsg" + ex.Message);//ZD 100263
                lblError.Text = obj.ShowSystemMessage();//ZD 100263
                lblError.Visible = true;
            }
        }
       
        #endregion

        #region BindMessages
        protected void BindMessages(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {

                    Label lblLangName = (Label)e.Item.FindControl("lblLangName");

                    if (lblLangName.Text.IndexOf("English") < 0 && lblLangName.Text != "")
                    {
                        lblLangName.Text = "&nbsp;&nbsp;&nbsp;&nbsp;" + lblLangName.Text;

                        LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                        btnDelete.Visible = false;
                    }

                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    if (btnTemp.Enabled == true)
                        btnTemp.Attributes.Add("onclick", "javascript:return fnDelete(this.disabled)");
                    else
                        btnTemp.Attributes.Add("onclick", "javascript:return false");
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindMessages" + ex.Message);//ZD 100263
                lblError.Text = obj.ShowSystemMessage();//ZD 100263
                lblError.Visible = true;
            }
        }
        #endregion

        #region dgTxtMsg_PageIndexChanged
        protected void dgTxtMsg_PageIndexChanged(Object sender, DataGridPageChangedEventArgs e)
        {
            try
            {
                dgTxtMsg.CurrentPageIndex = e.NewPageIndex;
                GetAllMessage();
            }
            catch (Exception ex)
            {
                lblError.Text = obj.ShowSystemMessage();//ZD 100263
                lblError.Visible = true;
            }
        }
        #endregion

        #region SetMessages
        /// <summary>
        /// SetMessages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetMessages(object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();
                inXML += " <userID>" + Session["userID"].ToString() + "</userID>";

                inXML += "<MessageList>";

                inXML += "<Message>";
                inXML += "<TxtMsg>" + txtTxtMsg.Text + "</TxtMsg>";
                inXML += "<Type>0</Type>";
                inXML += "<LangID>1</LangID>";
                inXML += "</Message>";
                /*
                for (int i = 0; i < dgLangOptionlist.Items.Count; i++)
                {
                    if (i == 0)
                    {
                        inXML += "<Message>";
                        inXML += "<TxtMsg>" + txtTxtMsg.Text + "</TxtMsg>";
                        inXML += "<Type>0</Type>";
                        inXML += "<LangID>1</LangID>";
                        inXML += "</Message>";
                    }

                    TextBox txtmsg = ((TextBox)dgLangOptionlist.Items[i].FindControl("txtMsg"));
                    String strMsg = txtmsg.Text.Trim();

                    if (strMsg == "")
                        strMsg = txtTxtMsg.Text;

                    inXML += "<Message>";
                    inXML += "<TxtMsg>" + strMsg + "</TxtMsg>";
                    inXML += "<Type>0</Type>";
                    inXML += "<LangID>" + ((Label)dgLangOptionlist.Items[i].FindControl("lblLangID")).Text.Trim() + "</LangID>";
                    inXML += "</Message>";
                }
                 * */
                inXML += "</MessageList>";
                inXML += "</login>";

                SetMessage(inXML);
                
            }
            catch (Exception ex)
            {
                log.Trace("SetMessages" + ex.Message);
            }
        }
        #endregion

        #region SetMessage
        /// <summary>
        /// SetMessage
        /// </summary>
        /// <param name="inXML"></param>
        private void SetMessage(String inXML)
        {
            try
            {
                String outXML = obj.CallMyVRMServer("SetMessage", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    lblError.Text = obj.GetTranslatedText("Operation Successful!");
                    GetAllMessage();
                }
                else
                    lblError.Text = obj.ShowErrorMessage(outXML);

                txtTxtMsg.Text = "";
                if (Session["dtLanguages"] != null)
                {
                    dgLangOptionlist.DataSource = (DataTable)Session["dtLanguages"];
                    dgLangOptionlist.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace("SetMessage" + ex.Message);
            }
        }

         #endregion

        #region EditItem

        protected void EditItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                dgTxtMsg.EditItemIndex = e.Item.ItemIndex;                
                GetAllMessage();
            }
            catch (Exception ex)
            {
                log.Trace("EditItem" + ex.Message);//ZD 100263
                lblError.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }

        #endregion

        #region UpdateItem
        protected void UpdateItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();
                inXML += " <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<MessageList>";
                inXML += "  <Message>";
                inXML += "      <ID>" + ((Label)e.Item.FindControl("lblID")).Text.Trim() + "</ID>";
                inXML += "      <TxtMsg>" + ((TextBox)e.Item.FindControl("txtTxtMsg")).Text.Trim() + "</TxtMsg>";
                inXML += "      <Type>0</Type>";
                inXML += "      <LangID>" + ((Label)e.Item.FindControl("lblLangID")).Text.Trim() + "</LangID>";
                inXML += "  </Message>";
                inXML += "</MessageList>";
                inXML += "</login>";

                dgTxtMsg.EditItemIndex = -1;
                dgTxtMsg.DataBind();

                SetMessage(inXML);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateItem" + ex.Message);//ZD 100263
                lblError.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }

        #endregion

        #region CancelItem
        protected void CancelItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                dgTxtMsg.EditItemIndex = -1;
                dgTxtMsg.DataBind();
                GetAllMessage();               
            }
            catch (Exception ex)
            {
                log.Trace("CancelItem" + ex.Message);//ZD 100263
                lblError.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }
        #endregion

        #region DeleteItem
        /// <summary>
        /// Delete Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteItem(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();
                inXML += " <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += " <MsgID>" + ((Label)e.Item.FindControl("lblMsgID")).Text.Trim() + "</MsgID>";
                inXML += "</login>";

                String outXML = obj.CallMyVRMServer("DeleteMessage", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    lblError.Text = obj.GetTranslatedText("Operation Successful!");
                    GetAllMessage();
                }
                else
                    lblError.Text = obj.ShowErrorMessage(outXML);
            }
            catch (Exception ex)
            {
                log.Trace("DeleteItem" + ex.Message);//ZD 100263
                lblError.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }
        #endregion

    }
}
