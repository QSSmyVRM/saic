/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;

namespace ns_MyVRM
{
    public partial class ManageOrganization : System.Web.UI.Page
    {
        #region Private Variables
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        CustomizationUtil.CSSReplacementUtility cssUtil;  //Organization/CSS Module
        
        private int remainingLicense = 0;
        private int orgLimit = 0;

        #endregion

        #region Protected Variables
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstSortBy;
        protected System.Web.UI.WebControls.Table tblAlphabet;
        protected System.Web.UI.WebControls.DataGrid dgOrganizations;
        protected System.Web.UI.WebControls.Table tblNoOrganizations;
        protected System.Web.UI.WebControls.TextBox txtOrganizationName;
        protected System.Web.UI.WebControls.TextBox txtEmailAddress;
        protected System.Web.UI.WebControls.TextBox txtPhone;
        protected System.Web.UI.WebControls.RegularExpressionValidator regOrgName;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail;
        protected System.Web.UI.WebControls.Button btnSearchOrganization;
        protected System.Web.UI.WebControls.Button btnNewOrganization;
        protected System.Web.UI.WebControls.LinkButton lnkEdit;
        protected System.Web.UI.WebControls.LinkButton lnkDelete;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpnLicense;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpnActiveOrgs;
       
        #endregion

        #region Constructor
        public ManageOrganization()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageOrganization.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                errLabel.Text = "";
                
                if(!IsPostBack)
                    BindData();
                
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful! Please switch to respective organization to see the changes.");//FB 1830 - Translation //FB 2278
                        errLabel.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region Bind Data
        protected void BindData()
        {
            try
            {
                
                String inXML = "<GetOrganizationList>";
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "</GetOrganizationList>";
                
                String outXML = obj.CallMyVRMServer("GetOrganizationList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlNode xnode;
                if (outXML.IndexOf("<error>") >= 0)
                {
                    xnode = xmldoc.SelectSingleNode("<error>");
                    throw new Exception(xnode.InnerText);
                }

                XmlTextReader xtr;
                DataSet ds = new DataSet();
                XmlNodeList nodes = xmldoc.SelectNodes("//GetOrganizationList/Organization");
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataView dv = new DataView(ds.Tables[0]);
                    DataTable dt = new DataTable();
                    
                    dgOrganizations.DataSource = dv;
                    dgOrganizations.DataBind();
                }
                else
                    tblNoOrganizations.Visible = true;

                xnode = xmldoc.SelectSingleNode("//GetOrganizationList/LicenseRemaining");
                string remLicense = xnode.InnerText.Trim();

                xnode = xmldoc.SelectSingleNode("//GetOrganizationList/OrganizationLimit");
                string orglmt = xnode.InnerText.Trim();

                Int32.TryParse(remLicense, out remainingLicense);
                Int32.TryParse(orglmt, out orgLimit);

                if (remainingLicense < 0)
                    remainingLicense = 0;

                SpnLicense.InnerText = remainingLicense.ToString();
                SpnActiveOrgs.InnerText = (orgLimit - remainingLicense).ToString();
                
                if(remainingLicense <= 0)
                {
                    btnNewOrganization.Enabled = false;
                    btnNewOrganization.Attributes.Add("Class", "btndisable"); //FB 2664
                }
                //FB 2664 start
                else
                {
                    btnNewOrganization.Enabled = true;
                    btnNewOrganization.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                }
                //FB 2664 End
            }

            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region Create New Organization
        protected void CreateNewOrganization(Object sender, EventArgs e)
        {
            try
            {
                Session.Remove("OrganizationToEdit");
                Session.Add("OrganizationToEdit", "new");
                Response.Redirect("ManageOrganizationProfile.aspx");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region Edit Organization Profile
        /// <summary>
        /// Edit Organization Profile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditOrganizationProfile(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Remove("OrganizationToEdit");
                Session.Add("OrganizationToEdit", e.Item.Cells[0].Text.ToString());
                Response.Redirect("ManageOrganizationProfile.aspx?");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region Delete Organization Profile
        /// <summary>
        /// Delete Organization Profile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteOrganizationProfile(Object sender,DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "<GetOrganizationList>";
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<orgId>" + e.Item.Cells[0].Text + "</orgId>";
                inXML += "</GetOrganizationList>";

                String outXML = obj.CallMyVRMServer("DeleteOrganizationProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    //Organization\CSS Module - Create folder for UI Settings --- Strat
                    cssUtil = new CustomizationUtil.CSSReplacementUtility();
                    cssUtil.ApplicationPath = Server.MapPath(".."); //FB 1830
                    cssUtil.DeleteDirectoryPath(e.Item.Cells[1].Text.Trim());
                    //Organization\CSS Module -- End
                    Response.Redirect("ManageOrganization.aspx?m=1");
                }
                else
                {
                    //FB 1881 start
                    /*XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);
                    XmlNode nd = null;
                    nd = xd.SelectSingleNode("error");
                    errLabel.Text = nd.InnerText;*/
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    //FB 1881 end
                    errLabel.Visible = true;
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region BindRowsDeleteMessage

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView dr = e.Item.DataItem as DataRowView;
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("lnkDelete");
                    if (dr != null)  // FB 1753
                    {
                        if (dr[0].ToString() != "11")  // FB 1753
                            btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this organization profile?") + "')){DataLoading(1); return true;} else {return false;}");
                        if (dr[0].ToString().Equals("11"))
                            btnTemp.Enabled = false;
                    }

                    //FB 2074
                    btnTemp = (LinkButton)e.Item.FindControl("lnkPurge");
                    if (dr != null)  
                    {
                        if (dr[0].ToString() != "11")  // FB 1753
                            btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Warning: This is an IRREVERSIBLE process. Once purged the data cannot be recovered.Are you sure you want to continue ?") + "')){DataLoading(1); return true;} else {return false;}");
                        if (dr[0].ToString().Equals("11"))
                            btnTemp.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindRowsDeleteMsg" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Purge organization
        /// <summary>
        /// /*** 2074 ***/
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public void PurgeNow_Click(Object sender, DataGridCommandEventArgs e)
        {
            String inXML = "";
            String outXML = "";
            try
            {
                
                inXML = "";
                inXML += "<PurgeOrganization>";
                inXML += "<orgId>" + e.Item.Cells[0].Text + "</orgId>";
                inXML += "  <Path>" + Application["MyVRMServer_ConfigPath"].ToString() + "</Path>";
                inXML += "</PurgeOrganization>";



                outXML = obj.CallMyVRMServer("PurgeOrganization", inXML, Application["MyVRMServer_ConfigPath"].ToString());


                if (outXML.IndexOf("<error>") < 0)
                {
                   
                    cssUtil = new CustomizationUtil.CSSReplacementUtility();
                    cssUtil.ApplicationPath = Server.MapPath(".."); 
                    cssUtil.DeleteDirectoryPath(e.Item.Cells[1].Text.Trim());
                    Response.Redirect("ManageOrganization.aspx?m=1");
                }
                else
                {
                   
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                    
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace); ;
            }
        }

        #endregion
    }
}
