/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;

namespace ns_ManageRoom
{
    public partial class ManageRoom : System.Web.UI.Page
    {
        #region Protected Data Members
        /// <summary>
        /// All web Controls to be declare
        /// </summary>

        protected System.Web.UI.HtmlControls.HtmlInputHidden MainLoc;
        protected System.Web.UI.HtmlControls.HtmlGenericControl hMgHdg;
        protected System.Web.UI.HtmlControls.HtmlGenericControl hSearchHdg;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spMgRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spSearch;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spNwRm;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spSrhRm;
        protected System.Web.UI.HtmlControls.HtmlInputHidden getLocID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden sSession;
        protected System.Web.UI.HtmlControls.HtmlInputHidden settings2locstr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden settings2locpg;
        protected System.Web.UI.WebControls.Label title;
        protected System.Web.UI.WebControls.Label lblSearchHdg;
        protected System.Web.UI.WebControls.Label totalNumber;
        protected System.Web.UI.WebControls.Label licensesRemain;
        protected System.Web.UI.WebControls.Label lblTtlRooms;
        protected System.Web.UI.WebControls.Label LblError;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnNewSubmit;
        protected System.Web.UI.WebControls.Button btnEdit;
        protected System.Web.UI.WebControls.Button btnDeleteRoom;
        protected System.Web.UI.WebControls.Button btnSearchSubmit;
        protected System.Web.UI.WebControls.TextBox txtRoomName;
        protected System.Web.UI.WebControls.TextBox txtRoomCapacity;
        protected System.Web.UI.WebControls.DropDownList Projector;
        protected System.Web.UI.HtmlControls.HtmlGenericControl ifrmLocation;
        protected System.Web.UI.WebControls.Label vidLbl;//New license
        protected System.Web.UI.WebControls.Label nvidLbl;//New license
        protected System.Web.UI.WebControls.Label ttlnvidLbl;//New license
        protected System.Web.UI.HtmlControls.HtmlControl RoomFrame;//Code added for Room Search
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.WebControls.Label tntvmrrooms;//FB 2586
        protected System.Web.UI.WebControls.Label vmrvidLbl;//FB 2586

        protected System.Web.UI.WebControls.Button btnCreateVMRRoom; //FB 2448
        protected System.Web.UI.WebControls.Button btnHotdeskingRoom; //FB 2694
        

        #endregion

        #region Private Data Members
        /// <summary>
        /// All private Data members to be declare or define.
        /// </summary>


        private ns_Logger.Logger log;
        private myVRMNet.NETFunctions obj;


        XmlDocument XmlDoc = null;
        XmlDocument XmlGtOldUser = null;
        XmlDocument XmlMgRm = null;

        private String paramFVal = "";
        private String paramHFVal = "";
        private String paramPubVal = "";
        private String paramMod = "";
        private String paramCompVal = "";
        private String paramFrm = "";
        private String paramDVal = "";
        private String paramMVal = "";
        private String rmID = "";
        private String strTmp = "";

        private Int32 roomlmt = 0;//Organization Module
        private Int32 existingRoomlmt = 0;
        private Int32 existingvidLimit = 0;
        private Int32 existingnonVidLimit = 0;
        private Int32 ttlVidLimit = 0;
        private Int32 ttlnonVidLimit = 0;
        private Int32 ttlVmrLimit = 0;//FB 2586
        private Int32 existingVmrLimit = 0;//FB 2586

        protected int Cloud = 0; //FB 2262  //FB 2599

        #endregion

        #region Page Load
        /// <summary>
        /// This Method loads intially while the page called
        /// </summary>

        protected void Page_Load(object sender, EventArgs e)
        {

            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageRoom.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if(Session["RoomLimit"] != null)
                {
                    if (Session["RoomLimit"].ToString() != "")
                        Int32.TryParse(Session["RoomLimit"].ToString(), out roomlmt);
                }

                if (Session["VideoRooms"] != null)
                {
                    if (Session["VideoRooms"].ToString() != "")
                        Int32.TryParse(Session["VideoRooms"].ToString(), out ttlVidLimit);
                }

                if (Session["NonVideoRooms"] != null)
                {
                    if (Session["NonVideoRooms"].ToString() != "")
                        Int32.TryParse(Session["NonVideoRooms"].ToString(), out ttlnonVidLimit);
                }

                if (Session["VMRRooms"] != null)//FB 2586
                {
                    if (Session["VMRRooms"].ToString() != "")
                        Int32.TryParse(Session["VMRRooms"].ToString(), out ttlVmrLimit);
                }
                //FB 2599 Start //FB 2717 Vidyo Integration Start
               /* if (Session["Cloud"] != null) //FB 2262
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                if (Cloud == 1) //FB 2262
                {
                    btnNewSubmit.Visible = false;
                    btnCreateVMRRoom.Visible = false;
                    btnHotdeskingRoom.Visible = false;//FB 2694
                }*/
				//FB 2717 Vidyo Integration End
                //FB 2599 End

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnNewSubmit.ForeColor = System.Drawing.Color.Gray; //FB 2796

                    
                    //btnCreateVMRRoom.ForeColor = System.Drawing.Color.Gray;//FB 2796

                    
                    //btnHotdeskingRoom.ForeColor = System.Drawing.Color.Gray;//FB 2694 // FB 2796
                    //btnNewSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnCreateVMRRoom.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnHotdeskingRoom.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnNewSubmit.Visible = false;
                    btnCreateVMRRoom.Visible = false;
                    btnHotdeskingRoom.Visible = false;
                }
                // FB 2796 Start
                else
                {
                    btnNewSubmit.Attributes.Add("Class", "altShortBlueButtonFormat");
                    btnCreateVMRRoom.Attributes.Add("Class", "altShortBlueButtonFormat");
                    btnHotdeskingRoom.Attributes.Add("Class", "altShortBlueButtonFormat");
                }
                // FB 2796 End

                String strSrc = "RoomSearch.aspx?rmsframe=" + selectedloc.Value + "&rmEdit=Y";

                RoomFrame.Attributes.Add("src", strSrc);



                LblError.Visible = false;
                IntializeUIResources();
                /*ClientValidation();
                BindIframe();
                BindData();
                SetQueryStringVal();*/

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " PageLoad : " + ex.Message);
            }
        }

        #endregion

        #region User Defined Methods

        #region IntializeUIResources
        /// <summary>
        /// All event handlers to be Intialize
        /// </summary>

        private void IntializeUIResources()
        {
           /* this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.btnEdit.Click += new EventHandler(btnEdit_Click);
            this.btnSearchSubmit.Click += new EventHandler(btnSearchSubmit_Click);
            this.btnDeleteRoom.Click += new EventHandler(btnDeleteRoom_Click);*/
            this.btnNewSubmit.Click += new EventHandler(btnNewSubmit_Click);
            this.btnCreateVMRRoom.Click += new EventHandler(CreateVMRRoom); //FB 2448
            this.btnHotdeskingRoom.Click += new EventHandler(CreateHotdeskingRoom); //FB 2694 
        }

       

        #endregion

        #region Method to set the Query string values

        private void SetQueryStringVal()
        {
            if (Request.QueryString["hf"] != null)
            {
                paramHFVal = Request.QueryString["hf"].ToString();
            }
            if (Request.QueryString["m"] != null)
            {
                paramMVal = Request.QueryString["m"].ToString();
            }
            if (Request.QueryString["pub"] != null)
            {
                paramPubVal = Request.QueryString["pub"].ToString();
            }
            if (Request.QueryString["d"] != null)
            {
                paramDVal = Request.QueryString["d"].ToString();
            }
            if (Request.QueryString["comp"] != null)
            {
                paramCompVal = Request.QueryString["comp"].ToString();
            }
            if (Request.QueryString["f"] != null)
            {
                paramFVal = Request.QueryString["f"].ToString();
            }
            if (Request.QueryString["mod"] != null)
            {
                paramMod = Request.QueryString["mod"].ToString();
            }
            if (Request.QueryString["frm"] != null)
            {
                paramFrm = Request.QueryString["frm"].ToString();
            }
            if (Request.QueryString["rId"] != null)
            {
                if (Request.QueryString["rId"].ToString() != "")
                {
                    rmID = Request.QueryString["rId"].ToString();
                    if (rmID != "")
                        ActiveRoom(rmID);
                    ReplaceValue("manageroom.aspx?hf=&rId='"+ rmID +"'&m=&pub=&d=&comp=&f=&frm=", "rId", String.Empty);
                    //Request.QueryString["rId"].Replace(rmID, "").ToString();
                }
            }
            if (Request.QueryString["m"] != null)
            {
                if (Request.QueryString["m"].ToString() != "")
                {
                    if (Request.QueryString["m"].ToString() == "1")
                    {
                        LblError.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        LblError.Visible = true;
                    }
                }
            }

        }
        #endregion

        #region BindIframe
        /// <summary>
        /// Build InXML, for cmd's - 'GetOldUser'&'ManageConfRoom'
        /// </summary>
        protected void BindIframe()
        {
            obj = new myVRMNet.NETFunctions();
            XmlGtOldUser = new XmlDocument();
            XmlMgRm = new XmlDocument();
            log = new ns_Logger.Logger();

            try
            {
                obj.GetManageConfRoomData(paramFVal, paramHFVal, paramFrm, paramMVal);

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " BindIframe : " + ex.Message);
            }
        }
        #endregion

        #region Client Validation
        /// <summary>
        /// To Validae the Client
        /// </summary>

        private void ClientValidation()
        {
            //Added for FB 1428 Start
            if ((Convert.ToString(Application["Client"])).ToUpper() == "MOJ")
            {
                title.Text = "Hearing";
            }
            else
            {
                //Added for FB 1428 End
                if ((Convert.ToString(Application["Client"])).ToUpper() == "PNG")
                {
                    title.Text = "Event";
                }
                else
                {
                    title.Text = obj.GetTranslatedText("Conference");//FB 1830 - Translation
                }
            }//Added for FB 1428
            hMgHdg.Visible = true;
            spMgRooms.Visible = true;
            spSrhRm.Visible = true;

            hSearchHdg.Visible = false;
            spNwRm.Visible = false;
            spSearch.Visible = false;
        }
        #endregion

        #region BindData
        /// <summary>
        /// To Bind the data's on iframe control
        /// </summary>

        private void BindData()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            try
            {

                XmlDoc = new XmlDocument();

                String strIn = Session["outXML"].ToString();

                XmlDoc.LoadXml(strIn);
                settings2locstr.Value = obj.mkLocJSstr(strIn, false, "frmManageroom");
                getLocID.Value = obj.GetOrgLocIDs(XmlMgRm.InnerXml, "1");

                if ((settings2locstr.Value.Trim() == "!") || (settings2locstr.Value.Trim() == ""))
                {
                    settings2locpg.Value = "settings2locfail.aspx?wintype=ifr";
                }
                else
                {

                    settings2locpg.Value = "settings2loc.aspx?f=frmManageroom&wintype=ifr";
                }
                
                
                //totalNumber.Text = XmlDoc.DocumentElement.SelectSingleNode("totalNumber").InnerText.Trim();
                //Int32.TryParse(totalNumber.Text,out existingRoomlmt);
                //licensesRemain.Text = (roomlmt - existingRoomlmt).ToString();  //XmlDoc.DocumentElement.SelectSingleNode("licensesRemain").InnerText.Trim();

                totalNumber.Text = ttlVidLimit.ToString();
                ttlnvidLbl.Text = ttlnonVidLimit.ToString();
                tntvmrrooms.Text = ttlVmrLimit.ToString();//FB 2586

                Int32.TryParse(XmlDoc.DocumentElement.SelectSingleNode("totalVideo").InnerText.Trim(), out existingvidLimit);
                Int32.TryParse(XmlDoc.DocumentElement.SelectSingleNode("totalNonVideo").InnerText.Trim(), out existingnonVidLimit);
                Int32.TryParse(XmlDoc.DocumentElement.SelectSingleNode("totalVMR").InnerText.Trim(), out existingVmrLimit);//FB 2586

                vidLbl.Text = (ttlVidLimit - existingvidLimit).ToString();
                nvidLbl.Text = (ttlnonVidLimit - existingnonVidLimit).ToString();
                vmrvidLbl.Text = (ttlVmrLimit - existingVmrLimit).ToString();//FB 2586

                if (totalNumber.Text == "")
                {
                    totalNumber.Text = "N/A";
                }
                if (licensesRemain.Text == "")
                {
                    licensesRemain.Text = "N/A";
                }

                if (vidLbl.Text == "")
                {
                    vidLbl.Text = "N/A";
                }

                if (nvidLbl.Text == "")
                {
                    nvidLbl.Text = "N/A";
                }

                if (vmrvidLbl.Text == "")//FB 2586
                {
                    vmrvidLbl.Text = "N/A";
                }

                sSession.Value = licensesRemain.Text;

                XmlDoc = null;
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Bind Data Error : " + ex.Message);

            }
        }
        #endregion

        #region Create Room - Submit Button Event Handler
        /// <summary>
        /// To create New Room - Submit Click Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            try
            {
                Response.Redirect("ManageRoomProfile.aspx?cal=2");//code  added for calendar popup
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Submit Error : " + ex.Message);

            }

        }

        #endregion

        #region Create New Room - Submit Button Event Handler
        /// <summary>
        /// To create New Room - While No Room are available
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewSubmit_Click(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            try
            {
                Response.Redirect("ManageRoomProfile.aspx?cal=2");//code  added for calendar popup
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Submit Error : " + ex.Message);

            }
        }
        #endregion

        #region Delete Room - Submit Button Event Handler
        /// <summary>
        /// To Delete Room - Delete Click Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteRoom_Click(object sender, EventArgs e)
        {
            String sDelMnLoc = MainLoc.Value.Trim();
            String sRmId = "";
            String inDelXML = "";
            String outDelXML = "";
            String cmdDelRM = "DeleteRoom";
            XmlDocument XmlDel = null;
            XmlNodeList delnodes = null;
            XmlNode srch = null;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();

            try
            {
                XmlDoc = new XmlDocument();

                inDelXML += "<login>";
                inDelXML += obj.OrgXMLElement();//Organization Module Fixes
                inDelXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                String[] ary = sDelMnLoc.Trim().Split(',');
                foreach (String i in ary)
                {
                    if (i.Trim() != "")
                    {
                        if (sRmId == "")
                            sRmId = "<roomID>" + i + "</roomID>";
                        else
                            sRmId += "<roomID>" + i + "</roomID>";
                    }
                }
                inDelXML += "<rooms>" + sRmId + "</rooms>";
                inDelXML += "</login>";

                XmlDel = new XmlDocument();
                outDelXML = obj.CallMyVRMServer(cmdDelRM, inDelXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027 DeleteRoom
                Session.Add("outDelXML", outDelXML);
                if (outDelXML.IndexOf("<error>") >= 0)
                {

                    LblError.Text = outDelXML;
                    LblError.Visible = true;
                }
                else
                {
                    XmlDel.LoadXml(outDelXML);
                    delnodes = XmlDel.DocumentElement.SelectNodes("/locationList/search");
                    if ((Convert.ToInt32(delnodes.Count) <= 0))
                    {
                        LblError.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        LblError.Visible = true;
                        BindIframe();
                        BindData();
                        sSession.Value = licensesRemain.Text;
                    }
                    else
                    {
                        srch = delnodes.Item(0);
                        String rn = srch.SelectSingleNode("roomName").InnerText;
                        String rc = srch.SelectSingleNode("roomCapacity").InnerText;
                        String p = srch.SelectSingleNode("projector").InnerText;
                        srch = null;
                        delnodes = null;
                        Response.Redirect("admindispatcher.asp?cmd=SearchRoom&f=dr&rn=" + rn + "&rc=" + rc + "&p=" + p);
                    }
                    XmlDoc = null;
                }
            }
            catch (System.Threading.ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Delete Room Error : " + ex.Message);

            }
        }

        #endregion

        #region Edit Room - Submit Button Event Handler
        /// <summary>
        /// To Edit Room - Submit Click Button
        /// </summary>

        private void btnEdit_Click(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            try
            {
                Session["multisiloOrganizationID"] = null; //FB 2274
                String RoomID = "";
                RoomID = MainLoc.Value.ToString();
                RoomID = RoomID.Replace(",", "");
                String inEdXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + RoomID + "</roomID></login>";//Organization Module Fixes
                String outEdXML = obj.CallMyVRMServer("GetOldRoom", inEdXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                if (RoomID != "")
                    Response.Redirect("ManageRoomProfile.aspx?cal=2&rid=" + RoomID); //code  added for calendar popup

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Edit Room Error : " + ex.Message);
            }
        }

        #endregion

        #region Search Room - Submit Button Event Handler
        /// <summary>
        /// To Search Room - Submit Click Button
        /// </summary>

        private void btnSearchSubmit_Click(object sender, EventArgs e)
        {
            String rn = "";
            String rc = "";
            String p = "";
            XmlDocument XmlSrchDoc = null;
            String inSrchXML = "";
            String outSrchXML = "";
            String srchStr = "";
            XmlNode myXMLFuncRefNode = null;
            XmlNode myXMLFuncPrtNode = null;
            XmlNode myXMLFuncNewNode = null;
            XmlNode myXMLFuncCurNode = null;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();

            try
            {
                inSrchXML = "<login>";
                inSrchXML += obj.OrgXMLElement();//Organization Module Fixes
                inSrchXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inSrchXML += "</login>";

                outSrchXML = obj.CallMyVRMServer("GetRoomProfile", inSrchXML, Application["MyVRMServer_ConfigPath"].ToString());    //FB 2027

                BindIframe();
                if (Request.QueryString["rId"] != null)
                {
                    if (Request.QueryString["rId"].ToString() != "")
                    {
                        if (rmID != "")
                        {
                            rmID = "";
                            Request.QueryString.ToString().Replace("hf=&rId='" + rmID + "'&m=&pub=&d=&comp=&f=&frm=", "hf=&rId=&m=&pub=&d=&comp=&f=&frm=");
                        }
                    }
                }

                XmlSrchDoc = new XmlDocument();
                LblError.Visible = false;

                Projector.Items[0].Text = "N/A";
                lblTtlRooms.Text = obj.GetTranslatedText("Rooms Found :");//FB 1830 - Translation

                hSearchHdg.Visible = true;
                spNwRm.Visible = true;
                spSearch.Visible = true;

                hMgHdg.Visible = false;
                spMgRooms.Visible = false;
                spSrhRm.Visible = false;


                if (Request.QueryString["f"] == "dr")
                {
                    rn = Request.QueryString["rn"];
                    rc = Request.QueryString["rc"];
                    p = Request.QueryString["p"];
                }
                else
                {
                    rn = txtRoomName.Text;
                    rc = txtRoomCapacity.Text;
                    p = Projector.SelectedValue.ToString();
                }


                inSrchXML = "<login>";
                inSrchXML += obj.OrgXMLElement();//Organization Module Fixes
                inSrchXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inSrchXML += "<roomName>" + rn + "</roomName>";
                inSrchXML += "<roomCapacity>" + rc + "</roomCapacity>";
                inSrchXML += "<projector>" + p + "</projector>";
                inSrchXML += "</login>";

                outSrchXML = obj.CallMyVRMServer("SearchRoom", inSrchXML, Application["MyVRMServer_ConfigPath"].ToString());  //FB 2027

                if (outSrchXML.IndexOf("<error>") <= 0)
                {
                    XmlSrchDoc.LoadXml(outSrchXML);
                    if (Convert.ToInt32(XmlSrchDoc.DocumentElement.SelectNodes("locationList/selected").Count) > 0)
                    {
                        XmlNode nodSrch = XmlSrchDoc.CreateElement("selected");
                        myXMLFuncRefNode = XmlSrchDoc.DocumentElement.SelectSingleNode("locationList/selected");
                        myXMLFuncPrtNode = myXMLFuncRefNode.ParentNode;
                        myXMLFuncPrtNode.ReplaceChild(nodSrch, myXMLFuncRefNode);

                        outSrchXML = XmlSrchDoc.InnerText;

                    }

                    srchStr = "<search>";
                    srchStr += "<roomName>" + rn + "</roomName>";
                    srchStr += "<roomCapacity>" + rc + "</roomCapacity>";
                    srchStr += "<projector>" + p + "</projector>";
                    srchStr += "</search>";

                    XmlTextReader xmlReader = new XmlTextReader(new StringReader(srchStr));
                    XmlNode srch = XmlSrchDoc.ReadNode(xmlReader);

                    myXMLFuncNewNode = srch.CloneNode(true);
                    myXMLFuncCurNode = XmlSrchDoc.DocumentElement.SelectSingleNode("locationList/level3List");
                    myXMLFuncRefNode = myXMLFuncCurNode.NextSibling;
                    myXMLFuncPrtNode = myXMLFuncCurNode.ParentNode;
                    myXMLFuncPrtNode.InsertBefore(myXMLFuncNewNode, myXMLFuncRefNode);

                    outSrchXML = XmlSrchDoc.InnerXml;
                    settings2locstr.Value = obj.mkLocJSstr(outSrchXML, false, "frmManageroom");
                    Session["outXML"] = outSrchXML;
                    if ((settings2locstr.Value.Trim() == "!") || (settings2locstr.Value.Trim() == ""))
                    {
                        settings2locpg.Value = "settings2locfail.aspx?wintype=ifr";
                    }
                    else
                    {

                        settings2locpg.Value = "settings2loc.aspx?f=frmManageroom";
                    }


                    totalNumber.Text = XmlSrchDoc.DocumentElement.SelectSingleNode("totalNumber").InnerText.Trim();

                    licensesRemain.Text = sSession.Value;
                    if (licensesRemain.Text == "")
                    {
                        BindData();
                        XmlDoc = new XmlDocument();
                        XmlDoc.LoadXml(Session["outXML"].ToString());
                        licensesRemain.Text = XmlDoc.DocumentElement.SelectSingleNode("licensesRemain").InnerText.Trim();
                    }
                }
                txtRoomName.Text = "";
                if (Session["delXML"] != null)
                    Session.Remove("delXML");
                XmlDoc = null;

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Search Error : " + ex.Message);

            }
        }

        #endregion

        #region Active Room Validation
        /// <summary>
        /// To Search Room - Submit Click Button
        /// </summary>

        private void ActiveRoom(String rId)
        {
            String actCmd = "";
            String actCmdXML = "";
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            XmlDocument xmlAct = null;
            XmlNodeList xNLAct = null;
            XmlNode srch = null;
            String strIn = "";

            try
            {
                xmlAct = new XmlDocument();
                XmlDoc = new XmlDocument();

                actCmd = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + rId + "</roomID></login>";//Organization Module Fixes
                actCmdXML = obj.CallMyVRMServer("ActiveRoom", actCmd, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027


                if (actCmdXML.IndexOf("<error>") >= 0)
                {
                    xmlAct.LoadXml(actCmdXML);
                    xNLAct = xmlAct.DocumentElement.SelectNodes("/locationList/search");
                    if (xNLAct.Count <= 0)
                    {
                        xNLAct = null;
                        LblError.Visible = true;
                        LblError.Text = actCmdXML;
                    }
                    else
                    {
                        srch = xNLAct.Item(0);
                        String rn = srch.SelectSingleNode("roomName").InnerText;
                        String rc = srch.SelectSingleNode("roomCapacity").InnerText;
                        String p = srch.SelectSingleNode("projector").InnerText;
                        srch = null;
                    }
                    BindIframe();
                    BindData();
                    sSession.Value = licensesRemain.Text;
                }
                else
                {
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    LblError.Visible = true;
                    BindIframe();
                    BindData();
                    sSession.Value = licensesRemain.Text;
                }
            }
            catch (System.Threading.ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Active Room : " + ex.Message);

            }
        }

        #endregion

        #region ReplaceValue
        /// <summary>
        /// ReplaceValue
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameter"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        private string ReplaceValue(string url, string parameter, string newValue)
        {
            return Regex.Replace(url, string.Format("({0}=.*$)|({0}=.*(?=&))", parameter), string.Format("{0}={1}", parameter, newValue), RegexOptions.IgnoreCase);
        }

        #endregion

        //FB 2448
        #region Create New VMR Room
        /// <summary>
        /// To create New VMR Room
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateVMRRoom(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            try
            {
                Response.Redirect("ManageVirtualMeetingRoom.aspx");
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(" Submit Error : " + ex.StackTrace);

            }
        }
        #endregion

        //FB 2694 Starts

        #region CreateHotdeskingRoom
        /// <summary>
        /// CreateHotdeskingRoom
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateHotdeskingRoom(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            try
            {
                Response.Redirect("ManageRoomProfile.aspx?cal=2&pageid=Hotdesking");
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Submit Error: " + ex.Message);

            }
        }
        #endregion

        //FB 2694 Ends

        #endregion


    }
}
