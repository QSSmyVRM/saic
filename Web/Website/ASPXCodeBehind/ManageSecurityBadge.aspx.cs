/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;
using System.Text;
using System.Drawing.Drawing2D;
using System.Collections.Specialized;//ZD 100263
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

// This class contains definition for all room profile attributes
// The methods in this class contain all operations performed on Room Profile

public partial class en_ManageSecurityBadge : System.Web.UI.Page
{
        #region Private Members 
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        string selOption = null;
        #endregion

        #region protected Members 
       
        protected System.Web.UI.WebControls.FileUpload uploadImage1;
        protected System.Web.UI.WebControls.Image imgCont;
        protected System.Web.UI.WebControls.ImageButton btnRemoveImg1;
        protected System.Web.UI.WebControls.DropDownList drpSecImgList;
        protected System.Web.UI.WebControls.Button btnMngSecImg;
        protected System.Web.UI.WebControls.Button btnSave;
        protected System.Web.UI.WebControls.Button btnRemove;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label selImageName;
        protected System.Web.UI.WebControls.Label hdnImageSize;
        protected System.Web.UI.WebControls.Button saveLink;//FB 2670    

        protected System.Web.UI.HtmlControls.HtmlInputText selectedFile1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl pnlImgArray;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSecImg1Axis;
        protected System.Web.UI.HtmlControls.HtmlInputHidden clickedImage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelFile;

        #endregion

        #region Constructor 
        public en_ManageSecurityBadge()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }
        #endregion

        #region Page_Load 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageSecurityBadge.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if (Request.QueryString["drpSelOption"].ToString() != "")
                    selOption = Request.QueryString["drpSelOption"].ToString();

                errLabel.Visible = false;

                if (!IsPostBack)
                {
                    BindSecurityImages();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "setPostBack", "<script>postback=0;</script>", false);
                }

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //saveLink.ForeColor = System.Drawing.Color.Gray;
                    //saveLink.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    saveLink.Visible = false;
                }
                else
                    saveLink.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796

            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.Message);
            }
        }
        #endregion

        #region BindSecurityImages
        protected void BindSecurityImages()
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<GetAllSecImages>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</GetAllSecImages>");

                log.Trace("GetAllSecImages InXML: " + inXML);
                String outXML = obj.CallMyVRMServer("GetAllSecImages", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetAllSecImages OutXML: " + outXML);

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlTextReader xtr;
                DataSet ds = new DataSet();
                XmlNodeList nodes = xmldoc.SelectNodes("//GetAllSecImages/badge");
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                if (ds.Tables.Count == 0)
                {
                    btnSave.Visible = false;
                    btnRemove.Visible = false;
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    System.Web.UI.HtmlControls.HtmlImage[] imgfld = new System.Web.UI.HtmlControls.HtmlImage[dt.Rows.Count];
                    System.Web.UI.WebControls.Label[] lblfld = new System.Web.UI.WebControls.Label[dt.Rows.Count];

                    System.Web.UI.WebControls.Label dummy = new System.Web.UI.WebControls.Label();
                    dummy.Text = "<br /><br />";
                    pnlImgArray.Attributes.Add("position", "absolute");
                    pnlImgArray.Attributes.Add("z-index", "100");

                    string newPath = Server.MapPath("../image/SecurityBadge/" + Session["userID"].ToString());
                    if (!Directory.Exists(newPath))
                        System.IO.Directory.CreateDirectory(newPath);

                    HtmlTable htmTbl = new HtmlTable();

                    foreach (DataRow dr in dt.Rows)
                    {
                        imgfld[dt.Rows.IndexOf(dr)] = new System.Web.UI.HtmlControls.HtmlImage();
                        lblfld[dt.Rows.IndexOf(dr)] = new System.Web.UI.WebControls.Label();

                        MemoryStream ms = new MemoryStream(Convert.FromBase64String((dr["badgeimage"]).ToString()));   
                        System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                        returnImage.Save(Server.MapPath("../image/SecurityBadge/" + Session["userID"].ToString() + "/" + dr["badgename"].ToString() + ".jpg"));

                        string divAxis = dr["textaxis"].ToString() + "," + dr["barcodeaxis"].ToString() + "," + dr["photoaxis"].ToString();
                        if (dt.Rows.IndexOf(dr) == 0)
                        {
                            imgCont.ImageUrl = "../image/SecurityBadge/" + Session["userID"].ToString() + "/" + dr["badgename"].ToString() + ".jpg";
                            imgCont.Visible = true;
                            imgCont.AlternateText = divAxis;
                            imgCont.DescriptionUrl = dr["badgeid"].ToString();
                            imgCont.Attributes.Add("longdesc", dr["badgeid"].ToString());
                        }

                        if (selOption == dr["badgeid"].ToString())
                        {
                            imgCont.ImageUrl = "../image/SecurityBadge/" + Session["userID"].ToString() + "/" + dr["badgename"].ToString() + ".jpg";
                            imgCont.Visible = true;
                            imgCont.AlternateText = divAxis;
                            imgCont.DescriptionUrl = dr["badgeid"].ToString();
                            imgCont.Attributes.Add("longdesc", dr["badgeid"].ToString());
                        }

                        imgfld[dt.Rows.IndexOf(dr)].Src = "../image/SecurityBadge/" + Session["userID"].ToString() + "/" + dr["badgename"].ToString() + ".jpg";
                        imgfld[dt.Rows.IndexOf(dr)].Attributes.Add("alt", divAxis);
                        imgfld[dt.Rows.IndexOf(dr)].Attributes.Add("longdesc", dr["badgeid"].ToString());
                        imgfld[dt.Rows.IndexOf(dr)].Attributes.Add("onclick", "javascript:return updateImage(this.src, this.alt, this.title, '" + dr["badgeid"].ToString() + "')");
                        imgfld[dt.Rows.IndexOf(dr)].Width = 200;
                        lblfld[dt.Rows.IndexOf(dr)].Text = dr["badgename"].ToString();

                        HtmlTableRow tblRow1 = new HtmlTableRow();
                        htmTbl.Controls.Add(tblRow1);

                        HtmlTableCell tblCell1 = new HtmlTableCell();
                        tblCell1.Controls.Add(imgfld[dt.Rows.IndexOf(dr)]);
                        tblRow1.Controls.Add(tblCell1);

                        HtmlTableRow tblRow2 = new HtmlTableRow();
                        htmTbl.Controls.Add(tblRow2);

                        HtmlTableCell tblCell2 = new HtmlTableCell();
                        tblCell2.Controls.Add(lblfld[dt.Rows.IndexOf(dr)]);
                        tblRow2.Controls.Add(tblCell2);

                        HtmlTableRow tblRow3 = new HtmlTableRow();
                        htmTbl.Controls.Add(tblRow3);

                        HtmlTableCell tblCell3 = new HtmlTableCell();
                        tblCell3.InnerHtml = "<br />";
                        tblRow3.Controls.Add(tblCell3);

                    }

                    pnlImgArray.Controls.Add(htmTbl);
                }
            }
            catch (Exception e)
            {
                log.Trace("BindSecurityImages"+ e.Message);
            }
        }
        #endregion

        #region UploadSecurtiyImage 
        protected void UploadSecurtiyImage(object sender, EventArgs e)
        {
            //ZD 100263
            string filextn = "";
            List<string> strExtension = null;
            bool filecontains = false;

            if (uploadImage1.HasFile)
            {
                try
                {
                    StringBuilder inXML = new StringBuilder();
                    string filename = null;
                    System.Drawing.Image img = null;
                    string file1;
                    btnSave.Visible = true;
                    btnRemove.Visible = true;
                    file1 = selectedFile1.Value;
                    if (uploadImage1.HasFile)
                    {
                        filename = Path.GetFileName(uploadImage1.FileName);

                        //ZD 100263 Starts
                        filextn = Path.GetExtension(filename);
                        strExtension = new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif", ".tif", ".tiff" };
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("Please select a valid Image Format");
                            errLabel.Visible = true;
                            return;
                        }
                        //ZD 100263 End

                        img = System.Drawing.Image.FromStream(uploadImage1.PostedFile.InputStream);
                    }

                    if ((img.Width < 137 || img.Height < 90) || (img.Width > 1366 || img.Height > 768))
                    {
                        errLabel.Text = obj.GetTranslatedText("Image dimension too large");
                        errLabel.Visible = true;
                        img.Dispose();

                        BindSecurityImages();

                        return;
                    }

                    double aspectRatio;
                    int aspectWidth, aspectHight;
                    if (img.Width > 273 || img.Height > 180)
                    {

                        if (((double)img.Width / img.Height) > (273.0 / 180.0))
                            aspectRatio = (double)img.Width / 273.0;
                        else
                            aspectRatio = (double)img.Height / 180.0;

                        aspectWidth = (int)Math.Round(img.Width / aspectRatio);
                        aspectHight = (int)Math.Round(img.Height / aspectRatio);

                    }
                    else if (img.Width < 273 || img.Height < 180)
                    {
                        if (((double)img.Width / img.Height) > (273.0 / 180.0))
                            aspectRatio = 273.0 / img.Width;
                        else
                            aspectRatio = 180.0 / img.Height;

                        aspectWidth = (int)Math.Round(img.Width * aspectRatio);
                        aspectHight = (int)Math.Round(img.Height * aspectRatio);
                    }
                    else
                    {
                        aspectWidth = img.Width;
                        aspectHight = img.Height;
                    }

                    Bitmap bmp2 = new Bitmap(273, 180, PixelFormat.Format24bppRgb);
                    bmp2.SetResolution(72, 72);

                    Graphics gfx = Graphics.FromImage(bmp2);
                    gfx.SmoothingMode = SmoothingMode.AntiAlias;
                    gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    gfx.Clear(Color.LightGray);
                    gfx.DrawImage(img, new Rectangle((int)Math.Round((double)(273 - aspectWidth) / 2), (int)Math.Round((double)(180 - aspectHight) / 2), aspectWidth, aspectHight), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                    gfx.Dispose();

                    string userId = Session["userID"].ToString();
                    string activeDir = Server.MapPath("../image/SecurityBadge/");
                    string newPath = System.IO.Path.Combine(activeDir, userId);

                    if (!Directory.Exists(newPath))
                        System.IO.Directory.CreateDirectory(newPath);

                    MemoryStream ms = new MemoryStream();
                    bmp2.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] myData = new byte[ms.Length];
                    myData = ms.ToArray();
                    string[] axis = hdnSecImg1Axis.Value.ToString().Split(',');

                    inXML.Append("<SetSecurityImage>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<userid>" + Session["userID"].ToString() + "</userid>");
                    inXML.Append("<badgeid>new</badgeid>");
                    inXML.Append("<badgeimage>" + Convert.ToBase64String(myData).ToString() + "</badgeimage>");
                    inXML.Append("<textaxis>" + axis[0].ToString() + "</textaxis>");
                    inXML.Append("<barcodeaxis>" + axis[1].ToString() + "</barcodeaxis>");
                    inXML.Append("<photoaxis>" + axis[2].ToString() + "</photoaxis>");
                    inXML.Append("</SetSecurityImage>");

                    log.Trace("SetSecurityImage InXML: " + inXML);
                    String outXML = obj.CallMyVRMServer("SetSecurityImage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("SetSecurityImage OutXML: " + outXML);
                    String selName = "";
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        selOption = xmldoc.SelectSingleNode("//SetSecurityImage/Id").InnerText;
                        selName = xmldoc.SelectSingleNode("//SetSecurityImage/ImageName").InnerText;
                    }

                    BindSecurityImages();

                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "assignImage", "<script>showDiv('" + bmp2.Width + "','" + bmp2.Height + "');</script>", false);
                    bmp2.Dispose();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "addorremove1", "<script>fnAddOrRemoveItem('1','" + selOption + "','" + selName + "');</script>", false);
                }
                catch (Exception ex)
                {
                    errLabel.Visible = true;
                    log.Trace("UploadSecurtiyImage: " + ex.Message);
                }
            }
        }

        #endregion

        #region UpdateImageAxis
        /// <summary>
        /// UpdateImageAxis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateImageAxis(object sender, EventArgs e)
        {
            try
            {
                string[] axis = hdnSecImg1Axis.Value.ToString().Split(','); 
                StringBuilder inXML = new StringBuilder();

                inXML.Append("<SetSecurityImage>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userid>" + Session["userID"].ToString() + "</userid>");
                inXML.Append("<badgeid>" + clickedImage.Value.ToString() + "</badgeid>");
                inXML.Append("<textaxis>" + axis[0].ToString() + "</textaxis>");
                inXML.Append("<barcodeaxis>" + axis[1].ToString() + "</barcodeaxis>");
                inXML.Append("<photoaxis>" + axis[2].ToString() + "</photoaxis>");
                inXML.Append("</SetSecurityImage>");

                log.Trace("SetSecurityImage InXML: " + inXML);
                String outXML = obj.CallMyVRMServer("SetSecurityImage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetSecurityImage OutXML: " + outXML);

                selOption = clickedImage.Value.ToString();
                errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                errLabel.Visible = true;

                BindSecurityImages();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message);
            }
        }

        #endregion
    
        #region RemoveSecImage
        /// <summary>
        /// RemoveSecImage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveSecImage(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<DeleteSecurityImage>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<badgeid>" + clickedImage.Value.ToString() + "</badgeid>");
                inXML.Append("</DeleteSecurityImage>");

                String outXML = obj.CallMyVRMServer("DeleteSecurityImage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("DeleteSecurityImage OutXML: " + outXML);

                selOption = "remove";
                BindSecurityImages();
                errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                errLabel.Visible = true;
                hdnSelFile.Value = clickedImage.Value;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "addorremove2", "<script>fnAddOrRemoveItem('0','" + hdnSelFile.Value.ToString() + "',null);</script>", false);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message);
            }
        }
        #endregion

}