/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_MyVRM
{
    public partial class Template : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Table tblNoTemplates;
        protected System.Web.UI.WebControls.DataGrid dgTemplates;
        protected System.Web.UI.WebControls.TextBox Bridges;
        protected System.Web.UI.WebControls.TextBox txtSortBy;
        protected System.Web.UI.WebControls.TextBox txtSTemplateName;
        protected System.Web.UI.WebControls.TextBox txtSParticipant;
        protected System.Web.UI.WebControls.TextBox txtSDescription;
        protected System.Web.UI.WebControls.Button btnManageOrder;
        protected System.Web.UI.WebControls.Button btnCreate;//FB 1522
        protected ns_Logger.Logger log = null;//FB 2027
        bool isViewUser = false;//FB 1522
        //Organization/CSS Module -- Start
       
        CustomizationUtil.CSSReplacementUtility cssUtil;
        //Organization/CSS Module -- End

        public Template()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();//FB 2027
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageTemplate.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                isViewUser = obj.ViewUserRole(); //FB 1522
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                if (!IsPostBack)
                    BindDataWithInXML();
                else
                    if (Request.Params.Get("__EVENTTARGET") != null)
                    if (Request.Params.Get("__EVENTTARGET").ToString().ToUpper().Equals("MANAGEORDER"))
                        ManageOrder();

                btnCreate.Enabled = !isViewUser;//FB 1522
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Page_Load" + ex.Message);//ZD 100263
            }

        }
        protected void BindDataWithInXML()
        {
            //if (txtSortBy.Text.Equals(""))
            //    txtSortBy.Text = "1";
            Session["multisiloOrganizationID"] = null; //FB 2274
            //FB 2027
            StringBuilder inXML = new StringBuilder(); //<login><userID>11</userID><sortBy>1</sortBy></login>
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
            inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append("<sortBy>" + txtSortBy.Text + "</sortBy>");
            inXML.Append("</login>");
            log.Trace(inXML.ToString());
            BindData(inXML.ToString(), "GetTemplateList");
        }
        private void BindData(String inXML, String cmd)
        {
            try
            {
                //Response.Write("<br>" + obj.Transfer(inXML));
                String outXML = "";
                //FB 2027
                outXML = obj.CallMyVRMServer(cmd, inXML.ToString(), Application["MyVRMserver_ConfigPath"].ToString());
                //outXML = obj.CallCOM(cmd, inXML, Application["COM_ConfigPath"].ToString());

                //String outXML = "<bridgeInfo><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><bridges><bridge><ID>1</ID><name>Test Bridge</name><interfaceType>3</interfaceType><administrator>VRM Administrator</administrator><exist>1</exist><status>1</status><order></order></bridge></bridges><totalNumber>1</totalNumber><licensesRemain>9</licensesRemain></bridgeInfo>";
                //Response.Write("<br>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//templates/template");
                    if (nodes.Count > 0)
                    {
                        LoadTemplateGrid(nodes);
                        foreach (DataGridItem dgi in dgTemplates.Items)
                            ((Button)dgi.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:showTemplateDetails('" + dgi.Cells[0].Text + "');return false;");
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgTemplates.Controls[0].Controls[dgTemplates.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text = nodes.Count.ToString();
                        dgTemplates.Visible = true;
                        tblNoTemplates.Visible = false;
                        Bridges.Text = "";
                        foreach (XmlNode node in nodes)
                            Bridges.Text += node.SelectSingleNode("ID").InnerText + "``" + node.SelectSingleNode("name").InnerText + "||";
                    }
                    else
                    {
                        dgTemplates.Visible = false;
                        tblNoTemplates.Visible = true;
                        btnManageOrder.Visible = false;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindData" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }

        protected void LoadTemplateGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();
                //Response.Write(ds.Tables[1].Columns[3].ColumnName);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    dt1 = ds.Tables[1];
                }
                if (!dt.Columns.Contains("administrator")) dt.Columns.Add("administrator");
                //foreach (DataColumn dc in dt.Columns)
                //    Response.Write(dc.ColumnName + " ");
                //Response.Write("<br>");
                //foreach (DataColumn dc in dt1.Columns)
                //    Response.Write(dc.ColumnName + " ");

                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataRow dr1 in dt1.Rows)
                        if (dr["template_Id"].ToString().Equals(dr1["template_Id"].ToString()))
                            dr["administrator"] = dr1["firstName"] + " " + dr1["lastName"];
                    //Response.Write(dr.column
                    if (dr["public"].ToString().Trim().Equals("1"))
                        dr["public"] = obj.GetTranslatedText("Public");
                    else
                        dr["public"] = obj.GetTranslatedText("Private");
                }

                dgTemplates.DataSource = dt;
                dgTemplates.DataBind();

                //Organization/CSS Module - Create folder for UI Settings --- Strat
                String fieldText = "";
                cssUtil = new CustomizationUtil.CSSReplacementUtility();
                //FB 1830 - Translation - Starts
                if (Session["isMultiLingual"] != null)
                {
                    if (Session["isMultiLingual"].ToString() != "1")
                    {
                        fieldText = cssUtil.GetUITextForControl("ManageTemplate.aspx", "btnCreateConf");
                        foreach (DataGridItem dgi in dgTemplates.Items)
                        {
                            LinkButton lnkCreateConf = (LinkButton)dgi.FindControl("btnCreateConf");
                            lnkCreateConf.Text = fieldText;
                            //FB 1522 - Start
                            if (isViewUser) //if (menuset != "1")
                            {
                                if (((LinkButton)dgi.FindControl("btnCreateConf")) != null)
                                {
                                    ((LinkButton)dgi.FindControl("btnCreateConf")).Visible = false;
                                }
                                if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                {
                                    ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                }
                                if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                {
                                    ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                }
                            }
                            //FB 1522 - End
                        }
                        //Organization/CSS Module - Create folder for UI Settings --- End
                    }
                }
				//FB 1830 - Translation - End

                BoundColumn bcTemp;
                switch (txtSortBy.Text)
                {
                    case "1":
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[1];
                        //Code Changed for Window Dressing
                        //bcTemp.HeaderStyle.CssClass = "selectedColumn";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[3];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[4];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        break;
                    case "2":
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[1];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[4];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[3];
                        //Code Changed for Window Dressing
                        // bcTemp.HeaderStyle.CssClass = "selectedColumn";
                        break;
                    case "3":
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[1];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[3];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgTemplates.Columns[4];
                        //Code Changed for Window Dressing
                        // bcTemp.HeaderStyle.CssClass = "selectedColumn";
                        break;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("LoadTemplateGrid" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion
        protected void DeleteTemplate(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //FB 2027 start
                StringBuilder inXML = new StringBuilder();
                // String inXML = "";  <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login> 
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<templates>");
                inXML.Append("<template>");
                inXML.Append("<templateID>" + e.Item.Cells[0].Text + "</templateID>");
                inXML.Append("</template>");
                inXML.Append("</templates>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("DeleteTemplate", inXML.ToString(), Application["MyVRMserver_ConfigPath"].ToString());
                //FB 2027 End
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    //FB 1719 start
                    if (Session["defaultConfTemp"].ToString() == e.Item.Cells[0].Text)
                        Session["defaultConfTemp"] = 0;
                    //FB 1719 end
                    Response.Redirect("ManageTemplate.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("DeleteTemplate" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void EditTemplate(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //Response.Redirect("dispatcher/userdispatcher.asp?cmd=GetOldTemplate&tid=" + e.Item.Cells[0].Text); //Login Management
                Response.Redirect("managetemplate2.aspx?f=m&cmd=O&templateID=" + Request.Form["templateID"] + "&tid=" + e.Item.Cells[0].Text); //ZD 100263_T
            }
            catch (System.Threading.ThreadAbortException) { }//Login Management
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("EditTemplate" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CreateConference(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Remove("confid");
                Session.Add("confid",e.Item.Cells[0].Text);
                //FB 1765 start
                Session.Remove("confTempID");
                Session.Add("confTempID", e.Item.Cells[0].Text);
                //FB 1765 end
                Response.Redirect("ConferenceSetup.aspx?t=t");
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("CreateConference" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void SearchTemplate(Object sender, EventArgs e)
        {
            //FB 2027 - Starts
            StringBuilder inXML = new StringBuilder();
            inXML.Append("<login>");

            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            inXML.Append(obj.OrgXMLElement());

            inXML.Append("<userID> "+Session["userID"].ToString()+ "</userID>"); //FB 1720
            inXML.Append("<templateName>" + txtSTemplateName.Text +"</templateName>");
            inXML.Append("<includedParticipant>" + txtSParticipant.Text + "</includedParticipant>");
            inXML.Append("<descriptionIncludes>" + txtSDescription.Text + "</descriptionIncludes>");
            inXML.Append("</login>");
            log.Trace(inXML.ToString());
            BindData(inXML.ToString(), "SearchTemplate");
            //FB 2027 - End

            txtSTemplateName.Text = "";
            txtSParticipant.Text = "";
            txtSDescription.Text = "";
        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Template ?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
            }
        }
        protected void CreateNewTemplate(Object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("dispatcher/userdispatcher.asp?cmd=GetNewTemplate&tid=new"); //Login Management
                Response.Redirect("managetemplate2.aspx?f=n&cmd=N"); //ZD 100263_T
            }
            catch (System.Threading.ThreadAbortException) { }//Login Management
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("CreateNewTemplate" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void ManageOrder()
        {
            try
            {
                String BridgeOrder = Request.Params["Bridges"].ToString();
                String inXML = "<SetTemplateOrder>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <TemplateOrder>";
                for (int i = 0; i < BridgeOrder.Split(';').Length - 1; i++)
                {
                    inXML += "      <Template>";
                    inXML += "          <Order>" + (i + 1).ToString() + "</Order>";
                    inXML += "          <TemplateID>" + BridgeOrder.Split(';')[i].ToString() + "</TemplateID>";
                    inXML += "      </Template>";
                }
                inXML += "  </TemplateOrder>";
                inXML += "</SetTemplateOrder>";
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                String outXML = obj.CallMyVRMServer("SetTemplateOrder", inXML, Application["MyVRMserver_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("ManageTemplate.aspx?m=1");
            }
            catch (Exception ex)
            {
                log.Trace("ManageOrder" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void SortTemplates(Object sender, DataGridSortCommandEventArgs e)
        {
            txtSortBy.Text = e.SortExpression;
            BindDataWithInXML();
        }
    }
}
