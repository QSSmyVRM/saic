/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;


namespace ns_ManageTemplate2
{
    public partial class ManageTemplate2 : System.Web.UI.Page
    {
        # region Private Members
        private String temp = "";
        private String templateName = "";
        public String strTemplatePublic = "";  
        private String templateFirstName = "";
        private String templateLastName = "";
        public String publicConf = "0";
        public String strDynamicInvite = "0";
        //public String getOrgLocIDs = ""; //Commented for FB 1398
        public Int32 dhour = 0;
        public Int32 dmin = 0;
		//FB 2450
        myVRMNet.NETFunctions obj;
        //private msxml4_Net.IXMLDOMNodeList nodes = null;
        //private msxml4_Net.IXMLDOMNodeList subnotes = null;
        //msxml4_Net.IXMLDOMNode node = null;
        //msxml4_Net.IXMLDOMNode subnode = null;
        //msxml4_Net.IXMLDOMNode sellocnode = null;
        //msxml4_Net.IXMLDOMNode newChildNode = null;
        //public msxml4_Net.DOMDocument40Class XmlDoc = null;

        private XmlNodeList nodes = null;
        private XmlNodeList subnotes = null;
        XmlNode node = null;
        public XmlDocument XmlDoc = null;

        String pProtocol = "";
        String pConnectionType = "";
        String pAddress = "";
        String pAddressType = "";
        String pIsOutside = "";
        String partys = "";
        String usersstr = "";
        String partyID = "";
        String partyFirstName = "";
        String partyLastName = "";
        String partyEmail = "";
        String partyInvite = "";
        String partyNotify = "";
        String partyAudVid = "";
        String[] pNames = null;
        String[] pEmails = null;
        String[] pStatuss = null;
        protected String settings2locstr = "";
        Int32 length = 0;
        String[] agIds = null;
        String[] cgIds = null;
        String[] gIds = null;
        String[] gNames = null;
        //String usersstr = "";
        Int32 sublength = 0;
        String memberID = "";
        String memberFirstName = "";
        String memberLastName = "";
        String memberEmail = "";
        protected String settings2locpg = "";
        String topcomm = "";
        String confName = "";
        protected String confPassword = "";
        String durationMin = "";
        String description = "";          
        public String gId = "";
        String outXml = "";
        String cmd = "";
        String f = "";
        String xmlstr = "";
        protected String emailClient = "";
        protected String TxtConferencePassword = "";
        ns_Logger.Logger log;
        MyVRMNet.Util utilObj ;//FB 2236
        String Survey = "";//FB 2348
        string publicParty = "0"; //FB 2550
        # endregion

        # region protected Members
        protected System.Web.UI.WebControls.Label LblTitle;
        protected System.Web.UI.WebControls.Label LblOwner;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox TxtConferenceName;
 
        protected System.Web.UI.WebControls.TextBox TxtConferenceDurationhr;
        protected System.Web.UI.WebControls.TextBox TxtConferenceDurationmi;
        protected System.Web.UI.WebControls.ListBox Group;
        protected System.Web.UI.WebControls.Button Submit;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSettingsStr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden VideoLayout;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Lecturer;
        protected System.Web.UI.HtmlControls.HtmlInputHidden VideoProtocol;
        protected System.Web.UI.HtmlControls.HtmlInputHidden maxAudio;
        protected System.Web.UI.HtmlControls.HtmlInputHidden maxVideo;
        protected System.Web.UI.HtmlControls.HtmlInputHidden restrictProtocol;
        protected System.Web.UI.HtmlControls.HtmlInputHidden restrictAV;
        protected System.Web.UI.HtmlControls.HtmlInputHidden ManualVideoLayout;
        protected System.Web.UI.HtmlControls.HtmlInputHidden LineRate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden AudioAlgorithm;
        protected System.Web.UI.HtmlControls.HtmlInputHidden VideoSession;
        protected System.Web.UI.HtmlControls.HtmlInputHidden conferenceOnPort;
        protected System.Web.UI.HtmlControls.HtmlInputHidden encryption;
        protected System.Web.UI.HtmlControls.HtmlInputHidden dualStream;
        protected System.Web.UI.HtmlControls.HtmlInputHidden MainLoc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden LectureMode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden TemplateID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtUsersStr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtPartysInfo;
        protected System.Web.UI.HtmlControls.HtmlInputHidden ownerFName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden ownerLName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSubmit;
        protected System.Web.UI.HtmlControls.HtmlInputHidden PartysNum;
        protected System.Web.UI.HtmlControls.HtmlInputHidden getOrgLocIDs;//Added for FB 1398
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSettings2locpg;
        protected System.Web.UI.HtmlControls.HtmlInputText txtTemplateName;
        protected System.Web.UI.HtmlControls.HtmlInputText ConferencePassword; 
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox TemplatePublic;
        protected System.Web.UI.HtmlControls.HtmlTextArea TemplateDescription;
        protected System.Web.UI.HtmlControls.HtmlTextArea TxtDescription;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox ChkPublicConf;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox DynamicInvite;
        protected System.Web.UI.HtmlControls.HtmlAnchor aLocation;
        protected System.Web.UI.HtmlControls.HtmlContainerControl ifrmLocation;
        protected System.Web.UI.WebControls.DropDownList ddlConType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnXML;

        /* **Code added for MOJ Phase 2 QA Bug -- Start ** */
        protected System.Web.UI.HtmlControls.HtmlTableRow ParticipantTR1;
        protected System.Web.UI.HtmlControls.HtmlTableRow ParticipantTR2;
        protected System.Web.UI.HtmlControls.HtmlTableRow AVTR1;
        /* **Code added for MOJ Phase 2 QA Bug -- End ** */

        /**** code added for room search  ****/

        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;
        //Audio Add On..
        protected String enableAdvAvParams = "0";

        //FB 1719
        protected System.Web.UI.WebControls.CheckBox chkSetDefault;
        protected System.Web.UI.HtmlControls.HtmlInputHidden CreateBy; //FB 1759
        protected System.Web.UI.WebControls.RegularExpressionValidator regDescription; // ZD 100263
        # endregion

        # region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageTemplate2.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].ToString().Equals("1")) // ZD 100263
                {
                    regDescription.ValidationExpression = "^(a-z|A-Z|0-9)*[^\\^<>&+|!`\\[\\]{}\\=%~]*$";
                    regDescription.ErrorMessage = "<br> < > & + % \\ | ^ = ! `  { }  and ~ are invalid characters.";
                }

                obj = new myVRMNet.NETFunctions();
                utilObj = new MyVRMNet.Util(); //FB 2236
                /* **Code added for MOJ Phase 2 QA Bug -- Start ** */
                if (Session["enableParticipants"] != null)
                {
                    if (Session["enableParticipants"].ToString().Equals("0"))
                    {
                        ParticipantTR1.Attributes.Add("style", "display:none");
                        ParticipantTR2.Attributes.Add("style", "display:none");
                    }
                }

                if (Session["enableAV"] != null)
                {
                    if (Session["enableAV"].ToString().Equals("0"))
                        AVTR1.Attributes.Add("style", "display:none");
                }
                /* ** Code added for MOJ Phase 2 QA Bug -- End ** */
                
                log = new ns_Logger.Logger();

                if (!IsPostBack)
                {
                   
                    if (Request.QueryString["f"] != null)
                    {
                        if (Request.QueryString["f"].ToString() != "")
                            f = Request.QueryString["f"].ToString();

                    }
                    if (Session["AdvAvParams"] != null)
                        enableAdvAvParams = Session["AdvAvParams"].ToString();
                    //Audio Add On Starts..
                    if (enableAdvAvParams == "0")
                    {
                        AVTR1.Attributes.Add("Style", "display:none");
                    }
                    //else//FB 1982
                    //    AVTR1.Attributes.Add("Style", "display:block");
                    //Audio Add On Ends..


                    if (Request.QueryString["cmd"] != null)
                    {
                        if (Request.QueryString["cmd"].ToString() != "")
                        {
                            cmd = Request.QueryString["cmd"].ToString();
                            if (cmd == "O") //GetOldTemplaite
                                ConstructoutXmlForOldTemp();
                            else if (cmd == "N") //GetNewTemplaite                           
                                CreateNewTemplate();
                        }

                    }
                  
                    GetOtherFields();
                    if (ddlConType.SelectedValue != null) //FB 1759
                        CreateBy.Value = ddlConType.SelectedValue; 

                    //SetFrameLocations();Commented for room search

                    // FB 1719 Starts
                    chkSetDefault.Enabled = true;
                    if (Session["organizationID"].ToString() != myVRMNet.NETFunctions.defaultOrgID.ToString())
                    {
                        if (Session["UsrCrossAccess"].ToString() == "1")
                            chkSetDefault.Enabled = false;
                    }
                    // FB 1719 Ends
                }
                if (TemplateID.Value == "new")
                {
                    LblTitle.Text = obj.GetTranslatedText("Create");//FB 1830 - Translation
                    topcomm = "Please follow steps 1, 2, and 3  to create a new template.";
                }
                else
                {
                    LblTitle.Text = obj.GetTranslatedText("Edit");//FB 1830 - Translation
                    topcomm = "Please follow steps 1, 2, and 3  to edit an old template.";
                }
            
                if (IsPostBack)
                {
                   
                    if (hdnSubmit.Value == "M")
                        Submit_Click(null, null);
                    
                }
                //Added for FB 1428 START
                if (Application["Client"].ToString().ToUpper().Equals("MOJ"))
                {
                    ddlConType.Items[3].Value = "7";
                    ddlConType.Items[3].Text = "Room Hearing";
                }
                //Added for FB 1428 End


            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }

        }
        # endregion

        # region ConstructoutXmlForOldTemp
        private void ConstructoutXmlForOldTemp()
        {
            try
            {

                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                if ((Request.QueryString["templateID"] == null) || (Request.QueryString["templateID"].ToString() == ""))
                    inXML += "<templateID>" + Request.QueryString["tid"].ToString() + "</templateID>";
                else
                    inXML += "<templateID>" + Request.QueryString["templateID"] + "</templateID>";
                inXML += "</login>";
                obj = new myVRMNet.NETFunctions();
               // outXml = obj.CallCOM("GetOldTemplate", inXML, Application["COM_ConfigPath"].ToString()); //FB 2027
                outXml = obj.CallMyVRMServer("GetOldTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetOldTemplate: outXML -" + outXml);
                getOrgLocIDs.Value = obj.GetOrgLocIDs(outXml, "1");//Added for FB 1398
                Session["transcodingXML"] = outXml;
                if (outXml.IndexOf("<error>") > 0)
                    Response.Write(obj.ShowErrorMessage(outXml));
                //ZD 100263_T Start
                else if (outXml.IndexOf("<error>-1</error>") >= 0)
                    Response.Redirect("ShowError.aspx");
                //ZD 100263_T End
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }
        }

        # endregion     

        #region GetOtherFields
        private void GetOtherFields()
        {

            String errorMessage = "";
            String userName = "";
            
            String lineRates = "";
            String lineRateID = "";
            String lineRateName = "";
            String videoEquipments = "";
            String videoEquipmentID = "";
            String videoEquipmentName = "";
			//FB 2450
            //msxml4_Net.IXMLDOMElement lineRate = null;
            try
            {
                xmlstr = outXml;
                Session["outXML"] = outXml;                                      
                    //FB 2450
                //XmlDoc = new msxml4_Net.DOMDocument40Class();
                XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(xmlstr);
                if (XmlDoc.InnerText == "" || XmlDoc.InnerText.IndexOf("<error>") >= 0) //XmlDoc.parseError.errorCode != 0)
                {
                    if (Session["who_use"].ToString() == "VRM")
                    {
                        //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";
                        //errorMessage = errorMessage + xmlstr;
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + xmlstr);
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                    }
                    else
                    {
                        //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                        //Response.Write("<br><br><p align='left'><font size=2><b>" + xmlstr + "<b></font></p>");
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + xmlstr);
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                    }
                    XmlDoc = null;
                    Response.End();
                }

                XmlDocument docs = new XmlDocument();
                docs.LoadXml(outXml);
                XmlNode Locnode = docs.SelectSingleNode("template/confInfo/locationList/selected");
                if (Locnode != null)
                {
                    String locndes = "";
                    XmlNodeList locnodes = Locnode.SelectNodes("level1ID");
                    foreach (XmlNode nd in locnodes)
                    {
                        if (locndes == "")
                            locndes = nd.InnerText;
                        else
                            locndes += "," + nd.InnerText;
                    }

                    if (locndes != "")
                    {
                        myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                        locstrname.Value = ntfuncs.RoomDetailsString(locndes);
                        selectedloc.Value = locndes;
                        BindRoomToList();
                        ntfuncs = null;
                    }

                    locnodes = null;
                    Locnode = null;

                }

                docs = null;

				//FB 2450
                node = XmlDoc.SelectSingleNode("//template/userInfo");
                userName = node.SelectSingleNode("userFirstName").InnerText + " " + node.SelectSingleNode("userLastName").InnerText;
                emailClient = node.SelectSingleNode("emailClient").InnerText;
                //FB 1719 start
                if (node.SelectSingleNode("setDefault") != null)
                    if (node.SelectSingleNode("setDefault").InnerText == "True")
                        chkSetDefault.Checked = true;
                    else
                        chkSetDefault.Checked = false;
                //FB 1719 end
				//FB 2450
                node = XmlDoc.SelectSingleNode("//template/confInfo");

                // used in immediate conf (if user is invited + new)	
                lineRates = "";
				//FB 2450
                nodes = XmlDoc.SelectNodes ("/template/confInfo/lineRate/rate");
                length = nodes.Count;
                for (Int32 i = 0; i < length; i++)
                {
                    node = nodes[i];// nodes.nextNode();
                    lineRateID = node.SelectSingleNode("lineRateID").InnerText;
                    lineRateName = node.SelectSingleNode("lineRateName").InnerText;

                    lineRates = lineRates + lineRateID + "$" + lineRateName + "%";
                }
                // used in immediate conf (if user is invited + new)	
                videoEquipments = "";
				//FB 2450
                nodes = XmlDoc.SelectNodes("/template/confInfo/videoEquipment/equipment");
                length = nodes.Count;
                for (Int32 i = 0; i < length; i++)
                {
                    node = nodes[i];// nodes.nextNode();
                    videoEquipmentID = node.SelectSingleNode("videoEquipmentID").InnerText;
                    videoEquipmentName = node.SelectSingleNode("videoEquipmentName").InnerText;
                    videoEquipments = videoEquipments + videoEquipmentID + "$" + videoEquipmentName + "%";
                }

                LineRate.Value = XmlDoc.SelectSingleNode("/template/confInfo/lineRateID").InnerText;


                gId = XmlDoc.SelectSingleNode("/template/confInfo/groupID").InnerText;
                usersstr = "";
                nodes = XmlDoc.SelectNodes("/template/confInfo/groups/group");
                length = nodes.Count;              
                ArrayList groupArray = new ArrayList();
                ListItem item = null;
                for (Int32 i = 0; i < length; i++)
                {

                    item = new ListItem();
                    item.Text = nodes[i].SelectSingleNode("groupName").InnerText;
                    item.Value = nodes[i].SelectSingleNode("groupID").InnerText;                   
                    Group.Items.Add(item);
                    subnotes = nodes[i].SelectNodes("user");
                    sublength = subnotes.Count;
                    for (Int32 j = 0; j < sublength; j++)
                    {
                        memberID = subnotes[j].SelectSingleNode("userID").InnerText;
                        memberFirstName = subnotes[j].SelectSingleNode("userFirstName").InnerText;
                        memberLastName = subnotes[j].SelectSingleNode("userLastName").InnerText;
                        memberEmail = subnotes[j].SelectSingleNode("userEmail").InnerText;
                        if (memberLastName.Trim() == "") //FB 2023
                            memberLastName = "&nbsp;";
                        usersstr = usersstr + memberID + "!!" + memberFirstName + "!!" + memberLastName + "!!" + memberEmail + "!!1!!0!!0!!0!!0!!1!!!!!!3!!!!!!||";//FB 1888
                        //usersstr = usersstr + memberID + "," + memberFirstName + "," + memberLastName + "," + memberEmail + ",1,0,0,0,0,1,,,3,,,;";
                    }

                    //usersstr = usersstr + "|";
                    usersstr = usersstr + "``";//FB 1888

                }
                if (usersstr != "")
                    usersstr = usersstr.Substring(0, usersstr.Length - 2); //FB 1888D
                              
                txtUsersStr.Value = usersstr;

                //FB 1985 - Start
                ListItem audioItem = new ListItem("Audio Only", "6");
                ListItem avItem = new ListItem("Audio/Video", "2");
                ListItem ppItem = new ListItem("Point-to-Point", "4");
                ListItem rmItem = new ListItem("Room Conference", "7");

                if (Session["EnableAudioOnlyConfType"].ToString() != "0")//Organization Module Fixes
                {
                    if (Session["DefaultConferenceType"].ToString() == "6")//Organization Module Fixes
                        ddlConType.SelectedValue = "6";
                }
                else
                    ddlConType.Items.Remove(audioItem);

                if (Session["EnableAudioVideoConfType"].ToString() != "0")//Organization Module Fixes
                {
                    if (Session["DefaultConferenceType"].ToString() == "2")//Organization Module Fixes
                        ddlConType.SelectedValue = "2";
                }
                else
                    ddlConType.Items.Remove(avItem);

                if (Session["P2PEnable"].ToString() != "0")
                {
                    if (Session["DefaultConferenceType"].ToString() == "4")//Organization Module Fixes
                        ddlConType.SelectedValue = "4";
                }
                else
                    ddlConType.Items.Remove(ppItem);
                if (Session["EnableRoomConfType"].ToString() != "0")//Organization Module Fixes
                {
                    if (Session["DefaultConferenceType"].ToString() == "7")//Organization Module Fixes
                        ddlConType.SelectedValue = "7";
                }
                else
                    ddlConType.Items.Remove(rmItem);
                //FB 1985 - End
               
                if (Request.QueryString["f"] == "n")
                {
                    TemplateID.Value = "new";
                    templateName = "";
                    TemplatePublic.Value = "";
                    TemplateDescription.Value = "";
                    templateFirstName = Session["userFirstName"].ToString();
                    templateLastName = Session["userLastName"].ToString();
                    LblOwner.Text = templateFirstName + " " + templateLastName;
                    confName = "";
                    confPassword = "";

                    durationMin = "60";

                    description = "";
                    
                    switch ((Application["Client"].ToString().ToUpper()).ToUpper())
                    {
                        case "HKLAW":
                            publicConf = "1";
                            break;
                        default:
                            publicConf = "0";
                            break;
                    }
                    strDynamicInvite = "0";

                    VideoLayout.Value = "";
                    VideoSession.Value = "";
                    ManualVideoLayout.Value = "";
                    AudioAlgorithm.Value = "";
                    VideoProtocol.Value = "";
                    LectureMode.Value = "";
                    Lecturer.Value = "";
                    partys = "";
                    /* Commented For FB 1985 - Start
                    ListItem audioItem = new ListItem("Audio Only", "6");
                    ListItem avItem = new ListItem("Audio/Video", "2");
                    ListItem ppItem = new ListItem("Point-to-Point", "4");
                    ListItem rmItem = new ListItem("Room Conference", "7");

                    if (Session["EnableAudioOnlyConfType"].ToString() != "0")//Organization Module Fixes
                    {
                        if (Session["DefaultConferenceType"].ToString() == "6")//Organization Module Fixes
                            ddlConType.SelectedValue = "6";
                    }
                    else
                        ddlConType.Items.Remove(audioItem);

                    if (Session["EnableAudioVideoConfType"].ToString() != "0")//Organization Module Fixes
                    {
                        if (Session["DefaultConferenceType"].ToString() == "2")//Organization Module Fixes
                            ddlConType.SelectedValue = "2";
                    }
                    else
                        ddlConType.Items.Remove(avItem);

                    if (Session["P2PEnable"].ToString() != "0")
                    {
                        if (Session["DefaultConferenceType"].ToString() == "4")//Organization Module Fixes
                            ddlConType.SelectedValue = "4";
                    }
                    else
                        ddlConType.Items.Remove(ppItem);
                    if (Session["EnableRoomConfType"].ToString() != "0")//Organization Module Fixes
                    {
                        if (Session["DefaultConferenceType"].ToString() == "7")//Organization Module Fixes
                            ddlConType.SelectedValue = "7";
                    }
                    else
                        ddlConType.Items.Remove(rmItem);             
					Commented For FB 1985 - End */
                }
                else
                {
					//FB 2450
                    node = XmlDoc.SelectSingleNode("//template/templateInfo");
                    TemplateID.Value = node.SelectSingleNode("ID").InnerText;
                    txtTemplateName.Value = node.SelectSingleNode("name").InnerText;
                    strTemplatePublic = node.SelectSingleNode("public").InnerText;
                    if (node.SelectSingleNode("public").InnerText == "1")
                        TemplatePublic.Checked = true;

                    TemplateDescription.Value = node.SelectSingleNode("description").InnerText;
                    templateFirstName = node.SelectSingleNode("owner/firstName").InnerText;
                    templateLastName = node.SelectSingleNode("owner/lastName").InnerText;
                    LblOwner.Text = templateFirstName + " " + templateLastName;
                    node = XmlDoc.SelectSingleNode("//template/confInfo");
                    TxtConferenceName.Text = node.SelectSingleNode("confName").InnerText;
                    confPassword = node.SelectSingleNode("confPassword").InnerText;
                    ConferencePassword.Attributes.Add("value", node.SelectSingleNode("confPassword").InnerText);
                    ddlConType.SelectedValue = node.SelectSingleNode("confType").InnerText;
                    dhour = (Int32)Math.Floor(Convert.ToDecimal(node.SelectSingleNode("durationMin").InnerText.ToString()) / 60);
                    dmin = (Int32)Convert.ToDecimal(node.SelectSingleNode("durationMin").InnerText.ToString()) % 60;

                    TxtConferenceDurationhr.Text = dhour.ToString();
                    TxtConferenceDurationmi.Text = dmin.ToString();
                    TxtDescription.Value = utilObj.ReplaceOutXMLSpecialCharacters(node.SelectSingleNode("description").InnerText, 1); //FB 2236

                    publicConf = node.SelectSingleNode("publicConf").InnerText;
                    if (Session["dynInvite"].ToString() == "1")//Organization Module Fixes
                        strDynamicInvite = node.SelectSingleNode("dynamicInvite").InnerText;
                    else
                        strDynamicInvite = "0";

                    LectureMode.Value = node.SelectSingleNode("lectureMode").InnerText;
                    Lecturer.Value = node.SelectSingleNode("lecturer").InnerText;
                    //----Code-----
						//FB 2450
                   	node = XmlDoc.SelectSingleNode("/template/confInfo/advAVParam");
				        maxAudio.Value = node.SelectSingleNode("maxAudioPart").InnerText	;
				        maxVideo.Value = node.SelectSingleNode("maxVideoPart").InnerText;
				        restrictProtocol.Value = node.SelectSingleNode("restrictProtocol").InnerText;
				        restrictAV.Value = node.SelectSingleNode("restrictAV").InnerText;
				        ManualVideoLayout.Value = node.SelectSingleNode("videoLayout").InnerText;
                        VideoLayout.Value = node.SelectSingleNode("videoLayout").InnerText;
				        LineRate.Value = node.SelectSingleNode("maxLineRateID").InnerText;
				        AudioAlgorithm.Value = node.SelectSingleNode("audioCodec").InnerText;
				        VideoSession.Value = node.SelectSingleNode("videoCodec").InnerText;
				        dualStream.Value = node.SelectSingleNode("dualStream").InnerText;
				        conferenceOnPort.Value = node.SelectSingleNode("confOnPort").InnerText;
				        encryption.Value = node.SelectSingleNode("encryption").InnerText;
                        LectureMode.Value = node.SelectSingleNode("lectureMode").InnerText;
                    //Code Change by offshore for FB Issue 1120
                    //-----
                        if (Session["primaryBridge"] != null && Session["primaryBridge"].ToString() == "1")
                        {
							//FB 2450
                            node = XmlDoc.SelectSingleNode("/template/confInfo/advAVParam");
                            maxAudio.Value = node.SelectSingleNode("maxAudioPart").InnerText;
                            maxVideo.Value = node.SelectSingleNode("maxVideoPart").InnerText;
                            restrictProtocol.Value = node.SelectSingleNode("restrictProtocol").InnerText;
                            restrictAV.Value = node.SelectSingleNode("restrictAV").InnerText;
                            ManualVideoLayout.Value = node.SelectSingleNode("videoLayout").InnerText;
                            VideoLayout.Value = node.SelectSingleNode("videoLayout").InnerText;
                            LineRate.Value = node.SelectSingleNode("maxLineRateID").InnerText;
                            AudioAlgorithm.Value = node.SelectSingleNode("audioCodec").InnerText;
                            VideoSession.Value = node.SelectSingleNode("videoCodec").InnerText;
                            dualStream.Value = node.SelectSingleNode("dualStream").InnerText;
                            conferenceOnPort.Value = node.SelectSingleNode("confOnPort").InnerText;
                            encryption.Value = node.SelectSingleNode("encryption").InnerText;
                            LectureMode.Value = node.SelectSingleNode("lectureMode").InnerText;
                        }
                    //else
                    //{
                    //    VideoLayout.Value = "";
                    //    VideoSession.Value = "";
                    //    ManualVideoLayout.Value = "";
                    //    AudioAlgorithm.Value = "";
                    //    VideoProtocol.Value = "";
                    //}
                    partys = "";
					//FB 2450
                    nodes = XmlDoc.SelectNodes("/template/confInfo/partys/party");
                    length = nodes.Count;
                    // used in templatedetails
                    if (length > 0)
                    {                        
                        String pStatuss = "";
                        //foreach (msxml4_Net.IXMLDOMNode xmlNode in nodes)
                        foreach (XmlNode xmlNode in nodes)
                        {
                            partyID = xmlNode.SelectSingleNode("partyID").InnerText;
                            partyFirstName = xmlNode.SelectSingleNode("partyFirstName").InnerText;
                            partyLastName = xmlNode.SelectSingleNode("partyLastName").InnerText;
                            partyEmail = xmlNode.SelectSingleNode("partyEmail").InnerText;
                            partyInvite = xmlNode.SelectSingleNode("partyInvite").InnerText;
                            partyNotify = xmlNode.SelectSingleNode("partyNotify").InnerText;
                            partyAudVid = xmlNode.SelectSingleNode("partyAudVid").InnerText;
                            if (xmlNode.SelectSingleNode("partyPublicVMR") != null) //FB 2550
                                publicParty = xmlNode.SelectSingleNode("partyPublicVMR").InnerText; 
                            if (partyLastName.Trim() == "") //FB 2023
                                partyLastName = "&nbsp;";
                            Survey = xmlNode.SelectSingleNode("Survey").InnerText;//FB 2348
                            partys = partys + partyID + "!!" + partyFirstName + "!!" + partyLastName + "!!" + partyEmail + "!!";//FB 1888
                            
                            switch (partyInvite)
                            {
                                case "0":
                                    pStatuss = obj.GetTranslatedText("CC");
                                    partys = partys + "0!!0!!1!!";//FB 1888
                                    //partys = partys + "0,0,1,";
                                    break;
                                case "1":
                                    pStatuss = obj.GetTranslatedText("External Attendee");//FB 1888
                                    partys = partys + "1!!0!!0!!";
                                    //partys = partys + "1,0,0,";
                                    break;
                                case "2":
                                    pStatuss = obj.GetTranslatedText("Room Attendee");
                                    partys = partys + "0!!1!!0!!";//FB 1888
                                    //partys = partys + "0,1,0,";
                                    break;
                               
                                default:
                                    pStatuss = "";
                                    partys = partys + "0!!0!!0!!";//FB 1888
                                    //partys = partys + "0,0,0,";
                                    break;
                            }

                            switch (partyNotify)
                            {
                                case "0":
                                    partys = partys + "0!!";//FB 1888
                                    break;
                                case "1":
                                    pStatuss = pStatuss + "!! notify";//FB 1888
                                    partys = partys + "1!!";
                                    //partys = partys + "1,";
                                    break;
                                default:
                                    partys = partys + "0!!";//FB 1888
                                    break;
                            }

                            switch (partyAudVid)
                            {
                                case "2": //FB 1744 - Video
                                    partys = partys + "1!!0!!";//FB 1888
                                    break;
                                case "1": //FB 1744 - Audio
                                    partys = partys + "0!!1!!";//FB 1888
                                    break;
                                default:
                                    partys = partys + "0!!0!!";//FB 1888
                                    break;
                            }
							//FB 2450
                            if (xmlNode.SelectNodes("partyAddressType").Count > 0)
                            {
                                pProtocol = xmlNode.SelectSingleNode("partyProtocol").InnerText;
                                pConnectionType = xmlNode.SelectSingleNode("partyConnectionType").InnerText;
                                pAddress = xmlNode.SelectSingleNode("partyAddress").InnerText;
                                pAddressType = xmlNode.SelectSingleNode("partyAddressType").InnerText;
                                pIsOutside = xmlNode.SelectSingleNode("partyIsOutside").InnerText;
                                partys = partys + pProtocol + "!!" + pConnectionType + "!!" + "-3!!" + pAddress + "!!" + pAddressType + "!!" + pIsOutside + "!!";//FB 1888 //FB 2348
                                //partys = partys + pProtocol + "," + pConnectionType + "," + "-3," + pAddress + "," + pAddressType + "," + pIsOutside + ";";
                            }
                            else
                                partys = partys + "!!!!-3!!!!!!!!";//FB 1888//FB 2348
                                //partys = partys + ",,-3,,;";

                            switch (Survey)
                            {
                                case "0":
                                    partys = partys + "0!!0||";//FB 1888 //FB 2550
                                    break;
                                case "1":
                                    pStatuss = pStatuss + "!! survey";//FB 1888 //FB 2550
                                    partys = partys + "1!!0||";
                                    //partys = partys + "1,";
                                    break;
                                default:
                                    partys = partys + "0!!0||";//FB 1888 //FB 2550
                                    break;
                            }

                            
                        }
                    }
                    txtPartysInfo.Value = partys;
                    ownerFName.Value = templateFirstName;
                    ownerLName.Value = templateLastName;

                }

                if (Session["dynInvite"].ToString() != "")//Organization Module Fixes
                    ChkPublicConf.Attributes.Add("onClick", "JavaScript: clkpublic(this, '" + Session["dynInvite"].ToString() + "');");//Organization Module Fixes

           
                 
               
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }
        }

        # endregion

        # region SetFrameLocations
        private void SetFrameLocations()
        {
            try
            {
                myVRMNet.NETFunctions netFunctions = new myVRMNet.NETFunctions();
                if (outXml.IndexOf("<error>") >= 0)
                {
                    settings2locstr = hdnSettingsStr.Value;
                    Session["outXML"] = Session["transcodingXML"];
                }
                else
                    settings2locstr = netFunctions.mkLocJSstr(xmlstr, false, f);

               hdnSettingsStr.Value = settings2locstr;

                if ((settings2locstr == "!")||(settings2locstr == ""))
                    settings2locpg = "settings2locfail.aspx?f=frmSettings2&wintype=ifr";
                else              
                    settings2locpg = "settings2loc.aspx?f=frmSettings2&sp=1";

                nodes = null;
                //subnode = null;
                subnotes = null;
                XmlDoc = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }

        }
        # endregion

        # region GetCheckBoxVal
        private String GetCheckBoxVal(String checkBoxName)
        {
            try
            {
                if (checkBoxName == "TP")
                {
                    if (TemplatePublic.Checked)
                        return "1";
                    else
                        return "0";
                }
                else if (checkBoxName == "DI")
                {
                    if (DynamicInvite.Checked)
                        return "1";
                    else
                        return "0";
                }
                else
                {
                    if (ChkPublicConf.Checked)
                        return "1";
                    else
                        return "0";
                }
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        # endregion

        # region SetTemplate
        private void SetTemplate()
        {
            Int32 confLocation_loc = 0;
            Int32 confLocation_loc2 = 0;
            String location_str = "";
            String[] partyarr = null;
            String[] partysary = null;
            String partyInvite = "";
            String partyAudVid = "";
            Int32 numInvitedParty = 0;
            try
            {
                if (TxtConferenceDurationmi.Text == "")
                    TxtConferenceDurationmi.Text = "0";
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<template>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userInfo>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</userInfo>");
                inXML.Append("<templateInfo>");
                inXML.Append("<ID>" + Request.Form["TemplateID"].ToString() + "</ID>");
                inXML.Append("<name>" + txtTemplateName.Value + "</name>");
                inXML.Append("<public>" + GetCheckBoxVal("TP") + "</public>");
                inXML.Append("<owner>");
                inXML.Append("<firstName>" + Request.Form["ownerFName"].ToString() + "</firstName>");
                inXML.Append("<lastName>" + Request.Form["ownerLName"].ToString() + "</lastName>");
                inXML.Append("</owner>");
                //FB 1719 start
                if (chkSetDefault.Checked)
                {
                    Session["defaultConfTemp"] = Request.Form["TemplateID"].ToString();
                    inXML.Append("      <setDefault>true</setDefault>");
                }
                else
                {
                   inXML.Append("<setDefault></setDefault>");
                   if( Session["defaultConfTemp"].ToString() == Request.Form["TemplateID"].ToString() )
                            Session["defaultConfTemp"] = "0";
                }
                //FB 1719 end
                inXML.Append("<description>" + TemplateDescription.Value + "</description>");
                inXML.Append("</templateInfo>");
                inXML.Append("<confInfo>");
                inXML.Append("<confName>" + TxtConferenceName.Text + "</confName>");
                if (ddlConType.SelectedValue == "7") //FB 2017
                {
                    ConferencePassword.Value = "";
                }
                inXML.Append("<confPassword>" + ConferencePassword.Value+ "</confPassword>");
                inXML.Append("<confType>" + ddlConType.SelectedValue + "</confType>");                
                //inXML.Append("<durationMin>" + Convert.ToInt32(2) * 60 + Convert.ToInt32(TxtConferenceDurationmi.Text) + "</durationMin>");                
                inXML.Append("<durationMin>" +Convert.ToString(Convert.ToInt32(TxtConferenceDurationhr.Text) * 60 + Convert.ToInt32(TxtConferenceDurationmi.Text))+ "</durationMin>");
                inXML.Append("<description>" + utilObj.ReplaceInXMLSpecialCharacters(TxtDescription.Value) + "</description>"); //FB 2236
                inXML.Append("<locationList>");
                inXML.Append("<selected>");/*location_str = MainLoc.Value; Code changed for Room Search
                if (location_str != "")
                {
                    while (location_str.IndexOf(",", confLocation_loc) > 0)
                    {
                        confLocation_loc2 = location_str.IndexOf(",", confLocation_loc);
                        inXML.Append("  <level1ID>" + location_str.Substring(confLocation_loc, confLocation_loc2 - confLocation_loc)+" </level1ID>");
                        confLocation_loc = confLocation_loc2 + 2;
                    }
                }*/
                if (selectedloc.Value != "")
                {
                    String[] roomslst = selectedloc.Value.Split(',');

                    foreach (String s in roomslst)
                        inXML.Append("<level1ID>" + s + " </level1ID>");

                    roomslst = null;

                }
                /*** Code changed for Room Search ***/
                inXML.Append("</selected>");
                inXML.Append("</locationList>");
                inXML.Append("<publicConf>" + GetCheckBoxVal("P") + "</publicConf>");
                inXML.Append("<advAVParam>");
                inXML.Append("<maxAudioPart>" + Request.Form["maxAudio"].ToString() + "</maxAudioPart>");
                inXML.Append("<maxVideoPart>" + Request.Form["maxVideo"].ToString() + "</maxVideoPart>");
                inXML.Append("<restrictProtocol>" + Request.Form["restrictProtocol"].ToString() + "</restrictProtocol>");
                if (Request.Form["CreateBy"] != "6")
                    inXML.Append("<restrictAV>" + Request.Form["restrictAV"].ToString() + "</restrictAV>");
                else
                  //inXML.Append("<restrictAV>" + "2" + "</restrictAV>"); 
                    inXML.Append("<restrictAV>" + "1" + "</restrictAV>"); //FB 1744

                inXML.Append("<videoLayout>" + Request.Form["ManualVideoLayout"].ToString() + "</videoLayout>");
                inXML.Append("<maxLineRateID>" + Request.Form["LineRate"].ToString() + "</maxLineRateID>");
                inXML.Append("<audioCodec>" + Request.Form["AudioAlgorithm"].ToString() + "</audioCodec>");
                inXML.Append("<videoCodec>" + Request.Form["VideoSession"].ToString() + "</videoCodec>");
                inXML.Append("<dualStream>" + Request.Form["dualStream"].ToString() + "</dualStream>");
                inXML.Append("<confOnPort>" + Request.Form["conferenceOnPort"].ToString() + "</confOnPort>");
                inXML.Append("<encryption>" + Request.Form["encryption"].ToString() + "</encryption>");
                inXML.Append("<lectureMode>" + Request.Form["LectureMode"].ToString() + "</lectureMode>");
                inXML.Append("</advAVParam>");
                inXML.Append("<partys>");
                Char[] chAr ={ ';' };
                String[] delimitedArr = { "||" };//FB 1888
                String[] delimitedArr1 = { "!!" };//FB 1888
                partysary = Request.Form["txtPartysInfo"].ToString().Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries);//FB 1888
                //partysary = Request.Form["txtPartysInfo"].ToString().Split(';');
                //PartysNum.Value =Convert.ToString(partysary.Length-1);
                for (Int32 i = 0; i < Convert.ToInt32(PartysNum.Value); i++)
                {
                    partyarr = partysary[i].Split(delimitedArr1, StringSplitOptions.None);//FB 1888 //FB 2348
                    //partyarr = partysary[i].Split(',');
                    partyInvite = "-1";
                    partyAudVid = "-1";
                    if (partyarr[4] == "1")
                    {
                        partyInvite = "1";
                        numInvitedParty = numInvitedParty + 1;
                    }
                    if (partyarr[5] == "1")
                        partyInvite = "2";
                    if (partyarr[6] == "1")
                        partyInvite = "0";
                    partyNotify = partyarr[7];
                    if (partyarr[8] == "1") //Video
                        partyAudVid = "2";  //FB 1744
                    if (partyarr[9] == "1") //Audio
                        partyAudVid = "1";  //FB 1744
                   
                    inXML.Append("<party>");
                    inXML.Append("<partyID>" + partyarr[0] + "</partyID>");
                    inXML.Append("<partyFirstName>" + partyarr[1] + "</partyFirstName>");
                    inXML.Append("<partyLastName>" + partyarr[2] + "</partyLastName>");
                    inXML.Append("<partyEmail>" + partyarr[3] + "</partyEmail>");
                    inXML.Append("<partyInvite>" + partyInvite + "</partyInvite>");
                    inXML.Append("<partyNotify>" + partyNotify + "</partyNotify>");
                    inXML.Append("<partyAudVid>" + partyAudVid + "</partyAudVid>");
                    inXML.Append("<partyPublicVMR>0</partyPublicVMR>");//FB 2550
                    if ((partyarr[0] == "new") && (partyInvite == "1"))
                    {
                        inXML.Append("<partyProtocol>" + partyarr[10] + "</partyProtocol>");
                        inXML.Append("<partyConnectionType>" + partyarr[11] + "</partyConnectionType>");
                        inXML.Append("<partyAddress>" + partyarr[13] + "</partyAddress>");
                        inXML.Append("<partyAddressType>" + partyarr[14] + "</partyAddressType>");
                        inXML.Append("<partyIsOutside>" + partyarr[15] + "</partyIsOutside>");

                    }
                    if (partyarr[16] == null)
                        partyarr[16] = "0";
                    inXML.Append("<Survey>" + partyarr[16] + "</Survey>"); //FB 2348
                    inXML.Append("</party>");
                }
                         
                inXML.Append("</partys>");
                if (DynamicInvite.Checked)
                    inXML.Append("<dynamicInvite>" + "1" + "</dynamicInvite>");
                else
                    inXML.Append("<dynamicInvite>" + "0" + "</dynamicInvite>");
                inXML.Append("</confInfo>");
                inXML.Append("</template>");
                obj = new myVRMNet.NETFunctions();
                Session["outXML"] = null;
                outXml = obj.CallMyVRMServer("SetTemplate", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                //outXml = obj.CallCOM("SetTemplate", inXML.ToString(), Application["COM_ConfigPath"].ToString());
                Session["outXML"] = outXml;

                ///Fb 1719
                if (chkSetDefault.Checked)
                {
                    XmlDocument defaultTempdocs = new XmlDocument();
                    defaultTempdocs.LoadXml(outXml);
                    XmlNode defaulttemp = defaultTempdocs.SelectSingleNode("template/templateInfo/ID");
                    if (defaulttemp != null)
                    {
                        Session["defaultConfTemp"] = defaulttemp.InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = "System has encountered an error. Please take appropriate action or contact your myVRM Administrator for further help.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                log.Trace("SetTemplate" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        # endregion

        # region Settings Submit Click
        protected void Submit_Click(object sender, EventArgs e)
        {
            try
            {              
                    SetTemplate();
                    //SetFrameLocations();  Coomented for room search
                    if (outXml.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXml);//FB 1881
                        //errLabel.Text = "System has encountered an error. Please take appropriate action or contact your myVRM Administrator for further help �"+obj.ShowErrorMessage(outXml);
                    }
                    else
                        Response.Redirect("confirmtemplate.aspx?f=m");
                 
                
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                //errLabel.Text = "System has encountered an error. Please contact your VRM Administrator.";
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        # endregion

        # region CreateNewTemplate
        private void CreateNewTemplate()
        {
            try
            {
             
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                dhour = 1;
                dmin = 0;
                obj = new myVRMNet.NETFunctions();
                //outXml = obj.CallCOM("GetNewTemplate", inXML, Application["COM_ConfigPath"].ToString());
                outXml = obj.CallMyVRMServer("GetNewTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                Session["transcodingXML"] = outXml;
                TxtConferenceDurationhr.Text = dhour.ToString();
                TxtConferenceDurationmi.Text = dmin.ToString();               
	

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }
        }

        # endregion
 
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();            
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           //      this.Load += new System.EventHandler(this.Page_Load);
            Submit.Attributes.Add("onclick", "javascript:return frmSettings2_Validator();");
            Submit.Click+=new EventHandler(Submit_Click);
            ddlConType.Attributes.Add("onchange", "javascript:fnConfTypeChange(this);");
        }
        # endregion

        #region Bind to Rooms

        public void BindRoomToList()
        {
            String[] locsName = null;
            try
            {
                if (locstrname.Value != "")
                {

                    locsName = locstrname.Value.Split('+');
                    RoomList.Items.Clear();
                    foreach (String s in locsName)
                    {
                        if (s != "")
                        {

                            if (s.Split('|').Length > 1)
                                RoomList.Items.Add(new ListItem(s.Split('|')[1], s.Split('|')[0]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }
        #endregion
        }

    }
}


 



	
