/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_UserTemplates
{
    public partial class UserTemplates : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DataGrid dgUserTemplates;
        protected System.Web.UI.WebControls.Table tblNoUserTemplates;
        protected System.Web.UI.WebControls.Button btnCreateNew; //FB 2670

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;//ZD 100263
        public UserTemplates()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageUserTemplatesList.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                errLabel.Text = "";
                BindData();
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    
                    //btnCreateNew.ForeColor = System.Drawing.Color.Gray; //FB 2796 
                    //btnCreateNew.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnCreateNew.Visible = false;
                }
                    else
                    btnCreateNew.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796
                    
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("Page_Load" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            }

        }

        protected void BindData()
        {
            try
            {
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("GetUserTemplateList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //                Response.End();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlTextReader xtr;
                DataSet ds = new DataSet();
                XmlNodeList nodes = xmldoc.SelectNodes("//UserTemplates/Template");
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataView dv = new DataView(ds.Tables[0]);
                    DataTable dt = dv.Table;

                    dgUserTemplates.DataSource = dv;
                    dgUserTemplates.DataBind();

                    Label lblTemp = new Label();
                    DataGridItem dgFooter = (DataGridItem)dgUserTemplates.Controls[0].Controls[dgUserTemplates.Controls[0].Controls.Count - 1];
                    lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                    lblTemp.Text = nodes.Count.ToString();

                }
                else
                    tblNoUserTemplates.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindData" + ex.Message);//ZD 100263
                //Response.End();
            }

        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    //FB 2670
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                    btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this user template?") + "')){DataLoading(1); return true;} else {return false;}");

                    if (Session["admin"].ToString() == "3")
                    {
                        
                        //btnTemp.Attributes.Remove("onClick");
                        //btnTemp.Style.Add("cursor", "default");
                        //btnTemp.ForeColor = System.Drawing.Color.Gray;
                        btnEdit.Text = "View";
                        //ZD 100263
                        btnTemp.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
                log.Trace("BindRowsDeleteMessage" + ex.Message);//ZD 100263
            }
        }

        protected void DeleteUserTempate(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <userTemplateID>" + e.Item.Cells[0].Text + "</userTemplateID>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("DeleteUserTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageUserTemplatesList.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("DeleteuserTemplate" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void EditUserTemplate(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("UTID", e.Item.Cells[0].Text.ToString());
                Response.Redirect("EditUserTemplate.aspx");
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("EditUserTemplate" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CreateNewTemplate(Object sender, EventArgs e)
        {
            try
            {
                Session.Add("UTID", "new");
                Response.Redirect("EditUserTemplate.aspx");
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("CreateNewTemplate" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

    }


}