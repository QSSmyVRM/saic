﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Specialized;//ZD 100263
using System.Collections.Generic;


namespace ns_MyVRM
{
    public partial class en_ManageVirtualMeetingRoom : System.Web.UI.Page
    {
        #region protected Members
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblMUser;
        protected System.Web.UI.WebControls.Label lblMdate;

        protected System.Web.UI.WebControls.TextBox txtRoomName;
        protected System.Web.UI.WebControls.TextBox Assistant;
        protected System.Web.UI.WebControls.TextBox txtExternalnum;
        protected System.Web.UI.WebControls.TextBox txtInternalnum;
        protected System.Web.UI.WebControls.TextBox txtVMRLink;//FB 2727

        protected System.Web.UI.WebControls.ListBox DepartmentList;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMultipleDept;
        protected System.Web.UI.HtmlControls.HtmlInputHidden AssistantID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomID;
		//FB 2994 Start
        protected System.Web.UI.HtmlControls.HtmlInputFile roomfileimage; 
        protected System.Web.UI.WebControls.Button BtnUploadRmImg;
        protected System.Web.UI.WebControls.DataGrid dgItems;
        protected System.Web.UI.WebControls.Button btnUploadImages; 
		//FB 2994 End

        //FB 2670
        protected System.Web.UI.WebControls.Button btnSubmitAddNew;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnReset;//ZD 100263

        //FB 2994 Start
        string sessionKey = "";
        string rmImg = "";
        DataTable roomImageDt;
        String fName; 
        string fileext;
        String pathName;
        myVRMNet.ImageUtil imageUtilObj = null;
        byte[] imgArray = null;
        //FB 2994 End
        #endregion

        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log = null;

        protected String language = "";
        protected String dtFormatType = "MM/dd/yyyy";
        String tformat = "hh:mm tt";

        public en_ManageVirtualMeetingRoom()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            imageUtilObj = new myVRMNet.ImageUtil(); //FB 2994
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("ManageVirtualMeetingRoom.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            lblTitle.Text = "Virtual Meeting Room";//FB 2994

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() == "")
                    Session["FormatDateType"] = "MM/dd/yyyy";
                else
                    dtFormatType = Session["FormatDateType"].ToString();
            }
            //FB 2670
            if (Session["admin"].ToString() == "3")
            {
                
                //btnSubmit.ForeColor = System.Drawing.Color.Gray;
                //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796

                
                //btnSubmitAddNew.ForeColor = System.Drawing.Color.Gray;
                //btnSubmitAddNew.Attributes.Add("Class", "btndisable");// FB 2796
                //ZD 100263
                btnSubmit.Visible = false;
                btnSubmitAddNew.Visible = false;
                btnReset.Visible = false;
            }
            //FB 2796 Start
            else
            {
                btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                btnSubmitAddNew.Attributes.Add("Class", "altMedium0BlueButtonFormat");
            }
            //FB 2796 End



            if (Session["language"] == null)
                Session["language"] = "en";

            if (Session["language"].ToString() != "")
                language = Session["language"].ToString();

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

            if (!IsPostBack) //if page loads for the first time
            {
                if (Request.QueryString["rID"] != null)
                    hdnRoomID.Value = Request.QueryString["rID"].ToString();
                else
                    hdnRoomID.Value = "new";

                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.ShowSuccessMessage();
                        errLabel.Visible = true;
                    }
                
                Assistant.Attributes.Add("readonly", "");
                LoadDepartments();
                if (!hdnRoomID.Value.ToLower().Equals("new"))
                    LoadRoomProfile();
            }

            if (hdnRoomID.Value.ToLower().Equals("new"))
            {
                if (!IsPostBack) // FB 2586
                    lblTitle.Text = obj.GetTranslatedText("Create New ") + lblTitle.Text;
            }
            else
                lblTitle.Text = obj.GetTranslatedText("Edit ") + lblTitle.Text;
            //FB 2670
            if (Session["admin"].ToString() == "3")
                lblTitle.Text = "View Virtual Meeting Rooms";

        }

        #region LoadDepartments
        /// <summary>
        /// LoadDepartments
        /// </summary>
        protected void LoadDepartments()
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<GetDepartmentsForLocation>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("</GetDepartmentsForLocation>");
                log.Trace("GetDepartmentsForLocation Inxml: " + inXML.ToString());

                String outXML = obj.CallMyVRMServer("GetDepartmentsForLocation", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetDepartmentsForLocation outxml: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    hdnMultipleDept.Value = xmldoc.SelectSingleNode("//GetDepartmentsForLocation/multiDepartment").InnerText;
                    if (hdnMultipleDept.Value == "1")
                    {
                        XmlNodeList nodes = xmldoc.SelectNodes("//GetDepartmentsForLocation/departments/department");
                        if (nodes.Count > 0)
                            obj.LoadList(DepartmentList, nodes, "id", "name");
                    }
                }
                else
                {
                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);

                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadEndpoints: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region LoadRoomProfile
        /// <summary>
        /// LoadRoomProfile
        /// </summary>
        protected void LoadRoomProfile()
        {
            DateTime deactDate = DateTime.MinValue;
            try
            {
                String inXML = "<GetRoomProfile>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <RoomID>" + hdnRoomID.Value + "</RoomID>";
                inXML += "</GetRoomProfile>";
                log.Trace("GetRoomProfile inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("GetRoomProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetRoomProfile outxml: " + outXML);

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                txtRoomName.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomName").InnerText;
                hdnRoomID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/RoomID").InnerText;

                if (xmldoc.SelectSingleNode("//GetRoomProfile/UserStaus") != null)
                {
                    errLabel.Text = "The Assistant-In-Charge \"" + xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText + "\" is not found in Active User List.";//FB 2974
                    errLabel.Visible = true;
                }
                else
                {
                    AssistantID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeID").InnerText;
                    Assistant.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText;
                }


                //FB 2994 starts here...
                rmImg = "";
                XmlNodeList rmImgNodes = xmldoc.SelectNodes("//GetRoomProfile/RoomImages/ImageDetails");

                if (rmImgNodes != null)
                    LoadRoomImageGrid(rmImgNodes);
                //FB 2994 End here...


                XmlNodeList nodes = xmldoc.SelectNodes("//GetRoomProfile/Departments/Department");
                Int32 c = nodes.Count;
                for (Int32 d = 0; d < nodes.Count; d++)
                {
                    foreach (ListItem item in DepartmentList.Items)
                    {
                        if (c == 0)
                            break;
                        if (item.Value == nodes[d].SelectSingleNode("ID").InnerText)
                        {
                            item.Selected = true;
                            c--;
                            break;
                        }
                    }
                }

                if (xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber") != null)
                    txtInternalnum.Text = xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber") != null)
                    txtExternalnum.Text = xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetRoomProfile/VMRLink") != null)//FB 2727
                    txtVMRLink.Text = xmldoc.SelectSingleNode("//GetRoomProfile/VMRLink").InnerText.Trim().Replace("&amp;", "&");


                if (xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate") != null)
                {
                    DateTime.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate").InnerText, out deactDate);

                    if (deactDate != DateTime.MinValue)
                        lblMdate.Text = deactDate.ToString(dtFormatType) + " " + deactDate.ToString(tformat);
                }
                lblMUser.Text = xmldoc.SelectSingleNode("//GetRoomProfile/LastModififeduserName").InnerText;
            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.StackTrace);
            }
        }
        #endregion

        #region SetRoomProfile
        /// <summary>
        /// SetRoomProfile
        /// </summary>
        /// <returns></returns>
        protected void SetRoomProfile(Object sender,EventArgs e)
        {
            XmlDocument createRooms = null;
            XmlDocument loadRooms = null;
            String roomxmlPath = "";//Room search
            XmlNodeList roomList = null;
            string roomId = "";
            XmlNode ndeRoomID = null;
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<SetRoomProfile>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<RoomID>" + hdnRoomID.Value + "</RoomID>");
                inXML.Append("<RoomName>" + txtRoomName.Text + "</RoomName>");
                inXML.Append("<RoomPhoneNumber></RoomPhoneNumber>");
                inXML.Append("<MaximumCapacity>0</MaximumCapacity>");
                inXML.Append("<MaximumConcurrentPhoneCalls>0</MaximumConcurrentPhoneCalls>");
                inXML.Append("<SetupTime>0</SetupTime>");
                inXML.Append("<TeardownTime>0</TeardownTime>");
                inXML.Append("<AssistantInchargeID>" + AssistantID.Value + "</AssistantInchargeID>");
                inXML.Append("<AssistantInchargeName></AssistantInchargeName>");
                inXML.Append("<MultipleAssistantEmails></MultipleAssistantEmails>");
                inXML.Append("<Tier1ID>" + Session["OnflyTopTierID"].ToString() + "</Tier1ID>"); 
                inXML.Append("<Tier2ID>" + Session["OnflyMiddleTierID"].ToString() + "</Tier2ID>");
                inXML.Append("<Floor></Floor>");
                inXML.Append("<RoomNumber></RoomNumber>");
                inXML.Append("<StreetAddress1></StreetAddress1>");
                inXML.Append("<StreetAddress2></StreetAddress2>");
                inXML.Append("<City></City>");
                inXML.Append("<State></State>");
                inXML.Append("<ZipCode></ZipCode>");
                inXML.Append("<Country></Country>");
                inXML.Append("<Handicappedaccess>0</Handicappedaccess>");
                inXML.Append("<isVIP>0</isVIP>");
                inXML.Append("<isTelepresence>0</isTelepresence>");
                inXML.Append("<RoomQueue></RoomQueue>");
                inXML.Append("<MapLink></MapLink>");
                inXML.Append("<ParkingDirections></ParkingDirections>");
                inXML.Append("<AdditionalComments></AdditionalComments>");
                inXML.Append("<TimezoneID>26</TimezoneID>");
                inXML.Append("<Longitude></Longitude>");
                inXML.Append("<Latitude></Latitude>");
                inXML.Append("<Approvers>");
                inXML.Append("  <Approver1ID></Approver1ID>");
                inXML.Append("  <Approver1Name></Approver1Name>");
                inXML.Append("  <Approver2ID></Approver2ID>");
                inXML.Append("  <Approver2Name></Approver2Name>");
                inXML.Append("  <Approver3ID></Approver3ID>");
                inXML.Append("  <Approver3Name></Approver3Name>");
                inXML.Append("</Approvers>");
                inXML.Append("<EndpointID></EndpointID>");
                inXML.Append("<Custom1></Custom1>");
                inXML.Append("<Custom2></Custom2>");
                inXML.Append("<Custom3></Custom3>");
                inXML.Append("<Custom4></Custom4>");
                inXML.Append("<Custom5></Custom5>");
                inXML.Append("<Custom6></Custom6>");
                inXML.Append("<Custom7></Custom7>");
                inXML.Append("<Custom8></Custom8>");
                inXML.Append("<Custom9></Custom9>");
                inXML.Append("<Custom10></Custom10>");
                inXML.Append("<Projector>0</Projector>");
                inXML.Append("<RoomCategory>" + (int)myVRMNet.NETFunctions.RoomCategory.VMRRoom + "</RoomCategory>"); //FB 2694
                inXML.Append("<Video>2</Video>"); //Media type - video rooms //FB 2488
                inXML.Append("<DynamicRoomLayout>1</DynamicRoomLayout>");
                inXML.Append("<CatererFacility>0</CatererFacility>");
                inXML.Append("<ServiceType>-1</ServiceType>");
                inXML.Append("<DedicatedVideo>0</DedicatedVideo>");
                inXML.Append("<DedicatedCodec>0</DedicatedCodec>");
                inXML.Append("<IsVMR>1</IsVMR>");
                inXML.Append("<InternalNumber>" + txtInternalnum.Text + "</InternalNumber>");
                inXML.Append("<ExternalNumber>" + txtExternalnum.Text + "</ExternalNumber>");
                inXML.Append("<VMRLink>" + txtVMRLink.Text.Replace("&","&amp;").Trim() + "</VMRLink>");//FB 2727
                inXML.Append("<AVOnsiteSupportEmail></AVOnsiteSupportEmail>");
                inXML.Append("<Departments>");
                
                foreach (ListItem item in DepartmentList.Items)
                {
                    if (item.Selected)
                        inXML.Append("<Department><ID>" + item.Value + "</ID><Name>" + item.Text + "</Name><SecurityKey></SecurityKey></Department>");
                }

                inXML.Append("</Departments>");

                //FB 2994 start...
                rmImg = "";
                inXML.Append("<RoomImages>"); 
                foreach (DataGridItem dgi in dgItems.Items)
                {
                    if (rmImg == "")
                        rmImg = dgi.Cells[0].Text.Trim();
                    else
                        rmImg += "," + dgi.Cells[0].Text.Trim();

                    inXML.Append("<Image>");
                    inXML.Append("<ActualImage>" + dgi.Cells[1].Text.Trim() + "</ActualImage>");
                    inXML.Append("</Image>");
                }
                inXML.Append("</RoomImages>");
                inXML.Append("<RoomImageName>" + rmImg + "</RoomImageName>");
                //FB 2994 End ...

                inXML.Append("<Images>");
                inXML.Append("  <Map1></Map1>");
                inXML.Append("  <Map1Image></Map1Image>");
                inXML.Append("  <Map2></Map2>");
                inXML.Append("  <Map2Image></Map2Image>");
                inXML.Append("  <Security1></Security1>");
                inXML.Append("  <Security1ImageId></Security1ImageId>");
                inXML.Append("  <Misc1></Misc1>");
                inXML.Append("  <Misc1Image></Misc1Image>");
                inXML.Append("  <Misc2></Misc2>");
                inXML.Append("  <Misc2Image></Misc2Image>");
                inXML.Append("</Images>");
                inXML.Append("</SetRoomProfile>");

                log.Trace("SetRoomProfile Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetRoomProfile", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetRoomProfile Outxml: " + outXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);

                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    String innerXML = "";
                    Boolean newroom = true;
                    roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 1830
                    if (File.Exists(roomxmlPath))
                    {
                        loadRooms = new XmlDocument();
                        loadRooms.LoadXml(outXML);
                        ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room/RoomID");
                        if (ndeRoomID != null)
                            roomId = ndeRoomID.InnerText;
                        if (roomId != "")
                        {
                            createRooms = new XmlDocument();
                            if (!obj.WaitForFile(roomxmlPath))
                            {
                                return;
                            }
                            else
                                createRooms.Load(roomxmlPath);

                            roomList = createRooms.SelectNodes("Rooms/Room");
                            ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room");
                            innerXML = ndeRoomID.InnerXml;
                            if (roomList != null)
                            {
                                for (int a = 0; a < roomList.Count; a++)
                                {
                                    if (roomList[a].SelectSingleNode("RoomID").InnerText.Trim() == roomId.Trim())
                                    {
                                        newroom = false;
                                        if (ndeRoomID != null)
                                            roomList[a].InnerXml = ndeRoomID.InnerXml;
                                    }

                                }

                                if (newroom)
                                {
                                    if (innerXML != "")
                                    {
                                        ndeRoomID = createRooms.CreateElement("Room");
                                        ndeRoomID.InnerXml = innerXML;
                                        createRooms.SelectSingleNode("Rooms").AppendChild(ndeRoomID);
                                    }
                                }

                                if (File.Exists(roomxmlPath))
                                {
                                    if (obj.WaitForFile(roomxmlPath))
                                    {
                                        File.Delete(roomxmlPath);
                                        createRooms.Save(roomxmlPath);
                                    }
                                }
                            }

                        }

                    }
                    
                }

                Button btnCtrl = (System.Web.UI.WebControls.Button)sender;
                if (btnCtrl.ID.Trim().Equals("btnSubmitAddNew"))
                    Response.Redirect("ManageVirtualMeetingRoom.aspx?m=1");
                else
                    Response.Redirect("manageroom.aspx?hf=&m=1&pub=&d=&comp=&f=&frm=");

            }
            catch (Exception ex)
            {
                log.Trace("SetRoomProfile: "+ ex.StackTrace);
                return;
            }
        }
        #endregion

        #region ResetRoomProfile
        /// <summary>
        /// ResetRoomProfile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetRoomProfile(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageVirtualMeetingRoom.aspx?rID=" + hdnRoomID.Value);
            }
            catch (Exception ex)
            {
                log.Trace("ResetRoomProfile" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        // FB 2994 starts...

        #region UploadRoomImage
        /// <summary>
        /// UploadRoomImage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UploadRoomImage(Object sender, EventArgs e)
        {
            try
            {
                sessionKey = "RoomImg" + Session.SessionID;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                string imageString = "";
                //ZD 100263
                string filextn = "";
                List<string> strExtension = null;
                bool filecontains = false;

                if (roomImageDt == null)
                {
                    roomImageDt = new DataTable();
                    roomImageDt.Columns.Add(new DataColumn("ImageName"));
                    roomImageDt.Columns.Add(new DataColumn("Image"));
                    roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                    roomImageDt.Columns.Add(new DataColumn("ImagePath"));
                }
                FillRoomDataTable();

                if (!roomfileimage.Value.Equals(""))
                {
                    fName = Path.GetFileName(roomfileimage.Value);

                    //ZD 100263 Starts
                    //filextn = fName.Split('.')[fName.Split('.').Length - 1];
                    filextn = Path.GetExtension(fName);
                    strExtension = new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif" };
                    filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                    if (!filecontains)
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End

                    myFile = roomfileimage.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];

                    fileext = Path.GetExtension(roomfileimage.Value);

                    pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/" + fName;

                    if (File.Exists(pathName))
                        errMsg += "Room image already exists.";
                    else
                    {
                        myFile.InputStream.Read(myData, 0, nFileLen);
                        if (nFileLen <= 5000000)
                        {
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName, ref myData);

                            imageString = imageUtilObj.ConvertByteArrToBase64(myData);

                            DataRow dr = roomImageDt.NewRow();
                            dr["ImageName"] = fName;
                            dr["Imagetype"] = fileext;
                            dr["Image"] = imageString;
                            dr["ImagePath"] = pathName;

                            roomImageDt.Rows.Add(dr);

                            dgItems.DataSource = roomImageDt;
                            dgItems.DataBind();

                            dgItems.Visible = true;
                        }
                        else
                            errMsg += "Room image is greater than 5MB. File has not been uploaded.";
                    }
                }

                if (errMsg != "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region FillRoomDataTable

        private void FillRoomDataTable()
        {
            try
            {
                foreach (DataGridItem dgi in dgItems.Items)
                {
                    DataRow dr = roomImageDt.NewRow();

                    dr["ImageName"] = dgi.Cells[0].Text;
                    dr["Imagetype"] = dgi.Cells[2].Text;
                    dr["Image"] = dgi.Cells[1].Text;
                    dr["ImagePath"] = dgi.Cells[3].Text;

                    roomImageDt.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("FillRoomDatatable" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }

        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Item ?") + "')"); 
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindRowsDeleteMessage" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region LoadRoomImageGrid - Added for Image Project Edit Mode

        private void LoadRoomImageGrid(XmlNodeList rmImgNodes)
        {
            try
            {
                roomImageDt = new DataTable();
                roomImageDt.Columns.Add(new DataColumn("ImageName"));
                roomImageDt.Columns.Add(new DataColumn("Image"));
                roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                roomImageDt.Columns.Add(new DataColumn("ImagePath"));

                string imgData = "";
                pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/";

                foreach (XmlNode xnode in rmImgNodes)
                {
                    fName = xnode.SelectSingleNode("ImageName").InnerText;
                    fileext = xnode.SelectSingleNode("Imagetype").InnerText;
                    imgData = xnode.SelectSingleNode("Image").InnerText;

                    if (!File.Exists(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName))
                    {
                        File.Delete((Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName));
                    }
                    imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);
                    WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName, ref imgArray);

                    DataRow dr = roomImageDt.NewRow();

                    dr["ImageName"] = fName;
                    dr["Imagetype"] = fileext;
                    dr["Image"] = imgData;
                    dr["ImagePath"] = pathName + fName;

                    roomImageDt.Rows.Add(dr);
                }
                if (roomImageDt.Rows.Count > 0)
                    dgItems.DataSource = roomImageDt;
                else
                    dgItems.DataSource = null;

                dgItems.DataBind();
                dgItems.Visible = true;

            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region RemoveImage
        protected void RemoveImage(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string imgName = dgItems.Items[e.Item.ItemIndex].Cells[0].Text;

                roomImageDt = new DataTable();
                roomImageDt.Columns.Add(new DataColumn("ImageName"));
                roomImageDt.Columns.Add(new DataColumn("Image"));
                roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                roomImageDt.Columns.Add(new DataColumn("ImagePath"));

                foreach (DataGridItem dgi in dgItems.Items)
                {
                    if (!dgi.ItemIndex.Equals(e.Item.ItemIndex))
                    {
                        DataRow dr = roomImageDt.NewRow();

                        dr["ImageName"] = dgi.Cells[0].Text;
                        dr["Imagetype"] = dgi.Cells[2].Text;
                        dr["Image"] = dgi.Cells[1].Text;
                        dr["ImagePath"] = dgi.Cells[3].Text;

                        roomImageDt.Rows.Add(dr);
                    }
                }

                if (roomImageDt.Rows.Count > 0)
                    dgItems.DataSource = roomImageDt;
                else
                    dgItems.DataSource = null;

                dgItems.DataBind();
                dgItems.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("RemoveImage" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion    

        // FB 2994 ends...

    }
}
