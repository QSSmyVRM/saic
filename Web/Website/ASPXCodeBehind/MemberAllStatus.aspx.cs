/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Xml;



namespace ns_MyVRM
{
    public partial class en_MemberAllStatus : System.Web.UI.Page
    {


        myVRMNet.NETFunctions obj;

        protected System.Web.UI.WebControls.DataGrid dgViewDetails; 
        protected System.Web.UI.WebControls.Label GroupName;

      
        
        #region private data members

        private String groupID = "";
        private Int32 userID = Int32.MinValue;

        #endregion

        public en_MemberAllStatus()
        {
            obj = new myVRMNet.NETFunctions();
        }

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("MemberAllStatus.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
               // Response.Write(Request.QueryString["GroupID"]);

                if (Request.QueryString["GroupID"] != null)
                {
                    if (Request.QueryString["GroupID"] != "")
                    {
                        groupID = Request.QueryString["GroupID"].ToString();
                    }
                }


                if (!IsPostBack)
                    BindDataWithInXML();

            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        #region Bind Data

        protected void BindDataWithInXML()
        {
          
            try
            {
                //FB 2027 - Starts
                String inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>"+ obj.OrgXMLElement() + "<groupID>" + groupID + "</groupID><sortBy>0</sortBy></login>";
                //String outXML = obj.CallCOM("GetGroup", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetGroup", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
               // Response.Write(outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                

                    XmlNodeList nodes = xmldoc.SelectNodes("//groups/group");
                   
                    
                    if (nodes.Count > 0)
                    {
                        DataGrid dgViewDetails = (DataGrid)FindControl("dgViewDetails");
                        
                        foreach (XmlNode node in nodes)
                        {
                          
                            if (node.SelectSingleNode("groupID").InnerText.Equals(groupID))
                            {
                                GroupName.Text = node.SelectSingleNode("groupName").InnerText;

                                XmlNodeList subNodes = node.SelectNodes("user");
                                LoadGroupDetailsGrid(subNodes, dgViewDetails);
                            }
                        }
                         

                    }

                   

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region LoadGroupDetailsGrid

       
        protected void LoadGroupDetailsGrid(XmlNodeList nodes, DataGrid dgViewDetails)
        {
            try
            {
                XmlTextReader xtr;
                DataSet dset = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    dset.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dta = new DataTable();
                DataTable dta1 = new DataTable();

                if (dset.Tables.Count > 0)
                {
                    dta = dset.Tables[0];
                    DataRow dr = null;
                    for (int i = 0; i < dta.Rows.Count; i++)
                    {
                        dr = dta.Rows[i];
                        if (dr["userLastName"] != null) //FB 2023
                            if (dr["userLastName"].ToString().Trim() == "")
                                dr["userEmail"] = "";
                           
                    }
                    
                    dgViewDetails.DataSource = dta;
                    dgViewDetails.DataBind();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion
    }
}


