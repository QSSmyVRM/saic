﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
#region Namespaces
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web.Services;
using System.Xml;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Text;
using System.Net;
using System.IO;
#endregion

#region ns_MonitorMCU
public partial class ns_MonitorMCU : System.Web.UI.Page
{

    # region prviate DataMember
    protected System.Web.UI.WebControls.CheckBox chkEncryption;
    protected System.Web.UI.WebControls.CheckBox chkIsOutside;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGridState;
    protected System.Web.UI.HtmlControls.HtmlInputHidden TotalMcuCount;
    protected System.Web.UI.WebControls.CheckBox chkAllSilo;   //FB 2646 
    protected System.Web.UI.WebControls.Label lblchksilo; //FB 2646 
    protected System.Web.UI.WebControls.Label lblError; // FB 2653   
    #endregion

    #region Global Variable declaration
    protected string[] MCUConfList;
    protected string[] MCUConfAddr;
    protected string[] MCUConfId; // FB 2653
    protected string[] ConfPartList;
    protected string[] ConfPartId;
    protected XDocument xd;
    protected XElement tree;
    protected XElement nodes;
    protected int gblMcuIndex;
    protected int gblConfIndex;
    protected int gblMcuCount;
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    protected System.Web.UI.WebControls.GridView grdGrand;
    //FB 2616 Start
    MyVRMNet.LoginManagement loginMgmt = null;
    String userdetails = "";
    //FB 2616 End
    #endregion

    public ns_MonitorMCU()
    {
        //
        // TODO: Add constructor logic here
        //
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
    }

    #region Declared Classes

    #region Class_MCU
    public class MCU
    {

        public string nameid { get; set; }//FB 2956
        public string name { get; set; }
        public string siloName { get; set; } //FB 2646
        public string ip { get; set; }
        public string port { get; set; }
        public string type { get; set; }
        public string state { get; set; }
        public string error { get; set; }
        public int mcuID { get; set; }
        public int mcuIndex { get; set; }
        public int confCount { get; set; }
        public int favorite { get; set; }

        public string MCUTotalPort { get; set; }
        public string MCUStatus { get; set; }

        // Visible Status Supporting proprty (Conference)
        public string confMultiInfo { get; set; }
        public string partyMultiInfo { get; set; }
        //ZD 100113 Start
        public string urlInfo { get; set; }
        public string passPhrase { get; set; }
        //ZD 100113 End
    }
    #endregion

    #region Class_Conference
    /// <summary>
    /// Conference Class
    /// </summary>
    public class Conference
    {
        public string confId { get; set; }
        public string confStatus { get; set; }
        public string mcuAlias { get; set; }
        public string confName { get; set; }
        public string xconfName { get; set; }
        public string confType { get; set; }
        public string confDate { get; set; }
        public string confserverTime { get; set; }        
        public string duration { get; set; }
        public string CurSetlayoutValue { get; set; }
        public string confRecording { get; set; } // FB 2441
        public TimeSpan ConfFinishingTime { get; set; }

        // FB 2652 Starts
        public string confMuteAllExcept{ get; set; }
        public string confUnmuteAll { get; set; }
        //FB 2652 Ends

        public string confActualStatus { get; set; }
        public string connectingStatus { get; set; }
        public string confLastRun { get; set; }
        public string xparentId { get; set; }
        public string xmergeString { get; set; }
        public string conforgID { get; set; } //FB 2646
        
        

        public string ConflockStatus { get; set; }
        public string ConfMsgStatus { get; set; }
        public string ConfAudioTxStatus { get; set; }
        public string ConfAudioRxStatus { get; set; }
        public string ConfVideoTxStatus { get; set; }
        public string ConfVideoRxStatus { get; set; }
        
        public string confcameraStatus { get; set; }
        public string confpacketlossStatus { get; set; }
        public string confrecordStatus { get; set; }
        public string ConftypeStatus { get; set; }
        public string ConfCurrentRowIndex { get; set; }
        public string ConftotalRowCount { get; set; }
        public string confMcuBridgeID { get; set; }
        public int confMcuIndex { get; set; }
        public int xconfMcuIndex { get; set; }
        public int confCurIndex { get; set; }

        public int partCount { get; set; }
        public int confTimer { get; set; }
        public string McuIP { get; set; }
        // Visible Status Supporting proprty (Conference)
        public string ConfLockVisibleStatus2 { get; set; }
        public string ConfMessageVisibleStatus2 { get; set; }
        public string ConfAudioTXVisibleStatus2 { get; set; }
        public string ConfAudioRXVisibleStatus2 { get; set; }
        public string ConfVideoTXVisibleStatus2 { get; set; }
        public string ConfVideoRXVisibleStatus2 { get; set; }
        public string ConfLayoutVisibleStatus2 { get; set; }
        public string ConfCameraVisibleStatus2 { get; set; }
        public string ConfPacketlossVisibleStatus2 { get; set; }
        public string ConfRecordVisibleStatus2 { get; set; }

        public string partyMultiInfo2 { get; set; }
        public string confDate1 { get; set; }

    }
    #endregion

    #region Class_Participant
    /// <summary>
    /// Participant Class
    /// </summary>
    public class Participant
    {
        public string xchildId { get; set; }
        public string mergStr { get; set; }
        public string participantName { get; set; }
        public string partAddr { get; set; }
        public string connectionStatus { get; set; }
        public string conPartyId { get; set; }
        public string conTerminalType { get; set; }

        public string bridID { get; set; }

        // Change Status

        public string partyCallStatusinfo { get; set; }
        public string partyCallStatus { get; set; }
        
        public string partyMsgStatus { get; set; }
        public string partyAudioTxStatus { get; set; }
        public string partyAudioRxStatus { get; set; }
        public string partyVideoTxStatus { get; set; }
        public string partyVideoRxStatus { get; set; }
        public string partyLayoutVisibleStatus { get; set; }
        public string partycameraStatus { get; set; }
        public string partypacketlossStatus { get; set; }
        public string partyrecordStatus { get; set; }

        public string LayoutValue { get; set; }
		public string partychairperson { get; set; } //FB 2553
        public string partylecturemode { get; set; } //FB 2553
        public string partytotalRowCount { get; set; }
        public string partyCurrentRowIndex { get; set; }
        //public string terminalCount { get; set; }
        public int partyMcuIndex { get; set; }
        public int partyConfIndex { get; set; }
        public int partyCurIndex { get; set; }

        public string partybandwidth3 { get; set; }
        public string partysetfocus3 { get; set; }
        public string partymessage3 { get; set; }
        public string partyAudioTx3 { get; set; }
        public string partyAudioRx3 { get; set; }
        public string partyVideoTx3 { get; set; }
        public string partyVideoRx3 { get; set; }
        public string partyLayout3 { get; set; }
        public string partycamera3 { get; set; }
        public string partypacketloss3 { get; set; }
        public string partyimagestream3 { get; set; }
        public string partylock3 { get; set; }
        public string partyrecord3 { get; set; }
        public string partylecturemode3 { get; set; }
        public string partyrxpreviewURL { get; set; }
        public string partyLeader { get; set; } //FB 2553
        public string bridgeAddress { get; set; } //ZD 100113

    }
    #endregion
      

    #endregion

    #region Bind Details

    #region Page Load & load the Master grid
    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder inXml = new StringBuilder();
        String URI = "", ipAddress = "", passPhrase = "";  //ZD 100113 
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("MonitorMCU.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            //FB 2646 Starts
            if (Session["UsrCrossAccess"] != null)
            {
                if (Session["UsrCrossAccess"].ToString().Equals("1") && Session["organizationID"].ToString() == "11" && Session["OrganizationsLimit"].ToString().Trim() != "1")
                {
                    chkAllSilo.Attributes.Add("style", "display:block");
                    lblchksilo.Attributes.Add("style", "display:block");
                }
                else
                {
                    chkAllSilo.Attributes.Add("style", "display:none");
                    lblchksilo.Attributes.Add("style", "display:none");
                }
            }
            //FB 2646 Ends

            //FB 2616 Start
            if (Request.QueryString["tp"] == "CM")
            {
                loginMgmt = new MyVRMNet.LoginManagement();
                if (Request.QueryString["req"] != null)
                    userdetails = Request.QueryString["req"];

                loginMgmt.simpleDecrypt(ref userdetails);
                string[] id = userdetails.Split(',');
                loginMgmt.queyStraVal = id[0].ToString();
                loginMgmt.qStrPVal = id[1].ToString();
                loginMgmt.GetHomeCommand();
            }
            //FB 2616 End


            inXml.Append("<CallMonitor>");
            if (!chkAllSilo.Checked)//FB 2646
                inXml.Append(obj.OrgXMLElement()); 
            inXml.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
            inXml.Append("<ConferenceID></ConferenceID><ConferenceUniqueID></ConferenceUniqueID>");
            inXml.Append("<StatusFilter><ConferenceStatus>5</ConferenceStatus></StatusFilter>");
            inXml.Append("<TypeFilter><ConfType>2</ConfType></TypeFilter>");
            inXml.Append("</CallMonitor>");

            String outxml = obj.CallMyVRMServer("GetCallsforMonitor", inXml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

            XmlDocument xdoc = new XmlDocument();
            
            xdoc.LoadXml(outxml);
            xd = XDocument.Parse(xdoc.OuterXml);

            int k = 0;
            var MCUs = (from XMcu in xd.Descendants("Mcu")
                        select new MCU
                        {
                            //FB 2501 Dec10
                            nameid = Convert.ToString(XMcu.Element("name").Value) + Convert.ToString(XMcu.Element("Address").Value),//FB 2956
                            name = Convert.ToString(XMcu.Element("name").Value),
                            siloName = Convert.ToString(XMcu.Element("SiloName").Value), //FB 2646
                            ip = Convert.ToString(XMcu.Element("Address").Value),
                            type = Convert.ToString(XMcu.Element("McuType").Value),
                            favorite = Convert.ToInt32(XMcu.Element("McuFavorite").Value),
                            mcuID = Convert.ToInt32(XMcu.Element("McuID").Value),
                            MCUTotalPort = Convert.ToString(XMcu.Element("McuPorts").Value).Replace("!","/"), //FB 2501 Dec7
                            //FB 2501 Dec10
                            MCUStatus = obj.GetTranslatedText("Status")+": "+((Convert.ToString(XMcu.Element("McuStatus").Value) == "1") ? obj.GetTranslatedText("Active") : ((Convert.ToString(XMcu.Element("McuStatus").Value) == "2") ? obj.GetTranslatedText("Maintanance") :obj.GetTranslatedText("Disabled"))),                                                     
                            mcuIndex = k++,
                            confCount = Convert.ToInt32(XMcu.Element("Conferences").Element("ConferenceCount").Value),
                            //ZD 100113 Start
                            passPhrase = XMcu.Element("SupportMCUParams").Element("PassPhrase").Value.Replace("||","&"),
                            urlInfo = XMcu.Element("SupportMCUParams").Element("LoginURL").Value,
                            //ZD 100113 End
                            // Visible Status Supporting proprty (Conference)

                            confMultiInfo = Convert.ToString(XMcu.Element("SupportMCUParams").Element("conflockunlock").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confmessage").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confAudioTx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confAudioRx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confVideoTx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confVideoRx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confLayout").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confcamera").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confpacketloss").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confrecord").Value) + ","
							//FB 2652 Starts
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confMuteAllExcept").Value) + "," 
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("confUnmuteAll").Value), 
							//FB 2652 Ends

                            partyMultiInfo = Convert.ToString(XMcu.Element("SupportMCUParams").Element("partybandwidth").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partysetfocus").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partymessage").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyAudioTx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyAudioRx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyVideoTx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyVideoRx").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyLayout").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partycamera").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partypacketloss").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyimagestream").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partylock").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyrecord").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partylecturemode").Value) + ","
                            + Convert.ToString(XMcu.Element("SupportMCUParams").Element("partyLeader").Value), //FB 2553

                        }).ToList();

            for (int i = 0; i < MCUs.Count; i++)
            {
                //ZD 100113 Start
                URI = ""; ipAddress = ""; passPhrase = "";
                URI = MCUs[i].urlInfo;
                ipAddress = MCUs[i].ip;
                passPhrase = MCUs[i].passPhrase;
                FillLoginCookieforCodian(ref URI, ref passPhrase, ref ipAddress);
                //ZD 100113 End
                if (MCUs[i].favorite == 1)
                {
                    hdnGridState.Value += "divid-" + MCUs[i].nameid + ",";//FB 2964
                }
            }

            MCUConfList = (from node in xd.Descendants("Mcu")
                           select node.Element("Conferences").Value).ToArray();

            MCUConfAddr = (from node in xd.Descendants("Mcu")
                           select node.Element("Address").Value).ToArray();

            MCUConfId = (from node in xd.Descendants("Mcu")
                           select node.Element("McuID").Value).ToArray(); // FB 2653

            var mcuCount = (from node in xd.Descendants("McuList")
                            select node.Element("McuTotalListCount").Value).ToArray();


            // FB 2653 Starts
            lblError.Text = "";
            for (int i = 0; i < MCUConfAddr.Length; i++)
            {
                for (int j = 0; j < MCUConfAddr.Length; j++)
                {
                    if (j == i)
                        continue;
                    if (MCUConfAddr[j] == MCUConfAddr[i])
                        lblError.Text = obj.GetTranslatedText("Note: MCU IP address is already in use.");                    
                }
            }
            // FB 2653 Ends


            gblMcuCount = Convert.ToInt32(mcuCount[0]);

            TotalMcuCount.Value = gblMcuCount.ToString();

            grdGrand.DataSource = MCUs;
            grdGrand.DataBind();

        }
        catch (Exception ex)
        {
            log.Trace("Page_Load" + ex.Message);
        }
    }

    #endregion

    #region bindConference
    /// <summary>
    /// bindConference
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void bindConference(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                gblMcuIndex = index;

                if (MCUConfList[index].ToString() != "")
                {

                   
                    Label McuIP = (Label)e.Row.FindControl("lblMcuIP");

                    Label confMultiInfo = (Label)e.Row.FindControl("lblConfMultiInfo");
                    Label partyMultiInfo = (Label)e.Row.FindControl("lblPartyMultiInfo");

                    string[] confInfo = confMultiInfo.Text.Split(',');

                    tree = (XElement)(from c in xd.Descendants("Mcu")
                                      where c.Element("McuID").Value == MCUConfId[index].ToString()
                                      select c).Single(); // FB 2653


                    //FB 2588 Start
                    string tformat = "";
                     if (Session["timeFormat"].ToString().Equals("0"))
                         tformat = "MM/dd/yyyy HH:mm";
                     else if (Session["timeFormat"].ToString().Equals("1"))
                         tformat = "MM/dd/yyyy hh:mm tt";
                     else if (Session["timeFormat"].ToString().Equals("2"))
                         tformat = "MM/dd/yyyy HHmmZ";
                     //FB 2588 End

                    int j = 0;
                    var conf = (from XMcuConf in tree.Descendants("Conference")
                                select new Conference
                                {
                                    confId = Convert.ToString(XMcuConf.Element("ConferenceID").Value),
                                    confActualStatus = Convert.ToString(XMcuConf.Element("ConferenceActualStatus").Value),
                                    mcuAlias = Convert.ToString(XMcuConf.Element("ConferenceUniqueID").Value),
                                    confName = Convert.ToString(XMcuConf.Element("ConferenceName").Value) + " (" + Convert.ToString(XMcuConf.Element("ConferenceUniqueID").Value) + ")",
                                    xconfName = Convert.ToString(XMcuConf.Element("ConferenceName").Value) + gblMcuIndex.ToString(),
                                    confType = Convert.ToString(XMcuConf.Element("ConferenceType").Value),
                                    confDate = DateTime.Parse(XMcuConf.Element("ConferenceDateTime").Value).ToString(tformat),//FB 2501 Dec7 //FB 2588
                                    confDate1 = Convert.ToString(XMcuConf.Element("ConferenceDateTime").Value),
                                    confserverTime = Convert.ToString(XMcuConf.Element("ServerDateTime").Value),                                   
                                    duration = ((Convert.ToInt32(XMcuConf.Element("ConferenceDuration").Value) / 60) + obj.GetTranslatedText("hr")+" : " + ((Convert.ToInt32(XMcuConf.Element("ConferenceDuration").Value)) - (Convert.ToInt32(XMcuConf.Element("ConferenceDuration").Value) / 60) * 60)+obj.GetTranslatedText("min")),
                                    CurSetlayoutValue = Convert.ToString(XMcuConf.Element("confLayout").Value),                                   
                                    confRecording = Convert.ToString(XMcuConf.Element("confrecord").Value), // FB 2441
                                    
                                    ConfFinishingTime = (Convert.ToDateTime(Convert.ToString(XMcuConf.Element("ConferenceDateTime").Value)).AddMinutes(Convert.ToInt64(XMcuConf.Element("ConferenceDuration").Value))).Subtract(Convert.ToDateTime(Convert.ToString(XMcuConf.Element("ServerDateTime").Value))),

                                    //TimeSpan diff = dateTime1 - dateTime2,
                                    
                                    confStatus = Convert.ToString(XMcuConf.Element("ConferenceStatus").Value),
                                    connectingStatus = Convert.ToString(XMcuConf.Element("CallStartMode").Value),
                                    confLastRun = Convert.ToString(XMcuConf.Element("LastRunDate").Value),
                                    xparentId = MCUConfAddr[index].ToString().Replace(".", "-") + "_" + Convert.ToString(XMcuConf.Element("ConferenceUniqueID").Value),
                                    xmergeString = Convert.ToString(XMcuConf.Element("ConferenceID").Value),
                                    conforgID = Convert.ToString(XMcuConf.Element("ConferenceOrgID").Value), // FB 2646

                                   
                                    // Change status
                                    ConftypeStatus = Convert.ToString(XMcuConf.Element("ConferenceType").Value),
                                    ConflockStatus = Convert.ToString(XMcuConf.Element("conflockunlock").Value),
                                    ConfMsgStatus = Convert.ToString(XMcuConf.Element("confmessage").Value),
                                    ConfAudioTxStatus = Convert.ToString(XMcuConf.Element("confAudioTx").Value),
                                    ConfAudioRxStatus = Convert.ToString(XMcuConf.Element("confAudioRx").Value),
                                    ConfVideoTxStatus = Convert.ToString(XMcuConf.Element("confVideoTx").Value),
                                    ConfVideoRxStatus = Convert.ToString(XMcuConf.Element("confVideoRx").Value),                                    
                                    confcameraStatus = Convert.ToString(XMcuConf.Element("confcamera").Value),
                                    confpacketlossStatus = Convert.ToString(XMcuConf.Element("confpacketloss").Value),
                                    confrecordStatus = Convert.ToString(XMcuConf.Element("confrecord").Value),
                                    confTimer = TimeSpan.Parse((Convert.ToDateTime(XMcuConf.Element("ConferenceDateTime").Value)).Subtract(Convert.ToDateTime(XMcuConf.Element("ServerDateTime").Value)).ToString()).Seconds,
                                    confMcuIndex = gblMcuIndex,
                                    xconfMcuIndex  = gblMcuIndex +1,
                                    confCurIndex = j++,
                                    confMcuBridgeID = Convert.ToString(XMcuConf.Element("ConferenceMcuID").Value),
                                    partCount = Convert.ToInt32(XMcuConf.Element("PartyList").Element("PartyCount").Value),
                                    McuIP = McuIP.Text,
                                    
                                    // Visible Status Supporting proprty (Conference)
                                    ConfLockVisibleStatus2 = confInfo[0].ToString(),
                                    ConfMessageVisibleStatus2 = confInfo[1].ToString(),
                                    ConfAudioTXVisibleStatus2 = confInfo[2].ToString(),
                                    ConfAudioRXVisibleStatus2 = confInfo[3].ToString(),
                                    ConfVideoTXVisibleStatus2 = confInfo[4].ToString(),
                                    ConfVideoRXVisibleStatus2 = confInfo[5].ToString(),
                                    ConfLayoutVisibleStatus2 = confInfo[6].ToString(),
                                    ConfCameraVisibleStatus2 = confInfo[7].ToString(),
                                    ConfPacketlossVisibleStatus2 = confInfo[8].ToString(),
                                    ConfRecordVisibleStatus2 = confInfo[9].ToString(),
                                    confMuteAllExcept = confInfo[10].ToString(), // FB 2652
                                    confUnmuteAll = confInfo[11].ToString(), // FB 2652

                                    partyMultiInfo2 = partyMultiInfo.Text,

                                }).ToList();


                    ConfPartList = (from node in tree.Descendants("Conference")
                                    select node.Element("PartyList").Value).ToArray();

                    ConfPartId = (from node in tree.Descendants("Conference")
                                  select node.Element("ConferenceUniqueID").Value).ToArray();

                    GridView gvChild = (GridView)e.Row.FindControl("grdParent");

                    gvChild.DataSource = conf;
                    gvChild.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("bindconference" + ex.Message);
        }
    }
    #endregion

    #region bindParticipant
    /// <summary>
    /// bindParticipant
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void bindParticipant(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                gblConfIndex = index;
                if (ConfPartList[index].ToString() != "")
                {
                    Label lbl = (Label)e.Row.FindControl("lblxparentid");
                    Label lbl2 = (Label)e.Row.FindControl("lblxmergestr");

                    string parentId = lbl.Text;
                    string mergeString = lbl2.Text;
                    nodes = (XElement)(from p in tree.Descendants("Conference")
                                       where p.Element("ConferenceUniqueID").Value == ConfPartId[index].ToString()
                                       select p).Single();
                    Label MCUBRIDGEID = (Label)e.Row.FindControl("mcubridgeID");

                    Label partyMultiInfo2 = (Label)e.Row.FindControl("lblPartyMultiInfo2");
                    string[] partyInfo2 = partyMultiInfo2.Text.Split(',');

                    //Label CamVisibleStatus = (Label)e.Row.FindControl("CamVisible");  FB 2553
                    

                    int i = 0;
                    var party = (from XMcuPart in nodes.Descendants("Party")
                                 select new Participant
                                 {
                                     xchildId = parentId + "__" + i,
                                     mergStr = mergeString,
                                     participantName = Convert.ToString(XMcuPart.Element("Name").Value),
                                     partAddr = Convert.ToString(XMcuPart.Element("Address").Value),
                                     connectionStatus = Convert.ToString(XMcuPart.Element("Connectstatus").Value),
                                     conPartyId = Convert.ToString(XMcuPart.Element("PartyId").Value),
                                     conTerminalType = Convert.ToString(XMcuPart.Element("TerminalType").Value),
                                     partyCallStatus = ((Convert.ToString(XMcuPart.Element("Connectstatus").Value) == "2") ? "0" : ((Convert.ToString(XMcuPart.Element("Connectstatus").Value) == "0") ? "1" : "0")),
                                     partyCallStatusinfo = Convert.ToString(XMcuPart.Element("Connectstatus").Value),
                                     partyMsgStatus = Convert.ToString(XMcuPart.Element("partymessage").Value),
                                     partyAudioTxStatus = Convert.ToString(XMcuPart.Element("partyAudioTx").Value),
                                     partyAudioRxStatus = Convert.ToString(XMcuPart.Element("partyAudioRx").Value),
                                     partyVideoTxStatus = Convert.ToString(XMcuPart.Element("partyVideoTx").Value),
                                     partyVideoRxStatus = Convert.ToString(XMcuPart.Element("partyVideoRx").Value),                                     
                                     partycameraStatus = Convert.ToString(XMcuPart.Element("partycamera").Value),
                                     partypacketlossStatus = Convert.ToString(XMcuPart.Element("partypacketloss").Value),
                                     partyrecordStatus = Convert.ToString(XMcuPart.Element("partyrecord").Value),
									 LayoutValue = Convert.ToString(XMcuPart.Element("partyLayout").Value),
                                     partychairperson = Convert.ToString(XMcuPart.Element("partychairperson").Value),
                                     partylecturemode = Convert.ToString(XMcuPart.Element("partylecturemode").Value), //FB 2553
                                     bridgeAddress = XMcuPart.Element("MCUAddress").Value, //ZD 100113 
                                     //terminalCount = Convert.ToString(XMcuPart.Element("PartyCount").Value),

                                     //ConnectionStatusText = ((Convert.ToString(XMcuPart.Element("partyrecord").Value) == "0") ? "<b style='color:Red;'>Disconnected   </b>" : ((Convert.ToString(XMcuPart.Element("partyrecord").Value) == "1") ? "<b style='color:Yellow;'>Connecting...   </b>" : ((Convert.ToString(XMcuPart.Element("partyrecord").Value) == "2") ? "<b style='color:Green;'>Connected   </b>" : "<b style='color:Blue;'>Online   </b>"))),

                                     bridID = MCUBRIDGEID.Text, 

                                     partyMcuIndex = gblMcuIndex,
                                     partyConfIndex = gblConfIndex,
                                     partyCurIndex = i++,


                                     partybandwidth3 = partyInfo2[0].ToString(),
                                     partysetfocus3 = partyInfo2[1].ToString(),
                                     partymessage3 = partyInfo2[2].ToString(),
                                     partyAudioTx3 = partyInfo2[3].ToString(),
                                     partyAudioRx3 = partyInfo2[4].ToString(),
                                     partyVideoTx3 = partyInfo2[5].ToString(),
                                     partyVideoRx3 = partyInfo2[6].ToString(),
                                     partyLayout3 = partyInfo2[7].ToString(),
                                     partycamera3 = partyInfo2[8].ToString(), //FB 2553
                                     partypacketloss3 = partyInfo2[9].ToString(),
                                     partyimagestream3 = partyInfo2[10].ToString(),
                                     partylock3 = partyInfo2[11].ToString(),
                                     partyrecord3 = partyInfo2[12].ToString(),
                                     partylecturemode3 = partyInfo2[13].ToString(),
                                     partyLeader = partyInfo2[14].ToString(), //FB 2553
                                     
                                     partyrxpreviewURL = XMcuPart.Element("rxPreviewURL").Value

                                 }).ToList();

                    GridView gvImgGrid = (GridView)e.Row.FindControl("grdChild1");

                    DataTable slave2 = new DataTable();
                    slave2.Columns.Add("imgUrl");
                    DataRow dr22;
                    String URI = "", mcuIP = ""; //ZD 100113
                    for (int m = 0; m < party.Count; m++)
                    {
                        dr22 = slave2.NewRow();
                        //ZD 100113 Start
                        URI = party[m].partyrxpreviewURL.Replace("&amp;", "&");
                        mcuIP = party[m].bridgeAddress;
                        String URL = ReturnImageString(ref URI,ref mcuIP);
                        //ZD 100113 End
                        URL = URL.Replace("&amp;","&");
                        dr22["imgUrl"] = URL;
                        slave2.Rows.Add(dr22);
                    }
                    gvImgGrid.DataSource = slave2;
                    gvImgGrid.DataBind();


                    GridView gvChild = (GridView)e.Row.FindControl("grdChild2");

                    gvChild.DataSource = party;
                    gvChild.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("bindparticipant" + ex.Message);
        }
    }
    #endregion

    #endregion

    #region PopUp Window Functions

    #region fnPopupSetLayout
    [WebMethod]
    public static string fnPopupSetLayout(string fnmethod, string userID, string terminal, string Etype, string confID, string CurrentImgVal)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        XmlDocument xmlDoc = null;
        //XmlDocument res = new XmlDocument();        
        string result = "";
        try
        {

            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("  <userID>" + userID + "</userID>");
            if (fnmethod == "conference") // Change For Conference Layout
            {
                inXML.Append("<confID>" + confID + "</confID>");
                inXML.Append("<endpointID>0</endpointID>");
                inXML.Append("<terminalType>0</terminalType>");
                inXML.Append("<displayLayout>" + CurrentImgVal + "</displayLayout>");
                inXML.Append("<displayLayoutAll>1</displayLayoutAll>");
            }
            if (fnmethod == "particpant") //Change For Endpoint Layout
            {
                inXML.Append("<confID>" + confID + "</confID>");
                inXML.Append("<endpointID>" + Etype + "</endpointID>");
                inXML.Append("<terminalType>" + terminal + "</terminalType>");
                inXML.Append("<displayLayout>" + CurrentImgVal + "</displayLayout>");
                inXML.Append("<displayLayoutAll>2</displayLayoutAll>");
            }
            inXML.Append("</login>");
            outXML = obj.CallCOM2("DisplayTerminal", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            
                if (outXML.IndexOf("<error>") >= 0)
                {
                    result = obj.GetTranslatedText("Operation UnSuccessful");
                    
                }
                else
                {
                    outXML = obj.CallMyVRMServer("DisplayTerminal", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    result = obj.GetTranslatedText("Operation Successful!");
                }
            }
            
        catch (Exception ex)
        {
            log.Trace("fnPopupSetLayout" + ex.Message);
        }
        return result.ToString();
    }
    #endregion

    #region Add New User
    /// <summary>
    /// fnPopupAddUser
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="hdnEndpointID"></param>
    /// <param name="txtEndpointName"></param>
    /// <param name="hdnProfileID"></param>
    /// <param name="hdnApiPortNo"></param>
    /// <param name="lstAddressType"></param>
    /// <param name="txtAddress"></param>
    /// <param name="hdnEndpointURL"></param>
    /// <param name="lstConnectionType"></param>
    /// <param name="hdnVideoEquipment"></param>
    /// <param name="hdnExchangeID"></param>
    /// <param name="hdnLineRate"></param>
    /// <param name="lstBridges"></param>
    /// <param name="lstConnection"></param>
    /// <param name="lstProtocol"></param>
    /// <param name="hdnMCUServiceAdd"></param>
    /// <param name="lstMCUAddressType"></param>
    /// <param name="chkEncryption"></param>
    /// <param name="chkIsOutside"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnPopupAddUser(string userID, string confID, string MCUBridgeID, string hdnEndpointID, string txtEndpointName, string hdnProfileID, string hdnApiPortNo, string lstAddressType, string txtAddress, string hdnEndpointURL, string lstConnectionType, string hdnVideoEquipment, string hdnExchangeID, string hdnLineRate, string lstConnection, string lstProtocol, string hdnMCUServiceAdd, string chkEncryption, string chkIsOutside, string ConforgID,string chkMute) //FB 2646 //FB 2680
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string epID = "";
        StringBuilder inXML = new StringBuilder();
        StringBuilder AddXML = new StringBuilder();
        string outXML = "", MCUAddressType="";
        XmlDocument xmlDoc = null;
        //XmlDocument res = new XmlDocument();        
        string res ="";
        int ConfOrgID = 11;
        try
        {
            //FB 2646 Starts
            int.TryParse(ConforgID, out ConfOrgID);
            if (ConfOrgID > 11) 
            {
                HttpContext.Current.Session.Add("multisiloOrganizationID", ConfOrgID); ;
            }
            //FB 2646 Ends
            inXML.Append("<SetConferenceEndpoint>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<UserID>" + userID + "</UserID>");
            inXML.Append("<ConfID>" + confID + "</ConfID>");
            inXML.Append("<Endpoint>");
            inXML.Append("<EndpointID>" + hdnEndpointID + "</EndpointID>");
            inXML.Append("<EndpointName>" + txtEndpointName + "</EndpointName>");
            inXML.Append("<EndpointLastName></EndpointLastName>");
            inXML.Append("<EndpointEmail>admin@myvrm.com</EndpointEmail>");
            inXML.Append("<ProfileID>" + hdnProfileID + "</ProfileID>");
            inXML.Append("<Type>U</Type>");
            inXML.Append("<ApiPortno>" + hdnApiPortNo + "</ApiPortno>");
            inXML.Append("<EncryptionPreferred>" + chkEncryption + "</EncryptionPreferred>");
            inXML.Append("<AddressType>" + lstAddressType + "</AddressType>");
            inXML.Append("<Address>" + txtAddress.Trim() + "</Address>");
            inXML.Append("<URL>" + hdnEndpointURL + "</URL>");
            inXML.Append("<IsOutside>" + chkIsOutside + "</IsOutside>");
            inXML.Append("<ConnectionType>" + lstConnectionType + "</ConnectionType>");
            inXML.Append("<VideoEquipment>" + hdnVideoEquipment + "</VideoEquipment>");
            inXML.Append("<ExchangeID>" + hdnExchangeID + "</ExchangeID>");
            inXML.Append("<LineRate>" + hdnLineRate + "</LineRate>");
            inXML.Append("<Bridge>" + MCUBridgeID + "</Bridge>");
            inXML.Append("<Connection>" + lstConnection + "</Connection>");
            inXML.Append("<Protocol>" + lstProtocol + "</Protocol>");
            inXML.Append("<IsMute>" + chkMute + "</IsMute>");//FB 2680

            //FB 2501 Dec5
            if (lstProtocol == "1")
                MCUAddressType = "1";
            else if (lstProtocol == "2")
                MCUAddressType = "4";
            else if (lstProtocol == "4")
                MCUAddressType = "5";
            else if (lstProtocol == "3")
                MCUAddressType = "6";

            inXML.Append("<BridgeAddress>" + hdnMCUServiceAdd + "</BridgeAddress>");
            inXML.Append("<BridgeAddressType>" + MCUAddressType + "</BridgeAddressType>");
            inXML.Append("</Endpoint>");
            inXML.Append("</SetConferenceEndpoint>");
            outXML = obj.CallMyVRMServer("SetConferenceEndpoint", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
                res = obj.GetTranslatedText("Operation UnSuccessful");
            else
            {
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    if (xmlDoc.SelectSingleNode("/SetConferenceEndpoint/EndpointID") != null)
                        epID = xmlDoc.SelectSingleNode("/SetConferenceEndpoint/EndpointID").InnerText;
                    if (epID != "")
                    {
                        AddXML.Append("<AddConferenceEndpoint>");
                        AddXML.Append(obj.OrgXMLElement());
                        AddXML.Append("<UserID>" + userID + "</UserID>");
                        AddXML.Append("<ConfID>" + confID + "</ConfID>");
                        AddXML.Append("<EndpointID>" + epID + "</EndpointID>");
                        AddXML.Append("</AddConferenceEndpoint>");
                        outXML = obj.CallCommand("AddConferenceEndpoint", AddXML.ToString());
                    }
                }
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                    res = obj.GetTranslatedText("Operation Successful");
            }
        }
        catch (Exception ex)
        {
            log.Trace("fnPopupAddUser" + ex.Message);
        }
        return res.ToString();
    }
    #endregion

    #region Send Message
    /// <summary>
    /// fnSaveComments
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="endpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="Message"></param>
    /// <param name="Direction"></param>
    /// <param name="Duration"></param>
    /// <param name="fnIdentification"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnSaveComments(string userID, string confID, string endpointID, string terminalType, string Message, string Direction, string Duration, string fnIdentification,string MCUBridgeID)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        String outXML = "";
        string res = "";
        StringBuilder inXML = new StringBuilder ();
        bool partymessage = false; //Tamil
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<mcuId>" + MCUBridgeID + "</mcuId>");
            if (fnIdentification == "particpant")
            {
                partymessage = true; //Tamil
                inXML.Append("<endpointID>" + endpointID + "</endpointID>");
                inXML.Append("<terminalType>" + terminalType + "</terminalType>");
            }
            inXML.Append("<messageText>" + Message + "</messageText>");
            inXML.Append("<direction>" + Direction + "</direction>");
            inXML.Append("<duration>" + Duration + "</duration>");
            inXML.Append("<SendMsg>1</SendMsg>");//FB 3059
            inXML.Append("</login>");

            if (fnIdentification == "particpant")
                outXML = obj.CallCOM2("SendMessageToEndpoint", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
            else
                outXML = obj.CallCOM2("SendMessageToConference", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(outXML);
            //Tamil Start
            if (partymessage)
            {
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                    res = obj.GetTranslatedText("Operation Successful");
            }
            else
                res = obj.GetTranslatedText("Operation Successful");
            //Tamil End
        }
        catch (Exception ex)
        {
            log.Trace("fnSaveComments" + ex.Message);
        }
        return res;
    }
    #endregion

    #region fnCameraDirection
    [WebMethod]
    public static string fnCameraDirection(string userID, string confID, string endpointID, string terminalType, string direction, string fnIdentification)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            if (fnIdentification == "particpant") // Change For Participant
            {
                inXML.Append("<confID>" + confID + "</confID>");
                inXML.Append("<endpointID>" + endpointID + "</endpointID>");
                inXML.Append("<terminalType>" + terminalType + "</terminalType>");// 1 = user , 2 = room, 3 = guest
                inXML.Append("<Direction>" + direction + "</Direction>");
            }
            if (fnIdentification == "conference") // Change For Conference
            {
                inXML.Append("<confID>" + confID + "</confID>");
                inXML.Append("<endpointID>" + endpointID + "</endpointID>");
                inXML.Append("<terminalType></terminalType>");// 1 = user , 2 = room, 3 = guest
                inXML.Append("<Direction>" + direction + "</Direction>");
             }
            inXML.Append("</login>");
            outXML = obj.CallCOM2("ParticipantFECC", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") >= 0)
            {
                res = "Operation UnSuccessful";
            }
            else
            {
                res = obj.GetTranslatedText("Operation Successful!");
                //Do we need to save in Myvrm End?
                //outXML = obj.CallMyVRMServer("ParticipantFECC", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            }
        }
        catch (Exception ex)
        {
            log.Trace("fnCameraDirection" + ex.Message);
        }
        return res;

      }

    #endregion

    #region Conference Extend Time
    /// <summary>
    /// fnTimeExpand
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="Duration"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnTimeExpand(string userID, string confID, string Duration)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confInfo>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<retry>0</retry>");
            inXML.Append("<extendEndTime>" + Duration + "</extendEndTime>");
            inXML.Append("</confInfo>");
            inXML.Append("</login>");
            outXML = obj.CallCOM2("SetTerminalControl", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") >= 0)
            {
                res = obj.GetTranslatedText("Operation UnSuccessful");
            }
            else
            {
                outXML = obj.CallMyVRMServer("SetTerminalControl", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                    res = obj.GetTranslatedText("Operation Successful");
            }
        }
        catch (Exception ex)
        {
            log.Trace("fnTimeExpand" + ex.Message);
        }
        return res;

    }
    #endregion

    #region GetPacketDetails
    /// <summary>
    /// PacketDetails
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <returns></returns>
    [WebMethod]
    public static string PacketDetails(string userID, string confID, string EndpointID, string terminalType)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res="";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<endpointID>" + EndpointID + "</endpointID>");
            inXML.Append("<terminalType>" + terminalType + "</terminalType>");
            inXML.Append("</login>");
            outXML = obj.CallMyVRMServer("GetPacketLossTerminal", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_Configpath"].ToString());


            if (outXML.IndexOf("<error>") >= 0)
                res = obj.GetTranslatedText("Operation UnSuccessful");
            else
            {
                XDocument xd;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(outXML);
                xd = XDocument.Parse(xdoc.OuterXml);
                var AudioPacketReceived = ((Convert.ToString(xd.Element("PacketLossTerminal").Element("AudioPacketReceived").Value) != "") ? (xd.Element("PacketLossTerminal").Element("AudioPacketReceived").Value) : "N/A");
                var AudioPacketError = ((Convert.ToString(xd.Element("PacketLossTerminal").Element("AudioPacketError").Value) != "") ? (xd.Element("PacketLossTerminal").Element("AudioPacketError").Value) : "N/A");
                var AudioPacketMissing = ((Convert.ToString(xd.Element("PacketLossTerminal").Element("AudioPacketMissing").Value) != "") ? (xd.Element("PacketLossTerminal").Element("AudioPacketMissing").Value) : "N/A");
                var VideoPacketError = ((Convert.ToString(xd.Element("PacketLossTerminal").Element("VideoPacketError").Value) != "") ? (xd.Element("PacketLossTerminal").Element("VideoPacketError").Value) : "N/A");
                var VideoPacketReceived = ((Convert.ToString(xd.Element("PacketLossTerminal").Element("VideoPacketReceived").Value) != "") ? (xd.Element("PacketLossTerminal").Element("VideoPacketReceived").Value) : "N/A");
                var VideoPacketMissing = ((Convert.ToString(xd.Element("PacketLossTerminal").Element("VideoPacketMissing").Value) != "") ? (xd.Element("PacketLossTerminal").Element("VideoPacketMissing").Value) : "N/A");

                res = "AudioPacketReceived_" + AudioPacketReceived + "_AudioPacketError_" + AudioPacketError + "_AudioPacketMissing_" + AudioPacketMissing + "_VideoPacketError_" + VideoPacketError + "_VideoPacketReceived_" + VideoPacketReceived + "_VideoPacketMissing_" + VideoPacketMissing;
            }
        }
        catch (Exception ex)
        {
            log.Trace("PacketDetails" + ex.Message);
        }
        return res;
    }
    #endregion

    #endregion

    #region Audio/Video and Conference Connect/Disconnect Status
    /// <summary>
    /// fnAudioVideoStatus
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="ImgStatus"></param>
    /// <param name="fnidentification"></param>
    /// <param name="fnmethod"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnAudioVideoStatus(string userID, string confID, string EndpointID, string terminalType, string ImgStatus, string fnidentification, string fnmethod)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        bool connectall = false; //Tamil
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");

            if (fnmethod == "particpant")
            {
                inXML.Append("<endpointID>" + EndpointID + "</endpointID>");
                inXML.Append("<terminalType>" + terminalType + "</terminalType>");
            }
            else
            {
                inXML.Append("<endpointID>0</endpointID>");
                inXML.Append("<terminalType>0</terminalType>");
            }

            if (fnidentification == "videoTx" || fnidentification == "videoRx" || fnidentification == "audioTx" || fnidentification == "audioRx")
            {
                if (fnmethod == "particpant")
                {
                    if (ImgStatus == "1")
                    {
                        inXML.Append("<mute>0</mute>");
                        inXML.Append("<muteAll>1</muteAll>");
                    }
                    else
                    {
                        inXML.Append("<mute>1</mute>");
                        inXML.Append("<muteAll>1</muteAll>");
                    }
                }
                else if (fnmethod == "conference")
                {
                    if (ImgStatus == "1")
                    {
                        inXML.Append("<mute>0</mute>");
                        inXML.Append("<muteAll>2</muteAll>");
                    }
                    else
                    {
                        inXML.Append("<mute>1</mute>");
                        inXML.Append("<muteAll>2</muteAll>");
                    }
                }

                inXML.Append("<MuteIdentificationId>" + fnidentification + "</MuteIdentificationId>");
            }
            else if (fnidentification == "call")
            {
                if (fnmethod == "particpant")
                {
                    if (ImgStatus == "0")
                    {
                        inXML.Append("<connectOrDisconnect>0</connectOrDisconnect>");
                        inXML.Append("<connectOrDisconnectAll>0</connectOrDisconnectAll>");
                    }
                    else
                    {
                        inXML.Append("<connectOrDisconnect>1</connectOrDisconnect>");
                        inXML.Append("<connectOrDisconnectAll>0</connectOrDisconnectAll>");
                    }
                }
                else if (fnmethod == "conference")
                {
                    connectall = true;
                    if (ImgStatus == "0")
                    {
                        inXML.Append("<connectOrDisconnect>0</connectOrDisconnect>");
                        inXML.Append("<connectOrDisconnectAll>1</connectOrDisconnectAll>");
                    }
                    else
                    {
                        inXML.Append("<connectOrDisconnect>1</connectOrDisconnect>");
                        inXML.Append("<connectOrDisconnectAll>1</connectOrDisconnectAll>");
                    }
                }
            }
            inXML.Append("</login>");

            if (fnidentification == "audioTx" || fnidentification == "videoRx" || fnidentification == "videoTx" || fnidentification == "audioRx")
            {
                if (fnidentification == "audioRx")
                    outXML = obj.CallCOM2("MuteTransmitTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString()); //FB 3008
                else if (fnidentification == "audioTx")
                    outXML = obj.CallCOM2("MuteTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString()); //FB 3008
                else if (fnidentification == "videoTx")
                    outXML = obj.CallCOM2("MuteVideoTransmitTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                else if (fnidentification == "videoRx")
                    outXML = obj.CallCOM2("MuteVideoReceiveTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                }
                else
                {
                    outXML = obj.CallMyVRMServer("MonitorMuteTerminal", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        if (ImgStatus == "1")
                            res = "0";
                        else
                            res = "1";
                }
            }
            else if (fnidentification == "call")
            {
                outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                //Tamil Start
                if (!connectall)
                {
                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        if (ImgStatus == "1")
                            res = "0";
                        else
                            res = "1";
                }
                else
                    res = obj.GetTranslatedText("Operation Successful");
                //Tamil End
            }
        }
        catch (Exception ex)
        {
            log.Trace("fnAudioVideoStatus" + ex.Message);
        }
        return res;
    }
    #endregion

    #region Image Streaming

    [WebMethod]
    public static string loadstreamimage()
    {

        //MemoryStream ms = new MemoryStream(byteArrayIn);
        //Image returnImage = Image.FromStream(ms);
        //return returnImage;

        //string newPath = "/image/MonitorMCU/video.gif";
        //new FileStream("new.jpg", FileMode.Create);


        //System.IO.FileStream fs = new System.IO.FileStream("/image/MonitorMCU/video.gif", System.IO.FileMode.Open, System.IO.FileAccess.Read);
        //System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
        //Byte[] image = br.ReadBytes((int)fs.Length);

        //System.Drawing.Image img = System.Drawing.Image.FromFile("/image/MonitorMCU/video.gif");
        //System.IO.MemoryStream fs = new System.IO.MemoryStream(Convert.ToInt64(img));


        //System.IO.MemoryStream ms = new System.IO.MemoryStream();
        //var test = img.Save(ms, System.Drawing.img.ImageFormat.Jpeg);



        //System.IO.BinaryReader br = new System.IO.BinaryReader(test);

        // string imgstr = "0x4749463839613000240AEFB997468D3A3DF9EF6989870EBB3AF87C6161A10003B"; 
        //System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
        //Byte[] byteArray = br.ReadBytes((Int32)fs.Length);

        //System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(imgstr));
        //System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
        //return returnImage;




        //MemoryStream ms = new MemoryStream(Convert.FromBase64String(("0x47494638396130002400F700000000006E6EA9C0C0C027277BDDDDDDFF333300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000021F90405000006002C00000000300024000008CC0005081C48B0A0C1830703201CA83080C38710234A9C287180008A0E2D62DCC811A2468C1F3B8A9C1892E4C591283D9EA45832E5C8961161BAE428F361CD992C579AC4897240800140830ABDC933E642810D8B76244A5469469D15A13A8DBAB1A953A652A7AAAC9A55EB53AE5E73820D5BF1E7D0A15DBD5A3C9A94EC569069B562756B742CDDAF70EFDA8C6B55E95CBD3FF99A3D4BB8B0E1A0830B1FBDB85821DBC67C23DBDD99B7B258CB942F6BCECC992A66CF9B4177AEFB997468D3A3DF9EF6989870EBB3AF87C6161A10003B").ToString()));
        //System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
        //returnImage.Save(Server.MapPath("../image/MonitorMCU/test"+ms.ToString()+".jpg"));

        string img = "img/ManageTiersIcon.png";
        return img;


        //imageToByteArray(img);

        //byte[] b = ImageToByte(img);
        //ImageConverter converter = new ImageConverter();
        //var res = (byte[])converter.ConvertTo(img, typeof(byte[]));


        ////System.Drawing.Image img = System.Drawing.Image.FromFile("/image/MonitorMCU/video.gif");

        //System.IO.MemoryStream fs = new System.IO.MemoryStream(img);
        //System.IO.BinaryReader br = new System.IO.BinaryReader(fs);        
        //Byte[] bytes = br.ReadBytes((Int32)fs.Length);
        //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
        //var ImageUrl = "/image/MonitorMCU/video.gif;base64," + base64String;            
        //return ImageUrl; 
        // string res = "";
        //return res;
    }


    public byte[] imageToByteArray(System.Drawing.Image imageIn)
    {
        MemoryStream ms = new MemoryStream();
        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        return ms.ToArray();
    }
    //public Image byteArrayToImage(byte[] byteArrayIn)
    //{
    //    MemoryStream ms = new MemoryStream(byteArrayIn);
    //    Image returnImage = Image.FromStream(ms);
    //    return returnImage;
    //}

    #endregion
 
    #region Lock/UnLock Details
    /// <summary>
    /// fnslock
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="imgStatus"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnslock(string userID, string confID, string imgStatus, string MCUBridgeID)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<mcuId>" + MCUBridgeID + "</mcuId>");
            if (imgStatus == "1")
                inXML.Append("<lockorunlock>0</lockorunlock>");
            else
                inXML.Append("<lockorunlock>1</lockorunlock>");
            inXML.Append("</login>");
            
            outXML = obj.CallCOM2("LockTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
            {
                res = obj.GetTranslatedText("Operation UnSuccessful");
            }
            else
            {
                outXML = obj.CallMyVRMServer("LockTerminal", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                {
                    if (imgStatus == "1")
                        res = "0";
                    else
                        res = "1";
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("fnslock" + ex.Message);
        }
        return res;
    }
    #endregion


    // FB 2441
    #region fnsrecord
    /// <summary>
    /// fnsrecord
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="imgStatus"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnsrecord(string userID, string confID, string imgStatus, string MCUBridgeID)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<mcuId>" + MCUBridgeID + "</mcuId>");
            if (imgStatus == "1")
                inXML.Append("<confrecord>0</confrecord>");
            else
                inXML.Append("<confrecord>1</confrecord>");
            inXML.Append("</login>");

            outXML = obj.CallCOM2("ConferenceRecording", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
            {
                res = obj.GetTranslatedText("Operation UnSuccessful");
            }
            else
            {
                outXML = obj.CallMyVRMServer("LockTerminal", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                {
                    if (imgStatus == "1")
                        res = "0";
                    else
                        res = "1";
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("fnslock" + ex.Message);
        }
        return res;
    }
    #endregion
    
    #region Delete Command
    /// <summary>
    /// fndelete
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="fnmethod"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fndelete(string userID, string confID, string EndpointID, string terminalType, string fnmethod,string ImgStatus) //FB 2553
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");

                       
            if (fnmethod == "conference")
            {
                inXML.Append("<conferenceID>" + confID + "</conferenceID>");
            }
            else if (fnmethod == "particpant")
            {
                inXML.Append("<confID>" + confID + "</confID>");
                inXML.Append("<endpointID>" + EndpointID + "</endpointID>");
                inXML.Append("<terminalType>" + terminalType + "</terminalType>");
                inXML.Append("<connectOrDisconnect>0</connectOrDisconnect>");
                inXML.Append("<connectOrDisconnectAll>0</connectOrDisconnectAll>");
            }
            inXML.Append("<FromService>1</FromService>");//FB 2441
            inXML.Append("</login>");

            if (fnmethod == "conference")
            {
                outXML = obj.CallCOM2("TerminateConference", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                   return res = obj.GetTranslatedText("Operation UnSuccessful");
                }
                else
                {
                    outXML = obj.CallMyVRMServer("TerminateConference", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    //FB 3042 Starts
                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        res = obj.GetTranslatedText("Operation Successful");
                    //FB 3042 Ends
                }
            }
            else if (fnmethod == "particpant")
            {
                outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                   res = obj.GetTranslatedText("Operation UnSuccessful");
                }
                outXML = obj.CallCOM2("DeleteTerminal", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                   res = obj.GetTranslatedText("Operation UnSuccessful");
                }
                else
                {
                    outXML = obj.CallMyVRMServer("DeleteTerminal", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_Configpath"].ToString());
                    if ((outXML.IndexOf("<success>") >= 0))
                        res = obj.GetTranslatedText("Operation Successful");
                    else
                        res = obj.GetTranslatedText("Operation UnSuccessful"); //FB 3042

                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("fndelete" + ex.Message);
        }
        return res;
    }
    #endregion

    #region SetFavorite
    /// <summary>
    /// setFavorite
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="mcuid"></param>
    /// <param name="imgStatus"></param>
    /// <returns></returns>
    [WebMethod]
    public static string setFavorite(string userID, string mcuid, string imgStatus)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<bridgeId>" + mcuid + "</bridgeId>");
            inXML.Append("<favouriteMCU>" + imgStatus + "</favouriteMCU>");
            inXML.Append("</login>");
           
            outXML = obj.CallMyVRMServer("SetFavouriteMCU", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") > 0)
                return res = obj.GetTranslatedText("Operation UnSuccessful");
            else
                res = imgStatus;
        }
        catch (Exception ex)
        {
            log.Trace("setFavorite" + ex.Message);
        }
        return res;
    }
    #endregion

    //FB 2501 Dec7 Start

    //Method Changed for FB 2569

    #region GetEventLogs
    /// <summary>
    /// fnGetEventLogs
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnGetEventLogs(string userID, string confID)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            inXML.Append("<GetConferenceAlerts>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<ConferenceID>" + confID + "</ConferenceID>");
            inXML.Append("</GetConferenceAlerts>");

            outXML = obj.CallCOM2("GetConferenceAlerts", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") >= 0)
                res = obj.GetTranslatedText("Operation UnSuccessful");
            else
            {
                XDocument xd;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(outXML);
                xd = XDocument.Parse(xdoc.OuterXml);
                var recordCount = (from m in xd.Descendants("Alert") select m).Count();
                string[] ConfMsg = (from node in xd.Descendants("Alert") select node.Element("Message").Value).ToArray();
                string[] ConfDate = (from node in xd.Descendants("Alert") select node.Element("Timestamp").Value).ToArray();


                var htmlcontent = "";

                if (recordCount != 0)
                {
                    htmlcontent = "<table style='border-collapse:collapse; background-color:#F7EBF3;' border='1px solid red' width='98%'><tr><th width='10%'>S.NO</th><th width='30%'>Date&Time</th><th width='60%'>Message</th></tr></table>";
                    htmlcontent += "<table style='border-collapse:collapse;' border='1px solid red' width='98%'>";
                    for (int i = 0; i < recordCount; i++)
                    {
                        htmlcontent += "<tr><td align ='center' width='10%' >" + i + "</td><td align='center' width='30%'>" + ConfDate[i].ToString() + "</td><td align='left' style='padding-left:20px;' width='60%'>" + ConfMsg[i].ToString() + "</td></tr>";
                    }
                }
                else
                {
                    htmlcontent = "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='3' height='20px'></td></tr>";
                    htmlcontent += "<tr><td colspan='3' align='center'><b style='font-family:Verdana;' >" + obj.GetTranslatedText("No Events") + "</b><br><br><br><br></td></tr>";
                }
                htmlcontent += "<tr></table>";
                res = htmlcontent.ToString();
            }
        }
        catch (Exception ex)
        {
            log.Trace("PacketDetails" + ex.Message);
        }
        return res;
    }
    #endregion

    //FB 2501 Dec7 End


    // FB 2652 Starts
    #region MuteAllExceptSelectedParticipants
    /// <summary>
    /// MuteAllExceptSelectedParticipants
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="ParticipantsIdList"></param>
    /// <param name="ParticipantTerminalTypeList"></param>
    /// <returns></returns>
    [WebMethod]
    public static string MuteAllExceptSelectedParticipants(string userID, string confID, string ParticipantsIdList, string ParticipantTerminalTypeList)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        string[] PartyIdList = null;
        string[] PartyTypeList = null;

        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<mode>1</mode>"); //0-UnMuteAllParties 1-MuteAllPartiesExcept
            inXML.Append("<endpoints>");

            if (ParticipantsIdList.Length > 0)
                PartyIdList = ParticipantsIdList.Split(',');

            if(ParticipantTerminalTypeList.Length > 0)
                PartyTypeList = ParticipantTerminalTypeList.Split(',');

            for (int i = 0; i < PartyIdList.Length; i++)
            {
                inXML.Append("<endpoint>");
                inXML.Append("<endpointId>" + PartyIdList[i].ToString() + "</endpointId>");
                inXML.Append("<terminalType>" + PartyTypeList[i].ToString() + "</terminalType>");
                inXML.Append("</endpoint>");
            }
            inXML.Append("</endpoints>");
            inXML.Append("</login>");

            outXML = obj.CallCOM2("MuteUnMuteParties", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
                res = obj.ShowErrorMessage(outXML);
            else
            {
                outXML = obj.CallMyVRMServer("MuteUnMuteParticipants", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                res = obj.GetTranslatedText("Operation Successful!");
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
        return res;
    }
    #endregion   

    #region UnmuteAllParticipants
    /// <summary>
    /// UnmuteAllParticipants
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <returns></returns>
    [WebMethod]
    public static string UnmuteAllParticipants(string userID, string confID)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<mode>0</mode>"); //0-UnMuteAllParties 1-MuteAllPartiesExcept
            inXML.Append("</login>");

            outXML = obj.CallCommand("MuteUnMuteParties", inXML.ToString());
            if (outXML.IndexOf("<error>") >= 0)
                res = obj.ShowErrorMessage(outXML);
            else
            {
                outXML = obj.CallMyVRMServer("MuteUnMuteParticipants", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                res = obj.GetTranslatedText("Operation Successful!");
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
        return res;
    }
    #endregion   

    // FB 2652 Ends    

	//FB 2553-RMX Starts
    #region fnLectureMode
    /// <summary>
    /// fnLectureMode
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="fnmethod"></param>
    /// <param name="ImgStatus"></param>
    /// <returns></returns>
    [WebMethod]    
    public static string fnLectureMode(string userID, string confID, string EndpointID, string terminalType, string fnmethod,string ImgStatus)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");

            if (fnmethod == "conference") //No Conference Level for LecturerMode
            {
                inXML.Append("<conferenceID>" + confID + "</conferenceID>");
            }
            else if (fnmethod == "particpant")
            {
                inXML.Append("<confID>" + confID + "</confID>");
                inXML.Append("<endpointID>" + EndpointID + "</endpointID>");
                inXML.Append("<terminalType>" + terminalType + "</terminalType>");
                if (ImgStatus == "1")
                    inXML.Append("<isLecturer>0</isLecturer>");
                else
                    inXML.Append("<isLecturer>1</isLecturer>");
            }
            inXML.Append("</login>");

            if (fnmethod == "particpant")
            {
                outXML = obj.CallCOM2("SetLectureMode", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    return res = obj.GetTranslatedText("Operation UnSuccessful");
                }
                else
                {
                    outXML = obj.CallMyVRMServer("SetLectureParty", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        if (ImgStatus == "1")
                            res = "0";
                        else
                            res = "1";
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("fndelete" + ex.Message);
        }
        return res;
    }
    #endregion

    #region Party Leader
    /// <summary>
    /// fnPartyLeader
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="fnmethod"></param>
    /// <param name="ImgStatus"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnPartyLeader(string userID, string confID, string EndpointID, string terminalType, string fnmethod, string ImgStatus)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");

            if (fnmethod == "conference")
            {
                inXML.Append("<confID>" + confID + "</confID>");
            }
            else if (fnmethod == "particpant")
            {
                inXML.Append("<confID>" + confID + "</confID>");
                inXML.Append("<endpointID>" + EndpointID + "</endpointID>");
                inXML.Append("<terminalType>" + terminalType + "</terminalType>");
                if (ImgStatus == "1")
                    inXML.Append("<leaderParty>0</leaderParty>");
                else
                    inXML.Append("<leaderParty>1</leaderParty>");
            }

            inXML.Append("</login>");

            if (fnmethod == "particpant")
            {
                outXML = obj.CallCOM2("SetLeaderParty", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    return res = obj.GetTranslatedText("Operation UnSuccessful");
                }
                else
                {
                    outXML = obj.CallMyVRMServer("SetLeaderPerson", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        if (ImgStatus == "1")
                            res = "0";
                        else
                            res = "1";
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("fndelete" + ex.Message);
        }
        return res;
    }
    #endregion
    //FB 2553-RMX Ends

    //ZD 100113 Start

    #region FillLoginCookieforCodian
    /// <summary>
    /// 
    /// </summary>
    /// <param name="URI"></param>
    /// <param name="formParams"></param>
    /// <param name="mcuIPAddress"></param>
    private void FillLoginCookieforCodian(ref String URI, ref String formParams, ref String mcuIPAddress)
    {
        string cookieHeader = "";
        HttpWebRequest cookieReq = null;
        byte[] bytes = null;
        HttpWebResponse cookieResp = null;
        try
        {
            if (Session[mcuIPAddress] == null)
            {

                if (String.IsNullOrEmpty(URI))
                    return;

                if (String.IsNullOrEmpty(formParams))
                    return;

                if (String.IsNullOrEmpty(mcuIPAddress))
                    return;

                cookieReq = (HttpWebRequest)WebRequest.Create(URI);
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                cookieReq.ContentType = "application/x-www-form-urlencoded";
                cookieReq.Method = "POST";
                bytes = Encoding.ASCII.GetBytes(formParams);
                cookieReq.ContentLength = bytes.Length;
                using (Stream os = cookieReq.GetRequestStream())
                {
                    os.Write(bytes, 0, bytes.Length);
                }
                cookieReq.Timeout = 3000;
                cookieReq.AllowAutoRedirect = false;
                cookieResp = (HttpWebResponse)cookieReq.GetResponse();

                if (cookieResp.Headers["Set-cookie"] != null && cookieResp.Headers["Set-cookie"].Split('=').Length > 1)
                {
                    cookieHeader = cookieResp.Headers["Set-cookie"].Split('=')[1];
                }


                Session.Add(mcuIPAddress, cookieHeader);
            }

        }
        catch (Exception ex)
        {


        }
    }

    #endregion

    #region ReturnImageString
    /// <summary>
    /// 
    /// </summary>
    /// <param name="URI"></param>
    /// <param name="mcuIPAddress"></param>
    /// <returns></returns>

    private String ReturnImageString(ref String URI, ref String mcuIPAddress)
    {

        HttpWebRequest imageReq = null;
        CookieContainer cookiecollection = null;
        HttpWebResponse imageResp = null;
        String sRet = "";
        byte[] bytArray = new byte[2048];
        Stream imgStream = null;
        int length = 0;
        MemoryStream mStream = null;
        String b64Header = "data:image/jpeg;base64,";
        try
        {
            if (Session[mcuIPAddress] != null)
            {

                if (String.IsNullOrEmpty(URI))
                    return sRet;

                if (String.IsNullOrEmpty(Session[mcuIPAddress].ToString()))
                    return sRet;
                imageReq = (HttpWebRequest)WebRequest.Create(URI);

                cookiecollection = new CookieContainer();
                cookiecollection.Add(new Cookie("cookie", Session[mcuIPAddress].ToString(), "/", mcuIPAddress));
                imageReq.CookieContainer = cookiecollection;
                
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                imageReq.ContentType = "application/x-www-form-urlencoded";
                imageReq.Method = "GET";
                imageReq.Timeout = 3000;
                imageResp = (HttpWebResponse)imageReq.GetResponse();

                imgStream = imageResp.GetResponseStream();
                bytArray = new byte[imageResp.ContentLength];
                length = imgStream.Read(bytArray, 0, bytArray.Length);
                if (length > 0)
                {
                    mStream = new MemoryStream();
                    mStream.Write(bytArray, 0, bytArray.Length);
                }

                if (bytArray.Length > 0)
                    sRet = b64Header + Convert.ToBase64String(bytArray);
            }



        }
        catch (Exception ex)
        {


        }
        finally
        {
            if (imgStream != null)
            {
                imgStream.Close();
                imgStream.Dispose();
            }

            if (mStream != null)
            {
                mStream.Close();
                mStream.Dispose();
            }

        }
        return sRet;
    }

    #endregion

    //ZD 100113 End

}
#endregion
