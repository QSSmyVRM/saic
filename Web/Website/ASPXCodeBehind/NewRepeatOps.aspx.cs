﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Threading;
using System.Xml;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;//GP


public partial class en_NewRepeatOps : System.Web.UI.Page
{

    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPath;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnThrds;

    private myVRMNet.NETFunctions obj;
    private ns_Logger.Logger log;
    private ns_InXML.InXML objInXML;
    private DataTable _dtConfEPTs = null;
    Int32 _tCnt = 0;
    Int32 _tsuccess = 0;//GP
    Int32 _tfailure = 0;//GP        
    private Thread _thrd = null;
    DateTime strtTime = DateTime.MinValue;//GP Time Issue
    int eptThStarted = 0;
    int eptThCompleted = 0;
    bool isCallLaunched = false;
    ArrayList launchedConfs = new ArrayList(); //PSU Fix

    protected void Page_Load(object sender, EventArgs e)
    {
        if (obj == null)
            obj = new myVRMNet.NETFunctions();
        obj.AccessandURLConformityCheck("NewRepeatOps.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
        objInXML = new ns_InXML.InXML();
        //logService = new NS_LOGGER(); //GP Time Issue
        try
        {
            if (strtTime == DateTime.MinValue) //GP Time ISsue
                strtTime = DateTime.Now;//Code added for Gp timeout

            if (hdnThrds.Value.Length < 1)
                hdnThrds.Value = "100";

            obj.CallCommand("TerminateCompletedP2PConfs", "<Admin>11</Admin>");
            Getconferences();//GP Time Issue

            _thrd = new Thread(new ThreadStart(MiscThread));
            _thrd.SetApartmentState(ApartmentState.STA);
            _thrd.Start();

            //GP START
            String strTrd = "";
            String strTrdStatus = "";
            //GP START

            strTrd = "Thread Name: GetLastMailRunDate or SendEmails" + _thrd.Name + "\r\nThread Description: Mail Details" + "\r\nStart Time: " + DateTime.Now.ToString("F");
            if (_thrd != null)
            {
                if (_thrd.IsAlive)
                {
                    _tsuccess++;
                    strTrdStatus = "Successful completion";
                    WriteToLogFile(strTrd + "\r\nEnd Time: " + DateTime.Now.ToString("F") + "\r\nStatus: " + strTrdStatus + "\r\n-----");
                }

                else
                {
                    _tfailure++;
                    strTrdStatus = "Timed Out";
                    WriteToLogFile(strTrd + "\r\nEnd Time: " + DateTime.Now.ToString("F") + "\r\nStatus: " + strTrdStatus + "\r\n-----");
                }
            }

            //Second sub thread to launch calls
            LaunchOngoingCalls(); //GP issue

            //third sub thread Update endpoint status
            UpdateEptStatus();

            //Following block is to loop the updateconferene status threads
            //Code added for Gp timeout
            DateTime nowDate = DateTime.Now;
            TimeSpan span = nowDate.Subtract(strtTime);
            double secsDiff = span.TotalSeconds;

            while (secsDiff > 20)
            {
                nowDate = DateTime.Now;
                span = nowDate.Subtract(strtTime);
                secsDiff = span.TotalSeconds;

                if (eptThStarted == eptThCompleted)
                {
                    eptThStarted = 0;
                    eptThCompleted = 0;

                    Getconferences();

                    if (!isCallLaunched)
                        LaunchOngoingCalls();

                    UpdateEptStatus();
                }
            }
            //GP END

            #region ThreadPool
            //GetConfs1();
            //ThreadPool.QueueUserWorkItem(new WaitCallback(MiscThread));

            //if (_dtConfEPTs != null)
            //{
            //    if (_dtConfEPTs.Rows.Count > 0)
            //        ThreadPool.SetMaxThreads(_dtConfEPTs.Rows.Count,100);
            //    foreach (DataRow dr in _dtConfEPTs.Rows)
            //    {
            //        _confid = dr["confid"].ToString();
            //        _confType = dr["ConfType"].ToString();
            //        _endpointID = dr["EndpointID"].ToString();
            //        _eptTerminaltype = dr["EPTTerminaltype"].ToString();
            //        if (dr["rowType"].ToString().Equals("L"))
            //            ThreadPool.QueueUserWorkItem(new WaitCallback(LaunchOngoing));
            //        else if (dr["rowType"].ToString().Equals("M"))
            //            ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateConfStatus));
            //    }
            //}
            //Thread.Sleep(TimeSpan.FromSeconds(10));
            //ThreadPool.GetAvailableThreads(out _workerThrds, out _portedThrds);
            //while (_workerThrds > 1)
            //{
            //    ThreadPool.GetAvailableThreads(out _workerThrds, out _portedThrds);
            //}
            #endregion

            if (_thrd != null)
                while (_thrd.IsAlive)
                    _thrd.Join();//To Terminate current thread

            //GP START
            if (_thrd != null)
            {
                if (_tCnt > 0 && (!_thrd.IsAlive))
                {
                    WriteToLogFile("Summary:" + "\r\nTotal threads started  in cycle: " + _tCnt + "\r\nSuccessful completion: " + _tsuccess + "\r\nTimed out: " + _tfailure + "\r\n-----");
                    _tCnt = 0;
                    _tsuccess = 0;
                    _tfailure = 0;
                }
            }
            //GP END

            //Response.Write("Success....");
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
        finally //PSU Fix
        {
            launchedConfs = null;
        }
    }

    #region Testing
    private void GetConfs1()
    {
        try
        {
            //_arrOngoingConfs = new ArrayList();
            for (int i = 1; i <= 1000; i++)
            {
                //_rptOPS = new RepeatOPS();
                //_rptOPS.Confid = i.ToString();
                //_rptOPS.ConfType = "2";
                //_rptOPS.RowType = "L";
                //_rptOPS.EptTerminaltype = "";
                //_rptOPS.EndpointID = "";
                //_objConfList.Enqueue(_rptOPS);
                DataRow dr = _dtConfEPTs.NewRow();
                dr["confid"] = i.ToString();
                dr["ConfType"] = "2";
                dr["endpointID"] = "";
                dr["EPTTerminaltype"] = "";
                dr["rowType"] = "L";
                _dtConfEPTs.Rows.Add(dr);

                dr = _dtConfEPTs.NewRow();
                dr["confid"] = i.ToString();
                dr["ConfType"] = "4";
                dr["endpointID"] = i.ToString();
                dr["EPTTerminaltype"] = "2";
                dr["rowType"] = "M";
                _dtConfEPTs.Rows.Add(dr);
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
    }
    #endregion

    #region Commented
    /*
     //private void LaunchOngoing(Object stateInfo)
    private void LaunchOngoing(object rptOPS)
    {
        try
        {
            //Push conferences to MCU
            //Use _confid to send the command
            RepeatOPS rpt1 = (RepeatOPS)rptOPS;

            #region Code Testing 
            //Response.Write("In Thread " + _tCnt.ToString() + " = LaunchOutGoing of call " + _confid.ToString() + " - Start " + DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss:fff") + " <br>");
            //int i = 3000;
            //while (i > 0)
            //{
            //    int j = 1000;
            //    while (j > 0)
            //        j--;
            //    i--;
            //}
            //Response.Write("In Thread " + _tCnt.ToString() + " = LaunchOutGoing of call " + _confid.ToString() + " - End " + DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss:fff") + " <br><br>");
            #endregion 

            string inXML = "";
            if (rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()) || rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()))
            {
                inXML = "<Conference><confID>" + rpt1.Confid.ToString() + "</confID></Conference>";
                string outXML = "";
                outXML = obj.CallCommand("SetConferenceOnMcu", inXML);
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
    }
 
     * */
    #endregion

    #region LaunchOngoing Threads

    private void LaunchOngoingCalls()
    {
        DataTable dtOngoing = null;
        try
        {
            if (_dtConfEPTs != null)
            {
                dtOngoing = _dtConfEPTs.Copy();

                DataRow[] drOngoing = dtOngoing.Select("rowType = 'L'");

                foreach (DataRow dr in drOngoing)
                {
                    RepeatOPS rpt1 = new RepeatOPS();

                    rpt1.Confid = dr["confid"].ToString();
                    rpt1.ConfType = dr["ConfType"].ToString();
                    rpt1.EndpointID = dr["EndpointID"].ToString();
                    rpt1.EptTerminaltype = dr["EPTTerminaltype"].ToString();
                    rpt1.RowType = dr["rowType"].ToString();
                    //GP START
                    rpt1.ConfUniqueID = dr["confUniqueID"].ToString();
                    rpt1.ConferenceName = dr["confName"].ToString();
                    rpt1.ConfEndPointName = dr["endpointname"].ToString();

                    if (rpt1.ConfUniqueID != "")
                    {
                        if (rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()) || rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()))
                        {
                            isCallLaunched = true;  //GP Issue

                            if (!launchedConfs.Contains(rpt1.Confid.ToString().Trim())) //PSU FIX
                            {
                                launchedConfs.Add(rpt1.Confid.ToString().Trim()); //PSU FIX

                                _thrd = new Thread(this.LaunchOngoing);
                                _thrd.SetApartmentState(ApartmentState.MTA);
                                _thrd.Start(rpt1);

                                _tCnt++;

                                if (_tCnt.ToString().Equals(hdnThrds.Value))
                                {
                                    _tfailure++;//GP
                                    break;
                                }
                            }
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
    }

    private void LaunchOngoing(object rptOPS)
    {
        string strTrd = "";
        string strTrdStatus = "";
        try
        {
            RepeatOPS rpt1 = (RepeatOPS)rptOPS;

            Thread currTd = Thread.CurrentThread;

            strTrd = "Thread Name: SetConferenceOnMcu" + "\r\nThread Description: Launch OutGoing of call" + "\r\n Conference Name: "+ rpt1.ConferenceName + "\r\nStart Time: " + DateTime.Now.ToString("F");// +"Status: " + _thrd.ThreadState;

            //Push conferences to MCU
            //Use _confid to send the command

            string inXML = "";
            //if (rptOPS.ConfType.Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()) || rptOPS.ConfType.Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()))
            //{
            inXML = "<Conference><confID>" + rpt1.Confid.ToString() + "</confID></Conference>";
            string outXML = "";
            outXML = obj.CallCommand("SetConferenceOnMcu", inXML);
            //}

            if (currTd != null)
            {
                if (currTd.IsAlive)
                {
                    strTrdStatus = "Successful completion";
                    _tsuccess++;
                }
                else
                {
                    _tfailure++;
                    strTrdStatus = "Timed Out";
                }
            }
            WriteToLogFile(strTrd + "\r\nEnd Time:" + DateTime.Now.ToString("F") + "\r\nStatus: " + strTrdStatus + "\r\n-----");
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
    }

    #endregion

    #region MiscThread
    //private void MiscThread(Object stateInfo)
    private void MiscThread()
    {
        try
        {
            String outXML = "";

            ns_SuperAdministrator.SuperAdministrator objActv = new ns_SuperAdministrator.SuperAdministrator();
            objActv.GetActivation(hdnPath.Value);

            en_EventLog objLog = new en_EventLog();
            objLog.PurgeConferenceRegular();

            String mailInXML = "<GetMailRunDate><UserID>11</UserID></GetMailRunDate>";
            String mailOutXML = obj.CallCommand("GetLastMailRunDate", mailInXML);
            if (IsMailRunnable(mailOutXML))
                outXML = obj.CallCommand("SendEmails", "");

            _tCnt++;
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
    }

    private Boolean IsMailRunnable(String inXML)
    {
        try
        {
            if (inXML.IndexOf("<error>") > 0)
                return false;

            if (inXML.IndexOf("<LastRunDate>") > 0)
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inXML);
                XmlNode node;
                node = xmldoc.SelectSingleNode("//GetMailRunDate/LastRunDate");
                String lstrun = node.InnerXml.Trim();
                TimeSpan diffTime = new TimeSpan();
                if (lstrun.Length > 0)
                    diffTime = DateTime.UtcNow.Subtract(Convert.ToDateTime(lstrun));

                if (diffTime.TotalSeconds > 0)
                {
                    if (diffTime.TotalSeconds > 29)
                        return true;
                    else if (diffTime.TotalSeconds < 30 && diffTime.TotalSeconds < 120)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return false;
        }
        catch (Exception ex)
        {
            return false;
            log.Trace(ex.Message);
        }
    }
    #endregion

    #region Commented
    //private void UpdateConfStatus(Object stateInfo)
    /*private void UpdateConfStatus(object rptOPS)
    {
        String inXML = "";
        String outXML = "";
        try
        {
            RepeatOPS rpt1 = (RepeatOPS)rptOPS;
           
            if (rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()) || rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()))
            {
                inXML += "<GetTerminalStatus>";
                inXML += "  <login>";
                inXML += "      <userID>11</userID>";
                inXML += "      <confID>" + rpt1.Confid.ToString() + "</confID>";
                inXML += "      <endpointID>" + rpt1.EndpointID.ToString() + "</endpointID>";
                inXML += "      <terminalType>" + rpt1.EptTerminaltype.ToString() + "</terminalType>";
                inXML += "  </login>";
                inXML += "</GetTerminalStatus>";
                outXML = obj.CallCommand("GetTerminalStatus", inXML);
            }
        }
        catch (Exception e)
        {
            log.Trace(e.Message);
        }
    }*/
    #endregion

    #region UpdateConfStatus Thread

    //Fetched endpoint status for conference even before call launching
    private void UpdateEptStatus()
    {
        //DataTable dtUpdate = null;
        try
        {
            if (_dtConfEPTs != null)
            {
                //dtUpdate = _dtConfEPTs.Copy();
                //DataRow[] drUpdate = dtUpdate.Select("rowType = 'M'");

                foreach (DataRow dr in _dtConfEPTs.Rows)
                {

                    RepeatOPS rpt1 = new RepeatOPS();

                    rpt1.Confid = dr["confid"].ToString();
                    rpt1.ConfType = dr["ConfType"].ToString();
                    rpt1.EndpointID = dr["EndpointID"].ToString();
                    rpt1.EptTerminaltype = dr["EPTTerminaltype"].ToString();
                    rpt1.RowType = dr["rowType"].ToString();
                    //GP START
                    rpt1.ConfUniqueID = dr["confUniqueID"].ToString();
                    rpt1.ConferenceName = dr["confName"].ToString();
                    rpt1.ConfEndPointName = dr["endpointname"].ToString();
                    
                    if (rpt1.EptTerminaltype != "" && rpt1.EndpointID != "")
                    {
                        if (rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()) || rpt1.ConfType.Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()))
                        {
                            _thrd = new Thread(this.UpdateConfStatus);
                            _thrd.SetApartmentState(ApartmentState.MTA);
                            _thrd.Start(rpt1);

                            //GP END
                            _tCnt++;

                            if (_tCnt.ToString().Equals(hdnThrds.Value))
                            {
                                _tfailure++;//GP
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.Trace(e.Message);
        }
    }

    private void UpdateConfStatus(object rptOPS1)
    {
        String inXML = "";
        String outXML = "";
        string strTrd2 = "";
        string strTrdStatus = "";
        try
        {
            RepeatOPS rptOPS = (RepeatOPS)rptOPS1;

            Thread curThd = Thread.CurrentThread;
            strTrd2 = "Thread Name: UpdateConfStatus" + "\r\nThread Description: Conf Name# " + rptOPS.ConferenceName + " ; Unique ID# " + rptOPS.ConfUniqueID + "; Endpoint Name# " + rptOPS.ConfEndPointName + "; Endpoint ID# " + rptOPS.EndpointID + "\r\nStart Time: " + DateTime.Now.ToString("F");

            eptThStarted++;

            if (rptOPS.ConfType.Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()) || rptOPS.ConfType.Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()))
            {
                inXML += "<GetTerminalStatus>";
                inXML += "  <login>";
                inXML += "      <userID>11</userID>";
                inXML += "      <confID>" + rptOPS.Confid.ToString() + "</confID>";
                inXML += "      <endpointID>" + rptOPS.EndpointID.ToString() + "</endpointID>";
                inXML += "      <terminalType>" + rptOPS.EptTerminaltype.ToString() + "</terminalType>";
                inXML += "  </login>";
                inXML += "</GetTerminalStatus>";
                outXML = obj.CallCommand("GetTerminalStatus", inXML);
            }

            if (curThd != null)
            {
                if (curThd.IsAlive)
                {
                    _tsuccess++;
                    strTrdStatus = "Successful completion";
                    eptThCompleted++;
                }
                else
                {
                    _tfailure++;
                    strTrdStatus = "Timed Out";
                    eptThCompleted++;
                }
            }
            WriteToLogFile(strTrd2 + "\r\nEnd Time:" + DateTime.Now.ToString("F") + "\r\nStatus: " + strTrdStatus + "\r\n-----");
        }
        catch (Exception e)
        {
            log.Trace(e.Message);
            eptThCompleted++;
        }
    }
    #endregion

    #region GetConfs
    private void GetConfs(String pinXML)
    {
        String inXML = "";
        String outXML = "";
        try
        {
            if (pinXML.IndexOf("<error>") > 0)
                return;
            if (_dtConfEPTs == null)
                CreateEPTDataTable();
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(pinXML);
            XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
            foreach (XmlNode node in nodes)
            {
                XmlNode nodeConf = node.SelectSingleNode("ConferenceID");
                String ConfInstID = nodeConf.InnerXml.Trim();

                string confUID = node.SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
                string confname = node.SelectSingleNode("ConferenceName").InnerText.Trim();

                XmlNode nodeConftype = node.SelectSingleNode("ConferenceType");
                Int32 ConferenceType = Int32.Parse(nodeConftype.InnerXml.Trim());
                XmlNode nodeStatus = node.SelectSingleNode("ConferenceActualStatus");
                String ConferenceStatus = nodeStatus.InnerXml.Trim();
                XmlNode nodeLastRun = node.SelectSingleNode("LastRunDate");
                String lstrun = nodeLastRun.InnerXml.Trim();

                if (ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()) || ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()))
                {
                    TimeSpan diffTime = new TimeSpan();
                    if (lstrun.Length > 0)
                        diffTime = DateTime.UtcNow.Subtract(Convert.ToDateTime(lstrun));

                    //Commented as requested to fetch endpoint status repeatedly - GP Issue
                    //Get Status  
                    //if (diffTime.TotalSeconds > 29)
                    //{
                    //Multi-Point Conf Status
                    //if (ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()) || ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()))
                    //{

                    inXML = "<login><userID>11</userID><confID>" + ConfInstID + "</confID></login>";
                    outXML = obj.CallCommand("GetTerminalControl", inXML);
                    XmlDocument xmldocEP = new XmlDocument();
                    xmldocEP.LoadXml(outXML);

                    //FB 1785 Start
                    XmlNodeList iCALNodes = xmldocEP.SelectNodes("descendant::terminal[isCalendarInvite='1']");
                    if (iCALNodes != null)
                        if (iCALNodes.Count > 0)
                            continue;
                    
                    //FB 1785 End

                    XmlNodeList nodesEP = xmldocEP.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                    foreach (XmlNode nodeEP in nodesEP)
                    {
                        DataRow dr = _dtConfEPTs.NewRow();

                        //Commented as requested to fetch endpoint status repeatedly - GP Issue
                        //XmlNode nodeLastRunEP = nodeEP.SelectSingleNode("LastRunDate");
                        //String lstrunEP = nodeLastRunEP.InnerXml.Trim();
                        //TimeSpan diffTimeEP = new TimeSpan();
                        //if (lstrunEP.Length > 0)
                        //    diffTimeEP = DateTime.UtcNow.Subtract(Convert.ToDateTime(lstrunEP));
                        //if (diffTimeEP.TotalSeconds > 0)
                        //    if (diffTimeEP.TotalSeconds < 29)
                        //        continue;
                        dr["confid"] = ConfInstID;
                        dr["ConfType"] = ConferenceType.ToString();
                        dr["confUniqueID"] = confUID;
                        dr["confName"] = confname;

                        if (ConferenceStatus == "0")
                        {
                            //Launch the call for Ongoing conferences
                            dr["rowType"] = "L";
                        }
                        else
                        {
                            //Fetch Endpoint status - now this is not used - GP Issue
                            dr["rowType"] = "M";
                        }

                        dr["endpointname"] = nodeEP.SelectSingleNode("name").InnerText.Trim();
                        dr["endpointID"] = nodeEP.SelectSingleNode("endpointID").InnerText.Trim();
                        dr["EPTTerminaltype"] = nodeEP.SelectSingleNode("type").InnerText.Trim();

                        _dtConfEPTs.Rows.Add(dr);
                    }
                }
                else
                {
                    DataRow dr = _dtConfEPTs.NewRow();

                    dr["confid"] = ConfInstID;
                    dr["ConfType"] = ConferenceType.ToString();
                    dr["confUniqueID"] = confUID;
                    dr["confName"] = confname;
                    dr["rowType"] = "M";
                    dr["endpointname"] = "";
                    dr["endpointID"] = "";
                    dr["EPTTerminaltype"] = "";

                    _dtConfEPTs.Rows.Add(dr);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
        }
    }
    #region Commented - GP Issue
    //private void GetConfs(String pinXML)
    //{
    //    String inXML = "";
    //    String outXML = "";
    //    try
    //    {
    //        if (pinXML.IndexOf("<error>") > 0)
    //            return;
    //        if (_dtConfEPTs == null)
    //            CreateEPTDataTable();
    //        XmlDocument xmldoc = new XmlDocument();
    //        xmldoc.LoadXml(pinXML);
    //        XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
    //        foreach (XmlNode node in nodes)
    //        {
    //            XmlNode nodeConf = node.SelectSingleNode("ConferenceID");
    //            String ConfInstID = nodeConf.InnerXml.Trim();

    //            string confUID = node.SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
    //            string confname = node.SelectSingleNode("ConferenceName").InnerText.Trim();

    //            XmlNode nodeConftype = node.SelectSingleNode("ConferenceType");
    //            Int32 ConferenceType = Int32.Parse(nodeConftype.InnerXml.Trim());
    //            XmlNode nodeStatus = node.SelectSingleNode("ConferenceActualStatus");
    //            String ConferenceStatus = nodeStatus.InnerXml.Trim();
    //            XmlNode nodeLastRun = node.SelectSingleNode("LastRunDate");
    //            String lstrun = nodeLastRun.InnerXml.Trim();
    //            if (ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()) || ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()))
    //            {
    //                TimeSpan diffTime = new TimeSpan();
    //                if (lstrun.Length > 0)
    //                    diffTime = DateTime.UtcNow.Subtract(Convert.ToDateTime(lstrun));
    //                if (ConferenceStatus == "0")
    //                {
    //                    //Launch the call for Ongoing conferences
    //                    DataRow dr = _dtConfEPTs.NewRow();
    //                    dr["confid"] = ConfInstID;
    //                    dr["ConfType"] = ConferenceType.ToString();
    //                    dr["rowType"] = "L";

    //                    //GP START
    //                    dr["confUniqueID"] = confUID;
    //                    dr["confName"] = confname;
    //                    dr["endpointname"] = "";
    //                    //GP End

    //                    _dtConfEPTs.Rows.Add(dr);
    //                }
    //                else
    //                {
    //                    //Get Status 
    //                    if (diffTime.TotalSeconds > 29)
    //                    {
    //                        //Multi-Point Conf Status
    //                        if (ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.AudioVideo.ToString()) || ConferenceType.ToString().Equals(ns_MyVRMNet.vrmConfType.P2P.ToString()))
    //                        {
    //                            inXML = "<login><userID>11</userID><confID>" + ConfInstID + "</confID></login>";
    //                            outXML = obj.CallCommand("GetTerminalControl", inXML);
    //                            XmlDocument xmldocEP = new XmlDocument();
    //                            xmldocEP.LoadXml(outXML);
    //                            XmlNodeList nodesEP = xmldocEP.SelectNodes("//terminalControl/confInfo/terminals/terminal");
    //                            foreach (XmlNode nodeEP in nodesEP)
    //                            {
    //                                DataRow dr = _dtConfEPTs.NewRow();
    //                                dr["confid"] = ConfInstID;
    //                                dr["ConfType"] = ConferenceType.ToString();
    //                                XmlNode nodeLastRunEP = nodeEP.SelectSingleNode("LastRunDate");
    //                                String lstrunEP = nodeLastRunEP.InnerXml.Trim();
    //                                TimeSpan diffTimeEP = new TimeSpan();
    //                                if (lstrunEP.Length > 0)
    //                                    diffTimeEP = DateTime.UtcNow.Subtract(Convert.ToDateTime(lstrunEP));
    //                                if (diffTimeEP.TotalSeconds > 0)
    //                                    if (diffTimeEP.TotalSeconds < 29)
    //                                        continue;

    //                                dr["endpointID"] = nodeEP.SelectSingleNode("endpointID").InnerText.Trim();
    //                                dr["EPTTerminaltype"] = nodeEP.SelectSingleNode("type").InnerText.Trim();
    //                                dr["rowType"] = "M";
    //                                //GP START
    //                                dr["confUniqueID"] = xmldocEP.SelectSingleNode("//terminalControl/confInfo/confUniqueID").InnerText.Trim();
    //                                dr["confName"] = xmldocEP.SelectSingleNode("//terminalControl/confInfo/confName").InnerText.Trim();
    //                                dr["endpointname"] = nodeEP.SelectSingleNode("name").InnerText.Trim();
    //                                //GP End
    //                                _dtConfEPTs.Rows.Add(dr);
    //                            }

    //                        }
    //                        else
    //                        {
    //                            DataRow dr = _dtConfEPTs.NewRow();
    //                            dr["confid"] = ConfInstID;
    //                            dr["ConfType"] = ConferenceType.ToString();
    //                            dr["rowType"] = "M";
    //                            _dtConfEPTs.Rows.Add(dr);
    //                        }
    //                    }

    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        log.Trace(ex.Message);
    //    }
    //}
    #endregion
    #endregion

    #region CreateEPTDataTable
    private void CreateEPTDataTable()
    {
        if (_dtConfEPTs == null)
        {
            _dtConfEPTs = new DataTable();
            _dtConfEPTs.Columns.Add("confid");
            _dtConfEPTs.Columns.Add("ConfType");
            _dtConfEPTs.Columns.Add("rowType");
            _dtConfEPTs.Columns.Add("EndpointID");
            _dtConfEPTs.Columns.Add("EPTTerminaltype");
            //GP START
            _dtConfEPTs.Columns.Add("confUniqueID");
            _dtConfEPTs.Columns.Add("confName");
            _dtConfEPTs.Columns.Add("endpointname");
            //GP START
        }
        else
        {
            if (!_dtConfEPTs.Columns.Contains("confid"))
                _dtConfEPTs.Columns.Add("confid");
            if (!_dtConfEPTs.Columns.Contains("rowType"))
                _dtConfEPTs.Columns.Add("rowType");
            if (!_dtConfEPTs.Columns.Contains("ConfType"))
                _dtConfEPTs.Columns.Add("ConfType");
            if (!_dtConfEPTs.Columns.Contains("EndpointID"))
                _dtConfEPTs.Columns.Add("EndpointID");
            if (!_dtConfEPTs.Columns.Contains("EPTTerminaltype"))
                _dtConfEPTs.Columns.Add("EPTTerminaltype");
            //GP START
            if (!_dtConfEPTs.Columns.Contains("confUniqueID"))
                _dtConfEPTs.Columns.Add("confUniqueID");
            if (!_dtConfEPTs.Columns.Contains("confName"))
                _dtConfEPTs.Columns.Add("confName");
            if (!_dtConfEPTs.Columns.Contains("endpointname"))
                _dtConfEPTs.Columns.Add("endpointname");
            //GP End
        }
    }
    #endregion

    //GP START
    #region WriteToLogFile
    private void WriteToLogFile(string logRecord)
    {

        Console.WriteLine(logRecord);
        StreamWriter sw;
        String logFilePath = @"C:\VRMSchemas_v1.8.3\VRMMaintServiceLog.log";

        try
        {
            if (!File.Exists(logFilePath))
            {
                // file doesnot exist . hence, create a new log file.
                sw = File.CreateText(logFilePath);
                sw.Flush();
                sw.Close();
            }
            else
            {
                // check if exisiting log file size is greater than 50 MB
                FileInfo fi = new FileInfo(logFilePath);
                if (fi.Length > 50000000)
                {
                    // delete the log file						
                    File.Delete(logFilePath);

                    // create a new log file 
                    sw = File.CreateText(logFilePath);
                    sw.Flush();
                    sw.Close();
                }
            }

            // write the log record.
            sw = File.AppendText(logFilePath);
            sw.WriteLine(logRecord);
            sw.Flush();
            sw.Close();
        }
        catch (Exception)
        {
            // do nothing
        }
    }
    #endregion

    #region Get Conferences
    private void Getconferences()
    {

        try
        {
            _dtConfEPTs = new DataTable();

            CreateEPTDataTable();

            //String SearchType = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.OnMCU + "," + ns_MyVRMNet.vrmConfStatus.Ongoing;
            //String searchConfInXML = objInXML.SearchConference("11", SearchType, "", "", "", "1", "", "", "", "", "", "1", "", "1", "3", "0", "");
            String searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/>";
            searchConfInXML += "<StatusFilter><ConferenceStatus>0</ConferenceStatus><ConferenceStatus>6</ConferenceStatus><ConferenceStatus>5</ConferenceStatus></StatusFilter>";
            searchConfInXML += "<ConferenceName/><ConferenceSearchType>1</ConferenceSearchType><DateFrom/><DateTo/><ConferenceHost/><ConferenceParticipant/><Public/>";
            searchConfInXML += "<ApprovalPending>0</ApprovalPending><RecurrenceStyle>0</RecurrenceStyle>";
            searchConfInXML += "<Location><SelectionType>1</SelectionType><SelectedRooms/></Location>";
            searchConfInXML += "<PageNo>1</PageNo><SortBy>3</SortBy></SearchConference>";
            String searchConfOutXML = obj.CallCommand("SearchConference", searchConfInXML);
            GetConfs(searchConfOutXML);
        }
        catch (Exception ex)
        { }
    }
    #endregion
    //GP END
}
public class RepeatOPS
{
    private String _confid = "";
    private String _endpointID = "";
    private String _eptTerminaltype = "";
    private String _confType = "";
    private String _rowType = "";
    //GP START
    private String _confUniqueID = "";
    private String _confName = "";
    private String _endpointname = "";
    //GP END

    public RepeatOPS()
    {
    }

    public String Confid { get { return _confid; } set { _confid = value; } }
    public String EndpointID { get { return _endpointID; } set { _endpointID = value; } }
    public String EptTerminaltype { get { return _eptTerminaltype; } set { _eptTerminaltype = value; } }
    public String ConfType { get { return _confType; } set { _confType = value; } }
    public String RowType { get { return _rowType; } set { _rowType = value; } }
    //GP START
    public String ConfUniqueID { get { return _confUniqueID; } set { _confUniqueID = value; } }
    public String ConferenceName { get { return _confName; } set { _confName = value; } }
    public String ConfEndPointName { get { return _endpointname; } set { _endpointname = value; } }
    //GP END
}
