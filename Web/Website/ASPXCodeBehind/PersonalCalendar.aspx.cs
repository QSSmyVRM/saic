/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
/** FB 1850 Both aspx and cs were restructured for performance **/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using DevExpress.Web.ASPxTabControl;

public partial class PersonalCalendar : System.Web.UI.Page
{


    protected System.Web.UI.HtmlControls.HtmlGenericControl Html1;
    protected System.Web.UI.HtmlControls.HtmlHead Head1;
    protected System.Web.UI.HtmlControls.HtmlForm frmPersonalCalendar;
    protected System.Web.UI.ScriptManager CalendarScriptManager;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsMonthChanged;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsSettingsChange;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsWeekChanged;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsWeekOverLap;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnMonthlyXml;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Label CalenMenu;
    protected System.Web.UI.UpdatePanel PanelTab;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnXml;
    protected System.Web.UI.WebControls.CheckBox officehrDaily;
    protected System.Web.UI.WebControls.CheckBox officehrWeek;
    protected System.Web.UI.WebControls.CheckBox officehrMonth;
    protected System.Web.UI.WebControls.Button btnDate;
    protected System.Web.UI.WebControls.TreeView treeRoomSelection;
    protected System.Web.UI.WebControls.TextBox txtType;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer2;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer3;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer4;
    protected System.Web.UI.HtmlControls.HtmlTableCell ActionsTD;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p;
    protected System.Web.UI.HtmlControls.HtmlSelect lstCalendar;//FB 2585

    protected DayPilot.Web.Ui.DayPilotCalendar schDaypilot;
    protected DayPilot.Web.Ui.DayPilotMonth schDaypilotMonth;
    protected DayPilot.Web.Ui.DayPilotCalendar schDaypilotweek;
    protected DayPilot.Web.Ui.DayPilotBubble Details;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsMonthly;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsWeekly;

    protected DevExpress.Web.ASPxTabControl.ASPxPageControl CalendarContainer;

    protected Int32 hasApproal;
    protected string paramHF = "";
    protected string dtFormatType = "MM/dd/yyyy";
    protected string CalendarType = "D";
    protected string timeFormat = "1";
    protected string tFormats = "hh:mm tt";
    protected string isPublic = "D", isFuture = "D", isPending = "D", isApproval = "D", isOngoing = "D";
    protected Boolean bypass = false, isAdminRole = false;
    protected DateTime conf = DateTime.Now;
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    protected string enableBufferZone = "";
    //Organization/CSS Module -- Start
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAV;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrg;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudCon;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConf;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfM;
    CustomizationUtil.CSSReplacementUtility cssUtil;
    //Organization/CSS Module -- End

    private delegate void GetCalendar();

    protected System.Web.UI.WebControls.CheckBox showDeletedConf; //FB 1800
    
    DateTime dtCell = DateTime.Now;//FB 1861
    string cellColor = "";//FB 1861
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend1; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend2; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend3; //FB 1985
    //ZD 100157 Starts
    protected MetaBuilders.WebControls.ComboBox lstStartHrs;
    protected MetaBuilders.WebControls.ComboBox lstEndHrs;
    protected System.Web.UI.WebControls.RegularExpressionValidator reglstStartHrs;
    protected System.Web.UI.WebControls.RegularExpressionValidator reglstEndHrs;
    StringBuilder inXML = new StringBuilder();
    int isShowHrs = 0;
    //ZD 100157 Ends

    #region Page_Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("PersonalCalendar.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            // ZD 100170                
            string path = Request.Url.AbsoluteUri.ToLower();
            if (path.IndexOf("%3c") > -1 || path.IndexOf("%3e") > -1)
                Response.Redirect("ShowError.aspx");

            log = new ns_Logger.Logger();   //Location Issues
            obj = new myVRMNet.NETFunctions();

            //Organization/CSS Module - Create folder for UI Settings --- Strat

            if (Session["isMultiLingual"] != null)
            {
                if (Session["isMultiLingual"].ToString() != "1")
                {
                    string fieldText = "";
                    cssUtil = new CustomizationUtil.CSSReplacementUtility();

                    fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnAV");
                    spnAV.InnerText = obj.GetTranslatedText(fieldText);
                    spnAVW.InnerText = obj.GetTranslatedText(fieldText);
                    spnAVM.InnerText = obj.GetTranslatedText(fieldText);

                    fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnRmHrg");
                    spnRmHrg.InnerText = obj.GetTranslatedText(fieldText);
                    spnRmHrgW.InnerText = obj.GetTranslatedText(fieldText);
                    spnRmHrgM.InnerText = obj.GetTranslatedText(fieldText);

                    if (Application["client"] != null)
                        if (Application["client"].ToString().ToUpper() != "MOJ")
                        {
                            fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnAudCon");
                            spnAudCon.InnerText = obj.GetTranslatedText(fieldText);
                            spnAudConW.InnerText = obj.GetTranslatedText(fieldText);
                            spnAudConM.InnerText = obj.GetTranslatedText(fieldText);

                            fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnPpConf");
                            spnPpConf.InnerText = obj.GetTranslatedText(fieldText);
                            spnPpConfW.InnerText = obj.GetTranslatedText(fieldText);
                            spnPpConfM.InnerText = obj.GetTranslatedText(fieldText);
                        }
                    //Organization/CSS Module - Create folder for UI Settings --- End
                }
            }

            if (Session["EnableBufferZone"] == null)//Organization Module Fixes
            {
                Session["EnableBufferZone"] = "0";//Organization Module Fixes
            }

            enableBufferZone = Session["EnableBufferZone"].ToString();//Organization Module Fixes

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
            {
                Tdbuffer1.Attributes.Add("style", "display:none");
                Tdbuffer2.Attributes.Add("style", "display:none");
                Tdbuffer3.Attributes.Add("style", "display:none");
                Tdbuffer4.Attributes.Add("style", "display:none");
                TdA.Attributes.Add("style", "display:none");
                TdA1.Attributes.Add("style", "display:none");
                TdP2p.Attributes.Add("style", "display:none");
                TdP2p1.Attributes.Add("style", "display:none");
                TdAV.Attributes.Add("style", "display:none");
                TdAV1.Attributes.Add("style", "display:none");
                
                enableBufferZone = "0";
            }

            if (enableBufferZone == "0")
            {
                schDaypilot.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
                schDaypilotweek.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
            }

            if (Session["admin"] != null)
            {
                if (Session["admin"].ToString() == "1" || Session["admin"].ToString() == "2")   //buffer zone - Super & conf admin
                    isAdminRole = true;
            }

            if (!DateTime.TryParse(txtSelectedDate.Value, out conf))
                conf = DateTime.Today;

            if (Request.QueryString["hf"] != null)
            {
                paramHF = Request.QueryString["hf"].ToString();
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() == "")
                    Session["FormatDateType"] = "MM/dd/yyyy";
                else
                    dtFormatType = Session["FormatDateType"].ToString();
            }

            schDaypilot.HeaderDateFormat = "d MMMM yyyy";
            schDaypilotweek.HeaderDateFormat = dtFormatType;
            //ZD 100157 Starts
            lstStartHrs.Items.Clear();
            lstEndHrs.Items.Clear();
            obj.BindTimeToListBox(lstStartHrs, true, false);
            obj.BindTimeToListBox(lstEndHrs, true, false);
            //ZD 100157 Ends
            timeFormat = ((Session["timeFormat"] != null) ? Session["timeFormat"].ToString() : timeFormat);

            if (Session["timeFormat"].ToString() == "0")
            {
                tFormats = "HH:mm";
                schDaypilot.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
                schDaypilotweek.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
            }
            else if (Session["timeFormat"].ToString() == "2")//FB 2588
            {
                tFormats = "HHmmZ";
            }

        
            //ZD 100157 Starts
            if (Session["timeFormat"].ToString().Equals("0"))
            {
                reglstStartHrs.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                reglstStartHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                reglstEndHrs.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                reglstEndHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
            }
            else if (Session["timeFormat"].ToString().Equals("2"))
            {
                reglstStartHrs.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                reglstStartHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                reglstEndHrs.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                reglstEndHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
            }
            //ZD 100157 Starts
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    schDaypilotweek.BeforeCellRender += new DayPilot.Web.Ui.Events.BeforeCellRenderEventHandler(BeforeCellRenderhandler);
                    
                    schDaypilotMonth.BeforeCellRender +=new DayPilot.Web.Ui.Events.Month.BeforeCellRenderEventHandler(BeforeMonthRenderhandler);

                }
            }
			//FB 1985 
            if (Application["Client"].ToString().ToUpper() == "DISNEY")
            {
                trlegend1.Style.Add("display", "none");
                trlegend2.Style.Add("display", "none");
                trlegend3.Style.Add("display", "none");
            }
            if (!IsPostBack)
            {
                GetUserCalendarHrs(); //ZD 100157
                //if (Session["CalendarMonthly"] == null) //FB 2027
                {
					//FB 1675 start
                    //GetCalendar monthly = new GetCalendar(GetMonthlyThread);
                    //monthly.BeginInvoke(null, null);
                    //GetMonthlyThread();  //ZD 100151
					//FB 1675 end
                }

                

                //officehrDaily.Checked = true; //ZD 100157
                officehrWeek.Checked = true;
                officehrMonth.Checked = true;

                if (Session["DefaultCalendarToOfficeHours"] != null)//Organization Module Fixes
                {
                    if (Session["DefaultCalendarToOfficeHours"].ToString() == "0")
                    {
                        //officehrDaily.Checked = false;
                        officehrWeek.Checked = false;
                        officehrMonth.Checked = false;
                    }
                    
                }


                treeRoomSelection.Nodes[0].Checked = true;

                foreach (TreeNode nds in treeRoomSelection.Nodes[0].ChildNodes)
                    nds.Checked = true;

                if (Request.QueryString["hf"].ToString() == "1")
                {
                    if (Request.QueryString["d"] != null)
                    {
                        if (!DateTime.TryParse(myVRMNet.NETFunctions.GetDefaultDate(Request.QueryString["d"].ToString()), out conf))
                            conf = DateTime.Today;
                    }

                }

                if (Session["CalendarMonthly"] != null)
                {
                    HdnMonthlyXml.Value = Session["CalendarMonthly"].ToString();
                    ViewState["HdnMonthlyXml"] = Session["CalendarMonthly"].ToString();
 
                }
                
                ChangeCalendarDate(null, null);
                //FB 2949 - Start
                if (Session["isExpressUser"] != null)//FB 1779
                {
                    if (Session["isExpressUser"].ToString() == "1")
                        schDaypilot.CellSelectColor = System.Drawing.Color.Empty;
                }
                //FB 2949 - End
            }
            if (IsPostBack) //ZD 100157
                ScriptManager.RegisterStartupScript(this, this.GetType(), "updtCalScroll", "setTimeout('fnTimeScroll();',250);", true); 
            
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_Load" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }

    }

    #endregion

    #region ChangeCalendarDate

    protected void ChangeCalendarDate(Object sender, EventArgs e)
    {
        String xmls = ""; //ZD 100151
        try
        {
            SetCalendarTImes(); //ZD 100157
            switch (CalendarContainer.ActiveTabIndex)
            {
                case 0://ZD 100151
                    xmls = GetCalendarOutXml();
                    DatatablefromXML(xmls, 1);
                    BindDaily(xmls,"D");
                    break;
                case 1:
                    Int32 addsessn = 1;
                    if (IsWeekOverLap.Value == "Y")
                        addsessn = 0;
                    else
                        Session.Remove("PersonalWeekly");
					//ZD 100151
                    xmls = GetWeeklyCalendar();
                    DatatablefromXML(xmls, addsessn);
                    BindWeekly(xmls);
                    break;
                case 2:
					//ZD 100151
                    //xmls = GetMonthlyCalendar();
                    DatatablefromXML(GetMonthlyCalendar(), 1);
                    SetOfficeHours(HdnMonthlyXml.Value);
                    BindDaily("","M");
                    break;
            }
			//ZD 100151
            //BindWeekly();
            //BindDaily();
            
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region GetCalendarOutXml
    public string GetCalendarOutXml()
    {
        string inXML = "";
        string outXMLDly = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
            }

            outXMLDly = HdnMonthlyXml.Value;

            if ((IsMonthChanged.Value == "Y") || outXMLDly == "")
            {

                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
                outXMLDly = obj.CallMyVRMServer("GetRoomDailyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
               
            }


            return outXMLDly;
        }
        catch (Exception ex)
        {
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetCalendarOutXml: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";//FB 1881
        }
    }
    #endregion

    #region GetMonthlyCalendar
    public string GetMonthlyCalendar()
    {
        string inXML = "";
        string outXmlMn = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] == null) //FB 1675
                Thread.Sleep(100);
          
            
            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800

            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                {
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
                    
                }
            }

            outXmlMn = HdnMonthlyXml.Value;

            if ((IsMonthChanged.Value == "Y" || HdnMonthlyXml.Value == ""))
            {
                outXmlMn = obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                HdnMonthlyXml.Value = outXmlMn;
                ViewState["HdnMonthlyXml"] = outXmlMn;
            }

            return outXmlMn;
        }
        catch (Exception ex)
        {
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetMonthlyCalendar: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";
        }
    }

    public void GetMonthlyThread()
    {
        string inXML = "";
        string outXmlMn = "";
        try
        {
            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
            outXmlMn = obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
            outXmlMn = outXmlMn.Replace("\t", "").Replace("\n", "").Replace("\r", ""); //FB 2012
            ViewState["HdnMonthlyXml"] = outXmlMn;            
            HdnMonthlyXml.Value = outXmlMn;
            Session.Remove("CalendarMonthly");
            Session.Add("CalendarMonthly", outXmlMn);

        }
        catch (Exception ex)
        {
            log.Trace("GetMonthlyThread" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;

        }
        
    }
    #endregion

    #region GetWeeklyCalendar
    public string GetWeeklyCalendar()
    {
        string inXML = "";
        string outXMLWkly = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] == null) //FB 1675
                Thread.Sleep(100); 

            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                {
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
                    ViewState["HdnMonthlyXml"] = "";
                }
            }

            outXMLWkly = HdnMonthlyXml.Value;
                      
            if (IsMonthChanged.Value == "Y"  || IsWeekOverLap.Value == "Y" || outXMLWkly == "")
            {

                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
                outXMLWkly = obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                //HdnMonthlyXml.Value = outXMLWkly;
            }
            

            return outXMLWkly;
        }
        catch (Exception ex)
        {
            //errLabel.Text = ex.StackTrace;
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetWeeklyCalendar: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";
        }
    }


    public void GetWeekly()
    {
        string inXML = "";
        string outXMLWkly = "";
        try
        {
            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
            outXMLWkly = obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
            ViewState["HdnWeeklyXml"] = outXMLWkly;
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("GetWeekly" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BeforeEventRenderhandler

    protected void BeforeEventRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeEventRenderEventArgs e)
    {
        try
        {

            e.InnerHTML = e.Text;
            switch (e.Tag[0])
            {
                case ns_MyVRMNet.vrmConfType.AudioOnly: e.BackgroundColor = "#EAA2D4"; ; break;
                case ns_MyVRMNet.vrmConfType.AudioVideo: e.BackgroundColor = "#BBB4FF"; ; break;
                case ns_MyVRMNet.vrmConfType.P2P: e.BackgroundColor = "#85EE99"; ; break;
                case ns_MyVRMNet.vrmConfType.RoomOnly: e.BackgroundColor = "#F16855"; ; break;
                case ns_MyVRMNet.vrmConfType.HotDesking: e.BackgroundColor = "#cc0033"; ; break;//FB 2694
                case "S": e.BackgroundColor = "#FFCC99"; ; break;
                case "T": e.BackgroundColor = "#CCCC99"; ; break;
                case "9": e.BackgroundColor = "#01DFD7"; ; break;
                case "10": e.BackgroundColor = "#82CAFF"; ; break; //FB 2448

            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeEventRenderhandler" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BeforeEventRenderhandler

    protected void BeforeEventRenderhandler(object sender, DayPilot.Web.Ui.Events.Month.BeforeEventRenderEventArgs e )
    {
        try
        {

            e.InnerHTML = e.Text;
            switch (e.Tag[0])
            {
                case ns_MyVRMNet.vrmConfType.AudioOnly: e.BackgroundColor = "#EAA2D4"; ; break;
                case ns_MyVRMNet.vrmConfType.AudioVideo: e.BackgroundColor = "#BBB4FF"; ; break;
                case ns_MyVRMNet.vrmConfType.P2P: e.BackgroundColor = "#85EE99"; ; break;
                case ns_MyVRMNet.vrmConfType.RoomOnly: e.BackgroundColor = "#F16855"; ; break;
                case ns_MyVRMNet.vrmConfType.HotDesking: e.BackgroundColor = "#cc0033"; ; break;//FB 2694
                case "S": e.BackgroundColor = "#FFCC99"; ; break;
                case "T": e.BackgroundColor = "#CCCC99"; ; break;
                case "9": e.BackgroundColor = "#01DFD7"; ; break;
                case "10": e.BackgroundColor = "#82CAFF"; ; break; //FB 2448


            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeEventRenderhandler" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }
    #endregion
    //2272 - Start

    #region BeforeTimeHeaderRender
    protected void BeforeTimeHeaderRender(Object sender, DayPilot.Web.Ui.Events.BeforeHeaderRenderEventArgs e)
    {
        try
        {
            string dt, mon;            
            DayPilot.Web.Ui.Events.BeforeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.BeforeHeaderRenderEventArgs)e;            
            dt = DateEvent.InnerHTML;
            mon = dt.Split(' ')[1];
            dt = dt.Replace(mon, obj.GetTranslatedText(mon));
            DateEvent.InnerHTML = dt;
            DateEvent.ToolTip = dt;            
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeTimeHeaderRender" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }
    #endregion

    #region BeforeTimeHeaderRender
    protected void BeforeTimeHeaderRender(Object sender, DayPilot.Web.Ui.Events.Month.BeforeHeaderRenderEventArgs e)
    {
        try
        {
            DayPilot.Web.Ui.Events.Month.BeforeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.Month.BeforeHeaderRenderEventArgs)e;

            DateEvent.InnerHTML = obj.GetTranslatedText(e.DayOfWeek.ToString());

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeTimeHeaderRender" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }
    #endregion

    //2272 - End

    #region BubbleRenderhandler
    protected void BubbleRenderhandler(object sender, DayPilot.Web.Ui.Events.Bubble.RenderEventArgs e)
    {
        try
        {
            DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs re = (DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs)e;
            //ZD 100151
            StringBuilder m = new StringBuilder(); ;
            m.Append("<table cellspacing='0' cellpadding='0' border='0' width='300px' class='promptbox' bgColor = '#ccccff'>");
            m.Append("<tr valign='middle'>");
            m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
            m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + re.Tag["Heading"]);
            m.Append("</td>");
            m.Append("</tr>");
            m.Append("</table>");
            m.Append("<div style='width: 300px; height: 140px; overflow: auto;'>");
            m.Append("<table cellspacing='0' cellpadding='0' border='0' class='promptbox' bgColor = '#ccccff'>");
            m.Append("<tr>");
            m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Name") + ": </span></td>");
            m.Append("<td width='200px'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["confName"] + "</span></td>");
            m.Append("</tr>");
            m.Append("<tr>");
            m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'> " + obj.GetTranslatedText("Unique ID") + ": </span></td>"); // FB 2002
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["ID"] + "</span></td>");
            m.Append("</tr>");
            m.Append("<tr>");
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Start - End") + ": </span></td>");
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["ConfTime"] + "</span></td>");
            m.Append("</tr>");
            m.Append("<tr>");
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Duration") + ": </span></td>");
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["Hours"] + " hr(s) " + re.Tag["Minutes"] + " min(s)</span></td>");
            m.Append("</tr>");
            m.Append("<tr>");//FB 2622 Starts
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Password") + ": </span></td>");
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["Password"] + "</span></td>");
            m.Append("</tr>");//FB 2622 Ends
            m.Append("<tr>");
            m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Location") + ": </span></td>");
            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["Location"] + "</span></td>");
            m.Append("</tr>");

            if (re.Tag["ConfSupport"] == "")
            {
                m.Append("<tr>");
                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'></span></td>");
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'></span></td>");
                m.Append("</tr>");
            }
            else
            {
                m.Append("<tr>"); //FB 2632
                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Conference Support") + ": </span></td>"); //FB 3023
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["ConfSupport"] + "</span></td>");
                m.Append("</tr>");
            }

            if (re.Tag["CustomOptions"] != "")
            {
                m.Append("<tr>");
                m.Append("<td valign='top' nowrap><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Custom Options") + ":&nbsp;&nbsp; </span></td>");
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["CustomOptions"] + "</span></td>");
                m.Append("</tr>");
            }
            
            m.Append("</table>");
            m.Append("</div>");             
            
            re.InnerHTML = m.ToString();// re.Tag["CustomDescription"];

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BubbleRenderhandler" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }
    #endregion    

    #region BeforeCellRenderhandler
    /// <summary>
    /// // FB 1860
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BeforeCellRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeCellRenderEventArgs e)
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    dtCell = e.Start;
                    GetdayColour();

                    if (cellColor != "")
                        e.BackgroundColor = cellColor;

                }
            }

            

           /* DateTime cellTIme = e.Start;
            if (cellTIme.Day % 5 == 0)
                e.BackgroundColor = "#EAA2D5";
            else if (cellTIme.Day % 7 == 0)
                e.BackgroundColor = "#85EE98";*/
            
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeCellRenderhandler" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BeforeCellRenderhandler
    /// <summary>
    /// // FB 1860
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BeforeMonthRenderhandler(object sender, DayPilot.Web.Ui.Events.Month.BeforeCellRenderEventArgs e)
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    dtCell = e.Start;
                    GetdayColour();

                    if (cellColor != "")
                        e.BackgroundColor = cellColor;

                }
            }
            
            /* DateTime cellTIme = e.Start;
             if (cellTIme.Day % 5 == 0)
                 e.BackgroundColor = "#EAA2D5";
             else if(cellTIme.Day % 7 == 0)
                 e.BackgroundColor = "#85EE98";*/
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeMonthRenderhandler" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }

    

    #endregion

    #region Get Data Table

    private DataTable GetDataTable()
    {
        DataTable dt = null;
        try
        {
            dt = new DataTable();

            if (!dt.Columns.Contains("start")) dt.Columns.Add("start");
            if (!dt.Columns.Contains("end")) dt.Columns.Add("end");
            if (!dt.Columns.Contains("formatstart")) dt.Columns.Add("formatstart");
            if (!dt.Columns.Contains("formatend")) dt.Columns.Add("formatend");
            if (!dt.Columns.Contains("confDetails")) dt.Columns.Add("confDetails");
            if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
            if (!dt.Columns.Contains("ConfID")) dt.Columns.Add("ConfID");
            if (!dt.Columns.Contains("ConferenceType")) dt.Columns.Add("ConferenceType");
            if (!dt.Columns.Contains("RoomID")) dt.Columns.Add("RoomID");
            if (!dt.Columns.Contains("confName")) dt.Columns.Add("confName");
            if (!dt.Columns.Contains("durationMin")) dt.Columns.Add("durationMin");
            if (!dt.Columns.Contains("CustomDescription")) dt.Columns.Add("CustomDescription");
			//ZD 100151
            if (!dt.Columns.Contains("Heading")) dt.Columns.Add("Heading");
            if (!dt.Columns.Contains("ConfTime")) dt.Columns.Add("ConfTime");
            if (!dt.Columns.Contains("Hours")) dt.Columns.Add("Hours");
            if (!dt.Columns.Contains("Minutes")) dt.Columns.Add("Minutes");
            if (!dt.Columns.Contains("Password")) dt.Columns.Add("Password");
            if (!dt.Columns.Contains("Location")) dt.Columns.Add("Location");
            if (!dt.Columns.Contains("ConfSupport")) dt.Columns.Add("ConfSupport");
            if (!dt.Columns.Contains("CustomOptions")) dt.Columns.Add("CustomOptions");

            


        }
        catch (Exception ex)
        {

            throw ex;
        }

        return dt;

    }

    #endregion

    #region changeDate

    protected void changeDate(Object sender, EventArgs e)
    {
        String xmls = ""; //ZD 100151
        try
        {
            SetCalendarTImes(); //ZD 100157
            
            //ZD 100151
            BindDaily("","");

            if (IsWeekOverLap.Value != "Y")
                Session.Remove("PersonalWeekly");

            if ((IsWeekOverLap.Value == "Y" && Session["PersonalWeekly"] == null) || (IsWeekOverLap.Value == "Y" && IsWeekChanged.Value == "Y"))
            {
                int addsessn = 1;
                if (IsWeekOverLap.Value == "Y")
                    addsessn = 0;
                //ZD 100151
                xmls = GetWeeklyCalendar();
                DatatablefromXML(xmls, addsessn);
            }

            BindWeekly(xmls); //ZD 100151

        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("changeDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Datatable from XML

    protected void DatatablefromXML(string xmls, int addtoSession)
    {
        string hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        int hrs, mins = 0;
        string ConfPassword = "N/A", Password = obj.GetTranslatedText("Password"); //FB 2622
        string CongAtt = "", CongAttStatus = obj.GetTranslatedText("No"), DedicatedVNOCOperator = obj.GetTranslatedText("Dedicated VNOC Operator");//FB 2632
        string OnSiteAVSupport = obj.GetTranslatedText("On-Site A/V Support"), MeetandGreet = obj.GetTranslatedText("Meet and Greet"), ConciergeMonitoring = obj.GetTranslatedText("Call Monitoring");//FB 3023

        string locStr = "";
        string setupTxt = "";
        string trdnTxt = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        XmlDocument xmldoc = null;
        DataTable dt = null;
        StringBuilder m = null;
        XmlNodeList subnotes2 = null;
        XmlNode subnode2 = null;
        //FB 2961 Starts
        DataTable dtDaily = null; 
        Boolean Blnsetup = false, BlnTear = false;
        //FB 2961 Ends
        try
        {

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
                hdr = "Hearing Details";

            String spn = "<span  class=\"eventtext\">";

            if (xmls != "")
            {
                bypass = false;

                if (treeRoomSelection.Nodes[0].Checked)
                    bypass = true;

                if (!bypass)
                {

                    foreach (TreeNode tr in treeRoomSelection.Nodes[0].ChildNodes)
                    {
                        if (tr.Checked)
                        {
                            switch (tr.Value)
                            {

                                case "OG":
                                    isOngoing = "1";
                                    break;
                                case "FC":
                                    isFuture = "1";
                                    break;
                                case "PC":
                                    isPublic = "1";
                                    break;
                                case "PNC":
                                    isPending = "1";
                                    break;
                                case "AC":
                                    isApproval = "1";
                                    break;

                            }
                        }
                    }
                }


                xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmls);

                dt = GetDataTable();
                dtDaily = GetDataTable(); //FB 2961
                
                //ZD 100157 Starts
                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/ShowPerHrs") != null)
                    open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/ShowPerHrs").InnerText; //ZD 100157
                if (open24 == "1")
                {
                    officehrDaily.Checked = true;
                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/startTime/startHour") != null)
                        startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/startTime/startHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/startTime/startMin") != null)
                        startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/startTime/startMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/startTime/startSet") != null)
                        startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/startTime/startSet").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/endTime/endHour") != null)
                        endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/endTime/endHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/endTime/endMin") != null)
                        endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/endTime/endMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/endTime/endSet") != null)
                        endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Personal/endTime/endSet").InnerText;

                    schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));
                    if (endMin != "00") //FB 2057
                        schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                    schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));
                    if (endMin != "00")// FB 2057
                        schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;

                }
                else
                {
                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                        open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                    if (open24 == "1")
                    {
                        officehrDaily.Checked = false;

                        schDaypilot.BusinessBeginsHour = 0;
                        schDaypilot.BusinessEndsHour = 24;

                        schDaypilotweek.BusinessBeginsHour = 0;
                        schDaypilotweek.BusinessEndsHour = 24;
                    }
                    else
                    {

                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                            startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                            startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                            startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                            endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                            endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                            endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                        schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                        schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                        if (endMin != "00") //FB 2057
                            schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                        schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                        schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                        if (endMin != "00")// FB 2057
                            schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
                    }

                }
                //ZD 100157 Ends
                nodes = xmldoc.SelectNodes("//days/day/conferences/conference");

                for (int confnodes = 0; confnodes < nodes.Count; confnodes++)// XmlNode node in nodes)
                {
                    node = nodes[confnodes];
                    string confSTime = "";
                    string setupSTime = "", teardownSTime = "", confSDate = "", uniqueID = ""; // FB 2002

                    if (node.SelectSingleNode("durationMin") != null)
                    {

                        if (node.SelectSingleNode("durationMin").InnerText != "")
                        {
                            if (!showDeletedConf.Checked)
                            {
                                if (node.SelectSingleNode("deleted") != null)
                                {
                                    if (node.SelectSingleNode("deleted").InnerText == "1")
                                        continue;
                                }
                            }

                            if (node.SelectSingleNode("isImmediate").InnerText == "0" && node.SelectSingleNode("isFuture").InnerText == "0" && node.SelectSingleNode("isPublic").InnerText == "0" && node.SelectSingleNode("isPending").InnerText == "0" && node.SelectSingleNode("isApproval").InnerText == "0")
                                bypass = true;
                            else if (!treeRoomSelection.Nodes[0].Checked)
                                bypass = false;

                            if (bypass || (node.SelectSingleNode("isImmediate").InnerText == isOngoing || node.SelectSingleNode("isFuture").InnerText == isFuture || node.SelectSingleNode("isPublic").InnerText == isPublic || node.SelectSingleNode("isPending").InnerText == isPending || node.SelectSingleNode("isApproval").InnerText == isApproval))
                            {
                                DataRow dr = dt.NewRow();
                                DataRow drDaily = dtDaily.NewRow(); //FB 2961

                                if (node.SelectSingleNode("confName") != null)
                                    dr["confName"] = node.SelectSingleNode("confName").InnerText;

                                if (Session["isVIP"] != null)
                                {
                                    if (Session["isVIP"].ToString() == "1")
                                    {
                                        if (node.SelectSingleNode("isVIP") != null) // FB 1864
                                            if (node.SelectSingleNode("isVIP").InnerText == "1")
                                                dr["confName"] = dr["confName"].ToString() + " {VIP}";
                                    }
                                }
                                            
                                
                                dr["durationMin"] = node.SelectSingleNode("durationMin").InnerText;
                                hrs = Convert.ToInt32(dr["durationMin"].ToString()) / 60;
                                mins = Convert.ToInt32(dr["durationMin"].ToString()) % 60;
                                if (node.SelectSingleNode("confID") != null)
                                    dr["ConfID"] = node.SelectSingleNode("confID").InnerText;
                                if (node.SelectSingleNode("ConferenceType") != null)
                                    dr["ConferenceType"] = node.SelectSingleNode("ConferenceType").InnerText;

                                //FB 2448 Starts
                                Password = obj.GetTranslatedText("Password"); //FB 2622
                                if (node.SelectSingleNode("isVMR") != null)
                                {
									// if (node.SelectSingleNode("isVMR").InnerText == "1")
                                    if (Convert.ToInt32(node.SelectSingleNode("isVMR").InnerText) > 0) // FB 2620
                                    {
                                        dr["ConferenceType"] = "10";
                                        Password = obj.GetTranslatedText("VMR Pin"); //FB 2622
                                    }
                                }
                                //FB 2448 Ends
                                //FB 2622 Starts
                                ConfPassword = obj.GetTranslatedText("N/A");
                                if (node.SelectSingleNode("confPassword") != null)
                                {
                                    if (node.SelectSingleNode("confPassword").InnerText.Trim() != "")
                                        ConfPassword = node.SelectSingleNode("confPassword").InnerText.Trim();
                                }
                                //FB 2622 Ends
                                if (showDeletedConf.Checked)
                                {
                                    if (node.SelectSingleNode("deleted") != null)
                                    {
                                        if (node.SelectSingleNode("deleted").InnerText == "1")
                                            dr["ConferenceType"] = "9";
                                    }
                                }


                                if (node.SelectSingleNode("confTime") != null)
                                    confSTime = node.SelectSingleNode("confTime").InnerText;

                                if (node.SelectSingleNode("setupTime") != null)
                                    setupSTime = node.SelectSingleNode("setupTime").InnerText;

                                if (node.SelectSingleNode("teardownTime") != null)
                                    teardownSTime = node.SelectSingleNode("teardownTime").InnerText;

                                if (node.SelectSingleNode("confDate") != null)
                                {
                                    if (node.SelectSingleNode("confDate").InnerText != "")
                                    {
                                        confSDate = node.SelectSingleNode("confDate").InnerText;
                                    }
                                }
                                // FB 2002 starts
                                if (node.SelectSingleNode("uniqueID") != null)
                                {
                                    if (node.SelectSingleNode("uniqueID").InnerText != "")
                                    {
                                        uniqueID = node.SelectSingleNode("uniqueID").InnerText;
                                    }
                                }
                                // FB 2002 ends

                                dr["ID"] = uniqueID;// dr["ConfID"].ToString(); //ZD 100151

                                int adddays = 0;
                                if (confSTime.Trim() == "00:00 AM")
                                    adddays = 1;

                                DateTime start = DateTime.Parse(confSDate + " " + confSTime);
                                DateTime stUp = DateTime.Parse(confSDate + " " + setupSTime);
                                DateTime end = start.AddMinutes(Convert.ToDouble(dr["durationMin"]));
                                DateTime trDn = DateTime.Parse(end.ToString("MM/dd/yyyy") + " " + teardownSTime);

                                start = start.AddDays(adddays);
                                stUp = stUp.AddDays(adddays);
                                end = end.AddDays(adddays);
                                trDn = trDn.AddDays(adddays);

                                if (adddays < 1 && stUp.ToString("hh:mm tt") == "12:00 AM" && start.ToString("hh:mm tt") != "12:00 AM") //FB 2961
                                    stUp = stUp.AddDays(1);


                                dr["start"] = start;
                                dr["end"] = end;

                                dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats); ;

                                subnotes2 = node.SelectNodes("mainLocation/location");

                                if (subnotes2 != null)
                                {
                                    for (int subnodescnt = 0; subnodescnt < subnotes2.Count; subnodescnt++)
                                    {
                                        subnode2 = subnotes2[subnodescnt];
                                        locStr = locStr + subnode2.SelectSingleNode("locationName").InnerText;
                                        locStr = locStr + "<br>";
                                    }
                                }
                                // FB 2961 Starts
                                if (end != trDn && enableBufferZone == "1")
                                    BlnTear = true;

                                if (start != stUp && enableBufferZone == "1")
                                    Blnsetup = true;

                                trdnTxt = "";
                                if (end != trDn)//&& cnt > 1
                                    trdnTxt = "Tear Down :" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "<br>";

                                setupTxt = "";
                                if (stUp != start)//&& cnt > 1
                                    setupTxt = "Setup :" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "<br>";

                                if (Blnsetup)
                                {
                                    dr["start"] = stUp;
                                    dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(stUp.ToString("MM/dd/yyyy")) + " " + stUp.ToString(tFormats);
                                }

                                if (BlnTear)
                                {
                                    dr["end"] = trDn;
                                    dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(trDn.ToString("MM/dd/yyyy")) + " " + trDn.ToString(tFormats);
                                }

                                if (enableBufferZone == "0")
                                {
                                    Blnsetup = false;
                                    BlnTear = false;
                                    trDn = end;
                                    stUp = start;
                                    trdnTxt = "";
                                    setupTxt = "";
                                }

                                //FB 2961 Ends

                                //FB 2013 start
                                string customText = "";    
                               //if (Session["EnableEntity"].ToString() != "0")//Organization Module Fixes //FB 2547
                               // {
                                    XmlNodeList customnodes = node.SelectNodes("CustomAttributesList/CustomAttribute");
                                    for (int custnodescnt = 0; custnodescnt < customnodes.Count; custnodescnt++)
                                    {
                                        XmlNode customnode = customnodes[custnodescnt];
                                        string attriName = "", attriValue = "";

                                        if (customnode.SelectSingleNode("IncludeInCalendar") != null)
                                            if (customnode.SelectSingleNode("IncludeInCalendar").InnerText.Trim().Equals("0"))
                                                continue;
                                        
                                        if (customnode.SelectSingleNode("Status") != null)
                                        {
                                            if (customnode.SelectSingleNode("Status").InnerText == "0")
                                            {
                                                //customText = "<b>Custom Attrributes</b>";
                                                if (customnode.SelectSingleNode("Title") != null)
                                                    attriName = customnode.SelectSingleNode("Title").InnerText;

                                                if (customnode.SelectSingleNode("Type") != null)
                                                {
                                                    if (customnode.SelectSingleNode("Type").InnerText == "3")
                                                    {
                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                        {
                                                            if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                attriValue = "Yes";
                                                            else
                                                                attriValue = "No";
                                                        }
                                                    }
                                                    else if (customnode.SelectSingleNode("Type").InnerText == "2")//FB 2377
                                                    {
                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                        {
                                                            if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                attriValue = "Yes";
                                                            else
                                                                attriValue = "No";
                                                        }
                                                    }
                                                    else
                                                    {

                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                            attriValue = customnode.SelectSingleNode("SelectedValue").InnerText;
                                                    }
                                                }

                                                XmlNodeList optNodes = customnode.SelectNodes("OptionList/Option");

                                                if (customnode.SelectSingleNode("Type").InnerText.Trim() == "5" || customnode.SelectSingleNode("Type").InnerText.Trim() == "6") //FB 1718
                                                {
                                                    if (optNodes != null)
                                                    {
                                                        for (int optnodescnt = 0; optnodescnt < optNodes.Count; optnodescnt++)
                                                        {
                                                            XmlNode optNode = optNodes[optnodescnt];
                                                            if (optNode.SelectSingleNode("Selected") != null)
                                                            {
                                                                if (optNode.SelectSingleNode("Selected").InnerText == "1")
                                                                {
                                                                    if (optNode.SelectSingleNode("DisplayCaption") != null)
                                                                    {
                                                                        if (attriValue == "")
                                                                            attriValue = optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                        else
                                                                            attriValue += "," + optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                    }

                                                                }


                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            if (attriValue == "")
                                                attriValue = "N/A";

                                            customText += attriName + " - " + attriValue + "<br>";
                                        }
                                    }

                                //}
                               //FB 2013 end
                               //FB 2632 Starts//FB2670 Start//FB 3007 Start
                               string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                               string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                               string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                               string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();
                               CongAtt = "";
                               if (EnableOnsiteAV == "1" && dr["ConferenceType"].ToString() != "8")
                               {
                                   
                                   CongAttStatus = obj.GetTranslatedText("No");
                                   if (node.SelectSingleNode("OnSiteAVSupport") != null)
                                   {
                                       if (node.SelectSingleNode("OnSiteAVSupport").InnerText.Equals("1"))
                                           CongAttStatus = obj.GetTranslatedText("Yes");
                                   }
                                   CongAtt = OnSiteAVSupport + " - " + CongAttStatus + "</br>";
                               }

                               if (EnableMeetandGreet == "1" && dr["ConferenceType"].ToString() != "8")
                               {
                                   CongAttStatus = obj.GetTranslatedText("No");
                                   if (node.SelectSingleNode("MeetandGreet") != null)
                                   {
                                       if (node.SelectSingleNode("MeetandGreet").InnerText.Equals("1"))
                                           CongAttStatus = obj.GetTranslatedText("Yes");
                                   }
                                   CongAtt += MeetandGreet + " - " + CongAttStatus + "</br>";
                               }
                               if (EnableConciergeMonitoring == "1" && dr["ConferenceType"].ToString() != "8")
                               {

                                   CongAttStatus = obj.GetTranslatedText("No");
                                   if (node.SelectSingleNode("ConciergeMonitoring") != null)
                                   {
                                       if (node.SelectSingleNode("ConciergeMonitoring").InnerText.Equals("1"))
                                           CongAttStatus = obj.GetTranslatedText("Yes");
                                   }
                                   CongAtt += ConciergeMonitoring + " - " + CongAttStatus + "</br>";

                                   
                               }

                               //FB 2670 Start
                               if (EnableDedicatedVNOC == "1" && dr["ConferenceType"].ToString() != "8")
                               {
                                   CongAttStatus = obj.GetTranslatedText("No");
                                   XmlNodeList VNOCNodes = node.SelectNodes("ConfVNOCOperators/VNOCOperator");
                                   if (VNOCNodes != null && VNOCNodes.Count > 0)
                                   {
                                       CongAttStatus = "";
                                       for (int VNOCNodescnt = 0; VNOCNodescnt < VNOCNodes.Count; VNOCNodescnt++)
                                       {
                                           XmlNode VoptNode = VNOCNodes[VNOCNodescnt];
                                           if (VoptNode != null)
                                           {
                                               if (CongAttStatus == "")
                                                   CongAttStatus = VoptNode.InnerText.Trim();
                                               else
                                                   CongAttStatus += "<br>" + VoptNode.InnerText.Trim();
                                           }
                                       }
                                   }

                                   CongAtt += DedicatedVNOCOperator + " - " + CongAttStatus;
                               }

                               //ZD 100151
                               dr["Heading"] = hdr;
                               dr["ConfTime"] = stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats);
                               dr["Hours"] = hrs.ToString();
                               dr["Minutes"] = mins.ToString();
                               dr["Password"] = ConfPassword;
                               dr["Location"] = ((locStr == "") ? "N/A" : locStr);
                               if (Session["EnableOnsiteAV"].ToString() != "1" && Session["EnableMeetandGreet"].ToString() != "1" && Session["EnableConciergeMonitoring"].ToString() != "1" && Session["EnableDedicatedVNOC"].ToString() != "1" || dr["ConferenceType"].ToString() == "8")//FB 3007
                                   dr["ConfSupport"] = "";
                               else
                                   dr["ConfSupport"] = CongAtt;

                               if (Session["EnableEntity"].ToString() == "1" && Session["ShowCusAttInCalendar"].ToString() == "1")
                                   dr["CustomOptions"] = ((customText == "") ? "N/A" : customText);
                               else
                                   dr["CustomOptions"] = "";
                                
                               //FB 2670 End FB 3007 END                               
                                //m = new StringBuilder(); ;
                                
                                //m.Append("<table cellspacing='0' cellpadding='0' border='0' width='300px' class='promptbox' bgColor = '#ccccff'>");
                                //m.Append("<tr valign='middle'>");
                                //m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
                                //m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + hdr);
                                //m.Append("</td>");
                                //m.Append("</tr>");
                                //m.Append("</table>");                                
                                //m.Append("<div style='width: 300px; height: 140px; overflow: auto;'>");
                                //m.Append("<table cellspacing='0' cellpadding='0' border='0' class='promptbox' bgColor = '#ccccff'>");
                                //m.Append("<tr>");
                                //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Name") + ": </span></td>");
                                //m.Append("<td width='200px'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dr["confName"].ToString() + "</span></td>");
                                //m.Append("</tr>");
                                //m.Append("<tr>");
                                //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'> " + obj.GetTranslatedText("Unique ID") + ": </span></td>"); // FB 2002
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + uniqueID.ToString() + "</span></td>");
                                //m.Append("</tr>");
                                //m.Append("<tr>");
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Start - End") + ": </span></td>");
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</span></td>");
                                //m.Append("</tr>");
                                //m.Append("<tr>");
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Duration") + ": </span></td>");
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)</span></td>");
                                //m.Append("</tr>");
                                //m.Append("<tr>");//FB 2622 Starts
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + Password + ": </span></td>");
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ConfPassword + "</span></td>");
                                //m.Append("</tr>");//FB 2622 Ends
                                //m.Append("<tr>");
                                //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Location") + ": </span></td>");
                                //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((locStr == "") ? "N/A" : locStr) + "</span></td>");
                                //m.Append("</tr>");
                                ////FB 2670 START                            

                                //if (Session["EnableOnsiteAV"].ToString() != "1" && Session["EnableMeetandGreet"].ToString() != "1" && Session["EnableConciergeMonitoring"].ToString() != "1" && Session["EnableDedicatedVNOC"].ToString() != "1" || dr["ConferenceType"].ToString() == "8")//FB 3007
                                //{
                                //    m.Append("<tr>");
                                //    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'></span></td>");
                                //    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'></span></td>");
                                //    m.Append("</tr>");
                                //}
                                //else
                                //{
                                //    m.Append("<tr>"); //FB 2632
                                //    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Conference Support") + ": </span></td>"); //FB 3023
                                //    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + CongAtt + "</span></td>");
                                //    m.Append("</tr>");
                                //}
                                ////FB 2670 END
                                ////if (Session["EnableEntity"].ToString() != "0")//FB 2013 //FB 2547
                                ////{
                                ////    m.Append("<tr>");
                                //if (Session["EnableEntity"].ToString() == "1" && Session["ShowCusAttInCalendar"].ToString() == "1") //ZD 100151
                                //{
                                //    m.Append("<td valign='top' nowrap><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Custom Options") + ":&nbsp;&nbsp; </span></td>");
                                //    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((customText == "") ? "N/A" : customText) + "</span></td>");
                                //    m.Append("</tr>");
                                //}
                                ////}
                                //m.Append("</table>");
                                //m.Append("</div>");
                                //FB 2508 - End
                                dr["confDetails"] = spn + dr["confName"].ToString() + " - (UID : " + uniqueID + " )<br>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)<br>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</span><br>" + spn + setupTxt + trdnTxt + Password + " : " + ConfPassword + "<br>" + obj.GetTranslatedText("Locations") + ":<br>" + locStr + "</span><br>"; // FB 2002 //FB 2622

                                dr["CustomDescription"] = "";// m.ToString();

                                //dt.Rows.Add(dr);
                                
                                //FB 2961 Starts
                                String party = "0";
                                Boolean isParticipant = false;
                                if (node.SelectNodes("party") != null)
                                {
                                    if (node.SelectNodes("party").Count >= 1)
                                    {
                                        party = node.SelectSingleNode("party").InnerText;
                                    }
                                }

                                if (Session["userID"].ToString() == party)
                                    isParticipant = true;

                                if (isParticipant && !isAdminRole)
                                {
                                    Blnsetup = false;
                                    BlnTear = false;
                                }
                                drDaily["CustomDescription"] = dr["CustomDescription"];
                                drDaily["confDetails"] = dr["confDetails"];
                                drDaily["start"] = dr["start"];
                                drDaily["end"] = dr["end"];
                                drDaily["formatstart"] = dr["formatstart"];
                                drDaily["formatend"] = dr["formatend"];
                                drDaily["ID"] = dr["ID"];
                                drDaily["ConfID"] = dr["ConfID"];
                                drDaily["ConferenceType"] = dr["ConferenceType"];
                                drDaily["RoomID"] = dr["RoomID"];
                                drDaily["durationMin"] = dr["durationMin"];
                                drDaily["confName"] = dr["confName"];
								//ZD 100151
                                drDaily["Heading"] = dr["Heading"];
                                drDaily["ConfTime"] = dr["ConfTime"];
                                drDaily["Hours"] = dr["Hours"];
                                drDaily["Minutes"] = dr["Minutes"];
                                drDaily["Password"] = dr["Password"];
                                drDaily["Location"] = dr["Location"];
                                drDaily["ConfSupport"] = dr["ConfSupport"];
                                drDaily["CustomOptions"] = dr["CustomOptions"];

                                dt.Rows.Add(dr);

                                if (Blnsetup)
                                {
                                    DataRow setup = dtDaily.NewRow();
                                    //setup["RoomID"] = room.Value;
                                    setup["ConfID"] = dr["ConfID"].ToString();
                                    setup["ConferenceType"] = "S";
                                    setup["ID"] = dr["ConfID"].ToString();
                                    setup["start"] = start;
                                    setup["end"] = stUp;
                                    setup["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                    setup["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(stUp.ToString("MM/dd/yyyy")) + " " + stUp.ToString(tFormats);
                                    setup["confDetails"] = spn + "Setup Time<br>" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "</span>";
                                    setup["CustomDescription"] = m;
                                    dtDaily.Rows.Add(setup);
                                }
                                dtDaily.Rows.Add(drDaily);

                                if (BlnTear)
                                {
                                    DataRow trDown = dtDaily.NewRow();
                                    //trDown["RoomID"] = room.Value;
                                    trDown["ConfID"] = dr["ConfID"].ToString();
                                    trDown["ConferenceType"] = "T";
                                    trDown["ID"] = dr["ConfID"].ToString();
                                    trDown["start"] = trDn;
                                    trDown["end"] = end;
                                    trDown["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(trDn.ToString("MM/dd/yyyy")) + " " + trDn.ToString(tFormats);
                                    trDown["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats);
                                    trDown["confDetails"] = spn + "Tear Down Time<br>" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "</span>";
                                    trDown["CustomDescription"] = m;
                                    dtDaily.Rows.Add(trDown);
                                }

                                Blnsetup = false; BlnTear = false;
                                //FB 2961 Ends

                                locStr = "";


                            }
                        }
                    }
                }



                if (addtoSession > 0)
                {
                    if (Session["PersonalCalendar"] != null)
                        Session.Add("PersonalCalendar", dtDaily); //FB 2961
                    else
                        Session["PersonalCalendar"] = dtDaily; //FB 2961
                }
                else
                {
                    if (Session["PersonalWeekly"] != null)
                        Session.Add("PersonalWeekly", dtDaily); //FB 2961
                    else
                        Session["PersonalWeekly"] = dtDaily; //FB 2961
                }


            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("DatatablefromXML: " + ex.StackTrace + " : " + ex.Message + ":" + ex.InnerException);
        }

    }

    #endregion

    #region BindDaily

    protected void BindDaily(String xmls,String type) //ZD 100151
    {
        DataTable dt = null;

        try
        {
            //ZD 100157 Starts
            if (officehrDaily.Checked)
                SetCalOfficeHours(GetUserCalendarHrs());
            else
            {
                //ZD 100151
                if (type == "D" || type == "")
                {
                    if (xmls == "")
                        xmls = GetCalendarOutXml();

                    SetOfficeHours(xmls);// FB 2057
                }
            }
            //ZD 100157 Ends

            if (dt == null)
            {
                if (Session["PersonalCalendar"] != null)
                {
                    dt = (DataTable)Session["PersonalCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilot.StartDate = conf;
                schDaypilot.DataSource = dt;
                schDaypilot.DataBind();
                if (officehrDaily.Checked)
                    schDaypilot.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHoursNoScroll;
                else
                    schDaypilot.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.Full;
                schDaypilot.Visible = true;

                schDaypilotMonth.StartDate = conf;
                schDaypilotMonth.DataSource = dt;
                schDaypilotMonth.DataBind();
                if (officehrMonth.Checked)
                    schDaypilotMonth.ShowWeekend = false;
                else
                    schDaypilotMonth.ShowWeekend = true; ;
                schDaypilotMonth.Visible = true;
               
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("BindDaily: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindWeekly

    protected void BindWeekly(String xmls)//ZD 100151
    {
        DataTable dt = null; ; 

        try
        {
            //ZD 100157 Starts
            if (officehrDaily.Checked)
                SetCalOfficeHours(GetUserCalendarHrs());
            else
            {    //ZD 100151
                if (xmls == "")
                    xmls = GetWeeklyCalendar();
             
                SetOfficeHours(xmls);// FB 2057 ZD 100151
            }
            
            //ZD 100157 Ends

            if (dt == null)
            {

                if (Session["PersonalWeekly"] != null)
                {
                    dt = (DataTable)Session["PersonalWeekly"];
                }
                else if(Session["PersonalCalendar"] != null)
                {
                    dt = (DataTable)Session["PersonalCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilotweek.StartDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);
                if (officehrWeek.Checked)
                    schDaypilotweek.Days = 5;
                else
                    schDaypilotweek.Days = 7;

                if (officehrDaily.Checked) //ZD 100157
                    schDaypilotweek.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHoursNoScroll;
                else
                    schDaypilotweek.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.Full;

                schDaypilotweek.DataSource = dt;
                schDaypilotweek.DataBind();
                schDaypilotweek.Visible = true;
               
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("BindWeekly: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion
   
    #region Page_UnLoad
    /*
    protected void Page_UnLoad(object sender, EventArgs e)
    {
        try
        {
            
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }

    }
     */
    #endregion

    #region LoadXmlsAsync

    public void LoadXmlsAsync(Object sender, EventArgs e)
    {
        try
        {
            GetMonthlyThread();
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("LoadXmlsAsync" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }

    }

    #endregion 

    #region Get Day Colour
    /// <summary>
    /// FB 1860
    /// </summary>
    private void GetdayColour()
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    cellColor = "";


                    XElement root = XElement.Parse(Session["Holidays"].ToString());
                    IEnumerable<XElement> hldys =
                        from hldyelmnts in root.Elements("Holiday")
                        where (string)hldyelmnts.Element("Date") == dtCell.ToString("MM/dd/yyyy") //FB 2052
                        select hldyelmnts;

                    foreach (XElement elmnts in hldys)
                    {
                        cellColor = (string)elmnts.Element("Color");
                        break;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("GetdayColour" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion



    // FB 2588 Starts

    protected void BeforeTimeHeaderRenderDaily(DayPilot.Web.Ui.Events.Calendar.BeforeTimeHeaderRenderEventArgs e)
    {
        try
        {
            if (Session["timeFormat"].ToString().Equals("2"))
            {
                e.InnerHTML = convertToZulu(e.InnerHTML);
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("BeforeTimeHeaderRenderDaily: " + ex.StackTrace + " : " + ex.Message);
        }
    }


    protected void BeforeTimeHeaderRenderWeekly(DayPilot.Web.Ui.Events.Calendar.BeforeTimeHeaderRenderEventArgs e)
    {
        try
        {
            if (Session["timeFormat"].ToString().Equals("2"))
            {
                e.InnerHTML = convertToZulu(e.InnerHTML);
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("BeforeTimeHeaderRenderWeekly: " + ex.StackTrace + " : " + ex.Message);
        }
    }


    protected string convertToZulu(string str)
    {
        try
        {
            string hour = str.Split('>')[1].Split('<')[0];
            string session = str.Split('>')[2].Split('<')[0].Split(';')[1];
            if (session == "AM")
            {
                if (hour.Length == 1)
                    hour = "0" + hour;
                if (hour == "12")
                    hour = "00";
            }
            else if (session == "PM")
            {
                if (hour == "12")
                    hour = "12";
                else
                    hour = (Int32.Parse(hour) + 12).ToString();
            }
            string firstPart = "<div style='padding:2px; font-family:Tahoma;font-size:16pt;color:#000000;' unselectable='on'>";
            string lastPart = "<span style='font-size:10px; vertical-align: super; ' unselectable='on'>&nbsp;00Z</span></div>";
            return (firstPart + hour + lastPart);
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("convertToZulu: " + ex.StackTrace + " : " + ex.Message);
            return null;
        }
    }

    // FB 2588 Ends



    // FB 2057

    private void SetOfficeHours(string xmls)
    {
        string hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        
        XmlDocument xmldoc = null;
        DataTable dt = null;
        try
        {
            xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmls);

            dt = GetDataTable();

            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

            if (open24 == "1")
            {
                officehrDaily.Checked = false;

                schDaypilot.BusinessBeginsHour = 0;
                schDaypilot.BusinessEndsHour = 24;

                schDaypilotweek.BusinessBeginsHour = 0;
                schDaypilotweek.BusinessEndsHour = 24;
            }
            else
            {

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                    startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                    startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                    startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                    endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                    endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                    endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
            }


        }
        catch (Exception ex)
        {

            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("SetOfficeHours: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    //ZD 100157 StartS

    #region SetCalOfficeHours
    /// <summary>
    /// SetCalOfficeHours
    /// </summary>
    /// <param name="xmls"></param>
    private void SetCalOfficeHours(string xmls)
    {
        string hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";

        XmlDocument xmldoc = null;
        DataTable dt = null;
        DateTime sysSttime = DateTime.Now;
        try
        {
            xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmls);

            dt = GetDataTable();

            if (xmldoc.DocumentElement.SelectSingleNode("PerosnalCalendar/ShowPerHrs") != null)
                open24 = xmldoc.DocumentElement.SelectSingleNode("PerosnalCalendar/ShowPerHrs").InnerText;

            if (open24 == "1")
            {
                officehrDaily.Checked = true;
                if (xmldoc.DocumentElement.SelectSingleNode("PerosnalCalendar/StartDateTime") != null)
                    startHour = xmldoc.DocumentElement.SelectSingleNode("PerosnalCalendar/StartDateTime").InnerText;

                DateTime.TryParse(startHour, out sysSttime);

                startHour = sysSttime.ToString("hh");
                startMin = sysSttime.ToString("mm");
                startSet = sysSttime.ToString("tt");


                if (xmldoc.DocumentElement.SelectSingleNode("PerosnalCalendar/EndDateTime") != null)
                    endHour = xmldoc.DocumentElement.SelectSingleNode("PerosnalCalendar/EndDateTime").InnerText;

                DateTime.TryParse(endHour, out sysSttime);

                endHour = sysSttime.ToString("hh");
                endMin = sysSttime.ToString("mm");
                endSet = sysSttime.ToString("tt");
            }
            else
            {
                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                    open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                if (open24 == "1")
                {
                    officehrDaily.Checked = false;

                    schDaypilot.BusinessBeginsHour = 0;
                    schDaypilot.BusinessEndsHour = 24;

                    schDaypilotweek.BusinessBeginsHour = 0;
                    schDaypilotweek.BusinessEndsHour = 24;
                }
                else
                {

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                        startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                        startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                        startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                        endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                        endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                        endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;
                }
            }
            schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
            schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

            if (endMin != "00")
                schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

            schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
            schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

            if (endMin != "00")
                schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
        }
        catch (Exception ex)
        {

            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("SetCalOfficeHours: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    #region GetUserCalendarHrs
    /// <summary>
    /// GetUserCalendarHrs
    /// </summary>
    /// <returns></returns>
    private string GetUserCalendarHrs()
    {
        try
        {
            string outXML = "", EndTime = "";
            int Hour = 0, OffStartHr = 0;
            inXML = new StringBuilder();
            string PerStTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string PerEndTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            DateTime sysSttime = DateTime.Now;
            DateTime CalStTime = DateTime.Now;

            inXML.Append("<GetCalendarTimes>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append("</GetCalendarTimes>");
            outXML = obj.CallMyVRMServer("GetCalendarTimes", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") < 0)
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = null;
                XmlNode node = (XmlNode)xmldoc.DocumentElement;
                node = xmldoc.SelectSingleNode("//GetCalendarTimes/PerosnalCalendar/ShowPerHrs");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out isShowHrs);
                    if (isShowHrs.Equals(1))
                    {
                        officehrDaily.Checked = true;
                    }
                    else
                    {
                        officehrDaily.Checked = false;
                    }

                }
                if (isShowHrs == 1)
                {
                    node = xmldoc.SelectSingleNode("//GetCalendarTimes/PerosnalCalendar/StartTime");
                    if (node != null)
                    {
                        PerStTime = node.InnerText;
                        DateTime.TryParse(PerStTime, out sysSttime);
                        DateTime.TryParse(PerStTime, out CalStTime);
                        if (Session["timeFormat"].ToString() == "2")
                        {
                            myVRMNet.NETFunctions.ChangeTimeFormat(CalStTime.ToShortTimeString());
                        }
                        Hour = obj.GetHour(CalStTime.ToShortTimeString());
                        //mins = obj.GetMinute(CalStTime.ToShortTimeString());
                        OffStartHr = obj.GetHour(Session["SystemStartTime"].ToString());
                        //OffMins = obj.GetMinute(Session["SystemStartTime"].ToString());

                        if (Hour >= OffStartHr)
                        {
                           // if (mins >= OffMins)
                                lstStartHrs.Text = sysSttime.ToString(tFormats);
                        }
                        else
                            lstStartHrs.Text = lstStartHrs.Items[0].ToString();
                       
                    }
                    node = xmldoc.SelectSingleNode("//GetCalendarTimes/PerosnalCalendar/EndTime");
                    if (node != null)
                    {
                        PerEndTime = node.InnerText;
                        DateTime.TryParse(PerEndTime, out sysSttime);

                        if (Session["timeFormat"].ToString() == "2")
                        {
                            myVRMNet.NETFunctions.ChangeTimeFormat(PerEndTime);
                        }
                        Hour = obj.GetHour(PerEndTime);

                        OffStartHr = obj.GetHour(Session["SystemEndTime"].ToString());

                        if (Hour <= OffStartHr)
                            lstEndHrs.Text = sysSttime.ToString(tFormats);
                        else
                            lstEndHrs.Text = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                        //}
                        //else
                        //{
                        //    ListItem tempLI = new ListItem(sysSttime.ToString(tFormats), sysSttime.ToString(tFormats));
                        //    if (lstStartHrs.Items.Contains(tempLI))
                        //        lstEndHrs.Text = sysSttime.ToString(tFormats);
                        //    else
                        //        lstEndHrs.Text = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                        //}
                    }
                }
                else
                {
                    lstStartHrs.Text = lstStartHrs.Items[0].ToString();
                    EndTime = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                    lstEndHrs.Text = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                    //lstEndHrs.Text = myVRMNet.NETFunctions.GetEndTime(EndTime, Session["timeFormat"].ToString());

                }
                return outXML;
            }
            return outXML;
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    #endregion

    #region SetCalendarTImes
    /// <summary>
    /// SetCalendarTImes
    /// </summary>
    private void SetCalendarTImes()
    {
        try
        {
            string outxml = "";
            inXML = new StringBuilder();
            inXML.Append("<SetCalendarTimes>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append("<PerosnalCalendar>");
            if (officehrDaily.Checked)
                inXML.Append("<ShowPerHrs>1</ShowPerHrs>");
            else
                inXML.Append("<ShowPerHrs>0</ShowPerHrs>");
            inXML.Append("<StartTime>" + DateTime.Parse("06/06/2006 " + myVRMNet.NETFunctions.ChangeTimeFormat(lstStartHrs.Text)).ToShortTimeString() + "</StartTime>"); //DateTime.Parse("06/06/2006 " + myVRMNet.NETFunctions.ChangeTimeFormat(lstStartHrs.Text)).ToString("HH")
            inXML.Append("<EndTime>" + DateTime.Parse("06/06/2006 " + myVRMNet.NETFunctions.ChangeTimeFormat(lstEndHrs.Text)).ToShortTimeString() + "</EndTime>");
            inXML.Append("</PerosnalCalendar>");
            inXML.Append("</SetCalendarTimes>");
            outxml = obj.CallMyVRMServer("SetCalendarTimes", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
        }
        catch (Exception ex)
        {
            log.Trace("SetCalendarTImes: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    //ZD 100157 Ends

}

