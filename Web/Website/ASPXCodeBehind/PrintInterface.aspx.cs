﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class en_PrintInterface : System.Web.UI.Page
{
    myVRMNet.NETFunctions obj;
    private ns_Logger.Logger log;
    protected System.Web.UI.WebControls.PlaceHolder PrintHolder;
    protected System.Web.UI.WebControls.Label ErrorLabel;

    public DataTable dt;
    public System.Web.UI.WebControls.Table dTable;//Added for Graphical reports
    public String reportType = "";
    public DataGrid temp;
    public String titleString = "";

    protected void Page_Load(object sender, EventArgs e)    
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            obj.AccessandURLConformityCheck("PrintInterface.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            if (Session["PrintTable"] != null)
            {
                dt = (DataTable)Session["PrintTable"];
            }

            if (Session["PrintGrid"] != null)
            {
                temp = (DataGrid)Session["PrintGrid"];
            }

            if (Session["reportType"] != null)
            {
                reportType = Session["reportType"].ToString();
            }

            if (Session["titleString"] != null)
            {
                titleString = Session["titleString"].ToString();
            }

            if (Session["PrintAspTable"] != null)//Added for Graphical reports
            {
                dTable = (Table)Session["PrintAspTable"];
            }

            BindDataToPrint();            
        }
        catch(Exception ex)
        {
            //ZD 100263           
            log.Trace("Page_Load" + ex.StackTrace);
            //ErrorLabel.Text = obj.ShowSystemMessage();
            ErrorLabel.Text = "No data to process";
            ErrorLabel.Visible = true;
        }
    }

    #region Bind data to be printed
    /// <summary>
    /// Bind data to be printed
    /// </summary>
    public void BindDataToPrint()
    {
        try
        {
            Control ucPrint = LoadControl(ResolveUrl(@"PrintUtil.ascx"));
            PrintHolder.Controls.Add(ucPrint);
            en_PrintUtil printUt = PrintHolder.Controls[0] as en_PrintUtil;
            printUt.printTable = dt;
            printUt.printGrid = temp;
            printUt.printGrpTable = dTable;//Added for Graphical reports
            printUt.reportType = reportType;
            printUt.titleString = titleString;
            if (reportType == "T")//Added for Graphical reports
                printUt.BindTableGraphicalReports();
            else
                printUt.BuildPrintTable();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    public void BindData()
    {
        String str = "";
        if (temp != null)
        {
            for (Int32 i = 0; i < temp.Columns.Count; i++)
            {
                str = temp.Columns[i].HeaderText;
            }
        }
    }
}
