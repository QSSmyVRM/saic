﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Drawing;


public partial class en_PrintUtil : System.Web.UI.UserControl
{
    protected System.Web.UI.WebControls.Table TablePrint;
    public String titleString = "";
    public DataTable printTable = null;
    public System.Web.UI.WebControls.Table printGrpTable = null;//Added for Graphical Reports
    public DataGrid printGrid = null;
    public String reportType = "";
    protected String tformat = "hh:mm tt";

    
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
        tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
    }

    #region Build Print Table
    /// <summary>
    /// Build the dynamic html code for printing purpose
    /// </summary>
    public void BuildPrintTable()
    {
        try
        {
            if (printTable == null)
                throw new Exception("No data to print");

            if (printTable.Rows.Count == 0)
                throw new Exception("No data to print");
            //else
            //{
            //    if (printTable.Columns.Contains("PK_ID"))
            //        printTable.Columns.Remove("PK_ID");
            //    if (printTable.Columns.Contains("Condenser Air On Temp"))
            //        printTable.Columns.Remove("Condenser Air On Temp");
            //    if (printTable.Columns.Contains("Product Temp"))
            //        printTable.Columns.Remove("Product Temp");
            //}

            TablePrint.BorderStyle = BorderStyle.Solid;
            TablePrint.BorderWidth = Unit.Pixel(1);
            TablePrint.BorderColor = Color.Black;

            TableRow rw = null;
            TableCell cl = null;

            rw = new TableRow();
            rw.BackColor = Color.White;
            rw.Height = Unit.Pixel(40);

            cl = new TableCell();
            cl.Text = "<h3>" + titleString + "</h3>";
            rw.BackColor = Color.White;
            cl.ColumnSpan = printTable.Columns.Count;//FB 2670
            rw.HorizontalAlign = HorizontalAlign.Center;
            rw.Cells.Add(cl);

            TablePrint.Rows.Add(rw);
            rw = null;

            //Header Row 
            rw = new TableRow();
            rw.BackColor = Color.White;

            for (Int32 i = 0; i < printTable.Columns.Count; i++) //FB 2047
            {
                cl = new TableCell();
                cl.CssClass = "tableHeader";
                cl.HorizontalAlign = HorizontalAlign.Left;
                cl.Text = printTable.Columns[i].Caption;

                rw.Cells.Add(cl);
            }

            TablePrint.Rows.Add(rw);
            rw = null;

            BindTable();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    public void BuildPrintGrid()
    {
          TableRow rw = null;
            TableCell cl = null;
        try
        {
            if (printGrid == null)
                throw new Exception("No data to print");

            if (printGrid.Items.Count == 0)
                throw new Exception("No data to print");

            rw = new TableRow();
            rw.BackColor = Color.White;
            for(Int32 i = 0; i< printGrid.Columns.Count ; i++)
            {
                cl = new TableCell();
                cl.CssClass = "tableHeader";
                cl.HorizontalAlign = HorizontalAlign.Left;
                cl.Text = printGrid.Columns[i].HeaderText;

                rw.Cells.Add(cl);
            }
            TablePrint.Rows.Add(rw);

            DataTable dt = null;

            foreach (DataGridItem item in printGrid.Items)
            {
                rw = new TableRow();
                rw.BackColor = Color.White;
                rw.CssClass = "tableBody";

                for (Int32 i = 0; i < dt.Columns.Count; i++)
                {
                    cl = new TableCell();
                    cl.Text = item.Cells[i].Text;
                    rw.Cells.Add(cl);
                }

                TablePrint.Rows.Add(rw);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindTable()
    {
        
        TableRow tr = null;
        TableCell tc = null;
        String stDate = "";
        try
        {
            //FB 2670
            if (reportType == "")
                reportType = "RptDtal";
            foreach (DataRow row in printTable.Rows)
            {
                tr = new TableRow();
                tr.CssClass = "tableBody";
                switch (reportType)
                {
                    case "CAL":
                        tc = new TableCell();
                        tc.Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startdate"]).ToShortDateString());
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = Convert.ToDateTime(row["startdate"]).ToString(tformat);
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = Convert.ToDateTime(row["endDate"]).ToString(tformat);
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["duration"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Room"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Title"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["lastname"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["firstname"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["email"].ToString().Replace("@", "@ ");
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["State"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["City"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["ConfNum"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["meetType"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["connType"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["vidProtocol"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Status"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["description"].ToString();
                        tr.Cells.Add(tc);

                        // Added for FB 1511 START
                        for (Int32 i = 16; i < printTable.Columns.Count; i++)
                        {
                            tc = new TableCell();
                            tc.Text = row[i].ToString();
                            tr.Cells.Add(tc);
                        }
                        // Added for FB 1511 START
                        break;
                    case "DS":
                        tc = new TableCell();
                        tc.Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startDate"]).ToShortDateString());
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = Convert.ToDateTime(row["StartDate"]).ToString(tformat);
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = Convert.ToDateTime(row["endDate"]).ToString(tformat);
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Room"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["title"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["lastname"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["confNum"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Remarks"].ToString();
                        tr.Cells.Add(tc);

                        // Added for FB 1511 START
                        for (Int32 i = 10; i < printTable.Columns.Count; i++)
                        {
                            tc = new TableCell();
                            tc.Text = row[i].ToString();
                            tr.Cells.Add(tc);
                        }
                        // Added for FB 1511 START
                        break;
                    case "PRI":
                        tc = new TableCell();
                        if (stDate != Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString())
                            tc.Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString());
                        else
                            tc.Text = "";
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["PRI"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Title"].ToString();
                        tr.Cells.Add(tc);

                        DateTime blockDate = DateTime.Parse("00:00:00");
                        for (Int32 i = 3; i < printGrid.Columns.Count; i++)
                        {
                            tc = new TableCell();
                            tc.BackColor = (BlockBetween(blockDate, Convert.ToDateTime(row["startdate"]), Convert.ToDateTime(row["enddate"]))) ? Color.Gold : Color.White;
                            blockDate = blockDate.AddMinutes(30);
                            tr.Cells.Add(tc);
                        }

                        stDate = Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString();
                        // Added for FB 1511 START
                        for (Int32 i = 5; i < printTable.Columns.Count; i++)
                        {
                            tc = new TableCell();
                            tc.Text = row[i].ToString();
                            tr.Cells.Add(tc);
                        }
                        // Added for FB 1511 START

                        break;
                    case "RAL":
                        tc = new TableCell();
                        if (stDate != Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString())
                            tc.Text = myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString());
                        else
                            tc.Text = "";
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = Convert.ToDateTime(row["startdate"].ToString()).ToString(tformat);
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = Convert.ToDateTime(row["enddate"].ToString()).ToString(tformat);
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Room"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Title"].ToString();
                        tr.Cells.Add(tc);

                        DateTime blockDate1 = DateTime.Parse("00:00:00");
                        for (Int32 i = 5; i < printGrid.Columns.Count; i++)
                        {
                            tc = new TableCell();
                            tc.BackColor = (BlockBetween(blockDate1, Convert.ToDateTime(row["startdate"]), Convert.ToDateTime(row["enddate"]))) ? Color.Gold : Color.White;
                            blockDate1 = blockDate1.AddMinutes(30);
                            tr.Cells.Add(tc);

                            // Added for FB 1511 START
                            if (i == 52)
                                for (Int32 j = 4; j < printTable.Columns.Count; j++)
                                {
                                    tc = new TableCell();
                                    tc.Text = row[j].ToString();
                                    tr.Cells.Add(tc);
                                }
                            // Added for FB 1511 START
                        }

                        stDate = Convert.ToDateTime(row["startdate"].ToString()).ToShortDateString();
                        break;
                    case "CL":
                        tc = new TableCell();
                        tc.Text = row["TopTier"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["MiddleTier"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["VoIP"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["Room"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["ISDN"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["firstname"].ToString() + " " + row["lastname"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["telephone"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["VTC"].ToString();
                        tr.Cells.Add(tc);
                        break;
                    case "AU":
                        tc = new TableCell();
                        tc.Text = row["AudioConf"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["AudioVideoConf"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["PointToPointConf"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["RoomConf"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["TotalConf"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["TotalDuration"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = "";
                        tr.Cells.Add(tc);
                        break;

                    case "UR":
                        tc = new TableCell();
                        tc.Text = row["name"].ToString();
                        tr.Cells.Add(tc);

                        tc = new TableCell();
                        tc.Text = row["TotalMinutes"].ToString();
                        tr.Cells.Add(tc);
                        break;

                    case "RptDtal": //FB 2155
                        for (int i = 0; i < printTable.Columns.Count; i++)
                        {
                            tc = new TableCell();
                            tc.Text = row[printTable.Columns[i].Caption.ToString()].ToString();
                            tr.Cells.Add(tc);
                        }
                        break;
                }

                TablePrint.Rows.Add(tr);
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Added for Graphical Reports
    public void BindTableGraphicalReports()
    {
        try
        {
            //TablePrint.BorderStyle = BorderStyle.Solid;
            TablePrint.BorderWidth = Unit.Pixel(1);
            //TablePrint.BorderColor = Color.Black;

            TableRow rw = null;
            TableCell cl = null;

            rw = new TableRow();
            rw.BackColor = Color.White;
            rw.Height = Unit.Pixel(40);

            cl = new TableCell();
            //cl.ColumnSpan = printGrpTable.Rows[0].Cells.Count;
            cl.Text = "<h3>" + titleString + "</h3>";
            rw.BackColor = Color.White;
            rw.HorizontalAlign = HorizontalAlign.Center;
            rw.Cells.Add(cl);

            TablePrint.Rows.Add(rw);
            rw = null;
            rw = new TableRow();
            cl = new TableCell();

            cl.Controls.Add(printGrpTable);
            rw.Cells.Add(cl);
            TablePrint.Rows.Add(rw);
            rw = null;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region BlockBetween

    private Boolean BlockBetween(DateTime blockDate, DateTime startT, DateTime endT)
    {
        bool isBlock = false;
        try
        {
            DateTime stDate;
            DateTime etDate;

            stDate = DateTime.Parse(startT.ToShortTimeString());
            etDate = DateTime.Parse(endT.ToShortTimeString());

            if (blockDate == stDate || (blockDate > stDate && blockDate < etDate))
                isBlock = true;
            else
                isBlock = false;

        }
        catch (Exception ex)
        {
            throw ex;
        }
        return isBlock;
    }

    #endregion
}
