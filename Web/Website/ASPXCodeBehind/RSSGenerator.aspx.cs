/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;


namespace ns_MyVRM
{

    public partial class RSSGenerator : System.Web.UI.Page
    {
    
         #region Private Data Members

         String UserID = "";
         String Cmdid = "";
         DataTable dt = null;
         String webSiteUrl = "";
         bool isVal = false;
         String myvrmser = "";
         protected String webLink = "";
         bool publicConf = false;
         ArrayList confIDList = new ArrayList();
      
        
         #endregion

         #region Protected Data Members

         protected System.Web.UI.WebControls.LinkButton RSSLink;
         protected System.Web.UI.WebControls.Repeater Repeater1;
       

         myVRMNet.NETFunctions obj;
         myVRMNet.RSSXMLGenerator RSSObj = null; 
         ns_Logger.Logger log;
         protected String language = "";//FB 1830
         MyVRMNet.Util utilObj;//FB 2236

         #endregion

         #region Constructor
            public RSSGenerator()
            {
                obj = new myVRMNet.NETFunctions();
                log = new ns_Logger.Logger();
                utilObj = new MyVRMNet.Util(); //FB 2236
            }
        #endregion

         #region  Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("RSSGenerator.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if (Request.QueryString["m"] != null)
                {
                    if (Request.QueryString["m"].ToString().Equals("p"))
                    {
                         publicConf  = true;
                         Session.Add("userID", "11");
                    }
                }
                if (Request.QueryString["u"] != null)
                {
                    if (Request.QueryString["u"].ToString() != "")
                    {
                        Session.Add("UserID", Request.QueryString["u"].ToString());
                    }
                }
                //RSS Fix -- Start
                if (Request.QueryString["OrgID"] != null)
                {
                    if (Request.QueryString["OrgID"].ToString() != "")
                    {
                        Session.Add("organizationID", Request.QueryString["OrgID"].ToString());
                    }
                }
                //RSS Fix -- End

                //FB 1830 - Starts
                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 1830 - End

                if (!IsPostBack)
                    BindData();
                
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }

        }
        #endregion

         #region BindData
        private void BindData()
        {
            String outXML = "";
            ArrayList cmdList = null;
            try
            {
                if (RSSObj == null)
                    RSSObj = new myVRMNet.RSSXMLGenerator();
                if (cmdList == null)
                    cmdList = new ArrayList();

                string orgElement = obj.OrgXMLElement(); //organization module

                webLink = Server.MapPath(".");

                if (Application["MyVRMServer_ConfigPath"] == null)
                    myvrmser = "C:\\VRMSchemas_v1.8.3\\";
                else
                    myvrmser = Application["MyVRMServer_ConfigPath"].ToString();

                if (publicConf)
                {
                    Session.Add("feedTitle", "myVRM Public Feeds");
                }
                else
                {
                    Session.Add("feedTitle", "myVRM Feeds");
                }

                dt = RSSTable();

                if (Session["userID"] != null)
                {
                    if (Session["userID"].ToString() != "")
                        UserID = Session["userID"].ToString();

                    if (publicConf)
                    {
                        cmdList.Add("Public");
                    }
                    else
                    {
                        cmdList.Add("Ongoing");
                        cmdList.Add("Future");
                        cmdList.Add("Pending");
                    }
                   

                    for (int r = 0; r < cmdList.Count; r++)
                    {
                        Cmdid = cmdList[r].ToString();
                        if (publicConf)
                        {
                            outXML = RSSObj.SearchConferenceOutXML(UserID, Cmdid, "P", orgElement); //organization module
                        }
                        else
                        {
                            outXML = RSSObj.SearchConferenceOutXML(UserID, Cmdid, "", orgElement); //organization module
                        }

                        if (outXML.IndexOf("<error>") < 0)
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");

                            if (nodes.Count > 0)
                            {
                                isVal = true;
                                GenerateRSSXML(nodes);

                            }
                        }
                    }
                }
                else
                {
                    Repeater1.DataSource = null;
                    Repeater1.DataBind();

                    LogOnDetails();
                }

                if (!isVal)
                {
                    LogOnDetails();
                }
            }
            catch (Exception ex)
            { 
                log.Trace(ex.Message); 
            }
        }
        #endregion

         #region GenerateRSSXML
        private void GenerateRSSXML(XmlNodeList nodes)
        {
             
            try
            {

                foreach (XmlNode node in nodes)
                {
                    string confHost = "";
                    string confHostEmail = "";
                    string webURL = "";
                    string isrecur = "";
                    string confid = "";

                                     
                    if (node.SelectSingleNode("IsRecur") != null)
                        if (node.SelectSingleNode("IsRecur").InnerText != "")
                            isrecur = node.SelectSingleNode("IsRecur").InnerText;
                    if (isrecur == "1")
                    {
                        if (node.SelectSingleNode("ConferenceID") != null)
                            if (node.SelectSingleNode("ConferenceID").InnerText != "")
                                confid = node.SelectSingleNode("ConferenceID").InnerText;

                        if (confid.IndexOf(",") > 0)
                        {
                           String[] confID =  confid.Split(',');
                           confid = confID[0];
                        }
                    }
                    else
                    {
                        if (node.SelectSingleNode("ConferenceID") != null)
                            if (node.SelectSingleNode("ConferenceID").InnerText != "")
                                confid = node.SelectSingleNode("ConferenceID").InnerText;
                    }

                    DataRow dr = dt.NewRow();

                    if (node.SelectSingleNode("WebsiteURL") != null)
                        if (node.SelectSingleNode("WebsiteURL").InnerText != "")
                            webURL = node.SelectSingleNode("WebsiteURL").InnerText;

                    int uniqueID = 0;
                    if (node.SelectSingleNode("ConferenceUniqueID") != null)
                        if (node.SelectSingleNode("ConferenceUniqueID").InnerText != "")
                            uniqueID = Convert.ToInt32(node.SelectSingleNode("ConferenceUniqueID").InnerText);


                    if (confIDList.Contains(uniqueID))
                    {
                        continue;
                    }

                    //dr["Title"] = uniqueID + " " +  "#" + " " + node.SelectSingleNode("ConferenceName").InnerText;
                    dr["Title"] = "#" + " " + uniqueID + " " + node.SelectSingleNode("ConferenceName").InnerText; //FB 1619
                   
                    confIDList.Add(uniqueID);
                    string url = "";
                    //RSS Fix -- Start
                    String orgID = "";

                    if (Session["organizationID"] != null)
                    {
                        if (Session["organizationID"].ToString() != "")
                        {
                            orgID = Session["organizationID"].ToString();
                        }
                    }
                    if (webURL.Contains("genlogin.aspx"))//Login Management
                    {
                        if (publicConf)
                        {
                            dr["Link"] = webURL + "/en/Manageconference.aspx?hf=1&amp;confid=" + confid + "&amp;orgID=" + orgID;
                        }
                        else
                        {
                            dr["Link"] = webURL + "?rss=" + confid + "&amp;orgID=" + orgID;
                        }
                        //url = webURL.Remove("genlogin.aspx") + "/mirror/styles/main.css";//Login Management
                    }
                    else
                    {
                        if (publicConf)
                        {
                            dr["Link"] = webURL + "/" + language + "/Manageconference.aspx?hf=1&amp;confid=" + confid + "&amp;orgID=" + orgID;//FB 1830
 
                        }
                        else
                        {
                            dr["Link"] = webURL + "/" + language + "/genlogin.aspx?rss=" + confid + "&amp;orgID=" + orgID;//Login Management //FB 1830
                        }
                       
                    }
                    //RSS Fix -- End
                    Session.Add("ImageURL", url);

                    if (Cmdid.Trim() == "Future")
                    {
                        dr["category"] = "Scheduled";
                    }
                    else
                    {
                        dr["category"] = Cmdid;
                    }

                    if(node.SelectSingleNode("ConferenceHost") != null)
                        if(node.SelectSingleNode("ConferenceHost").InnerText != "")
                             confHost =  node.SelectSingleNode("ConferenceHost").InnerText;

                    if (node.SelectSingleNode("ConferenceHostEmail") != null)
                        if (node.SelectSingleNode("ConferenceHostEmail").InnerText != "")
                            confHostEmail = node.SelectSingleNode("ConferenceHostEmail").InnerText;

                    dr["ConfHostEmail"] = confHost + "&lt;" + confHostEmail + "&gt;";

                   
                   // webSiteUrl = webURL + "/en/genlogin.aspx";
                    webSiteUrl = webURL;
                    
                   // Session.Add("feedLink", webSiteUrl);

                    dr["Description"] = obj.GetTranslatedText("Location(s)") + " : ";//FB 1619//FB 2272

                    String location = "";
                    XmlNodeList locnodes = null;

                   locnodes = node.SelectNodes("Location/Selected");

                    if (locnodes.Count > 0)
                    {
                        location = locnodes.Item(0).SelectSingleNode("Name").InnerText;
                        foreach (XmlNode locnode in locnodes)
                        {
                            if (location != locnode.SelectSingleNode("Name").InnerText)
                                location += "," + locnode.SelectSingleNode("Name").InnerText;
                        }
                    }
                    else
                    {
                        location = obj.GetTranslatedText("There is no location for this conference");
                    }

                    dr["Description"] += location;

                    if (node.SelectSingleNode("ConfDescription").InnerText != "")
                        dr["Description"] += "&lt;br&gt;" + obj.GetTranslatedText("Description") + " : " + utilObj.ReplaceOutXMLSpecialCharacters((node.SelectSingleNode("ConfDescription").InnerText), 3);//FB 2236
                    else
                        dr["Description"] += "&lt;br&gt;" + obj.GetTranslatedText("Description") + " : N/A";
                   
                    //FB 1619
                    dr["Description"] += "&lt;br&gt;" + obj.GetTranslatedText("Conference Status") + " : " + Cmdid ;

                    dr["Description"] += "&lt;br&gt;" + obj.GetTranslatedText("Host") + " : " + confHost + " (" + confHostEmail + ")";

                    // DateTime confDate = Convert.ToDateTime(node.SelectSingleNode("ConferenceDateTime").InnerText);
					//FB 1619
                    String pubDate = node.SelectSingleNode("SetupTime").InnerText.ToString();
					 String endDate = node.SelectSingleNode("TearDownTime").InnerText.ToString();
                     //pubDate = confDate.ToShortDateString() + " " + pubDate;
                     DateTime date = Convert.ToDateTime(pubDate);

                     dr["PubDate"] = date.ToString("ddd, dd MMM yyyy HH:mm");
                     //FB 1619
                     DateTime date1 = Convert.ToDateTime(endDate);
                     String startTime = date.ToString("ddd, dd MMM yyyy hh:mm tt");
                     String endTime = date1.ToString("ddd, dd MMM yyyy hh:mm tt");

                     dr["GuidURL"] = webURL + "/" + language + "/Manageconference.aspx?t=hf&amp;confid=" + confid;//FB 1830

                    dr["WebLink"] = webURL;

                    dr["Description"] += "&lt;br&gt;" + obj.GetTranslatedText("Conference Start") + " : " + startTime; //FB 1619

                    dr["Description"] += "&lt;br&gt;" + obj.GetTranslatedText("Conference End") + " : " + endTime; //FB 1619

                    dt.Rows.Add(dr);
                }

                DataView dv = new DataView(dt);

                dv.Sort = "PubDate";

                Repeater1.DataSource = dt;
                Repeater1.DataBind();

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

         #region RSSTable

        protected DataTable RSSTable()
        {
            try
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("Title");
                dt.Columns.Add("Link");
                dt.Columns.Add("Description");
                dt.Columns.Add("category");
                dt.Columns.Add("ConfHostEmail");
                dt.Columns.Add("PubDate");
                dt.Columns.Add("GuidURL");
                dt.Columns.Add("WebLink");
               

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

         #region LogOnDetails

        private void LogOnDetails()
        {
            isVal = true;
           
            try
            {
                webSiteUrl = RSSObj.GetSysSettingDetails(myvrmser);

                DataRow dr = dt.NewRow();
                dr["Title"] = "Account Log On";
                if (!webSiteUrl.Contains("genlogin.aspx"))//Login Management
                {
                    webSiteUrl = webSiteUrl + "/en/genlogin.aspx";//Login Management
                }
              
                dr["Link"] = webSiteUrl + "?rsslogon=1";
                dr["category"] = "Log On";
                dr["ConfHostEmail"] = "Support@myvrm.com";
                dr["PubDate"] = DateTime.Now.ToString("ddd, dd MMM yyyy HH:mm:ss");
                dr["GuidURL"] = webSiteUrl + "?unique=?" + DateTime.Now;
                dr["Description"] = "&lt;a href= " + webSiteUrl + "?rsslogon=1 +  &gt; Click here to log on &lt;/a&gt; to your myVRM account to access the RSS feed. If your RSS reader does not support cookies, add this to the end of the feed link to store your myVRM user name and password: Email=youremailaddress and Password=yourPassword";

                dt.Rows.Add(dr);

                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }

        }
        #endregion
    }
}
