/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Globalization;
using System.Text;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.XtraGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using System.IO;
using System.Drawing;
using DevExpress.XtraPrintingLinks;


namespace MyVRMNet
{
    public partial class en_Report_Details : System.Web.UI.Page
    {
        #region Private Members

        protected DevExpress.XtraCharts.Web.WebChartControl webChartCtrl;
        protected DevExpress.Web.ASPxGridView.ASPxGridView gvReportDetails;
        protected DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter gridExport;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox DrpChrtType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstCountries;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstStates;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstBridges;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox DrpAllType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstConfType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox DrpLocations;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox DrpMCU;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstReport;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox CmbStrtTime;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox CmbEndTime;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox ReportsList;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox ConfScheRptDivList;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstUsageReports;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstOrg; //FB 2155
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstResourseType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstUserType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstRoomType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstConfRoomRpt;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstDailyMonthly;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstStatusType;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstWeeklyStart;//FB 2343
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstWeeklyEnd;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstMonthlyStart;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstMonthlyEnd;

        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstWStartYear;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstWStartMonth;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstWEndYear;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstWEndMonth;

        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstMStartYear;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstMStartMonth;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstMEndYear;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstMEndMonth;

        protected DevExpress.Web.ASPxEditors.ASPxTextBox txtZipCode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChartPrint; //FB 2205
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnReportType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnInputType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnInputValue;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDateFrom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDateTo;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDetailsView;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWebChart;
        protected System.Web.UI.HtmlControls.HtmlTable SummaryTable;
        protected System.Web.UI.HtmlControls.HtmlTable DetailsHeadingTable;
        protected System.Web.UI.HtmlControls.HtmlGenericControl DetailsDiv;

        protected System.Web.UI.WebControls.CheckBoxList cblRoom;
        protected System.Web.UI.WebControls.TextBox txtCusDateFrm;
        protected System.Web.UI.WebControls.TextBox txtCusDateTo;
        protected System.Web.UI.WebControls.TextBox txtStartDate;
        protected System.Web.UI.WebControls.TextBox txtEndDate;
        protected System.Web.UI.WebControls.TextBox txtSelectedDate;//FB 2155
        protected System.Web.UI.WebControls.TextBox txtRooms;//FB 2289
        protected System.Web.UI.WebControls.Label lblHeading;
        protected System.Web.UI.WebControls.Label lblOrgName;
        protected System.Web.UI.WebControls.Label lblErrLabel;
        protected System.Web.UI.WebControls.CheckBox chkSelectall;
        protected System.Web.UI.WebControls.Label PtPtCnt;
        protected System.Web.UI.WebControls.Label MultiPtCnt;
        protected System.Web.UI.WebControls.Label VTCMtTotCnt;
        protected System.Web.UI.WebControls.Label GenerateText;
        protected System.Web.UI.WebControls.Table VTCTotal;
        protected System.Web.UI.WebControls.Table VTCDetails;
        protected System.Web.UI.WebControls.TableRow FirstRow;

        protected System.Web.UI.HtmlControls.HtmlForm frmMenu;
        protected System.Web.UI.HtmlControls.HtmlTableRow UserReportsCell;

        protected System.Web.UI.WebControls.Button btnAdvRep; //FB 2205
        protected System.Web.UI.WebControls.Button btnMCCRpt; //FB 2047
        protected System.Web.UI.HtmlControls.HtmlTable tblmonthlyRoom; //FB 2343
        protected System.Web.UI.HtmlControls.HtmlTable tblweeklyRoom;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divReportDetais; 
        protected System.Web.UI.HtmlControls.HtmlGenericControl divWeeklyReport;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divMonthlyReport;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divNoData;
        // FB 2501 Starts        
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstConferenceTZ;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox lstEndpoint;
        protected DevExpress.Web.ASPxEditors.ASPxComboBox QueryType;
        protected System.Web.UI.WebControls.TextBox ConferenceName;
        protected System.Web.UI.WebControls.TextBox CallURL;
        protected System.Web.UI.WebControls.TextBox txtConferenceUniqueID;
        protected System.Web.UI.WebControls.TextBox txtApprover4;
        protected System.Web.UI.WebControls.TextBox txtApprover6;
        protected System.Web.UI.WebControls.TextBox txtRVNOCOperator;//FB 2670
        protected System.Web.UI.WebControls.TextBox hdnApprover4;
        protected System.Web.UI.WebControls.TextBox hdnApprover6;
        protected System.Web.UI.WebControls.TextBox hdnVNOCOperator;//FB 2670
       
        //FB 2501 Ends

        //FB 2501 CDR Reports - Start
        protected System.Web.UI.WebControls.Label lbl_McuName;
        protected System.Web.UI.WebControls.Label lbl_McuType;
        protected System.Web.UI.WebControls.Label lbl_IPAddress;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUDetails;
        
       //FB 2501 CDR Reports - End
        
         

        private ns_Logger.Logger log;
        private myVRMNet.NETFunctions obj;
        private myVRMNet.Reports objt;
        myVRMNet.DateUtilities dtObj;
        MyVRMNet.Util utilObj;

        protected String format = "dd/MM/yyyy";
        protected String tformat = "hh:mm tt";
        XmlDocument xmlDoc = null;
        StringBuilder InXML = null;
        String outXML = "";
        int usrID = 11;
        DataSet ds = null;
        DataTable rptTable = new DataTable();
        DataTable rptCopyTable = new DataTable();
        String rptTitle = "";
        String tmzone = "";
        CultureInfo cInfo = null;
        decimal tmpVal = 0;
        int crossAccess = 0, admin = 0;
        protected DataTable table = null;
        protected DataTable table1 = null;
        String roomIds = "";
        Int32 workingHours = 0; //FB 2343
        Int32 startFrom = 1, endTo = 1;
        DataSet ds1 = new DataSet();
        //FB 2501 starts
        String selTZ = "-1";
        String TempSubmitClick = null;        
        //FB 2501 ends

        protected int EnableAdvancedReport = 0; //FB 2593

        #endregion

        #region Constructor

        public en_Report_Details()
        {
            obj = new myVRMNet.NETFunctions();
            objt = new myVRMNet.Reports();
            log = new ns_Logger.Logger();
            dtObj = new myVRMNet.DateUtilities();
            utilObj = new MyVRMNet.Util();
        }

        #endregion

        #region Page_init
        /// <summary>
        /// Page_init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_init(object sender, EventArgs e)
        {
            if (Session["timeFormat"] != null)
            {
                if (Session["timeFormat"].ToString() != "")
                    if (Session["timeFormat"].ToString() == "0")
                        tformat = "HH:mm";
                    else if (Session["timeFormat"].ToString() == "2")//FB 2588
                        tformat = "HHmmZ";
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }
            //FB 2343
            if (Session["WorkingHours"] != null)
            {
                if (Session["WorkingHours"].ToString() != "")
                    Int32.TryParse(Session["WorkingHours"].ToString(), out workingHours);
            }

            if (IsPostBack && Session["ReportsXML"] != null && hdnValue.Value.Trim() == "")
            {
                BindData();
            }

        }
        #endregion

        #region Page_Load

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ReportDetails.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                lblErrLabel.Text = "";

                if (Session["userID"] != null)
                {
                    if (Session["userID"].ToString() != "")
                        Int32.TryParse(Session["userID"].ToString(), out usrID);
                }

                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString() != "")
                        Int32.TryParse(Session["admin"].ToString(), out admin);

                    if (admin != 2) //FB 2205
                        btnAdvRep.Visible = false;
                }

                //FB 2593 Starts
                
                if (Session["EnableAdvancedReport"] != null)
                {
                    if (!string.IsNullOrEmpty(Session["EnableAdvancedReport"].ToString()))
                    {
                        int.TryParse(Session["EnableAdvancedReport"].ToString(), out EnableAdvancedReport);
                    }
                }

                //FB 2047
                //if (admin == 2 && crossAccess == 0) 
                if (EnableAdvancedReport == 1)
                    btnMCCRpt.Visible = true;
                else
                    btnMCCRpt.Visible = false;

                //FB 2593 End

                if (Session["UsrCrossAccess"] != null)
                {
                    if (Session["UsrCrossAccess"].ToString() != "")
                        Int32.TryParse(Session["UsrCrossAccess"].ToString(), out crossAccess);
                }

                if (Session["organizationName"] != null)
                {
                    if (Session["organizationName"].ToString() != "")
                        lblOrgName.Text = Session["organizationName"].ToString();
                }

                if (Session["timezoneID"] != null)
                    tmzone = Session["timezoneID"].ToString();//drpTimeZone.SelectedValue;
                else
                    tmzone = "26";

                //FB 2343
                if (Session["WorkingHours"] != null)
                {
                    if (Session["WorkingHours"].ToString() != "")
                        Int32.TryParse(Session["WorkingHours"].ToString(), out workingHours);
                }
                //FB 2598 Start
                string CDR = "";
                if (Session["EnableCDR"] != null)
                {
                    if (Session["EnableCDR"].ToString() != "")
                        CDR = Session["EnableCDR"].ToString();

                    if (CDR == "0")
                        lstResourseType.Items.Remove(lstResourseType.Items.FindByValue("6"));
                }
                //FB 2598 End

                if (!IsPostBack)
                {
                    if (obj != null)
                    {
                        Session["ReportsXML"] = null;
                        CmbStrtTime.Items.Clear();
                        CmbEndTime.Items.Clear();
                        objt.BindTimeToListBox(CmbStrtTime, false, false);
                        objt.BindTimeToListBox(CmbEndTime, false, false);
                        if (format.ToLower().Trim() == "dd/mm/yyyy")
                        {
                            txtStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.ToShortDateString());
                            txtEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.AddDays(1).ToShortDateString());
                            txtCusDateFrm.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.ToShortDateString());
                            txtCusDateTo.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.AddDays(1).ToShortDateString());
                            txtSelectedDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.ToShortDateString());//FB 2155
                        }
                        else
                        {
                            txtStartDate.Text = DateTime.Today.ToShortDateString();
                            txtEndDate.Text = DateTime.Today.ToShortDateString();
                            txtCusDateFrm.Text = DateTime.Today.ToShortDateString();
                            txtCusDateTo.Text = DateTime.Today.ToShortDateString();
                            txtSelectedDate.Text = DateTime.Today.ToShortDateString();//FB 2155
                        }
                        CmbStrtTime.Text = ((CmbStrtTime.SelectedIndex < 0) ? DateTime.Parse("12:00 AM").ToString(tformat) : CmbStrtTime.Text);
                        CmbEndTime.Text = ((CmbEndTime.SelectedIndex < 0) ? DateTime.Parse("11:00 PM").ToString(tformat) : CmbEndTime.Text); //FB 2501 

                        objt.GetCountryCodes(lstCountries);
                        lstCountries.SelectedIndex = lstCountries.Items.FindByValue("225").Index; // United States
                        objt.GetCountryStates(lstStates, lstCountries.Value.ToString());
                        lstStates.SelectedIndex = lstStates.Items.FindByValue("34").Index; // NY
                        objt.BindBridges(lstBridges);
                        GetLocations();
                        objt.BindOrganizationNames(lstOrg); //FB 2155

                        chkSelectall.Attributes.Add("onclick", "javascript:fnSelectAll(this,'" + cblRoom.ClientID + "');");
                        cblRoom.Attributes.Add("onclick", "javascript:fnDeselectAll(this,'" + chkSelectall + "');");

                        //FB 2501 starts
                        InXML = new StringBuilder();
                        lstConferenceTZ.Items.Clear();
                        objt.GetTimezones(lstConferenceTZ, ref selTZ);
                        objt.GetAllEndpoints(lstEndpoint);
                        //FB 2501 Ends
                    }
                    if (admin >= 2) //FB 2670
                    {
                        ReportsList.Items.Add(obj.GetTranslatedText("Organization Reports"), "6");
                        if (crossAccess == 1)
                            ReportsList.Items.Add(obj.GetTranslatedText("Site Reports"), "7");
                    }
                    //BindData();
                    UsrMenuMask();
                }
                //FB 2501 starts
                else
                    selTZ = lstConferenceTZ.Value.ToString();
                
		        //FB 2501 ends
                //To print the webchart - //FB 2205
                if (hdnChartPrint.Value == "p" && ReportsList.Value.ToString() == "4" && DrpChrtType.Value.ToString() != "3")
                {
                    BindData();
                    hdnChartPrint.Value = "";
                }
            }
            catch (Exception ex)
            {
                lblErrLabel.Visible = true;//ZD 100263                
                log.Trace("Page_Load" + ex.StackTrace);
                lblErrLabel.Text = obj.ShowSystemMessage();
            }
        }

        #endregion

        #region BindData

        protected void BindData()
        {
            try
            {
                xmlDoc = new XmlDocument();
                InXML = new StringBuilder();
                String orgID = "";
                string strParam = "";
                String pivotInput = "";
                String SelectedYears = "";
                Int32 selectedYear = DateTime.Now.Year, monthweekCount = 0;
                if (ReportsList.Value.ToString() == "7")
                    orgID = lstOrg.SelectedItem.Value.ToString();

                bool rptt = true;
                #region Weekly and Monthly Room Usage
                if (ReportsList.Value.ToString() == "3" && (lstUsageReports.Value.ToString() == "3" || lstUsageReports.Value.ToString() == "4"))
                {

                    StringBuilder inXML = new StringBuilder();
                    String outXML1 = "";
                    String startFrom1 = "", endTo1 = "";
                    DateTime startDate = new DateTime();
                    DateTime endDate = new DateTime();

                    DataRow[] detailRow = null;
                    String strFilter = "";
                    Int32 wkgDays = 0;

                    String monthName = "";

                    String pivotParam = "", pivotParam1 = "", pivotParam2 = "", pivotParam3 = "";

                    int startYear = 0, endYear = 0, yearCount = 0;

                    strParam = "Name ";

                    int startWeek = 0, endWeek = 0;

                    if (ds1 == null || ds1.Tables.Count == 0)
                    {
                        inXML = new StringBuilder();
                        xmlDoc = new XmlDocument();
                        inXML.Append("<GetUsageReports>");
                        inXML.Append(obj.OrgXMLElement());
                        inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                        inXML.Append("<ReportType>MY</ReportType>");
                        inXML.Append("</GetUsageReports>");

                        outXML1 = obj.CallMyVRMServer("GetUsageReports", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                        if (outXML1.IndexOf("<error>") < 0)
                        {
                            xmlDoc.LoadXml(outXML1);
                            ds1.ReadXml(new XmlNodeReader(xmlDoc));
                        }
                    }
                    DataTable detailsTable1 = ds1.Tables[0];
                    String ss = detailsTable1.Rows[0][0].ToString();
                    if (ss == "")
                    {
                        trDetailsView.Attributes.Add("style", "display:block;");
                        divWeeklyReport.Attributes.Add("style", "display: none;");
                        divMonthlyReport.Attributes.Add("style", "display: none;");
                        divReportDetais.Attributes.Add("style", "display:none;");
                        divNoData.Attributes.Add("style", "display: block;");
                        rptt = false;
                    }
                    else
                    {
                        if (lstUsageReports.Value.ToString() == "3")
                        {
                            startWeek = Convert.ToInt32(lstWStartMonth.Value);
                            endWeek = Convert.ToInt32(lstWEndMonth.Value);

                            Int32 wknumber = 0, ii = 0;

                            startYear = Convert.ToInt32(lstWStartYear.Text);
                            endYear = Convert.ToInt32(lstWEndYear.Text);

                            for (int k = startYear; k <= endYear; k++)
                            {
                                if (k == startYear)
                                    SelectedYears = startYear.ToString();
                                else
                                    SelectedYears += "," + k.ToString();
                                yearCount = yearCount + 1;
                            }
                            monthweekCount = yearCount * 52; //Calculating week count for selected years

                            if (yearCount > 1)
                                ii = (52 * (yearCount - 1) - startWeek) + endWeek;
                            else
                                ii = endWeek - startWeek;

                            wknumber = startWeek;

                            int selectYear = startYear;

                            detailRow = null;
                            strFilter = "";
                            wkgDays = 0;
                            if (ds1 == null || ds1.Tables.Count == 0)
                            {
                                inXML = new StringBuilder();
                                xmlDoc = new XmlDocument();
                                inXML.Append("<GetUsageReports>");
                                inXML.Append(obj.OrgXMLElement());
                                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                                inXML.Append("<ReportType>WY</ReportType>");
                                inXML.Append("<SelectedYear>" + SelectedYears + "</SelectedYear>");
                                inXML.Append("</GetUsageReports>");

                                outXML1 = obj.CallMyVRMServer("GetUsageReports", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                                if (outXML1.IndexOf("<error>") < 0)
                                {
                                    xmlDoc.LoadXml(outXML1);
                                    ds1.ReadXml(new XmlNodeReader(xmlDoc));
                                }
                            }
                            for (int d = 0; d <= ii; d++)
                            {
                                strFilter = "id ='" + wknumber + "' and CurrentYear ='" + selectYear + "'";
                                detailRow = ds1.Tables[3].Select(strFilter);

                                if (detailRow != null && detailRow.Length > 0)
                                {
                                    wkgDays = Convert.ToInt32(detailRow[0]["WeekWorkingDays"].ToString()) * workingHours;
                                }

                                strParam += ", isnull(Max([Week" + wknumber + selectYear + "]),0) as [Confs" + wknumber + "], isnull(max([Week" + wknumber + selectYear + "1]),0) as [Total Hours" + wknumber + "]";
                                strParam += ", isnull(Max([Week" + wknumber + selectYear + "2])," + wkgDays + ") as [Avail.Hours" + wknumber + "]";
                                strParam += ", isnull(Max([Week" + wknumber + selectYear + "3]),0) as [Usage" + wknumber + "]";

                                if (d == 0)
                                    pivotParam = "[Week" + wknumber + selectYear + "]";
                                else
                                    pivotParam += ",[Week" + wknumber + selectYear + "]";

                                if (d == 0)
                                    pivotParam1 = "[Week" + wknumber + selectYear + "1]";
                                else
                                    pivotParam1 += ",[Week" + wknumber + selectYear + "1]";

                                if (d == 0)
                                    pivotParam2 = "[Week" + wknumber + selectYear + "2]";
                                else
                                    pivotParam2 += ",[Week" + wknumber + selectYear + "2]";

                                if (d == 0)
                                    pivotParam3 = "[Week" + wknumber + selectYear + "3]";
                                else
                                    pivotParam3 += ",[Week" + wknumber + selectYear + "3]";

                                wknumber = wknumber + 1;
                                if (wknumber == 52)
                                {
                                    selectYear = selectYear + 1;
                                    wknumber = 1;
                                }
                            }
                            pivotInput = pivotParam + ";" + pivotParam1 + ";" + pivotParam2 + ";" + pivotParam3;

                        }
                        else
                        {
                            startFrom1 = lstMStartMonth.Value + "/01/" + lstMStartYear.Text;
                            endTo1 = lstMEndMonth.Value + "/01/" + lstMEndYear.Text;

                            startDate = Convert.ToDateTime(startFrom1);
                            endDate = Convert.ToDateTime(endTo1);

                            startYear = startDate.Year;
                            endYear = endDate.Year;

                            for (int k = startYear; k <= endYear; k++)
                            {
                                if (k == startYear)
                                    SelectedYears = startYear.ToString();
                                else
                                    SelectedYears += "," + k.ToString();
                                yearCount = yearCount + 1;
                            }

                            monthweekCount = yearCount * 12; //Calculating month count for selected years

                            detailRow = null;
                            strFilter = "";
                            wkgDays = 0;
                            if (ds1 == null || ds1.Tables.Count == 0)
                            {
                                inXML = new StringBuilder();
                                xmlDoc = new XmlDocument();
                                inXML.Append("<GetUsageReports>");
                                inXML.Append(obj.OrgXMLElement());
                                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                                inXML.Append("<ReportType>MY</ReportType>");
                                inXML.Append("<SelectedYear>" + SelectedYears + "</SelectedYear>");
                                inXML.Append("</GetUsageReports>");

                                outXML1 = obj.CallMyVRMServer("GetUsageReports", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                                if (outXML1.IndexOf("<error>") < 0)
                                {
                                    xmlDoc.LoadXml(outXML1);
                                    ds1.ReadXml(new XmlNodeReader(xmlDoc));
                                }
                            }

                            for (DateTime dd = startDate; dd <= endDate; dd = dd.AddMonths(1))
                            {

                                strFilter = "id ='" + dd.Month + "' and CurrentYear ='" + dd.Year + "'";
                                detailRow = ds1.Tables[1].Select(strFilter);//FB 2343

                                if (detailRow != null && detailRow.Length > 0)
                                {
                                    wkgDays = Convert.ToInt32(detailRow[0]["MonthWorkingDays"].ToString()) * workingHours;//FB 2343
                                }

                                monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dd.Month);

                                strParam += ", isnull(Max([" + monthName + dd.Year + "]),0) as [Confs" + dd.Month + "], isnull(max([" + monthName + dd.Year + "1]),0) as [Total Hours" + dd.Month + "]";
                                strParam += ", isnull(Max([" + monthName + dd.Year + "2])," + wkgDays + ") as [Avail.Hours" + dd.Month + "]";
                                strParam += ", isnull(Max([" + monthName + dd.Year + "3]),0) as [Usage" + dd.Month + "]";

                                if (dd == startDate)
                                    pivotParam = "[" + monthName + dd.Year + "]";
                                else
                                    pivotParam += ",[" + monthName + dd.Year + "]";

                                if (dd == startDate)
                                    pivotParam1 = "[" + monthName + dd.Year + "1]";
                                else
                                    pivotParam1 += ",[" + monthName + dd.Year + "1]";

                                if (dd == startDate)
                                    pivotParam2 = "[" + monthName + dd.Year + "2]";
                                else
                                    pivotParam2 += ",[" + monthName + dd.Year + "2]";

                                if (dd == startDate)
                                    pivotParam3 = "[" + monthName + dd.Year + "3]";
                                else
                                    pivotParam3 += ",[" + monthName + dd.Year + "3]";
                            }
                            pivotInput = pivotParam + ";" + pivotParam1 + ";" + pivotParam2 + ";" + pivotParam3;
                        }
                    }
                }
                #endregion
                if (rptt == true)
                {
                    if (Session["ReportsXML"] == null)
                    {
                        if (!GetReportParams()) //ZD 100263_SQL
                            return;
						//FB 2501 
                        if (TempSubmitClick == "btnConferenceUniqueID")
                        {
                            InXML.Append("<GetUsageReports>");
                            InXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                            if (Session["organizationID"].ToString() != "")
                                InXML.Append("<organizationID>" + Session["organizationID"].ToString() + "</organizationID>");
                            else
                                InXML.Append(obj.OrgXMLElement());

                            //ZD 100263_SQL Starts
                            int confNumName = 0;
                            int.TryParse(txtConferenceUniqueID.Text.Trim(), out confNumName);
                            InXML.Append("<UniqueID>" + confNumName + "</UniqueID>");
                            //ZD 100263_SQL End
                            
                            InXML.Append("<ReportType>CUS</ReportType>"); //Conference UniqueID Search
                            InXML.Append("</GetUsageReports>");                            
                        }
                        else
                        {
                            InXML.Append("<GetUsageReports>");
                            if (orgID != "")
                                InXML.Append("<organizationID>" + orgID + "</organizationID>");
                            else
                                InXML.Append(obj.OrgXMLElement());
                            InXML.Append("<DateFrom>" + hdnDateFrom.Value + "</DateFrom>");
                            InXML.Append("<DateTo>" + hdnDateTo.Value + "</DateTo>");
                            InXML.Append("<DateFormat>" + format + "</DateFormat>");
                            InXML.Append("<TimeFormat>" + tformat + "</TimeFormat>"); //FB 2588
                            InXML.Append("<UserID>" + usrID + "</UserID>");
                            InXML.Append("<ReportType>" + hdnReportType.Value + "</ReportType>");
                            InXML.Append("<InputType>" + hdnInputType.Value + "</InputType>");
                            InXML.Append("<InputValue>" + hdnInputValue.Value + "</InputValue>");
                            //FB 2501 Starts
                            if (hdnReportType.Value == "CDD")
                            {
                                InXML.Append("<QueryType>"+ QueryType.SelectedItem.Value+ "</QueryType>");
                                InXML.Append("<timezone>" + selTZ + "</timezone>");
                                InXML.Append("<ConfTitle>" + ConferenceName.Text.Trim().Replace("'", "''") + "</ConfTitle>"); //ZD 100263_SQL
                                InXML.Append("<OwnerID>" + hdnApprover4.Text + "</OwnerID>");
                                InXML.Append("<RequestorID>" + hdnApprover6.Text + "</RequestorID>");
                                InXML.Append("<VNOCoperator>" + hdnVNOCOperator.Text + "</VNOCoperator>");//FB 2670                                
                                InXML.Append("<CallURL>" + CallURL.Text.Trim().Replace("'", "''") + "</CallURL>");//ZD 100263_SQL
                                InXML.Append("<EndpointID>" + lstEndpoint.Value.ToString() + "</EndpointID>"); 
                                InXML.Append("<LocationID>" + roomIds + "</LocationID>");
                            }
                            else
                                InXML.Append("<timezone>" + tmzone + "</timezone>");
                            
                            //FB 2501 Ends
                            InXML.Append("<WorkingHours>" + workingHours + "</WorkingHours>");//FB 2343
                            InXML.Append("<SelectedYear>" + SelectedYears + "</SelectedYear>");
                            InXML.Append("<strParam>" + strParam + "</strParam>");
                            InXML.Append("<pivotInput>" + pivotInput + "</pivotInput>");
                            InXML.Append("</GetUsageReports>");
                        }
                        outXML = obj.CallMyVRMServer("GetUsageReports", InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                        Session["ReportsXML"] = outXML;
                    }
                    else
                        outXML = Session["ReportsXML"].ToString();

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        lblErrLabel.Text = obj.ShowErrorMessage(outXML);
                        lblErrLabel.Visible = true;
                    }
                    else
                    {
                        xmlDoc.LoadXml(outXML);
                        ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(xmlDoc));

                        if (ds.Tables.Count > 0)
                        {
                            if (ReportsList.Value.ToString() == "3" && (lstUsageReports.Value.ToString() == "3" || lstUsageReports.Value.ToString() == "4"))
                            {
                                if (ds.Tables[0].Rows.Count == monthweekCount)
                                {
                                    BindWeeklyMonthlyUsageReport(ds);
                                }
                                else
                                {
                                    trDetailsView.Attributes.Add("style", "display:block;");
                                    divWeeklyReport.Attributes.Add("style", "display: none;");
                                    divMonthlyReport.Attributes.Add("style", "display: none;");
                                    divReportDetais.Attributes.Add("style", "display:none;");
                                    divNoData.Attributes.Add("style", "display: block;");
                                }
                            }
                            else
                            {
                                CreateTable();
                                CreateGridColumns();
                                divWeeklyReport.Attributes.Add("style", "display: none;");
                                divMonthlyReport.Attributes.Add("style", "display: none;");
                                divNoData.Attributes.Add("style", "display: none;");

                                if (ReportsList.Value.ToString() == "4" && DrpChrtType.Value.ToString() != "3")
                                {
                                    trWebChart.Attributes.Add("style", "display:block;");
                                    trDetailsView.Attributes.Add("style", "display:none;");

                                    BindwebChartCtrl(null, null);
                                }
                                else
                                {
                                    trWebChart.Attributes.Add("style", "display:none;");
                                    trDetailsView.Attributes.Add("style", "display:block;");
                                    divReportDetais.Attributes.Add("style", "display:block;");//FB 2343

                                    gvReportDetails.DataSource = rptTable;
                                    gvReportDetails.DataBind();
                                    

                                    if (rptTable.Rows.Count > 1)//FB 2205-s
                                    {
                                        gvReportDetails.Settings.ShowGroupPanel = true;
                                        gvReportDetails.Settings.ShowFilterRow = true;
                                    }
                                    else
                                    {
                                        gvReportDetails.Settings.ShowGroupPanel = false;
                                        gvReportDetails.Settings.ShowFilterRow = false;
                                    }


                                    if (hdnReportType.Value == "CR")
                                    {
                                        table = new DataTable();
                                        table1 = new DataTable();
                                        table = ds.Tables[0];

                                        if (ds.Tables.Count == 2)
                                            table1 = ds.Tables[1];

                                        BindCalendarSummary(ds);
                                    }
                                }
                                foreach (GridViewColumn col in gvReportDetails.Columns)
                                {
                                    if (col.Caption == "enddate")
                                        col.Visible = false;
                                }
                                PrintReport();
								//FB 2501 starts
                                txtConferenceUniqueID.Text = "";								
                				txtApprover4.Text = "";
                				txtApprover6.Text = "";
                                txtRVNOCOperator.Text = "";//FB 2670
                				hdnApprover4.Text = "";
                				hdnApprover6.Text = "";
                                hdnVNOCOperator.Text = "";//FB 2670
                                ConferenceName.Text = "";
                                CallURL.Text = "";
                                lstConferenceTZ.Value = "-1";
                                lstBridges.Value = "-1";
                                lstEndpoint.Value = "-1";
                                foreach (ListItem li in cblRoom.Items)
                                {
                                    li.Selected = false;
                                }
                				//FB 2501 ends
                            }
                        }
                    }
                }
                if (orgID != "")
                {
                    lblOrgName.Text = "";
                    lblOrgName.Text = lstOrg.SelectedItem.Text.ToString();
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.Message);
                lblErrLabel.Text = obj.ShowSystemMessage();
            }
        }

        #endregion

        #region GetReportParams

        private bool GetReportParams()
        {
            try
            {
                // CONF - Conf Usage , LOC - Locations, MCU - MCU
                String strRptType = "";
                // ALL-Conf All, CA-Conf Audio, CV-Conf Video, CR-Conf Room, CP-Conf pt to pt, 
                //RM - Room Id's, CS - Country/State, ZP - Zip, MCU
                String strInput = "";
                String strValue = "";
                String strHeading = "";
                String str1 = "";
                String str2 = "";
                String str3 = "";
                switch (ReportsList.Value.ToString())
                {
                    case "1":
                        switch (ConfScheRptDivList.Value.ToString())
                        {
                            case "1":
                                strRptType = "CR";
                                strInput = "RM";
                                strHeading = obj.GetTranslatedText("VTC Calendar");
                                strValue = roomIds;
                                break;
                            case "2": strRptType = "DS"; strHeading = obj.GetTranslatedText("VTC Daily Schedule"); break;
                            case "3": strRptType = "PRI"; strHeading = obj.GetTranslatedText("PRI Allocation Report"); break;
                            case "4": strRptType = "RA"; strHeading = obj.GetTranslatedText("VTC Resource Allocation Report"); break;
                            // FB 2501 starts
                            case "5": strRptType = "CDD"; strValue = lstBridges.Value.ToString(); strHeading = obj.GetTranslatedText("Exportable Conference Report"); break; // CDD Conference Reports for Dimension Data
                            //FB 2501 Ends
                        }
                        break;
                    case "2": strRptType = "CL"; strHeading = obj.GetTranslatedText("Contact List"); break;//FB 2861
                    case "3":
                        if (lstUsageReports.Value.ToString() == "1")
                        {
                            strRptType = "AU";
                            strHeading = obj.GetTranslatedText("Aggregate Usage Report");
                        }
                        else if (lstUsageReports.Value.ToString() == "2")
                        {
                            strRptType = "UR";
                            strHeading = obj.GetTranslatedText("Usage By Room");
                        }
                        else if (lstUsageReports.Value.ToString() == "3") //FB 2343
                        {
                            strRptType = "WUR";
                            strHeading = obj.GetTranslatedText("Weekly Room Usage");
                        }
                        else if (lstUsageReports.Value.ToString() == "4")
                        {
                            strRptType = "MUR";
                            strHeading = obj.GetTranslatedText("Monthly Room Usage");
                        }
                        break;
                    case "4":
                        switch (DrpAllType.Value.ToString())
                        {
                            case "1":
                                strRptType = "CONF";

                                if (lstConfType.Value.ToString() == "1") strInput = "ALL";
                                else if (lstConfType.Value.ToString() == "6") { strInput = "CA"; str2 = obj.GetTranslatedText("Audio"); }
                                else if (lstConfType.Value.ToString() == "2") { strInput = "CV"; str2 = obj.GetTranslatedText("Video"); }
                                else if (lstConfType.Value.ToString() == "4") { strInput = "CP"; str2 = obj.GetTranslatedText("Point to Point"); }
                                else if (lstConfType.Value.ToString() == "7") { strInput = "CR"; str2 = obj.GetTranslatedText("Room"); }

                                if (strInput != "ALL")
                                    strValue = lstConfType.Value.ToString();

                                str3 = str2 + obj.GetTranslatedText(" Conferences Usage Report");
                                break;
                            case "2":
                                strRptType = "LOC";

                                if (DrpLocations.Value.ToString() == "1")
                                {
                                    strInput = "RM";
                                    str3 = obj.GetTranslatedText("Location(s) Report");
                                    strValue = roomIds; //FB 2205
                                }
                                else if (DrpLocations.Value.ToString() == "2")
                                {
                                    strInput = "CS";
                                    strValue = lstCountries.Value + "|" + lstStates.Value;
                                    str3 = obj.GetTranslatedText("Locations(Country/State) Report");
                                }
                                else if (DrpLocations.Value.ToString() == "3")
                                {
                                    strInput = "ZP";
                                    strValue = txtZipCode.Text.Trim();
                                    str3 = obj.GetTranslatedText("Locations(Zip Code) Report");
                                }
                                break;
                            case "3":
                                strRptType = "MCU";
                                strInput = "MCU";
                                if (DrpMCU.Value.ToString() == "2")
                                {
                                    strValue = lstBridges.Value.ToString();
                                    str3 = "MCU(" + lstBridges.SelectedItem.Text + ") Report";
                                }
                                else
                                    str3 = obj.GetTranslatedText("MCU(All) Report");
                                break;

                        }
                        break;
                    case "5": //FB 2155
                        strRptType = "PERS";
                        String reportRange = DateTime.Today.ToShortDateString();

                        if (lstDailyMonthly.Value.ToString() == "1")
                            reportRange = txtSelectedDate.Text;
                        else
                        {
                            int report = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(txtSelectedDate.Text)).Month;
                            reportRange = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(txtSelectedDate.Text)).Year.ToString();
                            reportRange = obj.GetTranslatedText(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(report).ToString()) + " " + reportRange;//FB 2272
                        }

                        switch (lstStatusType.Value.ToString())
                        {
                            case "1":
                                strInput = "ALL"; strHeading = obj.GetTranslatedText("Personal Report") + " (" + reportRange + ")";//FB 2272 - starts
                                break;
                            case "2":
                                strInput = "ON"; strHeading = obj.GetTranslatedText("Ongoing Conference") + " (" + reportRange + ")";
                                break;
                            case "3":
                                strInput = "PE"; strHeading = obj.GetTranslatedText("Pending Conference") + " (" + reportRange + ")";
                                break;
                            case "4":
                                strInput = "RE"; strHeading = obj.GetTranslatedText("Reservation Conference") + " (" + reportRange + ")";
                                break;
                            case "5":
                                strInput = "TE"; strHeading = obj.GetTranslatedText("Terminated Conference") + " (" + reportRange + ")";
                                break;
                            case "6":
                                strInput = "DE"; strHeading = obj.GetTranslatedText("Deleted Conference") + " (" + reportRange + ")";
                                break;
                            case "7":
                                strInput = "PU"; strHeading = obj.GetTranslatedText("Public Conference") + " (" + reportRange + ")";//FB 2272 - starts
                                break;
                        }

                        if (strInput != "ALL")
                            strValue = lstStatusType.Value.ToString();

                        break;
                    case "6":  //FB 2155 - LeftSide - Header Title
                    case "7":
                        switch (lstResourseType.Value.ToString())
                        {
                            case "1":
                                strRptType = "USR";

                                if (lstUserType.Value.ToString() == "1") { strInput = "ALL"; str2 = ""; }
                                else if (lstUserType.Value.ToString() == "2") { strInput = "ACU"; str2 = obj.GetTranslatedText("Active "); }
                                else if (lstUserType.Value.ToString() == "3") { strInput = "IA"; str2 = obj.GetTranslatedText("Inactive "); }
                                else if (lstUserType.Value.ToString() == "4") { strInput = "BL"; str2 = obj.GetTranslatedText("Blocked "); }

                                strHeading = str2 + obj.GetTranslatedText("User Reports");
                                break;
                            case "2":
                                strRptType = "RR";

                                if (lstRoomType.Value.ToString() == "1") { strInput = "ACR"; str2 = obj.GetTranslatedText("Active "); }
                                else { strInput = "DE"; str2 = obj.GetTranslatedText("Disabled "); }

                                strHeading = str2 + obj.GetTranslatedText("Room Reports");
                                break;
                            case "3":
                                strRptType = "ER";
                                strHeading = obj.GetTranslatedText("Endpoint Reports");
                                break;
                            case "4":
                                strRptType = "MR";
                                strHeading = obj.GetTranslatedText("MCU Reports");
                                break;
                            case "5":
                                strRptType = "DSR";

                                if (lstConfRoomRpt.Value.ToString() == "1") { strInput = "RSC"; str2 = "Scheduled"; }
                                else if (lstConfRoomRpt.Value.ToString() == "2") { strInput = "RAC"; str2 = "Actual"; }
                                else if (lstConfRoomRpt.Value.ToString() == "3") { strInput = "RCO"; str2 = "Conference"; }

                                strHeading = obj.GetTranslatedText("Usage By Room - ") + str2;
                                break;

                            case "6": strRptType = "CDR"; strValue = lstBridges.Value.ToString(); strHeading = obj.GetTranslatedText("Call Detail Records"); break; // FB 2501 CDR Report
                        }
                        break;

                }
                hdnReportType.Value = strRptType;
                hdnInputType.Value = strInput;
                hdnInputValue.Value = strValue;

                DateTime dtFrom = DateTime.Today;
                DateTime dtTo = DateTime.Today;
                DateTime calDate = DateTime.Today;//FB 2155
                if (ReportsList.Value.ToString() == "4")
                {
                    if (lstReport.Value.ToString() == "1") //Yesterday
                    {
                        dtFrom = dtFrom.AddDays(-1);
                        dtTo = new DateTime(dtFrom.Year, dtFrom.Month, dtFrom.Day, 23, 59, 59);
                        str1 = obj.GetTranslatedText("Yesterday");
                    }
                    else if (lstReport.Value.ToString() == "2") //Last Week
                    {
                        dtFrom = dtObj.GetStartOfLastWeek();
                        dtTo = dtObj.GetEndOfLastWeek();
                        str1 = obj.GetTranslatedText("Last Week");
                    }
                    else if (lstReport.Value.ToString() == "3") //Last Month
                    {
                        dtFrom = dtObj.GetStartOfLastMonth();
                        dtTo = dtObj.GetEndOfLastMonth();
                        str1 = obj.GetTranslatedText("Last Month");
                    }
                    else if (lstReport.Value.ToString() == "4") //This Week
                    {
                        dtFrom = dtObj.GetStartOfCurrentWeek();
                        dtTo = dtObj.GetEndOfCurrentWeek();
                        str1 = obj.GetTranslatedText("This Week");
                    }
                    else if (lstReport.Value.ToString() == "5") //Year to Date
                    {
                        dtTo = dtFrom;
                        dtFrom = dtObj.GetStartOfCurrentYear();
                        str1 = obj.GetTranslatedText("Year to Date");
                    }
                    else if (lstReport.Value.ToString() == "6") //Custom Date
                    {
                        if (txtCusDateFrm.Text != "")
                            dtFrom = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(txtCusDateFrm.Text));

                        if (txtCusDateTo.Text != "")
                            dtTo = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(txtCusDateTo.Text));
                        str1 = obj.GetTranslatedText("Custom Date");
                    }

                    strHeading = String.Format("{0} - {1}", str1, str3);
                }
                else if (ReportsList.Value.ToString() == "5")//FB 2155
                {
                    calDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(txtSelectedDate.Text));
                    if (lstDailyMonthly.Value.ToString() == "1")
                    {
                        dtFrom = new DateTime(calDate.Year, calDate.Month, calDate.Day, 0, 0, 0);
                        dtTo = new DateTime(calDate.Year, calDate.Month, calDate.Day, 23, 59, 59);
                    }
                    else
                    {
                        dtFrom = new DateTime(calDate.Year, calDate.Month, 1, 0, 0, 0);
                        dtTo = new DateTime(calDate.Year, calDate.Month, calDate.AddMonths(1).AddDays(-calDate.Day).Day, 23, 59, 59);
                    }
                }
                else
                {
                    //ZD 100263_SQL Starts
                    DateTime dtConfStart = DateTime.MinValue;
                    DateTime dtConfEnd = DateTime.MinValue;
                    if (DateTime.TryParse(myVRMNet.NETFunctions.GetDefaultDate(txtStartDate.Text) + " " + CmbStrtTime.SelectedItem.Value, out dtConfStart) && DateTime.TryParse(myVRMNet.NETFunctions.GetDefaultDate(txtEndDate.Text) + " " + CmbEndTime.SelectedItem.Value, out dtConfEnd))
                    {
                        dtFrom = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(txtStartDate.Text) + " " + CmbStrtTime.SelectedItem.Value);// .Text);
                        dtTo = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(txtEndDate.Text) + " " + CmbEndTime.SelectedItem.Value);// .Text);
                    }
                    else
                    {
                        lblErrLabel.Text = obj.GetTranslatedText("Invalid Date/Time.");
                        lblErrLabel.Visible = true;
                        return false;
                    }
                    //ZD 100263_SQL End
                }

                hdnDateFrom.Value = dtFrom.ToString();
                hdnDateTo.Value = dtTo.ToString();

                lblHeading.Text = strHeading;
            }
            catch (Exception ex)
            {
                log.Trace("GetReportParams " + ex.Message);
                lblErrLabel.Text = obj.ShowSystemMessage();
            }
            return true;
        }
        //ZD 100263_SQL End

        #endregion

        #region CreateTable

        private void CreateTable()
        {
            try
            {
                String colName = "";
                rptTable = new DataTable();
                DateTime startDate = new DateTime();
                
                for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                {
                    colName = "";
                    colName = ds.Tables[0].Columns[i].ToString().ToLower();

                    if (colName == obj.GetTranslatedText("category1") || colName == "d1")
                        continue;

                    if (ds.Tables[0].Columns[i].ToString().ToLower().IndexOf("range") > 0)
                        rptTable.Columns.Add(ds.Tables[0].Columns[i].ToString().Trim(), typeof(Int32));
                    else
                        rptTable.Columns.Add(ds.Tables[0].Columns[i].ToString().Trim(), typeof(String));
                }

                DataTable detailsTable = ds.Tables[0];

                for (int i = 0; i < detailsTable.Rows.Count; i++)
                {
                    DataRow dataRow = rptTable.NewRow();
                    if (detailsTable.Rows[0][0].ToString() == "")
                        break;
                    for (int j = 0; j < rptTable.Columns.Count; j++)
                    {
                        colName = "";
                        colName = rptTable.Columns[j].ColumnName.ToLower().Trim();

                        //if (colName == obj.GetTranslatedText("category1") || colName == "d1")
                        if (colName == "category1" || colName == "d1")//FB 2701
                            continue;

                        //if (colName == obj.GetTranslatedText("start") || colName == obj.GetTranslatedText("end"))
                        if (colName == "start" || colName == "end")//FB 2701
                            dataRow[j] = Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat);
                        //else if (colName == obj.GetTranslatedText("enddate") || colName == obj.GetTranslatedText("start date") || colName == obj.GetTranslatedText("date"))
                        else if (colName == "enddate" || colName == "start date" || colName == "date")//FB 2701
                        {
                            startDate = Convert.ToDateTime(detailsTable.Rows[i][colName].ToString());
                            dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate(startDate); 
                            // +" " + Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat); FB 2205-s
                            //myVRMNet.NETFunctions.GetFormattedTime(detailsTable.Rows[i][colName].ToString(),tformat);
                        }
                        //else if (colName == obj.GetTranslatedText("conf start")) //FB 2501
                        else if (colName == "conf start") //FB 2701
                        {
                            startDate = Convert.ToDateTime(detailsTable.Rows[i][colName].ToString());                            
                            dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate(startDate) + " " + Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat);
                        }
                        else
                            dataRow[j] = detailsTable.Rows[i][colName].ToString().Trim();
                    }

                    rptTable.Rows.Add(dataRow);
                }

                DataTable mcuTable = null;
                if (ReportsList.Value.ToString() == "6" && lstResourseType.Value.ToString() == "6")
                {
                    if (detailsTable.Rows[0][0].ToString() != "")
                    {
                        mcuTable = ds.Tables[1];

                        lbl_McuName.Visible = true;
                        lbl_McuType.Visible = true;
                        lbl_IPAddress.Visible = true;
                        trMCUDetails.Visible = true;

                        if (mcuTable.Rows.Count > 0)
                        {
                            lbl_McuName.Text = mcuTable.Rows[0]["MCUName"].ToString();
                            lbl_McuType.Text = mcuTable.Rows[0]["MCUType"].ToString();
                            lbl_IPAddress.Text = mcuTable.Rows[0]["MCUaddress"].ToString();
                        }
                    }
                    else
                    {
                        lbl_McuName.Visible = false;
                        lbl_McuType.Visible = false;
                        lbl_IPAddress.Visible = false;
                        trMCUDetails.Visible = false;

                    }
                }
                else
                {
                    lbl_McuName.Visible = false;
                    lbl_McuType.Visible = false;
                    lbl_IPAddress.Visible = false;
                    trMCUDetails.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("CreateTable " + ex.Message);
                lblErrLabel.Text = obj.ShowSystemMessage();
            }
        }

        #endregion

        #region CreateGridColumns

        private void CreateGridColumns()
        {
            try
            {
                GridViewDataColumn dataColumn = new GridViewDataColumn();
                gvReportDetails.Columns.Clear();
                //gvReportDetails.DataBind();
                for (Int32 i = 0; i < rptTable.Columns.Count; i++)
                {
                    String colName = "";
                    dataColumn = new GridViewDataColumn();

                    colName = rptTable.Columns[i].ColumnName;                    

                    //FB 2501 - Start
                    if (colName == obj.GetTranslatedText("Conf#") || colName == obj.GetTranslatedText("Duration"))
                        dataColumn.Width = Unit.Pixel(70);
                    if (colName == obj.GetTranslatedText("Title") || colName == obj.GetTranslatedText("Timezone") || colName == obj.GetTranslatedText("Room") || colName == obj.GetTranslatedText("MCU") || colName == obj.GetTranslatedText("Call URI") || colName == obj.GetTranslatedText("Guest Room") || colName == obj.GetTranslatedText("Guest Endpoint Address") || colName == obj.GetTranslatedText("User Name") || colName == obj.GetTranslatedText("User Endpoint"))
                        dataColumn.Width = Unit.Pixel(250);
                    if (colName == obj.GetTranslatedText("Conf Start") || colName == obj.GetTranslatedText("Host") || colName == obj.GetTranslatedText("Requestor") || colName == obj.GetTranslatedText("VNOC Operator") || colName == obj.GetTranslatedText("Meet Type"))
                        dataColumn.Width = Unit.Pixel(150);


                    if (colName == obj.GetTranslatedText("Message") || colName == obj.GetTranslatedText("Conference Title"))
                    {
                        dataColumn.Width = Unit.Pixel(250);
                    }

                    if (colName == obj.GetTranslatedText("Conf Start") || colName == obj.GetTranslatedText("Type"))
                    {
                        dataColumn.Width = Unit.Pixel(150);
                    }
                    if (colName == obj.GetTranslatedText("Email"))//FB 2862
                    {
                        dataColumn.Width = Unit.Pixel(250);
                    }

                    //dataColumn.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
                    dataColumn.Caption = obj.GetTranslatedText(colName);//FB 2272
                    dataColumn.FieldName = colName;
                    dataColumn.HeaderStyle.CssClass = "templateCaption";
                    //FB 2501 - End

                    if (ConfScheRptDivList.Value.ToString() != "1")
                        dataColumn.CellStyle.CssClass = "";
                    else
                        dataColumn.CellStyle.CssClass = "templateTable";

                    gvReportDetails.Columns.Add(dataColumn);

                    if ((hdnReportType.Value == "PRI" && i == 3) || (hdnReportType.Value == "RA" && i == 5))
                    {
                        DateTime blockDate = DateTime.Parse("00:00:00");
                        for (Int32 k = 0; k < 48; k++)
                        {
                            dataColumn = new GridViewDataColumn();

                            dataColumn.Caption = blockDate.ToString(tformat);

                            dataColumn.HeaderStyle.CssClass = "templateCaption";
                            gvReportDetails.Columns.Add(dataColumn);
                            gvReportDetails.DataBind();
                            blockDate = blockDate.AddMinutes(30);
                        }
                    }
                }

                if (ReportsList.Value.ToString() == "4")
                {
                    //Change the DataGrid Column Headings for Graphical Report (Table)
                    if (lstReport.Value.ToString() == "3" || lstReport.Value.ToString() == "5")
                        gvReportDetails.Columns[0].Caption = obj.GetTranslatedText("Month(s)");//FB 2272 - Starts
                    else
                        gvReportDetails.Columns[0].Caption = obj.GetTranslatedText("Date");

                    if (DrpAllType.Value.ToString() == "1")
                    {
                        gvReportDetails.Columns[1].Caption = obj.GetTranslatedText("Conference Type");
                        gvReportDetails.Columns[2].Caption = obj.GetTranslatedText("Conferences");
                    }
                    else if (DrpAllType.Value.ToString() == "2")
                    {
                        gvReportDetails.Columns[1].Caption = obj.GetTranslatedText("Room Name");
                        gvReportDetails.Columns[2].Caption = obj.GetTranslatedText("Room Usage");
                    }
                    else if (DrpAllType.Value.ToString() == "3")
                    {
                        gvReportDetails.Columns[1].Caption = obj.GetTranslatedText("MCU Name");
                        gvReportDetails.Columns[2].Caption = obj.GetTranslatedText("MCU Usage");//FB 2272 - End
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("CreateGridColumns" + ex.Message);
                lblErrLabel.Text = obj.ShowSystemMessage();
            }
        }

        #endregion

        #region BindwebChartCtrl
        /// <summary>
        /// BindwebChartCtrl()
        /// </summary>
        protected void BindwebChartCtrl(Object sender, EventArgs e)
        {
            try
            {
                webChartCtrl.DataSource = rptTable;
                webChartCtrl.SeriesDataMember = rptTable.Columns["SeriesName"].ToString();
                webChartCtrl.SeriesTemplate.ArgumentDataMember = rptTable.Columns["Category"].ToString();  //Values for X-axis
                webChartCtrl.SeriesTemplate.ValueDataMembers.AddRange(new string[] { rptTable.Columns["ReportsRange"].ToString() });  //Values for Y-axis
                webChartCtrl.DataBind();
                if (webChartCtrl.Titles.Count == 0)
                {
                    ChartTitle chtTitle = new ChartTitle();
                    chtTitle.Dock = ChartTitleDockStyle.Top;
                    chtTitle.Antialiasing = true;
                    chtTitle.TextColor = System.Drawing.Color.Red;
                    chtTitle.Indent = 10;
                    chtTitle.Text = rptTitle;  //Title of the Chart
                    webChartCtrl.Titles.AddRange(new ChartTitle[] { chtTitle });
                }
                if (DrpChrtType.Value.ToString() == "1")
                    webChartCtrl.SeriesTemplate.ChangeView(ViewType.Bar);
                else if (DrpChrtType.Value.ToString() == "2")
                    webChartCtrl.SeriesTemplate.ChangeView(ViewType.Line);

                ((XYDiagram)webChartCtrl.Diagram).AxisX.Title.Visible = true;
                ((XYDiagram)webChartCtrl.Diagram).AxisX.Title.Text = gvReportDetails.Columns[0].Caption;
                ((XYDiagram)webChartCtrl.Diagram).AxisY.Title.Visible = true;
                ((XYDiagram)webChartCtrl.Diagram).AxisY.Title.Text = gvReportDetails.Columns[2].Caption;

                ((XYDiagram)webChartCtrl.Diagram).AxisX.Label.Angle = -70;
                ((XYDiagram)webChartCtrl.Diagram).AxisY.Label.Angle = -70;

            }
            catch (Exception ex)
            {
                log.Trace("BindwebChartCtrl" + ex.Message);
                lblErrLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region ViewReport

        protected void ViewReport(object sender, EventArgs e)
        {
            try
            {
                if (gvReportDetails.GroupCount > 0 && (ReportsList.Value.ToString() == "4" && DrpChrtType.Value.ToString() != "3")) //FB 2205-S
                {
                    GridViewColumn col = null;
                    for (int i = 0; i < gvReportDetails.Columns.Count; i++)
                    {
                        col = gvReportDetails.Columns[i];
                        gvReportDetails.UnGroup(col);
                    }
                }
                gvReportDetails.FilterExpression = "";
                Session["ReportsXML"] = null;
                gvReportDetails.DataSource = "";
                gvReportDetails.DataBind();
                hdnValue.Value = "";
                roomIds = GetRoomIDsforParams();
                //FB 2501 starts
                Button btn = (Button)sender;
                TempSubmitClick = btn.ID;
                //FB 2501 ends
                BindData();                
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region GetLocations
        protected void GetLocations()
        {
            try
            {
                String defaultRoomID = "";
                InXML = new StringBuilder();
                InXML.Append("<GetLocationsList>" + obj.OrgXMLElement() + "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></GetLocationsList>");
                outXML = obj.CallMyVRMServer("GetLocationsList", InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//level3List/level3");
                DataTable dt = new DataTable();
                dt.Columns.Add("roomID", typeof(Int32));
                dt.Columns.Add("roomName", typeof(String));
                DataRow dr = null;
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node3 in nodes)
                    {
                        if (node3.SelectNodes("level2List/level2/level1List/level1").Count > 0)
                        {
                            XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");
                            foreach (XmlNode node2 in nodes2)
                            {
                                if (node2.SelectNodes("level1List/level1").Count > 0)
                                {
                                    XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                    foreach (XmlNode node1 in nodes1)
                                    {
                                        dr = dt.NewRow();
                                        dr[dt.Columns[0].ToString()] = node1.SelectSingleNode("level1ID").InnerText;
                                        if (defaultRoomID == "")
                                            defaultRoomID = node1.SelectSingleNode("level1ID").InnerText;
                                        else
                                            defaultRoomID = defaultRoomID + "," + node1.SelectSingleNode("level1ID").InnerText;
                                        dr[dt.Columns[1].ToString()] = node1.SelectSingleNode("level1Name").InnerText;
                                        dt.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }
                //chkSelectall.Checked = true;//FB 2205-s FB 2501
                cblRoom.DataSource = dt;
                cblRoom.DataBind();
                if (dt.Rows.Count == 0)
                {
                    chkSelectall.Checked = false;
                    chkSelectall.Enabled = false;
                }
                GetRoomIDs();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        # endregion

        #region GetRoomIDsforParams
        private String GetRoomIDsforParams()
        {
            Boolean isItemChecked = true;
            String rooms = "", roomIds = "";
            try
            {
                foreach (ListItem item in cblRoom.Items)
                {
                    if (item.Selected)
                    {
                        if (roomIds != "")
                            roomIds += ",";

                        roomIds += item.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
            return roomIds;
        }
        # endregion

        #region GetRoomIDs
        private String GetRoomIDs()
        {
            Boolean isItemChecked = false;//FB 2501
            String rooms = "", roomIds = "";
            try
            {
                if (cblRoom.SelectedValue == "")
                    isItemChecked = false;

                foreach (ListItem item in cblRoom.Items)
                {
                    if (!isItemChecked)
                        item.Selected = false;//FB 2501

                    if (item.Selected)
                    {
                        if (roomIds != "")
                        {
                            roomIds += ",";
                            rooms += ", ";
                        }
                        roomIds += item.Value;
                        rooms += item.Text;
                    }
                }

                if (rooms != "")
                    txtRooms.Text = rooms;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
            return roomIds;
        }
        # endregion

        #region ExportExcel

        protected void ExportExcel(object sender, EventArgs e)
        {
            try
            {
                if (ReportsList.Value.ToString() == "4" && (DrpChrtType.Value.ToString() == "1" || DrpChrtType.Value.ToString() == "2"))
                {
                    PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());

                    pcl.PageHeaderFooter = new PageHeaderFooter(new PageHeaderArea(new string[] { "A", "Header" }, SystemFonts.DialogFont, BrickAlignment.Center), new PageFooterArea(new string[] { "B" }, SystemFonts.DialogFont, BrickAlignment.Center));
                    BindData(); //FB 2205
                    pcl.Component = ((IChartContainer)webChartCtrl).Chart;
                    pcl.Landscape = true;
                    ((Chart)pcl.Component).OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom;
                    pcl.CreateDocument();

                    MemoryStream stream = new MemoryStream();
                    pcl.PrintingSystem.ExportToXls(stream);

                    bool inline = false;

                    Response.ContentType = "application/xls";
                    Response.AddHeader("Accept-Header", stream.Length.ToString());
                    Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + lblHeading.Text.Trim() + ".xls");
                    Response.AddHeader("Content-Length", stream.Length.ToString());
                    Response.BinaryWrite(stream.ToArray());
                    Response.End();
                    pcl.Dispose();
                } //FB 2343 - Export to Excel
                else if (ReportsList.Value.ToString() == "3" && (lstUsageReports.Value.ToString() == "3" || lstUsageReports.Value.ToString() == "4"))
                {
                    BindData();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Report.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter hw = new HtmlTextWriter(sw);
                    if (lstUsageReports.Value.ToString() == "3")
                        tblweeklyRoom.RenderControl(hw);
                    else if (lstUsageReports.Value.ToString() == "4")
                        tblmonthlyRoom.RenderControl(hw);

                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else
                    gridExport.WriteXlsToResponse();

                /*else if (hdnReportType.Value == "CR")
                {
                    //Response.Clear();
                    //Response.Buffer = true;
                    //Response.AddHeader("content-disposition", "attachment;filename=" + lblHeading.Text.Trim() + ".xls");
                    //Response.Charset = "";
                    //Response.ContentType = "application/vnd.ms-excel";
                    //frmMenu = new HtmlForm();
                    //frmMenu.Controls.Clear();
                    //frmMenu.Controls.Add(gvReportDetails); 

                    ////StringWriter sw = new StringWriter();
                    ////System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    ////HtmlTextWriter hw = new HtmlTextWriter(new System.IO.StringWriter(sb));
                    ////gvReportDetails.RenderControl(hw); 
                    ////string strRenderedHTML = sb.ToString(); 
                    ////int startindex = strRenderedHTML.IndexOf("<table");
                    ////int endindex = strRenderedHTML.IndexOf("</table>");
                    ////int length = (endindex - startindex) + 8;
                    ////strRenderedHTML = strRenderedHTML.Substring(startindex, length);
                    ////Response.Output.Write(strRenderedHTML);

                    ////StringBuilder sb = new StringBuilder();
                    ////StringWriter tw = new StringWriter(sb);
                    ////HtmlTextWriter hw = new HtmlTextWriter(tw);

                    ////gvReportDetails.RenderControl(hw);
                    ////Response.Output.Write(sb.ToString());

                    string filename = String.Format("Results_{0}_{1}.xls",
                     DateTime.Today.Month.ToString(), DateTime.Today.Year.ToString());

                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    Response.Charset = "";

                    // SetCacheability doesn't seem to make a difference (see update)
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

                    Response.ContentType = "application/vnd.xls";

                    System.IO.StringWriter stringWriter = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

                    // Replace all gridview controls with literals
                   // ClearControls(AccountGrid);

                    // Throws exception: Control 'ComputerGrid' of type 'GridView'
                    // must be placed inside a form tag with runat=server.
                    // ComputerGrid.RenderControl(htmlWrite);

                    // Alternate to ComputerGrid.RenderControl above
                    System.Web.UI.HtmlControls.HtmlForm form
                        = new System.Web.UI.HtmlControls.HtmlForm();
                    Controls.Add(form);
                    form.Controls.Add(gvReportDetails);
                    form.RenderControl(htmlWriter);

                    Response.Write(stringWriter.ToString());
                    Response.End();


                    ////gridExport.WriteXlsToResponse();
                    ////gvReportDetails.RenderControl(hw);

                    //sw = new StringWriter();
                    //hw = new HtmlTextWriter(sw);
                    //SummaryTable.RenderControl(hw);
                    //Response.Output.Write(sw.ToString());

                    //sw = new StringWriter();
                    //hw = new HtmlTextWriter(sw);
                    //DetailsHeadingTable.RenderControl(hw);
                    //Response.Output.Write(sw.ToString());

                    //sw = new StringWriter();
                    //hw = new HtmlTextWriter(sw);
                    //VTCDetails.RenderControl(hw);
                    //Response.Output.Write(sw.ToString());

                    //sw = new StringWriter();
                    //hw = new HtmlTextWriter(sw);
                    //VTCTotal.RenderControl(hw);
                    //Response.Output.Write(sw.ToString());
                }*/

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region ExportPDF

        protected void ExportPDF(object sender, EventArgs e)
        {
            try
            {
                if (ReportsList.Value.ToString() == "4" && (DrpChrtType.Value.ToString() == "1" || DrpChrtType.Value.ToString() == "2"))
                {
                    PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
                    
                    pcl.PageHeaderFooter = new PageHeaderFooter(new PageHeaderArea(new string[] { "A", "Header" }, SystemFonts.DialogFont, BrickAlignment.Center), new PageFooterArea(new string[] { "B" }, SystemFonts.DialogFont, BrickAlignment.Center));
                    BindData(); //FB 2205
                    pcl.Component = ((IChartContainer)webChartCtrl).Chart;
                    pcl.Landscape = true;
                    ((Chart)pcl.Component).OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom;
                    pcl.CreateDocument();

                    MemoryStream stream = new MemoryStream();
                    pcl.PrintingSystem.ExportToPdf(stream);

                    bool inline = false;

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Accept-Header", stream.Length.ToString());
                    Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + lblHeading.Text.Trim() + ".pdf");
                    Response.AddHeader("Content-Length", stream.Length.ToString());
                    Response.BinaryWrite(stream.ToArray());
                    Response.End();
                    pcl.Dispose();

                }
                else
                {
                    DevExpress.Web.ASPxGridView.Export.Helper.GridViewLink link = new DevExpress.Web.ASPxGridView.Export.Helper.GridViewLink(gridExport);
                    DevExpress.XtraPrinting.PrintingSystem ps = link.CreatePS();
                    ps.PageSettings.Landscape = true;
                    ps.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    ps.ExportToPdf(stream);
                    WriteToResponse("filename", true, "pdf", stream);
                }
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region WriteToResponse
        protected void WriteToResponse(string fileName, bool saveAsFile, string fileFormat, MemoryStream stream)
        {
            if (Page == null || Page.Response == null) return;
            string disposition = saveAsFile ? "attachment" : "inline";
            Page.Response.Clear();
            Page.Response.Buffer = false;
            Page.Response.AppendHeader("Content-Type", string.Format("application/{0}", fileFormat));
            Page.Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Page.Response.AppendHeader("Content-Disposition", string.Format("{0}; filename={1}.{2}", disposition, HttpUtility.UrlEncode(fileName).Replace("+", "%20"), fileFormat));
            if (stream.Length > 0)
                Page.Response.BinaryWrite(stream.ToArray());
        }

        #endregion

        #region PrintReport
        /// <summary>
        /// PrintReport
        /// </summary>
        protected void PrintReport()
        {
            try
            {
                DataGrid grid = new DataGrid();
                Type cstype = this.GetType();
                BoundColumn column = null;

                if (ReportsList.Value.ToString() == "4" && DrpChrtType.Value.ToString() != "3")
                             return;

                for (int i = 0; i < rptTable.Columns.Count; i++)
                {
                    column = new BoundColumn();
                    column.DataField = rptTable.Columns[i].Caption;
                    column.HeaderText = rptTable.Columns[i].Caption;
                    grid.Columns.Add(column);
                }

                grid.DataSource = rptTable;
                grid.DataBind();
                
                if (Session["PrintGrid"] == null)
                    Session.Add("PrintGrid", grid);
                else
                    Session["PrintGrid"] = grid;

                if (Session["PrintTable"] == null)
                    Session.Add("PrintTable", gvReportDetails.DataSource);
                else
                    Session["PrintTable"] = gvReportDetails.DataSource;

                if (Session["reportType"] == null)
                    Session.Add("reportType", "RptDtal");
                else
                    Session["reportType"] = "RptDtal";

                if (lblHeading.Text.Trim() != "")
                {
                    if (Session["titleString"] == null)
                        Session.Add("titleString", lblHeading.Text);
                    else
                        Session["titleString"] = lblHeading.Text;
                }

                //pageURL = "<script>window.open('PrintInterface.aspx')</script>";
                //ClientScript.RegisterStartupScript(cstype, "Print", pageURL);
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region gvReportDetails_HtmlRowCreated

        protected void gvReportDetails_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
                if (hdnReportType.Value == "PRI" || hdnReportType.Value == "RA")
                {
                    DateTime blockDate = DateTime.Parse("00:00:00");
                    DateTime startDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(gvReportDetails.GetRowValues(e.VisibleIndex, "Date").ToString()));
                    DateTime endDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(gvReportDetails.GetRowValues(e.VisibleIndex, "enddate").ToString()));

                    Boolean isBackColor = false;

                    Int32 n = 3;

                    if (hdnReportType.Value == "PRI")
                        n = 3;
                    else if (hdnReportType.Value == "RA")
                        n = 5;

                    for (Int32 i = n; i < e.Row.Cells.Count; i++)
                    {
                        isBackColor = BlockBetween(blockDate, startDate, endDate);
                        String strBack = "N";
                        if (isBackColor)
                            strBack = "Y";

                        e.Row.Cells[i].Attributes.Add("strVal", strBack);                        
                        blockDate = blockDate.AddMinutes(30);
                    }
               

                }
            }
            catch (Exception ex)
            {
                lblErrLabel.Visible = true;
                lblErrLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " gvReportDetails_HtmlRowCreated: " + ex.Message);
            }
        }

        #endregion

        #region gvReportDetails_HtmlCellCreated
        protected void gvReportDetails_HtmlCellCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
        {
            try
            {
                if (hdnReportType.Value == "PRI" || hdnReportType.Value == "RA")
                {
                    if (e.Cell.Attributes["strVal"].ToString() == "Y")
                        e.Cell.BackColor = Color.Gold;
                    else
                        e.Cell.BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                lblErrLabel.Visible = true;
                lblErrLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " gvReportDetails_HtmlCellCreated: " + ex.Message);
            }
        }

        #endregion

        #region BlockBetween

        private Boolean BlockBetween(DateTime blockDate, DateTime startT, DateTime endT)
        {
            bool isBlock = false;
            try
            {
                DateTime stDate;
                DateTime etDate;

                stDate = DateTime.Parse(startT.ToShortTimeString());
                etDate = DateTime.Parse(endT.ToShortTimeString());

                if (blockDate == stDate || (blockDate > stDate && blockDate < etDate))
                    isBlock = true;
                else
                    isBlock = false;

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
            return isBlock;
        }

        #endregion

        #region BindCalendarSummary

        private void BindCalendarSummary(DataSet ds)
        {
            System.Web.UI.WebControls.TableRow row = null;
            TableCell cell = null;
            Decimal duration = 0;
            Decimal ISDNoutBound = 0;
            Decimal ISDNinBound = 0;
            Decimal IPduration = 0;
            String strValue = "";
            try
            {
                DataTable dt = null;
                if (table1 != null)
                    dt = table1;

                DataRow[] filterRows = null;

                #region Bind Summary

                if (dt != null && dt.Rows.Count > 0)
                {
                    filterRows = dt.Select("conftype=4");
                    PtPtCnt.Text = filterRows.Length.ToString();

                    filterRows = null;
                    filterRows = dt.Select("conftype not in(11,7,4,9)");
                    MultiPtCnt.Text = filterRows.Length.ToString();

                    filterRows = null;
                    filterRows = dt.Select("conftype not in(11,7,9)");
                    VTCMtTotCnt.Text = filterRows.Length.ToString();
                }
                #endregion

                DataTable totTable = null;

                if (table != null)
                    totTable = table;

                if (totTable != null)
                {
                    DetailsHeadingTable.Attributes.Add("Style", "Display:Block");
                    VTCTotal.Attributes.Add("Style", "Display:Block");

                    #region

                    //FB 1830
                    cInfo = new CultureInfo(Session["NumberFormat"].ToString());
                    Int32 divHeight = 0;
                    foreach (DataRow dr in totTable.Rows)
                    {
                        row = new System.Web.UI.WebControls.TableRow();
                        row.HorizontalAlign = HorizontalAlign.Right;
                        cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(12); row.Cells.Add(cell);

                        cell = new TableCell();
                        cell.Width = Unit.Percentage(10);
                        //FB 1830
                        decimal.TryParse(dr["Duration"].ToString(), out tmpVal);
                        dr["Duration"] = tmpVal.ToString("n0", cInfo);

                        cell.Text = dr["Duration"].ToString();
                        row.Cells.Add(cell);

                        cell = new TableCell();
                        cell.Width = Unit.Percentage(10);
                        //FB 1830
                        cell.Text = Math.Round(tmpVal / 60, 1).ToString("n0", cInfo);
                        //cell.Text = Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1).ToString();
                        row.Cells.Add(cell);

                        cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(10); row.Cells.Add(cell);

                        cell = new TableCell();
                        cell.Width = Unit.Percentage(10);
                        //FB 1830
                        //cell.Text = ((dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Outbound") ? Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1) : 0).ToString();
                        cell.Text = ((dr["Protocol"].ToString() == "ISDN" && dr["Connection Type"].ToString() == "Outbound") ? Math.Round(tmpVal / 60, 1) : 0).ToString("n0", cInfo);
                        row.Cells.Add(cell);

                        cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(10); row.Cells.Add(cell);

                        cell = new TableCell();
                        cell.Width = Unit.Percentage(10);
                        //FB 1830
                        //cell.Text = ((dr["vidProtocol"].ToString() == "IP") ? Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1) : 0).ToString();
                        cell.Text = ((dr["Protocol"].ToString() == "IP") ? Math.Round(tmpVal / 60, 1) : 0).ToString("n0", cInfo);
                        row.Cells.Add(cell);

                        cell = new TableCell(); cell.Text = ""; cell.Width = Unit.Percentage(10); row.Cells.Add(cell);

                        cell = new TableCell();
                        cell.Width = Unit.Percentage(10);
                        //FB 1830
                        //cell.Text = ((dr["vidProtocol"].ToString() == "ISDN" && dr["connType"].ToString() == "Inbound") ? Math.Round(Convert.ToDecimal(dr["Duration"]) / 60, 1) : 0).ToString();
                        cell.Text = ((dr["Protocol"].ToString() == "ISDN" && dr["Connection Type"].ToString() == "Inbound") ? Math.Round(tmpVal / 60, 1) : 0).ToString("n0", cInfo);
                        cell.HorizontalAlign = HorizontalAlign.Center; //FB 2205-s
                        row.Cells.Add(cell);

                        VTCDetails.Rows.Add(row);

                        if (divHeight <= 150)
                            divHeight = divHeight + 23;//FB 2205-s

                        DetailsDiv.Attributes.Add("Style", "Height:" + divHeight + "px;display:none;overflow-y: auto;overflow-x: hidden;Width:80%");
                        //FB 1830
                        //duration += Convert.ToDecimal(dr["Duration"]);
                        duration += tmpVal;

                        if (dr["Protocol"].ToString() == "ISDN" && dr["Connection Type"].ToString() == "Outbound")
                            //ISDNoutBound += Convert.ToDecimal(dr["Duration"]);
                            ISDNoutBound += tmpVal;//FB 1830

                        if (dr["Protocol"].ToString() == "IP")
                            //IPduration += Convert.ToDecimal(dr["Duration"]);
                            IPduration += tmpVal;//FB 1830

                        if (dr["Protocol"].ToString() == "ISDN" && dr["Connection Type"].ToString() == "Inbound")
                            //ISDNinBound += Convert.ToDecimal(dr["Duration"]);
                            ISDNinBound += tmpVal;//FB 1830

                        //FB 1830
                        ds.Tables[0].Select()[0]["Duration"] = tmpVal.ToString("n0", cInfo);
                    }

                    #endregion

                    #region Bind Total Row

                    cell = new TableCell();
                    //duration = Convert.ToDecimal(ds.Tables[0].Compute("sum(Duration)", ""));
                    //FB 1830
                    cell.Text = duration.ToString("n0", cInfo);
                    cell.Width = Unit.Percentage(10);
                    FirstRow.Cells.Add(cell);

                    cell = new TableCell();
                    //FB 1830
                    cell.Text = Math.Round(duration / 60, 1).ToString("n0", cInfo);
                    cell.Width = Unit.Percentage(10);
                    FirstRow.Cells.Add(cell);

                    cell = new TableCell();
                    //FB 1830
                    cell.Text = totTable.Rows.Count.ToString("n0", cInfo);
                    cell.Width = Unit.Percentage(10);
                    FirstRow.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    //strValue = totTable.Compute("sum(Duration)", "vidProtocol = 'ISDN' and connType = 'Outbound'").ToString();
                    if (strValue != "")
                    {
                        //ISDNoutBound = Math.Round(Convert.ToDecimal(strValue) / 60);
                        //FB 1830
                        cell.Text = (Math.Round(ISDNoutBound / 60, 1)).ToString("n0", cInfo);
                    }
                    else
                        cell.Text = "0";

                    FirstRow.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    //FB 1830
                    cell.Text = Math.Round((ISDNoutBound / (duration / 60)) * 100).ToString("n0", cInfo);
                    FirstRow.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    strValue = "";
                    // strValue = totTable.Compute("sum(Duration)", "vidProtocol = 'IP'").ToString();
                    // IPduration = Math.Round(Convert.ToDecimal(strValue) / 60);
                    //FB 1830
                    cell.Text = Math.Round(IPduration / 60).ToString("n0", cInfo);
                    FirstRow.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    //FB 1830
                    cell.Text = Math.Round((IPduration / (duration / 60)) * 100).ToString("n0", cInfo);
                    FirstRow.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Width = Unit.Percentage(10);
                    strValue = "";
                    //strValue = totTable.Compute("sum(Duration)", "vidProtocol = 'ISDN' and connType = 'Inbound' ").ToString();
                    if (ISDNinBound != 0)
                        //FB 1830
                        cell.Text = Math.Round(ISDNinBound / 60).ToString("n0", cInfo);
                    else
                        cell.Text = "0";
                    FirstRow.Cells.Add(cell);

                    VTCTotal.Rows.Add(FirstRow);
                    //FB 1830
                    ds.AcceptChanges();
                    #endregion
                }
                else
                {
                    DetailsHeadingTable.Attributes.Add("Style", "Display:None");
                    VTCTotal.Attributes.Add("Style", "Display:None");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region UsrMenuMask
        /// <summary>
        /// UsrMenuMask
        /// </summary>
        protected void UsrMenuMask()
        {
            try
            {
                string[] mary = Session["sMenuMask"].ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int topMenu = Convert.ToInt32(ccary[1]);
                int adminMenu = Convert.ToInt32(mmary[0].Split('*')[1]);
                int subMenu;
                int orgMenu = Convert.ToInt32(mmary[1].Split('*')[1]);

                UserReportsCell.Attributes.Add("Style", "display:block");

                //hasCalendar = Convert.ToBoolean(topMenu & 64);
                //hasConference = Convert.ToBoolean(topMenu & 32);
                if (!Convert.ToBoolean(topMenu & 32))
                {
                    if (ReportsList.Items.FindByValue("5") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("5"));

                }
                else
                {
                    if (lstUsageReports.Items.FindByValue("1") == null)
                        lstUsageReports.Items.Add(obj.GetTranslatedText("Aggregate Usage"), "1");

                    if (DrpAllType.Items.FindByValue("1") == null)
                        DrpAllType.Items.Add(obj.GetTranslatedText("Conference Type"), "1");
                }

                subMenu = Convert.ToInt32(mmary[3].Split('*')[1]);
                //hasRooms = Convert.ToBoolean(subMenu & 2);
                if (!Convert.ToBoolean(subMenu & 2))
                {
                    UserReportsCell.Attributes.Add("Style", "display:none");
                    if (ReportsList.Items.FindByValue("2") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("2")); //User report
                }
                else
                {
                    if (lstUsageReports.Items.FindByValue("2") == null)
                        lstUsageReports.Items.Add(obj.GetTranslatedText("Usage by Room"), "2");

                    if (DrpAllType.Items.FindByValue("2") == null)
                        DrpAllType.Items.Add(obj.GetTranslatedText("Locations"), "2");

                    //FB 2343 - Starts
                    if (lstUsageReports.Items.FindByValue("3") == null)
                        lstUsageReports.Items.Add(obj.GetTranslatedText("Weekly Room Usage"), "3");

                    if (lstUsageReports.Items.FindByValue("4") == null)
                        lstUsageReports.Items.Add(obj.GetTranslatedText("Monthly Room Usage"), "4");
                }

                
                subMenu = Convert.ToInt32(mmary[2].Split('*')[1]);
                if (Convert.ToBoolean(subMenu & 1))
                    if (DrpAllType.Items.FindByValue("3") == null)
                        DrpAllType.Items.Add("MCU", "3");

                if (lstUsageReports.Items.FindByValue("3") != null)
                {
                    
                    StringBuilder inXML = new StringBuilder();
                    xmlDoc = new XmlDocument();
                    inXML.Append("<GetUsageReports>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                    inXML.Append("<ReportType>MY</ReportType>");
                    inXML.Append("</GetUsageReports>");
                    
                    String outXML1 = obj.CallMyVRMServer("GetUsageReports", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML1.IndexOf("<error>") < 0)
                    {
                        xmlDoc.LoadXml(outXML1);
                        ds1.ReadXml(new XmlNodeReader(xmlDoc));
                    }
                    int d = DateTime.Now.Year;
                    int indexyearM = 0, indexyearW = 0;
                    String ss = "";
                    if (ds1 != null && ds1.Tables.Count > 0)
                    {
                        DataTable dtM = ds1.Tables[0];//Month
                        Int32 startYearM = 0, endYearM = 0;

                        DataTable dtW = new DataTable();//Week
                        Int32 startYearW = 0, endYearW = 0;
                        ss = dtM.Rows[0][0].ToString();
                        if (dtM.Rows.Count > 0 && ss != "")
                        {
                            startYearM = Convert.ToInt32(dtM.Rows[0]["CurrentYear"]);
                            endYearM = Convert.ToInt32(dtM.Rows[dtM.Rows.Count - 1]["CurrentYear"]);

                            dtW = ds1.Tables[2];//Week
                            startYearW = 0; endYearW = 0;
                            if (dtW.Rows.Count > 0)
                            {
                                startYearW = Convert.ToInt32(dtW.Rows[0]["CurrentYear"]);
                                endYearW = Convert.ToInt32(dtW.Rows[dtW.Rows.Count - 1]["CurrentYear"]);
                            }
                        }
                        if (startYearM == 0 && ss == "")
                        {
                            lstMStartYear.Items.Add(d.ToString(), "1");
                            lstMEndYear.Items.Add(d.ToString(), "1");
                            lstWStartYear.Items.Add(d.ToString(), "1");
                            lstWEndYear.Items.Add(d.ToString(), "1");
                            indexyearM = 0;
                        }
                        else
                        {
                            for (int j = startYearM, p = 1; j <= endYearM; j++, p++)
                            {
                                if (j == d)
                                    indexyearM = p - 1;
                                lstMStartYear.Items.Add(j.ToString(), p);
                                lstMEndYear.Items.Add(j.ToString(), p);
                            }
                            for (int j = startYearW, p = 1; j <= endYearW; j++, p++)
                            {
                                if (j == d)
                                    indexyearW = p - 1;
                                lstWStartYear.Items.Add(j.ToString(), p);
                                lstWEndYear.Items.Add(j.ToString(), p);
                            }
                        }
                    }
                    lstWStartYear.SelectedIndex = indexyearW;
                    lstWEndYear.SelectedIndex = indexyearW;
                    lstMStartYear.SelectedIndex = indexyearM;
                    lstMEndYear.SelectedIndex = indexyearM;
                    
                    //Week and Month Drpdownlsit

                    Int32 wknumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                    String weekname = "";
                    String weeklang = obj.GetTranslatedText("Week ") + " ";
                    for (int k = 1; k <= 52; k++)
                    {
                        weekname = weeklang + k;

                        lstWStartMonth.Items.Add(weekname, k);
                        lstWEndMonth.Items.Add(weekname, k);
                    }
                    lstWStartMonth.SelectedIndex = 0;
                    lstWEndMonth.SelectedIndex = wknumber - 1;//FB2343Doubt
                    
                    String monthName = "";
                    Int32 n = DateTime.Now.Month;
                    for (int h = 1; h <= 12; h++)
                    {
                        monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(h);

                        lstMStartMonth.Items.Add(obj.GetTranslatedText(monthName), h);//FB 2272
                        lstMEndMonth.Items.Add(obj.GetTranslatedText(monthName), h);
                    }

                    lstMStartMonth.SelectedIndex = 0;
                    lstMEndMonth.SelectedIndex = n - 1;
                    //FB 2343 - End
                }

                if (lstUsageReports.Items.Count == 0)
                {
                    if (ReportsList.Items.FindByValue("3") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("3")); //Usage report
                }
                else if (lstUsageReports.Items.Count > 0)
                {
                    lstUsageReports.SelectedIndex = 0;
                }

                if (DrpAllType.Items.Count == 0)
                {
                    if (ReportsList.Items.FindByValue("4") != null)
                        ReportsList.Items.Remove(ReportsList.Items.FindByValue("4")); //Graphical report
                }
                else if (DrpAllType.Items.Count > 0)
                {
                    DrpAllType.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                log.Trace("" + ex.StackTrace);
            }
        }
        #endregion

        #region BindWeeklyMonthlyUsageReport
        /// <summary>
        /// BindWeeklyMonthlyUsageReport
        /// </summary>
        /// <param name="ds"></param>
        private void BindWeeklyMonthlyUsageReport(DataSet ds)
        {
            Boolean isMonthly = true;
            try
            {
                String startFrom1 = "", endTo1 = "";

                Int32 wknumber = 0;
                DateTime startDate1 = new DateTime();
                DateTime startDate2 = new DateTime();
                DateTime endDate1 = new DateTime();
                DateTime endDate2 = new DateTime();

                trDetailsView.Attributes.Add("style", "display: block;");
                trWebChart.Attributes.Add("style", "display:none;");//FB 2505
                divReportDetais.Attributes.Add("style", "display: none;");
                divNoData.Attributes.Add("style", "display: none;");

                System.Web.UI.HtmlControls.HtmlTableRow tr = null;
                System.Web.UI.HtmlControls.HtmlTableCell tcc = null;
                System.Web.UI.HtmlControls.HtmlTableCell tcc1 = null;
                
                tblweeklyRoom.Rows.Clear();
                tblmonthlyRoom.Rows.Clear();
                    
                DataTable dt = ds.Tables[1];
                DataTable dtt = ds.Tables[0];

                if (ds.Tables[0].Columns.Contains("MonthWorkingDays"))
                    isMonthly = true;
                else
                    isMonthly = false;

                int yeardiff = 0;
                int startWeek = 0, endWeek = 0;
                int startMonth = 0, endMonth = 0;
                int ii = 0;


                if (isMonthly)
                {
                    startFrom1 = lstMStartMonth.Value + "/01/" + lstMStartYear.Text;
                    endTo1 = lstMEndMonth.Value + "/01/" + lstMEndYear.Text;

                    startDate1 = Convert.ToDateTime(startFrom1);
                    startDate2 = Convert.ToDateTime(endTo1);

                    yeardiff = Convert.ToInt32(lstMEndYear.Text) - Convert.ToInt32(lstMStartYear.Text);

                    startMonth = startDate1.Month;
                    endMonth = startDate2.Month;

                    if (yeardiff >= 1)
                        ii = (12 * yeardiff - startMonth) + endMonth;
                    else
                        ii = endMonth - startMonth;
                }
                else
                {

                    yeardiff = Convert.ToInt32(lstWEndYear.Text) - Convert.ToInt32(lstWStartYear.Text);

                    startWeek = Convert.ToInt32(lstWStartMonth.Value);
                    endWeek = Convert.ToInt32(lstWEndMonth.Value);

                    if (yeardiff >= 1)
                        ii = (52 * yeardiff - startWeek) + endWeek;
                    else
                        ii = endWeek - startWeek;
                }

                tr = new System.Web.UI.HtmlControls.HtmlTableRow();
                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                tcc.Attributes.Add("class", "horizontaltexts");
                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
                //tcc.Style.Add(HtmlTextWriterStyle.Color, "#C68FDA");
                tcc.InnerText = obj.GetTranslatedText("ROOM USAGE BY SITE");//FB 2272
                tr.Cells.Add(tcc);

                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                tcc.Attributes.Add("class", "horizontaltexts");
                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
                tcc.InnerText = "";
                if (ii == 0)
                    tcc.ColSpan = (ii + 1) * 4;
                else
                    tcc.ColSpan = (ii + 2) * 4;
                //tcc.ColSpan = (endTo - startFrom + 1) * 4;
                tr.Cells.Add(tcc);
                
                if (isMonthly)
                    tblmonthlyRoom.Rows.Add(tr);
                else
                    tblweeklyRoom.Rows.Add(tr);

                tr = new System.Web.UI.HtmlControls.HtmlTableRow();
                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                tcc.InnerText = obj.GetTranslatedText("Room");//FB 2272
                tr.Cells.Add(tcc);

                #region HeaderTexts
                if (isMonthly)
                {
                    for (DateTime r1 = startDate1; r1 <= startDate2; r1 = r1.AddMonths(1))
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl dv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");

                        BindHeader(dv, ref tr, tcc);

                        if (r1 == startDate2)
                        {
                            if (startDate2 > startDate1)
                            {
                                BindHeader(dv, ref tr, tcc);
                            }
                        }
                    }
                }
                else
                {
                    for (int r1 = 0; r1 <= ii; r1++)
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl dv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                        BindHeader(dv, ref tr, tcc);

                        if (r1 == ii && ii > 0)
                        {
                            dv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            BindHeader(dv, ref tr, tcc);
                        }
                    }
                }

                if (isMonthly)
                    tblmonthlyRoom.Rows.Add(tr);
                else
                    tblweeklyRoom.Rows.Add(tr);

                tr = new System.Web.UI.HtmlControls.HtmlTableRow();
                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                tcc.Attributes.Add("class", "horizontaltexts");
                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
                tcc.InnerText = "";
                tr.Cells.Add(tcc);

                String monthName = "",monthName1 = "";
                int years = startDate1.Year;
                if (isMonthly)
                {
                    for (DateTime r1 = startDate1; r1 <= startDate2; r1 = r1.AddMonths(1))
                    { 
                        monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(r1.Month);
                        if(r1 == startDate1)
                            monthName1 = monthName.Substring(0, 3);

                        monthName = monthName.Substring(0, 3);

                        tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                        tcc.InnerText = obj.GetTranslatedText(monthName);//FB 2272

                        if (yeardiff >= 1)
                        {
                            if (r1 == startDate1)
                                tcc.InnerText = obj.GetTranslatedText(monthName) + "(" + startDate1.Year + ")";//FB 2272

                            else if (r1 > startDate1 && r1.Month == 1)
                            {
                                years = years + 1;
                                tcc.InnerText = obj.GetTranslatedText(monthName) + "(" + years + ")";
                            }
                            else
                                tcc.InnerText = obj.GetTranslatedText(monthName);
                        }
                        else
                            tcc.InnerText = obj.GetTranslatedText(monthName);

                        tcc.ColSpan = 4;
                        tcc.Attributes.Add("class", "horizontaltexts");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                        tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                        tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                        tr.Cells.Add(tcc);

                        if (r1 == startDate2)
                        {
                            if (startDate2 > startDate1)
                            {
                                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                                tcc.Attributes.Add("NoWrap", "NoWrap");
                                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                                tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                                if (yeardiff >= 1)
                                    tcc.InnerText = obj.GetTranslatedText("Total") + " ( " + monthName1 + " (" + startDate1.Year + ") " + obj.GetTranslatedText("to") + monthName + " (" + years.ToString() + "))";
                                else
                                    tcc.InnerText = obj.GetTranslatedText("Total") + " ( " + monthName1 + " " + obj.GetTranslatedText("to") + " " + monthName + ")";
                                tcc.ColSpan = 4;
                                tcc.Attributes.Add("class", "horizontaltexts");
                                tr.Cells.Add(tcc);
                            }
                        }
                    }
                }
                else
                {
                    int firstwkno = 0;
                    wknumber = startWeek;
                    years =   Convert.ToInt32(lstWStartYear.Text) ;
                    for (int r1 = 0; r1 <= ii; r1++)
                    {
                        if (r1 == 0)
                            firstwkno = wknumber;

                        tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                        tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                        tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                        tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                        if (yeardiff >= 1)
                        {
                            if (r1 == 0)
                                tcc.InnerText = obj.GetTranslatedText("Week ") + wknumber + "(" + Convert.ToInt32(lstWStartYear.Text) + ")";
                            else if (r1 > 0 && wknumber == 1)
                            {
                                years = years + 1;
                                tcc.InnerText = obj.GetTranslatedText("Week ") + wknumber + " (" + years.ToString() + ")";
                            }
                            else
                                tcc.InnerText = obj.GetTranslatedText("Week ") + wknumber;
                        }
                        else
                            tcc.InnerText = obj.GetTranslatedText("Week ") + wknumber;

                        tcc.ColSpan = 4;
                        tcc.Attributes.Add("class", "horizontaltexts");
                        tr.Cells.Add(tcc);

                        if (r1 == ii && ii > 0)
                        {
                            tcc = new System.Web.UI.HtmlControls.HtmlTableCell();

                            if (yeardiff >= 1)
                                tcc.InnerText = obj.GetTranslatedText("Total(Week ") + firstwkno + "(" + Convert.ToInt32(lstWStartYear.Text) + ") " + obj.GetTranslatedText(" to Week ") + wknumber + "(" + years.ToString() + "))";
                            else
                                tcc.InnerText = obj.GetTranslatedText("Total(Week ") + firstwkno + obj.GetTranslatedText(" to Week ") + wknumber + ")";

                            tcc.ColSpan = 4;
                            tcc.Attributes.Add("class", "horizontaltexts");
                            tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                            tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                            tr.Cells.Add(tcc);
                        }
                        if (wknumber == 52)
                            wknumber = 0;
                        wknumber = wknumber + 1;
                        
                    }
                }
                if (isMonthly)
                    tblmonthlyRoom.Rows.Add(tr);
                else
                    tblweeklyRoom.Rows.Add(tr);
                #endregion

                #region WorkDays and WorkingHours
                tr = new System.Web.UI.HtmlControls.HtmlTableRow();
                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();

                tcc.InnerText = "* " + workingHours + " " + obj.GetTranslatedText("Hours per working day");
                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
                tcc.Attributes.Add("NoWrap", "NoWrap");
                tr.Cells.Add(tcc);

                int month = 0;
                int totaldays = 0;

                if (isMonthly)
                {
                    month = startMonth;
                    for (int p = 0; p <= ii; p++)
                    {
                        
                        tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                        tcc.ColSpan = 2;
                        tcc.InnerText = obj.GetTranslatedText("Work.Dys");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                        tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                        tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                        tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                        tcc1.ColSpan = 2;

                        tcc1.InnerText = dtt.Rows[month - 1][4].ToString();

                        totaldays = totaldays + Convert.ToInt32(dtt.Rows[month - 1][4]);

                        month = month + 1;

                        tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                        tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                        tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                        tcc1.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                        tcc1.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                        tr.Cells.Add(tcc);
                        tr.Cells.Add(tcc1);

                        if (p == ii && ii > 0)
                        {
                            tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                            tcc.ColSpan = 2;
                            tcc.InnerText = obj.GetTranslatedText("Work.Dys");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                            tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                            tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                            tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                            tcc1.ColSpan = 2;
                            tcc1.InnerText = totaldays.ToString();
                            tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                            tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                            tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                            tcc1.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                            tcc1.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                            tr.Cells.Add(tcc);
                            tr.Cells.Add(tcc1);
                        }
                    }
                }
                else
                {
                    totaldays = 0;

                    wknumber = startWeek;
                    for (int p = 0; p <= ii; p++)
                    {
                        tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                        tcc.ColSpan = 2;
                        tcc.InnerText = obj.GetTranslatedText("Work.Dys");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                        tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                        tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                        tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                        tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                        tcc1.ColSpan = 2;

                        tcc1.InnerText = dtt.Rows[wknumber - 1][4].ToString();

                        totaldays = totaldays + Convert.ToInt32(dtt.Rows[wknumber - 1][4]);

                        wknumber = wknumber + 1;

                        tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                        tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                        tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                        tcc1.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                        tcc1.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                        tr.Cells.Add(tcc);
                        tr.Cells.Add(tcc1);

                        if (p == ii && ii > 0)
                        {
                            tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                            tcc.ColSpan = 2;
                            tcc.InnerText = obj.GetTranslatedText("Work.Dys");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                            tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                            tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                            tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                            tcc1.ColSpan = 2;
                            tcc1.InnerText = totaldays.ToString();
                            tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                            tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                            tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                            tcc1.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C0C0C0");
                            tcc1.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");

                            tr.Cells.Add(tcc);
                            tr.Cells.Add(tcc1);
                        }
                    }
                }

                if (isMonthly)
                {
                    divWeeklyReport.Attributes.Add("style", "display: none;");
                    divMonthlyReport.Attributes.Add("style", "display: block; height:420px; overflow: scroll;");
                    tblmonthlyRoom.Rows.Add(tr); // Monthly
                }
                else
                {
                    divMonthlyReport.Attributes.Add("style", "display: none;");
                    divWeeklyReport.Attributes.Add("style", "display: block; height:420px; overflow: scroll;");
                    tblweeklyRoom.Rows.Add(tr);  //Weekly
                }
                #endregion

                #region Filling datas
                System.Web.UI.HtmlControls.HtmlTableRow tr1 = new System.Web.UI.HtmlControls.HtmlTableRow();
                tr1.Attributes.Clear();
                
                Int32 attVal = 0;
                Decimal attValD = 0;
                Int32 cattVal = 0;
                Decimal cattValD = 0;

                Int32 confs = 0, availHrs = 0; decimal totalHrsD = 0; // Main Data
                Int32 confsV = 0, availHrsV = 0; decimal totalHrsVD = 0; // Vertical Total
                Int32 confsT = 0, availHrsT = 0; decimal totalHrsTD = 0; //RightHandside Full Total

                if (dt.Rows.Count > 0)
                {
                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        confs = 0; totalHrsD = 0; availHrs = 0;
                        tr = new System.Web.UI.HtmlControls.HtmlTableRow();

                        for (Int32 c = 0; c < dt.Columns.Count; c++)
                        {
                            tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                            tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                            tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                            cattVal = 0;
                            if (c != 0)
                            {
                                tcc.Attributes.Add("NoWrap", "NoWrap");
                                tcc.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                            }

                            if (dt.Columns[c].ColumnName.IndexOf("Confs") >= 0)
                            {
                                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F9F988");

                                tcc.InnerText = dt.Rows[i][c].ToString();
                                Int32.TryParse(tcc.InnerText, out cattVal);
                                confs = confs + cattVal;
                            }
                            else if (dt.Columns[c].ColumnName.IndexOf("Avail.Hours") >= 0)
                            {
                                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C68FDA");

                                tcc.InnerText = dt.Rows[i][c].ToString();
                                Int32.TryParse(tcc.InnerText, out cattVal);
                                availHrs = availHrs + cattVal;
                            }
                            else if (dt.Columns[c].ColumnName.IndexOf("Total Hours") >= 0)
                            {
                                tcc.InnerText = dt.Rows[i][c].ToString();
                                Decimal.TryParse(tcc.InnerText, out cattValD);

                                totalHrsD = totalHrsD + cattValD;
                            }
                            else if (dt.Columns[c].ColumnName.IndexOf("Usage") >= 0)
                            {
                                tcc.Attributes.Add("NoWrap", "NoWrap");
                                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C68FDA");

                                tcc.InnerText = dt.Rows[i][c].ToString() + " %";
                            }
                            else
                                tcc.InnerText = dt.Rows[i][c].ToString();


                            if (c > 0)
                            {
                                if (i == 0)
                                    tr1.Attributes.Add(dt.Columns[c].ColumnName, dt.Rows[i][c].ToString());
                                else
                                {
                                    if (dt.Rows[i][c].ToString().IndexOf('.') >= 0)
                                    {
                                        attValD = Convert.ToDecimal(tr1.Attributes[dt.Columns[c].ColumnName].ToString());
                                        attValD = attValD + Convert.ToDecimal(dt.Rows[i][c].ToString());
                                        tr1.Attributes[dt.Columns[c].ColumnName] = attValD.ToString();
                                    }
                                    else
                                    {
                                        attVal = Convert.ToInt32(tr1.Attributes[dt.Columns[c].ColumnName].ToString());
                                        attVal = attVal + Convert.ToInt32(dt.Rows[i][c].ToString());
                                        tr1.Attributes[dt.Columns[c].ColumnName] = attVal.ToString();
                                    }
                                }
                            }

                            tr.Cells.Add(tcc);

                            if (i == dt.Rows.Count - 1)
                            {
                                tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                                tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");

                                if (c == 0)
                                    tcc1.InnerText = obj.GetTranslatedText("Total");
                                else
                                {
                                    if (dt.Columns[c].ColumnName.IndexOf("Total Hours") >= 0)
                                    {
                                        tcc1.InnerText = Convert.ToDecimal(tr1.Attributes[dt.Columns[c].ColumnName]).ToString();
                                        totalHrsVD = Convert.ToDecimal(tcc1.InnerText);
                                    }
                                    else if (dt.Columns[c].ColumnName.IndexOf("Avail.Hours") >= 0)
                                    {
                                        availHrsV = Convert.ToInt32(tr1.Attributes[dt.Columns[c].ColumnName]);
                                        tcc1.InnerText = tr1.Attributes[dt.Columns[c].ColumnName].ToString();
                                    }
                                    else if (dt.Columns[c].ColumnName.IndexOf("Usage") >= 0)
                                    {
                                        if (totalHrsVD != 0 && availHrsV != 0)
                                        {
                                            tcc1.InnerText = (Math.Round((Convert.ToDecimal(totalHrsVD) * 100) / Convert.ToDecimal(availHrsV))).ToString() + " %";
                                        }
                                        else
                                            tcc1.InnerText = "0 %";
                                    }
                                    else
                                        tcc1.InnerText = tr1.Attributes[dt.Columns[c].ColumnName].ToString();
                                }
                                tr1.Cells.Add(tcc1);
                            }

                            if (c == dt.Columns.Count - 1 && ii > 0)
                            {
                                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F9F988");
                                tcc.InnerText = confs.ToString();
                                confsT = confsT + confs;
                                tr.Cells.Add(tcc);

                                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                                tcc.InnerText = totalHrsD.ToString();
                                totalHrsTD = totalHrsTD + totalHrsD;
                                tr.Cells.Add(tcc);

                                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C68FDA");
                                tcc.InnerText = availHrs.ToString();
                                availHrsT = availHrsT + availHrs;
                                tr.Cells.Add(tcc);

                                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                                tcc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#C68FDA");
                                tcc.Attributes.Add("NoWrap", "NoWrap");
                                if (totalHrsD != 0 && availHrs != 0)
                                    tcc.InnerText = (Math.Round((Convert.ToDecimal(totalHrsD) * 100) / Convert.ToDecimal(availHrs))).ToString() + " %";
                                else
                                    tcc.InnerText = "0 %";
                                tr.Cells.Add(tcc);
                            }
                            if (i == dt.Rows.Count - 1 && ii > 0)
                            {
                                if (c == dt.Columns.Count - 1)
                                {

                                    tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                                    tcc1.InnerText = confsT.ToString();
                                    tr1.Cells.Add(tcc1);

                                    tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                                    tcc1.InnerText = totalHrsTD.ToString();
                                    tr1.Cells.Add(tcc1);

                                    tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");
                                    tcc1.InnerText = availHrsT.ToString();
                                    tr1.Cells.Add(tcc1);

                                    tcc1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                                    tcc1.Style.Add(HtmlTextWriterStyle.BorderColor, "#C0C0C0");

                                    if (totalHrsTD != 0 && availHrsT != 0)
                                        tcc1.InnerText = (Math.Round((Convert.ToDecimal(totalHrsTD) * 100) / Convert.ToDecimal(availHrsT))).ToString() + " %";
                                    else
                                        tcc1.InnerText = "0 %";
                                    tr1.Cells.Add(tcc1);
                                }
                            }
                        }
                        if (isMonthly)
                        {
                            tblmonthlyRoom.Rows.Add(tr);
                            tblmonthlyRoom.Rows.Add(tr1);
                        }
                        else
                        {
                            tblweeklyRoom.Rows.Add(tr);
                            tblweeklyRoom.Rows.Add(tr1);
                        }
                    }
                }
                else 
                {
                    tr = new System.Web.UI.HtmlControls.HtmlTableRow();
                    tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                    tcc.ColSpan = 4;
                    tcc.InnerText = obj.GetTranslatedText("No Data to display");//FB 2272
                    tr.Cells.Add(tcc);

                    tblmonthlyRoom.Rows.Add(tr);
                    tblweeklyRoom.Rows.Add(tr1);
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Trace("" + ex.StackTrace);
            }
        }

        #endregion

        #region SetWorkingDays
        /// <summary>
        /// SetWorkingDays
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetWorkingDays(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("WorkingDays.aspx");
            }
            catch (Exception ex)
            {
                lblErrLabel.Visible = true;
                lblErrLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("SetWorkingDays: " + ex.StackTrace);
            }
        }
        #endregion

        #region BindHeader

        private void BindHeader(HtmlGenericControl dv, ref HtmlTableRow tr, HtmlTableCell tcc)
        {
            try
            {
                // FB 2913 Starts
                string ht = "200px";
                if (Session["language"].ToString() == "en")
                    ht = "150px";

                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                dv = new System.Web.UI.HtmlControls.HtmlGenericControl("span");
                dv.InnerText = obj.GetTranslatedText("# Confs");
                dv.Style.Add("display", "none");
                tcc.Controls.Add(dv);
                tcc.Attributes.Add("class", "verticaltextyellow");
                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                tcc.Style.Add(HtmlTextWriterStyle.Height, ht);
                tcc.Style.Add("background-color", "#F9F988");
                tr.Cells.Add(tcc);

                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                dv = new System.Web.UI.HtmlControls.HtmlGenericControl("span");
                dv.InnerText = obj.GetTranslatedText("Total Hrs");
                dv.Style.Add("display", "none");
                tcc.Controls.Add(dv);
                tcc.Attributes.Add("class", "verticaltexts");
                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                tcc.Style.Add(HtmlTextWriterStyle.Height, ht);
                tcc.Style.Add("background-color", "transparent");
                tr.Cells.Add(tcc);

                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                dv = new System.Web.UI.HtmlControls.HtmlGenericControl("span");
                dv.InnerText = obj.GetTranslatedText("Available Hrs");
                dv.Style.Add("display", "none");
                tcc.Controls.Add(dv);
                tcc.Attributes.Add("class", "verticaltextviolet1");
                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                tcc.Style.Add(HtmlTextWriterStyle.Height, ht);
                tcc.Style.Add("background-color", "#C68FDA");
                tr.Cells.Add(tcc);

                tcc = new System.Web.UI.HtmlControls.HtmlTableCell();
                dv = new System.Web.UI.HtmlControls.HtmlGenericControl("span");
                dv.InnerText = obj.GetTranslatedText("% Usage (Avail. Hrs)");
                dv.Style.Add("display", "none");
                tcc.Controls.Add(dv);
                tcc.Attributes.Add("class", "verticaltextviolet2");
                tcc.Style.Add(HtmlTextWriterStyle.BorderStyle, "Solid");
                tcc.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                tcc.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
                tcc.Style.Add(HtmlTextWriterStyle.Height, ht);
                tcc.Style.Add("background-color", "#C68FDA");
                tr.Cells.Add(tcc);
                // FB 2913 Ends

            }
            catch (Exception ex)
            {
                log.Trace("" + ex.StackTrace);
            }
        }

        #endregion

     }
    
}
