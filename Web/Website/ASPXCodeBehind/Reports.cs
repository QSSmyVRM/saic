﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Web.Security;
using System.Web.Util;
using System.Linq;//Added for FB 1712
using System.Xml.Linq;
using DevExpress.Web.ASPxEditors; //FB 2155

namespace myVRMNet
{
    public class Reports
    {
        #region Data Members
        /// <summary>
        /// Data Members
        /// </summary>
        
        //private string errXML;
        public Label errLabel;
        public ns_Logger.Logger log;
        public const int defaultOrgID = 11; //Organization Module Fixes
        myVRMNet.NETFunctions obj;


        #endregion

        #region Transfer
        /// <summary>
        /// Transfer
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public String Transfer(String xml)
        {
            return xml.Replace(">", "&gt;").Replace("<", "&lt;");
        }
        #endregion

        #region Reports
        /// <summary>
        /// Reports
        /// </summary>
        public Reports()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region BindBridges
        /// <summary>
        /// BindBridges
        /// </summary>
        /// <param name="sender"></param>
        public void BindBridges(ASPxComboBox sender)
        {
            try
            {
                String inXML = "<GetBridges>" + obj.OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetBridges>";
                String outXML = obj.CallMyVRMServer("GetBridges", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//Bridges/Bridge");
                LoadList(sender, nodes, "BridgeID", "BridgeName");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region LoadCountryList
        /// <summary>
        /// LoadCountryList
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadCountryList(ASPxComboBox lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                DataTable dt = new DataTable();
                dt = ds.Tables[0].Clone();

                string conditionForPriorityCountry = "ID in (38,137,225)";
                string conditionForRemainingCountry = "ID not in (38,137,225)";
                string orderByPriorityCountry = "Name DESC";
                string orderByRemainingCountry = "Name ASC";

                var result = (from pc in ds.Tables[0].Select(conditionForPriorityCountry, orderByPriorityCountry) select pc).
                    Union(from rc in ds.Tables[0].Select(conditionForRemainingCountry, orderByRemainingCountry) select rc);

                foreach (var item in result)
                {
                    dt.ImportRow(item);
                }

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.NewRow();
                    dr["ID"] = "-1";
                    dr["Name"] = obj.GetTranslatedText("Please select...");//FB 1830 -Translation
                    dt.Rows.InsertAt(dr, 0);
                }
                else
                {
                    dt.Columns.Add(col1);
                    dt.Columns.Add(col2);
                    DataRow dr = dt.NewRow();
                    dr["ID"] = "-1";
                    dr["Name"] = obj.GetTranslatedText("No Items...");//FB 1830 -Translation
                    dt.Rows.InsertAt(dr, 0);
                }

                lstList.DataSource = dt;
                lstList.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetCountryCodes
        /// <summary>
        /// GetCountryCodes
        /// </summary>
        /// <param name="lstList"></param>
        public void GetCountryCodes(ASPxComboBox lstList)
        {
            try
            {
                String inXML = "<GetCountryCodes><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "</GetCountryCodes>";
                String outXML = obj.CallMyVRMServer("GetCountryCodes", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetCountryCodes/Country");
                LoadCountryList(lstList, nodes, "ID", "Name"); 
            }
            catch (Exception ex)
            {
                log.Trace("GetCountryCodes: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetCountryStates
        /// <summary>
        /// GetCountryStates
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="strCountryCode"></param>
        public void GetCountryStates(ASPxComboBox lstList, String strCountryCode)
        {
            try
            {
                String inXML = "<GetCountryStates><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<CountryCode>" + strCountryCode + "</CountryCode></GetCountryStates>";
                String outXML = obj.CallMyVRMServer("GetCountryStates", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetCountryStates outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetCountryStates/State");
                LoadList(lstList, nodes, "ID", "Code");
            }
            catch (Exception ex)
            {
                log.Trace("GetCountryStates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindTimeToListBox
        /// <summary>
        /// BindTimeToListBox
        /// </summary>
        /// <param name="cntrl"></param>
        /// <param name="blnOH"></param>
        /// <param name="blnInterval"></param>
        public void BindTimeToListBox(ASPxComboBox cntrl, Boolean blnOH, Boolean blnInterval)
        {
            String val = "";
            Int32 sHours = 0;
            Int32 eHours = 24;
            Int32 sMins = 0;
            Int32 eMins = 0;
            Int32 intval = 60;
            String tFormat = "hh:mm tt";
            try
            {

                if (blnOH)
                {
                    if (HttpContext.Current.Session["SystemStartTime"] != null)
                        if (HttpContext.Current.Session["SystemStartTime"].ToString() != "")
                        {
                            sHours = DateTime.Parse(HttpContext.Current.Session["SystemStartTime"].ToString()).Hour;
                            sMins = DateTime.Parse(HttpContext.Current.Session["SystemStartTime"].ToString()).Minute;
                        }
                    if (HttpContext.Current.Session["SystemEndTime"] != null)
                        if (HttpContext.Current.Session["SystemEndTime"].ToString() != "")
                        {
                            eHours = DateTime.Parse(HttpContext.Current.Session["SystemEndTime"].ToString()).Hour;
                            eMins = DateTime.Parse(HttpContext.Current.Session["SystemEndTime"].ToString()).Minute;
                        }
                }
                if (HttpContext.Current.Session["timeFormat"] != null)
                    if (HttpContext.Current.Session["timeFormat"].ToString().Equals("0"))
                        tFormat = "HH:mm";
                if (blnInterval)
                    if (HttpContext.Current.Application["interval"] != null)
                        if (HttpContext.Current.Application["interval"].ToString() != "")
                            intval = Int32.Parse(HttpContext.Current.Application["interval"].ToString());

                Int32 k = 0;
                sHours = ((eHours < sHours) ? eHours : sHours);
                eHours = ((eHours == 0) ? 24 : eHours);
                eHours = (((eMins > 0) && (eHours != 24)) ? (eHours + 1) : eHours);
                for (k = sHours; k < eHours; k++)
                {
                    for (Int32 i = 0; i < 60; i = i + intval)
                    {
                        i = (((k == sHours) && (i == 0)) ? sMins : i);
                        if ((k == eHours) && (eMins == i))
                            break;
                        String j = "";
                        j = ((i.ToString().Length == 1) ? ("0" + i.ToString()) : i.ToString());
                        val = ((k.ToString().Length == 1) ? ("0" + k.ToString() + ":" + j.ToString()) : (k.ToString() + ":" + j.ToString()));
                        if (HttpContext.Current.Session["timeFormat"].ToString().Equals("2")) //FB 2588
                            cntrl.Items.Add(Convert.ToDateTime(val).ToString("HHmmZ"), Convert.ToDateTime(val).ToString(tFormat));// Convert.ToDateTime(val).ToString(tFormat));
                        else
                            cntrl.Items.Add(Convert.ToDateTime(val).ToString(tFormat));
                    }
                }
                if ((k == eHours) && (eMins == 0) && (k < 24))
                {
                    val = eHours.ToString() + ":" + eMins.ToString();
                    if (HttpContext.Current.Session["timeFormat"].ToString().Equals("2")) //FB 2588
                        cntrl.Items.Add(Convert.ToDateTime(val).ToString("HHmmZ"), Convert.ToDateTime(val).ToString(tFormat));// Convert.ToDateTime(val).ToString(tFormat));
                    else
                        cntrl.Items.Add(Convert.ToDateTime(val).ToString(tFormat));
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                throw ex;
            }

        }
        #endregion

        #region BindOrganizationNames
        /// <summary>
        /// BindOrganizationNames
        /// </summary>
        /// <param name="sender"></param>
        public void BindOrganizationNames(ASPxComboBox sender)
        {
            try
            {
                String inXML = "<Organization><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></Organization>";
                String outXML;
                outXML = obj.CallMyVRMServer("GetOrganizationList", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetOrganizationList/Organization");
                LoadOrgList(sender, nodes, "OrgId", "OrganizationName");
            }
            catch (Exception ex)
            {
                log.Trace("GetAddressType: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region LoadOrgList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="drpOrg"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadOrgList(ASPxComboBox drpOrg, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add(col1);
                    dt.Columns.Add(col2);
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = "No Items...";
                    dt.Rows.InsertAt(dr, 0);
                }
                drpOrg.DataSource = dt;
                drpOrg.DataBind();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        #region LoadList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadList(ASPxComboBox lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                //FB 2577 Start
                XmlNodeList MultiCodeList = null;
                
                string MultiCodecAdds = "";

                for (int n = 0; n < nodes.Count; n++)
                {
                    if (nodes[n] != null)
                    {
                        if (nodes[n].SelectSingleNode("isTelePresence") != null)
                        {
                            if (nodes[n].SelectSingleNode("isTelePresence").InnerText == "1")
                            {
                                if (nodes[n].SelectNodes("//MultiCodec/Address") != null)
                                {
                                    MultiCodeList = nodes[n].SelectNodes("//MultiCodec/Address");

                                    for (int i = 0; i < MultiCodeList.Count; i++)
                                    {
                                        if (MultiCodeList[i] != null)
                                        {
                                            if (MultiCodeList[i].InnerText.Trim() != "")
                                            {
                                                if (i > 0)
                                                    MultiCodecAdds += ",";

                                                MultiCodecAdds += MultiCodeList[i].InnerText.Trim();
                                            }
                                        }
                                    }
                                    if (MultiCodeList.Count > 0)
                                        nodes[n].SelectSingleNode("MultiCodec").InnerText = MultiCodecAdds;
                                }
                            }
                        }
                    }
                    xtr = new XmlTextReader(nodes[n].OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                //FB 2577 End

                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    //Response.Write(ds.Tables[0].Columns[0].ColumnName);
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = obj.GetTranslatedText("Please select...");//FB 1830 - Translation
                    if (lstList.ID != null) // FB 1860
                    {
                        if ((lstList.ID.IndexOf("lstBridge") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0))
                            dr[1] = obj.GetTranslatedText("Auto select...");//FB 1830 - Translation
                        if ((lstList.ID.IndexOf("lstTemplates") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0)) //Added for Template List
                            dr[1] = obj.GetTranslatedText("None...");//FB 1830 - Translation 
                        dt.Rows.InsertAt(dr, 0);
                    }
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add(col1);
                    dt.Columns.Add(col2);
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = obj.GetTranslatedText("No Items...");//FB 1830 - Translation
                    dt.Rows.InsertAt(dr, 0);
                }
                lstList.DataSource = dt;
                lstList.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        //FB 2501 Start
        #region GetAllEndpoints
        public void GetAllEndpoints(ASPxComboBox sender1)
        {
            String userID = "11";
            try
            {
                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                String inXML = "<EndpointDetails><organizationID>" + HttpContext.Current.Session["organizationID"].ToString() + "</organizationID><multisiloOrganizationID>0</multisiloOrganizationID>  <UserID>" + userID + "</UserID></EndpointDetails>";
                String OutXml = obj.CallMyVRMServer("GetAllEndpoints", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(OutXml);                
                XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint");
                LoadList(sender1, nodes, "EndpointID", "EndpointName");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetTimezones        
        public void GetTimezones(ASPxComboBox sender1, ref String sel)
        {
            String userID = "11";
            try
            {
                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                String inXML = "<GetTimezones><organizationID>" + HttpContext.Current.Session["organizationID"].ToString() + "</organizationID><multisiloOrganizationID>0</multisiloOrganizationID> <UserID>" + userID + "</UserID></GetTimezones>";
                String OutXml = obj.CallMyVRMServer("GetTimezones", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(OutXml);
                if (xmldoc.SelectSingleNode("//Timezones/selected") != null)
                    sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;
                XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                LoadList(sender1, nodes, "timezoneID", "timezoneName");                                   
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion        
        //FB 2501 End
    }
}