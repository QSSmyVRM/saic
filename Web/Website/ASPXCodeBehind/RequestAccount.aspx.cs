/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_RequestAccount : System.Web.UI.Page
{

    #region Protected Data Members

    protected System.Web.UI.WebControls.Label ErrLabel;
    protected System.Web.UI.WebControls.Label LblSuccess;
    protected System.Web.UI.WebControls.TextBox TxtFirstName;
    protected System.Web.UI.WebControls.TextBox TxtLastName;
    protected System.Web.UI.WebControls.TextBox TxtLoginName;
    protected System.Web.UI.WebControls.TextBox TxtEmail;
    protected System.Web.UI.WebControls.TextBox TxtConfirmEmail;
    protected System.Web.UI.WebControls.TextBox TxtAdditionalInfo;
    protected System.Web.UI.WebControls.Button BtnReset;
    protected System.Web.UI.WebControls.Button BtnSubmit;

    #endregion

    #region  Variables Declaration

    private myVRMNet.NETFunctions obj = null;
    private ns_Logger.Logger log;

    #endregion

    #region Page Load
    /// <summary>
    /// On Page Load,If Session of UserID is null,then assign values to variables COM_ConfigPath and MyVRMServer_ConfigPath
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("RequestAccount.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            obj = new myVRMNet.NETFunctions();

            if (Session["userID"] == null)
            {
                Session.Add("userID", "11");
                Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
                Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
            }

            InitializeUIResources();

            if (Request.QueryString["m"] != null)
                LblSuccess.Visible = true;
        }
        catch (Exception ex)
        {
            ErrLabel.Visible = true;
            ErrLabel.Text = obj.ShowSystemMessage();//ZD 100263
            WriteIntoLog("PageLoad: " + ex.StackTrace);
        }

    }
    #endregion

    #region InitializeUIResources

    private void InitializeUIResources()
    {
        this.BtnSubmit.Attributes.Add("onclick", "javascript:return fnValidate();");
        //FB 1888
        //this.TxtEmail.Attributes.Add("onkeyup", "javascript:chkLimit(this,'e');");
        //this.TxtConfirmEmail.Attributes.Add("onkeyup", "javascript:chkLimit(this,'e');");
        //this.TxtFirstName.Attributes.Add("onkeyup", "javascript:chkLimit(this,'2');");
        //this.TxtLastName.Attributes.Add("onkeyup", "javascript:chkLimit(this,'2');");
        //this.TxtLoginName.Attributes.Add("onkeyup", "javascript:chkLimit(this,'4');");
    }

    #endregion

    #region Submit Click Event
    /// <summary>
    /// Construct Input XML and Pass it to COM.If Success, then display message and redirect the page again.
    /// If Error, then display the error.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SubmitAccount(Object sender, EventArgs e)
    {
        try
        {
            String inXML = "";
            inXML += "<requestVRMAccount>";
            inXML += "<organizationID>11</organizationID>";//Organisation Module - VRM Administrator
            inXML += "<firstName>" + TxtFirstName.Text + "</firstName>";
            inXML += " <lastName>" + TxtLastName.Text + "</lastName>";
            inXML += " <email>" + TxtEmail.Text + "</email>";
            inXML += " <login>" + TxtLoginName.Text + "</login>";
            inXML += " <additionalInfo>" + TxtAdditionalInfo.Text + "</additionalInfo>";
            inXML += "</requestVRMAccount>";

            String outXML = obj.CallMyVRMServer("RequestVRMAccount", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 1830

            if (outXML.IndexOf("<error>") < 0)
            {
                Response.Redirect("RequestAccount.aspx?m=s");
            }
            else
            {
                ErrLabel.Visible = true;
                ErrLabel.Text = obj.ShowErrorMessage(outXML);
            }
            obj = null;

        }
        catch (System.Threading.ThreadAbortException) { }
        catch (Exception ex)
        {
            ErrLabel.Text = obj.ShowSystemMessage();
            ErrLabel.Visible = true;
            WriteIntoLog("SubmitAccount: "+ex.StackTrace);
        }
    }

    #endregion

    #region WriteIntoLog

    private void WriteIntoLog(String stackTrace)
    {
        log = new ns_Logger.Logger();
        log.Trace(stackTrace);
        log = null;
    }

    #endregion

}
