/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Schema;
using System.Xml;

public partial class en_ResourceAllocation : System.Web.UI.Page
{
    # region Private Members
    private ArrayList entityList = null;
    private static Hashtable roomsList = null;
    private ns_Logger.Logger log;
    //Code added by Offshore for FB Issue 1073 -- Start
    protected String format = "";
    protected String dFormat = "";
    //Code added by Offshore for FB Issue 1073 -- End
    private myVRMNet.NETFunctions obj;
    //Code Added For FB 1425
    protected String timeZone = "0";
    # endregion

    # region Protected members

    protected System.Web.UI.WebControls.TextBox txtStartDate;
    protected System.Web.UI.WebControls.TextBox txtEndDate;
    protected System.Web.UI.WebControls.DropDownList drpTimeZone;
    protected System.Web.UI.WebControls.Button btnviewRep;
    protected MetaBuilders.WebControls.ComboBox CmbStrtTime;
    protected MetaBuilders.WebControls.ComboBox CmbEndTime;
    protected Microsoft.Samples.ReportingServices.ReportViewer ReportViewer1;
    /* **** Code added for FB 1425  *** */
    protected System.Web.UI.HtmlControls.HtmlTableRow trTimeZone;
    String tformat = "hh:mm tt";

     # endregion

    # region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("ResourceAllocationReport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            string serverURL = Application["SQLReportPath"].ToString();
            ReportViewer1.ServerUrl = serverURL;

            //Code added by Offshore for FB Issue 1073 -- Start
            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            if (format == "dd/MM/yyyy")
                dFormat = "dmy";

            else
                dFormat = "mdy";
            //Code added by Offshore for FB Issue 1073 -- End

            /* **** Code added for FB 1425 *** */
            if (Session["timeZoneDisplay"] != null)
            {
                if (Session["timeZoneDisplay"].ToString() != "")
                    timeZone = Session["timeZoneDisplay"].ToString();
            }
            if (timeZone.Equals("0"))
            {
                drpTimeZone.SelectedValue = "26";

                if (Session["systemTimezoneID"] != null)
                {
                    if (Session["systemTimezoneID"].ToString() != "")
                    {
                        drpTimeZone.SelectedValue = Session["systemTimezoneID"].ToString();
                    }
                }
            }
            if (timeZone.Equals("0"))
                trTimeZone.Attributes.Add("style", "display:none");

            /* **** Code added for FB 1425 *** */

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
            obj = ((obj == null) ? new myVRMNet.NETFunctions() : obj);
            CmbStrtTime.Items.Clear();
            CmbEndTime.Items.Clear();
            obj.BindTimeToListBox(CmbStrtTime, false, false);
            obj.BindTimeToListBox(CmbEndTime, false, false);
           
            if (!IsPostBack)
            {
                CmbStrtTime.Text = ((CmbStrtTime.SelectedIndex < 0) ? DateTime.Parse("12:00 AM").ToString(tformat) : CmbStrtTime.Text);
                CmbEndTime.Text = ((CmbEndTime.SelectedIndex < 0) ? DateTime.Parse("12:00 AM").ToString(tformat) : CmbEndTime.Text);

                log = new ns_Logger.Logger();

                String inXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "</GetTimezones>";//Organization Module Fixes
               // String inXML = "<GetTimezones><UserID>11</UserID></GetTimezones>";

               String outXML = obj.CallMyVRMServer("GetTimezones", inXML, Application["MyVRMServer_ConfigPath"].ToString());
               // String outXML = obj.CallMyVRMServer("GetTimezones", inXML, "C:\\VRMSchemas_v1.8.3\\");               


                BindData(outXML);

                String repPath = "";
                repPath = "/" + Application["SQLReports"].ToString() + "%2fResourceAllocation_Table&rs:Command=Render&rs:Format=HTML4.0&rc:Toolbar=true&rc:Parameters=False&startTime=" + myVRMNet.NETFunctions.GetDefaultDate(txtStartDate.Text) + " " + CmbStrtTime.Text + "&endTime=" + myVRMNet.NETFunctions.GetDefaultDate(txtEndDate.Text) + " " + CmbEndTime.Text + "&timezone=" + drpTimeZone.SelectedValue + "&dFormat=" + dFormat + "&txtStart=" + txtStartDate.Text + " " + CmbStrtTime.Text + "&txtEnd=" + txtEndDate.Text + " " + CmbEndTime.Text + "&tzDisplay=" + timeZone.ToString().Trim() + "&tFormat=" + tformat.Trim();
            
                ReportViewer1.ReportPath = repPath;
                ReportViewer1.Visible = true;
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
            Response.Write(obj.ShowSystemMessage());//ZD 100263
        }
    }
    # endregion

    #region IntializeUIResources

    private void IntializeUIResources()
    {
        this.Page.Init += new EventHandler(Page_Init);
        btnviewRep.Attributes.Add("onclick", "javascript:return fnValidate();");
        btnviewRep.Click += new EventHandler(btnviewRep_Click);
    }

    void btnviewRep_Click(object sender, EventArgs e)
    {
        try
        {
            viewReport();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    # region ViewReport
    private void viewReport()
    {
        try
        {

            String repPath = "";
            repPath = "/" + Application["SQLReports"].ToString() + "%2fResourceAllocation_Table&rs:Command=Render&rs:Format=HTML4.0&rc:Toolbar=true&rc:Parameters=False&startTime=" + myVRMNet.NETFunctions.GetDefaultDate(txtStartDate.Text) + " " + CmbStrtTime.Text + "&endTime=" + myVRMNet.NETFunctions.GetDefaultDate(txtEndDate.Text) + " " + CmbEndTime.Text + "&timezone=" + drpTimeZone.SelectedValue + "&dFormat=" + dFormat + "&txtStart=" + txtStartDate.Text + " " + CmbStrtTime.Text + "&txtEnd=" + txtEndDate.Text + " " + CmbEndTime.Text + "&tzDisplay=" + timeZone.ToString().Trim() + "&tFormat=" + tformat.Trim();

            ReportViewer1.ReportPath = repPath;
            ReportViewer1.Visible = true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    # endregion

    #region Page Init Event Handler

    void Page_Init(object sender, EventArgs e)
    {
        try
        {
            IntializeUIResources();
        }
        catch
        {
            log.Trace("The method or operation is not implemented.");//ZD 100263
            Response.Write(obj.ShowSystemMessage());
        }
    }

    #endregion

    #region BindData

    private void BindData(String outXML)
    {
        string tmpstr = "";
        string sel = "";
        string[] tmpstrs;
        int i, tmpint;
        XmlDocument xmldoc = new XmlDocument();
        XmlNodeList nodelist;
        try
        {

            xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);

            log.Trace("GetTimezones: " + outXML);

            sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;
            XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");

            if (nodes.Count > 0)
            {
                obj.LoadList(drpTimeZone, nodes, "timezoneID", "timezoneName");
                
            }
            drpTimeZone.SelectedValue = sel;
            if (dFormat == "dmy")
            {
                txtStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.ToShortDateString());
                txtEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Today.AddDays(1).ToShortDateString());
            }
            else
            {
                txtStartDate.Text =DateTime.Today.ToShortDateString();
                txtEndDate.Text = DateTime.Today.AddDays(1).ToShortDateString();
            }
    
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    #endregion
}
