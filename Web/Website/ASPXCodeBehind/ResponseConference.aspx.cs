/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Text;
// FB 2136 start
using System.IO;
using System.Web.UI;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Web.UI.HtmlControls; //FB 2020
using System.Collections;//ZD 100263
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
// FB 2136 End
/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Conference
{
    public partial class ResponseConference : System.Web.UI.Page
    {

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblConfName;
        protected System.Web.UI.WebControls.Label lblPassword;
        protected System.Web.UI.WebControls.Label lblConfUniqueID;
        protected System.Web.UI.WebControls.Label lblConfType;
        protected System.Web.UI.WebControls.Label lblLocation;
        protected System.Web.UI.WebControls.Label lblConfDate;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        protected System.Web.UI.WebControls.Label lblTimezone;
        protected System.Web.UI.WebControls.Label lblFiles;
        protected System.Web.UI.WebControls.Label lblDescription;//FB 1964
        
        protected System.Web.UI.WebControls.Label lblUserName;
        protected System.Web.UI.WebControls.Label lblContactName;
        protected System.Web.UI.WebControls.Label lblContactEmail;
        protected System.Web.UI.WebControls.Label lblContactPhone;
        protected System.Web.UI.WebControls.Label lblAccept;
        protected System.Web.UI.WebControls.Label lblBridgeAddress;
        protected System.Web.UI.WebControls.Label lblBridgeName;
        protected System.Web.UI.WebControls.Label lblBridgeAddressType;

        protected System.Web.UI.WebControls.HiddenField txtUserID;
        protected System.Web.UI.WebControls.HiddenField txtConfID;
        protected System.Web.UI.WebControls.HiddenField txtDecision;

        protected System.Web.UI.WebControls.DropDownList lstTimeZone;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstInterfaceType;
        protected MetaBuilders.WebControls.ComboBox lstDuration;

        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected MetaBuilders.WebControls.ComboBox txtStartTime;
        protected System.Web.UI.WebControls.TextBox txtStartDate;
        protected System.Web.UI.WebControls.TextBox txtComments;

        protected System.Web.UI.WebControls.RadioButtonList rdRooms;
        protected System.Web.UI.WebControls.DataGrid dgInstanceList;

        protected System.Web.UI.WebControls.CheckBox chkOutsideNetwork;
        protected System.Web.UI.WebControls.Button btnConfirmAccept;
        protected System.Web.UI.WebControls.Button btnConfirmChange;

        protected System.Web.UI.HtmlControls.HtmlTableRow trAccept;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRgprocess; //FB 2459
        protected System.Web.UI.HtmlControls.HtmlTableRow trPkroom; //FB 2459
        protected System.Web.UI.HtmlControls.HtmlTableRow trRecur;
        protected System.Web.UI.HtmlControls.HtmlTableRow trButtons;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRecurButton;
        protected System.Web.UI.HtmlControls.HtmlTableRow trChange;
        protected System.Web.UI.HtmlControls.HtmlTableRow trButtonsHeader;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAcceptMessage;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAcceptConfirm;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConnection;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConnection1;
        protected System.Web.UI.HtmlControls.HtmlInputHidden tempText;

        protected System.Web.UI.WebControls.CustomValidator CustomValidator1;
        protected System.Web.UI.WebControls.CustomValidator customAccept;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRecur;//FB 1757

        // FB 2136 start
        protected System.Web.UI.WebControls.FileUpload uploadImage;
        protected System.Web.UI.WebControls.Image imgCont;
        protected System.Web.UI.HtmlControls.HtmlInputText Text1;
        protected System.Web.UI.HtmlControls.HtmlInputText Text2;
        protected System.Web.UI.HtmlControls.HtmlInputText selectedFile;
        protected System.Web.UI.WebControls.Image imgPrev;

        protected myVRMWebControls.ImageControl badgeIcon1;
        protected System.Web.UI.WebControls.Button btnRemoveImg1;
        protected System.Web.UI.WebControls.Label hdnImageSize;
        protected System.Web.UI.WebControls.HiddenField hdnObjAxis;
        protected System.Web.UI.WebControls.HiddenField hdnImgName;
        protected System.Web.UI.WebControls.HiddenField hdnIsEnabledSecBadge;

        protected System.Web.UI.HtmlControls.HtmlTableRow secBadgeTr1;
        protected System.Web.UI.HtmlControls.HtmlTableRow secBadgeTr2;
        protected System.Web.UI.WebControls.Label lblhost;
        // FB 2136 end 
        // FB 2446 - Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpw;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpw1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpwd;//FB 2565
        // FB 2446 - End

        myVRMNet.NETFunctions obj;
        MyVRMNet.Util utilObj; //FB 2236

        String COM_ConfigPath = "C:\\VRMSchemas_v1.8.3\\COMConfig.xml";
        String MyVRMServer_ConfigPath = "C:\\VRMSchemas_v1.8.3\\";
        ns_Logger.Logger log;
        String tformat = "hh:mm tt";//FB 1628
        int encrypted = 0;//FB 1757
        protected String language = "";//FB 1830
        // FB 2136 Starts
        myVRMNet.ImageUtil imageUtilObj = null;
        private String SecImg1Name = "SecImage1.jpg";
        string SecTextAxis = "", SecPhotoAxis = "", SecBarCodeAxis = "", PartyphotoPath = "";
        // FB 2136 Ends
        protected String enableconferencepassword = "0";//FB 2446
       
        public ResponseConference()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util();
			imageUtilObj = new myVRMNet.ImageUtil(); //FB 2136

            //
            // TODO: Add constructor logic here
            //
        }
        private void Page_Init(object sender, System.EventArgs e)
        {
            Application.Add("COM_ConfigPath", COM_ConfigPath);
            Application.Add("MyVRMServer_ConfigPath", MyVRMServer_ConfigPath);
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ResponseConference.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                // ZD 100170                
                string path = Request.Url.AbsoluteUri.ToLower();
                if (path.IndexOf("%3c") > -1 || path.IndexOf("%3e") > -1)
                    Response.Redirect("ShowError.aspx");

                //FB 1830
                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 1830 - End
                errLabel.Text = "";
                PartyphotoPath = Server.MapPath("../image/SecurityBadge/partyPhoto_" + txtUserID.Value + ".jpg"); //FB 2136

                if (!IsPostBack)
                {
                    String selTimezone = "26";
                    obj.GetTimezones(lstTimeZone, ref selTimezone);
                    obj.BindAddressType(lstAddressType);
                    lblHeader.Text = obj.GetTranslatedText("Thank you! Your response has been recorded");//FB 1830 - Translation //FB 2391
                    //int encrypted = 1;//FB 1757 - Commented
                    if (Request.QueryString["t"] == "2")
                    {
                        Session.Add("req", Request.QueryString["req"]);
                        Session.Add("id", Request.QueryString["id"]);
                        encrypted = 1; //FB 1757
                    }

                    //FB 1628 START
                    if (Session["req"] == null && Session["id"] == null)
                    {
                        Session.Add("req", Request.QueryString["req"]);
                        Session.Add("id", Request.QueryString["id"]);
                    }
                    if (Session["systemDate"] == null)
                    {
                        //obj.GetSystemDateTime(Application["COM_ConfigPath"].ToString());
                        obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                        //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                        //Session.Add("systemDate", DateTime.Now.ToString("MM/dd/yyyy"));
                        //Session.Add("systemTime", DateTime.Now.ToString(tformat));
                    }
                    //FB 1628 END
                    txtUserID.Value = Session["req"].ToString();
                    txtConfID.Value = Session["id"].ToString();
                    //FB 2027 - Starts
                    StringBuilder inXML = new StringBuilder();
                    inXML.Append("<login>");
                    inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                    inXML.Append("<userID>" + txtUserID.Value + "</userID>");
                    inXML.Append("<confID>" + txtConfID.Value + "</confID>");
                    inXML.Append("<encrypted>" + encrypted + "</encrypted>");
                    inXML.Append("</login>");
                    log.Trace(inXML.ToString());
                    //Response.Write(obj.Transfer(inXML));
                    String outXML = obj.CallMyVRMServer("ResponseInvite", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    //String outXML = obj.CallCOM("ResponseInvite", inXML.ToString(), Application["COM_ConfigPath"].ToString());
                    //FB 2027 - End
                    log.Trace("ResponseInvite : OutXML" + outXML);//FB 1628
                    //Response.Write(obj.Transfer(outXML));
                    if (outXML.IndexOf("<error>") < 0)
                    { //FB 1628 START
                        XmlDocument NewXmlDoc = new XmlDocument();
                        NewXmlDoc.LoadXml(outXML);
                        // FB 1887 - Start
                        if (Session["userid"] == null)
                            Session["userID"] = "11";

                        if(Session["organizationID"] == null)
                            Session["organizationID"] = "11";

                        // FB 1887 - End
                        Session["confID"] = NewXmlDoc.DocumentElement.SelectSingleNode("conferences/conference/confInfo/confID").InnerText;
                        XmlNode node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/lotus");
                        Session["lnLoginName"] = node.SelectSingleNode("lnLoginName").InnerText;
                        Session["lnLoginPwd"] = node.SelectSingleNode("lnLoginPwd").InnerText;
                        Session["lnDBPath"] = node.SelectSingleNode("lnDBPath").InnerText;

                        node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/contactDetails");
                        Application["contactName"] = node.SelectSingleNode("name").InnerText;
                        Application["contactEmail"] = node.SelectSingleNode("email").InnerText;
                        Application["contactPhone"] = node.SelectSingleNode("phone").InnerText;
                        Application["contactAddInfo"] = node.SelectSingleNode("additionInfo").InnerText;

                        Session["dialoutEnabled"] = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/dialoutEnabled").InnerText;

                        hdnIsEnabledSecBadge.Value = "1";
                        if (NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/EnableSecurityBadge") != null)//FB 2136
                            if (NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/EnableSecurityBadge").InnerText.Trim() != "1" && NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/EnableSecurityBadge").InnerText.Trim() != "")
                                hdnIsEnabledSecBadge.Value = "0";

                        //FB 1628 END
                        DisplayConferenceDetails(outXML);
                        //Code added for FB 1694 Starts
                        trAccept.Visible = false;
                        trChange.Visible = false;
                        //Code added for FB 1694 End
                    }
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);


                    //FB 2136 start
                    PartyphotoPath = Server.MapPath("../image/SecurityBadge/partyPhoto_" + txtUserID.Value + ".jpg");
                    secBadgeTr1.Visible = false;
                    secBadgeTr2.Visible = false;

                    if (hdnIsEnabledSecBadge.Value == "1")
                    {
                        if (rdRooms.Items.Count >= 1 && rdRooms.SelectedValue.Trim() != "-1" && hdnImgName.Value.Trim() != "")
                        {
                            secBadgeTr1.Visible = true;
                            secBadgeTr2.Visible = true;
                        }

                        if (File.Exists(PartyphotoPath))
                        {

                            System.Drawing.Image img = System.Drawing.Image.FromFile(PartyphotoPath);
                            MemoryStream ms = new MemoryStream();
                            img.Save(ms, ImageFormat.Jpeg);
                            badgeIcon1.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                            badgeIcon1.Visible = true;
                            btnRemoveImg1.Visible = true;
                            uploadImage.Visible = false;
                            img.Dispose();
                        }
                    }
                    //FB 2136 end
                }
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                //FB 1628 START
                //Cache.Remove(Session["req"].ToString());
                //Cache.Remove(Session["id"].ToString());

                if (Session["req"] != null)
                    Session["req"] = null;
                if (Session["id"] != null)
                    Session["id"] = null;
                //FB 1628 END
                //FB 2446 - Start                   
                if (Session["EnableConfPassword"] != null)
                    enableconferencepassword = Session["EnableConfPassword"].ToString();
                if(enableconferencepassword != null)
                    if (enableconferencepassword == "0")
                    {
                        tdpw.Attributes.Add("style", "display:none");
                        tdpw1.Attributes.Add("style", "display:none");
                        tdpwd.Attributes.Add("style", "display:none");//FB 2565
                    }
                //FB 2446 - End  

            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;//ZD 100263                
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }

        }
        private void DisplayConferenceDetails(String outXML)
        {
            try
            {
                // FB 2136 Start
                String SecImgString = "", partyInvite = "";//FB 2347R
                FileStream newFile = null;
                byte[] imgArr = null;
                // FB 2136 End
                XmlDocument xmldoc = new XmlDocument();
                outXML = outXML.Replace("& ", "&amp; ");
                xmldoc.LoadXml(outXML);
                lblConfName.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confName").InnerText;
                lblPassword.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confPassword").InnerText;
                txtConfID.Value = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confID").InnerText;
                lblConfUniqueID.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confUniqueID").InnerText;
                lblhost.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confHost").InnerText; //FB 2136
                lblConfType.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/createBy").InnerText;
                partyInvite = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/partyInvite").InnerText; //FB 2347R
                                               
                txtUserID.Value = xmldoc.SelectSingleNode("//responseInvite/userID").InnerText;
                XmlNodeList nodes = xmldoc.SelectNodes("//responseInvite/conferences/conference/confInfo/mainLocation/location");
                string selRooms = ", ";
                Int32[] selectedRooms = new Int32[nodes.Count]; // = { 0 }; // = new Int32();
                // FB 2459 starts
               if (nodes.Count > 0)
                    trPkroom.Visible = true;
                 else
                    trPkroom.Visible = false;
               if (lblConfType.Text.Equals("7") && lblLocation.Text.Length.Equals(0))
                   trRgprocess.Visible = false;
               else
                   trRgprocess.Visible = true;
                // FB 2459 end
                foreach (XmlNode selNode in nodes)
                {
                    selRooms += selNode.SelectSingleNode("locationID").InnerText + ", ";
                    lblLocation.Text += selNode.SelectSingleNode("locationName").InnerText + ", <br/>"; //FB 2027ResponseInvite
                    rdRooms.Items.Add(new ListItem(selNode.SelectSingleNode("locationName").InnerText, selNode.SelectSingleNode("locationID").InnerText));

                    //FB 2136 start

                    if (selNode.SelectSingleNode("SecurityName") != null)
                        SecImg1Name = selNode.SelectSingleNode("SecurityName").InnerText.ToString().Trim();

                    if (selNode.SelectSingleNode("SecurityImage") != null)
                        SecImgString = selNode.SelectSingleNode("SecurityImage").InnerXml.ToString();

                    if (selNode.SelectSingleNode("SecBadgeTextAxis") != null)
                        SecTextAxis = selNode.SelectSingleNode("SecBadgeTextAxis").InnerXml.ToString();

                    if (selNode.SelectSingleNode("SecBadgePhotoAxis") != null)
                        SecPhotoAxis = selNode.SelectSingleNode("SecBadgePhotoAxis").InnerXml.ToString();

                    if (selNode.SelectSingleNode("SecBadgeBarCodeAxis") != null)
                        SecBarCodeAxis = selNode.SelectSingleNode("SecBadgeBarCodeAxis").InnerXml.ToString();


                    if (SecImgString.Trim() != "")
                    {
                        imgArr = imageUtilObj.ConvertBase64ToByteArray(SecImgString);
                        
                        SecImg1Name = selNode.SelectSingleNode("locationID").InnerText + "_SecBadge.jpg"; // lblConfUniqueID.Text + "_" + selNode.SelectSingleNode("locationID").InnerText + "_" + txtUserID.Value + "_" + SecImg1Name;
                        hdnObjAxis.Value += SecTextAxis + "," + SecPhotoAxis + "," + SecBarCodeAxis + "�";


                        if (File.Exists(Server.MapPath("~/image/SecurityBadge/") + SecImg1Name))
                            File.Delete(Server.MapPath("~/image/SecurityBadge/") + SecImg1Name);

                        if (imgArr != null)
                        {
                            newFile = new FileStream(Server.MapPath("~/image/SecurityBadge/") + SecImg1Name, FileMode.Create);
                            newFile.Write(imgArr, 0, imgArr.Length);
                            newFile.Close();
                            imgArr = null;

                            //MemoryStream ms = new MemoryStream(ima.FromBase64String(SecImgString));    // dr["badgeimage"]
                            //System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                            //returnImage.Save(Server.MapPath("../image/SecurityBadge/" + SecImg1Name));

                        }
                        
                        hdnImgName.Value = SecImg1Name;
                    }

                    //FB 2136 End

                }
                // Commented for FB 1738
                //if (!lblConfType.Text.Equals("7"))
                //    rdRooms.Items.Add(new ListItem("Other", "-1"));
                // fogbugz case 355
                try
                {
                    //Response.Write(rdRooms.Items.Count);
                    if (rdRooms.Items.Count > 0)
                        rdRooms.Items[0].Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace(ex.Message + " : " + ex.StackTrace);
                }
                // fogbugz case 355 ends here
               
                // FB 1738
                if (!lblConfType.Text.Equals("7") && partyInvite != "3") //FB 2347R
                    rdRooms.Items.Add(new ListItem("Other", "-1"));

                if (partyInvite == "3") //FB 2347R
                    rdRooms.Items.Add(new ListItem("PC Attendee", "-2"));

                if (lblLocation.Text.Length.Equals(0))
                    lblLocation.Text = "<font class='blackblodtext'>" + obj.GetTranslatedText("No Locations") + "</font>";//FB 1830 - Translation
                else
                    lblLocation.Text = lblLocation.Text.Substring(0, lblLocation.Text.Length - 7);//FB 2027ResponseInvite
                //lblLocation.Text = lblLocation.Text.Substring(0, lblLocation.Text.Length - 2);

                switch (lblConfType.Text.ToString())
                {
                    case "7": //ConferenceType.Room.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Room Conference");//FB 1830 - Translation
                        break;
                    case "6": // ConferenceType.AudioOnly.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Audio Only Conference");//FB 1830 - Translation
                        break;
                    case "2": // ConferenceType.AudioVideo.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Audio/Video Conference");//FB 1830 - Translation
                        break;
                    case "4": // ConferenceType.P2P.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Point-To-Point Conference");//FB 1830 - Translation
                        break;
                    default:
                        lblConfType.Text = "Undefined";
                        break;
                }
                String startDate = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startDate").InnerText;
                startDate += " " + xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startHour").InnerText;
                startDate += ":" + xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startMin").InnerText;
                startDate += " " + xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startSet").InnerText;

                //Code changed for FB Issue 1073
                //lblConfDate.Text = DateTime.Parse(startDate).ToString("MM/dd/yyyy hh:mm tt");
                lblConfDate.Text = DateTime.Parse(startDate).ToString("f");
                int dur = Int32.Parse(xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/durationMin").InnerText);
                lblConfDuration.Text = (dur / 60) + " hour(s) " + (dur % 60) + " min(s)"; //.ToString();
                string tzName = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/timeZoneName").InnerText;
                lblTimezone.Text = tzName;
                nodes = xmldoc.SelectNodes("//responseInvite/conferences/conference/confInfo/fileUpload/file");
                lblFiles.Text = "";
                foreach (XmlNode node in nodes)
                    if (!node.InnerText.Equals(""))
                    {
                        //FB 1830
                        //String fPath = node.InnerText;
                        //fPath = fPath.Replace("\\", "/");
                        //int startIndex = fPath.IndexOf("/" + language + "/");
                        //int len = fPath.Length - 1;
                        //fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + fPath.Substring(startIndex + 3);//FB 1830

                        String fileName = getUploadFilePath(node.InnerText);
                        String fPath = node.InnerText;
                        int startIndex = fPath.IndexOf(@"\en\");
                        if ((language == "en" || fPath.IndexOf(@"\en\") > 0) && startIndex > 0)
                        {
                            fPath = fPath.Replace("\\", "/");
                            int len = fPath.Length - 1;
                            //Response.Write(fPath + " : " + startIndex + " : " + len);
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830
                        }
                        else
                            fPath = "../Image/" + fileName;

                        lblFiles.Text += "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>, ";
                    }
                if (lblFiles.Text.Length > 0)
                    lblFiles.Text = lblFiles.Text.Substring(0, lblFiles.Text.Length - 2);
                else
                    lblFiles.Text = "N/A";//FB 2436
                lblDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters((xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/description").InnerText), 2).Trim(); //FB 1964 //FB 2236
                lblUserName.Text = xmldoc.SelectSingleNode("//responseInvite/globalInfo/userName/firstName").InnerText + " " + xmldoc.SelectSingleNode("//responseInvite/globalInfo/userName/lastName").InnerText;
                lblContactName.Text = xmldoc.SelectSingleNode("//responseInvite/globalInfo/contactDetails/name").InnerText;
                lblContactEmail.Text = xmldoc.SelectSingleNode("//responseInvite/globalInfo/contactDetails/email").InnerText;
                lblContactPhone.Text = xmldoc.SelectSingleNode("//responseInvite/globalInfo/contactDetails/phone").InnerText;
                txtAddress.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/response/connect/IPISDNAddress").InnerText;
                obj.BindDialingOptions(lstConnectionType);
                lstConnectionType.ClearSelection();
                try
                {
                    lstConnectionType.Items.FindByValue(xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/response/connect/connectionType").InnerText).Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace(ex.Message + " : " + ex.StackTrace);
                }
                lstAddressType.ClearSelection();
                try
                {
                    lstAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/response/connect/addressType").InnerText).Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace(ex.Message + " : " + ex.StackTrace);
                }
                lstInterfaceType.ClearSelection();
                try
                {
                    //if (!xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/response/connect/videoProtocol").InnerText.Equals("0"))
                    lstInterfaceType.Items.FindByText(xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/response/connect/videoProtocol").InnerText).Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace(ex.Message + " : " + ex.StackTrace);
                }
                if (xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/response/connect/addressType").InnerText.Equals("1"))
                    chkOutsideNetwork.Checked = true;
                else
                    chkOutsideNetwork.Checked = false;
               
                //for recurring conferences
                //inXML += "  <conferenceID>" + txtConfID.Value.Split(',')[0] + "</conferenceID>";
                String inXML = "<login><userID>" + txtUserID.Value + "</userID><conferenceID>" + Session["id"].ToString() + "</conferenceID><encrypted>" + encrypted + "</encrypted></login>";
                String outXMLRecur = obj.CallMyVRMServer("GetInstances", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027S
                //String outXMLRecur = obj.CallCOM("GetInstances", inXML, Application["COM_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXMLRecur));
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXMLRecur);
                nodes = xmldoc.SelectNodes("//getInstance/instances/instance");
                //Response.Write(nodes.Count);
                if (nodes.Count > 1)
                {
                    trRecur.Visible = true;
                    txtConfID.Value = txtConfID.Value.Split(',')[0];
                    GetInstances(txtConfID.Value, txtUserID.Value, nodes);
                    trButtons.Visible = false;
                    trRecurButton.Visible = true;
                    trAccept.Visible = true;
                    trChange.Visible = true;
                    btnConfirmAccept.Visible = false;
                    btnConfirmChange.Visible = false;
                    lblAccept.Visible = false;
                    trButtonsHeader.Visible = false;
                    trAcceptMessage.Visible = false;
                    hdnRecur.Value = "1";//FB 1757
                }
                //else if (nodes.Count == 1 && !nodes[0].SelectSingleNode("instanceInfo/seriesID").InnerText.Equals("1"))
                else if (nodes.Count == 1 && nodes[0].SelectSingleNode("instanceInfo/recurring").InnerText.Equals("1"))//Code Added for FB 1701
                {
                    trRecur.Visible = true;
                    txtConfID.Value = txtConfID.Value.Split(',')[0];
                    GetInstances(txtConfID.Value, txtUserID.Value, nodes);
                    trButtons.Visible = false;
                    trRecurButton.Visible = true;
                    trAccept.Visible = true;
                    trChange.Visible = true;
                    btnConfirmAccept.Visible = false;
                    btnConfirmChange.Visible = false;
                    lblAccept.Visible = false;
                    trButtonsHeader.Visible = false;
                    trAcceptMessage.Visible = false;
                    hdnRecur.Value = "1";//FB 1757
                }
                else
                {
                    trRecur.Visible = false;
                    hdnRecur.Value = "0";//FB 1757
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;//ZD 100263
                log.Trace("Details: " + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }

        protected void GetInstances(String ConfID, String UserID, XmlNodeList nodes)
        {
            try
            {
                DataTable dtInstances = new DataTable();
                dtInstances = GetInstancesDataTable(nodes, ConfID, lblConfName.Text);
                dgInstanceList.DataSource = dtInstances;
                dgInstanceList.DataBind();

                //Code Added for FB 1703 Start
                System.Web.UI.Control header = dgInstanceList.Controls[0].Controls[0];
                RadioButtonList rdDecideAllLst = ((RadioButtonList)header.FindControl("rdDecideAll"));
                if (dgInstanceList.Items.Count == 1 || dgInstanceList.Items.Count == 0)
                { 
                    rdDecideAllLst.Visible = false;
                }
                else
                {
                    rdDecideAllLst.Visible = true;
                }
                //Code Added for FB 1703 End
                //FB 2020
                foreach (DataGridItem dgi in dgInstanceList.Items)
                {
                    String selTimezone = "26";
                    DropDownList lsttmZone = (DropDownList)dgi.FindControl("lstTimeZone");
                    if (lsttmZone != null)
                        obj.GetTimezones(lsttmZone, ref selTimezone);
                    TextBox txtStDate = (TextBox)dgi.FindControl("txtStartDate");
                    HtmlImage cal_triggerd = (HtmlImage)dgi.FindControl("cal_triggerd");
                    if (cal_triggerd != null)
                        cal_triggerd.Attributes.Add("onclick", "return showCalendar('" + txtStDate.ClientID + "', '" + cal_triggerd.ClientID + "', 1, '%m/%d/%Y');");
                }
                
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetInstances" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
            }
        }

        protected DataTable GetInstancesDataTable(XmlNodeList nodes, String confID, String confName)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
				//FB 2020
                //Response.Write(obj.Transfer(nodes.Item(0).InnerXml));
                //foreach (XmlNode node in nodes)
                //{
                //    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                //    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                //}
                DataView dv;
                DataTable dt = new DataTable();

                //if (ds.Tables.Count > 0)
                if (nodes.Count > 0)
                {
                    //dv = new DataView(ds.Tables[0]);
                    //dt = dv.Table;
                    if (!dt.Columns.Contains("confID"))
                        dt.Columns.Add("confID");
                    if (!dt.Columns.Contains("confName"))
                        dt.Columns.Add("confName");
                    if (!dt.Columns.Contains("confUniqueID"))
                        dt.Columns.Add("confUniqueID");
                    if (!dt.Columns.Contains("confDate"))
                        dt.Columns.Add("confDate");
                    if (!dt.Columns.Contains("confTime"))
                        dt.Columns.Add("confTime");
                    if (!dt.Columns.Contains("owner"))
                        dt.Columns.Add("owner");
                    if (!dt.Columns.Contains("partyInvite"))
                        dt.Columns.Add("partyInvite");
                    if (!dt.Columns.Contains("hostEmail"))
                        dt.Columns.Add("hostEmail");
                    if (!dt.Columns.Contains("ResponseFor"))
                        dt.Columns.Add("ResponseFor");
                    if (!dt.Columns.Contains("ResponseLevel"))
                        dt.Columns.Add("ResponseLevel");
                    if (!dt.Columns.Contains("ResponseID"))
                        dt.Columns.Add("ResponseID");
                    if (!dt.Columns.Contains("Message"))
                        dt.Columns.Add("Message");
                    //Code added for FB issue 1073
                    if (!dt.Columns.Contains("confDate"))
                        dt.Columns.Add("confDate");

                    if (!dt.Columns.Contains("uniqueID"))
                        dt.Columns.Add("uniqueID");


                    DataRow dr;
                    //foreach (DataRow dr in dt.Rows)
                    foreach(XmlNode node in nodes)
                    {
                        //dr["confID"] = confID + "," + dr["seriesID"];
                        //dr["confName"] = confName;
                        ////Code added for FB issue 1073
                        //String startDate = dr["startDate"].ToString();
                        //startDate += " " + dr["startHour"].ToString();
                        //startDate += ":" + dr["startMin"].ToString();
                        dr = dt.NewRow();
                        dr["confID"] = confID + "," + node.SelectSingleNode("instanceInfo/seriesID").InnerText;
                        dr["confName"] = confName;
                        dr["uniqueID"] = node.SelectSingleNode("instanceInfo/uniqueID").InnerText;
                        //Code added for FB issue 1073
                        String startDate = node.SelectSingleNode("instanceInfo/startDate").InnerText;
                        startDate += " " + node.SelectSingleNode("instanceInfo/startHour").InnerText;
                        startDate += ":" + node.SelectSingleNode("instanceInfo/startMin").InnerText;

                        dr["confDate"] = DateTime.Parse(startDate).ToString("f"); //+ " " + dr["startSet"].ToString(); ;

                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetInstancesDataTable" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
                return null;
            }
        }

        protected string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '\\' };
                string[] fa = fpn.Split(splitter[0]);
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }

        protected void AcceptConference(Object sender, EventArgs e)
        {
            try
            {
                //FB 2027 - Start
                String inXML = "<party>" + obj.OrgXMLElement() + "<userID>" + txtUserID.Value + "</userID><confID>" + txtConfID.Value + "</confID><mode>1</mode></party>";//Organization Module Fixes
                //String outXML = obj.CallCOM("AcceptInvite", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("PartyInvitation", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
                if (outXML.IndexOf("<error>") < 0)
                {
                    txtDecision.Value = "1";
                    trAccept.Visible = true;
                    trChange.Visible = false;
                    try
                    {
                        //rdRooms.Items.FindByValue("-1").Selected = true;
                        //Response.Write("here");
                        //trConnection.Visible = true;
                        //trConnection1.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                }
                else
                    errLabel.Text = obj.ShowErrorMessage(outXML);
            }
            catch (Exception ex)
            {                
                //ZD 100263
                log.Trace("AcceptConference" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }

        protected void ChangeRoom(Object sender, EventArgs e)
        {
            try
            {
                if (rdRooms.SelectedValue.Equals("-1"))
                {
                    trConnection.Visible = true;
                    trConnection1.Visible = true;
                }
                else
                {
                    trConnection.Visible = false;
                    trConnection1.Visible = false;
                }

                if (hdnIsEnabledSecBadge.Value == "1" &&
                     File.Exists(Server.MapPath("~/image/SecurityBadge/" + rdRooms.SelectedValue.Trim() + "_SecBadge.jpg"))) //FB 2136 
                {
                    secBadgeTr1.Visible = true;
                    secBadgeTr2.Visible = true;
                }
                else
                {
                    secBadgeTr1.Visible = false;
                    secBadgeTr2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        protected void RequestChange(Object sender, EventArgs e)
        {
            String seltmzne = "26";//Code added for Error 200
            try
            {
                Context.Application.Add("COM_ConfigPath", COM_ConfigPath);//Code added for Error 200
                Context.Application.Add("MyVRMServer_ConfigPath", MyVRMServer_ConfigPath);//Code added for Error 200                

                trAccept.Visible = false;
                //Response.Write("in Requestchange");
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + txtUserID.Value + "</userID><confID>" + txtConfID.Value + "</confID></login>";//Organization Module Fixes
                //FB 2027 strat
                String outXML = obj.CallMyVRMServer("CounterInvite", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = obj.CallCOM("CounterInvite", inXML, Application["COM_ConfigPath"].ToString());
                //FB 2027 end
                if (outXML.IndexOf("<error>") < 0)
                {
                    //errLabel.Text = "operation Successful!";//Code added for FB 1694
                    trChange.Visible = true;
                    lstTimeZone.Items.Clear();//Code added for Error 200
                    obj.GetTimezones(lstTimeZone, ref seltmzne);//Code added for Error 200
                    if (lstTimeZone.Items.FindByValue("26") != null)//Code added for Error 200
                        lstTimeZone.Items.FindByValue("26").Selected = true;//Code added for Error 200
                    txtStartTime.Text = "08:00 AM";
                    lstDuration.Text = "01:00";
                    txtDecision.Value = "3";
                    trAccept.Visible = false;
                }
                else
                {
                    log.Trace("RequestChange:" + outXML); 
                    errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                    //errLabel.Text = "Please supply this error code to VRM Administrator: " + outXML;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("RequestChange" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
            }
        }
        protected void RejectConference(Object sender, EventArgs e)
        {
            try
            {
                //FB 2027 - Starts
                String inXML = "<party>" + obj.OrgXMLElement() + "<userID>" + txtUserID.Value + "</userID><confID>" + txtConfID.Value + "</confID><mode>2</mode></party>";//Organization Module Fixes
                //String outXML = obj.CallCOM("RejectInvite", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("PartyInvitation", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    errLabel.Text = obj.GetTranslatedText("We are sorry that you will not be able to join the following conference");//FB 1830 - Translation
                    trAccept.Visible = false;
                    trChange.Visible = false;
                    trButtons.Visible = false;
                    trButtonsHeader.Visible = false;
                    txtDecision.Value = "2";
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("RejectConference" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        protected void ConfirmAccept(Object sender, EventArgs e)
        {

            try
            {
                Page.Validate();
                //Response.Write("Confirm accept: " + Page.IsValid);
                if (IsValid)
                {
                    SecurityBadgeImage(); //FB 2136

                    CustomValidator1.Text = "";
                    customAccept.Text = "";
                    String inXML = "";
                    inXML += "<ConfirmConferenceInvitation>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <UserID>" + txtUserID.Value + "</UserID>";
                    inXML += "  <Conferences>";
                    inXML += "      <Conference>";
                    inXML += "          <ConfID>" + txtConfID.Value + "</ConfID>";
                    inXML += "          <Decision>" + txtDecision.Value + "</Decision>";
                    inXML += "          <Reason></Reason>";
                    inXML += "          <LocationID>" + rdRooms.SelectedValue + "</LocationID>";
                    inXML += "          <VideoProtocol>" + lstInterfaceType.SelectedValue + "</VideoProtocol>";
                    inXML += "          <IPISDNAddress>" + txtAddress.Text + "</IPISDNAddress>";
                    inXML += "          <ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                    if (chkOutsideNetwork.Checked)
                        inXML += "          <IsOutsideNetwork>1</IsOutsideNetwork>";
                    else
                        inXML += "          <IsOutsideNetwork>0</IsOutsideNetwork>";
                    inXML += "          <AddressType>" + lstAddressType.SelectedValue + "</AddressType>";
                    inXML += "          <StartDate>" + txtStartDate.Text + "</StartDate>";
                    inXML += "          <StartTime>" + txtStartTime.Text + "</StartTime>";
                    inXML += "          <Duration>" + lstDuration.Text + "</Duration>";
                    inXML += "          <Timezone>" + lstTimeZone.SelectedValue + "</Timezone>";
                    inXML += "          <Comment>" + txtComments.Text + "</Comment>";
                    inXML += "      </Conference>";
                    inXML += "  </Conferences>";
                    inXML += "</ConfirmConferenceInvitation>";
                    //Response.Write(obj.Transfer(inXML));
                    log.Trace("ConfirmConferenceInvitation Inxml: " + inXML);
                    String outXML = obj.CallMyVRMServer("ConfirmConferenceInvitation", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("ConfirmConferenceInvitation outXML: " + outXML);
                    //Response.Write("<br>" + obj.Transfer(outXML));
                    if (outXML.IndexOf("<error>") < 0)
                    {    //if (SetEndpointDetails())
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        try
                        {
                            lblBridgeAddress.Text = xmldoc.SelectSingleNode("//ConfirmConferenceInvitation/Bridge/Address").InnerText;
                            lblBridgeName.Text = xmldoc.SelectSingleNode("//ConfirmConferenceInvitation/Bridge/Name").InnerText;
                            lblBridgeAddressType.Text = lstAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//ConfirmConferenceInvitation/Bridge/AddressType").InnerText).Text;
                            if (lstConnectionType.SelectedValue.Equals("1") && rdRooms.SelectedValue.Equals("-1")) //FB 2347RDoubt
                                trAcceptConfirm.Visible = true;
                            else
                                trAcceptConfirm.Visible = false;
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        trAcceptConfirm.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ConfirmAccept" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected bool SetEndpointDetails()
        {
            try
            {
                String inXML = GenerateInxml();
                String outXML = obj.CallMyVRMServer("SetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                    return true;
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }
        protected String GenerateInxml()
        {
            try
            {
                /*                String inXML = "<SetEndpoint>";
                                inXML += "      <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                                inXML += "      <EndpointName>" + txtUserFirstName.Text + " " + txtUserLastName.Text + "</EndpointName>";
                                inXML += "      <EntityType>U</EntityType>";
                                inXML += "      <Profiles>";
                                inXML += "        <Profile>";
                                inXML += "          <ProfileID>0</ProfileID>";
                                inXML += "          <ProfileName>" + txtEndpointName.Text + "</ProfileName>";
                                inXML += "          <Default>1</Default>";
                                inXML += "          <EncryptionPreferred>" + lstEncryption.SelectedValue + "</EncryptionPreferred>";
                                inXML += "          <AddressType>" + lstAddressType.SelectedValue + "</AddressType>";
                                inXML += "          <Password>" + txtEPPassword1.Text + "</Password>";
                                inXML += "          <Address>" + txtAddress.Text + "</Address>";
                                inXML += "          <URL>" + txtWebAccessURL.Text + "</URL>";
                                inXML += "          <IsOutside>" + lstIsOutsideNetwork.SelectedValue + "</IsOutside>";
                                inXML += "          <ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                                inXML += "          <VideoEquipment>" + lstVideoEquipment.SelectedValue + "</VideoEquipment>";
                                inXML += "          <LineRate>" + lstLineRate.SelectedValue + "</LineRate>";
                                inXML += "          <Bridge>" + lstBridges.SelectedValue + "</Bridge>";
                                inXML += "          <MCUAddress>" + txtAssociateMCUAddress.Text + "</MCUAddress>";
                                inXML += "          <MCUAddressType>" + lstMCUAddressType.SelectedValue + "</MCUAddressType>";
                                inXML += "        </Profile>";
                                inXML += "      </Profiles>";
                                inXML += "    </SetEndpoint>";
                                return inXML;*/
                return "";
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GenerateInxml" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
                return "<error></error>";
            }
        }
        protected void ValidateAcceptRegistration(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                Regex check = new Regex(pattern);
                e.IsValid = true;
                customAccept.Text = "";
                // FB 1738
                String ipisdnadd = txtAddress.Text.ToString();
                ipisdnadd = SpiltAddress(ipisdnadd);
                //Response.Write(trAccept.Visible);
                //customAccept.Text = "Please provide a valid IP/ISDN value";
                //CustomValidator1.Text = "Please provide a valid IP/ISDN value";
                //Response.Write(trAccept.Visible);
                if (trAccept.Visible.Equals(true) && rdRooms.SelectedValue.Equals("-1"))
                {
                    //FB 3012 Starts
                    switch(lstAddressType.SelectedValue)
                    {
                        case "-1":
                                e.IsValid = false;
                                CustomValidator1.Text = obj.GetTranslatedText("Please select an Address Type.");//FB 1830 - Translation
                                customAccept.Text = obj.GetTranslatedText("Please select an Address Type.");//FB 1830 - Translation
                                break;

                        case ns_MyVRMNet.vrmAddressType.IP:
                                if (lstInterfaceType.SelectedValue.Equals("IP"))
                                {
                                    if (ipisdnadd.Equals(""))  // FB 1738
                                     {
                                       e.IsValid = false;
                                     }
                                    else
                                    {
                                        e.IsValid = check.IsMatch(ipisdnadd); // FB 1738
                                        if (!e.IsValid)
                                        {
                                            CustomValidator1.Text = obj.GetTranslatedText("Please select a valid IP Address.");//FB 1830 - Translation
                                            customAccept.Text = obj.GetTranslatedText("Please select a valid IP Address.");//FB 1830 - Translation
                                        }
                                    }
                                 }
                                else
                                {
                                    e.IsValid = false;
                                    CustomValidator1.Text = obj.GetTranslatedText("Invalid Interface Type and Address Type.");
                                    customAccept.Text = obj.GetTranslatedText("Invalid Interface Type and Address Type.");
                                }
                                break;

                        case ns_MyVRMNet.vrmAddressType.H323:
                        case ns_MyVRMNet.vrmAddressType.E164:
                        case ns_MyVRMNet.vrmAddressType.MPI:
                        case ns_MyVRMNet.vrmAddressType.SIP:
                                if (ipisdnadd.Equals(""))  // FB 1738
                                    e.IsValid = false;
                                
                                else
                                    e.IsValid = true;
                               
                                break;

                        case ns_MyVRMNet.vrmAddressType.ISDN:
                            // FB 1738 Starts
                            if (lstInterfaceType.SelectedValue.Equals("ISDN"))
                            {
                                String pattern1 = @"^[0-9]+$";
                                Regex check1 = new Regex(pattern1);
                                if (txtAddress.Text.Trim().Equals(""))
                                {
                                    e.IsValid = false;
                                }
                                else
                                {
                                    e.IsValid = check1.IsMatch(txtAddress.Text.Trim());
                                    if (!e.IsValid)
                                    {
                                        CustomValidator1.Text = obj.GetTranslatedText("Please select a valid ISDN Address.");
                                        customAccept.Text = obj.GetTranslatedText("Please select a valid ISDN Address.");
                                    }
                                }
                            }
                            else
                            {
                                e.IsValid = false;
                                CustomValidator1.Text = obj.GetTranslatedText("Invalid Interface Type and Address Type.");
                                customAccept.Text = obj.GetTranslatedText("Invalid Interface Type and Address Type.");
                            }
                            break;
                    }
                    /*else  
                    {
                        e.IsValid = false;
                        CustomValidator1.Text = obj.GetTranslatedText("Invalid Interface Type and Address Type.");//FB 1830 - Translation
                        customAccept.Text = obj.GetTranslatedText("Invalid Interface Type and Address Type.");//FB 1830 - Translation
                    }*/
                    // FB 1738 Ends
                    //FB 3012 Ends

                    if (rdRooms.SelectedValue.ToString().Trim().Equals("") && !e.IsValid)
                    {
                        //Response.Write(rdRooms.SelectedValue);
                        //customAccept.ErrorMessage = "Please select a Room";
                        //customAccept.Text = "Please select a Room.";
                        customAccept.ErrorMessage = obj.GetErrorMessage(433);//FB 1881
                        customAccept.Text = obj.GetErrorMessage(433);
                        e.IsValid = false;
                    }
                }

                if (trChange.Visible.Equals(true))
                {
                    if (lstTimeZone.SelectedIndex.Equals("1") || txtStartDate.Text.Equals("") || txtStartTime.Text.Equals("") || lstDuration.Text.Equals(""))
                    {
                        e.IsValid = false;
                        CustomValidator1.Text = obj.GetTranslatedText("Please fill in all the values for Change Request");//FB 1830 - Translation
                    }
                }
                //Response.Write(Page.IsValid);
            }
            catch (Exception ex)
            {                
                //ZD 100263
                log.Trace("ValidateAcceptRegistration" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                e.IsValid = false;
            }
        }

        protected void ChangeSelection(Object sender, EventArgs e)
        {
            try
            {
                RadioButtonList rdTemp = (RadioButtonList)sender;
                if (rdTemp.SelectedValue.Equals("1"))
                {
                    trChange.Visible = false;
                    trAccept.Visible = true;
                }
                if (rdTemp.SelectedValue.Equals("0")) //FB 1193
                {
                    trAccept.Visible = false;
                    //Code Added for FB 1694 Starts
                    //trChange.Visible = true;
                    trChange.Visible = false;
                    //Code Added for FB 1694 End
                }
                if (rdTemp.SelectedValue.Equals("2"))//FB 1193
                {
                    trAccept.Visible = false;
                    trChange.Visible = false;
                }
                //Code Added for FB 1694 Starts
                if (rdTemp.SelectedValue.Equals("3"))
                {
                    trAccept.Visible = false;
                    //trChange.Visible = true; //FB 2020
                }
                //Code Added for FB 1694 Starts
                foreach (DataGridItem dgi in dgInstanceList.Items)
                {
                    RadioButtonList rdInstance = (RadioButtonList)dgi.FindControl("rdAction");
                    rdInstance.ClearSelection();
                    rdInstance.Items.FindByValue(rdTemp.SelectedValue).Selected = true;

                    //FB 2020
                    HtmlTableRow tr = (HtmlTableRow)dgi.FindControl("trInstanceChange");
                    if (tr != null)
                    {
                        if (rdTemp.SelectedValue == "3")
                            tr.Visible = true;
                        else
                            tr.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ChangeSelection" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected void ChangeAction(Object sender, EventArgs e)
        {
            try
            {
                bool ShowAccept = false;
                bool ShowChange = false;
                //Code Added For FB 1693 Starts

                //foreach (DataGridItem dgi in dgInstanceList.Items)
                //{
                //    if (((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue.Equals("1"))
                //        ShowAccept = true;
                //    if (((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue.Equals("3"))//FB 1167 //Disney Recurrance Email Issues
                //        ShowChange = true;
                //}
                //trAccept.Visible = ShowAccept;
                //trChange.Visible = ShowChange;

                int totalRecord = 0;
                if (dgInstanceList.Items.Count > 0)
                    totalRecord = dgInstanceList.Items.Count;
                int noOfUnDecidedConference = 0, noOfAcceptedConference = 0, noOfRejectedConference = 0, noOfChangedConference = 0;

                foreach (DataGridItem dgi in dgInstanceList.Items)
                {
                    if (((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue.Equals("0"))
                    {
                        noOfUnDecidedConference++;
                    }

                    if (((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue.Equals("1"))
                    {
                        ShowAccept = true;
                        noOfAcceptedConference++;
                    }

                    if (((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue.Equals("2"))
                    {
                        noOfRejectedConference++;
                    }

                    if (((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue.Equals("3"))
                    {
                        ShowChange = true;
                        noOfChangedConference++;
                    }
                    //FB 2020
                    HtmlTableRow tr = (HtmlTableRow)dgi.FindControl("trInstanceChange");
                    if (tr != null)
                    {
                        if (ShowChange == true)
                            tr.Visible = true;
                        else
                            tr.Visible = false;

                    }
                    ShowChange = false;
                }
                trAccept.Visible = ShowAccept;
                //trChange.Visible = ShowChange;

                System.Web.UI.Control header = dgInstanceList.Controls[0].Controls[0];
                RadioButtonList rdDecideAllLst = ((RadioButtonList)header.FindControl("rdDecideAll"));
                rdDecideAllLst.ClearSelection();

                if (totalRecord != 0 && totalRecord == noOfUnDecidedConference)
                {
                    rdDecideAllLst.Items.FindByValue("0").Selected = true;
                    trAccept.Visible = false;
                    trChange.Visible = false;

                }
                if (totalRecord != 0 && totalRecord == noOfAcceptedConference)
                {
                    rdDecideAllLst.Items.FindByValue("1").Selected = true;
                    trChange.Visible = false;
                    trAccept.Visible = true;
                }
                if (totalRecord != 0 && totalRecord == noOfRejectedConference)
                {
                    rdDecideAllLst.Items.FindByValue("2").Selected = true;
                    trAccept.Visible = false;
                    trChange.Visible = false;
                }
                if (totalRecord != 0 && totalRecord == noOfChangedConference)
                {
                    rdDecideAllLst.Items.FindByValue("3").Selected = true;
                    trAccept.Visible = false;
                    if (ShowChange)   //ZD 100222
                        trChange.Visible = true;
                }
                //Code Added For FB 1693 End
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ChangeAction" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }

        protected void SubmitInvitation(Object sender, EventArgs e)
        {
            try
            {
                //Response.Write("sss");
                Page.Validate();
                //Response.Write(Page.IsValid);
                if (Page.IsValid)
                {
                    SecurityBadgeImage(); //FB 2136

                    customAccept.Text = "";
                    //Response.End();
                    String inXML = "";
                    inXML += "<ConfirmConferenceInvitation>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <UserID>" + txtUserID.Value + "</UserID>";
                    inXML += "  <Conferences>";
                    Int32 changeCnt = 0; //FB 2020
                    foreach (DataGridItem dgi in dgInstanceList.Items)
                    {
                        inXML += "      <Conference>";
                        inXML += "          <ConfID>" + dgi.Cells[0].Text + "</ConfID>";
                        inXML += "          <Decision>" + ((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue + "</Decision>";
                        if (((RadioButtonList)dgi.FindControl("rdAction")).SelectedValue == "3") //FB 2020
                            changeCnt = changeCnt + 1;
                        inXML += "          <Reason></Reason>";
                        inXML += "          <LocationID>" + rdRooms.SelectedValue + "</LocationID>";
                        inXML += "          <VideoProtocol>" + lstInterfaceType.SelectedValue + "</VideoProtocol>";
                        inXML += "          <IPISDNAddress>" + txtAddress.Text + "</IPISDNAddress>";
                        inXML += "          <ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                        if (chkOutsideNetwork.Checked)
                            inXML += "          <IsOutsideNetwork>1</IsOutsideNetwork>";
                        else
                            inXML += "          <IsOutsideNetwork>0</IsOutsideNetwork>";
                        inXML += "          <AddressType>" + lstAddressType.SelectedValue + "</AddressType>";
                        //FB 2020
                        inXML += "          <StartDate>" + ((TextBox)dgi.FindControl("txtStartDate")).Text + "</StartDate>";
                        inXML += "          <StartTime>" + ((MetaBuilders.WebControls.ComboBox)dgi.FindControl("txtStartTime")).Text + "</StartTime>";
                        inXML += "          <Duration>" + ((MetaBuilders.WebControls.ComboBox)dgi.FindControl("lstDuration")).Text + "</Duration>";
                        inXML += "          <Timezone>" + ((DropDownList)dgi.FindControl("lstTimeZone")).SelectedValue + "</Timezone>";
                        inXML += "          <Comment>" + ((TextBox)dgi.FindControl("txtComments")).Text + "</Comment>";
                        
                        inXML += "      </Conference>";
                    }
                    inXML += "  </Conferences>";
                    if(changeCnt > 1)//FB 2020
                        inXML += "<MultiChangeRequest>1</MultiChangeRequest>";
                    else
                        inXML += "<MultiChangeRequest>0</MultiChangeRequest>";

                    inXML += "</ConfirmConferenceInvitation>";
                    //Response.Write(obj.Transfer(inXML));
                    log.Trace("ConfirmConferenceInvitation Inxml: " + inXML);
                    String outXML = obj.CallMyVRMServer("ConfirmConferenceInvitation", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("ConfirmConferenceInvitation outXML: " + outXML);
                    //Response.Write("<br>" + obj.Transfer(outXML));
                    //Code Added for FB 1693 Starts
                    int totalRecord = 0;
                    if (dgInstanceList.Items.Count > 0)
                        totalRecord = dgInstanceList.Items.Count;
                    int noOfRejectedConference = 0;
                    foreach (DataGridItem dgi in dgInstanceList.Items)
                    {

                        RadioButtonList rdbRejected = (RadioButtonList)dgi.FindControl("rdAction");
                        //if (rdbUndecided.Items[3].Selected== true) 
                        if (rdbRejected.SelectedValue.Equals("2"))
                        {
                            noOfRejectedConference++;
                        }

                    }
                    //Code Added for FB 1693 End
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        //Code Added for FB 1693 Starts
                        errLabel.Visible = true;
                        if (totalRecord != 0 && totalRecord == noOfRejectedConference)
                        {
                            errLabel.Text = obj.GetTranslatedText("We are sorry that you will not be able to join the following conference");//FB 1830 - Translation

                        }
                        else
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        }
                        //Code Added for FB 1693 End
                    }
                    else
                    {
                        errLabel.Visible = true;//Code added for FB 1693
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SubmitInvitation" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected void ExportToPDF(object sender, EventArgs e)
        {
            try
            {
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.LeftMargin = 5;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 5;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;

                pdfConverter.PdfDocumentOptions.ShowHeader = false;

                pdfConverter.PdfFooterOptions.FooterText = "myVRM Version" + Application["Version"].ToString() + ",(c)Copyright " + Application["CopyrightsDur"].ToString() + " myVRM.com. All Rights Reserved."; //FB 1648
                int upYear = obj.GetYear(Session["systemDate"].ToString());
                int upMonth = obj.GetMonth(Session["systemDate"].ToString());
                int upDay = obj.GetDay(Session["systemDate"].ToString());
                int upHour = obj.GetHour(Session["systemTime"].ToString());
                int upMinute = obj.GetMinute(Session["systemTime"].ToString());
                String upSet = obj.GetTimeSet(Session["systemTime"].ToString());
                DateTime UserTime = new DateTime(upYear, upMonth, upDay, upHour, upMinute, 0);
                pdfConverter.PdfFooterOptions.FooterText += "\n" + UserTime.ToString("MMMM dd, yyyy, hh:mm tt ") + Session["systemTimeZone"].ToString();
                pdfConverter.PdfFooterOptions.FooterTextColor = System.Drawing.Color.Blue;
                pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                pdfConverter.LicenseKey = "kGb8Fr0gg2spzZd/SluMYY+Bv/RxuZmz6thATnaDnlkD20HkaCEyR4X+P9QqabXI";
                string html = tempText.Value;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(html);

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                String downloadName = "MyVRM_Meeting_Response.pdf";
                response.AddHeader("Content-Disposition", "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ExportToPDF" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }

        
        #region SpiltAddress

        //Method added for FB 1738

        private string SpiltAddress(String address)
        {

            String[] add = null;
            try
            {
                add = new String[3];
                if (address.Contains("D") && address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    if (address.IndexOf('+') > 0)
                    {
                        add[1] = address.Split('D')[1].Split('+')[0].Trim(); //Conference Code
                        add[2] = address.Split('D')[1].Split('+')[1].Trim(); // Leader Pin
                    }
                    else
                    {
                        add[1] = address.Split('D')[1].Trim(); //Conference Code
                        add[2] = ""; // Leader Pin
                    }
                }
                else if (address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address.Split('D')[0].Trim();//Address
                    add[1] = address.Split('D')[1].Trim(); //Conference Code
                    add[2] = ""; // Leader Pin
                }
                else if (!address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address;//Address
                    add[1] = ""; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                return add[0].Trim();

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                throw ex;
            }
        }

        #endregion


        // FB 2136 start
        #region uploadImage_Click
        protected void uploadImage_Click(object sender, EventArgs e)
        {
            //ZD 100263
            string filextn = "";
            List<string> strExtension = null;
            bool filecontains = false;

            if (uploadImage.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(uploadImage.FileName);

                    //ZD 100263 Starts
                    filextn = Path.GetExtension(filename);
                    strExtension = new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif", ".tif", ".tiff" };
                    filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                    if (!filecontains)
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End

                    //FileStream fileStream = new FileStream(uploadImage.ResolveUrl, FileMode.Open, FileAccess.Read);
                    //uploadImage.FileContent.Write(bs, 0, uploadImage.FileContent.Length);
                    //uploadImage.FileContent.
                    System.Drawing.Image img = System.Drawing.Image.FromStream(uploadImage.PostedFile.InputStream);

                    if (img.Width > 1366 || img.Height > 768)
                    {
                        errLabel.Text = "Image dimension too large";
                        errLabel.Visible = true;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "sizeAlert", "<script>alert('Image dimension too large');</script>", false);
                        img.Dispose();
                        return;
                    }
                    else if (img.Width < 120 || img.Height < 160)
                    {
                        errLabel.Text = "Image dimension too small";
                        errLabel.Visible = true;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "sizeAlert", "<script>alert('Image dimension too small');</script>", false);
                        img.Dispose();
                        return;
                    }
                    System.Drawing.Bitmap image2 = new System.Drawing.Bitmap(img);
                    filename = filename.Substring(0, filename.Length - 4);
                    Random ran = new Random();
                    int n = Math.Abs(ran.Next());
                    string random = (n * 100).ToString();

                    string alph = "abcdefghijklmnopqrstuvwxyz";
                    n = ran.Next(26);
                    alph = alph.Substring(n, 1);

                    filename = "photo" + alph + DateTime.Today.Second.ToString() + DateTime.Today.Millisecond.ToString() + random + filename + ".jpg";
                    //var ms = new MemoryStream();
                    //image2.Save(ms, ImageFormat.Jpeg);

                    ////image2.Save(Server.MapPath("~/image/SecurityBadge/") + "EX" + filename, ImageFormat.Jpeg);
                    // This block keep aspect ratio of the image, that is not stretched
                    double aspectRatio;
                    int aspectWidth, aspectHight;
                    // replace 640x480 with 400x300 to min size with aspect ratio

                    // 273 x 180 => 137 x 90
                    if ((img.Width < 137 || img.Height < 90) || (img.Width > 1366 || img.Height > 768))
                    {
                        //errLabel.Text = "Image dimension too large";
                        //errLabel.Visible = true;
                        img.Dispose();
                        errLabel.Text = "Image dimension not valid";
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "sizeAlert", "<script>alert('Image dimension not valid');</script>", false);
                        return;
                    }


                    if (img.Width > 252 || img.Height > 144)
                    {

                        if (((double)img.Width / img.Height) > (273.0 / 180.0))
                        {
                            aspectRatio = (double)img.Width / 273.0;
                        }
                        else
                        {
                            aspectRatio = (double)img.Height / 180.0;
                        }

                        aspectWidth = (int)Math.Round(img.Width / aspectRatio);
                        aspectHight = (int)Math.Round(img.Height / aspectRatio);

                    }
                    else if (img.Width < 273 || img.Height < 180)
                    {
                        if (((double)img.Width / img.Height) > (273.0 / 180.0))
                        {
                            aspectRatio = 273.0 / img.Width;
                        }
                        else
                        {
                            aspectRatio = 180.0 / img.Height;
                        }

                        aspectWidth = (int)Math.Round(img.Width * aspectRatio);
                        aspectHight = (int)Math.Round(img.Height * aspectRatio);
                    }
                    else
                    {
                        aspectWidth = img.Width;
                        aspectHight = img.Height;

                    }

                    //aspectWidth = img.Width;
                    //aspectHight = img.Height;


                    Bitmap bmp2 = new Bitmap(aspectWidth, aspectHight, PixelFormat.Format24bppRgb);
                    bmp2.SetResolution(72, 72);
                    Graphics gfx = Graphics.FromImage(bmp2);
                    gfx.SmoothingMode = SmoothingMode.AntiAlias;
                    gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    gfx.DrawImage(img, new Rectangle(0, 0, aspectWidth, aspectHight), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);

                    //bmp2.Save(newPath + "i" + filename, ImageFormat.Jpeg);
                    bmp2.Save(Server.MapPath("~/image/SecurityBadge/") + filename, ImageFormat.Jpeg);
                    img.Dispose();
                    image2.Dispose();
                    gfx.Dispose();

                    if (selectedFile.Value != string.Empty)
                    {
                        System.IO.File.Delete(Server.MapPath("../image/SecurityBadge/" + selectedFile.Value));
                    }
                    selectedFile.Value = filename;
                    imgCont.Visible = true;
                    imgCont.ImageUrl = "../image/SecurityBadge/" + filename;
                                        
                    if (Text1 != null)
                    {
                        string X = Text1.Value;
                        string Y = Text2.Value;
                    }
                    imgPrev.Visible = true;
                    imgPrev.ImageUrl = "../image/SecurityBadge/" + filename;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "assignImage", "<script>showDiv('" + aspectWidth + "','" + aspectHight + "');</script>", false);
                    bmp2.Dispose();

                }
                catch (Exception ex)
                {
                    errLabel.Visible = true;
                    log.Trace(ex.StackTrace + " : " + ex.Message);
                }
            }
        }
        #endregion

        #region saveImage_Click
        protected void saveImage_Click(object sender, EventArgs e)
        {
            try
            {
                string X = Text1.Value;
                string Y = Text2.Value;
                
                //string [] split = words.Split(new Char [] {' ', ',', '.', ':', '\t' });
                string[] cordinates = X.Split(new Char[] { ',' });
                string[] dimension = Y.Split(new Char[] { ',' });

                //string img;
                int expWidth = 90, expHeight=120, x, y;
                x = Int32.Parse(cordinates[0]);
                y = Int32.Parse(cordinates[1]);
                int inpWidth, inpHeight;
                inpWidth = Int32.Parse(dimension[0]);
                inpHeight = Int32.Parse(dimension[1]);
                                
                //System.Drawing.Image image = System.Drawing.Image.FromFile(img);
                // SELECTED PHOTO OF PARTICIPANT
                System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath("../image/SecurityBadge/" + selectedFile.Value));
                Bitmap bmp = new Bitmap(expWidth, expHeight, PixelFormat.Format24bppRgb);
                bmp.SetResolution(72, 72);
                Graphics gfx = Graphics.FromImage(bmp);
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gfx.DrawImage(img, new Rectangle(0, 0, expWidth, expHeight), x, y, inpWidth, inpHeight, GraphicsUnit.Pixel);
                // Dispose to free up resources
                
                // paste process starts here
                if (rdRooms.SelectedValue.Trim() == "-1" || rdRooms.SelectedValue.Trim() == "-2")//FB 2347R
                    return;

                if (File.Exists(PartyphotoPath))
                    File.Delete(PartyphotoPath);

                bmp.Save(PartyphotoPath, ImageFormat.Jpeg);

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Jpeg);
                badgeIcon1.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                badgeIcon1.Visible = true;
                btnRemoveImg1.Visible = true;
                uploadImage.Visible = false;

                img.Dispose();
                bmp.Dispose();
                gfx.Dispose();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        //create final sec badge image while submit the page
        private void SecurityBadgeImage()
        {
            try
            {
                string SecImgName = Server.MapPath("../image/Maillogo/SecImg_" + lblConfUniqueID.Text + "_" + txtUserID.Value + ".jpg");
                //SecTextAxis SecPhotoAxis SecBarCodeAxis
                string[] objaxis = hdnObjAxis.Value.Trim().Split('�');
                string[] arrObjAxis = objaxis[0].Split(',');
                SecTextAxis = arrObjAxis[0];
                SecPhotoAxis = arrObjAxis[1];

                string[] arrTextAxis = SecTextAxis.Split('|');
                string[] arrPhotoAxis = SecPhotoAxis.Split('|');
                int xOfPhoto, yOfPhoto, xOfInfo, yOfInfo;
                xOfPhoto = Int32.Parse(arrPhotoAxis[0]);
                yOfPhoto = Int32.Parse(arrPhotoAxis[1]);
                xOfInfo = Int32.Parse(arrTextAxis[0]);
                yOfInfo = Int32.Parse(arrTextAxis[1]);
                //String secImg = "";

                //string X = Text1.Value;
                //string Y = Text2.Value;
                int expWidth = 90, expHeight = 120, x = 0, y = 0;
                int inpWidth = 90, inpHeight = 120;

                //if (X != "")
                //{
                //    string[] cordinates = X.Split(new Char[] { ',' });
                //    string[] dimension = Y.Split(new Char[] { ',' });

                //    x = Int32.Parse(cordinates[0]);
                //    y = Int32.Parse(cordinates[1]);
                //    inpWidth = Int32.Parse(dimension[0]);
                //    inpHeight = Int32.Parse(dimension[1]);
                //}

                //System.Drawing.Image image = System.Drawing.Image.FromFile(img);
                // SELECTED PHOTO OF PARTICIPANT

                if (File.Exists(SecImgName))
                    File.Delete(SecImgName);

                if (rdRooms.SelectedValue.Trim() == "-1" || rdRooms.SelectedValue.Trim() == "-2")//FB 2347R
                    return;

                if (!(File.Exists(PartyphotoPath)))
                {
                    if(File.Exists(Server.MapPath("../image/BlankPerson.jpg")))
                     PartyphotoPath = Server.MapPath("../image/BlankPerson.jpg");
                    else
                            return;
                }

                System.Drawing.Image img = System.Drawing.Image.FromFile(PartyphotoPath);
                if (img == null)
                    return;

                Bitmap bmp = new Bitmap(expWidth, expHeight, PixelFormat.Format24bppRgb);
                bmp.SetResolution(72, 72);
                Graphics gfx = Graphics.FromImage(bmp);
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gfx.DrawImage(img, new Rectangle(0, 0, expWidth, expHeight), x, y, inpWidth, inpHeight, GraphicsUnit.Pixel);
                // Dispose to free up resources
                img.Dispose();

                // paste process starts here
                //secImg = lblConfUniqueID.Text + "_" + rdRooms.SelectedValue.ToString() + "_" + txtUserID.Value + "_" + SecImg1Name;

                // Security Badge URL given by scheduler
                

                //Graphics gfx = Graphics.FromImage(bmp);
               
                

                string confInfo = null;

                string[] confNameArray = lblConfName.Text.Split(' ');
                string confName = "";

                int l = 10, len = 0;
                for (int n = 0; n < confNameArray.Length; n++)
                {
                    confName += confNameArray[n];

                    len = confName.Length;

                    len = len - (l - 10);

                    if (len > l)
                    {
                        confName = confName + "\n";
                        l += 10;
                    }
                    else
                        confName = confName + " ";
                }
                confName = confName.Substring(0, confName.Length - 1);

                string[] confDescArray = lblDescription.Text.Split(' ');
                string confDesc = "";

                int d = 10, dlen = 0;
                for (int n = 0; n < confDescArray.Length; n++)
                {
                    confDesc += confDescArray[n];

                    dlen = confDesc.Length;

                    dlen = dlen - (d - 10);

                    if (dlen > d)
                    {
                        confDesc = confDesc + "\n";
                        d += 10;
                    }
                    else
                        confDesc = confDesc + " ";
                }
                confDesc = confDesc.Substring(0, confDesc.Length - 1);



                if (!lblConfName.Text.Equals(""))
                    confInfo += "Title : " + confName + "\n";

                if (!lblConfUniqueID.Text.Trim().Equals(""))
                    confInfo += "Unique ID : " + lblConfUniqueID.Text + "\n";

                if (!lblConfDuration.Text.Trim().Equals(""))
                    confInfo += "Duration : " + lblConfDuration.Text + "\n";

                if (!lblConfDuration.Text.Trim().Equals(""))
                    confInfo += "Host : " + lblhost.Text + "\n";

                if (!lblConfType.Text.Trim().Equals(""))
                    confInfo += "Type : " + lblConfType.Text + "\n";

                if (!lblDescription.Text.Trim().Equals(""))
                    confInfo += "Description : " + confDesc + "\n";

                if (dgInstanceList.Items.Count > 0)
                {
                    for (int i = 0; i < dgInstanceList.Items.Count; i++)
                    {
                        System.Drawing.Image badge = System.Drawing.Image.FromFile(Server.MapPath("../image/SecurityBadge/" + rdRooms.SelectedValue + "_SecBadge.jpg")); // + hdnImgName.Value.ToString())); //FB 2136
                        System.Drawing.Bitmap bmp2 = new System.Drawing.Bitmap(badge);

                        bmp2.SetResolution(72, 72);

                        Graphics gfx2 = Graphics.FromImage(bmp2); // NEW LINE 1
                        gfx2.SmoothingMode = SmoothingMode.AntiAlias;
                        gfx2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        gfx2.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        gfx2.DrawImage(bmp, xOfPhoto, yOfPhoto); // NEW LINE 2

                        Brush mybrush = new SolidBrush(Color.FromArgb(128, 255, 255, 255));

                        gfx2.FillRectangle(mybrush, new Rectangle(xOfInfo, yOfInfo, 173, 120));

                        if (((RadioButtonList)dgInstanceList.Items[i].FindControl("rdAction")).SelectedValue == "1")
                        {
                            SecImgName = Server.MapPath("../image/Maillogo/SecImg_" + ((Label)dgInstanceList.Items[i].FindControl("lblUniqueID")).Text
                                                                               + "_" + txtUserID.Value + ".jpg");

                            if (((Label)dgInstanceList.Items[i].FindControl("lblConfName")).Text.Trim() != confName)
                            {
                                confName = "";
                                l = 10; len = 0;
                                confNameArray = ((Label)dgInstanceList.Items[i].FindControl("lblConfName")).Text.Split(' ');

                                for (int n = 0; n < confNameArray.Length; n++)
                                {
                                    confName += confNameArray[n];
                                    len = confName.Length;
                                    len = len - (l - 10);

                                    if (len > l)
                                    {
                                        confName = confName + "\n";
                                        l += 10;
                                    }
                                    else
                                        confName = confName + " ";
                                }
                                confName = confName.Substring(0, confName.Length - 1);
                            }

                            confInfo = "Title : " + confName + "\n";
                            confInfo += "Unique ID : " + ((Label)dgInstanceList.Items[i].FindControl("lblUniqueID")).Text + "\n";

                            if (!lblConfDuration.Text.Trim().Equals(""))
                                confInfo += "Duration : " + lblConfDuration.Text + "\n";

                            if (!lblConfDuration.Text.Trim().Equals(""))
                                confInfo += "Host : " + lblhost.Text + "\n";

                            if (!lblConfType.Text.Trim().Equals(""))
                                confInfo += "Type : " + lblConfType.Text + "\n";

                            if (!lblDescription.Text.Trim().Equals(""))
                                confInfo += "Description : " + confDesc + "\n";

                            if (File.Exists(SecImgName))
                                File.Delete(SecImgName);

                            gfx2.DrawString(confInfo, new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(xOfInfo, yOfInfo));
                            bmp2.Save(SecImgName, ImageFormat.Jpeg);

                            badge.Dispose();
                            gfx2.Dispose();
                            bmp2.Dispose();
                        }
                    }
                }
                else
                {
                    System.Drawing.Image badge = System.Drawing.Image.FromFile(Server.MapPath("../image/SecurityBadge/" + rdRooms.SelectedValue + "_SecBadge.jpg")); // + hdnImgName.Value.ToString())); //FB 2136
                    System.Drawing.Bitmap bmp2 = new System.Drawing.Bitmap(badge);

                    bmp2.SetResolution(72, 72);

                    Graphics gfx2 = Graphics.FromImage(bmp2); // NEW LINE 1
                    gfx2.SmoothingMode = SmoothingMode.AntiAlias;
                    gfx2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    gfx2.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    gfx2.DrawImage(bmp, xOfPhoto, yOfPhoto); // NEW LINE 2

                    Brush mybrush = new SolidBrush(Color.FromArgb(128, 255, 255, 255));
                    gfx2.FillRectangle(mybrush, new Rectangle(xOfInfo, yOfInfo, 173, 120));

                    // conference info 
                    gfx2.DrawString(confInfo, new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(xOfInfo, yOfInfo));
                    //bmp2.Save(Server.MapPath("../image/SecurityBadge/Success2.jpg"), ImageFormat.Jpeg);
                    bmp2.Save(SecImgName, ImageFormat.Jpeg);
                    badge.Dispose();
                    gfx2.Dispose();
                    bmp2.Dispose();
                }

                // paste process ends here
                bmp.Dispose();
                gfx.Dispose();
            }
            catch (Exception ex)
            {
                log.Trace("d" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }

        #region remove file
        protected void RemoveFile(Object sender, CommandEventArgs e)
        {
            badgeIcon1.Visible = false;
            btnRemoveImg1.Visible = false;
            uploadImage.Visible = true;

            if (File.Exists(PartyphotoPath))
                File.Delete(PartyphotoPath);
        }
        #endregion

        // FB 2136 end


    }
}