/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

namespace ns_MyVRM
{
    public partial class en_RoomSearch : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        myVRMNet.ImageUtil imageUtilObj = null;
        ns_Logger.Logger log;
        DataRow[] filtrs = null;
        DataTable dtSel = null;
        DataTable dtDisp = null;
        DataSet DSdisp = null;
        ArrayList rmlist = null;

        DataSet ds = null;
        public String confID = "";
        public String duration = "";
        public String Parentframe = "";
        String outXML = "";
        String outXMLDept = "";
        String tzone = "";
        string immediate = "0"; //FB 2534
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        XmlTextReader txtrd = null;
        String roomEdit = "N";
        protected String favRooms = "";
        protected String GuestRooms = "";//FB 2426
        String pathName = "";
        string filePath = ""; //Image Project
        string fileImgPath = ""; //Image Project
        String roomxmlPath = "";//Room search
        byte[] imgArray = null;
        protected String language = "";//FB 1830
        protected String EnableRoomServiceType = "";//FB 2219
        protected String VMR = ""; //FB 2448
        protected string roomVMR = "0"; //FB 2448
        protected int Cloud = 0; //FB 2262 //FB 2599
        string Tier1ID = ""; //FB 2637
        string confOrgID = ""; //FB 2646
        protected string HotdeskingRooms = ""; //FB 2694
        protected string ConfType = ""; //FB 2694
        const string PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09";
        protected int CloudConferencing = 0; //FB 2717

        # region prviate DataMember

        protected DevExpress.Web.ASPxGridView.ASPxGridView grid;
        protected DevExpress.Web.ASPxGridView.ASPxGridView grid2;

        protected System.Web.UI.WebControls.DataGrid SelectedGrid;

        protected System.Web.UI.WebControls.Label LblError;
        protected System.Web.UI.WebControls.Label lblViewType;
        protected System.Web.UI.WebControls.Label vidLbl;
        protected System.Web.UI.WebControls.Label nvidLbl;
        protected System.Web.UI.WebControls.Label vmrvidLbl;
        protected System.Web.UI.WebControls.Label ROHotdeskingRooms;
        protected System.Web.UI.WebControls.Label VCHotdeskingRooms;
        protected System.Web.UI.WebControls.Label ttlVCHotdeskingRooms;
        protected System.Web.UI.WebControls.Label ttlROHotdeskingRooms;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Label ttlnvidLbl;
        protected System.Web.UI.WebControls.Label lblVMRRooms;
        protected System.Web.UI.WebControls.Label tntvmrrooms;
        protected System.Web.UI.WebControls.Label lblPublicRoom;
        protected System.Web.UI.WebControls.Label ttlPublicLbl;
        protected System.Web.UI.WebControls.Label totalNumber;


        protected System.Web.UI.WebControls.CheckBox chkHotdesking;
        protected System.Web.UI.WebControls.CheckBox Available;
        protected System.Web.UI.WebControls.CheckBox chkIsVMR;
        protected System.Web.UI.WebControls.CheckBox chkGuestRooms;
        protected System.Web.UI.WebControls.CheckBox chkFavourites;
        protected System.Web.UI.WebControls.CheckBox MediaNone;
        protected System.Web.UI.WebControls.CheckBox MediaAudio;
        protected System.Web.UI.WebControls.CheckBox MediaVideo;
        protected System.Web.UI.WebControls.CheckBox PhotosOnly;
        protected System.Web.UI.WebControls.CheckBox HandiCap;

        protected System.Web.UI.WebControls.CheckBoxList AVlist;

        protected System.Web.UI.WebControls.DropDownList DrpDwnListView;
        protected System.Web.UI.WebControls.DropDownList DrpActDct;
        protected System.Web.UI.WebControls.DropDownList lstCountry;
        protected System.Web.UI.WebControls.DropDownList lstStates;
        protected System.Web.UI.WebControls.DropDownList lstStates2;
        protected System.Web.UI.WebControls.DropDownList lstStates3;

        protected System.Web.UI.HtmlControls.HtmlTableRow trDateFromTo;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrRoomAvaible;
        protected System.Web.UI.HtmlControls.HtmlTableRow trActDct;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrLicense;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAvlChk;
        protected System.Web.UI.HtmlControls.HtmlTableRow DetailsView;
        protected System.Web.UI.HtmlControls.HtmlTableRow ListView;

        protected System.Web.UI.HtmlControls.HtmlTableCell TDSelectedRoom;

        protected System.Web.UI.WebControls.RegularExpressionValidator regRoomStartTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator regRoomEndTime;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVMRRoomadded;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Tierslocstr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnServiceType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTimeZone;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedlocframe;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnView;
        protected System.Web.UI.HtmlControls.HtmlInputHidden addroom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDelRoom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDelRoomID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditroom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden cmd;
        protected System.Web.UI.HtmlControls.HtmlInputHidden helpPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomIDs;
        protected System.Web.UI.HtmlControls.HtmlInputHidden IsSettingsChange;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCapacityH;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCapacityL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAV;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMedia;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLoc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnZipCode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAvailable;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnStartTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndTime;


        protected MetaBuilders.WebControls.ComboBox confRoomStartTime;
        protected MetaBuilders.WebControls.ComboBox confRoomEndTime;

        protected System.Web.UI.WebControls.TextBox txtRoomDateFrom;
        protected System.Web.UI.WebControls.TextBox txtRoomDateTo;
        protected System.Web.UI.WebControls.TextBox TxtNameSearch;
        protected System.Web.UI.WebControls.TextBox txtZipCode;
        protected System.Web.UI.HtmlControls.HtmlTableRow trHotdesking; //FB 2717
        
        #endregion

        protected int GridPageSize
        {
            get
            {
                if (Session[PageSizeSessionKey] == null)
                    return grid.SettingsPager.PageSize;
                return (int)Session[PageSizeSessionKey];
            }
            set { Session[PageSizeSessionKey] = value; }
        }

        const string PageSizeSession = "ed5e843d-cff7-47a7-815e-832923f7fb10";

        protected int GridPage
        {
            get
            {
                if (Session[PageSizeSession] == null)
                    return grid2.SettingsPager.PageSize;
                return (int)Session[PageSizeSession];
            }
            set { Session[PageSizeSession] = value; }
        }

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            imageUtilObj = new myVRMNet.ImageUtil();
            String stDate = "";
            String enDate = "";
            String serType = "";

            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("RoomSearch.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                LblError.Visible = false;
                LblError.Text = "";

                grid.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
                grid.Settings.ShowVerticalScrollBar = true;
                grid.Settings.VerticalScrollableHeight = 450;

                grid2.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
                grid2.Settings.ShowVerticalScrollBar = true;
                grid2.Settings.VerticalScrollableHeight = 450;



                grid.SettingsPager.PageSize = GridPageSize;
                grid.Templates.PagerBar = new CustomPagerBarTemplate();

                grid2.SettingsPager.PageSize = GridPage;
                grid2.Templates.PagerBar = new CustomPagerBarTemplate2();
                //GetArrayList();




                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                if (Session["timeFormat"].ToString() == "2") //FB 2588
                    tformat = "HHmmZ";

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                if (tformat == "HH:mm")
                {
                    regRoomEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                    regRoomEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    regRoomStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                    regRoomStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                }
                else if (tformat == "HHmmZ")
                {
                    regRoomEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                    regRoomEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    regRoomStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                    regRoomStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                }

                if (Session["DtDisp"] != null)
                {
                    grid.DataSource = (DataTable)Session["DtDisp"];
                    grid2.DataSource = (DataTable)Session["DtDisp"];
                }

                if (Session["systemTimezoneID"] != null)
                {
                    if (Session["systemTimezoneID"].ToString() != "")
                    {
                        tzone = Session["systemTimezoneID"].ToString();
                    }
                }

                if (Session["language"] == null)//FB 1830
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                if (Session["EnableRoomServiceType"] == null)//FB 2219 
                    Session["EnableRoomServiceType"] = "0";
                if (Session["EnableRoomServiceType"].ToString() != "")
                    EnableRoomServiceType = Session["EnableRoomServiceType"].ToString();

                //FB 2599 Start
                if (Session["Cloud"] != null) //FB 2262 
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }
                //FB 2599 End
                //FB 2694
                String searchType = "";
                if (Request.QueryString["type"] != null)
                    searchType = Request.QueryString["type"].ToString().Trim();

                if (searchType.ToLower() == "h")
                {
                    chkHotdesking.Checked = true;
                    chkHotdesking.Enabled = false;

                    Available.Enabled = false;
                    chkIsVMR.Enabled = false;
                    chkGuestRooms.Enabled = false;
                    chkFavourites.Enabled = false;
                }

                //FB 2646 Starts
                if (Request.QueryString["conforgID"] != null)
                {
                    confOrgID = Request.QueryString["conforgID"].ToString();
                    Session.Add("multisiloOrganizationID", confOrgID);
                }
                //FB 2646 Ends
                if (Request.QueryString["frm"] != null)
                    Parentframe = Request.QueryString["frm"].ToString().Trim();

                if (Request.QueryString["rmEdit"] != null)
                    roomEdit = Request.QueryString["rmEdit"].ToString().Trim();

                if (roomEdit == "Y")
                {
                    Session["multisiloOrganizationID"] = null; //FB 2274
                    TrLicense.Attributes.Add("style", "display:block");
                    trActDct.Attributes.Add("style", "display:block");
                }

                if (Parentframe == "frmCalendarRoom" || roomEdit == "Y")
                {
                    TrRoomAvaible.Attributes.Add("style", "display:none;");
                    trAvlChk.Attributes.Add("style", "display:none;");
                    trDateFromTo.Attributes.Add("style", "display:none;");
                }

                if (roomEdit == "N") //FB 2426 //FB 2448
                {
                    chkGuestRooms.Attributes.Add("style", "display:none");
                    chkIsVMR.Attributes.Add("style", "display:none");
                }
                //FB 2717 start
                /*//FB 2599 start
                if (Cloud == 1) //FB 2262
                {
                    lblViewType.Visible = false;
                    DrpDwnListView.Visible = false;
                    chkGuestRooms.Enabled = false;
                    chkIsVMR.Enabled = false;
                    chkIsVMR.Checked = true;
                }
                //FB 2599 End*/

                if (Request.QueryString["CloudConf"] != null)
                {
                    int.TryParse(Request.QueryString["CloudConf"].ToString(), out CloudConferencing);
                }

                if (Cloud == 1 && CloudConferencing == 1)
                {
					chkHotdesking.Checked = false;
                    trHotdesking.Attributes.Add("style", "display:none;");
                    //chkIsVMR.Enabled = false;
                    //chkIsVMR.Checked = true;
                }
                //FB 2717 End

                if (Request.QueryString["ConfType"] != null)//FB 2694
                {
                    if (Request.QueryString["ConfType"] != "")
                        ConfType = Request.QueryString["ConfType"].ToString();
                }

                GetData();

                if (!IsPostBack)
                {
                    if (Session["roomViewType"] != null) //FB 1577
                    {
                        if (Session["roomViewType"].ToString() != "")
                        {
                            DrpDwnListView.SelectedValue = Session["roomViewType"].ToString();
                        }
                    }

                    if (Request.QueryString["rmsframe"] != null)
                    {
                        selectedlocframe.Value += Request.QueryString["rmsframe"].ToString().Trim();

                        if (Request.QueryString["isVMR"] != null)//FB 2448
                        {
                            if (Request.QueryString["isVMR"] != "")
                                roomVMR = Request.QueryString["isVMR"].ToString();
                        }

                        String selrooms = "";


                        if (selectedlocframe.Value != "")
                        {
                            foreach (String s in selectedlocframe.Value.Split(','))
                            {
                                if (s != "")
                                {
                                    if (selrooms == "")
                                        selrooms = s.Trim();
                                    else
                                        selrooms += "," + s.Trim();
                                }
                            }
                        }

                        selectedlocframe.Value = selrooms;
                    }

                    confRoomStartTime.Items.Clear();
                    confRoomEndTime.Items.Clear();
                    obj.BindTimeToListBox(confRoomStartTime, true, true);
                    obj.BindTimeToListBox(confRoomEndTime, true, true);

                    if (selectedlocframe.Value != "" || selectedlocframe.Value != " ")
                        GetselectedRooms();
                    else
                        selectedlocframe.Value = "";

                    if (Request.QueryString["confID"] != null)
                    {
                        confID = Request.QueryString["confID"].ToString();
                        Available.Checked = true;
                        Available.Enabled = false;
                    }

                    if (Request.QueryString["stDate"] != null)
                        stDate = Request.QueryString["stDate"].ToString();

                    if (Request.QueryString["enDate"] != null)
                        enDate = Request.QueryString["enDate"].ToString();

                    if (Request.QueryString["serType"] != null)//FB 2219
                    {
                        if (Request.QueryString["serType"] != "")
                            serType = Request.QueryString["serType"].ToString();

                        if (serType != "")
                            hdnServiceType.Value = serType;
                    }

                    if (Request.QueryString["immediate"] != null) //FB 2534
                        immediate = Request.QueryString["immediate"].ToString();

                    DateTime dStart, dEnd;
                    DateTime.TryParse(stDate, out dStart);
                    DateTime.TryParse(enDate, out dEnd);

                    if (dStart == DateTime.MinValue || dEnd == DateTime.MinValue)
                    {
                        dStart = DateTime.Now;
                        dEnd = dStart.AddMinutes(60);
                    }

                    txtRoomDateFrom.Text = myVRMNet.NETFunctions.GetFormattedDate(dStart);
                    txtRoomDateTo.Text = myVRMNet.NETFunctions.GetFormattedDate(dEnd);

                    confRoomStartTime.Text = dStart.ToString(tformat);
                    confRoomEndTime.Text = dEnd.ToString(tformat);


                    if (Request.QueryString["tzone"] != null)
                    {
                        if (Request.QueryString["tzone"].ToString() != "")
                            tzone = Request.QueryString["tzone"].ToString();

                        //FB 1796
                        if (tzone != "")
                            hdnTimeZone.Value = tzone;
                    }

                    if (roomVMR == "1")//FB 2448
                        chkIsVMR.Attributes.Add("style", "display:block");

                    BindAv();
                    ChangeCalendarDate(null, null);


                }

                if (roomEdit == "Y")
                    TDSelectedRoom.Attributes.Add("style", "display:none");

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);

            }

        }
        #endregion

        #region Bind Rooms

        protected void ChangeCalendarDate(Object sender, EventArgs e)
        {
            String selQuery = "";
            String mediaQry = "";
            String stateQuery = "";
            String avalRoomIDs = "";
            String avValue = "";
            String avItemQueryAnd = "";
            String avItemQueryOr = "";
            String rmName = "";
            String deptQuery = "";
            XmlDocument deptdoc = null;
            DataView dv = null;
            try
            {

                if (hdnDelRoom.Value != "0")
                {
                    btnDeleteRoom_Click();
                    hdnDelRoom.Value = "";
                }

                //if (roomEdit == "N") //FB 2565
                //    selQuery = " Disabled = 0 ";
                //else
                //{
                if (DrpActDct.SelectedValue == "0")
                    selQuery = " Disabled = 0 ";
                if (DrpActDct.SelectedValue == "1")
                    selQuery = " Disabled = 1 ";
                //}

                if (Request.QueryString["isTel"] != null)//FB 2400
                {
                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                    selQuery += " isTelePresence = 0";

                }


                if (addroom.Value == "1")
                {
                    addroom.Value = "0";
                    GetselectedRooms();
                }
                else
                {
                    deptdoc = new XmlDocument();
                    deptdoc.LoadXml(outXMLDept);

                    if (Session["admin"] != null)
                    {
                        //FB 2670
                        Boolean isadmin = false;
                        if (Session["admin"].ToString() == "2" || Session["admin"].ToString() == "3")
                            isadmin = true;

                        if (!isadmin)
                        {
                            XmlNode roomId = deptdoc.SelectSingleNode("Rooms/roomIDs"); //FB 1672
                            XmlNode deptId = deptdoc.SelectSingleNode("Rooms/userDept");//Added for FB 1672
                            if (roomId != null)
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");

                                if (roomId.InnerText != "")
                                    selQuery += "RoomID in ( " + roomId.InnerText + " ) ";
                                else if (deptId.InnerText != "")
                                    selQuery += "Department = '" + deptId.InnerText + "'";//Added for FB 1672
                                else
                                    selQuery += "Department = ''";
                            }
                        }
                    }


                    XmlNode favlist = deptdoc.SelectSingleNode("Rooms/favourite");
                    if (favlist != null)
                        favRooms = favlist.InnerText;
                    if (Available.Checked)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " RoomID NOT IN  (" + GetAvailableRooms() + ")";
                    }

                    //FB 2411 Starts
                    string serivetypein = "0";
                    if (hdnServiceType.Value == "3")
                        serivetypein = "1,2,3";
                    else if (hdnServiceType.Value == "2")
                        serivetypein = "1,2";
                    else if (hdnServiceType.Value == "1")
                        serivetypein = "1";

                    if (EnableRoomServiceType == "1" && hdnServiceType.Value != "" && hdnServiceType.Value != "-1") //FB 2219
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " ServiceType in (" + serivetypein + ")";
                    }
                    //FB 2411 Ends
                    if (Session["DedicatedVideo"].ToString() != "1")
                    {
                        if (Request.QueryString["dedVid"] != null)//FB 2334
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " DedicatedVideo = 0";

                        }
                    }

                    if (favRooms == "0")
                        chkFavourites.Checked = false;

                    if (chkFavourites.Checked)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " RoomID  IN  (" + favRooms + ")";
                    }
                    //FB 2426 Start
                    if (GuestRooms == "0")
                        chkGuestRooms.Checked = false;

                    if (chkGuestRooms.Checked)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " Extroom = 1";
                        //FB 2549 Start
                        if (!(Session["admin"].ToString().Trim() == "2"))
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " LoginUserId = " + Session["userID"].ToString();
                        }
                        //FB 2549 End
                    }
                    else
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " Extroom = 0";
                    }
                    //FB 2426 End

                    //FB 2694 Start
                    if (HotdeskingRooms == "0")
                        chkHotdesking.Checked = false;

                    if (ConfType == "8")
                    {
                        chkHotdesking.Checked = true;
                        chkHotdesking.Enabled = false;
                    }

                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                    if (chkHotdesking.Checked)
                    {
                        selQuery += " RoomCategory = 4";
                        //if (!(Session["admin"].ToString().Trim() == "2"))
                        //{
                        //    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        //    selQuery += " LoginUserId = " + Session["userID"].ToString();
                        //}
                    }
                    else
                    {
                        selQuery += " RoomCategory NOT IN (4) ";
                    }
                    //FB 2694 End

                    //FB 2448 Start
                    if (VMR == "0")//roomVMR == "1" || 
                        chkIsVMR.Checked = true;


                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                    if (chkIsVMR.Checked)
                    {
                        selQuery += " IsVMR = 1";
                        DrpDwnListView.SelectedValue = "1";
                        if (Cloud == 1 && CloudConferencing == 1)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " RoomCategory = 5 and OwnerID = " + Session["userID"].ToString(); //FB 2717-RoomCategory = 5 ->Vidyo Rooms
                            //DrpDwnListView.Enabled = false;//FB 2717
                        }
                    }
                    else
                    {
                        selQuery += " IsVMR = 0";
                        //DrpDwnListView.Enabled = true; //FB 2717
                    }
                    //FB 2448 End

                    //FB 2717 - Starts
                    if (Cloud <= 0 && CloudConferencing <= 0)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " RoomCategory not in (5) ";
                    }
                    //FB 2717 - End


                    //FB 2717 - Starts
                    /*if (Cloud == 1 && CloudConferencing == 1)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " RoomCategory = 5 "; //FB 2717-RoomCategory = 5 ->Vidyo Rooms
                        //DrpDwnListView.Enabled = false;//FB 2717
                    }
                    else
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " RoomCategory not in (5) ";
                    }*/
                    //FB 2717 - End


                    //FB 2594 Starts
                    string isPublicEP = "0";
                    if (Session["EnablePublicRooms"].ToString() == "1")
                        isPublicEP = "1";

                    if (isPublicEP == "0")
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " isPublic = 0";
                    }
                    //FB 2594 Ends

                    if (hdnName.Value == "1")
                    {
                        selQuery = ((selQuery == "") ? " RoomName like '*" + TxtNameSearch.Text + "*'" : selQuery += " and RoomName like '*" + TxtNameSearch.Text + "*'");

                    }
                    else
                    {


                        if (hdnCapacityL.Value != "" || hdnCapacityH.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " MaximumCapacity >= " + hdnCapacityL.Value;

                            if (hdnCapacityH.Value != "")
                                selQuery += " and MaximumCapacity <= " + hdnCapacityH.Value;
                        }

                        //FB 2694 Start
                        if (ConfType != "" && ConfType != "8" && chkHotdesking.Checked)
                        {
                            mediaQry = " Video = 2 "; //Video = 1 or
                        }
                        else
                        {
                            if (MediaNone.Checked)
                                mediaQry += " Video = 0 ";

                            mediaQry = ((mediaQry == "" || !MediaAudio.Checked) ? mediaQry : mediaQry += " or ");

                            if (MediaAudio.Checked)
                                mediaQry += " Video = 1 ";

                            mediaQry = ((mediaQry == "" || !MediaVideo.Checked) ? mediaQry : mediaQry += " or ");

                            if (MediaVideo.Checked)
                                mediaQry += " Video = 2 ";//Code changed for FB 1744

                        }
                        mediaQry = "( " + mediaQry + " )";

                        //FB 2694 End

                        selQuery = ((selQuery == "") ? mediaQry : selQuery + " and " + mediaQry);
                        if (hdnZipCode.Value == "1")
                        {
                            selQuery += " and ZipCode Like  '*" + txtZipCode.Text + "*' ";

                        }
                        else
                        {

                            if (lstCountry.SelectedValue != "-1")
                                selQuery += " and Country = " + lstCountry.SelectedValue;

                            if (lstStates.SelectedValue != "-1")
                                stateQuery += " State = " + lstStates.SelectedValue;

                            stateQuery = ((stateQuery == "" || lstStates2.SelectedValue == "-1") ? stateQuery : stateQuery += " or ");

                            if (lstStates2.SelectedValue != "-1")
                                stateQuery += " State = " + lstStates2.SelectedValue;

                            stateQuery = ((stateQuery == "" || lstStates3.SelectedValue == "-1") ? stateQuery : stateQuery += " or ");

                            if (lstStates3.SelectedValue != "-1")
                                stateQuery += " State = " + lstStates3.SelectedValue;

                            stateQuery = ((stateQuery == "") ? stateQuery : "( " + stateQuery + " )");

                            selQuery = ((stateQuery == "") ? selQuery : selQuery + " and " + stateQuery);
                        }

                        if (avalRoomIDs != "" && Available.Checked)
                            selQuery += " and ( RoomID in ( " + avalRoomIDs + " ) )";

                        if (PhotosOnly.Checked)
                            selQuery += " and  ( ImageName <> '') "; //Image Project


                        if (hdnAV.Value == "1")
                        {
                            foreach (ListItem lstItem in AVlist.Items)
                            {

                                rmName = lstItem.Text.Trim();

                                if (lstItem.Text.Trim().Split(' ').Length > 1)
                                    rmName = lstItem.Text.Trim().Split(' ')[0] + lstItem.Text.Trim().Split(' ')[1];

                                if (lstItem.Selected)
                                {
                                    if (avItemQueryAnd == "")
                                        avItemQueryAnd = " and ( " + rmName + " = 1 ";
                                    else
                                        avItemQueryAnd += " or " + rmName + " = 1 ";
                                }
                            }

                            if (avItemQueryAnd != "")
                                avItemQueryAnd += " ) ";

                            selQuery += avItemQueryAnd;


                        }
                        if (HandiCap.Checked)
                            selQuery += " and Handicappedaccess = 1 ";

                    }


                    if (ds.Tables.Count > 0)
                    {

                        filtrs = ds.Tables[0].Select(selQuery);

                        dtDisp = ds.Tables[0].Clone();

                        if (filtrs != null)
                        {
                            fileImgPath = "";

                            pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/";//FB 1830
                            //filePath = HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\room\\"; //Image Project
                            filePath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\"; //FB 1830
                            foreach (DataRow rw in filtrs)
                            {

                                String Videoaval = rw["Video"].ToString();

                                if (rw["MaximumCapacity"].ToString() == "")
                                    rw["MaximumCapacity"] = 0;
                                else
                                    rw["MaximumCapacity"] = Int32.Parse(rw["MaximumCapacity"].ToString());


                                if (Videoaval == "2") //Code changed for FB 1744
                                    rw["Video"] = "Video";
                                else if (Videoaval == "1")
                                    rw["Video"] = "Audio";
                                else
                                    rw["Video"] = "None";

                                //Image Project starts ...

                                String RoomImage = rw["ImageName"].ToString();

                                String rImage = "";

                                if (RoomImage == "[None]" || RoomImage == "")
                                {
                                    rImage = "image/noimage.gif";
                                }
                                else
                                {
                                    rImage = pathName + RoomImage;
                                    if (!File.Exists(filePath + RoomImage)) //Image Project
                                    {
                                        WriteToFile(rw["RoomID"].ToString(), filePath + RoomImage);

                                    }

                                }
                                rw["ImageName"] = rImage;

                                //FB 2065 - Start
                                string AttributeImage = rw["AttributeImage"].ToString();
                                string Image1 = "";

                                if (rw["RoomIconTypeId"].ToString() != "")
                                {
                                    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.AudioOnly.ToString()) //RoomIconTypeId - Attribute Type ID for Room Types.
                                        Image1 = "Audio.jpg";

                                    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.VideoOnly.ToString())
                                        Image1 = "Video.jpg";

                                    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.RoomOnly.ToString())
                                        Image1 = "Room.jpg";

                                    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.Telepresence.ToString())
                                        Image1 = "Telepresence.jpg";

                                    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.HotdeskingAudio.ToString())
                                        Image1 = "HotdeskingAudio.jpg";

                                    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.HotdeskingVideo.ToString())
                                        Image1 = "HotdeskingVideo.jpg";

                                    if (AttributeImage != "")
                                    {
                                        imgArray = imageUtilObj.ConvertBase64ToByteArray(AttributeImage);
                                        FileStream newFile = new FileStream(filePath + Image1, FileMode.Create);

                                        newFile.Write(imgArray, 0, imgArray.Length);

                                        newFile.Close();
                                        newFile.Dispose();
                                        newFile = null;
                                        imgArray = null;
                                        AttributeImage = null;
                                    }
                                }
                                
                                //FB 2065 - End

                                dtDisp.ImportRow(rw);
                            }
                            //Image Project End

                            dv = new DataView(dtDisp);

                            dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";

                            if (Session["DtDisp"] != null)
                                Session["DtDisp"] = dv.ToTable();
                            else
                                Session.Add("DtDisp", dv.ToTable());

                        }

                        //FB 2694 Starts
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["DtDisp"];
                        //FB 2065 Starts
                        System.Data.DataColumn ImagePath = new System.Data.DataColumn("ImgRoomIcon", typeof(System.String));
                        ImagePath.DefaultValue = "";
                        dt.Columns.Add(ImagePath);

                        DataRow[] Audio = dt.Select("RoomIconTypeId = " + ns_MyVRMNet.vrmAttributeType.AudioOnly);
                        if (Audio.Count() > 0)
                        {
                            for (int i = 0; i < Audio.Count(); i++)
                            {
                                Audio[i]["ImgRoomIcon"] = pathName + "Audio.jpg";

                            }
                        }
                        DataRow[] Video = dt.Select("RoomIconTypeId = " + ns_MyVRMNet.vrmAttributeType.VideoOnly);
                        if (Video.Count() > 0)
                        {
                            for (int i = 0; i < Video.Count(); i++)
                            {
                                Video[i]["ImgRoomIcon"] = pathName + "Video.jpg";

                            }
                        }
                        DataRow[] Room = dt.Select("RoomIconTypeId = " + ns_MyVRMNet.vrmAttributeType.RoomOnly);
                        if (Room.Count() > 0)
                        {
                            for (int i = 0; i < Room.Count(); i++)
                            {
                                Room[i]["ImgRoomIcon"] = pathName + "Room.jpg";

                            }
                        }
                        DataRow[] Telepresence = dt.Select("RoomIconTypeId = " + ns_MyVRMNet.vrmAttributeType.Telepresence);
                        if (Telepresence.Count() > 0)
                        {
                            for (int i = 0; i < Telepresence.Count(); i++)
                            {
                                Telepresence[i]["ImgRoomIcon"] = pathName + "Telepresence.jpg";

                            }
                        }
                        DataRow[] HotdeskingAudio = dt.Select("RoomIconTypeId = " + ns_MyVRMNet.vrmAttributeType.HotdeskingAudio);
                        if (HotdeskingAudio.Count() > 0)
                        {
                            for (int i = 0; i < HotdeskingAudio.Count(); i++)
                            {
                                HotdeskingAudio[i]["ImgRoomIcon"] = pathName + "HotdeskingAudio.jpg";

                            }
                        }
                        DataRow[] HotdeskingVideo = dt.Select("RoomIconTypeId = " + ns_MyVRMNet.vrmAttributeType.HotdeskingVideo);
                        if (HotdeskingVideo.Count() > 0)
                        {
                            for (int i = 0; i < HotdeskingVideo.Count(); i++)
                            {
                                HotdeskingVideo[i]["ImgRoomIcon"] = pathName + "HotdeskingVideo.jpg";
                            }
                        }

                        /* FB 2065
                        //FB 2694 Starts                         

                        System.Data.DataColumn ImagePath = new System.Data.DataColumn("ImgHotDeskingRoom", typeof(System.String));
                        ImagePath.DefaultValue = "";
                        dt.Columns.Add(ImagePath);

                        System.Data.DataColumn ImageStatus = new System.Data.DataColumn("ImageStatus", typeof(System.String));
                        ImageStatus.DefaultValue = "0";
                        dt.Columns.Add(ImageStatus);

                        DataRow[] video = dt.Select("RoomCategory = 4 AND Video = 'video'");

                        DataRow[] Audio = dt.Select("RoomCategory = 4 AND Video = 'Audio'");

                        if (Audio.Count() > 0)
                        {
                            for (int i = 0; i < Audio.Count(); i++)
                            {
                                Audio[i]["ImgHotDeskingRoom"] = "../image/Audio.png";
                                Audio[i]["ImageStatus"] = "1";
                            }
                        }
                        if (video.Count() > 0)
                        {
                            for (int i = 0; i < video.Count(); i++)
                            {
                                video[i]["ImgHotDeskingRoom"] = "../image/Video.png";
                                video[i]["ImageStatus"] = "1";
                            }
                        }*/
                        //FB 2065 End

                        grid.DataSource = dt;
                        grid.DataBind();

                        grid2.DataSource = dt;
                        grid2.DataBind();
                        changeView();
                        //FB 2694 End
                    }
                    else
                    {
                        DataTable dtNone = new DataTable();
                        if (Session["DtDisp"] != null)
                            Session["DtDisp"] = dtNone;
                        else
                            Session.Add("DtDisp", dtNone);
                    }
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room  error : " + ex.Message);
            }
        }

        #endregion

        #region Rooms Attribute

        protected void SetRoomAttributes(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType.Equals(ListItemType.Item)) || (e.Item.ItemType.Equals(ListItemType.AlternatingItem)))
                {

                    HyperLink lnkRoom = (HyperLink)e.Item.FindControl("btnViewDetails");
                    if (lnkRoom != null)
                        lnkRoom.Attributes.Add("onclick", "javascript:chkresources('" + e.Item.Cells[0].Text + "');");

                    /* HyperLink lnkRooms = (HyperLink)e.Item.FindControl("btnEdit");
                     if (lnkRooms != null)
                         lnkRooms.Attributes.Add("onclick", "javascript:EditRoom('" + e.Item.Cells[0].Text + "');");
                     if (roomEdit == "Y")
                         lnkRooms.Attributes.Add("style", "display:block");*/



                    HtmlImage imageDel = (HtmlImage)e.Item.FindControl("ImageDel");

                    if (imageDel != null)
                        imageDel.Attributes.Add("onclick", "javascript:Delroms('" + e.Item.Cells[0].Text.Trim() + "');");
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        #endregion

        #region Bind AV List

        private void BindAv()
        {

            String itemInXML = "";
            String itemOutXML = "<ItemList></ItemList>";
            XmlDocument xmldoc = null;
            try
            {


                if (!IsPostBack)
                {
                    if (Session["roomModule"].ToString() == "1")
                    {
                        itemInXML = "<login><userID>11</userID>" + obj.OrgXMLElement() + "<Type>1</Type>" + obj.OrgXMLElement() + "</login>";
                        itemOutXML = obj.CallMyVRMServer("GetItemsList", itemInXML, Application["MyVRMServer_ConfigPath"].ToString());
                    }
                    xmldoc = new XmlDocument();
                    xmldoc.LoadXml(itemOutXML);

                    AVlist.Items.Add(new ListItem("None"));

                    if (AVlist.Items.FindByText("None") != null)
                        AVlist.Items.FindByText("None").Selected = true;

                    XmlNodeList nodes = xmldoc.SelectNodes("//ItemList/Item");
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("Name") != null)
                            AVlist.Items.Add(new ListItem(node.SelectSingleNode("Name").InnerText));

                        if (AVlist.Items.FindByText(node.SelectSingleNode("Name").InnerText) != null)
                            AVlist.Items.FindByText(node.SelectSingleNode("Name").InnerText).Selected = true;
                    }

                    obj.GetCountryCodes(lstCountry);

                    if (lstCountry.Items.FindByValue("-1") != null)
                        lstCountry.Items.FindByValue("-1").Selected = true;

                    BindStates();
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        private void BindStates()
        {
            try
            {
                if (hdnLoc.Value == "0")
                {
                    hdnLoc.Value = "1";

                    if (lstCountry.Items.FindByValue("-1") != null)
                        lstCountry.Items.FindByValue("-1").Text = "Any";

                    lstStates.Enabled = true;
                    lstStates2.Enabled = true;
                    lstStates3.Enabled = true;

                    obj.GetCountryStatesSearch(lstStates, lstCountry.SelectedValue);
                    obj.GetCountryStatesSearch(lstStates2, lstCountry.SelectedValue);
                    obj.GetCountryStatesSearch(lstStates3, lstCountry.SelectedValue);

                    if (lstStates.Items.FindByValue("-1") != null)
                    {
                        lstStates.Items.FindByValue("-1").Text = "Any";
                        lstStates.Items.FindByValue("-1").Selected = true;
                    }
                    if (lstStates2.Items.FindByValue("-1") != null)
                    {
                        lstStates2.Items.FindByValue("-1").Text = "Any";
                        lstStates2.Items.FindByValue("-1").Selected = true;
                    }
                    if (lstStates3.Items.FindByValue("-1") != null)
                    {
                        lstStates3.Items.FindByValue("-1").Text = "Any";
                        lstStates3.Items.FindByValue("-1").Selected = true;
                    }
                }


            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        private void CreateTable()
        {
            try
            {
                dtSel = new DataTable();
                dtSel.Columns.Add("RoomID");
                dtSel.Columns.Add("RoomName");
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }

        }

        protected void ASPxGridView1_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
                {
                    if (e.KeyValue != null)
                    {
                        string RoomT1ID = "";//FB 2637

                        HyperLink lnkRoom = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "btnViewDetailsDev") as HyperLink;
                        if (lnkRoom != null)
                            lnkRoom.Attributes.Add("onclick", "javascript:chkresource('" + e.KeyValue.ToString() + "');");

                        HyperLink lblh = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "Hyper1") as HyperLink;

                        //FB 2637 Starts
                        Tier1ID = "0";
                        if (Parentframe == "frmConferenceSetUp" || Parentframe == "frmExpress")
                            Tier1ID = e.GetValue("Tier1ID").ToString();

                        RoomT1ID = e.KeyValue.ToString() + ";" + Tier1ID;

                        if (lblh != null)
                        {
                            lblh.Attributes.Add("onclick", "Javascript:Addroms('" + RoomT1ID + "')"); //FB 2637 Ends
                            lblh.ForeColor = System.Drawing.Color.Green;
                        }

                        HyperLink lblDel = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "DelRoom") as HyperLink;

                        if (lblDel != null)
                            lblDel.Attributes.Add("onclick", "Javascript:delRoom('" + e.KeyValue.ToString() + "')");

                        HyperLink lblEdit = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "Editroom") as HyperLink;

                        if (lblEdit != null)
                            lblEdit.Attributes.Add("onclick", "Javascript:EditRoom('" + e.KeyValue.ToString() + "')");

                        //FB 2426 Start
                        HyperLink lblimport = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "importRoom") as HyperLink;

                        if (lblimport != null)
                            lblimport.Attributes.Add("onclick", "Javascript:ImportRoom('" + e.KeyValue.ToString() + "')");

                        HyperLink lblExtDel = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "DelGuestRoom") as HyperLink;

                        if (lblExtDel != null)
                            lblExtDel.Attributes.Add("onclick", "Javascript:delGuestRoom('" + e.KeyValue.ToString() + "')");
                        //FB 2599 Start
                        //FB 2717 Vidyo Integration Start
                        //if (Cloud == 1 || Session["admin"].ToString() == "3") //FB 2262 //FB 2670
                        if (Session["admin"].ToString() == "3") //FB 2262 //FB 2670
                        {
                            /*if (Cloud == 1)
                            {
                                lblEdit.Attributes.Remove("onClick");
                                lblEdit.Enabled = false;
                                lblEdit.Style.Add("cursor", "default");
                                lblEdit.ForeColor = System.Drawing.Color.Gray;
                            }*/
                            //else //FB 2717 Vidyo Integration End
                            lblEdit.Text = "View";

                            //lblDel.Attributes.Remove("onClick");
                            //lblExtDel.Attributes.Remove("onClick");

                            

                            //lblDel.Style.Add("cursor", "default");
                            //lblExtDel.Style.Add("cursor", "default");

                            //lblDel.ForeColor = System.Drawing.Color.Gray;
                            //lblExtDel.ForeColor = System.Drawing.Color.Gray;
                            //ZD 100263
                            lblDel.Visible = false;
                            lblExtDel.Visible = false;
                        }
                        else
                        {
                            lblDel.ForeColor = System.Drawing.Color.Red;
                            lblExtDel.ForeColor = System.Drawing.Color.Red;
                            lblh.ForeColor = System.Drawing.Color.Green;
                        }
                        //FB 2599 End
                        if (lblEdit != null && lblDel != null && lblh != null && lblimport != null && lblExtDel != null)
                        {

                            if (roomEdit == "Y" && chkGuestRooms.Checked == false)
                            {
                                lblh.Attributes.Add("style", "display:none");
                                lblimport.Attributes.Add("style", "display:none");
                                lblExtDel.Attributes.Add("style", "display:none");
                            }
                            else if (chkGuestRooms.Checked == true)
                            {
                                lblh.Attributes.Add("style", "display:none");
                                lblDel.Attributes.Add("style", "display:none");
                                lblEdit.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                chkGuestRooms.Attributes.Add("style", "display:none");
                                lblDel.Attributes.Add("style", "display:none");
                                lblEdit.Attributes.Add("style", "display:none");
                                lblimport.Attributes.Add("style", "display:none");
                                lblExtDel.Attributes.Add("style", "display:none");
                            }
                            //FB 2426 End
                            if (GetDeactRooms(e.KeyValue.ToString()))
                            {
                                lblDel.Text = obj.GetTranslatedText("Activate");
                                lblDel.Attributes.Add("onclick", "Javascript:ActivateRoom('" + e.KeyValue.ToString() + "')");
                                lblDel.Style.Add("color", "Green");
                                //lblEdit.Attributes.Add("style", "display:none");Code added for FB 1600
                                //FB 2670
                                if (Session["admin"].ToString() == "3")
                                {
                                    //lblDel.Attributes.Remove("onClick");
                                    
                                    //lblDel.Style.Add("cursor", "default");
                                    //lblDel.Style.Add("color", "Gray");
                                    //lblDel.ForeColor = System.Drawing.Color.Gray;
                                    //ZD 100263
                                    lblDel.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);

            }
        }


        public String GetAvailableRooms()
        {
            String rets = "11";
            myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();

            try
            {
                string inxmls = "<conferenceTime>";
                inxmls += func.OrgXMLElement();
                inxmls += "<userID>" + Session["userID"].ToString() + "</userID>";
                confID = "0";

                if (Request.QueryString["confID"] != null) // FB 1798
                {
                    confID = Request.QueryString["confID"].ToString();//Code added for FB 1727
                    if (confID == "" || confID.ToUpper() == "NEW")
                        confID = "0";
                }

                inxmls += "<confID>" + confID + "</confID>";


                DateTime dStart = DateTime.MinValue;
                DateTime dEnd = DateTime.MinValue;
                try
                {
                    dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateFrom.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confRoomStartTime.Text)); //FB 2588
                    dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateTo.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confRoomEndTime.Text)); //FB 2588
                }
                catch (Exception dt)
                {
                    throw new Exception("Please enter a valid date time.");
                }
                //FB 1796
                if (hdnTimeZone.Value != "")
                    tzone = hdnTimeZone.Value;

                TimeSpan ts = dEnd.Subtract(dStart);
                inxmls += "		<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateFrom.Text) + "</startDate>";
                inxmls += "		<startHour>" + dStart.Hour + "</startHour>";
                inxmls += "		<startMin>" + dStart.Minute + "</startMin>";
                inxmls += "		<startSet>" + dStart.ToString("tt") + "</startSet>";
                inxmls += "		<timeZone>" + tzone + "</timeZone>";
                inxmls += "		<immediate>" + immediate + "</immediate>"; //FB 2534

                Double durationMin = ts.TotalMinutes;
                inxmls += "		<durationMin>" + durationMin.ToString() + "</durationMin>";
                //FB 2634
                //FB 2594 Starts
                //string isPublicEP = "0";
                //if (Session["EnablePublicRooms"].ToString() == "1")
                //    isPublicEP = "1";
                //inxmls += "		<enablePublicRoom>" + isPublicEP + "</enablePublicRoom>";
                //FB 2594 Ends
                inxmls += "<ConfType>" + ConfType + "</ConfType>";
                inxmls += "</conferenceTime>";


                String outxmls = obj.CallMyVRMServer("GetBusyRooms", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(outxmls);

                XmlNode busy = doc.SelectSingleNode("roomIDs");

                if (busy != null)
                {
                    if (busy.InnerText != "")
                        rets = busy.InnerText;

                }

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
            func = null;
            return rets;
        }



        #endregion

        #region Get Selective rooms

        public void GetselectedRooms()
        {
            DataRow[] selrows = null;
            DataTable dtselected = null;
            String selrooms = "";
            try
            {
                if (ds.Tables.Count > 0)
                {

                    if (ds.Tables[0] != null)
                    {
                        if (selectedlocframe.Value != "")
                        {
                            foreach (String s in selectedlocframe.Value.Split(','))
                            {
                                if (s != "")
                                {
                                    if (selrooms == "")
                                        selrooms = s.Trim();
                                    else
                                        selrooms += "," + s.Trim();
                                }
                            }

                            selrows = ds.Tables[0].Select("RoomID in ( " + selrooms + ")");

                            if (selrows != null)
                            {
                                if (selrows.Length > 0)
                                {
                                    Tierslocstr.Value = "";
                                    locstr.Value = "";
                                    dtselected = ds.Tables[0].Clone();
                                    foreach (DataRow rw in selrows)
                                    {
                                        if (Tierslocstr.Value == "")
                                            Tierslocstr.Value = rw["Tier1Name"].ToString() + " > " + rw["Tier2Name"].ToString() + " > " + rw["RoomName"].ToString();
                                        else
                                            Tierslocstr.Value += "," + rw["Tier1Name"].ToString() + " > " + rw["Tier2Name"].ToString() + " > " + rw["RoomName"].ToString();

                                        if (locstr.Value == "")
                                            locstr.Value = rw["RoomID"].ToString() + "|" + rw["RoomName"].ToString();
                                        else
                                            locstr.Value += "++" + rw["RoomID"].ToString() + "|" + rw["RoomName"].ToString(); //FB 1640
                                        //locstr.Value += "," + rw["RoomID"].ToString() + "|" + rw["RoomName"].ToString(); //FB 1640

                                        dtselected.ImportRow(rw);
                                    }
                                }
                            }

                        }
                        else
                            dtselected = new DataTable();

                        SelectedGrid.DataSource = dtselected;
                        SelectedGrid.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        public void GetData()
        {
            myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
            XmlDocument createRooms = null;

            try
            {
                String inXMLDept = "<login>";
                inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXMLDept += func.OrgXMLElement();
                inXMLDept += "</login>";

                pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/";//FB 1830
                //filePath = HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\room\\";
                //roomxmlPath = HttpContext.Current.Request.MapPath(".").ToString() + "\\" + Session["RoomXmlPath"].ToString();
                filePath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\";// Fb 1830
                roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 1830
                // FB 2274 Starts
                string crossRoomxml;
                if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString() != "0")
                {
                    if (Session["multisiloOrganizationID"].ToString() != "11")
                    {
                        crossRoomxml = "Organizations/Org_" + Session["multisiloOrganizationID"].ToString() + "/Rooms/Room.xml";
                        roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + crossRoomxml;
                    }
                }

                String inXML = "<GetAllRoomsBasicInfo>";//FB 1756
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += func.OrgXMLElement();
                inXML += "  <RoomID></RoomID>";
                inXML += "</GetAllRoomsBasicInfo>"; //FB 1756

                //FB 2137 Start
                outXMLDept = obj.CallMyVRMServer("GetRoomsDepts", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());
                if (Session["UserdeptXML"] == null)
                {
                    Session.Add("UserdeptXML", outXMLDept);
                }
                else
                    Session["UserdeptXML"] = outXMLDept;
                //FB 2137 End


                if (!File.Exists(roomxmlPath))
                {
                    outXML = obj.CallMyVRMServer("GetAllRoomsBasicInfo", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 1756
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        createRooms = new XmlDocument();
                        createRooms.LoadXml(outXML);
                        createRooms.Save(roomxmlPath);
                        createRooms = null;

                    }
                }


                if (obj == null)
                    obj = new myVRMNet.NETFunctions();


                if (File.Exists(roomxmlPath))
                {
                    if (obj.WaitForFile(roomxmlPath))
                    {
                        txtrd = new XmlTextReader(roomxmlPath);
                        txtrd.Read();
                        ds = new DataSet();
                        ds.ReadXml(txtrd);
                        txtrd.Close();
                        txtrd = null;
                    }
                }


                String outXMLlicen = obj.CallMyVRMServer("GetRoomLicenseDetails", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmld = new XmlDocument();
                xmld.LoadXml(outXMLlicen);


                XmlNode nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxNonvideoRemaining");

                if (nde != null)
                    nvidLbl.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVideoRemaining");

                if (nde != null)
                    vidLbl.Text = nde.InnerText;

                //FB 2586 Start
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVMRRemaining");

                if (nde != null)
                    vmrvidLbl.Text = nde.InnerText;
                //FB 2586 End

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxNonvideo");

                if (nde != null)
                {
                    if (nde.InnerText != "" && nvidLbl.Text != "")
                    {
                        ttlnvidLbl.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(nvidLbl.Text)).ToString();
                    }

                }

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVideo");

                if (nde != null)
                {
                    if (nde.InnerText != "" && vidLbl.Text != "")
                    {
                        totalNumber.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(vidLbl.Text)).ToString();
                    }
                }

                //nde = null;
                //FB 2586 Start
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVMR");

                if (nde != null)
                {
                    if (nde.InnerText != "" && vmrvidLbl.Text != "")
                    {
                        tntvmrrooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(vmrvidLbl.Text)).ToString();
                    }
                }
                //FB 2586 End


                //nde = null;
                //FB 2594 Starts
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxPublicRoom");

                if (nde != null)
                {
                    if (nde.InnerText != "")
                        ttlPublicLbl.Text = nde.InnerText;
                }
                lblPublicRoom.Visible = false;
                ttlPublicLbl.Visible = false;
                if (Session["EnablePublicRooms"].ToString() == "1")
                {
                    lblPublicRoom.Visible = true;
                    ttlPublicLbl.Visible = true;
                }
                //FB 2594 Ends
                //FB 2694 Start
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxROHotdeskingRemaining");
                if (nde != null)
                    ROHotdeskingRooms.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxROHotdesking");

                if (nde != null)
                {
                    if (nde.InnerText != "" && ROHotdeskingRooms.Text != "")
                    {
                        ttlROHotdeskingRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(ROHotdeskingRooms.Text)).ToString();
                    }
                }
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVCHotdeskingRemaining");
                if (nde != null)
                    VCHotdeskingRooms.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVCHotdesking");

                if (nde != null)
                {
                    if (nde.InnerText != "" && VCHotdeskingRooms.Text != "")
                    {
                        ttlVCHotdeskingRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(VCHotdeskingRooms.Text)).ToString();
                    }
                }
                //FB 2694 Ends

                nde = null;


            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
            finally
            {
                if (txtrd != null)
                {
                    txtrd.Close();
                }
                txtrd = null;
                func = null;

            }
        }

        public Boolean GetDeactRooms(String rmids)
        {
            String deacRooms = "";
            Boolean retrn = false;
            try
            {
                myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
                String inXMLDept = "<login>";
                inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXMLDept += func.OrgXMLElement();
                inXMLDept += "</login>";
                if (rmlist == null)
                {

                    String outXML = func.CallMyVRMServer("GetDeactivatedRooms", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument docdata = new XmlDocument();
                    docdata.LoadXml(outXML);

                    XmlNode favlist = docdata.SelectSingleNode("roomIDs");
                    if (favlist != null)
                        deacRooms = favlist.InnerText;

                    if (deacRooms != "")
                    {
                        rmlist = new ArrayList();
                        foreach (String s in deacRooms.Split(','))
                        {
                            if (s != "")
                                rmlist.Add(s);

                        }
                    }
                }

                if (rmlist != null)
                    retrn = rmlist.Contains(rmids);

                func = null;

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }

            return retrn;

        }

        #endregion

        #region Delete Room - Submit Button Event Handler
        /// <summary>
        /// To Delete Room - Delete Click Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteRoom_Click()
        {
            String sDelMnLoc = selectedlocframe.Value.Trim();
            String sRmId = "";
            String inDelXML = "";
            String outDelXML = "";
            String cmdDelRM = "DeleteRoom";
            XmlDocument XmlDel = null;
            XmlNodeList delnodes = null;
            XmlNode srch = null;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            XmlDocument createRooms = null;
            XmlNodeList roomList = null;
            string actvteID = "";

            try
            {
                myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
                if (hdnDelRoomID.Value != "")
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    if (hdnDelRoom.Value == func.GetTranslatedText("Activate"))
                    {
                        cmdDelRM = "ActiveRoom";
                        actvteID = "0";
                    }
                    else if (hdnDelRoom.Value == func.GetTranslatedText("Deactivate"))
                    {
                        cmdDelRM = "DeleteRoom";
                        actvteID = "1";
                    }
                    //FB 2426 Start
                    else if (hdnDelRoom.Value == func.GetTranslatedText("Import"))
                    {
                        cmdDelRM = "SetGuesttoNormalRoom";
                        actvteID = "0";
                    }
                    else if (hdnDelRoom.Value == func.GetTranslatedText("Delete"))
                    {
                        cmdDelRM = "DeleteGuestRoom";
                        actvteID = "0";
                    }
                    //FB 2426 End
                    inDelXML += "<login>";
                    inDelXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                    inDelXML += func.OrgXMLElement();
                    inDelXML += "<roomID>" + hdnDelRoomID.Value + "</roomID>";
                    inDelXML += "</login>";

                    XmlDel = new XmlDocument();
                    outDelXML = obj.CallMyVRMServer(cmdDelRM, inDelXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027

                    if (outDelXML.IndexOf("<error>") >= 0)
                    {
                        LblError.Text = obj.ShowErrorMessage(outDelXML); //FB 1577
                        LblError.Visible = true;
                        return;
                    }

                    if (outDelXML.IndexOf("<UserStatus>") > 0) //FB 1644
                    {
                        XmlDocument xmld = new XmlDocument();
                        xmld.LoadXml(outDelXML);

                        LblError.Text = "The Assistant-In-Charge of \"" + xmld.SelectSingleNode("success/AssistantInchargeName").InnerText//FB 2974
                                    + "\" is not found in the Active User List.";
                        LblError.Visible = true;
                        return;
                    }

                    //roomxmlPath = HttpContext.Current.Request.MapPath(".").ToString() + "\\" + Session["RoomXmlPath"].ToString();
                    roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 1830
                    if (File.Exists(roomxmlPath))
                    {

                        if (hdnDelRoomID.Value != "")
                        {
                            createRooms = new XmlDocument();
                            createRooms.Load(roomxmlPath);
                            roomList = createRooms.SelectNodes("Rooms/Room");
                            if (roomList != null)
                            {
                                for (int a = 0; a < roomList.Count; a++)
                                {
                                    if (roomList[a].SelectSingleNode("RoomID").InnerText.Trim() == hdnDelRoomID.Value.Trim())
                                    {
                                        if (actvteID != "")
                                            roomList[a].SelectSingleNode("Disabled").InnerText = actvteID;
                                    }

                                }
                                if (obj == null)
                                    obj = new myVRMNet.NETFunctions();


                                if (File.Exists(roomxmlPath))
                                {
                                    if (obj.WaitForFile(roomxmlPath))
                                    {
                                        File.Delete(roomxmlPath);
                                        //createRooms.Save(roomxmlPath); commented for FB 2426
                                    }
                                }
                            }

                        }

                    }

                    rmlist = null;
                    func = null;
                    GetData();

                }

                hdnDelRoomID.Value = "";//FB 1830


            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);


            }
            finally
            {
                hdnDelRoomID.Value = "";
            }
        }

        #endregion

        #region BindCountry

        protected void BindCountry(Object sender, EventArgs e)
        {
            try
            {
                BindStates();
                /*Type t = typeof(Button);
                object[] p = new object[1];
                p[0] = EventArgs.Empty;
                MethodInfo m = t.GetMethod("OnClick", BindingFlags.NonPublic | BindingFlags.Instance);
                m.Invoke(btnRefreshRooms, p);*/
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        #endregion

        #region Custom Call

        protected void Grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GridPageSize = int.Parse(e.Parameters);

            grid.SettingsPager.PageSize = GridPageSize;
            grid.DataBind();


        }

        protected void Grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GridPage = int.Parse(e.Parameters);

            grid2.SettingsPager.PageSize = GridPage;
            grid2.DataBind();
        }

        protected void Grid_DataBound(object sender, EventArgs e)
        {
            grid.JSProperties["cpPageCount"] = grid.PageCount;
        }

        protected void Grid2_DataBound(object sender, EventArgs e)
        {

            grid2.JSProperties["cpPageCount"] = grid2.PageCount;
        }

        #endregion

        #region CustomPagerBarTemplate
        public class CustomPagerBarTemplate : ITemplate
        {
            ASPxGridView grid;
            enum PageBarButtonType { First, Prev, Next, Last }
            myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
            protected ASPxGridView Grid { get { return grid; } }

            public void InstantiateIn(Control container)
            {
                this.grid = (ASPxGridView)((GridViewPagerBarTemplateContainer)container).Grid;
                Table table = new Table();
                container.Controls.Add(table);
                TableRow row = new TableRow();
                table.Rows.Add(row);
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.First), obj.GetTranslatedText("First"), "pageBarFirstButton_Click");
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Prev), obj.GetTranslatedText("Prev"), "pageBarPrevButton_Click");
                AddLiteralCell(row.Cells, obj.GetTranslatedText("Page"));
                AddTextBoxCell(row.Cells);
                AddLiteralCell(row.Cells, string.Format("of {0}", grid.PageCount));
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Next), obj.GetTranslatedText("Next"), "pageBarNextButton_Click");
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Last), obj.GetTranslatedText("Last"), "pageBarLastButton_Click");
                AddSpacerCell(row.Cells);
                AddLiteralCell(row.Cells, obj.GetTranslatedText("Records per page") + ":");
                AddComboBoxCell(row.Cells);
            }
            void AddButtonCell(TableCellCollection cells, bool enabled, string text, string clickHandlerName)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                ASPxButton button = new ASPxButton();
                cell.Controls.Add(button);
                button.Text = text;
                button.AutoPostBack = false;
                button.UseSubmitBehavior = false;
                button.Enabled = enabled;
                if (enabled)
                    button.ClientSideEvents.Click = clickHandlerName;
            }
            void AddTextBoxCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                ASPxTextBox textBox = new ASPxTextBox();
                cell.Controls.Add(textBox);
                textBox.Width = 30;
                int pageNumber = grid.PageIndex + 1;
                textBox.JSProperties["cpText"] = pageNumber;
                textBox.ClientSideEvents.Init = "pageBarTextBox_Init";
                textBox.ClientSideEvents.ValueChanged = "pageBarTextBox_ValueChanged";
                textBox.ClientSideEvents.KeyPress = "pageBarTextBox_KeyPress";
            }
            void AddComboBoxCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                DropDownList comboBox = new DropDownList();
                cell.Controls.Add(comboBox);
                comboBox.Width = 60;
                comboBox.BorderWidth = 1;   //Edited for FF
                comboBox.BorderColor = System.Drawing.Color.Gray;  //Edited for FF
                comboBox.Items.Add(new ListItem("5"));
                comboBox.Items.Add(new ListItem("10"));
                comboBox.Items.Add(new ListItem("20"));
                comboBox.Items.Add(new ListItem("30"));
                comboBox.Items.Add(new ListItem("40"));
                comboBox.Items.Add(new ListItem("50"));
                comboBox.Items.Add(new ListItem("100"));
                comboBox.SelectedValue = grid.SettingsPager.PageSize.ToString();
                comboBox.Attributes.Add("onchange", "javascript:pagerBarComboBox_SelectedIndexChanged(this)");
            }
            void AddLiteralCell(TableCellCollection cells, string text)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                cell.Text = text;
                cell.Wrap = false;
            }
            void AddSpacerCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                cell.Width = Unit.Percentage(100);
            }
            bool IsButtonEnabled(PageBarButtonType type)
            {
                if (grid.PageIndex == 0)
                {
                    if (type == PageBarButtonType.First || type == PageBarButtonType.Prev)
                        return false;
                }
                if (grid.PageIndex == grid.PageCount - 1)
                {
                    if (type == PageBarButtonType.Next || type == PageBarButtonType.Last)
                        return false;
                }
                return true;
            }
        }
        #endregion

        #region CustomPagerBarTemplate
        public class CustomPagerBarTemplate2 : ITemplate
        {
            ASPxGridView grid;
            enum PageBarButtonType { First, Prev, Next, Last }
            myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
            protected ASPxGridView Grid { get { return grid; } }

            public void InstantiateIn(Control container)
            {
                this.grid = (ASPxGridView)((GridViewPagerBarTemplateContainer)container).Grid;
                Table table = new Table();
                container.Controls.Add(table);
                TableRow row = new TableRow();
                table.Rows.Add(row);
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.First), obj.GetTranslatedText("First"), "pageBarFirstButton_Click");
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Prev), obj.GetTranslatedText("Prev"), "pageBarPrevButton_Click");
                AddLiteralCell(row.Cells, obj.GetTranslatedText("Page"));
                AddTextBoxCell(row.Cells);
                AddLiteralCell(row.Cells, string.Format("of {0}", grid.PageCount));
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Next), obj.GetTranslatedText("Next"), "pageBarNextButton_Click");
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Last), obj.GetTranslatedText("Last"), "pageBarLastButton_Click");
                AddSpacerCell(row.Cells);
                AddLiteralCell(row.Cells, obj.GetTranslatedText("Records per page") + ":");
                AddComboBoxCell(row.Cells);
            }
            void AddButtonCell(TableCellCollection cells, bool enabled, string text, string clickHandlerName)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                ASPxButton button = new ASPxButton();
                cell.Controls.Add(button);
                button.Text = text;
                button.AutoPostBack = false;
                button.UseSubmitBehavior = false;
                button.Enabled = enabled;
                if (enabled)
                    button.ClientSideEvents.Click = clickHandlerName;
            }
            void AddTextBoxCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                ASPxTextBox textBox = new ASPxTextBox();
                cell.Controls.Add(textBox);
                textBox.Width = 30;
                int pageNumber = grid.PageIndex + 1;
                textBox.JSProperties["cpText"] = pageNumber;
                textBox.ClientSideEvents.Init = "pageBarTextBox_Init";
                textBox.ClientSideEvents.ValueChanged = "pageBarTextBox_ValueChanged";
                textBox.ClientSideEvents.KeyPress = "pageBarTextBox_KeyPress";
            }
            void AddComboBoxCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                DropDownList comboBox = new DropDownList();
                cell.Controls.Add(comboBox);
                comboBox.Width = 60;
                comboBox.BorderWidth = 1;   //Edited for FF
                comboBox.BorderColor = System.Drawing.Color.Gray;  //Edited for FF
                comboBox.Items.Add(new ListItem("5"));
                comboBox.Items.Add(new ListItem("10"));
                comboBox.Items.Add(new ListItem("20"));
                comboBox.Items.Add(new ListItem("30"));
                comboBox.Items.Add(new ListItem("40"));
                comboBox.Items.Add(new ListItem("50"));
                comboBox.Items.Add(new ListItem("100"));
                comboBox.SelectedValue = grid.SettingsPager.PageSize.ToString();
                comboBox.Attributes.Add("onchange", "javascript:pagerBarComboBox_SelectedIndexChanged(this)");
            }
            void AddLiteralCell(TableCellCollection cells, string text)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                cell.Text = text;
                cell.Wrap = false;
            }
            void AddSpacerCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                cell.Width = Unit.Percentage(100);
            }
            bool IsButtonEnabled(PageBarButtonType type)
            {
                if (grid.PageIndex == 0)
                {
                    if (type == PageBarButtonType.First || type == PageBarButtonType.Prev)
                        return false;
                }
                if (grid.PageIndex == grid.PageCount - 1)
                {
                    if (type == PageBarButtonType.Next || type == PageBarButtonType.Last)
                        return false;
                }
                return true;
            }
        }
        #endregion

        #region Change View


        private void changeView()
        {
            try
            {


                DetailsView.Attributes.Add("style", "display:none");
                ListView.Attributes.Add("style", "display:none");

                if (DrpDwnListView.SelectedValue == "2")
                    DetailsView.Attributes.Add("style", "display:block");
                else
                    ListView.Attributes.Add("style", "display:block");

            }
            catch (Exception ex)
            { }
        }

        #endregion

        #region ViewChanges
        /// <summary>
        /// Changing the View type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void DrpDwnListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session.Remove("roomViewType");

                if (Session["roomViewType"] == null)
                {
                    Session.Add("roomViewType", DrpDwnListView.SelectedValue);
                }
                else
                    Session["roomViewType"] = DrpDwnListView.SelectedValue;

                changeView();

            }
            catch (Exception ex)
            {
                log.Trace("ListViewChange: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string roomid, String path)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                XmlDocument XmlImages = null;
                if (roomid != "" && path != "")
                {
                    String inXML = "<GetImages>";//FB 1756
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += obj.OrgXMLElement();
                    inXML += "  <RoomID>" + roomid + "</RoomID>";
                    inXML += "</GetImages>"; //FB 1756

                    XmlImages = new XmlDocument();
                    String outImgXML = obj.CallCommand("GetImages", inXML);

                    if (outImgXML.IndexOf("<error>") < 0)
                    {
                        XmlImages.LoadXml(outImgXML);

                        XmlNode imgbytes = XmlImages.SelectSingleNode("//GetImages/Image");

                        if (imgbytes != null)
                        {
                            if (imgbytes.InnerText != "")
                            {
                                imgArray = imageUtilObj.ConvertBase64ToByteArray(imgbytes.InnerText);
                                // Create a file
                                FileStream newFile = new FileStream(path, FileMode.Create);

                                // Write data to the file
                                newFile.Write(imgArray, 0, imgArray.Length);

                                // Close file
                                newFile.Close();
                                newFile.Dispose();
                                newFile = null;
                                imgArray = null;
                                imgbytes = null;

                            }
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
    }
}
