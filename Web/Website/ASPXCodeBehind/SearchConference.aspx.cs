/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Collections; //FB 2607
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using System.Web.UI.HtmlControls; //Added for FB 1415,1416,1417,1418
using System.Text;


/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_SearchConference
{
    public partial class SearchConference : System.Web.UI.Page
    {

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.RadioButtonList rdRoomOption;
        protected System.Web.UI.WebControls.RadioButtonList rdDateOption;
        protected System.Web.UI.WebControls.DataGrid dgScheduledSearches;
        protected System.Web.UI.WebControls.Label lblNoSearchTemplates;
        protected System.Web.UI.WebControls.Label lblSearch;
        protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;
        protected System.Web.UI.WebControls.TreeView treeRoomSelection;
        protected System.Web.UI.WebControls.TextBox txtSearchTemplateID;
        protected System.Web.UI.WebControls.TextBox txtSearchTemplateName;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqName;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqFrom;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqTo;
        protected System.Web.UI.WebControls.TextBox txtDateFrom;
        protected System.Web.UI.WebControls.TextBox txtDateTo;
        protected System.Web.UI.WebControls.TextBox txtConferenceName;
        protected System.Web.UI.WebControls.TextBox txtHost;
        protected System.Web.UI.WebControls.TextBox txtParticipant;
        protected System.Web.UI.WebControls.TextBox txtConferenceUniqueID;      

        protected System.Web.UI.WebControls.RadioButtonList rdStatus;
        protected System.Web.UI.WebControls.RadioButtonList rdPublic;
        protected System.Web.UI.WebControls.RadioButtonList rdSelView;
        protected System.Web.UI.WebControls.Panel pnlLevelView;
        protected System.Web.UI.WebControls.Panel pnlListView;

        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSaveSearch;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;//Added for FB 1415,1416,1417,1418 
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox selectAllCheckBox;//Added for FB 1415,1416,1417,1418 
        protected System.Web.UI.WebControls.Panel pnlNoData;//Added for FB 1415,1416,1417,1418
        protected System.Web.UI.HtmlControls.HtmlInputButton btnCompare;//Added for FB 1415,1416,1417,1418

        //custom attribute fixes -- Start
        protected System.Web.UI.WebControls.Table tblCustomAttribute;  
        private string custControlIDs = ""; 
        myVRMNet.CustomAttributes CAObj = null;
        private bool isSearchTemplate = false;
        //custom attribute fixes -- End

        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        ns_Logger.Logger log;
        //Code added by Offshore for FB Issue 1073 -- Start
        protected String format = "";
        //Code added by Offshore for FB Issue 1073 -- End

        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;//Room Search
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;//Room Search

        private string custOrgXML = ""; //FB 2607
        
        //FB 2632 - Starts
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOCOperator;     
        protected System.Web.UI.WebControls.TextBox txtVNOCOperator;
        protected System.Web.UI.WebControls.TextBox hdnVNOCOperator;
        //FB 2632 - End
        //FB 2670
        protected System.Web.UI.WebControls.RadioButton radAnd;
        protected System.Web.UI.WebControls.RadioButton radOr;

        //FB 2670 START
        protected System.Web.UI.HtmlControls.HtmlTableCell tdonSiteAV;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDedicatedVNOC;
        protected System.Web.UI.HtmlControls.HtmlImage imgVNOC; //FB 2608
        protected System.Web.UI.HtmlControls.HtmlImage imgdeleteVNOC; //FB 2608
        protected System.Web.UI.HtmlControls.HtmlTableRow trConcierge;
        protected System.Web.UI.HtmlControls.HtmlTableRow tdandor;
        //FB 2670 END
        
        //FB 2670 END
		//FB 2694
        protected System.Web.UI.WebControls.CheckBox chkHotdesk; 
        protected System.Web.UI.WebControls.TextBox txtFirstName;
        protected System.Web.UI.WebControls.TextBox txtLastName;
        protected System.Web.UI.WebControls.TextBox txtEmail;
		//FB 2728 Starts
        protected System.Web.UI.WebControls.CheckBox chkAllSilo; 
        protected System.Web.UI.HtmlControls.HtmlTableCell tdchkSilo;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOrgName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChkSilo;
        protected System.Web.UI.WebControls.RadioButton radPending;
        protected System.Web.UI.WebControls.RadioButton radAssigned;
        protected System.Web.UI.WebControls.RadioButton radAll;
        //FB 2728 Ends
        protected System.Web.UI.WebControls.TextBox txtNumericID;//FB 2870

        protected System.Web.UI.HtmlControls.HtmlTableRow trHotdeskingSearch;//FB 2968
        protected System.Web.UI.HtmlControls.HtmlTableRow trchkHotdesk;//FB 2968
        protected System.Web.UI.WebControls.CheckBox chkDeleted;   //FB 2942
        public SearchConference()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger();
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("SearchConferenceInputParameters.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                int Cngsprt = 0; //FB 2670
                errLabel.Visible = false;
                if (Session["roomExpandLevel"] == null)
                    Session["roomExpandLevel"] = "1";
                Session["multisiloOrganizationID"] = null; //FB 2766
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                //Code added by Offshore for FB Issue 1073 -- End

                txtVNOCOperator.Attributes.Add("readonly", ""); //FB 2501//FB 2670

                //FB 2670 START
                string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();

                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0")
                    trConcierge.Visible = false;
                
                if (EnableOnsiteAV != "1")
                    tdonSiteAV.Visible = false;
                else
                    Cngsprt = Cngsprt + 1;

                if (EnableMeetandGreet != "1")
                    tdMeetandGreet.Visible = false;
                else
                    Cngsprt = Cngsprt + 1;

                if (EnableConciergeMonitoring != "1")
                    tdConciergeMonitoring.Visible = false;
                else
                    Cngsprt = Cngsprt + 1;

                if (EnableDedicatedVNOC != "1")
                {
                    tdDedicatedVNOC.Visible = false;
                    txtVNOCOperator.Visible = false;
                    imgVNOC.Visible = false;
                    imgdeleteVNOC.Visible = false;
                }
                else
                    Cngsprt = Cngsprt + 1;
              
                if (Cngsprt > 1)
                    tdandor.Visible = true;
                else
                    tdandor.Visible = false;
                //FB 2670 END
                //FB 2728 Starts
                if (Session["UsrCrossAccess"] != null)
                {
                    if (Session["UsrCrossAccess"].ToString().Equals("1") && Session["organizationID"].ToString() == "11" && Session["OrganizationsLimit"].ToString().Trim() != "1")
                    {
                        chkAllSilo.Visible = true;
                        tdchkSilo.Visible = true;
                    }
                    else
                    {
                        chkAllSilo.Visible = false;
                        tdchkSilo.Visible = false;
                    }
                }
                //Fb 2728 Ends
                //FB 2968 START
                if (Session["isExpressUser"] != null)
                {
                    if (Session["isExpressUser"].ToString() == "1")
                    {
                        trHotdeskingSearch.Visible = false;
                        trchkHotdesk.Visible = false;
                    }
                    else
                    {
                        trHotdeskingSearch.Visible = true;
                        trchkHotdesk.Visible = true;
                    }
                }
                //FB 2968 END
                if (!IsPostBack)
                {
                    BindData();
                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                   
                }
                for (int i = 0; i < rdRoomOption.Items.Count; i++)
                    rdRoomOption.Items[i].Attributes.Add("onclick", "javascript:changeRoomSelection('" + rdRoomOption.Items[i].Value + "');");
                for (int i = 0; i < rdDateOption.Items.Count; i++)
                    rdDateOption.Items[i].Attributes.Add("onclick", "javascript:changeDateSelection('" + rdDateOption.Items[i].Value + "');");

                //Added for FB 1415,1416,1417,1418-- Start
                if (treeRoomSelection.Nodes.Count == 0 && lstRoomSelection.Items.Count == 0)
                {
                    rdSelView.Enabled = false;
                    pnlNoData.Visible = true;
                    pnlListView.Visible = false;
                    pnlLevelView.Visible = false;
                    btnCompare.Disabled = true;
                }
                //Added for FB 1415,1416,1417,1418-- End
                //FB 2607 start
                    if (Session["CustomAttrs"] != null)
                        custOrgXML = Session["CustomAttrs"].ToString();
                    
                    FillCustomAttributeTable();
                //FB 2607 end
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }

        }

        protected void BindData()
        {
            try
            {
                //FB 2027 Start
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                String outXML = obj.CallMyVRMServer("GetSearchTemplateList", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 End
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//getSearchTemplateList/searchTemplates/searchTemplate");
                LoadTemplatesList(nodes);
                //FB 2027 Start
                StringBuilder inXML1 = new StringBuilder();
                inXML1.Append("<login>");
                inXML1.Append(obj.OrgXMLElement());
                inXML1.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML1.Append("</login>");
                log.Trace(inXML1.ToString());
                outXML = obj.CallMyVRMServer("GetSearchConference", inXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                //FB 2027 End
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                
                //FB 2607 start - filling session with cutom attributes from Custom Attributes Master Table
                custOrgXML = "";
                XmlNode custNode = xmldoc.SelectSingleNode("//CustomAttributesList");
                if (custNode != null)
                    custOrgXML = custNode.OuterXml.ToString();

                Session.Remove("CustomAttrs");
                Session.Add("CustomAttrs", custOrgXML);

                //Custom Attribute Fix --End
                //FB 2607 ... End
                
                nodes = xmldoc.SelectNodes("//conference/locationList/level3List/level3");  //Custom Attribute Fixes
                XmlNodeList selNodes = xmldoc.SelectNodes("//locationList/selected");
                String topNode = "You have no rooms available";
                if (nodes.Count > 0)
                {
                  
                    topNode = "Conference Rooms";
                    //Added for FB 1428 Start
                    if (Application["Client"].ToString() != null)
                    {
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            topNode = "Hearing Rooms";

                    }
                    //Added for FB 1428 Start
                   
                }
            
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }

        protected void LoadRooms(Object sender, EventArgs e)
        {
            try
            {
                if (treeRoomSelection.Nodes.Count.Equals(0) && rdRoomOption.SelectedValue.Equals("2"))
                {
                    String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";//Organization Module Fixes
                    String outXML = obj.CallMyVRMServer("GetSearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//conference/locationList/level3List/level3");   //Custom Attribute Fixes
                    XmlNodeList selNodes = xmldoc.SelectNodes("//locationList/selected");
                    String topNode = "You have no rooms available";
                    if (nodes.Count > 0)
                    {
                        topNode = "Conference Rooms";
                        //Added for FB 1428 Start
                        if (Application["Client"].ToString() != null)
                        {
                            if (Application["Client"].ToString().ToUpper() == "MOJ")
                                topNode = "Hearing Rooms";

                        }
                        //Added for FB 1428 Start
                        GenerateLocationList(nodes, selNodes, topNode);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        protected void LoadTemplatesList(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    dgScheduledSearches.DataSource = dt;
                    dgScheduledSearches.DataBind();
                }
                else
                    lblNoSearchTemplates.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadTemplatesList:" + ex.Message);//ZD 100263
            }
        }
        protected void GenerateLocationList(XmlNodeList nodes, XmlNodeList selNodes, String topNode)
        {
            try
            {
                TreeNode tnTop = new TreeNode(topNode, "0");
                tnTop.SelectAction = TreeNodeSelectAction.None;
                foreach (XmlNode node3 in nodes)
                {
                    if (node3.SelectNodes("level2List/level2/level1List/level1").Count > 0)
                    {
                        TreeNode tn3 = new TreeNode(node3.SelectSingleNode("level3Name").InnerText, node3.SelectSingleNode("level3ID").InnerText);
                        tnTop.ChildNodes.Add(tn3);
                        XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");
                        tn3.SelectAction = TreeNodeSelectAction.None;

                        tn3.Expanded = false;
                        if (Session["roomExpandLevel"].ToString() != "")
                        {
                            if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))
                            {
                                if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 2)
                                    tn3.Expanded = true;
                            }
                        }
                         
                        foreach (XmlNode node2 in nodes2)
                        {
                            TreeNode tn2 = new TreeNode(node2.SelectSingleNode("level2Name").InnerText, node2.SelectSingleNode("level2ID").InnerText);
                            tn2.SelectAction = TreeNodeSelectAction.None;
                            if (node2.SelectNodes("level1List/level1").Count > 0)
                            {
                                tn3.ChildNodes.Add(tn2);
                                XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                foreach (XmlNode node1 in nodes1)
                                {
                                    TreeNode tn1 = new TreeNode(node1.SelectSingleNode("level1Name").InnerText, node1.SelectSingleNode("level1ID").InnerText);
                                    //code changed for FB 1415,1416,1417,1418 -- start
                                    tn1.ToolTip = node1.SelectSingleNode("level1ID").InnerText;
                                    tn1.Value = node1.SelectSingleNode("level1ID").InnerText;
                                    //code changed for FB 1415,1416,1417,1418 -- end
                                    tn1.NavigateUrl = @"javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');";

                                    foreach (XmlNode selNode in selNodes)
                                        if (node1.SelectSingleNode("level1ID").InnerText.Equals(selNode.Value))
                                            tn1.Checked = true;
                                    tn2.ChildNodes.Add(tn1);

                                    tn2.Expanded = false;
                                    if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                                    {
                                        if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))
                                        {
                                            if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 3)
                                                tn2.Expanded = true;
                                        }
                                    }
                                            
                                    //code changed for FB 1415,1416,1417,1418 -- start
                                    string l1Name = "<a href='#'  title='" + node1.SelectSingleNode("level1ID").InnerText + "'  onclick='javascript:chkresource(\"" + node1.SelectSingleNode("level1ID").InnerText + "\");'>" + node1.SelectSingleNode("level1Name").InnerText + "</a>";
                                    //code changed for FB 1415,1416,1417,1418 -- end
                                    ListItem li = new ListItem(l1Name, node1.SelectSingleNode("level1ID").InnerText);
                                    li.Attributes.Add("title", node1.SelectSingleNode("level1ID").InnerText);
                                    lstRoomSelection.Items.Add(li);
                                }
                            }
                        }
                    }
                }
                treeRoomSelection.Nodes.Add(tnTop);
                //FB Case 1056 - Saima starts here 
                for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                    for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                        if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                        {
                            ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                            lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                            log.Trace(i + " : " + lstRoomSelection.Items[i].Text.Substring(lstRoomSelection.Items[i].Text.IndexOf(">")) + " : " + lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[j].Value = liTemp.Value;
                            lstRoomSelection.Items[j].Text = liTemp.Text;
                        }
                //FB Case 1056 - Saima ends here 

                //fogbugz case 466: Saima starts here
                if (Session["RoomListView"].ToString().ToUpper().Equals("LIST"))
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
                //fogbugz case 466: Saima ends here
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GenerateLocationList:" + ex.Message);//ZD 100263
            }
        }
        protected void SaveSearch(Object sender, EventArgs e)
        {
            try
            {
                if (txtSearchTemplateName.Visible.Equals(true))
                {
                    SaveSearchTemplate();
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SaveSearch:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void SaveSearchTemplate()
        {
            try
            {
                isSearchTemplate = false; //FB 2607
                String ConfStatus = "0";
                if (rdStatus.SelectedValue.Equals("1"))
                    ConfStatus = "1";
                if (rdStatus.SelectedValue.Equals("2"))
                    ConfStatus = "";
                String ConfPublic = "1";
                if (rdPublic.SelectedValue.Equals("0"))
                    ConfPublic = "";
                if (rdStatus.SelectedValue.Equals("2"))
                    ConfStatus = "0";

                String selRooms = "";
                //Code added for room search
                if (selectedloc.Value != "")
                {
                    String[] selectLoc = selectedloc.Value.Split(',');
                    for (Int32 i = 0; i < selectLoc.Length; i++)
                    {
                        selRooms += "<Selected>" + selectLoc[i] + "</Selected>";
                    }
                }
                String inXML = "<SetSearchTemplate>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <login>" + Session["userID"].ToString() + "</login>";
                inXML += "  <TemplateID>" + txtSearchTemplateID.Text + "</TemplateID>"; // <!-- new OR actual ID of existing template -->
                inXML += "  <TemplateName>" + txtSearchTemplateName.Text + "</TemplateName>";
                inXML += GetSearchInxml();
                inXML += "</SetSearchTemplate>";
                log.Trace("SetSearchTemplate: " + inXML);
                String outXML = obj.CallMyVRMServer("SetSearchTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("SearchConferenceInputParameters.aspx?m=1");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SaveSearchTemplate:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }

        }
        protected void Reset(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("SearchConferenceInputParameters.aspx");
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Reset:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void SaveAndLink(Object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SaveAndLink:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void SubmitSearch(Object sender, EventArgs e)
        {
            try
            {
                String inXML = GetSearchInxml();
                Session.Add("SearchType", "1"); //1 means Search
                Session.Add("inXML", inXML);
                if (chkHotdesk.Checked) //FB 2694 
                    Response.Redirect("HDConferenceList.aspx?t=1&frm=1",false); //FB 2763
                else
                    Response.Redirect("ConferenceList.aspx?t=1&frm=1"); //FB 2763
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SubmitSearch:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected String GetSearchInxml()
        {
            int v = 0; //FB 2670
            //FB 2728  Starts
            objInXML.isAllSilo = false;

            if (Session["UsrCrossAccess"] != null)
            {
                if (chkAllSilo.Checked && Session["organizationID"].ToString() == "11" && Session["UsrCrossAccess"].ToString() == "1")
                    objInXML.isAllSilo = true;
            }
            //FB 2728 ends
            try
            {
                String selLoc = "";
                String ConfStatus = rdStatus.SelectedValue.ToString();
                switch (rdStatus.SelectedValue.ToString())
                {
                    case "0":
                        ConfStatus = ns_MyVRMNet.vrmConfStatus.Completed + "," + ns_MyVRMNet.vrmConfStatus.Ongoing + "," + ns_MyVRMNet.vrmConfStatus.OnMCU + "," + ns_MyVRMNet.vrmConfStatus.Pending + "," + ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.Terminated;
                        break;
                    case "1":
                        ConfStatus = ns_MyVRMNet.vrmConfStatus.Pending;
                        break;
                    case "2":
                        ConfStatus = ns_MyVRMNet.vrmConfStatus.Completed + "," + ns_MyVRMNet.vrmConfStatus.Ongoing + "," + ns_MyVRMNet.vrmConfStatus.OnMCU + "," + ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.Terminated;
                        break;
                    default:
                        ConfStatus = "";
                        break;
                }
                if (chkDeleted.Checked)
                    ConfStatus += "," + ns_MyVRMNet.vrmConfStatus.Deleted; //FB 1942

                String ConfPublic = "";
                switch (rdPublic.SelectedValue.ToString())
                {
                    case "1":
                        ConfPublic = "1";
                        break;
                    case "2":
                        ConfPublic = "0";
                        break;
                    default:
                        ConfPublic = "";
                        break;
                }
                String ConfRoom = rdRoomOption.SelectedValue.ToString();
                if (ConfRoom.Equals("2"))
                {
                    //code changed for FB 1415,1416,1417,1418 -- start
                    //if (treeRoomSelection.CheckedNodes.Count > 0)
                    //    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    //        if (tn.Depth.Equals(3))
                    //            selLoc += "<Selected>" + tn.Value + "</Selected>";

                    if (selectedloc.Value != "")
                    {
                        String[] selectLoc = selectedloc.Value.Split(',');
                        for (Int32 i = 0; i < selectLoc.Length; i++)
                        {
                            if(selLoc == "")
                                selLoc = "<Selected>" + selectLoc[i] + "</Selected>";
                            else
                                selLoc += "<Selected>" + selectLoc[i] + "</Selected>";
                        }                        
                    }
                    //code changed for FB 1415,1416,1417,1418 -- end
                }


                //FB 3006 starts

                StringBuilder AllSiloInXML = new StringBuilder();
                if (chkAllSilo.Checked)
                    AllSiloInXML.Append("<isAllSilo>1</isAllSilo>");
                else
                    AllSiloInXML.Append("<isAllSilo>0</isAllSilo>");

                //FB 3006 Ends

                //FB 2632 - Starts //FB 2670 START
                StringBuilder ConciergeInXML = new StringBuilder();
                if (chkOnSiteAVSupport.Checked)
                    ConciergeInXML.Append("<OnSiteAVSupport>1</OnSiteAVSupport>");
                else
                    ConciergeInXML.Append("<OnSiteAVSupport>0</OnSiteAVSupport>");

                if (chkMeetandGreet.Checked)
                    ConciergeInXML.Append("<MeetandGreet>1</MeetandGreet>");
                else
                    ConciergeInXML.Append("<MeetandGreet>0</MeetandGreet>");

                if (chkConciergeMonitoring.Checked)
                    ConciergeInXML.Append("<ConciergeMonitoring>1</ConciergeMonitoring>");
                else
                    ConciergeInXML.Append("<ConciergeMonitoring>0</ConciergeMonitoring>");
                
                if (hdnVNOCOperator.Text.Trim() != "" && txtVNOCOperator.Text.Trim() != "")
                {
                    string[] VNOCIDs = hdnVNOCOperator.Text.Split(',');
                    string[] VNOCName = txtVNOCOperator.Text.Split('\n');
                    ConciergeInXML.Append("<DedicatedVNOCOperator>1</DedicatedVNOCOperator>");
                    ConciergeInXML.Append("<VNOCAssignAdminID>" + Session["userID"].ToString() + "</VNOCAssignAdminID>");
                    ConciergeInXML.Append("<ConfVNOCOperators>");
                    for (v = 0; v < VNOCIDs.Length; v++)
                        if (VNOCIDs[v] != "")
                        {
                            ConciergeInXML.Append("<VNOCOperatorID>" + VNOCIDs[v] + "</VNOCOperatorID>");
                            ConciergeInXML.Append("<VNOCOperator>" + VNOCName[v] + "</VNOCOperator>");
                        }
                    ConciergeInXML.Append("</ConfVNOCOperators>");
                }
                else
                {
                    ConciergeInXML.Append("<DedicatedVNOCOperator>0</DedicatedVNOCOperator>");
                    ConciergeInXML.Append("<VNOCAssignAdminID>0</VNOCAssignAdminID>");
                    ConciergeInXML.Append("<ConfVNOCOperators></ConfVNOCOperators>");
                }
                //FB 2670 END
                if (radOr.Checked)
                    ConciergeInXML.Append("<ConciergeCondtion>1</ConciergeCondtion>");
                else
                    ConciergeInXML.Append("<ConciergeCondtion>0</ConciergeCondtion>");
                //FB 2632 - End
				//FB 2694 Starts
                StringBuilder strHotdesking = new StringBuilder();

                if (chkHotdesk.Checked)
                {
                    strHotdesking.Append("<FirstName>" + txtFirstName.Text + "</FirstName>");
                    strHotdesking.Append("<LastName>" + txtLastName.Text + "</LastName>");
                    strHotdesking.Append("<Email>" + txtEmail.Text + "</Email>");
                    strHotdesking.Append("<SearchType>H</SearchType>");
                }
                else
                    strHotdesking.Append("<LastName/><FirstName/><Email/><SearchType/>");
				//FB 2694 End
				//FB 2729 Start
                //0- All, 1- Assigned 2 -Pending
                if (radAssigned.Checked)
                    ConciergeInXML.Append("<ConfVNOCStatus>1</ConfVNOCStatus>");
                else if(radPending.Checked)
                    ConciergeInXML.Append("<ConfVNOCStatus>2</ConfVNOCStatus>");
                else
                    ConciergeInXML.Append("<ConfVNOCStatus>0</ConfVNOCStatus>");
                //FB 2729 Ends
                String customInxml = "";
                //code added for custom attribute fixes - start
                isSearchTemplate = false; //FB 2607 - custom option values will be saved
                    if (CAObj == null)
                        CAObj = new myVRMNet.CustomAttributes();
                    //Corrected codes during 2377 FB 2501 VNOC
                    if (!isSearchTemplate)
                    {
                        customInxml = CAObj.CustomAttributeInxml(custControlIDs, tblCustomAttribute); 
                    }
                    //Corrected codes during FB 2501 VNOC
              
                //code added for custom attribute fixes - end
                
                //Code Changed by offshore FB Issue 1073 -- Start
                //String inXML = objInXML.SearchConference(Session["userID"].ToString(), ConfStatus, txtConferenceName.Text, "", txtConferenceUniqueID.Text, rdDateOption.SelectedValue.ToString(), txtDateFrom.Text, txtDateTo.Text, txtHost.Text, txtParticipant.Text, ConfPublic, ConfRoom, selLoc, "1", "3", "0");
                    String inXML = objInXML.SearchConference(Session["userID"].ToString(), ConfStatus, txtConferenceName.Text, "", txtConferenceUniqueID.Text, txtNumericID.Text, rdDateOption.SelectedValue.ToString(), myVRMNet.NETFunctions.GetDefaultDate(txtDateFrom.Text), myVRMNet.NETFunctions.GetDefaultDate(txtDateTo.Text), txtHost.Text, txtParticipant.Text, ConfPublic, ConfRoom, selLoc, "1", "3", "0", customInxml, ConciergeInXML.ToString(), strHotdesking.ToString(), "0", AllSiloInXML.ToString(), chkDeleted.Checked); //Custom Attribute Fix //FB 2632 //FB 2694 //FB 2822 //FB 2870 //FB 3006 //FB 1942
                //Code Changed by offshore FB Issue 1073 -- End
                
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("GetSearchInXML: " + ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }

        protected void EditSearchTemplate(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                XmlNodeList ConfVNOCnodes; //FB 2670
                log.Trace("in Search Template");
                lblSearch.Text = obj.GetTranslatedText("Update Search Parameters for Template: "); // +e.Item.Cells[1].Text;
                String inXML = "<GetSearchTemplate>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "<TemplateID>" + e.Item.Cells[0].Text + "</TemplateID>";
                inXML += "</GetSearchTemplate>";
                String outXML = obj.CallMyVRMServer("GetSearchTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace(outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                trSaveSearch.Attributes.Add("style", "display:");
                txtSearchTemplateID.Attributes.Add("style", "display:none");
                if (outXML.IndexOf("<error>") < 0)
                {
                    txtSearchTemplateName.Visible = true;
                    btnSubmit.Enabled = false; //FB Case 964 Saima
                    txtSearchTemplateID.Text = e.Item.Cells[0].Text;
                    txtSearchTemplateName.Text = xmldoc.SelectSingleNode("//SearchTemplate/TemplateName").InnerText;
                    if (xmldoc.SelectNodes("//SearchTemplate/confSearch/confInfo").Count > 0)
                    {
                        txtConferenceName.Text = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confName").InnerText;
                        txtConferenceUniqueID.Text = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confUniqueID").InnerText;
                        txtNumericID.Text = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/CTSNumericID").InnerText;//FB 2870
                        txtParticipant.Text = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confResource/confParticipant").InnerText;
                        txtHost.Text = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confHost").InnerText;
                        rdDateOption.ClearSelection();
                        rdDateOption.Items.FindByValue(xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confDate/type").InnerText).Selected = true;
                        rdStatus.ClearSelection();
                        rdStatus.Items.FindByValue(xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/pending").InnerText).Selected = true;
                        rdPublic.ClearSelection();
                        rdPublic.Items.FindByValue(xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/public").InnerText).Selected = true;
                        rdRoomOption.ClearSelection();
                        rdRoomOption.Items.FindByValue(xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confResource/confRooms/type").InnerText).Selected = true;
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetDateSelection", "<script language='javascript'>setTimeout(\"changeDateSelection('" + rdDateOption.SelectedValue + "')\",300);</script>");

                        /**** Code added for room search ****/

                        String locndes = "";
                        XmlNodeList locnodes = xmldoc.SelectNodes("//SelectedRooms/Selected");
                        foreach (XmlNode nd in locnodes)
                        {
                            if (locndes == "")
                                locndes = nd.InnerText;
                            else
                                locndes += "," + nd.InnerText;
                        }

                        if (locndes != "")
                        {
                            myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                            locstrname.Value = ntfuncs.RoomDetailsString(locndes);
                            selectedloc.Value = locndes;
                            BindRoomToList();
                            ntfuncs = null;
                        }

                        locnodes = null;


                        /**** Code added for room search ****/

                    }
                    else
                    {
                        txtConferenceName.Text = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConferenceName").InnerText;
                        txtConferenceUniqueID.Text = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConferenceUniqueID").InnerText;
                        txtNumericID.Text = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/CTSNumericID").InnerText;//FB 2870
                        txtParticipant.Text = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConferenceParticipant").InnerText;
                        txtHost.Text = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConferenceHost").InnerText;
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/SearchDeletedConf") != null)//FB 1942
                        {
                            chkDeleted.Checked = false;
                            if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/SearchDeletedConf").InnerText.Equals("1"))
                                chkDeleted.Checked = true;
                        }
                        rdDateOption.ClearSelection();
                        rdDateOption.Items.FindByValue(xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConferenceSearchType").InnerText).Selected = true;
                        rdStatus.ClearSelection();
                        if(xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/StatusFilter/Status") != null)
                            rdStatus.Items.FindByValue(xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/StatusFilter/Status").InnerText).Selected = true;
                        else
                            rdStatus.Items.FindByValue("0").Selected = true;
                        rdPublic.ClearSelection();
                        try
                        {
                            switch (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/Public").InnerText)
                            {
                                case "0":
                                    rdPublic.Items.FindByValue("2").Selected = true;
                                    break;
                                case "1":
                                    rdPublic.Items.FindByValue("1").Selected = true;
                                    break;
                                case "":
                                    rdPublic.Items.FindByValue("0").Selected = true;
                                    break;
                            }

                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        rdRoomOption.ClearSelection();
                        rdRoomOption.Items.FindByValue(xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/Location/SelectionType").InnerText).Selected = true;
                        
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetDateSelection", "<script language='javascript'>setTimeout(\"changeDateSelection('" + rdDateOption.SelectedValue + "')\",300);</script>");

                        /**** Code added for Room search ****/
                        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetRoomSelection", "<script language='javascript'>setTimeout(\"changeRoomSelection('" + rdRoomOption.SelectedValue + "')\",300);</script>");
                        String locndes = "";
                        XmlNodeList locnodes = xmldoc.SelectNodes("//SelectedRooms/Selected");
                        foreach (XmlNode nd in locnodes)
                        {
                            if (locndes == "")
                                locndes = nd.InnerText;
                            else
                                locndes += "," + nd.InnerText;
                        }

                        if (locndes != "")
                        {
                            myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                            locstrname.Value = ntfuncs.RoomDetailsString(locndes);
                            selectedloc.Value = locndes;
                            BindRoomToList();
                            ntfuncs = null;
                        }

                        locnodes = null;
                        /**** Code added for Room search ****/

                        //Added for FB 1419 - Start
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/DateFrom").InnerText != "" && xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/DateTo").InnerText != "")
                        {
                            txtDateFrom.Text = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/DateFrom").InnerText;
                            txtDateTo.Text = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/DateTo").InnerText;
                        }
                        //Added for FB 1419 - End
                    }

                    //FB 2607 start - filling session with cutom attributes from Template on Edit
                    
                    if (Session["CustomAttrs"] != null)
                        custOrgXML = Session["CustomAttrs"].ToString();

                    string custSelXML = "";
                    XmlNode custNode = xmldoc.SelectSingleNode("//CustomAttributesList");
                    if (custNode != null)
                        custSelXML = custNode.OuterXml.ToString();

                    FillTemplateValues(ref custOrgXML, custSelXML);

                    FillCustomAttributeTable();

                    //FB 2607 end
                    //FB 3006 Starts
                    if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/isAllSilo") != null)
                    {
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/isAllSilo").InnerText.Equals("1"))
                            chkAllSilo.Checked = true;
                        else
                            chkAllSilo.Checked = false;
                    }

                    //FB 3006 Ends
                    //FB 2632 Starts
                    if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/OnSiteAVSupport") != null)
                    {
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/OnSiteAVSupport").InnerText.Equals("1"))
                            chkOnSiteAVSupport.Checked = true;
                        else
                            chkOnSiteAVSupport.Checked = false;
                    }

                    if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/MeetandGreet") != null)
                    {
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/MeetandGreet").InnerText.Equals("1"))
                            chkMeetandGreet.Checked = true;
                        else
                            chkMeetandGreet.Checked = false;
                    }
                    if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/ConciergeMonitoring") != null)
                    {
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/ConciergeMonitoring").InnerText.Equals("1"))
                            chkConciergeMonitoring.Checked = true;
                        else
                            chkConciergeMonitoring.Checked = false;
                    }
                    if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/DedicatedVNOCOperator") != null)
                    {
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/DedicatedVNOCOperator").InnerText.Equals("1"))
                            chkDedicatedVNOCOperator.Checked = true;
                        else
                            chkDedicatedVNOCOperator.Checked = false;
                    }

                    //FB 2670
                    radAssigned.Checked = false; radPending.Checked = false; radAll.Checked = false;
                    if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/ConfVNOCStatus") != null)
                    {
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/ConfVNOCStatus").InnerText.Equals("1"))
                            radAssigned.Checked = true;
                        else if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/ConfVNOCStatus").InnerText.Equals("2"))
                            radPending.Checked = true;
                        else
                            radAll.Checked = true;
                    }

                    //FB 2728 Starts
                    radOr.Checked = false; radAnd.Checked = false;
                    if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/ConciergeCondtion") != null)
                    {
                        if (xmldoc.SelectSingleNode("//SearchTemplate/SearchConference/ConciergeSupport/ConciergeCondtion").InnerText.Equals("1"))
                            radOr.Checked = true;
                        else
                            radAnd.Checked = true;
                    }

                    ConfVNOCnodes = xmldoc.SelectNodes("//SearchTemplate/SearchConference/ConciergeSupport/ConfVNOCOperators/VNOCOperatorID");
                    if (ConfVNOCnodes.Count > 0)
                    {
                        hdnVNOCOperator.Text = "";
                        for (int i = 0; i < ConfVNOCnodes.Count; i++)
                        {
                            if (ConfVNOCnodes[i].InnerText != null)
                            {
                                if (hdnVNOCOperator.Text == "")
                                    hdnVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                                else
                                    hdnVNOCOperator.Text += "," + ConfVNOCnodes[i].InnerText.Trim();
                            }
                        }
                    }
                    ConfVNOCnodes = null;
                    ConfVNOCnodes = xmldoc.SelectNodes("//SearchTemplate/SearchConference/ConciergeSupport/ConfVNOCOperators/VNOCOperator");
                    if (ConfVNOCnodes.Count > 0)
                    {
                        txtVNOCOperator.Text = "";
                        for (int i = 0; i < ConfVNOCnodes.Count; i++)
                        {
                            {
                                if (ConfVNOCnodes[i].InnerText != null)
                                {
                                    if (hdnVNOCOperator.Text == "")
                                        txtVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                                    else
                                        txtVNOCOperator.Text += "\n" + ConfVNOCnodes[i].InnerText.Trim();
                                }
                            }
                        }
                    }
                    if (txtVNOCOperator.Text != "")
                        chkDedicatedVNOCOperator.Checked = true;
                    //FB 2501 Ends //FB 2670 END

                    //FB 2632 Ends

                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("EditSearchTemplate:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        
        //FB 2607 Fill custom attrs main xml with selected template values
        #region FillTemplateValues
        /// <summary>
        /// FillTemplateValues
        /// </summary>
        /// <param name="custXML"></param>
        /// <param name="custInxml"></param>
        private void FillTemplateValues(ref string custXML, string custInxml)
        {
            try
            {
                if (custXML != "" && custInxml != "")
                {
                    //All Orginal custom ids
                    XmlDocument xmlDOC = new XmlDocument();
                    xmlDOC.LoadXml(custXML);
                    XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                    XmlNodeList nodes = node.SelectNodes("//CustomAttributesList/CustomAttribute");

                    //Selected custom ids
                    XmlDocument xmlSelDOC = new XmlDocument();
                    xmlSelDOC.LoadXml(custInxml);
                    XmlNode selnode = (XmlNode)xmlSelDOC.DocumentElement;
                    XmlNodeList selnodes = selnode.SelectNodes("//CustomAttributesList/CustomAttribute");
                    XmlNodeList SelOptions = null;
                    Hashtable selNormalValues = new Hashtable();
                    
                    // TextBox-4, Dropdown-6, ListBox-5, Radio-3, CheckBox-2, URL -7, RadioButtonsList -8, Multiline textBox - 10
                    string custId = "", optionVal = "", OptionID = "", optType = "", temp = "";
                    foreach (XmlNode snod in selnodes)
                    {
                        temp = "";
                        custId = snod.SelectSingleNode("CustomAttributeID").InnerText.Trim();
                        optionVal = snod.SelectSingleNode("OptionValue").InnerText.Trim();
                        OptionID = snod.SelectSingleNode("OptionID").InnerText.Trim();
                        optType = snod.SelectSingleNode("Type").InnerText.Trim();

                        if (optType == "8" || optType == "6")
                        {
                            optionVal = OptionID + "<";
                        }
                        else if (optType == "5")
                        {
                            if(selNormalValues.Contains(custId))
                            {
                                temp = selNormalValues[custId].ToString();
                                optionVal = temp + OptionID + "<";
                                selNormalValues[custId] = optionVal;
                            }
                            else
                            {
                                optionVal = OptionID + "<";
                            }
                        }
                        
                        if (!selNormalValues.Contains(custId))
                            selNormalValues.Add(custId, optionVal);
                    }
                    
                    foreach (XmlNode orgnod in nodes)
                    {
                        custId = ""; optType = ""; temp = "";

                        custId = orgnod.SelectSingleNode("CustomAttributeID").InnerText.Trim();
                        optType = orgnod.SelectSingleNode("Type").InnerText.Trim();

                        if (selNormalValues.Contains(custId))
                        {
                            if (optType == "8" || optType == "6" || optType == "5")
                            {
                                temp = selNormalValues[custId].ToString().Trim();

                                if (temp != "")
                                {
                                    string[] optSel = temp.Trim().Split('<');

                                    for (int k = 0; k < optSel.Length; k++)
                                    {
                                        SelOptions = orgnod.SelectNodes("OptionList/Option");
                                        foreach (XmlNode optNode in SelOptions)
                                        {
                                            if (optNode.SelectSingleNode("OptionID").InnerText.Trim() == optSel[k])
                                            {
                                                optNode.SelectSingleNode("Selected").InnerText = "1";
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                orgnod.SelectSingleNode("Selected").InnerText = "1";
                                orgnod.SelectSingleNode("SelectedValue").InnerText = selNormalValues[custId].ToString();
                            }
                        }
                    }
                    custXML = node.OuterXml;
                }
             }
            catch (Exception ex)
            {
               // errLabel.Text = ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("FillTemplateValue:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        protected void SearchConferenceFromTemplate(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //FB 2728  Starts
                objInXML.isAllSilo = false;

                if (Session["UsrCrossAccess"] != null)
                {
                    if (chkAllSilo.Checked && Session["organizationID"].ToString() == "11" && Session["UsrCrossAccess"].ToString() == "1")
                        objInXML.isAllSilo = true;
                }
                //FB 2728 ends
                String inXML = "<GetSearchTemplate>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "<TemplateID>" + e.Item.Cells[0].Text + "</TemplateID>";
                inXML += "</GetSearchTemplate>";
                String outXML = obj.CallMyVRMServer("GetSearchTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                String ConferenceStatus = "0";
                if (xmldoc.SelectNodes("//SearchTemplate/confSearch").Count > 0)
                {
                    if (xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/pending").InnerText.Equals("1"))
                        ConferenceStatus = "1";
                    String ConferenceName = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confName").InnerText;
                    String ConferenceUniqueID = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confUniqueID").InnerText;
                    String CTSNumericID = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/CTSNumericID").InnerText;//FB 2870
                    String ConferenceSearchType = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confDate/type").InnerText;
                    String DateFrom = "";
                    String DateTo = "";
                    String ConferenceHost = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/confHost").InnerText;
                    String ConferenceParticipant = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confResource/confParticipant").InnerText;
                    String Public = "";
                    if (!xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/public").InnerText.Equals("0"))
                        Public = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confInfo/public").InnerText;
                    String SelectionType = xmldoc.SelectSingleNode("//SearchTemplate/confSearch/confResource/confRooms/type").InnerText;
                    String SelectedRooms = "";
                    String PageNo = "1";
                    String SortBy = "3";
                    inXML = objInXML.SearchConference(Session["userID"].ToString(), ConferenceStatus, ConferenceName, "", ConferenceUniqueID, CTSNumericID, ConferenceSearchType, DateFrom, DateTo, ConferenceHost, ConferenceParticipant, Public, SelectionType, SelectedRooms, PageNo, SortBy, "0", "", "", "", "0", "", chkDeleted.Checked);  //Custom Attribute Fix //FB 2632//FB 2694//FB 2822 //FB 2870  //FB 1942
                }
                else
                    inXML = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference").OuterXml;
                Session.Add("SearchType", "1"); //1 means Search
                Session.Add("inXML", inXML);
                Response.Redirect("ConferenceList.aspx?t=1&frm=1"); //FB 2763
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SearchConferenceTemplate:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void DeleteSearchTemplate(Object sender, DataGridCommandEventArgs e)
        {
            try
            {

                String inXML = "";
                inXML += "<DeleteSearchTemplate>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <TemplateID>" + e.Item.Cells[0].Text + "</TemplateID>";
                inXML += "</DeleteSearchTemplate>";
                String outXML = obj.CallMyVRMServer("DeleteSearchTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("SearchConferenceInputParameters.aspx?m=1");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteSearchTemplate:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this template ?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsDeleteMessage:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        //Edited for FB 1415,1416,1417,1418 -- Start

        #region rdSelView_SelectedIndexChanged

        protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
        {
           Int32 cnt = 0;
           Int32 mCnt = 0;
           Int32 tCnt = 0;
           if (rdSelView.SelectedValue.Equals("2"))
           {
               pnlListView.Visible = true;
               pnlLevelView.Visible = false;
               lstRoomSelection.ClearSelection();
               HtmlInputCheckBox selectAll = (HtmlInputCheckBox)FindControl("selectAllCheckBox");

               if (selectedloc.Value.Trim() != "")
               {
                   foreach (ListItem lstItem in lstRoomSelection.Items)
                   {
                       for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length - 1; i++)
                           if (lstItem.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                           {
                               lstItem.Selected = true;
                               cnt = cnt + 1;
                           }
                   }

                   if (selectAll != null)
                   {
                       if (cnt == lstRoomSelection.Items.Count)
                           selectAll.Checked = true;
                       else
                           selectAll.Checked = false;
                   }
               }
               else
               {
                   if (selectAll != null)
                       selectAll.Checked = false;
               }
           }
           else 
           {
               pnlLevelView.Visible = true;
               pnlListView.Visible = false;

               foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
               {
                   tCnt = 0;
                   foreach (TreeNode tnMid in tnTop.ChildNodes)
                   {
                       mCnt = 0;
                       foreach (TreeNode tn in tnMid.ChildNodes)
                       {
                           tn.Checked = false;
                           if (selectedloc.Value.Trim() != "")
                           {
                               for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length - 1; i++)
                                   if (tn.Depth.Equals(3) && tn.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                                   {
                                       tn.Checked = true;
                                       mCnt++;
                                   }
                           }
                       }

                       if (mCnt == tnMid.ChildNodes.Count)
                       {
                           tnMid.Checked = true;
                           tCnt++;
                       }
                       else
                           tnMid.Checked = false;

                   }

                   if (tCnt == tnTop.ChildNodes.Count)
                   {
                       tnTop.Checked = true;
                       treeRoomSelection.Nodes[0].Checked = true;
                   }
                   else
                   {
                       tnTop.Checked = false;
                       treeRoomSelection.Nodes[0].Checked = false;
                   }
               }

           }
        }

        #endregion

        //Edited for FB 1415,1416,1417,1418 -- End


        /* *** Custom Attribute Fixes - start *** */
        #region FillCustomAttributeTable

        private void FillCustomAttributeTable() //FB 2607
        {
            if (custOrgXML != "")
            {
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(custOrgXML); //FB 2607
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                XmlNodeList conciergeNodes = null; //FB 2377 corrected this during FB 2501
                XmlNodeList nodes = node.SelectNodes("//CustomAttributesList/CustomAttribute");
                if (nodes.Count > 0)
                {
                    if (CAObj == null)
                        CAObj = new myVRMNet.CustomAttributes();

                    //FB 2377 - Corrected this codes during FB 2501 Vnoc
                    custControlIDs = ""; //FB 2607
                    conciergeNodes = xmlDOC.SelectNodes("descendant::CustomAttribute[Description='Concierge Support']"); //FB 2377
                    custControlIDs = CAObj.CreateCustomAttributes(nodes, tblCustomAttribute, false); 
                }
                else
                {
                    TableCell tCol = new TableCell();
                    tCol.Text = "<br/>" + obj.GetTranslatedText("No Custom Options found.");//FB 1830 - Translation
                    tCol.HorizontalAlign = HorizontalAlign.Center;
                    TableRow tRow = new TableRow();
                    tRow.Cells.Add(tCol);
                    tRow.Visible = true;
                    tblCustomAttribute.Rows.Add(tRow);
                }
            }
        }
        #endregion
        /* *** Custom Attribute Fixes - end *** */

        public void BindRoomToList()
        {
            String[] locsName = null;
            try
            {
                if (locstrname.Value != "")
                {

                    locsName = locstrname.Value.Split('+');
                    RoomList.Items.Clear();
                    foreach (String s in locsName)
                    {
                        if (s != "")
                        {

                            if (s.Split('|').Length > 1)
                                RoomList.Items.Add(new ListItem(s.Split('|')[1], s.Split('|')[0]));
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }
    }
}