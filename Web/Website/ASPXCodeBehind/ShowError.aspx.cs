/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace en_ShowError
{
    public partial class ShowError : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        #region Private Data Members

        protected String paramWinType = "";
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] == null)
                Response.Redirect("thankyou.aspx");

            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower()); // ZD 100263

        }

        #endregion

        #region Set QueryString

        private void SetQueryString()
        {
            if (Request.QueryString["winType"] != null)
                paramWinType = Request.QueryString["winType"].ToString();

        }

        #endregion
    }
}
