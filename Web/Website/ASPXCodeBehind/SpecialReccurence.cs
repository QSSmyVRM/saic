/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using System.Web;
using System.Xml.Linq;
using System.Text;

namespace MyVRMNet
{
    public class SpecialReccurence
    {
        myVRMNet.DateUtilities dtObj = new myVRMNet.DateUtilities();
        private DateTime endDate = DateTime.Now;
        private DateTime strtDate = DateTime.Now;
        private int numInstncs = 0;
        private int endType = 0;
        public string recurString = "";
        private int holidayType = 0;
        private int recurType = 0;
        private int dayType = 0;
        public string recurXML = "";
        bool isDtremainMonth = false; //FB 2052
        Boolean instances = false;
        Boolean enddate = false;
        int counter = 0;
        int fltDatMonth = -1;
        myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions(); //FB 2272 Spanish
        #region Get SpecialReccurence 
        /// <summary>
        /// SpecialReccurence
        /// </summary>
        public void GetSpecialReccurence()
        {
            try
            {
                if (HttpContext.Current.Session["Holidays"] == null)
                    throw new Exception("Special reccurence failed : Holidays not defined");

                if (HttpContext.Current.Session["Holidays"].ToString().Trim() == "")
                    throw new Exception("Special reccurence failed : Holidays not defined");

                GetDataFromString();
                FilterOutDates();
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        #endregion

        #region Get recur params from string 

        private void GetDataFromString()
        {
            try
            {
                if (recurString != "")
                {
                    string[] main = recurString.Split('#');
                    holidayType = int.Parse(main[1].Split('&')[0]);
                    recurType = int.Parse(main[1].Split('&')[1]);
                    dayType = int.Parse(main[1].Split('&')[2]);
                    endType = int.Parse(main[2].Split('&')[1]);
                    strtDate = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(main[2].Split('&')[0]));

                    switch (endType)
                    {
                        case 1:
                            endDate = strtDate.AddYears(2);
                            enddate = true;
                            break;
                        case 2:
                            numInstncs = int.Parse(main[2].Split('&')[2]);
                            instances = true;
                            break;

                        case 3:
                            endDate = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(main[2].Split('&')[3]));
                            enddate = true;
                            break;
                        default:
                            break;
                    }
                }


            }
            catch (Exception)
            {


                throw new Exception("Special reccurence failed : Reccurence string is not in right format");
            }

        }

        #endregion

        #region Get Dates 

        #region Get recur params from string 

        private void FilterOutDates()
        {
            XElement root = null;
            IEnumerable<XElement> hldys = null;
            try
            {
                if (HttpContext.Current.Session["Holidays"] != null)
                {
                    if (HttpContext.Current.Session["Holidays"].ToString().Trim() != "")
                    {
                        root = XElement.Parse(HttpContext.Current.Session["Holidays"].ToString());

                        hldys =
                            from hldyelmnts in root.Elements("Holiday")
                            where (DateTime)hldyelmnts.Element("Date") >= strtDate
                                  && (int)hldyelmnts.Element("HolidayType") == holidayType
                            orderby (DateTime)hldyelmnts.Element("Date")
                            select hldyelmnts;

                        switch (recurType)
                        {
                            /*
                            Scenario 1:
                               Ability to define a pattern that will select every given weekday of a given color for X number of instances (less than 104) or up to given date.
                            For example, create a recurrence selecting every Blue Monday for next 12 weeks.
	
                            Scenario 2:
                               Ability to define a pattern that will select every other given weekday of a given color for X number of instances (less than 104) or up to given date .
                            For example , create a recurrence selecting every other Orange Tuesday up to 12/31/2012.
	
                            Scenario 3:
                              Ability to define a pattern that will select every given first weekday of the month of a given color for X number of instances (less than 24) or up to given date.
                            For example, create a recurrence selecting every first Blue Monday of every month for next 24 weeks.
    	
                            Scenario 4:
                              Ability to define a pattern that will select every given last weekday of the month of a given color for X number of instances (less than 24) or up to given date.
                            For example , creating a pattern selecting every last Blue Friday of every month for next 24 month
	
                            Scenario 5:
                              Ability to define a pattern that will select every given first color (weekday) of the month for X number of instances (less than 24) or up to given date.
                            For example, creating a pattern that will select every first Blue day of a month for the next 12 months
                            
                           Scenario 6:
                             Ability to define a pattern that will select every given last color (weekday) of the month for X number of instances (less than 24) or up to given date.
                           For example, creating a pattern that will select every last Orange day of a month for the next 12 months
                             
                             Scenario 7:
                              Ability to define a pattern that will select every given second weekday of the month of a given color for X number of instances (less than 24) or up to given date.
                            For example, create a recurrence selecting every second Blue Monday of every month for next 24 weeks.
                             
                            
                           */
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5://FB 1911
                                {
                                    hldys =
                                    from hldyelmnts in hldys
                                    where (int)((DateTime)hldyelmnts.Element("Date")).DayOfWeek == dayType
                                    orderby (DateTime)hldyelmnts.Element("Date")
                                    select hldyelmnts;
                                    break;
                                }
                            case 6:
                            case 7:
                            case 8://FB 1911
                                {
                                    break;
                                }
                        }

                        if (enddate)
                        {
                            IEnumerable<XElement> remainHldys =
                            from hldyelmnts in hldys
                            where (DateTime)hldyelmnts.Element("Date") > endDate
                            && (DateTime)hldyelmnts.Element("Date") < dtObj.GetEndOfMonth(endDate.Month, endDate.Year)
                            orderby (DateTime)hldyelmnts.Element("Date")
                            select hldyelmnts;
                            if (remainHldys != null)
                                if (remainHldys.Count() > 0)
                                    isDtremainMonth = true;


                            hldys =
                            from hldyelmnts in hldys
                            where (DateTime)hldyelmnts.Element("Date") <= endDate
                            orderby (DateTime)hldyelmnts.Element("Date")
                            select hldyelmnts;
                        }

                        GetRecurXMl(hldys);
                    }
                }
            }
            catch (Exception ex)//FB 2052
            {
                if (ex.Message.Trim() != "")
                    throw ex;
                else
                    throw new Exception("Special reccurence failed : Getting Dates");
            }
        }

        #endregion

		//FB 1911
        #region Get recur params from string

        private Int32 FilterFirstDates()
        {
            XElement root = null;
            IEnumerable<XElement> hldys = null;
            bool bRet = true;
            Int32 i = -1;
            try
            {
                if (HttpContext.Current.Session["Holidays"] != null)
                {
                    if (HttpContext.Current.Session["Holidays"].ToString().Trim() != "")
                    {
                        root = XElement.Parse(HttpContext.Current.Session["Holidays"].ToString());

                        hldys =
                            from hldyelmnts in root.Elements("Holiday")
                            where (DateTime)hldyelmnts.Element("Date") < strtDate
                                  && (DateTime)hldyelmnts.Element("Date") >= new DateTime(strtDate.Year, strtDate.Month, 1)
                                  && (int)hldyelmnts.Element("HolidayType") == holidayType
                            orderby (DateTime)hldyelmnts.Element("Date")
                            select hldyelmnts;

                        if (recurType == 3) //FB 2052 (|| recurType == 7)
                        {
                            hldys =
                            from hldyelmnts in hldys
                            where (int)((DateTime)hldyelmnts.Element("Date")).DayOfWeek == dayType
                            orderby (DateTime)hldyelmnts.Element("Date")
                            select hldyelmnts;
                           
                        }


                        foreach (XElement elmnts in hldys)
                        {
                            if (i < 0)
                                i = 1;
                            else
                                i++;

                            if (recurType < 7)
                            {
                                break;
                            }

                            if ((recurType >= 7) && i == 2)
                            {
                                fltDatMonth = ((DateTime)elmnts.Element("Date")).Month;
                                break;
                            }

                        }


                    }
                }

            }
            catch (Exception)
            {


                throw new Exception("Special reccurence failed : Getting Dates");
            }
            return i;

        }

        #endregion

        //FB 2052 Start
        #region Get recur params from string
        /// <summary>
        /// GetRecurXMl
        /// </summary>
        /// <param name="hldys"></param>
        private void GetRecurXMl(IEnumerable<XElement> hldys)
        {
            List<XElement> prevDates = null;
            int month = -1;
            int year = -1;
            int lclCounter = 0;
            StringBuilder dates = new StringBuilder();
            String date = "";
            int pttrnCounter = 0;//FB 1911
            
            try
            {
                //FB 1911 - Start
                pttrnCounter = FilterFirstDates();

                if (hldys != null)
                {

                    dates.Append("<startDates>");

                    foreach (XElement elmnts in hldys)
                    {
                        lclCounter++;

                        date = (string)elmnts.Element("Date");

                        if (year < 0)
                            year = ((DateTime)elmnts.Element("Date")).Year;


                        if (recurType == 2)
                        {
                            if (lclCounter % 2 == 0)
                                continue;
                        }
                        else if (recurType == 3) //First weekday
                        {
                            if ((int)Math.Ceiling(((DateTime)elmnts.Element("Date")).Day / 7.0) != 1)
                                continue;
                        }
                        else if (recurType == 4) //Second weekday
                        {
                            if ((int)Math.Ceiling(((DateTime)elmnts.Element("Date")).Day / 7.0) != 2)
                                continue;
                        }
                        else if (recurType == 7)
                        {
                            if (month < 0)
                            {
                                if (!(pttrnCounter == 2 && fltDatMonth != ((DateTime)elmnts.Element("Date")).Month))
                                    month = ((DateTime)elmnts.Element("Date")).Month;
                            }       

                            if (month != ((DateTime)elmnts.Element("Date")).Month)
                            {
                                month = ((DateTime)elmnts.Element("Date")).Month;
                                pttrnCounter = 1;
                                continue;
                            }
                            else if (month == ((DateTime)elmnts.Element("Date")).Month)
                            {
                                if (pttrnCounter == -1)
                                    pttrnCounter = 1;
                                else
                                    pttrnCounter++;

                                if (pttrnCounter != 2)
                                    continue;
                            }
                        }
                        else if (recurType == 5)
                        {
                            month = ((DateTime)elmnts.Element("Date")).Month;
                            year = ((DateTime)elmnts.Element("Date")).Year;
                            date = (string)myVRMNet.NETFunctions.GetFormattedDate(date);

                            String tempDate = myVRMNet.NETFunctions.GetFormattedDate(dtObj.LastDateOfWeekForMonth((DayOfWeek)dayType, month, year));
                            if (tempDate != date)
                                continue;

                            date = (string)elmnts.Element("Date");
                        }
                        else if (recurType == 8)
                        {
                            if (month < 0)
                                month = ((DateTime)elmnts.Element("Date")).Month;

                            if (prevDates == null)
                                prevDates = new List<XElement>();

                            if (month == ((DateTime)elmnts.Element("Date")).Month)
                            {
                                prevDates.Add(elmnts);
                                continue;
                            }
                            else
                            {
                                month = ((DateTime)elmnts.Element("Date")).Month;
                                year = ((DateTime)elmnts.Element("Date")).Year;

                                if (prevDates.Count > 0)
                                    date = (string)prevDates[prevDates.Count - 1].Element("Date");

                               prevDates.Add(elmnts);
                            }
                        }
                        else if (recurType == 6)
                        {
                            if (month < 0)
                                if (pttrnCounter > 0)
                                    month = ((DateTime)elmnts.Element("Date")).Month;

                            if (month == ((DateTime)elmnts.Element("Date")).Month)
                                continue;

                            month = ((DateTime)elmnts.Element("Date")).Month;
                            year = ((DateTime)elmnts.Element("Date")).Year;
                        }

                        counter++;

                        if (dates.ToString().IndexOf(date) < 0)
                            dates.Append("<startDate>" + date + "</startDate>");

                        if (instances)
                            if (numInstncs == counter)
                                break;

                        //FB 1911 - End              
                    }

                    if (prevDates !=  null && recurType == 8)
                    {
                        if (prevDates.Count > 0 && ((endType == 2 && counter < numInstncs) || endType != 2) && !isDtremainMonth)
                        {
                            dates.Append("<startDate>" + (string)prevDates[prevDates.Count - 1].Element("Date") + "</startDate>");
                            counter++;
                        }
                    }

                    dates.Append("</startDates>");
                }

                if (endType == 2 && counter < numInstncs)
                {
                    throw new Exception(obj.GetTranslatedText("No of Occurrence Exceeds color dates"));
                }
                else
                    recurXML = dates.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message.Trim() != "")
                    throw ex;
                else
                    throw new Exception(obj.GetTranslatedText("Special reccurence failed : Building reccurence"));
            }
        }
        #endregion
        //FB 2052 End
        #endregion

    }
}
