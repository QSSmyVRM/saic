/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_Thankyou : System.Web.UI.Page
{
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            if (Session["userID"] != null && Session["userID"].ToString() != "")
            {
                String localUserList = Application["OnlineUserList"].ToString();
                String user = Session["userID"].ToString() + ",";

                if (localUserList.Contains(user))
                {
                    String newlocalUserList = localUserList.Replace(user, "");
                    Application.Lock();
                    Application["OnlineUserList"] = newlocalUserList;
                    Application.UnLock();
                }
            }
            
            Session.Contents.RemoveAll();
            Session.Abandon();

            Response.Cookies.Remove("VRMuser");
            
        }
        catch (Exception ex)
        {
            log.Trace("Thankyou PageLoad: " + ex.StackTrace + " : " + ex.Message);
            
        }
    }
}
