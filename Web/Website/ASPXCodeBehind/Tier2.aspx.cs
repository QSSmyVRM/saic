/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Tiers2
{
    public partial class Tiers2 : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        
        protected System.Web.UI.WebControls.Label lblTier1Name;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtNewTier2Name;
        protected System.Web.UI.WebControls.Table tblNoTier2s;
        protected System.Web.UI.WebControls.DataGrid dgTier2s;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNew; //FB 2670
        ns_Logger.Logger log;
        public Tiers2()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageTier2.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                errLabel.Text = "";
                //tblTier2.Visible = false;
                txtNewTier2Name.Focus();//FB 2094
                
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                
                //FB 2670
                if (Session["admin"].ToString().Trim().Equals("3"))
                    trNew.Visible = false;

                if (!IsPostBack)
                    BindData();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
               // errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }

        }

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<GetLocations2>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <Tier1ID>" + Session["Tier1ID"].ToString() + "</Tier1ID>";
                inXML += "</GetLocations2>";
                String outXML = obj.CallMyVRMServer("GetLocations2", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetLocations2 outxml: " + outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    //FB 2272
                    lblTier1Name.Text = obj.GetTranslatedText("Manage ") + Session["Tier1Name"].ToString(); // xmldoc.SelectSingleNode("//location2List/tier1/tier1Name").InnerText;
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations2/Location");
                    if (nodes != null && nodes.Count > 0)
                    {
                        LoadTier2Grid(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgTier2s.Controls[0].Controls[dgTier2s.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        //Response.Write(lblTemp.ClientID);
                        lblTemp.Text = nodes.Count.ToString();
                        dgTier2s.ShowFooter = true;
                        tblNoTier2s.Visible = false;
                        dgTier2s.Visible = true;
                    }
                    else
                    {
                        tblNoTier2s.Visible = true;
                        dgTier2s.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }


        protected void LoadTier2Grid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv = new DataView();
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    Session.Add("Tier2DS", dt);
                    dgTier2s.DataSource = dt;
                    dgTier2s.DataBind();

                    //FB 2670
                    if (Session["admin"].ToString() == "3")
                    {
                        foreach (DataGridItem dgi in dgTier2s.Items)
                        {
                            LinkButton btnDelete = (LinkButton)dgi.FindControl("btnDelete");
                            
                            //btnDelete.Attributes.Remove("onClick");
                            //btnDelete.Style.Add("cursor", "default");
                            //btnDelete.ForeColor = System.Drawing.Color.Gray;
                            LinkButton btnEdit = (LinkButton)dgi.FindControl("btnEdit");
                            
                            //btnEdit.Attributes.Remove("onClick");
                            //btnEdit.Style.Add("cursor", "default");
                            //btnEdit.ForeColor = System.Drawing.Color.Gray;
                            //ZD 100263
                            btnDelete.Visible = false;
                            btnEdit.Visible = false;
                        }
                    }
                }
                else
                {
                   //tblNoTier2s.Visible = true;
                    DataRow dr = dt.NewRow();
                    dt.Columns.Add("tier2ID");
                    dt.Columns.Add("tier2Name");
                    dr["tier2Name"] = "No Tiers found.";
                    dr["tier2ID"] = "";
                    dt.Rows.Add(dr);
                    dgTier2s.DataSource = dt;
                    dgTier2s.DataBind();
                    dgTier2s.Columns[2].Visible = false;
                    dgTier2s.Columns[1].HeaderText = "Message";
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadTier2Grid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion
        protected void DeleteTier2(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<DeleteTier2>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <ID>" + e.Item.Cells[0].Text + "</ID>";
                inXML += "</DeleteTier2>";
                log.Trace("DeleteTier2 Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("DeleteTier2", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("DeleteTier2 Outxml: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTier2.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteTier2:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void EditTier2(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = false;
                DataTable dtTemp = (DataTable)Session["Tier2DS"];
                dgTier2s.EditItemIndex = e.Item.ItemIndex;
                Session.Add("Tier2ID", e.Item.Cells[0].Text);
                //Response.Write(dtTemp.Rows.Count);
                dgTier2s.DataSource = dtTemp;
                dgTier2s.DataBind();
                foreach (DataGridItem dgi in dgTier2s.Items)
                    if (dgi.ItemIndex.Equals(e.Item.ItemIndex))
                    {
                        LinkButton btnTemp = (LinkButton)dgi.FindControl("btnUpdate");
                        btnTemp.Visible = true;
                        btnTemp = (LinkButton)dgi.FindControl("btnDelete");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)dgi.FindControl("btnCancel");
                        btnTemp.Visible = true;
                        btnTemp = (LinkButton)dgi.FindControl("btnEdit");
                        btnTemp.Visible = false;
                    }//Response.Write(":in edit");
                Label lblTemp = new Label();
                DataGridItem dgFooter = (DataGridItem)dgTier2s.Controls[0].Controls[dgTier2s.Controls[0].Controls.Count - 1];
                lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                lblTemp.Text = dgTier2s.Items.Count.ToString();
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("EditTier2:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CancelTier2(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Response.Redirect("ManageTiers.aspx");
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CancelTier2:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void UpdateTier2(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //Response.Write("here");
                TextBox tbTemp = (TextBox)e.Item.FindControl("txtTier2Name");
                String outXML = UpdateManageTier2(tbTemp);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTier2.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("UpdateTier2:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected String UpdateManageTier2(TextBox tbTemp)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<SetTier2>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "      <Tier1ID>" + Session["Tier1ID"].ToString() + "</Tier1ID>";
                inXML += "      <ID>" + Session["Tier2ID"].ToString() + "</ID>";
                inXML += "      <Name>" + tbTemp.Text + "</Name>";
                inXML += "</SetTier2>";
                log.Trace("SetTier2 Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetTier2", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetTier2 Outxml: " + outXML);
                return outXML;
            }
            catch (Exception ex)
            {
                log.Trace("UpdateManageTier2 : Error in Setting Tier1" + ex.Message);
                return obj.ShowSystemMessage();//FB 1881
                //return "Error in Setting Tier1";
            }
        }

        protected void BindRowsDeleteMessage2(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this middle tier ?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsDeleteMessage2:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CreateNewTier2(Object sender, EventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = true;
                Session.Add("Tier2ID", "new");
                //Response.Write("here");
                //Response.End();
                String outXML = UpdateManageTier2(txtNewTier2Name);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTier2.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CreateNewTier2:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void GoBack(Object sender, EventArgs e)
        {
            Response.Redirect("ManageTiers.aspx");
        }
    }
}
