/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient; 
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Linq;
using ns_SqlHelper;



namespace ns_MyVRM
{
    public partial class UserHistoryReport : System.Web.UI.Page
    {

        #region Private Data Members
        protected String format = "MM/dd/yyyy";
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        
        #endregion

     
        #region protected Data Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtFromDate;
        protected System.Web.UI.WebControls.TextBox txtToDate;
        protected System.Web.UI.WebControls.TextBox txtFirstName;
        protected System.Web.UI.WebControls.TextBox txtLastName;
        protected System.Web.UI.WebControls.TextBox txtLogin;
        protected System.Web.UI.WebControls.Button btnSearch;
        protected System.Web.UI.WebControls.DataGrid dgUserLoginReport;
        protected System.Web.UI.WebControls.Label lblusrloginrecord;
        protected System.Web.UI.WebControls.TextBox txtSortBy;

        #endregion

        public UserHistoryReport()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("UserHistoryReport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                if (!IsPostBack)
                {
                    txtFromDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    GetDataSource();
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                throw ex;
            }
        }
        /// <summary>
        /// This method will perform pagin operation
        /// </summary>
        /// <param name="sender"></param>
      /// <param name="e"></param>
      #region PageIndexChanging
      protected void dgUserLoginReport_PageIndexChanging(object sender, DataGridPageChangedEventArgs e)
      {
          try
          {
              dgUserLoginReport.CurrentPageIndex = e.NewPageIndex;
              GetDataSource();
          }
          catch (Exception ex)
          {
              log.Trace(ex.Message);
              //errLabel.Text = "Error 122: Please contact your VRM Administrator";ZD 100263
              errLabel.Text = obj.ShowSystemMessage();
              errLabel.Visible = true;
          }
      }
      #endregion
        /// <summary>
        /// // ExecuteQuery will be invoked to fetch user login history report data
        /// </summary>
        /// <returns></returns>
        private DataTable GetDataSource()
        {
            String inXML = "";
            DataSet dsEntity = null;
            String outXML = "";
            XmlDocument xmlusrDoc = new XmlDocument();
            ArrayList entityList = null;
            //FB 2588 Start
            string tformat = "";
            if (Session["timeFormat"].ToString().Equals("0"))
                tformat = "HH:mm";
            else if (Session["timeFormat"].ToString().Equals("1"))
                tformat = "hh:mm tt";
            else if (Session["timeFormat"].ToString().Equals("2"))
                tformat = "HHmmZ";
            //FB 2588 End
            try
            {
                inXML = "<FetchAllUsers>";
                inXML += "<Users>";
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += obj.OrgXMLElement();
                inXML += "<FirstName>" + txtFirstName.Text + "</FirstName>";
                inXML += "<LastName>" + txtLastName.Text + "</LastName>";
                inXML += "<Email>" + txtLogin.Text + "</Email>";
                inXML += "<FromDate>" + txtFromDate.Text + "</FromDate>";
                inXML += "<ToDate>" + txtToDate.Text + "</ToDate>";
                inXML += "<sortBy>" + txtSortBy.Text + "</sortBy>";
                inXML += "</Users>";
                inXML += "</FetchAllUsers>";

                outXML = obj.CallMyVRMServer("FetchUserLogin", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmlusrDoc.LoadXml(outXML);
                    dsEntity = new DataSet();
                    dsEntity.ReadXml(new XmlNodeReader(xmlusrDoc));
                }
                if (dsEntity != null)
                {
                    if (dsEntity.Tables.Count > 0)
                    {
                        entityList = null;
                        DataRow row = null;

                        for (int i = 0; i < dsEntity.Tables[0].Rows.Count; i++)
                        {
                            row = dsEntity.Tables[0].Rows[i];
                            row["LoginDateTime"] = DateTime.Parse(row["LoginDateTime"].ToString()).ToString(format+ " " + tformat);//FB 2588
                        }
                        if (entityList != null)
                            Cache["Entity"] = entityList;

                        dgUserLoginReport.DataSource = dsEntity.Tables[0];
                        dgUserLoginReport.DataBind();
                        dgUserLoginReport.Visible = true;
                        lblusrloginrecord.Text = "";
                    }
                    else
                    {
                        dgUserLoginReport.Visible = false;
                        lblusrloginrecord.Visible = true;
                        lblusrloginrecord.Text = obj.GetTranslatedText("No Records Found"); //FB 1830 - Translation
                    }
                   
                }
                BoundColumn bcTemp;
                switch (txtSortBy.Text)
                {
                    case "1":
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[0];
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[1];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[3];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        break;
                    case "2":
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[0];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[3];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[1];
                        break;
                    case "3":
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[0];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[1];
                        bcTemp.HeaderStyle.CssClass = "tableHeader";
                        bcTemp = new BoundColumn();
                        bcTemp = (BoundColumn)dgUserLoginReport.Columns[3];
                        break;
                    }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void dgUserLoginReport_Sorting(Object sender, DataGridSortCommandEventArgs e)
        {
            txtSortBy.Text = e.SortExpression;
            GetDataSource();
        }
    
        /// <summary>
        /// This method will convert sortDirection object into "ASC" or "DESC"
        /// </summary>
        /// <param name="sortDireciton"></param>
        /// <returns></returns>
        private string ConvertSortDirectionToSql(SortDirection sortDireciton)
        {
            string newSortDirection = String.Empty;

            switch (sortDireciton)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }
            return newSortDirection;
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgUserLoginReport.CurrentPageIndex = 0;
                txtSortBy.Text = "";
                GetDataSource();
            }
            catch (Exception ex)
            {
                Response.Write(ex.StackTrace);
            }
        }
    }
 }

