/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;





public partial class en_UserMenuController : System.Web.UI.Page
{

    #region Region of Protected members
    
    protected System.Web.UI.WebControls.Button UsermenucontrolSubmit1;
    protected System.Web.UI.WebControls.Button UsermenucontrolSubmit2;
    protected System.Web.UI.HtmlControls.HtmlTable ButtonTable;
    protected System.Web.UI.WebControls.CheckBox m5_3_5;
    myVRMNet.NETFunctions obj;//FB 1830 - Translation
    #endregion

    #region Region Of Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        if (obj == null)
            obj = new myVRMNet.NETFunctions();
        obj.AccessandURLConformityCheck("UserMenuController.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

        obj = new myVRMNet.NETFunctions();//FB 1830 - Translation
        UsermenucontrolSubmit1.Attributes.Add("onclick", "JavaScript:getUserMenuOpinion();");
        UsermenucontrolSubmit2.Attributes.Add("onclick", "JavaScript:window.close();");

        //PSU Fix
        if (Application["CosignEnable"] == null)
            Application["CosignEnable"] = "0";

        //PSU Fix
        if (Application["ssoMode"].ToString().ToLower() == "yes" || Application["CosignEnable"].ToString() == "1")
            m5_3_5.Text = obj.GetTranslatedText("Restore User");//FB 1830 - Translation
        else
            m5_3_5.Text = obj.GetTranslatedText("Inactive Users");//FB 1830 - Translation


       

        if (!IsPostBack)
        {
            if (Request.QueryString["n"] == "1")
                ButtonTable.Attributes.Add("style", "display:block");
            
        }

    }
}
#endregion
