/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_UserReportChart : System.Web.UI.Page
{
    #region Protected Data Members

    protected System.Web.UI.HtmlControls.HtmlInputHidden used;
    protected System.Web.UI.HtmlControls.HtmlInputHidden left;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTotal;
    protected System.Web.UI.HtmlControls.HtmlInputHidden login;

    #endregion

    #region Private and Public variables

    protected String xmlstr = "";
    protected String userID = "";
    String errorMessage = "";

    XmlNodeList nodes = null;
    myVRMNet.NETFunctions obj;//FB 1881
    ns_Logger.Logger log;

    #endregion

    #region Page Load Event Hander

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("UserReportChart.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            obj = new myVRMNet.NETFunctions(); //FB 1881
            //Code modified on 04Apr09 - FB 412- Start
            if (Session["userID"] != null || Session["userID"].ToString() != "")
            {
                if (Request.QueryString["uid"] != null)
                {
                    if (Request.QueryString["uid"].ToString().Trim() != "")
                        userID = Request.QueryString["uid"].ToString().Trim();
                }
                xmlstr = Session["outXMLUSR"].ToString();

                BindData(xmlstr);
            }
            //Code modified on 04Apr09 - FB 412- Start
            else
            {
                Response.Write("<script>self.close();</script>");
            }
            //Code modified on 04Apr09 - FB 412- End

        }
        catch (Exception ex)
        {
            log.Trace(ex.Message);
           
        }
    }

    #endregion

    #region Bind Data
    private void BindData(String xmlstr)
    {
        try
        {

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlstr);
            if (xmlDoc == null)
            {
                if (Convert.ToString(Session["who_use"]) == "VRM")
                {
                    
                    //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";
                    //errorMessage = errorMessage + Convert.ToString(Session["outXMLUSR"]);
                    log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + Convert.ToString(Session["outXMLUSR"]));
                    errorMessage  = obj.ShowSystemMessage();//FB 1881
                    Response.Write(errorMessage);
                }
                else
                {
                    //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");//FB 1881(D)
                    //Response.Write("<br><br><p align='left'><font size=2><b>" + Convert.ToString(Session["outXMLUSR"]) + "<b></font></p>");
                    log.Trace("Outcoming XML document is illegal: " + Convert.ToString(Session["outXMLUSR"]));//FB 1881
                    errorMessage  = obj.ShowSystemMessage();
                    Response.Write(errorMessage);
                }

                xmlDoc = null;
                Response.End();
            }

            nodes = xmlDoc.DocumentElement.SelectNodes("user");
            foreach (XmlNode nod in nodes)
            {
                if (userID == nod.SelectSingleNode("userID").InnerText)
                {
                    hdnTotal.Value = nod.SelectSingleNode("report/total").InnerText;
                    used.Value = nod.SelectSingleNode("report/used").InnerText;
                    left.Value = nod.SelectSingleNode("report/left").InnerText;                    
                    login.Value = nod.SelectSingleNode("login").InnerText;
                }
            }
            //Convert.(used.Value)
            //System.Math.Abs(

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
