/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 

namespace ns_MyVRM
{
    public partial class Users : System.Web.UI.Page
    {
        #region Private Variables

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        bool isDeletePresent = false;   //FB 1405
        bool isActionEvents = false; //FB 1405

        private Int32 userlmt = 0;//Organization Module
        private Int32 existinguserlmt = 0;
        StringBuilder inXML = new StringBuilder();//FB 2027

        #endregion

        #region Protected Variables

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;
        protected System.Web.UI.WebControls.DropDownList lstBridgeStatus;
        protected System.Web.UI.WebControls.Table tblNoUsers;
        protected System.Web.UI.WebControls.DataGrid dgUsers;
        protected System.Web.UI.WebControls.TextBox txtBridges;
        protected System.Web.UI.WebControls.TextBox txtFirstName;
        protected System.Web.UI.WebControls.TextBox txtLastName;
        protected System.Web.UI.WebControls.TextBox txtEmailAddress;
        protected System.Web.UI.WebControls.Table tblAlphabet;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.DropDownList lstSortBy;
        protected System.Web.UI.WebControls.Label lblLicencesRemaining;
        protected System.Web.UI.WebControls.Label lblTotalUsers;
        protected System.Web.UI.WebControls.Button btnDeleteAllGuest;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLicencesRemaining;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLegends;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNew;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNewH;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSearch;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSearchH;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSearchB;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAction; //FB 1405
        protected int Cloud = 0;//FB 2262
    
        #endregion

        #region Constructor
        public Users()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = string.Empty;
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());

                if (Request.QueryString["t"] != null)
                    confChkArg = Request.QueryString["t"].ToString();

                obj.AccessandURLConformityCheck("ManageUser.aspx?t="+confChkArg.Trim(), Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                if (Session["UserLimit"] != null)
                {
                    if (Session["UserLimit"].ToString() != "")
                        Int32.TryParse(Session["UserLimit"].ToString(), out userlmt);
                }

                //PSU Fix
                if (Application["CosignEnable"] == null)
                    Application["CosignEnable"] = "0";

                //lblHeader.Text = "Manage Guests";   Commented for FB 1405
                errLabel.Text = "";
                if (Request.QueryString["t"] == null)
                    txtType.Text = "1";
                else
                    txtType.Text = Request.QueryString["t"].ToString();
                //Added for FB 1405 - Start
                if (Request.Form["hdnAction"] != null)
                {
                    if (Request.Form["hdnAction"].ToString() == "Y")
                        isActionEvents = true;
                }
                //Added for FB 1405 - End

                //FB 2262,//FB 2599 - Starts //FB 2717 Vidyo Integration Start
                /*if (Session["Cloud"] != null) 
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                if (Cloud == 1)
                {
                    trNewH.Visible = false;
                    trNew.Visible = false;
                }*/
				//FB 2599 - Ends //FB 2717 Vidyo Integration End
                //FB 2262,//FB 2599 - Ends
                
                if (!IsPostBack)
                {
                    switch (txtType.Text)
                    {
                        case "1":
                            lblHeader.Text = obj.GetTranslatedText("Manage Active Users");//FB 1830 - Translation FB 2570
                            if (!Session["admin"].ToString().Equals("2"))
                            {
                                trNew.Visible = false;
                                trNewH.Visible = false;
                            }
                            break;
                        case "2":
                            lblHeader.Text = obj.GetTranslatedText("Manage Guests");//FB 1830 - Translation
                            trNew.Visible = false;
                            trNewH.Visible = false;
                            break;
                        case "3":
                            trNew.Visible = false;
                            trNewH.Visible = false;
                            trSearch.Visible = false;
                            trSearchH.Visible = false;
                            trSearchB.Visible = false;
                            lblHeader.Text = obj.GetTranslatedText("Manage Inactive Users");//FB 1830 - Translation
                            break;
                        default:
                            errLabel.Text = obj.GetTranslatedText("Invalid User Type");//FB 1830 - Translation
                            break;
                    }
                    //PSU Fix
                    if (Application["ssoMode"].ToString().ToUpper().Equals("YES")
                    || Application["CosignEnable"].ToString().ToUpper().Equals("1"))
                    {
                        trNewH.Visible = false;
                        trNew.Visible = false;
                    }
                    BindDataUser();
                }
                DisplayAlphabets(tblAlphabet);
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                // FB 2664 start
                if (btnDeleteAllGuest.Enabled == true)
                    btnDeleteAllGuest.Attributes.Add("class", "altLongBlueButtonFormat");
                else
                    btnDeleteAllGuest.Attributes.Add("class", "btndisable");
                //Fb 2664 End
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
            }

        }
        #endregion

        #region BindDataUser

        protected void BindDataUser()
        {
            try
            {
                string rmCasCtrl = "0"; // ZD 100263
                if (Session["roomCascadingControl"] != null)
                    rmCasCtrl = Session["roomCascadingControl"].ToString();

                String pageNo = "1";
                String Alpha = "ALL";//Edited For FB 1405
                String sortBy = "2";
                if (Request.QueryString["pageNo"] != null)
                    pageNo = Request.QueryString["pageNo"].ToString();
                if (Request.QueryString["sortBy"] != null)
                    sortBy = Request.QueryString["sortBy"].ToString();
                lstSortBy.ClearSelection();
                lstSortBy.Items.FindByValue(sortBy).Selected = true;
                if (Request.QueryString["alpha"] != null)
                    Alpha = Request.QueryString["alpha"].ToString();
                //<login><userID>11</userID><sortBy>2</sortBy><alphabet>A</alphabet><pageNo>1</pageNo></login>
                //FB 2027 Starts
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("  <sortBy>" + sortBy + "</sortBy>");
                inXML.Append("  <alphabet>" + Alpha + "</alphabet>");
                inXML.Append("  <pageNo>" + pageNo + "</pageNo>");
                inXML.Append("  <audioaddon>0</audioaddon>"); //FB 2023
                inXML.Append("</login>");
                //Response.Write("<br>" + obj.Transfer(inXML));
                String cmd = "GetManageUser";
                String outXML ="";
                if (txtType.Text.Equals("2"))
                    cmd = "GetManageGuest";
                if (txtType.Text.Equals("3"))
                    cmd = "GetUsers";
                outXML = obj.CallMyVRMServer(cmd, inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 Ends
                //String outXML = "<bridgeInfo><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><bridges><bridge><ID>1</ID><name>Test Bridge</name><interfaceType>3</interfaceType><administrator>VRM Administrator</administrator><exist>1</exist><status>1</status><order></order></bridge></bridges><totalNumber>1</totalNumber><licensesRemain>9</licensesRemain></bridgeInfo>";
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = null;
                    int totalPages = 1;
                    switch (txtType.Text)
                    {
                        case "1":
                            btnDeleteAllGuest.Visible = false;
                            lblTotalUsers.Text = xmldoc.SelectSingleNode("//users/totalNumber").InnerText;
                            Int32.TryParse(xmldoc.SelectSingleNode("//users/totalNumber").InnerText, out existinguserlmt);
                            //lblLicencesRemaining.Text = (userlmt - existinguserlmt).ToString();//xmldoc.SelectSingleNode("//users/licensesRemain").InnerText;
                            lblLicencesRemaining.Text = xmldoc.SelectSingleNode("//users/licensesRemain").InnerText; //FB 2027
                            nodes = xmldoc.SelectNodes("//users/user");
                            totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//users/totalPages").InnerText);
                            break;
                        case "2":
                            tdLicencesRemaining.Visible = false;
                            tdLegends.Visible = false;
                            if(Session["admin"].ToString().Equals("3")) //FB 2670
                                btnDeleteAllGuest.Visible = false;
                            else
                                btnDeleteAllGuest.Visible = true;

                            lblTotalUsers.Text = xmldoc.SelectSingleNode("//users/totalNumber").InnerText;
                            nodes = xmldoc.SelectNodes("//users/user");
                            totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//users/totalPages").InnerText);
                            break;
                        case "3":
                            tdLicencesRemaining.Visible = false;
                            tdLegends.Visible = false; 
                            btnDeleteAllGuest.Visible = false;
                            lblTotalUsers.Text = xmldoc.SelectSingleNode("//userInfo/users/totalNumber").InnerText;
                            nodes = xmldoc.SelectNodes("//userInfo/users/user");
                            totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//userInfo/users/totalPages").InnerText);
                            break;
                    }
                    if (totalPages > 1)
                    {
                        //  Request.QueryString.Remove("pageNo");
                        string qString = Request.QueryString.ToString();
                        if (Request.QueryString["pageNo"] != null)
                            qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                        if (Request.QueryString["m"] != null)
                            qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                        obj.DisplayPaging(totalPages, Int32.Parse(pageNo), tblPage, "ManageUser.aspx?" + qString);
                    }
                    //Response.Write(nodes.Count);
                    if (nodes.Count > 0)
                    {
                        dgUsers.Visible = true;
                        tblNoUsers.Visible = false;
                        LoadGrid(nodes);
                        foreach (DataGridItem dgi in dgUsers.Items)
                        {
                            LinkButton btnTemp = (LinkButton)dgi.FindControl("btnDelete" + rmCasCtrl); // ZD 100263
                            if (dgi.Cells[2].Text.Trim().Equals("1"))
                                btnTemp.Text = obj.GetTranslatedText("Undelete");//FB 1830 - Translation
                            btnTemp = (LinkButton)dgi.FindControl("btnLock" + rmCasCtrl); // ZD 100263
                            if (dgi.Cells[3].Text.Trim().Equals("1"))
                                btnTemp.Text = obj.GetTranslatedText("UnBlock");//FB 1830 - Translation
                        }
                    }
                    else
                    {
                        dgUsers.Visible = false;
                        tblNoUsers.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region LoadGrid From Xml
        
        protected void LoadGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                //Response.Write(ds.Tables[0].Rows[0]["firstName"]);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (ds.Tables.Count > 1)
                    {
                        foreach (DataColumn dc in ds.Tables[1].Columns)
                            if (!dt.Columns.Contains(dc.ColumnName)) dt.Columns.Add(dc.ColumnName);

                        foreach (DataRow dr in dt.Rows)
                            foreach (DataRow drStatus in ds.Tables[1].Rows)
                                if (dr["user_Id"].ToString().Equals(drStatus["user_Id"].ToString()))
                                {
                                    dr["level"] = drStatus["level"];
                                    dr["deleted"] = drStatus["deleted"];
                                    dr["locked"] = drStatus["locked"];
                                    dr["crossaccess"] = drStatus["crossaccess"];
                                }
                    }
                    else
                    {
                        if (!dt.Columns.Contains("level")) dt.Columns.Add("level");
                        if (!dt.Columns.Contains("deleted")) dt.Columns.Add("deleted");
                        if (!dt.Columns.Contains("locked")) dt.Columns.Add("locked");
                        if (!dt.Columns.Contains("crossaccess")) dt.Columns.Add("crossaccess");
                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["level"] = "0";
                            dr["deleted"] = "0";
                            dr["locked"] = "0";
                            dr["crossaccess"] = "0";
                        }
                    }
                    //FB 2346
                    string email;
                    string usermail, tempemail;
                    foreach (DataRow drr in ds.Tables[0].Rows)
                    {
                        if (drr["email"].ToString().Trim() != "") //FB 2023
                        {
                            email = drr["email"].ToString();
                            usermail = email.Substring(email.IndexOf('@'));
                            tempemail = usermail;
                            if (usermail.Contains("_D"))
                                usermail = usermail.Replace("_D", "");
                            email = email.Replace(tempemail, usermail);

                            drr["email"] = email;
                        }
                    }
                }
                dgUsers.DataSource = ds;
                dgUsers.DataBind();
                // Added for FB 1405 -- Start
                if (isDeletePresent)
                {
                    btnDeleteAllGuest.Enabled = true;
                    btnDeleteAllGuest.Attributes.Add("Class", "altLongBlueButtonFormat");//FB 2664
                }
                else
                {
                    btnDeleteAllGuest.Enabled = false;
                    btnDeleteAllGuest.Attributes.Add("Class", "btndisable");//FB 2664
                }

                // Added for FB 1405 -- End
                if (!txtType.Text.Equals("1"))
                    dgUsers.Columns[8].Visible=false;
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region DeleteUser

        protected void DeleteUser(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //Response.Write("In deleteuser");
                //Response.End();
                String inXML = ""; // <login><userID>11</userID><user><userID>26</userID><action>-1</action></user></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <user>";
                inXML += "      <userID>" + e.Item.Cells[0].Text + "</userID>";
                //Response.Write(e.Item.Cells[2].Text.Trim());
                if (e.Item.Cells[2].Text.Trim().Equals("1"))
                    inXML += "      <action>3</action>";
                else
                    if (txtType.Text.Equals("1"))
                        inXML += "      <action>-1</action>";
                    else
                        inXML += "  <action>-3</action>";
                inXML += "  </user>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String cmdName = "ChangeUserStatus";
                if (txtType.Text.Equals("2"))
                    cmdName = "ChangeGuestStatus";
                String outXML = obj.CallMyVRMServer(cmdName, inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(cmdName);
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    //string qString = "";
                    //qString += "t=" + txtType.Text;
                    //if (Request.QueryString["alpha"] != null)
                    //    qString += "&alpha=" + Request.QueryString["alpha"].ToString();
                    //if (Request.QueryString["sortBy"] != null)
                    //    qString += "&sortBy=" + Request.QueryString["sortBy"].ToString();

                    //qString += "&pageNo=1";
                    //qString += "&m=1";
                    //Response.Write
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    BindDataUser();
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region EditUser
        protected void EditUser(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("UserToEdit", e.Item.Cells[0].Text);
                Response.Redirect("ManageUserProfile.aspx?t=1");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region LockUser
        protected void LockUser(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><user><userID>26</userID><action>-1</action></user></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <user>";
                inXML += "      <userID>" + e.Item.Cells[0].Text + "</userID>";
                if (e.Item.Cells[3].Text.Equals("1"))
                    inXML += "      <action>2</action>";
                else
                    inXML += "      <action>-2</action>";
                inXML += "  </user>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("ChangeUserStatus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    //string qString = "";
                    //qString += "t=" + txtType.Text;
                    //if (Request.QueryString["alpha"] != null)
                    //    qString += "&alpha=" + Request.QueryString["alpha"].ToString();
                    //if (Request.QueryString["sortBy"] != null)
                    //    qString += "&sortBy=" + Request.QueryString["sortBy"].ToString();

                    //qString += "&pageNo=1";
                    //qString += "&m=1";
                    ////Response.Write
                    //Response.Redirect("ManageUser.aspx?" + qString);
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                    BindDataUser();
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                string rmCasCtrl = "0"; // ZD 100263
                if (Session["roomCascadingControl"] != null)
                    rmCasCtrl = Session["roomCascadingControl"].ToString();

                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView; //FB 2670
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete" + rmCasCtrl); // ZD 100263
                    //if (Cloud == 1) //FB 2262 //FB 2599 //FB 2717 Vidyo Integration
                    //    ((LinkButton)e.Item.FindControl("btnDelete")).Enabled = false;

                    String txtText = "";
                    //Response.Write("<br>'" + e.Item.Cells[2].Text.Trim() + "'");
                    if (e.Item.Cells[2].Text.Trim().Equals("1"))
                    // Added for FB 1405 -- Start
                    {
                        btnTemp.Text = obj.GetTranslatedText("Undelete");//FB 1830 - Translation
                        txtText = "UNDELETE";
                    }
                    else
                    {
                        btnTemp.Text = obj.GetTranslatedText("Delete");//FB 1830 - Translation
                        txtText = "DELETE";
                        isDeletePresent = true;
                    }

                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "2")
                    {
                        
                        //btnTemp.Attributes.Remove("onClick");
                        //btnTemp.Style.Add("cursor", "default");
                        //btnTemp.ForeColor = System.Drawing.Color.Gray;
                        //ZD 100263
                        btnTemp.Visible = false;
                        //btnTemp.Enabled = false;
                        
                    }
                    // Added for FB 1405 -- End

                    // fogbugz case 363 starts here
                    if (btnTemp.Enabled)
	                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to " + txtText + " this user account?") + "')"); //FB japnese
                    txtText = "BLOCK";
                    if (e.Item.Cells[3].Text.Trim().Equals("1"))
                        txtText = "UNBLOCK";
                    btnTemp = (LinkButton)e.Item.FindControl("btnLock" + rmCasCtrl); // ZD 100263
                    //if (Cloud == 1) //FB 2262 //FB 2599 //FB 2717 Vidyo Integration
                    //    ((LinkButton)e.Item.FindControl("btnLock")).Enabled = false;
                    if (btnTemp.Enabled)
	                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to " + txtText + " this user account?") + "')"); //FB japnese
                    btnTemp = (LinkButton)e.Item.FindControl("btnReset" + rmCasCtrl); // ZD 100263
                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "3")
                    {
                        
                        //btnTemp.Attributes.Remove("onClick");
                        //btnTemp.Style.Add("cursor", "default");
                        //btnTemp.ForeColor = System.Drawing.Color.Gray;
                        //ZD 100263
                        btnTemp.Visible = false;
                        //btnTemp.Enabled = false;
                    }
                    if (btnTemp.Enabled)
	                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to ACTIVATE this user account ?") + "')"); //FB japnese
                    btnTemp = (LinkButton)e.Item.FindControl("btnDeleteInactive" + rmCasCtrl); // ZD 100263
                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "3")
                    {
                        
                        //btnTemp.Attributes.Remove("onClick");
                        //btnTemp.Style.Add("cursor", "default");
                        //btnTemp.ForeColor = System.Drawing.Color.Gray;
                        //ZD 100263
                        btnTemp.Visible = false;
                        //btnTemp.Enabled = false;
                    }
                    if (btnTemp.Enabled)
	                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to DELETE this user account. This user will not longer be available to restore ?") + "')"); //FB japnese

                    //if (Cloud == 1) //FB 2262 //FB 2599 //FB 2717 Vidyo Integration
                    //    ((LinkButton)e.Item.FindControl("btnDeleteInactive")).Enabled = false;

                    // fogbugz case 363 ends here

                    /* *** FB 1401 To alert the user on edit of the blocked user - Start *** */
                    btnTemp = (LinkButton)e.Item.FindControl("btnEdit" + rmCasCtrl); // ZD 100263
                    if (btnTemp.Enabled)
                    {
                        if (e.Item.Cells[3].Text.Trim().Equals("1"))
                            btnTemp.Attributes.Add("onclick", "alert('" + obj.GetTranslatedText("You must UNBLOCK this user before editing their profile.") + "');return false;");
                    }
                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "1")
                    {                        
                        if (row["userID"].ToString() != "11" && Session["userID"].ToString() != row["userID"].ToString()
                            && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || row["crossaccess"].ToString() != "1" ))
                            btnTemp.Enabled = true;
                        else
                            btnTemp.Enabled = false;
                        btnTemp.Text = "View";
                        //ZD 100263
                        btnTemp.Visible = false;
                    }
                    /* *** FB 1401 To alert the user on edit of the blocked user - End *** */
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region CreateNewUser
        protected void CreateNewUser(Object sender, EventArgs e)
        {
            Session.Add("UserToEdit", "new");
            Response.Redirect("ManageUserProfile.aspx?t=1");
        }
        #endregion

        #region SearchUser
        protected void SearchUser(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<user>");
                inXML.Append("<firstName>" + txtFirstName.Text + "</firstName>");
                inXML.Append("<lastName>" + txtLastName.Text + "</lastName>");
                inXML.Append("<email>" + txtEmailAddress.Text + "</email>");
                inXML.Append("<type>" + txtType.Text + "</type>");//FB 2027
                inXML.Append("</user>");
                inXML.Append("</login>");
                /*Edited for FB 1405 - Start
                String cmd = "SearchUserForManage";
                if (txtType.Text.Equals("2"))
                    cmd = "SearchGuestForManage";
                String outXML = obj.CallCOM(cmd, inXML, Application["COM_ConfigPath"].ToString());
                Edited for FB 1405 - End
                Response.Write(obj.Transfer(outXML));*/
                String outXML = obj.CallMyVRMServer("SearchUserOrGuest", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    dgUsers.Visible = false;
                    tblNoUsers.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                    if (nodes.Count > 0)
                    {
                        dgUsers.Visible = true;
                        tblNoUsers.Visible = false;
                        LoadGrid(nodes);
                    }
                    else
                    {
                        dgUsers.Visible = false;
                        tblNoUsers.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region ChangeAlphabets
        protected void ChangeAlphabets(Object sender, EventArgs e)
        {
            try
            {
                //            DisplayAlphabets(tblAlphabet);
                String qString = Request.QueryString.ToString();
                if (Request.QueryString["sortBy"] == null)
                    qString += "&sortBy=" + lstSortBy.SelectedValue;
                else
                    qString = qString.Replace("&sortBy=" + Request.QueryString["sortBy"].ToString(), "&sortBy=" + lstSortBy.SelectedValue);
                if (Request.QueryString["pageNo"] == null)
                    qString += "&pageNo=1";
                else
                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "&pageNo=1");
                //Response.Write(qString);
                Response.Redirect("ManageUser.aspx?" + qString);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region DisplayAlphabets
        protected void DisplayAlphabets(Table tblAlpha)
        {
            try
            {
                String[] Alphabets = { "a", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "All" };
                TableRow tr = new TableRow();
                TableCell tc;
                tblAlpha.Rows.Clear();
                string alpha = "";
                if (Request.QueryString["alpha"] != null)
                    alpha = Request.QueryString["alpha"].ToString();
                //Added for FB 1405 - Start

                if (isActionEvents)
                    alpha = "";

                //Added for FB 1405 - End
                for (int i = 0; i < Alphabets.Length; i++)
                {
                    tc = new TableCell();
                    if (alpha.Equals(Alphabets[i]) || ((i == Alphabets.Length - 1) && (alpha.ToLower().Equals(Alphabets[i].ToLower()))))
                    {
                        if (i == 0)
                            Alphabets[i] = "0-a";
                        tc.Text = Alphabets[i];
                    }
                    else
                    {
                        string qString = "";
                        qString += "t=" + txtType.Text;
                        if (i == Alphabets.Length - 1)
                            qString += "&alpha=" + Alphabets[i].ToLower();
                        else
                            qString += "&alpha=" + Alphabets[i];
                        if (Request.QueryString["sortBy"] != null)
                            qString += "&sortBy=" + Request.QueryString["sortBy"];
                        else
                            qString += "&sortBy=" + lstSortBy.SelectedValue;

                        qString += "&pageNo=1";
                        //Response.Write("<br>" + qString);
                        //Response.End();
                        if (i == 0)
                            Alphabets[i] = "0-a";
                        tc.Text = "<a href='ManageUser.aspx?" + qString + "'>" + Alphabets[i] + "</a>";
                    }
                    tr.Cells.Add(tc);
                }
                tblAlpha.Rows.Add(tr);
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region DeleteAllGuest
        protected void DeleteAllGuest(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID></userID></user></login>";//Organization Module Fixes
                //String outXML = obj.CallCOM("DeleteAllGuests", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("DeleteAllGuests", inXML, Application["COM_ConfigPath"].ToString()); //FB 2027
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    BindDataUser();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

        #region RestoreUser
        protected void RestoreUser(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //<login><userID>11</userID><users><user><userID>13</userID><mode>2</mode></user></users></login>
                //FB 2027 - Starts
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<users>");
                inXML.Append("<user>");
                inXML.Append("<userID>" + e.Item.Cells[0].Text + "</userID>");
                inXML.Append("<mode>" + e.CommandArgument.ToString() + "</mode>");
                inXML.Append("</user>");
                inXML.Append("</users>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                //Response.Write(obj.Transfer(inXML));
                //FB 2027
                //String outXML = obj.CallCOM("SetUserStatus", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("SetUserStatus", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write("<br>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    //string qString = "";
                    //qString += "t=" + txtType.Text;
                    //if (Request.QueryString["alpha"] != null)
                    //    qString += "&alpha=" + Request.QueryString["alpha"].ToString();
                    //if (Request.QueryString["sortBy"] != null)
                    //    qString += "&sortBy=" + Request.QueryString["sortBy"].ToString();

                    //qString += "&pageNo=1";
                    //qString += "&m=1";
                    ////Response.Write
                    //Response.Redirect("ManageUser.aspx?" + qString);
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    BindDataUser();

                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion
    }
}
