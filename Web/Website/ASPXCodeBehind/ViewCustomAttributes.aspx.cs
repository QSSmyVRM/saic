/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;


namespace ns_MyVRM
{
    public partial class ViewCustomAttributes : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;

        #region Protected Data Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Table tblNoCustomAttribute;
        protected System.Web.UI.WebControls.DataGrid dgCustomAttribute;
        protected System.Web.UI.WebControls.Button btnCreateCusAtt;
        protected System.Web.UI.HtmlControls.HtmlTableCell ConfMessage;
        protected System.Web.UI.WebControls.Button CustomTrigger;
        protected System.Web.UI.WebControls.Table ConfListTbl;
        protected AjaxControlToolkit.ModalPopupExtender CustomPopUp;
        protected System.Web.UI.WebControls.Panel PopupCustomPanel;
        protected System.Web.UI.HtmlControls.HtmlInputHidden HdnCustOptID;
        protected System.Web.UI.WebControls.Button BtnEditCA;
        protected System.Web.UI.WebControls.Button BtnDeleteAll;
        protected string customAttrID = "";

        protected System.Web.UI.WebControls.ImageButton btnExcel;

        string confListHeader = "";
        XmlNodeList confList = null;
        ns_Logger.Logger log;
        int customAttrLimit = 10; //FB 1779
        #endregion

        public ViewCustomAttributes()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #region  Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ViewCustomAttributes.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                errLabel.Text = "";
                
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }

                if (!IsPostBack)
                    BindData();

                BtnDeleteAll.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Custom Option ?") + "')"); //FB japnese
                //BtnEditCA.Attributes.Add("onclick", "return confirm('Are you sure you want to edit this Custom Option?')");

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    
                    //btnCreateCusAtt.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //btnCreateCusAtt.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnCreateCusAtt.Visible = false;
                }
                else
                    btnCreateCusAtt.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }

        }
        #endregion

        #region BindData
        private void BindData()
        {
            try
            {
                String inXML = "<CustomAttribute>" + obj.OrgXMLElement() + "<DeptID></DeptID></CustomAttribute>";//Organization Module Fixes

                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                String outXML = obj.CallMyVRMServer("GetCustomAttributes", inXML, Application["MyVRMServer_ConfigPath"].ToString());
               
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    if (xmldoc.SelectSingleNode("//CustomAttributesList/CustomAttrLimit") != null) //FB 1779
                    {
                        int.TryParse(xmldoc.SelectSingleNode("//CustomAttributesList/CustomAttrLimit").InnerText, out customAttrLimit);
                    }

                    XmlNodeList nodes = xmldoc.SelectNodes("//CustomAttributesList/CustomAttribute");

                    DataTable dt = CustomTable();

                    if (nodes.Count > 0)
                    {
                        DataGrid dgCustomOptions = (DataGrid)FindControl("dgCustomOptions");
                       
                        int rowCount = 1;
                        foreach (XmlNode node in nodes)
                        {
                            DataRow dr = dt.NewRow();
                            dr["CustomAttributeID"] = node.SelectSingleNode("CustomAttributeID").InnerText;
                            dr["Title"] = node.SelectSingleNode("Title").InnerText;
                            dr["Description"] = node.SelectSingleNode("Description").InnerText;

                            string caType = node.SelectSingleNode("Type").InnerText;
                            string controlType = "";

                            switch (caType)
                            {
                                case "2": //FB 2377
                                    {
                                        controlType = "Check Box";
                                        break;
                                    }
                                case "3":
                                    {
                                        controlType = "Radio Button";
                                        break;
                                    }
                                case "4":
                                    {
                                        controlType = "Text Box";
                                        break;
                                    }
                                case "5":
                                    {
                                        controlType = "List Box";
                                        break;
                                    }
                                case "6":
                                    {
                                        controlType = "Drop-Down List"; //FB 2045
                                        break;
                                    }
                                case "7":
                                    {
                                        controlType = "URL";
                                        break;
                                    }
                                case "8": //FB 1718
                                    {
                                        controlType = "RadioButton List";
                                        break;
                                    }
                                case "10": //FB 1718
                                    {
                                        controlType = "Text-Multiline";
                                        break;
                                    }
                            }
                            dr["Type"] = controlType;

                            string castatus = node.SelectSingleNode("Status").InnerText;
                            if (castatus == "0")
                                dr["CAStatus"] = "Yes";
                            else
                                dr["CAStatus"] = "No";

                            string caRequired = "";
                            if(node.SelectSingleNode("Mandatory") != null)
                                caRequired = node.SelectSingleNode("Mandatory").InnerText;
                            
                            if (caRequired == "1")
                                dr["Mandatory"] = "Yes";
                            else
                                dr["Mandatory"] = "No";

                            string emailReq = "";
                            if (node.SelectSingleNode("IncludeInEmail") != null)
                                emailReq = node.SelectSingleNode("IncludeInEmail").InnerText;

                            if (emailReq == "1")
                                dr["IncludeInEmail"] = "Yes";
                            else
                                dr["IncludeInEmail"] = "No";


                            string createType = "";    //FB 1779
                            if (node.SelectSingleNode("CreateType") != null)
                                createType = node.SelectSingleNode("CreateType").InnerText.Trim();

                            if (createType == "1")
                            {
                                dr["CreateType"] = "System";
                                dr["RowUID"] = "*";
                            }
                            else
                            {
                                dr["CreateType"] = "User";
                                dr["RowUID"] = rowCount++;
                            }
                            dt.Rows.Add(dr);

                            //XmlNodeList optionNodes = node.SelectNodes("OptionList/Option");
                            //LoadGroupOptionsGrid(optionNodes, dgCustomOptions);
                        }
                        dgCustomAttribute.DataSource = dt;
                        dgCustomAttribute.DataBind();
                       
                        btnCreateCusAtt.Enabled = true;
                        if (rowCount > customAttrLimit)//FB 1779
                        {
                            btnCreateCusAtt.Enabled = false;
                        }

                        dgCustomAttribute.Visible = true;
                        tblNoCustomAttribute.Visible = false;

                    }
                    else
                    {
                        dgCustomAttribute.Visible = false;
                        tblNoCustomAttribute.Visible = true;

                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }
        #endregion

        #region LoadGroupOptionsGrid
        protected void LoadGroupOptionsGrid(XmlNodeList nodes, DataGrid dgCustomOptions)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
              
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    dgCustomOptions.DataSource = dt;
                    dgCustomOptions.DataBind();
                }

            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadGroupOptionsGrid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region CustomTable

        protected DataTable CustomTable()
        {
            try
            {
                DataTable dt = new DataTable();
                
                if (!dt.Columns.Contains("RowUID")) dt.Columns.Add("RowUID");
                if (!dt.Columns.Contains("CustomAttributeID")) dt.Columns.Add("CustomAttributeID");
                if (!dt.Columns.Contains("Title")) dt.Columns.Add("Title");
                if (!dt.Columns.Contains("Description")) dt.Columns.Add("Description");
                if (!dt.Columns.Contains("Type")) dt.Columns.Add("Type");
                if (!dt.Columns.Contains("CAStatus")) dt.Columns.Add("CAStatus");
                if (!dt.Columns.Contains("Mandatory")) dt.Columns.Add("Mandatory");
                if (!dt.Columns.Contains("IncludeInEmail")) dt.Columns.Add("IncludeInEmail");
                if (!dt.Columns.Contains("CreateType")) dt.Columns.Add("CreateType"); //FB 1779
                
                return dt;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CustomTable:" + ex.Message);//ZD 100263
                return null;
            }
        }

        #endregion

        //Method added during FB 1779 to disable system custom attributes
        #region BindRowsToGrid
        protected void BindRowsToGrid(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;
                    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete"); //FB 2670

                    if (row["CreateType"] != null)
                    {
                        if (row["CreateType"].ToString().ToLower() == "system" || Session["admin"].ToString() == "3") //FB 2670
                        {
                            
                            //btnDelete.Attributes.Remove("onClick");
                            //btnDelete.Style.Add("cursor", "default");
                            //btnDelete.ForeColor = System.Drawing.Color.Gray;
                            //ZD 100263
                            btnDelete.Visible = false;
                        }
                    }

                    //FB 2670
                    if (Session["admin"].ToString() == "3")
                    {
                        LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                        btnEdit.Text = "View";
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsToGrid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Delete Custom Attribute
        protected void DeleteCustomGrid(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                String outXML = "";
                inXML += "<CustomAttribute>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <ID>" + e.Item.Cells[0].Text + "</ID>";
                inXML += "</CustomAttribute>";
                                
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                outXML = obj.CallMyVRMServer("DeleteCustomAttribute", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ViewCustomAttributes.aspx?m=1");
                }
                else
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);

                    XmlNode nd = null;
                    nd = xd.SelectSingleNode("//error/message");

                    if (nd != null)
                    {
                        ConfMessage.InnerText = nd.InnerText.Trim();
                        confList = xd.SelectNodes("//error/conferencelist/past/conference");
                        if (confList != null)
                        {
                            if(confList.Count > 0)
                                confListHeader = obj.GetTranslatedText("Past Conference(s)"); // FB 2272
                            BuildConfList();
                            confListHeader = "";
                        }

                        confList = xd.SelectNodes("//error/conferencelist/future/conference");
                        if (confList != null)
                        {
                            if (confList.Count > 0)
                                confListHeader = obj.GetTranslatedText("Future Conference(s)"); // FB 2272
                            BuildConfList();
                            confListHeader = "";
                        }
                        if (ConfListTbl.Rows.Count > 0)
                        {
                            string sessionKey = "XML" + Session.SessionID;

                            Session.Remove(sessionKey);
                            Session.Add(sessionKey, outXML);

                            HdnCustOptID.Value = e.Item.Cells[0].Text;
                            BtnEditCA.Visible = false;
                            BtnDeleteAll.Visible = true;
                            CustomPopUp.Show();
                        }
                    }
                    else
                    {
                        //errLabel.Text = "Error 122: Please contact your VRM Administrator";
                        errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteCustomGrid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region EditCustomGrid
        protected void EditCustomGrid(object sender, DataGridCommandEventArgs e)
        {
            String inXML = "";
            String outXML = "";
            try
            {
                inXML += "<CustomAttribute>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <ID>" + e.Item.Cells[0].Text + "</ID>";
                inXML += "</CustomAttribute>";

                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                outXML = obj.CallMyVRMServer("GetConfListByCustOptID", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0 && Session["admin"].ToString() != "3")//FB 2670
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);

                    XmlNode nd = null;
                    nd = xd.SelectSingleNode("//error/message");

                    if (nd != null)
                    {
                        ConfMessage.InnerText = nd.InnerText.Trim();
                        confList = xd.SelectNodes("//error/conferencelist/past/conference");
                        if (confList != null)
                        {
                            if (confList.Count > 0)
                                confListHeader = obj.GetTranslatedText("Past Conference(s)"); // FB 2272
                            BuildConfList();
                            confListHeader = "";
                        }

                        confList = xd.SelectNodes("//error/conferencelist/future/conference");
                        if (confList != null)
                        {
                            if (confList.Count > 0)
                                confListHeader = obj.GetTranslatedText("Future Conference(s)"); // FB 2272
                            BuildConfList();
                            confListHeader = "";
                        }
                        if (ConfListTbl.Rows.Count > 0)
                        {
                            string sessionKey = "XML" + Session.SessionID;

                            Session.Remove(sessionKey);
                            Session.Add(sessionKey, outXML);

                            HdnCustOptID.Value = e.Item.Cells[0].Text;
                            BtnDeleteAll.Visible = false;
                            BtnEditCA.Visible = true;
                            CustomPopUp.Show();
                        }
                        return;
                    }
                }
                else
                {
                    Response.Redirect("ManageCustomAttribute.aspx?CustomID=" + e.Item.Cells[0].Text);
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.Message);
                //errLabel.Text = "Error 122: Please contact your VRM Administrator";
                //errLabel.Text  = obj.ShowSystemMessage();//FB 1881 ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    
                    //FB 2045 - Start
                    btnTemp.Attributes.Add("onclick", "javascript:return fnconfirmdel(this.disabled)");
                    //btnTemp.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this Custom Option?')");
                    //FB 2045 - End


                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsDeleteMessage:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region  CreateNewCustomAtt
        protected void CreateNewCustomAtt(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageCustomAttribute.aspx?CustomID=");
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CreateNewCustomAtt:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Build Conferences Table

        private void BuildConfList()
        {
            try
            {
                TableRow tRow;
                TableCell tCol;

                if (ConfListTbl == null)
                    ConfListTbl = new Table();
                                
                if (ConfListTbl.Rows.Count <= 0)
                {
                    tRow = new TableRow();

                    tCol = new TableCell();
                    tCol.Text = obj.GetTranslatedText("Unique ID");//FB 1830 - Translation
                    tCol.Width = Unit.Percentage(15);
                    tCol.CssClass = "tableHeader";
                    tRow.Cells.Add(tCol);
                    tCol = null;

                    tCol = new TableCell();
                    tCol.Text = obj.GetTranslatedText("Conference Title");//FB 1830 - Translation
                    tCol.CssClass = "tableHeader";
                    tRow.Cells.Add(tCol);
                    tCol = null;

                    ConfListTbl.Rows.Add(tRow);
                    tRow = null;
                }

                tRow = new TableRow();

                tCol = new TableCell();
                tCol.Text = confListHeader;
                tCol.ColumnSpan = 2;
                tCol.HorizontalAlign = HorizontalAlign.Left;
                tCol.CssClass = "subtitleblueblodtext";
                tRow.Cells.Add(tCol);
                ConfListTbl.Rows.Add(tRow);
                tCol = null;
                tRow = null;

                string confuid = "";
                string confname = "";
                foreach (XmlNode node in confList)
                {
                    if (node.SelectSingleNode("confnumname") != null)
                        confuid = node.SelectSingleNode("confnumname").InnerText.Trim();

                    if (node.SelectSingleNode("confname") != null)
                        confname = node.SelectSingleNode("confname").InnerText.Trim();

                    if (confuid != "" && confname != "")
                    {
                        tRow = new TableRow();

                        tCol = new TableCell();
                        tCol.Text = confuid;
                        tCol.CssClass = "tableBody";
                        tRow.Cells.Add(tCol);
                        tCol = null;

                        tCol = new TableCell();
                        tCol.Text = confname;
                        tCol.CssClass = "tableBody";
                        tRow.Cells.Add(tCol);
                        tCol = null;

                        ConfListTbl.Rows.Add(tRow);
                        tRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DeleteCustomOptions
        /// <summary>
        /// DeleteCustomOptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteCustomOptions(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                String outXML = "";
                if (HdnCustOptID.Value.Trim() != "")
                {
                    inXML += "<CustomAttribute>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <ID>" + HdnCustOptID.Value.Trim() + "</ID>";
                    inXML += "  <mode>D</mode>";
                    inXML += "</CustomAttribute>";

                    if (obj == null)
                        obj = new myVRMNet.NETFunctions();

                    outXML = obj.CallMyVRMServer("DeleteConfsAttributeByID", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        Response.Redirect("ViewCustomAttributes.aspx?m=1");
                    }
                    else
                    {
                        XmlDocument xd = new XmlDocument();
                        xd.LoadXml(outXML);

                        XmlNode nd = null;
                        nd = xd.SelectSingleNode("error");
                        if (nd != null)
                        {
                            errLabel.Text = nd.InnerText.Trim();
                        }
                        else
                        {
                            //errLabel.Text = "Error 122: Please contact your VRM Administrator";
                            errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                        }
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.Message);
                //errLabel.Text = ex.Message;ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Edit Custom Options
        /// <summary>
        /// Edit Custom Options
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditCustomOptions(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                String outXML = "";
                if (HdnCustOptID.Value.Trim() != "")
                {
                    inXML += "<CustomAttribute>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <ID>" + HdnCustOptID.Value.Trim() + "</ID>";
                    inXML += "  <mode>E</mode>";
                    inXML += "</CustomAttribute>";

                    if (obj == null)
                        obj = new myVRMNet.NETFunctions();

                    outXML = obj.CallMyVRMServer("DeleteConfsAttributeByID", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        Response.Redirect("ManageCustomAttribute.aspx?CustomID=" + HdnCustOptID.Value.Trim());
                    }
                    else
                    {
                        XmlDocument xd = new XmlDocument();
                        xd.LoadXml(outXML);

                        XmlNode nd = null;
                        nd = xd.SelectSingleNode("error");
                        if (nd != null)
                        {
                            errLabel.Text = nd.InnerText.Trim();
                        }
                        else
                        {
                            //errLabel.Text = "Error 122: Please contact your VRM Administrator";
                            errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                        }
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.Message);
               // errLabel.Text = ex.Message;ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region ExportExcel

        protected void ExportExcel(object sender, EventArgs e)
        {
            try
            {
                string outXml = "";
                string sessionKey = "XML" + Session.SessionID;
                if (Session[sessionKey] != null)
                {
                    outXml = Session[sessionKey].ToString();
                }

                if (outXml != "")
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXml);

                    XmlNode nd = null;
                    nd = xd.SelectSingleNode("//error/message");

                    if (nd != null)
                    {
                        ConfMessage.InnerText = nd.InnerText.Trim();
                        confList = xd.SelectNodes("//error/conferencelist/past/conference");
                        if (confList != null)
                        {
                            if (confList.Count > 0)
                                confListHeader = obj.GetTranslatedText("Past Conference(s)"); // FB 2272
                            BuildConfList();
                            confListHeader = "";
                        }

                        confList = xd.SelectNodes("//error/conferencelist/future/conference");
                        if (confList != null)
                        {
                            if (confList.Count > 0)
                                confListHeader = obj.GetTranslatedText("Future Conference(s)"); // FB 2272
                            BuildConfList();
                            confListHeader = "";
                        }
                    }
                }
                // Excel 
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ConferenceList.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                ConfListTbl.RenderControl(hw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion
    }
}
