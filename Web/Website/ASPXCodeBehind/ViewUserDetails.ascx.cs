﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace ns_MyVRM
{
    public partial class en_ViewUserDetails : System.Web.UI.UserControl
    {
        protected System.Web.UI.WebControls.Table UserTable;
        protected System.Web.UI.WebControls.PlaceHolder HostDetailHolder;
        protected System.Web.UI.WebControls.Label lblUsrName;
        protected System.Web.UI.WebControls.Label lblUsrEmail;
        protected System.Web.UI.WebControls.Label lblUsrLogin;
        protected System.Web.UI.WebControls.Label lblUsrTimeZone;
        protected System.Web.UI.WebControls.Label lblUsrLang;
        protected System.Web.UI.WebControls.Label lblUsrEmailLang;
        protected System.Web.UI.WebControls.Label lblUsrBlockedEmail;
        protected System.Web.UI.WebControls.Label lblUsrWork;
        protected System.Web.UI.WebControls.Label lblUsrCell;

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj; //FB 2272

        public en_ViewUserDetails()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions(); //FB 2272
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            
        }

        public void BindUserData()
        {
            try
            {
                if (Session["outXML"] == null)
                    return;
                else if (Session["outXML"].ToString() == "")
                    return;

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(Session["outXML"].ToString());

                if (xmldoc.SelectSingleNode("conference/confInfo/hostName") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText.Trim() != "")
                        lblUsrName.Text = xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText.ToString();

                if (xmldoc.SelectSingleNode("conference/confInfo/hostEmail") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostEmail").InnerText.Trim() != "")
                        lblUsrEmail.Text = xmldoc.SelectSingleNode("conference/confInfo/hostEmail").InnerText.Trim().ToString();

                if (xmldoc.SelectSingleNode("conference/confInfo/hostLogin") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostLogin").InnerText.Trim() != "")
                        lblUsrLogin.Text = xmldoc.SelectSingleNode("conference/confInfo/hostLogin").InnerText.Trim().ToString();

                if (xmldoc.SelectSingleNode("conference/confInfo/hostTimeZone") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostTimeZone").InnerText.Trim() != "")
                    {
                        String tzID = xmldoc.SelectSingleNode("conference/confInfo/hostTimeZone").InnerText.Trim().ToString();
                        XmlNodeList nodes = xmldoc.SelectNodes("/conference/confInfo/timezones/timezone");
                        int length = nodes.Count;
                        for (int i = 0; i < length; i++)
                            if (nodes[i].SelectSingleNode("timezoneID").InnerText.Equals(tzID))
                                lblUsrTimeZone.Text = nodes[i].SelectSingleNode("timezoneName").InnerText;
                    }

                if (xmldoc.SelectSingleNode("conference/confInfo/hostLang") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostLang").InnerText.Trim() != "")
                        lblUsrLang.Text = xmldoc.SelectSingleNode("conference/confInfo/hostLang").InnerText.Trim().ToString();

                if (xmldoc.SelectSingleNode("conference/confInfo/hostEmailLang") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostEmailLang").InnerText.Trim() != "")
                        lblUsrEmailLang.Text = xmldoc.SelectSingleNode("conference/confInfo/hostEmailLang").InnerText.Trim().ToString();

                lblUsrBlockedEmail.Text = obj.GetTranslatedText("No"); //FB 2272
                if (xmldoc.SelectSingleNode("conference/confInfo/hostMailBlocked") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostMailBlocked").InnerText.Trim() != "")
                        if (xmldoc.SelectSingleNode("conference/confInfo/hostMailBlocked").InnerText.Trim().ToString() == "1")
                            lblUsrBlockedEmail.Text = obj.GetTranslatedText("Yes"); //FB 2272

                if (xmldoc.SelectSingleNode("conference/confInfo/hostWork") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostWork").InnerText.Trim() != "")
                        lblUsrWork.Text = xmldoc.SelectSingleNode("conference/confInfo/hostWork").InnerText.Trim().ToString();

                if (xmldoc.SelectSingleNode("conference/confInfo/hostCell") != null)
                    if (xmldoc.SelectSingleNode("conference/confInfo/hostCell").InnerText.Trim() != "")
                        lblUsrCell.Text = xmldoc.SelectSingleNode("conference/confInfo/hostCell").InnerText.Trim().ToString();
            }
            catch (Exception ex)
            {
                log.Trace("BindUserData :" + ex.Message);
            }

        }
    }
}
