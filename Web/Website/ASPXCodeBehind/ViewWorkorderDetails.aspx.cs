/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System.IO;
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using System.Globalization;
using System.Web;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Workorder
{
    public partial class WorkorderDetails : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblName;
        protected System.Web.UI.WebControls.Label lblPersonInCharge;
        protected System.Web.UI.WebControls.Label lblStartBy;
        protected System.Web.UI.WebControls.Label lblCompletedBy;
        protected System.Web.UI.WebControls.Label lblComments;
        protected System.Web.UI.WebControls.Label lblRooms;
        protected System.Web.UI.WebControls.Label lblDeliveryCost;
        protected System.Web.UI.WebControls.Label lblServiceCharges;
        protected System.Web.UI.WebControls.Label lblTotalCharges;
        protected System.Web.UI.WebControls.Label lblStatus;
        protected System.Web.UI.WebControls.DataGrid dgItems;
        protected System.Web.UI.WebControls.DropDownList lstTimezones;
        protected System.Web.UI.WebControls.DropDownList lstServices;
        protected System.Web.UI.WebControls.Label lblTimezone;
        protected System.Web.UI.HtmlControls.HtmlInputHidden tempText;  //WO Bug Fix
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.HtmlControls.HtmlTableCell trSC;
        protected System.Web.UI.HtmlControls.HtmlTableCell trDC;
        protected System.Web.UI.HtmlControls.HtmlTableCell trSC1;
        protected System.Web.UI.HtmlControls.HtmlTableCell trDC1;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCatering;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNonCatering;

        protected System.Web.UI.WebControls.Label lblCateringName;
        protected System.Web.UI.WebControls.Label lblPrice;
        protected System.Web.UI.WebControls.Label lblPComments;
        protected System.Web.UI.WebControls.Label lblDeliverByDateTime;
        protected System.Web.UI.WebControls.Label lblCateringServiceType;
        protected System.Web.UI.WebControls.Label lblCateringRoomName;
        protected System.Web.UI.WebControls.DataGrid dgMenus;
        protected System.Web.UI.WebControls.Image ImgRoomLyout; //FB 2214
        protected System.Web.UI.HtmlControls.HtmlTableCell lablRMLO;
        protected System.Web.UI.HtmlControls.HtmlTableCell lablIMGRMLO;
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        //Image Project...
        String fName;
        byte[] imgArray = null;
        myVRMNet.ImageUtil imageUtilObj = null;
        string fileext;
        // WO Bug Fix
        protected String tformat = "hh:mm tt"; 
        protected string language = ""; //FB 1830
        protected string currencyFormat = "";//FB 1830
        //FB 1830
        CultureInfo cInfo = null;
        decimal tmpVal = 0;

        public WorkorderDetails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();

            imageUtilObj = new myVRMNet.ImageUtil(); //Image Project
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = string.Empty;//ZD_T
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ViewWorkorderDetails.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                //FB 1830- Starts
                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
               
                if (Session["CurrencyFormat"] == null)
                    Session["CurrencyFormat"] = ns_MyVRMNet.vrmCurrencyFormat.dollar;

                currencyFormat = Session["CurrencyFormat"].ToString();
                
                cInfo = new CultureInfo(Session["NumberFormat"].ToString());

				//FB 1830- End
                //WO Bug Fix
                //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 Ends

                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("2"))
                        GetProviderWorkorderDetails();
                    else
                        GetWorkOrderDetails(Request.QueryString["woID"].ToString());
                else
                    GetWorkOrderDetails(Request.QueryString["woID"].ToString());
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }

        }

        protected void GetProviderWorkorderDetails()
        {
            try
            {
                //trNonCatering.Visible = false;
                //trCatering.Visible = true;
                txtType.Text = Request.QueryString["t"].ToString();
                String inXML = "<GetProviderWorkorderDetails>" + obj.OrgXMLElement() + "<ConfID>" + Request.QueryString["confID"].ToString() + "</ConfID><WorkorderID>" + Request.QueryString["woID"].ToString() + "</WorkorderID><Type>2</Type></GetProviderWorkorderDetails>";//Organization Module Fixes
                String outXML = obj.CallMyVRMServer("GetProviderWorkorderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetProviderWorkorderDetails: " + outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    log.Trace(xmldoc.InnerXml);
                    lblCateringName.Text = xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/Name").InnerText;
                    lblCateringRoomName.Text = xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/RoomName").InnerText;
                    lblCateringServiceType.Text = lstServices.Items.FindByValue(xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/SelectedService").InnerText).Text;
                    //Code added for FB Issue 1073
                    //lblDeliverByDateTime.Text = xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/DeliverByDate").InnerText + " " + xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/DeliverByTime").InnerText;
                    lblDeliverByDateTime.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/DeliverByDate").InnerText) + " "
                        + DateTime.Parse(xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/DeliverByTime").InnerText).ToString(tformat);
                    //Code added for FB Issue 1073
                    //FB 1686
                    //lblPrice.Text = xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/Price").InnerText;
                    lblPrice.Text = Decimal.Parse(xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/Price").InnerText).ToString("0.00");
                    //FB 1830
                    decimal.TryParse(lblPrice.Text, out tmpVal);
                    lblPrice.Text = tmpVal.ToString("n", cInfo);
                    tmpVal = 0;

                    lblPComments.Text = xmldoc.SelectSingleNode("//GetProviderWorkorderDetails/Workorders/Workorder/Comments").InnerText;

                    XmlNodeList nodes = xmldoc.SelectNodes("//GetProviderWorkorderDetails/Workorders/Workorder/MenuList/Menu");
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            decimal.TryParse(dr["Quantity"].ToString(), out tmpVal);
                            dr["Quantity"] = tmpVal.ToString("n0", cInfo);
                            tmpVal = 0;
                        }
                        dgMenus.DataSource = ds.Tables[0];
                        dgMenus.DataBind();
                        foreach (DataGridItem dgi in dgMenus.Items)
                        {
                            DataGrid dgMenuItems = (DataGrid)dgi.FindControl("dgMenuItems");
                            foreach (XmlNode node in nodes)
                                if (dgi.Cells[0].Text.Equals(node.SelectSingleNode("ID").InnerText))
                                {
                                    XmlNodeList subNodes = node.SelectNodes("ItemsList/Item");
                                    ds = new DataSet();
                                    foreach (XmlNode subNode in subNodes)
                                    {
                                        xtr = new XmlTextReader(subNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                                    }
                                    if (ds.Tables.Count > 0)
                                    {
                                        dgMenuItems.DataSource = ds.Tables[0];
                                        dgMenuItems.DataBind();
                                        dgMenuItems.Attributes.Add("style", "display:none");
                                    }
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void ExportToPDF(object sender, EventArgs e)
        {
            try
            {
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.LeftMargin = 5;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 5;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;

                pdfConverter.PdfDocumentOptions.ShowHeader = false;

                pdfConverter.PdfFooterOptions.FooterText = "myVRM Version " + Application["Version"].ToString() + ",(c)Copyright " + Application["CopyrightsDur"].ToString() + " myVRM.com. All Rights Reserved."; //FB 1648
                pdfConverter.PdfFooterOptions.FooterTextColor = System.Drawing.Color.Blue;
                pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                int upYear = obj.GetYear(Session["systemDate"].ToString());
                int upMonth = obj.GetMonth(Session["systemDate"].ToString());
                int upDay = obj.GetDay(Session["systemDate"].ToString());
                int upHour = obj.GetHour(Session["systemTime"].ToString());
                int upMinute = obj.GetMinute(Session["systemTime"].ToString());
                String upSet = obj.GetTimeSet(Session["systemTime"].ToString());
                DateTime UserTime = new DateTime(upYear, upMonth, upDay, upHour, upMinute, 0);
                pdfConverter.PdfFooterOptions.FooterText += "\n" + UserTime.ToString("MMMM dd, yyyy, hh:mm tt ") + Session["systemTimeZone"].ToString();
                pdfConverter.LicenseKey = "kGb8Fr0gg2spzZd/SluMYY+Bv/RxuZmz6thATnaDnlkD20HkaCEyR4X+P9QqabXI";
                string html = tempText.Value; //WO Bug Fix
                //"<html><body><TBODY><TR><TD align=middle colSpan=3><H3><SPAN id=lblHeader>Manage Conference</SPAN><INPUT id=cmd style='BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 0px; BORDER-BOTTOM: 0px' name=cmd> </H3></TD></TR><TR><TD align=middle colSpan=3><BR><BR><B><SPAN id=lblAlert style='COLOR: red'></SPAN></B></TD></TR><TR><TD align=middle colSpan=3><TABLE width='90%' border=0><TBODY><TR><TD><INPUT class=btprint id=lblConfID style='BORDER-LEFT-COLOR: white; BORDER-BOTTOM-COLOR: white; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: white; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: white; BORDER-RIGHT-COLOR: white; BORDER-BOTTOM-STYLE: none' value=2155 name=lblConfID> <TABLE><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right width=100>Title:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfName style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana'>VetPro</SPAN> </TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right>Unique ID:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblConfUniqueID style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: red; FONT-FAMILY: Verdana'>6067</SPAN></TD><TD class=btprint style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=left width=200 rowSpan=8><TABLE id=tblActions style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 100%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader style='HEIGHT: 25px'><TD class=tableHeader>Actions</TD></TR><TR id=trCancel style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell2><A onclick=javascript:btnDeleteConference_Click() href='#'>Cancel</A> </TD></TR><TR id=trClone style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell4><A id=btnClone href='javascript:__doPostBack('btnClone','')'>Clone</A></TD></TR><TR id=trEdit style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell5><A id=btnEdit href='javascript:__doPostBack('btnEdit','')'>Edit</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPDF onclick=javascript:pdfReport(); href='javascript:__doPostBack('btnPDF','')'>Export to PDF</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPrint onclick=javascript:printpage(); href='javascript:__doPostBack('btnPrint','')'>Print</A></TD></TR><TR id=TableRow4 style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell6><A id=btnOutlook onclick='javascript:saveToOutlookCalendar('0','0','1','');' href='javascript:__doPostBack('btnOutlook','')'>Save to Outlook</A></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold' align=right width=100>Host:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfHost style='FONT-WEIGHT: normal'><A href='mailto:vrmadmin@expeditevcs.com'>VRM Administrator</A></SPAN></TD><TD style='FONT-WEIGHT: bold' align=right>Password:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblPassword style='FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN>&nbsp;</TD></TR><TR><TD style='FONT-WEIGHT: bold' vAlign=top align=right width=100>Date:</TD><TD style='FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left width=300><SPAN id=lblConfDate style='FONT-WEIGHT: normal'></SPAN><SPAN id=lblConfTime style='FONT-WEIGHT: normal'>(GMT-05:00) EST</SPAN> <SPAN id=lblTimezone>Custom Date Selection: 9/13/2007, 9/20/2007, 9/27/2007</SPAN><INPUT id=Recur style='BORDER-LEFT-COLOR: transparent; BORDER-BOTTOM-COLOR: transparent; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: transparent; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 10px; BORDER-RIGHT-COLOR: transparent; BORDER-BOTTOM-STYLE: none' value=26&amp;03&amp;00&amp;PM&amp;90#5#9/13/2007&amp;9/20/2007&amp;9/27/2007 name=Recur> <!--26&05&00&PM&60#1&2&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#7/24/2007&2&5&-1--></TD><TD style='FONT-WEIGHT: bold' vAlign=top align=right>Duration:</TD><TD style='HEIGHT: 21px' vAlign=top align=left width=200 colSpan=2><SPAN id=lblConfDuration style='FONT-WEIGHT: normal; FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=right width=100>Status:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=left><SPAN id=lblStatus style='FONT-WEIGHT: normal'>Scheduled</SPAN></TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right>Type:</TD><TD align=left colSpan=2><SPAN id=lblConfType style='FONT-WEIGHT: normal'>Room Conference</SPAN> </TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Public:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=4><SPAN id=lblPublic style='FONT-WEIGHT: normal'>No</SPAN> <SPAN id=lblRegistration style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Files:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblFiles style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Description:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblDescription style='FONT-WEIGHT: normal'>Laura Graves, x6969</SPAN>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=right colSpan=3><INPUT class=btprint id=chkExpandCollapse onclick=javascript:ExpandAll() type=checkbox>Collapse All</TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=2><IMG id=img_LOC onclick='ShowHideRow('LOC', this,false)' src='image/loc/nolines_minus.gif' border=0>Locations <SPAN id=lblLocCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(1 rooms)</SPAN> </TD></TR><TR id=tr_LOC><TD width='5%'>&nbsp;</TD><TD style='FONT-WEIGHT: bold' vAlign=top align=left><SPAN id=lblLocation style='FONT-WEIGHT: normal'>Ohio &gt; Chillicothe &gt; <A onclick='javascript:chkresource('97')' href='#'>Chillicothe b1 r285 Training Room</A><BR></SPAN></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=4><IMG id=img_PAR onclick='ShowHideRow('PAR', this,false)' src='image/loc/nolines_minus.gif' border=0>Participants <SPAN id=lblPartyCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(No participants)</SPAN></TD></TR><TR id=tr_PAR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=middle colSpan=4 rowSpan=3><TABLE id=tblNoParty style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 90%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader id=TableRow6 style='HEIGHT: 30px'><TD class=tableHeader id=TableCell7>Name</TD><TD class=tableHeader id=TableCell8>Email</TD><TD class=tableHeader id=TableCell9>Status</TD></TR><TR id=TableRow7 style='FONT-SIZE: x-small; FONT-FAMILY: Verdana; HEIGHT: 30px; BACKGROUND-COLOR: #e0e0e0' vAlign=center align=middle><TD id=TableCell10 colSpan=3>There are no participants in this conference.</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3><CENTER></CENTER></TD></TR><TR><TD align=middle colSpan=3></TD></TR></TBODY></body></html>";
                //pdfConverter.LicenseKey = "put your serial number here";
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(html);

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                String downloadName = "WorkorderDetails.pdf";
                response.AddHeader("Content-Disposition", "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();

            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ExportToPDF:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void GetWorkOrderDetails(String woID)
        {
            try
            {
                string pathName = "", RoomLyImg=""; //FB 2214
                String inxml = "<login>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inxml += "  <ConfID>" + Request.QueryString["confID"].ToString() + "</ConfID>";
                inxml += "  <WorkorderID>" + woID + "</WorkorderID>";
                inxml += "</login>";
                log.Trace(inxml);
                String outxml = obj.CallMyVRMServer("GetWorkOrderDetails", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outxml));
                log.Trace(outxml);

                if (outxml.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    lblName.Text = xmldoc.SelectSingleNode("//WorkOrder/Name").InnerText;
                    lblPersonInCharge.Text = obj.GetMyVRMUserName(xmldoc.SelectSingleNode("//WorkOrder/AssignedToID").InnerText);
                    //Code addded for FB Issue 1073
                    //lblStartBy.Text = xmldoc.SelectSingleNode("//WorkOrder/StartByDate").InnerText + "<br>" + DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/StartByTime").InnerText).ToString("hh:mm tt");
                    //lblCompletedBy.Text = xmldoc.SelectSingleNode("//WorkOrder/CompletedByDate").InnerText + "<br>" + DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/CompletedByTime").InnerText).ToString("hh:mm tt");
                    lblStartBy.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//WorkOrder/StartByDate").InnerText) + "<br>" + DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/StartByTime").InnerText).ToString(tformat);  // WO Bug Fix
                    lblCompletedBy.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//WorkOrder/CompletedByDate").InnerText) + "<br>" + DateTime.Parse(xmldoc.SelectSingleNode("//WorkOrder/CompletedByTime").InnerText).ToString(tformat); // WO Bug Fix
                    //Code added for FB Issue 1073
                    lblComments.Text = xmldoc.SelectSingleNode("//WorkOrder/Comments").InnerText;
                    lblRooms.Text = xmldoc.SelectSingleNode("//WorkOrder/RoomName").InnerText;
                    txtType.Text = xmldoc.SelectSingleNode("//WorkOrder/Type").InnerText;
                    //FB 2214 Starts
                    if (xmldoc.SelectSingleNode("//WorkOrder/RoomLayoutData") != null)
                    {
                        if (txtType.Text == "3")
                        {
                            //ZD 100237 start
                            if (lablIMGRMLO != null)
                            {
                                lablRMLO.Visible = true;
                                lablIMGRMLO.Visible = true;
                            }
                            else
                            {
                                lablRMLO.Visible = false;
                            }
                            //ZD 100237 End
                        }
                        RoomLyImg = xmldoc.SelectSingleNode("//WorkOrder/RoomLayoutData").InnerText.ToString();
                        if (RoomLyImg != "")
                        {
                            imgArray = imageUtilObj.ConvertBase64ToByteArray(RoomLyImg);
                            fName = xmldoc.SelectSingleNode("//WorkOrder/RoomLayout").InnerText;

                            pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/" + fName;
                            if (!File.Exists(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName))
                                File.Delete((Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName));

                            imgArray = imageUtilObj.ConvertBase64ToByteArray(RoomLyImg);
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName, ref imgArray);//FB 1830

                            ImgRoomLyout.ImageUrl = pathName;
                            ImgRoomLyout.ToolTip = fName;
                        }
                    }
                    //FB 2214 Ends
                    lblDeliveryCost.Text = Double.Parse(xmldoc.SelectSingleNode("//WorkOrder/DeliveryCost").InnerText).ToString("0.00");
                    //FB 1830
                    decimal.TryParse(lblDeliveryCost.Text, out tmpVal);
                    lblDeliveryCost.Text = tmpVal.ToString("n", cInfo);
                    tmpVal = 0;
                    lblServiceCharges.Text = Double.Parse(xmldoc.SelectSingleNode("//WorkOrder/ServiceCharge").InnerText).ToString("0.00");
                    decimal.TryParse(lblServiceCharges.Text, out tmpVal);
                    lblServiceCharges.Text = tmpVal.ToString("n", cInfo);
                    tmpVal = 0;
                    lblTotalCharges.Text = Double.Parse(xmldoc.SelectSingleNode("//WorkOrder/TotalCost").InnerText).ToString("0.00");
                    decimal.TryParse(lblTotalCharges.Text, out tmpVal);
                    lblTotalCharges.Text = tmpVal.ToString("n", cInfo);
                    tmpVal = 0;

                    if (xmldoc.SelectSingleNode("//WorkOrder/Status").InnerText.Equals("0"))
                        lblStatus.Text = obj.GetTranslatedText("Pending");//FB 1830 - Translation
                    else
                        lblStatus.Text = obj.GetTranslatedText("Completed");//FB 1830 - Translation
                    XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrder/ItemList/Item");
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    foreach (XmlNode node in nodes)
                    {
                        //FB 1830
                        if (!node.SelectSingleNode("QuantityRequested").InnerText.Equals("0") || node.SelectSingleNode("QuantityRequested").InnerText.Equals("0.00"))
                        {
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                    }
                    DataView dv = new DataView();
                    DataTable dt = new DataTable();

                    if (ds.Tables.Count > 0)
                    {
                        dv = new DataView(ds.Tables[0]);
                        dt = dv.Table;

                        if (dt.Columns.Contains("ItemCost").Equals(false))
                        {
                            dt.Columns.Add("ItemCost");
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            //FB 1830
                            dr["ItemCost"] = (Double.Parse(dr["Price"].ToString()) + Double.Parse(dr["ServiceCharge"].ToString()) + Double.Parse(dr["DeliveryCost"].ToString())) * Double.Parse(dr["QuantityRequested"].ToString());

                            decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                            dr["ServiceCharge"] = tmpVal.ToString("n", cInfo);
                            tmpVal = 0;

                            decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                            dr["DeliveryCost"] = tmpVal.ToString("n", cInfo);
                            tmpVal = 0;

                            decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                            dr["Price"] = tmpVal.ToString("n", cInfo);
                            tmpVal = 0;                            


                            decimal.TryParse(dr["ItemCost"].ToString(), out tmpVal);
                            dr["ItemCost"] = tmpVal.ToString("n", cInfo);
                            tmpVal = 0;

                            decimal.TryParse(dr["QuantityRequested"].ToString(), out tmpVal);
                            dr["QuantityRequested"] = tmpVal.ToString("n0", cInfo);
                            tmpVal = 0;

                            //dr["ServiceCharge"] = Double.Parse(dr["ServiceCharge"].ToString()).ToString("0.00");
                            //dr["DeliveryCost"] = Double.Parse(dr["DeliveryCost"].ToString()).ToString("0.00");
                            //dr["Price"] = Double.Parse(dr["Price"].ToString()).ToString("0.00");
                            //dr["ItemCost"] = (Double.Parse(dr["Price"].ToString()) + Double.Parse(dr["ServiceCharge"].ToString()) + Double.Parse(dr["DeliveryCost"].ToString())) * Double.Parse(dr["QuantityRequested"].ToString());
                            //dr["ItemCost"] = Double.Parse(dr["ItemCost"].ToString()).ToString("0.00");
                           
                        }
                    }
                    dgItems.DataSource = dt;
                    dgItems.DataBind();
                    if (txtType.Text.Equals("1"))
                    {
                        trDC.Visible = true;
                        trSC.Visible = true;
                        trDC1.Visible = true;
                        trSC1.Visible = true;
                        dgItems.Columns[5].Visible = true;
                        dgItems.Columns[6].Visible = true;
                    }
                    String sel = "26";
                    obj.GetTimezones(lstTimezones, ref sel);
                    lblTimezone.Text = lstTimezones.Items.FindByValue(xmldoc.SelectSingleNode("//WorkOrder/Timezone").InnerText).Text;

                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetWorkOrderDetails:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void ExportToEXCEL(Object sender, EventArgs e)
        {
            try
            {
                dgItems.Columns.RemoveAt(3);
                //String strScript = "<script>ExportToExcel();</script>";
                //RegisterClientScriptBlock("exportToExcel", strScript);
                String FileName = "workorderdetails.xls";
                Response.Charset = "";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("Windows-1252"); //FB 1830
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "inline; filename=" + FileName);
                
                Response.Write(tempText.Value);//WO Bug Fix
                Response.End();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadCateringServices(Object sender, EventArgs e)
        {
            DropDownList lstServices = (DropDownList)sender;
            obj.GetCateringServices(lstServices);
        }

        //Image Project..
        protected void BindImages(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    string imCont = "";
                    string fullPath = "";
                    fName = "";

                    if (row["ImageName"] != null)
                        fName = row["ImageName"].ToString().Trim();  // Bug Fixing

                    if (row["Image"] != null)
                        imCont = row["Image"].ToString().Trim();

                    string pathName = "resource";

                    if (Request.QueryString["t"].ToString().Equals("2"))
                        pathName = "food";

                    if (Request.QueryString["t"].ToString().Equals("3"))
                        pathName = "Housekeeping";

                    fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/image/" + pathName + "/" + fName; //FB 1830

                    if (imCont != "")
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imCont);

                        if (File.Exists(Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName))
                            File.Delete(Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName);

                        WriteToFile(Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + fName, ref imgArray);

                        Image imgContrl = (Image)e.Item.FindControl("imgItem");
                        imgContrl.ImageUrl = fullPath;

                        //myVRMWebControls.ImageControl imgCtrl = (myVRMWebControls.ImageControl)e.Item.FindControl("imgItem");

                        //MemoryStream ms = new MemoryStream(imgArray, 0, imgArray.Length);
                        //ms.Write(imgArray, 0, imgArray.Length);

                        //if (fileext == "gif")
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        //else
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        //imgCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        //imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                        ////imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms);
                        //imgCtrl.Visible = true;
                        //imCont = null;

                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindImages:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("WriteToFile:" + ex.Message);//ZD 100263
            }
        }
    }


}