﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Web.UI;

namespace ns_WorkingDays
{
    public partial class WorkingDays : System.Web.UI.Page
    {
        #region protected Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        MyVRMNet.LoginManagement objLogin;
        MyVRMNet.Util utilObj;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMonth;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWeek;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelectYear;
        protected System.Web.UI.WebControls.ImageButton imgMonthOption;
        protected System.Web.UI.WebControls.ImageButton imgWeekOption;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMonthdays;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnWeekdays;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFebDays;
        protected System.Web.UI.WebControls.TextBox txtMonthYear;
        protected System.Web.UI.WebControls.TextBox txtWeekYear;
        protected System.Web.UI.HtmlControls.HtmlTable tblMonth;
        protected System.Web.UI.HtmlControls.HtmlTableRow trlistingYears;
        protected System.Web.UI.HtmlControls.HtmlTable tblMonthLink;
        protected AjaxControlToolkit.TabContainer WorkingDaysTabs;

        protected System.Web.UI.WebControls.Button btnMonthSubmit;//FB 2670
        protected System.Web.UI.WebControls.Button btnWeekSubmit;//FB 2670


        #endregion

        #region WorkingDays

        public WorkingDays()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objLogin = new MyVRMNet.LoginManagement();
            utilObj = new MyVRMNet.Util();
        }

        #endregion

        #region Page_Load

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("WorkingDays.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                Int32 currentYear = DateTime.Now.Year;
                if (txtMonthYear.Text == "")
                    txtMonthYear.Text = currentYear.ToString();
                if (txtWeekYear.Text == "")
                    txtWeekYear.Text = currentYear.ToString();

                errLabel.Text = "";

                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    btnMonthSubmit.Enabled = false;
                    //btnMonthSubmit.ForeColor = System.Drawing.Color.Gray;
                    btnWeekSubmit.Enabled = false;
                    //btnWeekSubmit.ForeColor = System.Drawing.Color.Gray;
                    btnMonthSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    btnWeekSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                }
                // FB 2796 Start
                else
                {
                    btnMonthSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796
                    btnWeekSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796
                }
                //FB 2796 End
                if (!IsPostBack)
                {
                    SetDays(currentYear);
                    hdnFebDays.Value = DateTime.DaysInMonth(DateTime.Now.Year, 2).ToString();
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region BindData

        private void BindData()
        {
            string days = "", input = "", name = "", Month = "", week = "";
            int id = 0, currentMonthYear = 0, currentWeekYear = 0;
            TextBox monthTXT = null;
            TextBox weekTXT = null;
            StringBuilder inXML = new StringBuilder();
            StringBuilder inXML1 = new StringBuilder();
            try
            {
                currentMonthYear = Convert.ToInt32(txtMonthYear.Text);
                inXML.Append("<GetMonthlydays>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<currentYear>" + currentMonthYear + "</currentYear>");
                inXML.Append("</GetMonthlydays>");
                String outXML = obj.CallMyVRMServer("GetMonthlydays", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetMonthlydays/MonthlyTypes/MonthlyType");
                    string[] monthname = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                    foreach (XmlNode node in nodes)
                    {
                        id = Int32.Parse(node.SelectSingleNode("ID").InnerText);
                        days = node.SelectSingleNode("MonthWorkingDays").InnerText;
                        input = monthname[id - 1];
                        name = input.Substring(0, 3);
                        Month = "WorkingdaysTabs$MonthlyView$Txt" + name;
                        monthTXT = (TextBox)Page.FindControl(Month);
                        monthTXT.Text = days;

                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }

                currentWeekYear = Convert.ToInt32(txtWeekYear.Text);
                inXML1.Append("<GetWeeklydays>");
                inXML1.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML1.Append(obj.OrgXMLElement());
                inXML1.Append("<currentYear>" + currentWeekYear + "</currentYear>");
                inXML1.Append("</GetWeeklydays>");
                String outXML1 = obj.CallMyVRMServer("GetWeeklydays", inXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML1.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML1);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetWeeklydays/WeeklyTypes/WeeklyType");
                    foreach (XmlNode node in nodes)
                    {
                        id = Int32.Parse(node.SelectSingleNode("ID").InnerText);
                        days = node.SelectSingleNode("WeekWorkingDays").InnerText;
                        week = "WorkingdaysTabs$WeeklyView$TxtWeek" + id;
                        weekTXT = (TextBox)Page.FindControl(week);
                        weekTXT.Text = days;

                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML1);
                    errLabel.Visible = true;
                }

                GetWorkingYears();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }

        #endregion

        #region SetDays
        protected void SetDays(int selectedYear)
        {
            DateTime startofweek = new DateTime();
            DateTime endofweek = new DateTime();
            DateTime startofmonth = new DateTime();
            DateTime endofmonth = new DateTime();

            try
            {
                StringBuilder inXML = new StringBuilder();
                string[] names = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));

                inXML.Append("<SetDays>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<Year>" + selectedYear + "</Year>");
                inXML.Append("<SetMonth>");
                for (int n = 1; n <= 12; n++)
                {
                    utilObj.GetStartAndEndOfMonth(selectedYear, n, out startofmonth, out endofmonth);
                    inXML.Append("<MonthDetails>");
                    inXML.Append("<MonthNo>" + n + "</MonthNo>");
                    inXML.Append("<MonthName>" + names[n - 1].ToString() + "</MonthName>");
                    inXML.Append("<MonthWorkingDays>21</MonthWorkingDays>");
                    inXML.Append("<MonthStartDate>" + startofmonth + "</MonthStartDate>");
                    inXML.Append("<MonthEndDate>" + endofmonth + "</MonthEndDate>");
                    inXML.Append("<Year>" + selectedYear + "</Year>");
                    inXML.Append("</MonthDetails>");
                }
                inXML.Append("</SetMonth>");

                inXML.Append("<SetWeek>");
                for (int n = 1; n <= 52; n++)
                {
                    utilObj.GetStartAndEndOfWeek(selectedYear, n, out startofweek, out endofweek);
                    inXML.Append("<WeekDetails>");
                    inXML.Append("<WeekNo>" + n + "</WeekNo>");
                    inXML.Append("<Weeks>" + "Week" + n.ToString().Trim() + "</Weeks>");
                    inXML.Append("<WeekWorkingDays>5</WeekWorkingDays>");
                    inXML.Append("<WeekStartDate>" + startofweek + "</WeekStartDate>");
                    inXML.Append("<WeekEndDate>" + endofweek + "</WeekEndDate>");
                    inXML.Append("<Year>" + selectedYear + "</Year>");
                    inXML.Append("</WeekDetails>");
                }
                inXML.Append("</SetWeek>");
                inXML.Append("</SetDays>");

                String outXML = obj.CallMyVRMServer("Setdays", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                BindData();
            }

            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SetDays:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region UpdateWorkingDays

        #region UpdateMonthWorkingDays
        protected void UpdateMonthWorkingDays(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                Int32 currentMonthYear = DateTime.Now.Year;
                if (txtMonthYear.Text != "")
                    currentMonthYear = Convert.ToInt32(txtMonthYear.Text);

                SetDays(currentMonthYear);

                string[] monthdays = hdnMonthdays.Value.Split('|');
                string[] names = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                inXML.Append("<SetMonthlyDays>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append(obj.OrgXMLElement());
                for (int n = 1; n <= 12; n++)
                {
                    if (monthdays[n].ToString() == "-1")
                    {
                        continue;
                    }
                    inXML.Append("<MonthDetails>");
                    inXML.Append("<MonthNo>" + n + "</MonthNo>");
                    inXML.Append("<MonthName>" + names[n - 1].ToString() + "</MonthName>");
                    inXML.Append("<MonthWorkingDays>" + monthdays[n].ToString() + "</MonthWorkingDays>");
                    inXML.Append("<CurrentYear>" + currentMonthYear + "</CurrentYear>");
                    inXML.Append("</MonthDetails>");
                }
                inXML.Append("</SetMonthlyDays>");

                String outXML = obj.CallMyVRMServer("SetMonthlydays", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowSuccessMessage();
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                BindData();
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("UpdateMonthWorkingDays:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        # endregion

        #region UpdateWeekWorkingDays
        protected void UpdateWeekWorkingDays(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML1 = new StringBuilder();
                string[] weekdays = hdnWeekdays.Value.Split('|');
                Int32 currentWeekYear = DateTime.Now.Year;
                if (txtMonthYear.Text != "")
                    currentWeekYear = Convert.ToInt32(txtWeekYear.Text);

                SetDays(currentWeekYear);

                inXML1.Append("<SetWeeklydays>");
                inXML1.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML1.Append(obj.OrgXMLElement());
                for (int n = 1; n <= 52; n++)
                {
                    if (weekdays[n].ToString() == "-1")
                    {
                        continue;
                    }
                    inXML1.Append("<WeekDetails>");
                    inXML1.Append("<WeekNo>" + n + "</WeekNo>");
                    inXML1.Append("<Weeks>" + "Week" + n.ToString().Trim() + "</Weeks>");
                    inXML1.Append("<WeekWorkingDays>" + weekdays[n].ToString() + "</WeekWorkingDays>");
                    inXML1.Append("<CurrentYear>" + currentWeekYear + "</CurrentYear>");
                    inXML1.Append("</WeekDetails>");
                }
                inXML1.Append("</SetWeeklydays>");

                String outXML1 = obj.CallMyVRMServer("SetWeeklydays", inXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML1.IndexOf("<error>") < 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowSuccessMessage();
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML1);
                }
                BindData();
            }

            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("UpdateWeekWorkingDays:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        # endregion
        
        #region GetWeekDaysforYear
        /// <summary>
        /// GetWeekDaysforYear
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void GetWeekDaysforYear(object src, EventArgs e)
        {
            //DropDownList lstTemp = (DropDownList)sender;
            txtWeekYear.Text = ((DropDownList)src).SelectedValue;
            BindData();
            //lstYearConfigWeek.Items.FindByValue(lstTemp.SelectedValue).Selected = true;

        }
        #endregion

        #region GetMonthDaysforYear
        /// <summary>
        /// GetMonthDaysforYear
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void GetMonthDaysforYear(object src, EventArgs e)
        {
            //DropDownList lstTemp = (DropDownList)sender;
            txtMonthYear.Text = ((DropDownList)src).SelectedValue;
            BindData();
//            lstYearConfigMonth.Items.FindByValue(lstTemp.SelectedValue).Selected = true;
        }
        #endregion

        #region GetResetMonthly
        /// <summary>
        /// GetResetMonthly
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void GetResetMonthly(object sender, EventArgs e)
        {
            txtMonthYear.Text = DateTime.Now.Year.ToString(); //Shall use same command or differenr command? Same na - Both monthly and weekly reseted na?
            BindData();
        }
        #endregion

        #region GetResetWeekly
        /// <summary>
        /// GetResetWeekly
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void GetResetWeekly(object sender, EventArgs e)
        {
            txtWeekYear.Text = DateTime.Now.Year.ToString();
            BindData();
        }
        #endregion

        #region GetWorkingYears
        /// <summary>
        /// GetWorkingYears
        /// </summary>
        private void GetWorkingYears()
        {
            StringBuilder inXML = new StringBuilder();
            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                inXML.Append("<GetUsageReports>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<ReportType>MY</ReportType>");
                inXML.Append("</GetUsageReports>");

                String outXML = obj.CallMyVRMServer("GetUsageReports", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    xmlDoc.LoadXml(outXML);
                    DataSet ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));

                    if (ds.Tables.Count > 0)
                    {
                        DropDownList lstYearConfigMonth = (DropDownList)Page.FindControl("WorkingdaysTabs$MonthlyView$lstYearConfigMonth");
                        DropDownList lstYearConfigWeek = (DropDownList)Page.FindControl("WorkingdaysTabs$WeeklyView$lstYearConfigWeek");
                        lstYearConfigMonth.ClearSelection();
                        lstYearConfigMonth.DataSource = ds.Tables[0];
                        lstYearConfigMonth.DataBind();
                        lstYearConfigMonth.SelectedValue = txtMonthYear.Text;


                        lstYearConfigWeek.ClearSelection();
                        lstYearConfigWeek.DataSource = ds.Tables[2];
                        lstYearConfigWeek.DataBind();
                        lstYearConfigWeek.SelectedValue = txtWeekYear.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
