/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Drawing;

public partial class en_approvalstatus : System.Web.UI.Page
{
    ns_Logger.Logger log;
    myVRMNet.NETFunctions obj;

    #region Protected Data Members
    
    protected System.Web.UI.WebControls.Label lblTitle;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Label ConfName;
    protected System.Web.UI.WebControls.Label DateStr;
    protected System.Web.UI.HtmlControls.HtmlTable RoomDetailsTable;
    protected System.Web.UI.HtmlControls.HtmlTable MCUDetailsTable;
    protected System.Web.UI.HtmlControls.HtmlTable DeptDetailsTable;
    protected System.Web.UI.HtmlControls.HtmlTable SysDetailsTable;
   
    #endregion

    #region private data Members

    protected ArrayList enumList = null;
    protected String roomDetails = "";
    protected String mcuDetails = "";
    protected String deptDetails = "";
    protected String sysDetails = "";

    #endregion

    #region Pageload Event Handler

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("approvalstatus.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            

            if (!IsPostBack)
                BindData();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region BindData

    private void BindData()
    {
        String outXML = "";
        String inXML = "";
        try
        {

            enumList = new ArrayList();            
            enumList.Add("roomName");
            enumList.Add("MCUName");
            enumList.Add("departmentName");
            enumList.Add("approverName");
            enumList.Add("decision");
            enumList.Add("message");
            enumList.Add("responseTime");

            inXML += "<login>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
            inXML += "  <confID>" + Request.QueryString["confid"].ToString() + "</confID>";
            inXML += "</login>";
            //FB 2027
            outXML = obj.CallMyVRMServer("GetApprovalStatus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            //outXML = obj.CallCOM("GetApprovalStatus", inXML, Application["COM_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") < 0)
            {
                XmlDocument xmldoc = new XmlDocument();
                XmlNodeList nodes = null;
                xmldoc.LoadXml(outXML);
                
                nodes = xmldoc.SelectNodes("//room/response");
                if (nodes.Count > 0)
                {
                    roomDetails = BindDetails(nodes, "R");
                }
                else
                    RoomDetailsTable.Attributes.Add("Style", "Display:None");

                nodes = xmldoc.SelectNodes("//MCU/response");
                if (nodes.Count > 0)
                {
                    mcuDetails = BindDetails(nodes, "M");
                }
                else
                    MCUDetailsTable.Attributes.Add("Style", "Display:None");

                nodes = xmldoc.SelectNodes("//department/response");
                if (nodes.Count > 0)
                {
                   deptDetails = BindDetails(nodes, "D");
                }
                else
                    DeptDetailsTable.Attributes.Add("Style", "Display:None");

                nodes = xmldoc.SelectNodes("//system/response");
                if (nodes.Count > 0)
                {
                    sysDetails = BindDetails(nodes, "S");
                }
                else
                    SysDetailsTable.Attributes.Add("Style", "Display:None");
            }
            else
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindDatails

    private String BindDetails(XmlNodeList nodes, String type)
    {
        String strDetailsTemp = "";
        String strDetailsTemp1 = "";
        String strDetails = "";

        foreach (XmlNode node in nodes)
        {
            for (Int32 i = 0; i < enumList.Count; i++)
            {

                if ((type == "R" && (i == 1 || i == 2)) || (type == "M" && (i == 0 || i == 2))
                    || (type == "D" && (i == 0 || i == 1)) || (type == "S" && i <= 2))
                    continue;

                strDetailsTemp = "";

                if (enumList[i].ToString().ToUpper() == "DECISION")
                {
                    String decision = node.SelectSingleNode(enumList[i].ToString()).InnerText;
                    if (decision == "1")
                        strDetailsTemp = "<img border='0' src='image/icon_thumbup.gif' width='14' height='14' alt='approval'>";
                    else if (decision == "2")
                        strDetailsTemp = "<img border='0' src='image/icon_thumbdown.gif' width='14' height='14' alt='deny'>";
                    else
                        strDetailsTemp = "<img border='0' src='image/icon_warning.gif' width='14' height='14' alt='not response yet'>";                    
                }
                else
                    strDetailsTemp = node.SelectSingleNode(enumList[i].ToString()).InnerText;

                if (strDetailsTemp1 == "")
                    strDetailsTemp1 = strDetailsTemp;
                else
                    strDetailsTemp1 = strDetailsTemp1 + "~" + strDetailsTemp; //FB 1888

            }
            if (strDetails == "")
                strDetails = strDetailsTemp1;
            else
                strDetails = strDetails + "`" + strDetailsTemp1; //FB 1888
            /* Code Added For FB 1448 */
            strDetailsTemp1 = "";
        }

        strDetails = strDetails.Replace("\"", "??").Replace("'", "**"); //FB 2321

        return strDetails;
    }

    #endregion
}
