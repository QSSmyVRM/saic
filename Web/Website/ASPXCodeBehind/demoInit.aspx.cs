﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Mail;

namespace ns_demoInit
{

    #region Class  demoInit
    /// <summary>
    /// Public Class demoInit
    /// </summary>
    public partial class demoInit : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;

        #region Data Members
        /// <summary>
        /// All Data members
        /// </summary>
        protected String errMessage = "";
        ns_Logger.Logger log = null;

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if (Request.Form["fName0"] != null)
                {
                    if (Request.Form["fName0"] != "")
                        RequestFormfName0NotEmpty();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.Message);
            }



        }
        #endregion

        #region RequestFormfName0NotEmpty
        /// <summary>
        /// It executes only  while Query String [fName] Not equals empty
        /// </summary>
        private void RequestFormfName0NotEmpty()
        {
            String mailTo = "";
            String mailFrom = "";
            String mailSubject = "";
            String mailMsg = "";
            String senderName = "";
            String err_page = "";
            String fname = "";
            String lname = "";
            String email = "";
            String phone = "";
            String coName = "";
            String coPhone = "";
            String contactName = "";

            String[] invFName = null;
            String[] invLName = null;
            String[] invEmail = null;
            String fullName = "";
            String[] rDesignation = null;
            String[] rProtocol = null;
            String[] rAddress = null;

            Int32 intCtr = 0;

            try
            {
                fname = Request.Form["fname0"];
                lname = Request.Form["lname0"];
                email = Request.Form["email0"];
                phone = Request.Form["phone0"];
                coName = Request.Form["company1"];
                coPhone = Request.Form["phone1"];
                invFName = new String[1];
                invLName = new String[1];
                invEmail = new String[1];
                while (Request.Form["fname" + Convert.ToString(intCtr + 1)] != "")
                {
                    //TODO Redim Preserve not supported.
                    invFName = new String[intCtr + 1];
                    //TODO Redim Preserve not supported.
                    invLName = new String[intCtr + 1];
                    //TODO Redim Preserve not supported.
                    invEmail = new String[intCtr + 1];
                    invFName[intCtr] = Request.Form["fname" + Convert.ToString(intCtr + 1)];
                    invLName[intCtr] = Request.Form["lname" + Convert.ToString(intCtr + 1)];
                    invEmail[intCtr] = Request.Form["email" + Convert.ToString(intCtr + 1)];
                    intCtr = intCtr + 1;
                }
                if (Request.Form["cName"] == "")
                {
                    rDesignation = new String[0 + 1];
                    rProtocol = new String[0 + 1];
                    rAddress = new String[0 + 1];
                    intCtr = 0;
                    while (Request.Form["rName" + Convert.ToString(intCtr + 1)] != "")
                    {
                        //TODO Redim Preserve not supported.
                        rDesignation = new String[intCtr + 1];
                        //TODO Redim Preserve not supported.
                        rProtocol = new String[intCtr + 1];
                        //TODO Redim Preserve not supported.
                        rAddress = new String[intCtr + 1];
                        rDesignation[intCtr] = Request.Form["rName" + Convert.ToString(intCtr + 1)];
                        if (Request.Form["r" + Convert.ToString(intCtr + 1)] == "0")
                        {
                            rProtocol[intCtr] = "IP";
                        }
                        else
                        {
                            rProtocol[intCtr] = "ISDN";
                        }
                        rAddress[intCtr] = Request.Form["address" + Convert.ToString(intCtr + 1)];
                        intCtr = intCtr + 1;
                    }
                }
                else
                {
                    contactName = Request.Form["cName"];
                    contactName = Request.Form["cEmail"];
                    contactName = Request.Form["cPhone"];
                }
                mailTo = "achae@myVRMvcs.com"; //this mailTo value should get from DB.
                mailFrom = email;
                mailSubject = "myVRM Demonstration Initialize Request";
                err_page = "../showerror.aspx";
                senderName = fname + " " + lname;
                mailMsg = "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">" + "\r\n";
                mailMsg = mailMsg + "<html>";
                mailMsg = mailMsg + "<head>";
                mailMsg = mailMsg + "<meta http-equiv=\"Content-Type\"";
                mailMsg = mailMsg + "\"content=\"text/html; charset=iso-8859-1\">";
                mailMsg = mailMsg + "<title>" + mailSubject + "</title>";
                mailMsg = mailMsg + "</head>";
                mailMsg = mailMsg + "<body>";
                mailMsg = mailMsg + senderName;
                mailMsg = mailMsg + " from " + coName;
                mailMsg = mailMsg + " has requested to initialized a myVRM Demonstration Account.";
                mailMsg = mailMsg + "<br><br>";
                //request person info.
                mailMsg = mailMsg + "Company Name: " + coName + "<br>";
                mailMsg = mailMsg + "Requested by: " + senderName + "<br>";
                mailMsg = mailMsg + "Direct No.: " + phone + "<br>";
                mailMsg = mailMsg + "Company Phone: " + coPhone + "<br>";
                //invitee info.
                for (intCtr = 0; intCtr <= Convert.ToInt32(invFName[intCtr]); intCtr++) //Need Code Review
                {
                    fullName = invFName[intCtr] + " " + invLName[intCtr];
                    mailMsg = mailMsg + "Room Attendee " + Convert.ToString(intCtr + 1) + ": " + fullName + "<br>";
                    mailMsg = mailMsg + "Room Attendee's Email: " + invEmail[intCtr] + "<br><br>";
                }
                for (intCtr = 0; intCtr <= Convert.ToInt32(rDesignation[intCtr]); intCtr += 1) //Need Code Review
                {
                    mailMsg = mailMsg + "Designated Room " + Convert.ToString(intCtr + 1) + ": " + rDesignation[intCtr] + "<br>";
                    mailMsg = mailMsg + "Protocol: " + rProtocol[intCtr] + "<br>";
                    mailMsg = mailMsg + "Address: " + rAddress[intCtr] + "<br><br>";
                }
                mailMsg = mailMsg + "<BR></body>";
                mailMsg = mailMsg + "</html>";
                // On Error Resume Next (UNSUPPORT) 
                //sendMail mailFrom, senderName, mailTo, mailSubject, mailMsg, err_page
                //email = new MailMessage();
                //email.From = mailFrom;
                //email.To = mailTo;
                //email.Subject = mailSubject;
                //email.Body = mailMsg;
                //email.BodyFormat = 0;
                //email.MailFormat = 0;
                //email.Send;
                //if (Information.Err().Number != 0)
                //{
                //    errMessage = "The requests mail cannot be sent.  Please try again later, or contact <a href='http://141.155.241.11/'>myVRM.</a>";
                //}
                //else
                //{
                //    errMessage = errMessage + "<p>&nbsp;</p><center><table width='75%' height='300'><tr><td align='center'>The requests mail was sent successfully.<br>";
                //    errMessage = errMessage + "Our representative will contact you soon.  Thank you.";
                //    errMessage = errMessage + "</td></tr>";
                //    errMessage = errMessage + "<tr><td><a href='http://localhost/v12/genlogin.aspx'>";
                //    errMessage = errMessage + "Click here</a> to go back to the Login page again.";
                //    errMessage = errMessage + "</td></tr></table></center>";
                //    //bSuccess = True
                //}
                //email = null;
                Response.Write(errMessage);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.Message);

            }
        }
        #endregion
    }

    #endregion
}
