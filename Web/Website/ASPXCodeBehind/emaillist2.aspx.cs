/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;

namespace ns_EmailList2Popup
{
    public partial class en_emaillist2 : System.Web.UI.Page
    {

        #region Protected Data members

        protected System.Web.UI.WebControls.Label LblError;
        protected System.Web.UI.HtmlControls.HtmlInputHidden sortBy;
        protected System.Web.UI.HtmlControls.HtmlInputHidden alphabet;
        protected System.Web.UI.HtmlControls.HtmlInputHidden pageNo;
        protected System.Web.UI.HtmlControls.HtmlInputHidden canAll;
        protected System.Web.UI.HtmlControls.HtmlInputHidden totalPages;
        protected System.Web.UI.HtmlControls.HtmlInputHidden xmljs;
        protected System.Web.UI.HtmlControls.HtmlInputHidden fromSearch;
        protected System.Web.UI.HtmlControls.HtmlInputHidden FirstName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden LastName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden LoginName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden HdnAudParty;//FB 1779
        #endregion        

        #region Private and Public variables

        protected String xmlstr = null;
        protected string errorMessage = "";
        protected string xmlj = "";
        protected int length = 0;
        protected int j = 0;
        protected int i = 0;
        protected string titlealign = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        protected string[] userID = null;
        protected string[] login = null;
        protected string[] strfirstName = null;
        protected string[] strlastName = null;
        protected string[] email = null;
        protected string[] survey = null; //FB 2348
        protected string title = "";
        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log;
        StringBuilder inXML = new StringBuilder();//FB 2027

        #endregion

        #region Page Load  Event Hander
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("emaillist2.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263


                obj = new myVRMNet.NETFunctions();
                
                if (Request.QueryString["audioid"] != null)//FB 1779
                    HdnAudParty.Value = Request.QueryString["audioid"];

               // GetEmailList(); //Login Management //FB 2027

                if (Session["userID"] != null || Session["userID"].ToString() != "")
                {
                    GetQueryStrings();

                    if (Request.QueryString["t"] == "g")
                        title = "Guest";
                    else
                        title = "myVRM";

                    if (Request.QueryString["srch"] != null)
                    {
                        if (Request.QueryString["srch"] != "")
                        {
                            if (Request.QueryString["srch"].ToString() == "y")
                            {
                                xmlstr = BuildSearchXML();
                            }
                        }
                    }

                    if (xmlstr == null)
                        xmlstr = BuildXML();

                    BindData(xmlstr);
                }
                else
                {
                    //Code modified on 20MAr09 - FB 412- Start
                    Response.Write("<script>self.close();</script>");
                    //Code modified on 20MAr09 - FB 412- End
                }

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = "PageLoad: " + ex.Message;

            }
        }
        #endregion

        #region GetQueryStrings

        /// <summary>
        /// User Defined Method - Building GetQueryStrings Structure
        /// </summary>

        private void GetQueryStrings()
        {

            try
            {
                if (Request.QueryString["sb"] != null)
                {
                    if (Request.QueryString["sb"].ToString().Trim() != "")
                        sortBy.Value = Request.QueryString["sb"].ToString().Trim();
                    else
                        sortBy.Value = "2";

                }
                else
                    sortBy.Value = "2";


                if (Request.QueryString["a"] != null)
                {
                    if (Request.QueryString["a"].ToString().Trim() != "")
                    {

                        alphabet.Value = Request.QueryString["a"].ToString().Trim();
                    }
                    else
                        alphabet.Value = "All"; //FB 2781
                }
                else
                    alphabet.Value = "All";

                if (Request.QueryString["pn"] != null)
                {
                    if (Request.QueryString["pn"].ToString().Trim() != "")
                    {

                        pageNo.Value = Request.QueryString["pn"].ToString().Trim();
                    }
                    else
                        pageNo.Value = "1";
                }
                else
                    pageNo.Value = "1";


                if (Request.QueryString["srch"] != null)
                {
                    if (Request.QueryString["srch"].ToString().Trim() != "")
                    {

                        fromSearch.Value = Request.QueryString["srch"].ToString().Trim();
                    }
                }
                if (Request.QueryString["fname"] != null)
                {
                    if (Request.QueryString["fname"].ToString().Trim() != "")
                    {

                        FirstName.Value = Request.QueryString["fname"].ToString().Trim();
                    }
                }

                if (Request.QueryString["lname"] != null)
                {
                    if (Request.QueryString["lname"].ToString().Trim() != "")
                    {

                        LastName.Value = Request.QueryString["lname"].ToString().Trim();
                    }
                }
                if (Request.QueryString["gname"] != null)
                {
                    if (Request.QueryString["gname"].ToString().Trim() != "")
                    {

                        LoginName.Value = Request.QueryString["gname"].ToString().Trim();
                    }
                }


            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("QueryString" + ex.Message);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "QueryString: " + ex.Message;

            }
        }
        #endregion

        #region Build XML

        private String BuildXML()
        {
            try
            {
                String cmd = "";
                //FB 2027 Starts
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("  <sortBy>" + sortBy.Value + "</sortBy>");
                inXML.Append("  <alphabet>" + alphabet.Value + "</alphabet>");
                inXML.Append("  <pageNo>" + pageNo.Value + "</pageNo>");

                if (Request.QueryString["t"].ToString() == "g")
                    cmd = "GetGuestList";
                else
                {
                    inXML.Append("<audioaddon>0</audioaddon>"); //FB 2023
                    cmd = "GetEmailList";
                }
                inXML.Append("</login>");//FB 2023

                //xmlstr = obj.CallCOM(cmd, inXML, Application["COM_ConfigPath"].ToString()); 
                xmlstr = obj.CallMyVRMServer(cmd, inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027 Ends
                
                return xmlstr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Build Search  XML

        private String BuildSearchXML()
        {
            try
            {
                String fname = "";
                String lname = "";
                String gname = "";
                String cmd = "";

                if (Request.QueryString["fname"] != null)
                    fname = Request.QueryString["fname"].ToString();
                if (Request.QueryString["lname"] != null)
                    lname = Request.QueryString["lname"].ToString();
                if (Request.QueryString["gname"] != null)
                    gname = Request.QueryString["gname"].ToString();
                //FB 2027 Starts
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("  <name>");
                inXML.Append("      <firstName>" + fname + "</firstName>");
                inXML.Append("      <lastName>" + lname + "</lastName>");
                inXML.Append("      <userName>" + gname + "</userName>");
                inXML.Append("  </name>");
                inXML.Append("  <pageNo>" + pageNo.Value + "</pageNo>");
                inXML.Append("</login>");

                if (Request.QueryString["t"].ToString() == "g")
                    cmd = "RetrieveGuest";
                else
                    cmd = "RetrieveUsers";

                xmlstr = obj.CallMyVRMServer(cmd, inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 Ends
                return xmlstr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Bind Data
        private void BindData(String xmlstr)
        {
            string internalNumber, externalNumber = "";//FB 2376
            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlstr);
                if (xmlDoc == null)
                {
                    if (Convert.ToString(Session["who_use"]) == "VRM")
                    {
                        //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";
                        //errorMessage = errorMessage + Convert.ToString(Session["outXML"]);
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + Convert.ToString(Session["outXML"]));
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                        //CatchErrors(errorMessage);
                        //Response.Redirect("underconstruction.asp");
                    }
                    else
                    {
                        //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                        //Response.Write("<br><br><p align='left'><font size=2><b>" + Convert.ToString(Session["outXML"]) + "<b></font></p>");
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + Convert.ToString(Session["outXML"])); 
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                    }

                    xmlDoc = null;
                    Response.End();
                }
                if (xmlDoc.DocumentElement.SelectNodes("sortBy").Count > 0)
                {
                    sortBy.Value = xmlDoc.DocumentElement.SelectSingleNode("sortBy").InnerText;
                }
                if (xmlDoc.DocumentElement.SelectNodes("alphabet").Count > 0)
                {
                    alphabet.Value = xmlDoc.DocumentElement.SelectSingleNode("alphabet").InnerText;
                }
                if (xmlDoc.DocumentElement.SelectNodes("pageNo").Count > 0)
                {
                    pageNo.Value = xmlDoc.DocumentElement.SelectSingleNode("pageNo").InnerText;
                }
                if (xmlDoc.DocumentElement.SelectNodes("totalPages").Count > 0)
                {
                    totalPages.Value = xmlDoc.DocumentElement.SelectSingleNode("totalPages").InnerText;
                }
                if (xmlDoc.DocumentElement.SelectNodes("canAll").Count > 0)
                {
                    canAll.Value = xmlDoc.DocumentElement.SelectSingleNode("canAll").InnerText;
                }
                else
                {
                    canAll.Value = "0";
                }

                xmlj = "";
                nodes = xmlDoc.DocumentElement.SelectNodes("user");
                length = nodes.Count;
                userID = new string[length - 1 + 1];
                login = new string[length - 1 + 1];
                strfirstName = new string[length - 1 + 1];
                strlastName = new string[length - 1 + 1];
                email = new string[length - 1 + 1];
                survey = new string[length - 1 + 1];//FB 2348
                j = 0;
                foreach (XmlNode nod in nodes)
                {
                    if (nod.SelectSingleNode("status/deleted").InnerText == "0")
                    {
                        //FB 1779 - Start
                        if (nod.SelectSingleNode("userID").InnerText.Trim() == HdnAudParty.Value)
                        {
                            continue;
                        }
                        //FB 1779 - End

                        userID[j] = nod.SelectSingleNode("userID").InnerText;
                        if (nod.SelectNodes("login").Count > 0)
                        {
                            login[j] = nod.SelectSingleNode("login").InnerText;
                        }
                        else
                        {
                            login[j] = "N/A";
                        }
                        strfirstName[j] = nod.SelectSingleNode("firstName").InnerText;
                        strlastName[j] = nod.SelectSingleNode("lastName").InnerText;
                        email[j] = nod.SelectSingleNode("email").InnerText;
						survey[j] = nod.SelectSingleNode("Survey").InnerText;//FB 2348

                        /** FB 2376 **/

                        internalNumber = "";
                        externalNumber = "";

                        if (nod.SelectSingleNode("internalBridgeNumber") != null)
                            internalNumber = nod.SelectSingleNode("internalBridgeNumber").InnerText;

                        if (nod.SelectSingleNode("externalBridgeNumber") != null)
                            externalNumber = nod.SelectSingleNode("externalBridgeNumber").InnerText;
                        /** FB 2376 **/

                        xmlj = xmlj + userID[j] + "%%" + strfirstName[j] + "%%" + strlastName[j] + "%%" + email[j] + "%%" + login[j] + "%%" + survey[j] +"%%" + internalNumber + "%%" + externalNumber + "==";//FB 1888(";;";),FB 2348  , FB 2376
                        j = j + 1;
                    }
                    xmljs.Value = xmlj;
                }

                if (length > 6)
                {
                    titlealign = "left";
                }
                else
                {
                    titlealign = "center";
                }

                node = null;
                nodes = null;
                xmlDoc = null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetEmailList
        /// <summary>
        /// GetEmailList Command converted for conferencedispatcher.asp
        /// </summary>
        private void GetEmailList() //Login Management
        {
            Int32 sb = Int32.MinValue;
            string a = "";
            Int32 pn = Int32.MinValue;

            try
            {

                string inxml = "<login>";
                inxml += "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                inxml += "<organizationID>" + HttpContext.Current.Session["organizationID"].ToString() + "</organizationID>";
                if (HttpContext.Current.Request.QueryString["sb"] != null)
                {
                    if (HttpContext.Current.Request.QueryString["sb"] == "")
                        sb = 2;
                    else
                        sb = Int32.Parse(HttpContext.Current.Request.QueryString["sb"]);
                }
                if (HttpContext.Current.Request.QueryString["a"] != null)
                {
                    if (HttpContext.Current.Request.QueryString["a"] == "")
                        a = "ALL";//FB 2781
                    else
                        a = HttpContext.Current.Request.QueryString["a"];
                }
                if (HttpContext.Current.Request.QueryString["pn"] != null)
                {
                    if (HttpContext.Current.Request.QueryString["pn"] == "")
                        pn = 1;
                    else
                        pn = Int32.Parse(HttpContext.Current.Request.QueryString["pn"]);
                }

                inxml += "<sortBy>"+ sb + "</sortBy>";
                inxml += "<alphabet>" + a + "</alphabet>";
                inxml += "<pageNo>" + pn + "</pageNo>";
                inxml += "<audioaddon>0</audioaddon>"; //FB 2023
                inxml += "</login>";


                string outXML = obj.CallMyVRMServer("GetEmailList", inxml, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

    }
}
