﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Management;
using System.Management.Instrumentation;

namespace ns_Login
{
    public partial class genlogin : System.Web.UI.Page
    {
        /* Login management - Codes changed for FB 1820 */
        /*SetApplicationVariables - method removed as it is not used anywhere */

        #region Private Data Members
        /// <summary>
        /// Declaring Data Members
        /// </summary>
        const String loginPage = "../en/genlogin.aspx";
        const String loginPageTitle = "VRM - Video Conferencing Made Simple !";
        const String company_logo = "../image/company-logo/SiteLogo.jpg"; //FB Icon
        const String mainTopImg = "../en/image/vrmtopleft.GIF";
        const String bgColor = "#FFEBCE";
        const String popUpbgColor = "#FFEBCE";

        protected Boolean wizard_enable = false;
        protected String ind = "";
        protected String WHO_USE = "";
        protected Boolean blockNSFF = false;
        protected Boolean corBroswer = true;
        //protected System.Web.UI.HtmlControls.HtmlGenericControl CompanyTagLine; //FB 2719
        protected System.Web.UI.HtmlControls.HtmlInputText UserName;
        protected System.Web.UI.HtmlControls.HtmlInputPassword UserPassword;
        protected System.Web.UI.HtmlControls.HtmlTableCell lblViewPublicConf;//FB 2858
        protected System.Web.UI.HtmlControls.HtmlTableCell lblForgotPassword;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnscreenres; //ZD 100157 //ZD 100335
       
        MyVRMNet.LoginManagement loginMgmt = null;
        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log = null;
        protected System.Web.UI.WebControls.Label errLabel;
        int versionNo = 0;
        public String companyInfo = "";
        String language = "en"; //FB 1830

       // protected System.Web.UI.WebControls.CheckBox RememberMe;//FB 2009 //ZD 100263
        bool isloginFailed = false; //FB 2027 GetHome
        public int exist = 0; // FB 2779
        string macID = ""; //ZD 100263
        #endregion

        public string mailextn = "";//FB 1943

        # region Constructor
        /// <summary>
        /// Public Constructor 
        /// </summary>
        public genlogin()
        {
            obj = new myVRMNet.NETFunctions();
            loginMgmt = new MyVRMNet.LoginManagement();
            log = new ns_Logger.Logger();

        }
        #endregion

        #region Page Load Event handler
        /// <summary>
        /// Page Load Event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Session.Clear();// ZD 100263
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheckNoSession(Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                //FB 1633 start
                loginMgmt.SetSitePaths(); 
                loginMgmt.SetSiteThemes();
                //FB 1633 end
                BindProcessorDetails();//ZD 100263
                mailextn = loginMgmt.mailextn;//FB 1943
                //ZD 100335 start
                //Session["hdnscreenresoul"] = 1024;
                //if (hdnscreenres != null)//ZD 100157
                //    Session["hdnscreenresoul"] = hdnscreenres.Value;
                if (Request.Cookies["hdnscreenres"] != null)
                    Session["hdnscreenresoul"] = Request.Cookies["hdnscreenres"].Value;
                //ZD 100335 End
                if (Session["ViewPublicConfs"] != null) //FB 2858
                {
                    if (Session["ViewPublicConfs"].ToString() == "2")
                    {
                        lblViewPublicConf.Visible = false;
                        lblForgotPassword.Attributes.Add("style", "padding-right:10px;text-align:center");                        
                    }
                }

                // FB 2779 Starts
                if(File.Exists(Server.MapPath("..\\image\\company-logo\\LoginBackground.jpg")))
                {
                    exist = 1;
                }
                // FB 2779 Ends

                if (!IsPostBack)
                {                    
                    errLabel.Text = "";

                    if (Request.QueryString["m"] == "1")
                    {
                        string themeVal = Session["ThemeType"].ToString(); // FB 2719
                        Session.Abandon();
                        Session.RemoveAll();
                        Session["ThemeType"] = themeVal; // FB 2719
                        errLabel.Text = "You have successfully logged out of myVRM.";
                        errLabel.Visible = true;
                    }
                    //ZD 100386 - Starts
                    if (Request.QueryString["m"] == "2")
                    {
                        string themeVal = Session["ThemeType"].ToString(); // FB 2719
                        Session.Abandon();
                        Session.RemoveAll();
                        Session["ThemeType"] = themeVal; // FB 2719
                        errLabel.Text = "Your session expired. Please sign in to continue...";
                        errLabel.Visible = true;
                    }
                    //ZD 100386 - End

                    if (loginMgmt.errMessage.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(loginMgmt.errMessage);
                        errLabel.Visible = true;
                    }
                    //CompanyTagLine.InnerText = loginMgmt.companyTagline; //FB 2719
                }

                loginMgmt.HeaderASP();
                loginMgmt.readaspconfigASP();
                //loginMgmt.cookiedectASP();  Remember me
                

                if (File.Exists(Application["SchemaPath"].ToString() + "\\company.ifo"))
                {
                    companyInfo = File.ReadAllText(Application["SchemaPath"].ToString() + "\\company.ifo");
                }
                if (Session["versionNo"] != null)
                {
                    Int32.TryParse(Session["versionNo"].ToString().Trim(), out versionNo);
                }

                if (Session["browser"] != null)
                {
                    if (Session["browser"].ToString().Trim().ToLower() == "internet explorer")
                    {
                        if (versionNo > 5.0)
                        {
                            corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "netscape")
                    {
                        if (versionNo > 5)
                        {
                            if (blockNSFF)
                                corBroswer = false;
                            else
                                corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "firefox")
                    {
                        if (versionNo >= 1)
                        {
                            if (blockNSFF)
                                corBroswer = false;
                            else
                                corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "opera")
                    {
                        corBroswer = true;
                    }
                }

                if (Session["wizard_enable"] != null)
                    if (Session["wizard_enable"].ToString().Equals("1"))
                        wizard_enable = true;

                if (Session["WHO_USE"] != null)
                    WHO_USE = Session["WHO_USE"].ToString();
                
                //FB 1628 START
                if (Request.QueryString["id"] != null && Request.QueryString["req"] != null && Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["id"].ToString() != "" && Request.QueryString["req"].ToString() != "" && Request.QueryString["tp"].ToString() != "")
                    {
                        try
                        {
                            String idStr = Request.QueryString["id"].ToString();
                            String reqStr = Request.QueryString["req"].ToString();
                            String tpStr = Request.QueryString["tp"].ToString();

                            if (Session["req"] == null && Session["id"] == null)
                            {
                                Session.Add("req", Request.QueryString["req"]);
                                Session.Add("id", Request.QueryString["id"]);
                            }
                            else
                            {
                                Session["req"] = Request.QueryString["req"].ToString();
                                Session["id"] = Request.QueryString["id"].ToString();
                            }

                            Session["userID"] = "11";//Hard Corded to fetch the System Time Zone
                            Session["organizationID"] = "11";//default organization ID

                            //FB 2616 Start
                            if (Request.QueryString["tp"].ToString() == "M")
                                Response.Redirect("ManageConference.aspx?t=&confid=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "");

                            else if (Request.QueryString["tp"].ToString() == "CM")
                                Response.Redirect("MonitorMCU.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "");

                            else if (Request.QueryString["tp"].ToString() == "P2P")
                                Response.Redirect("point2point.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "");

                            else
                                Response.Redirect("ResponseConference.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "&t=hf");
                            //FB 2616 End
                        }
                        catch (System.Threading.ThreadAbortException) { }

                    }
                }

                //FB 1628 END

              //  RemembermeLogin(); //ZD 100263

                GetQueryStrings();
            }
            catch (Exception ex)
            {
                log.Trace("Page Load: "+ ex.Message);
            }
        }

        #endregion

        #region BrowswerDetect
        /// <summary>
        /// BrowswerDetect
        /// </summary>
        private void BrowserDetect()
        {
            try
            {
				Session["macid"] = macID; //ZD 100263
                Session["ip"] = Request.ServerVariables["REMOTE_ADDR"];
                Session["referrer"] = Request.ServerVariables["HTTP_REFERER"];
                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("WINDOWS"))
                    Session["os"] = "Windows";
                else if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MAC"))
                    Session["os"] = "Mac";
                else if (Request.ServerVariables["HTTP_USER_AGENT"].ToUpper().ToUpper().Contains("LINUX"))
                    Session["os"] = "Linux";
                else
                    Session["os"] = "Other";

                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MOZILLA"))
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        Session["browser"] = "Internet Explorer";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("NETSCAPE"))
                    {
                        Session["browser"] = "Netscape";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }

                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("FIREFOX"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("SAFARI"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("CHROME"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Session["browser"] == null)
                        Session["browser"] = "Undetectable";
                    if (Session["versionNo"] == null)
                        Session["versionNo"] = "Undetectable";
                    if (Session["version"] == null)
                        Session["version"] = "Undetectable";
                }
                else
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("OPERA"))
                    {
                        Session["browser"] = "Opera";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    else
                    {
                        Session["browser"] = "Not Mozilla";
                        Session["version"] = "Undetectable";
                    }
                }
            }
            catch (Exception ex)
            {
                //log.trace
                log.Trace(ex.StackTrace + ex.Message);

            }
        }
        #endregion
        
        #region GetQueryStrings
        /// <summary>
        /// GetQueryStrings
        /// </summary>
        private void GetQueryStrings()
        {
            Int32 domain = 0;
            String user = "";
            try
            {
                Session.Add("c", "0");
                //Session.Add("guestlogin", ""); - This variable is not used in the system
                
                if (Request.QueryString["sct"] != null)
                    if (Request.QueryString["sct"] != "")
                    {
                        Session["c"] = Request.QueryString["sct"];
                    }
                ind = Session["c"].ToString();

                if (Application["CosignEnable"].ToString() == "1") //Check for cosign environment
                {
                    if (Request.ServerVariables["HTTP_REMOTE_USER"] != null) //FB 1820
                    {
                        loginMgmt.queyStraVal = Request.ServerVariables["HTTP_REMOTE_USER"].ToString();
                        
                        log.Trace("HTTP_REMOTE_USER: " + loginMgmt.queyStraVal);

                        loginMgmt.qStrPVal = "pwd";
                        loginMgmt.windwsAuth = true;
                        login();
                    }
                }
                else if (Application["ssoMode"].ToString().ToUpper() == "YES")    
                {
                    if (Request.ServerVariables["AUTH_USER"] != null)
                    {
                        domain = (Request.ServerVariables["AUTH_USER"]).ToLower().IndexOf("\\", 0) + 1;
                        user = ((Request.ServerVariables["AUTH_USER"]).ToLower()).Substring(domain);
                        
                        log.Trace("AUTH_USER: "+user); //FB 1820
                        loginMgmt.queyStraVal = user;
                        loginMgmt.qStrPVal = "pwd";
                        loginMgmt.qStrIpVal = macID; //ZD 100263
                        loginMgmt.windwsAuth = true;
                        login();
                    }
                }
                else
                {  
                    String orgId = "";
                    if (Request.QueryString["OrgID"] != null)
                    {
                        if (Request.QueryString["OrgID"].ToString() != "")
                        {
                            orgId = Request.QueryString["OrgID"].ToString();
                        }
                    }
                    //RSS Fix -- End

                    //FB 1830
                    if (Session["language"] != null)
                    {
                        if (Session["language"].ToString() != "")
                            language = Session["language"].ToString();
                    }

                    if (Request.QueryString["rss"] != null)//Rss Feed
                    {
                        if (Request.QueryString["rss"] != "")
                        {
                            if (Session["userID"].ToString() != "")
                            {
                                Response.Redirect("../" + language + "/ManageConference.aspx?t=&confid=" + Request.QueryString["rss"] + "&OrgID=" + orgId);
                            }
                        }
                    }
                }

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("GetQueryStrings: " + ex.Message); //FB 1820
            }
        }

        #endregion

        #region Submit Event Handlers

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                /*
                if (UserName.Value.ToString().Trim().IndexOf("@") <= 0)
                {
                    errLabel.Text = "Please enter Email"; // FB 2310
                    return;
                }
                */
                if (isloginFailed)//FB 2027 GetHome
                    return;

                loginMgmt = new MyVRMNet.LoginManagement();
                loginMgmt.queyStraVal = UserName.Value.ToString().Trim();

                Session["LoginUserName"] = UserName.Value.ToString().Trim(); // FB 1725
                loginMgmt.qStrPVal = UserPassword.Value.ToString().Trim();
                loginMgmt.windwsAuth = false;
                loginMgmt.qStrIpVal = macID; //ZD 100263
                login();
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                log.Trace("btnSubmit_Click: "+ ex.Message);
            }
        }

        #endregion

        protected void checkSession()
        {

        }

        #region Login
        //MEthod modified for FB 1820
        private void login()
        {
            String outXML = "";
            try
            {
                errLabel.Text = "";
                errLabel.Visible = false;

                outXML = loginMgmt.GetHomeCommand();

                if (outXML != "")
                {
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        XmlDocument errDoc = new XmlDocument();
                        errDoc.LoadXml(outXML);
                        if (errDoc.SelectSingleNode("error/message") != null)
                        { 
                            errLabel.Text = errDoc.SelectSingleNode("error/message").InnerText.Trim();
                            errLabel.Visible = true;
                            isloginFailed = true;
                            return;
                        }
                    }
                }
				//FB 1830
                if(Session["language"] != null)
                {
                    if (Session["language"].ToString() != "")
                        language = Session["language"].ToString();
                }

                //RSS Fix-Single Sign On -- Start
                String orgID = "";
                if (Request.QueryString["OrgID"] != null)
                {
                    if (Request.QueryString["OrgID"].ToString() != "")
                    {
                        orgID = Request.QueryString["OrgID"].ToString();
                    }
                }
                //RSS Fix -- End

                if (Request.QueryString["rss"] != null)//Rss Feed
                {
                    if (Request.QueryString["rss"] != "")
                    {
                        if (Session["userID"].ToString() != "")
                        {
                            Response.Redirect("ManageConference.aspx?t=&confid=" + Request.QueryString["rss"] + "&OrgID=" + orgID);
                        }
                    }
                }
                //Single Sign On -- End

                if (Session["NavLobby"] == null) // To avoid the alert message on refreshing lobby page
                    Session["NavLobby"] = "1";

                // //FB 2009
                if (Session["outXML"] != null) //ZD 100263
                    outXML = Session["outXML"].ToString();

                //ZD 100263
                //if (RememberMe.Checked)
                //{
                //    XmlDocument xdoc = new XmlDocument();
                //    xdoc.LoadXml(outXML);
                //    if (xdoc.SelectNodes("enEmailID") != null)
                //        Response.Cookies["VRMuser"]["act"] = xdoc.SelectSingleNode("user/globalInfo/enEmailID").InnerText.Trim();
                //    if (xdoc.SelectNodes("enUserPWD") != null)
                //        Response.Cookies["VRMuser"]["pwd"] = xdoc.SelectSingleNode("user/globalInfo/enUserPWD").InnerText.Trim();
                //    if (xdoc.SelectNodes("enIPAddress") != null)
                //        Response.Cookies["VRMuser"]["macid"] = xdoc.SelectSingleNode("user/globalInfo/enMacID").InnerText.Trim();
                //    Response.Cookies["VRMuser"].Expires = DateTime.Now.AddDays(365);
                //    Response.Cookies["VRMuser"].HttpOnly = true; // ZD 100263
                //}
                //FB 2009
               
                //FB 1779 start
                if (Session["isExpressUser"] != null)
                {
                    if (Session["isExpressUser"].ToString() == "1")
                        Response.Redirect("~/" + language + "/ExpressConference.aspx?t=n"); //FB 1830
                }
                //FB 1779 end
                
				//FB 1830
                Response.Redirect("~/" + language + "/SettingSelect2.aspx?c=GH");
                //Response.Redirect("~/" + language + "/lobby.aspx"); //LOBBY
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace("login: "+ex.Message);
            }
        }
        #endregion

        #region Remember me
        /// <summary>
        /// FB 2009
        /// </summary>
        private void RemembermeLogin()
        {
            try
            {
                if (Request.Cookies["VRMuser"] != null)
                {
                    if (Request.Cookies["VRMuser"].HasKeys)
                    {
                        if (Request.Cookies["VRMuser"]["macid"] == null) // ZD 100263 Starts
                            return;


                        if (Request.Cookies["VRMuser"]["act"] != null)
                            if (Request.Cookies["VRMuser"]["act"] != "")
                                loginMgmt.qStrEnUnVal = Request.Cookies["VRMuser"]["act"];

                        if (Request.Cookies["VRMuser"]["pwd"] != null)
                            if (Request.Cookies["VRMuser"]["pwd"] != "")
                                loginMgmt.qStrEnPwVal = Request.Cookies["VRMuser"]["pwd"];

                        if (Request.Cookies["VRMuser"]["macid"] != null)
                            if (Request.Cookies["VRMuser"]["macid"] != "")
                                loginMgmt.qStrEnIpVal = Request.Cookies["VRMuser"]["macid"];

                        /*
                        if (UserName.Value.ToString().Trim().IndexOf("@") <= 0)
                        {
                            UserName.Value = "";
                            return;
                        }

                        if (UserPassword.Value.ToString().Trim() == "")
                        {
                            UserPassword.Value = "";
                            return;
                        }
                        */

                        //loginMgmt = new MyVRMNet.LoginManagement();
                        loginMgmt.queyStraVal = UserName.Value.ToString().Trim();

                        Session["LoginUserName"] = loginMgmt.qStrEnUnVal; // FB 1725
                        //loginMgmt.qStrPVal = UserPassword.Value.ToString().Trim();
                        
                        loginMgmt.qStrIpVal = macID; // ZD 100263 Ends
                        loginMgmt.windwsAuth = false;
                        login();
                    }
                }

            }
            catch (Exception ex)
            {

                log.Trace("Rememberme: " + ex.Message);
            }
        }

        #endregion

        //ZD 100263 Starts
        #region Bind Processor

        public void BindProcessorDetails()
        {
            ManagementObjectSearcher objCS;
            try
            {
                objCS = null;
                objCS = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter");

                foreach (ManagementObject objmgmt in objCS.Get())
                {
                    if (objmgmt.Properties["AdapterTypeID"].Value != null)//Ethernet 802.3
                        if (objmgmt.Properties["AdapterTypeID"].Value.ToString() == "0")
                            macID += "," + objmgmt.Properties["MACAddress"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion
        //ZD 100263 Starts
    }
}
