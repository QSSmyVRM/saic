/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Schema;
using MetaBuilders.WebControls;

namespace ns_Administration
{
    public partial class eventLog : System.Web.UI.Page
    {
        #region protected member

        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.Label lblMyVRMVersion;
        protected System.Web.UI.WebControls.Label lblASPILVersion;
        protected System.Web.UI.WebControls.Label lblDatabaseVersion;
        protected System.Web.UI.WebControls.Label lblRTCVersion;
        protected System.Web.UI.WebControls.DropDownList moduleType;
        protected System.Web.UI.WebControls.TextBox txtErrorCode;
        protected System.Web.UI.WebControls.TextBox startDate;
        protected System.Web.UI.WebControls.TextBox endDate;
        protected System.Web.UI.WebControls.HiddenField startTime;
        protected System.Web.UI.WebControls.HiddenField endTime;
        protected System.Web.UI.WebControls.CheckBoxList Severity;
        protected System.Web.UI.WebControls.TextBox ErrorMessage;
        protected System.Web.UI.WebControls.Table tblLogPref;
        protected RequiredFieldValidator startDateRequired;
        protected RequiredFieldValidator endDateRequired;
        protected CompareValidator startDateComp;
        protected RegularExpressionValidator startDateRegEx;
        protected RequiredFieldValidator startTimeReq;
        protected RegularExpressionValidator startTimeRegEx;
        protected RequiredFieldValidator endTimeReq;
        protected RegularExpressionValidator endTimeRegEx;
        protected CustomValidator validateSeverity;
        protected ComboBox startTimeCombo;
        protected ComboBox endTimeCombo;
        protected TextBox filePath;
        myVRMNet.NETFunctions obj;

        //Code added by Offshore for FB Issue 1073 -- Start
        String tformat = "hh:mm tt";
        protected String format = "";
        //Code added by Offshore for FB Issue 1073 -- End
        
        #endregion
        private string[] moduleName;
        private string[] logLevel;
        private string[] logLife;
        private string getLogPrefInXml = "";

        private DropDownList[] lstLogLevel;
        private DropDownList[] lstLogLife;
        myVRMNet.NETFunctions com;
        ns_Logger.Logger log;

        private bool firstLoad;

        private Button delButton;

        public eventLog()
        {
             com = new myVRMNet.NETFunctions();
             log = new ns_Logger.Logger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("hardwareissuelog.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            //Code added by Offshore for FB Issue 1073 -- Start
            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            

            //Code added by Offshore for FB Issue 1073 -- End
            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
            startTimeCombo.Items.Clear();
            endTimeCombo.Items.Clear();
            com.BindTimeToListBox(startTimeCombo, false, false);
            com.BindTimeToListBox(endTimeCombo, false, false);
            if (tformat.Equals("HH:mm"))
            {
                startTimeRegEx.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                startTimeRegEx.ErrorMessage = com.GetTranslatedText("Invalid Time (HH:mm)");
                endTimeRegEx.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                endTimeRegEx.ErrorMessage = com.GetTranslatedText("Invalid Time (HH:mm)");
            }
            if (!IsPostBack)
            {
                startTimeCombo.Text = DateTime.Parse("01:00 AM").ToString(tformat);
                endTimeCombo.Text = DateTime.Parse("11:00 PM").ToString(tformat);
                string inXML = "<getLogPreferences>";
                inXML += com.OrgXMLElement();//Organization Module Fixes
                inXML += "<userID>" + Session["userID"] + "</userID>";
                inXML += "</getLogPreferences>";

                getLogPrefInXml = inXML;

                //string returnXML = CallCOM("GetLogPreferences", inXML);
                string returnXML = com.CallMyVRMServer("GetLogPreferences", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                firstLoad = true;
                extractData(returnXML);
                lblMessage.Visible = false;
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = com.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    }
                GetSiteDiagnostics();
            }
            else
            {
                if (Request.QueryString["d"] != null)
                {
                    deleteLog(Request.QueryString["m"], Request.QueryString["d"]);
                }
                firstLoad = false;
                extractData(Session["outXML"].ToString());

            }

        }


        private void searchLog()
        {
            try
            {
                string severityOption = "";
                string fromDate, toDate = "";
                DateTime dtFrom = new DateTime();
                DateTime dtTo = new DateTime();
                //FB 2027 Start
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<searchLog>");
                inXML.Append(com.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<module>");
                inXML.Append("<moduleName>" + moduleType.SelectedValue + "</moduleName>");
                inXML.Append("<moduleSystemID></moduleSystemID>");
                inXML.Append("<moduleErrorCode>" + txtErrorCode.Text + "</moduleErrorCode>");
                inXML.Append("</module>");

                //Code changed by Offshore for FB Issue 1073 -- Start
                //fromDate = startDate.Text;
                //toDate = endDate.Text;
                fromDate = myVRMNet.NETFunctions.GetDefaultDate(startDate.Text);
                toDate = myVRMNet.NETFunctions.GetDefaultDate(endDate.Text);
                //Code changed by Offshore for FB Issue 1073 -- End

                //fromTime = startTime.Value;
                //toTime = endTime.Value;

                dtFrom = Convert.ToDateTime(startTimeCombo.Text);
                dtTo = Convert.ToDateTime(endTimeCombo.Text);

                inXML.Append("<from>" + fromDate + " " + dtFrom.ToString("HH:mm") + "</from>");
                inXML.Append("<to>" + toDate + " " + dtTo.ToString("HH:mm") + "</to>");

                for (int i = 0; i < Severity.Items.Count; i++)
                {
                    if (Severity.Items[i].Selected == true)
                    {
                        severityOption += Severity.Items[i].Value;
                        severityOption += ",";
                    }
                }

                severityOption = severityOption.Remove(severityOption.LastIndexOf(","));
                Severity.CssClass = "blackblodtext";//Window Dressing

                inXML.Append("<severity>" + severityOption + "</severity>");
                inXML.Append("<file></file>");
                inXML.Append("<function></function>");
                inXML.Append("<line></line>");
                inXML.Append("<message>" + ErrorMessage.Text + "</message>");
                inXML.Append("<pageNo>1</pageNo>");
                inXML.Append("</searchLog>");
                //Response.Write(inXML.Replace("<", "&lt;").Replace(">", "&gt;"));
                //Response.End();
                //log.Trace("SearchLog: " + inXML.ToString());
                //string returnXML = CallCOM("SearchLog", inXML.ToString());
                string returnXML = com.CallMyVRMServer("SearchLog", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 End
                log.Trace("SearchLog: " + returnXML);

                if (returnXML.IndexOf("<error>") < 0)
                {
                    Session.Add("searchParam", inXML);
                    Session.Add("searchResult", returnXML);
                    Response.Redirect("logList.aspx", true);
                }

            }
            catch (Exception ex)
            {
                log.Trace("searchLog: " + ex.StackTrace + " : " + ex.Message);
            }
        }


        protected void deleteLog(string moduleName, string deleteAll)
        {
            string inXML = "<login>";
            inXML += com.OrgXMLElement();//Organization Module Fixes
            inXML += "<userID>" + Session["userID"] + "</userID>";

            if (deleteAll == "0")
            {
                inXML += "<moduleName>" + moduleName + "</moduleName>";
                inXML += "<deleteAll>0</deleteAll>";
            }
            else
            {
                inXML += "<moduleName></moduleName>";
                inXML += "<deleteAll>" + deleteAll + "</deleteAll>";
            }

            inXML += "</login>";

            string returnXML = com.CallMyVRMServer("DeleteModuleLog", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(DeleteModuleLog)
            //string returnXML = CallCOM("DeleteModuleLog", inXML);
            
            //Response.Write(inXML.Replace("<", "&lt;").Replace(">","&gt;"));
            //Response.Write("<br>" + returnXML.Replace("<", "&lt;").Replace(">", "&gt;"));
            //Response.End();

            if (returnXML.IndexOf("<error>") < 0)
            {
                string logPrefXML = "<getLogPreferences>";
                logPrefXML += com.OrgXMLElement();//Organization Module Fixes
                logPrefXML += "<userID>" + Session["userID"] + "</userID>";
                logPrefXML += "</getLogPreferences>";

                //returnXML = CallCOM("GetLogPreferences", logPrefXML);
                returnXML = com.CallMyVRMServer("GetLogPreferences", logPrefXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                if (returnXML.IndexOf("<error>") < 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = com.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                }
                Response.Redirect("hardwareissuelog.aspx?m=1");
            }
            else
            {
                myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
                lblMessage.Text = obj.ShowErrorMessage(returnXML.ToString());
                lblMessage.Visible = true;

                string logPrefXML = "<getLogPreferences>";
                logPrefXML += obj.OrgXMLElement();//Organization Module Fixes
                logPrefXML += "<userID>" + Session["userID"] + "</userID>";
                logPrefXML += "</getLogPreferences>";

                //returnXML = CallCOM("GetLogPreferences", logPrefXML);
                returnXML = obj.CallMyVRMServer("GetLogPreferences", logPrefXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027

                //firstLoad = false;
                //extractData(returnXML);

            }
        }


        private void updateLogPref()
        {
            int ctr = 0;
            string inXML = "<setLogPreferences>";
            inXML += com.OrgXMLElement();//Organization Module Fixes

            XmlDocument logXML = new XmlDocument();

            logXML.LoadXml(Session["outXML"].ToString());

            XmlNodeList moduleList = logXML.SelectNodes("//getLogPreferences/module");

            foreach (XmlNode node in moduleList)
            {
                inXML += "<module>";
                inXML += "<moduleName>" + node.SelectSingleNode("moduleName").InnerText + "</moduleName>";
                inXML += "<logLevel>" + lstLogLevel[ctr].SelectedValue + "</logLevel>";
                inXML += "<logLife>" + lstLogLife[ctr].SelectedValue + "</logLife>";

                inXML += "</module>";

                ctr++;
            }

            inXML += "</setLogPreferences>";

            string returnXML = CallCOM("SetLogPreferences", inXML);

            if (returnXML.IndexOf("<error>") < 0)
            {
                Response.Redirect("hardwareissuelog.aspx?m=1");
                //returnXML = CallCOM("GetLogPreferences", getLogPrefInXml);
                //if (returnXML.IndexOf("<error>") < 0)
                // {
                //lblMessage.Visible = true;
                //lblMessage.Text = "Operation Succesful!";
                // }
            }

        }


        //private string retrieveLog()
        // {
        // }


        private string CallCOM(string command, string inXML)
        {
            String outXML = com.CallCOM(command, inXML, Application["COM_ConfigPath"].ToString());
            Session.Add("outXML", outXML);

            return outXML;

        }


        private void extractData(string resultXML)
        {
            int ctr = 0;
            int recordCount = 0;

            tblLogPref.BorderWidth = 0;
            tblLogPref.Width = Unit.Percentage(90);

            //creates the static column header
            TableRow tr_header = new TableRow();
            tr_header.CssClass = "tableHeader";
            TableCell td_header = new TableCell();
            td_header.CssClass = "tableHeader";//Window Dressing
            td_header.Font.Bold = true;
            td_header.Font.Size = 8;
            td_header.HorizontalAlign = HorizontalAlign.Center;
            td_header.Text = com.GetTranslatedText("Module Type");//FB 1830 - Translation
            tr_header.Cells.Add(td_header);

            TableCell td_space = new TableCell();
            td_space.CssClass = "tableHeader";//Window Dressing
            td_space.Width = Unit.Percentage(4);
            td_space.Text = " ";
            tr_header.Cells.Add(td_space);

            td_header = new TableCell();
            td_header.CssClass = "tableHeader";//Window Dressing
            td_header.Font.Bold = true;
            td_header.Font.Size = 8;
            td_header.HorizontalAlign = HorizontalAlign.Center;
            td_header.Text = com.GetTranslatedText("Event level to be logged");//FB 1830 - Translation

            tr_header.Cells.Add(td_header);

            td_space = new TableCell();
            td_space.CssClass = "tableHeader";//Window Dressing
            tr_header.Cells.Add(td_space);

            td_header = new TableCell();
            td_header.CssClass = "tableHeader";//Window Dressing
            td_header.Font.Bold = true;
            td_header.Font.Size = 8;
            td_header.HorizontalAlign = HorizontalAlign.Center;
            td_header.Text = com.GetTranslatedText("Overwrite events older than");//FB 1830 - Translation

            tr_header.Cells.Add(td_header);

            td_space = new TableCell();
            td_space.CssClass = "tableHeader";//Window Dressing
            tr_header.Cells.Add(td_space);

            td_header = new TableCell();
            td_header.CssClass = "tableHeader";//Window Dressing
            td_header.Font.Bold = true;
            td_header.Font.Size = 8;
            td_header.HorizontalAlign = HorizontalAlign.Center;
            td_header.Text = com.GetTranslatedText("Delete Log");//FB 1830 - Translation

            tr_header.Cells.Add(td_header);

            //add the static column header
            tblLogPref.Rows.Add(tr_header);

            //read from the return XML 
            XmlDocument logPrefXML = new XmlDocument();

            logPrefXML.LoadXml(resultXML);

            XmlNodeList logPrefNL = logPrefXML.SelectNodes("//getLogPreferences/module");
            recordCount = logPrefNL.Count;

            moduleName = new string[recordCount];
            logLevel = new string[recordCount];
            logLife = new string[recordCount];

            lstLogLevel = new DropDownList[recordCount];
            lstLogLife = new DropDownList[recordCount];
            
            if (firstLoad == true)
            {
                //Severity.Attributes.Add("OnCheck", "javascript:validateCheck()");

                Severity.Items.Add(new ListItem("System", "0"));
                Severity.Items.Add(new ListItem("User", "1"));
                Severity.Items.Add(new ListItem("Warning", "2"));
                Severity.Items.Add(new ListItem("Information", "3"));
                Severity.Items.Add(new ListItem("Debug", "9"));
                Severity.Items[0].Selected = true;
            }


            if (firstLoad == true)
            {
                moduleType.Items.Add(new ListItem("Any", ""));
            }

            foreach (XmlNode node in logPrefNL)
            {
                moduleName[ctr] = node.SelectSingleNode("moduleName").InnerText;
                logLevel[ctr] = node.SelectSingleNode("logLevel").InnerText;
                logLife[ctr] = node.SelectSingleNode("logLife").InnerText;
                //creates the two drop down list for log level and log life

                lstLogLevel[ctr] = new DropDownList();
                lstLogLevel[ctr].Items.Add(new ListItem("Sytem Errors", "0"));
                lstLogLevel[ctr].Items.Add(new ListItem("System & User Errors", "1"));
                lstLogLevel[ctr].Items.Add(new ListItem("Warnings (and all above)", "2"));
                lstLogLevel[ctr].Items.Add(new ListItem("Information (and all above)", "3"));
                lstLogLevel[ctr].Items.Add(new ListItem("Debug (and all above)", "9"));
                lstLogLevel[ctr].CssClass = "SelectFormat";
                //lstLogLevel[ctr].ID = logName + "Level" + rowCount.ToString();

                //creates the row dynamically for each module
                lstLogLife[ctr] = new DropDownList();
                lstLogLife[ctr].Items.Add(new ListItem("Off", "0"));
                lstLogLife[ctr].Items.Add(new ListItem("1 day", "1"));
                lstLogLife[ctr].Items.Add(new ListItem("3 days", "3"));
                lstLogLife[ctr].Items.Add(new ListItem("7 days", "7"));
                lstLogLife[ctr].CssClass = "SelectFormat";
                //lstLogLife[ctr].ID = logName + "Life" + rowCount.ToString();

                constructTable(ctr, moduleName[ctr], logLevel[ctr], logLife[ctr]);

                if (firstLoad == true)
                {
                    moduleType.Items.Add(new ListItem(moduleName[ctr], moduleName[ctr]));
                }
                ctr++;
            }

            Button delAllButton = new Button();
            delAllButton.Text = com.GetTranslatedText("Delete All");//FB 1830 - Translation
            delAllButton.CssClass = "altShort2BlueButtonFormat";
            delAllButton.ID = "Delete All";
            delAllButton.OnClientClick = "deleteLog(\"\", 1)";

            TableRow tr = new TableRow();
            TableCell td = new TableCell();
            td.ColumnSpan = 6;
            td.RowSpan = 2;
            td.HorizontalAlign = HorizontalAlign.Right;
            td.Text = "&nbsp;";
            tr.Cells.Add(td);

            td = new TableCell();
            td.Controls.Add(delAllButton);

            tr.Cells.Add(td);

            tblLogPref.Rows.Add(tr);
            TimeSpan sevenDays = new TimeSpan(7, 0, 0, 0);
            //Code changed by Offshore for FB Issue 1073 -- Start
            //startDate.Text = DateTime.Now.Subtract(sevenDays).ToString("MM/dd/yyyy");
            //endDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            startDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now.Subtract(sevenDays));
            endDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now);
            //Code changed by Offshore for FB Issue 1073 -- End
        }




        private void constructTable(int rowCount, string logName, string logLevel, string logLife)
        {
            TableRow tr = new TableRow();

            TableCell td = new TableCell();
            td.CssClass = "tableBody"; //Window Dressing
            td.Text = logName;
            td.HorizontalAlign = HorizontalAlign.Center;
            tr.Cells.Add(td);

            TableCell cellSpace = new TableCell();
            cellSpace.Width = Unit.Percentage(2);
            cellSpace.Text = " ";
            tr.Cells.Add(cellSpace);

            td = new TableCell();
            td.HorizontalAlign = HorizontalAlign.Center;

            //set the selected value
            foreach (ListItem li in lstLogLevel[rowCount].Items)
            {
                if (logLevel == li.Value)
                {
                    li.Selected = true;
                }
            }

            //add the Log Level dropdown list to the cell
            td.Controls.Add(lstLogLevel[rowCount]);

            tr.Cells.Add(td);

            cellSpace = new TableCell();
            tr.Cells.Add(cellSpace);

            td = new TableCell();
            td.HorizontalAlign = HorizontalAlign.Center;

            //set the selected value
            foreach (ListItem li in lstLogLife[rowCount].Items)
            {
                if (logLife == li.Value)
                {
                    li.Selected = true;
                }
            }

            //add the Log Life dropdown list to the cell
            td.Controls.Add(lstLogLife[rowCount]);

            tr.Cells.Add(td);

            cellSpace = new TableCell();
            tr.Cells.Add(cellSpace);

            td = new TableCell();

            delButton = new Button();
            delButton.Text = com.GetTranslatedText("Delete");//FB 1830 - Translation
            delButton.CssClass = "altShort2BlueButtonFormat";
            delButton.ID = logName;
            delButton.OnClientClick = "deleteLog(\"" + logName + "\", 0)";

            td.HorizontalAlign = HorizontalAlign.Center;
            td.Controls.Add(delButton);

            tr.Cells.Add(td);

            tblLogPref.Rows.Add(tr);

        }




        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            updateLogPref();
        }


        protected void searchSubmit_Click(object sender, EventArgs e)
        {
            if (validateSeverity.IsValid)
            {
                searchLog();
            }


        }


        protected void validateSeverity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int checkSel = 0;
            foreach (ListItem severityEvent in Severity.Items)
            {

                if (severityEvent.Selected == true)
                {

                    checkSel += 1;
                }
            }

            if (checkSel > 0)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }

        }

        protected void logRetSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string logFilePath = filePath.Text;
                string strScript = "";

                logFilePath = logFilePath.Replace("\\", "\\\\");
                strScript = "<script language=JavaScript>";
                strScript += "window.open('" + logFilePath + "','','width=600,height=400,resizable=yes,scrollbars=yes,status=no');";
                strScript += "</script>";

                if (!Page.IsStartupScriptRegistered("openLog"))
                {
                    Page.RegisterStartupScript("openLog", strScript);
                }

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("logRetSubmit_Click" + ex.StackTrace);
                lblMessage.Text = obj.ShowSystemMessage();
                lblMessage.Visible = true;                
            }
        }
        protected void GetSiteDiagnostics()
        {
            try
            {
            //    String inXML = "";
            //    inXML += "<SiteDiagnostics>";
            //    inXML += "  <UserID>11</UserID>";
            //    inXML += "</SiteDiagnostics>";
            //    //String outXML = com.CallMyVRMServer("GetSiteDiagnostics", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            //    String outXML = "";
            //    outXML += "<SiteDiagnostics>";
            //    outXML += "     <Database>123</Database>";
            //    outXML += "     <ASPIL>123</ASPIL>";
            //    outXML += "     <MYVRM>123</MYVRM>";
            //    outXML += "     <RTC>123</RTC>";
            //    outXML += "</SiteDiagnostics>";
            //    if (outXML.IndexOf("<error>") >= 0)
            //    {
            //        lblMessage.Text = com.ShowErrorMessage(outXML);
            //        lblMessage.Visible = true;
            //    }
            //    else
            //    {
            //        XmlDocument xmldoc = new XmlDocument();
            //        xmldoc.LoadXml(outXML);
            //        lblDatabaseVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/Database").InnerText;
            //        lblASPILVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/ASPIL").InnerText;
            //        //lblMyVRMVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/MYVRM").InnerText;
            //        String filePath = "../bin/v1.8.3.dll";
            //        FileVersionInfo fleVersion;

            //        if (File.Exists(filePath))
            //        {
            //            fleVersion = FileVersionInfo.GetVersionInfo(filePath);
            //            lblMyVRMVersion.Text = fleVersion.FileMajorPart + "." + fleVersion.FileMinorPart;
            //        }
            //        else
            //            lblMessage.Text = "Error Reading file";
            //        lblRTCVersion.Text = xmldoc.SelectSingleNode("//SiteDiagnostics/RTC").InnerText;
            //    }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetSiteDiagnostics" + ex.StackTrace);
                lblMessage.Text = obj.ShowSystemMessage();
                lblMessage.Visible = true;                
            }
        }
    }
}

