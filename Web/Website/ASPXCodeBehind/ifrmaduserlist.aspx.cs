/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;

public partial class en_ifrmaduserlist : System.Web.UI.Page
{
    #region Private Data Members
    /// <summary>
    /// Private Data Members
    /// </summary>

    StringBuilder inxml = new StringBuilder();//FB 2027

    String outGetAllocationXML = "";
    String outXml = "";

    XmlDocument XmlDoc = null;
    XmlDocument XmlBulkDoc = null;
    XmlNode node = null;
    XmlNodeList nodes = null;
 

    private String errorMessage = "";

  
    private String userID = "";
    private String firstName = "";
    private String lastName = "";
    private String users = "";
    private Int32 length = 0;
    private String[] userIds = null;
    private String Email = "";

    private ns_Logger.Logger log;
    private myVRMNet.NETFunctions obj;

    #endregion

    #region Protected Data members

    /// <summary>
    /// Protected Data members
    /// </summary>

    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnusers;
    protected System.Web.UI.HtmlControls.HtmlInputHidden SelectedUser;
    protected System.Web.UI.HtmlControls.HtmlInputHidden sortby;
    protected System.Web.UI.HtmlControls.HtmlInputHidden alphabet;
    protected System.Web.UI.HtmlControls.HtmlInputHidden pageno;
    protected System.Web.UI.HtmlControls.HtmlInputHidden timezones;
    protected System.Web.UI.HtmlControls.HtmlInputHidden roles;
    protected System.Web.UI.HtmlControls.HtmlInputHidden languages;
    protected System.Web.UI.HtmlControls.HtmlInputHidden bridges;
    protected System.Web.UI.HtmlControls.HtmlInputHidden departments;
    protected System.Web.UI.HtmlControls.HtmlInputHidden canAll;
    protected System.Web.UI.HtmlControls.HtmlInputHidden totalPages;
    protected System.Web.UI.HtmlControls.HtmlInputHidden adusers;
    protected System.Web.UI.HtmlControls.HtmlInputHidden totalNumber;

    #endregion

    #region Page Load Event Handlers
    /// <summary>
    /// Page Load Event Handlers
    /// </summary>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            this.IntializeUIResources();
            BindData();
            outGetAllocationXML = "";
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_Load" + ex.Message);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;            
        }

    }

    #endregion

    #region Intialize UI Resources
    /// <summary>
    /// Intializing UI Controls
    /// </summary>
    /// 
    private void IntializeUIResources()
    {
       
    }


    #endregion

    #region User Defined Methods

    /// <summary>
    /// User Defined Methods 
    /// </summary>

    #region BindData
    /// <summary>
    /// User Defined Method - Building GetQueryStrings Structure
    /// </summary>

    private void BindData()
    {
        try
        {
            GetQueryStrings();
            XMLTransactions();
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("GetQueryStrings" + ex.StackTrace);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;            

        }
    }
    #endregion

    #region GetQueryStrings

    /// <summary>
    /// User Defined Method - Building GetQueryStrings Structure
    /// </summary>

    private void GetQueryStrings()
    {

        try
        {
            if (Request.QueryString["sb"] != null)
            {
                if (Request.QueryString["sb"].ToString().Trim() != "")
                {
                  
                    sortby.Value = Request.QueryString["sb"].ToString().Trim();
                }
            }
            else
            {
                
                sortby.Value = "2";

            }
            if (Request.QueryString["a"] != null)
            {
                if (Request.QueryString["a"].ToString().Trim() != "")
                {
                    
                    alphabet.Value =Request.QueryString["a"].ToString().Trim();
                }
            }
            else
                alphabet.Value = "A";
            if (Request.QueryString["pn"] != null)
            {
                if (Request.QueryString["pn"].ToString().Trim() != "")
                {
                   
                    pageno.Value = Request.QueryString["pn"].ToString().Trim();
                }
            }
            else
                pageno.Value = "1";

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("QueryString" + ex.StackTrace);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;
        }
    }
    #endregion
    //Modified during FB 2027
    #region BuildInXML
    /// <summary>
    /// User Defined Method - Building InXML Structure
    /// </summary>

    private void BuildInXML(String commandName)
    {
        try
        {
            switch (commandName)
            {
                case ("GetAllocation"):
                    inxml.Append("<login>");
                    inxml.Append(obj.OrgXMLElement());//Organization Module Fixes
                    inxml.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                    inxml.Append("<sortBy>" + sortby.Value+ "</sortBy>");
                    inxml.Append("<alphabet>" + alphabet.Value + "</alphabet>");
                    inxml.Append("<pageNo>" + pageno.Value + "</pageNo>");
                    inxml.Append("</login>");
                    break;
               
            }

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BuildInXML" + ex.StackTrace);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;
        }
    }
    #endregion

    #region GetOutXML
    /// <summary>
    /// User Defined Method - Retrieving OutXML  from COM
    /// </summary>

    private void XMLTransactions()
    {
        XmlDocument xmlDocAlloc = null;

        try
        {
            obj = new myVRMNet.NETFunctions();

            String cmd = "GetAllocation";
            BuildInXML(cmd);
            //outGetAllocationXML = obj.CallCOM(cmd, inXML, Application["COM_ConfigPath"].ToString());
            outGetAllocationXML = obj.CallMyVRMServer(cmd,inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
            if (outGetAllocationXML.IndexOf("<error>") < 0)
            {
                
                    xmlDocAlloc = new XmlDocument();
                    xmlDocAlloc.LoadXml(outGetAllocationXML);

                    AllocationIncFile();

                    xmlDocAlloc = null;
            }


        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("SendInXML" + ex.StackTrace);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;
        }
    }
    #endregion

    #region Included Method
    /// <summary>
    /// User Defined Method - Included File'inc/AllocationInc.asp' logic is implemented as method
    /// </summary>

    private void AllocationIncFile()
    {
        ArrayList ddlOperations = null;
        XmlDoc = new XmlDocument();

        try
        {
            if (Convert.ToString(Session["errMsg"]) != "")
            {
                if ((Convert.ToString(outGetAllocationXML).IndexOf("<error>") != 0))
                {
                    //FB 2018 Start
                    //Response.Write("<br><br><p align='center'><font size=4><b>" + Convert.ToString(outGetAllocationXML) + "<b></font></p>");
                    //Response.End();
                    XmlDoc.LoadXml(outGetAllocationXML);
                }
            }
            else
            {
                XmlDoc.LoadXml(outGetAllocationXML);
            }
            //FB 2018 End
           
           if (XmlDoc != null)
            {
                if (XmlDoc.DocumentElement.SelectNodes("users/sortBy").Count > 0)
                {
                    sortby.Value = XmlDoc.DocumentElement.SelectSingleNode("users/sortBy").InnerText;
                    alphabet.Value = XmlDoc.DocumentElement.SelectSingleNode("users/alphabet").InnerText;
                    pageno.Value = XmlDoc.DocumentElement.SelectSingleNode("users/pageNo").InnerText;
                    totalPages.Value = XmlDoc.DocumentElement.SelectSingleNode("users/totalPages").InnerText;
                    canAll.Value = XmlDoc.DocumentElement.SelectSingleNode("users/canAll").InnerText;
                    totalNumber.Value = XmlDoc.DocumentElement.SelectSingleNode("users/totalNumber").InnerText;
                }
                
                nodes = XmlDoc.DocumentElement.SelectNodes("users/user");
                length = nodes.Count;
                userIds = new string[length - 1 + 1];
                users = "";
                for (int i = 0; i <= length - 1; i += 1)
                {
                    if ((nodes[i].SelectSingleNode("userID").InnerText != "11") && (nodes[i].SelectSingleNode("userID").InnerText != Session["userID"].ToString())) //FB 2027
                    {

                        userIds[i] = nodes[i].SelectSingleNode("userID").InnerText;
                        users = users + userIds[i] + "``" + nodes[i].SelectSingleNode("firstName").InnerText + "``" + nodes[i].SelectSingleNode("lastName").InnerText + "``" + nodes[i].SelectSingleNode("email").InnerText + "||"; // FB 1888
                    }
                }

                hdnusers.Value = users;
                adusers.Value = users;
           }
            
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("AllocationInc" + ex.StackTrace);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;
        }
    }
    #endregion

    #endregion


}
