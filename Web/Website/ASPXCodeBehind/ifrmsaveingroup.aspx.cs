﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;

namespace ns_ifrmsaveingroup
{
    #region Class ifrmsaveingroup
    /// <summary>
    /// ifrmsaveingroup Class called in Conference Setup
    /// </summary>
    public partial class ifrmsaveingroup : System.Web.UI.Page
    {
        #region Private Data Members
        /// <summary>
        /// Private Data Members
        /// </summary>
        
        string GroupID = "";
        string GroupName = "";
        string GroupOwnerID = "";
        string GroupOwnerName = "";
        string GroupDescription = "";
        string GroupPublic = "";
        string PartysInfo = "";
        string formFrom = "";
        string strScript = "";

        ns_Logger.Logger log = null;
        myVRMNet.NETFunctions obj = null;

        protected string errmsg = "";
        protected string insertSt = "0";

        #endregion

        #region Public Constructor
        /// <summary>
        /// Public Constructor
        /// </summary>
        public ifrmsaveingroup()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page  Load Event Handlers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ifrmsaveingroup.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                GetFormValues();

                if (!IsPostBack)
                {
                    if (GroupID.Trim() != "" && GroupName.Trim() != "")
                        SaveInGroup();
                }
            }
            catch (Exception ex)
            {
                log.Trace("ifrmsaveingroup : PageLoad" + ex.StackTrace + ex.Message);
            }
        }
        #endregion

        #region Save In Group
        /// <summary>
        /// To Save in group
        /// </summary>
        private void SaveInGroup()
        {
            //FB 2027 - Starts
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("  <userInfo>");
                inXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("  </userInfo>");
                inXML.Append("  <groupInfo>");
                inXML.Append("  <group>");
                inXML.Append("<groupID>" + GroupID + "</groupID>");
                inXML.Append("<groupName>" + GroupName + "</groupName>");
                inXML.Append("<ownerID>" + GroupOwnerID + "</ownerID>");
                inXML.Append("<ownerName>" + GroupOwnerName + "</ownerName>");
                inXML.Append("<description>" + GroupDescription + "</description>");
                if (GroupPublic.Equals("1"))
                    inXML.Append("<public>" + GroupPublic + "</public>");

                if (PartysInfo.Trim() != "")
                {
                    //FB 1888
                    String[] delimitedArr = { "||" };
                    String[] delimitedArr1 = { "!!" };
                    //String[] partysInfoSplit = PartysInfo.Split(';');
                    String[] partysInfoSplit = PartysInfo.Split(delimitedArr,StringSplitOptions.RemoveEmptyEntries);
                    String[] pInfoDetails = null;
                    //FB 1888
                    for (Int32 s = 0; s < partysInfoSplit.Length; s++)
                    {
                        pInfoDetails = null;
                        //FB 1888
                        //pInfoDetails = partysInfoSplit[s].Split(',');
                        pInfoDetails = partysInfoSplit[s].Split(delimitedArr1, StringSplitOptions.RemoveEmptyEntries);

                        inXML.Append("<user>");
                        inXML.Append("<userID>" + pInfoDetails[0] + "</userID>");
                        inXML.Append("<userFirstName>" + pInfoDetails[1] + "</userFirstName>");
                        inXML.Append("<userLastName>" + pInfoDetails[2] + "</userLastName>");
                        inXML.Append("<userEmail>" + pInfoDetails[3] + "</userEmail>");
                        inXML.Append("</user>");
                    }
                }
                inXML.Append("</group>");
                inXML.Append("</groupInfo>");
                inXML.Append("</login>");
                log.Trace("SetGroup inXML: " + inXML.ToString());
                
                //String outXML = obj.CallCOM("SetGroup", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("SetGroup", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
                if (outXML.IndexOf("<error>") >= 0)
                {
                    insertSt = "0";
                    errmsg = obj.ShowErrorMessage(outXML);
                    Session.Remove("GrpErrMsg");
                    Session.Add("GrpErrMsg", errmsg);

                    Response.Write("<script>var prnt = parent.document.getElementById('cbtgroup');prnt.click();</script>");                   
                }
                else
                {
                    insertSt = "1";
                    Response.Write("<script>var prnt = parent.document.getElementById('sbtgroup');prnt.click();</script>");                    
                }

            }
            catch (System.Threading.ThreadAbortException) { }

            catch (Exception ex)
            {
                log.Trace("SaveInGroup inXML: " + ex.StackTrace + ex.Message);
            }
        }

        #endregion

        #region Get Form Values
        /// <summary>
        /// To Get Form Values
        /// </summary>

        private void GetFormValues()
        {
            try
            {
                if (Request.Form["GroupID"] != null)
                    GroupID = Request.Form["GroupID"].ToString();

                if (Request.Form["GroupName"] != null)
                    GroupName = Request.Form["GroupName"].ToString();

                if (Request.Form["GroupOwnerID"] != null)
                    GroupOwnerID = Request.Form["GroupOwnerID"].ToString();

                if (Request.Form["GroupOwnerName"] != null)
                    GroupOwnerName = Request.Form["GroupOwnerName"].ToString();

                if (Request.Form["GroupDescription"] != null)
                    GroupDescription = Request.Form["GroupDescription"].ToString();

                if (Request.Form["GroupPublic"] != null)
                    GroupPublic = Request.Form["GroupPublic"].ToString();

                if (Request.Form["PartysInfo"] != null)
                    PartysInfo = Request.Form["PartysInfo"].ToString();

                if(Request.Form["from"] != null)
                    formFrom = Request.Form["from"].ToString();
            }
            catch (Exception ex)
            {
                log.Trace("SaveInGroup inXML: " + ex.StackTrace + ex.Message);
            }
        }

        #endregion
    }
    #endregion
}
