﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace ns_license
{
    #region Class License
    /// <summary>
    /// Public license Class
    /// </summary>
    public partial class license : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        #region Data Members
        /// <summary>
        /// All Data Members
        /// </summary>
        protected String init = "";

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("license.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            if (Session["InitPage"] != null)
                init = Session["InitPage"].ToString();
        }
        #endregion
    }
    #endregion
}
