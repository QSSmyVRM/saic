/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Schema;
using System.Drawing;

namespace ns_Administration
{
    public partial class logList : System.Web.UI.Page
    {
        #region protected Members
        myVRMNet.NETFunctions obj;

        protected System.Web.UI.WebControls.PlaceHolder resultHolder;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtPage;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.WebControls.DataGrid dgResult;
        protected System.Web.UI.WebControls.Label lblNoRecords;

        ns_Logger.Logger log;
        myVRMNet.NETFunctions com;
        String tformat = "hh:mm tt";

        #endregion

        int pageNo;
        int totalPage;

        public logList()
        {
            com = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("logList.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            //pageSelect.AutoPostBack = true;
            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
            if (!IsPostBack)
            {
                errLabel.Visible = false;
                if (Request.QueryString["pageNo"] != null)
                {
                    txtPage.Text = Request.QueryString["pageNo"].ToString();
                    SearchResult();
                }
                else
                    txtPage.Text = "1";
                outputSearchResult();
            }
            else
            {
                //outputSearchResult();
            }

        }

        private void outputSearchResult()
        {
            try
            {
                //read from the return XML 
                XmlDocument searchResultXML = new XmlDocument();
                log.Trace("searchResult: " + Session["searchResult"].ToString());
                searchResultXML.LoadXml(Session["searchResult"].ToString());

                XmlNodeList resultNL = searchResultXML.SelectNodes("//searchLog/log");

                pageNo = Convert.ToInt32(searchResultXML.DocumentElement.SelectSingleNode("pageNo").InnerText);
                totalPage = Convert.ToInt32(searchResultXML.DocumentElement.SelectSingleNode("totalPages").InnerText);
                //Response.Write(searchResultXML.DocumentElement.InnerXml.Replace("<", "&lt;").Replace(">","&gt;"));
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in resultNL)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                //Response.Write(resultNL.Count);
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    //Code added by Offshore for FB Issue 1073 -- Start
                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["timestamp"] = myVRMNet.NETFunctions.GetFormattedDate(dr["timestamp"].ToString())
                            + " " + Convert.ToDateTime(dr["timestamp"].ToString()).ToString(tformat);
                    }
                    //Code added by Offshore for FB Issue 1073 -- End 
                    dgResult.DataSource = dv;
                    dgResult.DataBind();
                    dgResult.Visible = true;
                    lblNoRecords.Visible = false;
                }
                else
                {
                    dgResult.Visible = false;
                    lblNoRecords.Visible = true;
                }
                if (totalPage > 1)
                    com.DisplayPaging(totalPage, pageNo, tblPage, "logList.aspx?t=1");
            }
            catch (Exception ex)
            {
                log.Trace("outputSearchResults: " + ex.StackTrace + " : " + ex.Message);
            }
        }


        protected void SearchResult()
        {
            string moduleName, errorCode, fromDate, toDate, severity, errorMsg;
            //read from the return XML 
            XmlDocument searchParamXML = new XmlDocument();
            //Response.Write(Session["searchParam"].ToString());
            //Response.End();
            searchParamXML.LoadXml(Session["searchParam"].ToString());

            moduleName = searchParamXML.DocumentElement.SelectSingleNode("//searchLog/module/moduleName").InnerText;
            errorCode = searchParamXML.DocumentElement.SelectSingleNode("//searchLog/module/moduleErrorCode").InnerText;
            fromDate = searchParamXML.DocumentElement.SelectSingleNode("//searchLog/from").InnerText;
            toDate = searchParamXML.DocumentElement.SelectSingleNode("//searchLog/to").InnerText;
            severity = searchParamXML.DocumentElement.SelectSingleNode("//searchLog/severity").InnerText;
            errorMsg = searchParamXML.DocumentElement.SelectSingleNode("//searchLog/message").InnerText;
            //FB 2027 Start
            StringBuilder inXML = new StringBuilder();
            inXML.Append("<searchLog>");
            inXML.Append(com.OrgXMLElement());//Organization Module Fixes
            inXML.Append("<module>");
            inXML.Append("<moduleName>" + moduleName + "</moduleName>");
            inXML.Append("<moduleSystemID></moduleSystemID>");
            inXML.Append("<moduleErrorCode>" + errorCode + "</moduleErrorCode>");
            inXML.Append("</module>");

            inXML.Append("<from>" + fromDate + "</from>");
            inXML.Append("<to>" + toDate + "</to>");
            inXML.Append("<severity>" + severity + "</severity>");
            inXML.Append("<file></file>");
            inXML.Append("<function></function>");
            inXML.Append("<line></line>");
            inXML.Append("<message>" + errorMsg + "</message>");
            inXML.Append("<pageNo>" + txtPage.Text + "</pageNo>");
            inXML.Append("</searchLog>");

            //log.Trace("SearchLog: " + inXML.ToString());
            String outXML = com.CallMyVRMServer("SearchLog", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
            //String outXML = com.CallCOM("SearchLog", inXML, Application["COM_ConfigPath"].ToString());
            //FB 2027 End
            log.Trace("SearchLog: " + outXML);

            Session.Add("outXML", outXML);
            Session.Add("searchParam", inXML);
            Session.Add("searchResult", outXML);

            outputSearchResult();
        }


        protected void searchAgain_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EventLog.aspx?S=ES"); // Diagnostics
            }
            catch (System.Threading.ThreadAbortException)
            {   
            }
        }
        protected void DisplayPaging(int totalPages, int currentPage)
        {
            try
            {
                tblPage.Visible = true;
                TableRow tr = new TableRow();
                TableCell tc;
                tblPage.CellPadding = 2;
                tblPage.Rows[0].Cells.Clear();
                tc = new TableCell();
                tc.Text = "Page(s):";
                tblPage.Rows[0].Cells.Clear();
                tblPage.Rows[0].Cells.Add(tc);
                for (int i = 1; i <= totalPages; i++)
                {
                    if ((i % 25) == 0)
                    {
                        tr = new TableRow();
                        tblPage.Rows.Add(tr);
                    }
                    tc = new TableCell();
                    if (i == currentPage)
                        tc.Text = i.ToString();
                    else
                        tc.Text = "<a href='LogList.aspx?pageNo=" + i + "'>" + i.ToString() + "</a>";
                    tr.Cells.Add(tc); //.Cells[0].Controls.Add(btnPageNo);
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DisplayPaging" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }
        }
    }
}
