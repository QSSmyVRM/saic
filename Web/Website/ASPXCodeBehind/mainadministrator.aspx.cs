/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using web_com_v18_Net;
using System.Xml.Schema;
/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

namespace ns_MYVRM
{
    public partial class MainAdministrator : System.Web.UI.Page
    {
        #region protected Members
        protected Label errLabel;
        protected DropDownList StartTimehr;
        protected DropDownList StartTimemi;
        protected DropDownList StartTimeap;
        protected DropDownList EndTimehr;
        protected DropDownList EndTimemi;
        protected DropDownList EndTimeap;
        protected DropDownList TimezoneSystems;
        protected DropDownList AutoAcpModConf;
        protected DropDownList RecurEnabled;
        protected DropDownList p2pConfEnabled;
        protected DropDownList DialoutEnabled;
        protected DropDownList DefaultPublic;
        protected DropDownList RealtimeType;
        protected DropDownList lstDefaultConferenceType;
        protected DropDownList lstRFIDValue;//FB 2724
        protected DropDownList lstEnableRoomConference;
        protected DropDownList lstEnableAudioVideoConference;
        protected DropDownList lstEnableAudioOnlyConference;
        protected DropDownList lstEnableNumericID;//FB 2870
        protected DropDownList IsDefaultOfficeHours;
        protected DropDownList lstRoomTreeLevel;
        protected DropDownList lstDefaultOfficeHours;
        protected DropDownList DynamicInviteEnabled;
        protected DropDownList drpenablesecuritybadge;//FB 2136 start
        protected DropDownList drpsecuritybadgetype;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecurityType;
        protected TextBox txtsecdeskemailid;
        protected System.Web.UI.HtmlControls.HtmlTableRow tdsecdeskemailid;//FB 2136 end
        protected MetaBuilders.WebControls.ComboBox systemStartTime;
        protected MetaBuilders.WebControls.ComboBox systemEndTime;
        
        protected TextBox ContactName;
        protected TextBox ContactEmail;
        protected TextBox ContactPhone;
        protected TextBox ContactAdditionInfo;
        protected TextBox txtiControlTimeout; //FB 2724
        protected CheckBox Open24;
        protected CheckBoxList DayClosed;
        //Audio Add On Starts..
        protected System.Web.UI.WebControls.DropDownList DrpConfcode;
        protected System.Web.UI.WebControls.DropDownList DrpLedpin;
        protected System.Web.UI.WebControls.DropDownList Drpavprm;
        protected System.Web.UI.WebControls.DropDownList DrpAudprm;
        //Audio Add On Ends..
        protected System.Web.UI.WebControls.TextBox txtiCalEmailId;//FB 1786
		
	
        protected DropDownList EnableBufferZone; //Merging Recurence
        protected DropDownList DrpListIcal; //FB 1782
        protected DropDownList DrpAppIcal; //FB 1782
        protected DropDownList DrpVIP;
        protected DropDownList DrpUniquePassword;

        protected DropDownList DrpDwnListAssignedMCU; // FB 1901

        //FB 1926 start
        protected CheckBox WklyChk;
        protected CheckBox DlyChk;
        protected CheckBox HourlyChk;
        protected CheckBox MinChk;
        //FB 1926 end
        protected DropDownList DrpDwnListMultiLingual; //FB 1830 - Translation
        protected DropDownList DrpPluginConfirm; // FB 2141
        protected DropDownList DrpDwnAttachmnts; // FB 2154
        protected DropDownList DrpDwnFilterTelepresence; // FB 2170
        protected DropDownList lstEnableRoomServiceType;//FB 2219
        protected DropDownList DrpDwnListSpRecur; // FB 2052         
        protected DropDownList DrpDwnListDeptUser; //FB 2269
		protected DropDownList lstEnablePIMServiceType; // FB 2038
        protected DropDownList lstEnableImmediateConference;//FB 2036
		protected DropDownList DrpDwnPasswordRule;//FB 2339
        protected DropDownList DrpDwnDedicatedVideo; // FB 2334
        //FB 2598 Starts
        protected DropDownList DrpDwnEnableCallmonitor;
        protected DropDownList DrpDwnEnableEM7;
        protected DropDownList DrpDwnEnableCDR;
        //FB 2598 Ends
        protected DropDownList DrpEnableE164DialPlan; // FB 2636
        protected DropDownList DropDownZuLu;//FB 2588 
        protected DropDownList lstEnableProfileSelection;//FB 2839
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnZuluChange; //FB 2588
        // FB 2335 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden Poly2MGC;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Poly2RMX;
        protected System.Web.UI.HtmlControls.HtmlInputHidden CTMS2Cisco;
        protected System.Web.UI.HtmlControls.HtmlInputHidden CTMS2Poly;

        protected System.Web.UI.WebControls.Image imgLayoutMapping1;
        protected System.Web.UI.WebControls.Image imgLayoutMapping2;
        protected System.Web.UI.WebControls.Image imgLayoutMapping3;
        protected System.Web.UI.WebControls.Image imgLayoutMapping4;
        // FB 2335 Ends
        protected DropDownList lstEnableAudioBridges;//FB 2023
        //FB 2359 Start
        protected DropDownList lstEnableConfPassword;
        protected DropDownList lstEnablePublicConf;
        protected DropDownList lstRoomprm;
        //FB 2359 End
        //FB 2348
        protected System.Web.UI.WebControls.DropDownList drpenablesurvey;
        protected System.Web.UI.WebControls.DropDownList drpsurveyoption;
        protected System.Web.UI.HtmlControls.HtmlContainerControl divSurveytimedur;
        protected System.Web.UI.HtmlControls.HtmlContainerControl divSurveyURL;
        protected System.Web.UI.WebControls.TextBox txtSurWebsiteURL;
        protected System.Web.UI.WebControls.TextBox txtTimeDur;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSurveyengine;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdsurveyoption;
        //FB 2565 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trUSROPT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trROOM;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFOPT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAUD;
        protected System.Web.UI.HtmlControls.HtmlTableRow trADMOPT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPIM;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSYS;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSUR;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFMAIL;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFSECDESK; 
        protected System.Web.UI.HtmlControls.HtmlTableRow trEPT; 
        protected System.Web.UI.HtmlControls.HtmlTableRow trFLY;
        protected System.Web.UI.HtmlControls.HtmlTableRow trOnfly;//FB 2426
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFDEF;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFTYPE;
        protected System.Web.UI.HtmlControls.HtmlTableRow trFEAT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAUTO;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNETSWT; //FB 2594
        protected System.Web.UI.HtmlControls.HtmlTableRow trICP; //FB 2724
        //FB 2565 End
        protected System.Web.UI.WebControls.DropDownList lstTopTier;//FB 2426
        protected System.Web.UI.WebControls.DropDownList lstMiddleTier;//FB 2426

        //FB 2565 Start
        protected System.Web.UI.WebControls.ImageButton img_USROPT;
        protected System.Web.UI.WebControls.ImageButton img_ROOM;
        protected System.Web.UI.WebControls.ImageButton img_CONFOPT;
        protected System.Web.UI.WebControls.ImageButton img_AUD;
        protected System.Web.UI.WebControls.ImageButton img_EPT;
        protected System.Web.UI.WebControls.ImageButton img_CONFMAIL; 
        protected System.Web.UI.WebControls.ImageButton img_FLY;
        protected System.Web.UI.WebControls.ImageButton img_SYS;
        protected System.Web.UI.WebControls.ImageButton img_CONFSECDESK;
        protected System.Web.UI.WebControls.ImageButton img_PIM;
        protected System.Web.UI.WebControls.ImageButton img_SUR;
        protected System.Web.UI.WebControls.ImageButton img_ADMOPT;
        protected System.Web.UI.WebControls.ImageButton img_CONFDEF;
        protected System.Web.UI.WebControls.ImageButton img_CONFTYPE;
        protected System.Web.UI.WebControls.ImageButton img_FEAT;
        protected System.Web.UI.WebControls.ImageButton img_AUTO;
        protected System.Web.UI.WebControls.ImageButton img_NETSWT; //FB 2595
        protected System.Web.UI.WebControls.ImageButton img_ICP;//FB 2724
        //FB 2565 End
       
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSurURL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTimeDur;

        //FB 2348
		protected DropDownList lstEnableVMR;
        protected System.Web.UI.WebControls.Button btnSubmit; //FB 2347
        protected System.Web.UI.WebControls.DropDownList lstEPinMail;//FB 2401

        protected System.Web.UI.WebControls.TextBox txtTearDownTime; //FB 2398
        protected System.Web.UI.WebControls.TextBox txtSetupTime;
        protected System.Web.UI.WebControls.TextBox txtDefaultConfDuration;//FB 2501
        protected System.Web.UI.HtmlControls.HtmlTableRow trBufferOptions;//FB 2398
        protected System.Web.UI.WebControls.DropDownList lstAcceptDecline; //FB 2419
        protected System.Web.UI.WebControls.DropDownList lstLineRate; //FB 2429

        protected System.Web.UI.WebControls.TextBox txtMCUSetupTime; //FB 2440
        protected System.Web.UI.WebControls.TextBox txtMCUTearDownTime;//FB 2440
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUBufferOptions;//FB 2440
        protected System.Web.UI.HtmlControls.HtmlTableRow trForceMCUBuffer;//FB 2440
        protected System.Web.UI.HtmlControls.HtmlTable tblForceMCUBuffer;//FB 2440
        protected DropDownList DrpForceMCUBuffer;//FB 2440
        protected System.Web.UI.WebControls.DropDownList MCUSetupDisplay; //FB 2998
        protected System.Web.UI.WebControls.DropDownList MCUTearDisplay; //FB 2998

		//FB 2469 - Starts
        protected System.Web.UI.WebControls.DropDownList lstEnableConfTZinLoc; //FB 2419
        protected System.Web.UI.WebControls.DropDownList lstSendConfirmationEmail; //FB 2419
        protected System.Web.UI.WebControls.DropDownList lstEnableSmartP2P; //FB 2430
        protected DropDownList lstMcuAlert;//FB 2472
		//FB 2486 Starts
        protected System.Web.UI.ScriptManager OrgOptionScriptManager;
        protected AjaxControlToolkit.ModalPopupExtender MessagePopup;
        protected System.Web.UI.WebControls.CheckBox chkmsg1;
        protected System.Web.UI.WebControls.CheckBox chkmsg2;
        protected System.Web.UI.WebControls.CheckBox chkmsg3;
        protected System.Web.UI.WebControls.CheckBox chkmsg4;
        protected System.Web.UI.WebControls.CheckBox chkmsg5;
        protected System.Web.UI.WebControls.CheckBox chkmsg6;
        protected System.Web.UI.WebControls.CheckBox chkmsg7;
        protected System.Web.UI.WebControls.CheckBox chkmsg8;
        protected System.Web.UI.WebControls.CheckBox chkmsg9;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration1;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration2;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration3;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration4;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration5;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration6;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration7;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration8;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration9;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg1;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg2;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg3;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg4;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg5;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg6;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg7;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg8;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg9;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTxtMsgDetails;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTxtMsg;
        protected System.Web.UI.WebControls.Button btnMsgSubmit;
        protected System.Web.UI.WebControls.Button btnReset;//ZD 100263
        //FB 2486 Ends

        //FB 2550 Starts
        protected System.Web.UI.WebControls.TextBox txtMaxVMRParty;
        protected System.Web.UI.HtmlControls.HtmlContainerControl tdMaxParty;
        protected System.Web.UI.HtmlControls.HtmlContainerControl tdMaxPartyCount;
        //FB 2550 Ends
        //FB 2571 Start
        protected System.Web.UI.WebControls.DropDownList lstenableFECC;
        protected System.Web.UI.WebControls.DropDownList lstdefaultFECC;
        protected System.Web.UI.HtmlControls.HtmlTableCell DefaultFECC;//FB 2571
        protected System.Web.UI.HtmlControls.HtmlTableCell DefaultFECCoptions;//FB 2571
        //FB 2571 End
		protected System.Web.UI.WebControls.DropDownList drpBrdgeExt; //FB 2610
        protected System.Web.UI.WebControls.TextBox txtMeetandGreetBuffer;//FB 2609

        //FB 2632 Starts
        protected System.Web.UI.WebControls.DropDownList drpCngSupport; 
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOCOperator;
        //FB 2632 Ends
		protected int Cloud = 0;//FB 2262 //FB 2599
		protected System.Web.UI.WebControls.DropDownList lstEnableRoomAdminDetails;//FB 2631
		//FB 2637 Starts
        protected System.Web.UI.WebControls.ListBox lstTier1;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTierIDs;
        //FB 2637 Ends
        //FB 2595 Starts
        protected System.Web.UI.WebControls.DropDownList drpSecureSwitch;
        protected System.Web.UI.WebControls.TextBox txtHardwareAdminEmail;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdsecureadminaddress;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNwtSwtiching;
        protected System.Web.UI.WebControls.DropDownList drpNwtSwtiching;
        protected System.Web.UI.WebControls.DropDownList drpNwtCallLaunch;
        protected System.Web.UI.WebControls.TextBox txtSecureLaunch;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNwtCallBuffer;
        //FB 2595 Ends
        //FB 2670 Starts
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnsiteAV;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGret;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitor;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOC;
        //FB 2670 Ends
		// FB 2641 Start
        protected System.Web.UI.WebControls.DropDownList drpEnableLineRate;
        protected System.Web.UI.WebControls.DropDownList drpEnableStartMode;
        // FB 2641 End
        protected System.Web.UI.WebControls.DropDownList lstSigRoomConf;//FB 2817
        protected System.Web.UI.WebControls.TextBox txtResponseTimeout; //FB 2993
        protected System.Web.UI.HtmlControls.HtmlTableRow trNetwork;//FB 2993
        //ZD 100263
        protected System.Web.UI.WebControls.TextBox txtWhiteList;
        protected System.Web.UI.WebControls.Button btnWhiteList;
        protected System.Web.UI.WebControls.ListBox lstWhiteList;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFileWhiteList;
        protected System.Web.UI.WebControls.CheckBox chkWhiteList;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWhiteList;

		protected System.Web.UI.WebControls.DropDownList DrpDwnEnableCA; //ZD 100151
        protected System.Web.UI.HtmlControls.HtmlTableRow trShwCusAttr; //ZD 100151 
        #endregion

        #region Private Members
        //string companyinfofile = "C:\\VRMSchemas_v18\\company.ifo";
        ns_Logger.Logger log;
        String tformat = "hh:mm tt";
        Int32 orgId = 11;
        //FB 1926 start
        Int32 wkly = 8;
        Int32 dly = 4;
        Int32 hourly = 2;
        Int32 minutely = 1;
        //FB 1926 end
        protected string language = ""; //FB 2335
        protected int languageid = 1; //FB 2486
        string isZuluChanged = ""; //FB 2588
        protected int enableCloudInstallation = 0; //ZD 100166
        #endregion

        myVRMNet.NETFunctions obj;
        public MainAdministrator()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }


        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("mainadministrator.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                Session["multisiloOrganizationID"] = null; //FB 2274 
                if (Application["client"].ToString().ToUpper() == "DISNEY")//FB 1985
                {
                    lstDefaultConferenceType.Enabled = false;
                    lstEnableAudioOnlyConference.Enabled = false;
                    //p2pConfEnabled.Enabled = false; //p2p for exchange
                    tblForceMCUBuffer.Attributes.Add("Style", "Display:block");//FB 2440
                }

                //FB 2426 Start
                if (Session["GuestRooms"].ToString() == "0")
                    trOnfly.Attributes.Add("style", "display:none");
                //FB 2426 End
                //FB 2335
                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                if (Session["languageID"] == null)
                    Session["languageID"] = "1";
                if (Session["languageID"].ToString() != "")
                    Int32.TryParse(Session["languageID"].ToString(), out languageid);

                //FB 2262,//FB 2599 - Starts
                if (Session["Cloud"] != null)
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                //ZD 100151
                if (Session["EnableEntity"].ToString() == "1")
                    trShwCusAttr.Visible = true;
                else
                    trShwCusAttr.Visible = false;

                //FB 2717 Starts
                if (Cloud == 1)
                {
                    lstEnableVMR.Items.Remove(lstEnableVMR.Items.FindByValue("0"));
                    lstEnableAudioVideoConference.Enabled = false;
					DrpDwnListAssignedMCU.Enabled = false;
                    //lstEnableAudioBridges.SelectedValue = "0";
                    //lstEnableAudioBridges.Enabled = false;
                    //lstDefaultConferenceType.Enabled = false;
                    //lstEnableVMR.Enabled = false;
                    //lstEnableVMR.SelectedValue = "3";
                }
                //FB 2262,//FB 2599 - Ends
                //FB 2717 End

                //ZD 100166 Starts
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);

                if (enableCloudInstallation == 1)
                {
                    EnableBufferZone.Enabled = false;
                    MCUSetupDisplay.Enabled = false;
                    MCUTearDisplay.Enabled = false;
                }
                //ZD 100166 End

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                Application["interval"] = ((Application["interval"] == null) ? "60" : Application["interval"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ"; 
                //FB 2588 Ends

                systemStartTime.Items.Clear();
                systemEndTime.Items.Clear();
                obj.BindTimeToListBox(systemStartTime, false, false);
                obj.BindTimeToListBox(systemEndTime, false, false);
                if (lstLineRate.Items.Count <= 0)
                    obj.BindLineRate(lstLineRate); //FB 2429
                //FB 2348 start FB 2842 Start
                img_USROPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trUSROPT.ClientID + "', this.alt);return false;");
                img_ROOM.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trROOM.ClientID + "', this.alt);return false;");
                img_CONFOPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFOPT.ClientID + "', this.alt);return false;");
                img_AUD.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trAUD.ClientID + "', this.alt);return false;");
                img_ADMOPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trADMOPT.ClientID + "', this.alt);return false;");
                img_EPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trEPT.ClientID + "', this.alt);return false;");
                img_PIM.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trPIM.ClientID + "', this.alt);return false;");
                img_SUR.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trSUR.ClientID + "', this.alt);return false;");
                img_SYS.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trSYS.ClientID + "', this.alt);return false;");
                img_FLY.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trFLY.ClientID + "', this.alt);return false;");//FB 2426
                img_CONFMAIL.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFMAIL.ClientID + "', this.alt);return false;");
                img_CONFSECDESK.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFSECDESK.ClientID + "', this.alt);return false;");
                img_CONFDEF.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFDEF.ClientID + "', this.alt);return false;");
                img_CONFTYPE.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFTYPE.ClientID + "', this.alt);return false;");
                img_FEAT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trFEAT.ClientID + "', this.alt);return false;");
                img_AUTO.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trAUTO.ClientID + "', this.alt);return false;");
                img_NETSWT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trNETSWT.ClientID + "', this.alt);return false;");
                img_ICP.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trICP.ClientID + "', this.alt);return false;");//FB 2724
                //FB 2348 End FB 2842  End

                //FB 2486 Starts
                #region DropdownMsg

                if (!IsPostBack)
                {

                    String inxml = "<GetConfMsg>" + obj.OrgXMLElement() + "<UserID>" + Session["userID"].ToString() + "</UserID><language>1</language></GetConfMsg>";
                    String outxml = obj.CallMyVRMServer("GetConfMsg", inxml, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetConfMsgList/GetConfMsg");

                    obj.LoadOrgList(drpdownconfmsg1, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg2, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg3, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg4, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg5, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg6, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg7, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg8, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg9, nodes, "ConfMsgID", "ConfMsg");

                    obj.SetToolTip(drpdownconfmsg1);
                    obj.SetToolTip(drpdownconfmsg2);
                    obj.SetToolTip(drpdownconfmsg3);
                    obj.SetToolTip(drpdownconfmsg4);
                    obj.SetToolTip(drpdownconfmsg5);
                    obj.SetToolTip(drpdownconfmsg6);
                    obj.SetToolTip(drpdownconfmsg7);
                    obj.SetToolTip(drpdownconfmsg8);
                    obj.SetToolTip(drpdownconfmsg9);

                    ArrayList min5 = new ArrayList();
                    ArrayList min2 = new ArrayList();
                    ArrayList minS = new ArrayList();
                    Int32 ii = 0;
                    for (Int32 m = 5; m <= 30; m = m + 5)
                        min5.Insert(ii++, m + "M");

                    drpdownmsgduration1.DataSource = min5;
                    drpdownmsgduration1.DataBind();

                    drpdownmsgduration4.DataSource = min5;
                    drpdownmsgduration4.DataBind();

                    drpdownmsgduration7.DataSource = min5;
                    drpdownmsgduration7.DataBind();

                    ii = 0;
                    for (Int32 m = 2; m <= 20; m = m + 2)
                        min2.Insert(ii++, m + "M");

                    drpdownmsgduration2.DataSource = min2;
                    drpdownmsgduration2.DataBind();

                    drpdownmsgduration5.DataSource = min2;
                    drpdownmsgduration5.DataBind();

                    drpdownmsgduration8.DataSource = min2;
                    drpdownmsgduration8.DataBind();
                    
                    ii = 0;
                    for (Int32 m = 30; m <= 120; m = m + 30)
                        minS.Insert(ii++, m + "s");

                    drpdownmsgduration3.DataSource = minS;
                    drpdownmsgduration3.DataBind();

                    drpdownmsgduration6.DataSource = minS;
                    drpdownmsgduration6.DataBind();

                    drpdownmsgduration9.DataSource = minS;
                    drpdownmsgduration9.DataBind();
                }

                #endregion
                //FB 2486 Ends
                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    
                    //btnMsgSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnMsgSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263 start
                    btnReset.Visible = false;
                    btnSubmit.Visible = false;
                    btnMsgSubmit.Visible = false;
                    //ZD 100263 End
                }
                // FB 2796 Start
                else
                {
                    btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    btnMsgSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                }
                // FB 2796 End
                if (!IsPostBack)
                {
                    
                    btnSubmit.Attributes.Add("onclick", "javascript:ChangeValidator()");//FB 2347

                    string inXML = "<GetOrgOptions><UserID>" + Session["userID"] + "</UserID>"+ obj.OrgXMLElement() +"</GetOrgOptions>";
                    string outXML = obj.CallMyVRMServer("GetOrgOptions", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    BindData(outXML);
                }
                if (Request.QueryString["m"] != null) 
                    if (Request.QueryString["m"].ToString().Equals("2"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");
                        errLabel.Visible = true;
                    }
                    else  if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }
                //FB 2993 Start
                if (Session["EnableNetworkFeatures"].ToString() != null)
                {
                    if (Session["EnableNetworkFeatures"].ToString().Equals("1"))
                    {
                        trNetwork.Attributes.Add("style", "display:");
                    }
                    else
                    {
                        trNETSWT.Attributes.Add("style", "display:none");
                        trNetwork.Attributes.Add("style", "display:none");
                        
                    }
                }
                //FB 2993 End

            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.Message + " : " + ex.StackTrace);
            }
        }


        private void BindData(String outXML)
        {
            string tmpstr = "";
            string[] tmpstrs;
            int i, tmpint;
            XmlDocument xmldoc = new XmlDocument();
            XmlNodeList nodelist;
            XmlNode node = (XmlNode)xmldoc.DocumentElement;

            try
            {
                xmldoc.LoadXml(outXML);
                log.Trace("GetOrgOptions: " + outXML);
                tmpstr = xmldoc.SelectSingleNode("//GetOrgOptions/TimezoneSystemID").InnerText;
                nodelist = xmldoc.SelectNodes("//GetOrgOptions/TimezoneSystems/TimezoneSystem");

                TimezoneSystems.Items.Clear();
                for (i = 0; i < nodelist.Count; i++)
                    TimezoneSystems.Items.Add(new ListItem(obj.GetTranslatedText(nodelist.Item(i).SelectSingleNode("Name").InnerText), nodelist.Item(i).SelectSingleNode("ID").InnerText));
                try
                {
                    TimezoneSystems.ClearSelection();
                    TimezoneSystems.Items.FindByValue(tmpstr).Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace("Timezone Type: " + ex.Message);
                }

                //Auto Accept modified conferences
                try
                {
                    AutoAcpModConf.ClearSelection();
                    AutoAcpModConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AutoAcceptModifiedConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("Autoacceptmodifiedconference: " + ex.Message); }

                //Recur Enabled
                try
                {
                    RecurEnabled.ClearSelection();
                    RecurEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRecurringConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("RecurEnabled: " + ex.Message); }

                // Dialout enabled
                try
                {
                    DialoutEnabled.ClearSelection();
                    DialoutEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDialout").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DialoutEnabled: " + ex.Message + " : " + ex.StackTrace); }

                //EnableDynamicInvite
                try
                {
                    DynamicInviteEnabled.ClearSelection();
                    DynamicInviteEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDynamicInvite").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableDynamicInvite: " + ex.Message); }

                // EnableP2PConference
                try
                {
                    p2pConfEnabled.ClearSelection();
                    //if (Application["client"].ToString().ToUpper() != "DISNEY")//FB 1985 //p2p for exchange
                    if(p2pConfEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableP2PConference").InnerText) != null)
                        p2pConfEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableP2PConference").InnerText).Selected = true;
                    //else //p2p for exchange
                    //   p2pConfEnabled.SelectedIndex = 2; //p2p for exchange
                }
                catch (Exception ex) { log.Trace("EnableP2PConference: " + ex.Message); }
                // EnableRealtimeDisplay
                try
                {
                    RealtimeType.ClearSelection();
                    RealtimeType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRealtimeDisplay").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableRealtimeDisplay: " + ex.Message); }
                // DefaultConferencesAsPublic
                try
                {
                    DefaultPublic.ClearSelection();
                    DefaultPublic.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultConferencesAsPublic").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DefaultConferencesAsPublic: " + ex.Message); }
                // DefaultConferenceType
                try
                {
                    lstDefaultConferenceType.ClearSelection();
                    if (Application["client"].ToString().ToUpper() != "DISNEY")//FB 1985
                        lstDefaultConferenceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultConferenceType").InnerText).Selected = true;
                    else
                        lstDefaultConferenceType.SelectedIndex = 1;
                }
                catch (Exception ex) { log.Trace("DefaultConferenceType: " + ex.Message); }
                // RFIC Tag Value //FB 2724
                try
                {
                    lstRFIDValue.ClearSelection();
                    lstRFIDValue.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/RFIDTagValue").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("RFIDTagValue: " + ex.Message); }

                if (xmldoc.SelectSingleNode("//GetOrgOptions/iControlTimeout") != null)
                    txtiControlTimeout.Text = xmldoc.SelectSingleNode("//GetOrgOptions/iControlTimeout").InnerText;

                // EnableRoomConference
                try
                {
                    lstEnableRoomConference.ClearSelection();
                    lstEnableRoomConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableRoomConference: " + ex.Message); }
                // EnableAudioVideoConference
                try
                {
                    lstEnableAudioVideoConference.ClearSelection();
                    lstEnableAudioVideoConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioVideoConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableAudioVideoConference: " + ex.Message); }
                // EnableAudioOnlyConference
                //FB 2870 Start
                try
                {
                    lstEnableNumericID.ClearSelection();
                    lstEnableNumericID.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableNumericID").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableNumericID: " + ex.Message); }
                //FB 2870 End
                try
                {
                    lstEnableAudioOnlyConference.ClearSelection();
                    if (Application["client"].ToString().ToUpper() != "DISNEY")//FB 1985
                        lstEnableAudioOnlyConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioOnlyConference").InnerText).Selected = true;
                    else
                        lstEnableAudioOnlyConference.SelectedIndex = 2;
                }
                catch (Exception ex) { log.Trace("EnableAudioOnlyConference: " + ex.Message); }
                // DefaultCalendarToOfficeHours
                try
                {
                    lstDefaultOfficeHours.ClearSelection();
                    lstDefaultOfficeHours.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultCalendarToOfficeHours").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DefaultCalendarToOfficeHours: " + ex.Message); }
                // RoomTreeExpandLevel
                try
                {
                    lstRoomTreeLevel.ClearSelection();
                    lstRoomTreeLevel.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/RoomTreeExpandLevel").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("RoomtreeExpandLevel " + ex.Message); }
				

                //Merging Recurrence
                 try
                {
                    EnableBufferZone.ClearSelection();
                    EnableBufferZone.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableBufferZone").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableBufferZone " + ex.Message); }

                //FB 2440
                 if (xmldoc.SelectSingleNode("//GetOrgOptions/MCUTeardonwnTime") != null)
                     txtMCUTearDownTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/MCUTeardonwnTime").InnerText;
                if (xmldoc.SelectSingleNode("//GetOrgOptions/McuSetupTime") != null)
                    txtMCUSetupTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/McuSetupTime").InnerText;
                try
                {
                    DrpForceMCUBuffer.ClearSelection();
                    DrpForceMCUBuffer.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUBufferPriority").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DrpForceMCUBuffer " + ex.Message); }

                 //FB 2440

                //FB 2998
                try
                {
                    MCUSetupDisplay.ClearSelection();
                    MCUSetupDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUSetupDisplay").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("MCUSetupDisplay " + ex.Message); }
                try
                {
                    MCUTearDisplay.ClearSelection();
                    MCUTearDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUTearDisplay").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("MCUTearDisplay " + ex.Message); }

                //FB 2398 Start
                 txtSetupTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SetupTime").InnerText;
                 txtTearDownTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/TearDownTime").InnerText;

                 if (EnableBufferZone.SelectedValue == "1")
                 {
                     //trBufferOptions.Attributes.Remove("style");
                     trBufferOptions.Attributes.Add("style", "display:");//TCK  #100154
                     trMCUBufferOptions.Attributes.Add("style", "display:");//FB 2440 //TCK  #100154
                     trForceMCUBuffer.Attributes.Add("style", "display:");//FB 2440 // TCK  #100154
                 }
                 else
                 {
                     //trBufferOptions.Attributes.Remove("style");
                     trBufferOptions.Attributes.Add("style", "display:none");//TCK #100154
                     trMCUBufferOptions.Attributes.Add("style", "display:none");//FB 2440 //TCK  #100154
                     trForceMCUBuffer.Attributes.Add("style", "display:none");//FB 2440 //TCK  #100154
                 }
                 //FB 2398 End
                ContactName.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/Name").InnerText;
                ContactEmail.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/Email").InnerText;
                ContactPhone.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/Phone").InnerText;
                ContactAdditionInfo.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/AdditionInfo").InnerText;


                //MYVRM.systemavailtime systemAvail = new MYVRM.systemavailtime(root.SelectSingleNode("systemAvail").OuterXml);
                Open24.Checked = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/IsOpen24Hours").InnerText.Equals("1");

                if (!Open24.Checked)
                {
                    systemStartTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/StartTime").InnerText;
                    systemEndTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/EndTime").InnerText;
                    try
                    {
                        systemStartTime.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemStartTime.Text)).ToString(tformat); //FB 2588
                        systemStartTime.ClearSelection();
                        systemStartTime.Items.FindByValue(systemStartTime.Text).Selected = true;
                    }
                    catch (Exception ex) { log.Trace(ex.Message + ":" + ex.StackTrace); }
                    try
                    {
                        systemEndTime.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemEndTime.Text)).ToString(tformat); //FB 2588
                        systemEndTime.ClearSelection();
                        systemEndTime.Items.FindByValue(systemEndTime.Text).Selected = true;
                    }
                    catch (Exception ex) { log.Trace(ex.Message + ":" + ex.StackTrace); }
                }

                else
                {
                    systemStartTime.Text = DateTime.Parse("12:00 AM").ToString(tformat);
                    systemEndTime.Text = DateTime.Parse("11:00 PM").ToString(tformat); //FB 2588
                }
                //FB-1642 Audio Add On Starts..
                try
                {
                    DrpConfcode.ClearSelection();
                    DrpConfcode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ConferenceCode").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("ConferenceCode: " + ex.Message + " : " + ex.StackTrace); }

                try
                {
                    DrpLedpin.ClearSelection();
                    DrpLedpin.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/LeaderPin").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("LeaderPin: " + ex.Message + " : " + ex.StackTrace); }

                try
                {
                    Drpavprm.ClearSelection();
                    Drpavprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AdvAvParams").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("AdvAvParams: " + ex.Message + " : " + ex.StackTrace); }

                try
                {
                    DrpAudprm.ClearSelection();
                    DrpAudprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AudioParams").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("AudioParams: " + ex.Message + " : " + ex.StackTrace); }
                //FB-1642 Audio Add On Ends..

                //FB 1786 - Start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/iCalReqEmailId") != null)
                    txtiCalEmailId.Text = xmldoc.SelectSingleNode("//GetOrgOptions/iCalReqEmailId").InnerText;
                //FB 1786 - End

                //FB 2610 Starts
                drpBrdgeExt.ClearSelection();
                if (drpBrdgeExt.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/showBridgeExt").InnerText) != null)
                    drpBrdgeExt.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/showBridgeExt").InnerText).Selected = true;
                //FB 2610 Ends

                tmpstrs = ((tmpstr = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/DaysClosed").InnerText)).Split(',');
                tmpint = ((tmpstr.Trim()).Equals("")) ? -1 : tmpstrs.Length;
                for (i = 0; i < tmpint; i++)
                    DayClosed.Items[Convert.ToInt16(tmpstrs[i]) - 1].Selected = true;
				//FB 1782 start
                DrpListIcal.ClearSelection();
                if (DrpListIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendIcal").InnerText) != null)
                    DrpListIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendIcal").InnerText).Selected = true;
                DrpAppIcal.ClearSelection();
                if (DrpAppIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendApprovalIcal").InnerText) != null)
                    DrpAppIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendApprovalIcal").InnerText).Selected = true;
                
                //FB 1782 end

                DrpVIP.ClearSelection();
                if (DrpVIP.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isVIP").InnerText) != null)
                    DrpVIP.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isVIP").InnerText).Selected = true;

                DrpUniquePassword.ClearSelection();
                if (DrpUniquePassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isUniquePassword").InnerText) != null)
                    DrpUniquePassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isUniquePassword").InnerText).Selected = true;

                //FB 1901
                DrpDwnListAssignedMCU.ClearSelection();
                if (DrpDwnListAssignedMCU.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isAssignedMCU").InnerText) != null)
                    DrpDwnListAssignedMCU.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isAssignedMCU").InnerText).Selected = true;
                //FB 1830 - Translation
                DrpDwnListMultiLingual.ClearSelection();
                if (DrpDwnListMultiLingual.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isMultiLingual").InnerText) != null)
                    DrpDwnListMultiLingual.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isMultiLingual").InnerText).Selected = true;

                //FB 2141
                DrpPluginConfirm.ClearSelection();
                if (DrpPluginConfirm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/PIMNotifications").InnerText) != null)
                    DrpPluginConfirm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/PIMNotifications").InnerText).Selected = true;
                //FB 2154
                DrpDwnAttachmnts.ClearSelection();
                if (DrpDwnAttachmnts.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ExternalAttachments").InnerText) != null)
                    DrpDwnAttachmnts.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ExternalAttachments").InnerText).Selected = true;

                //FB 2170
                DrpDwnFilterTelepresence.ClearSelection();
                if (DrpDwnFilterTelepresence.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/filterTelepresence").InnerText) != null)
                    DrpDwnFilterTelepresence.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/filterTelepresence").InnerText).Selected = true;

                //FB 2219
                lstEnableRoomServiceType.ClearSelection();
                if (lstEnableRoomServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomServiceType").InnerText) != null)
                    lstEnableRoomServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomServiceType").InnerText).Selected = true;

                //FB 1926
                if (xmldoc.SelectSingleNode("//GetOrgOptions/ReminderMask") != null)
                    SetRemainderfromMask(xmldoc.SelectSingleNode("//GetOrgOptions/ReminderMask").InnerText);

                 //FB 2052
                DrpDwnListSpRecur.ClearSelection();
                if (DrpDwnListSpRecur.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isSpecialRecur").InnerText) != null)
                    DrpDwnListSpRecur.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isSpecialRecur").InnerText).Selected = true;

                //FB 2269
                DrpDwnListDeptUser.ClearSelection();
                if (DrpDwnListDeptUser.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isDeptUser").InnerText.Trim()) != null)
                    DrpDwnListDeptUser.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isDeptUser").InnerText.Trim()).Selected = true;

				//FB 2038
                lstEnablePIMServiceType.ClearSelection();
                if (lstEnablePIMServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePIMServiceType").InnerText) != null)
                    lstEnablePIMServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePIMServiceType").InnerText).Selected = true;
                
                //FB 2036
                lstEnableImmediateConference.ClearSelection();
                if (lstEnableImmediateConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableImmConf").InnerText) != null)
                    lstEnableImmediateConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableImmConf").InnerText).Selected = true;

                lstEnableAudioBridges.ClearSelection(); //FB 2023
                if (lstEnableAudioBridges.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioBridges").InnerText) != null)
                {
                    lstEnableAudioBridges.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioBridges").InnerText).Selected = true;
                    //if (Cloud == 1) //FB 2262 //FB 2599 //FB 2717
                    //    if (lstEnableAudioBridges.SelectedValue == "1")
                    //        lstEnableAudioBridges.SelectedValue = "0";
                }
                //FB 2359 Start
                lstEnableConfPassword.ClearSelection(); 
                if (lstEnableConfPassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConferencePassword").InnerText) != null)
                    lstEnableConfPassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConferencePassword").InnerText).Selected = true;

                lstEnablePublicConf.ClearSelection(); 
                if (lstEnablePublicConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePublicConference").InnerText) != null)
                    lstEnablePublicConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePublicConference").InnerText).Selected = true;

                lstRoomprm.ClearSelection(); 
                if (lstRoomprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomParam").InnerText) != null)
                    lstRoomprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomParam").InnerText).Selected = true;
                //FB 2359 End
                //FB 2136 start
                drpenablesecuritybadge.ClearSelection();
                if (drpenablesecuritybadge.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSecurityBadge").InnerText) != null)
                    drpenablesecuritybadge.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSecurityBadge").InnerText).Selected = true;

                drpsecuritybadgetype.Style.Add("display","block");
                tdSecurityType.Attributes.Add("style", "display:block");
                if ((xmldoc.SelectSingleNode("//GetOrgOptions/EnableSecurityBadge").InnerText) == "0")
                {
                    drpsecuritybadgetype.Style.Add("display","none");
                    tdSecurityType.Attributes.Add("style", "display:none");
                    tdsecdeskemailid.Attributes.Add("style", "visibility:hidden");
                }

                obj.BindSecurityBadgeType(drpsecuritybadgetype); //FB 2136
                drpsecuritybadgetype.ClearSelection();
                if (drpsecuritybadgetype.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecurityBadgeType").InnerText) != null)
                    drpsecuritybadgetype.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecurityBadgeType").InnerText).Selected = true;
                if ((xmldoc.SelectSingleNode("//GetOrgOptions/SecurityBadgeType").InnerText) == "1")
                {
                    tdsecdeskemailid.Attributes.Add("style", "visibility:hidden");
                }

                if (xmldoc.SelectSingleNode("//GetOrgOptions/SecurityDeskEmailId") != null)
                    txtsecdeskemailid.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SecurityDeskEmailId").InnerText;

                //FB 2136 end
                //FB 2339 Start
                DrpDwnPasswordRule.ClearSelection();
                if (DrpDwnPasswordRule.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePasswordRule").InnerText) != null)
                    DrpDwnPasswordRule.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePasswordRule").InnerText).Selected = true;
                //FB 2339 End

                //FB 2334
                DrpDwnDedicatedVideo.ClearSelection();
                if (DrpDwnDedicatedVideo.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DedicatedVideo").InnerText) != null)
                    DrpDwnDedicatedVideo.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DedicatedVideo").InnerText).Selected = true;

                //FB 2335
                //VMR Start
                lstEnableVMR.ClearSelection();
                if (lstEnableVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableVMR").InnerText) != null)
                    lstEnableVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableVMR").InnerText).Selected = true;
                Session.Add("EnableVMR", xmldoc.SelectSingleNode("//GetOrgOptions/EnableVMR").InnerText); // FB 2620-Doubt
                //VMR End

                //FB 2401 Start
                lstEPinMail.ClearSelection();
                if (lstEPinMail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEPDetails").InnerText) != null)
                    lstEPinMail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEPDetails").InnerText).Selected = true;
                //FB 2401 End

                //FB 2472 Start
                lstMcuAlert.ClearSelection();
                if (lstMcuAlert.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUAlert").InnerText) != null)
                    lstMcuAlert.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUAlert").InnerText).Selected = true;
                //FB 2472 End

                try
                {
                    fnSetLayoutImage(xmldoc.SelectSingleNode("//GetOrgOptions/defPolycomMGCLO").InnerText, xmldoc.SelectSingleNode("//GetOrgOptions/defPolycomRMXLO").InnerText, xmldoc.SelectSingleNode("//GetOrgOptions/defCTMSLO").InnerText, xmldoc.SelectSingleNode("//GetOrgOptions/defCiscoTPLO").InnerText);  
                }
                catch (Exception ex) { log.Trace("Def Layout: " + ex.Message + " : " + ex.StackTrace); }

                //FB 2348 Start
                drpenablesurvey.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/EnableSurvey").InnerText;
                if (drpenablesurvey.SelectedValue == "1")
                {
                    tdSurveyengine.Attributes.Add("style", "visibility: visible");
                    tdsurveyoption.Attributes.Add("style", "visibility: visible");
                }
                drpsurveyoption.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/SurveyOption").InnerText;
                if (drpsurveyoption.SelectedValue == "2" && drpenablesurvey.SelectedValue == "1")
                {
                    divSurveytimedur.Attributes.Add("Style", "Display:block");
                    divSurveyURL.Attributes.Add("Style", "Display:block");
                }
                else
                {
                    divSurveytimedur.Attributes.Add("Style", "Display:none");
                    divSurveyURL.Attributes.Add("Style", "Display:none");
                }
                txtSurWebsiteURL.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SurveyURL").InnerText;
                txtTimeDur.Text = xmldoc.SelectSingleNode("//GetOrgOptions/TimeDuration").InnerText;

                hdnSurURL.Value = txtSurWebsiteURL.Text;
                hdnTimeDur.Value = txtTimeDur.Text;
                //FB 2348 End
                //FB 2419 - Starts
                lstAcceptDecline.ClearSelection();
                if (lstAcceptDecline.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAcceptDecline").InnerText) != null)
                    lstAcceptDecline.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAcceptDecline").InnerText).Selected = true;
                //FB 2419 - End
                //FB 2429 - Starts
                lstLineRate.ClearSelection();
                if (lstLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultLineRate").InnerText) != null)
                    lstLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultLineRate").InnerText).Selected = true;
                //FB 2429 - End

                string TxtMsg = "";
                node = xmldoc.SelectSingleNode("//GetOrgOptions/OrgMessage"); //FB 2486
                if (node != null)
                {
                    TxtMsg = xmldoc.SelectSingleNode("//GetOrgOptions/OrgMessage").InnerText.Trim();
                    GetOrgMessages(TxtMsg);
                }
                
                //FB 2426 Start
                lstEnableSmartP2P.ClearSelection();//FB 2430
                if (lstEnableSmartP2P.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSmartP2P").InnerText) != null)
                    lstEnableSmartP2P.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSmartP2P").InnerText).Selected = true;


               //FB 2571 - START
                try
                {
                    lstenableFECC.ClearSelection();
                    if (lstenableFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableFECC").InnerText) != null)
                        lstenableFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableFECC").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableFECC " + ex.Message); }

                if (lstenableFECC.SelectedValue == "2")
                {
                    DefaultFECC.Attributes.Add("style", "visibility: hidden");
                    DefaultFECCoptions.Attributes.Add("style", "visibility: hidden");
                }
                else
                {
                    DefaultFECC.Attributes.Add("style", "visibility:visible");
                    DefaultFECCoptions.Attributes.Add("style", "visibility:visible");
                }
                if (lstenableFECC.SelectedValue == "0")
                {
                    DefaultFECC.Attributes.Add("style", "visibility:visible");
                    DefaultFECCoptions.Attributes.Add("style", "value:1");
                    lstdefaultFECC.Attributes.Add("disabled", "disabled");
                }
                //FB 2571 - Starts
                lstdefaultFECC.ClearSelection();
                if (lstdefaultFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultFECC").InnerText) != null)
                    lstdefaultFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultFECC").InnerText).Selected = true;
                //FB 2571 End
                //FB 2588 Start
                try
                {
                    DropDownZuLu.Attributes.Add("onchange", "javascript:fnChangeZulu()");
                    DropDownZuLu.ClearSelection();
                    if (DropDownZuLu.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableZulu").InnerText) != null)
                        DropDownZuLu.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableZulu").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableZulu " + ex.Message); }
                if (DropDownZuLu.SelectedValue == "1")
                {
                    TimezoneSystems.Attributes.Add("style", "visibility:visible");
                    TimezoneSystems.Attributes.Add("style", "value:0");
                    TimezoneSystems.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    TimezoneSystems.Attributes.Add("style", "visibility:visible");
                    
                }
                //FB 2588 End
                
                //FB 2598 Starts 
                DrpDwnEnableCallmonitor.ClearSelection();
                if (DrpDwnEnableCallmonitor.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCallmonitor").InnerText) != null)
                    DrpDwnEnableCallmonitor.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCallmonitor").InnerText).Selected = true;
                Session.Add("EnableCallmonitor", xmldoc.SelectSingleNode("//GetOrgOptions/EnableCallmonitor").InnerText);

                DrpDwnEnableEM7.ClearSelection();
                if (DrpDwnEnableEM7.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEM7").InnerText) != null)
                    DrpDwnEnableEM7.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEM7").InnerText).Selected = true;
                Session.Add("EnableEM7", xmldoc.SelectSingleNode("//GetOrgOptions/EnableEM7").InnerText);

                DrpDwnEnableCDR.ClearSelection();
                if (DrpDwnEnableCDR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCDR").InnerText) != null)
                    DrpDwnEnableCDR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCDR").InnerText).Selected = true;
                Session.Add("EnableCDR", xmldoc.SelectSingleNode("//GetOrgOptions/EnableCDR").InnerText);
                //FB 2598 Ends

                if (xmldoc.SelectSingleNode("//GetOrgOptions/AlertforTier1") != null) //FB 2637
                    hdnTierIDs.Value = xmldoc.SelectSingleNode("//GetOrgOptions/AlertforTier1").InnerText + "|";

                string alert = hdnTierIDs.Value;
				
				// FB 2636 Starts
                DrpEnableE164DialPlan.ClearSelection();
                if (DrpEnableE164DialPlan.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDialPlan").InnerText) != null)
                    DrpEnableE164DialPlan.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDialPlan").InnerText).Selected = true;
                // FB 2636 End


                //FB 2839 Start
                lstEnableProfileSelection.ClearSelection();
                if (lstEnableProfileSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableProfileSelection").InnerText) != null)
                    lstEnableProfileSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableProfileSelection").InnerText).Selected = true;
                //FB 2839 End

                String inXML = "", toptier = "", middletier = "";
                inXML += "<GetLocations>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "</GetLocations>";
                log.Trace("getLocations InXML: " + inXML);
                String outXML1 = obj.CallMyVRMServer("GetLocations", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("getLocations OutXML: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc1 = new XmlDocument();
                    xmldoc1.LoadXml(outXML1);
                    XmlNodeList nodes = xmldoc1.SelectNodes("//GetLocations/Location");
                    obj.LoadList(lstTopTier, nodes, "ID", "Name");
                    //FB 2637 Starts
                    obj.LoadList(lstTier1, nodes, "ID", "Name"); 
                    foreach (ListItem li in lstTier1.Items)
                    {
                        if (alert.IndexOf(li.Value) > -1)
                        {
                            li.Selected = true;
                        }
                    }
                    //FB 2637 Ends
                }

                toptier = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/DefTopTier").InnerText;
                middletier = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/DefMiddleTier").InnerText;
                Session.Add("OnflyTopTierName", toptier);
                Session.Add("OnflyMiddleTierName", middletier);
                
                lstTopTier.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyTopTierID").InnerText;
                Session.Add("OnflyTopTierID", lstTopTier.SelectedValue);
                UpdateMiddleTiers(new Object(), new EventArgs());
                lstMiddleTier.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyMiddleTierID").InnerText;
                Session.Add("OnflyMiddleTierID", lstMiddleTier.SelectedValue);

                try
                {
                    lstTopTier.ClearSelection();
                    lstTopTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyTopTierID").InnerText).Selected = true;
                    if ((lstTopTier.Items.Count > 1) && (lstTopTier.SelectedIndex > 0))
                    {
                        UpdateMiddleTiers(new Object(), new EventArgs());
                        if (lstMiddleTier.Items.Count > 0)
                        {
                            lstMiddleTier.ClearSelection();
                            lstMiddleTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyMiddleTierID").InnerText).Selected = true;
                        }
                    }
                     }
                catch (Exception ex1) { }
                //FB 2426 End
                //FB 2469 - Starts
                lstEnableConfTZinLoc.ClearSelection();
                if (lstEnableConfTZinLoc.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConfTZinLoc").InnerText) != null)
                    lstEnableConfTZinLoc.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConfTZinLoc").InnerText).Selected = true;
                lstSendConfirmationEmail.ClearSelection();
                if (lstSendConfirmationEmail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SendConfirmationEmail").InnerText) != null)
                    lstSendConfirmationEmail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SendConfirmationEmail").InnerText).Selected = true;
                //FB 2469 - End
                //FB 2501 - Start
                txtDefaultConfDuration.Text = xmldoc.SelectSingleNode("//GetOrgOptions/DefaultConfDuration").InnerText;
                if (txtDefaultConfDuration.Text == "")
                {
                    txtDefaultConfDuration.Text = "60";
                }
                //FB 2501 - End
                //FB 2550 Starts
                int VMRtype=0;
                Int32.TryParse(lstEnableVMR.SelectedValue.ToString(),out VMRtype);
                tdMaxParty.Attributes.Add("style", "display:none");
                tdMaxPartyCount.Attributes.Add("style", "display:none");
                if ((DynamicInviteEnabled.SelectedValue.ToString().Equals("1")) && VMRtype > 0)
                {
                    tdMaxParty.Attributes.Add("style", "display:block");
                    tdMaxPartyCount.Attributes.Add("style", "display:block");
                    txtMaxVMRParty.Text = xmldoc.SelectSingleNode("//GetOrgOptions/MaxPublicVMRParty").InnerText;
                }
                else
                    txtMaxVMRParty.Text = "0";
                //FB 2550 Ends
                txtMeetandGreetBuffer.Text = xmldoc.SelectSingleNode("//GetOrgOptions/MeetandGreetBuffer").InnerText;//FB 2609

                //FB 2632 Starts
                drpCngSupport.Attributes.Add("onchange", "javascript:fnUpdateCngSupport()");
                drpCngSupport.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport") != null)
                {
                    if (drpCngSupport.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport").InnerText) != null)
                    {
                        drpCngSupport.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport").InnerText).Selected = true;
                        if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport").InnerText == "0")
                        {
                            chkMeetandGreet.Disabled = true;
                            chkOnSiteAVSupport.Disabled = true;
                            chkDedicatedVNOCOperator.Disabled = true;
                            chkConciergeMonitoring.Disabled = true;
                        }
                    }
                }

                if (node.SelectSingleNode("//GetOrgOptions/MeetandGreetinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/MeetandGreetinEmail").InnerText.Equals("1"))
                        chkMeetandGreet.Checked = true;
                    else
                        chkMeetandGreet.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/OnSiteAVSupportinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/OnSiteAVSupportinEmail").InnerText.Equals("1"))
                        chkOnSiteAVSupport.Checked = true;
                    else
                        chkOnSiteAVSupport.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/ConciergeMonitoringinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/ConciergeMonitoringinEmail").InnerText.Equals("1"))
                        chkConciergeMonitoring.Checked = true;
                    else
                        chkConciergeMonitoring.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/DedicatedVNOCOperatorinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/DedicatedVNOCOperatorinEmail").InnerText.Equals("1"))
                        chkDedicatedVNOCOperator.Checked = true;
                    else
                        chkDedicatedVNOCOperator.Checked = false;
                }


                //FB 2632 Ends
                //FB 2631
                lstEnableRoomAdminDetails.ClearSelection();
                if (lstEnableRoomAdminDetails.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomAdminDetails").InnerText) != null)
                    lstEnableRoomAdminDetails.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomAdminDetails").InnerText).Selected = true;

                //FB 2595 Start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/SecureSwitch") != null)
                    if (drpSecureSwitch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecureSwitch").InnerText) != null)
                        drpSecureSwitch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecureSwitch").InnerText).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/HardwareAdminEmail") != null)
                    if (xmldoc.SelectSingleNode("//GetOrgOptions/HardwareAdminEmail").InnerText != null)
                        txtHardwareAdminEmail.Text = xmldoc.SelectSingleNode("//GetOrgOptions/HardwareAdminEmail").InnerText.Trim();

                drpNwtSwtiching.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/NetworkSwitching") != null)
                    if (drpNwtSwtiching.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkSwitching").InnerText) != null)
                        drpNwtSwtiching.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkSwitching").InnerText).Selected = true;
                
                drpNwtCallLaunch.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/NetworkCallLaunch") != null)
                    if (drpNwtCallLaunch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkCallLaunch").InnerText) != null)
                        drpNwtCallLaunch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkCallLaunch").InnerText).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/SecureLaunchBuffer") != null)
                    txtSecureLaunch.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SecureLaunchBuffer").InnerText.Trim();

                //FB 2993 starts
                if (xmldoc.SelectSingleNode("//GetOrgOptions/ResponseTimeout") != null)
                    txtResponseTimeout.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ResponseTimeout").InnerText.Trim();
                //FB 2993 End


                if (drpSecureSwitch.SelectedValue == "0")
                {
                    tdsecureadminaddress.Attributes.Add("style", "visibility: hidden");
                    txtHardwareAdminEmail.Attributes.Add("style", "visibility: hidden");
                    trNwtSwtiching.Attributes.Add("style", "visibility: hidden");
                    trNwtCallBuffer.Attributes.Add("style", "visibility: hidden");
                }
                else
                {
                    tdsecureadminaddress.Attributes.Add("style", "visibility:visible");
                    txtHardwareAdminEmail.Attributes.Add("style", "visibility:visible");
                    trNwtSwtiching.Attributes.Add("style", "visibility: visible");
                    trNwtCallBuffer.Attributes.Add("style", "visibility: visible");
                }

                //FB 2595 End
                // FB 2641 start
                drpEnableLineRate.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableLinerate") != null)
                    if (drpEnableLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableLinerate").InnerText) != null)
                        drpEnableLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableLinerate").InnerText).Selected = true;

                drpEnableStartMode.ClearSelection();
                if(xmldoc.SelectSingleNode("//GetOrgOptions/EnableStartMode")!=null)
                    if (drpEnableStartMode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableStartMode").InnerText) != null)
                        drpEnableStartMode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableStartMode").InnerText).Selected = true;

                // FB 2641 End
                //FB 2670 START
                if (node.SelectSingleNode("//GetOrgOptions/EnableOnsiteAV") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableOnsiteAV").InnerText.Equals("1"))
                        chkOnsiteAV.Checked = true;
                    else
                        chkOnsiteAV.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/EnableMeetandGreet") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableMeetandGreet").InnerText.Equals("1"))
                        chkMeetandGret.Checked = true;
                    else
                        chkMeetandGret.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/EnableConciergeMonitoring") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableConciergeMonitoring").InnerText.Equals("1"))
                        chkConciergeMonitor.Checked = true;
                    else
                        chkConciergeMonitor.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/EnableDedicatedVNOC") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableDedicatedVNOC").InnerText.Equals("1"))
                        chkDedicatedVNOC.Checked = true;
                    else
                        chkDedicatedVNOC.Checked = false;
                }
                //FB 2670 END
                //FB 2817
                lstSigRoomConf.ClearSelection();
                if (lstSigRoomConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSingleRoomConfEmails").InnerText) != null)
                    lstSigRoomConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSingleRoomConfEmails").InnerText).Selected = true;

                //ZD 100151
                DrpDwnEnableCA.ClearSelection();
                if (DrpDwnEnableCA.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ShowCusAttInCalendar").InnerText) != null)
                    DrpDwnEnableCA.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ShowCusAttInCalendar").InnerText).Selected = true;
                Session.Add("ShowCusAttInCalendar", xmldoc.SelectSingleNode("//GetOrgOptions/ShowCusAttInCalendar").InnerText);

                //ZD 100263 Starts
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableFileWhiteList") != null)
                {
                    if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableFileWhiteList").InnerText.Trim() == "1")
                        chkWhiteList.Checked = true;
                    else
                        chkWhiteList.Checked = false;
                }

                if (xmldoc.SelectSingleNode("//GetOrgOptions/FileWhiteList") != null)
                {
                    ListItem item = null;
                    hdnFileWhiteList.Value = xmldoc.SelectSingleNode("//GetOrgOptions/FileWhiteList").InnerText.Trim();
                    string[] FileList = xmldoc.SelectSingleNode("//GetOrgOptions/FileWhiteList").InnerText.Trim().Split(';');
                    for (int k = 0; k < FileList.Length; k++)
                    {
                        if (FileList[k].Trim() != "")
                        {
                            item = new ListItem();
                            item.Text = FileList[k];
                            item.Attributes.Add("title", FileList[k]);
                            lstWhiteList.Items.Add(item);
                        }
                    }
                }

                if (chkWhiteList.Checked)
                {
                    txtWhiteList.Attributes.Add("style", "display:");
                    btnWhiteList.Attributes.Add("style", "display:");
                    trWhiteList.Attributes.Add("style", "display:");
                }
                else
                {
                    txtWhiteList.Attributes.Add("style", "display:none");
                    btnWhiteList.Attributes.Add("style", "display:none");
                    trWhiteList.Attributes.Add("style", "display:none");
                }
                //ZD 100263 End

            }
            catch (Exception ex)
            {
                log.Trace("BindData: " + ex.Message + " : " + ex.StackTrace);
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            InitializeUIComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //            this.Load += new System.EventHandler(this.Page_Load);

        }

        private void InitializeUIComponent()
        {

        }
        #endregion

        protected void SetOrgOptions()
        {
            try
            {
                String inXML = "";
                inXML += "<SetOrgOptions>";
                inXML += obj.OrgXMLElement(); //Organization Module
                inXML += "  <ContactDetails>";
                inXML += "      <Name>" + ContactName.Text + "</Name>";
                inXML += "      <Email>" + ContactEmail.Text + "</Email>";
                inXML += "      <Phone>" + ContactPhone.Text + "</Phone>";
                inXML += "      <AdditionInfo>" + ContactAdditionInfo.Text + "</AdditionInfo>";
                inXML += "  </ContactDetails>";
                //inXML += "  <SystemTimeZoneID>" + PreferTimezone.SelectedValue + "</SystemTimeZoneID>";
                inXML += "  <TimezoneSystemID>" + TimezoneSystems.SelectedValue + "</TimezoneSystemID>";
                inXML += "  <AutoAcceptModifiedConference>" + AutoAcpModConf.SelectedValue + "</AutoAcceptModifiedConference>";
                inXML += "  <EnableRecurringConference>" + RecurEnabled.SelectedValue + "</EnableRecurringConference>";
                inXML += "  <EnableDynamicInvite>" + DynamicInviteEnabled.SelectedValue + "</EnableDynamicInvite>";
                inXML += "  <EnableP2PConference>" + p2pConfEnabled.SelectedValue + "</EnableP2PConference>";
                inXML += "  <iCalReqEmailId>" + txtiCalEmailId.Text.Trim() + "</iCalReqEmailId>";//FB 1786
                inXML += "  <showBridgeExt>" + drpBrdgeExt.SelectedValue + "</showBridgeExt>";//FB 2610
                inXML += "  <EnableRealtimeDisplay>" + RealtimeType.SelectedValue + "</EnableRealtimeDisplay>";
                inXML += "  <EnableDialout>" + DialoutEnabled.SelectedValue + "</EnableDialout>";
                inXML += "  <DefaultConferencesAsPublic>" + DefaultPublic.SelectedValue + "</DefaultConferencesAsPublic>";
                inXML += "  <DefaultConferenceType>" + lstDefaultConferenceType.SelectedValue + "</DefaultConferenceType>";
                inXML += "  <RFIDTagValue>" + lstRFIDValue.SelectedValue + "</RFIDTagValue>";//FB 2724
                inXML += "  <iControlTimeout>" + txtiControlTimeout.Text.Trim() + "</iControlTimeout>";//FB 2724
                inXML += "  <EnableRoomConference>" + lstEnableRoomConference.SelectedValue + "</EnableRoomConference>";
                inXML += "  <EnableAudioVideoConference>" + lstEnableAudioVideoConference.SelectedValue + "</EnableAudioVideoConference>";
                inXML += "  <EnableAudioOnlyConference>" + lstEnableAudioOnlyConference.SelectedValue + "</EnableAudioOnlyConference>";
                inXML += "  <EnableNumericID>" + lstEnableNumericID.SelectedValue + "</EnableNumericID>";//FB 2870
                inXML += "  <DefaultCalendarToOfficeHours>" + lstDefaultOfficeHours.SelectedValue + "</DefaultCalendarToOfficeHours>";
                inXML += "  <RoomTreeExpandLevel>" + lstRoomTreeLevel.SelectedValue + "</RoomTreeExpandLevel>";
                //Code added for FB 1470
                inXML += "  <EnableEntity></EnableEntity>";
                inXML += "  <EnableBufferZone>" + EnableBufferZone.SelectedValue + "</EnableBufferZone>";
                inXML += "  <SetupTime>" + txtSetupTime.Text.Trim() + "</SetupTime>"; //FB 2398
                inXML += "  <TearDownTime>" + txtTearDownTime.Text.Trim() + "</TearDownTime>";
                //FB-1642 Audio Add On Starts..
                inXML += "<ConferenceCode>" + DrpConfcode.SelectedValue + "</ConferenceCode>";
                inXML += "<LeaderPin>" + DrpLedpin.SelectedValue + "</LeaderPin>";
                inXML += "<AdvAvParams>" + Drpavprm.SelectedValue + "</AdvAvParams>";
                inXML += "<AudioParams >" + DrpAudprm.SelectedValue + "</AudioParams >";
                //FB-1642 Audio Add On Ends..
                inXML += "<sendIcal >" + DrpListIcal.SelectedValue + "</sendIcal>";//FB 1782
                inXML += "<sendApprovalIcal>" + DrpAppIcal.SelectedValue + "</sendApprovalIcal>";//FB 1782
                inXML += "<isVIP >" + DrpVIP.SelectedValue + "</isVIP>";
                inXML += "<isUniquePassword>" + DrpUniquePassword.SelectedValue + "</isUniquePassword>";
                inXML += "<isAssignedMCU>" + DrpDwnListAssignedMCU.SelectedValue + "</isAssignedMCU>";// FB 1901
                inXML += "<ReminderMask>" + buildRemainderMask() + "</ReminderMask>";// FB 1926
                inXML += "<isMultiLingual>" + DrpDwnListMultiLingual.SelectedValue + "</isMultiLingual>";//FB 1830 - Translation
                inXML += "<PIMNotifications>" + DrpPluginConfirm.SelectedValue + "</PIMNotifications>";// FB 2141
                inXML += "<ExternalAttachments>" + DrpDwnAttachmnts.SelectedValue + "</ExternalAttachments>";// FB 2154
                inXML += "<filterTelepresence>" + DrpDwnFilterTelepresence.SelectedValue + "</filterTelepresence>";// FB 2170
                inXML += "<EnableRoomServiceType>" + lstEnableRoomServiceType.SelectedValue + "</EnableRoomServiceType>";//FB 2219
                inXML += "<isSpecialRecur>" + DrpDwnListSpRecur.SelectedValue + "</isSpecialRecur>";// FB 2052                                
				inXML += "<isDeptUser>" + DrpDwnListDeptUser.SelectedValue + "</isDeptUser>";// FB 2269                                
				inXML += "<EnablePIMServiceType>" + lstEnablePIMServiceType.SelectedValue + "</EnablePIMServiceType>";//FB 2038
                inXML += "<EnableImmConf>" + lstEnableImmediateConference.SelectedValue + "</EnableImmConf>";//FB 2036                
                inXML += "<EnablePasswordRule>" + DrpDwnPasswordRule.SelectedValue + "</EnablePasswordRule>";//FB 2339                               
                inXML += "<EnableAudioBridges>" + lstEnableAudioBridges.SelectedValue + "</EnableAudioBridges>";//FB 2023
                inXML += "<DedicatedVideo>" + DrpDwnDedicatedVideo.SelectedValue + "</DedicatedVideo>";// FB 2334	
                //FB 2359 Start
                inXML += "<EnableConferencePassword>" + lstEnableConfPassword.SelectedValue + "</EnableConferencePassword>";
                inXML += "<EnablePublicConference>" + lstEnablePublicConf.SelectedValue + "</EnablePublicConference>";
                inXML += "<EnableRoomParam>" + lstRoomprm.SelectedValue + "</EnableRoomParam>";
                //FB 2359 End
                inXML += "<EnableVMR>" + lstEnableVMR.SelectedValue + "</EnableVMR>";//VMR 
                inXML += "<EnableEPDetails>" + lstEPinMail.SelectedValue + "</EnableEPDetails>";//FB 2401
				//FB 2136 start
                inXML += "<MCUAlert>" + lstMcuAlert.SelectedValue + "</MCUAlert>";//FB 2472
                inXML += "<EnableSecurityBadge>" + drpenablesecuritybadge.SelectedValue + "</EnableSecurityBadge>";
                inXML += "<SecurityBadgeType>" + drpsecuritybadgetype.SelectedValue + "</SecurityBadgeType>";
                inXML += "<SecurityDeskEmailId>" + txtsecdeskemailid.Text + "</SecurityDeskEmailId>";
                //FB 2136 end
                //FB 2348 Start
                inXML += "<EnableSurvey>" + drpenablesurvey.SelectedValue + "</EnableSurvey>";
                inXML += "<SurveyOption>" + drpsurveyoption.SelectedValue + "</SurveyOption>";
                if (drpsurveyoption.SelectedValue == "2" && drpenablesurvey.SelectedValue == "1")
                {
                    inXML += "<SurveyURL>" + txtSurWebsiteURL.Text.Trim() + "</SurveyURL>";
                    inXML += "<TimeDuration>" + txtTimeDur.Text.Trim() + "</TimeDuration>";
                }
                else
                {
                    inXML += "<SurveyURL>" + hdnSurURL.Value + "</SurveyURL>";
                    inXML += "<TimeDuration>" + hdnTimeDur.Value + "</TimeDuration>";
                }
                //FB 2348 end

                //FB 2335 start
                inXML += "<defPolycomRMXLO>" + Poly2RMX.Value + "</defPolycomRMXLO>";
                inXML += "<defPolycomMGCLO>" + Poly2MGC.Value + "</defPolycomMGCLO>";
                inXML += "<defCiscoTPLO>" + CTMS2Poly.Value + "</defCiscoTPLO>";
                inXML += "<defCTMSLO>" + CTMS2Cisco.Value + "</defCTMSLO>";
                //FB 2335 end
                inXML += "<EnableAcceptDecline>" + lstAcceptDecline.SelectedValue + "</EnableAcceptDecline>";//FB 2419
                inXML += "<DefaultLineRate>" + lstLineRate.SelectedValue + "</DefaultLineRate>";//FB 2429
                inXML += "<McuSetupTime>" + txtMCUSetupTime.Text + "</McuSetupTime>";//FB 2440
                inXML += "<MCUTeardonwnTime>" + txtMCUTearDownTime.Text + "</MCUTeardonwnTime>";//FB 2440
				inXML += "<MCUBufferPriority>" + DrpForceMCUBuffer.SelectedValue.Trim() + "</MCUBufferPriority>";//FB 2440
                inXML += "<MCUSetupDisplay>" + MCUSetupDisplay.SelectedValue.Trim() + "</MCUSetupDisplay>";//FB 2998
                inXML += "<MCUTearDisplay>" + MCUTearDisplay.SelectedValue.Trim() + "</MCUTearDisplay>";//FB 2998

                //FB 2426 Start
                inXML += "<OnflyTier>";
                inXML += "<DefTopTier>" + lstTopTier.SelectedItem.Text.Trim() + "</DefTopTier>";
                inXML += "<DefMiddleTier>" + lstMiddleTier.SelectedItem.Text.Trim() + "</DefMiddleTier>";
                inXML += "<OnflyTopTierID>" + lstTopTier.SelectedValue + "</OnflyTopTierID>";
                inXML += "<OnflyMiddleTierID>" + lstMiddleTier.SelectedValue + "</OnflyMiddleTierID>";
                inXML += "</OnflyTier>";
                //FB 2426 End
                inXML += "<EnableSmartP2P>" + lstEnableSmartP2P.SelectedValue + "</EnableSmartP2P>";//FB 2430
                //FB 2469 Starts
                inXML += "<EnableConfTZinLoc>" + lstEnableConfTZinLoc.SelectedValue + "</EnableConfTZinLoc>";
                inXML += "<SendConfirmationEmail>" + lstSendConfirmationEmail.SelectedValue + "</SendConfirmationEmail>";
                //FB 2469 End
                inXML += "<DefaultConfDuration>" + txtDefaultConfDuration.Text.Trim() + "</DefaultConfDuration>";//FB 2501
                //FB 2571 Start
                inXML += "<EnableFECC>" + lstenableFECC.SelectedValue + "</EnableFECC>";
                inXML += "<DefaultFECC>" + lstdefaultFECC.SelectedValue + "</DefaultFECC>";
                //FB 2571 End

                //FB 2598 Starts
                inXML += "<EnableCallmonitor>" + DrpDwnEnableCallmonitor.SelectedValue + "</EnableCallmonitor>";// DD Feature                                
                inXML += "<EnableEM7>" + DrpDwnEnableEM7.SelectedValue + "</EnableEM7>";
                inXML += "<EnableCDR>" + DrpDwnEnableCDR.SelectedValue + "</EnableCDR>";                                
                //FB 2598 Ends
                inXML += "<EnableDialPlan>" + DrpEnableE164DialPlan.SelectedValue + "</EnableDialPlan>"; // FB 2636                                
                //FB 2550 Starts
                int VMRtype=0;
                Int32.TryParse(lstEnableVMR.SelectedValue.ToString(),out VMRtype);
                if (!((DynamicInviteEnabled.SelectedValue.ToString().Equals("1")) && VMRtype > 0))
                    txtMaxVMRParty.Text = "0";
            
                inXML += "<MaxPublicVMRParty>" + txtMaxVMRParty.Text.Trim() + "</MaxPublicVMRParty>";
                //FB 2550 Ends
                inXML += "<MeetandGreetBuffer>" + txtMeetandGreetBuffer.Text.Trim() + "</MeetandGreetBuffer>";//FB 2609

                //FB 2632 Starts
                inXML += "<EnableCongSupport>" + drpCngSupport.SelectedValue + "</EnableCongSupport>";
                
                if (chkMeetandGreet.Checked)
                    inXML += "<MeetandGreetinEmail>1</MeetandGreetinEmail>";
                else
                    inXML += "<MeetandGreetinEmail>0</MeetandGreetinEmail>";
                
                if (chkOnSiteAVSupport.Checked)
                    inXML += "<OnSiteAVSupportinEmail>1</OnSiteAVSupportinEmail>";
                else
                    inXML += "<OnSiteAVSupportinEmail>0</OnSiteAVSupportinEmail>";

                if (chkConciergeMonitoring.Checked)
                    inXML += "<ConciergeMonitoringinEmail>1</ConciergeMonitoringinEmail>";
                else
                    inXML += "<ConciergeMonitoringinEmail>0</ConciergeMonitoringinEmail>";

                if (chkDedicatedVNOCOperator.Checked)
                    inXML += "<DedicatedVNOCOperatorinEmail>1</DedicatedVNOCOperatorinEmail>";
                else
                    inXML += "<DedicatedVNOCOperatorinEmail>0</DedicatedVNOCOperatorinEmail>";
                //FB 2632 Ends
				inXML += "<EnableRoomAdminDetails>" + lstEnableRoomAdminDetails.SelectedValue + "</EnableRoomAdminDetails>";//FB 2631

                //FB 2637 Starts
                string tierIds = "";
                hdnTierIDs.Value = "";
                foreach (ListItem li in lstTier1.Items)
                {
                    if (li.Selected.Equals(true))
                        hdnTierIDs.Value += li.Value + "|";
                }
               
                if (hdnTierIDs.Value.Trim().Length > 0)
                    tierIds = hdnTierIDs.Value.Substring(0, hdnTierIDs.Value.Length - 1);

                inXML += "<AlertforTier1>" + tierIds + "</AlertforTier1>"; //FB 2637
                inXML += "<EnableZulu>" + DropDownZuLu.SelectedValue + "</EnableZulu>";//FB 2588
                //FB 2637 Ends
                //FB 2595 Start
                inXML += "<SecureSwitch>" + drpSecureSwitch.SelectedValue + "</SecureSwitch>";
                inXML += "<HardwareAdminEmail>" + txtHardwareAdminEmail.Text.Trim() + "</HardwareAdminEmail>";
                inXML += "<NetworkSwitching>" + drpNwtSwtiching.SelectedValue + "</NetworkSwitching>";
                inXML += "<NetworkCallLaunch>" + drpNwtCallLaunch.SelectedValue + "</NetworkCallLaunch>";
                inXML += "<SecureLaunchBuffer>" + txtSecureLaunch.Text.Trim() + "</SecureLaunchBuffer>";
                //FB 2595 End
                inXML += "<EnableLinerate>" + drpEnableLineRate.SelectedValue + "</EnableLinerate>";//FB 2641
                inXML += "<EnableStartMode>" + drpEnableStartMode.SelectedValue + "</EnableStartMode>";//FB 2641
                //FB 2670 START
                if (chkOnsiteAV.Checked)
                    inXML += "<EnableOnsiteAV>1</EnableOnsiteAV>";
                else
                    inXML += "<EnableOnsiteAV>0</EnableOnsiteAV>";

                if (chkMeetandGret.Checked)
                    inXML += "<EnableMeetandGreet>1</EnableMeetandGreet>";
                else
                    inXML += "<EnableMeetandGreet>0</EnableMeetandGreet>";

                if (chkConciergeMonitor.Checked)
                    inXML += "<EnableConciergeMonitoring>1</EnableConciergeMonitoring>";
                else
                    inXML += "<EnableConciergeMonitoring>0</EnableConciergeMonitoring>";

                if (chkDedicatedVNOC.Checked)
                    inXML += "<EnableDedicatedVNOC>1</EnableDedicatedVNOC>";
                else
                    inXML += "<EnableDedicatedVNOC>0</EnableDedicatedVNOC>";
                //FB 2670 END
                inXML += "<EnableSingleRoomConfEmails>" + lstSigRoomConf.SelectedValue + "</EnableSingleRoomConfEmails>";//FB 2817

                inXML += "<EnableProfileSelection>" + lstEnableProfileSelection.SelectedValue + "</EnableProfileSelection>";//FB 2839                

                inXML += "<ResponseTimeout>" + txtResponseTimeout.Text.Trim() + "</ResponseTimeout>";//FB 2993
				inXML += "<ShowCusAttInCalendar>" + DrpDwnEnableCA.SelectedValue + "</ShowCusAttInCalendar>";//ZD 100151

                //ZD 100263 Starts
                if (chkWhiteList.Checked)
                {
                    inXML += "<EnableFileWhiteList>1</EnableFileWhiteList>";
                    inXML += "<FileWhiteList>" + hdnFileWhiteList.Value + "</FileWhiteList>";
                }
                else
                {
                    inXML += "<EnableFileWhiteList>0</EnableFileWhiteList>";
                    inXML += "<FileWhiteList></FileWhiteList>";
                }
                //ZD 100263 End

                inXML += "  <SystemAvailableTime>";
                string tmpStr = "0";
                if (Open24.Checked.Equals(true))
                    tmpStr = "1";
                inXML += "      <IsOpen24Hours>" + tmpStr + "</IsOpen24Hours>";
                if (tmpStr.Equals("1"))
                {
                    inXML += "      <StartTime>12:00 AM</StartTime>";
                    inXML += "      <EndTime>11:59 PM</EndTime>";
                }
                else
                {
                    inXML += "      <StartTime>" + DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemStartTime.Text)).ToShortTimeString() + "</StartTime>"; //FB 2588
                    inXML += "      <EndTime>" + DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemEndTime.Text)).ToShortTimeString() + "</EndTime>"; //FB 2588
                }
                inXML += "      <DaysClosed>";
                tmpStr = "";
                foreach (ListItem li in DayClosed.Items)
                    if (li.Selected)
                        tmpStr += li.Value + ",";
                if (tmpStr.Length > 0)
                    tmpStr = tmpStr.Substring(0, tmpStr.Length - 1);
                inXML += tmpStr;
                inXML += "</DaysClosed>";
                inXML += "  </SystemAvailableTime>";
                inXML += "</SetOrgOptions>";
                log.Trace("SetOrgOptions Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetOrgOptions", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetOrgOptions Outxml: " + outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    if (Session["organizationID"] != null)
                    {
                        if (Session["organizationID"].ToString() != "")
                            Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                    }

                    obj.SetOrgSession(orgId);
                    Session["CalendarMonthly"] = null;//FB 1850
                    Session.Add("AlertforTier1", tierIds); //FB 2637
                    if (hdnZuluChange != null)
                        isZuluChanged = hdnZuluChange.Value;
                    if (isZuluChanged == "1")
                        Response.Redirect("mainadministrator.aspx?m=2");
                    else
                        Response.Redirect("mainadministrator.aspx?m=1");
                }
            }
            catch (Exception ex)
            {
                log.Trace("SetOrgOptions: " + ex.Message + " : " + ex.StackTrace);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if(Validate())//FB 2440            
                SetOrgOptions();
        }

        #region Set reminder mask from selection
        /// <summary>
        /// Set reminder mask from selection (FB 1926)
        /// </summary>
        /// <returns>string remindermsk</returns>
        private string buildRemainderMask()
        {
            Int32 remindermsk = 0;
            try
            {

                if (WklyChk.Checked)
                    remindermsk += wkly;
                if (DlyChk.Checked)
                    remindermsk += dly;
                if (HourlyChk.Checked)
                    remindermsk += hourly;
                if (MinChk.Checked)
                    remindermsk += minutely;

            }
            catch (Exception ex)
            {

                log.Trace("SetOrgOptions: " + ex.Message + " : " + ex.StackTrace);
            }

            return remindermsk.ToString();
        }
        #endregion

        #region Set reminder from mask
        /// <summary>
        /// To set Reminder from mask ( FB 1926)
        /// </summary>
        /// <returns></returns>
        private void SetRemainderfromMask(String mask)
        {
            Int32 remindermsk = 0;
            try
            {
                Int32.TryParse(mask,out remindermsk);

                if(remindermsk > 0)
                {
                    if (Convert.ToBoolean(remindermsk & wkly))
                        WklyChk.Checked = true;
                    if (Convert.ToBoolean(remindermsk & dly))
                        DlyChk.Checked = true;
                    if (Convert.ToBoolean(remindermsk & hourly))
                        HourlyChk.Checked = true;
                    if (Convert.ToBoolean(remindermsk & minutely))
                        MinChk.Checked = true;

                }

            }
            catch (Exception ex)
            {

                log.Trace("SetOrgOptions: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region Auto Screen Layout Mapping - Load Id and Image
        // FB 2335 Starts
        protected void fnSetLayoutImage(string xPoly2MGC, string xPoly2RMX, string xCTMS2Cisco, string xCTMS2Poly)
        {
            String fullPath = "";
            if (xPoly2MGC == "00") //FB 2384
                xPoly2MGC = "01";
            fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/"+ language +"/") + 3);
            Poly2MGC.Value = xPoly2MGC;
            imgLayoutMapping1.ImageUrl =  fullPath + "/image/displaylayout/" + xPoly2MGC + ".gif";
            if (xPoly2RMX == "00") //FB 2384
                xPoly2RMX = "01";
            Poly2RMX.Value = xPoly2RMX;
            imgLayoutMapping2.ImageUrl = fullPath + "/image/displaylayout/" + xPoly2RMX + ".gif";
            if (xCTMS2Cisco == "00") //FB 2384
                xCTMS2Cisco = "01";
            CTMS2Cisco.Value = xCTMS2Cisco;
            imgLayoutMapping3.ImageUrl = fullPath + "/image/displaylayout/" + xCTMS2Cisco + ".gif";
            if (xCTMS2Poly == "00") //FB 2384
                xCTMS2Poly = "01";
            CTMS2Poly.Value = xCTMS2Poly;
            imgLayoutMapping4.ImageUrl = fullPath + "/image/displaylayout/" + xCTMS2Poly + ".gif";

        }
        // FB 2335 Ends
        #endregion

        #region Validate times
        /// <summary>
        /// FB 2440
        /// Check if MCU setup and tear donw are with room setup and tear down
        /// </summary>
        /// <returns></returns>
        private Boolean Validate()
        {
            bool bRet = true;
            int mcuSetup = 0, roomSetup = 0,mcuTear = 0,roomTear = 0;
            try
            {
                //FB 2634
                if (EnableBufferZone.SelectedValue.Trim() == "0")
                {
                    txtMCUSetupTime.Text = "0";
                    txtSetupTime.Text = "0";
                    txtMCUTearDownTime.Text = "0";
                    txtTearDownTime.Text = "0";
                }
                else if (EnableBufferZone.SelectedValue.Trim() == "1" && DrpForceMCUBuffer.SelectedValue.Trim() == "0")
                {
                    Int32.TryParse(txtMCUSetupTime.Text.Trim(), out mcuSetup);
                    Int32.TryParse(txtSetupTime.Text.Trim(), out roomSetup);
                    Int32.TryParse(txtMCUTearDownTime.Text.Trim(), out mcuTear);
                    Int32.TryParse(txtTearDownTime.Text.Trim(), out roomTear);

                    if (mcuSetup != 0 || mcuTear != 0)
                    {
                        if (mcuSetup > roomSetup)
                        {
                            errLabel.Text = obj.GetTranslatedText("Setup time cannot be less than the MCU pre start time.");
                            errLabel.Visible = true;
                            bRet = false;
                        }

                        if (mcuTear < 0)
                        {
                            if ((roomTear + mcuTear) < 0)
                            {

                                errLabel.Text = obj.GetTranslatedText("Teardown time cannot be less than the MCU pre end time.");
                                errLabel.Visible = true;
                                bRet = false;
                            }
                        }

                    }
                }
                

            }
            catch (Exception ex)
            {
                log.Trace("Validate: " + ex.Message + " : " + ex.StackTrace);
                
            }

            return bRet;
        }
#endregion

        //FB 2426 Start
        #region UpdateMiddleTiers
        protected void UpdateMiddleTiers(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<GetLocations2>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <Tier1ID>" + lstTopTier.SelectedValue + "</Tier1ID>";
                inXML += "</GetLocations2>";
                String outXML = obj.CallMyVRMServer("GetLocations2", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetLocations2 outxml: " + outXML);

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations2/Location");
                    obj.LoadList(lstMiddleTier, nodes, "ID", "Name");
                }
                if(IsPostBack)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "addorremove1", "<script>var img_FLY_obj = document.getElementById('img_FLY'); ExpandCollapse(img_FLY_obj,'trFLY', false);</script>", false);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateMiddleTiers: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //FB 2426 End

        //FB 2486
        #region SetOrgTextMessage
        /// <summary>
        /// SetMessage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnOrgTxtMsgSubmit(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inxml = new StringBuilder();

                inxml.Append("<SetOrgTextMsg><UserID>" + Session["userID"] + "</UserID>" + obj.OrgXMLElement());
                #region OrgMessageList
                inxml.Append("<OrgMessage>");

                if (chkmsg1.Checked)
                    inxml.Append("1#" + drpdownconfmsg1.SelectedItem.Value + "#" + drpdownmsgduration1.SelectedItem.Text.Trim()+";");

                if (chkmsg2.Checked)
                    inxml.Append("2#" + drpdownconfmsg2.SelectedItem.Value + "#" + drpdownmsgduration2.SelectedItem.Text.Trim() + ";");

                if (chkmsg3.Checked)
                    inxml.Append("3#" + drpdownconfmsg3.SelectedItem.Value + "#" + drpdownmsgduration3.SelectedItem.Text.Trim() + ";");

                if (chkmsg4.Checked)
                    inxml.Append("4#" + drpdownconfmsg4.SelectedItem.Value + "#" + drpdownmsgduration4.SelectedItem.Text.Trim() + ";");

                if (chkmsg5.Checked)
                    inxml.Append("5#" + drpdownconfmsg5.SelectedItem.Value + "#" + drpdownmsgduration5.SelectedItem.Text.Trim() + ";");

                if (chkmsg6.Checked)
                    inxml.Append("6#" + drpdownconfmsg6.SelectedItem.Value + "#" + drpdownmsgduration6.SelectedItem.Text.Trim() + ";");

                if (chkmsg7.Checked)
                    inxml.Append("7#" + drpdownconfmsg7.SelectedItem.Value + "#" + drpdownmsgduration7.SelectedItem.Text.Trim() + ";");

                if (chkmsg8.Checked)
                    inxml.Append("8#" + drpdownconfmsg8.SelectedItem.Value + "#" + drpdownmsgduration8.SelectedItem.Text.Trim() + ";");

                if (chkmsg9.Checked)
                    inxml.Append("9#" + drpdownconfmsg9.SelectedItem.Value + "#" + drpdownmsgduration9.SelectedItem.Text.Trim() + ";");

                inxml.Append("</OrgMessage>");
                #endregion
                inxml.Append("</SetOrgTextMsg>");

                String outXML = obj.CallMyVRMServer("SetOrgTextMsg", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("fnOrgTxtMsgSubmit" + ex.Message);
            }
        }
        #endregion

        #region GetOrgMessages
        public void GetOrgMessages(string TxtMsgList)
        {
            try
            {
                string controlID = "0", duration = "0", textMessage = "0";
                String[] TxtMsgID;

                String[] TxtMsg = TxtMsgList.Split(';');
                if (TxtMsg.Length > 0)
                {
                    for (int i = 0; i < TxtMsg.Length -1; i++)
                    {
                        controlID = "0";
                        duration = "0";
                        textMessage = "0";

                        TxtMsgID = TxtMsg[i].Split('#');

                        controlID = TxtMsgID[0];
                        textMessage = TxtMsgID[1];
                        duration = TxtMsgID[2];
                       
                        if (controlID == "1")
                        {
                            chkmsg1.Checked = true;

                            drpdownconfmsg1.ClearSelection();
                            if (drpdownconfmsg1.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg1.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration1.ClearSelection();
                            if (drpdownmsgduration1.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration1.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration1.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "2")
                        {
                            chkmsg2.Checked = true;

                            drpdownconfmsg2.ClearSelection();
                            if (drpdownconfmsg2.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg2.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration2.ClearSelection();
                            if (drpdownmsgduration2.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration2.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration2.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "3")
                        {
                            chkmsg3.Checked = true;

                            drpdownconfmsg3.ClearSelection();
                            if (drpdownconfmsg3.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg3.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration3.ClearSelection();
                            if (drpdownmsgduration3.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration3.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration3.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "4")
                        {
                            chkmsg4.Checked = true;

                            drpdownconfmsg4.ClearSelection();
                            if (drpdownconfmsg4.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg4.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration4.ClearSelection();
                            if (drpdownmsgduration4.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration4.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration4.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "5")
                        {
                            chkmsg5.Checked = true;

                            drpdownconfmsg5.ClearSelection();
                            if (drpdownconfmsg5.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg5.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration5.ClearSelection();
                            if (drpdownmsgduration5.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration5.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration5.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "6")
                        {
                            chkmsg6.Checked = true;

                            drpdownconfmsg6.ClearSelection();
                            if (drpdownconfmsg6.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg6.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration6.ClearSelection();
                            if (drpdownmsgduration6.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration6.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration6.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "7")
                        {
                            chkmsg7.Checked = true;

                            drpdownconfmsg7.ClearSelection();
                            if (drpdownconfmsg7.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg7.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration7.ClearSelection();
                            if (drpdownmsgduration7.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration7.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration7.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "8")
                        {
                            chkmsg8.Checked = true;

                            drpdownconfmsg8.ClearSelection();
                            if (drpdownconfmsg8.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg8.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration8.ClearSelection();
                            if (drpdownmsgduration8.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration8.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration8.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "9")
                        {
                            chkmsg9.Checked = true;

                            drpdownconfmsg9.ClearSelection();
                            if (drpdownconfmsg9.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg9.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration9.ClearSelection();
                            if (drpdownmsgduration9.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration9.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration9.Attributes["PreValue"] = duration;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetOrgMessages" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
                
            }
        }

        #endregion
        //FB 2486 Ends
    }
}