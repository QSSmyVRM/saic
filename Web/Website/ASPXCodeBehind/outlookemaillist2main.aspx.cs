/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_outlookemaillist2main : System.Web.UI.Page
{
    myVRMNet.NETFunctions obj;
    #region Private and Public variables

    protected string titlealign = "";
    protected string alphabet = "";
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("outlookemaillist2main.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            if (Request.QueryString["outlookEmails"] != null)
                Session["outLookUpEmail"] = Request.QueryString["outlookEmails"];
            else
                Session["outlookEmails"] = Request.Form["outlookEmails"];

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
