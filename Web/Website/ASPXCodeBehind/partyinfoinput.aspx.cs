/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_partyinfoinput : System.Web.UI.Page
{
    # region private Members
    ns_Logger.Logger log;
	//FB 2450	
    //public msxml4_Net.DOMDocument40Class XmlDoc = null;    
    //public msxml4_Net.IXMLDOMNodeList nodes = null;
    //msxml4_Net.IXMLDOMNode node = null;
    //msxml4_Net.IXMLDOMNode subnode = null;

    public XmlDocument XmlDoc = null;
    public XmlNodeList nodes = null;
    XmlNode node = null;

    public String addressTypeIDList = null;
    public String addressTypeNameList = null;
    private String errorMessage = "";
    private String sessionXML = "";
    myVRMNet.NETFunctions obj;//FB 1881
    # endregion

    # region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("partyinfoinput.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions(); //FB 1881

            if (!IsPostBack)
            {
                BuildXML();
                PartyInfoVal();
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
            Response.Write(obj.ShowSystemMessage());//ZD 100263
        }
    }
    # endregion

    #region BuildXML
    /// <summary>
    /// Build InputXML -Get the Session XML or build XML,from mangetemplate2.asp to bind values 
    /// </summary>
    private void BuildXML()
    {
        try
        {
            if (Session["transcodingXML"] != null)
            {
                if (Session["transcodingXML"].ToString() != "")
                    sessionXML = Session["transcodingXML"].ToString();

            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
            Response.Write(obj.ShowSystemMessage());//ZD 100263
        }
    }
       #endregion

    #region Party InfoInput Values

    private void PartyInfoVal()
    {
		//FB 2450
        //XmlDoc = new msxml4_Net.DOMDocument40Class();
        XmlDoc = new XmlDocument();

        try
        {
            if (sessionXML != "")
            {
				//FB 2450
                //XmlDoc.async = false;
                XmlDoc.LoadXml(Convert.ToString(sessionXML));

                if (XmlDoc.InnerText == "" || XmlDoc.InnerText.IndexOf("<error>") >= 0) //if (XmlDoc.parseError.errorCode != 0)
                {
                    if (Convert.ToString(Session["who_use"]) == "VRM")
                    {
                        //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";
                        //errorMessage = errorMessage + Convert.ToString(Session["outXML"]);
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + Convert.ToString(Session["outXML"]));
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        //CatchErrors(errorMessage);
                        Response.Redirect("underconstruction.aspx");
                    }
                    else
                    {
                        //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                        ////Response.Write("<br><br><p align='left'><font size=2><b>" + Convert.ToString(Transfer[Session["outXML"]]) + "<b></font></p>");
                        //Response.Write("<br><br><p align='left'><font size=2><b>" + sessionXML + "<b></font></p>");
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + sessionXML);
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                    }
                    XmlDoc = null;
                    Response.End();
                }
				//FB 2450
                node = XmlDoc.SelectSingleNode("//template/confInfo");
             
                nodes = node.SelectNodes("addressType/type");
               Int32 length = nodes.Count;
                for(Int32 i = 0;i<length;i++)
                {
                    node = nodes[i];// nodes.nextNode();
                    addressTypeIDList = addressTypeIDList + "," + node.SelectSingleNode("ID").InnerText;
                    addressTypeNameList = addressTypeNameList + "," + node.SelectSingleNode("name").InnerText;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
            Response.Write(obj.ShowSystemMessage());//ZD 100263
        }
    }
    #endregion

}
