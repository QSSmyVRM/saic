/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Drawing;
using System.Collections.Generic;


public partial class en_recurNET : System.Web.UI.Page
{

    #region Protected Data Members
    
    protected System.Web.UI.WebControls.TextBox RecurDurationhr;
    protected System.Web.UI.WebControls.TextBox RecurDurationmi;
    protected System.Web.UI.WebControls.TextBox EndText;
    protected System.Web.UI.WebControls.TextBox DurText;
    protected System.Web.UI.WebControls.TextBox StartDate;
    protected System.Web.UI.WebControls.RadioButton EndType;
    protected System.Web.UI.WebControls.RadioButton REndAfter;
    protected System.Web.UI.WebControls.TextBox Occurrence;
    protected System.Web.UI.WebControls.RadioButton REndBy;
    protected System.Web.UI.WebControls.TextBox EndDate;
    protected System.Web.UI.WebControls.Button Cancel;
    protected System.Web.UI.WebControls.Button Reset;
    protected System.Web.UI.WebControls.Button RecurSubmit;
    protected System.Web.UI.WebControls.Button RecurSubmit1;
    protected System.Web.UI.HtmlControls.HtmlTableRow RangeRow;
    protected MetaBuilders.WebControls.ComboBox confStartTime;
    protected MetaBuilders.WebControls.ComboBox confEndTime;
    //FB 2634
    //protected System.Web.UI.WebControls.RequiredFieldValidator reqStartTime;
    //protected System.Web.UI.WebControls.RequiredFieldValidator reqStartDate;
    protected System.Web.UI.WebControls.RegularExpressionValidator regConfStartTime;
    protected System.Web.UI.WebControls.RegularExpressionValidator regEndTime;
    protected System.Web.UI.WebControls.RegularExpressionValidator regTime;
    protected System.Web.UI.WebControls.RadioButtonList rdlstDayColor;
    protected System.Web.UI.WebControls.Label lblNoneDayColor;

    protected System.Web.UI.WebControls.TextBox SetupDuration;
    protected System.Web.UI.WebControls.TextBox TearDownDuration;
    
    #endregion

    #region Private Data Members 

    protected Int32 CustomSelectedLimit = 100;
    protected String format = "MM/dd/yyyy";
    protected String timeZone = "0";
    protected String tformat = "hh:mm tt";
    myVRMNet.NETFunctions obj = null;
    ns_Logger.Logger log;
    protected Int32 OrgSetupTime = 0, OrgTearDownTime = 0, EnableBufferZone = 0;
    protected String setup = "0", teardown = "0"; //FB 2634

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("recurNET.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            String outXML = "", inXML = "";//FB 2052
            Int32 CustomSelectedLimit = 100;
            Int32 timeFmrt = 12; ;

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            Session["FormatDateType"] = ((Session["FormatDateType"] == null) ? "1" : Session["FormatDateType"]);
            Session["timeZoneDisplay"] = ((Session["timeZoneDisplay"] == null) ? "1" : Session["timeZoneDisplay"]);
            Application["interval"] = ((Application["interval"] == null) ? "60" : Application["interval"]);
            format = Session["FormatDateType"].ToString();
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
            timeZone = Session["timeZoneDisplay"].ToString();
            //FB 2558
            if (Session["timeFormat"].ToString().Equals("2"))
                tformat = "HHmmZ";

            //FB 2634
            if (Request.QueryString["su"] != null)
                setup = Request.QueryString["su"].ToString();

            if (Request.QueryString["tdn"] != null)
                teardown = Request.QueryString["tdn"].ToString();

            if (!IsPostBack)
            {
                obj = new myVRMNet.NETFunctions();
                log = new ns_Logger.Logger();

                //FB 2634
                SetupDuration.Text = setup;
                TearDownDuration.Text = teardown;

                String tmpInterval = "";
                if (Application["interval"] != null)
                    if (Application["interval"].ToString() != "")
                        tmpInterval = Application["interval"].ToString();
                Application["interval"] = "1";
                Application["interval"] = tmpInterval;

                confStartTime.Items.Clear();
                confEndTime.Items.Clear();
                obj.BindTimeToListBox(confStartTime, true, true);
                obj.BindTimeToListBox(confEndTime, true, true);
                if (Session["timeFormat"] != null)
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        //FB 2634
                        regConfStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regConfStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        regEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        //regTearDownStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        //regTearDownStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    }
                    else if (Session["timeFormat"].ToString().Equals("2")) //FB 2588
                    {
                        regConfStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        regConfStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                        regEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    }

                if (Session["EnableBufferZone"] != null)
                    if (Session["EnableBufferZone"].ToString() != "")
                        Int32.TryParse(Session["EnableBufferZone"].ToString(), out EnableBufferZone);

                inXML = "<login>" 
                      + "  <userID>" + Session["userID"].ToString() + "</userID>"
                      + obj.OrgXMLElement()
                      + "</login>";

                outXML = obj.CallMyVRMServer("GetHolidayType", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                lblNoneDayColor.Visible = true;
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);
                    XmlNodeList nodes = xd.SelectNodes("//HolidayTypes/HolidayType");

                    for (int i = 0; i < nodes.Count; i++)
                    {
                        if (nodes[i].SelectSingleNode("id") != null)
                            if (nodes[i].SelectSingleNode("id").InnerText.Trim() != "")
                            {
                                if (nodes[i].SelectSingleNode("HolidayDescription") != null)
                                    if (nodes[i].SelectSingleNode("HolidayDescription").InnerText.Trim() != "")
                                    {
                                        rdlstDayColor.Items.Add(nodes[i].SelectSingleNode("HolidayDescription").InnerText.Trim());

                                        rdlstDayColor.Items[i].Value = nodes[i].SelectSingleNode("id").InnerText.Trim();
                                    }
                            }
                    }

                    if (rdlstDayColor.Items.Count > 0)
                    {
                        lblNoneDayColor.Visible = false;
                        rdlstDayColor.SelectedIndex = 0;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            if(log == null)
                log = new ns_Logger.Logger();

            log.Trace("Page_Load" + ex.StackTrace);
        }
    }
}
