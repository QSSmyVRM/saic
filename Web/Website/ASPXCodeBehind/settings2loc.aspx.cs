/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_settings2loc : System.Web.UI.Page
{
    protected string xmlstr = "";
    protected string paramSpecial = "";
    protected string paramFVal = "";
    protected string paramHFVal = "";
    protected string strsettings2loc = "";
    protected string locxml = "";
    protected string locidnames = "";
    protected string selectedlocs = "";
    protected string comparedselLoc = "";
    protected string paramMod = "";
    protected string paramSp = "";
    protected string roomExpandLevel = ""; 
    protected myVRMNet.NETFunctions obj = null;
    protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
    protected System.Web.UI.HtmlControls.HtmlInputHidden locidname;
    protected System.Web.UI.HtmlControls.HtmlInputHidden special;
    protected System.Web.UI.HtmlControls.HtmlInputHidden nonvideolocname;
    protected System.Web.UI.HtmlControls.HtmlInputHidden comparedlocs;
    protected System.Web.UI.HtmlControls.HtmlInputHidden comparedsellocs;
    protected System.Web.UI.HtmlControls.HtmlInputHidden locstr;

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("settings2loc.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            SetQueryStringVal();
            
            GetGlobalVariables();

            BindRoomCalendarData();

            if (strsettings2loc == "!")
            {
                Response.Redirect("settings2locfail.aspx?f=frmSettings2&wintype=ifr");
            }
        }
        catch (System.Threading.ThreadAbortException) { }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    #endregion

    #region Method to set the Query string values

    private void SetQueryStringVal()
    {
        if(Request.QueryString["special"] != null)
        {
            paramSpecial = Request.QueryString["special"].ToString();
        }
        if(Request.QueryString["hf"] != null)
        {
            paramHFVal = Request.QueryString["hf"].ToString();
        }
        if(Request.QueryString["f"] != null)
        {
            paramFVal = Request.QueryString["f"].ToString();
        }
        if (Request.QueryString["mod"] != null)
        {
            paramMod = Request.QueryString["mod"].ToString();
        }
        if (Request.QueryString["sp"] != null)
        {
            paramSp = Request.QueryString["sp"].ToString();
        }
    }
    #endregion

    #region Method to get the global variables

    private void GetGlobalVariables()
    {
        if (Session["roomExpandLevel"] != null)//Organization Module Fixes
        {
            roomExpandLevel = Session["roomExpandLevel"].ToString();//Organization Module Fixes
        }
    }
    #endregion

    #region MEthod to get the Calendar XML

    private void GetRoomCalendarXML()
    {
        if(paramSpecial == "1")
        {
            if (Session["oth_strXML"] != null)
            {
                xmlstr = Session["oth_strXML"].ToString().Replace("\"", "\\\"");
            }
        }
        else
        {
            if (paramHFVal == "1")
            {
                if (Session["calXML"] != null)
                {
                    xmlstr = Session["calXML"].ToString().Replace("\"", "\\\"");
                }
            }
            else
            {
                if (Session["outXML"] != null)
                {
                    xmlstr = Session["outXML"].ToString().Replace("\"", "\\\"");
                }
            }
       }
    }
    #endregion

    #region Bind Room Calendar Data

    private void BindRoomCalendarData()
    {
        try
        {
            obj = new myVRMNet.NETFunctions();

            //code added for FB 1479 -- Start

            if (paramFVal.IndexOf("alendar") >= 0)
            {
                obj.GetManageConfRoomData(paramFVal, paramHFVal, "", "");
            }

            //code added for FB 1479 -- End

            GetRoomCalendarXML();

            strsettings2loc = obj.mkLocJSstr(xmlstr, false, paramFVal);

            /* following line is to fix the "\" escape character in javascript, Fog bugz #2194 */

            strsettings2loc = strsettings2loc.Replace("\\", "\\\\");

            locxml = xmlstr;
            if (xmlstr.IndexOf("<scheduleRoom>") >= 0)
            {
                locxml = "<locationList>" + obj.ParseStr(xmlstr, "locationList", 1) + "</locationList>";
            }

            //Assign room details to the hidden controls
            locidnames = obj.GetAllRoomIDName(locxml);
            this.locidname.Value = locidnames;

            selectedlocs = obj.GetOrgLocIDs(locxml, "1");
            this.selectedloc.Value = selectedlocs;

            comparedselLoc = obj.GetOrgLocSrcs(locxml);
            this.comparedsellocs.Value = comparedselLoc;

            this.locstr.Value = strsettings2loc;
            this.special.Value = paramSpecial;
        }
        catch
        {
            Response.Redirect("UnderConstruction.asp");
        }
    }
    #endregion
}
