/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections.Generic;
using System.Text;

namespace ns_MyVRMNet
{
    public class ErrorList
    {
        public const String InvalidDuration = "Please enter a valid duration.";
        public const String InvalidIPAddress = "Invalid IP Address.";
        public const String InvalidISDNAddress = "Invalid ISDN Address.";
        public const String InvalidMPIAddress = "Invalid MPI Address - Alphanumeric characters only.";
        public const String InvalidAccountExpiry = "Invalid Account Expiration Date.";
        public const String InvalidMPIBridge = "Please select a bridge with at least one associated MPI Service.";
        public const String ExceedDuration = "Maximum limit is 24 hours including buffer period. Please enter a valid duration."; //FB 2634
    }
}
