//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Conference
    {
        int externalDatabaseType;
        DataTable masterDT;
        String configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;

        

        public Conference(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        public bool Process(ref int cnt)
        {
            try
            {
                // split the array list and extract the tiers
                String inXML = "<login><userID>11</userID></login>";
                String outXML = obj.CallCOM("GetLocations", inXML, configPath);
                XmlDocument xdTier1 = new XmlDocument();
                xdTier1.LoadXml(outXML);
                XmlNodeList nodesTier1 = xdTier1.SelectNodes("//locationList/tier1List/tier1");
                //log.Trace("\r\nTiers Loaded Successfully!");
                inXML = "<login><userID>11</userID></login>";
                outXML = obj.CallCOM("GetManageDepartment", inXML, configPath);
                XmlDocument xdDepartment = new XmlDocument();
                xdDepartment.LoadXml(outXML);
                XmlNodeList nodesDept = xdDepartment.SelectNodes("//getManageDepartment/departments/department");
                //log.Trace("\r\nTier2s Loaded Successfully!");
                inXML = "<login><userID>11</userID></login>";
                outXML = obj.CallCOM("ManageConfRoom", inXML, configPath);
                XmlDocument xdRooms = new XmlDocument();
                xdRooms.LoadXml(outXML);
                XmlNodeList nodesRoom = xdRooms.SelectNodes("//manageConfRoom/locationList/level3List/level3");
                //log.Trace("\r\nRooms Loaded Successfully!");
                //HttpContext.Current.Response.Write(nodesRoom.Count);

                


                DataRow dr;
                DataRow drNext; // = new DataRow();
                //log.Trace("Rows count: " + masterDT.Rows.Count);
                for (int j = 0; j < masterDT.Rows.Count - 1 ; j++)
                {
                    dr = masterDT.Rows[j];
                    drNext = masterDT.Rows[j + 1];
                    log.Trace(j.ToString());
                    int tier1ID = 0;
                    if (dr["Tier1"].ToString().IndexOf("(none)") < 0)
                        tier1ID = GetTier1ID(nodesTier1, dr["Tier1"].ToString());
                    String RoomID = "";
                    int tier2ID = GetTier2ID(tier1ID, dr["Tier2"].ToString());

                    if (dr["Room"].ToString().IndexOf("(none)") < 0)
                        RoomID = GetRoomID(nodesRoom, tier1ID, tier2ID, dr["Room"].ToString()).ToString();

                    if (dr["ConferenceName"].ToString().Trim().Equals(drNext["ConferenceName"].ToString().Trim()) && dr["StartDateTime"].ToString().Trim().Equals(drNext["StartDateTime"].ToString().Trim()) && dr["Duration"].ToString().Trim().Equals(drNext["Duration"].ToString().Trim()))
                        while (dr["ConferenceName"].ToString().Trim().Equals(drNext["ConferenceName"].ToString().Trim()) && dr["StartDateTime"].ToString().Trim().Equals(drNext["StartDateTime"].ToString().Trim()) && dr["Duration"].ToString().Trim().Equals(drNext["Duration"].ToString().Trim()))
                        {
                            //log.Trace("in while: " + RoomID + " : " + dr["Room"].ToString() + " : " + drNext["Room"].ToString());
                            if (drNext["Tier1"].ToString().IndexOf("(none)") < 0)
                                tier1ID = GetTier1ID(nodesTier1, drNext["Tier1"].ToString());
                            tier2ID = GetTier2ID(tier1ID, drNext["Tier2"].ToString());
                            if (drNext["Room"].ToString().IndexOf("(none)") < 0)
                                RoomID += "," + GetRoomID(nodesRoom, tier1ID, tier2ID, drNext["Room"].ToString()).ToString();
                            j++;
                            dr = masterDT.Rows[j];

                            if (j < masterDT.Rows.Count - 1)
                                drNext = masterDT.Rows[j + 1];
                            else
                                break;//drNext = masterDT.Rows[j];
                            
                        }

                    log.Trace("\r\nRoomID: " + RoomID + ", ConfName: " + dr["ConferenceName"].ToString() + ", ConfType: " + dr["ConferenceType"].ToString() + ", Timezone: " + dr["TimeZone"].ToString() + ", StartDateTime: " + dr["StartDateTime"].ToString() + ", Duration: " + dr["Duration"].ToString() + ", Remarks: " + dr["Remarks"].ToString());
                    String confInXML = Create_ConferenceInXML(RoomID, dr["ConferenceName"].ToString(), dr["ConferenceType"].ToString(), dr["TimeZone"].ToString(), dr["StartDateTime"].ToString(), dr["Duration"].ToString(), dr["Remarks"].ToString(),dr["UserID"].ToString());
                    log.Trace("\r\nInxml: " + confInXML);
                    if (!inXML.Equals("-1"))
                    {
                        //log.Trace("\r\n$$$in SetConference$$$");
                        System.Threading.Thread.Sleep(3000);
                        outXML = obj.CallCOM("SetConference", confInXML, configPath);
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            //write to a log file                 
                            log.Trace("error: " + outXML);
                            //log.Trace(obj.ShowErrorMessage(outXML)); //"\r\nError in SetConference");
                        }
                        else
                        {
                            log.Trace("\r\nSuccess");
                            cnt++;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace("\r\nSetConference: Finished");
                return false;
            }
        }

        protected int GetTier1ID(XmlNodeList nodes, String tier1Name)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("tier1Name").InnerText.Trim().Equals(tier1Name.Trim()))
                        return Int32.Parse(node.SelectSingleNode("tier1ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }
        protected int GetTier2ID(int tier1ID, String tier2Name)
        {
            try
            {
                String inXML = "<login><userID>11</userID><tier1ID>" + tier1ID + "</tier1ID></login>";
                String outXML = obj.CallCOM("GetLocations2", inXML, configPath);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//location2List/tier2List/tier2");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("tier2Name").InnerText.Trim().Equals(tier2Name.Trim()))
                        return Int32.Parse(node.SelectSingleNode("tier2ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }
        protected int GetRoomID(XmlNodeList nodes, int tier1Id, int tier2ID, String RoomName)
        {
            try
            {
                //HttpContext.Current.Response.Write("<br>" + tier1Id + " : " + tier2ID + " : " + RoomName);

                foreach (XmlNode node1 in nodes)
                {
                    //HttpContext.Current.Response.Write("<br>" + node1.SelectSingleNode("level3ID").InnerText);
                    if (node1.SelectSingleNode("level3ID").InnerText.Trim().Equals(tier1Id.ToString()))
                    {
                        //HttpContext.Current.Response.Write("in if 1");
                        XmlNodeList nodes2 = node1.SelectNodes("level2List/level2");
                        foreach (XmlNode node2 in nodes2)
                        {
                            if (node2.SelectSingleNode("level2ID").InnerText.Trim().Equals(tier2ID.ToString()))
                            {
                                //HttpContext.Current.Response.Write("in if 2");
                                XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                                foreach(XmlNode node in nodes1)
                                    if (node.SelectSingleNode("level1Name").InnerText.Trim().Equals(RoomName.Trim()))
                                        return Int32.Parse(node.SelectSingleNode("level1ID").InnerText);
                            }
                        }
                    }
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }
        private String Create_ConferenceInXML(String RoomID, String ConfName, String ConfType, String Timezone, String StartDateTime, String Duration, String Description,string userID )
        {
            try
            {
                String inXML = ""; 
                inXML += "<conference>";
                inXML += "  <userID>" + userID + "</userID>";
                inXML += "  <confInfo>";
                inXML += "    <confID>new</confID>";
                inXML += "    <confHost>"+ userID +"</confHost>";
                inXML += "    <confOrigin>0</confOrigin>";
                inXML += "    <timeCheck>1</timeCheck>";
                inXML += "    <confName>" + ConfName + "</confName>";
                inXML += "    <confPassword></confPassword>";
                inXML += "    <immediate>0</immediate>";
                inXML += "    <recurring>0</recurring>";
                log.Trace("\r\nStartDateTime: " + StartDateTime);
                DateTime sDateTime = DateTime.Parse(StartDateTime);
                inXML += "    <startDate>" + sDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                inXML += "    <startHour>" + sDateTime.ToString("HH") + "</startHour>";
                inXML += "    <startMin>" + sDateTime.ToString("mm") + "</startMin>";
                inXML += "    <startSet>" + sDateTime.ToString("tt") + "</startSet>";
                inXML += "    <timeZone>26</timeZone>";
               

                inXML += "    <createBy>" + ConfType + "</createBy>";
                inXML += "    <durationMin>" + Duration + "</durationMin>";
                inXML += "    <description>" + Description + "</description>";
                inXML += "    <locationList>";
                inXML += "      <selected>";
                if (RoomID.IndexOf(",") < 0)
                    inXML += "        <level1ID>" + RoomID + "</level1ID>";
                else
                    for(int i=0;i<RoomID.Split(',').Length;i++)
                        if (!RoomID.Split(',')[i].Equals("") && !RoomID.Split(',')[i].Equals("-1"))
                            inXML += "        <level1ID>" + RoomID.Split(',')[i].ToString() + "</level1ID>";
                inXML += "      </selected>";
                inXML += "    </locationList>";
                inXML += "    <publicConf>1</publicConf>";
                inXML += "    <dynamicInvite>1</dynamicInvite>";
                inXML += "    <advAVParam>";
                inXML += "      <maxAudioPart></maxAudioPart>";
                inXML += "      <maxVideoPart></maxVideoPart>";
                inXML += "      <restrictProtocol>1</restrictProtocol>";
                inXML += "      <restrictAV>1</restrictAV>";
                inXML += "      <videoLayout>01</videoLayout>";
                inXML += "      <maxLineRateID></maxLineRateID>";
                inXML += "      <audioCodec></audioCodec>";
                inXML += "      <videoCodec></videoCodec>";
                inXML += "      <dualStream>0</dualStream>";
                inXML += "      <confOnPort>0</confOnPort>";
                inXML += "      <encryption>0</encryption>";
                inXML += "      <lectureMode>0</lectureMode>";
                inXML += "      <VideoMode></VideoMode>";
                inXML += "    </advAVParam>";
                inXML += "    <ModifyType>1</ModifyType>";
                inXML += "    <fileUpload>";
                inXML += "      <file></file>";
                inXML += "      <file></file>";
                inXML += "      <file></file>";
                inXML += "    </fileUpload>";
                inXML += "    <CustomAttributesList>";
                inXML += "      <CustomAttribute>";
                inXML += "        <CustomAttributeID>1</CustomAttributeID>";
                inXML += "        <OptionID>1</OptionID>";
                inXML += "        <Type>4</Type>";
                inXML += "        <OptionValue></OptionValue>";
                inXML += "     </CustomAttribute>";
                inXML += "    </CustomAttributesList>";
                inXML += "    <ICALAttachment></ICALAttachment>";
                inXML += "  </confInfo>";
                inXML += "</conference>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("\r\nException in Generating InXML: " + ex.StackTrace + " | " + ex.Message);
                return "-1";
            }
        }
    }
}