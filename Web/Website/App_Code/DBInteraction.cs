//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DBInteraction
/// </summary>
public class DBInteraction
{
    string connectionString;
    
	public DBInteraction()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// ConnectionString is a public property
    /// </summary>
    public string ConnectionString
    {
        get
        {
            return connectionString;
        }
        set
        {
            connectionString = value; 
        }
    }

    /// <summary>
    /// This function will use connectionstring property to connect to DB
    /// and will fire sql query passed in as parameter. It will then return the
    /// resultant dataset
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public DataSet ExecuteQuery(string query)
    {
        DataSet ds = new DataSet();
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand(); 
        SqlDataAdapter da;
        try
        {
            cmd.Connection = connection;
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;

            da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            return (ds);
        }
        catch (Exception ex)
        {
            throw ex;
            return null;
        }
        finally
        {
            connection.Close();
            connection = null;
            cmd.Dispose();
            cmd = null;
            //da.Dispose();
            //da = null; 
        }
    }
}
