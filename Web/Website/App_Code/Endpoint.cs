//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Endpoint
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        Hashtable lstBridgeaddress = null;
        

        public Endpoint(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        public bool Process(ref int cnt)
        {
            DataRow dr;
            //DataRow drNext;//FB 2519
            String outXML = "";

            String inXML = "";
            string eptnme = "";//FB 2362
            try
            {
               
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                outXML = obj.CallMyVRMServer("GetBridgeList", inXML, configPath);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
                if (nodes.Count > 0)
                {
                    for (int j = 0; j < nodes.Count; j++)
                    {
                        if (lstBridgeaddress == null)
                            lstBridgeaddress = new Hashtable();

                        if (!lstBridgeaddress.Contains(nodes[j]["address"].InnerText.ToString().Trim()))
                            lstBridgeaddress.Add(nodes[j]["address"].InnerText.ToString().Trim(), nodes[j]["ID"].InnerText.ToString());
                    }

                }


                lstVideoEquipment = new DropDownList();
                lstVideoEquipment.DataValueField = "VideoEquipmentID";
                lstVideoEquipment.DataTextField = "VideoEquipmentName";
                obj.BindVideoEquipment(lstVideoEquipment);


                for (int j = 0; j <= masterDT.Rows.Count - 1; j++)//FB 2519
                {
                    dr = masterDT.Rows[j];
                    //drNext = masterDT.Rows[j + 1];//FB 2519
                    if (dr["Name"].ToString().Trim() != "")//FB 2362
                    {
                        String str = "";

                        if (dr["Name"].ToString().IndexOf('&') >= 0)
                            str = "";

                        String EndpointInXML = Create_EndpointInXML(dr);
                        log.Trace("<br>" + EndpointInXML);
                        outXML = obj.CallMyVRMServer("SetEndpoint", EndpointInXML, configPath);
                        log.Trace(outXML);
                        if (outXML.IndexOf("<error>") >= 0)
                        {

                            log.Trace(obj.ShowErrorMessage(outXML));
                        }
                        else
                        {
                            log.Trace("Success");
                            cnt++;
                        }
                    }
                }

            }
            catch (Exception)
            {
                
                
            }
            
            return true;
        }
        //FB 2519 start
        protected string EndpointPassword(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }
        //FB 2519 End
        private String Create_EndpointInXML(DataRow dr)
        {
            string bridgeID = "-1";
            string equipmentID = "24";
            try
            {
                TextBox txtapiTemp = null;
                String inXML = "<SetEndpoint>";
                inXML += obj.OrgXMLElement();
                inXML += "      <EndpointID>new</EndpointID>";
                inXML += "      <EndpointName>" + dr["Name"].ToString().Replace("&", " and ") + "</EndpointName>";
                inXML += "      <EntityType></EntityType>";
                inXML += "      <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
                inXML += "      <userID>new</userID>";
                inXML += "      <Profiles>";
                inXML += "      <Profile>";
                inXML += "      <ProfileID>new</ProfileID>";
                inXML += "      <ProfileName>" + dr["Name"].ToString() + "</ProfileName>";
                inXML += "      <Deleted>0</Deleted>";//FB 2362
                inXML += "      <Default>1</Default>";
                inXML += "      <EncryptionPreferred>0</EncryptionPreferred>";
                inXML += "          <AddressType>1</AddressType>";
                inXML += "          <Password>" + EndpointPassword(dr["Password"].ToString())+"</Password>";//FB 2519
                inXML += "          <Address>" + dr["Address"].ToString() + "</Address>";
                inXML += "          <URL></URL>";
                inXML += "          <IsOutside>0</IsOutside>";
                inXML += "          <ConnectionType>1</ConnectionType>";//FB 2519

                for (int j = 0; j < lstVideoEquipment.Items.Count ; j++)
                {
                    if(lstVideoEquipment.Items[j].Text.ToUpper().Contains(dr["Model"].ToString().ToUpper()) ||dr["Model"].ToString().ToUpper().Contains(lstVideoEquipment.Items[j].Text.ToUpper()))
                        equipmentID = lstVideoEquipment.Items[j].Value;

                }

                inXML += "          <VideoEquipment>"+ equipmentID +"</VideoEquipment>";
                inXML += "          <LineRate>"+ dr["Preferred Bandwidth"].ToString()+ "</LineRate>";
                inXML += "          <DefaultProtocol>1</DefaultProtocol>";

                if (lstBridgeaddress != null)
                    if (lstBridgeaddress.Contains(dr["MCU Assignment"].ToString().Trim()))
                        bridgeID = lstBridgeaddress[dr["MCU Assignment"].ToString().Trim()].ToString();

                inXML += "          <Bridge>" + bridgeID + "</Bridge>";
                inXML += "          <MCUAddress>" + dr["MCU Assignment"].ToString() + "</MCUAddress>";
                inXML += "          <MCUAddressType>1</MCUAddressType>";
                inXML += "          <TelnetAPI>0</TelnetAPI>";
                inXML += "          <ExchangeID></ExchangeID>";
                inXML += "          <IsCalendarInvite>0</IsCalendarInvite>";
                inXML += "              <ApiPortno></ApiPortno>";
				//FB 2519 start
                inXML += "          <isTelePresence>0</isTelePresence>"; 
                inXML += "         <Secured>0</Secured>";
                inXML += "         <NetworkURL></NetworkURL>";
                inXML += "         <Securedport></Securedport>";
				//FB 2519 End
                inXML += "        </Profile>";
                inXML += "      </Profiles>";
                inXML += "<EM7EndpointStatus>0</EM7EndpointStatus>"; //FB 2519
                inXML += "<Eptcurrentstatus>0</Eptcurrentstatus>"; //FB 2519
                inXML += "    </SetEndpoint>";
                return inXML;
            }

            catch (Exception ex)
            {
                return "";
            }
        }
    }
}