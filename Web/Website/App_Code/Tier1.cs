//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Tier1
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;

        public Tier1(int external, DataTable masterDataTable, string config)
        {
            try
            {
                externalDatabaseType = external;
                masterDT = masterDataTable;
                configPath = config;
                log = new ns_Logger.Logger();
                obj = new myVRMNet.NETFunctions();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.StackTrace + " : " + ex.Message);
            }
        }

        public bool Process(ref int cnt)
        {
            DataRow dr;
            //DataRow drNext; // = new DataRow(); //FB 2519
            String outXML = "";
            for (int j = 0; j <= masterDT.Rows.Count - 1; j++)//FB 2519
            {
                dr = masterDT.Rows[j];
                //drNext = masterDT.Rows[j + 1];//FB 2519
                String Tier1InXML = Create_Tier1InXML(dr["Tier One"].ToString());
                log.Trace("<br>" + Tier1InXML);
                outXML = obj.CallMyVRMServer("SetTier1", Tier1InXML, configPath);

                log.Trace(outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    //write to a log file                 
                    log.Trace(obj.ShowErrorMessage(outXML));
                }
                else
                {
                    log.Trace("Success");
                    cnt++;
                }
            }

            // put that in a messenger object 
            // call the com cmd 
            // get the tiers 
            // save the info in csv file
            return true;
        }

    private String Create_Tier1InXML(String TierOne)
    {
        try
        {
            String inXML = "<SetTier1>";
                   inXML += obj.OrgXMLElement();
                   inXML += "  <UserID> " + HttpContext.Current.Session["userID"] + "</UserID>";
                   //inXML += " <userID>new</userID>";
               // inXML += "  <tier1>";
                   inXML += "   <ID>new</ID>";
                inXML += "      <Name>" + TierOne + "</Name>";
               // inXML += "  </tier1>";
                inXML += "</SetTier1>";
            return inXML;
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    }
}