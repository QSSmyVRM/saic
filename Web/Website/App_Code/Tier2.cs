//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Tier2
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;

        public Tier2(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        public bool Process(ref int cnt)
        {
            DataRow dr;
            //DataRow drNext; // = new DataRow();//FB 2519
            String inXML = "";
            inXML += "<GetLocations>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
            inXML += "</GetLocations>";
            String outXML = obj.CallMyVRMServer("GetLocations", inXML, configPath);
            XmlDocument xdTier1 = new XmlDocument();
            xdTier1.LoadXml(outXML);
            XmlNodeList nodesTier1 = xdTier1.SelectNodes("//GetLocations/Location");
            outXML = "";
            for (int j = 0; j <= masterDT.Rows.Count - 1; j++)//FB 2519
            {
                dr = masterDT.Rows[j];
                //drNext = masterDT.Rows[j + 1];//FB 2519
                int tier1ID = 0;
                if (dr["Tier One"].ToString().IndexOf("(none)") < 0)
                    tier1ID = GetTier1ID(nodesTier1, dr["Tier One"].ToString());
                String Tier2InXML = Create_Tier2InXML(dr["Tier Two"].ToString(), tier1ID);
                log.Trace("<br>" + Tier2InXML);
                outXML = obj.CallMyVRMServer("SetTier2", Tier2InXML, configPath);
                log.Trace(outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    //write to a log file                 
                    log.Trace(obj.ShowErrorMessage(outXML));
                }
                else
                {
                    log.Trace("Success");
                    cnt++;
                }
            }

            // put that in a messenger object 
            // call the com cmd 
            // get the tiers 
            // save the info in csv file
            return true;
        }
        protected int GetTier1ID(XmlNodeList nodes, String tier1Name)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("Name").InnerText.Trim().Equals(tier1Name.Trim()))
                        return Int32.Parse(node.SelectSingleNode("ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }

        private String Create_Tier2InXML(String TierTwo, int tier1ID)
      {
        try
        {
                String inXML = "<SetTier2>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID> " + HttpContext.Current.Session["userID"] + "</UserID>";
                inXML += "  <Tier1ID>" + tier1ID + "</Tier1ID>";
                inXML += "   <ID>new</ID>";
                inXML += "   <Name>" + TierTwo + "</Name>";
                inXML += "   </SetTier2>";
            return inXML;
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    }
}