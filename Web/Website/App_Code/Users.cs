//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class User
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        DataTable dtZone;
        DataView dvZone;
        Hashtable lstBridgeaddress = null;
        System.Web.UI.WebControls.DropDownList lstVideoEquipment;

        public User(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        public bool Process(ref int cnt,ref bool alluserimport,ref DataTable dterror)
        {
            DataRow dr;
            //DataRow drNext; 
            String outXML = "";
            XmlDocument failedlist = new XmlDocument();
            XmlNodeList nodelist2;
            XmlNode newnode = null;
            DataRow errow = null;
            string lotus = "0", exchange = "0", mobile = "0", roleid = "0";
            String bridgeID = "";
            String inXML = "";
            string pattern = "";
            inXML += "<login>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
            inXML += "</login>";
            outXML = obj.CallMyVRMServer("GetBridgeList", inXML, configPath);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
            if (nodes.Count > 0)
            {
                for (int j = 0; j < nodes.Count; j++)
                {
                    if (lstBridgeaddress == null)
                        lstBridgeaddress = new Hashtable();

                    if (!lstBridgeaddress.Contains(nodes[j]["name"].InnerText.ToString().Trim()))
                        lstBridgeaddress.Add(nodes[j]["name"].InnerText.ToString().Trim(), nodes[j]["ID"].InnerText.ToString());
                }

            }


            lstVideoEquipment = new DropDownList();
            lstVideoEquipment.DataValueField = "VideoEquipmentID";
            lstVideoEquipment.DataTextField = "VideoEquipmentName";
            obj.BindVideoEquipment(lstVideoEquipment);
            
            for (int j = 0; j < masterDT.Rows.Count; j++)
            {
                dr = masterDT.Rows[j];
                errow = dterror.NewRow();
                String lstConferenceTZ = null, tdzone = null;
                int errorusernum = 0;
                errorusernum = j + 2;
                //drNext = masterDT.Rows[j + 1];
                if (dr["First Name"].ToString() == "" || dr["Last Name"].ToString() == "" || dr["Email"].ToString() == "" || dr["Initial Password"].ToString() == "" || dr["Timezone"].ToString()=="")
                {
                    if (dr["First Name"].ToString() == "")
                    {
                        errow["RowNo"] = errorusernum.ToString();
                        errow["Reason"] = "User doesnt have first name";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr["Last Name"].ToString() == "")
                    {
                        errow["RowNo"] = errorusernum.ToString();
                        errow["Reason"] = "User doesnt have Last name";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr["Email"].ToString() == "")
                    {
                        errow["RowNo"] = errorusernum.ToString();
                        errow["Reason"] = "User doesnt have EmailId";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr["Initial Password"].ToString() == "")
                    {
                        errow["RowNo"] = errorusernum.ToString();
                        errow["Reason"] = "User doesnt have Password";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr["Timezone"].ToString() == "")
                    {
                        errow["RowNo"] = errorusernum.ToString();
                        errow["Reason"] = "User doesnt have Timezone";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                }
                else
                {
                    pattern = @"^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$";
                    Regex check = new Regex(pattern);
                    if (!check.IsMatch(dr["First Name"].ToString(), 0))
                    {
                        errow["RowNo"] = errorusernum.ToString();
                        errow["Reason"] = "<br>& < > + % ? | = ! ` [ ] { } # $ @ and ~ are invalid characters.";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (!check.IsMatch(dr["Last Name"].ToString(), 0))
                    {
                        errow["RowNo"] = errorusernum.ToString();
                        errow["Reason"] = "<br>& < > + % ? | = ! ` [ ] { } # $ @ and ~ are invalid characters.";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr["Email"].ToString() != "")
                    {
                        String email = dr["Email"].ToString();
                        String em = email.Split('@')[1];
                        if (!email.Contains("@"))
                        {
                            errow["RowNo"] = errorusernum.ToString();
                            errow["Reason"] = "User doesnt have valid EmailId";
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                        else if(!em.Contains("."))
                        {
                            errow["RowNo"] = errorusernum.ToString();
                            errow["Reason"] = "User doesnt have valid EmailId";
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                    }
                    string outuser = dr["Outlook/Notes"].ToString();
                    if(outuser == "Outlook")
                    {
                         lotus = "0"; exchange = "1"; mobile = "0";
                    }
                    else if(outuser == "Notes")
                    {
                        lotus = "1"; exchange = "0"; mobile = "0";
                    }
                    else if (outuser == "Mobile")
                    {
                        lotus = "0"; exchange = "0"; mobile = "1";
                    }
                    else
                    {
                        lotus = "0"; exchange = "0"; mobile = "0";
                    }
                    string userrole = dr["User Role (User/Admin/Super Adminetc)"].ToString();
                    if (userrole == "Super Admin")
                    {
                        roleid = "3";
                    }
                    else if (userrole == "Admin")
                    {
                        roleid = "2";
                    }
                    else
                    {
                        roleid = "1";
                    }

                    if (lstBridgeaddress != null)
                        if (lstBridgeaddress.Contains(dr["Assigned MCU (only in case of multiple)"].ToString().Trim()))
                            bridgeID = lstBridgeaddress[dr["Assigned MCU (only in case of multiple)"].ToString().Trim()].ToString();

                    lstConferenceTZ = dr["Timezone"].ToString();
                    tdzone = GetTimeZoneID(lstConferenceTZ);
                    String userInXML = Create_userInXML(dr["First Name"].ToString(), dr["Last Name"].ToString(), dr["Email"].ToString(), dr["Initial Password"].ToString(),tdzone,exchange,lotus,mobile,roleid,bridgeID);
                    log.Trace("<br>" + userInXML);
                    outXML = obj.CallMyVRMServer("SetUser", userInXML, configPath);
                    log.Trace(outXML);
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        alluserimport = false;
                        //write to a log file                 
                        log.Trace(obj.ShowErrorMessage(outXML));
                        failedlist.LoadXml(outXML);
                        newnode = failedlist.SelectSingleNode("//error/message");
                        nodelist2 = failedlist.SelectNodes("//error/message");
                        if (nodelist2.Count > 0)
                        {
                            errow["RowNo"] = errorusernum.ToString();
                            errow["Reason"] = newnode.InnerXml;
                            dterror.Rows.Add(errow);
                        }
                    }
                    else
                    {
                        log.Trace("Success");
                        cnt++;
                    }
                }
            }
            return true;
        }

        private String Create_userInXML(String fName, String lName, String userEmail, String password, String tdzone, String exchange, String lotus, String mobile,String roleid,String bridgeid)
        {
            try
            {
                String inXML = ""; 
                inXML += "<saveUser>";
                inXML += obj.OrgXMLElement();
                inXML += "<login>";
                inXML += "<userID>" + HttpContext.Current.Session["userID"] + "</userID>";
                inXML += "</login>";
                inXML += "<user>";
                inXML += "<userID>new</userID>";
                inXML += "<userName>";
                inXML += "<firstName>" + fName + "</firstName>";
                inXML += "<lastName>" + lName + "</lastName>";
                inXML += "</userName>";
                inXML += "<login>" + fName + "." + lName + "</login>";
                inXML += "<password>" + UserPassword(password) + "</password>";//FB 2519
                inXML += "<userEmail>" + userEmail + "</userEmail>";
                inXML += "<alternativeEmail></alternativeEmail>";
                inXML += "<SavedSearch>-1</SavedSearch>";
                inXML += "<workPhone></workPhone>";
                inXML += "<cellPhone></cellPhone>";
                inXML += "<sendBoth>0</sendBoth>";
                inXML += "<enableParticipants>1</enableParticipants>"; //FB 2519
                inXML += "<enableAV>1</enableAV>"; //FB 2519
                inXML += "<emailClient>-1</emailClient>";//FB 2519
                inXML += "<userInterface>2</userInterface>";
                inXML += "<timeZone>" + tdzone + "</timeZone>";
                inXML += "<location></location>";
                inXML += "<dateFormat>MM/dd/yyyy</dateFormat>";
                inXML += "<tickerStatus>1</tickerStatus>";
                inXML += "<tickerPosition>0</tickerPosition>";
                inXML += "<tickerSpeed>3</tickerSpeed>";
                inXML += "<tickerBackground>#660066</tickerBackground>";
                inXML += "<tickerDisplay>0</tickerDisplay>";
                inXML += "<rssFeedLink></rssFeedLink>";
                inXML += "<tickerStatus1>1</tickerStatus1>";
                inXML += "<tickerPosition1>0</tickerPosition1>";
                inXML += "<tickerSpeed1>3</tickerSpeed1>";
                inXML += "<tickerBackground1>#99ff99</tickerBackground1>";
                inXML += "<tickerDisplay1>0</tickerDisplay1>";
                inXML += "<rssFeedLink1></rssFeedLink1>";
                inXML += "<lineRateID>384</lineRateID>";
                inXML += "<videoProtocol>IP</videoProtocol>";
                inXML += "<conferenceCode></conferenceCode>";
                inXML += "<leaderPin></leaderPin>";
                inXML += "<IPISDNAddress></IPISDNAddress>";
                inXML += "<connectionType>1</connectionType>";
                inXML += "<videoEquipmentID></videoEquipmentID>";
                inXML += "<isOutside>0</isOutside>";
                inXML += "<addressTypeID>1</addressTypeID>";
                inXML += "<group></group>";
                inXML += "<ccGroup></ccGroup>";
                inXML += "<roleID>" + roleid + "</roleID>";
                inXML += "<initialTime>10000</initialTime>";
                inXML += "<expiryDate></expiryDate>";
                inXML += "<bridgeID>" + bridgeid + "</bridgeID>";
                inXML += "<languageID></languageID>";
                inXML += "<EmailLang></EmailLang>";
                inXML += "<departments>";
                inXML += "</departments>";
                inXML += "<status>";
                inXML += "<deleted>0</deleted>";
                inXML += "<locked>0</locked>";
                inXML += "</status>";
                inXML += "<creditCard>0</creditCard>";
                inXML += "<IntVideoNum></IntVideoNum>";
                inXML += "<ExtVideoNum></ExtVideoNum>";
                inXML += "</user>";
                inXML += "<emailMask>1</emailMask>";
                inXML += "<exchangeUser>" + exchange + "</exchangeUser>";
                inXML += "<dominoUser>" + lotus + "</dominoUser>";
                inXML += "<mobileUser>" + mobile + "</mobileUser>";
                inXML += "<audioaddon>0</audioaddon>";
                //FB 2519 start
                inXML += "<location></location>";
                inXML += "<SavedSearch></SavedSearch>";
                inXML += "<ExchangeID></ExchangeID>";
                inXML += "<HelpRequsetorEmail></HelpRequsetorEmail>";
                inXML += "<HelpRequsetorPhone></HelpRequsetorPhone>";
                inXML += "<RFIDValue></RFIDValue>";
                inXML += "<SendSurveyEmail></SendSurveyEmail>";
                inXML += "<EnableVNOCselection>0</EnableVNOCselection>";
                inXML += "<Secured>0</Secured>";
                inXML += "<PrivateVMR></PrivateVMR>";
                inXML += "<EnablePCUser>0</EnablePCUser>";
                //FB 2519 End
                inXML += "</saveUser>";
                return inXML;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        protected String GetTimeZoneID(String tZoneStr)
       {

           DataSet dsZone = new DataSet();
           myVRMNet.NETFunctions obj;
           String tZoneID = "33";
           String tZone = "";
           String[] tZoneArr = null;
           try
           {
               //code added for BCS - start
               //Time Zone
               String zoneInXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetTimezones>";
               String zoneOutXML;
               tZone = tZoneStr;
               if (dtZone == null)
               {
                   if (dsZone.Tables.Count == 0)
                   {
                       obj = new myVRMNet.NETFunctions();
                       zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                       obj = null;
                       XmlDocument xmldoc = new XmlDocument();
                       xmldoc.LoadXml(zoneOutXML);
                       XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                       if (nodes.Count > 0)
                       {
                           XmlTextReader xtr;
                           foreach (XmlNode node in nodes)
                           {
                               xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                               dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                           }
                           if (dsZone.Tables.Count > 0)
                           {
                               dvZone = new DataView(dsZone.Tables[0]);
                               dtZone = dvZone.Table;
                           }
                       }
                       //code added for BCS - start
                   }
               }
               if (tZone != "")
               {
                   tZoneArr = tZone.Split(' ');
                   int tzonelength = tZoneArr.Length;
                   String t1 = null, t2 = null;
                   if (tzonelength == 1)
                   {
                       t1 = tZoneArr[0];
                   }
                   else
                   {
                       t1 = tZoneArr[0];
                       t2 = tZoneArr[1];
                   }
                   foreach (DataRow row in dtZone.Rows)
                   {
                       if (tzonelength == 1)
                       {
                           if (row["timezoneName"].ToString().Contains(t1))
                           {

                               tZoneID = row["timezoneID"].ToString();
                               break;
                           }
                       }
                       else
                       {
                           if (row["timezoneName"].ToString().Contains(t1) && row["timezoneName"].ToString().Contains(t2))
                           {
                               tZoneID = row["timezoneID"].ToString();
                               break;
                           }
                       }

                   }

               }

           }
           catch (Exception ex)
           {
               throw ex;
           }

           return tZoneID;
       }
        //FB 2519 start
        protected string UserPassword(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                     obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }
        //FB 2519 End
    }
}