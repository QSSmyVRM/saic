﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class mcu
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        DataTable dtZone;//FB 2519
        DataView dvZone;//FB 2519
        System.Web.UI.WebControls.DropDownList lstTimeZone;//FB 2519

        public mcu(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        public bool Process(ref int cnt)
        {
            DataRow dr;
            //DataRow drNext; // = new DataRow();//FB 2519
            String outXML = "";
			//FB 2519 start
            string tzID = "-1";
            lstTimeZone = new DropDownList();
            lstTimeZone.DataValueField = "timezoneID";
            lstTimeZone.DataTextField = "timezoneName";
            obj.GetTimezones(lstTimeZone, ref tzID);
			//FB 2519 End
            for (int j = 0; j <= masterDT.Rows.Count - 1; j++) //FB 2519
            {
                dr = masterDT.Rows[j];
                //drNext = masterDT.Rows[j + 1]; //FB 2519
                String MCUInXML = Create_MCUInXML(dr["Name"].ToString(), dr["Login"].ToString(), dr["Password"].ToString(), dr["Vendor Type"].ToString(), GetTimeZoneID(dr["Timezone"].ToString()),dr["Control Port IP Address"].ToString());//FB 2519
                log.Trace("<br>" + MCUInXML);
                outXML = obj.CallMyVRMServer("SetBridge", MCUInXML, configPath);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    //write to a log file                 
                    log.Trace(obj.ShowErrorMessage(outXML));
                }
                else
                {
                    log.Trace("Success");
                    cnt++;
                }
            }
               return true;
        }
        //FB 2519 Starts
        protected String GetTimeZoneID(String tZoneStr)
        {

            DataSet dsZone = new DataSet();
            myVRMNet.NETFunctions obj;
            String tZoneID = "33";
            String tZone = "";
            String[] tZoneArr = null;
            try
            {
                String zoneInXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetTimezones>";
                String zoneOutXML;
                tZone = tZoneStr;
                if (dtZone == null)
                {
                    if (dsZone.Tables.Count == 0)
                    {
                        obj = new myVRMNet.NETFunctions();
                        zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                        obj = null;
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(zoneOutXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                        if (nodes.Count > 0)
                        {
                            XmlTextReader xtr;
                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            if (dsZone.Tables.Count > 0)
                            {
                                dvZone = new DataView(dsZone.Tables[0]);
                                dtZone = dvZone.Table;
                            }
                        }
                    }
                }
                if (tZone != "")
                {
                    tZoneArr = tZone.Split(' ');
                    int tzonelength = tZoneArr.Length;
                    String t1 = null, t2 = null;
                    if (tzonelength == 1)
                    {
                        t1 = tZoneArr[0];
                    }
                    else
                    {
                        t1 = tZoneArr[0];
                        t2 = tZoneArr[1];
                    }
                    foreach (DataRow row in dtZone.Rows)
                    {
                        if (tzonelength == 1)
                        {
                            if (row["timezoneName"].ToString().Contains(t1))
                            {

                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                        }
                        else
                        {
                            if (row["timezoneName"].ToString().Contains(t1) && row["timezoneName"].ToString().Contains(t2))
                            {
                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tZoneID;
        }
        //FB 2519 Ends
        private String Create_MCUInXML(String Name, String Login, String Password, String VendorType, string timezoneID,string ControlPortIPAddress )
        {
            try
            {
                int confserID = 0; //FB 2016
                String inXML = "<setBridge>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "    <userID>" + HttpContext.Current.Session["userID"] + "</userID>";
                inXML += "    <bridge>";
                inXML += "      <bridgeID>new</bridgeID>";
                inXML += "      <name>" + Name + "</name>";
                inXML += "      <login>" + Login + "</login>";
                inXML += "      <password>" +MCUPassword(Password) + "</password>";//FB 2519
                inXML += "      <timeZone>" + timezoneID  +"</timeZone>";//FB 2519
                inXML += "      <maxAudioCalls>10</maxAudioCalls>";
                inXML += "      <maxVideoCalls>10</maxVideoCalls>";
                inXML += "      <bridgeType>3</bridgeType>";//FB 2519
                inXML += "      <bridgeStatus>1</bridgeStatus>";
                inXML += "      <virtualBridge>1</virtualBridge>";
                //inXML += "      <virtualBridge>0</virtualBridge>"; //FB 2519
                inXML += "      <isPublic>1</isPublic>";
              //inXML += "      <isPublic>0</isPublic>";
                inXML += "      <setFavourite>0</setFavourite>";//FB 2519
                inXML += "      <enableCDR>0</enableCDR>";//FB 2519
                inXML += "      <bridgeAdmin>";
                inXML += "      <ID>11</ID>";//FB 2519
                inXML += "      </bridgeAdmin>";
                inXML += "      <firmwareVersion>2.x</firmwareVersion>";
                inXML += "      <percentReservedPort>20</percentReservedPort>";
                inXML += "      <approvers>";
                inXML += "      <approver>";
                inXML += "      <ID></ID>";
                inXML += "      </approver>";
                inXML += "      <approver>";
                inXML += "      <ID></ID>";
                inXML += "      </approver>";
                inXML += "      <approver>";
                inXML += "      <ID></ID>";
                inXML += "      </approver>";
                inXML += "      </approvers>";
                inXML += "      <ApiPortNo>80</ApiPortNo>";//API Port...
                inXML += "      <bridgeDetails>";
                inXML += "      <controlPortIPAddress>" + ControlPortIPAddress + "</controlPortIPAddress>";//FB 2519
                inXML += "      <EnableIVR>0</EnableIVR>";//FB 2519
                inXML += "      <IVRServiceName></IVRServiceName>";//FB 2519
                inXML += "      <enableRecord>0</enableRecord>";//FB 2519
                inXML += "      <enableLPR>0</enableLPR>";//FB 2519
                inXML += "      <EnableMessage>0</EnableMessage>";//FB 2519
                inXML += "      <IPServices>0</IPServices>";
                inXML += "      <ISDNServices>0</ISDNServices>";
                inXML += "          <ISDNService></ISDNService>";
                inXML += "          <MPIService>0</MPIService>";
                inXML += "      <MCUCards>0</MCUCards>";
                inXML += "          <portA></portA>";
                inXML += "          <portB></portB>";
                inXML += "          <ISDNAudioPref></ISDNAudioPref>";//FB 2003
                inXML += "          <ISDNVideoPref></ISDNVideoPref>";
                inXML += "      </bridgeDetails>";
                inXML += "  <ISDNThresholdAlert></ISDNThresholdAlert>";
                inXML += "  <ISDNPortCharge></ISDNPortCharge>";
                inXML += "  <ISDNLineCharge></ISDNLineCharge>";
                inXML += "  <ISDNMaxCost></ISDNMaxCost>";
                inXML += "  <ISDNThresholdTimeframe></ISDNThresholdTimeframe>";
                inXML += "  <ISDNThreshold></ISDNThreshold>";
                inXML += "      <malfunctionAlert>1</malfunctionAlert>";
                inXML += " <DTMF>";
                inXML += "     <PreConfCode></PreConfCode>";
                inXML += "     <PreLeaderPin></PreLeaderPin>";
                inXML += "     <PostLeaderPin></PostLeaderPin>";
                inXML += "</DTMF>";
                inXML += "<EnhancedMCU>0</EnhancedMCU>";//FB 2519
                inXML += "  </bridge>";
                inXML += "</setBridge>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }
        //FB 2519 start
        protected string MCUPassword(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }
        //FB 2519 End
      
    }
}