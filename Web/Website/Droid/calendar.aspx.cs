﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;

public partial class Droid_calendar : System.Web.UI.Page
{

    protected DateTime conf = DateTime.Now;
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;

    protected Boolean bypass = false, isAdminRole = false;
    protected String isPublic = "D", isFuture = "D", isPending = "D", isApproval = "D", isOngoing = "D";
    protected String tFormats = "hh:mm tt";
    protected String timeFormat = "1";
    private String open24 = "";
    #region Page_Load 

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userID"] == null)
        {
            Response.Redirect("login.aspx");
        }

        timeFormat = ((Session["timeFormat"] != null) ? Session["timeFormat"].ToString() : timeFormat);

        log = new ns_Logger.Logger();   
        obj = new myVRMNet.NETFunctions();

        if (!IsPostBack)
        {
            hdnDayDate.Value = conf.ToShortDateString();
            hdnWeekDate.Value = conf.ToString();
            hdnMonthDate.Value = conf.ToString();

            ChangeCalendarDate(null, null);
        }
    }
    #endregion

    #region ChangeCalendarDate

    protected void ChangeCalendarDate(Object sender, EventArgs e)
    {
        try
        {
            Int32 addsessn = 1;
            CalHeaderTextDay.Attributes.Add("style", "display:none");
            CalHeaderTextWeek.Attributes.Add("style", "display:none");
            CalHeaderTextMonth.Attributes.Add("style", "display:none");

            if (hdnView.Value == "")
                hdnView.Value = "1";

            switch (hdnView.Value)
            {
                case "1":
                    DatatablefromXML(GetMonthlyThread(), 1);
                    CalHeaderTextDay.Attributes.Add("Style", "Display:block");
                    CalHeaderTextDay.Text = conf.ToShortDateString();
                    break;
                case "2":
                    if (IsWeekOverLap.Value == "Y")
                        addsessn = 0;
                    else
                        Session.Remove("PersonalWeekly");
                    DatatablefromXML(GetWeeklyCalendar(), addsessn);
                    CalHeaderTextWeek.Attributes.Add("Style", "Display:block");

                    DateTime wkDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);
                    CalHeaderTextWeek.Text = wkDate.ToString("dd MMM") + " - " + wkDate.AddDays(6).ToString("dd MMM")
                       + " " + wkDate.Year;
                    break;
                case "3":
                    DatatablefromXML(GetMonthlyCalendar(), 1);
                    CalHeaderTextMonth.Attributes.Add("Style", "Display:block");
                    CalHeaderTextMonth.Text = conf.ToString("MMM") + " - " + conf.Year;
                    break;
            }

            if(Session["PersonalWeekly"] == null)
                DatatablefromXML(GetWeeklyCalendar(), addsessn);

            BindWeekly();
            BindDaily();
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindDaily 

    protected void BindDaily()
    {
        DataTable dt = null;

        try
        {
            SetOfficeHours(GetMonthlyThread());

            if (dt == null)
            {
                if (Session["PersonalCalendar"] != null)
                    dt = (DataTable)Session["PersonalCalendar"];
            }

            if (dt != null)
            {
                schDaypilot.StartDate = conf;


                schDaypilot.DataSource = dt;
                schDaypilot.DataBind();
                if (open24 == "0")
                {
                    schDaypilot.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHoursNoScroll;
                    schDaypilotweek.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHoursNoScroll;
                }
                else
                {
                    schDaypilot.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHours;
                    schDaypilotweek.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHours;
                }
                schDaypilot.Visible = true;

                //CalHeaderTextDay.Text = conf.ToShortDateString();
                //CalHeaderTextMonth.Text = conf.ToString("MMM") + " - " + conf.Year;
                schDaypilotMonth.StartDate = conf;
                schDaypilotMonth.DataSource = dt;
                schDaypilotMonth.DataBind();
                schDaypilotMonth.ShowWeekend = true; ;
                schDaypilotMonth.Visible = true;
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("BindDaily: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindWeekly

    protected void BindWeekly()
    {
        DataTable dt = null; ;

        try
        {
            SetOfficeHours(GetWeeklyCalendar());

            if (dt == null)
            {
                if (Session["PersonalWeekly"] != null)
                    dt = (DataTable)Session["PersonalWeekly"];
                else if (Session["PersonalCalendar"] != null)
                    dt = (DataTable)Session["PersonalCalendar"];
            }

            if (dt != null)
            {
                schDaypilotweek.StartDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);

                if(!IsPostBack)
                    CalHeaderTextWeek.Text = schDaypilotweek.StartDate.ToString("dd MMM") + " - " + schDaypilotweek.StartDate.AddDays(6).ToString("dd MMM")
                        + " " + schDaypilot.StartDate.Year;

                schDaypilotweek.HeaderDateFormat = "dd MMM";

                schDaypilotweek.Days = 7;
                schDaypilotweek.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHours;
                schDaypilotweek.DataSource = dt;
                schDaypilotweek.DataBind();
                schDaypilotweek.Visible = true;
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region SetOfficeHours 
    private void SetOfficeHours(string xmls)
    {
        String hdr = ("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM";
        Int32 hrs, mins = 0, cnt = 1;

        XmlDocument xmldoc = null;
        DataTable dt = null;
        try
        {
            xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmls);

            dt = GetDataTable();

            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

            if (open24 == "1")
            {
                schDaypilot.BusinessBeginsHour = 0;
                schDaypilot.BusinessEndsHour = 24;

                schDaypilotweek.BusinessBeginsHour = 0;
                schDaypilotweek.BusinessEndsHour = 24;
            }
            else
            {
                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                    startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                    startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                    startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                    endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                    endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                    endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("SetOfficeHours: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    #region Get Data Table

    private DataTable GetDataTable()
    {
        DataTable dt = null;
        try
        {
            dt = new DataTable();

            if (!dt.Columns.Contains("start")) dt.Columns.Add("start");
            if (!dt.Columns.Contains("end")) dt.Columns.Add("end");
            if (!dt.Columns.Contains("formatstart")) dt.Columns.Add("formatstart");
            if (!dt.Columns.Contains("formatend")) dt.Columns.Add("formatend");
            if (!dt.Columns.Contains("confDetails")) dt.Columns.Add("confDetails");
            if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
            if (!dt.Columns.Contains("ConfID")) dt.Columns.Add("ConfID");
            if (!dt.Columns.Contains("ConferenceType")) dt.Columns.Add("ConferenceType");
            if (!dt.Columns.Contains("RoomID")) dt.Columns.Add("RoomID");
            if (!dt.Columns.Contains("confName")) dt.Columns.Add("confName");
            if (!dt.Columns.Contains("durationMin")) dt.Columns.Add("durationMin");
            if (!dt.Columns.Contains("CustomDescription")) dt.Columns.Add("CustomDescription");
            if (!dt.Columns.Contains("deleted")) dt.Columns.Add("deleted");
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dt;
    }

    #endregion

    #region GetMonthlyThread
    public string GetMonthlyThread()
    {
        String inXML = "";
        String outXmlMn = "";
        try
        {
            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";
            outXmlMn = obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            outXmlMn = outXmlMn.Replace("\t", "").Replace("\n", "").Replace("\r", "");

            return outXmlMn;
        }
        catch (Exception ex)
        {
            return "";
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion

    #region Datatable from XML

    protected void DatatablefromXML(String xmls, int addtoSession)
    {
        String hdr = ("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM";
        Int32 hrs, mins = 0, cnt = 1;


        String locStr = "";
        String setupTxt = "";
        String trdnTxt = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        XmlDocument xmldoc = null;
        DataTable dt = null;
        StringBuilder m = null;
        XmlNodeList subnotes2 = null;
        XmlNode subnode2 = null;
        try
        {
            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                hdr = "Hearing Details";

            String spn = "<span  class=\"eventtext\">";

            if (xmls != "")
            {
                bypass = false;

                xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmls);

                dt = GetDataTable();

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                    open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                if (open24 == "1")
                {
                    schDaypilot.BusinessBeginsHour = 0;
                    schDaypilot.BusinessEndsHour = 24;

                    schDaypilotweek.BusinessBeginsHour = 0;
                    schDaypilotweek.BusinessEndsHour = 24;
                }
                else
                {
                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                        startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                        startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                        startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                        endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                        endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                        endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                    schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                    if (endMin != "00")
                        schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                    schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                    if (endMin != "00")
                        schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
                }

                nodes = xmldoc.SelectNodes("//days/day/conferences/conference");

                for (int confnodes = 0; confnodes < nodes.Count; confnodes++)
                {
                    node = nodes[confnodes];
                    String tempFor = "", confSTime = "";
                    String setupSTime = "", teardownSTime = "", confSDate = "", uniqueID = ""; 

                    if (node.SelectSingleNode("durationMin") != null)
                    {
                        if (node.SelectSingleNode("durationMin").InnerText != "")
                        {
                            //if (node.SelectSingleNode("isImmediate").InnerText == "0" && node.SelectSingleNode("isFuture").InnerText == "0" && node.SelectSingleNode("isPublic").InnerText == "0" && node.SelectSingleNode("isPending").InnerText == "0" && node.SelectSingleNode("isApproval").InnerText == "0")
                            //    bypass = true;

                            //if (bypass || (node.SelectSingleNode("isImmediate").InnerText == isOngoing || node.SelectSingleNode("isFuture").InnerText == isFuture || node.SelectSingleNode("isPublic").InnerText == isPublic || node.SelectSingleNode("isPending").InnerText == isPending || node.SelectSingleNode("isApproval").InnerText == isApproval))
                            //{
                                DataRow dr = dt.NewRow();

                                if (node.SelectSingleNode("confName") != null)
                                    dr["confName"] = node.SelectSingleNode("confName").InnerText;

                                if (Session["isVIP"] != null)
                                {
                                    if (Session["isVIP"].ToString() == "1")
                                    {
                                        if (node.SelectSingleNode("isVIP") != null) 
                                            if (node.SelectSingleNode("isVIP").InnerText == "1")
                                                dr["confName"] = dr["confName"].ToString() + "<B> {VIP} </B>";
                                    }
                                }

                                dr["durationMin"] = node.SelectSingleNode("durationMin").InnerText;
                                hrs = Convert.ToInt32(dr["durationMin"].ToString()) / 60;
                                mins = Convert.ToInt32(dr["durationMin"].ToString()) % 60;
                                if (node.SelectSingleNode("confID") != null)
                                    dr["ConfID"] = node.SelectSingleNode("confID").InnerText;
                                if (node.SelectSingleNode("ConferenceType") != null)
                                    dr["ConferenceType"] = node.SelectSingleNode("ConferenceType").InnerText;

                                dr["ID"] = dr["ConfID"].ToString();

                                if (node.SelectSingleNode("confTime") != null)
                                    confSTime = node.SelectSingleNode("confTime").InnerText;

                                if (node.SelectSingleNode("setupTime") != null)
                                    setupSTime = node.SelectSingleNode("setupTime").InnerText;

                                if (node.SelectSingleNode("teardownTime") != null)
                                    teardownSTime = node.SelectSingleNode("teardownTime").InnerText;

                                if (node.SelectSingleNode("confDate") != null)
                                {
                                    if (node.SelectSingleNode("confDate").InnerText != "")
                                        confSDate = node.SelectSingleNode("confDate").InnerText;
                                }
                                
                                if (node.SelectSingleNode("uniqueID") != null)
                                {
                                    if (node.SelectSingleNode("uniqueID").InnerText != "")
                                        uniqueID = node.SelectSingleNode("uniqueID").InnerText;
                                }

                                if (node.SelectSingleNode("deleted") != null)
                                {
                                    if (node.SelectSingleNode("deleted").InnerText != "")
                                        dr["deleted"] = node.SelectSingleNode("deleted").InnerText;
                                }

                            
                                int adddays = 0;
                                if (confSTime.Trim() == "00:00 AM")
                                    adddays = 1;

                                DateTime start = DateTime.Parse(confSDate + " " + confSTime);
                                DateTime stUp = DateTime.Parse(confSDate + " " + setupSTime);
                                DateTime end = start.AddMinutes(Convert.ToDouble(dr["durationMin"]));
                                DateTime trDn = DateTime.Parse(end.ToString("MM/dd/yyyy") + " " + teardownSTime);

                                start = start.AddDays(adddays);
                                stUp = stUp.AddDays(adddays);
                                end = end.AddDays(adddays);
                                trDn = trDn.AddDays(adddays);

                                dr["start"] = start;
                                dr["end"] = end;

                                dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats); ;

                                subnotes2 = node.SelectNodes("mainLocation/location");

                                if (subnotes2 != null)
                                {
                                    for (int subnodescnt = 0; subnodescnt < subnotes2.Count; subnodescnt++)
                                    {
                                        subnode2 = subnotes2[subnodescnt];
                                        locStr = locStr + subnode2.SelectSingleNode("locationName").InnerText;
                                        locStr = locStr + "<br>";
                                    }
                                }

                                trdnTxt = "";

                                if (Session["EnableEntity"] == null)
                                    Session["EnableEntity"] = "0";
                                
                                String customText = "";
                                if (Session["EnableEntity"].ToString() != "0")
                                {
                                    XmlNodeList customnodes = node.SelectNodes("CustomAttributesList/CustomAttribute");
                                    for (int custnodescnt = 0; custnodescnt < customnodes.Count; custnodescnt++)
                                    {
                                        XmlNode customnode = customnodes[custnodescnt];
                                        String attriName = "", attriValue = "";

                                        if (customnode.SelectSingleNode("IncludeInCalendar") != null)
                                            if (customnode.SelectSingleNode("IncludeInCalendar").InnerText.Trim().Equals("0"))
                                                continue;

                                        if (customnode.SelectSingleNode("Status") != null)
                                        {
                                            if (customnode.SelectSingleNode("Status").InnerText == "0")
                                            {
                                                if (customnode.SelectSingleNode("Title") != null)
                                                    attriName = customnode.SelectSingleNode("Title").InnerText;

                                                if (customnode.SelectSingleNode("Type") != null)
                                                {
                                                    if (customnode.SelectSingleNode("Type").InnerText == "3")
                                                    {
                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                        {
                                                            if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                attriValue = "Yes";
                                                            else
                                                                attriValue = "No";
                                                        }
                                                    }
                                                    else if (customnode.SelectSingleNode("Type").InnerText == "2")//FB 2377
                                                    {
                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                        {
                                                            if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                attriValue = "Yes";
                                                            else
                                                                attriValue = "No";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                            attriValue = customnode.SelectSingleNode("SelectedValue").InnerText;
                                                    }
                                                }

                                                XmlNodeList optNodes = customnode.SelectNodes("OptionList/Option");

                                                if (customnode.SelectSingleNode("Type").InnerText.Trim() == "5" || customnode.SelectSingleNode("Type").InnerText.Trim() == "6") //FB 1718
                                                {
                                                    if (optNodes != null)
                                                    {
                                                        for (int optnodescnt = 0; optnodescnt < optNodes.Count; optnodescnt++)
                                                        {
                                                            XmlNode optNode = optNodes[optnodescnt];
                                                            if (optNode.SelectSingleNode("Selected") != null)
                                                            {
                                                                if (optNode.SelectSingleNode("Selected").InnerText == "1")
                                                                {
                                                                    if (optNode.SelectSingleNode("DisplayCaption") != null)
                                                                    {
                                                                        if (attriValue == "")
                                                                            attriValue = optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                        else
                                                                            attriValue += "," + optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (attriValue == "")
                                                attriValue = "N/A";

                                            if(attriName.ToUpper() == "SPECIAL INSTRUCTIONS")
                                                customText += attriName + " - " + attriValue + "<br>";
                                        }
                                    }
                                }
                                
                                m = new StringBuilder(); ;

                                m.Append("<table cellspacing='0' cellpadding='0' border='0' width='310px' class='promptbox' >");
                                m.Append("<tr valign='middle'>");
                                m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2'>");
                                m.Append(hdr);
                                m.Append("</td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold;'>" + ("Name") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + dr["confName"].ToString() + "</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold;'> " + ("Unique ID") + ": </style></td>"); // FB 2002
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + uniqueID.ToString() + "</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + ("Start - End") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + ("Duration") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold;'>" + ("Location") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + ((locStr == "") ? "N/A" : locStr) + "</style></td>");
                                m.Append("</tr>");
                                if (dr["Deleted"].ToString() == "1")
                                {
                                    m.Append("<tr>");
                                    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold;'>" + ("Status") + ": </style></td>");
                                    m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>Deleted</style></td>");
                                    m.Append("</tr>");
                                }
                                if (Session["EnableEntity"].ToString() != "0")
                                {   
                                    m.Append("<tr>");
                                    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold;'>" + ("Custom Options") + ": </style></td>");
                                    m.Append("<td><span style='font-size: 8pt; font-weight: bold;'>" + ((customText.IndexOf("N/A") > 0) ? "N/A" : customText) + "</style></td>");
                                     
                                    m.Append("</tr>");
                                }
                                m.Append("</table>");

                                dr["confDetails"] = spn + dr["confName"].ToString() + " - (UID : " + uniqueID + " )<br>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)<br>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</span><br>" + spn + setupTxt + trdnTxt + "Locations:<br>" + locStr + "</span><br>"; 
                                dr["CustomDescription"] = m.ToString();

                                dt.Rows.Add(dr);
                                locStr = "";
                            //}
                        }
                    }
                }

                if (addtoSession > 0)
                {
                    if (Session["PersonalCalendar"] != null)
                        Session.Add("PersonalCalendar", dt);
                    else
                        Session["PersonalCalendar"] = dt;
                }
                else
                {
                    if (Session["PersonalWeekly"] != null)
                        Session.Add("PersonalWeekly", dt);
                    else
                        Session["PersonalWeekly"] = dt;
                }
            }
        }
        catch (Exception ex)
        {
            //errLabel.Text = ex.StackTrace;
            //errLabel.Visible = true;
            //log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message + ":" + ex.InnerException);
        }

    }

    #endregion

    #region BubbleRenderhandler
    protected void BubbleRenderhandler(object sender, DayPilot.Web.Ui.Events.Bubble.RenderEventArgs e)
    {
        try
        {
            //DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs re = (DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs)e;
            //re.InnerHTML = re.Tag["CustomDescription"];
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion   

    #region GetWeeklyCalendar
    public string GetWeeklyCalendar()
    {
        String inXML = "";
        String outXMLWkly = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] == null) 
                Thread.Sleep(100);

            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                {
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
                    ViewState["HdnMonthlyXml"] = "";
                }
            }

            outXMLWkly = HdnMonthlyXml.Value;

            DateTime wkStartDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);

            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + wkStartDate.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";
            outXMLWkly = obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString());

            return outXMLWkly;
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetWeeklyCalendar: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();
        }
    }

    #endregion

    #region PreviousCal

    public void PreviousCal(Object sender, EventArgs e)
    {
        try
        {
            if (hdnView.Value == "1")
            {
                conf = Convert.ToDateTime(hdnDayDate.Value).AddDays(-1);
                CalHeaderTextDay.Attributes.Add("Style", "Display:block");
                hdnDayDate.Value = conf.ToShortDateString();
                //CalHeaderTextDay.Text = conf.ToShortDateString();
            }
            else if (hdnView.Value == "2")
            {
                conf = Convert.ToDateTime(hdnWeekDate.Value).AddDays(-7);
                CalHeaderTextWeek.Attributes.Add("Style", "Display:block");
                hdnWeekDate.Value = conf.ToString();
                //DateTime wkDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);
                //CalHeaderTextWeek.Text = wkDate.ToString("dd MMM") + " - " + wkDate.AddDays(7).ToString("dd MMM")
                //   + " " + wkDate.Year;
            }
            else if (hdnView.Value == "3")
            {
                conf = Convert.ToDateTime(hdnMonthDate.Value).AddMonths(-1);
                CalHeaderTextMonth.Attributes.Add("Style", "Display:block");
                hdnMonthDate.Value = conf.ToString();
                //CalHeaderTextMonth.Text = conf.ToString("MMM") + " - " + conf.Year;
            }
            ChangeCalendarDate(null, null);
            uplDetail.Update();
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }

    }

    #endregion

    #region NextCal

    public void NextCal(Object sender, EventArgs e)
    {
        try
        {
            WeekScheduleRow.Attributes.Add("style", "display:none");
            DayScheduleRow.Attributes.Add("style", "display:none");
            MonthScheduleRow.Attributes.Add("style", "display:none");

            if (hdnView.Value == "1")
            {
                conf = Convert.ToDateTime(hdnDayDate.Value).AddDays(1);
                DayScheduleRow.Attributes.Add("style", "display:block");
                CalHeaderTextDay.Attributes.Add("Style", "Display:block");
                hdnDayDate.Value = conf.ToShortDateString();
                CalHeaderTextDay.Text = conf.ToShortDateString();
            }
            else if (hdnView.Value == "2")
            {
                conf = Convert.ToDateTime(hdnWeekDate.Value).AddDays(7);
                WeekScheduleRow.Attributes.Add("style", "display:block");
                CalHeaderTextWeek.Attributes.Add("Style", "Display:block");
                hdnWeekDate.Value = conf.ToString();

                DateTime wkDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);
                CalHeaderTextWeek.Text = wkDate.ToString("dd MMM") + " - " + wkDate.AddDays(6).ToString("dd MMM")
                   + " " + wkDate.Year;
            }
            else if (hdnView.Value == "3")
            {
                conf = Convert.ToDateTime(hdnMonthDate.Value).AddMonths(1);
                MonthScheduleRow.Attributes.Add("style", "display:block");
                CalHeaderTextMonth.Attributes.Add("Style", "Display:block");
                hdnMonthDate.Value = conf.ToString();
                CalHeaderTextMonth.Text = conf.ToString("MMM") + " - " + conf.Year;
            }

            ChangeCalendarDate(null, null);
            uplDetail.Update();
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }

    #endregion

    #region GetMonthlyCalendar
    public string GetMonthlyCalendar()
    {
        String inXML = "";
        String outXmlMn = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] == null)
                Thread.Sleep(100);


            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";

            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
            }

            outXmlMn = HdnMonthlyXml.Value;

            outXmlMn = obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            HdnMonthlyXml.Value = outXmlMn;
            ViewState["HdnMonthlyXml"] = outXmlMn;

            return outXmlMn;
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetMonthlyCalendar: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();
        }
    }

    #endregion
}
