﻿<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="conference.aspx.cs" Inherits="Droid_conference" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<%
    if(Session["userID"] == null)
    {
        Response.Redirect("login.aspx");

    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>
<!DOCTYPE HTML>
<html lang="en">
<head runat="server">
<meta name="viewport" content="width=400, initial-scale=1,">
<meta charset="utf-8" /> 
<title></title>

<link type="text/css" rel="stylesheet" href="Style/jscal2.css" />
<link type="text/css" rel="stylesheet" href="Style/border-radius.css" />
<link type="text/css" rel="stylesheet" href="Style/reduce-spacing.css" />

<script type="text/javascript" src="Script/jscal2.js"></script>
<script type="text/javascript" src="Script/en.js"></script>

<style type="text/css">
.maxRound
{
	border-radius:10px;
}
.btnCorner
{
    border:1px solid #a1a1a1;
    border-radius:5px;
}

.recurDays
{
    border:1px solid #a1a1a1;
    border-radius:5px;
    background-color:white;
}

body
{
	color: #333333;
	font-family:Arial;
	font-size:medium;
}
.xajax__calendar_container
{
    border:1px solid #646464;
}

.textCorner
{
	border-radius:5px;
	box-shadow: 2px 2px 5px #555;
}

select 
{
	border-radius:5px;
	box-shadow: 2px 2px 5px #555;
	border-color:Gray;
        	
}

.headerGradient
{
	text-shadow: 0 -1px 1px #ffffff;
    background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#005599));

}

.roundTd
{
	border-radius:10px;
	background-color:#eeeeee;
	padding: 10px 0px 10px 5px;
}

.txtbox
{
    border-style:hidden;
    border-color:#ffffff;
}

.recurOptions
{
	cursor:pointer;
	background-color:#ddd;
    border-radius:5px;

}

.calButton
{
	background-color:#555;
	color:White;
}
</style>

<script type="text/javascript">

var globalChange = true;

function fnSetGlobalValue(val)
{
globalChange = val;
}

function fnLogout()
{
    window.location.assign('login.aspx?m=1');
    return true;
}

function fnMoveToConf()
{
    window.location.assign('calendar.aspx');
    return true;
}

function fnHideDiv()
{

    document.getElementById("contx").style.display="none";
    document.getElementById("tab1").style.display="none";
    document.getElementById("tab2").style.display="none";
    document.getElementById("tab3").style.display="none";
    document.getElementById("tab4").style.display="none";
    document.getElementById("tab5").style.display="none";

}


function fnShowDiv(tab)
{

    if(tab==1)
    {
        document.getElementById("contx").style.display="none";
        document.getElementById("tab1").style.display="block";
        return false;
    }

    var confTit = document.getElementById("confTitle").value;
    if(confTit == "")
    {
        document.getElementById("title1").style.color = "Red";
        document.getElementById("titId").style.color = "Red";
        
        return false;
    }

    fnHideDiv();
    if(tab == 2)
        document.getElementById("tab2").style.display="block";
    else if(tab == 3)
    {
        document.getElementById("tab3").style.display="block";
        if(globalChange == true)
        {
            document.getElementById("tempBut").click();
            globalChange = false;
        }
    }
    else if(tab == 4)
        document.getElementById("tab4").style.display="block";
    else if(tab == 5)
    {
        fnUpdateReview();
        document.getElementById("tab5").style.display="block";
    }

}


function fnRecurToggle()
{
    var btnId = document.getElementById("recurSwitch");
    if(btnId.value == "OFF")
    {
        btnId.value = "ON";
        document.getElementById("recurStatus").value = "Yes";
        btnId.style.borderColor = "Green";
        document.getElementById("recurOff").style.display="none";
        document.getElementById("recurOn").style.display="block";
    }
    else
    {
        btnId.value = "OFF";
        document.getElementById("recurStatus").value = "No";
        btnId.style.borderColor = "Red";
        document.getElementById("recurOn").style.display="none";
        document.getElementById("recurOff").style.display="block";
    }
}


function fnConfDisc(status)
{
    var conDis = document.getElementById("confDisc");
    if(status == '1')
    {
        if (conDis.value.trim() == "Conference Description") 
            conDis.value = "";
    }
    else
    {
        if (conDis.value.trim() == "") 
            conDis.value = "Conference Description";
    }
}


function fnSplInst(status)
{
    var splIns = document.getElementById("splInst");
    if(status == '1')
    {
        if (splIns.value.trim() == "Type instruction here.") 
            splIns.value = "";
    }
    else
    {
        if (splIns.value.trim() == "") 
            splIns.value = "Type instruction here.";
    }
}

function fnMoveBack(idx)
{
    if(fnValidateRecurrance() == false)
        return false;
    if(document.getElementById("drpTimeZone").selectedIndex == 0)
    {
        alert('Select Time Zone');
        return false;
    }
    fnFormRecurText();
    var confTit = document.getElementById("confTitle").value;
    if(idx == "tab1" && confTit != "")
    {
        setColor('Black');
    }
    else if(idx == "tab1" && confTit == "")
    {
        setColor('Red');
    }
    document.getElementById(idx).style.display="none";
    document.getElementById("contx").style.display="block";
}

function setColor(clr)
{
    document.getElementById("title1").style.color = clr;
    document.getElementById("titId").style.color = clr;
}

function fnValidateDate()
{
    globalChange = true;
    
    var startDate = document.getElementById("txtConfStart");
    var endDate = document.getElementById("txtConfEnd");
    
    var drpObj1 = document.getElementById("drpStartTime");
    var selec1 = drpObj1.options[drpObj1.selectedIndex].text;
    
    var drpObj2 = document.getElementById("drpEndTime");
    var selec2 = drpObj2.options[drpObj2.selectedIndex].text;
    
    var d1 = new Date(startDate.value);
    
    var d2 = new Date(endDate.value);
    
    if(d1 > d2)
    {
        alert("End date should be greater than or equal to start date");
        endDate.value = startDate.value;
    }
    fnUpdateHdnEndDate();
    return false;

}

function fnPrevNext(hide, show)
{
//fnValidateDate();
    if(hide=="tab1" && show=="tab2")
    {
        if(fnValidateRecurrance() == false)
        return false;
        fnFormRecurText();
        var confTit = document.getElementById("confTitle").value;
        if(confTit != "")
        {
        //document.getElementById("titId").style.color = "Black";
        setColor('Black');
        }
        else
        {
        //document.getElementById("titId").style.color = "Red";
        setColor('Red');
        return false;
        }
        if(document.getElementById("drpTimeZone").selectedIndex == 0)
        {
            alert('Select Time Zone');
            return false;
        }
        
    }
    
    if(hide=="tab4" && show=="tab5")
        fnUpdateReview();
    
    document.getElementById(hide).style.display="none";
    document.getElementById(show).style.display="block";

    if( (hide=="tab2" && show=="tab3") || (hide=="tab4" && show=="tab3"))
    {
        if(globalChange == true)
        {
            document.getElementById("tempBut").click();
            globalChange = false;
        }
    }


}

function fnChangeColor()
{
    var confTit = document.getElementById("confTitle").value;
    if(confTit != "")
    {
        setColor('Black');
    }
    else
    {
        setColor('Red');
    }
}


function fnLoadOptions(drpId)
{
    var obj = document.getElementById(drpId);
    var hour = 1;
    var min = 0;
    
    var h = "";
    var m = "";

    for(i=0; i<96; i++)
    {
        var option=document.createElement("option");
    
        if(hour < 10)
            h = "0"+hour;
        else
            h = hour;
        
        if(min == 0)
            m = "00";
        else
            m = min;
        
        if(i < 44 || i > 91)
            option.text= h + ":" + m + " AM";
        else
            option.text= h + ":" + m + " PM";
    
        min = min + 15;
    
        if(min>45)
        {
            hour = hour + 1;
            min = 0;
        }
    
        if(hour>12)
        hour = 1;
        
        obj.add(option,null);
    }

return false;
}

function fnCheckPastDate(tochk)
{
    var chkDate="";
    
    if(tochk == 1)
        chkDate = document.getElementById("txtConfStart");
    else
        chkDate = document.getElementById("recConfStart");
    
    var curDate = document.getElementById("curDate").value;

    var d1 = new Date(curDate);
    
    var d2 = new Date(chkDate.value);
    
    if(d1 > d2)
    {
        alert("Start date should be greater than or equal to current date");
        chkDate.value = curDate;
        return false;
    }
    else
        return true
    
}

function fnConfirmReset()
{
if(confirm("Reset all fields?"))
document.getElementById("confForm").reset();

document.getElementById('hdnGuestUsers').value = "";
userDetails = "";
//document.getElementById('myTable').deleteRow(i);
var tableObj = document.getElementById('tblGuestUsers');
for(var i=0; i<tableObj.rows.length; i++)
tableObj.deleteRow(i);
fnUpdateStartDate();
}

function fnUpdateDate()
{
var mon = new Array('0','31','28','31','30','31','30','31','31','30','31','30','31');
var curMonth = parseInt(document.getElementById("custMon").value,10);

days = parseInt(mon[curMonth],10);
    if(curMonth == 2)
    {
        if(isleap())
        {
            days += 1;
            //alert(days);
        }
    }
}

var days;
function fnIncVal(obj)
{

var cont;
var n;
    if(obj == 'custMon')
    {
        cont = document.getElementById(obj);
        n = parseInt(cont.value,10) + 1;
        if(n == 13)
        n = 12;
        //cont.value = n;
    }
    if(obj == 'custDate')
    {
        fnUpdateDate();
        cont = document.getElementById(obj);
        n = parseInt(cont.value,10) + 1;
        if(n > days)
        n = days;
        //cont.value = n;
        
    }
    if(obj == 'custYear')
    {
        cont = document.getElementById(obj);
        n = parseInt(cont.value,10) + 1;
        if(n == 10000)
        n = 9999;
        //cont.value = n;
    }
    if(n < 10)
        n = '0' + n;
    cont.value = n;

fnUpdateDate();
var dtObj = document.getElementById("custDate");
if(parseInt(dtObj.value,10) > days)
dtObj.value = days;
}

function fnDecVal(obj)
{

var cont;
var n;

    if(obj == 'custMon')
    {
        cont = document.getElementById(obj);
        n = parseInt(cont.value,10) - 1;
        if(n == 0)
        n = 1;
        //cont.value = n;
    }
    if(obj == 'custDate')
    {
        cont = document.getElementById(obj);
        n = parseInt(cont.value,10) - 1;
        if(n == 0)
        n = 1;
        //cont.value = n;
    }
    if(obj == 'custYear')
    {
        cont = document.getElementById(obj);
        n = parseInt(cont.value,10) - 1;
        if(n == 1899)
        n = 1900;
        //cont.value = n;
    }
    if(n < 10)
        n = '0' + n;
    cont.value = n;

fnUpdateDate();
var dtObj = document.getElementById("custDate");
if(parseInt(dtObj.value,10) > days)
dtObj.value = days;
}

var curObj;

function fnSelectDate(par)
{
curObj = par.id;
if(par.value == "")
par.value = document.getElementById('txtConfEnd').value;
var dt = par.value.split('/');
document.getElementById("custMon").value = dt[0];
document.getElementById("custDate").value = dt[1];
document.getElementById("custYear").value = dt[2];
if(par.id == 'recConfStart')
document.getElementById("innerTab").style.marginTop = '250px';
else if(par.id == 'recConfEnd')
document.getElementById("innerTab").style.marginTop = '350px';
else
document.getElementById("innerTab").style.marginTop = '100px';
document.getElementById("custCal").style.display = "block";
}

function fnSetDate()
{
var d = document.getElementById("custMon").value;
d = d + "/" + document.getElementById("custDate").value;
d = d + "/" + document.getElementById("custYear").value;
document.getElementById(curObj).value = d;
document.getElementById("custCal").style.display = "none";

if(curObj == 'txtConfStart')
fnUpdateText(d);

if(curObj == 'txtConfEnd')
fnValidateDate();

if(curObj == 'recConfStart')
fnCheckPastDate(2);

}


function fnSetCurDate()
{
document.getElementById(curObj).value = document.getElementById("curDate").value;
document.getElementById("custCal").style.display = "none";
}


function isleap()
{
 var yr=document.getElementById("custYear").value;
 if ((parseInt(yr)%4) == 0)
 {
  if (parseInt(yr)%100 == 0)
  {
    if (parseInt(yr)%400 != 0)
    {
    //alert("Not Leap");
    return false;
    }
    if (parseInt(yr)%400 == 0)
    {
    //alert("Leap");
    return true;
    }
  }
  if (parseInt(yr)%100 != 0)
  {
    //alert("Leap");
    return true;
  }
 }
 if ((parseInt(yr)%4) != 0)
 {
    //alert("Not Leap");
    return false;
 } 
}

</script>

</head>

<body>
<form id="confForm" runat="server">
<div id="custCal" style="display:none; width:480px; height:640px; position:fixed; background-color:Transparent; z-index:1000;">
<table id="innerTab" style="background-color:#ece9d8; margin-left:50px; margin-top:100px; padding:5px 5px 5px 5px; text-align:center" class="btnCorner" >

<tr>
<td colspan="3">
<input style="width:105px;" type="button" value="Today" class="btnCorner" onclick="javascript:fnSetCurDate();" />
</td>
</tr>
<tr>
<td><input class="calButton" type="button" value="+" style="width:30px" onclick="javascript:fnIncVal('custMon')" /></td>
<td><input class="calButton" type="button" value="+" style="width:30px" onclick="javascript:fnIncVal('custDate')" /></td>
<td><input class="calButton" type="button" value="+" style="width:35px" onclick="javascript:fnIncVal('custYear')" /></td>
</tr>
<tr>
<td><input id="custMon" type="text" value="01" style="width:30px; text-align:center" readonly="readonly" /></td>
<td><input id="custDate" type="text" value="01" style="width:30px; text-align:center" readonly="readonly" /></td>
<td><input id="custYear" type="text" value="2000" style="width:35px; text-align:center" readonly="readonly" /></td>
</tr>
<tr>
<td><input class="calButton" type="button" value="-" style="width:30px" onclick="javascript:fnDecVal('custMon')" /></td>
<td><input class="calButton" type="button" value="-" style="width:30px" onclick="javascript:fnDecVal('custDate')" /></td>
<td><input class="calButton" type="button" value="-" style="width:35px" onclick="javascript:fnDecVal('custYear')" /></td>
</tr>
<tr>
<td style="text-align:center; color:Gray; font-size:10px">MM</td>
<td style="text-align:center; color:Gray; font-size:10px">DD</td>
<td style="text-align:center; color:Gray; font-size:10px">YYYY</td>
</tr>
<tr>
<td colspan="3">
<input type="button" style="width:50px" value="Set" class="btnCorner" onclick="javascript:fnSetDate();" />
<input type="button" value="Cancel" class="btnCorner" onclick="javascript:document.getElementById('custCal').style.display = 'none';" />
</td>
</tr>
</table>
</div>
<input type="hidden" id="hdnEndTime" runat="server" value="" />
<input type="hidden" id="hdnStartTime" runat="server" value="" />
<input type="hidden" id="hdnGuestUsers" runat="server" value="" />
<input type="hidden" id="hdnRecurStart" runat="server" value="" />

<input type="hidden" id="recurStatus" runat="server" value="No" />
<input type="hidden" id="recurType" runat="server" value="divDaily" />
<input type="hidden" id="recurRange" runat="server" value="1" />

<input type="hidden" id="customDates" runat="server" value="" />
<input type="hidden" id="curDate" runat="server" value="" />
<input type="hidden" id="hdnCustStartDate" runat="server" value="" />
<input type="hidden" id="hdnRecurText" runat="server" value="" />

<div id="contx" style="display:block">
<table id="mainMenu" style="border-collapse:separate">
<tr style="background-color:#cccccc">
<td class="headerGradient">Create Conference
<input type="button" class="btnCorner" value="View Conference" onclick="fnMoveToConf();" />
<input type="button" class="btnCorner" value="Log out" onclick="fnLogout();" /></td>
</tr>
<tr>
<td id="title1" colspan="3" align="left" onclick="javascript:return fnShowDiv(1)" style="cursor: pointer;" class="roundTd" >1. Schedule a Video Conference</td>
</tr>
<tr>
<td colspan="3" align="left" onclick="javascript:return fnShowDiv(2)" style="cursor: pointer;" class="roundTd" >2. Invite Participants</td>
</tr>
<tr>
<td colspan="3" align="left" onclick="javascript:return fnShowDiv(3)" style="cursor: pointer;" class="roundTd" >3. Select Rooms</td>
</tr>
<tr>
<td colspan="3" align="left" onclick="javascript:return fnShowDiv(4)" style="cursor: pointer;" class="roundTd" >4. Special Instructions</td>
</tr>
<tr>
<td colspan="3" align="left" onclick="javascript:return fnShowDiv(5)" style="cursor: pointer;" class="roundTd" >5. Review</td>
</tr>
</table>
<asp:Button ID="submitConf" runat="server" Visible="false" Text="Create Conf" OnClick="setConf" />
</div>


<div id="tab1" style="display:none">
<table style="border-collapse:collapse">

<tr style="background-color:#cccccc">
<td colspan="2" class="headerGradient"><input type="button" value="Back" class="btnCorner" onclick="javascript:return fnMoveBack('tab1')" />
1. Schedule Video Conference</td>
</tr>

<tr>
<td id="titId"><br />Conference Title</td>
<td><br /><input id="confTitle" type="text" onkeypress="return (event.keyCode!=13)" onblur="javascript:return fnChangeColor()" class="textCorner" runat="server" style="width:185px" /></td>
</tr>

<tr>
<td colspan="2">
<textarea id="confDisc" runat="server" class="textCorner" rows="3" cols="36" style="resize:none; border-color:Gray" onfocus="javascript:return fnConfDisc('1')" onblur="javascript:return fnConfDisc('0')" >Conference Description</textarea>
</td>
</tr>

<tr>
<td>Requestor Name</td>
<td>
<asp:TextBox ID="txtReqName" class="textCorner" Width="185px" runat="server" ReadOnly="false"></asp:TextBox>
</td>
</tr>

<tr>
<td>Time Zone</td>
<td>



<select id="drpTimeZone" runat="server" style="width:190px" onchange="fnSetGlobalValue(true);" >
		<option value="-1">Please select...</option>
		<option value="21">(GMT-12:00) Eniwetok</option>
		<option value="57">(GMT-11:00) Samoa</option>
		<option value="35">(GMT-10:00) Hawaii</option>
		<option value="2">(GMT-09:00) Alaska</option>
		<option value="51">(GMT-08:00) Pacific Time</option>
		<option value="67">(GMT-07:00) Mountain - Arizona</option>
		<option value="42">(GMT-07:00) Mountain Time</option>
		<option value="40">(GMT-06:00) Mexico</option>
		<option value="10">(GMT-06:00) Saskatchewan</option>
		<option value="14">(GMT-06:00) Central</option>
		<option value="19">(GMT-06:00) Central Time</option>
		<option value="55">(GMT-05:00) Bogota</option>
		<option value="66">(GMT-05:00) Indiana</option>
		<option selected="selected" value="26">(GMT-05:00) Eastern Time </option>
		<option value="56">(GMT-04:00) Caracas</option>
		<option value="50">(GMT-04:00) Santiago</option>
		<option value="6">(GMT-04:00) Atlantic</option>
		<option value="47">(GMT-03:30) Newfoundland</option>
		<option value="54">(GMT-03:00) Buenos</option>
		<option value="25">(GMT-03:00) Brasilia</option>
		<option value="32">(GMT-03:00) Greenland</option>
		<option value="41">(GMT-02:00) Mid-Atlantic</option>
		<option value="9">(GMT-01:00) Azores</option>
		<option value="11">(GMT-01:00) Cape</option>
		<option value="33">(GMT) Casablanca</option>
		<option value="31">(GMT) Dublin</option>
		<option value="16">(GMT+01:00) Belgrade</option>
		<option value="17">(GMT+01:00) Sarajevo</option>
		<option value="52">(GMT+01:00) Brussels</option>
		<option value="70">(GMT+01:00) West</option>
		<option value="71">(GMT+01:00) Amsterdam</option>
		<option value="75">(GMT+01:00) Berlin</option>
		<option value="60">(GMT+02:00) Pretoria</option>
		<option value="38">(GMT+02:00) Jerusalem</option>
		<option value="34">(GMT+02:00) Athens</option>
		<option value="30">(GMT+02:00) Helsinki</option>
		<option value="24">(GMT+02:00) Bucharest</option>
		<option value="27">(GMT+02:00) Cairo</option>
		<option value="22">(GMT+03:00) Nairobi</option>
		<option value="3">(GMT+03:00) Kuwai</option>
		<option value="5">(GMT+03:00) Baghdad</option>
		<option value="53">(GMT+03:00) Moscow</option>
		<option value="37">(GMT+03:30) Tehran</option>
		<option value="4">(GMT+04:00) Abu</option>
		<option value="12">(GMT+04:00) Baku</option>
		<option value="1">(GMT+04:30) Kabul</option>
		<option value="28">(GMT+05:00) Ekaterinburg</option>
		<option value="72">(GMT+05:00) Karachi</option>
		<option value="36">(GMT+05:30) New Delhi</option>
		<option value="45">(GMT+05:45) Kathmandu</option>
		<option value="44">(GMT+06:00) Almaty</option>
		<option value="61">(GMT+06:00) Sri</option>
		<option value="15">(GMT+06:00) Astana</option>
		<option value="43">(GMT+06:30) Rangoon</option>
		<option value="49">(GMT+07:00) Krasnoyarsk</option>
		<option value="58">(GMT+07:00) Jakarta</option>
		<option value="59">(GMT+08:00) Singapore</option>
		<option value="62">(GMT+08:00) Taipei</option>
		<option value="48">(GMT+08:00) Irkutsk</option>
		<option value="20">(GMT+08:00) Beijing</option>
		<option value="69">(GMT+08:00) Perth</option>
		<option value="74">(GMT+09:00) Yakutsk</option>
		<option value="39">(GMT+09:00) Seoul</option>
		<option value="64">(GMT+09:00) Tokyo</option>
		<option value="7">(GMT+09:30) Darwin</option>
		<option value="13">(GMT+09:30) Adelaide</option>
		<option value="8">(GMT+10:00) Sydney</option>
		<option value="23">(GMT+10:00) Brisbane</option>
		<option value="68">(GMT+10:00) Vladivostok</option>
		<option value="73">(GMT+10:00) Guam</option>
		<option value="63">(GMT+10:00) Hobart</option>
		<option value="18">(GMT+11:00) Magadan</option>
		<option value="29">(GMT+12:00) Fiji</option>
		<option value="46">(GMT+12:00) Auckland</option>
		<option value="65">(GMT+13:00) Nuku alofa</option>
</select>




</td>
</tr>

<tr id="recurTr" runat="server">
<td>Recurrence</td>
<td align="left"><input id="recurSwitch" runat="server" type="button" class="btnCorner" value="OFF" style="width:50px; border-color:Red" onclick="javascript:return fnRecurToggle();" /></td>
</tr>

<tr><td colspan="2"><div id="recurOff" style="display:block">


<table>
<tr>
<td>Conference Start</td>
<td>
<asp:TextBox ID="txtConfStart" CssClass="textCorner" runat="server" Width="85px" onclick="javascript:fnSelectDate(this)" onchange="javascript:return fnUpdateText(this.value)" ></asp:TextBox>
@
<select id="drpStartTime" runat="server" onchange="javascript:return fnUpdateHdnStartDate()" >
</select>
</td>
</tr>



<tr>
<td>Conference End</td>
<td>
<asp:TextBox ID="txtConfEnd" CssClass="textCorner" runat="server" Width="85px" onclick="javascript:fnSelectDate(this)" onchange="javascript:return fnValidateDate()" ></asp:TextBox>
@
<select id="drpEndTime" runat="server" onchange="javascript:return fnUpdateHdnEndDate()" >
</select>
</td>
</tr>
</table>


</div></td></tr>

<!-- Recurrence Start -->

<tr><td colspan="2"><div id="recurOn" style="display:none">

<table>

<tr>
<td>Conference Duration: <input id="txtHrs" class="textCorner" type="text" runat="server" value="1" style="width:30px" onblur="javascript:fnCheckDuration()" /> hrs <input id="txtMins" class="textCorner" type="text" runat="server" value="0" style="width:30px" onblur="javascript:fnCheckDuration()" /> mins</td>
<td></td>
</tr>




<tr>
<td colspan="2">

<table width="300px">
<tr>
<td id="recurOpt1" class="recurOptions" onclick="javascript:return fnShowRecur('divDaily',this.id)" style="background-color:#bbb">&nbsp;Daily</td>
<td id="recurOpt2" class="recurOptions" onclick="javascript:return fnShowRecur('divWeekly',this.id)">&nbsp;Weekly</td>
<td id="recurOpt3" class="recurOptions" onclick="javascript:return fnShowRecur('divMonthly',this.id)">&nbsp;Monthly</td>
<td id="recurOpt4" class="recurOptions" onclick="javascript:return fnShowRecur('divYearly',this.id)">&nbsp;Yearly</td>
<td id="recurOpt5" class="recurOptions" onclick="javascript:return fnShowRecur('divCustom',this.id)">&nbsp;Custom</td>
</tr>
<tr>
<td colspan="5">





<div id="divDaily" style="display:block">
<table>
<tr><td>
Recurring Pattern:
</td></tr>
<tr><td id="tdEveryWeekOff" >
<input id="dailyType1" type="radio" name="dailyGroup" runat="server" checked="true" />
Every <input id="txtDailyType" runat="server" type="text" style="width:25px" class="textCorner" /> day(s)
</td></tr>
<tr><td>
<input id="dailyType2" type="radio" name="dailyGroup" runat="server" />
Every weekday
<!--: <input id="everyWeekday" type="button" class="btnCorner" value="ON" onclick="javascript:return fnEveryWeekday()" />-->
</td></tr>
</table>
</div>





<div id="divWeekly" style="display:none">
<table>
<tr><td>
Recurring Pattern:
</td></tr>
<tr><td>
Recur every <input id="txtWkGap" runat="server" value="1" type="text" style="width:25px" class="textCorner" /> week(s)
</td></tr>
<tr><td>
Days to recur:<br />
<input type="checkbox" id="chkSunId" value="1" runat="server" />Sun
<input type="checkbox" id="chkMonId" value="2" runat="server" />Mon
<input type="checkbox" id="chkTueId" value="3" runat="server" />Tue
<input type="checkbox" id="chkWedId" value="4" runat="server" />Wed<br />
<input type="checkbox" id="chkThuId" value="5" runat="server" />Thu
<input type="checkbox" id="chkFriId" value="6" runat="server" />Fri
<input type="checkbox" id="chkSatId" value="7" runat="server" />Sat
<!--
<input id="monId" type="button" value="MON" class="recurDays" onclick="javascript:return fnToggleDays(this.id)" />
<input id="tueId" type="button" value="TUE" class="recurDays" onclick="javascript:return fnToggleDays(this.id)" />
<input id="wedId" type="button" value="WED" class="recurDays" onclick="javascript:return fnToggleDays(this.id)" />
<input id="thuId" type="button" value="THU" class="recurDays" onclick="javascript:return fnToggleDays(this.id)" />
<input id="friId" type="button" value="FRI" class="recurDays" onclick="javascript:return fnToggleDays(this.id)" /><br />
<input id="satId" type="button" value="SAT" class="recurDays" onclick="javascript:return fnToggleDays(this.id)" />
<input id="sunId" type="button" value="SUN" class="recurDays" onclick="javascript:return fnToggleDays(this.id)" />
-->
</td></tr>

</table>
</div>


<div id="divMonthly" style="display:none">
<table>
<tr><td>
Recurring Pattern:
</td></tr>
<tr><td>
<input id="monthlyType1" type="radio" name="monthlyGroup" checked="true" runat="server" /> 
Day 
<select id="drpSelDay1" runat="server" style="width:50px" onchange="javascript:fnCheckMonthDays('drpSelMon1','drpSelDay1')" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
		<option value="31">31</option>
</select>
 of every
<select id="drpSelMon1" runat="server" style="width:50px" onchange="javascript:fnCheckMonthDays('drpSelMon1','drpSelDay1')" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
</select>
 month(s)
</td></tr>
<tr><td>
<input id="monthlyType2" type="radio" name="monthlyGroup" runat="server" /> 
The 
<select id="drpSelPos1" runat="server" style="width:40px" >
		<option value="1">first</option>
		<option value="2">second</option>
		<option value="3">third</option>
		<option value="4">fourth</option>
		<option value="5">last</option>
</select>
<select id="drpSelWeek1" runat="server" style="width:40px" >
		<option value="1">day</option>
		<option value="2">weekday</option>
		<option value="3">weekend</option>
		<option value="4">Sunday</option>
		<option value="5">Monday</option>
		<option value="6">Tuesday</option>
		<option value="7">Wednesday</option>
		<option value="8">Thursday</option>
		<option value="9">Friday</option>
		<option value="10">Saturday</option>
</select>
 of every
<select id="drpSelMonth1" runat="server" style="width:40px" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
</select>
 month(s)
</td></tr>
</table>
</div>



<div id="divYearly" style="display:none">
<table>
<tr><td>
Recurring Pattern:<br />
<input id="yearlyType1" type="radio" name="yearlyGroup" checked="true" runat="server" /> 
Every 
<select id="drpSelMon2" runat="server" style="width:50px" onchange="javascript:fnCheckMonthDays('drpSelMon2','drpSelDay2')" >
		<option value="1">January</option>
		<option value="2">February</option>
		<option value="3">March</option>
		<option value="4">April</option>
		<option value="5">May</option>
		<option value="6">June</option>
		<option value="7">July</option>
		<option value="8">August</option>
		<option value="9">September</option>
		<option value="10">October</option>
		<option value="11">November</option>
		<option value="12">December</option>
</select>
<select id="drpSelDay2" runat="server" style="width:50px" onchange="javascript:fnCheckMonthDays('drpSelMon2','drpSelDay2')" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
		<option value="31">31</option>
</select>
 month(s)
</td></tr>


<tr><td>
<input id="yearlyType2" type="radio" name="yearlyGroup" runat="server" /> 
The 
<select id="drpSelPos2" runat="server" style="width:50px" >
		<option value="1">first</option>
		<option value="2">second</option>
		<option value="3">third</option>
		<option value="4">fourth</option>
		<option value="5">last</option>
</select>
<select id="drpSelWeek2" runat="server" style="width:50px" >
		<option value="1">day</option>
		<option value="2">weekday</option>
		<option value="3">weekend</option>
		<option value="4">Sunday</option>
		<option value="5">Monday</option>
		<option value="6">Tuesday</option>
		<option value="7">Wednesday</option>
		<option value="8">Thursday</option>
		<option value="9">Friday</option>
		<option value="10">Saturday</option>
</select>
 of
<select id="drpSelMonth2" runat="server" style="width:50px" >
		<option value="1">January</option>
		<option value="2">February</option>
		<option value="3">March</option>
		<option value="4">April</option>
		<option value="5">May</option>
		<option value="6">June</option>
		<option value="7">July</option>
		<option value="8">August</option>
		<option value="9">September</option>
		<option value="10">October</option>
		<option value="11">November</option>
		<option value="12">December</option>
</select>
</td></tr>

</table>
</div>




<div id="divCustom" style="display:none">

<table>
<tr>
<td rowspan="2">



<div id="cont"></div>
          <script type="text/javascript">
            var LEFT_CAL = Calendar.setup({
                    cont: "cont",
                    weekNumbers: true,
                    selectionType: Calendar.SEL_MULTIPLE,
                    showTime: 12
                    // titleFormat: "%B %Y"
            })
          </script>
          
          </td>

          <td>
          
                <select multiple="true" id="f_selection" style="width: 100px; height: 125px" onchange="javascript:removedate(this)" runat="server" ></select>
                <script type="text/javascript">//<![CDATA[
                  LEFT_CAL.args.showTime = false;
                  LEFT_CAL.args.weekNumbers = false;
                  LEFT_CAL.fdow = 0;
                  LEFT_CAL.redraw();
                  LEFT_CAL.addEventListener("onSelect", function(){
                          var curSel = this.selection.print("%m/%d/%Y");
                          var curDt = document.getElementById("curDate").value;
                          var d1 = new Date(curSel);
                          var d2 = new Date(curDt);
                          if(d1 >= d2)
                          {
                          var custDate = document.getElementById("customDates").value;
                          if(custDate.indexOf(curSel) > -1)
                          {
                            alert("Date already added");
                            return false;
                          }
                          var ta = document.getElementById("f_selection");
                          //ta.value = this.selection.countDays() + " days selected:\n\n" + this.selection.print("%Y/%m/%d").join("\n");
                          var option = document.createElement("option");
                          option.innerHTML  = curSel;
                          //ta.add(option ,null);
                          ta.appendChild(option);
                          fnUpdateCustomDates();
                          SortDates(0);
                          }
                          else
                          {
                          alert("Custom date should be greater than or equal to current date");
                          }
                          //ta.value = ta.value + this.selection.print("%m/%d/%Y").join("\n") + '\n';
                  });

                //]]></script>
                
                </td>
                </tr>
<tr>
<td><input type="button" value="Sort" class="btnCorner" onclick="javascript:return SortDates(1)"  /></td>
</tr>          
</table>

  
    
</div>





</td>
</tr>
</table>

</td>
</tr>


<tr>
<td><div><table>

<tr>
<td>
<hr style="width:300px" />
Range of Recurrence:</td>
<td></td>
</tr>

<tr>
<td>
<div id="recurRangeDiv1" style="display:block">
Start date
<asp:TextBox ID="recConfStart" CssClass="textCorner" runat="server" Width="85px" onclick="javascript:fnSelectDate(this)" onchange="javascript:return fnCheckPastDate(2)" ></asp:TextBox>

</div>
Start time
<select id="drpRecurStart" runat="server" onchange="javascript:return fnUpdateHdnRecurStart()" >
</select>
</td>
</tr>

<tr>
<td>
<div id="recurRangeDiv2" style="display:block">
<input id="radNoEndDt" value="1" type="radio" name="recRange" checked="checked" onclick="javascript:return recRangeOptions(this)" /> No end date <br />

<input id="radEndAft" value="2" type="radio" name="recRange" onclick="javascript:return recRangeOptions(this)" /> End after <input id="txtEndAft" runat="server" type="text" style="width:50px" disabled="disabled" class="textCorner" /> occurrences <br />

<input id="radEndBy" value="3" type="radio" name="recRange" onclick="javascript:return recRangeOptions(this)" /> End by 
<asp:TextBox ID="recConfEnd" CssClass="textCorner" Enabled="false" runat="server" Width="85px" onclick="javascript:fnSelectDate(this)"></asp:TextBox>

</div>
</td>

</tr>

</table></div></td>
</tr>



</table>


</div></td></tr>


<!-- Recurrence End -->

<tr>
<td colspan="2"><hr style="width:300px" /></td>
</tr>

<!--
<tr>
<td>Conference Duration</td>
<td><input type="text" /></td>
</tr>
-->

<tr>
<td colspan="2">
<table><tr>
<td><input type="button" onclick="javascript:fnConfirmReset()" class="btnCorner" value="Reset Fields" /></td>
<td><input id="btnNext1" type="button" class="btnCorner" value="Next" onclick="javascript:return fnPrevNext('tab1','tab2')" /></td>
</tr></table>
</td>
</tr>


</table>

</div>

<div id="tab2" style="display:none">


<table>

<tr style="background-color:#cccccc">
<td colspan="2" class="headerGradient"><input type="button" value="Back" class="btnCorner" onclick="javascript:return fnMoveBack('tab2')" />
2. Invite Participants</td>
</tr>

<tr>
<td>

<input type="button" value="Invite Guest Users" class="btnCorner" onclick="javascript:return fnShowHideGuest('tab2', 'guestUsers')" />
<br />


<div style="height:100%; overflow:visible; border-color:Gray; padding:5px" class="btnCorner">

                    <asp:DataGrid ID="dgParticipants" runat="server" BorderStyle="None" BorderWidth="0" BorderColor="White" AutoGenerateColumns="false" ShowHeader="false" >
                        <Columns>
                            <asp:BoundColumn DataField="xUserid" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="xFirstName" Visible="true" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="xLastName" Visible="true" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="xEmail" Visible="true" ></asp:BoundColumn>
       
                            
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox id="chkSelectItem" runat="server" onclick="javascript:return fnSelParticipants(this.checked, this.parentNode.previousSibling.previousSibling.previousSibling.innerHTML, this.parentNode.previousSibling.previousSibling.innerHTML)" />
                                </ItemTemplate>
                            </asp:TemplateColumn>

                        </Columns>
                    </asp:DataGrid>

</div>


</td>
</tr>

<tr>
<td>
<table><tr>
<td><input id="btnPrev2" type="button" class="btnCorner" value="Previous" onclick="javascript:return fnPrevNext('tab2','tab1')" /></td>
<td><input type="button" onclick="javascript:fnConfirmReset()" class="btnCorner" value="Reset Fields" /></td>
<td><input id="btnNext2" type="button" class="btnCorner" value="Next" onclick="javascript:return fnPrevNext('tab2','tab3')" /></td>
</tr></table>
</td>
</tr>

</table>

</div>


<div id="guestUsers" style="display:none">
<table width="300px">

<tr style="background-color:#cccccc">
<td colspan="2" class="headerGradient"><input type="button" value="Back" class="btnCorner" onclick="javascript:return fnShowHideGuest('guestUsers', 'tab2')" />
Guest Participants</td>
</tr>
</table>
<br />
<div style="height:200px; width:325px; overflow-y:scroll; overflow-x:hidden; border-color:Gray" class="btnCorner">
<table id="tblGuestUsers" width="300px">



</table>
</div>
<br />
<input type="button" value="Add Guest" class="btnCorner" onclick="javascript:return fnShowHideGuest('guestUsers', 'getUser')" />
</div>

<div id="getUser" style="display:none">
<table width="300px">

<tr style="background-color:#cccccc">
<td colspan="2" class="headerGradient"><input type="button" value="Cancel" class="btnCorner" onclick="javascript:return fnShowHideGuest('getUser', 'guestUsers')" />
Add Guest User Details</td>
</tr>

<tr>
<td><br />First Name</td>
<td><br /><input id="getFname" type="text" class="textCorner" onkeypress="return (event.keyCode!=13)" /></td>
</tr>

<tr>
<td>Last Name</td>
<td><input id="getLname" type="text" class="textCorner" onkeypress="return (event.keyCode!=13)" /></td>
</tr>

<tr>
<td>Email</td>
<td><input id="getEmail" type="text" class="textCorner" onkeypress="return (event.keyCode!=13)" /></td>
</tr>

<tr>
<td colspan="2"><br /><input id="btnGetUser" class="btnCorner" type="button" value="Add Guest User" onclick="javascript:return fnGetUser();" /></td>
</tr>

<tr>
<td colspan="2"><label id="lblMsg" style="display:none; font-size:small; color:Red" >Please enter all the fields</label></td>
</tr>

</table>
</div>



<div id="tab3" style="display:none">

<table>

<tr style="background-color:#cccccc">
<td colspan="2" class="headerGradient"><input type="button" value="Back" class="btnCorner" onclick="javascript:return fnMoveBack('tab3')" />
3. Select Rooms</td>
</tr>


<tr>
<td><br />


<div style="height:300px; overflow-y:scroll; overflow-x:hidden; border-color:Gray" class="btnCorner">

<asp:ScriptManager id="saveUpdt" runat="server" />
<asp:UpdatePanel ID="updtRoom" runat="server">
<contenttemplate>
<div style="display:none">
<asp:Button ID="tempBut" runat="server" Text="Invoke" OnClick="selectRooms" />
</div>
                    <asp:DataGrid ID="dgRooms" runat="server" BorderStyle="None" BorderWidth="0" BorderColor="White" AutoGenerateColumns="false" ShowHeader="false" >
                        <Columns>
                        <asp:BoundColumn DataField="xRoomId" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="xRoomName" Visible="true"></asp:BoundColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:Button ID="info" Text="i" CssClass="maxRound" runat="server" OnClientClick="javascript:return fnShowRoomDetail(this.id); return false;" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox id="chkSelectItem" runat="server" onclick="javascript:return fnSelRoom(this.checked, this.parentNode.previousSibling.previousSibling.innerHTML)" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            </Columns>
                    </asp:DataGrid>

</contenttemplate>
</asp:UpdatePanel>

</div>

</td>
</tr>

<tr>
<td>
<table><tr>
<td><input id="btnPrev3" type="button" class="btnCorner" value="Previous" onclick="javascript:return fnPrevNext('tab3','tab2')" /></td>
<td><input type="button" onclick="javascript:fnConfirmReset()" class="btnCorner" value="Reset Fields" /></td>
<td><input id="btnNext3" type="button" class="btnCorner" value="Next" onclick="javascript:return fnPrevNext('tab3','tab4')" /></td>
</tr></table>
</td>
</tr>

</table>

</div>

<div id="tab4" style="display:none">
<table>
<tr style="background-color:#cccccc">
<td class="headerGradient"><input type="button" value="Back" class="btnCorner" onclick="javascript:return fnMoveBack('tab4')" />
4. Special Instructions
</td>
</tr>

<tr>
<td><br />
Addition Support Requirements
</td>
</tr>

<tr>
<td>
<textarea id="splInst" runat="server" class="textCorner" rows="10" cols="30" style="resize:none; border-color:Gray" onfocus="javascript:return fnSplInst('1')" onblur="javascript:return fnSplInst('0')" >Type instruction here.</textarea>
</td>
</tr>

<tr>
<td>
<table><tr>
<td><input id="btnPrev4" type="button" class="btnCorner" value="Previous" onclick="javascript:return fnPrevNext('tab4','tab3')" /></td>
<td><input type="button" onclick="javascript:fnConfirmReset()" class="btnCorner" value="Reset Fields" /></td>
<td><input id="btnNext4" type="button" class="btnCorner" value="Next" onclick="javascript:return fnPrevNext('tab4','tab5')" /></td>
</tr></table>
</td>
</tr>

</table>
</div>

<div id="tab5" style="display:none">
<asp:UpdatePanel ID="updtConf" runat="server">
<contenttemplate>

<table>
<tr style="background-color:#cccccc">
<td class="headerGradient"><input type="button" value="Back" class="btnCorner" onclick="javascript:return fnMoveBack('tab5')" />
5. Review
</td>
</tr>

<tr>
<td>
<asp:Label ID="errMsg" runat="server" Font-Size="Small" ForeColor="Red" ></asp:Label>
</td>
</tr>

<tr>
<td>
Summay of the Conference
</td>
</tr>

<tr>
<td>


<div style="border-color:Gray; width:100%; overflow:visible" class="btnCorner">

<table>

<tr>
<td>Conference Title</td>
<td><input id="xConfTitle" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>
</tr>

<tr>
<td nowrap="nowrap">Conference Description</td>
<td><input id="xConfDesc" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>

</tr>


<tr id="xRecurTr" runat="server">
<td>Recurrence</td>
<td><input id="xRecur" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>
</tr>

<tr>
<td>Conference Start</td>
<td><input id="xConfStart" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>
</tr>

<tr>
<td>Conference End</td>
<td><input id="xConfEnd" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>
</tr>

<tr id="xRecurTextTr" runat="server">
<td>Recurring Text</td>
<td><input id="xRecurText" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray; width:750px" /></td>
</tr>

<tr>
<td>Requestor Name</td>
<td><input id="xReqName" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>
</tr>

<tr>
<td>Time Zone</td>
<td><input id="xTimeZone" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>
</tr>


<tr>
<td>Selected Rooms</td>
<td><input id="xSelRoom" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray; width:500px" /></td>
</tr>


<tr>
<td>Invited Participants</td>
<td><input id="xSelPart" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray; width:500px" /></td>
</tr>

<tr>
<td>Invited Guests</td>
<td><input id="xGuests" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray; width:500px" /></td>
</tr>

<tr>
<td>Special Instructions</td>
<td><input id="xSplInst" readonly="readonly" runat="server" class="txtbox" type="text" style="color:Gray" /></td>
</tr>

</table>

</div>



</td>
</tr>

<tr>
<td colspan="2">
<asp:Button runat="server" Text="Create Conference" CssClass="btnCorner" OnClientClick="javascript:return fnValidateForm();" OnClick="setConf" />
</td>
</tr>

<tr>
<td>
<table><tr>
<td><input id="btnPrev5" type="button" class="btnCorner" value="Previous" onclick="javascript:return fnPrevNext('tab5','tab4')" /></td>
<td><input type="button" onclick="javascript:fnConfirmReset()" class="btnCorner" value="Reset Fields" /></td>
</tr></table>
</td>
</tr>

</table>

</contenttemplate>
</asp:UpdatePanel>
</div>

<div id="roomInfo" style="display:none">
<table width="250px">
<tr style="background-color:#cccccc">
<td class="headerGradient"><input type="button" value="Back" class="btnCorner" onclick="javascript:return fnHideRoomDetails()" />
Room Details
</td>
</tr>
</table>
<iframe id="frmRoomInfo" src="" style="height:600px" frameborder="0">

</iframe>
</div>


</form>

</body>
</html>

<script type="text/javascript">

var guestList = "";
function fnFormGuestList()
{
guestList = "";
var hdnGuest = document.getElementById('hdnGuestUsers').value;
var list = hdnGuest.split('~');
var guest = "";
    for(var i=0; i<list.length-1; i++)
    {
    guest = list[i].split('`');
    guestList += guest[0] + " " + guest[1];
    if(i < list.length-2)
    guestList += ", ";
    }
//alert(guestList);
}

function fnFormRecurText()
{
        var recurStatus = document.getElementById("recurStatus");
        
        if(recurStatus.value == "No")
            return false;

        var recurText = "Occurs ";
        recurType = document.getElementById("recurType");

        if (recurType.value == "divDaily")
        {
            dailyType1 = document.getElementById("dailyType1");
            txtDailyType = document.getElementById("txtDailyType");
            if (dailyType1.checked == true)
            {
                recurText += "every " + txtDailyType.value + " day(s)";
            }
            else
            {
                recurText += "every weekday";
            }
        }

        if (recurType.value == "divWeekly")
        {
            txtWkGap = document.getElementById("txtWkGap");
            recurText += "every " + txtWkGap.value + " week(s) on ";

            chkSunId = document.getElementById("chkSunId");
            chkMonId = document.getElementById("chkMonId");
            chkTueId = document.getElementById("chkTueId");
            chkWedId = document.getElementById("chkWedId");
            chkThuId = document.getElementById("chkThuId");
            chkFriId = document.getElementById("chkFriId");
            chkSatId = document.getElementById("chkSatId");
            
            var days="";
            if(chkSunId.checked == true)
            {
                days += "Sunday, ";
            }
            if (chkMonId.checked == true)
            {
                days += "Monday, ";
            }
            if (chkTueId.checked == true)
            {
                days += "Tuesday, ";
            }
            if (chkWedId.checked == true)
            {
                days += "Wednesday, ";
            }
            if (chkThuId.checked == true)
            {
                days += "Thursday, ";
            }
            if (chkFriId.checked == true)
            {
                days += "Friday, ";
            }
            if (chkSatId.checked == true)
            {
                days += "Saturday, ";
            }
            days = days.substr(0,days.length-2)
            recurText += days;

        }

        if (recurType.value == "divMonthly")
        {
            monthlyType1 = document.getElementById("monthlyType1");
            if (monthlyType1.checked == true)
            {
                drpSelDay1 = document.getElementById("drpSelDay1");
                recurText += "day " + drpSelDay1.options[drpSelDay1.selectedIndex].text + " of every ";
                
                drpSelMon1 = document.getElementById("drpSelMon1");
                recurText += drpSelMon1.options[drpSelMon1.selectedIndex].text + " month(s)";
            }
            else
            {
                drpSelPos1 = document.getElementById("drpSelPos1");
                recurText += "the " + drpSelPos1.options[drpSelPos1.selectedIndex].text + " ";
                
                drpSelWeek1 = document.getElementById("drpSelWeek1");                
                recurText += drpSelWeek1.options[drpSelWeek1.selectedIndex].text + " of every ";
                
                drpSelMonth1 = document.getElementById("drpSelMonth1");                
                recurText += drpSelMonth1.options[drpSelMonth1.selectedIndex].text;

            }

        }

        if (recurType.value == "divYearly")
        {
            yearlyType1 = document.getElementById("yearlyType1");
            if (yearlyType1.checked == true)
            {
                drpSelMon2 = document.getElementById("drpSelMon2");
                recurText += "every " + drpSelMon2.options[drpSelMon2.selectedIndex].text + " ";
                
                
                drpSelDay2 = document.getElementById("drpSelDay2");
                recurText += drpSelDay2.options[drpSelDay2.selectedIndex].text;
            }
            else
            {
                drpSelPos2 = document.getElementById("drpSelPos2");                
                recurText += "the " + drpSelPos2.options[drpSelPos2.selectedIndex].text + " ";
                
                drpSelWeek2 = document.getElementById("drpSelWeek2");                
                recurText += drpSelWeek2.options[drpSelWeek2.selectedIndex].text + " of ";
                
                drpSelMonth2 = document.getElementById("drpSelMonth2");                
                recurText += drpSelMonth2.options[drpSelMonth2.selectedIndex].text;

            }
        }

        if (recurType.value == "divCustom")
        {
            customDates = document.getElementById("customDates").value;
            recurText = "Custom Date Selection: ";

            var dates = customDates.split(',');
            for (var i = 0; i < dates.length-1; i++)
            {
                if(i == 0)
                    recurText += dates[i];
                else
                    recurText += ", " + dates[i];

            }

        }
        else
        {
            recConfStart = document.getElementById("recConfStart");
            recurText += " effective " + recConfStart.value;
            
            recurRange = document.getElementById("recurRange");

            if (recurRange.value == "2")
            {
                txtEndAft = document.getElementById("txtEndAft");
                recurText += " until " + txtEndAft.value;
            }
            else if (recurRange.value == "3")
            {
                recConfEnd = document.getElementById("recConfEnd");
                recurText += " occurs " + recConfEnd.value + " time(s)";
            }


        }
        
        drpRecurStart = document.getElementById("drpRecurStart");
        txtHrs = document.getElementById("txtHrs");
        txtMins = document.getElementById("txtMins");
        
        recurText += " from " + drpRecurStart.options[drpRecurStart.selectedIndex].text + " for " + txtHrs.value + " hr(s) " + txtMins.value + " min(s)" ;
        
        document.getElementById("hdnRecurText").value = recurText;
        //alert(recurText);


}

function fnShowConfAlert()
{
alert("Conference created successfully");
window.location.assign('calendar.aspx');
}

function fnUpdateCustomDates()
{
    var custDate = document.getElementById("customDates");
    var listbox = document.getElementById("f_selection");
    
    var custDateVal = "";
   
    for(var i=0; i<parseInt(listbox.length); i++)
    {
        custDateVal += listbox.options[i].text + ",";
    }
    //alert(custDateVal);
    custDate.value = custDateVal;

}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function fnValidateRecurrance()
{
    var recStat = document.getElementById("recurStatus").value;
    if(recStat == 'Yes')
    {
        txtHrs = document.getElementById("txtHrs");
        txtMins = document.getElementById("txtMins");
        if(isNumber(txtHrs.value) == false)
        {
            alert("Hours should be a number");
            txtHrs.value = "1";
            return false;
        }
        if(isNumber(txtMins.value) == false)
        {
            alert("Minutes should be a number");
            txtMins.value = "0";
            return false;
        }
        
        var recType = document.getElementById("recurType").value;
        if(recType == 'divDaily')
        {
            
            var opt = document.getElementById("dailyType1").checked;
            var val = document.getElementById("txtDailyType");
            
            if(opt == true)
            {
                if(val.value == "" && opt == true)
                {
                    alert("Please enter interval");
                    return false;
                }
                if(isNumber(val.value) == false)
                {
                    alert("Please enter number");
                    val.value="";
                    return false;
                }
            }
        }
        
        if(recType == 'divWeekly')
        {
            var val = document.getElementById("txtWkGap");
            if(val.value == "")
            {
                alert("Please enter interval")
                return false;
            }
            if(isNumber(val.value) == false)
            {
                alert("Please enter number");
                val.value="";
                return false;
            }
                
            var chks = "";
            chks += document.getElementById("chkSunId").checked;
            chks += document.getElementById("chkMonId").checked;
            chks += document.getElementById("chkTueId").checked;
            chks += document.getElementById("chkWedId").checked;
            chks += document.getElementById("chkThuId").checked;
            chks += document.getElementById("chkFriId").checked;
            chks += document.getElementById("chkSatId").checked;
            if(chks.indexOf("true") == -1)
            {
                alert("Please select atleast one day");
                return false;
            }
        }
        
        if(recType == 'divMonthly')
        {
        }
        
        if(recType == 'divYearly')
        {
        }
        
        if(recType == 'divCustom')
        {
            var sel = document.getElementById("f_selection")
            if(sel.length < 1)
            {
                alert("Please select atleast one date");
                return false;
            }
        }
        else
        {
            var chk = document.getElementById("radNoEndDt").checked;
            if(chk == false)
            {
            var endAft = document.getElementById("txtEndAft");
            var endBy = document.getElementById("recConfEnd").value;
                if(endAft.value == "" && endBy == "")
                {
                    alert("Please finish range of recurrence");
                    return false;
                }
                if(endAft.value != "" && isNumber(endAft.value) == false)
                {
                    alert("Please enter number");
                    endAft.value="";
                    return false;
                }
                
            }
            
        }
        return true;
    }
}

function fnCheckMonthDays(m,d)
{
    var monthDays = new Array('0','31','28','31','30','31','30','31','31','30','31','30','31');

    var mon = document.getElementById(m);
    var monVal = mon.options[mon.selectedIndex].value;

    var day = document.getElementById(d);
    var dayVal = day.options[day.selectedIndex].value;

    //alert('Month: ' + monVal + ' ,Day: ' + dayVal);
    if(parseInt(dayVal) > parseInt(monthDays[monVal]))
    {
        alert("Day Interval for Month is not Valid");
        day.selectedIndex = parseInt(monthDays[monVal])-1;
    }
}

function fnCheckDuration()
{
    var hr = document.getElementById("txtHrs");
    var min = document.getElementById("txtMins");
    
    var totMin = (parseInt(hr.value) * 60) + parseInt(min.value);
    //alert(totMin);
    if(totMin < 15 || totMin > 1440)
    {
        alert("Invalid duration");
        hr.value = "1";
        min.value = "0";
    }
    /*
    if(min.value == 0)
    {
        if(hr.value > 24)
        {
            alert("Invalid Hours")
            hr.value = "1";
        }
    }
    else
    {
        if(hr.value > 23)
        {
            alert("Invalid Hous");
            hr.value = "1";
        }
    }
    
    if(min.value > 59)
    {
        alert("Invalid Minutes");
        min.value = "0";
    }
    */
}

function recRangeOptions(par1)
{
    var obj1 = document.getElementById("txtEndAft");
    if(par1.id == "radEndAft")
        obj1.disabled = false;
    else
    {
        obj1.disabled = true;
        obj1.value = "";
    }
        
    var obj2 = document.getElementById("recConfEnd");
    if(par1.id == "radEndBy")
        obj2.disabled = false;
    else
    {
        obj2.disabled = true;
        obj2.value = "";
    }
    
    document.getElementById("recurRange").value = par1.value;
    //alert(par1.value);
}   

function removedate(cb)
{
	if (cb.options.selectedIndex != -1) {
	    //cb.options[cb.selectedIndex] = null;
        var optionToRemove=cb.options.selectedIndex;
        cb.remove(optionToRemove);

	}
	fnUpdateCustomDates();
	SortDates(0);
	//cal.refresh();
	//optionToRemove=htmlSelect.options.selectedIndex;
    //htmlSelect.remove(optionToRemove);
}

/*
function mydatesort(a, b)
{
    dFormat = "dd/MM/yyyy";
	var datereg = /^(\d{2})[\/\- ](\d{2})[\/\- ](\d{4})/;

    //* *** Code added by offshore for FB Issue 1073 -- start  *** * /
   if(dFormat == "dd/MM/yyyy")
   {
        a = a.replace(datereg,"$3$2$1");
	    b = b.replace(datereg,"$3$2$1");
   }
   else
   {
        a = a.replace(datereg,"$3$1$2");
	    b = b.replace(datereg,"$3$1$2");
   }  
   //* *** Code added by offshore for FB Issue 1073 -- end *** * /
    
	if (a > b) return 1;
	if (a < b) return -1;

	return 0;
}
*/

function SortDates(update)
{
	var temp;
    
    datecb = document.getElementById('f_selection');
	//datecb = document.frmSettings2.CustomDate;
	
	var dateary = new Array();

	for (var i=0; i<datecb.length; i++) {
		dateary[i] = datecb.options[i].value;
		
		dateary[i] = ( (parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10) ) + "/" + ( (parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10) ) + "/" + ( parseInt(dateary[i].split("/")[2], 10) );
	}
	
	dateary.sort();
    /*
	for (i=0;i<dateary.length-1;i++)
	 	for(j=i+1;j<dateary.length;j++)
			if (mydatesort(dateary[i], dateary[j]) > 0)
			{
				temp = dateary[i];
				dateary[i] = dateary[j];
				dateary[j] = temp;	
			}
	*/	
    document.getElementById("hdnCustStartDate").value = dateary[0];
    //alert(dateary[0]);
    
    if(update == 1)
    {
	    for (var i=0; i<dateary.length; i++) {
    		datecb.options[i].text = dateary[i];
		    datecb.options[i].value = dateary[i];		
	    } 
    	
    	fnUpdateCustomDates();
    }
    return false;
}


// Remove this function by using checkboxes
/*
function fnToggleDays(idx)
{
    var curColor = document.getElementById(idx).style.backgroundColor;
    if(curColor == "blue")
    {
        document.getElementById(idx).style.backgroundColor = "white";
    }
    else
    {
        document.getElementById(idx).style.backgroundColor = "blue";
    }
}
*/

// Remove this function by using radio buttons
/*
function fnEveryWeekday()
{
    var everyWeekday = document.getElementById('everyWeekday');
    if(everyWeekday.value == "ON")
    {
        everyWeekday.value = "OFF";
        document.getElementById('tdEveryWeekOff').style.display = "block";
    }
    else
    {
        everyWeekday.value = "ON";
        document.getElementById('tdEveryWeekOff').style.display = "none";
    }
}
*/

function fnShowRecur(divId, tdid)
{
    document.getElementById('divDaily').style.display = "none";
    document.getElementById('divWeekly').style.display = "none";
    document.getElementById('divMonthly').style.display = "none";
    document.getElementById('divYearly').style.display = "none";
    document.getElementById('divCustom').style.display = "none";
    document.getElementById(divId).style.display = "block";
    
    document.getElementById('recurOpt1').style.backgroundColor = "#ddd";
    document.getElementById('recurOpt2').style.backgroundColor = "#ddd";
    document.getElementById('recurOpt3').style.backgroundColor = "#ddd";
    document.getElementById('recurOpt4').style.backgroundColor = "#ddd";
    document.getElementById('recurOpt5').style.backgroundColor = "#ddd";
    document.getElementById(tdid).style.backgroundColor = "#bbb";
    
   
    if(divId == 'divCustom')
    {
        document.getElementById('recurRangeDiv1').style.display = "none";
        document.getElementById('recurRangeDiv2').style.display = "none";
    }
    else
    {
        document.getElementById('recurRangeDiv1').style.display = "block";
        document.getElementById('recurRangeDiv2').style.display = "block";
    }
    document.getElementById("recurType").value = divId;
    //alert(divId);
}


function validateEmail(tfld)
{
    var stat = true;
    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
    var illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/;

    if (tfld == "") {
        stat = false;
    } else if (!emailFilter.test(tfld)) {
        stat = false;
    } else if (tfld.match(illegalChars)) {
        stat = false;
    } else {
        stat = true;
    }
    return stat;
}

function fnShowHideGuest(hide, show)
{
    document.getElementById(hide).style.display = "none";
    document.getElementById(show).style.display = "block";
    if(hide =='getUser' && show == 'guestUsers')
    {
        document.getElementById("getFname").value = "";
        document.getElementById("getLname").value = "";
        document.getElementById("getEmail").value = "";
        document.getElementById("lblMsg").style.display = "none";
    }
}

var userDetails = "";
function fnGetUser()
{
    var fname = document.getElementById("getFname");
    var lname = document.getElementById("getLname");
    var email = document.getElementById("getEmail");

    if(fname.value == "" || lname.value == "")
    {
        document.getElementById("lblMsg").style.display = "block";
        return false;
    }

    if(!validateEmail(email.value))
    {
        document.getElementById("lblMsg").style.display = "block";
        return false;
    }
    
    if(fname.value.indexOf('`') > -1 || fname.value.indexOf('~') > -1)
    {
        document.getElementById("lblMsg").style.display = "block";
        return false;
    }
    
    if(lname.value.indexOf('`') > -1 || lname.value.indexOf('~') > -1)
    {
        document.getElementById("lblMsg").style.display = "block";
        return false;
    }

    if(email.value.indexOf('`') > -1 || email.value.indexOf('~') > -1)
    {
        document.getElementById("lblMsg").style.display = "block";
        return false;
    }
    
    document.getElementById("lblMsg").style.display = "none";

    userDetails = userDetails + fname.value + "`" + lname.value + "`" + email.value + "~";
    document.getElementById('hdnGuestUsers').value = userDetails;

    
    var rowid = document.getElementById('tblGuestUsers').rows.length;

    var row=document.getElementById('tblGuestUsers').insertRow(rowid);
    var cell1=row.insertCell(0);
    var cell2=row.insertCell(1);
    var cell3=row.insertCell(2);
    var cell4=row.insertCell(3);
    cell1.innerHTML=fname.value;
    cell2.innerHTML=lname.value;
    cell3.innerHTML=email.value;
    cell4.innerHTML="<input type='button' class='btnCorner' id='" + rowid + "' value='X' onclick='javascript:return fnRemoveRow(this.id)' />";
    
    fname.value = "";
    lname.value = "";
    email.value = "";
    document.getElementById('getUser').style.display = "none";
    document.getElementById('guestUsers').style.display = "block";

}

function fnRemoveRow(rowid)
{
    //previousSibling
    var ele = document.getElementById(rowid).parentNode;
    var xfname = ele.previousSibling.previousSibling.previousSibling.innerHTML;
    var xlname = ele.previousSibling.previousSibling.innerHTML;
    var xemail = ele.previousSibling.innerHTML;
    var remove = xfname + "`" + xlname + "`" + xemail + "~"
    //alert(remove);
    userDetails = userDetails.replace(remove,"");
    document.getElementById('hdnGuestUsers').value = userDetails;
    var ele2 = document.getElementById(rowid).parentNode.parentNode;
    ele2.parentElement.removeChild(ele2);
    //document.getElementById('tblGuestUsers').deleteRow(rowid);
    return false;
}

function fnShowRoomDetail(idx)
{
    var frm = document.getElementById("frmRoomInfo");
    frm.src = "roomInfo.aspx?rid=" + idx.split("_")[2];
    frm.src = frm.src;
    document.getElementById("tab3").style.display = "none";
    document.getElementById("roomInfo").style.display = "block";
    return false;
}

function fnHideRoomDetails()
{
    var frame = document.getElementById("frmRoomInfo");
    var frameDoc = frame.contentDocument || frame.contentWindow.document;
    frameDoc.documentElement.innerHTML = "";
    //alert('hai');
    document.getElementById("roomInfo").style.display = "none";
    document.getElementById("tab3").style.display = "block";

}

function fnUpdateText(arg)
{
    globalChange = true;
    var flag = fnCheckPastDate(1);
    if(flag == true)
    {
        /*
        var startDate = document.getElementById("txtConfStart");
        var endDate = document.getElementById("txtConfEnd");

        var d1 = new Date(startDate.value);
        var d2 = new Date(endDate.value);
        
        if(d1 > d2)
        {
        */
        document.getElementById("txtConfEnd").value = arg;
        fnAdjustEndDate();
        //}
    }
}

var rooms = "";
var participants = "";

function fnSelRoom(checked,text)
{
    if(checked == true)
    {
        rooms = rooms + text + ",";
    
    }
    if(checked == false)
    {
        text = text + ","
        rooms = rooms.replace(text,"");
    }
    //alert(rooms);
}


function fnSelParticipants(checked,text1,text2)
{
    var text = text1 + " " + text2;
    if(checked == true)
    {
        participants = participants + text + ",";
    
    }
    if(checked == false)
    {
        text = text + ","
        participants = participants.replace(text,"");
    }
    //alert(participants);

}


function fnUpdateReview()
{
    //fnFormRecurText();
    //fnFormGuestList();
    
    fnFormGuestList();
    //alert(guestList);
    //alert(document.getElementById("hdnRecurText").value);

    document.getElementById("xConfTitle").value = document.getElementById("confTitle").value;
    var confDiscObj = document.getElementById("confDisc");
    document.getElementById("xConfDesc").value = (confDiscObj.value == "Conference Description")? "None" : confDiscObj.value;
    
    var recurStatus = document.getElementById("recurStatus");
    if(document.getElementById("xRecur") != null)
        document.getElementById("xRecur").value = recurStatus.value;
    
    var xConfStart = document.getElementById("xConfStart");
    var xConfEnd = document.getElementById("xConfEnd");
    
    if(recurStatus.value == "No")
    {
        var drpObj1 = document.getElementById("drpStartTime");
        var selec1 = drpObj1.options[drpObj1.selectedIndex].text;
        xConfStart.value = document.getElementById("txtConfStart").value + " " +selec1;
        
        var drpObj2 = document.getElementById("drpEndTime");
        var selec2 = drpObj2.options[drpObj2.selectedIndex].text;
        xConfEnd.value = document.getElementById("txtConfEnd").value + " " +selec2;
    }
    else
    {
        xConfStart.value = "See Recurring Text";
        xConfEnd.value = "See Recurring Text";
    }

    if(document.getElementById("xRecurText") != null)
    {
        if(recurStatus.value != "No")
        document.getElementById("xRecurText").value = document.getElementById("hdnRecurText").value;
        else
        document.getElementById("xRecurText").value = "None";
    }
    
    document.getElementById("xReqName").value = document.getElementById("txtReqName").value; //;
    
    var drpObj = document.getElementById("drpTimeZone");
    var selec = drpObj.options[drpObj.selectedIndex].text;
    document.getElementById("xTimeZone").value = selec; //
    
    if(rooms != "")
    document.getElementById("xSelRoom").value = rooms.substr(0,rooms.length-1);
    else
    document.getElementById("xSelRoom").value = "None";
    
    if(participants != "")
    document.getElementById("xSelPart").value = participants.substr(0,participants.length-1);
    else
    document.getElementById("xSelPart").value = "None";
    
    if(guestList != "")
    document.getElementById("xGuests").value = guestList;
    else
    document.getElementById("xGuests").value = "None";
    //alert(participants.substring(0,participants.length-2));
    
    var splInstObj = document.getElementById("splInst");
    document.getElementById("xSplInst").value = (splInstObj.value == "Type instruction here.") ? "None" : splInstObj.value;

}



function fnLoadFunctions()
{
    //fnAlwaysDisplay();
    document.getElementById('txtConfStart').readOnly=true; //
    document.getElementById('txtConfEnd').readOnly=true;
    document.getElementById('txtReqName').readOnly=true;
    
    document.getElementById('recConfStart').readOnly=true;
    document.getElementById('recConfEnd').readOnly=true;
    
    
    fnLoadOptions('drpStartTime');
    fnLoadOptions('drpEndTime');
    fnLoadOptions('drpRecurStart');
    fnUpdateStartDate();
}

function fnUpdateHdnRecurStart()
{
    var drpObj = document.getElementById("drpRecurStart");
    var selec = drpObj.options[drpObj.selectedIndex].text;
    document.getElementById('hdnRecurStart').value = selec;
    //alert(selec);

}


function fnUpdateHdnStartDate()
{
    globalChange = true;
    var drpObj = document.getElementById("drpStartTime");
    var selec = drpObj.options[drpObj.selectedIndex].text;
    document.getElementById('hdnStartTime').value = selec;
    fnUpdateDrop2();
    //fnAdjustEndDate();
}

function fnUpdateHdnEndDate()
{
    //alert('hi');
    globalChange = true;
    
    if(document.getElementById("txtConfStart").value == document.getElementById("txtConfEnd").value)
    {
    var drpObj1 = document.getElementById("drpStartTime");
    var selec1 = drpObj1.options[drpObj1.selectedIndex].index;

    var drpObj2 = document.getElementById("drpEndTime");
    var selec2 = drpObj2.options[drpObj2.selectedIndex].index;

        if(selec1 >= selec2)
        {
            alert("End time should be greater than or equal to start time");
            fnUpdateDrop2();
            return false;
        }
    }
    var drpObj = document.getElementById("drpEndTime");
    var selec = drpObj.options[drpObj.selectedIndex].text;
    document.getElementById('hdnEndTime').value = selec;

}

function fnAdjustEndDate()
{

    var drpObj = document.getElementById("drpStartTime");
    var selec = drpObj.options[drpObj.selectedIndex].text;
    
    if(selec.indexOf("11")>-1 && selec.indexOf("PM")>-1)
    {
        var startTime = document.getElementById("txtConfStart").value;
    
        var d = new Date(startTime.split("/")[2],startTime.split("/")[0]-1,startTime.split("/")[1]);
        d.setDate(d.getDate()+1);
    
        var mm = d.getMonth()+1;
        mm = (mm < 10) ? '0' + mm : mm;
        var dd = d.getDate();
        dd = (dd < 10) ? '0' + dd : dd;
        var yyyy = d.getFullYear();
        var date = mm + '/' + dd + '/' + yyyy;
        //alert(date);
        document.getElementById("txtConfEnd").value = date;
    }
    else
    {
        document.getElementById("txtConfEnd").value = document.getElementById("txtConfStart").value;;
    }
    
}

function fnUpdateDrop2()
{
    var drpStart = document.getElementById("drpStartTime");
    var drpEnd = document.getElementById("drpEndTime");

    var ind = drpStart.selectedIndex + 4;
    if(ind > 95)
        ind = ind - 96;
    drpEnd.selectedIndex = ind;

    //var drpObj = document.getElementById("drpEndTime");
    var selec = drpEnd.options[drpEnd.selectedIndex].text;
    document.getElementById('hdnEndTime').value = selec;

}

function fnUpdateStartDate()
{
    var drpStart = document.getElementById("drpStartTime");
    var drpEnd = document.getElementById("drpEndTime");
    
    var selText = document.getElementById('hdnStartTime').value;
    document.getElementById('hdnRecurStart').value = selText;
    
    var recStart = document.getElementById('drpRecurStart');
    
    for(i=0; i<95; i++)
    {
        if(drpStart.options[i].text == selText)
        {
        drpStart.selectedIndex = i;
        recStart.selectedIndex = i;
        break;
        }
    }

    fnUpdateDrop2();

    return false;
}

window.onload = fnLoadFunctions;

//alert(globalChange);

</script>