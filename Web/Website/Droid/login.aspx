﻿<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="Droid_login" %>
<!DOCTYPE html>
<%
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
%>
<html>
<head id="Head1" runat="server">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1">
<title>myVRM</title>
<style type="text/css">
.gradient
{
	color: #fef4e9;
	border: solid 1px #000033;
	background: #000099;
	background: -webkit-gradient(linear, left top, left bottom, from(#000066), to(#0000CC));
	background: -moz-linear-gradient(top,  #000066,  #0000CC);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#000066', endColorstr='#0000CC');
}
.txtbox
{
    border-style:hidden;
    border-color:#ffffff;
}
.corner
{
    border:2px solid #a1a1a1;
    border-radius:15px;
}
.btnCorner
{
    border:1px solid #a1a1a1;
    border-radius:5px;
}
body
{
	color: #333333;
	font-family:Arial;
	font-size:medium;
}
</style>
<script type="text/javascript">

/*
function trim(s)
{
	var l=0; var r=s.length -1;
	while(l < s.length && s[l] == ' ')
	{	l++; }
	while(r > l && s[r] == ' ')
	{	r-=1;	}
	return s.substring(l, r+1);
}
*/

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

    function fnTextFocus(xid, par) {
        var obj = document.getElementById(xid);
        if (par == 1) {
            if (trim(obj.value) == "name@domain.com") {
                obj.value = "";
                obj.style.color = "Black";
            }
            else {
                document.getElementById('btnClrEmail').style.visibility = 'visible';
            }
        }
        else {
            if (trim(obj.value) == "Required") {
                obj.value = "";
                obj.style.color = "Black";
                obj.type = "password";
            }
            else {
                document.getElementById('btnClrPwd').style.visibility = 'visible';
            }
        }
    }

    function fnTextBlur(xid, par) {
        var obj = document.getElementById(xid);
        if (par == 1) {
            if (trim(obj.value) == "") {
                obj.value = "name@domain.com";
                document.getElementById('btnClrEmail').style.visibility = 'hidden';
                obj.style.color = "Gray";
            }
            else {
                if(obj.value.indexOf("@") == -1)
                    obj.value = trim(obj.value) + "@myvrm.com";
                document.getElementById('btnClrEmail').style.visibility = 'visible';
            }
        }
        else {
            if (trim(obj.value) == "") {
                obj.type = "text";
                obj.value = "Required";
                document.getElementById('btnClrPwd').style.visibility = 'hidden';
                obj.style.color = "Gray";
            }
            else {
                document.getElementById('btnClrPwd').style.visibility = 'visible';
            }
        }
    }

    function fnClearValue(btnId, val) {
        var cntrl = document.getElementById(btnId);
        if (val == 1) {
            var objId = document.getElementById('username');
            objId.value = 'name@domain.com';
            objId.style.color = "Gray";
            cntrl.style.visibility = 'hidden';
        }
        if (val == 2) {
            var objId2 = document.getElementById('pwdid');
            objId2.type = "text";
            objId2.value = 'Required';
            objId2.style.color = "Gray";
            cntrl.style.visibility = 'hidden';
        }
        return false;
    }

    function fnValidateLogin() {
        var userMail = document.getElementById('username').value;
        var pwd = document.getElementById('pwdid').value;

        var msg = "";

        if (!validateEmail(userMail) || userMail == "name@domain.com") {
            msg = "Please enter valid Email";
        }

        else if (pwd == "Required") {
            msg = "Please enter password";
        }

        if (msg != "") {
            document.getElementById('errLabel').innerHTML = msg;
            return false;
        }
        else {
            document.getElementById('errLabel').innerHTML = "";
            return true;
        }

    }

    function validateEmail(tfld) {
        var stat = true;
        var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
        var illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/;

        if (tfld == "") {
            stat = false;
        } else if (!emailFilter.test(tfld)) {
            stat = false;
        } else if (tfld.match(illegalChars)) {
            stat = false;
        } else {
            stat = true;
        }
        return stat;
    }
    
    /*
    function fnSubmitForm(e)
    {
        if (e.keyCode == 13)
        {
            //document.getElementById('btnSubmit').click();
            return false;
        }
    }
    */
    
    
</script>
</head>
<body id="loginPage">
    <form id="form1" runat="server">
    <div style="border-collapse:collapse">
    <img id="siteLogoId" src="../Droid/image/myvrm.jpg" alt="" />
    <div class="gradient" style="white-space:nowrap">Saving the planet...one meeting at a time</div><br />
    
    <table align="center" width="50%">
    <tr><td align="center">
    <asp:Label id="errLabel" runat="server" style="font-size:small; color:Red"></asp:Label>
    </td></tr>
    </table><br />
    
    <table class="corner" align="center" width="50%">
    
        <tr>
        <td width="5%"></td>
        <td width="15%" align="left">Email</td>
        <td width="20%" align="left"><input id="username" runat="server" class="txtbox" type="text" value="name@domain.com" onfocus="javascript:return fnTextFocus(this.id,1)" onblur="javascript:return fnTextBlur(this.id,1)" onkeypress="return (event.keyCode!=13)" style="color:Gray" /></td>
        <td width="10%"><input id="btnClrEmail" type="image" src="../Droid/image/btnClear.jpg" name="image" onclick="javascript:return fnClearValue(this.id,1)" style="visibility:hidden; vertical-align:middle" /></td>
        </tr>
        
        <tr>
        <td></td>
        <td align="left">Password</td>
        <td align="left"><input id="pwdid" runat="server" class="txtbox" type="text" value="Required" onfocus="javascript:return fnTextFocus(this.id,2)" onblur="javascript:return fnTextBlur(this.id,2)" onkeypress="return (event.keyCode!=13)" style="color:Gray" /></td>
        <td><input id="btnClrPwd" type="image" src="../Droid/image/btnClear.jpg" name="image" onclick="javascript:return fnClearValue(this.id,2)" style="visibility:hidden; vertical-align:middle" /></td>
        </tr>
        
    </table>
    <table align="center" width="50%"><tr><td align="center"><br />
    <asp:Button  id="btnSubmit" runat="server" Text="Sign in"  CssClass="btnCorner" OnClientClick="javascript:return fnValidateLogin()" OnClick="btnSubmit_Click"/>
    </td></tr></table>
    </div>
    </form>
</body>
</html>
<script type="text/javascript">
function fnResetValues()
{
    //alert="hai";
    document.getElementById('username').value = 'name@domain.com';
    document.getElementById('pwdid').value = 'Required';
    //changeScreenSize();
}

/*
function changeScreenSize()
{
    //alert(screen.width);
    //alert(screen.height);
    document.getElementById("loginPage").style.width = screen.width + 'px';
    document.getElementById("loginPage").style.height = screen.height + 'px';
    
    
}
*/

window.onload = fnResetValues;
</script>

