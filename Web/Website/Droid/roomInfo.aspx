﻿<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="roomInfo.aspx.cs" Inherits="Droid_roomInfo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<!DOCTYPE HTML>
<html lang="en">
<head runat="server">
<meta name="viewport" content="width=device-width" />
<meta charset="utf-8" /> 
<title></title>

<style type="text/css">
body
{
	color: #333333;
	font-family:Arial;
}
</style>

<script type="text/javascript">
function fnRedirectPage()
{
//alert('hai');
parent.window.location.assign('login.aspx');
}
</script>

</head>

<body>
<form id="confForm" runat="server">
<div id="cont" style="display:block">
</div>
<table>

<tr>
<td colspan="2"><asp:Label ID="lblTier" runat="server" /></td>
</tr>

<tr>
<td colspan="2"><asp:Label ID="lblRoomName" runat="server" /></td>
</tr>

<tr>
<td colspan="2"><img id="imgRoom" src="" alt="" runat="server" width="200" /></td>
</tr>

<tr>
<td>Number</td>
<td><asp:Label ID="lblNumber" runat="server" /></td>
</tr>

<tr>
<td>Phone</td>
<td><asp:Label ID="lblPhone" runat="server" /></td>
</tr>

<tr>
<td>Capacity</td>
<td><asp:Label ID="lblCapacity" runat="server" /></td>
</tr>

<tr>
<td>Assistant Name</td>
<td><asp:Label ID="lblAssistant" runat="server" /></td>
</tr>

<tr>
<td>Projector</td>
<td><asp:Label ID="lblProjector" runat="server" /></td>
</tr>

<tr>
<td>Caterer Facility</td>
<td><asp:Label ID="lblCaterer" runat="server" /></td>
</tr>


</table>
</form>

</body>
</html>

