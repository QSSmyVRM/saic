﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.IO;
//using System.Xml;

public partial class Droid_roomInfo : System.Web.UI.Page
{

    myVRMNet.NETFunctions obj = null;
    protected string roomId = null;
    protected string roomDetailOutXML = null;
    RoomDetails rd = null;
    myVRMNet.ImageUtil imageUtilObj = null;

    public Droid_roomInfo()
    {
        obj = new myVRMNet.NETFunctions();
        imageUtilObj = new myVRMNet.ImageUtil();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["username"] == null)
        {
            //Response.Redirect("login.aspx");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "<script>fnRedirectPage();</script>", false);
            return;
        }

        if (Request.QueryString["rid"] != null)
        {
            roomId = Request.QueryString["rid"].ToString();
            getRoomDetails();
            showRoomDetails();
        }
        else
        {
            return;
        }



    }


    public class RoomDetails
    {
        public string xTier1 { get; set; }
        public string xTier2 { get; set; }
        public string xRoomName { get; set; }
        public string xImage { get; set; }

        public string xNumber { get; set; }
        public string xPhone { get; set; }
        public string xCapacity { get; set; }

        public string xAsstName { get; set; }
        public string xProjector { get; set; }
        public string xCatFacil { get; set; }

    }


    protected void getRoomDetails()
    {
        string roomDetailInXML = "<GetRoomProfile>" + obj.OrgXMLElement() + "<UserID>" + Session["userID"].ToString() + "</UserID><RoomID>" + roomId.ToString() + "</RoomID></GetRoomProfile>";
        roomDetailOutXML = obj.CallMyVRMServer("GetRoomProfile", roomDetailInXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027(GetOldRoom)

        XDocument xd = XDocument.Parse(roomDetailOutXML);

        rd = new RoomDetails();
        rd = (from node in xd.Descendants("GetRoomProfile")
                     //where lv1.Element("videoAvailable").Equals("2")
                     select new RoomDetails 
                     {
                         xTier1 = Convert.ToString(node.Element("Tier1Name").Value),
                         xTier2 = Convert.ToString(node.Element("Tier2Name").Value),
                         xRoomName = Convert.ToString(node.Element("RoomName").Value),
                         xImage = Convert.ToString(node.Element("RoomImages").Value),

                         xNumber = Convert.ToString(node.Element("RoomNumber").Value),
                         xPhone = Convert.ToString(node.Element("RoomPhoneNumber").Value),
                         xCapacity = Convert.ToString(node.Element("MaximumCapacity").Value),

                         xAsstName = Convert.ToString(node.Element("AssistantInchargeName").Value),
                         xProjector = Convert.ToString(node.Element("Projector").Value),
                         xCatFacil = Convert.ToString(node.Element("CatererFacility").Value)
                     }).ToArray()[0];


        if (rd.xImage != "")
        {
            string xnodes = (from xnode in xd.Descendants("ImageDetails")
                             select xnode.Element("Image").Value).ToArray()[0].ToString();

            if (xnodes != "")
            {
                byte[] imgArray = imageUtilObj.ConvertBase64ToByteArray(xnodes);

                if (File.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\img.jpg"))
                    File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\img.jpg");

                WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\img.jpg", ref imgArray);

                imgRoom.Src = "~\\Droid\\image\\img.jpg";
                //Image imgContrl = (Image)e.Item.FindControl("imgItem");
                //imgContrl.ImageUrl = fullPath;
            }


        }
        else
        {
            imgRoom.Src = "~\\Droid\\image\\noimg.jpg";
        }

        //nodes.ToList()[0];




    }

    private void WriteToFile(string strPath, ref byte[] Buffer)
    {
        try
        {
            // Create a file
            FileStream newFile = new FileStream(strPath, FileMode.Create);

            // Write data to the file
            newFile.Write(Buffer, 0, Buffer.Length);

            // Close file
            newFile.Close();
        }
        catch (Exception ex)
        {
            //log.Trace(ex.StackTrace + " : " + ex.Message);
        }
    }


    protected void showRoomDetails()
    {
        //sample.Value = "test";
        lblTier.Text = rd.xTier1.ToString() + " > " + rd.xTier2.ToString() + " > ";
        lblRoomName.Text = rd.xRoomName.ToString();
        lblNumber.Text = rd.xNumber.ToString();
        lblPhone.Text = rd.xPhone.ToString();
        lblCapacity.Text = rd.xCapacity.ToString();
        lblAssistant.Text = rd.xAsstName.ToString();
        lblProjector.Text = (rd.xProjector.ToString() == "1") ? "Available" : "";
        lblCaterer.Text = (rd.xCatFacil.ToString() == "1") ? "Available" : "";
        //lblCaterer.Text = rd.xCatFacil.ToString();


    }

    



}