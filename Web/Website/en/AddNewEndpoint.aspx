﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_AddNewEndpoint" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2779 -->
<% if(Request.QueryString["ifrm"] == null){%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<% }%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AddNewEndpoint</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css"/>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css"/>
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css"/>--%> 
    <script type="text/javascript">         // FB 2790
         var path = '<%=Session["OrgCSSPath"]%>';
         path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
         document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script> 

</head>

<%-- Access the web method through ajax --%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
<%-- Jquery popup window --%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<%-- User Defined Jquery Functions  --%>
<script type="text/javascript" src="script/CallMonitorJquery/MonitorMCU.js"></script>

<body>
    <form id="frmAddNewEndpoint" runat="server" onsubmit="DataLoading(1);"> <%--ZD 100176--%>
    <input id="hdnWebAccURL" name="hdnWebAccURL" runat="server" type="hidden" />
    <input id="hdnLineRate" name="hdnLineRate" runat="server" type="hidden" />
    <input id="hdnApiPortNo" name="hdnApiPortNo" runat="server" type="hidden" />
    <input id="hdnVideoEquipment" name="hdnVideoEquipment" runat="server" type="hidden" />
    <input id="hdnMCUServiceAdd" name="hdnBridgeServiceAdd" runat="server" type="hidden" />
    <input id="hdnExchangeID" name="hdnExchangeID" runat="server" type="hidden" />
    <input id="hdnEndpointID" name="hdnEndpointID" runat="server" type="hidden" />
    <input id="hdnEndpointURL" name="hdnEndpointURL" runat="server" type="hidden" />
    <a style="display:none"><asp:Button ID="selectEndPoint" runat="server"  onclick="BindEndpointData" /></a>
    <div>
      <center>
      <table border="0" width="100%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center" colspan="2">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Add New Endpoint"></asp:Label></h3><br />
                     <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                     <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%> 
                </td>
            </tr>
            <tr>
                <td class="subtitleblueblodtext" align="left" colspan="2">Basic Information</td>                            
            </tr>
            <tr>
                <td width="50%" valign="top">
                    <table width="100%" style="margin-left:20px">
                        <tr>
                            <td align="left" class="blackblodtext" width="35%">Listed Endpoint</td>
                            <td align="left">
                                <asp:CheckBox ID="chkListedEndpoint" runat="server" onclick="javascript:OpenEndpointlist(this)" />
                            </td>
                        </tr>
                        <tr>
                           <td align="left"  class="blackblodtext">Unlisted Endpoint</td>
                           <td align="left" >
                                <asp:CheckBox ID="chkUnlistedEndpoint" runat="server" onclick="javascript:OpenEndpointlist(this)" />
                           </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%" style="padding:0px;">
                      <tr valign="top" >
                         <td align="left" width="35%" class="blackblodtext" valign="middle">Terminal Type </td>
                         <td align="left"><asp:Label ID="lblTerminalType" CssClass="subtitleblueblodtext" runat="server" Text="Guest"/></td>
                      </tr>
                    </table>
               </td>
            </tr>
            <tr>
                <td class="subtitleblueblodtext" align="left" colspan="2">Endpoint Parameters</td>                            
            </tr>
            <tr>
                <td width="50%">
                    <table width="100%" style="margin-left:20px">
                        <tr>
                            <td align="left" class="blackblodtext" width="35%">
                                Name<span class="reqfldText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText"  width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqEndpointName" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Required"  ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regEndpointName" SetFocusOnError="true" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" ValidationGroup="Submit" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Protocol<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID" Width="50%" onchange="javascript:ValidateSelection(this)"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqlstProtocol" ErrorMessage="Required" runat="server" ControlToValidate="lstProtocol" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                           
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Connection <span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" runat="server" ID="lstConnection" Width="50%">
                                    <asp:ListItem Text="Please select.." Value="-1"></asp:ListItem> 
                                    <asp:ListItem Text="Audio Only" Value="1"></asp:ListItem> 
                                    <asp:ListItem Text="Audio/video" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="-1" ControlToValidate="lstConnection" ValidationGroup="Submit" ErrorMessage="Required"></asp:RequiredFieldValidator>                                    
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Located Outside Network</td>
                            <td align="left">
                                <asp:CheckBox ID="chkIsOutside" runat="server"  />
                            </td>
                            
                        </tr>
                        <tr><%-- FB 2441 --%>
                            <td align="left" class="blackblodtext">Mute</td>
                            <td align="left">
                                <asp:CheckBox ID="chkMute" runat="server"  />
                            </td>
                            
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table width="100%">
                        <tr>
                            <td align="left" class="blackblodtext" width="35%">
                                Address Type<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" runat="server" DataTextField="Name" DataValueField="ID" Width="50%" onchange="javascript:ValidateSelection(this);"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqAddressType" runat="server" InitialValue="-1" ControlToValidate="lstAddressType" ValidationGroup="Submit" ErrorMessage="Required"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Address<span class="reqfldText">*</span></td>
                            <td align="left">                            
                                <asp:TextBox CssClass="altText"  ID="txtAddress" runat="server" width="50%" ></asp:TextBox>                                
                                    <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" runat="server" ErrorMessage="Required" ></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>Invalid Address." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator> --%><%--FB 2267--%>
                                
                                <%--FB 1972--%>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Connection Type<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" CssClass="altText" Width="50%"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqConnectionType" ErrorMessage="Required" runat="server" ControlToValidate="lstConnectionType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Encryption</td>
                            <td align="left">
                                <asp:CheckBox ID="chkEncryption" runat="server" />
                            </td>  
                        </tr>
                    </table>
                </td>
             </tr>
             <% if(Request.QueryString["ifrm"] == null){%> <%--FB 2501 Call monitoring--%>
            <tr>
                <td class="subtitleblueblodtext" align="left" colspan="2">MCU Parameters</td>                            
            </tr>
             <tr>
                <td colspan="2">
                    <table width="100%" border="0">
                        <tr>
                            <td width="2%"></td>
                            <td align="left" width="17%" class="blackblodtext" valign="top">Assigned to MCU<span class="reqfldText">*</span></td>
                            <td align="left"  valign="bottom">
                                <asp:DropDownList CssClass="altLong0SelectFormat" OnSelectedIndexChanged="DisplayBridgeDetails" AutoPostBack="true" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" Width="20%"></asp:DropDownList>
                                <input type='submit' name='SoftEdgeTest1'  style='max-height:0px;max-width:0px;height:0px;width:0px;background-color:Transparent;border:None;'/>
                                <asp:Button id="btnViewMCU" Text="View" runat="server" class="altShortBlueButtonFormat" OnClientClick="javascript:return viewMCU(document.frmAddNewEndpoint.lstBridges.options[document.frmAddNewEndpoint.lstBridges.selectedIndex].value);" Width="12%"/>
                                <% if(Request.QueryString["ifrm"] == null){%> <%--FB 2501 Call monitoring--%>
                                <asp:RequiredFieldValidator ID="reqBridges" runat="server" InitialValue="-1" ControlToValidate="lstBridges" ValidationGroup="Submit" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <%} %>
                            </td>
                         </tr>
                         <tr>
                            <td></td>
                            <td align="left" class="blackblodtext" valign="top">MCU Address Type</td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="lstMCUAddressType" CssClass="altLong0SelectFormat" runat="server" DataTextField="Name" DataValueField="ID" Width="20%" onchange="javascript:ValidateSelection(this);"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqMCUAT" ErrorMessage="Required" runat="server" ControlToValidate="lstMCUAddressType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
             <% } %> <%--FB 2501 Call monitoring--%>
             <tr style="height:20px">
               <td></td>
             </tr>  
             <tr>
                <td align="center" colspan="2">
                  <table>
                    <tr>
                     <td><asp:Button ID="btnCancel" runat="server" CssClass="altShortBlueButtonFormat" Text="Cancel" OnClick="CancelEndpoint" OnClientClick="javascript:DataLoading(1);"  /></td><%--ZD 100176--%> 
                     <% if(Request.QueryString["ifrm"] == null){%> <%--FB 2501 Call monitoring--%>
                     <td><asp:Button ID="btnSubmitAddNew" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit/Add New Endpoint" OnClick="SubmitEndpoint" ValidationGroup="Submit" /></td>
                     <%} %> <%--FB 2501 Call monitoring--%>
                     <td><asp:Button ID="btnSubmit" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit/Go Back" OnClick="SubmitEndpoint" ValidationGroup="Submit" /></td>
                    </tr>
                  </table>
                </td>
             </tr>
        </table>
</center>
    </div>
    </form>
   
<script language="javascript" type="text/javascript">
    if (document.getElementById("hdnEndpointID").value == '') 
    {
        document.getElementById("hdnEndpointID").value = "new";
        var obj = document.getElementById("chkUnlistedEndpoint");
        obj.checked = true;
        OpenEndpointlist(obj);
    }

    function OpenEndpointlist(obj) {
        if (obj.id == "chkListedEndpoint" && obj.checked)
        {
            url = "EndpointSearch.aspx?t=TC&hf=1&DrpValue=";
            window.open(url, "EndpointSearch", "width=" + screen.availWidth + ",height=600px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
        }
        else if (obj.id == "chkUnlistedEndpoint" && obj.checked)
         {
            document.getElementById("chkListedEndpoint").checked = false;
            document.getElementById("txtEndpointName").value = "Add on";
            document.getElementById("lstProtocol").value = "1";
            document.getElementById("lstAddressType").value = "1";
            document.getElementById("lstConnection").value = "2";
            document.getElementById("lstConnectionType").value = "2";
            document.getElementById("txtAddress").value = "";
            document.getElementById("chkIsOutside").checked = false;
            document.getElementById("chkEncryption").checked = false;
            if (document.getElementById("lstBridges") != null) //FB 2501 Call monitoring
                document.getElementById("lstBridges").value = "-1";
            if (document.getElementById("lstMCUAddressType") != null) //FB 2501 Call monitoring
                document.getElementById("lstMCUAddressType").value = "-1";
            document.getElementById("hdnVideoEquipment").value = "0";
            document.getElementById("hdnLineRate").value = "384";
            document.getElementById("hdnApiPortNo").value = "23"; 
        }
    }

    function viewMCU(val)
    {
        var mcuid =  val.split("@")[0];
        
        if(mcuid != "-1" && mcuid != "")
        {
            url = "BridgeDetailsViewOnly.aspx?hf=1&bid="+ mcuid;
            window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
        }
        return false;
    }

    function ValidateSelection(obj) {
        var lstProtocol = document.getElementById("lstProtocol");
        var lstMCUAddressType = document.getElementById("lstMCUAddressType");
        var lstAddressType = document.getElementById("lstAddressType");
        if (obj == lstAddressType) 
        {
            if (lstAddressType.value == 5) {
                lstProtocol.selectedIndex = 4;
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 5;
            }
            else if (lstAddressType.value == 4) {
                lstProtocol.selectedIndex = 2;
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 4;
            }
            else {
                if (lstProtocol.value == 4 || lstProtocol.value == 2)
                    lstProtocol.selectedIndex = 1;
                if(lstMCUAddressType != null){
                if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                    lstMCUAddressType.selectedIndex = 1;
                }
            }
        }
        if (obj == lstMCUAddressType)
         {
            if(lstMCUAddressType != null){
            if (lstMCUAddressType.value == 5) {
                lstProtocol.selectedIndex = 4;
                lstAddressType.selectedIndex = 5;
            }
            else if (lstMCUAddressType.value == 4) {
                lstProtocol.selectedIndex = 2;
                lstAddressType.selectedIndex = 4;
            }
            }
            else {
                if (lstProtocol.value == 4 || lstProtocol.value == 2)
                    lstProtocol.selectedIndex = 1;
                if (lstAddressType.value == 5 || lstAddressType.value == 4)
                    lstAddressType.selectedIndex = 1;
            }
        }
        if (obj == lstProtocol)
         {
            if (lstProtocol.value == 4) {
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 5;
                lstAddressType.selectedIndex = 5;
            }
            else if (lstProtocol.value == 2) {
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 4;
                lstAddressType.selectedIndex = 4;
            }
            else {
                if(lstMCUAddressType != null){
                if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                    lstMCUAddressType.selectedIndex = 1;
                }
                if (lstAddressType.value == 5 || lstAddressType.value == 4)
                    lstAddressType.selectedIndex = 1;
            }
        }
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End
</script>
</body>
</html>

<% if(Request.QueryString["ifrm"] == null){%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<% }%>