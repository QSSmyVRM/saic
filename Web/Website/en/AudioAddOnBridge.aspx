﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.AudioAddOnBridge" %>
<%--FB 2779 starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>
<script type="text/javascript" language="javascript">
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
</head>
<body>
    <form id="frmAudioAddOnBridge" runat="server"><%--ZD 100176--%> 
    <input id="hdnEndpointID" name="hdnWebAccUR" runat="server" value="new" type="hidden" />
    <input id="hdnUserID" name="hdnWebAccUR1" runat="server" type="hidden" />
    <input id="hdninitialTime" name="hdninitialTime" runat="server" type="hidden" />
    <div>
        <table style="width: 90%" border="0" cellpadding="5" align="center">
            <tr>
                <td align="center" colspan="5">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Audio Bridge"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5">
                    <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
            <tr>
            <td></td>
                <td style="width: 20%" align="left" visible="false" class="blackblodtext" id="td2"
                    runat="server">
                    Email to notify
                </td>
                <td style="width: 30%" align="left" visible="false" id="td3" runat="server">
                    <asp:TextBox CssClass="altText" ID="txtEmailtoNotify" runat="server" onblur="CheckSecondaryEmail()"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegtxtEmailtoNotify" ControlToValidate="txtEmailtoNotify" ValidationGroup="Submit"
                        Display="dynamic" runat="server" ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegtxtEmailtoNotify1" ControlToValidate="txtEmailtoNotify" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /() ; ? | ^= ! ` , [ ] { } : # $ and &#34; are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
            <td></td>
                <td style="width: 20%" align="left" class="blackblodtext" valign="top">
                    Name<span class="reqfldstarText">*</span>
                </td>
                <td style="width: 30%" align="left" valign="top">
                    <asp:TextBox ID="txtBridgName" Enabled='<%# Application["ssoMode"].ToString().ToUpper().Equals("NO") %>'
                        CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqtxtBridgName" runat="server" ControlToValidate="txtBridgName"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegtxtBridgName" ControlToValidate="txtBridgName" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ and ~ are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>
                </td>
                <td style="width: 20%" align="left" class="blackblodtext" valign="top" rowspan="2" >
                    Bridge department
                </td>
                <td style="width: 30%" align="left" valign="top" rowspan="2">
                    <asp:ListBox runat="server" ID="lstBridgeDepts" CssClass="altSelectFormat" DataTextField="name"
                        DataValueField="id" Rows="6" SelectionMode="Multiple"></asp:ListBox>
                </td>
            </tr>
            <tr>
            <td></td>
                <td style="width: 20%" align="left" class="blackblodtext" id="Td4" runat="server"
                    valign="top">
                    Time-zone<span class="reqfldstarText">*</span>
                </td>
                <td style="width: 30%" align="left" id="Td5" runat="server" valign="top">
                    <asp:DropDownList ID="lstBridgetimezone" runat="server" CssClass="altSelectFormat"
                        DataTextField="timezoneName" DataValueField="timezoneID">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstBridgetimezone" ControlToValidate="lstBridgetimezone"
                        ValidationGroup="Submit" ErrorMessage="Required" InitialValue="-1" Display="dynamic"
                        runat="server"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr id="trLblAV" runat="server" style="display: none">
                <td align="left" colspan="5">
                    <table cellspacing="5">
                        <tr>
                            <td style="width: 20">
                                &nbsp;
                            </td>
                            <td>
                                <span class="subtitleblueblodtext">Determine Audio/Video Connection Parameters</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" class="blackblodtext">
                    Endpoint Profile Name<span class="reqfldstarText">*</span>
                </td>
                <td align="left"><%--FB 2655 --%>
                    <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Width="250px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtEndpointName" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="ReqtxtEndpointName" runat="server" ControlToValidate="txtEndpointName"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </td>
                <%--FB 2655 Start--%>
                 <td align="left" class="blackblodtext">
                   Email Address
                </td>
                <td align="left">
                    <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="altText" Width="250px"></asp:TextBox>
                      <asp:RegularExpressionValidator ID="regEmailAddres_1" ControlToValidate="txtEmailAddress" ValidationGroup="Submit" Display="dynamic" runat="server" 
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                      <asp:RegularExpressionValidator ID="regEmailAddres_2" ControlToValidate="txtEmailAddress" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
                <%--FB 2655 End--%>
            </tr>
            <tr>
            <td></td>
                <td align="left" class="blackblodtext">
                    Address Type<span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:DropDownList ID="lstAddressType" CssClass="altSelectFormat" DataTextField="Name"
                        DataValueField="ID" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstAddressType" ControlToValidate="lstAddressType"
                        ValidationGroup="Submit" ErrorMessage="Required" InitialValue="-1" Display="dynamic"
                        runat="server"></asp:RequiredFieldValidator>
                </td>
                <td align="left" class="blackblodtext">
                    Address<span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = `<br> [ ] { } $ and ~ are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="ReqtxtAddress" runat="server" ControlToValidate="txtAddress"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" class="blackblodtext">
                    Default Protocol<span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:DropDownList CssClass="altSelectFormat" ID="lstProtocol" runat="server" DataTextField="Name"
                        DataValueField="ID">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstProtocol" ControlToValidate="lstProtocol" ValidationGroup="Submit"
                        ErrorMessage="Required" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                </td>
                <td align="left" class="blackblodtext">
                    Outside Network?
                </td>
                <td align="left">
                    <asp:DropDownList ID="lstIsOutsideNetwork" CssClass="altText" runat="server">
                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trMCUnumber" runat="server">
            <td></td>
                <td align="left" class="blackblodtext">
                    Internal Video MCU Number
                </td>
                <td align="left">
                    <asp:TextBox ID="txtIntVideoMcuNo" CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtIntVideoMcuNo" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
                <td align="left" class="blackblodtext">
                    External Video MCU Number
                </td>
                <td align="left">
                    <asp:TextBox ID="txtExtVideoMcuNo" CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtExtVideoMcuNo" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ` , [ ] { } : $ @ ~ and &#34; are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|`,\[\]{}\x22;=:@$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" class="blackblodtext">
                    Leader Pin
                </td>
                <td align="left">
                    <asp:TextBox CssClass="altText" ID="txtLeaderPin" runat="server"></asp:TextBox>
                </td>
                <td align="left" class="blackblodtext">
                    Conference Code
                </td>
                <td align="left">
                    <asp:TextBox CssClass="altText" ID="txtConfCode" runat="server"></asp:TextBox>
                </td>
            </tr>
            
            <%--FB 2655 - DTMF Start--%>
            <tr>
                <td align="left" colspan="5">
                                <span class="subtitleblueblodtext">DTMF Settings</span>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%">
                    Pre Conference Code
                </td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="PreConfCode" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regPreConfCode" ControlToValidate="PreConfCode"
                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                        ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ and &#34; are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%">
                    Pre Leader Pin
                </td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="PreLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regPreLeaderPin" ControlToValidate="PreLeaderPin"
                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                        ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ and &#34; are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%">
                    Post Leader Pin
                </td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="PostLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regPostLeaderPin" ControlToValidate="PostLeaderPin"
                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                        ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ and &#34; are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%">
                    Audio Dial-In Prefix
                </td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="AudioDialInPrefix" runat="server" CssClass="altText"></asp:TextBox>
                </td>
            </tr>
            <%--FB 2655 - DTMF End--%>
            
            <tr>
              <td style="height:20px" colspan="5">
              </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Button runat="server" ID="btnReset" Text="Reset" ValidationGroup="Reset" CssClass="altMedium0BlueButtonFormat"
                        OnClick="ResetPage" OnClientClick="javascript:DataLoading(1)" /> <%--ZD 100176--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnGoBack" Text="Go Back" CssClass="altMedium0BlueButtonFormat"
                        OnClick="GobackToParentPage" OnClientClick="javascript:DataLoading(1)" /><%--ZD 100176--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnnewSubmi"  Text="Submit / Add New Audio bridge"
                        ValidationGroup="Submit" OnClick="SubmitAudioBridge" Width="250px" /> <%--FB 2796--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnnewSubmit" runat="server" 
                        Text="Submit" ValidationGroup="Submit" OnClick="SubmitAudioBridge" Width="250px" /><%--FB 2796--%>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
