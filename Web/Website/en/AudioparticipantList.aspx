﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AudioparticipantList.aspx.cs" Inherits="en_AudioparticipantList" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe"%>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
        function grid_SelectionChanged(s, e) {
    s.GetSelectedFieldValues("ifrmDetails", GetSelectedFieldValuesCallback);
}

 

function GetSelectedFieldValuesCallback(values)
 {    
   var partyinfo = "";
   var nonAudiouser = document.getElementById("nonAudiouser");
        //alert(partyinfo);
        //alert(values);
        for(var i = 0; i < values.length; i ++)
         {
                partyinfo += Addparticipants(values[i]);
         }

         //alert(nonAudiouser.value);
        parent.opener.document.getElementById("hdnAudioBridges").value = partyinfo //FB 2359
        if(nonAudiouser.value != "")
                partyinfo = partyinfo + nonAudiouser.value;
                
        parent.opener.document.getElementById("txtPartysInfo").value = partyinfo;
		parent.opener.ifrmPartylist.location.reload(true);
     
    }
        
    function Addparticipants (pinfo)
    {
    
	pary = pinfo.split("|");
	pid = pary[0]; pfn = pary[1]; pln = pary[2]; pemail = pary[3]; ischk = pary[4];
	pno =  parseInt(pary[5], 10) ;
	
	if (pemail == "") 
      {        
        alert("Users without email addresses cannot be added as participants.");
        return ;
      }
		
	var partysinfo = "";//parent.opener.document.getElementById("txtPartysInfo").value;
			
	//if (partysinfo.indexOf("," + pemail + ",") == -1) 
	if (partysinfo.indexOf("!!" + pemail + "!!") == -1) //FB 1888
	{
	    
	    var txtTemp;
	    //txtTemp = ",1,0,0,1,0,1,";//Default audio..
	    txtTemp = "!!1!!0!!0!!1!!0!!1!!";//Default audio //FB 1888
        if (pno == 0)
            txtTemp = "!!1!!0!!0!!1!!1!!0!!";//FB 1888
            //txtTemp = ",1,0,0,1,1,0,";
        if (pno == 1)
            txtTemp = "!!1!!0!!0!!1!!0!!1!!";//FB 188
            //txtTemp = ",1,0,0,1,0,1,";

        mp = pid + "!!" + pfn + "!!" + " " + "!!" + pemail + txtTemp; //FB 2388
		//mp = pid + "," + pfn + "," + pln + "," + pemail + txtTemp;

		mp += "!!!!!!-5!!!!!!0!!0!!0!!0||";//FB 1888//FB 2348 //FB 2550
		//mp += ",,,-5,,,;";
      if (pemail == "") 
      {        
        alert("Users without email addresses cannot be added as participants.");
      }
      else
		if ( (partysinfo.indexOf("!!" +pemail + "!!") == -1)) {
			partysinfo += mp
		} else {
			lp = partysinfo.lastIndexOf("||",partysinfo.indexOf("!!" + pemail + "!!")) + 1;
			rp = partysinfo.indexOf("||",partysinfo.indexOf("!!" + pemail + "!!")) + 1;
			
			newpartysinfo = partysinfo.substring(0, (lp=="-1") ? 0 : lp);
			newpartysinfo += mp;
			newpartysinfo += partysinfo.substring(rp, partysinfo.length);
			partysinfo = newpartysinfo;		
			
		}
	} 
	
	return 	partysinfo ;
			
			
}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Audio Participants</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width = "100%">
    <tr>
            <td align="center">
            
            <input type="hidden" id="partysValue" name="partysValue" value=""/>
            <input type="hidden" id="nonAudiouser" runat="server"  name="nonAudiouser" value=""/>
            <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="100%"  CssClass="treeSelectedNode" >
                <table width="100%" align="center" border="0">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center"> <%--FB 2023--%>
                        <dxwgv:ASPxGridView AllowSort="true" ID="grid"   ClientInstanceName="grid" runat="server" KeyFieldName="userID" Width="100%" EnableRowsCache="True" OnDataBound="ASPxGridView1_DataBound">
															    <Columns>
															        <dxwgv:GridViewCommandColumn  ShowSelectCheckbox="True" VisibleIndex="0"></dxwgv:GridViewCommandColumn>
																    <dxwgv:GridViewDataTextColumn FieldName="firstName" Caption="Bridge Name" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"/>
																    <dxwgv:GridViewDataTextColumn FieldName="lastName" Caption="Last name" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center" Visible="false"/>
																    <dxwgv:GridViewDataTextColumn FieldName="login" Caption="Login Name" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center" Visible="false"/>
																    <dxwgv:GridViewDataCheckColumn FieldName="audioaddon" VisibleIndex="4" Visible="false">
                                                                    <PropertiesCheckEdit DisplayTextChecked="Yes" DisplayTextUnchecked="No" />
                                                                    </dxwgv:GridViewDataCheckColumn>
                                                                    
																    <dxwgv:GridViewDataTextColumn FieldName="userEmail" Caption="Email" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center" Width="32%" Visible="false"/>
																    <dxwgv:GridViewDataTextColumn FieldName="audioDialIn" Caption="Audio Dial-In #" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center" />
																    <dxwgv:GridViewDataTextColumn FieldName="confCode" Caption="Conference Code" VisibleIndex="7" HeaderStyle-HorizontalAlign="Center" />
																    <dxwgv:GridViewDataTextColumn FieldName="leaderPin" Caption="Leader Pin" VisibleIndex="8" HeaderStyle-HorizontalAlign="Center" />
            
																    <dxwgv:GridViewDataColumn FieldName="userID" Visible="False" />
																    <dxwgv:GridViewDataColumn FieldName="ifrmDetails" Visible="False" />
															    </Columns>
															    <Styles><CommandColumn Paddings-Padding="1"/></Styles>
															    <SettingsBehavior AllowMultiSelection="false" />
															    <%--<Settings ShowFilterRow="true" />--%><%--commented for FB 2359--%>
															    <Settings ShowFilterRowMenu="true" />
															    <SettingsPager Mode="ShowPager"  PageSize="10" AlwaysShowPager="true"  Position="Bottom"></SettingsPager>
															    <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
														    </dxwgv:ASPxGridView>
                      </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                     <input align="middle" type="button" runat="server" style="cursor:pointer" id="ClosePUp" value=" Close " onclick="javascript:window.close();" class="altMedium0BlueButtonFormat"  />
                      </td>
                    </tr>
                </table>
              </asp:Panel>
            </td>
           
        </tr>
    </table>
    </div>
    </form>
</body>


</html>
