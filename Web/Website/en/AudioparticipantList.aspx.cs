﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

public partial class en_AudioparticipantList : System.Web.UI.Page
{
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    String partystring = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("AudioparticipantList.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            log = new ns_Logger.Logger();

            if (Request.QueryString["partys"] != null)
                partystring = Request.QueryString["partys"].ToString().Trim().Replace("@@","++"); //FB 1640
            //if (Session["DtParticipant"] == null)
            //    Session.Add("DtParticipant", populateAudiousers());

            //FB 2359 Start
            if (Application["Client"].ToString().ToUpper() == "DISNEY")
            {
                grid.Settings.ShowFilterRow = false;
            }
            else
            {
                grid.Settings.ShowFilterRow = true;
            }
            //FB 2359 End

            SetAudioParticipant();

        }
        catch (Exception ex)
        {

            log.Trace(ex.ToString());
        }

    }

    public DataTable populateAudiousers()
    {
        String inXML1 = "";
        String outXML1 = "";
        XmlDocument xmldoc = null;
        DataTable dt = null;
        try
        {
            inXML1 = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";
            outXML1 = obj.CallMyVRMServer("GetAudioUserList", inXML1, Application["COM_ConfigPath"].ToString());
            xmldoc = new XmlDocument();
            dt = new DataTable();
            if (outXML1.IndexOf("Error") <= 0)
            {
                xmldoc.LoadXml(outXML1);
                XmlNodeList nodes = xmldoc.SelectNodes("//users");
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    

                    if (!dt.Columns.Contains("ifrmDetails"))
                        dt.Columns.Add("ifrmDetails");

                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["ifrmDetails"] = dr["userID"].ToString() + "|" + dr["firstName"].ToString() + "|" + dr["lastName"].ToString() + "|" + dr["userEmail"].ToString() + "|1|" + dr["audioaddon"].ToString();

                    }


                }

            }
        }
        catch (Exception)
        {

            throw;
        }


        return dt;

    }

    protected void SetAudioParticipant()
    {
        try
        {
            grid.DataSource = populateAudiousers(); //(DataTable)Session["DtParticipant"];
            grid.DataBind();

        }
        catch (Exception ex)
        {

            log.Trace(ex.ToString());
        }
    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        bool isuserpresent = false;
        try
        {
            String[] delimiter = { "!!" }; //FB 1888
            String[] delimiter1 = { "||" }; //FB 1888
            if (!IsPostBack)
            {
                if (partystring != "")
                {
                    string[] partys = partystring.Split(delimiter1, StringSplitOptions.RemoveEmptyEntries);//FB 1888
                    //string[] partys = partystring.Split(';');

                    for (int j = 0; j < partys.Length ; j++)
                    {
                        isuserpresent = false;
                        string temp = partys[j].ToString();
                        int tmpUserId = 0; //FB 1734
                        if (temp != "")
                        {
                            for (int i = 0; i < this.grid.VisibleRowCount; i++)
                            {
                                tmpUserId = 0; //FB 1734
                                Int32.TryParse(temp.Split(delimiter , StringSplitOptions.RemoveEmptyEntries)[0], out tmpUserId); //FB 1888
                                //Int32.TryParse(temp.Split(',')[0], out tmpUserId); //FB 1734
                                if (this.grid.GetRowValues(i, "userID") != null)
                                {

                                    if (Convert.ToInt32(this.grid.GetRowValues(i, "userID")) == tmpUserId) //FB 1734
                                    {
                                        this.grid.Selection.SelectRow(i); 
                                        isuserpresent = true;
                                    }
                                }
                            }

                            if (!isuserpresent)
                            {

                                if (!nonAudiouser.Value.Contains(temp))
                                    nonAudiouser.Value += temp + "||";//FB 1888
                            }
                        }

                    }
                }
            }

        }
        catch (Exception ex)
        {

            log.Trace(ex.ToString());
        }
        
    }

    
}
