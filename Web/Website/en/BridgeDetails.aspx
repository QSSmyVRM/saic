<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Bridges.BridgeDetails" Buffer="true" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %> <%--FB 1938--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!--Window Dressing-->
<% if (Request.QueryString["hf"] != null && Request.QueryString["hf"].ToString().Equals("1"))
   {
%>
        <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<% }
else
    {
%>
        <!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<%
    }
%>

<script type="text/javascript">

  var servertoday = new Date(); //FB 1938

</script>
<script language="javascript">
//FB 1766 - Start
function disableservname()
{
    var enable = document.getElementById("chkEnableIVR");
    if((enable != null) && (enable.checked != true) )
    {
      reqServName.innerText = "";
      document.getElementById("txtIVRName").value = "";
      document.getElementById("txtIVRName").disabled = true;
    }
    else
    {
      document.getElementById("txtIVRName").disabled = false;
    }
}
function RequireIVRName()
{   
    if(document.getElementById("chkEnableCDR") != null) //FB 2660
        CDRDays();
    var txtIVRName = document.getElementById("txtIVRName");
    var enable = document.getElementById("chkEnableIVR");
    if (enable != null)
    {
        if (enable.checked && txtIVRName.value == '')
        {
        reqServName.style.display = 'block';
        reqServName.innerText = 'Please specify IVR Service Name';
        document.getElementById("txtIVRName").focus();
        return false;
        }
    }
    if (!Page_ClientValidate())//FB 2714
               return Page_IsValid;
}
//FB 1766 - End
function DeleteApprover(id)
{
    document.getElementById("hdnApprover" + id).value="";
    document.getElementById("txtApprover" + id).value="";
    document.getElementById("hdnApprover" + id + "_1").value="";
    document.getElementById("txtApprover" + id + "_1").value="";
}

function SavePassword()
{
    for(i=1;i<5;i++)
    {
        document.getElementById("txtApprover" + i).value = document.getElementById("txtApprover" + i + "_1").value;
        document.getElementById("hdnApprover" + i).value = document.getElementById("hdnApprover" + i + "_1").value;
    }
}

function ShowISDN(obj)
{
    if (obj.checked)
        document.getElementById("trISDN").style.display="";
    else
        document.getElementById("trISDN").style.display="none";
}



function getYourOwnEmailList (i)	// -1, 0, 1, 2
{
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmBridgesetting&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management	
	url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmBridgesetting&n=" + i; //Login Management
	
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2735
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2735
	        winrtc.focus();
		}
}
//FB 1937 - Start
function ShowHideRow(frm)
{
    var trow1 = document.getElementById("trMPIServices");
    var trow2 = document.getElementById("trIPServices");
    var trow3 = document.getElementById("trMCUCards");
    var trow4 = document.getElementById("trISDN1");
    var trow5 = document.getElementById("trE164Services"); //FB 2636
    var chkExpand = document.getElementById("chkExpandCollapse")
    var frmCheck, frmVal
    frmVal = 0;
    if(frm)
        frmVal = frm;
    
    if(chkExpand)
    {
        if(frmVal == 1)
            chkExpand.checked = true;
        
        frmCheck = chkExpand.checked;
        
        if (frmCheck == true)
        {   
            if(trow1 != null)
                trow1.style.display = "none";
            if(trow2 != null)
                trow2.style.display = "none";
            if(trow3 != null)
                trow3.style.display = "none";
            if(trow4 != null)
                trow4.style.display = "none";      
           if(trow5 != null) //FB 2636
                trow5.style.display = "none";                                     
        }
        else
        {
            if(trow1 != null)
                trow1.style.display = "block";
            if(trow2 != null)
                trow2.style.display = "block";
            if(trow3 != null)
                trow3.style.display = "block";
            if(trow4 != null)
                trow4.style.display = "block"; 
            if(trow5 != null) //FB 2636
                trow5.style.display = "block";   
        }
    }
    
    if(frmVal == 1)
        return true;
}


function fnShowHide(arg)
{
    var varg
    
    if(arg == 2)
    {
        varg = 2;
        arg = 1;
    }
    
    if (arg == '1')    
      document.getElementById("UsageReportDiv").style.display = 'block';
    else if (arg == '0')
    {
      document.getElementById("UsageReportDiv").style.display = 'none';
      return false;
    }
    
    if(varg == 2)
        ShowHideRow(1);
    
    return true; 
}

function fnCheck()
{
    var rptTime = document.getElementById("ReportTime_Text");
    if(rptTime)
    {
        if(rptTime.value == "")
        {
            document.getElementById("reqStartTime").style.display = 'block';
            return false;
        }
        else if('<%=Session["timeFormat"]%>' == "0")
        {
            if(rptTime.value.search(/[0-2][0-9]:[0-5][0-9]/)==-1 ) 
            {
                document.getElementById("regStartTime").style.display = 'block';
                rptTime.focus();
                return false;
            }
        }
        else if('<%=Session["timeFormat"]%>' == "2")//FB 2588
        {
            if(rptTime.value.search(/[0-2][0-9][0-5][0-9][Z|z]/)==-1 ) 
            {
                document.getElementById("regStartTime").style.display = 'block';
                rptTime.focus();
                return false;
            }
        }
        else if (rptTime.value.search(/[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/)==-1)
        {
            document.getElementById("regStartTime").style.display = 'block';
            rptTime.focus();
            return false;
        }   
    }
    
    var rptDate = document.getElementById("ReportDate");
    if(rptDate)
    {
        if(rptDate.value == "")
        {
            document.getElementById("reqStartData").style.display = 'block';
            return false;
        }
        else if (rptDate.value.search(/(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/)==-1) 
        {
            document.getElementById("regStartDate").style.display = 'block';
            rptTime.focus();
            return false;
        }
    }
    

    return true;
}  
//FB 1937 - End

    function fnCancel() //FB 2565
	{
	    DataLoading(1); // ZD 100176
		window.location.replace('ManageBridge.aspx');  //CSS Project
	}


//FB 2660 Starts
function CDRDays() {
        var enableCDR=document.getElementById("chkEnableCDR");

        if ((enableCDR != null) && enableCDR.checked) {//FB 2714
            document.getElementById("spCDREvent").style.display = 'block';
            document.getElementById("divtxtCDR").style.display = 'block';
            //FB 2714 Start
            ValidatorEnable(document.getElementById("regDeleteCBR"), true);
            document.getElementById("regDeleteCBR").enabled = "true";
            //FB 2714 End
        }
        else {
            //FB 2714 Start
            ValidatorEnable(document.getElementById("regDeleteCBR"), false);
            document.getElementById("regDeleteCBR").enabled = "false";
            //FB 2714 End
            document.getElementById("spCDREvent").style.display = 'none';
            document.getElementById("divtxtCDR").style.display = 'none';
        }
    }
//FB 2660 Ends

//FB 2714 Starts
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
//FB 2714 Ends
//FB 3054 Starts
function PasswordChange(par) 
{
    document.getElementById("hdnPasschange").value = true;
    document.getElementById("confPassword").value = false;
    if (par == 1)
        document.getElementById("hdnPW1Visit").value = true;
    else
        document.getElementById("hdnPW2Visit").value = true;
}  
function DMAPasswordChange(par) 
{
    document.getElementById("hdnDMSPW").value = true;
    if (par == 1)
        document.getElementById("hdnDMA1Visit").value = true;
    else
        document.getElementById("hdnDMA2Visit").value = true;
} 
 function fnTextFocus(xid,par) {
 
          // ZD 100263 Starts
    var obj1 = document.getElementById("txtDMAPassword");
    var obj2 = document.getElementById("txtDMARetypePwd");
    var obj3 = document.getElementById("txtPassword1");
    var obj4 = document.getElementById("txtPassword2");
    
    
    var obj = document.getElementById(xid);
    if (par == 1) {
     // ZD 100263 Starts
    if (obj3.value == "" && obj4.value == "") {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
        document.getElementById("txtPassword1").value = "";
        document.getElementById("txtPassword2").value = "";
    }
    return false;
    // ZD 100263 Ends
        if(document.getElementById("hdnPW2Visit") != null)
        {
            if(document.getElementById("hdnPW2Visit").value == "false")
            { 
            document.getElementById("txtPassword1").value = ""; 
            document.getElementById("txtPassword2").value = "";
            }
            else
            {
            document.getElementById("txtPassword1").value = "";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = ""; 
            document.getElementById("txtPassword2").value = "";
        }
    }
    else if (par == 2)
    {
     // ZD 100263 Starts
    if (obj3.value == "" && obj4.value == "") {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
        document.getElementById("txtPassword1").value = "";
        document.getElementById("txtPassword2").value = "";
    }
    return false;
    // ZD 100263 Ends
        if(document.getElementById("hdnPW1Visit") != null)
        {
            if(document.getElementById("hdnPW1Visit").value == "false")
            {
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword2").value = "";
            }
            else
            {
                document.getElementById("txtPassword2").value = "";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = ""; 
            document.getElementById("txtPassword2").value = "";
        }

    }
    else if(par ==3)
    {
if (obj1.value == "" && obj2.value == "") {
        document.getElementById("txtDMAPassword").style.backgroundImage = "";
        document.getElementById("txtDMARetypePwd").style.backgroundImage = "";
        document.getElementById("txtDMAPassword").value = "";
        document.getElementById("txtDMARetypePwd").value = "";
    }
    return false;
        if(document.getElementById("hdnDMA2Visit") != null)
        {
            if(document.getElementById("hdnDMA2Visit").value == "false")
            { 
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
            }
            else
            {
            document.getElementById("txtDMAPassword").value = "";
            }
        }
        else
        {
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
        }
    }
    else
    {if (obj1.value == "" && obj2.value == "") {
        document.getElementById("txtDMAPassword").style.backgroundImage = "";
        document.getElementById("txtDMARetypePwd").style.backgroundImage = "";
        document.getElementById("txtDMAPassword").value = "";
        document.getElementById("txtDMARetypePwd").value = "";
    }
    return false;
        if(document.getElementById("hdnDMA1Visit") != null)
        {
            if(document.getElementById("hdnDMA1Visit").value == "false")
            { 
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
            }
            else
            {
            document.getElementById("txtDMARetypePwd").value = "";
            }
        }
        else
        {
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
        }
    }
        
        
        
        if(document.getElementById("cmpValPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
         if(document.getElementById("cmp2")!= null)
        {
            ValidatorEnable(document.getElementById('cmp2'), false);
            ValidatorEnable(document.getElementById('cmp2'), true);
        }
           if(document.getElementById("CmptxtDMAPassword")!= null)
        {
            ValidatorEnable(document.getElementById('CmptxtDMAPassword'), false);
            ValidatorEnable(document.getElementById('CmptxtDMAPassword'), true);
        }
            if(document.getElementById("CmptxtDMARetypePwd")!= null)
        {
            ValidatorEnable(document.getElementById('CmptxtDMARetypePwd'), false);
            ValidatorEnable(document.getElementById('CmptxtDMARetypePwd'), true);
        }
}
//FB 3054 Ends
//ZD 100176 start
function DataLoading(val) 
{
 if (val == "1")
    document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
 else
    document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MCU Details</title>
    <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
    <script type="text/javascript" src="inc/functions.js"></script>
	<%--FB 1938--%>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1938--%> <%--FB 1982--%>
    
</head>
<body>
    <form id="frmMCUManagement" runat="server" method="post" onsubmit="return true;DataLoading(1);"> <%--ZD 100176--%> 
    <input id="confPassword" runat="server" type="hidden" />
    <input id="txtApprover1_1" runat="server" type="hidden" />
    <input id="txtApprover2_1" runat="server" type="hidden" />
    <input id="txtApprover3_1" runat="server" type="hidden" />
    <input id="txtApprover4_1" runat="server" type="hidden" />
    <input id="hdnApprover1_1" runat="server" type="hidden" />
    <input id="hdnApprover2_1" runat="server" type="hidden" />
    <input id="hdnApprover3_1" runat="server" type="hidden" />
    <input id="hdnApprover4_1" runat="server" type="hidden" />
    <%--Code changed for FB 1425 QA Bug -Start--%>
      <input type="hidden" id="hdntzone" runat="server"/>
      <%--Code changed for FB 1425 QA Bug -End--%>
       <input type="hidden" id="hdnMcuCount" runat="server"/> <%--FB 2486 --%>
       <input type="hidden" id="hdnRPRMUserid" runat="server"/> <%--FB 2709 --%>
       <input type="hidden" id="hdnRPRMCallCount" runat="server"/> <%--FB 2709 --%>
       <input type="hidden" id="hdnPasschange" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnDMSPW" value="false" runat="server"/> <%--FB 3054--%>
        <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--FB 3054--%>
        <input type="hidden" id="hdnDMA1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnDMA2Visit" value="false" runat="server"/> <%--FB 3054--%>
      <%--FB 1938 - Start --%>
     <div id="UsageReportDiv"  runat="server" align="center" style="top: 170px;left:105px; POSITION: absolute; WIDTH:92%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none"> 
      <table width="75%" border="1" style="border-color:Blue;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
              <table cellpadding="2" cellspacing="1"  width="100%" class="tableBody" align="center">
                 <tr>
                    <td  class="subtitleblueblodtext" align="center" colspan="2">
                        MCU Port Usage
                    </td>            
                 </tr>
                 <tr>
                    <td width="27%"  style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                        Total Audio/Video Used
                    </td> 
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblVideoused" runat="server" ></asp:Label>
                   </td>           
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                        Total Audio/Video Available 
                    </td> 
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblVideoavail" runat="server" ></asp:Label>
                   </td>            
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                        Total Audio Only Used 
                    </td>
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblAudioused" runat="server" ></asp:Label>
                   </td>             
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                         Total Audio Only Available 
                    </td>
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblAudioavail" runat="server" ></asp:Label>
                   </td>             
                 </tr>
                 <tr>
                    <td  class="subtitleblueblodtext" align="center" colspan="2">
                        Conferences
                    </td>            
                 </tr>
                 <tr>
                    <td align="center" colspan="2">
                       <div  style="width: 95%;height: 350px;overflow: auto;" id="dd" runat="server"  >
                            <asp:DataGrid ID="dgUsageReport" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="100%" AutoGenerateColumns="false" ShowFooter="false" runat="server" style="border-collapse:separate">
                                 <HeaderStyle HorizontalAlign="Left" />
                                <Columns> <%-- FB 2050 Starts --%>
                                    <asp:BoundColumn HeaderText="Conference <br> Name" DataField="ConfName"  ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Conf #" DataField="ConfID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Start Date" DataField="StartDate" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="End Date" DataField="EndDate" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Audio Port <br> Used" DataField="AudioOnly" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Video Port <br> Used" DataField="AudioVideo" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns> <%-- FB 2050 Ends --%>
                            </asp:DataGrid>
                            <asp:Label ID="lblDetails" Text="No Details found" CssClass="lblError" runat="server"></asp:Label>
                        </div>
                    </td>
                  </tr>
                  <tr>
                   <td align="center" colspan="2">
                      <asp:Button ID="BtnClose" Text="Close" CssClass="altMedium0BlueButtonFormat" Runat="server" OnClientClick="javascript:return fnShowHide('0')"></asp:Button>
                   </td>
                  </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
      <%--FB 1938 - End --%>
    
    <div>
      <input type="hidden" id="helpPage" value="65">
        
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="left" style="font-weight:bold">
                  <asp:Label ID="lblRequired" ForeColor="red" Font-Size="XX-Small" Text="* Required" runat="server" Font-Italic="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <asp:CustomValidator id="customvalidation" runat="server" display="dynamic" OnServerValidate="ValidateIP" />           
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Basic Configuration</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Name<span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtMCUID" Visible="false" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtMCUName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMCUName" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regMCUName" ControlToValidate="txtMCUName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Login<span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtMCULogin" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMCULogin" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="Submit" ControlToValidate="txtMCULogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator> <%--Polycom CMA--%>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Password<%--<span class="reqfldstarText">*</span>FB 1408--%></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" TextMode="Password" CssClass="altText" runat="server" onblur="PasswordChange(1)" onfocus=" fnTextFocus(this.id,1)" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ControlToValidate="txtPassword1" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <%--<asp:RegularExpressionValidator ID="reg1" ValidationExpression="{1,128}" ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<br>Only numeric passwords between 1000-9999 are allowed." runat="server"></asp:RegularExpressionValidator>
                                --%><asp:CompareValidator ID="cmpValPassword1" runat="server" ValidationGroup="Submit" ControlToCompare="txtPassword2"
                                    ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="Submit" ControlToValidate="txtPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Retype Password<%--<span class="reqfldstarText">*</span>FB 1408--%></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" ID="txtPassword2" TextMode="Password" runat="server" onblur="PasswordChange(2)" onfocus=" fnTextFocus(this.id,2)"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <asp:CompareValidator ID="cmp2" runat="server" ControlToCompare="txtPassword1"
                                    ControlToValidate="txtPassword2" Display="Dynamic" ValidationGroup="Submit" ErrorMessage="<br>Passwords do not match."></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td width="20%" align="left" style="font-weight:bold" id ="TzTD1" runat="server" class="blackblodtext">MCU Time Zone</td>
                            <td width="30%" align="left" id ="TzTD2" runat="server">
                                <asp:DropDownList ID="lstTimezone" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -End--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Type</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstMCUType" CssClass="altSelectFormat" OnSelectedIndexChanged="ChangeFirmwareVersion" AutoPostBack="true" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                                <asp:DropDownList ID="lstInterfaceType" Visible="false" DataTextField="interfaceType" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Status</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstStatus" CssClass="altSelectFormat" runat="server" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Virtual MCU</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkIsVirtual" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Administrator<span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover4" ReadOnly="true" EnableViewState="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(3);" onmouseover="window.status='';return true;"  id="imgAdminsearch" runat="server"><img border="0" src="image/edit.gif" alt="edit" WIDTH="17" HEIGHT="15" style="cursor:pointer;" title="myVRM Address Book"></a> <%--FB 2798--%>                                
                                <asp:TextBox ID="hdnApprover4" runat="server" Width="0" Height="0" ForeColor="transparent" BorderColor="transparent"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Submit" ErrorMessage="Required" ControlToValidate="hdnApprover4" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Firmware Version</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstFirmwareVersion" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        
                        <%--Api port starts--%>
                        <tr>
                        <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">API Port</td>
                        <td width="30%" align="left">
                        <asp:TextBox ID="txtapiportno" CssClass="altText" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ValidationGroup="Submit" ControlToValidate="txtapiportno" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </td>
                        <%--FB 1920 - Starts--%>
                        <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">
                            <asp:Label ID="lblIsPublic" runat="server">Public</asp:Label>
                        </td>
                        <td width="30%" align="left">
                            <asp:CheckBox ID="chkbxIsPublic" runat="server" />
                        </td>
                        <%--FB 1920 - Ends--%>
                        </tr>
                        <%--Api port ends--%>
                        <%--FB 1937--%>
                         <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><label id="lblAVPorts" runat="server">Total Audio/Video Ports</label></td> <%--window dressing FB 1937--%><%--FB 2659--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtMaxVideoCalls" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ValidationGroup="Submit" ControlToValidate="txtMaxVideoCalls" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numreic values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                            </td>
                            <td  width="20%" align="left" id="tdAudio"  style="font-weight:bold" class="blackblodtext" runat="server"><label id="lblAudioPorts" runat="server">Total Audio Only Ports</label></td> <%--window dressing FB 1937--%><%--FB 2659--%>
                            <td  width="30" align="left" id="tdtxtAudio" runat="server"> 
                                <asp:TextBox ID="txtMaxAudioCalls" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ValidationGroup="Submit" ControlToValidate="txtMaxAudioCalls" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--ZD 100113 Starts--%>
                         <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">URL Access</td> 
                            <td width="30%" align="left">
                                <asp:DropDownList ID="drpURLAccess" CssClass="altSelectFormat" runat="server">
                                    <asp:ListItem Text="http" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="https" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                         <%--ZD 100113 End--%>
                        <%--FB 2660 Starts--%>
                        <tr>
                            <td width="20%" align="left"> <span class="blackblodtext" id="spEnableCDR" runat="server"> Enable CDR </span></td>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkEnableCDR" runat="server" onclick="javascript:CDRDays();" /> 
                            </td>
                            <td  width="20%" align="left"> <span class="blackblodtext" id="spCDREvent" runat="server">Keep CDR Events</span></td>
                            <td  width="30" align="left" > 
                            <div id="divtxtCDR" runat="server" nowrap="nowrap"> <%--FB 2714 Starts--%>
                                <asp:TextBox ID="txtdltCDR" CssClass="altText" onkeypress="return isNumberKey(event)" runat="server" Width="50px" MaxLength="3"></asp:TextBox> (days)
                                <asp:RegularExpressionValidator ID="regDeleteCBR" Enabled="false" ValidationGroup="Submit" ControlToValidate="txtdltCDR"
                                 Display="dynamic" runat="server" SetFocusOnError="true"
                                ErrorMessage="<br>Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator><%--FB 2714 End--%>
                            </div>
                            </td>
                         </tr>
                         <%--FB 2660 Ends--%>
                        <tr style="display:none">
                            <%--FB 2501 CallMonitor Favourite Starts--%>
                            <td width="20%" align="left"  class="blackblodtext">Set Favourite</td> <%--FB 2591--%>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkSetFavourite" runat="server" /> <%--FB 2591--%>
                            </td>
                            <%--FB 2501 Ends--%>
                            <td width="20%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                  
                         
                         </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>MCU Approvers</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Primary Approver</td><%--window dressing--%>
                            <td width="800px" align="left"><%--Edited For FF--%>
                                <asp:TextBox ID="txtApprover1" ReadOnly="true" EnableViewState="true" CssClass="altText" runat="server"></asp:TextBox><%--FB 2659--%>
                                <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;" id="imgApprover1" runat="server" ><img border="0" src="image/edit.gif" style="vertical-align:top" alt="edit" WIDTH="17" HEIGHT="15" style="cursor:pointer;" title="myVRM Address Book"></a><%--Edited For FF FB 2798--%>
                                <img border="0" src="image/btn_delete.gif" id="imgDelApprover1" runat="server"  style="vertical-align:top;cursor:pointer;" alt="delete" WIDTH="16" HEIGHT="16" onclick="javascript:DeleteApprover(1)"  title="Delete" ><%--Edited For FF FB 2798--%> 
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover1" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Secondary Approver 1</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover2" ReadOnly="true" CssClass="altText" runat="server"></asp:TextBox><%--FB 2659--%>
                                <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;" id="imgApprover2" runat="server"><img border="0" src="image/edit.gif" style="vertical-align:top" alt="edit" WIDTH="17" HEIGHT="15" style="cursor:pointer;" title="myVRM Address Book"></a><%--Edited For FF FB 2798--%>
                                <img border="0" src="image/btn_delete.gif" id="imgDelApprover2" runat="server" alt="delete" WIDTH="16" HEIGHT="16" style="vertical-align:top;cursor:pointer;" onclick="javascript:DeleteApprover(2)"  title="Delete" ><%--Edited For FF FB 2798--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover2" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Secondary Approver 2</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover3" ReadOnly="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;"  id="imgApprover3" runat="server"><img border="0" src="image/edit.gif" style="vertical-align:top" alt="edit" WIDTH="17" HEIGHT="15" style="cursor:pointer;" title="myVRM Address Book"></a> <%--Edited For FF FB 2798--%>                           
                                <img border="0" src="image/btn_delete.gif"  id="imgDelApprover3" runat="server"  alt="delete" WIDTH="16" HEIGHT="16" style="vertical-align:top;cursor:pointer;" onclick="javascript:DeleteApprover(3)"  title="Delete" ><%--Edited For FF FB 2798--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover3" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>                 
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label CssClass="subtitleblueblodtext" ID="lblHeader1" runat="server" Text="MGC Accord MCU Configuration" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>  <%-- Split tr1 row as tr1, tr2 and tr5 for FB 1937--%>
			<%--FB 2636 Starts --%>
            <tr id="tr3" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Port A</td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortA" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortA" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortA" ErrorMessage="Required" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Port B</td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortB" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortB" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
<%--                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Enabled="true" ControlToValidate="txtPortB" ErrorMessage="Required" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
--%>                            </td>
                        </tr>

                        <%--FB 2003 - Start--%>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">ISDN Audio Prefix</td>
                            <td width="30%" align="left">
                               <asp:TextBox CssClass="altText" ID="txtISDNAudioPref" MaxLength="4" runat="server"></asp:TextBox>
			                   <asp:RegularExpressionValidator ID="regISDNAudioPref" ControlToValidate="txtISDNAudioPref" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationGroup="Submit" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
			                </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">ISDN Video Prefix</td>
                            <td width="30%" align="left">
                               <asp:TextBox CssClass="altText" ID="txtISDNVideoPref" MaxLength="4" runat="server"></asp:TextBox>
			                   <asp:RegularExpressionValidator ID="regISDNVideoPref" ControlToValidate="txtISDNVideoPref" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationGroup="Submit" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
			                </td>
                        </tr>
                        <%--FB 2003 - End--%>
						<tr id="tr6" runat="server">
                            <td  width="20%" align="left" style="font-weight:bold" class="blackblodtext">ISDN Gateway</td> 
                            <td  width="30" align="left"> 
                                <asp:TextBox ID="txtISDNGateway" CssClass="altText" runat="server"></asp:TextBox>
                                
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtISDNGateway" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                  
                         
                         </tr>
                          <%--FB 2610 starts--%>
                         <tr id="trTempExtNumber" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Ext #
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtTempExtNumber" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                            <%--FB 2636 starts--%>
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblE164">Use E.164 Dialing</asp:Label> 
                            </td>
                            <td align="left" ><%-- FB 2779 --%>
                                <asp:CheckBox ID="chkE164" runat="server" />
                            </td>
                        </tr>
                        <tr>
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblEH323">Use H.323 Dialing</asp:Label> 
                            </td>
                            <td align="left" ><%-- FB 2779 --%>
                                <asp:CheckBox ID="chkH323" runat="server" />
                            </td>
                        </tr>
                        <%--FB 2636 End --%>
                        <%--FB 2610 Ends--%>
                    </table>
                </td>
            </tr>
			<%--FB 2636 Ends --%>
            <tr id="tr1" runat="server">
                <td align="center" >
                    <table width="90%" >
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Control Port IP address<span class="reqfldstarText">*</span></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortP" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPortP" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortP" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="reqPortP" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortP" ErrorMessage="Required" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2016 Starts--%>
                        <tr width="100%" id="trConfServiceID" runat="server">
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Conference Service ID<span class="reqfldstarText">*</span></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtConfServiceID" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RngValidConfServiceID" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtConfServiceID" ValidationExpression="^\d{0,9}$" ErrorMessage="Invalid Service ID" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <%--<asp:rangevalidator id="RngValidConfServiceID" runat="server"
                                    display="dynamic" controltovalidate="txtConfServiceID" errormessage="Invalid Conference Service ID"
                                   minimumvalue="0" maximumvalue="1000"  type="Integer"></asp:rangevalidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtConfServiceID" ErrorMessage="Required" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2016 Ends--%>
                        <%--FB 2591 Starts--%>
                        <%--FB 2427 Starts--%>
                        <tr width="100%" id="trProfileID" runat="server">
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Profile ID</td>
                            <td width="30%" align="left">
                                <%--<asp:TextBox ID="txtProfileID" CssClass="altText" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList ID="txtProfileID" CssClass="altSelectFormat" runat="server" DataValueField="Id" DataTextField="Name" > <%--FB 2556 --%>
                                <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RegularExpressionValidator ID="RegtxtProfileID" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtProfileID" ValidationExpression="^\d{0,9}$" ErrorMessage="Invalid Profile ID" Display="dynamic" runat="server"></asp:RegularExpressionValidator>--%><%--Commented for FB 2591--%>
                                <asp:Button ID="btnGetProfiles" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Get Profiles" OnClick="GetProfiles" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2427 Ends--%>
                        <%--FB 2591 Ends--%>
                        <%--FB 1907 - Start--%>
                        <tr width="100%" id="trEnableIVR" runat="server">
                            <td width="20%" align="left" style="font-weight: bold" class="blackblodtext">
                                Enable IVR
                            </td>
                            <td width="15%" align="left">
                                <asp:CheckBox ID="chkEnableIVR" runat="server" onclick="javascript:disableservname();" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trIVRName" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                IVR Service Name
                            </td>
                            <td align="left" width="55%">
                                <asp:TextBox ID="txtIVRName" MaxLength="99" CssClass="altText" runat="server" Width="128px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqServName" runat="server" ControlToValidate="lstIVR"
                                    Display="dynamic" SetFocusOnError="true" Text="" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                            
                            </td>
                            <td style="width: 20%; display: none" align="left" valign="top">
                                <asp:DropDownList ID="lstIVR" runat="server" CssClass="altSelectFormat" DataTextField="Name"
                                    DataValueField="ID">
                                    <asp:ListItem Text="Please select..." Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trEnableRecord" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Enable Recording
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkEnableRecord" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trEnableLPR" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Enable LPR
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkLPR" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trEnableMessage" runat="server"><%--FB 2486--%>
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Enable Messaging
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkMessage" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                         <%--FB 2610 starts--%>
                         <tr id="trExtensionNumber" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Ext #
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtExtNumber" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <%--FB 2610 Ends--%>
                        
                        <%--FB 2441 Starts--%>
                        <tr id="trTemplate" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               MCU Domain    <span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDomain" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtDomain" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <%--FB 2709 --%>
                        <tr id="trRPRMLogin" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               RPRM Userid  <span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtRPRMLogin" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtRPRMLogin" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                            </td>                      
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                              Concurrent Calls  <span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCallsCount" MaxLength="4" CssClass="altText" runat="server" ></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtCallsCount" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="LoginCountValidator" ControlToValidate="txtCallsCount" ValidationGroup="Submit"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numeric values only."
                                        ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
                                 <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCallsCount" ErrorMessage="Minimun count is 1 and Maximum count is 20." Operator="GreaterThan" Type="Integer" ValueToCompare="0" /> <%--FB 2709--%>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trRPRMemail" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               RPRM Email Address <span class="reqfldstarText">*</span>  
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtRPRMEmail" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEmailID" ControlToValidate="txtRPRMEmail"  runat="server" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Email ID should be Example@exp.com format." ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtRPRMEmail" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                            </td>                      
                        </tr>
                        <%--FB 2709 --%>
                        <tr id="trSynchronous" runat="server">
                            <td id="tdSynchronous" runat="server" width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"> <%--FB 2556 --%>
                               Synchronous  
                            </td>
                            <td align="left">
                                <input type="checkbox" id="chkSynchronous" runat="server" />
                            </td>
                            <td id="tdSendMail" runat="server" width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"> <%--FB 2556 --%>
                               Send Mail</td>
                            <td align="left">
                                <asp:CheckBox ID="chkSendmail" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        
                        <tr id="trEhancedMCU" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                DMA Monitoring                            </td>
                            <td align="left">
                                <asp:CheckBox ID="ChkEhancedMCU" runat="server" /><%--FB 2441 II --%>
                            </td>
                            <td colspan="3"></td>
                        </tr>
                        
                        <tr id="trDMAName" runat="server"> 
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               DMA Name  <span class="reqfldstarText">*</span>  
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMAName" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="rfvSubject"  runat="server" ControlToValidate="txtDMAName" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator><%--FB 2441 II --%>                                
                            </td>
                        <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               DMA Login    <span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMALogin" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="reqtxtDMALogin"  runat="server" ControlToValidate="txtDMALogin" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator><%--FB 2441 II --%>
                            </td>
                        
                        </tr>
                        <tr id="trDMAPassword" runat="server"> 
                             <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">DMA Password<%--<span class="reqfldstarText">*</span>FB 1408--%></td> <%--window dressing--%>
                            <td width="30%" align="left">
                            	<%--FB 2441 II Starts--%>
                                <asp:TextBox ID="txtDMAPassword" TextMode="Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" CssClass="altText" runat="server" onblur="DMAPasswordChange(1)" onfocus=" fnTextFocus(this.id,3)"></asp:TextBox>
                                <asp:CompareValidator ID="CmptxtDMAPassword" runat="server" ValidationGroup="Submit" ControlToCompare="txtDMARetypePwd"
                                    ControlToValidate="txtDMAPassword" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RgltxtDMAPassword" ValidationGroup="Submit" ControlToValidate="txtDMAPassword" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$"></asp:RegularExpressionValidator>
                                <%--FB 2441 II Ends--%>
                            </td>
                            <%--FB 2441 II Starts--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"> DMA Retype Password<%--<span class="reqfldstarText">*</span>FB 1408--%></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtDMARetypePwd" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" TextMode="Password" runat="server" onblur="DMAPasswordChange(2)" onfocus=" fnTextFocus(this.id,4)"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <asp:CompareValidator ID="CmptxtDMARetypePwd" runat="server" ControlToCompare="txtDMAPassword"
                                    ControlToValidate="txtDMARetypePwd" Display="Dynamic" ValidationGroup="Submit" ErrorMessage="<br>Passwords do not match."></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RgltxtDMARetypePwd" ValidationGroup="Submit" ControlToValidate="txtDMARetypePwd" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                             <%--FB 2441 II Ends--%>
                           
                        
                        </tr>
                        <tr id="trDMAURL" runat="server"> 
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               DMA URL    <span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMAURL" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="ReqtxtDMAURL" runat="server" ControlToValidate="txtDMAURL" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator><%--FB 2441 II --%>
                            </td>
                   
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               DMA Port    
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMAPort"  CssClass="altText" runat="server" MaxLength="5"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" ValidationGroup="Submit" ControlToValidate="txtDMAPort" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                
                            </td>
                        
                        </tr>
						 <%--FB 2441 II Starts--%>
						 <tr width="100%" id="trDMATemplate" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               DMA Domain <span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMADomain"  CssClass="altText" runat="server" MaxLength="5"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtDMADomain" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Profile ID </td>
                            <td width="30%" align="left">
                                <asp:DropDownList Width="44%" ID="lstTemplate" CssClass="altSelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
                                <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="Button1" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Get Profiles" OnClick="GetProfiles" />
                            </td>
                            
                        </tr>
                        <%--FB 2441 II Ends--%>
                        <%--FB 2689 Start--%>
                        <tr id="trDMADialinprefix" runat="server"> 
                          <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                               Dial-in Prefix<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                <asp:TextBox ID="txtDialinprefix" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="reqtxtDMAprefix"  runat="server" ControlToValidate="txtDialinprefix" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <%--FB 2689 End--%>
                        
                        <tr id="trDMATestCon" runat="server"><%-- FB 2441 II--%>
                            <td align="center" colspan="4">
                                <asp:Button runat="server" ID="btn_DMATestConnection" ValidationGroup="TestConnection" OnClick="TestConnection" Text="Test Connection" CssClass="altLongBlueButtonFormat" /> 
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <%--FB 2441 Ends--%>
						<%--FB 2556 --%>
                         <tr id="trMultiTenant" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                IsMultiTenant
                            </td>
                            <td  width="30%" align="left">
                                <asp:CheckBox ID="chkMultiTenant" runat="server" />
                            </td>
                        </tr>
                        <tr id="iViewOrgDetails1" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Scopia Desktop URL
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtScopiaURL" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                             <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Scopia Organization ID
                            </td>
                            <td  width="30%" align="left" nowrap="nowrap">
                                <%--<asp:TextBox ID="txtScopiaOrgID" CssClass="altText" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList Width="44%" ID="lstScopiaOrg" CssClass="alt2SelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
                                <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="btnMCUSilo" Width="50%" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Get Organization" OnClick="GetMCUSilo" />
                            </td>
                        </tr>
                        <tr id="iViewOrgDetails2" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Scopia Service ID
                            </td>
                            <td  width="30%" align="left">
                            
                                <%--<asp:TextBox ID="txtScopiaServiceID" CssClass="altText" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList Width="44%" ID="lstScopiaService" CssClass="alt2SelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
                                <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="btnMCUService" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Get Services" OnClick="GetMCUService" />
                            </td>
                             <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                Scopia Organization Login
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtScopiaOrgLogin" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <%--FB 2556 --%>
                     </table>
                </td>
            </tr><%-- FB 1937--%>
            <tr id="tr5" runat="server"> 
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left" valign="top" style="font-weight:bold" id="tdExpandCollapse"  class="subtitleblueblodtext">
                                Collapse All<input id="chkExpandCollapse" type="checkbox" onclick="javascript:ShowHideRow()" runat="server" class="btprint" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>             
            <tr id="tr2" runat="server">
                <td align="center" >
                    <table width="90%" >
                        <%--FB 1907 - End--%>
                        <tr id="trMCUCards" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                             <%--FB 1766 - Start
                             <table width="100%">
                                <tr>
                                   <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Enable IVR</td>
                                   <td width="15%" align="left">
                                      <asp:CheckBox ID="chkEnableIVR" runat="server" onclick="javascript:disableservname();" />
                                   </td>
                                   <td>&nbsp;</td>
                                   <td>&nbsp;</td>                                
                                </tr>
                                <tr>
                                   <td width="20%" align="left" valign="top" style="font-weight:bold" class="blackblodtext">IVR Service Name</td>
                                   <td align="left">
                                      <asp:TextBox ID="txtIVRName" MaxLength="99" CssClass="altText" runat="server"></asp:TextBox>
                                   </td>
                                   <td width="25%">
                                      <asp:RequiredFieldValidator ID="reqServName" runat="server" ControlToValidate="lstIVR" Display="dynamic" SetFocusOnError="true" Text="" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                   </td>
                                   <td style="width:20%;display:none" align="left" valign="top"> 
                                      <asp:DropDownList ID="lstIVR" runat="server"  CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID">
                                      <asp:ListItem Text="Please Select..." Value="-1"></asp:ListItem>
                                      </asp:DropDownList>
                                   </td>
                                </tr>
                              </table>
                              FB 1766 - End--%>
                              <h5 class="subtitleblueblodtext">MCU Cards</h5>
                              <asp:DataGrid ID="dgMCUCards" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server" style="border-collapse:separate"> <%--Edited for FF--%>
                                 <HeaderStyle CssClass="tableHeader" Height="30" HorizontalAlign="Left" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" />
                                 <Columns>
                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" DataField="Name" HeaderText="Card Name" ItemStyle-Width="40%" HeaderStyle-Width="50%"></asp:BoundColumn><%--window dressing--%>
                                    <asp:TemplateColumn HeaderText="Maximum Connections @ 384 Kbps" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" FooterStyle-BorderStyle="NotSet"><%--window dressing--%>
                                       <ItemTemplate>
                                          <asp:TextBox ID="txtMaxCalls" runat="server" CssClass="altText" Text='<%#DataBinder.Eval(Container, "DataItem.MaximumCalls") %>'></asp:TextBox>
                                          <asp:RangeValidator runat="server" ID="rangeMaxCalls" ValidationGroup="Submit" ControlToValidate="txtMaxCalls" ErrorMessage="please enter an integer value from 0 to 100" MinimumValue="0" MaximumValue="100" Type="integer" SetFocusOnError="true"></asp:RangeValidator> 
                                       </ItemTemplate>
                                    </asp:TemplateColumn>
                                 </Columns>
                              </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="trIPServices" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                                <h5 class="subtitleblueblodtext">IP Services</h5>
                                <asp:DataGrid ID="dgIPServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn  ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeIPOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Name" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regIPName" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address Type" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstAddressType" DataTextField="Name" DataValueField="ID" CssClass="altText" runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                                <asp:RequiredFieldValidator ControlToValidate="lstAddressType" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required" runat="server" InitialValue="-1" ></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAddress" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Submit" ControlToValidate="txtAddress" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regIPAddress" ValidationGroup="Submit" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ` , [ ] { } : $ @ - ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|`,\[\]{}\x22;=^:@$%&()\-'~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Network Access" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="Audio Conferencing" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video Conferencing" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Delete" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button Text="Add" OnClick="AddNewIPService" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoIPServices" Text="No IP Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <asp:Button ID="btnAddNewIPService" Text="Add" OnClick="AddNewIPService" CssClass="altShortBlueButtonFormat" runat="server" />
                                </td>
                            </tr>
                        <tr align="center" id="trISDN1" runat="server"><%--window dressing--%> <%--FB 2636  --%>
                                <td colspan="4">
                                
                                <h5 class="subtitleblueblodtext">ISDN Services</h5>
                                <asp:DataGrid ID="dgISDNServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeISDNOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Name" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNName" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtName" ErrorMessage="Required" CssClass="lblError"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regISDNName" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Prefix" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPrefix" CssClass="altText" Width="50" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.prefix") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNPrefix" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtPrefix" ErrorMessage="Required" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Start Range" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStartRange" CssClass="altText" Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.startRange") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNStartRange" ValidationGroup="Submit" runat="server" Display="dynamic" ControlToValidate="txtStartRange" ErrorMessage="Required" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="End Range" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtEndRange" CssClass="altText" Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.endRange") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNEndRange" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtEndRange" ErrorMessage="Required" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Range Sort Order" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstRangeSortOrder" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.RangeSortOrder") %>'>
                                                    <asp:ListItem Text="Start to End" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="End to Start" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Network Access" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="Audio Conferencing" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video Conferencing" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Delete" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btnAddISDNService" Text="Add" OnClick="AddNewISDNService" ValidationGroup="New" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoISDNServices" Text="No ISDN Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <asp:Button ID="btnAddISDNService" Text="Add" OnClick="AddNewISDNService" ValidationGroup="New" CssClass="altShortBlueButtonFormat" runat="server" />
                                </td>
                            </tr>
                        <tr id="trMPIServices" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                                <h5 class="subtitleblueblodtext">MPI Services</h5>
                                <asp:DataGrid ID="dgMPIServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeIPOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Name" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regMPIName" ControlToValidate="txtName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address Type" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstAddressType" DataTextField="Name" DataValueField="ID" CssClass="altText" runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="lstAddressType" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required" runat="server" InitialValue="-1" ></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAddress" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator is="regMPIName" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Alphanumeric characters only." ValidationExpression="\w+" runat="server"  ></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Network Access" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage"  ItemStyle-CssClass="tableBody" > 
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="Audio Conferencing" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video Conferencing" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Delete" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btnAddNewMPIService" Text="Add" OnClick="AddNewMPIService" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoMPIServices" Text="No MPI Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <asp:Button ID="btnAddNewMPIService" Text="Add" OnClick="AddNewMPIService" CssClass="altShortBlueButtonFormat" runat="server" />
                                </td>
                            </tr>                            
                        <%--FB 2636 Starts --%>
                        <tr id="trE164Services" runat="server" align="center">
                        <td colspan="4">
                            <h5 class="subtitleblueblodtext">E.164 Services</h5>
                            <asp:DataGrid ID="dgE164Services" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                <HeaderStyle CssClass="tableHeader" Height="30" />
                                <FooterStyle Height="30" />
                                <Columns>
                                   <asp:TemplateColumn ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                        <asp:TextBox ID="lstOrder" CssClass="altText" Width="20px" Enabled="false" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Start Range" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtStartRange" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartRange") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtStartRange" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regE164StartRange2" ControlToValidate="txtStartRange" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="End Range" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtEndRange" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndRange") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqFieldValidator3" ValidationGroup="Submit" ControlToValidate="txtEndRange" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regE164StartRange3" ControlToValidate="txtEndRange" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnAddNewE164Service" Text="Add" OnClick="AddNewE164Service" CssClass="altShortBlueButtonFormat" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:Label ID="lblNoE164Services" Text="No E.164 Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                            <asp:Button ID="btnAddNewE164Service" Text="Add" OnClick="AddNewE164Service" CssClass="altShortBlueButtonFormat" runat="server" />
                            </td>
                        </tr>
                        <%-- FB 2636 Ends --%>
                    </table>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold">Port A</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortT" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortT" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortT" ErrorMessage="Required" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAlert" runat="server"> <%--FB 2556 --%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Alerts</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAlertchks" runat="server"> <%--FB 2556 --%>
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" class="blackblodtext">ISDN Threshold Alert</td>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkISDNThresholdAlert" onclick="javascript:ShowISDN(this)" runat="server" />
                            </td>
                            <td width="20%" align="left" class="blackblodtext">Malfunction Alert</td>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkMalfunctionAlert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trISDN" style="display:none" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" class="blackblodtext">MCU ISDN Port Charge</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtMCUISDNPortCharge" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ValidationGroup="Submit" ControlToValidate="txtMCUISDNPortCharge" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" class="blackblodtext">ISDN Line Cost</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNLineCost" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ValidationGroup="Submit" ControlToValidate="txtISDNLineCost" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" class="blackblodtext">ISDN Max Cost</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNMaxCost" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ValidationGroup="Submit" ControlToValidate="txtISDNMaxCost" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" class="blackblodtext">ISDN Threshold Timeframe</td>
                            <td width="30%" align="left">
                                <asp:RadioButtonList ID="rdISDNThresholdTimeframe" runat="server" CssClass="blackblodtext">
                                    <asp:ListItem Text="Monthly" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Yearly" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                         <tr>
                            <td width="20%" align="left" class="blackblodtext">ISDN Threshold Percentage</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNThresholdPercentage" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ValidationGroup="Submit" ControlToValidate="txtISDNThresholdPercentage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d{0,2}(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                   </table>
                </td>
            </tr>
            <%--FB 1642 - DTMF Start--%>
            <tr style="display:none"> <%--FB 2655--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>DTMF Settings</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="display:none"> <%--FB 2655--%>
                <td align="center">                
                    <table cellpadding="3" cellspacing="0" width="90%" border="0">
                         <tr>
                            <td align="left" height="21" style="font-weight:bold" class="blackblodtext" width="20%">
                                Pre Conference Code
                            </td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PreConfCode" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPreConfCode" ControlToValidate="PreConfCode" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left" height="21" style="font-weight:bold" class="blackblodtext" width="20%">
                                Pre Leader Pin
                            </td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PreLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPreLeaderPin" ControlToValidate="PreLeaderPin" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" height="21" style="font-weight:bold" class="blackblodtext" width="20%">
                                Post Leader Pin
                            </td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PostLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPostLeaderPin" ControlToValidate="PostLeaderPin" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--FB 1938 - Start--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Utilization Reports</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="ReportRow" runat="server">
                <td align="center">                
                    <table cellpadding="3" cellspacing="0" width="90%" border="0">
                        <tr>
                            <td width="30%" align="left"> <%-- FB 2050 --%>
                                 <asp:TextBox ID="ReportDate" runat="server" CssClass="altText" Width="35%" ></asp:TextBox>
                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align:middle;" title="Date selector" onclick="return showCalendar('<%=ReportDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /> 
                                 <span class="blackblodtext">@</span> 
                                 <mbcbb:combobox id="ReportTime" runat="server" CssClass="altText" Rows="10"  style="width:75px" CausesValidation="True"><%--FB 1982--%>
                                 </mbcbb:combobox>
                                <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="ReportTime" ValidationGroup="Submit1" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regStartTime" runat="server"  ControlToValidate="ReportTime" ValidationGroup="Submit1" Display="Dynamic"  ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> 
                                <asp:RequiredFieldValidator ID="reqStartData" runat="server" ControlToValidate="ReportDate" ValidationGroup="Submit1" Display="Dynamic" ErrorMessage="Date is Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="ReportDate" ValidationGroup="Submit1" Display="Dynamic" SetFocusOnError="true"  ErrorMessage="Invalid Date " ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left"> <!-- FB 2050 -->
                                <asp:Button runat="server" ID="UsageReport" Text="Show Report" CssClass="altMedium0BlueButtonFormat" ValidationGroup="Submit1" 
                                CausesValidation="True" OnClientClick="javascript: return fnCheck();"  OnClick="MCUusageReport"/>  
                            </td>
                        </tr>                      
                    </table>
                </td>
            </tr>
            <%--FB 1938 - End--%>
            <tr>
                <td align="center">
                    <table width="90%" border="0">
                        <tr>
                            <td align="left" style="width:65%"> <%-- FB 2050 --%>
                                <asp:Button runat="server" ID="btnTestConnection" ValidationGroup="TestConnection" OnClick="TestConnection" Text="Test Connection" CssClass="altLongBlueButtonFormat" />  <%--OnClientClick="javascript:testConnection();return false;"--%>
                            </td>
                            <td align="left"> <%-- FB 2050 --%>
                                <input type="button" id="Close" class="altLongYellowButtonFormat" value="Cancel" onclick="fnCancel()" style="width:100px" /> <%--FB 2565--%>
                                <asp:Button ID="btnSubmit" OnClick="SubmitMCU" runat="server" ValidationGroup="Submit"  Text="Submit" OnClientClick="javascript:return RequireIVRName();" Width="100px" /><%--FB 1766 FB 2786--%>
                            </td>
                            
                        </tr>
                        <tr id="trCodianMessage" runat="server"> <%--FB Case 698--%>
                            <td class="cmmttext" align="left"> <%-- FB 2050 --%>
                                Management API should be activated for successful connection.<br />The code can be entered via the MCU website in the Feature Management Section<br />(Settings > Upgrade).
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" Visible="false" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                    <asp:DropDownList runat="server" ID="lstNetworkAccess" Visible="false" CssClass="altLong0SelectFormat">
                        <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
<script language="javascript">
    SavePassword();
    //FB 1937
    document.getElementById("reqStartTime").controltovalidate = "ReportTime_Text";

    // ZD 100263
    if ('<%=Session["BridgeID"]%>' == 'new') {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
    }
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
<% if (Request.QueryString["hf"] != null)
   {
       if (!Request.QueryString["hf"].ToString().Equals("1"))
       {
        %>
 
<% }
}%>
   
</body>
</html>

