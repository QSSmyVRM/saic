<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Bridges.BridgeDetails" Buffer="true" CodeFile="~/en/bridgedetailsviewonly.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 ,FB 2779 -->
<!-- #INCLUDE FILE="inc/maintop4.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MCU Details</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>
</head>
<body>
    <form id="frmMCUManagement" runat="server" method="post" onsubmit="return true">
    <input id="confPassword" runat="server" type="hidden" />
    <input id="txtApprover1_1" runat="server" type="hidden" />
    <input id="txtApprover2_1" runat="server" type="hidden" />
    <input id="txtApprover3_1" runat="server" type="hidden" />
    <input id="txtApprover4_1" runat="server" type="hidden" />
    <input id="hdnApprover1_1" runat="server" type="hidden" />
    <input id="hdnApprover2_1" runat="server" type="hidden" />
    <input id="hdnApprover3_1" runat="server" type="hidden" />
    <input id="hdnApprover4_1" runat="server" type="hidden" />

    <div>
      <input type="hidden" id="helpPage" value="65">
        
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class="subtitleblueblodtext">Basic Configuration</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Name</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtMCUID" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="txtMCUName"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU login</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label  ID="txtMCULogin" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="display:none">
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Password</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtPassword1"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Retype Password</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label  ID="txtPassword2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <%--Code changed for FB 1425 QA Bug -Start--%> 
                            <td width="20%" align="left" style="font-weight:bold" id ="TzTD1" runat="server" class="blackblodtext">MCU Timezone</td>
                            <td width="30%" align="left"  id ="TzTD2" runat="server">
                                <b>:</b>&nbsp;<asp:Label ID="lblTimezone" runat="server" ></asp:Label>
                                <asp:DropDownList Visible="false" ID="lstTimezone" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                          <%--Code changed for FB 1425 QA Bug -End--%>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Type</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="lblMCUType" runat="server" ></asp:Label>
                                <asp:DropDownList ID="lstMCUType" Visible="false" CssClass="altSelectFormat" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                                <asp:DropDownList ID="lstInterfaceType" Visible="false" DataTextField="interfaceType" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU Status</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                <asp:DropDownList ID="lstStatus" Visible="false" CssClass="altSelectFormat" runat="server" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Virtual MCU</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="chkIsVirtual" runat="server" />
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Administrator</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtApprover4" EnableViewState="true"  runat="server"></asp:Label>
                                <asp:Label ID="hdnApprover4" Visible="false" runat="server" Width="0" Height="0" ForeColor="transparent" BorderColor="transparent"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Firmware Version</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:DropDownList ID="lstFirmwareVersion" Visible="false" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Total Audio Only <br />Ports</td> <%--FB 1937--%>
                            <td width="30%" align="left" valign="middle">
                                <b>:</b>&nbsp;<asp:Label ID="txtMaxAudioCalls"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Total Audio/Video <br />Ports</td> <%--FB 1937--%>
                            <td width="30%" align="left" valign="middle">
                               <b>:</b>&nbsp;<asp:Label  ID="txtMaxVideoCalls" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class=subtitleblueblodtext>MCU Approvers</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Primary Approver</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtApprover1" EnableViewState="true"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover1" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Secondary Approver 1</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtApprover2"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover2" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Secondary Approver 2</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtApprover3"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover3" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>                 
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <asp:Label CssClass="subtitleblueblodtext" ID="lblHeader1" runat="server" Text="MGC Accord MCU Configuration" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr1" runat="server">
                <td align="center" >
                    <table width="90%" align="center"><%--Edited for FF--%>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Control Port IP address</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtPortP"  runat="server"></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr id="trMCUCards" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>MCU Cards</h5>
                                <asp:DataGrid ID="dgMCUCards" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="left" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name" ItemStyle-Width="40%" HeaderStyle-Width="50%" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Maximum Calls" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtMaxCalls" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaximumCalls") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                </td>
                            </tr>
                        <tr id="trIPServices" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>IP Services</h5>
                                <asp:DataGrid ID="dgIPServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Name" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address Type" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressType" runat="server"></asp:Label>
                                                <asp:DropDownList Visible="false" ID="lstAddressType" DataTextField="Name" DataValueField="ID"  runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtAddress"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Network Access" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  runat="server" Visible="false" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoIPServices" Text="No IP Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <%--Window Dressing--%>
                                <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                
                                <h5>ISDN Services</h5>
                                <asp:DataGrid ID="dgISDNServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Name" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Prefix" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtPrefix"  Width="50" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.prefix") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Start Range" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtStartRange"  Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.startRange") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="End Range" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtEndRange"  Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.endRange") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Range Sort Order" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label id="lblRangeSortOrder" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstRangeSortOrder" Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.RangeSortOrder") %>'>
                                                    <asp:ListItem Text="Start to End" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="End to Start" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Network Access" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>' CssClass="altText">
                                                    <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>' CssClass="altText">
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoISDNServices" Text="No ISDN Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        <tr id="trMPIServices" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>MPI Services</h5>
                                <asp:DataGrid ID="dgMPIServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Name" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address Type" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressType" runat="server"></asp:Label>
                                                <asp:DropDownList Visible="false" ID="lstAddressType" DataTextField="Name" DataValueField="ID"  runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Address" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtAddress"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Network Access" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>' CssClass="altText">
                                                    <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  runat="server" Visible="false" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>' CssClass="altText">
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoMPIServices" Text="No MPI Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr3" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold">Port A</td>
                            <td width="30%" align="left">
                                &nbsp;<b>:</b>&nbsp;<asp:Label ID="txtPortA"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold">Port B</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtPortB"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" class="blackblodtext" align="left" style="font-weight:bold">Port A</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtPortT"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                            <td width="20%" align="left">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Alerts</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">ISDN Threshold Alert</td>
                            <td width="30%" align="left"> <b>:</b>&nbsp;
<%--                              <asp:CheckBox ID="chkISDNThresholdAlert" onclick="javascript:ShowISDN(this)" runat="server" />
--%>                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Malfunction Alert</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="chkMalfunctionAlert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trISDN" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" class="blackblodtext" align="left">MCU ISDN Port Charge</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtMCUISDNPortCharge"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%"  class="blackblodtext" align="left">ISDN Line Cost</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtISDNLineCost"  runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" class="blackblodtext">ISDN Max Cost</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtISDNMaxCost"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" class="blackblodtext">ISDN Threshold Timeframe</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="lblISDNThresholdTimeframe" runat="server"></asp:Label>
                                <asp:RadioButtonList ID="rdISDNThresholdTimeframe" Visible="false" runat="server">
                                    <asp:ListItem Text="Monthly" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Yearly" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                         <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" class="blackblodtext">ISDN Threshold Percentage</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtISDNThresholdPercentage"  runat="server"></asp:Label>
                            </td>
                            <td width="20%" align="left">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                   </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" Visible="false" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                    <asp:DropDownList runat="server" ID="lstNetworkAccess" Visible="false">
                        <asp:ListItem Text="Public" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>   
    </form>
</body>
</html>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>