//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Bridges
{
    public partial class BridgeDetails : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log; //General Fix during Login Management
        /*
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblHeader1;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblNoIPServices;
        protected System.Web.UI.WebControls.Label lblNoISDNServices;

        protected System.Web.UI.WebControls.TextBox txtPassword1;
        protected System.Web.UI.WebControls.TextBox txtPassword2;
        protected System.Web.UI.WebControls.TextBox txtApprover1;
        protected System.Web.UI.WebControls.TextBox txtApprover2;
        protected System.Web.UI.WebControls.TextBox txtApprover3;
        protected System.Web.UI.WebControls.TextBox txtApprover4;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover1_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover2_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover3_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover4_1;
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.TextBox hdnApprover2;
        protected System.Web.UI.WebControls.TextBox hdnApprover3;
        protected System.Web.UI.WebControls.TextBox hdnApprover4;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover1_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover2_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover3_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover4_1;
        protected System.Web.UI.WebControls.TextBox txtMCUID;
        protected System.Web.UI.WebControls.TextBox txtMCUName;
        protected System.Web.UI.WebControls.TextBox txtMaxVideoCalls;
        protected System.Web.UI.WebControls.TextBox txtMaxAudioCalls;
        protected System.Web.UI.WebControls.TextBox txtMCULogin;
        protected System.Web.UI.WebControls.TextBox txtPortA;
        protected System.Web.UI.WebControls.TextBox txtPortB;
        protected System.Web.UI.WebControls.TextBox txtPortP;
        protected System.Web.UI.WebControls.TextBox txtPortT;
        protected System.Web.UI.WebControls.TextBox txtMCUISDNPortCharge;
        protected System.Web.UI.WebControls.TextBox txtISDNLineCost;
        protected System.Web.UI.WebControls.TextBox txtISDNMaxCost;
        protected System.Web.UI.WebControls.TextBox txtISDNThresholdPercentage;
        protected System.Web.UI.HtmlControls.HtmlTableRow trIPServices;
        protected System.Web.UI.WebControls.RadioButtonList rdISDNThresholdTimeframe;

        protected System.Web.UI.WebControls.DropDownList lstMCUType;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstInterfaceType;
        protected System.Web.UI.WebControls.DropDownList lstStatus;
        protected System.Web.UI.WebControls.DropDownList lstTimezone;
        protected System.Web.UI.WebControls.DropDownList lstFirmwareVersion;
//        protected System.Web.UI.WebControls.DropDownList lstTimezone;
        
        protected System.Web.UI.WebControls.DataGrid dgIPServices;
        protected System.Web.UI.WebControls.DataGrid dgISDNServices;

        protected System.Web.UI.WebControls.Button btnAddISDNService;
        protected System.Web.UI.WebControls.Button btnAddNewIPService;

        protected System.Web.UI.WebControls.CheckBox chkIsVirtual;
        protected System.Web.UI.WebControls.CheckBox chkMalfunctionAlert;
        protected System.Web.UI.WebControls.CheckBox chkISDNThresholdAlert;

        protected System.Web.UI.HtmlControls.HtmlTableRow trISDN;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr1;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr3;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr4;

        protected System.Web.UI.HtmlControls.HtmlInputHidden confPassword;

        protected System.Web.UI.WebControls.RequiredFieldValidator reqPortP;
        protected System.Web.UI.WebControls.RegularExpressionValidator regPortP;
        protected System.Web.UI.WebControls.CustomValidator customvalidation;
        */
        public BridgeDetails()
        {
             // ZD 100263
            log = new ns_Logger.Logger(); //General Fix during Login Management

            //
            // TODO: Add constructor logic here
            //
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("BridgeDetailsViewOnly.aspx", Request.Url.AbsoluteUri.ToLower());

                if (!IsPostBack)
                {
                    if (Request.QueryString["hf"] != null)
                    {
                        if (Request.QueryString["hf"].ToString().Equals("1"))
                        {
                            Session.Add("BridgeID", Request.QueryString["bid"]);
                            DisableAllControls(Page.Controls[1]);
                        }
                    }
                    txtPassword1.Attributes.Add("onchange", "javascript:SavePassword()");
                    txtApprover1.Attributes.Add("onBlur", "javascript:SavePassword()");
                    txtApprover2.Attributes.Add("onBlur", "javascript:SavePassword()");
                    txtApprover3.Attributes.Add("onBlur", "javascript:SavePassword()");
                    txtApprover4.Attributes.Add("onBlur", "javascript:SavePassword()");
                    obj.BindAddressType(lstAddressType);
                    if (Session["BridgeID"].ToString().Equals("new"))
                    {
                        lblHeader.Text = "Create New MCU";
                        BindDataNew();
                    }
                    else
                    {
                        lblHeader.Text = "MCU Details";
                        BindDataOld();
                    }
                } 
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = "Operation Successful!";
                        errLabel.Visible = true;
                    }
                txtPassword1.Attributes.Add("value", confPassword.Value);
                txtPassword2.Attributes.Add("value", confPassword.Value);
                txtApprover1.Attributes.Add("value", txtApprover1_1.Value);
                txtApprover2.Attributes.Add("value", txtApprover2_1.Value);
                txtApprover3.Attributes.Add("value", txtApprover3_1.Value);
                txtApprover4.Attributes.Add("value", txtApprover4_1.Value);
                hdnApprover1.Attributes.Add("value", hdnApprover1_1.Value);
                hdnApprover2.Attributes.Add("value", hdnApprover2_1.Value);
                hdnApprover3.Attributes.Add("value", hdnApprover3_1.Value);
                hdnApprover4.Attributes.Add("value", hdnApprover4_1.Value);

                /* *** Code added for FB 1425 QA Bug -Start *** */

                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() == "0")
                    {
                        TzTD1.Attributes.Add("style", "display:none");
                        TzTD2.Attributes.Add("style", "display:none");
                    }

                }

                /* *** Code added for FB 1425 QA Bug -End *** */
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
            }

        }
        private void BindDataNew()
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeID>" + Session["BridgeID"].ToString() + "</bridgeID>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetNewBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridge/bridgeTypes/type");
                    LoadList(lstMCUType, nodesTemp);
                    LoadList(lstInterfaceType, nodesTemp);
                    nodesTemp = xmldoc.SelectNodes("//bridge/bridgeStatuses/status");
                    LoadList(lstStatus, nodesTemp);
                    nodesTemp = xmldoc.SelectNodes("//bridge/timezones/timezone");
                    LoadList(lstTimezone, nodesTemp);
                    ChangeFirmwareVersion(new object(), new EventArgs());
                    txtMCUID.Text = "new";
                    lblNoISDNServices.Visible = true;
                    txtMaxVideoCalls.Text = "1";
                    txtMaxAudioCalls.Text = "1";
                    lblNoIPServices.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        private void BindDataOld()
        {
            try
            {
                String inXML = ""; //<login><userID>11</userID><bridgeID>1</bridgeID></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement(); //FB 1920
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeID>" + Session["BridgeID"].ToString() + "</bridgeID>";
                inXML += "</login>";
                /* Code Modified For FB 1462 - Start */
                //String outXML = obj.CallCOM("GetOldBridge", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                /* Code Modified For FB 1462 - End */
                //String outXML = "<bridge><bridgeID>1</bridgeID><name>Test Bridge</name><login>login</login><password>pwd</password><bridgeType>3</bridgeType><bridgeStatus>1</bridgeStatus><virtualBridge>0</virtualBridge><malfunctionAlert>0</malfunctionAlert><timeZone>26</timeZone><firmwareVersion>7.0</firmwareVersion><percentReservedPort>0</percentReservedPort><bridgeAdmin><ID>11</ID><firstName>VRM</firstName><lastName>Administrator</lastName><maxAudioCalls>1</maxAudioCalls><maxVideoCalls>1</maxVideoCalls></bridgeAdmin><bridgeDetails><controlPortIPAddress>1.1.1.1</controlPortIPAddress><IPServices><IPService><ID>1</ID><name>Service 1</name><addressType>1</addressType><address>2.2.2.2</address><networkAccess>1</networkAccess><usage>1</usage></IPService></IPServices><ISDNServices><ISDNService><ID>1</ID><name>Service2</name><prefix>2</prefix><startRange>1234</startRange><endRange>8765</endRange><networkAccess>1</networkAccess><usage>1</usage></ISDNService></ISDNServices></bridgeDetails><approvers><responseMsg></responseMsg><responseTime>60</responseTime></approvers><timezones><timezone><timezoneID>35</timezoneID><timezoneName>(GMT-10:00) Hawaii</timezoneName></timezone><timezone><timezoneID>2</timezoneID><timezoneName>(GMT-09:00) Alaska</timezoneName></timezone><timezone><timezoneID>51</timezoneID><timezoneName>(GMT-08:00) Pacific Time</timezoneName></timezone><timezone><timezoneID>67</timezoneID><timezoneName>(GMT-07:00) Mountain - Arizona</timezoneName></timezone><timezone><timezoneID>42</timezoneID><timezoneName>(GMT-07:00) Mountain Time</timezoneName></timezone><timezone><timezoneID>66</timezoneID><timezoneName>(GMT-05:00) Indiana</timezoneName></timezone><timezone><timezoneID>19</timezoneID><timezoneName>(GMT-06:00) Central Time</timezoneName></timezone><timezone><timezoneID>26</timezoneID><timezoneName>(GMT-05:00) Eastern Time </timezoneName></timezone><timezone><timezoneID>73</timezoneID><timezoneName>(GMT+10:00) Guam</timezoneName></timezone><timezone><timezoneID>18</timezoneID><timezoneName>(GMT+11:00) Magadan</timezoneName></timezone></timezones><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><addressType><type><ID>1</ID><name>IP Address</name></type><type><ID>2</ID><name>H323 ID</name></type><type><ID>3</ID><name>E.164</name></type><type><ID>4</ID><name>ISDN Phone number</name></type></addressType><ISDNThresholdAlert>0</ISDNThresholdAlert></bridge>";
                //Response.Write("<br>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridge/bridgeTypes/type");
                    LoadList(lstMCUType, nodesTemp);
                    LoadList(lstInterfaceType, nodesTemp);
                    nodesTemp = xmldoc.SelectNodes("//bridge/bridgeStatuses/status");
                    LoadList(lstStatus, nodesTemp);
                    nodesTemp = xmldoc.SelectNodes("//bridge/timezones/timezone");
                    LoadList(lstTimezone, nodesTemp);
                    //Response.Write(Session["systemTimeZone"].ToString()); 
                    //lstTimezone.Items.FindByText(Session["systemTimeZone"].ToString()).Selected = true;
                    txtMCUName.Text = xmldoc.SelectSingleNode("//bridge/name").InnerText;
                    txtMCUID.Text = xmldoc.SelectSingleNode("//bridge/bridgeID").InnerText;
                    txtMCULogin.Text = xmldoc.SelectSingleNode("//bridge/login").InnerText;
                    txtPassword1.Attributes.Add("value", xmldoc.SelectSingleNode("//bridge/password").InnerText);
                    txtPassword2.Attributes.Add("value", xmldoc.SelectSingleNode("//bridge/password").InnerText);
                    try //General Fix during Login Management
                    {
                        lstTimezone.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/timeZone").InnerText).Selected = true;
                    }
                    catch (Exception e)
                    {
                        log.Trace(e.Message);
                    }
                    lblTimezone.Text = lstTimezone.SelectedItem.Text;

                    try //General Fix during Login Management
                    {
                        lstStatus.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeStatus").InnerText).Selected = true;
                    }
                    catch (Exception e)
                    {
                        log.Trace(e.Message);
                    }

                    lblStatus.Text = lstStatus.SelectedItem.Text;

                    try //General Fix during Login Management
                    {
                        lstMCUType.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeType").InnerText).Selected = true;
                    }
                    catch (Exception e)
                    {
                        log.Trace(e.Message);
                    }
                    lblMCUType.Text = lstMCUType.SelectedItem.Text;
                    //lstInterfaceType.Items.FindByValue(lstMCUType.SelectedValue).Selected = true;
                    confPassword.Value = xmldoc.SelectSingleNode("//bridge/password").InnerText;
                    /* Code Modified For FB 1462 - Start */
                    if (xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("A")) // || !xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("D"))
                    {
                        txtApprover4.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/firstName").InnerText + " " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/lastName").InnerText;
                        hdnApprover4.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/ID").InnerText;
                        txtApprover4_1.Value = txtApprover4.Text;
                        hdnApprover4_1.Value = hdnApprover4.Text;
                    }
                    /* Code Modified For FB 1462 - End */                 

                    chkIsVirtual.Text = "No";
                    if (xmldoc.SelectSingleNode("//bridge/virtualBridge").InnerText.Equals("1"))
                        chkIsVirtual.Text = "Yes";
                    ChangeFirmwareVersion(new object(), new EventArgs());
                    txtMaxAudioCalls.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/maxAudioCalls").InnerText;
                    txtMaxVideoCalls.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/maxVideoCalls").InnerText;
                    //FB 1938
                    if (lstInterfaceType.SelectedItem.Text != "3" && lstInterfaceType.SelectedItem.Text != "6")
                        txtMaxAudioCalls.Text = "N/A";
                        
                    if (lstInterfaceType.SelectedItem.Text.Equals("1") || lstInterfaceType.SelectedItem.Text.Equals("4")) //NGC fixes
                    {
                        txtPortP.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress").InnerText;
                        XmlNodeList nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/IPServices/IPService");
                        if (nodes.Count > 0)
                        {
                            GetServices(nodes, dgIPServices);
                            lblNoIPServices.Visible = false;
                            dgIPServices.Visible = true;
                        }
                        else
                        {
                            lblNoIPServices.Visible = true;
                            dgIPServices.Visible = false;
                        }
                        nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/ISDNServices/ISDNService");
                        if (nodes.Count > 0)
                        {
                            GetServices(nodes, dgISDNServices);
                            lblNoISDNServices.Visible = false;
                            dgISDNServices.Visible = true;
                        }
                        else
                        {
                            lblNoISDNServices.Visible = true;
                            dgISDNServices.Visible = false;
                        }
                        nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/MPIServices/MPIService");
                        if (nodes.Count > 0)
                        {
                            GetServices(nodes, dgMPIServices);
                            lblNoMPIServices.Visible = false;
                            dgMPIServices.Visible = true;
                        }
                        else
                        {
                            lblNoMPIServices.Visible = true;
                            dgMPIServices.Visible = false;
                        }
                    }
                    if (lstInterfaceType.SelectedItem.Text.Equals("3") || lstInterfaceType.SelectedItem.Text.Equals("6"))//FB 2008
                    {
                        txtPortA.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/portA").InnerText;
                        txtPortB.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/portB").InnerText;
                    }
                    nodesTemp = xmldoc.SelectNodes("//bridge/approvers/approver");
                    
                    bool adminStatus = true;    //FB 1462
                    foreach (XmlNode node in nodesTemp)
                    {
                        if (txtApprover1.Text.Equals(""))
                        {
                            txtApprover1.Text = node.SelectSingleNode("firstName").InnerText + " " + node.SelectSingleNode("lastName").InnerText;
                            hdnApprover1.Text = node.SelectSingleNode("ID").InnerText;
                            txtApprover1_1.Value = txtApprover1.Text;
                            hdnApprover1_1.Value = hdnApprover1.Text;
                        }
                        else if (txtApprover2.Text.Equals(""))
                        {
                            txtApprover2.Text = node.SelectSingleNode("firstName").InnerText + " " + node.SelectSingleNode("lastName").InnerText;
                            hdnApprover2.Text = node.SelectSingleNode("ID").InnerText;
                            txtApprover2_1.Value = txtApprover2.Text;
                            hdnApprover2_1.Value = hdnApprover2.Text;
                        }
                        else if (txtApprover3.Text.Equals(""))
                        {
                            txtApprover3.Text = node.SelectSingleNode("firstName").InnerText + " " + node.SelectSingleNode("lastName").InnerText;
                            hdnApprover3.Text = node.SelectSingleNode("ID").InnerText;
                            txtApprover3_1.Value = txtApprover3.Text;
                            hdnApprover3_1.Value = hdnApprover3.Text;
                        }
                    }
                    //Code Added For BridgeDetails Error - Related to FB 1422 Nathira
                    if(lstFirmwareVersion.Text == xmldoc.SelectSingleNode("//bridge/firmwareVersion").InnerText)
                        lstFirmwareVersion.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/firmwareVersion").InnerText).Selected = true;
                    chkMalfunctionAlert.Text = "No";
                    if (xmldoc.SelectSingleNode("//bridge/malfunctionAlert").InnerText.Trim().Equals("1"))
                        chkMalfunctionAlert.Text = "Yes";
                    if (xmldoc.SelectSingleNode("//bridge/ISDNThresholdAlert").InnerText.Trim().Equals("1"))
                    {
                        trISDN.Visible = true;
                        trISDN.Attributes.Add("style", "display:");
                        txtMCUISDNPortCharge.Text = xmldoc.SelectSingleNode("//bridge/ISDNPortCharge").InnerText;
                        txtISDNLineCost.Text = xmldoc.SelectSingleNode("//bridge/ISDNLineCharge").InnerText;
                        txtISDNMaxCost.Text = xmldoc.SelectSingleNode("//bridge/ISDNMaxCost").InnerText;
                        txtISDNThresholdPercentage.Text = xmldoc.SelectSingleNode("//bridge/ISDNThreshold").InnerText;
                        if (xmldoc.SelectSingleNode("//bridge/ISDNThresholdTimeframe").InnerText.Equals(""))
                            rdISDNThresholdTimeframe.Items.FindByValue("2").Selected = true;
                        else
                            rdISDNThresholdTimeframe.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/ISDNThresholdTimeframe").InnerText).Selected = true;
                        lblISDNThresholdTimeframe.Text = rdISDNThresholdTimeframe.SelectedItem.Text;
                    }
                    /* *** Code Added For FB 1462 - Start *** */
                    if (xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("I"))
                    {
                        if (errLabel.Text == "")
                            errLabel.Text += "Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User - " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/firstName").InnerText.Trim() + ", " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/lastName").InnerText.Trim() + " is In-Active. Please make sure the user is Active.";
                        else
                            errLabel.Text += "<br>Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User - " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/firstName").InnerText.Trim() + ", " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/lastName").InnerText.Trim() + " is In-Active. Please make sure the user is Active.";

                        adminStatus = false;
                    }
                    else if (xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("D"))
                    {
                        if (errLabel.Text == "")
                            errLabel.Text += "Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User has been Deleted";
                        else
                            errLabel.Text += "<br>Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User has been Deleted";

                        adminStatus = false;
                    }
                    if (!adminStatus)
                        errLabel.Visible = true;

                    /* *** Code Added For FB 1462 - End *** */
                }
                else
                {
                    errLabel.Text = outXML;
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }
        protected void GetServices(XmlNodeList nodes, DataGrid dgTemp)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    dgTemp.DataSource = dt;
                    dgTemp.DataBind();
                    foreach (DataGridItem dgi in dgTemp.Items)
                    {
                        Label lblTemp = (Label)dgi.FindControl("lblAddressType");
                        if (lblTemp != null)
                            lblTemp.Text = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedItem.Text;
                        lblTemp = (Label)dgi.FindControl("lblNetworkAccess");
                        lblTemp.Text = ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedItem.Text;
                        lblTemp = (Label)dgi.FindControl("lblUsage");
                        if (lblTemp != null)
                            lblTemp.Text = ((DropDownList)dgi.FindControl("lstUsage")).SelectedItem.Text;
                        lblTemp = (Label)dgi.FindControl("lblRangeSortOrder");
                        if (lblTemp != null)
                            lblTemp.Text = ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedItem.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        protected void ChangeFirmwareVersion(Object sender, EventArgs e)
        {
            try
            {
                lstInterfaceType.SelectedIndex = lstMCUType.SelectedIndex;
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("Name");
                lstFirmwareVersion.Items.Clear();
                DataRow dr;
                switch (lstInterfaceType.SelectedItem.Text)
                {
                    case "1": 
                        dr = dt.NewRow();
                        dr["ID"] = "7.0"; dr["Name"] = "7.x";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = "MGC Accord MCU Configuration";
                        tr1.Visible = true;
                        tr3.Visible = false;
                        tr4.Visible = false;
                        break;
                    case "3":
                        dr = dt.NewRow();
                        dr["ID"] = "1.5"; dr["Name"] = "1.5";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "2.2"; dr["Name"] = "2.2";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = "Codian MCU Configuration";
                        tr1.Visible = false;
                        tr3.Visible = true;
                        tr4.Visible = false;
                        if (txtPortA.Text.Equals(""))
                            txtPortA.Text = txtPortP.Text;
                        break;
                    case "4":
                        dr = dt.NewRow();
                        dr["ID"] = "J4.0"; dr["Name"] = "J4.0";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "D3.9"; dr["Name"] = "D3.9";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = "Tandberg MCU Configuration";
                        if (txtPortT.Text.Equals(""))
                            txtPortT.Text = txtPortP.Text;
                        tr1.Visible = true;
                        tr3.Visible = false;
                        tr4.Visible = false;
                        trIPServices.Visible = false;
                        break;
                    case "6": //FB 2008
                        dr = dt.NewRow();
                        dr["ID"] = "2.2"; dr["Name"] = "2.2";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = "MSE 8000 MCU Configuration";
                        tr1.Visible = false;
                        trMPIServices.Visible = false;
                        tr3.Visible = true;
                        tr4.Visible = false;
                        if (txtPortA.Text.Equals(""))
                            txtPortA.Text = txtPortP.Text;
                        //tr6.Visible = true;
                        break;
                    default:
                        dr = dt.NewRow();
                        dr["ID"] = "-1"; dr["Name"] = "None";
                        dt.Rows.Add(dr);
                        break;
                }
                lstFirmwareVersion.DataSource = dt;
                lstFirmwareVersion.DataBind();
            }
            catch (Exception ex)
            {
            }
        }
        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        #endregion
        //protected void SubmitMCU(Object sender, EventArgs e)
        //{
        //    if (Page.IsValid)
        //    {
        //        String inXML = GenerateInXML();
        //        //Response.Write(obj.Transfer(inXML));
        //        //Response.End();
        //        String outXML = "";
        //        if (!inXML.Equals(""))
        //        {
        //            outXML = obj.CallCOM("SetBridge", inXML, Application["COM_ConfigPath"].ToString());
        //            if (outXML.IndexOf("<error>") < 0)
        //                Response.Redirect("ManageBridge.aspx?m=1");
        //            else
        //                errLabel.Text = (outXML);
        //        }
        //    }
        //}

        //protected String GenerateInXML()
        //{
        //    try
        //    {
        //        String inXML = "<setBridge>";
        //        inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";

        //        inXML += "  <bridge>";
        //        inXML += "      <bridgeID>" + txtMCUID.Text + "</bridgeID>";
        //        inXML += "      <name>" + txtMCUName.Text + "</name>";
        //        inXML += "      <login>" + txtMCULogin.Text + "</login>";
        //        inXML += "      <password>" + confPassword.Value + "</password>";
        //        inXML += "      <timeZone>" + lstTimezone.SelectedValue + "</timeZone>";
        //        inXML += "      <maxAudioCalls>" + txtMaxAudioCalls.Text + "</maxAudioCalls>";
        //        inXML += "      <maxVideoCalls>" + txtMaxVideoCalls.Text + "</maxVideoCalls>";
        //        inXML += "      <bridgeType>" + lstMCUType.SelectedValue + "</bridgeType>";
        //        inXML += "      <bridgeStatus>" + lstStatus.SelectedValue + "</bridgeStatus>";
        //        if (chkIsVirtual.Checked)
        //            inXML += "      <virtualBridge>1</virtualBridge>";
        //        else
        //            inXML += "      <virtualBridge>0</virtualBridge>";
        //        inXML += "      <bridgeAdmin>";
        //        inXML += "          <ID>" + hdnApprover4.Text + "</ID>";
        //        inXML += "      </bridgeAdmin>";
        //        inXML += "      <firmwareVersion>" + lstFirmwareVersion.SelectedValue + "</firmwareVersion>";
        //        inXML += "      <percentReservedPort>20</percentReservedPort>";
        //        inXML += "      <approvers>";
        //        inXML += "          <approver>";
        //        inXML += "              <ID>" + hdnApprover1.Text + "</ID>";
        //        inXML += "          </approver>";
        //        inXML += "          <approver>";
        //        inXML += "              <ID>" + hdnApprover2.Text + "</ID>";
        //        inXML += "          </approver>";
        //        inXML += "          <approver>";
        //        inXML += "              <ID>" + hdnApprover3.Text + "</ID>";
        //        inXML += "          </approver>";
        //        inXML += "      </approvers>";
        //        inXML += "      <bridgeDetails>";
        //        switch (lstInterfaceType.SelectedItem.Text)
        //        {
        //            case "1":
        //                inXML += "      <controlPortIPAddress>" + txtPortP.Text + "</controlPortIPAddress>";
        //                inXML += "      <IPServices>";
        //                foreach (DataGridItem dgi in dgIPServices.Items)
        //                {
        //                    if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
        //                    {
        //                        inXML += "          <IPService>";
        //                        //inXML += "              <ID>" + dgi.Cells[0].Text + "</ID>";
        //                        inXML += "              <SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>";
        //                        inXML += "              <name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>";
        //                        inXML += "              <addressType>" + ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue + "</addressType>";
        //                        inXML += "              <address>" + ((TextBox)dgi.FindControl("txtAddress")).Text + "</address>";
        //                        inXML += "              <networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>";
        //                        inXML += "              <usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>";
        //                        inXML += "          </IPService>";
        //                    }
        //                }
        //                inXML += "      </IPServices>";
        //                inXML += "      <ISDNServices>";
        //                foreach (DataGridItem dgi in dgISDNServices.Items)
        //                {
        //                    if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
        //                    {
        //                        inXML += "          <ISDNService>";
        //                        //inXML += "              <ID>" + dgi.Cells[0].Text + "</ID>";
        //                        inXML += "              <SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>";
        //                        inXML += "              <name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>";
        //                        inXML += "              <prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>";
        //                        inXML += "              <startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>";
        //                        inXML += "              <endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>";
        //                        inXML += "              <RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>";
        //                        inXML += "              <networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>";
        //                        inXML += "              <usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>";
        //                        inXML += "          </ISDNService>";
        //                    }
        //                }
        //                inXML += "      </ISDNServices>";
        //                break;
        //            case "3":
        //                inXML += "          <portA>" + txtPortA.Text + "</portA>";
        //                inXML += "          <portB>" + txtPortB.Text + "</portB>";
        //                break;
        //            case "4":
        //                inXML += "      <controlPortIPAddress>" + txtPortP.Text + "</controlPortIPAddress>";
        //                inXML += "      <ISDNServices>";
        //                foreach (DataGridItem dgi in dgISDNServices.Items)
        //                {
        //                    if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
        //                    {
        //                        inXML += "          <ISDNService>";
        //                        inXML += "              <SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>";
        //                        inXML += "              <name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>";
        //                        inXML += "              <prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>";
        //                        inXML += "              <startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>";
        //                        inXML += "              <endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>";
        //                        inXML += "              <RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>";
        //                        inXML += "              <networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>";
        //                        inXML += "              <usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>";
        //                        inXML += "          </ISDNService>";
        //                    }
        //                }
        //                inXML += "      </ISDNServices>";
        //                break;
        //        }
        //        inXML += "      </bridgeDetails>";
        //        String chkTemp = "0";
        //        if (chkISDNThresholdAlert.Checked) chkTemp = "1";
        //        inXML += "  <ISDNThresholdAlert>" + chkTemp + "</ISDNThresholdAlert>";
        //        if (chkTemp.Equals("1"))
        //        {
        //            inXML += "  <ISDNPortCharge>" + txtMCUISDNPortCharge.Text + "</ISDNPortCharge>";
        //            inXML += "  <ISDNLineCharge>" + txtISDNLineCost.Text + "</ISDNLineCharge>";
        //            inXML += "  <ISDNMaxCost>" + txtISDNMaxCost.Text + "</ISDNMaxCost>";
        //            inXML += "  <ISDNThresholdTimeframe>" + rdISDNThresholdTimeframe.SelectedValue + "</ISDNThresholdTimeframe>";
        //            inXML += "  <ISDNThreshold>" + txtISDNThresholdPercentage.Text + "</ISDNThreshold>";
        //        }
        //        chkTemp = "0";
        //        if (chkMalfunctionAlert.Checked) chkTemp = "1";
        //        inXML += "      <malfunctionAlert>" + chkTemp + "</malfunctionAlert>";
        //        inXML += "  </bridge>";
        //        inXML += "</setBridge>";
        //        //Response.Write(obj.Transfer(inXML));
        //        //Response.End();
        //        return inXML;
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace;
        //        errLabel.Visible = true;
        //        return "";
        //    }
        //}
        //protected void TestConnection(Object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //Response.Write("in test connection");
        //        String inXML = GenerateInXML();
        //        String outXML = "";
        //        errLabel.Visible = true;
        //        if (!inXML.Equals(""))
        //        {
        //            outXML = obj.CallCOM2("TestMCUConnection", inXML, Application["RTC_ConfigPath"].ToString());
        //            if (outXML.IndexOf("<error>") < 0)
        //                //Response.Redirect("ManageBridge.aspx?m=1");
        //                errLabel.Text = "Operation Successful!";
        //            else
        //                errLabel.Text = obj.ShowErrorMessage(outXML);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace;
        //        errLabel.Visible = true;
        //    }
        //}

        //protected void EditService(Object sender, DataGridCommandEventArgs e)
        //{
        //    try
        //    {
        //        //Response.Write(e.Item.ItemIndex);
        //        dgIPServices.EditItemIndex = e.Item.ItemIndex;
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace;
        //        errLabel.Visible = true;
        //    }
        //}

        protected void BindAddressType(Object sender, EventArgs e)
        {
            try
            {
                obj.BindAddressType((DropDownList)sender);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        //protected void AddNewIPService(Object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //Response.Write(dgIPServices.Items.Count);
        //        if (dgIPServices.Items.Count < 10)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            //dt.Columns.Add("ID");
        //            dt.Columns.Add("SortID");
        //            dt.Columns.Add("name");
        //            dt.Columns.Add("addressType");
        //            dt.Columns.Add("address");
        //            dt.Columns.Add("networkAccess");
        //            dt.Columns.Add("usage");
        //            foreach (DataGridItem dgi in dgIPServices.Items)
        //            {
        //                dr = dt.NewRow();
        //                //dr["ID"] = dgi.Cells[0].Text;
        //                dr["SortID"] = ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue;
        //                dr["name"] = ((TextBox)dgi.FindControl("txtName")).Text;
        //                dr["addressType"] = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue;
        //                dr["address"] = ((TextBox)dgi.FindControl("txtAddress")).Text;
        //                dr["networkAccess"] = ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue;
        //                dr["usage"] = ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue;
        //                dt.Rows.Add(dr);
        //            }
        //            dr = dt.NewRow();
        //            //dr["ID"] = "new";
        //            dr["SortID"] = dt.Rows.Count + 1;
        //            dr["name"] = "";
        //            dr["addressType"] = "-1";
        //            dr["address"] = "";
        //            dr["networkAccess"] = "3";
        //            dr["usage"] = "3";
        //            dt.Rows.Add(dr);
        //            dgIPServices.DataSource = dt;
        //            dgIPServices.DataBind();
        //            dgIPServices.Visible = true;
        //            lblNoIPServices.Visible = false;
        //        }
        //        else
        //            errLabel.Text = "More than 10 IP Services can not be added.";

        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace;
        //        errLabel.Visible = true;
        //    }
        //}
        //protected void AddNewISDNService(Object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (dgISDNServices.Items.Count < 10)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            //dt.Columns.Add("ID");
        //            dt.Columns.Add("SortID");
        //            dt.Columns.Add("RangeSortOrder");
        //            dt.Columns.Add("name");
        //            dt.Columns.Add("prefix");
        //            dt.Columns.Add("startRange");
        //            dt.Columns.Add("endRange");
        //            dt.Columns.Add("networkAccess");
        //            dt.Columns.Add("usage");
        //            foreach (DataGridItem dgi in dgISDNServices.Items)
        //            {
        //                dr = dt.NewRow();
        //                //dr["ID"] = dgi.Cells[0].Text;
        //                dr["SortID"] = ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue;
        //                dr["name"] = ((TextBox)dgi.FindControl("txtName")).Text;
        //                dr["prefix"] = ((TextBox)dgi.FindControl("txtPrefix")).Text;
        //                dr["startRange"] = ((TextBox)dgi.FindControl("txtStartRange")).Text;
        //                dr["endRange"] = ((TextBox)dgi.FindControl("txtEndRange")).Text;
        //                dr["RangeSortOrder"] = ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue;
        //                dr["networkAccess"] = ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue;
        //                dr["usage"] = ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue;
        //                dt.Rows.Add(dr);
        //            }
        //            dr = dt.NewRow();
        //            //dr["ID"] = "new";
        //            dr["SortID"] = dt.Rows.Count + 1;
        //            dr["name"] = "";
        //            dr["prefix"] = "";
        //            dr["startRange"] = "";
        //            dr["endRange"] = "";
        //            dr["RangeSortOrder"] = "0";
        //            dr["networkAccess"] = "3";
        //            dr["usage"] = "3";
        //            dt.Rows.Add(dr);
        //            dgISDNServices.DataSource = dt;
        //            dgISDNServices.Visible = true;
        //            dgISDNServices.DataBind();
        //            lblNoISDNServices.Visible = false;
        //        }
        //        else
        //        {
        //            errLabel.Text = "More than 10 ISDN Services can not be added.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace + " : " + ex.Message;
        //        errLabel.Visible = true;
        //    }
        //}

        //protected void ChangeIPOrder(Object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList lstTemp = (DropDownList)sender;
        //        //Response.Write(Int32.Parse(lstTemp.SelectedValue) + " : " + dgIPServices.Items.Count);
        //        if (Int32.Parse(lstTemp.SelectedValue) > dgIPServices.Items.Count)
        //        {
        //            errLabel.Text = "Invalid Selection.";
        //            lstTemp.SelectedValue = "1";
        //        }
        //        else
        //            foreach (DataGridItem dgi in dgIPServices.Items)
        //                if (((DropDownList)dgi.FindControl("lstOrder")).SelectedValue.Equals(lstTemp.SelectedValue))
        //                {
        //                    errLabel.Text = "This value has already been taken.";
        //                    lstTemp.SelectedValue = dgi.ItemIndex.ToString();
        //                    break;
        //                }
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace;
        //    }
        //}
        //protected void ChangeISDNOrder(Object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList lstTemp = (DropDownList)sender;
        //        //Response.Write(Int32.Parse(lstTemp.SelectedValue) + " : " + dgIPServices.Items.Count);
        //        if (Int32.Parse(lstTemp.SelectedValue) > dgISDNServices.Items.Count)
        //        {
        //            errLabel.Text = "Invalid Selection.";
        //            lstTemp.SelectedValue = "1";
        //        }
        //        else
        //            foreach (DataGridItem dgi in dgISDNServices.Items)
        //                if (((DropDownList)dgi.FindControl("lstOrder")).SelectedValue.Equals(lstTemp.SelectedValue))
        //                {
        //                    errLabel.Text = "This value has already been taken.";
        //                    lstTemp.SelectedValue = dgi.ItemIndex.ToString();
        //                    break;
        //                }
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace;
        //    }
        //}
        //protected void ValidateIP(Object sender, ServerValidateEventArgs e)
        //{
        //    try
        //    {
        //        customvalidation.Text = "";
        //        string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
        //        Regex check = new Regex(pattern);
        //        String IPToValidate = txtPortP.Text;
        //        bool valid = true;
        //        if (lstInterfaceType.SelectedItem.Text.Equals("1"))
        //        {
        //            if (IPToValidate == "")
        //            {
        //                valid = false;
        //            }
        //            else
        //            {
        //                valid = check.IsMatch(IPToValidate, 0);
        //            }
        //            if (valid.Equals(true))
        //                e.IsValid = true;
        //            else
        //                e.IsValid = false;
        //            foreach (DataGridItem dgi in dgIPServices.Items)
        //            {
        //                //Response.Write(((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue);
        //                if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals("1"))
        //                {
        //                    String IPToValidate1 = ((TextBox)dgi.FindControl("txtAddress")).Text;
        //                    if (IPToValidate1 == "")
        //                        valid = false;
        //                    else
        //                        valid = check.IsMatch(IPToValidate1, 0);
        //                    //Response.Write("IPToValidate = " + IPToValidate1 + "valid: " + valid);
        //                    customvalidation.Text = "Invalid Start and End Range";
        //                }
        //            }
        //            foreach (DataGridItem dgi in dgISDNServices.Items)
        //            {
        //                if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
        //                {
        //                    if (((TextBox)dgi.FindControl("txtEndRange")).Text.Length > 22 || ((TextBox)dgi.FindControl("txtStartRange")).Text.Length > 22)
        //                    {
        //                        valid = false;
        //                        customvalidation.Text = "Max. length for start and end range is 22 characters.";
        //                    }
        //                    else
        //                    {
        //                        Double startRange = Double.Parse(((TextBox)dgi.FindControl("txtStartRange")).Text);
        //                        Double endRange = Double.Parse(((TextBox)dgi.FindControl("txtEndRange")).Text);
        //                        if (endRange <= startRange)
        //                        {
        //                            valid = false;
        //                            customvalidation.Text = "Invalid Start and End Range";
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        e.IsValid = valid;
        //        //Response.End();
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Text = ex.StackTrace;
        //        e.IsValid = false;
        //    }
        //}
        protected void DisableAllControls(Control ctrl)
        {
            try
            {
                //Response.Write("<br>" + ctrl.ID);
                foreach (Control ctl in ctrl.Controls)
                {
                    //if (ctl is TextBox)
                    //    ((TextBox)ctl).Enabled = false;
                    if (ctl is Button)
                        ((Button)ctl).Visible = false;
                    //if (ctl is DropDownList)
                    //    ((DropDownList)ctl).Enabled = false;
                    //if (ctl is ListBox)
                    //    ((ListBox)ctl).Enabled = false;
                    //if (ctl is CheckBox)
                    //    ((CheckBox)ctl).Enabled = false;
                    //if (ctl is RadioButtonList)
                    //    ((RadioButtonList)ctl).Enabled = false;
                    if (ctl is DataGrid)
                        ((DataGrid)ctl).Enabled = false;
                    DisableAllControls(ctl);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Visible = true;
            }
        }
    }
}
