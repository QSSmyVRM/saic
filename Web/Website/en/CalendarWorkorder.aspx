<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_CalendarWorkorder.CalendarWorkorder" enableEventValidation="false" EnableSessionState="true" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <%--FB 2779--%>
    <meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --><%--FB 2779--%>
	<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
    <script src="script/mytreeNET.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript">
        var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
        parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
    </script>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <script language="JavaScript" src="inc/functions.js"></script>	
    <script type="text/javascript" src="script\mousepos.js"></script>
    <script type="text/javascript" src="script\showmsg.js"></script>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="script/Workorder.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>
	<html>
	<head>
	    <script language="javascript">
	        function ViewDetails(e)
	        {
	          //Fogbugz 1113 - Start
	          //ViewWorkorderDetails(e.value(), e.tag("ConfID"));
	          ViewWorkorderDetails(e.value(), e.tag("ConfID"),queryField("t"));
	          //Fogbugz 1113 - End
	        }
	        function getUsers(evt)
	        {
                var obj;
                if (window.event != window.undefined)
                   obj =  window.event.srcElement;
                else
                   obj = evt.target; 
                   
                var treeNodeFound = false;
                var checkedState; 
                
                if (obj.tagName == "INPUT" && obj.type == "checkbox") 
                {    //AddResource(obj.name, obj.id, AddResource_CallBack);
                        alert(obj.parentNode.title);
                }
            }
            function showcalendar()
            {
                if (document.getElementById ("flatCalendarDisplay").style.display == "")
	                document.getElementById ("flatCalendarDisplay").style.display = "none";
                else
	                document.getElementById ("flatCalendarDisplay").style.display = "";
            }

	        function initsubtitle ()
            {
                var dd, m = "";

                var weekday=new Array(7)
                weekday[0]="Sunday"
                weekday[1]="Monday"
                weekday[2]="Tuesday"
                weekday[3]="Wednesday"
                weekday[4]="Thursday"
                weekday[5]="Friday"
                weekday[6]="Saturday"

                var months=new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
                var strcalicon = "<img src='image/calendar.gif' border='0' width='20' height='20' id='cal_trigger' style='cursor: pointer;' title='Date selector' onclick='showcalendar();' />";//Edited For FF...

                dd = new Date(confdate);
                m += weekday[dd.getDay()] + ", " + months[dd.getMonth()] + " " + dd.getDate() + ", " + dd.getFullYear();
                startDate = (dd.getMonth() + 1) + "/" + dd.getDate() + "/" + dd.getFullYear();
                //alert('<%=Session["systemTimezone"] %>');
                //Edited For FF...
                document.getElementById ("subtitlenamediv").innerHTML = "<table><tr><td><a href='javascript: showcalendar()'  style='cursor: pointer;color=blue'><span style='font-size: 14px; color=blue; font-weight: bold;'><u>" + m + ' <%=Session["systemTimezone"] %>' + "</u></span></a></td>" + 
	                "<td>" + strcalicon + "</td></tr></table>";
            }
            function datechg() {
                //alert(confdate);
                DataLoading("1");
                document.getElementById("<%=txtSelectedDate.ClientID %>").value=confdate;
	            document.getElementById ("flatCalendarDisplay").style.display = "none";
	            initsubtitle();
                document.getElementById("__EVENTTARGET").value="ChangeDate";
                __doPostBack("ChangeCalendarDate", document.getElementById("__EVENTTARGET").value);
            }
            function DataLoading(val)
            {
                if (val=="1")
                    document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' >"; // FB 2742
                else
                    document.getElementById("dataLoadingDIV").innerHTML="";                   
            }
	    </script>
	</head>
	<body>
<center>
    <h3>Workorder Calendar</h3>
</center>

<form id="frmCalendar" runat="server">
                <div id="divButton">
                    <%--code added for Soft Edge button--%>
                    <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;background-color:Transparent;border:None;'/>
                    <asp:Button ID="btnDaily" Text="Daily" OnCommand="ChangeView" runat="server" CommandArgument="1" Width="80px" /><%--FB 2664 --%>
                    <asp:Button ID="btnWeekly" Text="Weekly" OnCommand="ChangeView" runat="server" CommandArgument="2" Width="80px" /><%--FB 2664 --%>
                    <asp:Button ID="btnMonthly" Text="Monthly" OnCommand="ChangeView" runat="server" CommandArgument="3" Width="80px" /><%--FB 2664 --%>
                </div>
    <table cellspacing="0" cellpadding="1" border="0" width="100%">
        <tr>
            <td align="center" colspan="2">
                <div id="subtitlenamediv"></div>
                <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%> 
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table><tr><td><div id="flatCalendarDisplay" style="z-index:999;cursor:pointer"></div></td></tr></table>
            </td>
        </tr>
        <tr>
            <td  class="tableHeader">Last Name begins with</td>
            <td align="center">
                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" valign="top">
                <asp:Panel runat="server" ID="pnlUsers" Height="600" Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="0">
                    <table border="0" width="332" class="treeSelectedNode">
                        <tr>
                            <td valign="top" width="25%">
                                <table width="100%" >
                                    <tr>
                                        <td align="center" width="15%"><asp:LinkButton ID="LinkButton1" runat="server" Text="0-a" CommandArgument="a" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="btnAlpha" runat="server" Text="A" CommandArgument="A" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton2" runat="server" Text="B" CommandArgument="B" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton3" runat="server" Text="C" CommandArgument="C" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton4" runat="server" Text="D" CommandArgument="D" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton5" runat="server" Text="E" CommandArgument="E" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton6" runat="server" Text="F" CommandArgument="F" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton7" runat="server" Text="G" CommandArgument="G" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton8" runat="server" Text="H" CommandArgument="H" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton9" runat="server" Text="I" CommandArgument="I" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton10" runat="server" Text="J" CommandArgument="J" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton11" runat="server" Text="K" CommandArgument="K" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton12" runat="server" Text="L" CommandArgument="L" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton13" runat="server" Text="M" CommandArgument="M" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton14" runat="server" Text="N" CommandArgument="N" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton15" runat="server" Text="O" CommandArgument="O" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton16" runat="server" Text="P" CommandArgument="P" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton17" runat="server" Text="Q" CommandArgument="Q" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton18" runat="server" Text="R" CommandArgument="R" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton19" runat="server" Text="S" CommandArgument="S" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton20" runat="server" Text="T" CommandArgument="T" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton21" runat="server" Text="U" CommandArgument="U" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton22" runat="server" Text="V" CommandArgument="V" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton23" runat="server" Text="W" CommandArgument="W" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton24" runat="server" Text="X" CommandArgument="X" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton25" runat="server" Text="Y" CommandArgument="Y" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton26" runat="server" Text="Z" CommandArgument="Z" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" class="blackblodtext">
                                <asp:CheckBoxList AutoPostBack="true" ID="lstUsers" runat="server" OnSelectedIndexChanged="GetUserWorkOrders" onclick="javascript:DataLoading(1)"></asp:CheckBoxList>
                                <asp:Label runat="server" ID="tblNoUsers" CssClass="lblError" Text="No User(s) found" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Table ID="tblPaging" runat="server" Width="100%" >
                                    <asp:TableRow Width="100%" >
                                        <asp:TableCell Width="100%"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                    </table>
                    <asp:TextBox ID="txtAlpha" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtPageNo" runat="server" Visible="false"></asp:TextBox>
                </asp:Panel>
            </td>
            <td valign="top" align="left"><%--FB 2992--%>
                <div style="vertical-align:middle;Width:98%" >
                <DayPilot:DayPilotScheduler CssClass="tableHeader" RowHeaderWidth="200" ID="schDaypilot" runat="server" Days="1" Visible="true"
                    DataStartField="start" DataEndField="end" DataTextField="Name" DataValueField="ID" DataTagFields="ConfID,ToolText"
                    HeaderFontSize="8pt" HeaderHeight="17" DataResourceField="user" EventHeight="75" BubbleID="Details" ShowToolTip="false" 
                    CellDuration="30" CellWidth="20" CellGroupBy="Hour" EventClickHandling="JavaScript"
                    EventResizeHandling="CallBack" EventClickJavaScript="ViewDetails(e);" Layout="TableBased"
                    ClientObjectName="dps1" Width="900" OnBeforeTimeHeaderRender="BeforeTimeHeaderRender"/><%--FB 2087--%>
                <DayPilot:DayPilotBubble ID="Details" runat="server"  Visible="true"  Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                </div>
                <asp:Label ID="lblNoUsers" Text="Select one or more users." runat="server" CssClass="lblError" Visible="true"></asp:Label>
            </td>
        </tr>
    </table>  
    <asp:TextBox ID="txtType" Visible="false" runat="server"></asp:TextBox>  
    <input type="hidden" ID="txtSelectedDate" runat="server" />
    <input type="hidden" ID="startDate" runat="server" /><%--FB 2305--%>
    <input type="hidden" ID="endDate" runat="server" />
    <input type="hidden" ID="view" runat="server" />
</form>
<script language="javascript">

var curdate = new Date();

if (document.getElementById("txtSelectedDate").value == "") {
	confdate = (curdate.getMonth() + 1) + "/" + curdate.getDate() + "/" + curdate.getFullYear();
	showFlatCalendar(0, '%m/%d/%Y');
} else {
	confdate = document.getElementById("txtSelectedDate").value;
	showFlatCalendar(0, '%m/%d/%Y', document.getElementById("txtSelectedDate").value);
}
document.getElementById ("flatCalendarDisplay").style.position = 'absolute';
document.getElementById ("flatCalendarDisplay").style.top = 200;
document.getElementById ("flatCalendarDisplay").style.height = 150;
document.getElementById ("flatCalendarDisplay").style.textAlign = "center";
document.getElementById ("flatCalendarDisplay").style.display = "none";
document.getElementById("divButton").style.position = 'absolute';
document.getElementById("divButton").style.top = 150;
document.getElementById("divButton").style.height = 150;
document.getElementById("divButton").style.left = 10; // FB 2050 //FB 2897
document.getElementById("divButton").style.width = 350; // FB 2050

DataLoading("0");
initsubtitle();
</script>
</body></html> 
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
	<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->