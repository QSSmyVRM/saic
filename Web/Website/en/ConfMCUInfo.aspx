﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_ConfMCUInfo.ConfMCUInfo" %><%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ConfMCUInfo</title>
    <script type="text/javascript">    // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>

    <script type="text/javascript" src="script/errorList.js"></script>
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

</head>
<body>
    <form id="frmconfMCUinfo" runat="server">
    <div>
        <center>
            <br />
            <h3>
                <asp:Label ID="lblMCUInfo" runat="server" Font-Bold="true" Text="Server Time Details"></asp:Label></h3>
            <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table border="0" width="70%" align="center" cellpadding="2" cellspacing="3">
            <tr>
                <td class="subtitlexxsblueblodtext" colspan="3">
                    Conference Details
                </td>
            </tr>
            <tr>
                <td width="10%">
                </td>
                <td align="left" nowrap="nowrap" width="25%" valign="top"><%--FB 2508--%>
                    <b>Title</b>
                </td>
                <td width="65%">
                    <asp:Label ID="lblConfName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
                <td align="left" nowrap="nowrap">
                    <b>Start Date/time</b>
                </td>
                <td>
                    <asp:Label ID="lblStartTime" runat="server"></asp:Label>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="subtitlexxsblueblodtext" nowrap="nowrap" colspan="3">
                    Server Time
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left" nowrap="nowrap">
                    <b>Web </b>
                </td>
                <td>
                    <asp:Label ID="lblWebServerTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trDatabaseServerTime" runat="server">
                <td>
                </td>
                <td align="left" nowrap="nowrap">
                    <b>DataBase </b>
                    <br />
                </td>
                <td>
                    <asp:Label ID="lblDatabaseServerTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="subtitlexxsblueblodtext" colspan="3">
                    MCU
                </td>
            </tr>
            <tr class="blackblodtext">
                <td align="center" colspan="2">
                    Name
                </td>
                <td align="center">
                    Localized Time
                </td>
            </tr>
            <tr>
                <td height="1" colspan="3" bgcolor="CCCCCC">
                </td>
            </tr>
            <tr align="center">
                <td colspan="3">
                    <table id="tblConfMCUinfo" runat="server" border="0" width="90%">
                    </table>
                    <asp:Label runat="server" ID="tdNoMCU" Visible="false" Text="No MCU found."></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
