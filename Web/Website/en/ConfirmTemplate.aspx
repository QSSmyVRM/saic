<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_ConfirmTemplate" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Confirm Template</title>
</head>
<body>
          <script language="JavaScript">
<!--

	function frmsubmit(opr) {
	    DataLoading(1); // ZD 100176
		switch (opr) {
		    case "MODIFY"://ZD 100263		        
		        //document.frmConfirmtemplate.action = "dispatcher/userdispatcher.asp";
		        //document.frmConfirmtemplate.action = "managetemplate2.aspx?tid=<%=templateID%>&cmd=GetOldTemplate";
		        document.frmConfirmtemplate.action = "managetemplate2.aspx?tid=<%=templateID%>&cmd=O";		        
		        document.frmConfirmtemplate.cmd.value = "GetOldTemplate";
		        break;
			case "LIST":
				//document.frmConfirmtemplate.action = "dispatcher/conferencedispatcher.asp?cmd=GetTemplateList&frm=manage";
				document.frmConfirmtemplate.action = "ManageTemplate.aspx";
				//document.frmConfirmtemplate.cmd.value="GetTemplateList";
				break;
		}
		
		document.frmConfirmtemplate.opr.value = opr;
		document.frmConfirmtemplate.submit ();
	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
	    else
	        document.getElementById("dataLoadingDIV").innerHTML = "";
	}
	//ZD 100176 End

//-->
</script>
<div id="TempOK" runat="server" style="display:block">

            <center>
              <h3>Congratulations <% =userName %>!</h3>
            </center>            
			<br/>
            
            <center>
			<table width="90%" border="0" cellspacing="2" cellpadding="4">
              <tr> 
              <%--Window Dressing--%>
                <td colspan="3"  class="lblMessage" align="center"><%--FB 2487--%> 
                  <p><b><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Your template has been successfully submitted. You can use it for creating hearings later.<%}else{ %> Your template has been successfully submitted. You can use it for creating conferences later.<%} %></b></p><%--Edited for FB 1428--%>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" Text="" ForeColor="red" CssClass="lblError"></asp:Label>
                </td>
              </tr>
              <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
              <tr> 
                <td colspan="3">&nbsp;</td>
              </tr>
              
              <tr> 
                <td width="5%">&nbsp;</td>
              <%--Window Dressing--%>
                <td width="30%" align="left" class="subtitlexxsblueblodtext"><span style="margin-left:-20px"><b>Template Summary</b></span></td>
                <td width="65%">&nbsp; </td>
              </tr>
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top">Template Name</td><%--FB 2508--%>
                <td align="left">
                  <% =templateName %> &nbsp;&nbsp; 
				  <font color="darkblue"><b><i>

<% 
	if (templatePublic == "1" )
		Response.Write ("public");
	else
		Response.Write ("private");
	
%>

                  </i></b></font>
                </td>
              </tr>
 			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top">Template Description</td><%--FB 2508--%>
                <td align="left"><% =templateDescription %></td>
              </tr>
              <tr> 
                <td></td>
                <td></td>
                <td></td>
              </tr>
              
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="subtitlexxsblueblodtext"><span style="margin-left:-20px"><b><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Hearing Summary<%}else{ %> Conference Summary.<%} %></b></span></td><%--Edited for FB 1428--%>
                <td>&nbsp; </td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top">Name</td><%--FB 2508--%>
                <td align="left">
                  <% =confName %>&nbsp;&nbsp; 
				  <font color="darkblue"><b><i>

<% 
	if (publicConf == "1")
		Response.Write ("public");
	else
		Response.Write ("private");
	
%>

                  </i></b></font>
                </td>
              </tr>
              <%if(!(Application["Client"].ToString().ToUpper() == "MOJ")){%><%--Added for MOJ Phase 2 QA --%>
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Password</td>
                <td align="left"><% =confPassword %></td>
              </tr>
              <tr> 
              <%} %><%--Added for MOJ Phase 2 QA --%>
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top"><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Hearing Description<%}else{ %>Conference Description<%} %></td><%--Edited for FB 1428--%><%--FB 2508--%>
                <td align="left"><% =description %></td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Duration</td>
                <td align="left">
                <%
	                dhour = (Int32)durationMin / 60;
                    dmin =(Int32)durationMin - dhour * 60;

	                //Code added for FB 1216 - start
                    if (dhour == 0)
	                    Response.Write(dmin  + " minute(s)");
                    else if( dmin == 0 )
	                    Response.Write(dhour + " hour(s)");
	                else
	                    Response.Write(dhour + " hour(s) and " + dmin  + " minute(s)");
                	
                    //Code added for FB 1216 - end
                %>
                </td>
              </tr>
              
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Location</td>
                <td align="left"><% =locations %></td>
              </tr>           
              <tr id="trPart" runat="server"> <%--Added for FB 1425 QA Bug--%>
                <td></td>
              <%--Window Dressing--%>
                <td valign="top" align="left" class="blackblodtext">Participant(s) List</td>
                <td align="left"><table border=0 cellspacing=2 cellpadding=0><% =invited + invitee + cc %></table></td>
              </tr>
              <tr> 
                <td></td>
                <td></td>
                <td></td>
              </tr>              
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="subtitlexxsblueblodtext"><span style="margin-left:-20px"><b>Additional Information</b></span></td>
                <td></td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext"> 
                  Toll-free Help Line
                </td>
                <td align="left"><%=Application["contactPhone"]%></td>
              </tr>
            </table>
            </center>
            
			<br><br>


			<form name="frmConfirmtemplate" method="POST" action="">
			  <input type="hidden" name="cmd" value="">
			  <input type="hidden" name="templateID" value="<% =templateID %>">
			  <input type="hidden" name="opr" value="">
			  
			  <div align="center">
	            <table>
                  <tr> 
                    <td align="center"> 
					  <input type="button" runat="server" id="BtnEdit" name="ConfirmTemplateSubmit" value="Edit Template Settings" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('MODIFY');">
                    </td>
                    <td width="10%">&nbsp;</td>
                    <td align="center"> 
                      <%--code added for Soft Edge button--%>                    
					  <input type="button" onfocus="this.blur()" name="ConfirmTemplateSubmit0" value="Back to Template List" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('LIST');">
                    </td>
                  </tr>
                </table>
              </div>	
              
    </div>  
 <div id="divError" runat="server" style="display:none">
  <center>
 <table width="90%" border="0" cellspacing="2" cellpadding="4">
  <tr>
    <td align="center" height="10">
    </td>
  </tr>
  <tr>
    <td align="center">
      <font size="4"><b> Your request could not be completed due to the following reason:</b></font>
    </td>
  </tr>
  <tr>
    <td align="center" height="20">
    </td>
  </tr>
    <tr>
        <td align="center">
        <font size='3' color='red'><b>
        You are not permitted to edit this Template.<br/>
        Please contact your myVRM Administrator for further help.
        </b>
        </font>
        </td>
    </tr>
    <tr>
    <td align="center" height="20">
    </td>
  </tr>
  </table>
  </center>
 </div>
   
<script language="JavaScript">
<!--
 
if(document.getElementById("TempOK").style.display=='block')
    {
    document.frmConfirmtemplate.ConfirmTemplateSubmit0.focus ();
    document.getElementById("divError").style.display='none';
    }
    //Added for FB 1425 QA Bug START
    if('<%=Application["Client"].ToString().ToUpper()%>' =="MOJ")
	document.getElementById("trPart").style.display ="none";
	//Added for FB 1425 QA Bug END
    

//-->
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->			


</body>
</html>
