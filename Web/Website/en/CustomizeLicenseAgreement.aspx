﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.CustomizeLicenseAgreement" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="frmLicenseAgreement" runat="server">
    <div>
        <table width="100%" border="0">
            <tr>
                <td align="center" colspan="3">
                   <h3><asp:Label ID="lblHeader" runat="server" Text="End User License Agreement"></asp:Label></h3> 
                </td>
            </tr>
            <tr>
                <td align="center"  colspan="3">
                    <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <dxHE:ASPxHtmlEditor ID="dxHTMLEndUsrLicAgr" runat="server" Height="500px" Width="80%">
                                    <Settings AllowHtmlView="true" />
                                </dxHE:ASPxHtmlEditor>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td  style="height:15px" colspan="3">
                </td>
            </tr>
            <tr>
                <td align="right"><asp:Button ID="btnClose" runat="server" Text="Go Back"  CssClass="altMedium0BlueButtonFormat" OnClick="GoBack"/></td>
                <td style="width:10%"></td>
                <td align="left"><asp:Button ID="btnsubmit" runat="server" Text="Submit" Width="100pt"  OnClick="SetOrgLicenseAgreement" /></td> <%--FB 2796  --%>              
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
