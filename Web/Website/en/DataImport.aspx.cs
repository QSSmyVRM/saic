//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Data.Common;
using System.Data.OleDb;


namespace ns_DataImport
{
    public partial class DataImport : System.Web.UI.Page
    {
        String schemaPath = "C:\\VRMSchemas_V1.8.3\\";
        static DataSet ds;
        static DataTable dtMaster;
        //code added for BCS - start
        DataView dvZone;
        DataTable dtZone;
        myVRMNet.NETFunctions obj;
        DataSet dsUsers = new DataSet();
        DataView dvUsers;
        DataTable dtUsers = new DataTable();
        DataTable dtEpt = new DataTable();
        DataTable dtRoom = new DataTable();
        DataTable dtMCU = new DataTable();
      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("DataImport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            
//            if (IsPostBack)
//                //tblDataLoading.Visible = false;
            newErrGrid.Visible = false;
			//FB 2519 start
            if(btnImportTier1.Enabled==true)
                btnImportTier1.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportTier2.Attributes.Add("Class", "btndisable");

            if (btnImportTier1.Enabled == true)
                btnImportTier2.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportTier2.Attributes.Add("Class", "btndisable");

            if (btnImportDepartment.Enabled == true)
                btnImportDepartment.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportDepartment.Attributes.Add("Class", "btndisable");

            if (btnImportRooms.Enabled == true)
                btnImportRooms.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportRooms.Attributes.Add("Class", "btndisable");

            if (btnImportUsers.Enabled == true)
                btnImportUsers.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportUsers.Attributes.Add("Class", "btndisable");

            if (btnImportmcu.Enabled == true)
                btnImportmcu.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportmcu.Attributes.Add("Class", "btndisable");

            if (btnImportEndpoints.Enabled == true)
                btnImportEndpoints.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportEndpoints.Attributes.Add("Class", "btndisable");

            if (btnImportConferences.Enabled == true)
                btnImportConferences.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportConferences.Attributes.Add("Class", "btndisable");

            if (btnImportDefaultCSSXML.Enabled == true)
                btnImportDefaultCSSXML.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportDefaultCSSXML.Attributes.Add("Class", "btndisable");
            
            //FB 2519 End

        }

        protected void ImportTier1s(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;
                dtTemp = dtRoom.DefaultView.ToTable();//(true, new String[] { dtRoom.Columns["Room Name"].ColumnName, dtRoom.Columns["Tier One"].ColumnName, dtRoom.Columns["Tier Two"].ColumnName, dtRoom.Columns["Floor"].ColumnName, dtRoom.Columns["Room"].ColumnName, dtRoom.Columns["Room Phone"].ColumnName, dtRoom.Columns["Room Administrator"].ColumnName, dtRoom.Columns["Projector Available"].ColumnName, dtRoom.Columns["Media (None Audio-only Audio-Video)"].ColumnName, dtRoom.Columns["Endpoint name"].ColumnName, dtRoom.Columns["Time Zone"].ColumnName, dtRoom.Columns["Department"].ColumnName });
                Tier1 objTier1 = new Tier1(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objTier1.Process(ref cnt);
                errLabel.Text = cnt.ToString()+ "Top Tier(s) are imported successfully!";
                btnImportTier1.Enabled = false;
                btnImportTier1.Attributes.Add("Class", "btndisable");//FB 2519
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void GenerateDataTable(Object sender, EventArgs e)
        {
            try
            {
                if (fleMasterCSV.HasFile)
                {

                    string sourcefile = fleMasterCSV.FileName;
                    fleMasterCSV.SaveAs(Server.MapPath(".") + @"\upload\\" + sourcefile);
                    
                }
               ds = CreateDataTableFromCSV();
               if (ds != null)
               errLabel.Text = "Data Table has been generated successfully!";
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportTier2s(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;
                dtTemp = dtRoom.DefaultView.ToTable();//(true, new string[] { dtRoom.Columns["Room Name"].ColumnName, dtRoom.Columns["Tier One"].ColumnName, dtRoom.Columns["Tier Two"].ColumnName, dtRoom.Columns["Floor"].ColumnName, dtRoom.Columns["Room"].ColumnName, dtRoom.Columns["Room Phone"].ColumnName, dtRoom.Columns["Room Administrator"].ColumnName, dtRoom.Columns["Projector Available"].ColumnName, dtRoom.Columns["Media (None Audio-only Audio-Video)"].ColumnName, dtRoom.Columns["Endpoint name"].ColumnName, dtRoom.Columns["Time Zone"].ColumnName, dtRoom.Columns["Department"].ColumnName });
                Tier2 objTier2 = new Tier2(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objTier2.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " Middle Tier(s) are imported successfully!";
                btnImportTier2.Enabled = false;
                btnImportTier2.Attributes.Add("Class", "btndisable");//FB 2519
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportDepartments(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;//FB 2519
                dtTemp = dtRoom.DefaultView.ToTable();//(true, new string[] { dtMaster.Columns["Department"].ColumnName });//FB 2519
                Department objDepartment = new Department(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objDepartment.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " Department(s) are imported successfully!";
                btnImportDepartment.Enabled = false;
                btnImportDepartment.Attributes.Add("Class", "btndisable");//FB 2519
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportRooms(object sender, EventArgs e)
        {
            try
            {

                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;
                Session.Remove("dtRoomtable");
                dtTemp = dtRoom.DefaultView.ToTable();//(true, new string[] { dtRoom.Columns["Room Name"].ColumnName, dtRoom.Columns["Tier One"].ColumnName, dtRoom.Columns["Tier Two"].ColumnName, dtRoom.Columns["Floor"].ColumnName, dtRoom.Columns["Room"].ColumnName, dtRoom.Columns["Room Phone"].ColumnName, dtRoom.Columns["Room Administrator"].ColumnName, dtRoom.Columns["Projector Available"].ColumnName, dtRoom.Columns["Media (None Audio-only Audio-Video)"].ColumnName, dtRoom.Columns["Endpoint name"].ColumnName, dtRoom.Columns["Time Zone"].ColumnName , dtRoom.Columns["Department"].ColumnName });
                Room objRoom = new Room(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objRoom.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " Room(s) are imported successfully!";
                btnImportRooms.Enabled = false;
                btnImportRooms.Attributes.Add("Class", "btndisable");//FB 2519
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportUsers(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                DataTable dterror = new DataTable();
                dterror.Columns.Add("RowNo", typeof(string));
                dterror.Columns.Add("Reason", typeof(string));
                bool alluserimport = true;
                dtUsers = Session["dtUserstable"] as DataTable;
                Session.Remove("dtUserstable");
                dtTemp = dtUsers.DefaultView.ToTable();//(true, new string[] { dtUsers.Columns["First Name"].ColumnName, dtUsers.Columns["Last Name"].ColumnName, dtUsers.Columns["Outlook/Notes"].ColumnName, dtUsers.Columns["Initial Password"].ColumnName, dtUsers.Columns["Email"].ColumnName, dtUsers.Columns["Timezone"].ColumnName, dtUsers.Columns["User Role (User/Admin/Super Adminetc)"].ColumnName, dtUsers.Columns["Assigned MCU (only in case of multiple)"].ColumnName });
                User objUser = new User(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objUser.Process(ref cnt, ref alluserimport, ref dterror);
                errLabel.Text = cnt.ToString() + " User(s) are imported successfully!";
                btnImportUsers.Enabled = false;
                btnImportUsers.Attributes.Add("Class", "btndisable");//FB 2519
                
                if (alluserimport == false)
                {
                    newErrGrid.Visible = true;
                    newErrGrid.DataSource = dterror;
                    newErrGrid.DataBind();
                    errLabel.Text = "Failed To Create Users are listed below";
                    dterror.Clear();
                    ds = null;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        
        protected void Importmcu(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtMCU = Session["dtMCUtable"] as DataTable;
                Session.Remove("dtMCUtable");
                dtTemp = dtMCU.DefaultView.ToTable();//(true, new string[] { dtMCU.Columns["Name"].ColumnName, dtMCU.Columns["Login"].ColumnName, dtMCU.Columns["Password"].ColumnName, dtMCU.Columns["Vendor Type"].ColumnName, dtMCU.Columns["Timezone"].ColumnName, dtMCU.Columns["Control Port IP Address"].ColumnName, dtMCU.Columns["Firmware version"].ColumnName });
                mcu objmcu = new mcu(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objmcu.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " MCU(s) are imported successfully!";
                btnImportmcu.Enabled = false;
                btnImportmcu.Attributes.Add("Class", "btndisable");//FB 2519
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }

        protected void ImportEndpoints(object sender, EventArgs e) 
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtEpt = Session["dtEpttable"] as DataTable;
                Session.Remove("dtEpttable");
                dtTemp = dtEpt.DefaultView.ToTable();//(true, new string[] {dtEpt.Columns["Name"].ColumnName, dtEpt.Columns["Firmware Version"].ColumnName, dtEpt.Columns["Model"].ColumnName, dtEpt.Columns["Address"].ColumnName, dtEpt.Columns["Address Type"].ColumnName, dtEpt.Columns["Password"].ColumnName, dtEpt.Columns["Preferred Bandwidth(kbps)"].ColumnName, dtEpt.Columns["Preferred Dialing Option"].ColumnName, dtEpt.Columns["MCU Assignment"].ColumnName, dtEpt.Columns["Located outside the network"].ColumnName, dtEpt.Columns["SIP Address"].ColumnName });
                Endpoint objEndpoint = new Endpoint(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objEndpoint.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " EndPoint(s) are imported successfully!";
                btnImportEndpoints.Enabled = false;
                btnImportEndpoints.Attributes.Add("Class", "btndisable");//FB 2519

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        
        protected void ImportConferences(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtMaster.DefaultView.Sort = "ConfirmationNumber, StartDateTime, Duration, Room";
                dtTemp = dtMaster.DefaultView.ToTable(); //true, new string[] { dtMaster.Columns["ConferenceName"].ColumnName, dtMaster.Columns["StartDateTime"].ColumnName, dtMaster.Columns["Duration"].ColumnName, dtMaster.Columns["Tier1"].ColumnName, dtMaster.Columns["Tier2"].ColumnName, dtMaster.Columns["Room"].ColumnName, dtMaster.Columns["UserFirstName"].ColumnName, dtMaster.Columns["UserLastName"].ColumnName, dtMaster.Columns["ConferenceType"].ColumnName, dtMaster.Columns["Description"].ColumnName, dtMaster.Columns["TimeZone"].ColumnName, dtMaster.Columns["Remarks"].ColumnName });
                dtTemp.DefaultView.Sort = "ConfirmationNumber, StartDateTime, Duration, Room";
                Conference objConference = new Conference(1, dtTemp, Application["COM_ConfigPath"].ToString());
                
                //int i = 0;
                //foreach (DataRow dr in dtTemp.Rows)
                //{
                //    i++;
                //    Response.Write("<br>" + dr["Room"].ToString() + " : " + dr["ConferenceName"].ToString() + " : " + dr["StartDateTime"].ToString() + " : " + dr["ConfirmationNumber"].ToString());
                //}
                //tblDataLoading.Visible = true;
                int cnt = 0;
                objConference.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " Conference(s) are imported successfully!";
                btnImportConferences.Enabled = false;
                btnImportConferences.Attributes.Add("Class", "btndisable");//FB 2519
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }

        protected DataSet CreateDataTableFromCSV()
        {
            
            StreamReader sr = null;
            try
            {
                ds = new DataSet();
                ns_Logger.Logger log = new ns_Logger.Logger();
                String ExternalFileName = fleMasterCSV.FileName;
                ExternalFileName = Server.MapPath(".") + @"\upload\\" + ExternalFileName;
                String[] excelSheets = GetExcelSheetNames(ExternalFileName);
                if (excelSheets != null)
                {
                    for (int j = 0; j < excelSheets.Length; j++)
                    {
                        DataTable dt = new DataTable();
                        String shtname = excelSheets[j].Replace("$", "").Replace("'", "");

                        //if (shtname == "General Info")
                        //    continue;

                        switch (shtname)
                        {
                            case "Room Data":

                                dtRoom = ds.Tables[j];
                                Session["dtRoomtable"] = dtRoom;
                                break;
                            case "MCU Data":

                                dtMCU = ds.Tables[j];
                                Session["dtMCUtable"] = dtMCU;
                                break;
                            case "Endpoint Data":

                                dtEpt = ds.Tables[j];
                                Session["dtEpttable"] = dtEpt;
                                break;
                            case "User Data":

                                dtUsers = ds.Tables[j];
                                Session["dtUserstable"] = dtUsers;
                                break;
                            default:
                                break;

                        }


                        btnImportConferences.Enabled = true;
                        ds.Tables.Add(dt);
                    }
                    return ds;
                }
                else
                {
                    errLabel.Text = "Excel is not Uploaded";
                    return null;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
                return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                sr = null;
            }
        }

     
       

        //code added for BCS - start
        protected String GetConferenceTimeZoneID(String tZoneStr)
        {
            
            DataSet dsZone = new DataSet();
            myVRMNet.NETFunctions obj;
            String tZoneID = "33";
            String tZone = "";
            String[] tZoneArr = null;
             String[] tZoneGMTArr = null;
            try
            {
                //code added for BCS - start
                //Time Zone

                String zoneInXML = "<GetTimezones><UserID>11</UserID></GetTimezones>";
                String zoneOutXML;

                tZone = tZoneStr;

                if (dtZone == null)
                {
                    if (dsZone.Tables.Count == 0)
                    {
                        obj = new myVRMNet.NETFunctions();

                        zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                       
                        obj = null;

                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(zoneOutXML);
                        String sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;
                        XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                        if (nodes.Count > 0)
                        {
                            XmlTextReader xtr;

                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                            }

                            if (dsZone.Tables.Count > 0)
                            {
                                dvZone = new DataView(dsZone.Tables[0]);
                                dtZone = dvZone.Table;
                            }
                        }
                        //code added for BCS - start
                    }
                }

                if (tZone != "")
                {

                    tZoneArr = tZone.Split(' ');

                    foreach (String s in tZoneArr)
                    {
                        if (s.ToString().ToUpper() == "GMT")
                        {
                            tZoneID = "31";
                            break;
                        }
                        else if (s.Contains("GMT"))
                        {
                            if (dtZone.Rows.Count > 0)
                            {
                                foreach (DataRow row in dtZone.Rows)
                                {
                                    if (row["timezoneName"].ToString().Contains(s))
                                    {
                                        tZoneID = row["timezoneID"].ToString();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }

            return tZoneID;
        }

        protected String GetUsers(String firstName, String lastName)
        {

            string userid = "11";

            if (dsUsers.Tables.Count == 0)
            {
                obj = new myVRMNet.NETFunctions();

                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>11</userID>";
                inXML += "  <user>";
                inXML += "      <firstName>" + firstName + "</firstName>";
                inXML += "      <lastName>" + lastName + "</lastName>";
                inXML += "      <email></email>";
                inXML += "  </user>";
                inXML += "</login>";
                String outXML = obj.CallCOM("SearchUser", inXML, Application["COM_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                //String userName = xmldoc.SelectSingleNode("/oldUser/userName/firstName").InnerText + " " + xmldoc.SelectSingleNode("/oldUser/userName/lastName").InnerText;

                XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                if (nodes.Count > 0)
                {
                    XmlTextReader xtr;

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        dsUsers.ReadXml(xtr, XmlReadMode.InferSchema);
                    }

                    if (dsUsers.Tables.Count > 0)
                    {
                        dvUsers = new DataView(dsUsers.Tables[0]);
                        dtUsers = dvUsers.Table;
                    }
                }
            }

            if (dtUsers.Rows.Count > 0)
            {
                foreach (DataRow row in dtUsers.Rows)
                {
                    //String name = row["UserFirstName"].ToString() + "," + row["UserLastName"].ToString();
                    String name = row["FirstName"].ToString() + "," + row["LastName"].ToString();
                    if (name.ToLower() == firstName.ToLower() + "," + lastName.ToLower())
                    {
                        userid = row["userID"].ToString();
                        break;
                    }
                }
            }

            return userid;

        }
        //code added for BCS - end

        //Code added for Organization/CSS Module  -- Start
        #region ImportDefaultCSSXML
        protected void ImportDefaultCSSXML(object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML = "<SetDefaultCSSXML>";
                myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
                inXML += obj.OrgXMLElement();//Organization Module 
                myVRMNet.ImageUtil imageUtil = new myVRMNet.ImageUtil();
                string textXML = "";
                if (cssXMLFileUpload.PostedFile.FileName != "")
                    textXML = imageUtil.ConvertImageToBase64(cssXMLFileUpload.PostedFile.FileName);
                else
                {
                    errLabel.Text = "Please Upload the File";
                    errLabel.Visible = true;
                }

                inXML += "<DefaultCSSXml>" + textXML + "</DefaultCSSXml>";
                inXML += "</SetDefaultCSSXML>";

                String outXML = obj.CallMyVRMServer("SetDefaultCSSXML", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    this.errLabel.Text = obj.ShowErrorMessage(outXML);
                    this.errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text =" Default CSS XML has been imported successfully!";
                   // btnImportDefaultCSSXML.Enabled = false;
                }
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        #endregion
        //Code added for Organization/CSS Module -- End

        private String[] GetExcelSheetNames(string excelFile)
        {
            OleDbConnection objConn = null;
            System.Data.DataTable dt = null;

            try
            {
                // Connection String. Change the excel file to the file you

                // will search.

                   String connString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                   "Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";

               
                // Create connection object by using the preceding connection string.
                objConn = new OleDbConnection(connString);

                // Open connection with the database.
                objConn.Open();

                // Get the data table containg the schema guid.
                dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
              

                if (dt == null)
                    return null;

                String[] excelSheets = new String[dt.Rows.Count];
                int i = 0;

                // Add the sheet name to the string array.
                foreach (DataRow row in dt.Rows)
                {
                    excelSheets[i] = row["TABLE_NAME"].ToString();
                    i++;
                }

               
                for (int j = 0; j < excelSheets.Length; j++)
                {
                    dt = new DataTable();
                    String tempName = excelSheets[j].ToString();
                    new OleDbDataAdapter("SELECT * FROM [" + excelSheets[j].ToString() + "]", objConn).Fill(dt);
                    ds.Tables.Add(dt);
                }

                return excelSheets;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                // Clean up.
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }
     }
  }