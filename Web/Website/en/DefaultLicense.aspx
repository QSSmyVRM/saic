﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%--<%@ Page Language="C#" Inherits="ns_OrgDefaults.OrgDefaults" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_DefaultLicense.en_DefaultLicense"
    EnableEventValidation="false" %>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
  {%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%}
  else
  {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%} %>
<meta http-equiv="X-UA-Compatible" content="IE=8" />

<script runat="server"></script>

<script type="text/javascript" language="javascript" src='script/lib.js'></script>

<script type="text/javascript" src="script/calview.js"></script>

<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <script type="text/javascript">            // FB 2815
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script> 

</head>
<body>
    <form id="frmOrgProfile" runat="server" method="post" enctype="multipart/form-data"
    defaultbutton="btnSubmit">
    <%--FB 1840--%>
    <input type="hidden" runat="server" id="hdnOrganizationID" />
    <asp:TextBox ID="txtOrgID" Style="width: 0" Height="0" runat="server" BorderStyle="none"
        BorderWidth="0"></asp:TextBox>
    <div>
        <center>
            <div id="dataLoadingDIV" style="z-index: 1">
            </div>
            <h3>
                <asp:Label ID="lblTitle" runat="server" CssClass="h3" Text=""></asp:Label>
            </h3>
            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td align="center">
                    <table id="tblBasicDetails" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                        <tr>
                            <td align="center">
                                <span class="subtitleblueblodtext" style="margin-left: -15px">Manage License Defaults</span>
                            </td>
                            
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblOrgBasicDetails" cellpadding="2" cellspacing="2" border="0" style="width: 95%">                                    
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td align="left" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="blackblodtext" style="padding-left:300px">
                                                        <b><asp:Label ID="Label3" runat="server" Text="Assigned"  Height="35pt"></asp:Label></b>
                                                     </td>
                                                     <td align="center" style="width:195px"> <%--FB 2732--%><%--ZD 100263--%>
                                                        <asp:Label ID="Label4" runat="server" Text=" <b>Shared Resources Remaining</b> (*Includes the resources assigned to this organization)" 
                                                        Style="text-align: center" Font-Size="9pt"></asp:Label>
                                                    </td>                                                    
                                                </tr>
                                            </table>
                                        </td>
                                       </tr>
                                    <tr><%--FB 2693 Starts--%>
                                        <td colspan="2" style="vertical-align: top">
                                            <table width="100%" border="0" style="vertical-align: top; margin-top: inherit;">
                                                <tr>
                                                    <td valign="top" align="left" style="width: 2px">
                                                        <asp:ImageButton ID="img_Rooms" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                            Style="margin-left: -21px" vspace="0" hspace="0" Height="25" Width="25" /><%--FB 2730--%>
                                                    </td>
                                                    <td align="left">
                                                        <span class="blackblodtext" style="margin-left: -5px">Rooms</span>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="95%" id="tblRooms" align="left" runat="server" style="display: none;"
                                                border="0" cellpadding="0"><%--FB 2730--%>
                                                <%-- FB 2693 End--%>
                                                <tr style="display: none">
                                                    <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                                        Active Rooms
                                                    </td>
                                                    <td style="width: 20%" align="left" valign="top">
                                                        <asp:TextBox ID="TxtRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                        &nbsp;&nbsp;<span id="SpanActiveRooms" runat="server" class="orangesboldtext"></span>
                                                        <asp:RegularExpressionValidator ID="regRooms" ControlToValidate="TxtRooms" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnRooms" />
                                                    </td>
                                                    <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 25%" align="left" valign="top">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 21%" align="left" valign="top" class="blackblodtext"> <%--FB 2730--%>
                                                        Video Rooms
                                                    </td>
                                                    <%-- FB 2693--%>
                                                    <td style="width: 32%" align="left" valign="top">
                                                        <%--Edited for FF--%><%-- FB 2693--%>
                                                        <asp:TextBox ID="TxtVRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                        &nbsp;&nbsp;<span id="SpanActiveVRooms" runat="server" class="orangesboldtext" style="margin-left: 60px"></span>
                                                        <asp:RegularExpressionValidator ID="regVRooms" ControlToValidate="TxtVRooms" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnVRooms" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                        End Points
                                                    </td>
                                                    <%--FB 2693--%>
                                                    <td style="width: 25;" align="left" valign="top">
                                                        <asp:TextBox ID="TxtEndPoint" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                        &nbsp;&nbsp;<span id="SpanActiveEpts" runat="server" class="orangesboldtext" style="margin-left: 60px"></span>
                                                        <asp:RegularExpressionValidator ID="regEndPoint" ControlToValidate="TxtEndPoint"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnEndPoint" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <%--FB 2586 Start--%>
                                                    <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                        Non-Video Rooms
                                                    </td>
                                                    <%-- FB 2693--%>
                                                    <td style="width: 25%;" align="left" valign="top">
                                                        <asp:TextBox ID="TxtNVRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                        &nbsp;&nbsp;<span id="SpanActiveNVRooms" runat="server" class="orangesboldtext" style="margin-left: 60px"></span>
                                                        <asp:RegularExpressionValidator ID="regNVRooms" ControlToValidate="TxtNVRooms" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnNVRooms" />
                                                    </td>
                                                </tr>
                                                <%--FB 2693 start--%>
                                                <tr>
                                                    <%--UI MODIFICATION--%>
                                                    <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                        VMR Rooms
                                                    </td>
                                                    <%-- FB 2693--%>
                                                    <td style="width: 25%" align="left" valign="top">
                                                        <asp:TextBox ID="TxtVMR" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                        &nbsp;&nbsp;<span id="SpanVMRRooms" runat="server" class="orangesboldtext" style="margin-left: 60px"></span>
                                                        <asp:RegularExpressionValidator ID="regVMR" ControlToValidate="TxtVMR" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnVMR" />
                                                    </td>
                                                   
                                                </tr>
                                                <%--FB 2426 start--%>
                                                <tr>
                                                    <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                        Guest Rooms
                                                    </td>
                                                    <td style="width: 25%" align="left" valign="top">
                                                        <asp:TextBox ID="TxtExtRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                        &nbsp;&nbsp;<span id="SpanExternalRooms" runat="server" class="orangesboldtext" style="margin-left: 60px"></span>
                                                        <asp:RegularExpressionValidator ID="regExtRooms" ControlToValidate="TxtExtRooms"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnExtRooms" />
                                                    </td>
                                                </tr>
                                                <%--FB 2694  Starts--%>
                                                <tr>
                                                    <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                        Active VC Hotdesking Rooms
                                                    </td>
                                                    <td style="width: 25%" align="left" valign="top">
                                                        <asp:TextBox ID="txtVCHotRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanActiveVC" runat="server" class="orangesboldtext" style="margin-left: 64px"></span>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtVCHotRooms"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnVCHotdesking" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                        Active RO Hotdesking Rooms
                                                    </td>
                                                    <td style="width: 25%" align="left" valign="top">
                                                        <asp:TextBox ID="txtROHotRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanActiveRO" runat="server" class="orangesboldtext" style="margin-left: 64px"></span>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtROHotRooms"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnROHotdesking" />
                                                    </td>
                                                </tr>
                                                <%--FB 2694  Ends--%>
                                            </table>
                                            <%-- FB 2730 Start--%>
                                            <%--<td colspan="2" style="vertical-align: top">--%>
                                     <table>
                                     <tr>
                                        <td valign="top" align="left">
                                            <asp:ImageButton ID="img_MCUs" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Style="margin-left: -20px" vspace="0" hspace="0" Height="25" Width="25" /><%--FB 2730--%>
                                        </td>
                                        <td>
                                            <span class="blackblodtext" style="margin-left: -5px">MCUs</span>
                                        </td>
                                    </tr>
                                </table>
                                <table id="tblMCUs" align="left" runat="server" style="display: none; "
                                    width="95%" border="0" cellpadding="0"><%--FB 2730--%>
                                    <%--FB 2693 End--%>
                                    <tr>
                                        <%--FB 2586 UI MODIFICATION--%>
                                        <td style="width: 23%;" align="left" valign="top" class="blackblodtext"> <%--FB 2730--%>
                                            Standard MCU
                                        </td>
                                        <%--FB 2693--%>
                                        <td style="width: 35%;" align="left" valign="top"> <%--FB 2730--%>
                                            <%--FB 2693--%>
                                            <asp:TextBox ID="TxtMCU" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                            &nbsp;&nbsp;<span id="SpanActiveMCU" runat="server" class="orangesboldtext" style="margin-left: 59px"></span>
                                            <asp:RegularExpressionValidator ID="regMcu" ControlToValidate="TxtMCU" Display="dynamic"
                                                runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnMCU" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--FB 2586 UI MODIFICATION--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Enhanced MCU
                                        </td>
                                        <%--FB 2693--%>
                                        <td style="width: 25%;" align="left" valign="top">
                                            <%--FB 2693--%>
                                            <asp:TextBox ID="TxtMCUEncha" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                            &nbsp;&nbsp;<span id="SpanActiveMCUEnchanced" runat="server" class="orangesboldtext"
                                                style="margin-left: 59px"></span>
                                            <asp:RegularExpressionValidator ID="regMcuencha" ControlToValidate="TxtMCUEncha"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnMCUEncha" />
                                        </td>
                                    </tr>
                                    
                                </table>
                               
                            
                           <%-- FB 2730 End--%>
                                <%--FB 2693 start--%>
                                <table width="100%"><%--FB 2730--%>
                                    <tr>
                                        <td align="left"> <%--FB 2730--%>
                                            <table border="0" width="100%">
                                                <tr>
                                                    <td valign="top" align="left" style="width: 2px">
                                                        <asp:ImageButton ID="img_Users" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                            Style="margin-left: -24px; vertical-align: top;" vspace="0" hspace="0" Height="25"
                                                            Width="25" />
                                                    </td>
                                                    <td align="left">
                                                        <span class="blackblodtext" style="margin-left: -6px;">Users</span>
                                                    </td>
                                                </tr>
                                            </table>
                                            </td>
                                    </tr>
                                 </table>
                                            <table id="tblusers" runat="server" align="left" style="display: none;" width="100%"
                                                cellpadding="0" border="0">
                                                <%--FB 2693 End--%>
                                                <tr>
                                                    <%--UI MODIFICATION--%>
                                                    <td style="width: 38%" align="left" valign="top" class="blackblodtext"> 
                                                        Active Users
                                                    </td>
                                                    <td style="width: 62%" align="left" valign="top">
                                                        <%--FB 2347--%>
                                                        <asp:TextBox ID="TxtUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanActiveUsers" runat="server" class="orangesboldtext" style="margin-left: 59px"></span><%--FB 2694--%>
                                                        <asp:RegularExpressionValidator ID="regUsers" ControlToValidate="TxtUsers" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnUsers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 36%" align="left" valign="top" class="blackblodtext">
                                                        Outlook Users
                                                    </td>
                                                    <%--Edited for FB 2098--%>
                                                    <td style="width: 64%" align="left" valign="top" colspan="3">
                                                        <asp:TextBox ID="TxtExUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanActiveExUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                        <asp:RegularExpressionValidator ID="regExcUsers" ControlToValidate="TxtExUsers" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnEUsers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 36%" align="left" valign="top" class="blackblodtext">
                                                        Notes Users
                                                    </td>
                                                    <%--Edited for FB 2098--%>
                                                    <td style="width: 64%" align="left" valign="top" colspan="3">
                                                        <asp:TextBox ID="TxtDomUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanActiveDomUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                        <asp:RegularExpressionValidator ID="regDomUsers" ControlToValidate="TxtDomUsers"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnDUsers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <%--FB 1979--%>
                                                    <td style="width: 36%" align="left" valign="top" class="blackblodtext">
                                                        Mobile Users
                                                    </td>
                                                    <td style="width: 64%" align="left" valign="top" colspan="3">
                                                        <asp:TextBox ID="TxtMobUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanActiveMobUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                        <asp:RegularExpressionValidator ID="regMobUsers" ControlToValidate="TxtMobUsers"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnMUsers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <%--FB 2693--%>
                                                    <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                                        PC Users
                                                    </td>
                                                    <td style="width: 32%" align="left" valign="top" colspan="3">
                                                        <asp:TextBox ID="txtPCUser" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanActivePCUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                        <asp:RegularExpressionValidator ID="regPCUsers" ControlToValidate="txtPCUser" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnPCUser" />
                                                    </td>
                                                </tr>
                                                <%--FB 2426 Start--%>
                                                <tr>
                                                    <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                                        Guest Room Per User
                                                    </td>
                                                    <td style="width: 32%" align="left" valign="top" colspan="3">
                                                        <asp:TextBox ID="TxtGstPerUser" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                            id="SpanGuRoomUser" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                        <asp:RegularExpressionValidator ID="RegGstPerUser" ControlToValidate="TxtGstPerUser"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnGstPerUser" />
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                
                            
                           <%-- FB 2730 start--%>
                            <table style="vertical-align: top">
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:ImageButton ID="img_Modules" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Style="margin-left: -21px" vspace="0" hspace="0" Height="25" Width="25" />
                                        </td>
                                        <td align="left">
                                            <span class="blackblodtext" style="margin-left: -5px">Modules</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="95%" height="150pt" id="tblModules" runat="server" style="display: none;"
                                    border="0" cellpadding="1"> <%--FB 2730 start--%>
                                    <tr>
                                        <td style="width: 21%;" align="left" valign="top" class="blackblodtext"> <%--FB 2730--%>
                                            Audio/Visual
                                        </td>
                                        <%-- FB 2570 --%><%-- FB 2693--%>
                                        <td style="width: 31%" align="left" valign="top"><%--FB 2730--%>
                                            <asp:CheckBox ID="ChkFacility" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanActiveFacility" runat="server" class="orangesboldtext"
                                                style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnFacility" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;" align="left" valign="top" class="blackblodtext">
                                            Catering
                                        </td>
                                        <%-- FB 2693--%>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkCatering" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanActiveCat" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnCatering" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Edited for FB 1706--%>
                                        <td style="width: 15%;" align="left" valign="top" class="blackblodtext">
                                            Faciltity Services
                                        </td>
                                        <%--FB 2570--%>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkHK" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanActiveHK" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnHK" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;" align="left" valign="top" class="blackblodtext">
                                            API
                                        </td>
                                        <%-- FB 2693--%>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkAPI" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanActiveAPI" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnAPI" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                            Blue Jeans
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="chkBlueJeans" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanBJ" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnBJ" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                            Jabber
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="chkJabber" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanJabber" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnJabber" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                            Lync
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="chkLync" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanLync" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnLync" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                            Vidtel
                                        </td>
                                        <%-- FB 2693--%>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="chkVidtel" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanVidtel" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnVidtel" />
                                        </td>
                                    </tr>
                                    
                                     <tr>
                                        <td style="width: 15%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                            Adv Rep Mod
                                        </td>
                                        <%-- FB 2693--%>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkAdvReport" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanAdvReport" runat="server" class="orangesboldtext" style="margin-left: 93px"></span>
                                            <input type="hidden" runat="server" id="hdnAdvReport" />
                                        </td>
                                    </tr>
                                    
                                    <%--FB 2693 - Commented--%>
                                    <%-- <tr>
                                                <td style="height:16pt" align="left" valign="top" class="blackblodtext">&nbsp;&nbsp;PC Module</td> 
                                                <td style="width:25%" align="left" valign="top">
                                                  <asp:CheckBox ID="ChkPC" runat="server" />&nbsp;&nbsp;<span id="SpanActivePC" runat="server" class="orangesboldtext" style="margin-left:95px"></span>
                                                   <input type="hidden" runat="server" id="hdnPC" />
                                                     </td>
                                                      </tr>--%>
                                    <tr>
                                    </tr>
                                </table>
                                <%--FB 2730 End--%>
                            </td>
                        </tr>
                        <%--FB 2693 Ends--%>
                        <%--FB 2426 End--%>
                        <%--FB 2579 End--%>
                    </table>
                </td>
            </tr>
            </table> </td> </tr>
            <tr>
                <td style="height: 60px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table id="tblButtons" cellpadding="2" cellspacing="2" style="width: 90%">
                        <tr>
                            <td align="center" style="width: 33%">
                                <input name="btnClose" id="btnClose" type="button" class="altLongBlueButtonFormat"
                                    value="Close" />
                            </td>
                            <td align="center" style="width: 33%">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="altLongBlueButtonFormat"
                                    OnClick="SetDefaultLicense" ValidationGroup="Submit" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function() {

        $('#btnSubmit').click(function() {
            $('#popupdiv', window.parent.document).fadeOut();
            $('#PopupDefaultLicense', window.parent.document).fadeOut();
            return true;
        });

        $('#btnClose').click(function() {
            $('#popupdiv', window.parent.document).fadeOut();
            $('#PopupDefaultLicense', window.parent.document).fadeOut();
        });
    });

    // FB 2693 Start
    function ExpandCollapse(img, str, frmCheck) {

        obj = document.getElementById(str);

        if (obj != null) {
            if (frmCheck == true) {
                if (document.getElementById("chkExpandCollapse").checked) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
            if (frmCheck == false) {
                if (img.src.indexOf("minus") >= 0) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
        }
    }
    // FB 2693 End

</script>

