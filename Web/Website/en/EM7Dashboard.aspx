﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_EM7Dashboard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--FB 2779--%>
<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
</script>

<script type="text/javascript" src="script/mousepos.js"></script>

<script type="text/javascript" src="script/managemcuorder.js"></script>



<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>EM7 Dashboard</title>
</head>
<body>
    <form id="form1" runat="server">
 
    <div style="height: 600px">
        <table>
            <tr>
                <td>
                    <h3>
                        EM7 Dashboard</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LblError" ForeColor="red" runat="server" Font-Bold="true" Font-Size="small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                
                  <asp:Image ID="Image1" src="image/info.png" runat="server"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <iframe id="Ifrm_EM7" runat="server" width="990" height="500" scrolling="yes">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <%--code added for Soft Edge button--%>

    <script type="text/javascript" src="inc/softedge.js"></script>

    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
