<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ESEventReport" EnableEventValidation="false"  Debug="true" %>

<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> <%--FB 2050--%>
<%--FB 2779 Ends--%>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"     Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly ="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%--Basic validation function--%>
<script type="text/javascript">   
  
    var servertoday = new Date();
    
     function fnClear() 
    {    
        MainGrid.UnselectRows();
        
        var hdnRequestID = document.getElementById("hdnRequestID");        
        hdnRequestID.value = '';
        
        return false;
    }
    
    function grid_SelectionChanged(s,e) 
    {    
         s.GetSelectedFieldValues("RequestID",GetSelectedFieldValuesCallback);
    }
    
    function GetSelectedFieldValuesCallback(values) 
    {        
        var hdnRequestID = document.getElementById("hdnRequestID");

        hdnRequestID.value = '';
        
        for(var i=0;i<values.length;i++) 
        {
            if(hdnRequestID.value == "")
               hdnRequestID.value = "'" + values[i] + "'"; //FB 2363X
            else
               hdnRequestID.value += ",'" + values[i] + "'";
               
           /* if(hdnRequestID.value == "")
             hdnRequestID.value = values[i] + ",";
            else
             hdnRequestID.value += values[i] + ",";*/   
        }
    }
     
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
    <title>Report</title>    
    <script type="text/javascript" src="inc/functions.js"></script>
    <style type="text/css">   
    LABEL 
    {
        FONT-SIZE: 8pt;
        VERTICAL-ALIGN: top;
        COLOR: black;
        FONT-FAMILY: Arial, Helvetica;
        TEXT-DECORATION: none;
    }
   
    </style>
</head>
<body>
    <form id="frmReport" runat="server">   
    <input type="hidden" id="hdnValue" runat="server" />
    <input type="hidden" id="hdnRequestID" runat="server" />
   
    <table width="100%" border="1" style="border-color:Blue;border-style:solid;" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" style="border-bottom-color:Blue;" bgcolor="#666699" align="center" height="10px" >
                <h3 style="color:White"> 
                      <asp:Label ID="lblHeading" runat="server">Event Manager</asp:Label>
                </h3>                
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom-color:Blue;">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" ></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <table width="100%" border="0" style="left: 0;" id="tbleBtns">
                                <tr>
                                    <td width="3%" valign="top" class="blackblodtext" align="right">Events</td>
                                    <td width="3%" class="blackblodtext" valign="middle">
                                         <dx:ASPxCheckBox ID="chkNew" runat="server" Text="New" TextWrap="False" ClientInstanceName="chkNew">                                            
                                        </dx:ASPxCheckBox>  
                                    </td>
                                    <td width="4%" class="blackblodtext" valign="middle">
                                        <dx:ASPxCheckBox ID="chkInProgress" runat="server" Text="InProgress" TextWrap="False" ClientInstanceName="chkInProgress">   
                                        </dx:ASPxCheckBox> 
                                    </td>
                                    <td width="4%" class="blackblodtext" valign="middle">
                                         <dx:ASPxCheckBox ID="chkCompleted" runat="server" Text="Completed" TextWrap="False" ClientInstanceName="chkCompleted">                                            
                                        </dx:ASPxCheckBox>  
                                    </td>
                                    <td width="3%" class="blackblodtext" valign="middle">
                                        <dx:ASPxCheckBox ID="chkError" runat="server" Text="Error" TextWrap="False" ClientInstanceName="chkError">   
                                        </dx:ASPxCheckBox> 
                                    </td>
                                    <td width="3%" class="blackblodtext" valign="middle"> <%--FB 2363X--%>
                                        <dx:ASPxCheckBox ID="chkCancel" runat="server" Text="Cancel" TextWrap="False" ClientInstanceName="chkCancel">   
                                        </dx:ASPxCheckBox> 
                                    </td>
                                    <td width="5%" class="blackblodtext" valign="top">From
                                       <dx:ASPxDateEdit ID="startDateedit"  EditFormatString="dd/MM/yyyy hh:mm tt" ClientInstanceName="startDateedit" runat="server" Width="160px">                                        
                                       </dx:ASPxDateEdit>
                                    </td>
                                    <td width="5%" class="blackblodtext" valign="top">To
                                       <dx:ASPxDateEdit ID="endDateedit" ClientInstanceName="endDateedit" runat="server" Width="160px">                                       
                                       </dx:ASPxDateEdit>
                                    </td>	
                                     <td width="20%" class="blackblodtext">&nbsp;
                                       <dx:ASPxButton ID="btnPreview" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                            CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                            Text="Search" Width="82px" OnClick="btnOk_Click">
                                            <ClientSideEvents Click="function(s, e) {document.getElementById('hdnValue').value = '1' } " />
                                        </dx:ASPxButton>                                      
                                    </td>							                                    
                                </tr>
                             </table>
                        </td>
                    </tr>
                     <tr>
                        <td >
                            <table width="100%" border="0" style="left: 0;" id="Table1">
                                <tr>
                                <td width="5%" valign="top" class="blackblodtext" align="right"></td>
                                    <td width="10%" class="blackblodtext" >
                                       <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                            CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                            Text="Retry Selected" Width="122px" OnClick="Retry_Click">
                                        </dx:ASPxButton>                                                                          
                                    </td>	
                                    <td width="10%" valign="top" class="blackblodtext" align="left"> <%--FB 2363X--%>
                                    <dx:ASPxButton ID="ASPxButton3" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                            CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                            Text="Cancel Selected" Width="122px" OnClick="Cancel_Click">
                                        </dx:ASPxButton> 
                                    </td> 
                                    <td width="80%" class="blackblodtext" colspan="6">
                                            <dx:ASPxButton ID="ASPxButton2" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                            CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                            Text="Clear Selected" Width="122px">
                                            <ClientSideEvents Click="function(s, e) {e.processOnServer = fnClear(); } " />
                                        </dx:ASPxButton>                             
                                    </td>							                                    
                                </tr>
                             </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>            
            <td  valign="top" >             
                <table width="100%" align="center" border="0">
                    <tr valign="top" runat="server" id="trDetails">
                        <td align="center">
                            <div id="MainDiv" style="overflow-y:auto;overflow-x:auto; word-break:break-all;HEIGHT: 440px;"  >
                            <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" runat="server" Width="100%" EnableCallBacks="false" AllowSort="true"
                             CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css" OnHtmlRowCreated="MainGrid_HtmlRowCreated"  KeyFieldName="RequestID" 
                             OnCustomButtonInitialize="ASPxGridView1_CustomButtonInitialize">                                
                                <Styles CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"  CssPostfix="PlasticBlue">
                                    <Header ImageSpacing="9px" SortingImageSpacing="9px" ForeColor="White" Font-Size="9pt" HorizontalAlign="Center"></Header>
                                    <Cell Font-Size="9pt"></Cell>                                 
                                </Styles>
                                <Images ImageFolder="../en/App_Themes/Plastic Blue/{0}/">
                                    <CollapsedButton Height="10px" Url="../en/App_Themes/Plastic Blue/GridView/gvCollapsedButton.png" Width="9px" />
                                    <ExpandedButton Height="9px" Url="../en/App_Themes/Plastic Blue/GridView/gvExpandedButton.png" Width="9px" />
                                    <HeaderFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png" Width="11px" />
                                    <HeaderActiveFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilterActive.png" Width="11px" />
                                    <HeaderSortDown Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortDown.png" Width="11px" />
                                    <HeaderSortUp Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortUp.png" Width="11px" />
                                    <FilterRowButton Height="13px" Width="13px" />
                                    <CustomizationWindowClose Height="14px" Width="14px" />
                                    <PopupEditFormWindowClose Height="14px" Width="14px" />
                                    <FilterBuilderClose Height="14px" Width="14px" />
                                </Images>  
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" ShowTitlePanel="True" 
                                ShowHeaderFilterBlankItems="False" />
                                <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true" Position="Top">
                                    <AllButton Text="All"></AllButton>
                                    <NextPageButton Text="Next &gt;"></NextPageButton>
                                    <PrevPageButton Text="&lt; Prev"></PrevPageButton>
                                </SettingsPager>                              
                                <ClientSideEvents SelectionChanged="grid_SelectionChanged" />                               
                            </dx:ASPxGridView>
                            </div>
                         </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript">
    var mainDiv = document.getElementById("MainDiv");
    if(mainDiv)
    {  //Difference 180
    
        if (window.screen.width <= 1024)
            mainDiv.style.width = "845px";
        else
            mainDiv.style.width = "1184px";        
    }
    
         
</script>


<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
