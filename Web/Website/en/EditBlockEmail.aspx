<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.EditBlockEmail" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> <html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit BlockEmai</title>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnEmailContID" name="hdnEmailContID" runat="server" type="hidden" />
    <input id="hdnEmailLangID" name="hdnEmailLangID" runat="server" type="hidden" />
    <input id="hdnEmailLangName" name="hdnEmailLangName" runat="server" type="hidden" />
    <input id="hdnPlaceHolders" name="hdnPlaceHolders" runat="server" type="hidden" />
    <input id="hdnCreateType" name="hdnCreateType" runat="server" type="hidden" />
    <input id="hdnEmailMode" name="hdnEmailMode" runat="server" type="hidden" />
    <input id="hdnuserid" name="hdnuserid" runat="server" type="hidden" />
    <input id="hdnImg" name="hdnImg" runat="server" type="hidden" />
    
    <div>
    <h3>Edit Block Mail</h3>
     <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" Visible="False"></asp:Label><br />
     <table width="80%" border="0" cellpadding="5">
        <tr>
            <td width="20%" valign="top" align="left" class="blackblodtext">
                From
            </td>
            <td align="left" colspan="2" >
                <asp:TextBox ID="txtFrom" runat="server" Width="220px" CssClass="altText"></asp:TextBox>
            </td>
        </tr>
        <tr>           
            <td align="left" class="blackblodtext" valign="top" >
               To
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtTo" runat="server" Width="220px" CssClass="altText"></asp:TextBox>
            </td>
        </tr>        
        <tr>
            <td valign="top" align="left" class="blackblodtext" nowrap>
                Email Subject
            </td>
            <td align="left" width="70%" colspan="2"> 
                <asp:TextBox ID="txtEmailSubject" runat="server" Width="85%" CssClass="altText">
                </asp:TextBox>
            </td>                             
        </tr>       
        <tr>
            <td valign="top" align="left"  class="blackblodtext" nowrap>
                <b>Email Message </b>
            </td>
            <td style="width:85%" align="left" valign='top' colspan="2">
                <dxHE:ASPxHtmlEditor ID="dxHTMLEditor" runat="server" Width="100%">
                </dxHE:ASPxHtmlEditor>
           </td>
        </tr>       
        <tr>
            <td></td>
            <td colspan="2">
                <table width="100%" align="center">
                    <tr>
                        <td align="right">
                            <asp:Button Text="Cancel" runat="server" ID="btnCancel" CssClass="altMedium0BlueButtonFormat" OnClick="RedirectToTargetPage"/>
                        </td>
                        <td width="2%"></td>
                        <td>
                             <asp:Button Text="Submit" runat="server" ID="btnSubmit" CssClass="altMedium0BlueButtonFormat" OnClick="SetBlockEmail" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
<script type="text/javascript">
function changeFocus()
{
  // FB 2050
  var obj2 = document.getElementById("txtFrom"); // For EditBlockEmail Page
  if(obj2 != null)
  obj2.focus();
}
window.onload = changeFocus;
</script>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
 <script>
 if (document.getElementById('dxHTMLEditor_TD_T0_DXI15_I')) //To hide ImageButton in Control
     document.getElementById('dxHTMLEditor_TD_T0_DXI15_I').style.display = 'none'
 </script>
