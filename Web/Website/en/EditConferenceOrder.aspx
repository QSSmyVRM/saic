<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_EditConferenceOrder.EditConferenceOrder" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript">
  var servertoday = new Date();
  
   var dFormat;
    dFormat = "<%=format %>";
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="extract.js"></script>
<script type="text/javascript" src="script/mytree.js"></script>
<%--FB 1861--%>
  <%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

<script type="text/javascript" src="script/mousepos.js"></script>

<script runat="server">

</script>
<script language="javascript">

    function UpdateCheckbox(obj)
    {
        if (obj.value == "")
        {
           document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = false;
           UpdatePrice(obj);
        }
        else
        {
            if (!isNaN(obj.value) && obj.value.indexOf(".") < 0)
            {
                document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = true;
                UpdatePrice(obj);
            }
            else
            {
                alert("Invalid Value.");
                obj.value = "";
            }
        }
    }
    
function UpdatePrice(obj)
{
    var lblPrice = document.getElementById(obj.id.substring(0, obj.id.indexOf("_dgCateringMenus")) + "_lblPrice");
    var price = parseFloat("0.00");
    var i = 2;
    
    while(document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity"))
    {
        if (i<10)
        {
            price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity").value;
        }
        else
        {
            price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtQuantity").value;
        }
        i++;
    }
    // WO Bug Fix
        lblPrice.innerHTML = ((price * 100.00) / 100.00).toFixed(2);
         //FB 1830
        if("<%=currencyFormat%>" == "�")
            lblPrice.innerHTML = (((price * 100.00) / 100.00).toFixed(2)).replace(","," ").replace(".",",");        
            
}

function ShowItems(obj)
{
    getMouseXY();
    var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
    str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY>");
    str = str.replace("</TBODY>", "</TBODY></TABLE>");
    document.getElementById("tblMenuItems").style.position = 'absolute';
    document.getElementById("tblMenuItems").style.left = mousedownX - 200;
    document.getElementById("tblMenuItems").style.top = mousedownY;
    document.getElementById("tblMenuItems").style.border = "1";
    document.getElementById("tblMenuItems").style.display="";
    document.getElementById("tblMenuItems").innerHTML = str;
}
function HideItems()
{
   document.getElementById("tblMenuItems").style.display="none";
}

function ShowImage(obj)
{
    //alert(obj.src);
    document.getElementById("myPic").src = obj.src;
    //getMouseXY();
    //alert(document.body.scrollHeight);
    document.getElementById("divPic").style.position = 'absolute';
    document.getElementById("divPic").style.left = mousedownX + 20;
    document.getElementById("divPic").style.top = mousedownY;
    document.getElementById("divPic").style.display="";
    //alert(obj.style.height + " : " + obj.style.width);
}

function HideImage()
{
    document.getElementById("divPic").style.display="none";
}

function viewLayout()
{
    var obj = document.getElementById("<%=lstRoomLayout.ClientID%>");
    if (obj.disabled == "disabled")
        alert("No Room Layouts defined for the selected room.");
    else
        if (obj.selectedIndex > 0)
        {
            url = "image/room/" + obj[obj.selectedIndex].text;
            window.open(url, "RoomLayout", "width=750,height=400,resizable=yes,scrollbars=yes,status=no");
        }
        else
            alert("Please select a Room Layout from the list");
        
}

function getYourOwnEmailList (i)
{
//	url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmSubmit&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";//Login Management
	url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmSubmit&n=" + i ; //Login Management
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
	        winrtc.focus();
		}
}

function btnSubmit_Click()
{
    document.frmSubmit.action = "EditConferenceOrder.aspx?cmd=4&id=" + "<%=Request.QueryString["id"] %>" + "&woid=" + "<%=Request.QueryString["woid"] %>";
    document.frmSubmit.submit();
}

function checkList(obj)
{
}

function listOrders(id)
{
/*    alert("itemList" + rownum + "'");
    var temp, temp1;
    temp = document.getElementById("itemList" + rownum);
    temp1 = document.getElementById("viewItems" + rownum);
    if (temp1.src.indexOf("plus") >0)
        temp1.src = "image/loc/nolines_minus.gif";
    else
        temp1.src = "image/loc/nolines_plus.gif";
    if (temp.style.display == "block")
        temp.style.display = "none";
    else
        temp.style.display = "block";
  */
    document.frmSubmit.action = "ConferenceOrders.aspx?cmd=2&id=" + id;
    document.frmSubmit.submit();
    return true;        
}

function CancelMainForm()
{
    document.frmSubmit.action = "ConferenceOrders.aspx?t=" + document.getElementById("<%=txtType.ClientID %>").value;
    document.frmSubmit.submit();
}

function DeleteWorkOrder(cid, woid)
{
    //FB 2011
    var isRemove = confirm("Are you sure you want to delete this work order?")

    if (isRemove == true)
    {
        var temp = document.getElementById("detailsRow");
        if (typeof(temp) != "undefined")
            temp.style.display = "none";
        document.frmSubmit.action = "EditConferenceOrder.aspx?cmd=1&id=" + cid + "&woid=" + woid;
        document.frmSubmit.submit();
    }
    else
        return false;
}

function EditWorkOrder(confid, woid)
{
    document.getElementById("cmd").value = "2";
    document.frmSubmit.action = "EditConferenceOrder.aspx?cmd=2&id=" + confid + "&woid=" + woid;
    document.frmSubmit.submit();
    return true;
}
function ViewWorkOrderDetails(confid, woid)
{
    document.getElementById("cmd").value = "3";
    document.frmSubmit.action = "EditConferenceOrder.aspx?cmd=3&id=" + confid + "&woid=" + woid;
    document.frmSubmit.submit();
    return true;
}
function DeleteItem(rownum)
{
    if (confirm("Are you sure you want to delete this item from AV Set?") )
    {
        //alert(document.getElementById("selItems").value + " : " + document.getElementById("itemName" + rownum).value + ":" + document.getElementById("itemImg" + rownum).value + ",");
        document.getElementById("selItems").value = document.getElementById("selItems").value.replace(document.getElementById("itemName" + rownum).value + ":" + document.getElementById("itemImg" + rownum).value + ",", "");
        document.getElementById("itemName" + rownum).value = "Deleted";
        document.getElementById("itemQuantity" + rownum).value = "2";
        document.getElementById("itemRow" + rownum).style.display = "none";
        document.getElementById("itemDeleted" + rownum).checked = "true";
    }
}

function viewconf(cid)
{
	url = "dispatcher/conferencedispatcher.asp?cmd=ViewConference&cid=" + cid;
	confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
	confdetail.focus();
}

function HideWorkOrderDetails()
{
    var temp = document.getElementById("detailsRow");
    var temp1 = document.getElementById("hideItem");
    if (temp1.src.indexOf("plus") >0)
    {
        temp1.src = "image/loc/nolines_minus.gif";
        temp.style.display = "block";
    }
    else 
    {
        temp1.src = "image/loc/nolines_plus.gif";
        temp.style.display = "none";
    }
}
function convertControls(id)
{
    var t = document.getElementById("Item" + id);
    t.style.display="none";
    t = document.getElementById("hdnItemList" + id);
    t.style.display="";
}

//FB1830
function ClientValidate(source, arguments)
{     
    var cFor = '<%=currencyFormat%>';
    var strValidation = "";

    if(cFor == "�")
        strValidation = /^\d+$|^\d+\,\d{1,2}$/ ;
    else
        strValidation = /^\d+$|^\d+\.\d{1,2}$/ ;

    if (!arguments.Value.match(strValidation ) ) 
        arguments.IsValid = false       
}
</script>
<!-- Removed inc/maintopNET.aspx INCLUDE file for Window Dressing--> 
<html>
<body>

    <form id="frmSubmit" runat="server" method="post" onsubmit="return true">
          <input type="hidden" id="helpPage" value="40">
          <input type="hidden" id="CreateBy" value="1" />
          <input type="hidden" id="hdnDuration" runat="server" /> <%--FB 2011--%>
    <div>
        <table width="100%">
            <tr>
                <td align="center" colspan="3">
                    <h3>
                        <asp:Label ID="lblType" runat="server" Text="">
                        </asp:Label> Conference Work Orders
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <%--Window Dressing--%>
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError" Visible="False"></asp:Label>
                    <div id="dataLoadingDIV" style="z-index:1"></div>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="3">
                    <table border="0" width="89%" runat="server" id="tblConferenceDetails">
                        <tr>
                    <%--Window Dressing--%>
                            <td align="left" width="10%" class="blackblodtext" style="font-weight:bold" valign="top"><%--FB 2508--%>
                              Title</td>
                             <td style="width:1px"  valign="top"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="5" valign="top"><%--FB 2508--%>
                                <asp:Label ID="lblConfName" runat="server" CssClass="subtitleblueblodtext"></asp:Label>
                                <asp:Label ID="lblConfID" runat="server" Visible="False" ></asp:Label>&nbsp;</td>
                        </tr>
                        <tr>
                    <%--Window Dressing--%>
                            <td align="left"  class="blackblodtext" style="font-weight:bold">
                                Host</td>
                                <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 19px"><%--FB 2508--%>
                                <asp:Label ID="lblConfHost" runat="server" Font-Bold="False" ></asp:Label></td>
                    <%--Window Dressing--%>
                    <%--Commented for FB 2181--%>
                            <%--<td align="left" class="blackblodtext" style="font-weight:bold">
                                Password:</td>
                            <td align="left" colspan="2" style="height: 21px" width="200">
                                <asp:Label ID="lblPassword" runat="server" Font-Names="Verdana" Font-Size="Small" ></asp:Label>&nbsp;</td>--%>
                        </tr>
                        <tr>
                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" style="font-weight:bold">
                                Date</td>
                                <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left">
                                <asp:Label ID="lblConfDate" runat="server" Font-Bold="False" ></asp:Label>,
                                <asp:Label ID="lblConfTime" runat="server" Font-Bold="False" ></asp:Label>
                                <asp:Label ID="lblConfTimezone" runat="server" Font-Bold="False" ></asp:Label></td>
                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" style="font-weight:bold">
                                Duration</td>
                                <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="2" style="height: 21px" valign="top" width="200">
                                <%--Altered for Window Dressing--%>                                
                                <asp:Label ID="lblConfDuration" runat="server" Font-Bold="False" ></asp:Label></td>
                        </tr>
                        <tr>
                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" style="font-weight:bold">
                                Public</td>
                                <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="4" class="altblackblodttext">
                                <asp:Label ID="lblPublic" runat="server" Font-Bold="False" ></asp:Label>
                                <asp:Label ID="lblRegistration" runat="server" Font-Bold="False" ></asp:Label></td>
                        </tr>
                        <tr>
                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" style="font-weight:bold">
                                Files</td>
                                <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="5" class="altblackblodttext">
                                <asp:Label ID="lblFiles" runat="server" Font-Bold="False" ></asp:Label></td>
                        </tr>
                        <tr>
                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" style="font-weight:bold" valign="top"><%--FB 2508--%>
                                Description</td>
                               <td style="width:1px" valign="top"><b>:</b>&nbsp;</td><%--FB 2508--%>
                            <td align="left" colspan="5" class="altblackblodttext" valign="top">
                                <asp:Label ID="lblDescription" runat="server" Font-Bold="False" ></asp:Label>&nbsp;</td>
                        </tr>
                        <tr>
                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" valign="top" style="font-weight:bold">
                                Locations</td>
                               <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="5" style="font-weight: bold" valign="top">
                                <asp:Label ID="lblLocation" runat="server" Font-Bold="False" ></asp:Label></td>
                        </tr>
                        </table>
                         <%--Window Dressing                        
                        </td>
                        </tr>
                    </table>--%>
                    <asp:TextBox ID="cmd" runat="server" BackColor="White" BorderColor="White" BorderStyle="None" ForeColor="white" Width="00px">0</asp:TextBox>
                    <%--Window Dressing--%>
                    <asp:TextBox ID="txtType" runat="server"  BorderStyle="None" Width="0px" ></asp:TextBox>
                 </td>
            </tr>
            <tr>
                    <%--Window Dressing--%>
                <td align="left" colspan="3" class="blackblodtext">
                    <asp:Label ID="lblInstructions" runat="server" Font-Bold="True" Text="To create a work order, click on 'Add New Work Order' button."></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:DataGrid ID="AVMainGrid" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" GridLines="None" OnItemCreated="BindRowsDeleteMessage" OnCancelCommand="AVMainGrid_Cancel"
                        OnDeleteCommand="AVMainGrid_DeleteCommand" OnEditCommand="AVMainGrid_Edit" OnUpdateCommand="AVMainGrid_Update"
                        Width="90%" Visible="true" >
                        <%-- Window Dressing start--%>
                        <FooterStyle CssClass="tableBody" Font-Bold="True" />
                        <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody"/>
                        <ItemStyle CssClass="tableBody"/>
                        <HeaderStyle CssClass="tableHeader" />
                        <%-- Window Dressing end--%>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Name" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AssignedToID" Visible="False" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AssignedToName" HeaderText="Assigned To" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="StartByDate" HeaderText="Start<br>Date" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="StartByTime" HeaderText="Start<br>Time" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CompletedByDate" HeaderText="Complete<br>Date" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CompletedByTime" HeaderText="Complete<br>Time" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" HeaderText="Status" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomID" Visible="False" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomName" HeaderText="Room" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Comments" HeaderText="Comments" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomLayout" Visible="False" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ServiceCharge" HeaderText="Service<br>Charge(USD)" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DeliveryType" HeaderText="Delivery<br>Type" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DeliveryCost" HeaderText="Delivery<br>Cost(USD)" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEdit" CommandName="Edit" runat="server" Text="Edit" Visible='<%# DataBinder.Eval(Container, "DataItem.Status") == "Pending" %>' ></asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" CommandName="Delete" runat="server" Text="Delete" ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
<%--                            <asp:EditCommandColumn CancelText="Cancel" EditText="Edit" HeaderText="Action" UpdateText="Update" HeaderStyle-CssClass="tableHeader">
                            </asp:EditCommandColumn>
                            <asp:TemplateColumn
                            <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
--%>                            <asp:BoundColumn DataField="SetID" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="CATMainGrid" runat="server" AutoGenerateColumns="False" CellPadding="4" OnItemDataBound="SetCalendar" OnItemCreated="BindRowsDeleteMessage" GridLines="None" 
                     OnCancelCommand="CancelChanges" OnUpdateCommand="UpdateWorkorder" OnEditCommand="EditWorkorder" OnDeleteCommand="DeleteWorkorder" Width="100%">
                        <%--Window Dressing Start--%>
                        <FooterStyle Font-Bold="True" CssClass="tableBody"/>
                        <AlternatingItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                        <ItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <EditItemStyle CssClass="tableBody"/>
                        <%--Window Dressing End--%>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="SelectedService" Visible="false"></asp:BoundColumn>
                        <%--Window Dressing--%>
                            <asp:TemplateColumn HeaderText="Room" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblRoomName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RoomName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label runat="server" ID="lblID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                    <asp:DropDownList ID="lstRooms" CssClass="altText" runat="server" selectedValue='<%# DataBinder.Eval(Container, "DataItem.RoomId") %>' OnLoad="LoadRooms" OnInit="LoadRooms" AutoPostBack="true" onchange="javascript:DataLoading(1)" OnSelectedIndexChanged="UpdateMenus" ></asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        <%--Window Dressing--%>
                            <asp:TemplateColumn HeaderText="Service Type" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                <ItemTemplate>
                                    <asp:Label ID="lblServiceName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="lstServices" CssClass="altText" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadCateringServices" OnLoad="LoadCateringServices" AutoPostBack="true" onchange="javascript:DataLoading(1)" OnSelectedIndexChanged="UpdateMenus" selectedValue='<%# DataBinder.Eval(Container, "DataItem.SelectedService") %>'></asp:DropDownList>  <%--OnLoad="GetServices"--%>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        <%--Window Dressing--%>
                            <asp:TemplateColumn HeaderText="Price(USD)" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"> 
                                <ItemTemplate>
                                    <asp:Label ID="lblPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate> 
                                    <asp:Label ID="lblPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>'></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        <%--Window Dressing--%>
                            <asp:TemplateColumn HeaderText="Comments" HeaderStyle-CssClass="tableHeader" ItemStyle-Wrap="true" ItemStyle-Width="20%" ItemStyle-CssClass="tableBody"> 
                                <ItemTemplate>
                                    <asp:Label ID="lblComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate> 
                                    <asp:TextBox ID="txtComments" MaxLength="2000" CssClass="altText" TextMode="multiLine" Rows="2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        <%--Window Dressing--%>
                            <asp:TemplateColumn HeaderText="Delivery By Date/Time" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeliverByDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DeliverByDate") + " " + DataBinder.Eval(Container, "DataItem.DeliverbyTime") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDeliverByDate" Width="80" runat="server" CssClass="altText" Text='<% # DataBinder.Eval(Container, "DataItem.DeliverByDate") %>'></asp:TextBox>
                                    <asp:Image ID="imgCalendar" runat="server" Height="20" Width="20" ImageUrl="image/calendar.gif" />
                                    <mbcbb:ComboBox ID="lstDeliverByTime" runat="server" BackColor="White" CausesValidation="True"
                                    CssClass="altText" Rows="10" style="width:auto" Text='<%# DataBinder.Eval(Container, "DataItem.DeliverByTime") %>'>
                                </mbcbb:ComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="lstDeliverByTime"
                                        Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="lstDeliverByTime"
                                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtDeliverByDate"
                                        Display="Dynamic" ErrorMessage="Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <%--Code changed by offshore for FB Issue 1073 -- start
                                    <asp:RegularExpressionValidator ID="regDeliverByDate" runat="server" ControlToValidate="txtDeliverByDate" Display="Dynamic" ErrorMessage="Invalid Date (mm/dd/yyyy)" ValidationExpression="\b(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2}\b"></asp:RegularExpressionValidator>-->--%>
                                    <asp:RegularExpressionValidator ID="regDeliverByDate" runat="server" ControlToValidate="txtDeliverByDate" Display="Dynamic" ErrorMessage="Invalid Date <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                    <%--//Code changed by Offshore for FB Issue 1073 -- End--%>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <table width="100%" cellpadding="0" cellpadding="4" border="0">
                                        <tr>
                                             <%--Window Dressing start--%>
                                            <td width="60%" class="tableHeader">Name</td>
                                            <td width="40%" class="tableHeader">Quantity</td>
                                             <%--Window Dressing end--%>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:DataGrid ID="dgCateringMenus" ShowHeader="false" Width="100%" CellPadding="4" CellSpacing="0" BorderWidth="1px" BorderStyle="Solid" runat="server" AutoGenerateColumns="false">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="top" />
                                        <AlternatingItemStyle HorizontalAlign="left" VerticalAlign="Top" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Name" ItemStyle-Width="60%" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Quantity" ItemStyle-Width="40%" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <asp:Label ID="lblCateringMenus" visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.strMenus") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DataGrid ID="dgCateringMenus" ShowHeader="false" Width="100%" CellPadding="4" CellSpacing="0" BorderWidth="0px" BorderStyle="none" runat="server" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Price" Visible="false"></asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtPrice" runat="server" style="display:none" Text='<%# Double.Parse(DataBinder.Eval(Container, "DataItem.Price").ToString()).ToString("0.00") %>'></asp:TextBox>
                                                    <asp:CheckBox ID="chkSelectedMenu" runat="server" />
                                                    <a href="#"><asp:Label ID="txtMenuName" onmouseover="javascript:ShowItems(this)" onmouseout="javascript:HideItems()" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' runat="server"></asp:Label></a>
                                                    <asp:DataGrid ID="dgMenuItems" AutoGenerateColumns="false" runat="server" >
                                                        <Columns>
                                                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Name" HeaderText="Items List" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtQuantity" onkeyup="javascript:UpdateCheckbox(this)" Width="50px" CssClass="altText" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <asp:Label ID="lblNoMenus" runat="server" Visible="false" Text="No menus found." CssClass="lblError" ></asp:Label>
                                    <asp:Label ID="lblCateringMenus" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.strMenus") %>'></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <%--Window Dressing--%>
                            <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" CommandArgument="2"></asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CommandName="Delete" CommandArgument="2"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update" CommandArgument="2"></asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" CommandArgument="2"></asp:LinkButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                                    <asp:DropDownList ID="lstServices" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadCateringServices" Visible="false"></asp:DropDownList>  

                    <asp:Label ID="lblNoWorkrders" Text="No Workorders found" Visible="false" runat="server" CssClass="lblError"></asp:Label>
                    </td></tr>
            <tr>
                <td colspan="3">
                 
                        <asp:Table ID="AVItemsTable" runat="server" Visible="False" Width="100%" CellSpacing="5" CellPadding="5" >
                        <asp:TableRow ID="TableRow2" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:TableCell runat="server">
                 <table border="0" cellpadding="5" cellspacing="0" width="80%">
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" width="15%" class="blackblodtext">Room<span class="reqfldstarText">*</span></td>
                            <td align="left" width="25%">
                                <asp:DropDownList ID="lstRooms" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selRooms_SelectedIndexChanged">
                                </asp:DropDownList></td>
                            <%--Window Dressing--%>
                            <td align="left" width="15%" class="blackblodtext">Set Name<span class="reqfldstarText">*</span></td>
                            <td align="left" width="25%">
                                <asp:DropDownList ID="lstAVSet" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selAVSet_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="req2" ControlToValidate="lstAVSet" ErrorMessage="Required" runat="server" InitialValue="-1" ValidationGroup="Submit" Display="dynamic"></asp:RequiredFieldValidator><%--FB 1951--%>
                                <%--<asp:RequiredFieldValidator ID="req2" ControlToValidate="lstAVSet" ErrorMessage="Required" runat="server" InitialValue="-1"></asp:RequiredFieldValidator>--%>
                                </td>
                        </tr>
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">Name<span class="reqfldstarText">*</span></td>
                            <td align="left">
                            
                                <asp:TextBox ID="txtWorkOrderName" runat="server" CssClass="altText"></asp:TextBox><asp:TextBox ID="txtWorkOrderID" runat="server" CssClass="altText" Visible="False"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ValidateWOName" runat="server" ControlToValidate="txtWorkOrderName" Display="Dynamic" ErrorMessage="Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regWOName" ControlToValidate="txtWorkOrderName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                            </td>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">Person-in-charge<span class="reqfldstarText">*</span></td>
                            <td align="left">
                            
                                <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                &nbsp;<img id="ImageButton1" onclick="javascript:getYourOwnEmailList(0)" src="image/edit.gif"  style="cursor:pointer;" title="myVRM Address Book"/> <%--FB 2798--%> <asp:TextBox
                                    ID="hdnApprover1" runat="server" BackColor="Transparent" BorderColor="White"
                                    BorderStyle="None" Width="0px" ForeColor="Transparent"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqFieldApproverAV" runat="server" ControlToValidate="txtApprover1"
                                    Display="Dynamic" ErrorMessage="Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">
                                Start by Date<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                                            <asp:TextBox ID="txtStartByDate" runat="server" CssClass="altText"></asp:TextBox>
                                <%--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="cal_trigger1" height="20px" onclick="javascript:showCalendar('<%=txtStartByDate.ClientID %>', 'cal_trigger1', 1, '%m/%d/%Y');"--%>
                                <img id="cal_trigger1" height="20px" onclick="javascript:showCalendar('<%=txtStartByDate.ClientID%>', 'cal_trigger1', 1, '<%=format%>');"
                                    src="image/calendar.gif" width="20px" />
                                <%--//Code changed by Offshore for FB Issue 1073 -- End--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStartByDate"
                                    Display="Dynamic" ErrorMessage="Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">Start by Time<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                 <mbcbb:ComboBox ID="startByTime" runat="server" CausesValidation="True"
                                    CssClass="altText" Rows="10" Style="width:auto"><%--Edited for FF--%>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="reqStartByTime" runat="server" ControlToValidate="startByTime"
                                    Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator> <%--WO Bug Fix--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="startByTime"
                                    Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator>    <%--PSU Fix--%>                            
                            </td>
                        </tr>
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"> Completed by Date<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                                            <asp:TextBox ID="txtCompletedBy" runat="server" CssClass="altText"></asp:TextBox>
                                <%--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="cal_trigger" height="20px" onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '%m/%d/%Y');"--%>
                                <img id="cal_trigger" height="20px" onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID%>', 'cal_trigger', 1, '<%=format%>');"
                                    src="image/calendar.gif" width="20px" />
                                <%--<!--//Code changed by Offshore for FB Issue 1073 -- End-->        --%>   
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompletedBy"
                                    Display="Dynamic" ErrorMessage="Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">Completed by Time<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                 <mbcbb:ComboBox ID="completedByTime" runat="server" CausesValidation="True"
                                    CssClass="altText" Rows="10" Style="width:auto"><%--Edited for FF--%>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="reqTime" runat="server" ControlToValidate="completedByTime"
                                    Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regTime" runat="server" ControlToValidate="completedByTime"
                                    Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%--PSU Fix--%>
                            </td>
                        </tr>
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">Notify Host</td>
                            <td style="font-weight: bold; font-size: small; color: #0000ff; font-family: Verdana; height: 18px;" align="left">
                                <asp:CheckBox ID="chkNotify" runat="server" /></td>
                            <%--Window Dressing--%>
                            <td align="left" id ="lblTZ" runat="server" class="blackblodtext">Time Zone</td> <%--FB 2588--%>
                            <td  align="left">
                                <asp:DropDownList ID="lstTimezones" CssClass="altText" OnInit="GetTimezones" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                                </td>
                        </tr>
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">Status</td>
                            <td style="font-weight: bold; font-size: small; color: #0000ff; font-family: Verdana; height: 18px;" align="left">
                                <asp:DropDownList CssClass="altText" ID="lstStatus" runat="server">
                                    <asp:ListItem Text="Pending" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Completed" Value="1"></asp:ListItem>
                                </asp:DropDownList></td>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">Comments</td>
                            <td style="font-weight: bold; font-size: small; color: #0000ff; font-family: Verdana; height: 18px;" align="left">
                                <asp:TextBox ID="txtComments" runat="server" CssClass="altText" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                        </tr>
                            <%--Window Dressing--%><%--UI changes for FB 1830 --%>
                        <tr id="trDelivery" runat="server" >
                            <td align="left" class="blackblodtext">Delivery Type<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altText" ID="lstDeliveryType" AutoPostBack="true" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadDeliveryTypes" OnSelectedIndexChanged="ChangeDeliveryType"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="lstDeliveryType" runat="server" ValidationGroup="Submit" ErrorMessage="Required" InitialValue="-1" Display="dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">
                                Delivery Cost (<%=currencyFormat %>)</td> <%--FB 1830--%>
                            <td  align="left">
                                <asp:TextBox ID="txtDeliveryCost" runat="server" CssClass="altText" ></asp:TextBox>
                                <asp:CustomValidator ID="cusDCost" runat="server" ControlToValidate="txtDeliveryCost"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage=" Invalid Amount" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                <%--<asp:RegularExpressionValidator ID="regDC" runat="server" ErrorMessage="Invalid Format." SetFocusOnError="True" ToolTip="Invalid Format." ControlToValidate="txtDeliveryCost" ValidationExpression="^\d+(?:\.\d{0,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                               </td>
                        </tr>
                        <tr id="trService" runat="server">
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext">
                                Service Charge (<%=currencyFormat %>)</td> <%--FB 1830--%>
                            <td  align="left">
                                <asp:TextBox ID="txtServiceCharges" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:CustomValidator ID="cusSvcCharge" runat="server" ControlToValidate="txtServiceCharges"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage=" Invalid Amount" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                <%--<asp:RegularExpressionValidator ID="regSC" runat="server" ErrorMessage="Invalid Format." SetFocusOnError="True" ToolTip="Invalid Format." ControlToValidate="txtServiceCharges" ValidationExpression="^\d+(?:\.\d{0,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                            </td>
                            <td align="left">
                                &nbsp;</td>
                            <td style="font-weight: bold; font-size: small; color: #0000ff; font-family: Verdana; height: 18px;" align="left">
                                </td>
                        </tr>
                    </table>
                    </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                     <asp:TableCell ID="tblRoomLayout" runat="server">
                       <table border="0" cellpadding="1" cellspacing="0" width="80%">
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" width="17%" class="blackblodtext">
                                    Room Layout</td>
                                 <td align="left" width="25%">
                                   <asp:DropDownList ID="lstRoomLayout" runat="server" CssClass="altSelectFormat"></asp:DropDownList>
                                   <input type="Button" ID="btnViewRoomLayout" Class="altShortBlueButtonFormat" value="View" onClick="javascript:viewLayout()" />
                                </td>
                                <td width="25%">
                                    &nbsp;</td>
                            <td align="left" width="25%">
                               &nbsp;</td>
                           </tr>
                        </table>
                       </asp:TableCell></asp:TableRow>
                       <asp:TableRow HorizontalAlign="Center" VerticalAlign="Middle" runat="server"><asp:TableCell runat="server">
                       <table width="90%">
                      <tr>
                          <td colspan="2" align="center">
                                <asp:DataGrid ID="itemsGrid" runat="server" AutoGenerateColumns="False" ShowFooter="true"
                                    CellPadding="4" Font-Bold="False" Font-Names="Verdana" Font-Size="Small" OnItemDataBound="SetDeliveryAttributes"
                                    GridLines="None" Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="1">
                                      <%--Window Dressing Start --%>
                                    <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                     <%--Window Dressing End --%>
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="False" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SerialNumber" HeaderText="Serial #" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Image">
                                            <HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate >
                                               <asp:Image runat="server" ID="imgItem" Height="30" Width="30" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                               <%--<cc1:ImageControl id="imgItem" Width="30" Height="30" Visible="false" Runat="server"></cc1:ImageControl>--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:BoundColumn DataField="Comments" HeaderText="Comments" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Description" HeaderText="Description" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Quantity" HeaderText="Quantity<br>in Hand" ItemStyle-CssClass="tableBody" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Delivery<br>Type<span class='reqfldstarText'>*</span>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
<%--                                                <asp:Label ID="lblDeliveryType" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryType") %>' runat="server"></asp:Label>--%>
                                                  <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ChangeDeliveryTypeItem" runat="server" ID="lstDeliveryTypeItem" Enabled="false" DataTextField="Name" DataValueField="ID" OnInit="LoadDeliveryTypes" SelectedText='<%# DataBinder.Eval(Container, "DataItem.DeliveryType") %>' CssClass="altText"></asp:DropDownList><%----%>
                                             </ItemTemplate>
                                         </asp:TemplateColumn>
                                         <asp:TemplateColumn HeaderText="Delivery<br>Cost (USD)" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDeliveryCost" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>' runat="server" CssClass="tableBody"/>
                                                <asp:DropDownList ID="lstDeliveryCost" runat="server" Visible="false" Enabled="true" DataValueField="DeliveryTypeID" DataTextField="DeliveryCost" /> <%--SelecteValue='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>'--%>
                                            </ItemTemplate>
                                         </asp:TemplateColumn>
                                         <asp:TemplateColumn HeaderText="Service<br>Charge (USD)" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:Label ID="lblServiceCharge" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>' runat="server" CssClass="tableBody"/>
                                                <asp:DropDownList AutoPostBack="true" runat="server" Visible="false" ID="lstServiceCharge" Enabled="true" DataTextField="ServiceCharge" DataValueField="DeliveryTypeID"></asp:DropDownList><%--SelecteValue='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>'--%>
                                            </ItemTemplate>
                                         </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Price (USD)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPrice" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>' runat="server" CssClass="tableBody"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Requested<br>Quantity<span class='reqfldstarText'>*</span>"><HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReqQuantity" CssClass="altText" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="numPassword1" runat="server" ValidationGroup="Submit" ErrorMessage="Numerics only." SetFocusOnError="True" ToolTip="Numerics only." ControlToValidate="txtReqQuantity" ValidationExpression="\d+" Display="Dynamic"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Submit" ControlToValidate="txtReqQuantity" ErrorMessage="Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="validateQuantityRange" runat="server" ValidationGroup="Submit" ErrorMessage="Invalid Quantity" ControlToValidate="txtReqQuantity" MinimumValue="0" Display="Dynamic" Type="Integer" MaximumValue='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'></asp:RangeValidator>
                                            </ItemTemplate>
                                            <FooterStyle Font-Bold="True" ForeColor="Blue" Horizontalalign="left" />
                                            <FooterTemplate>
                                               <span class="subtitleblueblodtext"> Total(<%=currencyFormat%>): </span><asp:Label runat="server" ID="lblTotalQuantity" Text="0" Width="50" OnLoad="UpdateTotal"></asp:Label>&nbsp;
                                                <asp:Button ID="btnUpdateTotal" OnClick="UpdateTotal" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Update" ValidationGroup="Submit" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="UID" Visible="false" HeaderText="UID"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                </asp:DataGrid>
                          </td>
                      </tr>
 
                      <tr>
                        <%--Window Dressing--%>
                          <td align="right">
                             <asp:Button ID="btnCancel" runat="server" CssClass="altMedium0BlueButtonFormat" ValidationGroup="Cancel" OnClick="btnCancel_Click" Text="Cancel" /></td>
                        <%--Window Dressing--%>
                          <td align="left" >
                             <asp:Button ID="btnSubmit" runat="server" CssClass="altMedium0BlueButtonFormat" ValidationGroup="Submit" OnClick="btnSubmit_Click" Text="Submit" /></td>
                      </tr>
                    </table>          
                            </asp:TableCell>
                            </asp:TableRow>
                      </asp:Table>
                </td>
            </tr>
            
            <tr>
                <td align="left" colspan="3" width="90%">
                </td>
            </tr>
            <tr>
                <td align="right" colspan="3" width="90%">
                    <asp:Button ID="btnAddNewAV" runat="server" CssClass="altLongBlueButtonFormat" OnClick="A_btnAddNew_Click"
                        Text="Add New Work Order"  />
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">&nbsp;


                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">

                </td>
            </tr>
            <tr>
                <td align="right" colspan="3" style="height: 21px">
                    <asp:Button ID="btnCancelMain" runat="server" CssClass="altLongBlueButtonFormat" OnClick="btnCancelMain_Click"
                        Text="Cancel" /><%--ZD 100170--%>
                    <asp:Button ID="btnSubmitMain" runat="server" CssClass="altLongBlueButtonFormat" ValidationGroup="Submit"  OnClick="btnSubmitMain_Click" Text="Submit" OnClientClick="DataLoading(1)" /></td>
            </tr>
        </table>
    </div>
                    <div id="tblMenuItems" style="display:none">
    
                    </div>
 
<div id="divPic" style="display:none;  z-index:1;">
    <img src="" name="myPic" id="myPic" width="200" height="200"> <%--Edited for FF--%>
</div>
<script language="javascript">
if (queryField("id").toUpperCase() != "PH" && queryField("id").toUpperCase() != "11,1")
    if (document.getElementById("<%=lblConfTime.ClientID %>").innerText.indexOf("#") >= 0)
    {
	        AnalyseRecurStr(document.getElementById("<%=lblConfTime.ClientID %>").innerText);
            st = calStart(atint[1], atint[2], atint[3]);
            et = calEnd(st, parseInt(atint[4], 10));
            document.getElementById("<%=lblConfTime.ClientID %>").innerText = recur_discription(document.getElementById("<%=lblConfTime.ClientID %>").innerText, et, "", Date());
    }
         if (document.getElementById("txtType").value == "1")
            document.getElementById("helpPage").value = "3";
         if (document.getElementById("txtType").value == "2")
            document.getElementById("helpPage").value = "5";
         if (document.getElementById("txtType").value == "3")
            document.getElementById("helpPage").value = "40";
         if (document.getElementById("startByTime") != null)
            document.getElementById("reqStartByTime").controltovalidate = "startByTime_Text";
         if (document.getElementById("completedByTime") != null)
            document.getElementById("reqTime").controltovalidate = "completedByTime_Text";

    //FB 1830
    changeCurrencyFormat("AVMainGrid",'<%=currencyFormat %>'); 
    changeCurrencyFormat("itemsGrid",'<%=currencyFormat %>'); 
    changeCurrencyFormat("CATMainGrid",'<%=currencyFormat %>'); 
        
</script>

    </form>
    <script language="javascript">
    if (typeof(reqTime) != "undefined")
        reqTime.controltovalidate = "completedByTime_Text";
    if (typeof(regTime) != "undefined")
        regTime.controltovalidate = "completedByTime_Text";
    </script>
</body></html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
