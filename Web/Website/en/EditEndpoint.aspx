<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_EditEndpoint.EditEndpoint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create/Edit Endpoint</title>
<script language="javascript">

    function fnTextFocus(xid,par) {
    
      var custId =xid.split("_");
          
    document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtCompare").value = true;
    // ZD 100263 Starts
    var obj1 =  document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword");
    var obj2 =  document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2");
    

    if (par == 1)
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword").value = true;
    else
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword2").value = true; 
         if (obj1.value == "" && obj2.value == "") {
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").style.backgroundImage = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").style.backgroundImage = "";
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
    }
    return false;
    // ZD 100263 Ends 
    if (par == 1) {
    
        if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword2") != null)
        {
            if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword2").value == "true")
            { 
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
                
            }else
            {
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
            }
        }
        else
        {
            document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
            document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";                
        }
    }
       else{
       if (obj1.value == "" && obj2.value == "") {
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").style.backgroundImage = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").style.backgroundImage = "";
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
    }
    return false;
           if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword") != null)
           {
            if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword").value == "true")
            { 
                
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";                
            }
            else{
            document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
                }
           
           }
           else{
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
                
                }
        }
        
         if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass1")!= null)
        {
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass1"), false);
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass1"), true);
        }
         if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass2")!= null)
        {
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass2"), false);
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass2"), true);
        }
}
function ValidateSelection(obj)
{

    var lstBridges = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstBridges"); //FB 2093
    var lstProtocol = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstVideoProtocol");
    var lstConnectionType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstConnectionType");
    var lstMCUAddressType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstMCUAddressType");
    var lstAddressType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstAddressType");
    var reqBridges = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqBridges");
    var regAddress = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_regAddress");
    var reqPrefDial = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqPrefDial");    //FB 1394
    
    if("<%=Session["isAssignedMCU"]%>" == "1" || ("<%=Session["isAssignedMCU"]%>" == "0" && lstBridges.value != "-1")) //FB 2093
        ValidatorEnable(reqPrefDial, true);    //FB 1394
    
    if (obj == lstAddressType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstAddressType.value == 5)
        {
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
            lstProtocol.selectedIndex = 4;
            lstConnectionType.selectedIndex = 3;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstMCUAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
        }
        else if (lstAddressType.value == 4)
        {
            lstProtocol.selectedIndex = 2;
            lstConnectionType.selectedIndex = 2;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstMCUAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstProtocol.value == 4 || lstProtocol.value == 2)
                lstProtocol.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                lstMCUAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstMCUAddressType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstMCUAddressType.value == 5)
        {
            lstProtocol.selectedIndex = 4;
            lstConnectionType.selectedIndex = 3;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
        }
        else if (lstMCUAddressType.value == 4)
        {
            lstProtocol.selectedIndex = 2;
            lstConnectionType.selectedIndex = 2;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstProtocol.value == 4 || lstProtocol.value == 2)
                lstProtocol.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstAddressType.value == 5 || lstAddressType.value == 4)
                lstAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstConnectionType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstConnectionType.value == 3) //MPI-Direct
        {
            lstProtocol.selectedIndex = 4;
            lstAddressType.selectedIndex = 5;
            lstMCUAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
        }
        else //If not MPI-Direct
        {
            if (lstProtocol.value == 4)
                lstProtocol.selectedIndex = 1;
            if (lstAddressType.value == 5)
                lstAddressType.selectedIndex = 1;
            if (lstMCUAddressType.value == 5)
                lstMCUAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstProtocol)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstProtocol.value == 4)
        {
            lstMCUAddressType.selectedIndex = 5;
            lstConnectionType.selectedIndex = 3;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
        }
        else if (lstProtocol.value == 2)
        {
            lstMCUAddressType.selectedIndex = 4;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            lstAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                lstMCUAddressType.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstAddressType.value == 5 || lstAddressType.value == 4)
                lstAddressType.selectedIndex = 1;
        }
    }
}

function EnableDefaults(varName, flag)
{
    var elements = document.getElementsByTagName('input'); 
    var chkDefault = varName + "_rdDefault";
    var chkDelete = varName + "_chkDelete";
    
    if (flag == false)
        document.getElementById("IsMarkedDeleted").value = "1";
    else
        document.getElementById("IsMarkedDeleted").value = "0"; 
   
    for (i=0;i<elements.length;i++)
    {
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("Default") >= 0) ) 
        {
            if(elements.item(i).id != chkDefault) // all rows but the current datagrid row
            { 
                if (flag == false) //if MPI
                    elements.item(i).checked = flag; 
                elements.item(i).disabled = !flag; 
            }
            else // in current row
            {
                if (flag == false) //if MPI
                    elements.item(i).checked= !flag; 
                else
                    elements.item(i).disabled = !flag; 
            }
        }
        if ( (elements.item(i).type == "checkbox") && (elements.item(i).id.indexOf("Delete") >= 0) ) 
        {                
            if(elements.item(i).id != chkDelete) // all rows but the current datagrid row
            {
                elements.item(i).disabled = !flag; 
                elements.item(i).checked = !flag;
            }
            else
            {
                if (flag == false) // if MPI then disable the current row delete check box 
                {
                    elements.item(i).checked= flag; 
                    elements.item(i).disabled = !flag;
                }
                else // if not MPI then enable the delete checkbox
                     elements.item(i).disabled = !flag;
            }
            if (elements.item(i).checked)
                    ValidatorEnable(document.getElementById(varName + "_regAddress"), false);
            else
                    ValidatorEnable(document.getElementById(varName + "_regAddress"), true);
        }
    }
}

function IsMarkedForDeletion()
{
    if (typeof(Page_ClientValidate) == 'function') 
        if (!Page_ClientValidate())
            return false;
    if (document.getElementById("IsMarkedDeleted").value == "1")
    {
        if (confirm("All Profiles other than MPI will be deleted for this endpoint. Are you sure you want to continue?"))
            return true;
        else return false;
    }
    else return true;
}
//ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
		    else
		        document.getElementById("dataLoadingDIV").innerHTML = "";
	}
    //ZD 100176  End                 


function SelectOneDefault(obj)
{
    if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        var chkDefault = obj.id.substring(0,obj.id.indexOf("rdDefault")) + "chkDelete";
        var objDef = document.getElementById(chkDefault);
//        alert(chkDefault);
        if(objDef.checked)
        {
//            obj.checked = false;
            alert("You cannot delete this profile now.");
            objDef.checked = false;
        }
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("Default") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= false; 
            }
        } 
    }
}  
//FB 2595 Start
function fnSecuredDetails(obj)
{
   var custId = obj.id.split("_");
  
   var tdNetworkURL = document.getElementById(custId[0]+"_"+custId[1]+"_"+"lblNetworkURL"); //"tdNetworkURL"
   var tdTxtNetworkURL = document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtNetworkURL");
   var tdSecuredPort = document.getElementById(custId[0]+"_"+custId[1]+"_"+"lblSecuredPort");
   var tdTxtSecuredPort = document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtSecuredport");
   
   if(obj.checked)
   {
        tdNetworkURL.style.visibility = 'visible';
        tdTxtNetworkURL.style.visibility = 'visible';
        tdSecuredPort.style.visibility = 'visible';
        tdTxtSecuredPort.style.visibility = 'visible';
   }
   else
   {
        tdNetworkURL.style.visibility = 'hidden';
        tdTxtNetworkURL.style.visibility = 'hidden';
        tdSecuredPort.style.visibility = 'hidden';
        tdTxtSecuredPort.style.visibility = 'hidden';
   }
}
//FB 2595 End

//ZD 100132 START
function fnOpengatekeeper(obj)
{
   var custId = obj.id.split("_");
  
   var tdGateKeeeperAddress = document.getElementById(custId[0]+"_"+custId[1]+"_"+"LblGateKeeeperAddress");
   var trTxtSecuredPort = document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtGateKeeeperAddress");
   
   if(obj.checked)
   {
        trTxtSecuredPort.style.visibility = 'visible';
        tdGateKeeeperAddress.style.visibility = 'visible';
           }
   else
   {
       trTxtSecuredPort.style.visibility = 'hidden';
       tdGateKeeeperAddress.style.visibility = 'hidden';
          }
}
//ZEN 100132 END

//FB 2044 - Starts
function CheckDefault(obj)
{
    var rdDefault = obj.id.substring(0,obj.id.indexOf("chkDelete")) + "rdDefault";
    var objDef = document.getElementById(rdDefault);
   
    if (obj.checked)
    {
        var objDef1 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqName");
        var objDef2 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regProfileName"); 
        var objDef3 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regnumPassword1"); 
        var objDef4 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "cmpPass1"); 
        var objDef5 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regnumPassword2"); 
        var objDef6 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "cmpPass2"); 
        var objDef7 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regAddress"); 
        var objDef8 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regMPI"); 
        var objDef9 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqAddressType"); 
        var objDef10 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqVideoEquipment"); 
        var objDef11 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqAddress");
        var objDef12 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqLineRate"); 
        var objDef13 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqBridges"); 
        var objDef14 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqPrefDial"); 
        var objDef15 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegMCUAddress");
        var objDef16 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegURL"); 
        var objDef17 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegExchangeID"); 
        var objDef18 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegApiport");
        var objDef19 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regRearSecCamAdd"); 
        
        if(objDef.checked)
        {
            obj.checked = false;
            alert("You cannot delete the default profile.");
        }
        
        ValidatorEnable(objDef1, false);
        objDef1.style.display = 'none';

        ValidatorEnable(objDef2, false);
        objDef2.style.display = 'none';

        ValidatorEnable(objDef3, false);
        objDef3.style.display = 'none';

        ValidatorEnable(objDef4, false);
        objDef4.style.display = 'none';

        ValidatorEnable(objDef5, false);
        objDef5.style.display = 'none';

        ValidatorEnable(objDef6, false);
        objDef6.style.display = 'none';

        ValidatorEnable(objDef7, false);
        objDef7.style.display = 'none';

        ValidatorEnable(objDef8, false);
        objDef8.style.display = 'none';

        ValidatorEnable(objDef9, false);
        objDef9.style.display = 'none';

        ValidatorEnable(objDef10, false);
        objDef10.style.display = 'none';

        ValidatorEnable(objDef11, false);
        objDef11.style.display = 'none';

        ValidatorEnable(objDef12, false);
        objDef12.style.display = 'none';

        ValidatorEnable(objDef13, false);
        objDef13.style.display = 'none';

        ValidatorEnable(objDef14, false);
        objDef14.style.display = 'none';

        ValidatorEnable(objDef15, false);
        objDef15.style.display = 'none';

        ValidatorEnable(objDef16, false);
        objDef16.style.display = 'none';

        ValidatorEnable(objDef17, false);
        objDef17.style.display = 'none';

        ValidatorEnable(objDef18, false);
        objDef18.style.display = 'none';

        ValidatorEnable(objDef19, false);
        objDef19.style.display = 'none';
    }
}
//FB 2044 - End

</script>
</head>
<body>
    <form id="frmSearchConference" runat="server" method="post" onsubmit="DataLoading(1)"><%--ZD 100176--%> 
        <center><table border="0" width="100%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Manage Endpoint"></asp:Label></h3><br />
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                   <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table>
                        <tr>
                            <td>
                                <SPAN class="subtitleblueblodtext">Endpoint Name<span style="color:Red">*</span></SPAN>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" MaxLength="20" Width="500" ></asp:TextBox><%--FB 2523--%><%--FB 2995--%>
                                <asp:RequiredFieldValidator ID="reqEndpointName" SetFocusOnError="true" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Required"  Display="dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                             <%-- Code Added for FB 1640--%>                                                
                                <asp:RegularExpressionValidator ID="regEndpointName" SetFocusOnError="true" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" ValidationGroup="Submit" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                <asp:TextBox CssClass="altText"  ID="txtEndpointID" runat="server" Visible="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                <asp:DataGrid runat="server" OnItemDataBound="InitializeLists" OnItemCreated="InitializeLists" ID="dgProfiles" AutoGenerateColumns="false"
                  CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="100%" style="border-collapse:separate"><%-- Edited for FF--%>
               <%--Window Dressing - Start--%>
                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                <EditItemStyle CssClass="tableBody" />
                <AlternatingItemStyle CssClass="tableBody" />
                <ItemStyle CssClass="tableBody" />
                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                <Columns>
                    <asp:TemplateColumn HeaderText="Profile #" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" />
                        <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:Label ID="lblProfileCount" Text="" runat="server" Font-Bold="true"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Profiles" HeaderStyle-HorizontalAlign="Left">
                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" Height="25" />
                        <ItemTemplate>
                        <table width="100%">
                            <tr>
                                <td align="left" class="tableBody">
                                    Profile Name<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txtProfileID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileID") %>'></asp:TextBox>
                                    <asp:TextBox CssClass="altText"  ID="txtProfileName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileName") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" ControlToValidate="txtProfileName" runat="server" ValidationGroup="Submit" ErrorMessage="Required" Display="Dynamic" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regProfileName" ControlToValidate="txtProfileName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /  ; ? | ^ = ! ` [ ] { } :  $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`\[\]{}\x22;=^:@$%&'~]*$"></asp:RegularExpressionValidator>  <%--FB 2954--%>
                                </td>
                                <td align="left" class="tableBody">
                                    Password</td>
                                <td align="left" >
                                    <asp:TextBox CssClass="altText"  ID="txtProfilePassword"  runat="server" TextMode="Password"  style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onfocus="javascript:fnTextFocus(this.id,1);"></asp:TextBox>
                                    <input type="hidden" runat="server" id="lbltxtProfilePassword" value="false" />
                                    <input type="hidden" runat="server" id="lbltxtCompare" />
                                    <asp:RegularExpressionValidator ID="regnumPassword1" runat="server" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." SetFocusOnError="True" ValidationGroup="Submit" ToolTip="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." ControlToValidate="txtProfilePassword" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$" Display="Dynamic"></asp:RegularExpressionValidator> <%--FB Case 557 Saima--%><%--FB 2319--%> <%--FB 2996--%>
                                    <asp:CompareValidator runat="server" ID="cmpPass1" ControlToCompare="txtProfilePassword2" ControlToValidate="txtProfilePassword" ValidationGroup="Submit" ErrorMessage="<br>Please confirm password" Display="dynamic"></asp:CompareValidator>
                                </td>
                                <td align="left" class="tableBody">
                                    Confirm Password</td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtProfilePassword2" runat="server" TextMode="Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onfocus="javascript:fnTextFocus(this.id,2);" ></asp:TextBox>
                                    <input type="hidden" runat="server" id="lbltxtProfilePassword2" value="false" />
                                    <asp:RegularExpressionValidator ID="regnumPassword2" runat="server" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." ValidationGroup="Submit" SetFocusOnError="True" ToolTip="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : ~ and &#34; are invalid characters." ControlToValidate="txtProfilePassword" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$" Display="Dynamic"></asp:RegularExpressionValidator> <%--FB Case 557 Saima--%><%--FB 2319--%> <%--FB 2996--%>
                                    <asp:CompareValidator runat="server" ID="cmpPass2" ControlToCompare="txtProfilePassword" ControlToValidate="txtProfilePassword2" ValidationGroup="Submit" ErrorMessage="<br>Your passwords do not match" Display="dynamic"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr> <%--FB 2400 start--%>
                                <td align="left" class="tableBody">
                                    TelePresence</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkTelepresence" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.isTelePresence").Equals("1") %>' OnClick="javascript:enableSubAddProfile(this)"/>
                                </td>
                                <td align="left" class="tableBody">
                                    Address <span style="color:Red">*</span>
                                <td align="left" nowrap="nowrap">
                                    <asp:TextBox CssClass="altText"  ID="txtAddress" runat="server"  value='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:TextBox>  <%-- Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>' SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Address") %>'--%>
                                    <%--FB 1972--%>
                                    <asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > % \ / ? | ^ = ` [ ] { } $ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>?|`\[\]{}\=^$%&~]*$"></asp:RegularExpressionValidator>  <%--FB 2267--%> <%--FB 2594--%>
                                    <%--<asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" Enabled="false" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                                    <asp:RegularExpressionValidator ID="regMPI" Enabled="false" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server" 
                                        ErrorMessage="Invalid MPI Address" ValidationExpression="^[0-9A-Za-z]+$"></asp:RegularExpressionValidator>
                                    <asp:Button id="btnProfileAddr" runat="server" Text="Add" class="btndisable" Width="25%" Height="20px"/> <%--FB 2664--%>
                                </td>
                                <td align="left" class="tableBody">
                                    Address Type<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);" ></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqAddressType" runat="server" InitialValue="-1" ControlToValidate="lstAddressType" ValidationGroup="Submit" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                 <td align="left" class="tableBody">
                                    Model<span style="color:Red">*</span></td>
                                <td align="left">
                                   <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.VideoEquipment") %>'></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqVideoEquipment" runat="server" InitialValue="-1" ControlToValidate="lstVideoEquipment" ValidationGroup="Submit" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>                                    
                                    </td>
                                <td align="left" valign="top"></td>
                                <td align="left">
                                    <asp:ListBox runat="server" ID="lstProfileAddress" CssClass="altSelectFormat" Rows="3" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveList('Rem',this)"></asp:ListBox>
                                    <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="lstProfileAddress" ValidationGroup="Submit" runat="server" ErrorMessage="Required" Display="Dynamic" Enabled="false" ></asp:RequiredFieldValidator>                                    
                                    <input type="hidden" name="hdnprofileAddresses" id="hdnprofileAddresses" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.MultiCodec") %>' />
                                </td>
                                <td align="left" class="tableBody">
                                    Preferred Bandwidth<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.LineRate") %>'></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqLineRate" runat="server" InitialValue="-1" ControlToValidate="lstLineRate" ValidationGroup="Submit" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="tableBody">
                                    Assigned to MCU<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Bridge") %>'></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqBridges" runat="server" InitialValue="-1" Enabled="false" ControlToValidate="lstBridges" ValidationGroup="Submit" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator><%--FB 1901--%>                                    
                                 </td>
                                 <td align="left" class="tableBody">
                                    Preferred Dialing Option<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>' onchange="javascript:ValidateSelection(this);" >
                                    </asp:DropDownList><%--Fogbugz case 427--%>
                                    <asp:RequiredFieldValidator ID="reqPrefDial" runat="server" InitialValue="-1" Enabled="false" ControlToValidate="lstConnectionType" ValidationGroup="Submit" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator><%--FB 2093--%> 
                                </td>
                                <td align="left" class="tableBody">
                                    Default Protocol</td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstVideoProtocol" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProtocol") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="tableBody">
                                    Associate with MCU Address</td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtMCUAddress" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.MCUAddress") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegMCUAddress" ControlToValidate="txtMCUAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" class="tableBody">
                                    MCU Address Type</td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstMCUAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.MCUAddressType") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);" ></asp:DropDownList>
                                </td>
                                <td align="left" class="tableBody">
                                    Web Access URL</td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtURL" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.URL") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegURL" ControlToValidate="txtURL" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()'~]*$"></asp:RegularExpressionValidator>    
                                </td>
                            </tr>
                            
                             <%-- Code Added For FB 1422- Start--%>                            
                            <tr>
                            <td align="left" class="tableBody">
                                    iCal Invite</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkIsCalderInvite" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.IsCalendarInvite").Equals("1") %>' />
                                </td>
                               <td align="left" class="tableBody">
                                    Encryption Preferred</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkEncryptionPreferred" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.EncryptionPreferred").Equals("1") %>'/>
                                </td>
                                <td align="left" class="tableBody">
                                    Telnet API Enabled</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkP2PSupport" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.TelnetAPI").Equals("1") %>'/>
                                </td>
                            </tr>
                            <%-- Code Added For FB 1422- End--%> 
                            <tr>
                                <td align="left" class="tableBody">Email ID</td> <%--ICAL Fix--%>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtExchangeID" runat="server" Width="200px" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.ExchangeID") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegExchangeID" ControlToValidate="txtExchangeID" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                
                                <%--API Port Starts--%>
                                <td align="left" class="tableBody">
                                API Port
                                </td>
                                <td align="left"> <!-- FB 2050 -->
                                    <asp:TextBox   ID="txtApiport" runat="server" MaxLength="5"  Text='<%# DataBinder.Eval(Container, "DataItem.apiPortno") %>' CssClass="altText"></asp:TextBox>                                
                                    <asp:RegularExpressionValidator ID="RegApiport" ControlToValidate="txtApiport" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="^\d{1,5}$"></asp:RegularExpressionValidator>
                                </td>
                               <%--API Port Ends--%>
                               <td align="left" class="tableBody">
                                 Rear/Security Camera Address
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtRearSecCamAdd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RearSecCameraAddress") %>' CssClass="altText" ValidationGroup="Submit"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator ID="regRearSecCamAdd" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtRearSecCamAdd" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <%--ZD 100132 START--%>
                            <tr>
                            <td align="left" class="tableBody">
                                    Located Outside Network</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkIsOutside" runat="server" onclick="javascript:fnOpengatekeeper(this)" Checked='<%# (DataBinder.Eval(Container, "DataItem.IsOutside").Equals("1")) %>' /><%--ZEN 100132--%>
                                </td>
                                <td align="left" id="tdGateKeeeperAddress"  runat="server">
                                   <asp:Label ID="LblGateKeeeperAddress" runat="server" Text="Gatekeeper Address"></asp:Label>
                                   </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtGateKeeeperAddress" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.GateKeeeperAddress") %>' Width="150px"></asp:TextBox>
                            </tr>
                            <%--ZEN 100132 End--%>
                            <tr>
                                <%--FB 2595 Start --%>
                                <td align="left" class="tableBody" >
                                <asp:Label ID="lblSecured" runat="server" Text="Secure"></asp:Label>
                                </td>
                                    <td align="left"> 
                                    <asp:CheckBox ID="chkSecured" runat="server" onclick="javascript:fnSecuredDetails(this)" Checked='<%# DataBinder.Eval(Container, "DataItem.Secured").Equals("1") %>' />
                                </td>
                                <td align="left" class="tableBody" id="tdNetworkURL"  runat="server">
                                <asp:Label ID="lblNetworkURL" runat="server" Text="Network URL"></asp:Label>
                                </td>
                                <td align="left" id="tdTxtNetworkURL" runat="server"> 
                                     <asp:TextBox CssClass="altText"  ID="txtNetworkURL" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.NetworkURL") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td align="left" class="tableBody" id="tdSecuredPort"  runat="server">
                             <asp:Label ID="lblSecuredPort" runat="server" Text="Secure Port"></asp:Label>
                                </td>
                                <td align="left" id="tdTxtSecuredPort" runat="server">
                                    <asp:TextBox ID="txtSecuredport" runat="server" MaxLength="5"  Text='<%# DataBinder.Eval(Container, "DataItem.Securedport") %>' CssClass="altText"></asp:TextBox>                                
                                    <asp:RegularExpressionValidator ID="RegSecuredport" ControlToValidate="txtSecuredport" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="^\d{1,5}$"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <%--FB 2595 End --%>
                        </table>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                        <FooterStyle BackColor="beige" />
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Default" ItemStyle-VerticalAlign="Top">
                    <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:RadioButton ID="rdDefault" runat="server" onclick="javascript:SelectOneDefault(this)" GroupName="Default" Checked='<%# DataBinder.Eval(Container, "DataItem.DefaultProfile").Equals("1") %>' /></td>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Delete" ItemStyle-VerticalAlign="Top">
                    <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" runat="server" Checked="false" onclick="javascript:CheckDefault(this)" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <SelectedItemStyle BackColor="Beige" />
             </asp:DataGrid>
         <tr>
            <td align="center">
                <asp:Button ID="btnCancel" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Cancel" OnClick="CancelEndpoint" OnClientClick="DataLoading(1)" /> <%--ZD 100176--%>
                <asp:Button ID="btnAddNewProfile" runat="server"  Text="Submit/Add New Profile" OnClick="AddNewProfile" CausesValidation="true" ValidationGroup="Submit"  Width="205px" Height="26px" /> <%--FB 2796--%>  <%--FB 3012--%>
                <asp:Button ID="btnSubmit" runat="server"  Text="Submit" OnClick="SubmitEndpoint" OnClientClick="javascript:return IsMarkedForDeletion();" Width="100pt" ValidationGroup="Submit"/> <%--FB 2796--%>
                <asp:CustomValidator ID="cvSubmit" ValidationGroup="Submit" OnServerValidate="ValidateInput" SetFocusOnError="true" runat="server" ErrorMessage="Invalid IP/ISDN Address" Display="dynamic"></asp:CustomValidator>
            </td>
         </tr>

        </table>
</center>
                <input type="hidden" id="helpPage" value="29">
                <input type="hidden" id="isMarkedDeleted" value="0" runat="server" />
    
    <script language="javascript" type="text/javascript">

        //FB 2400 start
        function enableSubAddProfile(obj)
         {
            var datagridID = obj.id.replace(obj.id.split("_", 3)[2], "");
             if (document.getElementById(obj.id).checked) {
                 document.getElementById(datagridID + "btnProfileAddr").disabled = false;
                 document.getElementById(datagridID + "btnProfileAddr").className = "altShortBlueButtonFormat"; //FB 2664
                 document.getElementById(datagridID + "lstProfileAddress").disabled = false;
                 //document.getElementById("btnAddNewProfile").disabled = true; //FB 2602
                 AddRemoveList('add', obj)
             }
             else {
                 document.getElementById(datagridID + "btnProfileAddr").disabled = true;
                 document.getElementById(datagridID + "btnProfileAddr").className = "btndisable"; //FB 2664
                 document.getElementById(datagridID + "lstProfileAddress").disabled = true;
                 if('<%=Session["admin"]%>' != "3") //FB 2670
                    document.getElementById("btnAddNewProfile").disabled = false;
                 
                 var lstBox = document.getElementById(datagridID + "lstProfileAddress");
                 if (lstBox.options.length > 0)
                 {
                     document.getElementById(datagridID + "txtAddress").value = lstBox.options[0].text
                     document.getElementById(datagridID + "hdnprofileAddresses").value = "";
                 }

                 for (i = lstBox.options.length - 1; i >= 0; i--)
                     lstBox.remove(i);
                 
             }

            return false;
        }
    
        function AddRemoveList(opr,obj)
        {
            if (obj.id == null)
                return false;
            
            var datagridID = obj.id.replace(obj.id.split("_",3)[2], "");
            var lstBox = document.getElementById(datagridID + "lstProfileAddress");
            var txtUsrInput = document.getElementById(datagridID + "txtAddress");
            var hdnprofileAddresses = document.getElementById(datagridID + "hdnprofileAddresses");
            ValidatorEnable(document.getElementById(datagridID + "reqAddress"), false);
            
            if (opr == "Rem") {
                var i;
                for (i = lstBox.options.length - 1; i >= 0; i--) {
                    if (lstBox.options[i].selected) {

                        if (lstBox.options.length > 1)
                        {
                            if (i == lstBox.options.length - 1)
                                lstBox.options[i].text = "~" + lstBox.options[i].text;
                            else if (i == 0)
                                lstBox.options[i].text = lstBox.options[i].text + "~";
                        }                        
                        hdnprofileAddresses.value = hdnprofileAddresses.value.replace(lstBox.options[i].text, "").replace(/��/i, "~")
                        lstBox.remove(i);
                    }
                }
            }
            else if (opr == "add") {
            if (txtUsrInput.value.replace(/\s/g, "") == "") //trim the textbox
                return false;
            
                if (lstBox.options.length >= 8) {
                    document.getElementById("errLabel").innerHTML = "Maximum 8 addresses";
                    document.getElementById("errLabel").focus();
                    return false;
                }
                else {
                    for (i = lstBox.options.length - 1; i >= 0; i--)
                    {
                        if (lstBox.options[i].text.replace(/\s/g, "") == txtUsrInput.value.replace(/\s/g, ""))
                        {
                            document.getElementById("errLabel").innerHTML = "Already Added address";
                            return false;
                        }
                    }
                }

                if (lstBox.options.length > 0)
                    hdnprofileAddresses.value = hdnprofileAddresses.value + "~";
                
                var option = document.createElement("Option");
                option.text = txtUsrInput.value;
                option.title = txtUsrInput.value;
                lstBox.add(option);
                              
                hdnprofileAddresses.value = hdnprofileAddresses.value + txtUsrInput.value;

                txtUsrInput.value = "";
                txtUsrInput.focus();
            }

            return false;
        }

        // ZD 100263
        function fnSetPasswordStatus() {
            if ('<%=Session["EndpointID"]%>' == 'new') {
                document.getElementById("dgProfiles_ctl02_txtProfilePassword").style.backgroundImage = "";
                document.getElementById("dgProfiles_ctl02_txtProfilePassword2").style.backgroundImage = "";
            }
            if ('<%=Session["profCnt"]%>' != '') {
                document.getElementById("dgProfiles_ctl0" + '<%=Session["profCnt"]%>' + "_txtProfilePassword").style.backgroundImage = "";
                document.getElementById("dgProfiles_ctl0" + '<%=Session["profCnt"]%>' + "_txtProfilePassword2").style.backgroundImage = "";
            }
        }
        setTimeout("fnSetPasswordStatus()", 100);
        
    </script>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->