﻿
<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_EditEntityOption.EditEntityOption" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
    
    function frmValidator()
    {   
        if (!Page_ClientValidate())
               return Page_IsValid;
     
        var txtentityname = document.getElementById('<%=txtEntityName.ClientID%>');
        if(txtentityname != null)//FB 2491
        {
            if(txtentityname.value == "")
            {        
                reqEntityName.style.display = 'block';
                txtentityname.focus();
                return false;
            }
            else if (txtentityname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
            {        
                regItemName1.style.display = 'block';
                txtentityname.focus();
                return false;
            }    
        }
        var txtentitydesc = document.getElementById('<%=txtEntityDesc.ClientID%>');
        if(txtentitydesc != null)//FB 2491
        {    
            if(txtentitydesc.value != "")
            {
                if (txtentitydesc.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
                {        
                    regItemDesc.style.display = 'block';
                    txtentitydesc.focus();
                    return false;
                }    
            }
        }
    return(true);
    }
    
    
    function frmClearValidator()
    {
        var regItemName1 = document.getElementById("regItemName1");
        if(regItemName1 != null)
            regItemName1.innerHTML = "";
        var regItemDesc = document.getElementById("regItemDesc");
        if(regItemDesc != null)
            regItemDesc.innerHTML = "";
            DataLoading(1);//ZD 100176
    
    }
    
    function FnConfirm(msg)
    {
              
        if(confirm(" " + msg + " are linked to the selected entity option and data will be lost. \n Are you sure you want to edit this entity option?"))
            {
	            var btn = document.getElementById("btnHidden");
                btn.click();
	            return true;
            }
        else
            {
	            return false;
            }
    
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End
    </script>

</head>
<body>
    <form id="frmEntityCode" runat="server">
    <div>
        <center>
            <table width="55%" border="0">
                <tr>
                    <td colspan="3">
                        <h3>
                            <asp:Label ID="lblHeader" runat="server">Manage Entity Code</asp:Label> <%--FB 2670--%>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <h3>
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                            <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>
                        </h3>
                    </td>
                </tr>
                <tr align="center">
                    <td align="center">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="4" height="30">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                </td>
                                <td align="left" style="width: 30%">
                                    <asp:Label ID="lblOptionName" runat="server" CssClass="blackblodtext" Text="Option Name"></asp:Label>
                                </td>
                                <td align="left" style="width: 30%">
                                    <asp:Label ID="lblOptionDesc" runat="server" CssClass="blackblodtext" Text="Option Description"></asp:Label>
                                </td>
                            </tr>
                            <tr id="trLang" runat="server">
                                <td valign="top" style="width: 3%"> <!-- FB 2050 -->
                                    <br />
                                    <asp:ImageButton ID="imgLangsOption" runat="server" ImageUrl="image/loc/nolines_minus.gif"
                                        Height="20" Width="100%" vspace="0" hspace="0" ToolTip="View Other Languages" />
                                </td>
                                <td style="width: 18%" align="left" valign="top">
                                    <br />
                                    <a class="blackblodtext">English</a>
                                </td>
                                <td align="left" valign="top" style="width: 40%">
                                    <br />
                                    <asp:TextBox ID="txtEntityID" Text="new" Visible="false" runat="server" Width="1%"></asp:TextBox>
                                    <asp:TextBox ID="txtEntityName" runat="server" Width="80%" CssClass="altText" MaxLength="35"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqEntityName" ValidationGroup="Submit" runat="server"
                                        ControlToValidate="txtEntityName" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" valign="top" style="width: 34%">
                                    <br />
                                    <asp:TextBox ID="txtEntityDesc" Width="93%" runat="server" CssClass="altText" MaxLength="35"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtEntityDesc"
                                        ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : #<br> $ @ ~ and &#34; are invalid characters."
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td width="9%">
                                </td>
                            </tr>
                            <tr id="LangsOptionRow" runat="server" style="width: 100%" align="center">
                                <td style="width: 1%">
                                </td>
                                <td colspan="4" align="left">
                                    <asp:DataGrid ID="dgLangOptionlist" runat="server" AutoGenerateColumns="false" GridLines="None"
                                        CellPadding="3" Style="border-collapse: separate; border: 1;" ShowHeader="false"
                                        Width="100%">
                                        <ItemStyle Height="2" VerticalAlign="Top" />
                                        <Columns>
                                            <asp:BoundColumn DataField="OptionID" Visible="false"></asp:BoundColumn>
                                            <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>'
                                                        Visible="false"></asp:Label>
                                                    <asp:Label ID="lblLangName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="35%" ItemStyle-Height="25%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtOptionID" Text='<%DataBinder.Eval(Container, "DataItem.OptionID") %>'
                                                        Visible="false" runat="server" Width="80%"></asp:TextBox>
                                                    <asp:TextBox ID="txtOptionName" runat="server" Width="80%" CssClass="altText" MaxLength="35"
                                                        Text='<%#DataBinder.Eval(Container, "DataItem.DisplayCaption") %>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtOptionName"
                                                        ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                                        ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="35%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtOptionDesc" Width="80%" runat="server" CssClass="altText" MaxLength="35"
                                                        Text='<%#DataBinder.Eval(Container, "DataItem.HelpText") %>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtOptionDesc"
                                                        ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                                        ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @<br> ~ and &#34; are invalid characters."
                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <br />
                                    <asp:Button ID="btnCancel" runat="server" Text="Go Back" CssClass="altMedium0BlueButtonFormat"
                                        OnClick="btnCancel_Click" OnClientClick="javascript:return frmClearValidator()" />
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit"  Width="100pt"
                                        OnClick="CreateNewEntityCode" OnClientClick="javascript:return frmValidator()" /> <%--FB 2796--%>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Submit" Width="100pt"
                                        OnClick="UpdateEntityCode" OnClientClick="javascript:return frmValidator()" /> <%--FB 2796--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>

<script type="text/javascript" src="inc/softedge.js"></script>

<script type="text/javascript">

ExpandCollapse(document.getElementById("imgLangsOption"), "<%=LangsOptionRow.ClientID %>", true);

function ExpandCollapse(img, str, frmCheck)
{
    obj = document.getElementById(str);
    if (obj != null)
    {
        if (img != null)
        {
            if (frmCheck == true)
            {
                img.src = img.src.replace("minus", "plus");
                obj.style.display = "none";
            }
            if (frmCheck == false)
            {
                if (img.src.indexOf("minus") >= 0)
                {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
        }
    }
} 
</script>
<%--FB 2500--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->