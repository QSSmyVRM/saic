<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_HolidayDetails.EditHolidayDetails" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Holiday Details</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script language="javascript" type="text/javascript" src="../en/Organizations/Original/Javascript/RGBColorPalette.js"> </script>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="script/calview.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/settings2.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
</head>
<body>
    <form id="frmHolidayManagement" runat="server" method="post">
    <input type="hidden" runat="server" id="hdnDateList"  />
    <input type="hidden" runat="server" id="hdnTypeID"  />
    <div>
        <table width="100%" >
            <tr><td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr style="height:15px">
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="50%" border="0">
                        <tr>
                            <td align="left" style="width:15%" class="blackblodtext">
                                Name <span class="reqfldText">*</span>              
                            </td>
                            <td align="left"> <!-- FB 2050 -->
                                <asp:TextBox ID="txtHolidayName" runat="server" CssClass="altText" Text="" Width="250" MaxLength="50" onblur="javascript:document.getElementById('errLabel').innerHTML = '' " ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqHolidayName" SetFocusOnError="true" runat="server" ControlToValidate="txtHolidayName" 
                                ErrorMessage="Required"  Display="dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regHolidayName" SetFocusOnError="true" ControlToValidate="txtHolidayName" Display="dynamic" 
                                runat="server" ValidationGroup="Submit" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">
                                Color <span class="reqfldText">*</span>
                            </td>
                            <td align="left"> <!-- FB 2050 -->
                                <asp:TextBox EnableViewState="True" id="txtbgcolor" Runat="server" CssClass="altText" onblur="javascript:document.getElementById('errLabel').innerHTML = '' "></asp:TextBox>
								<img title="Click to select the color" onclick="FnShowPalette(event,'txtbgcolor')"  style="vertical-align:text-bottom;"
																	height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgbgcolor"/>
								<asp:RequiredFieldValidator ID="reqbgColor" SetFocusOnError="true" runat="server" ControlToValidate="txtbgcolor" 
                                ErrorMessage="Required"  Display="dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>									
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">
                                Block
                            </td>
                            <td align="left"> <!-- FB 2050 -->
                                <asp:CheckBox ID="chkBlocked" runat="server" Enabled="false" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left"  valign="top" class="blackblodtext"><br />Color Dates</td>
                            <td align="left">
                                <table border="0" width="100%" cellspacing="0">
                                    <tr>
                                        <td style="width:30%">                            
                                            <div id="flatCalendarDisplay" style="float: right; clear: both;"></div><br />                                
                                            <div id="preview" style="font-size: 80%; text-align: center; padding: 2px"></div>
                                        </td>
                                         <td></td>
                                        <td style="width:20%" valign="top">
                                            <span class="blueSText"> <b>Selected Date</b></span><br />
                                            <asp:ListBox runat="server" id="CustomDate" Rows="8" CssClass="altSmall0SelectFormat" onChange="JavaScript: removedate(this);" onblur="javascript:document.getElementById('errLabel').innerHTML = '' " EnableViewState="true"></asp:ListBox>
                                            <br />
                                            <span class="blueSText">* click a date to remove it from the list.</span>
                                        </td>
                                        <td>                            
                                            <asp:Button ID="btnsortDates" runat="server" Text="Sort" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:return SortDates();" />
                                        </td>
                                    </tr>                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><br \ />
                   <input class="altMedium0BlueButtonFormat" onclick="fnCancel()" type="button" value="Cancel" name="btnCancel" />
                   <asp:Button ID="btnSubmit" runat="server" Width="100pt" Text="Submit" ValidationGroup="Submit" OnClientClick="javascript:return fnListValue();" OnClick="SetOrgHolidays"/><%-- FB 2976--%>
                </td>
            </tr> 
        </table>
    </div>
    <img src="keepalive.asp" name="myPic" width="1px" height="1px"/>
    <input type="hidden" value="<%=Session["ThemeType"]%>" id="sessionThemeType" /> <%--FB 2815--%>
    </form>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<script language="javascript" type="text/javascript">

    var servertoday = new Date();
    var dFormat;
    dFormat = "<%=format %>";
    
    showFlatCalendar(1,dFormat)
    
    function fnCancel()
    {
	    window.location.replace('HolidayDetails.aspx');
    }

    function fnListValue()
    {
	    var datecb = document.frmHolidayManagement.CustomDate;
	    var datelist = document.getElementById("hdnDateList");

	    if (!Page_ClientValidate())
	        return Page_IsValid;

	    if (datecb.length < 2) {
	        document.getElementById("errLabel").innerHTML = "Please select at least Two Dates";
	        return false;
	    }    
	  
	    for (var i=0; i<datecb.length; i++) 
	    {
	        if(datelist.value == "")   
		        datelist.value = datecb.options[i].value;
		    else
		        datelist.value = datelist.value + "," + datecb.options[i].value;
		}
    }
    
    function Browser() 
	{
	    var ua, s, i;
	    this.isIE    = false;
	    this.isNS    = false;
	    this.version = null;

	    ua = navigator.userAgent;

	    s = "MSIE";
	    if ((i = ua.indexOf(s)) >= 0) 
	    {
		    this.isIE = true;
		    this.version = parseFloat(ua.substr(i + s.length));
		    return;
	    }

	    s = "Netscape6/";
	    if ((i = ua.indexOf(s)) >= 0) 
	    {
		    this.isNS = true;
		    this.version = parseFloat(ua.substr(i + s.length));
		    return;
	    }

	    // Treat any other "Gecko" browser as NS 6.1.

	    s = "Gecko";
	    if ((i = ua.indexOf(s)) >= 0) 
	    {
		    this.isNS = true;
		    this.version = 6.1;
		    return;
	    }
	}
    var browser = new Browser();
	//FnHideShow();
	
	//Div Height Patch for Netscape 
	if (browser.isNS) 
	{
		var divmd = document.getElementById("md");
		divmd.style.height = "260px"; 		
	}
	//Div Height Patch for Netscape 
    function FnShowPalette()
    {
	    var args = FnShowPalette.arguments;
	    var x,y,e;
	    e= args[0]; 
	    if (!e) e = window.event;
	    if (browser.isIE) 
	    {
		    x = e.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
		    y = e.clientY + document.documentElement.scrollTop + document.body.scrollTop;
	    }
	    if (browser.isNS) 
	    {
		    x = e.clientX + window.scrollX;
		    y = e.clientY + window.scrollY;
	    }
	    show_RGBPalette(args[1],x,y);
    }
    function isOverInstanceLimit(cb)
    {
	    csl = parseInt("<%=CustomSelectedLimit%>");

	    if (!isNaN(csl)) {
		    if (cb.length >= csl) {
			    alert(EN_211)
			    return true;
		    }
	    }
    	
	    return false;
    }

    function SortDates()
    {
	    var temp;
	    datecb = document.frmHolidayManagement.CustomDate;
	    var dateary = new Array();

	    for (var i=0; i<datecb.length; i++) 
	    {
		    dateary[i] = datecb.options[i].value;
    		
		    dateary[i] = ( (parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10) ) + "/" + ( (parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10) ) + "/" + ( parseInt(dateary[i].split("/")[2], 10) );
	    }

	    for (i=0;i<dateary.length-1;i++)
	 	    for(j=i+1;j<dateary.length;j++)
			    if (mydatesort(dateary[i], dateary[j]) > 0)
			    {
				    temp = dateary[i];
				    dateary[i] = dateary[j];
				    dateary[j] = temp;	
			    }

	    for (var i=0; i<dateary.length; i++) 
	    {
		    datecb.options[i].text = dateary[i];
		    datecb.options[i].value = dateary[i];		
	    } 

        return false;
    }
    
    function removedate(cb)
    {    
	    if (cb.selectedIndex != -1) {
	        cb.options[cb.selectedIndex] = null;
	    }
	    cal.refresh();
    }

</script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

