<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_EmailCustomization.EmailCustomization" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Customisation</title>
</head>
<body>
    <form id="form1" runat="server" onsubmit="DataLoading(1);"> <%--ZD 100176--%>
    <input id="hdnEmailContID" name="hdnEmailContID" runat="server" type="hidden" />
    <input id="hdnEmailLangID" name="hdnEmailLangID" runat="server" type="hidden" />
    <input id="hdnEmailLangName" name="hdnEmailLangName" runat="server" type="hidden" />
    <input id="hdnPlaceHolders" name="hdnPlaceHolders" runat="server" type="hidden" />
    <input id="hdnCreateType" name="hdnCreateType" runat="server" type="hidden" />
    <input id="hdnEmailMode" name="hdnEmailMode" runat="server" type="hidden" />
    <input id="hdnuserid" name="hdnuserid" runat="server" type="hidden" />
    
    <div id="PlaceHolder"  runat="server" align="center" style="top: 250px;left:565px; POSITION: absolute; WIDTH:45%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none"> 
      <table cellpadding="2" cellspacing="1"  width="70%" class="tableBody" align="center">
         <tr>
            <td class="subtitleblueblodtext" align="center" colspan="2">
                Placeholder Description
            </td>            
         </tr>
         <tr>
            <td width="9%"></td>
           <td align="center">
               <div style="width: 100%;height: 350px;overflow: auto;" id="dd" runat="server" >
                    <asp:DataGrid ID="dgPlaceHolders" runat="server" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true" 
                          BorderStyle="solid" BorderWidth="0" ShowFooter="False" 
                         Width="100%" Visible="true" style="border-collapse:separate" >
                        <SelectedItemStyle  CssClass="tableBody"/>
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody"  />                        
                        <FooterStyle CssClass="tableBody"/>
                        <Columns>
                            <asp:BoundColumn DataField="PlaceHolderID"  HeaderStyle-Width="5%" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" ItemStyle-CssClass="tableBody" ></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
           </td>
          </tr>
          <tr>
           <td align="center" colspan="2">
              <asp:Button ID="BtnClose" Text="Close" CssClass="altShortBlueButtonFormat" Runat="server" OnClientClick="javascript:return fnShowHide('0')"></asp:Button>
           </td>
          </tr>
        </table>
    </div>
    <div>
    <h3><span id="spnHeader" runat="server"></span></h3><!-- FB 2570 -->
     <table width="100%" border="0" cellpadding="5">
        <tr>
        <td style="width:15%"></td> <!-- FB 2050 -->
        <td align="center" style="width:55%"> <!-- FB 2050 -->
            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" Visible="False"></asp:Label><br />  
             <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>  
        </td>
        <td style="width:30%"></td> <!-- FB 2050 -->
        </tr>
        <tr>
            <td valign="top" align="left" class="blackblodtext"> <!-- FB 2050 -->
                Email Language Name
            </td>
            <td align="left" colspan="2" >
                <asp:TextBox ID="txtEmailLang" runat="server" Width="15%" CssClass="altText"></asp:TextBox>
            </td>
        </tr>
        <tr>           
            <td align="left" class="blackblodtext" valign="top" >
               Email Category
            </td>
            <td colspan="2">
                <table width="100%" border="0" cellspacing="0">
                   <tr>            
                        <td align="left" valign="top" width="18%">
                            <asp:DropDownList ID="lstEmailCategory" runat="server" AutoPostBack="true" CssClass="altSelectFormat" OnSelectedIndexChanged="BindMailTypes" onchange="JavaScript:modedisplay(); DataLoading(1);" ><%--ZD 100176--%> 
                                <asp:ListItem Value="1">General</asp:ListItem>
                                <asp:ListItem Value="2">Pre Conf Scheduling</asp:ListItem>
                                <asp:ListItem Value="3">Approval Denial Mails</asp:ListItem>
                                <asp:ListItem Value="4">Confirmation Mails</asp:ListItem>
                                <asp:ListItem Value="5">Post Conf Scheduling</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top" class="blackblodtext" width="12%" align="center"> <%--FB 1948--%>
                            Email Type
                        </td>
                        <td width="25%" align="left" valign="top"> <%--FB 1948--%>
                            <asp:DropDownList ID="lstEmailType" runat="server" AutoPostBack="true" CssClass="altSelectFormat" OnSelectedIndexChanged="GetEmailContent" DataTextField="emailtype" DataValueField="emailtypeid" onchange="JavaScript:defaultmode(); DataLoading(1);" Width="90%"></asp:DropDownList> <%--ZD 100176--%> 
                        </td>
                        <td valign="top" id="tdConfMode">
                            <table width="100%" id="tableConfMode" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                <td valign="top" width="45%" class="blackblodtext" align="left">
                                    Conference Mode
                                </td>
                                <td valign="top"> <%--FB 1948--%>
                                    <asp:DropDownList ID="lstEmailMode" runat="server" AutoPostBack="true" CssClass="altSelectFormat" onchange="JavaScript:setconfmode('1'); DataLoading(1);" OnSelectedIndexChanged="GetEmailContent"> <%--ZD 100176--%> 
                                        <asp:ListItem Value="1">Create</asp:ListItem>
                                        <asp:ListItem Value="2">Edit</asp:ListItem>
                                    </asp:DropDownList>
                                    
                                    <asp:DropDownList ID="lstAllEmailMode" runat="server" AutoPostBack="true" onchange="JavaScript:setconfmode('2')" CssClass="altSelectFormat" OnSelectedIndexChanged="GetEmailContent">
                                        <asp:ListItem Value="1">Create</asp:ListItem>
                                        <asp:ListItem Value="2">Edit</asp:ListItem>
                                        <asp:ListItem Value="3">Delete</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                              </tr>
                            </table>
                        </td>
                        <%--<td valign="top"> <%--FB 1948--%>
                             <%--<asp:Button Text="Save Current" runat="server" ID="btnSubmit" CssClass="altShortBlueButtonFormat" OnClick="SetEmailContent" />
                        </td>--%>
                        
                    </tr>
                </table>
            </td>
        </tr>        
        <tr>
            <td valign="top" align="left" class="blackblodtext">
                Email Subject
            </td>
            <td align="left"> <!-- FB 2050 -->
                <asp:TextBox ID="txtEmailSubject" runat="server" CssClass="altText"> 
                </asp:TextBox><!-- FB 2050 -->
            </td>
            <td>
                 <asp:LinkButton ID="lnkShow" Text="Placeholders" runat="server" OnClientClick="javascript:return fnShowHide('1')"></asp:LinkButton>
            </td>                   
        </tr>       
        <tr>
            <td valign="top" align="left"  class="blackblodtext">
                Email Body <%--FB 2579--%>
            </td>
            <td align="left" valign='top' colspan="2"> <!-- FB 2050 -->
                <dxHE:ASPxHtmlEditor ID="dxHTMLEditor" runat="server" Width="100%">
                </dxHE:ASPxHtmlEditor>
           </td>
        </tr>
        <tr>
          <td class="blackblodtext"></td>
          <td style="color: Red;" align="left" colspan="2"> <!-- FB 2050 -->
              Note: Please don't change place holders "{*}". It may cause unrecoverable error in email.
           </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <table width="100%" align="center">
                    <tr>
                    <%--Added for SoftEdge Button--%>
                    <td>
                    <input type='submit' name='SoftEdgeTest1'  style='max-height:0px;max-width:0px;height:0px;width:0px;background-color:Transparent;border:None;'/></td>
                        <td align="center">
                            <asp:Button Text="Submit" runat="server" ID="btnSubmit"  OnClick="SetEmailContent" Width="100pt" /> <%--FB 2565 FB 2796--%>
                            <asp:Button Text="Go Back" runat="server" ID="btnCancel" CssClass="altMedium0BlueButtonFormat" OnClick="RedirectToTargetPage" OnClientClick="javascript:DataLoading(1);"/><%--ZD 100176--%> 
                        </td>
                        <td width="2%"></td>
                        
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                
    </div>
    </form>
</body>
</html>
<script type="text/javascript">
function changeFocus()
{
  // FB 2050
  var obj1 = document.getElementById("txtEmailSubject"); // For EmailCustomization Page
  if(obj1 != null)
  obj1.focus();
}
window.onload = changeFocus;
</script>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script language="JavaScript" type="text/javascript">
 modedisplay();
 function modedisplay()
  {
     var mode = document.getElementById("lstEmailMode");
     var modeall = document.getElementById("lstAllEmailMode");
     var hdnconfmode = document.getElementById("hdnEmailMode");
     
     document.getElementById("tableConfMode").style.display = "none";
     if (document.getElementById("lstEmailCategory").value == "2") 
     {
         document.getElementById("tableConfMode").style.display = "block";
         mode.style.display = "block";
         modeall.style.display = "none";
         hdnconfmode.value = "1";
     }
     else if (document.getElementById("lstEmailCategory").value == "4")
      {
         document.getElementById("tableConfMode").style.display = "block";
         mode.style.display = "none";
         modeall.style.display = "block";
         hdnconfmode.value = "1";
     }
 }
 
 function setconfmode()
 {
    var args=setconfmode.arguments;
    var mode = document.getElementById("lstEmailMode");
    var modeall = document.getElementById("lstAllEmailMode");
    var hdnconfmode = document.getElementById("hdnEmailMode");
    
    if(args[0] == '1')
    {
        hdnconfmode.value = '';
        hdnconfmode.value = mode.value;
    }
    else
    {
        hdnconfmode.value = '';
        hdnconfmode.value = modeall.value;
    }
 }
  
 function defaultmode() 
 {
     document.getElementById("lstAllEmailMode").value = "1";
     document.getElementById("lstEmailMode").value = "1";
 }
 
function fnShowHide(arg) {
    //f(document.getElementById("lnkShow").disabled == false) { //FB 2768
      if (arg == '1')
          document.getElementById("PlaceHolder").style.display = 'block';
      else if (arg == '0')
          document.getElementById("PlaceHolder").style.display = 'none';
    //}
    return false; 
  }

 if (document.getElementById('dxHTMLEditor_TD_T0_DXI15_I')) //To hide ImageButton in Control
     document.getElementById('dxHTMLEditor_TD_T0_DXI15_I').style.display = 'none'

 var btnsubmit = document.getElementById("btnSubmit");
 var lnkshow = document.getElementById("lnkShow");
 
 if (document.getElementById("lstEmailCategory").value == "4")
 {
     var modeall = document.getElementById("lstAllEmailMode");
     var mailtype = document.getElementById("lstEmailType");
     
     if (mailtype.value == "10" || mailtype.value == "25") //MCU Alert
     {
         if (modeall.value == "3" )
         {
             btnsubmit.disabled = true;
             lnkshow.disabled = true;
             //FB 2298  - Starts
             btnsubmit.style.color = "gray";
             lnkshow.style.color = "gray";
         }
         else  
             lnkshow.disabled = false;
     }
    else 
        lnkshow.disabled = false; 
      //FB 2298 //FB 2995 - End
      
 }
 else
 {
    //FB 2670
    if("<%=isVis%>" == "true")
    {
        btnsubmit.disabled = false;
        lnkshow.disabled = false;
    }
 }
 //ZD 100176 start
 function DataLoading(val) {
     if (val == "1")
         document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
     else
         document.getElementById("dataLoadingDIV").innerHTML = "";
 }
 //ZD 100176 End    
</script>
