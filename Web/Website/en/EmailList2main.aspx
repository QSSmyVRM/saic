﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="ns_EmailListMain.en_EmailList2main" ValidateRequest="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }
    
%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>myVRM</title>
   <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <script type="text/javascript">      // FB 2790
      var path = '<%=Session["OrgCSSPath"]%>';
      path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
      document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
  </script>
    <script type="text/javascript" src="script/errorList.js"></script>
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="inc\functions.js"></script>
    <script type="text/javascript" src="sorttable.js"></script> 
    <script language="JavaScript" src="script/group2.js"></script>
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
 <script type="text/javascript" language="javascript">
    
<!--
var tabs=new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","ALL")

function chgpageoption (totalpagenum)
{
	tpn = parseInt(totalpagenum, 10)
	cb = document.frmEmaillist2main.SelPage;
		
	RemoveAllOptions (cb);
		
	for (var i=1; i<=tpn; i++) {
		addopt(cb, i, i, false, false);
	}
}	

function nameimg(prename, sel)
{
	for (i in tabs) {
		document.getElementById(prename + tabs[i]).style.backgroundColor = ""; 
	}
	document.getElementById(prename + sel.toUpperCase()).style.backgroundColor = "#FF6699"; 
}	
	
function pageimg(selectedpn)
{
	document.frmEmaillist2main.pageno.value = selectedpn;
		
	if (document.frmEmaillist2main.SelPage.options.length == 1)
		document.frmEmaillist2main.SelPage.options.selected = true;
	else
		document.frmEmaillist2main.SelPage.options[parseInt(selectedpn, 10)-1].selected = true;
}

function seltype(ifrmname, sb)
{
    	
	document.frmEmaillist2main.sortby.value = sb;
		
	document.frmEmaillist2main.pageno.value = 1;
	if (document.frmEmaillist2main.SelPage.options.length>0)
		document.frmEmaillist2main.SelPage.options[0].selected = true; 

	cmd = ( (queryField("srch") == "y") ? "RetrieveUsers" : ( (queryField("t") == "g") ? "GetGuestList" : "GetEmailList" ) );
	url = "emaillist2.aspx?cmd=" + cmd + "&audioid=" + queryField("audioid") + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&n=" + queryField("n"); //FB 1779	
	url += "&sb=" + document.frmEmaillist2main.sortby.value + "&a=" + document.frmEmaillist2main.alphabet.value + "&pn=" + document.frmEmaillist2main.pageno.value;
	url += "&fname=" + document.frmEmaillist2main.FirstName.value + "&lname=" + document.frmEmaillist2main.LastName.value + "&t=" + document.frmEmaillist2main.t.value;


	//eval(ifrmname).window.location.href = url; // FB 2050
	document.getElementById(ifrmname).src = url; // FB 2050
	
}

function selname(ifrmname, sel)
{
	document.frmEmaillist2main.alphabet.value = sel;
	document.frmEmaillist2main.pageno.value = 1;
	if (document.frmEmaillist2main.SelPage.options.length>0)
		document.frmEmaillist2main.SelPage.options[0].selected = true;
	cmd = ( (queryField("srch") == "y") ? "RetrieveUsers" : ( (queryField("t") == "g") ? "GetGuestList" : "GetEmailList" ) );
	url = "emaillist2.aspx?cmd=" + cmd + "&audioid=" + queryField("audioid") + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&fn=" + queryField("fn") + "&n=" + queryField("n");//FB 1779
	url += "&sb=" + document.frmEmaillist2main.sortby.value + "&a=" + document.frmEmaillist2main.alphabet.value + "&pn=" + document.frmEmaillist2main.pageno.value;
	url += "&fname=" + document.frmEmaillist2main.FirstName.value + "&lname=" + document.frmEmaillist2main.LastName.value + "&t=" + document.frmEmaillist2main.t.value;
	//eval(ifrmname).window.location.href = url; // FB 2050
	document.getElementById(ifrmname).src = url; // FB 2050
}

function selpage(ifrmname, cb)
{
	document.frmEmaillist2main.pageno.value = cb.options[cb.selectedIndex].value;

	cmd = ( (queryField("srch") == "y") ? "RetrieveUsers" : ( (queryField("t") == "g") ? "GetGuestList" : "GetEmailList" ) );
	//url  = "dispatcher/conferencedispatcher.asp?cmd=" + cmd + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&n=" + queryField("n");
	url = "emaillist2.aspx?cmd=" + cmd + "&audioid=" + queryField("audioid") + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&n=" + queryField("n"); //FB 1779
	url += "&sb=" + document.frmEmaillist2main.sortby.value + "&a=" + document.frmEmaillist2main.alphabet.value + "&pn=" + document.frmEmaillist2main.pageno.value;
	//Code Modified on 12Mar09 -FB 412- start
	url += "&fname=" + document.frmEmaillist2main.FirstName.value + "&lname=" + document.frmEmaillist2main.LastName.value + "&t=" + document.frmEmaillist2main.t.value + "&srch=" + queryField("srch");
	//Code Modified on 12Mar09 -FB 412- End

	//eval(ifrmname).window.location.href = url; // FB 2050
	document.getElementById(ifrmname).src = url; // FB 2050
}


function tabAllshow(needshow)
{
	if (queryField("srch") != "y")
		document.getElementById ("tabAll").style.display =(parseInt(needshow)) ? "" : "none";
}

if (opener && opener.closed) {
	alert(EN_88);
	window.close;
} else {
	var isCreate, isFu, isRm, isFuCreate, isRmCreate;
	if (opener.document.frmSettings2) {
		isCreate = ( (opener.document.frmSettings2.ConfID.value == "new") ? true : false ) ;
		isFu = ( (opener.document.frmSettings2.CreateBy.value == "1") ? true : false ) ;
		isRm = ( ((parent.opener.document.frmSettings2.CreateBy.value == "2") || (parent.opener.document.frmSettings2.CreateBy.value == "7")) ? true : false ) ;

		isFuCreate = (isFu && isCreate)
		isRmCreate = (isRm && isCreate)
	} else {
		if (opener.document.location.href.indexOf(".aspx") <= 0)
          if ((!opener.document.frmManagegroup2) && (!opener.document.frmReplaceuser) && 
		    (!opener.document.frmMainroom) && (!opener.document.frmBridgesetting) && 
		    (!opener.document.frmMainsuperadministrator) && (!opener.document.frmManagedept) && (!opener.document.frmSettings2Report)) {
			alert(EN_88);
			window.close();
		}
		
	}
}
function viewVRM ()
{
	if (opener.getYourOwnEmailList) {
	
		opener.getYourOwnEmailList(999);
	} else {
		alert(EN_88);
		window.close();
	}
}
function viewGuest ()
{
    
	if (opener.getGuest) 
	{
		opener.getGuest();
		
	} else
	{
		alert(EN_88);
		window.close();
	}
}
function Sort(id)
{
   
	ifrmList.sortlist(id);
}
function Reset ()
{
	ifrmList.Reset();
}

function GoToSearch()
{
var url = "";

//url = "emailsearch.aspx?t="+ document.frmEmaillist2main.t.value +"&frm="+ document.frmEmaillist2main.frm.value +"&fn="+queryField('fn')+"&n=" + queryField('n');		
//url = "emailsearch.aspx?t="+ document.frmEmaillist2main.t.value +"&frm="+ document.frmEmaillist2main.frm.value +"&fn="+queryField('fn')+"&n=";
url = "../emailsearch.aspx?t=" + document.frmEmaillist2main.t.value + "&frm=" + document.frmEmaillist2main.frm.value + "&fn=" + queryField('fn') + "&n="+ queryField('n');
//alert(url);
//window.open(url+self.location,"myvrm","width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
window.location.reload(url,"myvrm","width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
}
//-->
</script>

</head>
<body>

  <form id="frmEmaillist2main" method="POST" runat="server">
  <input type="hidden" name="sortby" value="<%=sortBy%>" />
  <input type="hidden" name="alphabet" value="<%=alphabet%>" />
  <input type="hidden" name="pageno" value="<%=pageNo%>" />
  <input type="hidden" name="mt" value="" />
  <input type="hidden" name="fromSearch" id="fromSearch"  runat="server" />
  <input type="hidden" name="FirstName" id="FirstName"  runat="server" />
  <input type="hidden" name="LastName" id="LastName"  runat="server" />
  <input type="hidden" name="LoginName" id="LoginName"  runat="server" />
<%
if (Request.QueryString["frm"]!= "")
	Response.Write ("<input type='hidden'id='frm' name='frm' value='" + Request.QueryString["frm"] + "'>");
else
    Response.Write("<input type='hidden' id='frm' name='frm' value='" + Request.Form["frm"] + "'>");

%>
  <center>
  
  <h3><asp:Label ID="LblHeading" runat="server"></asp:Label></h3>
  
  <table width="800" border="0" cellpadding="2" cellspacing="4">
    <tr>
        <td align="center" style="width: 1168px">
            <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>
        </td>
    </tr> 
    <tr>
<%
if (Request.QueryString ["srch"] != "y" ){
%>
      <td align="left" width="590">
      <%--Window Dressing--%>
        <font name="Verdana" size="2"  class="blackblodtext">Start With:</font><%--FB 2579--%>
        <a onclick="JavaScript: selname('ifrmList', 'A')"><span class="tabtext" id="A">0-A</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'B')"><span class="tabtext" id="B">B</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'C')"><span class="tabtext" id="C">C</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'D')"><span class="tabtext" id="D">D</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'E')"><span class="tabtext" id="E">E</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'F')"><span class="tabtext" id="F">F</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'G')"><span class="tabtext" id="G">G</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'H')"><span class="tabtext" id="H">H</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'I')"><span class="tabtext" id="I">I</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'J')"><span class="tabtext" id="J">J</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'K')"><span class="tabtext" id="K">K</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'L')"><span class="tabtext" id="L">L</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'M')"><span class="tabtext" id="M">M</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'N')"><span class="tabtext" id="N">N</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'O')"><span class="tabtext" id="O">O</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'P')"><span class="tabtext" id="P">P</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'Q')"><span class="tabtext" id="Q">Q</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'R')"><span class="tabtext" id="R">R</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'S')"><span class="tabtext" id="S">S</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'T')"><span class="tabtext" id="T">T</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'U')"><span class="tabtext" id="U">U</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'V')"><span class="tabtext" id="V">V</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'W')"><span class="tabtext" id="W">W</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'X')"><span class="tabtext" id="X">X</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'Y')"><span class="tabtext" id="Y">Y</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'Z')"><span class="tabtext" id="Z">Z-</span></a>
        <a onclick="JavaScript: selname('ifrmList', 'ALL')"><span class="tabtext" id="ALL">All</span></a>
      </td>
      <td align="left" id="tabAll" width="20">
      </td>
      
<%
}
else 
%>
      <td align="left" width="590"></td>
<%

%>

      <!-- <td align="right" width="90"-->
      <!--Code Modified. Changed width to 20% from 90 For myVrmLookUP  on 20Mar09 - FB 412 – Start-->
      <%--Window Dressing--%>
      <td align="right" width="20%" class="blackblodtext">
<!--Code Modified. Changed width to 20% from 90 For myVrmLookUP  on 20Mar09 - FB 412 – End-->
        Go To <select size="1" name="SelPage" onchange="selpage('ifrmList', this)" class="altText"></select> <%--Edited for FF--%><%--FB 2579--%>
      </td>
    </tr>
  </table>
  
    <table width="800" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="<%=titlealign%>">

<script type="text/javascript" >
<!--   

_d = document;
var mt = "", mt1 = "", mt2 = "";
if (queryField("frm") == "party2")  
{
	mt +="   <thead><tr>";
//	<%--Window Dressing start--%>
	mt +="     <td align='center' width='90' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "First Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 1);'>First Name</a>") + "</td>";
	mt +="     <td align='center' width='100' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "Last Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 2);'>Last Name</a>") + "</td>";
	mt +="     <td align='center' width='90' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "Login Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 3);'>Login Name</a>") + "</td>";
	mt +="     <td align='center' width='190' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "Email" : "<a href='javascript:parent.seltype(\"ifrmList\", 4);'>Email</a>") + "</td>";

	mt +="     <td align='center' width='40' class='tableHeader' height='20'>Selected</td>";

	mt1 += "    <td align='center' width='40' class='tableHeader'>";
	mt1 += "      <a title='External Attendees have their own individual physical endpoints'>External Attendees</a>";
	mt1 += "    </td>";
	mt1 += "    <td align='center' width='40' class='tableHeader'>";
	mt1 += "      <a title='Invitee participants are room-based participants.Each room is associated with only one physical endpoint'>Room Attendees</a>";
	mt1 += "    </td>";
	
	mt2 += "    <td align='center' width='40' class='tableHeader'>" + (isFuCreate ? "External Attendees" : "Room Attendees") + "</td>";

	mt += ( (isFuCreate || isRm) ?  mt2 : mt1);
	
	mt +="     <td align='center' width='15' class='tableHeader'>CC</td>";
	mt +="     <td align='center' width='25' class='tableHeader'>NOTIFY</td>";
	mt +="     <td align='center' width='30' class='tableHeader'>Audio</td>";
	mt +="     <td align='center' width='30' class='tableHeader'>Video</td>";
	mt +="   </tr></thead>";
//	<%--Window Dressing end--%>
}
//Code Added by Offshore FB issue no:412-Start
else if (queryField("frm") == "party2Aspx")  
{	
	mt +="   <thead><tr>";
	//Class Added,changed width on 27Mar08 start
	mt +="     <td align='center' width='110' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "First Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 1);'>First Name</a>") + "</td>";	
	mt +="     <td align='center' width='110' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "Last Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 2);'>Last Name</a>") + "</td>";
	mt +="     <td align='center' width='200' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "Login Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 3);'>Login Name</a>") + "</td>";
	mt +="     <td align='center' width='300' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? "Email" : "<a href='javascript:parent.seltype(\"ifrmList\", 4);'>Email</a>") + "</td>";
    mt +="     <td align='center' width='100' height='20' class='tableHeader'>Selected</td>";
	//Class Added,changed width on 27Mar08 End
//Code Commented For myVrmLookUP From templates on 20Mar09 - FB 412 - Start
	//mt1 += "    <td align='center' width='40'>";
    //mt1 += "      <a title='External Attendees have their own individual physical endpoints'>External Attendees</a>";
	//mt1 += "    </td>";
	//mt1 += "    <td align='center' width='40'>";
	//mt1 += "      <a title='Invitee participants are room-based participants.Each room is associated with only one physical endpoint'>Room Attendees</a>";
	//mt1 += "    </td>";
	
	//mt2 += "    <td align='center' width='40'>" + (isFuCreate ? "External Attendees" : "Room Attendees") + "</td>";

	//mt += ( (isFuCreate || isRm) ?  mt2 : mt1);
	
	//mt +="     <td align='center' width='15'>CC</td>";
	//mt +="     <td align='center' width='25'>NOTIFY</td>";
	//mt +="     <td align='center' width='30'>Audio</td>";
	//mt +="     <td align='center' width='30'>Video</td>";
	//mt +="   </tr></thead>";
//Code Commented For myVrmLookUP From templates on 20Mar09 - FB 412 - End		

}
//Code Added by Offshore FB issue no:412-End

else
    if  (queryField("frm") == "party2NET")
    {
        
        //Changed width  on 30Mar08 Start 
        mt +="   <thead><tr>";
        mt +="     <td align='left' width='110' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "First Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 1);'>First Name</a>") + "</td>";
        mt +="     <td align='left' width='110' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Last Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 2);'>Last Name</a>") + "</td>";
        mt +="     <td align='left' width='200' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Login Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 3);'>Login Name</a>") + "</td>";
        mt +="     <td align='left' width='300' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Email" : "<a href='javascript:parent.seltype(\"ifrmList\", 4);'>Email</a>") + "</td>";
        mt +="     <td align='left' width='100' height='20' class='tableHeader'>Selected</td>";
        mt +="   </tr></thead>";
        //Changed width  on 30Mar08 End
    }
else 
{    
	mt +="   <thead><tr>";
	//Code Modified For myVrmLookUP From templates on 20Mar09- FB 412 - Start	
	mt +="     <td align='center' width='110' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "First Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 1)'>First Name</a></b>") + "</td>";
	//mt +="     <td align='center' width='110' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "First Name" : "<a href='javascript:seltype(\"ifrmList\", 1)'>First Name</a></b>") + "</td>";
	//Code Modified For myVrmLookUP From templates on 20Mar09- FB 412 - End	
	mt +="     <td align='center' width='110' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Last Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 2)'>Last Name</a></b>") + "</td>";
	mt +="     <td align='center' width='200' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Login Name" : "<a href='javascript:parent.seltype(\"ifrmList\", 3)'>Login Name</a></b>") + "</td>";
	//Changed width Added on 30Mar08 Start 
	mt +="     <td align='center' width='300' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Email" : "<a href='javascript:parent.seltype(\"ifrmList\", 4)'>Email</a></b>") + "</td>";
	mt +="     <td align='center' width='100' class='tableHeader'>Selected</td>";
	//mt +="     <td align='center' width='216' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Email" : "<a href='javascript:parent.seltype(\"ifrmList\", 4)'>Email</a></b>") + "</td>";
   //mt +="     <td align='center' width='118' class='tableHeader'>Selected</td>";
   //Changed width Added on 30Mar08 End
	mt +="   </tr><thead>";
}
document.frmEmaillist2main.mt.value = mt;

//-->
</script>
        </td>
      </tr>
      
      <tr>
        <td> <%--//FB 1779--%>
         <iframe src="emaillist2.aspx?wintype=ifr&audioid=<%=Request.QueryString["audioid"]%>&frm=<%=Request.QueryString["frm"]%>&sn=0&fn=<%=Request.QueryString["fn"]%>&n=<%=Request.QueryString["n"]%>&t=<%=Request.QueryString["t"]%>&srch=<%=Request.QueryString["srch"]%>&lname=<%=Request.QueryString["lname"]%>&fname=<%=Request.QueryString["fname"]%>&gname=<%=Request.QueryString["gname"]%>"
         name="ifrmList" id="ifrmList" width="100%" height="220" frameborder="0" align="left" valign="top" SCROLLING="auto" >
            <p>go to <a href="emaillist2.aspx?wintype=ifr">Email List</a></p>
         </iframe>
        </td>
      </tr>
    </table>

    <table cellpadding="4" cellspacing="6">
      <tr>
        <td height="3">
        </td>
      </tr>
      <tr>
      
<%
if (Request.QueryString ["frm"] != "users" && Request.QueryString ["frm"] != "approver" && Request.QueryString ["frm"] != "approverNET" && Request.QueryString ["frm"] != "roomassist" ){
%>
        <td>
          <input type="button" name="EmaillistSubmit" value="Reset" class="altMedium0BlueButtonFormat" onclick="Javascript: Reset()" />
        </td>
<%
}
%>
        <td>
            <asp:Button ID="Emaillist2Submit" Text="Search" runat="server" CssClass="altMedium0BlueButtonFormat" OnClick="GoToSearch" />
        </td>

<%

if (Request.QueryString ["frm"] != "approver" && Request.QueryString ["frm"] != "approverNET" && Request.QueryString ["frm"] != "roomassist")
	if (Request.QueryString ["t"] == "g" ){
%>
<%--FB 1985 - Starts--%>
<%if (Application["Client"].ToString().ToUpper() == "DISNEY")
  { %>
      <td align="right">
          <input type="button" name="Settings2Submit" value="Return" class="altMedium0BlueButtonFormat" onclick="viewVRM();" language="JavaScript" style="width:150px" />
        </td>
   <% }
  else
  { %>
        
        <td align="right">
          <input type="button" name="Settings2Submit" value="View myVRM" class="altMedium0BlueButtonFormat" onclick="viewVRM();" language="JavaScript"  style="width:150px" />
        </td>
  <% } %>
<%--FB 1985 - End--%>
<%
}
%>
       
<% else if (Application["Client"] != "PNG" )%>

<%
{
%>

    <td align="right">
      <input type="button" name="Settings2Submit" value="View Guest" class="altMedium0BlueButtonFormat" onclick="viewGuest();" language="JavaScript" />
    </td>
        
<% 
}
%>
    <td>
    <%--code added for Soft Edge button--%>
      <input type="button" onfocus="this.blur()" name="EmaillistSubmit" value="Submit" class="altMedium0BlueButtonFormat" onclick="JavaScript: window.close();" />
    </td>
  </tr>
</table>
    
  </center>
  <input type="hidden" name="fn" value="<%=Request.QueryString["fn"] %>" />
  <input type="hidden" name="n" value="<%=Request.QueryString["n"] %>" />
  <input type="hidden" name="t" value="<%=Request.QueryString["t"] %>" />
  <input type="hidden" name="cmd" value="" />
</form>
<script type="text/javascript" language="JavaScript">
//<!-- FB 2596
//window.resizeTo (950, 450)
////-->
</script>

</body>

</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>