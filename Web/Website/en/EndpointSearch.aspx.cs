﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

public partial class en_EndpointSearch : System.Web.UI.Page
{

    #region Devx Session Key Settings
    const string PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09";

    protected int GridPageSize
    {
        get
        {
            if (Session[PageSizeSessionKey] == null)
                return grid.SettingsPager.PageSize;
            return (int)Session[PageSizeSessionKey];
        }
        set { Session[PageSizeSessionKey] = value; }
    }

    const string PageSizeSession = "ed5e843d-cff7-47a7-815e-832923f7fb10";

    #endregion

    #region GridPage
    protected int GridPage
    {
        get
        {
            if (Session[PageSizeSession] == null)
                return grid2.SettingsPager.PageSize;
            return (int)Session[PageSizeSession];
        }
        set { Session[PageSizeSession] = value; }
    }
    #endregion

    #region Private Data members

    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    DataRow[] filtrs = null;
    DataTable dtSel = null;
    DataTable dtDisp = null;
    DataSet DSdisp = null;
    ArrayList rmlist = null;

    DataSet ds = null;
    public String confID = "";
    public String duration = "";
    public String Parentframe = "";
    public String IsTelespresence = ""; //FB 2400
    public bool IsTeleEP = false; //FB 2400
    String outXML = "";
    String outXMLDept = "";
    String tzone = "";
    protected String format = "MM/dd/yyyy";
    String tformat = "hh:mm tt";
    StringReader txtrd = null;
    String roomEdit = "N";
    protected String favRooms = "";
    public bool isAddTerminal = false;
    public Int32 profileCount = 0;
    private Hashtable endpointList = null;
    private String DrpValue = "";
    String eptxmlPath = ""; //FB 2361
    XmlTextReader txtrd1 = null;
    protected int Cloud = 0;//FB 2262 //FB 2599

    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (obj == null)
            obj = new myVRMNet.NETFunctions();
        obj.AccessandURLConformityCheck("EndpointSearch.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

        log = new ns_Logger.Logger();
        String stDate = "";
        String enDate = "";

        try
        {
            grid.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
            grid.Settings.ShowVerticalScrollBar = true;
            grid.Settings.VerticalScrollableHeight = 450;

            grid2.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
            grid2.Settings.ShowVerticalScrollBar = true;
            grid2.Settings.VerticalScrollableHeight = 450;

            grid.SettingsPager.PageSize = GridPageSize;
            grid.Templates.PagerBar = new CustomPagerBarTemplate();

            grid2.SettingsPager.PageSize = GridPage;
            grid2.Templates.PagerBar = new CustomPagerBarTemplate2();

            eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["EptXmlPath"].ToString();

            //FB 2274 Starts
            string crossEPxml;
            if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString() != "0")
            {
                if (Session["multisiloOrganizationID"].ToString() != "11")
                {
                    crossEPxml = "Organizations/Org_" + Session["multisiloOrganizationID"].ToString() + "/Endpoints/Endpoints.xml";
                    eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + crossEPxml;
                }
            }
            //FB 2274 Ends

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

            if (Request.QueryString["t"] != null)
                if (Request.QueryString["t"].ToUpper().Equals("TC"))
                {
                    isAddTerminal = true;
                }

            if (hdnEptID.Value != "")
                changeView();

			if (!isAddTerminal)
            {
                grid.Columns[grid.Columns.Count - 2].Visible = false;
                Session["multisiloOrganizationID"] = null;     //FB 2274
            }

            if (Request.QueryString["frm"] != null)
                if (Request.QueryString["frm"].ToString() != "")
                    Parentframe = Request.QueryString["frm"].ToString().Trim();

            //FB 2400 Starts
            if (Request.QueryString["isRoomTel"] != null)
            {
                if (Request.QueryString["isRoomTel"].ToString() != "")
                    IsTelespresence = Request.QueryString["isRoomTel"].ToString().Trim();
                IsTeleEP = true;
            }
            else
                IsTeleEP = false;
            //FB 2400 Ends

            if (Session["DtDisp"] != null)
            {
                grid.DataSource = (DataTable)Session["DtDisp"];
                grid2.DataSource = (DataTable)Session["DtDisp"];
            }

            if (Session["systemTimezoneID"] != null)
            {
                if (Session["systemTimezoneID"].ToString() != "")
                    tzone = Session["systemTimezoneID"].ToString();
            }
            //FB 2599 Start //FB 2717 Vidyo Integration Start
            /*if (Session["Cloud"] != null) //FB 2262
            {
                if (Session["Cloud"].ToString() != "")
                    Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
            }*/
            //FB 2599 End //FB 2717 Vidyo Integration End
            if (Request.QueryString["DrpValue"] != null)
            {
                if (Request.QueryString["DrpValue"].ToString().Trim() != "")
                    DrpValue = Request.QueryString["DrpValue"].ToString().Trim();
            }
           
            if(hdnEptID.Value == "")
                //FB 2426 Start
                if (File.Exists(eptxmlPath))
                    File.Delete(eptxmlPath); 
                //FB 2426 End
            if (Cloud == 1) //FB 2262
            {
                lblViewType.Visible = false;
                DrpDwnListView.Visible = false;
            }
            GetData();

            if (!IsPostBack)
            {
                obj.BindAddressType(lstAddressType);
                obj.BindBridges(lstBridges);
                obj.BindVideoEquipment(lstVideoEquipment);
                obj.BindLineRate(lstLineRate);
                obj.BindVideoProtocols(lstVideoProtocol);
                obj.BindDialingOptions(lstConnectionType);

                ChangeCalendarDate(null, null);

            }
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            LblError.Text = "Endpoint search error: " + ex.Message;
            log.Trace(ex.StackTrace + "Endpoint search error : " + ex.Message);
        }
    }
    #endregion

    #region ChangeCalendarDate

    protected void ChangeCalendarDate(Object sender, EventArgs e)
    {
        String selQuery = "";
        String mediaQry = "";
        String stateQuery = "";
        String avalRoomIDs = "";
        String avValue = "";
        String avItemQueryAnd = "";
        String avItemQueryOr = "";
        String rmName = "";
        String deptQuery = "";
        XmlDocument deptdoc = null;
        DataView dv = null;
        try
        {
            LblError.Visible = false;
            LblError.Text = "";

            if (hdnDelEndpointID.Value != "")
                btnDeleteEndpoint_Click();

            if (hdnName.Value == "1")
            {
                selQuery = ((selQuery == "") ? " EndpointName like '*" + TxtNameSearch.Text.Trim() + "*'" : selQuery += " and EndpointName like '*" + TxtNameSearch.Text.Trim() + "*'");

            }
            else if (hdnProfileName.Value == "1")
            {
                selQuery = ((selQuery == "") ? " ProfileName like '*" + TxtProfileName.Text.Trim() + "*'" : selQuery += " and ProfileName like '*" + TxtProfileName.Text.Trim() + "*'");

            }
            else
            {
                if (lstBridges.SelectedValue != "-1" && lstBridges.SelectedValue != "")
                {
                    if (selQuery == "")
                        selQuery += " Bridge = " + lstBridges.SelectedValue;
                    else
                        selQuery += " and Bridge = " + lstBridges.SelectedValue;
                }

                if (lstAddressType.SelectedValue != "-1" && lstAddressType.SelectedValue != "")
                {
                    if (selQuery == "")
                        selQuery += " AddressType = " + lstAddressType.SelectedValue;
                    else
                        selQuery += " and AddressType = " + lstAddressType.SelectedValue;
                }

                if (lstVideoEquipment.SelectedValue != "-1" && lstVideoEquipment.SelectedValue != "")
                {
                    if (selQuery == "")
                        selQuery += " VideoEquipment = " + lstVideoEquipment.SelectedValue;
                    else
                        selQuery += " and VideoEquipment = " + lstVideoEquipment.SelectedValue;
                }

                if (ChkOutsideNw.Checked)
                {
                    if (selQuery == "")
                        selQuery += " IsOutside = 1";
                    else
                        selQuery += " and IsOutside = 1";
                }

                if (ChkTelnetAPI.Checked)
                {
                    if (selQuery == "")
                        selQuery += " TelnetAPI = 1";
                    else
                        selQuery += " and TelnetAPI = 1";
                }
                if (IsTeleEP) //FB 2400
                {
                    if (selQuery == "")
                        selQuery += " isTelePresence = " + IsTelespresence;
                    else
                        selQuery += " and isTelePresence = " + IsTelespresence;
                }
            }
            //FB 2426 Start
            if(selQuery == "")
            selQuery += " Extendpoint = 0";
            else
                selQuery += " and Extendpoint = 0";
            //FB 2426 End

            if (ds.Tables.Count > 0)
            {
                filtrs = ds.Tables[0].Select(selQuery);

                dtDisp = ds.Tables[0].Clone();


                if (filtrs != null)
                {
                    foreach (DataRow dr in filtrs)
                    {
                        endpointList = new Hashtable();
                        // endpointList.Add(dr["EndpointID"], dr["EndpointName"]);

                        // endpointList.Add(dr["EndpointName"]);

                        //LoadProfile(dr["EndpointID"].ToString());
                        dr["TotalRecords"] = Convert.ToString(profileCount);

                        try
                        {
                            dr["VideoEquipment"] = lstVideoEquipment.Items.FindByValue(dr["VideoEquipment"].ToString()).Text;
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        try
                        {
                            String connType = dr["ConnectionType"].ToString();
                            if (connType.Equals("0"))
                                connType = "2";
                            dr["ConnectionType"] = lstConnectionType.Items.FindByValue(connType).Text;
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        try
                        {
                            if (!dr["LineRate"].ToString().Equals("-1"))
                                dr["LineRate"] = lstLineRate.Items.FindByValue(dr["LineRate"].ToString().Trim()).Text;
                            else
                                dr["LineRate"] = "";
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        try
                        {
                            if (!dr["AddressType"].ToString().Equals("-1"))
                                dr["AddressType"] = lstAddressType.Items.FindByValue(dr["AddressType"].ToString()).Text;
                            else
                                dr["AddressType"] = "";
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        try
                        {
                            //dr["Bridge"] = node.SelectSingleNode("Bridge").InnerText;
                            if (!dr["Bridge"].ToString().Equals("-1"))
                                dr["BridgeName"] = lstBridges.Items.FindByValue(dr["Bridge"].ToString().Trim()).Text;
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        try
                        {
                            if (!dr["DefaultProtocol"].ToString().Equals("-1"))
                                dr["DefaultProtocol"] = lstVideoProtocol.Items.FindByValue(dr["DefaultProtocol"].ToString().Trim()).Text;
                            else
                                dr["DefaultProtocol"] = "";
                        }
                        catch (Exception ex)
                        {
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        //dr["Address"] = node.SelectSingleNode("Address").InnerText.Trim();
                        if (dr["IsOutside"].ToString().Trim().Equals("1"))
                            dr["IsOutside"] = "Yes";
                        else
                            dr["IsOutside"] = "No";

                        //lblTtlEndpoints.Text = dr["TotalRecords"].ToString();
                        //dr["DefaultProfileName"] = node.SelectSingleNode("ProfileName").InnerText.Trim();

                        dtDisp.ImportRow(dr);
                    }
                }

                dv = new DataView(dtDisp);

                dv.Sort = "EndpointName ASC";

                if (Session["DtDisp"] != null)
                    Session["DtDisp"] = dv.ToTable();
                else
                    Session.Add("DtDisp", dv.ToTable());

                grid.DataSource = (DataTable)Session["DtDisp"];
                grid.DataBind();

                grid2.DataSource = (DataTable)Session["DtDisp"];
                grid2.DataBind();
            }
            else
            {
                grid.DataSource = null;
                grid.DataBind();

                grid2.DataSource = null;
                grid2.DataBind();

                lblTtlEndpoints.Text = "0";
                lblTempLic.Text = Session["EndPoints"].ToString();
            }

            changeView();

        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            LblError.Text = "Endpoint search error: " + ex.Message;
            log.Trace(ex.StackTrace + "Endpoint search error : " + ex.Message);
        }
    }

    #endregion

    #region Gridview HTML Controls
    protected void ASPxGridView1_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
            {
                if (e.KeyValue != null)
                {
                    ImageButton imgRoomList = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "imgRoomList") as ImageButton;
                    if (imgRoomList != null)
                        imgRoomList.Attributes.Add("onclick", "javascript:return fnViewRoom(this,'" + e.KeyValue.ToString() + "');");

                    Label lblIsTelepresence = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "lblIsTelepresence") as Label;
                    Label lblBridgeID = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "lblBridgeID") as Label;
                    ASPxLabel lblBrID = (ASPxLabel)grid.FindRowTemplateControl(e.VisibleIndex, "lblBrID");

                    HyperLink bridgrDetail = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "btnViewBridgeDetails") as HyperLink;
                    if (bridgrDetail != null)
                        bridgrDetail.Attributes.Add("onclick", "javascript:ViewBridgeDetails('" + lblBrID.Text + "');return false;");

                    HyperLink lblDel = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "DeleteEndpoint") as HyperLink;
                    if (lblDel != null)
                    {
                        lblDel.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this endpoint?')");
                        lblDel.Attributes.Add("onclick", "Javascript:DeleteEndpoint('" + e.KeyValue.ToString() + "')");
                    }

                    HyperLink lblEdit = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "Editendpoint") as HyperLink;
                    if (lblEdit != null)
                        lblEdit.Attributes.Add("onclick", "Javascript:EditEndpoint('" + e.KeyValue.ToString() + "')");

                    HyperLink selectEP = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "SelectEP") as HyperLink;
                    selectEP.Attributes.Add("onclick", "Javascript:AddTerminal('" + e.KeyValue.ToString() + "','" + Session["ConfID"] + "')");
                    DropDownList dropProfile = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "lstProfiles") as DropDownList;
                    Label txtPtrofile = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "txtProfile") as Label;
                    //FB 2599 Start //FB 2717 Starts
                    //if (Cloud == 1 || Session["admin"].ToString() == "3") //FB 2262 //FB 2670
					if (Session["admin"].ToString() == "3") //FB 2262 //FB 2670
                    {
                        /*if (Cloud == 1)
                        {
                            lblEdit.Enabled = false;
                            lblEdit.Attributes.Remove("onClick");
                            lblEdit.Style.Add("cursor", "default");
                            lblEdit.ForeColor = System.Drawing.Color.Gray;
                        }*/
                        //else //FB 2717 End
                            lblEdit.Text = "View";

                        //lblDel.Enabled = false;
                        //lblDel.Attributes.Remove("onClick");
                        //lblDel.Style.Add("cursor", "default");
                        //lblDel.ForeColor = System.Drawing.Color.Gray;
                            lblDel.Visible = false;

                    }
                    else
                    {
                        lblDel.ForeColor = System.Drawing.Color.Red;
                    }
                    //FB 2599 End
                    if (dropProfile != null)
                        if (Session["ProfileID"] != null)
                            Session["ProfileID"] = "";

                    if (isAddTerminal)
                    {
                        lblDel.Attributes.Add("style", "display:none");
                        lblEdit.Attributes.Add("style", "display:none");
                        selectEP.Attributes.Add("onclick", "Javascript:AddTerminal('" + e.KeyValue.ToString() + "','" + Session["ConfID"] + "')");
                        if (dropProfile != null)
                        {
                            DataTable dt = null; // LoadProfile(e.KeyValue.ToString());
                            dropProfile.DataSource = dt;
                            dropProfile.DataBind();
                            Session.Add("ProfileID", dropProfile.SelectedValue);
                            dropProfile.Attributes.Add("style", "display:block");
                            txtPtrofile.Visible = true;
                        }
                    }
                    else
                    {
                        selectEP.Attributes.Add("style", "display:none");
                        if (dropProfile != null)
                            dropProfile.Attributes.Add("style", "display:none");
                        if (txtPtrofile != null)
                            txtPtrofile.Visible = false;
                    }

                    HyperLink addEP = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "AddEP") as HyperLink;
                    if (Parentframe == "frmMainroom")
                    {
                        lblDel.Attributes.Add("style", "display:none");
                        lblEdit.Attributes.Add("style", "display:none");
                        if (dropProfile != null)
                            dropProfile.Attributes.Add("style", "display:none");
                        selectEP.Attributes.Add("style", "display:none");

                        //FB 2400 start
                        string strTel = "0";
                        if (lblIsTelepresence != null)
                            strTel = lblIsTelepresence.Text;

                        if (strTel.Trim() == "" && IsTelespresence.Trim() != "")
                            strTel = IsTelespresence;

                        addEP.Attributes.Add("onclick", "Javascript:ClosePopup('" + e.KeyValue.ToString() + "','" + strTel + "')");
                        //FB 2400 end
                        Session.Add("RoomEP", e.KeyValue.ToString());
                    }
                    else
                        addEP.Attributes.Add("style", "display:none");
                }
            }
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            LblError.Text = "Endpoint search error: " + ex.Message;
            log.Trace(ex.StackTrace + " Endpoint search error : " + ex.Message);
        }
    }
    #endregion

    #region GetData
    public void GetData()
    {
        XmlDocument endPoints = null; //FB 2361

        try
        {
            //FB 2594 Starts
            string isPublicEP = "0";
            if (Session["EnablePublicRooms"].ToString() == "1")
                isPublicEP = "1";
            //FB 2594 Ends
            String inXML = "";
            inXML += "<EndpointDetails>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
            inXML += "  <PublicEndpoint>" + isPublicEP + "</PublicEndpoint>"; //FB 2594
            inXML += "</EndpointDetails>";

            //FB 2361 - Start
            if (!File.Exists(eptxmlPath))
            {
                outXML = obj.CallMyVRMServer("GetAllEndpoints", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    endPoints = new XmlDocument();
                    endPoints.LoadXml(outXML);
                    endPoints.Save(eptxmlPath);
                    endPoints = null;
                }
            }
          
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            
            XmlDocument xmld = new XmlDocument();
            MemoryStream ms = null;
            StreamReader sr = null;
            String strXml = "";

            if (File.Exists(eptxmlPath))
            {
                if (obj.WaitForFile(eptxmlPath))
                {
                    txtrd1 = new XmlTextReader(eptxmlPath);
                    txtrd1.Read();
                    ds = new DataSet();
                    ds.ReadXml(txtrd1);

                    ms = new MemoryStream();
                    ds.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    strXml = sr.ReadToEnd();
                    sr.Close();
                    ms.Close();

                    txtrd1.Close();
                    txtrd1 = null;
                }
            }

            if (strXml != "")
                xmld.LoadXml(strXml);
            //FB 2361 - End

            XmlNode nde = xmld.SelectSingleNode("EndpointDetails/Endpoint/TotalRecords");

            if (nde != null)
                lblTtlEndpoints.Text = nde.InnerText;
            //FB 2594 Starts
            nde = xmld.SelectSingleNode("EndpointDetails/Endpoint/TotalPublicEPRecords");
            lblTtlPublicEP.Text = "0";
            if (nde != null)
                lblTtlPublicEP.Text = nde.InnerText;

            lblTtlPublicEP.Visible = false;
            lblpublEP.Visible = false;
            if (Session["EnablePublicRooms"].ToString() == "1")
            {
                lblTtlPublicEP.Visible = true;
                lblpublEP.Visible = true;
            }

            //FB 2594 Ends

            if (lblTtlEndpoints.Text != "" && Session["EndPoints"] != null)
                lblTempLic.Text = (Int32.Parse(Session["EndPoints"].ToString()) - Int32.Parse(lblTtlEndpoints.Text)).ToString();
            else
                lblTempLic.Text = Session["EndPoints"].ToString();
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            LblError.Text = "Endpoint search error: " + ex.Message;
            log.Trace(ex.StackTrace + " Endpoint search error : " + ex.Message);
        }
    }
    #endregion

    #region Delete Endpoint - Submit Button Event Handler
    /// <summary>
    /// To Delete Endpoint - Delete Click Button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnDeleteEndpoint_Click()
    {
        log = new ns_Logger.Logger();
        obj = new myVRMNet.NETFunctions();

        try
        {            
            if (hdnDelEndpointID.Value != "")
            {
                String inXML = "";
                inXML += "<DeleteEndpoint>";
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndPointID>" + hdnDelEndpointID.Value + "</EndPointID>";
                inXML += "</DeleteEndpoint>";
                String outXML = obj.CallMyVRMServer("DeleteEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                hdnDelEndpointID.Value = "";
                if (outXML.IndexOf("<error>") >= 0)
                {
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    String message = "";
                    XmlNodeList nodes = xmldoc.SelectNodes("//error");
                    if (nodes.Item(0).SelectSingleNode("message") != null)
                        if (nodes.Item(0).SelectSingleNode("message").InnerText != "")
                            message = nodes.Item(0).SelectSingleNode("message").InnerText;
                        else
                            message = outXML;

                    LblError.Text = message;
                    LblError.Visible = true;
                    return;
                }

                Session.Remove("EndpointXML");
                rmlist = null;

                if (File.Exists(eptxmlPath))
                    File.Delete(eptxmlPath); 

                GetData();
            }
        }
        catch (System.Threading.ThreadAbortException){}
        catch (Exception ex)
        {
            LblError.Visible = true;
            LblError.Text = "Endpoint search error: " + ex.Message;
            log.Trace(ex.StackTrace + " Endpoint search error : " + ex.Message);
        }
    }

    #endregion

    #region Grid_CustomCallback

    protected void Grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        GridPageSize = int.Parse(e.Parameters);

        grid.SettingsPager.PageSize = GridPageSize;
        grid.DataBind();
    }

    #endregion

    #region Grid2_CustomCallback

    protected void Grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        GridPage = int.Parse(e.Parameters);

        grid2.SettingsPager.PageSize = GridPage;
        grid2.DataBind();
    }

    #endregion

    #region Grid_DataBound

    protected void Grid_DataBound(object sender, EventArgs e)
    {
        grid.JSProperties["cpPageCount"] = grid.PageCount;
    }

    #endregion

    #region Grid2_DataBound

    protected void Grid2_DataBound(object sender, EventArgs e)
    {

        grid2.JSProperties["cpPageCount"] = grid2.PageCount;
    }

    #endregion

    #region CustomPagerBarTemplate
    public class CustomPagerBarTemplate : ITemplate
    {
        ASPxGridView grid;
        enum PageBarButtonType { First, Prev, Next, Last }

        protected ASPxGridView Grid { get { return grid; } }

        public void InstantiateIn(Control container)
        {
            myVRMNet.NETFunctions obja = new myVRMNet.NETFunctions();
            this.grid = (ASPxGridView)((GridViewPagerBarTemplateContainer)container).Grid;
            Table table = new Table();
            container.Controls.Add(table);
            TableRow row = new TableRow();
            table.Rows.Add(row);
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.First), "First", "pageBarFirstButton_Click");
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Prev), "Prev", "pageBarPrevButton_Click");
            AddLiteralCell(row.Cells, "Page");
            AddTextBoxCell(row.Cells);
            AddLiteralCell(row.Cells, string.Format("of {0}", grid.PageCount));
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Next), "Next", "pageBarNextButton_Click");
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Last), "Last", "pageBarLastButton_Click");
            AddSpacerCell(row.Cells);
			//FB 2361
            //AddLiteralCell(row.Cells, obja.GetTranslatedText("Records per page") + ":"); 
            //AddComboBoxCell(row.Cells);
        }
        void AddButtonCell(TableCellCollection cells, bool enabled, string text, string clickHandlerName)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            ASPxButton button = new ASPxButton();
            cell.Controls.Add(button);
            button.Text = text;
            button.AutoPostBack = false;
            button.UseSubmitBehavior = false;
            button.Enabled = enabled;
            if (enabled)
                button.ClientSideEvents.Click = clickHandlerName;
        }
        void AddTextBoxCell(TableCellCollection cells)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            ASPxTextBox textBox = new ASPxTextBox();
            cell.Controls.Add(textBox);
            textBox.Width = 30;
            int pageNumber = grid.PageIndex + 1;
            textBox.JSProperties["cpText"] = pageNumber;
            textBox.ClientSideEvents.Init = "pageBarTextBox_Init";
            textBox.ClientSideEvents.ValueChanged = "pageBarTextBox_ValueChanged";
            textBox.ClientSideEvents.KeyPress = "pageBarTextBox_KeyPress";
        }
        void AddComboBoxCell(TableCellCollection cells)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            DropDownList comboBox = new DropDownList();
            cell.Controls.Add(comboBox);
            comboBox.Width = 60;
            comboBox.BorderWidth = 1;   //Edited for FF
            comboBox.BorderColor = System.Drawing.Color.Gray;  //Edited for FF
            comboBox.Items.Add(new ListItem("5"));
            comboBox.Items.Add(new ListItem("10"));
            comboBox.Items.Add(new ListItem("20"));
            comboBox.Items.Add(new ListItem("30"));
            comboBox.Items.Add(new ListItem("40"));
            comboBox.Items.Add(new ListItem("50"));
            comboBox.Items.Add(new ListItem("100"));
            comboBox.SelectedValue = grid.SettingsPager.PageSize.ToString();
            comboBox.Attributes.Add("onchange", "javascript:pagerBarComboBox_SelectedIndexChanged(this)");
        }
        void AddLiteralCell(TableCellCollection cells, string text)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            cell.Text = text;
            cell.Wrap = false;
        }
        void AddSpacerCell(TableCellCollection cells)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            cell.Width = Unit.Percentage(100);
        }
        bool IsButtonEnabled(PageBarButtonType type)
        {
            if (grid.PageIndex == 0)
            {
                if (type == PageBarButtonType.First || type == PageBarButtonType.Prev)
                    return false;
            }
            if (grid.PageIndex == grid.PageCount - 1)
            {
                if (type == PageBarButtonType.Next || type == PageBarButtonType.Last)
                    return false;
            }
            return true;
        }
    }
    #endregion

    #region CustomPagerBarTemplate
    public class CustomPagerBarTemplate2 : ITemplate
    {
        ASPxGridView grid;
        enum PageBarButtonType { First, Prev, Next, Last }

        protected ASPxGridView Grid { get { return grid; } }

        public void InstantiateIn(Control container)
        {
            myVRMNet.NETFunctions obja = new myVRMNet.NETFunctions();
            this.grid = (ASPxGridView)((GridViewPagerBarTemplateContainer)container).Grid;
            Table table = new Table();
            container.Controls.Add(table);
            TableRow row = new TableRow();
            table.Rows.Add(row);
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.First), "First", "pageBarFirstButton_Click");
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Prev), "Prev", "pageBarPrevButton_Click");
            AddLiteralCell(row.Cells, "Page");
            AddTextBoxCell(row.Cells);
            AddLiteralCell(row.Cells, string.Format("of {0}", grid.PageCount));
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Next), "Next", "pageBarNextButton_Click");
            AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Last), "Last", "pageBarLastButton_Click");
            AddSpacerCell(row.Cells);
            AddLiteralCell(row.Cells, obja.GetTranslatedText("Records per page") + ":");
            AddComboBoxCell(row.Cells);
        }
        void AddButtonCell(TableCellCollection cells, bool enabled, string text, string clickHandlerName)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            ASPxButton button = new ASPxButton();
            cell.Controls.Add(button);
            button.Text = text;
            button.AutoPostBack = false;
            button.UseSubmitBehavior = false;
            button.Enabled = enabled;
            if (enabled)
                button.ClientSideEvents.Click = clickHandlerName;
        }
        void AddTextBoxCell(TableCellCollection cells)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            ASPxTextBox textBox = new ASPxTextBox();
            cell.Controls.Add(textBox);
            textBox.Width = 30;
            int pageNumber = grid.PageIndex + 1;
            textBox.JSProperties["cpText"] = pageNumber;
            textBox.ClientSideEvents.Init = "pageBarTextBox_Init";
            textBox.ClientSideEvents.ValueChanged = "pageBarTextBox_ValueChanged";
            textBox.ClientSideEvents.KeyPress = "pageBarTextBox_KeyPress";
        }
        void AddComboBoxCell(TableCellCollection cells)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            DropDownList comboBox = new DropDownList();
            cell.Controls.Add(comboBox);
            comboBox.Width = 60;
            comboBox.BorderWidth = 1;   //Edited for FF
            comboBox.BorderColor = System.Drawing.Color.Gray;  //Edited for FF
            comboBox.Items.Add(new ListItem("5"));
            comboBox.Items.Add(new ListItem("10"));
            comboBox.Items.Add(new ListItem("20"));
            comboBox.Items.Add(new ListItem("30"));
            comboBox.Items.Add(new ListItem("40"));
            comboBox.Items.Add(new ListItem("50"));
            comboBox.Items.Add(new ListItem("100"));
            comboBox.SelectedValue = grid.SettingsPager.PageSize.ToString();
            comboBox.Attributes.Add("onchange", "javascript:pagerBarComboBox_SelectedIndexChanged(this)");
        }
        void AddLiteralCell(TableCellCollection cells, string text)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            cell.Text = text;
            cell.Wrap = false;
        }
        void AddSpacerCell(TableCellCollection cells)
        {
            TableCell cell = new TableCell();
            cells.Add(cell);
            cell.Width = Unit.Percentage(100);
        }
        bool IsButtonEnabled(PageBarButtonType type)
        {
            if (grid.PageIndex == 0)
            {
                if (type == PageBarButtonType.First || type == PageBarButtonType.Prev)
                    return false;
            }
            if (grid.PageIndex == grid.PageCount - 1)
            {
                if (type == PageBarButtonType.Next || type == PageBarButtonType.Last)
                    return false;
            }
            return true;
        }
    }
    #endregion

    #region ChangeView

    private void changeView()
    {
        try
        {
            DetailsView.Attributes.Add("style", "display:none");
            ListView.Attributes.Add("style", "display:none");

            Session.Add("DrpListView", DrpDwnListView.SelectedValue);

            if (DrpDwnListView.SelectedValue == "2")
                DetailsView.Attributes.Add("style", "display:block");
            else
                ListView.Attributes.Add("style", "display:block");

        }
        catch (Exception ex)
        { }
    }

    #endregion

    #region Load Profile
    public DataTable LoadProfile(String endpointID)
    {
        DataTable dt = new DataTable();

        try
        {
            String inXML = "";
            inXML += "<EndpointDetails>";
            inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
            inXML += "  <EndpointID>" + endpointID + "</EndpointID>";
            inXML += "</EndpointDetails>";

            if (obj == null)
                obj = new myVRMNet.NETFunctions();

            String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            outXML = outXML.Replace(" % ", "&lt;br/&gt;");//FB 1886
            XmlTextReader xtr;
            DataSet ds = new DataSet();
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
            profileCount = nodes.Count;
            foreach (XmlNode node in nodes)
            {
                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                ds.ReadXml(xtr, XmlReadMode.InferSchema);
            }

            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            LblError.Text = "Endpoint search error: " + ex.Message;
            log.Trace(ex.StackTrace + " Endpoint search error : " + ex.Message);
        }
        return dt;
    }
    #endregion

    //FB 2361
    #region GetRoomDetails  

    protected void GetRoomDetails(object sender, EventArgs e)
    {
        log = new ns_Logger.Logger();
        obj = new myVRMNet.NETFunctions();

        try
        {
            if (hdnEptID.Value != "")
            {
                String inXML = "";
                inXML += "<GetRoomDetails>";
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndPointID>" + hdnEptID.Value + "</EndPointID>";
                inXML += "</GetRoomDetails>";
                String outXML = obj.CallMyVRMServer("GetRoomDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                hdnEptID.Value = "";
                String rmNames = "";
                if (outXML.IndexOf("<error>") >= 0)
                {
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    String message = "";
                    XmlNodeList nodes = xmldoc.SelectNodes("//error");
                    if (nodes.Item(0).SelectSingleNode("message") != null)
                        if (nodes.Item(0).SelectSingleNode("message").InnerText != "")
                            message = nodes.Item(0).SelectSingleNode("message").InnerText;
                        else
                            message = outXML;

                    LblError.Text = message;
                    LblError.Visible = true;
                    return;
                }
                else
                {
                    XmlDocument xmld = new XmlDocument();
                    xmld.LoadXml(outXML);

                    XmlNodeList nodes = xmld.SelectNodes("GetRoomDetails/Room");
                    Int32 i = 0;
                    String rmname = "";
                    foreach (XmlNode node in nodes)
                    {
                        i = i + 1;
                        rmname = i.ToString() + ". " + node.SelectSingleNode("Name").InnerText;

                        if (rmNames == "")
                            rmNames = rmname;
                        else
                            rmNames += "\n" + rmname;
                    }
                }
                
                MoreInfoPopup.ShowOnPageLoad = true;
                MoreInfoPopup.PopupHorizontalAlign = DevExpress.Web.ASPxClasses.PopupHorizontalAlign.WindowCenter;
                MoreInfoPopup.PopupVerticalAlign = DevExpress.Web.ASPxClasses.PopupVerticalAlign.WindowCenter;
                MoreInfoPopup.HeaderText = "Room(s)";

                if (rmNames == "")
                    rmNames = "No Rooms";

                txtMemo.Text = rmNames;

                changeView();                
            }
        }
        catch (System.Threading.ThreadAbortException) { }
        catch (Exception ex)
        {
            LblError.Visible = true;
            LblError.Text = "GetRoomDetails error: " + ex.Message;
            log.Trace(ex.StackTrace + " GetRoomDetails error : " + ex.Message);
        }
    }

    #endregion

}