﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>	
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_EventLog" %>
<%@ Register TagPrefix="mbcbb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.ComboBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

<%--fnConfirm--%>
<script type="text/javascript">
  var servertoday = new Date();
  
    function fnConfirm()
    {
        var blnret = true;
        var pass = "";
        var args = fnConfirm.arguments;
        var purgeType = "";
        
        if(document.getElementById("hdnPurgeData").value == "" || document.getElementById("hdnPurgeData").value == "0")  // FB 1725
        {
            alert("Please select the type of 'What to Purge?'");
            blnret = false;
        }
        purgeType = document.getElementById("hdnPurgeType").value;
        
        if(purgeType == "")    // FB 1725
        {
            alert("Please select the type of 'When to Purge?'");
            blnret =  false;
        }
        else
        {
           purgeType =  document.getElementById("hdnPurgeType").value
           
           if(purgeType == "5")
           {
               var purgeNowDummy = document.getElementById('<%=PurgeNowDummy.ClientID%>');
               if(purgeNowDummy)
               {
                    var txtpwd = document.getElementById('<%=txtPwd.ClientID%>'); 
               
                    purgeNowDummy.click();
                    if(txtpwd)
                        txtpwd.focus();
                    
                    blnret = false;
               }
           }
        }
       
        return blnret;
    }
    
   </script>
 
 <%--fnShow--%>
 
   <script type="text/javascript">

    function fnShow() {
    
    
        var args = fnShow.arguments;        
        var lbltext = document.getElementById('<%=lblText.ClientID%>');
        var confName = "";
        var lblVal = "";

        var timeofDay = document.getElementById('hdntformat').value; //FB 2588
        
        if( "<%=clientName %>" == "MOJ")
            confName = "Hearings";
        else
            confName = "Conferences";
        
         switch (args[0])
        {
            case "D":
                document.getElementById("hdnPurgeType").value = "1";
                lblVal = "at "+ timeofDay +" automatically on daily basis.";
                break;
            case "W":
                document.getElementById("hdnPurgeType").value = "2";
                lblVal = "every Friday at "+ timeofDay +" automatically.";
                break;
            case "M":
                document.getElementById("hdnPurgeType").value = "3";
                lblVal = "every Month End at "+ timeofDay +" automatically.";
                break;
            case "N":    
                document.getElementById("hdnPurgeType").value = "4";
                lblVal = "";
                break;
            case "R":
                document.getElementById("hdnPurgeType").value = "5";
                lblVal = "";
                break;
        }
        //Edited For FF..
        if(lblVal != "")
            
            lbltext.innerHTML = "Purge Completed " + confName + " " + lblVal;
        else
            lbltext.innerHTML = "";
          
    }
</script>
 
 <%--fnDisplay--%>
   <script type="text/javascript">

    function fnDisplay()
    {
        var args = fnDisplay.arguments;
        var reVal
       
        switch(args[0])
        {
            case 'WS':
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "None";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "None";     
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "block";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "block";
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "none";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "none";  
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "none";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "none";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "none";  
                      
                   
                   
                   document.getElementById("WSysInfoCell").className = "cellbackcolor";
                   document.getElementById("WAppLogInfoCell").className = "";
                   document.getElementById("DmyVRMLogInfoCell").className = "";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "";
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                break;
            case 'WA':
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "block";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "block"; 
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "None";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "None";  
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "none";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "none";  
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "block";    
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "none";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "none";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "none"; 
                   
                   document.getElementById("WSysInfoCell").className = "";
                   document.getElementById("WAppLogInfoCell").className = "cellbackcolor";
                   document.getElementById("DmyVRMLogInfoCell").className = "";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "";
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                   
                break;
            case 'MA':
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "block";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "block"; 
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "None";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "None";     
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "None";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "None";    
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "block";  
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "none";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "none";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "none"; 
                   
                   document.getElementById("WSysInfoCell").className = "";
                   document.getElementById("WAppLogInfoCell").className = "";
                   document.getElementById("DmyVRMLogInfoCell").className = "cellbackcolor";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "";
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                break;
            case 'AI':
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "None";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "None"; 
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "None";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "None";     
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "None";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "None";    
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "None";
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "None";    
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "Block";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "Block";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "Block";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "Block"; 
                   
                   document.getElementById("WSysInfoCell").className = "";
                   document.getElementById("WAppLogInfoCell").className = "";
                   document.getElementById("DmyVRMLogInfoCell").className = "";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "cellbackcolor";
                   
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                break;
            case 'DS':
                if(document.getElementById("DBInfoRow"))
                   document.getElementById("DBInfoRow").style.display = "block";       
                if(document.getElementById("DataPurgeRow"))
                   document.getElementById("DataPurgeRow").style.display = "None"; 
                
                 document.getElementById("DSysInfoCell").className = "cellbackcolor";
                 document.getElementById("DPurgeCell").className = "";
                 document.getElementById("hdnDisplayValue").value = args[0];
                 
                  var errLabel = document.getElementById('errLabel');
                  if(errLabel)            
                       errLabel.style.display = 'None';

                reVal = false;
                break;
            case 'DP':
                if(document.getElementById("DBInfoRow"))
                   document.getElementById("DBInfoRow").style.display = "None";       
                if(document.getElementById("DataPurgeRow"))
                   document.getElementById("DataPurgeRow").style.display = "block"; 
                 
                 document.getElementById("DSysInfoCell").className = "";
                 document.getElementById("DPurgeCell").className = "cellbackcolor";
                
                 document.getElementById("hdnDisplayValue").value = args[0];
                 
                 reVal = false;
                break;
            case 'DA':
                break;
            case 'PC':
                var lbltext = document.getElementById('<%=lblText.ClientID%>');
                if(document.getElementById("hdnPurgeData"))
                    document.getElementById("hdnPurgeData").value = '1';
                
                if(document.getElementById('<%=Daily.ClientID%>'))
                    document.getElementById('<%=Daily.ClientID%>').disabled = false;
                    
                if(document.getElementById('<%=Weekly.ClientID%>'))
                    document.getElementById('<%=Weekly.ClientID%>').disabled = false;
                
                if(document.getElementById('<%=Monthly.ClientID%>'))
                    document.getElementById('<%=Monthly.ClientID%>').disabled = false;
                 
                if(document.getElementById('<%=RNow.ClientID%>'))                
                    document.getElementById('<%=RNow.ClientID%>').checked = false;                     
                    
                if(document.getElementById('<%=RemoveType.ClientID%>') && document.getElementById("hdnPurgeType").value == "5") 
                {
                    document.getElementById('<%=RemoveType.ClientID%>').checked = true;                    
                    document.getElementById('<%=RemoveType.ClientID%>').disabled = false;                   
                    document.getElementById("hdnPurgeType").value = '4';                    
                    lbltext.innerHTML = "";//Edited For FF...
                }                    
                   
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = true;
                break;
            case 'PA':
            case 'PD':
                var lbltext = document.getElementById('<%=lblText.ClientID%>');               
                
                if(document.getElementById('<%=RNow.ClientID%>'))
                {
                    document.getElementById('<%=RNow.ClientID%>').checked = true;   
                    document.getElementById("hdnPurgeType").value = '5';
                    lbltext.innerHTML = "";//Edited For FF...
                } 
                
                if(document.getElementById('<%=Daily.ClientID%>'))
                {
                    document.getElementById('<%=Daily.ClientID%>').disabled = true;
                    document.getElementById('<%=Daily.ClientID%>').checked = false;                    
                    lbltext.innerHTML = "";//Edited For FF...
                }   
                
                if(document.getElementById('<%=Weekly.ClientID%>'))
                {
                    document.getElementById('<%=Weekly.ClientID%>').disabled = true;
                    document.getElementById('<%=Weekly.ClientID%>').checked = false;
                    lbltext.innerHTML = "";//Edited For FF...
                }   
                
                if(document.getElementById('<%=Monthly.ClientID%>'))
                {
                    document.getElementById('<%=Monthly.ClientID%>').disabled = true;
                    document.getElementById('<%=Monthly.ClientID%>').checked = false;
                    lbltext.innerHTML = "";//Edited For FF...
                }
                
                
                if(document.getElementById('<%=RemoveType.ClientID%>'))
                {
                    document.getElementById('<%=RemoveType.ClientID%>').disabled = true;
                    document.getElementById('<%=RemoveType.ClientID%>').checked = false;                    
                    lbltext.innerHTML = "";//Edited For FF...
                } 
                
                if(document.getElementById("hdnPurgeData"))
                {
                    if(args[0] == "PA")
                        document.getElementById("hdnPurgeData").value = '2';
                    else if(args[0] == "PD")
                        document.getElementById("hdnPurgeData").value = '3';
                }
                
                document.getElementById("hdnDisplayValue").value = args[0];
                
                reVal = true;
                break;
           case 'EL':
                if(document.getElementById("LogSetRow"))
                   document.getElementById("LogSetRow").style.display = "Block"; 
                if(document.getElementById("SearchLogRow"))
                   document.getElementById("SearchLogRow").style.display = "None"; 
                if(document.getElementById("RetrieveLogRow"))
                   document.getElementById("RetrieveLogRow").style.display = "None"; 
                   
               document.getElementById("ELogCell").className = "cellbackcolor";
               document.getElementById("ESearchLogCell").className = "";
               document.getElementById("ERetrieveCell").className = "";
                   
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = false;
                break;
           case 'ES':
                if(document.getElementById("LogSetRow"))
                   document.getElementById("LogSetRow").style.display = "None"; 
                if(document.getElementById("SearchLogRow"))
                   document.getElementById("SearchLogRow").style.display = "Block"; 
                if(document.getElementById("RetrieveLogRow"))
                   document.getElementById("RetrieveLogRow").style.display = "None"; 
                   
                if(document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text'))
                    document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text').style.width = "75px";
                if(document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text'))
                    document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text').style.width = "75px";
                
                document.getElementById("ELogCell").className = "";
                document.getElementById("ESearchLogCell").className = "cellbackcolor";
                document.getElementById("ERetrieveCell").className = "";
           
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = false;                
                break;                
           case 'ER':
                if(document.getElementById("LogSetRow"))
                   document.getElementById("LogSetRow").style.display = "None"; 
                if(document.getElementById("SearchLogRow"))
                   document.getElementById("SearchLogRow").style.display = "None"; 
                if(document.getElementById("RetrieveLogRow"))
                   document.getElementById("RetrieveLogRow").style.display = "Block"; 
                
                document.getElementById("ELogCell").className = "";
                document.getElementById("ESearchLogCell").className = "";
                document.getElementById("ERetrieveCell").className = "cellbackcolor";   
                
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = false;
                break;
        }
        
        if(args.length > 1 )
         {
            if(args[1] == 'N')
                reVal = '';
         }  
         else
            return reVal;
    }
</script>
 
   <script type="text/javascript">
    function deleteLog(moduleName, deleteAll)
    {
        form1.action = "EventLog.aspx?m=" + moduleName + "&d=" + deleteAll;//FB 2027(DeleteModuleLog)
        form1.submit();
    }
    
    function fnTabValue()
    {
        document.getElementById('hdnValue').value= fnTabValue.arguments[0];
    }
    </script>
    
 <%--OnChanged--%>
 
   <script type="text/javascript">
    function OnChanged(sender, args)	
    {	    
        var errLabel = document.getElementById('errLabel');
        if(errLabel)
            errLabel.style.display = 'None';
        
        sender.get_clientStateField().value = sender.saveClientState();	
        var activeIndex = sender.get_activeTabIndex();
        
        document.getElementById('hdnActiveIndex').value = activeIndex;
        
        fnMaintainDesign();
    }	
    
    function fnMaintainDesign()
    {
        var activeIndex = document.getElementById('hdnActiveIndex').value;
        if(activeIndex == 0)
        {
            if(document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text'))
                document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text').style.width = "75px";
            if(document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text'))
                document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text').style.width = "75px";
                
            fnDisplay(document.getElementById('hdnDisplayValue').value);
        }
        else if(activeIndex == 1)
        {
            if(document.getElementById('hdnDisplayValue').value == 'WA')
                fnDisplay('WA');
            else if(document.getElementById('hdnDisplayValue').value == 'MA')
                fnDisplay('MA');
            else if(document.getElementById('hdnDisplayValue').value == 'AI')
                fnDisplay('AI');
            else
                fnDisplay('WS');
        }
        else if(activeIndex == 2)
        {
            var hdnval = document.getElementById('hdnDisplayValue').value;
            
            if(hdnval == 'DP' || hdnval == 'PA' || hdnval == 'PC' || hdnval == 'PD')
            {
                fnDisplay('DP','N');
                
                if(hdnval != 'DP' && hdnval != 'PC')
                    document.getElementById("hdnPurgeData").value = '2';
                
                if(document.getElementById("hdnPurgeData"))
                {
                    if(document.getElementById("hdnPurgeData").value == "1")
                    {
                        if(document.getElementById("DCompletedConf"))
                            document.getElementById("DCompletedConf").checked = true;
                            
                        fnDisplay('PC');
                    }
                    else
                        fnDisplay(hdnval);
                }  
            } 
            else
                fnDisplay('DS');
        }
    }
  </script>
  
 <%--fnCheck--%>
   <script type="text/javascript">  
    function fnCheck()
    {
        var txtpwd = document.getElementById('<%=txtPwd.ClientID%>');      
        var btnClose = document.getElementById('<%=Close.ClientID%>');  
        var errLabel = document.getElementById('errLabel');
        if(errLabel)            
           errLabel.style.display = 'None';    
          
        var pass;
        var blnret;
        
        var lbltext = document.getElementById('<%=lblText.ClientID%>');
        
        lbltext.innerText = "";
        
         if(txtpwd)
            pass = txtpwd.value;
        
        if(Trim(pass) == "")
        {
            alert("Please enter the Password");
            blnret = false;
        }        
        else
        {
            document.getElementById('hdnPass').value = pass;
            blnret = true;
        }
        
        if(blnret)
        {
            if(document.getElementById("hdnPurgeData").value == '1')
            {
                if( "<%=clientName %>" == "MOJ")
                   blnret = confirm('Are you sure to delete the past Hearings?');
                else
                   blnret = confirm('Are you sure to delete the past Conferences?');
            }
            else if(document.getElementById("hdnPurgeData").value == '2')
            {
                if( "<%=clientName %>" == "MOJ")
                   blnret = confirm('Are you sure to delete all Hearings?');
                else
                   blnret = confirm('Are you sure to delete all Conferences?');
            }
            else if(document.getElementById("hdnPurgeData").value == '3')
            {
               blnret = confirm('Are you sure to delete all Data?');
            }
        }
      
        return blnret;
    }
 </script>
 
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<head runat="server">
    <title>myVRM Event Logs</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%><%--FB 1947--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="DiagnosticsScriptManager" runat="server" LoadScriptsBeforeUI="false">
                 </asp:ScriptManager>
    <input type="hidden" runat="server" id="hdnSeviceValue" />
    <input type="hidden" runat="server" id="hdnPurgeData" />
    <input type="hidden" runat="server" id="hdnPurgeType" />
    <input type="hidden" runat="server" id="hdnPass" />
    <input type="hidden" runat="server" id="hdnDisplayValue" />
    <input type="hidden" runat="server" id="hdnExportValue" />
    <input type="hidden" runat="server" id="hdnActiveIndex" />
    <input type="hidden" runat="server" id="hdnValue" />
    <input type="hidden" runat="server" id="hdnBlock" />
    <input type="hidden" runat="server" id="hdnEmailServiceStatus" />
    <input type="hidden" runat="server" id="hdnReminderServiceStatus" /><%--FB 1926--%>
    <input type="hidden" runat="server" id="hdntformat" /><%--FB 2588--%>
    <div>
       <table width="80%" align="center" border="0">
         <tr>
            <td>
                <h3>Diagnostics</h3> 
            </td>
         </tr>
         <tr>
              <td align="center">
                  <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
              </td>
          </tr>
          <tr>
              <td>
                <asp:HiddenField ID="activeTab" runat="server" />
                <ajax:TabContainer ID="DiagnosticsTabs" runat="server" OnClientActiveTabChanged="OnChanged">  
                    <ajax:TabPanel ID="EventLogView"  runat="server">
                        <HeaderTemplate>
                           <font class="blackblodtext"> Event Logs</font>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0" border="0"  width="100%">
                                <tr valign="top">
                                    <td class="blackblodtext" nowrap valign="top" width="10%" >
                                        <table cellspacing="0" cellpadding="5" width="100%">
                                             <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td id="ELogCell" class="cellbackcolor" nowrap>
                                                    <asp:LinkButton runat="server" ID="ELog" onfocus='this.blur()' GroupName="OptWebServer" CssClass="SelectedItem"  Text ="Log Settings"  OnClientClick="Javascript:return fnDisplay('EL')"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="ESearchLogCell" nowrap>
                                                    <asp:LinkButton runat="server" ID="ESearchLog" onfocus='this.blur()' GroupName="OptWebServer" CssClass="SelectedItem" Text="Search Logs"  OnClientClick="Javascript:return fnDisplay('ES')"></asp:LinkButton> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="ERetrieveCell" nowrap>   
                                                    <asp:LinkButton runat="server" ID="ERetrieve" onfocus='this.blur()' GroupName="OptWebServer" CssClass="SelectedItem"  Text="Retrieve Log File" OnClientClick="Javascript:return fnDisplay('ER')"></asp:LinkButton> 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <div class="rbroundbox">
	                                        <div class="rbtop">
	                                            <div></div></div>
		                                            <div class="rbcontent" >
                                                        <table cellpadding="0" cellspacing="0" width="90%" align="center">
                                                            <tr id="LogSetRow">
                                                                <td width="100%" valign=top>
                                                                    <input type="hidden" name="SetLog" value="SetLogPreferences">
                                                                    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">         	
                                                                        <tr>
                                                                            <td width="98%" align=left>
                                                                                <SPAN class=subtitleblueblodtext>Log Settings</SPAN><br>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height=200 valign=top align="center">
                                                                                <asp:Table ID="tblLogPref" runat="server" CssClass="tableBody" Width="100%" CellPadding="4" CellSpacing="0">
                                                                                </asp:Table> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align=center>
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                  <td align="center">
                                                                                    <input type="reset" name="SetlogReset" value="Reset" class="altMedium0BlueButtonFormat">
                                                                                  </td>
                                                                                  <td width="5%"></td>
                                                                                  <td align="center">
                                                                                      <asp:Button ID="btnSubmit" runat="server" Text="Update" CssClass="altMedium0BlueButtonFormat" />
                                                                                  </td>
                                                                                </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="SearchLogRow" style="display:none;">
                                                                <td valign=top width="1000px"> <%--FB 2906 --%>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="90%">
                                                                        <tr>
                                                                            <td align=left>
                                                                                <SPAN class=subtitleblueblodtext>Search Logs</SPAN><br>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >
                                                                                <table border="0" width="90%" cellspacing="0" cellpadding="8" align="left">  <%--FB 2906--%>
                                                                                    <tr>
                                                                                        <td align="left" valign=center width="10%"><%--FB 2579 Start--%><%--FB 2906--%>
                                                                                            <label class="blackblodtext">From</label>
                                                                                        </td>
                                                                                        <td valign=center nowrap="nowrap"><%--FB 2906--%>
                                                                                            <asp:TextBox ID="startDate" runat="server" CssClass="altText" ValidationGroup="Search"></asp:TextBox>
                                                                                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1" style="cursor: pointer;vertical-align:middle;" title="Date selector"
                                                                                                onclick="return showCalendar('<%=startDate.ClientID%>', 'cal_trigger1', -1, '<%=format%>');" />
                                                                                            <asp:RequiredFieldValidator ID="startDateRequired" runat="server" ControlToValidate="startDate"
                                                                                                Display="Dynamic" ErrorMessage="* Required" Font-Size="Smaller" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>								
                                                                                            <br />
                                                                                            
                                                                                            <asp:RegularExpressionValidator ID="startDateRegEx" runat="server" ErrorMessage="Invalid Date <%=format%>" Display="Dynamic" Font-Size="Smaller" ControlToValidate="startDate" SetFocusOnError="true" ValidationExpression="^(((((((0?[13578])|(1[02]))[\-/]?((0?[1-9])|([12]\d)|(3[01])))|(((0?[469])|(11))[\-/]?((0?[1-9])|([12]\d)|(30)))|((0?2)[\-/]?((0?[1-9])|(1\d)|(2[0-8]))))[\-/]?(((19)|(20))?([\d][\d]))))|((0?2)[\.\-/]?(29)[\-/]?(((19)|(20))?(([02468][048])|([13579][26])))))$|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" ValidationGroup="Search"></asp:RegularExpressionValidator>
                                                                                        </td>
                                                                                        <td class="blackblodtext"  width="15%">Time</td>
                                                                                        <td align="left" valign=center><%--FB 2906--%>
                                                                                            <table CELLSPACING="0" CELLPADDING="0" BORDER="0">
                                                                                                <tr>
	                                                                                                <td valign=center align="left"> <%--FB 2906--%>
		                                                                                                <mbcbb:ComboBox ID="startTimeCombo" runat=server style="width:auto"  CssClass="altSelectFormat"  CausesValidation=true ValidationGroup="Search"><%--Edited For FF--%>
                                                                                                        </mbcbb:ComboBox>
                                                                                                        <asp:RequiredFieldValidator ID="startTimeReq" runat="server" ErrorMessage="* Required" ControlToValidate="startTimeCombo" Font-Size="Smaller" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator ID="startTimeRegEx" runat="server" Font-Size="Smaller" ControlToValidate="startTimeCombo" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RegularExpressionValidator>
                                                                                                       &nbsp;&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="blackblodtext">To</td>
                                                                                        <td valign=center align="left">   <%--FB 2906--%>                                                                         
                                                                                            <asp:TextBox ID="endDate" runat="server" CssClass="altText" ValidationGroup="Search"></asp:TextBox>
                                                                                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger2" style="cursor: pointer;vertical-align:middle;" title="Date selector"
                                                                                                onclick="return showCalendar('<%=endDate.ClientID%>', 'cal_trigger2', -1, '<%=format%>');" />
                                                                                            <asp:RequiredFieldValidator ID="endDateRequired" runat="server" ControlToValidate="endDate"
                                                                                                Display="Dynamic" ErrorMessage="* Required" Font-Size="Smaller" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator></td>								
                                                                                        
                                                                                        <td class="blackblodtext">Time</td>
                                                                                        <td valign=center align="left"><%--FB 2906--%>
                                                                                            <table CELLSPACING="0" CELLPADDING="0" BORDER="0">
                                                                                                <tr>
        	                                                                                        <td valign=center align="left" style="height: 24px"><%--FB 2906--%>
		                                                                                                <mbcbb:ComboBox ID="endTimeCombo" runat=server  CssClass="altSelectFormat" style="width:auto" CausesValidation="true" ValidationGroup="Search"><%--Edited For FF--%>
		                                                                                                </mbcbb:ComboBox>
                                                                                                        <asp:RequiredFieldValidator ID="endTimeReq" runat="server" ErrorMessage="* Required" ControlToValidate="endTimeCombo" Font-Size="Smaller" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator ID="endTimeRegEx" runat="server" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" Font-Size="Smaller" ControlToValidate="endTimeCombo" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RegularExpressionValidator>
                                                                                                        &nbsp;&nbsp;
	                                                                                                </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="blackblodtext"> Module</td>
                                                                                        <td align="left" valign=center><%--FB 2906--%>
                                                                                            <asp:DropDownList CssClass="altText" ID="moduleType" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td class="blackblodtext" nowrap> Error Code</td>
                                                                                        <td valign=center align=left><%--FB 2906--%>
                                                                                            <asp:TextBox ID="txtErrorCode" runat="server" CssClass="altText"></asp:TextBox>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtErrorCode" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>                                                                        
                                                                                        <td valign=center colspan=2 align=left><%--FB 2906--%>
                                                                                            <label class="blackblodtext">  Error Message</label><br>  
                                                                                            <asp:TextBox ID="ErrorMessage" runat="server" MaxLength="4000" Rows="3" TextMode="MultiLine" CssClass="altText"></asp:TextBox>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="ErrorMessage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                                                        </td>
                                                                                         <td align=left  valign=center colspan=2 class="blackblodtext"><%--FB 2906--%>
                                                                                            <label class="blackblodtext"> Event</label>
                                                                                            <asp:CustomValidator ID="validateSeverity" runat="server" ErrorMessage="Please select at least one option" Font-Size="Smaller" OnServerValidate="validateSeverity_ServerValidate" Display="Dynamic" SetFocusOnError="true" AutoPostBack="true" ValidationGroup="Search"></asp:CustomValidator>
                                                                                            <table border="0" cellpadding="3" cellspacing="0" width="300px"><%--Edited For FF--%>
                                                                                                 <tr >
                                                                                                    <td class="blackblodtext">
                                                                                                      <asp:CheckBoxList ID="Severity" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" Height="50px" >
                                                                                                          </asp:CheckBoxList>
                                                                                                     </td>			    
                                                                                                 </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                  </table>
                                                                               </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align=center>
                                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                          <td align="center">
                                                                                            <input type="reset" name="HardwareissuelogSubmit1" value="Reset" class="altMedium0BlueButtonFormat">
                                                                                          </td>
                                                                                          <td width="5%"></td>
                                                                                          <td align="center">
                                                                                              <asp:Button ID="searchSubmit" runat="server" Text="Search" CssClass="altMedium0BlueButtonFormat" OnClick="searchSubmit_Click" ValidationGroup="Search" />
                                                                                          </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                         </table>
                                                                    </td>
                                                                </tr>
                                                            <tr id="RetrieveLogRow" style="display:none;">
                                                                <td  style="height:150px;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="90%" >
                                                                        <tr>
                                                                            <td width=98% align=left><SPAN class=subtitleblueblodtext>Retrieve Log File</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >
                                                                             <table border="0" width="90%" cellspacing="0" cellpadding="8" align="right">
                                                                                <tr>
                                                                                    <td align="left" class="blackblodtext" width="700px"> <%--Edited For FF--%>
	                                                                                    Select a log type and a corresponding date<%--FB 2579 End--%>
                                                                                        <asp:RadioButtonList ID="Servicesnames" runat="server">
                                                                                            <asp:ListItem   Text="Email Log" Value="0" Selected=True></asp:ListItem>
                                                                                            <asp:ListItem  Text="MCU/Launch Log" Value="1"></asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%--Edited For FF--%>
	                                                                                    <td>                                                                            
                                                                                            <asp:TextBox ID="logDate" runat="server" CssClass="altText"></asp:TextBox>
                                                                                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerLog" style="cursor: pointer;vertical-align:middle;" title="Date selector"
                                                                                                onclick="return showCalendar('<%=logDate.ClientID%>', 'cal_triggerLog', -1, '<%=format%>');" />
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center">
	                                                                                    <table align="center">
	                                                                                        <tr>
		                                                                                        <td>
			                                                                                        <input type="reset" name="HardwareissuelogSubmit2" value="Reset" class="altMedium0BlueButtonFormat">
		                                                                                        </td>
		                                                                                        <td align="center">
                                                                                                    <asp:Button ID="logRetSubmit" runat="server" Text="Retrieve" CssClass="altMedium0BlueButtonFormat" OnClick="logRetSubmit_Click"/>
			                                                                                    </td>
	                                                                                        </tr>
	                                                                                    </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                </td>
                                                            </tr>	
                                                        </table>
                                                    </div>
	                                             <div class="rbbot">
	                                        <div></div></div>
                                       </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                     </ajax:TabPanel>
                    <ajax:TabPanel ID="WebServerView" runat="server">   
                        <HeaderTemplate>
                            <font class="blackblodtext">Web Server</font>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td valign="top" style="width:10%;">
                                        <table cellspacing="0" cellpadding="5">
                                            <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="WSysInfoCell" class="cellbackcolor" >
                                                    <asp:LinkButton ID="WSysInfo" onfocus='this.blur()'  Text="System Information" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('WS')" ></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="WAppLogInfoCell">
                                                    <asp:LinkButton ID="WAppLogInfo" onfocus='this.blur()' Text="Application Log" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('WA')"></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="DmyVRMLogInfoCell" >
                                                    <asp:LinkButton ID="DmyVRMLogInfo" onfocus='this.blur()' Text="myVRM Alerts Log" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('MA')"></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="WmyVRMassemblyInfoCell" >
                                                    <asp:LinkButton ID="WmyVRMassemblyInfo" onfocus='this.blur()' Text="myVRM Assembly" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('AI')"></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                       </table>
                                    </td>
                                    <td valign="top">
                                       <div class="rbroundbox">
	                                        <div class="rbtop"><div></div></div>
		                                        <div class="rbcontent" >
                                                    <table width="100%" border="0">
                                                        <tr id="WebLogRow1"  style="display:none;">
                                                            <td>
                                                                <table width="600px"><%--Edited For FF--%>
                                                                    <tr >
                                                                        <td class="subtitleblueblodtext" align="center" nowrap><%--Edited For FF--%>                    
                                                                            Application Log
                                                                           <asp:Button ID="ApExport" runat="server" Text="Export" OnClientClick="javascript:document.getElementById('hdnExportValue').value='2'" CssClass="altMedium0BlueButtonFormat" />
                                                                            <asp:Button ID="ApExportAll" runat="server" Text="Export All Log" OnClientClick="javascript:document.getElementById('hdnExportValue').value='1'" CssClass="altMedium0BlueButtonFormat" Width="150px"/> 
                                                                        </td>       
                                                                        
                                                                    </tr>
                                                                </table>
                                                             </td>      
                                                        </tr>          
                                                        <tr id="WebLogRow2" style="display:none;">
                                                            <td>
                                                                <asp:DataGrid id="LogGrid" Width="100%" runat="server" AllowPaging="True" PageSize="10" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Right"
                                                                PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                                GridLines="Both" CellPadding="3" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                      <asp:BoundColumn HeaderText="Type" ItemStyle-Width="12%" />
                                                                      <asp:BoundColumn HeaderText="Description" />
                                                                      <asp:BoundColumn HeaderText="Date/Time" ItemStyle-Width="15%"/>
                                                                      <asp:BoundColumn HeaderText="Source" ItemStyle-Width="10%"/>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                        <tr id="WebSysInfoRow1" style="display:block;" align="center"><%--Edited For FF--%>
                                                            <td style="width:100%;" class="subtitleblueblodtext"  align="center">
                                                                System Information
                                                            </td>
                                                        </tr>
                                                        <tr id="WebSysInfoRow2" style="display:block;">
                                                            <td style="width:100%;">
                                                                <asp:Table ID="SystemInfoTable" runat="server" Width="700px" ></asp:Table><%--Edited For FF--%>
                                                            </td>
                                                        </tr>
                                                        <tr id="myVRMWebLogRow1"  style="display:None;">
                                                            <td class="subtitleblueblodtext" >                    
                                                                <table width="700px" border="0"><%--Edited For FF--%>
                                                                    <tr >
                                                                        <td class="subtitleblueblodtext" >                    
                                                                            myVRM Alerts Log
                                                                        </td>       
                                                                        <td align="right"  nowrap>
                                                                            <asp:Button ID="AlExport" runat="server" Text="Export" OnClientClick="javascript:document.getElementById('hdnExportValue').value='3'" CssClass="altMedium0BlueButtonFormat" />
                                                                            <asp:Button ID="AlExportAll" runat="server" Text="Export All Log" OnClientClick="javascript:document.getElementById('hdnExportValue').value='1'" CssClass="altMedium0BlueButtonFormat" Width="150px" />
                                                                        </td> 
                                                                    </tr>
                                                                </table>
                                                            </td>                
                                                        </tr>          
                                                        <tr id="myVRMWebLogRow2"  style="display:None;">
                                                            <td >
                                                                <asp:DataGrid id="myVRMLogGrid" runat="server" AllowPaging="True" PageSize="10" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Right"
                                                                PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                GridLines="Both" CellPadding="3" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                      <asp:BoundColumn HeaderText="Type" ItemStyle-Width="12%" />
                                                                      <asp:BoundColumn HeaderText="Description" ItemStyle-Width="50%" />
                                                                      <asp:BoundColumn HeaderText="Date/Time" ItemStyle-Width="15%"/>
                                                                      <asp:BoundColumn HeaderText="Source" ItemStyle-Width="10%"/>                                                                      
                                                                    </Columns>                                                                                                                                                        
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                        <tr id="myVRMassemblyRow1" style="display:None;">
                                                            <td class="subtitleblueblodtext" align="center"  width="700px"><%--Edited For FF--%>
                                                                myVRM Assemblies
                                                            </td>
                                                        </tr>
                                                        <tr id="myVRMassemblyRow2" style="display:None;">
                                                            <td width="700px"><%--Edited For FF--%>
                                                                <asp:Table ID="MyVRMassemblyTable" runat="server" Width="80%" HorizontalAlign="Center" ></asp:Table>
                                                            </td>
                                                        </tr>
                                                        <tr id="ComponentsRow1" style="display:None;">
                                                            <td class="subtitleblueblodtext" align="center"  width="700px"><%--Edited For FF--%>
                                                                Components
                                                            </td>
                                                        </tr>
                                                        <tr id="ComponentsRow2" style="display:None;">
                                                            <td width="700px"><%--Edited For FF--%>
                                                                <asp:Table ID="ComponentTable" runat="server" Width="80%"  HorizontalAlign="Center"></asp:Table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
	                                        <div class="rbbot"><div></div></div>
                                       </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel ID="DataServerView" runat="server">
                        <HeaderTemplate>
                           <font class="blackblodtext"> Database Server </font>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0">
                                <tr >
                                    <td class="blackblodtext" nowrap valign="top" width="10%" >
                                        <table cellspacing="0" cellpadding="5">
                                             <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td id="DSysInfoCell" class="cellbackcolor" nowrap>
                                                    <asp:LinkButton runat="server" ID="DSysInfo" onfocus='this.blur()' GroupName="OptWebServer" CssClass="SelectedItem"  Text ="Server Information"  OnClientClick="Javascript:return fnDisplay('DS')"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="DPurgeCell" nowrap>
                                                    <asp:LinkButton runat="server" ID="DPurge" onfocus='this.blur()' GroupName="OptWebServer" CssClass="SelectedItem" Text="Purge Config Settings"  OnClientClick="Javascript:return fnDisplay('DP')"></asp:LinkButton> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="DAppLogInfoCell" nowrap>   
                                                    <asp:LinkButton runat="server" ID="DAppLogInfo" onfocus='this.blur()' GroupName="OptWebServer" CssClass="SelectedItem" Visible="false" Text="Application Log" OnClientClick="Javascript:return fnDisplay('DA')"></asp:LinkButton> 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td  valign=top>
                                        <div class="rbroundbox">
	                                        <div class="rbtop"><div></div></div>
		                                        <div class="rbcontent" >
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr id="DataPurgeRow" style="display:none;">
                                                            <td width="50px"></td>
                                                            <td>
                                                                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="subtitleblueblodtext" colspan="2">
                                                                            What to Purge?
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="blackblodtext">
                                                                            <asp:RadioButton ID="DCompletedConf" runat="server" GroupName="DeleteType" Text="Purge Only Completed Conferences" onclick="javascript:return fnDisplay('PC')"  /> 
                                                                            <asp:RadioButton ID="DAllConf" runat="server" GroupName="DeleteType" Text="Purge All Conferences" onclick="javascript:return fnDisplay('PA')" /> 
                                                                            <asp:RadioButton ID="DAllData" runat="server" GroupName="DeleteType" Text="Purge All Data" Visible="true" onclick="javascript:return fnDisplay('PD')" /> 
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td class="subtitleblueblodtext" valign="baseline" align="left">
                                                                                        <asp:Button ID="PurgeNow" Visible="false" runat="server" CssClass="altShortBlueButtonFormat" OnClientClick="javascript:return fnConfirm('N');" Text="Purge Now" />
                                                                                        <asp:Button ID="PurgeNowDummy" runat="server" style="display:none;"  />
                                                                                        <ajax:ModalPopupExtender ID="PurgePopUp" runat="server" TargetControlID="PurgeNowDummy" PopupControlID="BlockDiv" CancelControlID="Close"  DropShadow="false" Drag="true" OnCancelScript = "javascript: document.getElementById('DiagnosticsTabs_DataServerView_txtPwd').value=''" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
                                                                                        <div id="BlockDiv" class="modalBackground"> 
                                                                                        <div class="rbroundbox">
                                                                                    <div ><div></div></div>
                                                                                        <div class="rbcontent" >
                                                                                            <table cellpadding="8" cellspacing="1">
                                                                                                <tr>
                                                                                                    <td class="blackblodtext">
                                                                                                        Please enter your Password :
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="txtPwd" runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center" colspan="2">
                                                                                                        <asp:Button ID="PurgeNowInDiv" runat="server" CssClass="altShortBlueButtonFormat" OnClientClick="javascript:return fnCheck();" Text="Purge Now" />
                                                                                                        <asp:Button ID="Close" Text="Close" runat="server" OnClientClick="javascript: document.getElementById('DiagnosticsTabs_DataServerView_txtPwd').value=''" CssClass="altShortBlueButtonFormat" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>  
                                                                                              </div>
                                                                                        <div ><div></div></div>
                                                                                      </div> 
                                                                                        </div>
                                                                                    </td>
                                                                                 </tr>
                                                                            </table>
                                                                        </td>          
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="left">
                                                                            <hr style="width:85%;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="subtitleblueblodtext" colspan="2">
                                                                           When to Purge?
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow1" runat="server">
                                                                        <td class="blackblodtext" colspan="2">
                                                                            Scheduled :&nbsp;&nbsp;
                                                                            <asp:RadioButton ID="RemoveType" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('N')" /> None
                                                                            <asp:RadioButton ID="RNow" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('R')" /> Now
                                                                            <asp:RadioButton ID="Daily" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('D')" /> Daily
                                                                            <asp:RadioButton ID="Weekly" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('W')" /> Weekly
                                                                            <asp:RadioButton ID="Monthly" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('M')" /> Monthly
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow2" runat="server">
                                                                        <td></td>
                                                                        <td>
                                                                            <br />
                                                                            <asp:Label ID="lblText" runat="server" CssClass="lblError"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow3" runat="server">
                                                                        <td></td>
                                                                        <td><br />
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:Button runat="server" ID="PurgeRegular"  OnClientClick="javascript:return fnConfirm();" Width="100pt"  Text="Submit" /> <%--FB 2796--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>&nbsp;<br /></td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow4" runat="server">
                                                                        <td></td>
                                                                        <td class="blackblodtext">
                                                                            Last Updated on:&nbsp;&nbsp;
                                                                            <asp:Label ID="lblUpdateOn" runat="server" CssClass="blackblodtext"></asp:Label>
                                                                         </td>
                                                                     </tr>
                                                                </table>                                            
                                                            </td>
                                                        </tr>
                                                        <tr id="DBInfoRow" >
                                                            <td colspan="2" >
                                                                <table width="100%" border="0">
                                                                    <tr>
                                                                        <td class="subtitleblueblodtext" align="center"  >
                                                                            Database Server Information
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td >
                                                                            <asp:Table ID="SQLServerInfoTable" runat="server" Width="700px" CellSpacing="1" CellPadding="3" ></asp:Table><%--Edited For FF--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                 </div>
	                                        <div class="rbbot"><div></div></div>
                                       </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel ID="ServiceView" runat="server">
                        <HeaderTemplate>
                            <font class="blackblodtext">Services</font> 
                        </HeaderTemplate>                        
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top" style="width:10%;">
                                        <table cellspacing="0" cellpadding="5" width="100%">
                                            <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="SStatusCell" class="cellbackcolor" >
                                                    <asp:LinkButton ID="SStatus" onfocus='this.blur()'  Text="Status of the Service" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('WS')" ></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                       </table>
                                    </td>
                                    <td valign="top" >
                                       <div class="rbroundbox">
                                        <div class="rbtop"><div></div></div>
		                                    <div class="rbcontent">
                                                <table cellpadding="5" cellspacing="0" width="100%" align="center"  border="0" style="height:130px;">
                                                    <tr><td height="25px" colspan="2"></td></tr>
                                                    <tr>
                                                        <td class="blackblodtext" style="width:50%;" align="right" nowrap>
                                                           MCU/Launch Status :
                                                        </td>
                                                        <td  >
                                                            <asp:Label runat="server" ID="ServiceStatus" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr><td height="25px" colspan="2"></td></tr>
                                                    <tr>
                                                        <td class="blackblodtext" style="width:50%;" align="right" nowrap>
                                                           Email Status :
                                                        </td>
                                                        <td  >
                                                            <asp:Label runat="server" ID="EmailServiceStatus" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr><%--FB 1926--%>
                                                        <td class="blackblodtext" style="width:50%;" align="right" nowrap>
                                                           Reminder Status :
                                                        </td>
                                                        <td  >
                                                            <asp:Label runat="server" ID="ReminderServiceStatus" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <table cellspacing="5" cellpadding ="8">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="Start" Visible =false ToolTip="Start Service" Width="40px" Height="33px" OnClientClick="javascript: document.getElementById('hdnSeviceValue').value='1'" runat="server" ImageUrl="~/en/image/Start.jpg"  />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="Pause" Visible =false ToolTip="Pause Service" Width="40px"  Height="33px" runat="server" OnClientClick="javascript: document.getElementById('hdnSeviceValue').value='2'" ImageUrl="~/en/image/Pause.jpg" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="Stop" Visible =false ToolTip="Stop Service" Width="40px" Height="33px" runat="server" ImageUrl="~/en/image/Stop.jpg" OnClientClick="javascript: document.getElementById('hdnSeviceValue').value='3'"/>
                                                                    </td>
                                                               </tr>
                                                           </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
	                                        <div class="rbbot"><div></div></div>
                                       </div>
                                     </td>
                                   </tr>
                                </table>
                        </ContentTemplate>
                    </ajax:TabPanel>
                 </ajax:TabContainer>
             </td>
          </tr>
          <tr>
            <td>
                 <asp:Button ID="ServerInfo" runat="server" OnClick="BindSQLServerInfoTable"  style="display:none" />                
            </td>
          </tr>
      </table>
    </div>
    </form>
</body>
</html>
<script type="text/javascript" src="inc/softedge.js"></script>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script type="text/javascript">
    fnMaintainDesign();
</script>