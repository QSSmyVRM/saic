<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="en_ExpressConference.ExpressConference" ValidateRequest="false" %>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"    TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<script type="text/javascript">
  var servertoday = new Date();
  var hdnCfSt;
  var dFormat;
    dFormat = "<%=format %>";
  
  var servertoday = new Date(parseInt("<%=DateTime.Today.Year%>", 10), parseInt("<%=DateTime.Today.Month%>", 10)-1, parseInt("<%=DateTime.Today.Day%>", 10),
        parseInt("<%=DateTime.Today.Hour%>", 10), parseInt("<%=DateTime.Today.Minute%>", 10), parseInt("<%=DateTime.Today.Second%>", 10)); 
 
  var  maxDuration = 24;
  if( '<%=Application["MaxConferenceDurationInHours"] %>' != "")
     maxDuration = parseInt('<%=Application["MaxConferenceDurationInHours"] %>',10); 
   
	function fnShowHideAVLink()
	{
	    var args = fnShowHideAVLink.arguments;
	    var obj = eval(document.getElementById("LnkAVExpand"));
	    
	    if(obj)
	    {
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	    }
	}
	
	function SelectAudioParty()
    {
        if(document.getElementById("txtAudioDialNo") != null)
            document.getElementById("txtAudioDialNo").value= "";
        
        if(document.getElementById("txtConfCode") != null)
            document.getElementById("txtConfCode").value = "";
        
        if(document.getElementById("txtLeaderPin") != null)
            document.getElementById("txtLeaderPin").value = "";
        
        document.getElementById("lblError2").innerHTML="";//FB 1981
        document.getElementById("lblError3").innerHTML="";//FB 1981
        document.getElementById("lblError4").innerHTML="";//FB 1981
        
        RegtxtAudioDialNo.style.display = "none";
        RegtxtConfCode.style.display = "none";
        RegtxtLeaderPin.style.display = "none";
        
        var audins = document.getElementById("lstAudioParty").value;
        if(audins == -1)
        {
            document.getElementById("hdnAudioInsID").value = "";
            document.getElementById("txtAudioDialNo").disabled = true;
            document.getElementById("txtConfCode").disabled = true;
            document.getElementById("txtLeaderPin").disabled = true;
        }
        else {

            party = audins.split("|"); 
            if(party.length > 0)
            {
                document.getElementById("hdnAudioInsID").value =  party[0];
                document.getElementById("txtAudioDialNo").disabled = false;
                document.getElementById("txtConfCode").disabled = false;
                document.getElementById("txtLeaderPin").disabled = false;            
                document.getElementById("txtAudioDialNo").value= party[2];
                document.getElementById("txtConfCode").value = party[3];
                document.getElementById("txtLeaderPin").value = party[4];
            }
        }
	}	
	
	function fnShowAVParams()
	{
	    var obj = eval(document.getElementById("trAVCommonSettings"));
	    var obj2 = eval(document.getElementById("trSelRoomsTitle")); 
	    var obj3 = eval(document.getElementById("trSelRoomsDetails")); 
	    var linkState = eval(document.getElementById("hdnAVParamState"));
	    var expandlink = eval(document.getElementById("LnkAVExpand"));
	    
	    if(linkState)
	    {
	        if(obj)
	        {
	            obj.style.display = 'none';
	            obj2.style.display = 'none'; 
	            obj3.style.display = 'none';
	            if(linkState.value == '')
	            {
	                obj.style.display = '';
	                obj2.style.display = '';
	                obj3.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Collapse';
	                }
	            }
	            else
	            {
	                obj.style.display = 'none';
	                obj2.style.display = 'none'; 
	                obj3.style.display = 'none'; 
	                
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Expand';
	                }
	            }
	        }
	     }
	     return false;
	}
	
	function fnShowHideMeetLink()
	{
	    var args = fnShowHideMeetLink.arguments;
	    var obj = eval(document.getElementById("LnkMeetExpand"));
	    
	    if(obj)
	    {
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	    }
	}
	
	function fnShowMeetPlanner()
	{
	    var obj = eval(document.getElementById("tdMeetingPlanner"));
	    var linkState = eval(document.getElementById("hdnMeetLinkSt"));
	    var expandlink = eval(document.getElementById("LnkMeetExpand"));
	    
	    if(linkState)
	    {
	        if(obj)
	        {
	            obj.style.display = 'none';
	            if(linkState.value == '')
	            {
	                obj.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Collapse';
	                }
	            }
	            else
	            {
	                obj.style.display = 'none';
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Expand';
	                }
	            }
	        }
	     }
	     return false;
	}
	
	function CheckUploadedFiles()
	{	
	    if(document.getElementById("FileUpload1") != null && document.getElementById("FileUpload2") != null && document.getElementById("FileUpload3") != null)
	    {
		    if(document.getElementById("FileUpload1").value=="" && document.getElementById("FileUpload2").value=="" && document.getElementById("FileUpload3").value=="")
	        {
	            document.getElementById("aFileUp").innerHTML="Attach Files";
	        }
	     }
	}
	
</script>

<script type="text/javascript" language="JavaScript" src="script/errorList.js"></script>

<script type="text/javascript" language="javascript1.1" src="extract.js"></script>

<script language="VBScript" src="script/lotus.vbs"></script>

<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>

<script type="text/javascript" src="script/settings2.js"></script>

<script type="text/vbscript" src="script/settings2.vbs"></script>

<script type="text/javascript" src="script/mousepos.js"></script>

<script type="text/javascript" src="script/cal-flat.js"></script>

<script type="text/javascript" src="script/calview.js"></script>

<script type="text/javascript" src="lang/calendar-en.js"></script>

<script type="text/javascript" src="script/calendar-setup.js"></script>

<script language="VBScript" src="script/outlook.vbs" type="text/vbscript"></script>

<script type="text/javascript" src="script/saveingroup.js"></script>

<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script language="javascript" type="text/javascript">
		
    function callalert(val)
    {
	    alert(val);
	    return false;
    }

    function OpenSetRoom()
    {
        var lb = document.getElementById("seltdRooms");
        if(lb != null)
        {
            for (var i=lb.options.length-1; i>=0; i--)    
            lb.options[i] = null;
        
             lb.selectedIndex = -1;
             
             lb.disabled=true;
        }
        if(document.getElementById("btnRoomSelect"))
            document.getElementById("btnRoomSelect").click();
    }
</script>

<script language="javascript" type="text/javascript">
    function fnEnableBuffer()
     {
        var chkenablebuffer = document.getElementById("chkEnableBuffer");
        
        //FB 1911
        if(document.getElementById("RecurSpec").value  != "")
            return;
    
        var chkrecurrence = document.getElementById("chkRecurrence");
     
        if(chkrecurrence && chkrecurrence.checked == true) //Recurrence enabled and buffer zone checked
        {
            var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
            var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
            var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
            var hrs = document.getElementById("RecurDurationhr").value;
            var mins = document.getElementById("RecurDurationmi").value;
            
            if(hrs == "")
                hrs = "0";
            
            if(mins == "")
                mins = "0"; 
            
            var timeMins = parseInt(hrs) * 60 ;
            timeMins = timeMins + parseInt(mins);
            endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
                
            var hh = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
            if (hh < 10)
                hh = "0" + hh;
                
            var mm = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
            if (mm < 10)
                mm = "0" + mm;
            var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
            var evntime = parseInt(hh,10);
                
            if(evntime < 12)
                evntime = evntime + 12
                           
            if("<%=Session["timeFormat"]%>" == "0")
            {
                if(ap == "AM")
                {
                    if(hh == "12")
                        document.getElementById("confEndTime_Text").value = "00:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                    
                }
                else
                {
                    if(evntime == "24")
                        document.getElementById("confEndTime_Text").value = "12:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                }
            }
            else
            {
                document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            }
                
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
        }
     
        if(chkenablebuffer && chkenablebuffer.checked) //Enable buffer zone check box checked state
        {
            document.getElementById("SDateText").innerHTML = "Conference Setup <span class='reqfldstarText'>*</span>";
            document.getElementById("EDateText").innerHTML  = "Post Conference End <span class='reqfldstarText'>*</span>";
            document.getElementById("NONRecurringConferenceDiv6").style.display = "Block";
            document.getElementById("NONRecurringConferenceDiv7").style.display = "Block";
            document.getElementById("SetupTime_Text").style.width = "75px"; 
            document.getElementById("TeardownTime_Text").style.width = "75px";
        }
        else
        {   
            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
            
            if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
            { 
                document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
            } 
              
        }
     }

     function SetRecurBuffer()
     {
        var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
        var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
        var chkenablebuffer = document.getElementById("chkEnableBuffer");
        var chkrecurrence = document.getElementById("chkRecurrence");
        
            if(chkrecurrence && chkrecurrence.checked == true)
            {
                var confstdate = '';
                confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
                var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
                var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
                var hrs = document.getElementById("RecurDurationhr").value;
                var mins = document.getElementById("RecurDurationmi").value;
            
                if(hrs == "")
                    hrs = "0";
                
                if(mins == "")
                    mins = "0"; 
            
                var timeMins = parseInt(hrs) * 60 ;
            
                timeMins = timeMins + parseInt(mins);
                endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
                
                var endTime1 = endTime;
                var month,date;
                month = endTime1.getMonth() + 1;
                date = endTime1.getDate();
                
                if(eval(endTime1.getMonth() + 1) <10)
                    month = "0" + (endTime1.getMonth() + 1);
                
                if(eval(date) <10)
                    date = "0" + endTime1.getDate();
                   
                if('<%=format%>' == 'MM/dd/yyyy')
                {
                    document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime1.getFullYear();
                    document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime1.getFullYear();
                }
                else
                {
                    document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime1.getFullYear();
                    document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime1.getFullYear();
                }
                
                var hh = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
                if (hh < 10)
                    hh = "0" + hh;
            
                var mm = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
                if (mm < 10)
                    mm = "0" + mm;
                var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
                var evntime = parseInt(hh,10);
                
                if(evntime < 12)
                    evntime = evntime + 12
                       
                if('<%=Session["timeFormat"]%>' == '0')
                {
                    if(ap == "AM")
                    {
                        if(hh == "12")
                            document.getElementById("confEndTime_Text").value = "00:" + mm ;
                        else
                            document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                        
                    }
                    else
                    {
                        if(evntime == "24")
                            document.getElementById("confEndTime_Text").value = "12:" + mm ;
                        else
                            document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                    }
                }
                else
                {
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
                }
                document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
                document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
            
                var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
                var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
                
                if(setupdate > tDate)
                    document.getElementById("SetupTime_Text").value = document.getElementById("SetupTime_Text").value;
                    
               if(sDate > setupdate)
               {
                   document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                   document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
               }
            
            }
     }
         
    function randam() { 
       var totalDigits = 4;
       var n = Math.random()*10000;
       n = Math.round(n);
       n = n.toString(); 
          var pd = ''; 
          if (totalDigits > n.length) { 
             for (i=0; i < (totalDigits-n.length); i++) { 
                pd += '0'; 
             } 
          } 
       return pd + n.toString();
    }

    function num_gen() 
    {
       var num = randam()
       if (num.indexOf(0) == 0) 
          num = num.replace(0,1)

       var pass = document.getElementById("ConferencePassword")
       var pass1 = document.getElementById("ConferencePassword2")
       if(pass != "")
         pass.value = num;
       if(pass1 != "")
         pass1.value = num;
         
    } 

    function UpdateCheckbox(obj)
    {
        if (obj.value == "")
        {
           document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = false;
           UpdatePrice(obj);
        }
        else
        {
            if (!isNaN(obj.value) && obj.value.indexOf(".") < 0)
            {
                document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = true;
                UpdatePrice(obj);
            }
            else
            {
                alert("Invalid Value.");
                obj.value = "";
            }
        }
    }
    
    function UpdatePrice(obj)
    {
        var lblPrice = document.getElementById(obj.id.substring(0, obj.id.indexOf("_dgCateringMenus")) + "_lblPrice");
        var price = parseFloat("0.00");
        var i = 2;
        
        while(document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity"))
        {
            if (i<10)
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity").value;
            }
            else
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtQuantity").value;
            }
            i++;
        }
        lblPrice.innerHTML = (price * 100.00) / 100.00;
    }

    function ShowItems(obj)
    {
        getMouseXY();
        var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
        str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY>");
        str = str.replace("</TBODY>", "</TBODY></TABLE>");
        document.getElementById("tblMenuItems").style.position = 'absolute';
        document.getElementById("tblMenuItems").style.left = mousedownX - 200;
        document.getElementById("tblMenuItems").style.top = mousedownY;
        document.getElementById("tblMenuItems").style.borderWidth = 1;
        document.getElementById("tblMenuItems").style.display="";
        document.getElementById("tblMenuItems").innerHTML = str;
    }
    
    function HideItems()
    {
       document.getElementById("tblMenuItems").style.display="none";
    }

    function ShowImage(obj)
    {
        document.getElementById("myPic").src = obj.src;
        getMouseXY();
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20;
        document.getElementById("divPic").style.top = mousedownY;
        document.getElementById("divPic").style.display="";
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }

    function CheckParty()
    {
        if (document.getElementById("ifrmPartylist"))
        {
            if (!ifrmPartylist.bfrRefresh())
            {
                DataLoading(0);
                return false;
            }
        }
        return true;
    }

function viewendpoint(val)
{
	
	url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else { // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
    	}
	}
}

function viewconflict(id, confTime, confDate, confDuration, confTZ, ConferenceName, confID)
{
    var rms = document.getElementById("selectedloc").value;
	url = "preconferenceroomchart.asp?wintype=ifr&cid=" + confID + "&cno=" + id +
		"&date=" + confDate + "&time=" + confTime + "&timeZone=" + confTZ +
		"&duration=" + confDuration + "&r=" + rms + "&n=" + ConferenceName;
	wincrm = window.open(url,'confroomchart','status=no,width=1,height=1,top=30,left=0,scrollbars=yes,resizable=yes')
	if (wincrm)
		wincrm.focus();
	else
		alert(EN_132);
}
		
function formatTime(timeText, regText)//FB 1715
{

   if("<%=Session["timeFormat"]%>" == "1")
   {    
        if (document.getElementById(timeText).value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
        {
            var a_p = "";
            var t = new Date();
            var d = new Date(t.getDay() + "/" + t.getMonth() + "/" + t.getYear() + " " + document.getElementById(timeText).value);
           
            var curr_hour = d.getHours();
            if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

            if (curr_hour == 0) curr_hour = 12;
            if (curr_hour > 12) curr_hour = curr_hour - 12;
            
            curr_hour = curr_hour + "";             
            if (curr_hour.length == 1)
               curr_hour = "0" + curr_hour;
                     
            var curr_min = d.getMinutes();
            curr_min = curr_min + "";
            if (curr_min.length == 1)
               curr_min = "0" + curr_min;

            document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;            
            document.getElementById(regText).style.display = "None"; 
            return true;            
       }
       else
       {    
            document.getElementById(regText).style.display = ""; 
            //document.getElementById(timeText).focus();
            return false;
       }
   }
    return true;
}
//FB 1716

function ChangeDuration()
{ 
    if ('<%=isEditMode%>' == "1" )    
    {        
        var duration = document.getElementById("hdnDuration").value;
        var chgVar =  document.getElementById("hdnChange").value;
              
        if(duration != '')
        {        
            var durArr = duration.split("&");
            var changeTime;
            
            var setupTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("confStartTime_Text").value);
            
            var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);
            
            var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);
           
            switch(chgVar)
            {
                case "ST":  
                    setupTime = setCDuration(setupTime,durArr[1]);                    
                    document.getElementById("SetupDate").value = getCDate(setupTime);                    
                    document.getElementById("SetupTime_Text").value = getCTime(setupTime);
                    var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
                    + document.getElementById("SetupTime_Text").value);
                    
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "SU":
                
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "TD":
                   endTime = setCDuration(endTime,durArr[2]);
                   document.getElementById("confEndDate").value = getCDate(endTime);                   
                   document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;               
            }
        }
    }  
}

//FB 1716
function setCDuration(setupTime, dura)
{
    var chTime = new Date(setupTime);
    var d = 0;
    var min = 0;
    var hh = 0, dur = 0;
    if(dura > 60)
    {   
        hh = dura / 60;
        min = dura % 60;                        
        if(min > 0)
            hh = Math.floor(hh) + 1;                        
     
        for(d = 1; d <= hh ; d++)
        {
            if(min > 0 && d == hh)
                dur = dura % 60;
            else
                dur = 60;
                
             chTime.setMinutes(chTime.getMinutes() + dur);   
        }
    }
    else if(dura > 0)        
        chTime.setTime(chTime.getTime() + (dura * 60 * 1000));
    else
        chTime;
        
   return chTime
}
//FB 1716

function getCDate(changeTime)
{
    var strDate;
    var month,date;
    month = changeTime.getMonth() + 1;
    date = changeTime.getDate();
    
    if(eval(changeTime.getMonth() + 1) <10)
        month = "0" + (changeTime.getMonth() + 1);
    
    if(eval(date) <10)
        date = "0" + changeTime.getDate();
       
    if('<%=format%>' == 'MM/dd/yyyy')    
        strDate = month + "/" + date + "/" + changeTime.getFullYear();
    else
        strDate =  date + "/" + month + "/" + changeTime.getFullYear();
    
    return strDate;
}
//FB 1716

function getCTime(changeTime)
{
    var hh = parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
    if (hh < 10)
        hh = "0" + hh;
    
    var mm = parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
    if (mm < 10)
        mm = "0" + mm;
    var ap = changeTime.toLocaleTimeString().toString().split(" ")[1];
   
    var evntime = parseInt(hh,10);
    
    if(evntime < 12 && ap == "PM")
       evntime = evntime + 12;
       
    var tiFormat =  "<%=Session["timeFormat"]%>" ;

    if(tiFormat == '0')
    {
        if(ap == "AM")                        
            strTime = (hh == "12") ? "00:" + mm : hh + ":" + mm ;
        else
        {
            if(evntime == "24")
                strTime = (hh == "12") ? "00:" + mm : evntime + ":" + mm;
            else
                strTime =  evntime + ":" + mm;
        }   
    }
    else
        strTime = hh + ":" + mm + " " + ap;
        
   return strTime;
}


function ChangeEndDate(frm)
{
       if(frm == 2)
       {
        frm = "0";
        hdnCfSt = '1';
       }

//    if (document.getElementById("Recur").value == ""  && document.getElementById("hdnRecurValue").value == "")//FB 1715
//    {
    var confstdate = '';
    if (document.getElementById("Recur").value == ""  && document.getElementById("hdnRecurValue").value == "")
    confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
    else
    confstdate = GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>');
    var confenddate = '';
    confenddate = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
    /*
    if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) )
        {
            document.getElementById("confStartDate").value = document.getElementById("confEndDate").value;
            if (frm == "0" || frm == "2")
            {
                alert("Invalid Start Date or Time. It should be greater than time current time in user preferred timezone.");
                DataLoading(0);
                return false;
            }
        }
        */
        if(frm == "0" && '<%=isEditMode%>' == "0")//FB 1716
        {
          ChangeEndDateTime(confstdate,confenddate);
        }
            
       EndDateValidation(frm,confstdate,confenddate,'E');
       ChangeDuration();//FB 1716 
//    }
   return true;
}

function DuraionCalculation()
{
    var confenddate = '';
    var confstdate = '';
    var durationMin = '';
    var totalMinutes;
    var totalDays;
    var totalHours;
    var lblconfduration = document.getElementById('<%=lblConfDuration.ClientID%>');
    
    lblconfduration.innerText = '';
    confenddate = GetDefaultDate(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value,'<%=format%>');
    confstdate = GetDefaultDate(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value,'<%=format%>');
    
    var durationMin = parseInt(Date.parse(confenddate) - Date.parse(confstdate)) /  1000  ;
    if(durationMin != '')
    {
        totalDays = Math.floor(durationMin / (24 * 60 * 60));
        totalHours = Math.floor((durationMin - (totalDays * 24 * 60 * 60)) / (60 * 60));
        totalMinutes = Math.floor((durationMin - (totalDays * 24 * 60 * 60) -  (totalHours * 60 * 60)) / 60);
    }
    
    if (Math.floor(durationMin) < 0)
        lblconfduration.innerText = "Invalid duration";
    if (totalDays > 0)
        lblconfduration.innerText = totalDays + " day(s) ";
    if (totalHours > 0)
        lblconfduration.innerText += totalHours + " hr(s) ";
    if (totalMinutes > 0)
        lblconfduration.innerText += totalMinutes + " min(s)";
}

function EndDateValidation(frm,confstdate,confenddate,msgchk)
{
    var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
    var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
    var eDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value);
    var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
    //FB 1715
    if ( (sDate >= eDate)&& (document.getElementById("confStartTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)        
    && (document.getElementById("confEndTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
         if(msgchk == 'S')
        {
            if (Date.parse(confstdate) == Date.parse(confenddate))        
            {
                if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert("End Time will be changed because it should be greater than Start Time.");  
               else if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) == Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert("End Time will be changed because it should be greater than Start Time.");              
            }    
            else
                alert("End Date will be changed because it should be equal/greater than Start Date.");
        }
            
        ChangeEndDateTime(confstdate,confenddate); 
     }
     
        var chkbuffer = document.getElementById("chkEnableBuffer");
        if (('<%=enableBufferZone%>' == "1") && (chkbuffer.checked))
        { 
              if ((setupdate < sDate) || (setupdate >= tDate) ||(setupdate > eDate) || (tDate > eDate) || (tDate < setupdate))
              {
                    //Modification for FB 1716 - Start
                    if( (setupdate < sDate) || (setupdate >= tDate) || (setupdate > eDate)|| (tDate < setupdate))
                    {
                        //if(!(setupdate >= tDate))
                        //{
                            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
                        //}
                    }
                    //Modification for FB 1716 - End 
              }
              if(tDate < setupdate)
               {
                     document.getElementById("TearDownDate").value = document.getElementById("SetupDate").value;                   
                    document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
               } 
              
                  if(hdnCfSt == '1')
                  if(setupdate < sDate)
                  {
                     alert("Start Date/time will be changed because it should not be lesser than Setup Date/time.");
                     document.getElementById("confStartDate").value = document.getElementById("SetupDate").value;
                     document.getElementById("confStartTime_Text").value = document.getElementById("SetupTime_Text").value;
                     hdnCfSt = '0';
                  }
             if(setupdate >= tDate)
              {
                    var hhT;
                    var mmT;
                    var apT;
                    var zerto;
                                        
                    if(setupdate < tDate)
                       confstdate = GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>');
                    else
                       confstdate = GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>');
                        
                    
            
            
                    var endTimeT = new Date(confstdate + " " + document.getElementById("SetupTime_Text").value); 
                    
                    
                               
                    if((setupdate > sDate))// && (document.getElementById("SetupTime_Text").value <= document.getElementById("confEndTime_Text").value))
                    {
                        endTimeT.setHours(endTimeT.getHours() + 1);
                        
                        
                        var monthT,dateT;
                        monthT = endTimeT.getMonth() + 1;
                        dateT = endTimeT.getDate();
                        
                        if(eval(endTimeT.getMonth() + 1) <10)
                            monthT = "0" + (endTimeT.getMonth() + 1);
                        
                        if(eval(dateT) <10)
                            dateT = "0" + endTimeT.getDate();
                           
                        if('<%=format%>' == 'MM/dd/yyyy')
                        {
                            document.getElementById("confEndDate").value = monthT + "/" + dateT + "/" + endTimeT.getFullYear();
                            document.getElementById("TearDownDate").value = monthT + "/" + dateT + "/" + endTimeT.getFullYear();
                        }
                        else
                        {
                            document.getElementById("confEndDate").value =  dateT + "/" + monthT + "/" + endTimeT.getFullYear();
                            document.getElementById("TearDownDate").value =  dateT + "/" + monthT + "/" + endTimeT.getFullYear();
                        }
                        
                        hhT = parseInt(endTimeT.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
                        if (hhT < 10)
                            hhT = "0" + hhT;
                        
                        mmT = parseInt(endTimeT.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
                        if (mmT < 10)
                            mmT = "0" + mmT;
                            
                        apT = endTimeT.toLocaleTimeString().toString().split(" ")[1];
                        
                        zerto = hhT + ":" + mmT + " " + apT;                
                        var evntime = parseInt(hhT,10);
                
                        if(evntime < 12 && apT == "PM")
                            evntime = evntime + 12;
                            if('<%=Session["timeFormat"]%>' == "0")
                            {
                                if(apT == "AM")
                                {
                                    if(hhT == "12")
                                        document.getElementById("confEndTime_Text").value = "00:" + mmT ;
                                    else
                                        document.getElementById("confEndTime_Text").value = hhT + ":" + mmT ;
                                    
                                }
                                else
                                {
                                    if(evntime == "24")
                                        document.getElementById("confEndTime_Text").value = "12:" + mmT ;
                                    else
                                        document.getElementById("confEndTime_Text").value = evntime + ":" + mmT ;
                                }
                                    
                            }
                            else
                            {
                                document.getElementById("confEndTime_Text").value = zerto;
                                
                            }
                        }
                   document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                   document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
              }
              var chkrecurrence = document.getElementById("chkRecurrence");    
              if(chkrecurrence != null && chkrecurrence.checked == true)
              {
                    document.getElementById("confEndDate").value = document.getElementById("TearDownDate").value;
                    document.getElementById("confEndTime_Text").value = document.getElementById("TeardownTime_Text").value;
              }
        }
        else
        {
            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                        
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
        }   
      DuraionCalculation();  
}

function ChangeEndDateTime(confstdate,confenddate)
{
      document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
         
        var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);
        
        var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1];          
        endTime.setHours(endTime.getHours() + 1);
            if('<%=client.ToString().ToUpper()%>' =="MOJ")
            endTime.setMinutes(endTime.getMinutes() - 45);
        
        var month,date;
        month = endTime.getMonth() + 1;
        date = endTime.getDate();
        
        if(eval(endTime.getMonth() + 1) <10)
            month = "0" + (endTime.getMonth() + 1);
        
        if(eval(date) <10)
            date = "0" + endTime.getDate();
        document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;//FB 1716   
        if('<%=format%>' == 'MM/dd/yyyy')
        {
            document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime.getFullYear();
        }
        else
        {
            document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime.getFullYear();
        }
        var hh = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
        if (hh < 10)
            hh = "0" + hh;
        
        var mm = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
        if (mm < 10)
            mm = "0" + mm;
        var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
        var evntime = parseInt(hh,10);
        
         if(evntime < 12 && ap == "PM")
            evntime = evntime + 12;
            
        if('<%=Session["timeFormat"]%>' == "0")
        {
            if(ap == "AM")
            {
                if(hh == "12")
                    document.getElementById("confEndTime_Text").value = "00:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                
            }
            else
            {
                if(evntime == "24")
                    document.getElementById("confEndTime_Text").value = "12:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
            }
                
        }
        else
        {
            document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        }      
}

function ChangeStartDate(frm)
{
    if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
    {
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
       /*
         if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value))
        {
            document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
            if (frm == "0") 
            {
                alert("Invalid End Date or Time. It should be greater than time current time in user preferred timezone.");
                DataLoading(0);
                return false;
            }
        }
        */
        EndDateValidation(frm,confstdate,confenddate,'S');

    }
    ChangeDuration();//FB 1716
    return true;
}

function CheckDuration()
{
    if (document.getElementById("Recur").value == "")
    if (Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value))
    {
        alert("Invalid Duration");
        return false;
    }
}

function ChangeImmediate()
{
    var t;
	var t1;
    document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
    document.getElementById("SetupTime_Text").style.width = "75px"; 
    document.getElementById("TeardownTime_Text").style.width = "75px"; 
    
    if (document.frmSettings2.Recur.value == "")
    {
        if( document.getElementById("hdnRecurValue").value == 'R')
        {
             t = "none";
             t1 = "";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "none";
        }
        else
        {
            t = "";
            t1 = "";
            Page_ValidationActive=true;
            document.getElementById("divDuration").style.display ="none";
        }
        
	    document.getElementById("NONRecurringConferenceDiv1").style.display = t1;
	    document.getElementById("NONRecurringConferenceDiv3").style.display = t1;
	    document.getElementById("NONRecurringConferenceDiv6").style.display = t1;
	    document.getElementById("NONRecurringConferenceDiv7").style.display = t1;
	    document.getElementById("NONRecurringConferenceDiv9").style.display = "None"; 
 		
 		//FB 1911
	    var args = ChangeImmediate.arguments;
	    if(args != null)
	    {
	        if(args[0] == "S")
	        {
	            document.getElementById("RecurDiv").style.display = "None"; 
	            document.frmSettings2.RecurSpec.value = '';
	            document.frmSettings2.RecurringText.value = '';	            
	        }
	    }
	    
        if ('<%=Application["recurEnable"]%>' == '0')
        {
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        }
        else if ("<%=isInstanceEdit%>" == "Y" )
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        else
        {
            document.getElementById("NONRecurringConferenceDiv8").style.display = t1;
        }
        
        if ('<%=timeZone%>' == "0" ) 
            document.getElementById("NONRecurringConferenceDiv3").style.display = "none";    
          
        document.getElementById("EndDateArea").style.display = t;
	    document.getElementById("TeardownArea").style.display = t;
	    
	    fnEnableBuffer();
    }
}


function isRecur()
{
    var t = (document.frmSettings2.Recur.value == "") ? "" : "none";
    
    if(isRecur.arguments.length > 0 || document.getElementById("hdnRecurValue").value == 'R')
    {
        if(isRecur.arguments[0] == 'R' || document.getElementById("hdnRecurValue").value == 'R')
            t = "None";
    }

    if ("<%=client.ToString().ToUpper() %>" == "MOJ")  
    {  
        document.getElementById("NONRecurringConferenceDiv6").style.display = "none"; 
	    document.getElementById("NONRecurringConferenceDiv7").style.display = "none"; 
	}
 
	document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
    document.getElementById("SetupTime_Text").style.width = "75px"; 
    document.getElementById("TeardownTime_Text").style.width = "75px"; 
    if(t == "")
    {
        document.getElementById("RecurrenceRow").style.display = 'None';
        document.getElementById("DurationRow").style.display = 'None';
        
    }
    else
    {
        document.getElementById("RecurrenceRow").style.display = 'Block';
        document.getElementById("DurationRow").style.display = 'Block';
    }
        
	if (t != "")
	{
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	}
	
	if ("<%=timeZone%>" == "0" )
      document.getElementById("NONRecurringConferenceDiv3").style.display = "none"; 
      
	document.getElementById("EndDateArea").style.display = t;
    document.getElementById("StartDateArea").style.display = t;
    document.getElementById("TeardownArea").style.display = t;
      
      return false;
}


function getYourOwnEmailList (i)
{
	if(i == 999)
	    url = "emaillist2main.aspx?t=e&frm=party2NET&fn=Setup&n=" + i;	
	else
	    url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i; 
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}

function goToCal()
{
	roomcalendarview();
}

function openRecur()
{
    //FB 1911
    var args = openRecur.arguments;
    var isarg = "0";
    if(args.length > 0)
        if(args[0] == 'S')
            isarg = "1";
            
	var chkrecurrence = document.getElementById("chkRecurrence");
	
	if(isarg == "1" && chkrecurrence.checked)
    {
        removerecur();        
        chkrecurrence.checked = false;
    }
    
    if(isarg =="0" && chkrecurrence.checked)
        document.getElementById("RecurSpec").value = "";
    
    if(chkrecurrence != null && chkrecurrence.checked == false && isarg == "0")
    {
        removerecur();
        ChangeEndDate();
    }
    else
    {
	    if (ChangeEndDate(1) && ChangeStartDate(1))
	    {
	        var st = document.getElementById("<%=lstConferenceTZ.ClientID%>");
	        var sd = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>')        
	        var stime = document.getElementById("<%=confStartTime.ClientID%>");
	        var setupTime = document.getElementById("SetupTime_Text");
	        var teardownTime = document.getElementById("TeardownTime_Text");
	        var sTime = document.getElementById("hdnSetupTime");
	        var tTime = document.getElementById("hdnTeardownTime");
	       
	         sTime.value  = setupTime.value;
	         tTime.value = teardownTime.value;
	        
	       
	       document.getElementById("EndDateArea").style.display = 'None';
	       document.getElementById("StartDateArea").style.display = 'None';
	       document.getElementById("TeardownArea").style.display = 'None';
    	   
    	   //FB 1911
	       if(isarg == "1")
           {
                showSpecialRecur();
	            top.recurWin = window.open('recurNET.aspx?pn=E&frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime + "&wintype=pop", 'recur1', 'titlebar=yes,width=650,height=563,resizable=yes,scrollbars=yes,status=yes');//buffer zone
	       }
           else
           {        
               document.getElementById("hdnRecurValue").value = 'R';	
	           document.getElementById("NONRecurringConferenceDiv1").style.display = '';
	           document.getElementById("NONRecurringConferenceDiv6").style.display = '';                
	           document.getElementById("NONRecurringConferenceDiv7").style.display = '';
	           document.getElementById("recurDIV").style.display = 'None';       
	           document.getElementById("RecurText").value = '';    
	           document.getElementById("hdnRecurValue").value = 'R';
	           isRecur();
	           initial();
	           fnShow();
	       }
	       
//            if (timeDur > (maxDuration * 60)) 
//                alert(EN_314);
		}
	}
}


//FB 1911
function visControls()
{
    if(document.getElementById("RecurSpec").value == "")
    {
	   document.getElementById("StartDateArea").style.display = '';
	   document.getElementById("TeardownArea").style.display = '';
	   document.getElementById("EndDateArea").style.display = '';
	   document.getElementById("NONRecurringConferenceDiv1").style.display = '';	   
	   //document.getElementById("NONRecurringConferenceDiv2").style.display = '';
	   document.getElementById("NONRecurringConferenceDiv6").style.display = '';
	   document.getElementById("NONRecurringConferenceDiv7").style.display = '';
	   
       document.getElementById("recurDIV").style.display = 'None';     
       document.getElementById("RecurText").value = ''; 
       
       document.getElementById("confStartTime_Text").style.width = "75px";
       document.getElementById("SetupTime_Text").style.width = "75px"; 
       document.getElementById("TeardownTime_Text").style.width = "75px";          
	}
}

function showSpecialRecur()
{
    document.getElementById("recurDIV").style.display = '';
    document.getElementById("NONRecurringConferenceDiv1").style.display = 'None';                
    document.getElementById("NONRecurringConferenceDiv6").style.display = 'None';                
    document.getElementById("NONRecurringConferenceDiv7").style.display = 'None';
    document.getElementById("EndDateArea").style.display = 'None';
    document.getElementById("StartDateArea").style.display = 'None';
    document.getElementById("TeardownArea").style.display = 'None';
    document.getElementById("NONRecurringConferenceDiv2").style.display = 'None';
    document.getElementById("RecurrenceRow").style.display = 'None';
}

function roomcalendarview()
{   
    var frm = document.getElementById("confStartDate").value;
    rn = document.getElementById("selectedloc").value; 
    rn = getListViewChecked(rn);	
    url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn + "&d=" + frm;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function roomconflict(confDate) //FB 1154 & //FB 2027 SetConference
{   
    var frm = confDate;
    rn = document.getElementById("selectedloc").value
    
    //rn = getListViewChecked(rn);	
    
    //url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn  + "&d=" + frm;
    url = "roomcalendar.aspx?f=v&hf=1&m=" + rn + "&d=" + confDate;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function CallMeetingPla()
{   

        var confTimeZone = "<%=lstConferenceTZ.SelectedValue%>";
        var sd = "<%=confStartDate.Text %>";
        var stime = "<%=confStartTime.Text %>";
        var confDateTime = sd + " " + stime;
        var roomId = document.getElementById("selectedloc").value;

        roomId = getListViewChecked(roomId); 
        rn = roomId.split(",").length - 1;
        rId = roomId.split(",").length - 1;

        if(rn==0 || (rn==1 && roomId == ', '))
        {
          alert("Please Select Atleast one Room.")
          return false;
        }
        if(rn >5)
        {
          alert("Only Five Rooms are allowed to view the Meeting Planner")
          return false;
        }
       
        url = "MeetingPlanner.aspx?";
                url += "ConferenceDateTime=" + confDateTime + "&";
                url += "ConferenceTimeZone=" + confTimeZone + "&";
                url += "RoomID=" + roomId;
    
	  window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
      return false;
          
      }

function AudioAddon()
{
    var args = AudioAddon.arguments;
    
    var drptyp = document.getElementById("CreateBy");
    var controlName = 'dgUsers_ctl';
    var ctlid = '';
    
    if(drptyp)
    {
        if(drptyp.value == "2")
        {
            if(args)
            {
                var tbl1 = document.getElementById(args[0]);
                var tbl2 = document.getElementById(args[1]);
                var drp = document.getElementById(args[2]);
                var drp2 = document.getElementById(args[3]);
                 
                var usrArr=args[2].split("_");
                var len=usrArr.length;

                if(len > 2)
                {
                    ctlid = usrArr[1];
                    ctlid = ctlid.replace(/ctl/i,'');
                }
                
                var lstAddType = document.getElementById((controlName + ctlid +'_lstAddressType'));
                var lstConType = document.getElementById((controlName + ctlid +'_lstConnectionType'));
                var txtAdd = document.getElementById((controlName + ctlid +'_txtAddress'));

                var txtaddphone = document.getElementById((controlName + ctlid +'_txtaddphone'));
                var txtconfcode = document.getElementById((controlName + ctlid +'_txtConfCode'));
                var txtleader = document.getElementById((controlName + ctlid +'_txtleaderPin'));
                
                if(drp.value == "2" && '<%=Application["EnableAudioAddOn"]%>' == "1")
                {
                    tbl1.style.display = 'none';
                    tbl2.style.display = 'block';
                    
                    if(lstAddType)
                    {
                        lstAddType.value = '1';
                    }
                    if(lstConType)
                    {
                        lstConType.value = '2';
                    }
                    if(txtAdd)
                    {
                        if(Trim(txtAdd.value) == '')
                            txtAdd.value = '127.0.0.1';
                    }
                }
                else
                {
                    if(txtaddphone)
                    {
                        if(Trim(txtaddphone.value) == '')
                        {
                            txtaddphone.value = '127.0.0.1';
                        }
                    }
                    if(txtconfcode)
                    {
                        if(Trim(txtconfcode.value) == '')
                            txtconfcode.value = '0';
                    }
                    if(txtleader)
                    {
                        if(Trim(txtleader.value) == '')
                            txtleader.value = '0';
                    }
                    tbl1.style.display = 'block';
                    tbl2.style.display = 'none';
                }
                drp2.value = drp.value;
            }
                
        }
    }
}

</script>

<%--Merging Recurrence Script Start Here--%>

<script type="text/javascript">

var openerfrm = document.frmSettings2;


function CheckDate(obj)
{    
    if (Date.parse(obj.value) < Date.parse(new Date()))
        alert("Invalid Date");
}

function fnHide()
{    
    document.getElementById("Daily").style.display = 'none';
    document.getElementById("Weekly").style.display = 'none';
    document.getElementById("Monthly").style.display = 'none';
    document.getElementById("Yearly").style.display = 'none';
    document.getElementById("Custom").style.display = 'none';
    document.getElementById("RangeRow").style.display = 'block';    
}

function fnShow()
{  
     var a = null;
    var f = document.forms[1];
    var e = f.elements["RecurType"];
    for (var i=0; i < e.length; i++)
    {
        if (e[i].checked)
        {
        a = e[i].value;
        document.frmSettings2.RecPattern.value = a;
        break;
        }
    }
    
    if(a != null)
    {
        fnHide();
        switch(a)
        {
            case "1":
	            initialdaily(rpstr);
                document.getElementById("Daily").style.display = 'block';
                
                break;
            case "2":
	            initialweekly(rpstr);
                document.getElementById("Weekly").style.display = 'block';                            
                break;
            case "3":
	            initialmonthly(rpstr);
                document.getElementById("Monthly").style.display = 'block';                            
                break;
            case "4":
	            initialyearly(rpstr);
                document.getElementById("Yearly").style.display = 'block';                            
                break;
            case "5":
            
                document.getElementById('flatCalendarDisplay').innerHTML = "";  // clear the div values
                showFlatCalendar(1, dFormat,document.getElementById("StartDate").value);  //for custom calendar display      
	            initialcustomly(rpstr);
                document.getElementById("Custom").style.display = 'block';                            
                document.getElementById("RangeRow").style.display = 'none';
                break;            
        }
    }      
}

function initial()
{

    var setuphr;
    var setupmin;
    var setupap;
    var teardownhr;
    var teardownmin;
    var teardownap;
    
    document.getElementById("hdnSetupTime").value = document.getElementById("SetupTime_Text").value;
    document.getElementById("hdnTeardownTime").value = document.getElementById("TeardownTime_Text").value;
    
	if (document.getElementById("Recur").value=="") {
		timeMin = document.getElementById("confStartTime_Text").value.split(":")[1].split(" ")[0] ; 
		timeDur = getConfDurNET(document.frmSettings2,dFormat);
		
		if("<%=Session["timeFormat"].ToString()%>" == '0')
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("confStartTime_Text").value.split(":")[0] + "&" + timeMin + "&" + "-1" + "&" + timeDur + "#"
		else
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("confStartTime_Text").value.split(":")[0] + "&" + timeMin + "&" + document.getElementById("confStartTime_Text").value.split(" ")[1] + "&" + timeDur + "#"
		
		tmpstr += "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#"
		tmpstr += document.getElementById("confStartDate").value + "&1&-1&-1";

 		document.frmSettings2.RecurValue.value = tmpstr;
		document.frmSettings2.RecurPattern.value = "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
		
	}
	else
	{
	    setuptime = document.getElementById("hdnBufferStr").value.split("&")[0];
	    teardowntime = document.getElementById("hdnBufferStr").value.split("&")[1];
	    
	    if(document.getElementById("SetupTime_Text"))
	        document.getElementById("SetupTime_Text").value = setuptime;
	    
	    if(document.getElementById("TeardownTime_Text"))
	        document.getElementById("TeardownTime_Text").value = teardowntime;
	}
     
    
	rpstr = AnalyseRecurStr(document.frmSettings2.RecurValue.value);
	//FB 1635 starts
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{
	    if(atint[1] < 12 && atint[3] == "PM")
             atint[1] = atint[1] + 12;
    }
	//FB 1635 Ends
	
	if(atint[1] < 10)
	    atint[1] = "0" + atint[1];
	    
	//FB 1715
	if(atint[2] < 10)
	    atint[2] = "0" + atint[2];
	
	//FB 1799
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	    startstr = (atint[1] == "12" && atint[3] == "AM"? "00" : atint[1]) + ":" + (atint[2]=="0" ? "00" : atint[2]);
	else
	    startstr = atint[1] + ":" + (atint[2]=="0" ? "00" : atint[2]) + " " + atint[3];
	    	
	if(document.getElementById("confStartTime_Text"))
	    document.getElementById("confStartTime_Text").value = startstr;
	
	if (document.frmSettings2.RecurDurationhr) {
		set_select_field (document.frmSettings2.RecurDurationhr, parseInt(atint[4]/60, 10) , true);
		set_select_field (document.frmSettings2.RecurDurationmi, atint[4]%60, true);
	}

	document.frmSettings2.RecurPattern.value = rpstr;	
	if(document.frmSettings2.RecurType)
	    document.frmSettings2.RecurType[rpint[0]-1].checked = true;

    document.frmSettings2.Occurrence.value ="";
    document.frmSettings2.EndDate.value ="";
   
	if (rpint[0] != 5) {
		document.frmSettings2.StartDate.value = rrint[0];

	
		switch (rrint[1]) 
		{
			case 1:
		        document.frmSettings2.EndType.checked = true;
				break;
			case 2:
		        document.frmSettings2.REndAfter.checked = true;
				document.frmSettings2.Occurrence.value = rrint[2];
				break;
			case 3:
		        document.frmSettings2.REndBy.checked = true;
				document.frmSettings2.EndDate.value = rrint[3];
				break;
			default:
				alert(EN_36);
				break;
		}
	}
	
}

function recurTimeChg()
{
   

    var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	
	rshr = aryStart[0].split(":")[0];
	rsmi = aryStart[0].split(":")[1];
	rsap = aryStart[1];

	if (document.frmSettings2.RecurDurationhr) {
		rdhr = parseInt(document.frmSettings2.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmSettings2.RecurDurationmi.value, 10);
		recurduration = rdhr * 60 + rdmi;
        if (recurduration > (maxDuration *60))
            alert(EN_314);
		document.frmSettings2.EndText.value = calEnd(calStart(rshr, rsmi, rsap), recurduration);
	}
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		document.frmSettings2.DurText.value = calDur(document.getElementById("confStartDate").value, document.getElementById("confEndDate").value, rshr, rsmi, rsap, rehr, remi, reap, 1);
	}
}

</script>

<%--daily Script--%>

<script type="text/javascript">
function summarydaily()
{
	dg  = Trim(document.frmSettings2.DayGap.value);
	rp  = "1&";
	rp += (document.frmSettings2.DEveryDay.checked) ? ("1&" + ((dg == "") ? "-1" : dg)) : "2&-1";
	rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

	document.frmSettings2.RecurPattern.value = rp;
}

function initialdaily(rp)   
{
	var rpary = (rp).split("&");
	if (rpary[0] == 1) {
		if (!isNaN(rpary[2]))
			dg = parseInt(rpary[2], 10);
		if ((dg == -1) || isNaN(dg))
			document.frmSettings2.DayGap.value = "";
		else
			document.frmSettings2.DayGap.value = dg;
		if (!isNaN(rpary[1]))
			dt =  parseInt(rpary[1], 10);
		if ((dt == -1) || isNaN(dt))
			rpary[1] = 1;
			
		if(rpary[1] == 1)
		    document.frmSettings2.DEveryDay.checked = true;
		else if(rpary[1] == 2)
		    document.frmSettings2.DWeekDay.checked = true;		
	} 
	else 
	{
		document.frmSettings2.DayGap.value = "";
	    document.frmSettings2.DEveryDay.checked = true;
	}
}

</script>

<%--Weekly Script--%>

<script type="text/javascript">

function summaryweekly ()
{
	wg  = Trim(document.frmSettings2.WeekGap.value);
	rp  = "2&-1&-1&";
	rp += ((wg == "") ? "" : wg) + "&";
	
	var elementRef = document.getElementById("WeekDay");
    var checkBoxArray = elementRef.getElementsByTagName('input');
    var checkedValues = '';
	for (var i = 0; i < checkBoxArray.length; i++)
        rp += checkBoxArray[i].checked ? ((i+1) + ",") : ""  
	
	if (rp.substr(rp.length-1,1)==",")
		rp = rp.substr(0, rp.length-1);

	rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

	document.frmSettings2.RecurPattern.value = rp;
}

function initialweekly(rp)
{
    var elementRef = document.getElementById("WeekDay");
    var checkBoxArray = elementRef.getElementsByTagName('input');
	rpary = (rp).split("&");
	if (rpary[0] == 2) {
		if (!(isNaN(rpary[3])))
			wg = parseInt(rpary[3], 10);
		if ((wg == -1) || isNaN(wg))
			document.frmSettings2.WeekGap.value = "1";
		else
			document.frmSettings2.WeekGap.value = wg;
		if (rpary[4] != -1) 
		{
			wdary = (rpary[4]).split(",");
	    
		    for (i = 0; i < wdary.length; i++) 
		        checkBoxArray[parseInt(wdary[i]) - 1].checked = true
		}
	} 
	else 
	{
		document.frmSettings2.WeekGap.value = "1";
  
		for (var i = 0; i < checkBoxArray.length; i++)
		{
            if ( checkBoxArray[i].checked == true )    
			     checkBoxArray[i].checked = false;
		}
	}
}
</script>

<%--Monthly Script--%>

<script type="text/javascript">

function summarymonthly()
{
	mdn  = Trim(document.frmSettings2.MonthDayNo.value);
	mg1  = Trim(document.frmSettings2.MonthGap1.value);
	mg2  = Trim(document.frmSettings2.MonthGap2.value);
	rp  = "3&-1&-1&-1&-1&";
	rp += (document.frmSettings2.MEveryMthR1.checked) ? ("1&" + ((mdn == "") ? "-1" : mdn) + "&" + ((mg1 == "") ? "-1" : mg1) + "&-1&-1&-1") 
		: ("2&-1&-1&" + (document.frmSettings2.MonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.MonthWeekDay.selectedIndex + 1) + "&" + ((mg2 == "") ? "-1" : mg2));
	rp += "&-1&-1&-1&-1&-1&-1";

	document.frmSettings2.RecurPattern.value = rp;
}

function initialmonthly(rp)
{
	rpary = (rp).split("&");
	if (rpary[0] == 3) {
		for (i=5; i<11; i++) {
			if (!isNaN(rpary[i]))
				rpary[i] = parseInt(rpary[i], 10);
			else
				rpary[i] = -1;
		}
		
		switch (rpary[5]) {
			case 1:
				document.frmSettings2.MEveryMthR1.checked = true;
				document.frmSettings2.MonthDayNo.value = (rpary[6] == -1) ? "" : rpary[6];
				document.frmSettings2.MonthGap1.value = (rpary[7] == -1) ? "" : rpary[7];
				document.frmSettings2.MonthWeekDayNo[0].checked = true;
				document.frmSettings2.MonthWeekDay[0].checked = true;
				document.frmSettings2.MonthGap2.value = "";
				break;
			case 2:
				document.frmSettings2.MEveryMthR2.checked = true;
				document.frmSettings2.MonthDayNo.value = "";
				document.frmSettings2.MonthGap1.value = "";
				if (rpary[8] == -1)
					document.frmSettings2.MonthWeekDayNo[0].selected = true;
				else
					document.frmSettings2.MonthWeekDayNo[rpary[8]-1].selected = true;
				if (rpary[9] == -1)
					document.frmSettings2.MonthWeekDay[0].selected = true;
				else
					document.frmSettings2.MonthWeekDay[rpary[9]-1].selected = true;
				document.frmSettings2.MonthGap2.value = (rpary[10] == -1) ? "" : rpary[10];
				break;
		}
	} 
	else 
	{
		document.frmSettings2.MEveryMthR1.checked = true;
		document.frmSettings2.MonthDayNo.value = "";
		document.frmSettings2.MonthGap1.value = "";
		document.frmSettings2.MonthWeekDayNo[0].checked = true;
		document.frmSettings2.MonthWeekDay[0].checked = true;
		document.frmSettings2.MonthGap2.value = "";
	}
}
</script>

<%--Yearly Script--%>

<script type="text/javascript">

function summaryyearly()
{
	ymd  = Trim(document.frmSettings2.YearMonthDay.value);
	rp  = "4&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&";
	rp += (document.frmSettings2.YEveryYr1.checked) ? ("1&" + (document.frmSettings2.YearMonth1.selectedIndex + 1) + "&" + ((ymd == "") ? "-1" : ymd) + "&-1&-1&-1") 
		: ("2&-1&-1&" + (document.frmSettings2.YearMonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonthWeekDay.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonth2.selectedIndex + 1));
	
	document.frmSettings2.RecurPattern.value = rp;
}

function initialyearly(rp)
{   
	rpary = (rp).split("&");
	if (rpary[0] == 4) 
	{
		for (i=11; i<17; i++) 
		{
			if (!isNaN(rpary[i]))
				rpary[i] = parseInt(rpary[i], 10);
			else
				rpary[i] = -1;
		}
		switch (rpary[11]) 
		{
			case 1:
				document.frmSettings2.YEveryYr1.checked = true;
				if (rpary[12] == -1)
					document.frmSettings2.YearMonth1[0].selected = true;
				else
					document.frmSettings2.YearMonth1[rpary[12]-1].selected = true;
				document.frmSettings2.YearMonthDay.value = (rpary[13] == -1) ? "" : rpary[13];
				document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
				document.frmSettings2.YearMonthWeekDay[0].selected = true;
				document.frmSettings2.YearMonth2[0].selected = true;
				break;
			case 2:
				document.frmSettings2.YEveryYr2.checked = true;
				document.frmSettings2.YearMonth1[0].selected = true;
				document.frmSettings2.YearMonthDay.value = "";
				if (rpary[14] == -1)
					document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
				else
					document.frmSettings2.YearMonthWeekDayNo[rpary[14]-1].selected = true;
				if (rpary[15] == -1)
					document.frmSettings2.YearMonthWeekDay[0].selected = true;
				else
					document.frmSettings2.YearMonthWeekDay[rpary[15]-1].selected = true;
				if (rpary[16] == -1)
					document.frmSettings2.YearMonth2[0].selected = true;
				else
					document.frmSettings2.YearMonth2[rpary[16]-1].selected = true;
				break;
		}
	} 
	else 
	{
		document.frmSettings2.YEveryYr1.checked = true;
		document.frmSettings2.YearMonth1[0].selected = true;
		document.frmSettings2.YearMonthDay.value = "";
		document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
		document.frmSettings2.YearMonthWeekDay[0].selected = true;
		document.frmSettings2.YearMonth2[0].selected = true;
	}
}
</script>

<%--Custom Script--%>

<script type="text/javascript" language="JavaScript">

function removedate(cb)
{
	if (cb.selectedIndex != -1) {
	    cb.options[cb.selectedIndex] = null;
	}
	cal.refresh();
}

function initialcustomly(seldates)
{
	if (seldates != "") 
	{	
		seldatesary = seldates.split("&");
	
		cb = document.getElementById("CustomDate");
		if(seldatesary[0].length > 2)
		{
		    for (var i = 0; i < seldatesary.length; i++)
			    chgOption(cb, seldatesary[i], seldatesary[i], false, false)	
    	
		    if (cal)	
			    cal.refresh();
	    }
	}
}

function summarycustomly()
{
	var selecteddate = "";
	dFormat = document.getElementById("HdnDateFormat").value;	
	SortDates();
	cb = document.getElementById("CustomDate");
	for (i=0; i<cb.length; i++)
		selecteddate += cb.options[i].value + "&";

	document.frmSettings2.CutomDates.value = (selecteddate == "") ? "" : selecteddate.substring(0, selecteddate.length-1);
}

function isOverInstanceLimit(cb)
{
	csl = parseInt("<%=CustomSelectedLimit%>");

	if (!isNaN(csl)) {
		if (cb.length >= csl) {
			alert(EN_211)
			return true;
		}
	}
	
	return false;
}

function SortDates()
{
	var temp;

	datecb = document.frmSettings2.CustomDate;
	
	var dateary = new Array();

	for (var i=0; i<datecb.length; i++) {
		dateary[i] = datecb.options[i].value;
		
		dateary[i] = ( (parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10) ) + "/" + ( (parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10) ) + "/" + ( parseInt(dateary[i].split("/")[2], 10) );
	}

	for (i=0;i<dateary.length-1;i++)
	 	for(j=i+1;j<dateary.length;j++)
			if (mydatesort(dateary[i], dateary[j]) > 0)
			{
				temp = dateary[i];
				dateary[i] = dateary[j];
				dateary[j] = temp;	
			}


	for (var i=0; i<dateary.length; i++) {
		datecb.options[i].text = dateary[i];
		datecb.options[i].value = dateary[i];		
	} 

return false;
}

//-->
</script>

<%--Submit Recurence --%>

<script type="text/javascript">

function SubmitRecurrence()
{
    var chkrecurrence = document.getElementById("chkRecurrence");
    
    
    if (typeof(Page_ClientValidate) == 'function') 
    if (!Page_ClientValidate())
    {
        DataLoading(0);
        return false;
    }
    
    if(!CheckParty())
        return false;
        
    if(chkrecurrence != null && chkrecurrence.checked == true)
    {
        if (validateConfDuration())  
        {
            if(summary())
                return true;
        }
        return false;        
    }
}

function validateDurationHr()
{
    var obj = document.getElementById("RecurDurationhr");
    
    if (obj.value == "") 
         obj.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        obj.focus();    
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "120";
    
    if (obj.value < 0 || eval(obj.value) > eval(maxdur))
    {
        alert(EN_314);
        obj.focus();    
        return false;
    }
    return true;
}

function validateDurationMi()
{
    var obj = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
        obj.value = "0";
    if (isNaN(obj.value))
    {
        alert(EN_314);
        obj.focus();
        return false;
    }
    return true;
}

function validateConfDuration()
{
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
         obj.value = "0";
         
    if (obj1.value == "") 
        obj1.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }
    if (isNaN(obj1.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    if(maxdur == "")
        maxdur = "24";

    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if (totDur < 0 || totDur > eval(maxdur*60))
    {
        alert(EN_314);
        return false;
    }
  
    return true;
}

function summary()
{
     SetRecurBuffer();
     switch(document.frmSettings2.RecPattern.value)
     {
        case "1":
		    summarydaily();
		    break;
		case "2":	
		    summaryweekly();
		    break;
        case "3":
		    summarymonthly();
		    break;
	    case "4":
		    summaryyearly();
		    break;
        case "5":
		    summarycustomly();
		    if(document.frmSettings2.CutomDates.value != "")
		    {
		        var cuDates = document.frmSettings2.CutomDates.value.split("&");
		        if(cuDates.length <=1)
		        {
		            alert("A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.");
		            return false;   
		        }
		    }
		    break;
     }
    
     if(document.frmSettings2.RecPattern.value != "5" && document.frmSettings2.REndAfter.checked)
     {
       if(document.frmSettings2.Occurrence.value <= 1)
       {
           alert("A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.");
           document.frmSettings2.Occurrence.focus();
           return false;   
       }
     }

	var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	
	sh = aryStart[0].split(":")[0];
	sm = aryStart[0].split(":")[1];
	ss = aryStart[1];
	tz = document.getElementById("<%=lstConferenceTZ.ClientID%>").value;
	
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{
	    if(sh != "")
	    { 
	        if(eval(sh) >= 12)
	            ss = "PM";
	        else
	            ss = "AM";
	    }
	}
	
	var aryStup = document.getElementById("SetupTime_Text").value.split(" ");
	
	setuphr = aryStup[0].split(":")[0];
	setupmi = aryStup[0].split(":")[1];
	setupap = aryStup[1];

	var aryTear = document.getElementById("TeardownTime_Text").value.split(" ");
	tearhr = aryTear[0].split(":")[0];
	tearmi = aryTear[0].split(":")[1];
	tearap = aryTear[1];
	
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{ 
	    startTime = sh + ":" + (sm =="0" ? "00" : sm);
	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi);
        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi);
    }
	else
	{
	    startTime = sh + ":" + (sm =="0" ? "00" : sm) + " " + ss;
	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi) + " " + setupap;
        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi) + " " + tearap;
    }
    
    var insStDate = '';
    if (document.frmSettings2.RecPattern.value == "5")
    {
        var cb = document.getElementById("CustomDate");

	    if(cb.length > 0)
	    {
		    insStDate = cb.options[0].value;
        }
    }
    else
    {
        insStDate = document.frmSettings2.StartDate.value;
    }
    startdate = insStDate + " " + startTime;
   
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if(totDur > 1440)
    {
        alert(EN_314);
        return false;
    }
    
    var confEndDt = '';
    var sysdate = dateAddition(startdate,"m",totDur);
    var dateP = sysdate.getDate();
	
	var monthN = sysdate.getMonth() + 1;
	var yearN = sysdate.getFullYear();
	var hourN = sysdate.getHours();
	var minN = sysdate.getMinutes();
	var secN = sysdate.getSeconds();
	var timset = 'AM';
	
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{
	    timset = '';
	}
	else
	{
	    if(hourN == 12)
	    {
	        timset = 'PM';
	    }
	    else if( hourN > 12)
	    {
	         timset = 'PM';
	         hourN = hourN - 12;
	    }
	    else if(hourN == 0)
	    {
	        timset = "AM";
	        hourN = 12;
	    }
	 }
	
	var confDtAlone = monthN + "/" + dateP + "/" + yearN;
	
	confEndDt = monthN + "/" + dateP + "/" + yearN + " "+ hourN + ":" + minN + ":" + secN +" "+ timset;
	if(document.getElementById("chkEnableBuffer").checked == true)	
	{
        setupDate = insStDate + " " + setupTime;
        if(Date.parse(setupDate) < Date.parse(startdate))
        {
            setupDate = confDtAlone + " " + setupTime;
        }
        
        teardownDate = confDtAlone + " " + teardownTime;
        
        if(Date.parse(confEndDt) < Date.parse(teardownDate))
        {
            teardownDate = insStDate + " " + teardownTime;
        }
      
        if(Date.parse(setupDate) < Date.parse(startdate))
        {
            alert(EN_316);
            return false;
        }

        if( (Date.parse(teardownDate) < Date.parse(setupDate)) || (Date.parse(teardownDate) < Date.parse(startdate)) || (Date.parse(teardownDate) > Date.parse(confEndDt)))
        {
             alert(EN_317);
             return false;
        }
      
        var setupDurMin = parseInt(Date.parse(setupDate) - Date.parse(startdate)) / (1000 * 60);
        var tearDurMin = parseInt(Date.parse(confEndDt) - Date.parse(teardownDate)) / (1000 * 60);
      }
    else
    {
        setupDate = startdate;
        teardownDate = confEndDt;
    }  
    
    if(isNaN(setupDurMin))
        setupDurMin = 0;
    
    if(isNaN(tearDurMin))
        tearDurMin = 0;
        
    if(setupDurMin >0 || tearDurMin > 0)
    {
        if( (totDur -(setupDurMin + tearDurMin)) < 15)
        {
            alert(EN_314);
            return false;
        }
    }
    
    document.frmSettings2.hdnBufferStr.value = setupTime + "&" + teardownTime;
	document.frmSettings2.hdnSetupTime.value = setupDurMin;
	document.frmSettings2.hdnTeardownTime.value = tearDurMin;
	
	if (document.frmSettings2.RecurDurationhr)
		dr = parseInt(document.frmSettings2.RecurDurationhr.value, 10) * 60 + parseInt(document.frmSettings2.RecurDurationmi.value, 10);
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		dr = calDur(sh, sm, ss, rehr, remi, reap, 0);
	}
	if ( dr < 15 ) {
		alert(EN_31);
		
		if (document.frmSettings2.RecurDurationhr)
			document.frmSettings2.RecurDurationhr.focus();
		if (document.frmSettings2.RecurEndhr)
			document.frmSettings2.RecurEndhr.focus();
			
		return (false);		
	}

	rv = tz + "&" + sh + "&" + sm + "&" + ss + "&" + dr + "#";
	recurrange = document.frmSettings2.StartDate.value + "&" + (document.frmSettings2.EndType.checked ? "1&-1&-1" : ( document.frmSettings2.REndAfter.checked ? ("2&" + document.frmSettings2.Occurrence.value + "&-1") : (document.frmSettings2.REndBy.checked ? ("3&-1&" + document.frmSettings2.EndDate.value) : "-1&-1&-1") ));
	rv += (document.frmSettings2.RecPattern.value == 5) ? ("5#" + document.frmSettings2.CutomDates.value) : (document.frmSettings2.RecurPattern.value + "#" + recurrange);
	if (frmRecur_Validator(document.frmSettings2.RecurPattern.value, recurrange, (document.frmSettings2.RecPattern.value == 5)?true:false)) {
		document.getElementById("Recur").value = rv;
	
		if (document.frmSettings2.EndText)
			endvalue = document.frmSettings2.EndText.value;
		if (document.frmSettings2.RecurEndhr)
			endvalue = document.frmSettings2.RecurEndhr.value + ":" + document.frmSettings2.RecurEndmi.value + " " + document.frmSettings2.RecurEndap.value;
		document.getElementById("RecurringText").value = recur_discription(rv, endvalue, document.frmSettings2.TimeZoneText.value, document.frmSettings2.StartDate.value,"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
		isRecur();
		BtnCheckAvailDisplay ();
		return true;
	}
}

function frmRecur_Validator(rp, rr, isCustomly)
{
	if (parseInt(document.frmSettings2.Occurrence.value) > parseInt("<%=Application["confRecurrence"]%>"))
	{
		alert("Maximum limit of " + "<%=Application["confRecurrence"]%>" + " instances/occurrences in the recurring series.");
		return false;
	}
	
	if (isCustomly) {
		if (document.frmSettings2.CutomDates.value == "") {
			alert(EN_193)
			return false;
		} else
			return true;
	}

	if (chkPattern(rp))
		if (chkRange(rr))
			return true;
		else
			return false;
	else
		return false;
}

function chkPattern(rp)
{
	rpary = rp.split("&");
	
	switch (rpary[0]) {
		case "1":
			for (i=3; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}

			switch (rpary[1]) {
				case "1":
					rst = isPositiveInt (rpary[2], "interval");
					if (rst == 1)
						return true;
					else
						return false;
					break;
				case "2":
					if (rpary[2] == "-1")
						return true;						
					else {
						alert(EN_37);
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;

		case "2":
			for (i=1; i<3; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=5; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			rst = isPositiveInt (rpary[3], "weeks interval");
			if (rst == 1) {
				if (rpary[4]!="")
					return true;
				else {
					alert(EN_107);
					return false;
				}
			}
			break;
			
		case "3":
			for (i=1; i<5; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=11; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			
			switch (rpary[5]) {
				case "1":
					for (i=8; i<11; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if ( (isPositiveInt (rpary[6], "days interval") == 1) && (isPositiveInt (rpary[7], "months interval") == 1) )
						if ( isMonthDayNo(parseInt(rpary[6], 10)) )
							return true;
						else
							return false;
					else
						return false;
					break;
				case "2":
					for (i=6; i<8; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt(rpary[10], "months interval") == "1")
						return true;						
					else {
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
			
		case "4":
			for (i=1; i<11; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			switch (rpary[11]) {
				case "1":
					for (i=14; i<rpary.length; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt (rpary[13], "date of the month") == 1)
						if ( isYearMonthDay(parseInt(rpary[12], 10), parseInt(rpary[13], 10)) )
							return true;
						else
							return false;
					else
						return false;						
					break;
				case "2":
					for (i=12; i<14; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					return true;
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}
}

var british = false;

function chkRange(rr)
{
	rrary = rr.split("&");
    
    if('<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
        british = true;
    
    var dDate = GetDefaultDate(rrary[0],dFormat);
     
	if ( (!isValidDate(rrary[0])) || ( caldatecompare(dDate, servertoday) == -1 ) ) {		// check start time is reasonable future time
		alert(EN_74);
		document.frmSettings2.StartDate.focus();
		return (false);
	}

	switch (rrary[1]) {
		case "1":
			if ( (rrary[2]!= "-1") && (rrary[3]!= "-1") ) {
				alert(EN_38);
				return false;
			} else 
				return true;
			break;
		case "2":
			if (rrary[3]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( isPositiveInt(rrary[2], "times of occurrences")==1 )
					return true;
				else {
					document.frmSettings2.Occurrence.focus();
					return false;
				}
			}
			break;
		case "3":
			if (rrary[2]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( (!isValidDate(rrary[3])) ||  (caldatecompare(GetDefaultDate(rrary[3],dFormat), servertoday)== -1) ) {
					alert(EN_108);
					document.frmSettings2.EndDate.focus();
					return false;
				} else {
					if ( !dateIsBefore(rrary[0], rrary[3]) ) {
						alert(EN_109);
						document.frmSettings2.EndDate.focus();
						return false;
					} else
						return true;
				}
			}
			return false;
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}	
}

function BtnCheckAvailDisplay ()
{		
	if ( (e = document.getElementById("btnCheckAvailDIV")) != null ) {
		e.style.display = (document.getElementById("Recur").value=="") ? "" : "none";
	}
}
</script>

<script type="text/javascript">

    function removerecur()
    {
	    document.getElementById("Recur").value=""; 
	    document.getElementById("RecurringText").value = "";
	    document.getElementById("hdnRecurValue").value = "";
	    document.getElementById('RecurValue').value = "";
	    isRecur(); 
	    initial();
        
        return false;
    }
    function fnRemoveFiles()
    {   
        var lblflelist = document.getElementById("lblFileList");
        var lblCtrl = document.getElementById(fnRemoveFiles.arguments[0]);
     
        if(lblflelist != null && lblCtrl != null)
        {
            lblflelist.innerText = lblflelist.innerText.replace(lblCtrl.innerText + "; " , "");
        }
     
    }

    function clearError(controlID, clearId) 
    {    
        var ErrorLabel=document.getElementById(clearId);
        if(controlID.value!="")
        {
            ErrorLabel.innerHTML="";//FB 1981
        }
                        
    }

    function fnCheckSpecialCharacters(ID)
	{
		var controlID=document.getElementById(ID);
		//var specialChars = "&<>+'dD"; 
		var specialChars = "&<>+dD"; //FB 1972
		
            for(var i = 0; i < controlID.value.length; i++)
            {
                if (specialChars.indexOf(controlID.value.charAt(i)) != -1 )
                {
                    controlID.focus();
                    return false;
                }
           }
           return true;
	}
 
    function isNumber(ID)
    {
        var numbers = '0123456789';
        var controlID=document.getElementById(ID);
     
        for (i=0; i<controlID.value.length; i++)
        {
            if (numbers.indexOf(controlID.value.charAt(i),0) == -1)
            {
            controlID.focus();
            return false;
            }
        }
        return true;   
    }
 	
	function fnCheckRequiredField()
	{
        var isRequired=true;

        if (document.getElementById("txtAudioDialNo").value == "")//@@@@@
         {
            document.getElementById("lblError2").innerHTML = "Required";//FB 1981
            document.getElementById("txtAudioDialNo").focus();
            //isRequired=false;
            return false;
          }
        else
            return true;
            
        if(document.getElementById("txtConfCode").value=="")
        {
        document.getElementById("lblError3").innerHTML="Required";//FB 1981
        document.getElementById("txtConfCode").focus();
        isRequired=false;
        }
        
        if(document.getElementById("txtLeaderPin").value=="")
        {
        document.getElementById("lblError4").innerHTML="Required";//FB 1981
        document.getElementById("txtLeaderPin").focus();
        isRequired=false;
        }
        if(isRequired==false)
        {
        return false;
        }
        return true;
	}
	
	function fnValidation()
    {
       if(document.getElementById("ConferenceName").value=="")
        {
                    document.getElementById("lblConferenceNameError").innerHTML="Required";//FB 1981
                    document.getElementById("ConferenceName").focus();
                    return false;
        }
     
          var list = document.getElementById("lstAudioParty").value;
        
       if (list != "-1")
        { 
                if(fnCheckRequiredField()==false)
                {
                 return false;
                }

                if(fnCheckSpecialCharacters('txtAudioDialNo')==false)
                {
                return false;
                }
                if(fnCheckSpecialCharacters('txtConfCode')==false)
                {
                return false;
                }
                if(fnCheckSpecialCharacters('txtLeaderPin')==false)
                {
                return false;
                }                    
        }
            else
            {
                document.getElementById("lblError2").innerHTML = "";//FB 1981
                document.getElementById("lblError3").innerHTML = "";//FB 1981
                document.getElementById("lblError4").innerHTML = "";//FB 1981
                return true;
            }
        return true;
            
    }

    //FB 1830 Email Edit - start
    function fnemailalert()
    {
         if ("<%=isEditMode%>" == "1")
         {
              fnShow();
              var hdnalert =   document.getElementById("hdnemailalert");
              var alertreq = hdnalert.value.trim();
              hdnalert.value = "";
              
              if( alertreq == "2") //Alert user for notify on edit
              {
                var alertmessage = 'Do you want to notify all participant(s)? \n\n (Click OK to notify all or CANCEL to notify new participants if any)';
                if (confirm(alertmessage))
                    hdnalert.value = "1"; //send email to all existing participants
                else
                    hdnalert.value = "0"; //dont notify old participants
              }  
          }
          if(document.getElementById('btnDummy'))
          {
            document.getElementById('btnDummy').click();
          }
    }
    //FB 1830 Email Edit - end
      
    function Final()
    {
        if(fnValidation()==false)
        {
            return false;
        }
        if(SubmitRecurrence()==false)
        {
            return false;
        }
        return true;
    }
</script>

<%--Merging Recurrence script End here--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Express Conference</title>
    <%--Merging  Recurrence start--%>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="Organizations/Original/Styles/main.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css"  /><%--FB 1947--%>
    <style type="text/css">
        .dnTableheadingRad label
        {
            font-weight: bold;
            font-size: 10pt;
            color: #556AAF;
            font-family: Arial;
            text-decoration: none;
        }
        .dnTableheadingChk label
        {
            font-size: 11px;
            vertical-align: middle;
            font-weight: bold;
            color: #556AAF;
        }
    </style>
</head>
<body>
    <form id="frmSettings2" runat="server" method="post" enctype="multipart/form-data">
    <asp:ScriptManager ID="ConfScriptManager" runat="server" AsyncPostBackTimeout="600">
    </asp:ScriptManager>
    <input type="hidden" name="hdnextusrcnt" id="hdnextusrcnt" runat="server" />
    <input type="hidden" name="hdnconftype" id="hdnconftype" runat="server" />
    <asp:TextBox ID="txtModifyType" Text="" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTimeCheck" Text="" Visible="false" runat="server"></asp:TextBox>
    <input type="hidden" runat="server" id="CreateBy" value="2" />
    <input type="hidden" name="ModifyType" value="0" />
    <input type="hidden" name="Recur" id="Recur" runat="server" />
    <input id="confPassword" runat="server" type="hidden" />
    <input runat="server" id="RecurFlag" type="hidden" />
    <input runat="server" id="selectedloc" type="hidden" />
    <input id="RecurringText" runat="server" type="hidden" />
    <input type="hidden" name="ConfID" value="" />
    <input type="hidden" name="NeedInitial" value="2" />
    <input type="hidden" name="outlookEmails" value="" />
    <input type="hidden" name="lotusEmails" value="" />
    <input type="hidden" name="hdnSetupTime" id="hdnSetupTime" runat="server" />
    <input type="hidden" name="hdnTeardownTime" id="hdnTeardownTime" runat="server" />
    <input type="hidden" name="hdnBufferStr" id="hdnBufferStr" runat="server" />
    <input type="hidden" runat="server" id="txtHasCalendar" />
    <input type="hidden" id="hdnAudioInsIDs" name="hdnAudioInsIDs" runat="server" />
    <input type="hidden" name="hdnHostIDs" id="hdnHostIDs" runat="server" />
    <input type="hidden" id="hdnAudioInsID" name="hdnAudioInsID" runat="server" />
    <%--Merging Recurrence - Start--%>
    <input type="hidden" id="TimeZoneText" name="TimeZoneText" value="" />
    <input type="hidden" id="TimeZoneValues" name="TimeZoneValues" value="" />
    <input type="hidden" id="EndDateText" name="EndDateText" value="" />
    <input type="hidden" id="RecurValue" name="RecurValue" value="" />
    <input type="hidden" id="RecurPattern" name="RecurPattern" value="" />
    <input type="hidden" id="CustomDates" name="CutomDates" value="" />
    <input type="hidden" id="RecPattern" name="RecPattern" value="" />
    <input type="hidden" id="HdnDateFormat" name="HdnDateFormat" value="<%=format %>" />
    <input type="hidden" id="hdnValue" runat="server" />
    <input type="hidden" id="hdnRecurValue" runat="server" />
    <%--Merging Recurrence - End--%>
    <input runat="server" name="hdnemailalert" id="hdnemailalert" type="hidden" /> <%--FB 1830 Email Edit--%>
    <%--FB 1716--%>
    <input runat="server" id="hdnChange" type="hidden" />
    <input runat="server" id="hdnDuration" type="hidden" />
    
     <%--FB 1911--%>    
     <input runat="server" id="hdnSpecRec" type="hidden" />
    <input type="hidden" name="Recur" id="RecurSpec" runat="server" />
    
    <table width="100%" cellpadding="2" cellspacing="0">
        <tr align="center">
            <td>
                <h3>
                    <asp:Label ID="lblConfHeader" runat="server" Text="Schedule"></asp:Label>&nbsp;a
                    Video Conference
                </h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="80%" border="0">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" CssClass="lblError" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="reqfldstarText" align="right">
                * Required Field
            </td>
        </tr>
        <tr>
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr>
            <td class="subtitlexxsblueblodtext">
                1) Schedule a Video Conference
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0">
                    <tr>
                        <td style="width: 40%;" valign="top">
                            <table width="100%" cellpadding="2" border="0" cellspacing="0">
                                <tr>
                                    <td class="blackblodtext" align="right" width="12%">
                                        <asp:Label ID="lblConfID" runat="server" Visible="False" Width="20%"></asp:Label>
                                        <span id="Field2">Conference Title</span> <span class="reqfldstarText">*</span>
                                    </td>
                                    <td style="height: 20px" valign="top">
                                        &nbsp;<asp:TextBox ID="ConferenceName" onblur="javascript:clearError(this,'lblConferenceNameError')"
                                            runat="server" CssClass="altText" Width="310px" ValidationGroup="Submit"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="ConferenceName"
                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@ --%>
                                        <label id="lblConferenceNameError" runat="server" style="font-weight: bold; color: Red">
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="color: Red">
                                        <asp:Label runat="server" ID="lblAudioNote" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="19%" align="right" nowrap style="font-weight: bold" class="blackblodtext"
                                        valign="top">
                                        Conference Description
                                    </td>
                                    <td align="left">
                                        &nbsp;<asp:TextBox ID="ConferenceDescription" runat="server" MaxLength="2000" class="altText"
                                            Rows="2" TextMode="MultiLine" Width="310PX" Columns="20" Wrap="true"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="ConferenceDescription"
                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters."
                                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--@@@@@--%> <%--FB 1888--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="left">
                                        &nbsp; <a href="#">
                                            <asp:Label ID="aFileUp" runat="server" Text="Attach Files" onclick="javascript:return CheckFiles();"
                                                Style="font-size: 10pt; color: Blue" /></a>
                                        <asp:Label ID="lblFileList" runat="server" class="blackblodtext" Style="font-size: 8pt;
                                            display: none;"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv1">
                                    <td class="blackblodtext" align="right" id="SDateText">
                                        Conference Setup<span class="reqfldstarText">*</span>
                                    </td>
                                    <td valign="top">
                                        <table border="0" width="100%">
                                            <tr>
                                                <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                    width: 403px;" align="center" colspan="2">
                                                    <span id="StartDateArea">
                                                        <asp:TextBox ID="confStartDate" runat="server" CssClass="altText" Width="22%" onblur="javascript:ChangeEndDate(0)"
                                                            OnTextChanged="confStartTime_TextChanged" AutoPostBack="false"></asp:TextBox>
                                                        <img src="image/calendar.gif" alt="Date selector" border="0" width="20" height="20"
                                                            id="cal_triggerd" style="cursor: pointer; vertical-align: middle;" title="Date selector"
                                                            onblur="javascript:a(0)" onclick="return showCalendar('<%=confStartDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />
                                                        <span class="blackblodtext">@</span> </span>
                                                        <%--FB 1716--%>
                                                    <mbcbb:ComboBox ID="confStartTime" runat="server" CssClass="altText" Rows="10" CausesValidation="True"
                                                        Style="width: auto" onblur="javascript:return document.getElementById('hdnChange').value='SU';formatTime('confStartTime_Text');return ChangeEndDate(0);"
                                                        OnTextChanged="confStartTime_TextChanged" OnSelectedIndexChanged="confStartTime_SelectedIndexChanged"
                                                        AutoPostBack="false">
                                                        <%--FB 1715--%>
                                                        <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                    </mbcbb:ComboBox>
                                                    <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="confStartTime"
                                                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regStartTime" runat="server" ControlToValidate="confStartTime"
                                                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                    <asp:RequiredFieldValidator ID="reqStartData" runat="server" ControlToValidate="confStartDate"
                                                        Display="Dynamic" ErrorMessage="Date is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="confStartDate"
                                                        Display="Dynamic" ErrorMessage="Invalid Date " ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv9" runat="server" style="display: none">
                                    <td class="blackblodtext">
                                        Enable Buffer Zone
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkEnableBuffer" runat="server" Checked="true" />
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv6">
                                    <td class="blackblodtext" align="right">
                                        Conference Start <span class="reqfldstarText">*</span>
                                    </td>
                                    <td valign="top">
                                        <table border="0" width="100%">
                                            <tr>
                                                <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                    width: 403px;" align="center" colspan="2">
                                                    <span id="EndDateArea">
                                                        <asp:TextBox ID="SetupDate" runat="server" CssClass="altText" Width="22%" onblur="javascript:ChangeEndDate(0)"
                                                            AutoPostBack="false"></asp:TextBox>
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="Img1" style="cursor: pointer;
                                                            vertical-align: middle;" title="Date selector" onblur="javascript:ChangeEndDate(0)"
                                                            onclick="return showCalendar('<%=SetupDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />
                                                        <span class="blackblodtext">@</span> </span>
                                                        <%--FB 1716--%>
                                                    <mbcbb:ComboBox ID="SetupTime" runat="server" CssClass="altText" Rows="10" CausesValidation="True"
                                                        Style="width: auto" onblur="javascript:return document.getElementById('hdnChange').value='ST';formatTime('SetupTime_Text');return ChangeEndDate(0);"
                                                        AutoPostBack="false">
                                                        <%--FB 1715--%>
                                                        <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                    </mbcbb:ComboBox>
                                                    <asp:RequiredFieldValidator ID="reqSetupStartTime" runat="server" ControlToValidate="SetupTime"
                                                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regSetupStartTime" runat="server" ControlToValidate="SetupTime"
                                                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                    <asp:RequiredFieldValidator ID="reqSetupStartDate" runat="server" ControlToValidate="SetupDate"
                                                        Display="Dynamic" ErrorMessage="Date is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regSetupStartDate" runat="server" ControlToValidate="SetupDate"
                                                        Display="Dynamic" ErrorMessage="Invalid Date <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                    <asp:TextBox runat="server" ID="SetupDateTime" Visible="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv7">
                                    <td class="blackblodtext" align="right">
                                        Conference End <span class="reqfldstarText">*</span>
                                    </td>
                                    <td valign="top">
                                        <table border="0" width="100%">
                                            <tr>
                                                <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                    width: 403px;" align="center" colspan="2">
                                                    <span id="TeardownArea">
                                                        <asp:TextBox ID="TearDownDate" runat="server" CssClass="altText" Width="22%" onblur="javascript:ChangeStartDate(0)"
                                                            AutoPostBack="false"></asp:TextBox>
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="Img4" style="cursor: pointer;
                                                            vertical-align: middle;" title="Date selector" onblur="javascript:ChangeStartDate(0)"
                                                            onclick="return showCalendar('<%=TearDownDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />
                                                        <span class="blackblodtext">@</span> </span>
                                                        <%--FB 1716--%>
                                                    <mbcbb:ComboBox ID="TeardownTime" runat="server" CssClass="altText" Rows="10" CausesValidation="True"
                                                        Style="width: auto" onblur="javascript:document.getElementById('hdnChange').value='TD';formatTime('TeardownTime_Text');return ChangeStartDate(0);"
                                                        AutoPostBack="false">
                                                        <%--FB 1715--%>
                                                        <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                    </mbcbb:ComboBox>
                                                    <asp:RegularExpressionValidator ID="regTearDownStartTime" runat="server" ControlToValidate="TeardownTime"
                                                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                    <asp:RequiredFieldValidator ID="reqTearDownStartDate" runat="server" ControlToValidate="TeardownDate"
                                                        Display="Dynamic" ErrorMessage="Date is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regTearDownStartDate" runat="server" ControlToValidate="TeardownDate"
                                                        Display="Dynamic" ErrorMessage="Invalid Date <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                    <asp:TextBox runat="server" ID="TearDownDateTime" Visible="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv2" style="display: none">
                                    <td class="blackblodtext" align="right" id="EDateText">
                                        End Date/Time<span class="reqfldstarText">*</span>
                                    </td>
                                    <td valign="top">
                                        <table border="0" width="100%">
                                            <tr>
                                                <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                    width: 403px;" align="center" colspan="2">
                                                    <asp:TextBox ID="confEndDate" runat="server" CssClass="altText" Width="30%" onblur="javascript:ChangeStartDate(0)"
                                                        OnTextChanged="confEndTime_TextChanged" AutoPostBack="false"></asp:TextBox>
                                                    <img src="image/calendar.gif" border="0" width="20" height="20" onblur="javascript:ChangeStartDate(0)"
                                                        id="cal_trigger1" style="cursor: pointer; vertical-align: middle;" title="Date selector"
                                                        onclick="return showCalendar('<%=confEndDate.ClientID %>', 'cal_trigger1', 1, '<%=format%>');" />
                                                    <span class="blackblodtext">@ </span>
                                                    <mbcbb:ComboBox ID="confEndTime" runat="server" CssClass="altSelectFormat" onblur="javascript:formatTime('confEndTime_Text');return ChangeStartDate(0)"
                                                        Rows="10" Width="75px" CausesValidation="True" OnSelectedIndexChanged="confEndTime_SelectedIndexChanged"
                                                        OnTextChanged="confEndTime_TextChanged" AutoPostBack="false">
                                                        <%--FB 1715--%>
                                                        <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                    </mbcbb:ComboBox>
                                                    <asp:RequiredFieldValidator ID="reqEndTime" runat="server" ControlToValidate="confEndTime"
                                                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regEndTime" runat="server" ControlToValidate="confEndTime"
                                                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                    <asp:RequiredFieldValidator ID="reqEndDate" runat="server" ControlToValidate="confEndDate"
                                                        Display="Dynamic" ErrorMessage="Date is Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regEndDate" runat="server" ControlToValidate="confEndDate"
                                                        Display="Dynamic" ErrorMessage="Invalid Date <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv8">
                                   <td class="blackblodtext" align="right" >Recurrence</td>
                                   <td> <%--FB 1911 Start--%>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr >
                                                <td style="height: 24px" width="5%" align="left">
                                                   <asp:CheckBox onClick="openRecur()" runat="server" ID="chkRecurrence" />
                                                </td>
                                                <td class="blackblodtext" align="right" nowrap id="SPCell1"><span class="subtitleblueblodtext">&nbsp;OR </span>&nbsp; Special Recurrence &nbsp;</td>
                                                <td id="SPCell2" width="45%">
                                                    <a onClick="openRecur('S')" style="cursor: hand;">
                                                      <img src="image/recurring.gif" width="25" height="25" border="0">
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>   
                                </tr> 
                                <tr id="recurDIV" style="display:none;">
                                    <td align="right" valign="top">
                                         <span class="blackblodtext">Special Recurrence Text</span>
                                    </td>
                                    <td>                                                               
                                        <asp:TextBox ID="RecurText" Enabled="false" runat="server" CssClass="altText" Rows="4" TextMode="MultiLine" Width="70%">No recurrence</asp:TextBox>
                                    </td>
                                </tr><%--FB 1911 End--%>
                                <tr id="divDuration" style="display: none">
                                    <td class="blackblodtext" align="left">
                                        Duration<span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <mbcbb:ComboBox ID="lstDuration" runat="server" CssClass="altSelectFormat" Rows="10"
                                            CausesValidation="True" Width="75px">
                                            <asp:ListItem Value="01:00" Selected="True">01:00</asp:ListItem>
                                            <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                            <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                            <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                            <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                            <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                            <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                            <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                            <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                            <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                            <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                            <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                        </mbcbb:ComboBox>
                                        hh:mm
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv5" style="display: none;">
                                    <td class="blackblodtext" align="right">
                                        Duration
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblConfDuration" runat="server"></asp:Label>
                                        <asp:Button ID="btnRefresh" CssClass="dnButtonLong" Text="Refresh" OnClick="CalculateDuration"
                                            OnClientClick="javascript:DataLoading(1);" runat="server" ValidationGroup="Update" />
                                    </td>
                                </tr>
                                <tr id="DurationRow" style="display: none;">
                                    <td align="right" class="blackblodtext" valign="top">
                                        Duration
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RecurDurationhr" runat="server" CssClass="altText" Width="15%" onchange="javascript: SetRecurBuffer();"
                                            onblur="Javascript:  return validateDurationHr();"></asp:TextBox><font class="blackblodtext">hrs</font>
                                        <asp:TextBox ID="RecurDurationmi" runat="server" CssClass="altText" Width="15%" onchange="javascript: SetRecurBuffer();recurTimeChg();"
                                            onblur="Javascript: return validateDurationMi();"></asp:TextBox><font class="blackblodtext">mins</font>
                                        <br />
                                        <span class="blackblodtext" style="font-size: 8pt">
                                            <%if (enableBufferZone == "0")
                                              { %>
                                            (Maximum Limit is
                                            <%=Application["MaxConferenceDurationInHours"]%>
                                            hours)
                                            <%}
                                              else
                                              { %>
                                            (Maximum Limit is
                                            <%=Application["MaxConferenceDurationInHours"]%>
                                            hours including buffer period)
                                            <%} %>
                                        </span>
                                        <asp:TextBox ID="EndText" runat="server" Style="display: none"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table border="0" align="left" cellspacing="0px" cellpadding="0px">
                                            <tr>
                                                <td align="left">
                                                    <table cellspacing="1px" border="0" width="100%">
                                                        <tr>
                                                            <td nowrap class="blackblodtext" align="right" id="Field4" runat="server" width="145px">
                                                                Requestor Name
                                                            </td>
                                                            <td>
                                                                &nbsp;<asp:TextBox ID="txtApprover4" runat="server" CssClass="altText" Width="110px"
                                                                    Enabled="true"></asp:TextBox>
                                                                <img id="Img8" onclick="javascript:getYourOwnEmailList(3)" src="image/edit.gif" />
                                                                <asp:TextBox ID="hdnApprover4" runat="server" BackColor="Transparent" BorderColor="White"
                                                                    BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                                                <asp:TextBox ID="hdnApproverMail" runat="server" Visible="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="NONRecurringConferenceDiv3">
                                                            <td class="blackblodtext" align="right">
                                                                Time Zone<span class="reqfldstarText">*</span>
                                                            </td>
                                                            <td>
                                                                &nbsp;<asp:DropDownList ID="lstConferenceTZ" runat="server" Width="160px" DataTextField="timezoneName"
                                                                    DataValueField="timezoneID" CssClass="altText">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left">
                                                    <table border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Table ID="tblHost" runat="server" CellPadding="3" CellSpacing="2" Visible="true">
                                                                </asp:Table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" align="left" width="60%">
                            <table width="50%">
                                <tr id="RecurrenceRow">
                                    <td colspan="2" align="left">
                                        <table border="0" style="background-color: #2C398D; width: 450px;" cellspacing="1">
                                            <tr style="background-color: #FFFFFF">
                                                <td align="left" style="width: 31%;" valign="top">
                                                    <table style="width: 100%;" border="0" cellpadding="3">
                                                        <tr>
                                                            <td colspan="2" valign="top">
                                                                <table style="width: 100%;" border="0" cellpadding="3">
                                                                    <tr>
                                                                        <td valign="top" align="left" colspan="2">
                                                                            <span class="blackblodtext">Recurring Pattern</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" nowrap style="width: 5%;" align="left" class="blackblodtext">
                                                                            <span class="blackblodtext">
                                                                                <asp:RadioButtonList ID="RecurType" runat="server" CssClass="blackblodtext" RepeatDirection="Vertical"
                                                                                    OnClick="javascript:return fnShow();" CellPadding="3">
                                                                                </asp:RadioButtonList>
                                                                            </span>
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <%--Daily Recurring Pattern--%>
                                                                            &nbsp;
                                                                            <asp:Panel ID="Daily" runat="server" HorizontalAlign="left" CssClass="blackblodtext">
                                                                                <asp:RadioButton ID="DEveryDay" runat="server" GroupName="RDaily" CssClass="blackblodtext" /><font
                                                                                    class="blackblodtext">Every </font>
                                                                                <asp:TextBox ID="DayGap" CssClass="altText" runat="server" Width="8%" onClick="javaScript: DEveryDay.checked = true;"
                                                                                    onChange="javaScript: summarydaily();"></asp:TextBox>
                                                                                <font class="blackblodtext">day(s)</font>
                                                                                <br />
                                                                                <asp:RadioButton ID="DWeekDay" CssClass="blackblodtext" runat="server" GroupName="RDaily"
                                                                                    onClick="javaScript: DayGap.value=''; summarydaily();" /><font class="blackblodtext">Every
                                                                                        weekday</font>
                                                                            </asp:Panel>
                                                                            <%--Weekly Recurring Pattern--%>
                                                                            <asp:Panel ID="Weekly" runat="server" nowrap Width="360">
                                                                                <font class="blackblodtext">Recur Every </font>
                                                                                <asp:TextBox ID="WeekGap" runat="server" CssClass="altText" Width="8%"></asp:TextBox>
                                                                                <font class="blackblodtext">week(s) on:</font>
                                                                                <asp:CheckBoxList ID="WeekDay" runat="server" CssClass="blackblodtext" RepeatDirection="horizontal"
                                                                                    RepeatColumns="4" CellPadding="2" CellSpacing="3" onClick="javaScript: summaryweekly();">
                                                                                </asp:CheckBoxList>
                                                                            </asp:Panel>
                                                                            <%--Monthly Recurring Pattern--%>
                                                                            <asp:Panel ID="Monthly" runat="server" nowrap Width="360">
                                                                                <asp:RadioButton ID="MEveryMthR1" runat="server" GroupName="GMonthly" onClick="javaScript: MonthGap2.value = ''; summarymonthly();" />
                                                                                <font class="blackblodtext">Day</font>
                                                                                <asp:TextBox ID="MonthDayNo" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR1.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();" class="altText"></asp:TextBox>
                                                                                <font class="blackblodtext">of every</font>
                                                                                <asp:TextBox ID="MonthGap1" runat="server" CssClass="altText" Width="20px" onClick="javaScript: MEveryMthR1.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();"></asp:TextBox>
                                                                                <font class="blackblodtext">month(s)</font>
                                                                                <br />
                                                                                <br />
                                                                                <asp:RadioButton ID="MEveryMthR2" runat="server" GroupName="GMonthly" onClick="javaScript: summarymonthly(); MonthDayNo.value = ''; MonthGap1.value = '';" />
                                                                                <font class="blackblodtext">The</font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDayNo" onClick="javaScript: summarymonthly();">
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDay" onClick="javaScript: summarymonthly();">
                                                                                </asp:DropDownList>
                                                                                <font class="blackblodtext">of every</font>
                                                                                <asp:TextBox ID="MonthGap2" runat="server" CssClass="altText" Width="20px" onClick="javaScript: MEveryMthR2.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();"></asp:TextBox>
                                                                                <font class="blackblodtext">month(s)</font></asp:Panel>
                                                                            <%--Yearly Recurring Pattern--%>
                                                                            <asp:Panel ID="Yearly" runat="server" nowrap Width="360">
                                                                                <asp:RadioButton ID="YEveryYr1" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly();" />
                                                                                <font class="blackblodtext">Every</font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth1" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="YearMonthDay" runat="server" CssClass="altText" Width="8%" onChange="javaScript: summaryyearly();"
                                                                                    onClick="YEveryYr1.checked = true;"></asp:TextBox>
                                                                                <br />
                                                                                <br />
                                                                                <asp:RadioButton ID="YEveryYr2" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly(); document.frmSettings2.YearMonthDay.value = '';" />
                                                                                <font class="blackblodtext">The </font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDayNo" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDay" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <font class="blackblodtext">of </font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth2" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="Custom" runat="server">
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td style="width: 30%">
                                                                                            <div id="flatCalendarDisplay" style="float: left; clear: both;">
                                                                                            </div>
                                                                                            <br />
                                                                                            <div id="Div2" style="font-size: 80%; text-align: center; padding: 2px">
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 25%" align="center">
                                                                                            <span class="blackblodtext">Selected Date</span><br />
                                                                                            <asp:ListBox runat="server" ID="CustomDate" Rows="8" CssClass="altSmall0SelectFormat"
                                                                                                onChange="JavaScript: removedate(this);"></asp:ListBox>
                                                                                            <br />
                                                                                            <%--<asp:ImageButton ID="btnsortDates" ImageUrl="~/en/image/sort.png" runat="server" Text="Sort"  OnClientClick="javascript:return SortDates();" />--%>
                                                                                            <asp:Button ID="btnsortDates" CssClass="altShortBlueButtonFormat" runat="server"
                                                                                                Text="Sort" OnClientClick="javascript:return SortDates();" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            <span><font class="blackblodtext">* click a date to remove it from the list.<font
                                                                                                class="blackblodtext"></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="RangeRow" runat="server">
                                                            <td colspan="2">
                                                                <table border="0" width="100%" cellpadding="5">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <span class="blackblodtext">Range of Recurrence </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="top">
                                                                        <td class="blackblodtext" nowrap style="width: 20%;" align="right">
                                                                            Start
                                                                            <asp:TextBox ID="StartDate" Width="70px" Font-Size="9" CssClass="altText" runat="server"></asp:TextBox>
                                                                            <img alt="" src="image/calendar.gif" border="0" id="cal_triggerd1" style="cursor: pointer;
                                                                                vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=StartDate.ClientID%>', 'cal_triggerd1', 1, '<%=format %>');" />
                                                                        </td>
                                                                        <td>
                                                                            <table width="100%" border="0">
                                                                                <tr>
                                                                                    <td class="blackblodtext" colspan="2">
                                                                                        <asp:RadioButton ID="EndType" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value=''; document.frmSettings2.EndDate.value='';" />
                                                                                        No end date
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="blackblodtext" nowrap style="width: 9%;">
                                                                                        <asp:RadioButton ID="REndAfter" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.EndDate.value='';" />
                                                                                        End after
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        <asp:TextBox ID="Occurrence" CssClass="altText" Width="70px" runat="server" onClick="javascript: document.frmSettings2.REndAfter.checked=true; document.frmSettings2.EndDate.value='';"></asp:TextBox>
                                                                                        occurrences
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="blackblodtext">
                                                                                        <asp:RadioButton ID="REndBy" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value='';" />
                                                                                        End by
                                                                                    </td>
                                                                                    <td nowrap>
                                                                                        <asp:TextBox ID="EndDate" Width="70px" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)"
                                                                                            CssClass="altText" runat="server" onClick="javascript: document.frmSettings2.REndBy.checked=true; document.frmSettings2.Occurrence.value='';"></asp:TextBox>
                                                                                        <img alt="" src="image/calendar.gif" border="0" id="cal_trigger2" style="cursor: pointer;
                                                                                            vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=EndDate.ClientID%>', 'cal_trigger2', 1, '<%=format %>');" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="blackblodtext" style="font-size: 8pt;">
                                                                            Note: Maximum limit of
                                                                            <%=Application["confRecurrence"]%>
                                                                            instances/occurrences in the recurring series.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--Below script is to hide Recurrence Div during page load--%>

                <script language="javascript" type="text/javascript">    
                
                    document.getElementById("regStartTime").controltovalidate = "confStartTime_Text";
                    document.getElementById("reqStartTime").controltovalidate = "confStartTime_Text";
                    document.getElementById("regEndTime").controltovalidate = "confEndTime_Text";
                    document.getElementById("reqEndTime").controltovalidate = "confEndTime_Text";
                    document.getElementById("regSetupStartTime").controltovalidate = "SetupTime_Text"; 
                    document.getElementById("reqSetupStartTime").controltovalidate = "SetupTime_Text";
                    document.getElementById("regTearDownStartDate").controltovalidate = "TeardownTime_Text";
                                
                    if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
                    {
                        isRecur();
                        AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
                        st = calStart(atint[1], atint[2], atint[3]);
                        et = calEnd(st, parseInt(atint[4], 10));
                        document.getElementById("RecurringText").value =  recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et, "Eastern Standard Time", Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
                    }
                    isRecur();
                    ChangeImmediate();

                    if ("<%=isInstanceEdit%>" == "Y" )
                    {  
                        document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
                    }
                    
                    if ("<%=timeZone%>" == "0" ) 
                        document.getElementById("NONRecurringConferenceDiv3").style.display = "none";
                                               
                    if ("<%=client.ToString().ToUpper() %>" == "MOJ")
                    {
                        document.getElementById("trPublic").style.display = "none"; 
                        document.getElementById("trConfType").style.display = "none"; 
                        document.getElementById("NONRecurringConferenceDiv6").style.display =  "none"; 
                        document.getElementById("NONRecurringConferenceDiv7").style.display =  "none"; 
                        
                    }
                </script>

            </td>
        </tr>
        <tr>
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="subtitlexxsblueblodtext">
                            2) Select Rooms
                        </td>
                    </tr>
                </table>
                <table border="0" width="100%">
                    <tr>
                        <td width="16%">
                        </td>
                        <td align="left">
                            <table border="0" width="60%">
                                <tr>
                                    <td>
                                        <%--<input type="image" id="RoomPoupup" value="RoomPoupup" onfocus="this.blur()" validationgroup="Submit" onclick="javascript:return OpenRoom();"  
                                        runat="server" src="~/en/image/PressToSelect.png" />--%>
                                        <input type="button" id="RoomPoupup" value="Press to Select Rooms" class="altLongBlueButtonFormat" 
                                            onfocus="this.blur()" validationgroup="Submit" onclick="javascript:return OpenRoom();"
                                            runat="server" />
                                    </td>
                                    <td align="right" class="blackblodtext" style="display: none">
                                        <a onclick="javascript:goToCal(); return false;" href="#">Check Room Calendar</a>
                                        <img src="image/calendar.gif" border="0" hspace="0" vspace="0" id="Img2" style="cursor: pointer;
                                            vertical-align: bottom;" title="Date selector" onclick="javascript:goToCal(); return false;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table border="0" width="100%">
                    <tr>
                        <td width="16%">
                        </td>
                        <td align="left">
                            <asp:UpdatePanel ID="UpdatePanelSelectedRooms" runat="server" RenderMode="Inline"
                                UpdateMode="Conditional">
                                <Triggers>
                                </Triggers>
                                <ContentTemplate>
                                    <asp:ListBox ID="seltdRooms" Enabled="false" runat="server" Width="60%" CssClass="altText"
                                        SelectionMode="multiple"></asp:ListBox>
                                    <br />
                                    <asp:Button ID="btnRoomSelect" runat="server" Style="display: none" OnClick="btnRoomSelect_Click"
                                        CssClass="dnButtonLong"></asp:Button>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <table border="0" width="100%">
                    <tr>
                        <td>
                            <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" id="tblAudioser" runat="server">
                    <tr>
                        <td class="subtitlexxsblueblodtext">
                            3) Audio Conferencing (If Applicable)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" width="100%">
                                <tr>
                                    <td width="13%">
                                    </td>
                                    <td align="left">
                                        <table cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td align="left" valign="middle" style="width: 6%;">
                                                </td>
                                                <td valign="middle">
                                                    <table border="0" width="80%">
                                                        <tr>
                                                            <td colspan="3" class="blackblodtext" align="left" valign="middle">
                                                                Audio Participants
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" align="left" valign="middle" class="blackblodtext">
                                                                <asp:DropDownList runat="server" ID="lstAudioParty" CssClass="altSelectFormat">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="td_5" class="blackblodtext" align="center" valign="middle">
                                                                Address/Phone
                                                            </td>
                                                            <td id="tdlblConfcode" runat="server" class="blackblodtext" align="center" valign="middle"><%--FB 1982--%>
                                                                <asp:Label ID="lblConfCode" runat="server" Text="Conference Code"></asp:Label>
                                                            </td>
                                                            <td id="tdlblLeaderPin" runat="server" class="blackblodtext" align="center" valign="middle"><%--FB 1982--%>
                                                                <asp:Label ID="lblLeaderPin" runat="server" Text="Leader PIN"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtAudioDialNo" onblur="javascript:clearError(this,'lblError2')"
                                                                    CssClass="altText" runat="server" Enabled="true"></asp:TextBox>
                                                            </td>
                                                            <td id="tdConfCode" runat="server"><%--FB 1982--%>
                                                                <asp:TextBox ID="txtConfCode" class="altText" onblur="javascript:clearError(this,'lblError3')"
                                                                    runat="server" Enabled="true"></asp:TextBox>
                                                            </td>
                                                            <td id="tdLeaderPin" runat="server"><%--FB 1982--%>
                                                                <asp:TextBox ID="txtLeaderPin" CssClass="altText" onblur="javascript:clearError(this,'lblError4')"
                                                                    runat="server" Enabled="true"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><%--FB 1972--%>
                                                                <asp:RegularExpressionValidator ID="RegtxtAudioDialNo" ControlToValidate="txtAudioDialNo"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid IP Address"
                                                                    ValidationExpression="^[^&<>+'dD][0-9'.':,\x22;@]*$" ></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtAudioDialNo"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid IP Address"
                                                                    ValidationExpression="^[^&<>+'dD][0-9'.]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>--%>     
                                                                <br />
                                                                <label id="lblError2" runat="server" style="font-weight: bold; color: Red">
                                                                </label>
                                                            </td>
                                                            <td id="tdRegConfCode" runat="server"><%--FB 1982--%>
                                                                <asp:RegularExpressionValidator ID="RegtxtConfCode" ControlToValidate="txtConfCode"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid Conference Code"
                                                                    ValidationExpression="^[^&<>+'dD][0-9'.]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                <br />
                                                                <label id="lblError3" runat="server" style="font-weight: bold; color: Red">
                                                                </label>
                                                            </td>
                                                            <td id="tdRegLeaderPin" runat="server"><%--FB 1982--%>
                                                                <asp:RegularExpressionValidator ID="RegtxtLeaderPin" ControlToValidate="txtLeaderPin"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid Leader PIN"
                                                                    ValidationExpression="^[^&<>+'dD][0-9'.]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                <br />
                                                                <label id="lblError4" runat="server" style="font-weight: bold; color: Red">
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <table id="webConf" runat="server" width="100%" border="0">
                    <tr>
                        <td class="subtitlexxsblueblodtext">
                            4) Web Conference Instructions
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td width="62px">
                                    </td>
                                    <td align="left">
                                        <asp:Table ID="tblWeb" runat="server" CellPadding="3" CellSpacing="2" Visible="true">
                                        </asp:Table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblSplinst" runat="server" width="100%">
                    <tr>
                        <td class="subtitlexxsblueblodtext">
                            4) Special Instructions <span class="blackblodtext" style="font-size: 8pt">(Additional
                                Support Requirements)</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table width="100%" border="0">
                                <tr>
                                    <td width="16%">
                                    </td>
                                    <td align="left">
                                        <asp:Table ID="tblSpecial" runat="server" CellPadding="3" CellSpacing="2" Visible="true">
                                        </asp:Table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="subtitlexxsblueblodtext" nowrap>
                            5) Invite Participants
                        </td>
                    </tr>
                    <tr>
                        <td width="17%" height="30px">
                        </td>
                        <td align="left">
                            <%--<input type="image" id="VRMLookup" value="VRMLookup" runat="server" src="~/en/image/PreesToInvite.png" onclick="javascript:getYourOwnEmailListNET();return false;"/>
                            <input type="image" id="btnAddNewParty" value="btnAddNewParty" onfocus="this.blur()"
                            runat="server" src="~/en/image/PressToParticipant.png" onclick="javascript:addNewPartyNET(1);return false;" /> --%>
                            <%--FB 1822--%>
                            <input type="button" id="VRMLookup" value="Press to Invite" class="altShortBlueButtonFormat"
                                onfocus="this.blur()" runat="server" onclick="javascript:getYourOwnEmailListNET();return false;" />
                            <input type="button" id="btnAddNewParty" value="Add New Participant" class="altLongBlueButtonFormat"
                                onfocus="this.blur()" runat="server" onclick="javascript:addNewPartyNET(1);return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td height="21" align="left" valign="top" width="100" class="blackblodtext">
                        </td>
                        <td bordercolor="#0000ff" colspan="4" align="left">
                            <table border="0" cellpadding="2" cellspacing="0" width="60%">
                                <tr>
                                    <td width="40%" valign="top" align="left">
                                        <iframe align="left" height="150" name="ifrmPartylist" runat="server" id="ifrmPartylist"
                                            src="settings2partyNET.aspx?wintype=ifr" valign="top" width="100%">
                                            <p>
                                                go to <a href="settings2partyNET.aspx?wintype=ifr">Participants</a></p>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                            <%--the following 2 controls have been added to confirm the validation on next and previous click under CheckFiles function--%>
                            <asp:TextBox ID="txtTempUser" runat="server" Visible="false" Text="User"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqUser" ControlToValidate="txtTempUser" runat="server"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <table border="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="100%" valign="top" align="left">
                                        <asp:TextBox ID="txtUsersStr" runat="server" Width="0px" ForeColor="transparent"
                                            BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                                        <asp:TextBox ID="txtPartysInfo" runat="server" Width="0px" ForeColor="Black" BackColor="transparent"
                                            BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Table runat="server" Width="100%" CellSpacing="5" CellPadding="2" ID="tblConflict"
                    Style="display: none;">
                    <asp:TableRow ID="TableRow11" runat="server">
                        <asp:TableCell ID="TableCell8" HorizontalAlign="Center" runat="server">
                           <h4>Resolve the following conflicts:</h4>
                        </asp:TableCell></asp:TableRow>
                    <asp:TableRow ID="TableRow12" runat="server">
                        <asp:TableCell ID="TableCell9" HorizontalAlign="Center" runat="server">
                            <asp:DataGrid Width="80%" ID="dgConflict" runat="server" AutoGenerateColumns="False"
                                OnItemDataBound="InitializeConflict">
                                <Columns>
                                    <asp:BoundColumn DataField="formatDate" HeaderText="Date">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="conflict" HeaderText="Conflict" ItemStyle-Wrap="false">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Setup Time">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictStartTime"
                                                DataValueField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>' DataTextField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>'>
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            Start Time
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictSetupTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            Teardown time
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictTeardownTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="End time">
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictEndTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Action">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnViewConflict" CssClass="altShortBlueButtonFormat"
                                                Text="View" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="startHour" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startSet" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="durationMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startDate" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkConflictDelete" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:TemplateColumn>
                                </Columns>
                                <AlternatingItemStyle CssClass="tablebody" />
                                <ItemStyle CssClass="tableBody" />
                                <HeaderStyle CssClass="tableHeader" />
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell ID="TableCell1" HorizontalAlign="Center" runat="server">
                            <table width="100%">
                                <tr align="center">
                                    <td align="center">
                                        <input type='submit' name='SoftEdgeTest1' style='width: 0px; display: none' />
                                        <%--<asp:ImageButton ImageUrl="~/en/image/Submit.png" ID="btnConflicts"  runat="server"  Text="Set Custom Instances" OnClick="SetConferenceCustom"/>--%>
                                        <asp:Button ID="btnConflicts" runat="server" CssClass="altShortBlueButtonFormat"
                                            Text="Set Custom Instances" OnClick="SetConferenceCustom" />
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </asp:Panel>
            </td>
        </tr>
        <tr align="center">
            <td align="center">
                <table width="100%">
                    <tr align="center">
                        <td align="center">
                            <input type='submit' name='SoftEdgeTest1' style='width: 0px; display: none' />                            
                            <%--<asp:ImageButton ImageUrl="~/en/image/Submit.png" ID="btnConfSubmit" onfocus="this.blur()" runat="server" Text="Submit" OnClientClick="javascript:return Final();" OnClick="SetConference" />--%>
                            <asp:Button ID="btnConfSubmit" CssClass="altShortBlueButtonFormat" onfocus="this.blur()"
                                runat="server" Text="Submit" OnClientClick="javascript:return Final();" OnClick="EmailDecision" />
                        </td>
                    </tr>
                    <tr align="right" style="display:none"> <%--FB 1830 Email Edit--%>
                        <td align="right" width="1px">
                            <asp:Button ID="btnDummy" BackColor="Transparent" runat="server" OnClick="SetConference" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <ajax:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="aFileUp"
                    PopupControlID="PanloadFiles" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                    CancelControlID="CloseFilePop" BehaviorID="aFileUp">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="PanloadFiles" Width="500px" Height="110px" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode" >
                    <table cellpadding="3" cellspacing="0" border="0" width="80%" class="treeSelectedNode" >
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <Triggers>
                                    </Triggers>
                                    <ContentTemplate>
                                        <div align="center" id="Div4" style="width: 500Px; height: 110px; vertical-align: middle;border:none;"
                                            class="treeSelectedNode" >
                                            <table cellpadding="3" cellspacing="0" border="0" width="80%">
                                                <tr id="trFile1" runat="server">
                                                    <td align="left" class="blackblodtext">
                                                        File1
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="50%">
                                                                    <input type="file" id="FileUpload1" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />
                                                                    <asp:Label ID="lblUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%" align="left">
                                                                    <%--<asp:ImageButton ID="btnRemove1" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />--%>
                                                                    <asp:Button ID="btnRemove1" Text="Remove" CssClass="altShortBlueButtonFormat" Visible="false"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="1" />
                                                                    <asp:Label ID="hdnUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trFile2" runat="server">
                                                    <td align="left" class="blackblodtext">
                                                        File2
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="50%">
                                                                    <input type="file" id="FileUpload2" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />
                                                                    <asp:Label ID="lblUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%">
                                                                    <%--<asp:ImageButton ID="btnRemove2" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="2" />--%>
                                                                    <asp:Button ID="btnRemove2" Text="Remove" CssClass="altShortBlueButtonFormat" Visible="false"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="2" />
                                                                    <asp:Label ID="hdnUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trFile3" runat="server">
                                                    <td align="left" class="blackblodtext">
                                                        File3
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="50%">
                                                                    <input type="file" id="FileUpload3" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />
                                                                    <asp:Label ID="lblUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%">
                                                                    <%--<asp:ImageButton ID="btnRemove3" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="3" />--%>
                                                                    <asp:Button ID="btnRemove3" Text="Remove" Visible="false" CssClass="altShortBlueButtonFormat"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="3" />
                                                                    <asp:Label ID="hdnUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                             
                    <br />
                    <%--<asp:ImageButton ID="btnUploadFiles" OnClick="UploadFiles" onfocus="this.blur()" ImageUrl="~/en/image/upload.png" runat="server" />--%>
                    <%--<asp:ImageButton ID="CloseFilePop" OnClientClick="CheckUploadedFiles();" onfocus="this.blur()"
                                                ImageUrl="~/en/image/close.png" runat="server" />--%>
                    <asp:Button ID="btnUploadFiles" Text="Upload Files" CssClass="altShortBlueButtonFormat"
                        OnClick="UploadFiles" onfocus="this.blur()" runat="server" />
                    <asp:Button ID="CloseFilePop" Text="Close" CssClass="altShortBlueButtonFormat" OnClientClick="CheckUploadedFiles();"
                        onfocus="this.blur()" runat="server" />
                     </td>
                    </tr>
                 </table>
                </asp:Panel>

                <script type="text/javascript" language="javascript">
                                        
                                        function CheckFiles()
                                        {
                                          if (typeof(Page_ClientValidate) == 'function') 
                                            if (!Page_ClientValidate())
                                            {
                                                DataLoading(0);
                                                return false;
                                            }
                                            
                                                                                          
                                        if (document.getElementById("FileUpload1"))
                                        {
                                            if(document.getElementById("FileUpload1").value)
                                            {
                                                if (confirm("Do you want to upload the browsed file?"))
                                                {
                                                    document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                                }
                                                else
                                                {
                                                    DataLoading(0);            
                                                    return false;
                                                }
                                            }
                                        }
                                        if (document.getElementById("FileUpload2"))
                                        {
                                            if(document.getElementById("FileUpload2").value)
                                            {
                                                if (confirm("Do you want to upload the browsed file?"))
                                                {
                                                    document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                                }
                                                else
                                                    DataLoading(0);            
                                            }
                                        }
                                        if (document.getElementById("FileUpload3"))
                                        {
                                            if(document.getElementById("FileUpload3").value)
                                            {
                                                if (confirm("Do you want to upload the browsed file?"))
                                                {
                                                    document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                                }
                                                else
                                                    DataLoading(0);            
                                            }
                                        }   

                                         return true;
	                                         }                               
                                        
                </script>

            </td>
        </tr>
        <tr>
            <asp:Button ID="CustomTrigger" runat="server" style="display:none;"  />
            <td>
                <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="CustomTrigger" Enabled="true"
                    PopupControlID="PopupRoomPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                    CancelControlID="ClosePUp" BehaviorID="RoomPopUp">
                </ajax:ModalPopupExtender>
                <%--FB 1858,1835 Start--%>
                <asp:Panel ID="PopupRoomPanel" Width="60%" Height="60%" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode" ScrollBars="Auto">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%" class="treeSelectedNode" >
                        <tr>
                            <td valign="top" align ="right">
                            <asp:ImageButton ID="ClosePUp" ImageUrl ="../image/Close.JPG" runat="server" OnClientClick="javascript:OpenSetRoom();" ToolTip="Close"></asp:ImageButton> 
                <%--FB 1858,1835 End--%>
                    <asp:UpdatePanel ID="UpdatePanelRooms" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <Triggers>
                        </Triggers>
                        <ContentTemplate>
                            <asp:Button ID="refreshrooms" runat="server" OnClick="RefreshRoom" Style="display: none;" />
                            <div align="center" id="conftypeDIV" style="width: 100%; height: 100%;border:none;" class="treeSelectedNode"><%--FB 1858,1835--%>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right" valign="top" width="10%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" width="78%">
                                            <table border="0" style="width: 100%">
                                                <tr>
                                                    <td valign="top" align="left" width="80">
                                                        <%-- <asp:ImageButton ID="btnCompare" onfocus="this.blur()"  OnClientClick="javascript:compareselected();" ImageUrl="~/en/image/compare.png" 
                                                        runat="server" />--%>
                                                        <asp:Button ID="btnCompare" Text="Compare" CssClass="altShortBlueButtonFormat" onfocus="this.blur()"
                                                            OnClientClick="javascript:compareselected();" runat="server" />
                                                    </td>
                                                    <td nowrap valign="top" align="left" class="blackblodtext">
                                                        <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                                            RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                                                            <asp:ListItem Selected="True" Value="1"><span class="blackblodtext">Level View</span></asp:ListItem>
                                                            <asp:ListItem Value="2"><span class="blackblodtext">List View</span></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%" align="right" valign="top" class="blackblodtext">
                                            <b>Rooms</b><br>
                                            <font size="1">Click room name to show room resource details.</font>
                                        </td>
                                        <td width="78%" align="left" style="font-weight: bold; font-size: small; color: green;
                                            font-family: arial" valign="top">
                                            <asp:Panel ID="pnlLevelView" runat="server" Height="300px" Width="100%" ScrollBars="Auto"
                                                BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left"><%--FB 1858,1835--%>
                                                <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="95%"
                                                    ShowCheckBoxes="All" onclick="javascript:getRooms(event)" ShowLines="True" Width="94%"
                                                    OnTreeNodeCheckChanged="treeRoomSelection_TreeNodeCheckChanged" 
                                                    OnSelectedNodeChanged="treeRoomSelection_SelectedNodeChanged">
                                                    <NodeStyle CssClass="treeNode" />
                                                    <RootNodeStyle CssClass="treeRootNode" />
                                                    <ParentNodeStyle CssClass="treeParentNode" />
                                                    <LeafNodeStyle CssClass="treeLeafNode" />
                                                </asp:TreeView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%"
                                                HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="arial"
                                                Font-Size="Small" ForeColor="Green"><%--FB 1858,1835--%>
                                                <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelect('lstRoomSelection',this);" /><font
                                                    size="2"> Select All</font>
                                                <br />
                                                <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller"
                                                    ForeColor="ForestGreen" onclick="javascript:getValues(event)" Font-Names="arial"
                                                    RepeatLayout="Flow">
                                                </asp:CheckBoxList>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"
                                                Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left"
                                                Direction="LeftToRight" Font-Size="Small"><%--FB 1858,1835--%>
                                                <table>
                                                    <tr align="center">
                                                        <td>
                                                            You have no Room(s) available for the selected Date/Time.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:TextBox runat="server" ID="txtTemp" Text="test" Visible="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="false" ControlToValidate="txtTemp"
                                                runat="server"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <%--<asp:ImageButton ID="ClosePUp" OnClientClick="OpenSetRoom()" ImageUrl="~/en/image/close.png" runat="server" />--%>
                    <%--<asp:Button ID="ClosePUp" Text="Close" CssClass="altShortBlueButtonFormat" OnClientClick="OpenSetRoom()"
                        runat="server" />--%><%--FB 1858,1835--%>
                         </td>
                    </tr>
                </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <ajax:ModalPopupExtender ID="SubModalPopupExtender" TargetControlID="adisNone" runat="server"
                    PopupControlID="panSubmit" DropShadow="false" BackgroundCssClass="modalBackground"
                    CancelControlID="showConfMsgImg">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="panSubmit" Width="40%" Height="40%" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode"><%--FB 1858,1835--%>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <Triggers>
                        </Triggers>
                        <ContentTemplate>
                            <div align="center" id="Div1" style="width: 100%; height: 100%;" class="treeSelectedNode"><%--FB 1858,1835--%>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%" style="vertical-align: middle;
                                    height: 40%;">
                                    <tr>
                                        <td align="center" style="vertical-align: middle">
                                            <asp:Label ID="showConfMsg" runat="server" CssClass="lblError"></asp:Label>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <%--<asp:ImageButton ID="showConfMsgImg" ImageUrl="~/en/image/close.png" OnClientClick="javascript:window.location.replace('ExpressConference.aspx?t=n');" runat="server"/>--%>
                                            <asp:Button ID="showConfMsgImg" Text="Close" CssClass="altShortBlueButtonFormat"
                                                OnClientClick="javascript:window.location.replace('ExpressConference.aspx?t=n');"
                                                runat="server" />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="adisNone" runat="server" style="display: none" />
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
   
        if(document.getElementById("confStartTime_Text"))
        {
            var confstarttime_text = document.getElementById("confStartTime_Text");
            confstarttime_text.onblur  =  function()
            {
                document.getElementById('hdnChange').value = 'ST'; //FB 1716
                if(formatTime('confStartTime_Text','regStartTime')) //FB 1715
                    ChangeEndDate(0);
            };
        }
        if(document.getElementById("confEndTime_Text"))
        {
            var confendtime_text = document.getElementById("confEndTime_Text");
            confendtime_text.onblur  =  function()
            {
                 document.getElementById('hdnChange').value = 'ET'; //FB 1716
                 if(formatTime('confEndTime_Text','regEndTime')) //FB 1715
                    ChangeStartDate(0);
            };
        }
        
        if(document.getElementById("SetupTime_Text"))
        {
            var setuptime_text = document.getElementById("SetupTime_Text");
            setuptime_text.onblur  =  function()
            {
               document.getElementById('hdnChange').value = 'SU'; //FB 1716
               if(formatTime('SetupTime_Text', 'regSetupStartTime')) //FB 1715
                    ChangeEndDate(0);
            };
        }
       if(document.getElementById("TeardownTime_Text"))
        {
            var teardowntime_text = document.getElementById("TeardownTime_Text");
            teardowntime_text.onblur  =  function()
            {
                document.getElementById('hdnChange').value = 'TD'; //FB 1716
                if(formatTime('TeardownTime_Text','regTearDownStartTime')) //FB 1715
                    ChangeStartDate(0);
            };
        }
      
      if(document.getElementById("<%=rdSelView.ClientID%>"))
      {
        if(!document.getElementById("<%=rdSelView.ClientID%>").disabled)
            document.getElementById("<%=rdSelView.ClientID%>").onclick= function()
            { DataLoading(1);};
      } 
      
      function openconflict()
      {
         if(document.getElementById("dgconflict"))
          {
            document.getElementById("dgconflict").focus();
            
          }
      } 
    </script>

    <script type="text/javascript">
      //SelectAudioParty();//@@@@@
	  fnEnableBuffer();
	  if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	  {
	    if (document.frmSettings2.EndText)
		    document.frmSettings2.EndText.disabled = true;
	    if (document.frmSettings2.DurText)
		    document.frmSettings2.DurText.disabled = true;

	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value;
        
        var chkrecurrence = document.getElementById("chkRecurrence");
         
        if(document.getElementById("Recur").value != "" || (chkrecurrence && chkrecurrence.checked == true))
        {
            var chkrecurrence = document.getElementById("chkRecurrence");
            if(chkrecurrence)
                chkrecurrence.checked = true;
            document.getElementById("hdnRecurValue").value = 'R'
            initial();
           
            fnShow();
        }
        
         fnEnableBuffer();
     } 
     
     function OpenRoom()
     {     
        var obj = document.getElementById("ConferenceName");     
        if(obj)
        {
            if(obj.value == "")
            {
                document.getElementById("lblConferenceNameError").innerHTML="Required";//FB 1981
                obj.focus();
                return false;
            }
        }
            
        if(document.getElementById("refreshrooms"))
            document.getElementById("refreshrooms").click();
                  
        SubmitRecurrence();
        
        var popup2 = $find('RoomPopUp');
        if(popup2)
            popup2.show();
     }
     
     
    //FB 1911 - Start
    if ('<%=isEditMode%>' == "1" || '<%=Application["Client"].ToString().ToUpper()%>' != "MAYO")
    {
        if(document.getElementById("SPCell1"))
            document.getElementById("SPCell1").style.display = 'None'
        
        if(document.getElementById("SPCell2"))
            document.getElementById("SPCell2").style.display = 'None'
    }    
    
	if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	{
        if(document.getElementById("RecurSpec").value != "")
        {
           showSpecialRecur();
           document.getElementById("RecurText").value = document.getElementById("RecurringText").value;
        }
    }
    
    
            
    //FB 1911 - End
    </script>


</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--FB 1822--%>
    <%--<script type="text/javascript" src="inc/softedge.js"></script>--%>

